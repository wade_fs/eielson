package com.google.android.gms.measurement.internal;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.internal.measurement.C2655oc;
import com.google.android.gms.internal.measurement.C2774wa;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.b4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2984b4 extends C2995c3 {

    /* renamed from: c */
    private String f4966c;

    /* renamed from: d */
    private String f4967d;

    /* renamed from: e */
    private int f4968e;

    /* renamed from: f */
    private String f4969f;

    /* renamed from: g */
    private long f4970g;

    /* renamed from: h */
    private long f4971h;

    /* renamed from: i */
    private List<String> f4972i;

    /* renamed from: j */
    private int f4973j;

    /* renamed from: k */
    private String f4974k;

    /* renamed from: l */
    private String f4975l;

    /* renamed from: m */
    private String f4976m;

    C2984b4(C3081j5 j5Var, long j) {
        super(j5Var);
        this.f4971h = j;
    }

    @WorkerThread
    /* renamed from: I */
    private final String m8400I() {
        if (!C2655oc.m7018b() || !mo19013h().mo19146a(C3135o.f5401J0)) {
            try {
                Class<?> loadClass = mo19016n().getClassLoader().loadClass("com.google.firebase.analytics.FirebaseAnalytics");
                if (loadClass == null) {
                    return null;
                }
                try {
                    Object invoke = loadClass.getDeclaredMethod("getInstance", Context.class).invoke(null, mo19016n());
                    if (invoke == null) {
                        return null;
                    }
                    try {
                        return (String) loadClass.getDeclaredMethod("getFirebaseInstanceId", new Class[0]).invoke(invoke, new Object[0]);
                    } catch (Exception unused) {
                        mo19015l().mo19006y().mo19042a("Failed to retrieve Firebase Instance Id");
                        return null;
                    }
                } catch (Exception unused2) {
                    mo19015l().mo19005x().mo19042a("Failed to obtain Firebase Analytics instance");
                    return null;
                }
            } catch (ClassNotFoundException unused3) {
                return null;
            }
        } else {
            mo19015l().mo18996B().mo19042a("Disabled IID for tests.");
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public final boolean mo18725A() {
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: B */
    public final String mo18800B() {
        mo18816x();
        return this.f4966c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: C */
    public final String mo18801C() {
        mo18816x();
        return this.f4974k;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: D */
    public final String mo18802D() {
        mo18816x();
        return this.f4975l;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: E */
    public final String mo18803E() {
        mo18816x();
        return this.f4976m;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: F */
    public final int mo18804F() {
        mo18816x();
        return this.f4968e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: G */
    public final int mo18805G() {
        mo18816x();
        return this.f4973j;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: H */
    public final List<String> mo18806H() {
        return this.f4972i;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final zzm mo18807a(String str) {
        String str2;
        boolean z;
        Boolean bool;
        Boolean b;
        mo18881c();
        mo18880a();
        String B = mo18800B();
        String C = mo18801C();
        mo18816x();
        String str3 = this.f4967d;
        long F = (long) mo18804F();
        mo18816x();
        String str4 = this.f4969f;
        long i = mo19013h().mo19157i();
        mo18816x();
        mo18881c();
        if (this.f4970g == 0) {
            this.f4970g = this.f5134a.mo19104w().mo19424a(mo19016n(), mo19016n().getPackageName());
        }
        long j = this.f4970g;
        boolean c = this.f5134a.mo19089c();
        boolean z2 = !mo19012g().f5626x;
        mo18881c();
        mo18880a();
        if (!this.f5134a.mo19089c()) {
            str2 = null;
        } else {
            str2 = m8400I();
        }
        long e = this.f5134a.mo19091e();
        int G = mo18805G();
        boolean booleanValue = mo19013h().mo19161q().booleanValue();
        C3110la h = mo19013h();
        h.mo18880a();
        Boolean b2 = h.mo19148b("google_analytics_ssaid_collection_enabled");
        boolean booleanValue2 = Boolean.valueOf(b2 == null || b2.booleanValue()).booleanValue();
        C3185s4 g = mo19012g();
        g.mo18881c();
        boolean z3 = g.mo19315t().getBoolean("deferred_analytics_collection", false);
        String D = mo18802D();
        if (!mo19013h().mo19146a(C3135o.f5435a0) || (b = mo19013h().mo19148b("google_analytics_default_allow_ad_personalization_signals")) == null) {
            z = z2;
            bool = null;
        } else {
            bool = Boolean.valueOf(!b.booleanValue());
            z = z2;
        }
        return new zzm(B, C, str3, F, str4, i, j, str, c, z, str2, 0, e, G, booleanValue, booleanValue2, z3, D, bool, this.f4971h, mo19013h().mo19146a(C3135o.f5455k0) ? this.f4972i : null, (!C2774wa.m7753b() || !mo19013h().mo19146a(C3135o.f5395G0)) ? null : mo18803E());
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x019c, code lost:
        if (r2 == 0) goto L_0x019e;
     */
    /* JADX WARNING: Removed duplicated region for block: B:102:0x0273 A[Catch:{ IllegalStateException -> 0x02a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0284 A[Catch:{ IllegalStateException -> 0x02a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x02c2  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x030d  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x031d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00cb  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x0216  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0228 A[Catch:{ IllegalStateException -> 0x02a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x022a A[Catch:{ IllegalStateException -> 0x02a1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0235 A[SYNTHETIC, Splitter:B:88:0x0235] */
    /* renamed from: v */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo18808v() {
        /*
            r11 = this;
            android.content.Context r0 = r11.mo19016n()
            java.lang.String r0 = r0.getPackageName()
            android.content.Context r1 = r11.mo19016n()
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            java.lang.String r2 = "Unknown"
            java.lang.String r3 = ""
            r4 = 0
            java.lang.String r5 = "unknown"
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != 0) goto L_0x002d
            com.google.android.gms.measurement.internal.f4 r7 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()
            java.lang.Object r8 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r0)
            java.lang.String r9 = "PackageManager is null, app identity information might be inaccurate. appId"
            r7.mo19043a(r9, r8)
            goto L_0x0088
        L_0x002d:
            java.lang.String r5 = r1.getInstallerPackageName(r0)     // Catch:{ IllegalArgumentException -> 0x0032 }
            goto L_0x0043
        L_0x0032:
            com.google.android.gms.measurement.internal.f4 r7 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()
            java.lang.Object r8 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r0)
            java.lang.String r9 = "Error retrieving app installer package name. appId"
            r7.mo19043a(r9, r8)
        L_0x0043:
            if (r5 != 0) goto L_0x0048
            java.lang.String r5 = "manual_install"
            goto L_0x0051
        L_0x0048:
            java.lang.String r7 = "com.android.vending"
            boolean r7 = r7.equals(r5)
            if (r7 == 0) goto L_0x0051
            r5 = r3
        L_0x0051:
            android.content.Context r7 = r11.mo19016n()     // Catch:{ NameNotFoundException -> 0x0076 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ NameNotFoundException -> 0x0076 }
            android.content.pm.PackageInfo r7 = r1.getPackageInfo(r7, r4)     // Catch:{ NameNotFoundException -> 0x0076 }
            if (r7 == 0) goto L_0x0088
            android.content.pm.ApplicationInfo r8 = r7.applicationInfo     // Catch:{ NameNotFoundException -> 0x0076 }
            java.lang.CharSequence r8 = r1.getApplicationLabel(r8)     // Catch:{ NameNotFoundException -> 0x0076 }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ NameNotFoundException -> 0x0076 }
            if (r9 != 0) goto L_0x0070
            java.lang.String r8 = r8.toString()     // Catch:{ NameNotFoundException -> 0x0076 }
            goto L_0x0071
        L_0x0070:
            r8 = r2
        L_0x0071:
            java.lang.String r2 = r7.versionName     // Catch:{ NameNotFoundException -> 0x0077 }
            int r6 = r7.versionCode     // Catch:{ NameNotFoundException -> 0x0077 }
            goto L_0x0088
        L_0x0076:
            r8 = r2
        L_0x0077:
            com.google.android.gms.measurement.internal.f4 r7 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r0)
            java.lang.String r10 = "Error retrieving package info. appId, appName"
            r7.mo19044a(r10, r9, r8)
        L_0x0088:
            r11.f4966c = r0
            r11.f4969f = r5
            r11.f4967d = r2
            r11.f4968e = r6
            r5 = 0
            r11.f4970g = r5
            r11.mo19018r()
            android.content.Context r2 = r11.mo19016n()
            com.google.android.gms.common.api.Status r2 = com.google.android.gms.common.api.internal.C2072f.m4879a(r2)
            r5 = 1
            if (r2 == 0) goto L_0x00aa
            boolean r6 = r2.mo16518v()
            if (r6 == 0) goto L_0x00aa
            r6 = 1
            goto L_0x00ab
        L_0x00aa:
            r6 = 0
        L_0x00ab:
            com.google.android.gms.measurement.internal.j5 r7 = r11.f5134a
            java.lang.String r7 = r7.mo19074A()
            boolean r7 = android.text.TextUtils.isEmpty(r7)
            if (r7 != 0) goto L_0x00c7
            com.google.android.gms.measurement.internal.j5 r7 = r11.f5134a
            java.lang.String r7 = r7.mo19075B()
            java.lang.String r8 = "am"
            boolean r7 = r8.equals(r7)
            if (r7 == 0) goto L_0x00c7
            r7 = 1
            goto L_0x00c8
        L_0x00c7:
            r7 = 0
        L_0x00c8:
            r6 = r6 | r7
            if (r6 != 0) goto L_0x00f4
            if (r2 != 0) goto L_0x00db
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19002u()
            java.lang.String r8 = "GoogleService failed to initialize (no status)"
            r2.mo19042a(r8)
            goto L_0x00f4
        L_0x00db:
            com.google.android.gms.measurement.internal.f4 r8 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19002u()
            int r9 = r2.mo16512c()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            java.lang.String r2 = r2.mo16513d()
            java.lang.String r10 = "GoogleService failed to initialize, status"
            r8.mo19044a(r10, r9, r2)
        L_0x00f4:
            if (r6 == 0) goto L_0x020a
            boolean r2 = com.google.android.gms.internal.measurement.C2472da.m6221b()
            if (r2 == 0) goto L_0x01a0
            com.google.android.gms.measurement.internal.la r2 = r11.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5429X0
            boolean r2 = r2.mo19146a(r6)
            if (r2 == 0) goto L_0x01a0
            com.google.android.gms.measurement.internal.j5 r2 = r11.f5134a
            int r2 = r2.mo19090d()
            switch(r2) {
                case 0: goto L_0x018f;
                case 1: goto L_0x0181;
                case 2: goto L_0x0173;
                case 3: goto L_0x0165;
                case 4: goto L_0x0157;
                case 5: goto L_0x0149;
                case 6: goto L_0x013b;
                case 7: goto L_0x012d;
                default: goto L_0x0111;
            }
        L_0x0111:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19007z()
            java.lang.String r8 = "App measurement disabled"
            r6.mo19042a(r8)
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19002u()
            java.lang.String r8 = "Invalid scion state in identity"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x012d:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19007z()
            java.lang.String r8 = "App measurement disabled via the global data collection setting"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x013b:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19006y()
            java.lang.String r8 = "App measurement deactivated via resources. This method is being deprecated. Please refer to https://firebase.google.com/support/guides/disable-analytics"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x0149:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18996B()
            java.lang.String r8 = "App measurement disabled via the init parameters"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x0157:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19007z()
            java.lang.String r8 = "App measurement disabled via the manifest"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x0165:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19007z()
            java.lang.String r8 = "App measurement disabled by setMeasurementEnabled(false)"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x0173:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18996B()
            java.lang.String r8 = "App measurement deactivated via the init parameters"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x0181:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19007z()
            java.lang.String r8 = "App measurement deactivated via the manifest"
            r6.mo19042a(r8)
            goto L_0x019c
        L_0x018f:
            com.google.android.gms.measurement.internal.f4 r6 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18996B()
            java.lang.String r8 = "App measurement collection enabled"
            r6.mo19042a(r8)
        L_0x019c:
            if (r2 != 0) goto L_0x020a
        L_0x019e:
            r2 = 1
            goto L_0x020b
        L_0x01a0:
            com.google.android.gms.measurement.internal.la r2 = r11.mo19013h()
            java.lang.Boolean r2 = r2.mo19160p()
            com.google.android.gms.measurement.internal.la r6 = r11.mo19013h()
            boolean r6 = r6.mo19159m()
            if (r6 == 0) goto L_0x01c8
            com.google.android.gms.measurement.internal.j5 r2 = r11.f5134a
            boolean r2 = r2.mo19107z()
            if (r2 == 0) goto L_0x020a
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19007z()
            java.lang.String r6 = "Collection disabled with firebase_analytics_collection_deactivated=1"
            r2.mo19042a(r6)
            goto L_0x020a
        L_0x01c8:
            if (r2 == 0) goto L_0x01e6
            boolean r6 = r2.booleanValue()
            if (r6 != 0) goto L_0x01e6
            com.google.android.gms.measurement.internal.j5 r2 = r11.f5134a
            boolean r2 = r2.mo19107z()
            if (r2 == 0) goto L_0x020a
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19007z()
            java.lang.String r6 = "Collection disabled with firebase_analytics_collection_enabled=0"
            r2.mo19042a(r6)
            goto L_0x020a
        L_0x01e6:
            if (r2 != 0) goto L_0x01fc
            boolean r2 = com.google.android.gms.common.api.internal.C2072f.m4882b()
            if (r2 == 0) goto L_0x01fc
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19007z()
            java.lang.String r6 = "Collection disabled with google_app_measurement_enable=0"
            r2.mo19042a(r6)
            goto L_0x020a
        L_0x01fc:
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()
            java.lang.String r6 = "Collection enabled"
            r2.mo19042a(r6)
            goto L_0x019e
        L_0x020a:
            r2 = 0
        L_0x020b:
            r11.f4974k = r3
            r11.f4975l = r3
            r11.f4976m = r3
            r11.mo19018r()
            if (r7 == 0) goto L_0x021e
            com.google.android.gms.measurement.internal.j5 r6 = r11.f5134a
            java.lang.String r6 = r6.mo19074A()
            r11.f4975l = r6
        L_0x021e:
            java.lang.String r6 = com.google.android.gms.common.api.internal.C2072f.m4881a()     // Catch:{ IllegalStateException -> 0x02a1 }
            boolean r7 = android.text.TextUtils.isEmpty(r6)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r7 == 0) goto L_0x022a
            r7 = r3
            goto L_0x022b
        L_0x022a:
            r7 = r6
        L_0x022b:
            r11.f4974k = r7     // Catch:{ IllegalStateException -> 0x02a1 }
            boolean r7 = com.google.android.gms.internal.measurement.C2774wa.m7753b()     // Catch:{ IllegalStateException -> 0x02a1 }
            java.lang.String r8 = "admob_app_id"
            if (r7 == 0) goto L_0x026d
            com.google.android.gms.measurement.internal.la r7 = r11.mo19013h()     // Catch:{ IllegalStateException -> 0x02a1 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.C3135o.f5395G0     // Catch:{ IllegalStateException -> 0x02a1 }
            boolean r7 = r7.mo19146a(r9)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r7 == 0) goto L_0x026d
            com.google.android.gms.common.internal.y r7 = new com.google.android.gms.common.internal.y     // Catch:{ IllegalStateException -> 0x02a1 }
            android.content.Context r9 = r11.mo19016n()     // Catch:{ IllegalStateException -> 0x02a1 }
            r7.<init>(r9)     // Catch:{ IllegalStateException -> 0x02a1 }
            java.lang.String r9 = "ga_app_id"
            java.lang.String r9 = r7.mo17046a(r9)     // Catch:{ IllegalStateException -> 0x02a1 }
            boolean r10 = android.text.TextUtils.isEmpty(r9)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r10 == 0) goto L_0x0257
            goto L_0x0258
        L_0x0257:
            r3 = r9
        L_0x0258:
            r11.f4976m = r3     // Catch:{ IllegalStateException -> 0x02a1 }
            boolean r3 = android.text.TextUtils.isEmpty(r6)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r3 == 0) goto L_0x0266
            boolean r3 = android.text.TextUtils.isEmpty(r9)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r3 != 0) goto L_0x0282
        L_0x0266:
            java.lang.String r3 = r7.mo17046a(r8)     // Catch:{ IllegalStateException -> 0x02a1 }
            r11.f4975l = r3     // Catch:{ IllegalStateException -> 0x02a1 }
            goto L_0x0282
        L_0x026d:
            boolean r3 = android.text.TextUtils.isEmpty(r6)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r3 != 0) goto L_0x0282
            com.google.android.gms.common.internal.y r3 = new com.google.android.gms.common.internal.y     // Catch:{ IllegalStateException -> 0x02a1 }
            android.content.Context r6 = r11.mo19016n()     // Catch:{ IllegalStateException -> 0x02a1 }
            r3.<init>(r6)     // Catch:{ IllegalStateException -> 0x02a1 }
            java.lang.String r3 = r3.mo17046a(r8)     // Catch:{ IllegalStateException -> 0x02a1 }
            r11.f4975l = r3     // Catch:{ IllegalStateException -> 0x02a1 }
        L_0x0282:
            if (r2 == 0) goto L_0x02b3
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()     // Catch:{ IllegalStateException -> 0x02a1 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()     // Catch:{ IllegalStateException -> 0x02a1 }
            java.lang.String r3 = "App measurement enabled for app package, google app id"
            java.lang.String r6 = r11.f4966c     // Catch:{ IllegalStateException -> 0x02a1 }
            java.lang.String r7 = r11.f4974k     // Catch:{ IllegalStateException -> 0x02a1 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ IllegalStateException -> 0x02a1 }
            if (r7 == 0) goto L_0x029b
            java.lang.String r7 = r11.f4975l     // Catch:{ IllegalStateException -> 0x02a1 }
            goto L_0x029d
        L_0x029b:
            java.lang.String r7 = r11.f4974k     // Catch:{ IllegalStateException -> 0x02a1 }
        L_0x029d:
            r2.mo19044a(r3, r6, r7)     // Catch:{ IllegalStateException -> 0x02a1 }
            goto L_0x02b3
        L_0x02a1:
            r2 = move-exception
            com.google.android.gms.measurement.internal.f4 r3 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()
            java.lang.Object r0 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r0)
            java.lang.String r6 = "Fetching Google App Id failed with exception. appId"
            r3.mo19044a(r6, r0, r2)
        L_0x02b3:
            r0 = 0
            r11.f4972i = r0
            com.google.android.gms.measurement.internal.la r0 = r11.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.C3135o.f5455k0
            boolean r0 = r0.mo19146a(r2)
            if (r0 == 0) goto L_0x0307
            r11.mo19018r()
            com.google.android.gms.measurement.internal.la r0 = r11.mo19013h()
            java.lang.String r2 = "analytics.safelisted_events"
            java.util.List r0 = r0.mo19150c(r2)
            if (r0 == 0) goto L_0x0303
            int r2 = r0.size()
            if (r2 != 0) goto L_0x02e6
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19006y()
            java.lang.String r3 = "Safelisted event list is empty. Ignoring"
            r2.mo19042a(r3)
        L_0x02e4:
            r5 = 0
            goto L_0x0303
        L_0x02e6:
            java.util.Iterator r2 = r0.iterator()
        L_0x02ea:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0303
            java.lang.Object r3 = r2.next()
            java.lang.String r3 = (java.lang.String) r3
            com.google.android.gms.measurement.internal.z9 r6 = r11.mo19011f()
            java.lang.String r7 = "safelisted event"
            boolean r3 = r6.mo19449b(r7, r3)
            if (r3 != 0) goto L_0x02ea
            goto L_0x02e4
        L_0x0303:
            if (r5 == 0) goto L_0x0307
            r11.f4972i = r0
        L_0x0307:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 16
            if (r0 < r2) goto L_0x031d
            if (r1 == 0) goto L_0x031a
            android.content.Context r0 = r11.mo19016n()
            boolean r0 = com.google.android.gms.common.p090j.C2281a.m5675a(r0)
            r11.f4973j = r0
            return
        L_0x031a:
            r11.f4973j = r4
            return
        L_0x031d:
            r11.f4973j = r4
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C2984b4.mo18808v():void");
    }
}
