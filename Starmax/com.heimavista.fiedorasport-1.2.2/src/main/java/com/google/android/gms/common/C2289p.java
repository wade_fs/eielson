package com.google.android.gms.common;

import java.util.Arrays;

/* renamed from: com.google.android.gms.common.p */
final class C2289p extends C2288o {

    /* renamed from: b */
    private final byte[] f3784b;

    C2289p(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.f3784b = bArr;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: H */
    public final byte[] mo17067H() {
        return this.f3784b;
    }
}
