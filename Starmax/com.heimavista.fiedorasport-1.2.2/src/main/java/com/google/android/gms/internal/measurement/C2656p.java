package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.internal.measurement.p */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2656p extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4410T;

    /* renamed from: U */
    private final /* synthetic */ String f4411U;

    /* renamed from: V */
    private final /* synthetic */ Object f4412V;

    /* renamed from: W */
    private final /* synthetic */ boolean f4413W;

    /* renamed from: X */
    private final /* synthetic */ C2525gd f4414X;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2656p(C2525gd gdVar, String str, String str2, Object obj, boolean z) {
        super(gdVar);
        this.f4414X = gdVar;
        this.f4410T = str;
        this.f4411U = str2;
        this.f4412V = obj;
        this.f4413W = z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4414X.f4192g.setUserProperty(this.f4410T, this.f4411U, C3992d.m11990a(this.f4412V), this.f4413W, super.f4193P);
    }
}
