package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.internal.C2221i;

/* renamed from: com.google.android.gms.common.api.internal.m0 */
final class C2104m0 implements C2221i.C2222a {

    /* renamed from: a */
    private final /* synthetic */ C2100l0 f3387a;

    C2104m0(C2100l0 l0Var) {
        this.f3387a = l0Var;
    }

    /* renamed from: c */
    public final boolean mo16754c() {
        return this.f3387a.mo16748j();
    }

    /* renamed from: n */
    public final Bundle mo16755n() {
        return null;
    }
}
