package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import java.util.List;

/* renamed from: com.google.android.datatransport.cct.b.k */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class C1682k {
    @NonNull
    /* renamed from: a */
    public static C1682k m4213a(@NonNull List<C1692r> list) {
        return new C1668e(list);
    }

    @NonNull
    /* renamed from: a */
    public abstract List<C1692r> mo13465a();
}
