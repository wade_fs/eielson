package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.c8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3000c8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzan f5015P;

    /* renamed from: Q */
    private final /* synthetic */ String f5016Q;

    /* renamed from: R */
    private final /* synthetic */ C2589kc f5017R;

    /* renamed from: S */
    private final /* synthetic */ C3232w7 f5018S;

    C3000c8(C3232w7 w7Var, zzan zzan, String str, C2589kc kcVar) {
        this.f5018S = w7Var;
        this.f5015P = zzan;
        this.f5016Q = str;
        this.f5017R = kcVar;
    }

    public final void run() {
        byte[] bArr = null;
        try {
            C3228w3 d = this.f5018S.f5724d;
            if (d == null) {
                this.f5018S.mo19015l().mo19001t().mo19042a("Discarding data. Failed to send event to service to bundle");
                return;
            }
            bArr = d.mo19195a(this.f5015P, this.f5016Q);
            this.f5018S.m9314J();
            this.f5018S.mo19011f().mo19440a(this.f5017R, bArr);
        } catch (RemoteException e) {
            this.f5018S.mo19015l().mo19001t().mo19043a("Failed to send event to the service to bundle", e);
        } finally {
            this.f5018S.mo19011f().mo19440a(this.f5017R, bArr);
        }
    }
}
