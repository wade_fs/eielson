package com.google.android.gms.measurement.internal;

import android.app.ActivityManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.WorkerThread;
import com.google.android.gms.internal.measurement.C2504f9;
import com.google.android.gms.internal.measurement.C2607la;

/* renamed from: com.google.android.gms.measurement.internal.i9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3073i9 {

    /* renamed from: a */
    final /* synthetic */ C3244x8 f5221a;

    C3073i9(C3244x8 x8Var) {
        this.f5221a = x8Var;
    }

    @WorkerThread
    /* renamed from: b */
    private final void m8733b(long j, boolean z) {
        this.f5221a.mo18881c();
        if (C2607la.m6742b() && this.f5221a.mo19013h().mo19146a(C3135o.f5426W)) {
            if (this.f5221a.f5134a.mo19089c()) {
                this.f5221a.mo19012g().f5624v.mo19327a(j);
            } else {
                return;
            }
        }
        this.f5221a.mo19015l().mo18996B().mo19043a("Session started, time", Long.valueOf(this.f5221a.mo19017o().elapsedRealtime()));
        Long l = null;
        if (this.f5221a.mo19013h().mo19146a(C3135o.f5412P)) {
            l = Long.valueOf(j / 1000);
        }
        this.f5221a.mo18884m().mo19270a("auto", "_sid", l, j);
        this.f5221a.mo19012g().f5620r.mo19332a(false);
        Bundle bundle = new Bundle();
        if (this.f5221a.mo19013h().mo19146a(C3135o.f5412P)) {
            bundle.putLong("_sid", l.longValue());
        }
        if (this.f5221a.mo19013h().mo19146a(C3135o.f5399I0) && z) {
            bundle.putLong("_aib", 1);
        }
        this.f5221a.mo18884m().mo19265a("auto", "_s", j, bundle);
        if (C2504f9.m6336b() && this.f5221a.mo19013h().mo19146a(C3135o.f5413P0)) {
            String a = this.f5221a.mo19012g().f5604B.mo19346a();
            if (!TextUtils.isEmpty(a)) {
                Bundle bundle2 = new Bundle();
                bundle2.putString("_ffr", a);
                this.f5221a.mo18884m().mo19265a("auto", "_ssr", j, bundle2);
            }
        }
        if (!C2607la.m6742b() || !this.f5221a.mo19013h().mo19146a(C3135o.f5426W)) {
            this.f5221a.mo19012g().f5624v.mo19327a(j);
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19068a() {
        if (C2607la.m6742b() && this.f5221a.mo19013h().mo19146a(C3135o.f5426W)) {
            this.f5221a.mo18881c();
            if (this.f5221a.mo19012g().mo19309a(this.f5221a.mo19017o().mo17132a())) {
                this.f5221a.mo19012g().f5620r.mo19332a(true);
                if (Build.VERSION.SDK_INT >= 16) {
                    ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
                    ActivityManager.getMyMemoryState(runningAppProcessInfo);
                    if (runningAppProcessInfo.importance == 100) {
                        this.f5221a.mo19015l().mo18996B().mo19042a("Detected application was in foreground");
                        m8733b(this.f5221a.mo19017o().mo17132a(), false);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19069a(long j, boolean z) {
        this.f5221a.mo18881c();
        this.f5221a.m9355C();
        if (this.f5221a.mo19012g().mo19309a(j)) {
            this.f5221a.mo19012g().f5620r.mo19332a(true);
            this.f5221a.mo19012g().f5625w.mo19327a(0);
        }
        if (z && this.f5221a.mo19013h().mo19146a(C3135o.f5416R)) {
            this.f5221a.mo19012g().f5624v.mo19327a(j);
        }
        if (this.f5221a.mo19012g().f5620r.mo19333a()) {
            m8733b(j, z);
        }
    }
}
