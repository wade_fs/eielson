package com.google.android.gms.common.internal.p089z;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2219h;

/* renamed from: com.google.android.gms.common.internal.z.j */
public final class C2275j extends C2219h<C2278m> {
    public C2275j(Context context, Looper looper, C2211e eVar, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        super(context, looper, 39, eVar, bVar, cVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.service.ICommonService");
        if (queryLocalInterface instanceof C2278m) {
            return (C2278m) queryLocalInterface;
        }
        return new C2279n(iBinder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public final String mo16453y() {
        return "com.google.android.gms.common.internal.service.ICommonService";
    }

    /* renamed from: z */
    public final String mo16454z() {
        return "com.google.android.gms.common.service.START";
    }
}
