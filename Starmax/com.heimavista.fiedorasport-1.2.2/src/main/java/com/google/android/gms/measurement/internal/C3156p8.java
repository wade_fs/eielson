package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.p8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3156p8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3228w3 f5553P;

    /* renamed from: Q */
    private final /* synthetic */ C3144o8 f5554Q;

    C3156p8(C3144o8 o8Var, C3228w3 w3Var) {
        this.f5554Q = o8Var;
        this.f5553P = w3Var;
    }

    public final void run() {
        synchronized (this.f5554Q) {
            boolean unused = this.f5554Q.f5505P = false;
            if (!this.f5554Q.f5507R.mo19368B()) {
                this.f5554Q.f5507R.mo19015l().mo18995A().mo19042a("Connected to remote service");
                this.f5554Q.f5507R.mo19380a(this.f5553P);
            }
        }
    }
}
