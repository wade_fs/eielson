package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

/* renamed from: com.google.android.gms.dynamite.a */
final class C2344a implements DynamiteModule.C2339b.C2340a {
    C2344a() {
    }

    /* renamed from: a */
    public final int mo17146a(Context context, String str, boolean z) {
        return DynamiteModule.m5836a(context, str, z);
    }

    /* renamed from: a */
    public final int mo17145a(Context context, String str) {
        return DynamiteModule.m5835a(context, str);
    }
}
