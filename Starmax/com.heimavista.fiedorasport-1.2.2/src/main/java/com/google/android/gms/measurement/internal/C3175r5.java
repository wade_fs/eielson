package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.r5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3175r5 implements Callable<List<zzv>> {

    /* renamed from: a */
    private final /* synthetic */ zzm f5584a;

    /* renamed from: b */
    private final /* synthetic */ String f5585b;

    /* renamed from: c */
    private final /* synthetic */ String f5586c;

    /* renamed from: d */
    private final /* synthetic */ C3141o5 f5587d;

    C3175r5(C3141o5 o5Var, zzm zzm, String str, String str2) {
        this.f5587d = o5Var;
        this.f5584a = zzm;
        this.f5585b = str;
        this.f5586c = str2;
    }

    public final /* synthetic */ Object call() {
        this.f5587d.f5496a.mo19237q();
        return this.f5587d.f5496a.mo19229e().mo18859b(this.f5584a.f5814P, this.f5585b, this.f5586c);
    }
}
