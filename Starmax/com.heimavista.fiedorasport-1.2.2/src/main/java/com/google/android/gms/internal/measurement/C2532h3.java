package com.google.android.gms.internal.measurement;

import java.util.Comparator;

/* renamed from: com.google.android.gms.internal.measurement.h3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2532h3 implements Comparator<C2498f3> {
    C2532h3() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        C2498f3 f3Var = (C2498f3) obj;
        C2498f3 f3Var2 = (C2498f3) obj2;
        C2580k3 k3Var = (C2580k3) f3Var.iterator();
        C2580k3 k3Var2 = (C2580k3) f3Var2.iterator();
        while (k3Var.hasNext() && k3Var2.hasNext()) {
            int compare = Integer.compare(C2498f3.m6304b(k3Var.mo17442a()), C2498f3.m6304b(k3Var2.mo17442a()));
            if (compare != 0) {
                return compare;
            }
        }
        return Integer.compare(f3Var.mo17469a(), f3Var2.mo17469a());
    }
}
