package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.a1 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2414a1 extends C2595l4<C2414a1, C2415a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2414a1 zzj;
    private static volatile C2501f6<C2414a1> zzk;
    private int zzc;
    private long zzd;
    private String zze = "";
    private String zzf = "";
    private long zzg;
    private float zzh;
    private double zzi;

    /* renamed from: com.google.android.gms.internal.measurement.a1$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2415a extends C2595l4.C2596a<C2414a1, C2415a> implements C2769w5 {
        private C2415a() {
            super(C2414a1.zzj);
        }

        /* renamed from: a */
        public final C2415a mo17258a(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5960a(j);
            return this;
        }

        /* renamed from: b */
        public final C2415a mo17261b(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5970b(str);
            return this;
        }

        /* renamed from: j */
        public final C2415a mo17262j() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5974z();
            return this;
        }

        /* renamed from: k */
        public final C2415a mo17263k() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5957A();
            return this;
        }

        /* renamed from: l */
        public final C2415a mo17264l() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5958B();
            return this;
        }

        /* synthetic */ C2415a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2415a mo17259a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5965a(str);
            return this;
        }

        /* renamed from: b */
        public final C2415a mo17260b(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5966b(j);
            return this;
        }

        /* renamed from: a */
        public final C2415a mo17257a(double d) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2414a1) super.f4288Q).m5959a(d);
            return this;
        }
    }

    static {
        C2414a1 a1Var = new C2414a1();
        zzj = a1Var;
        C2595l4.m6645a(C2414a1.class, super);
    }

    private C2414a1() {
    }

    /* access modifiers changed from: private */
    /* renamed from: A */
    public final void m5957A() {
        this.zzc &= -9;
        this.zzg = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: B */
    public final void m5958B() {
        this.zzc &= -33;
        this.zzi = 0.0d;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5960a(long j) {
        this.zzc |= 1;
        this.zzd = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m5970b(String str) {
        str.getClass();
        this.zzc |= 4;
        this.zzf = str;
    }

    /* renamed from: x */
    public static C2415a m5972x() {
        return (C2415a) zzj.mo17668i();
    }

    /* access modifiers changed from: private */
    /* renamed from: z */
    public final void m5974z() {
        this.zzc &= -5;
        this.zzf = zzj.zzf;
    }

    /* renamed from: n */
    public final boolean mo17248n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final long mo17249o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final String mo17250p() {
        return this.zze;
    }

    /* renamed from: q */
    public final boolean mo17251q() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: s */
    public final String mo17252s() {
        return this.zzf;
    }

    /* renamed from: t */
    public final boolean mo17253t() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: u */
    public final long mo17254u() {
        return this.zzg;
    }

    /* renamed from: v */
    public final boolean mo17255v() {
        return (this.zzc & 32) != 0;
    }

    /* renamed from: w */
    public final double mo17256w() {
        return this.zzi;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5965a(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m5966b(long j) {
        this.zzc |= 8;
        this.zzg = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5959a(double d) {
        this.zzc |= 32;
        this.zzi = d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2414a1();
            case 2:
                return new C2415a(null);
            case 3:
                return C2595l4.m6643a(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0002\u0000\u0002\b\u0001\u0003\b\u0002\u0004\u0002\u0003\u0005\u0001\u0004\u0006\u0000\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                C2501f6<C2414a1> f6Var = zzk;
                if (f6Var == null) {
                    synchronized (C2414a1.class) {
                        f6Var = zzk;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzj);
                            zzk = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
