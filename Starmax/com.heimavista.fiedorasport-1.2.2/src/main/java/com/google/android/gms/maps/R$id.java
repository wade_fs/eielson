package com.google.android.gms.maps;

public final class R$id {
    public static final int hybrid = 2131296605;
    public static final int none = 2131296778;
    public static final int normal = 2131296779;
    public static final int satellite = 2131296847;
    public static final int terrain = 2131296934;

    private R$id() {
    }
}
