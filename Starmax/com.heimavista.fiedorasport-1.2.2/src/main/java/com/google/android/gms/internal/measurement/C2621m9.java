package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.m9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2621m9 implements C2593l2<C2666p9> {

    /* renamed from: Q */
    private static C2621m9 f4335Q = new C2621m9();

    /* renamed from: P */
    private final C2593l2<C2666p9> f4336P;

    private C2621m9(C2593l2<C2666p9> l2Var) {
        this.f4336P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6831b() {
        return ((C2666p9) f4335Q.mo17285a()).mo17802a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4336P.mo17285a();
    }

    public C2621m9() {
        this(C2579k2.m6601a(new C2652o9()));
    }
}
