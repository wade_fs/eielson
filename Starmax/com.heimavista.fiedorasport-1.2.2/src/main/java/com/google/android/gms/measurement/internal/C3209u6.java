package com.google.android.gms.measurement.internal;

import android.os.Bundle;

/* renamed from: com.google.android.gms.measurement.internal.u6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3209u6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ Bundle f5681P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5682Q;

    C3209u6(C3154p6 p6Var, Bundle bundle) {
        this.f5682Q = p6Var;
        this.f5681P = bundle;
    }

    public final void run() {
        this.f5682Q.m9126c(this.f5681P);
    }
}
