package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.internal.C2211e;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.u0 */
public final class C2136u0 implements C2082h1, C2106m2 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Lock f3469a;

    /* renamed from: b */
    private final Condition f3470b;

    /* renamed from: c */
    private final Context f3471c;

    /* renamed from: d */
    private final C2169c f3472d;

    /* renamed from: e */
    private final C2144w0 f3473e;

    /* renamed from: f */
    final Map<C2016a.C2019c<?>, C2016a.C2027f> f3474f;

    /* renamed from: g */
    final Map<C2016a.C2019c<?>, ConnectionResult> f3475g = new HashMap();

    /* renamed from: h */
    private final C2211e f3476h;

    /* renamed from: i */
    private final Map<C2016a<?>, Boolean> f3477i;

    /* renamed from: j */
    private final C2016a.C2017a<? extends C4052e, C4047a> f3478j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public volatile C2132t0 f3479k;

    /* renamed from: l */
    private ConnectionResult f3480l = null;

    /* renamed from: m */
    int f3481m;

    /* renamed from: n */
    final C2100l0 f3482n;

    /* renamed from: o */
    final C2089i1 f3483o;

    public C2136u0(Context context, C2100l0 l0Var, Lock lock, Looper looper, C2169c cVar, Map<C2016a.C2019c<?>, C2016a.C2027f> map, C2211e eVar, Map<C2016a<?>, Boolean> map2, C2016a.C2017a<? extends C4052e, C4047a> aVar, ArrayList<C2102l2> arrayList, C2089i1 i1Var) {
        this.f3471c = context;
        this.f3469a = lock;
        this.f3472d = cVar;
        this.f3474f = map;
        this.f3476h = eVar;
        this.f3477i = map2;
        this.f3478j = aVar;
        this.f3482n = l0Var;
        this.f3483o = i1Var;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            C2102l2 l2Var = arrayList.get(i);
            i++;
            l2Var.mo16753a(this);
        }
        this.f3473e = new C2144w0(this, looper);
        this.f3470b = lock.newCondition();
        this.f3479k = new C2096k0(this);
    }

    /* renamed from: L */
    public final void mo16583L(int i) {
        this.f3469a.lock();
        try {
            this.f3479k.mo16739L(i);
        } finally {
            this.f3469a.unlock();
        }
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16708a(@NonNull C2056c cVar) {
        cVar.mo16599f();
        return this.f3479k.mo16740a(cVar);
    }

    /* renamed from: a */
    public final boolean mo16711a(C2099l lVar) {
        return false;
    }

    /* renamed from: b */
    public final void mo16712b() {
        this.f3479k.mo16743b();
    }

    /* renamed from: c */
    public final boolean mo16713c() {
        return this.f3479k instanceof C2143w;
    }

    /* renamed from: d */
    public final void mo16714d() {
        if (mo16713c()) {
            ((C2143w) this.f3479k).mo16793d();
        }
    }

    /* renamed from: e */
    public final void mo16715e() {
    }

    /* renamed from: f */
    public final ConnectionResult mo16716f() {
        mo16712b();
        while (mo16785g()) {
            try {
                this.f3470b.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, null);
            }
        }
        if (mo16713c()) {
            return ConnectionResult.f3156T;
        }
        ConnectionResult connectionResult = this.f3480l;
        if (connectionResult != null) {
            return connectionResult;
        }
        return new ConnectionResult(13, null);
    }

    /* renamed from: g */
    public final boolean mo16785g() {
        return this.f3479k instanceof C2153z;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public final void mo16786h() {
        this.f3469a.lock();
        try {
            this.f3479k = new C2153z(this, this.f3476h, this.f3477i, this.f3472d, this.f3478j, this.f3469a, this.f3471c);
            this.f3479k.mo16744c();
            this.f3470b.signalAll();
        } finally {
            this.f3469a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public final void mo16787i() {
        this.f3469a.lock();
        try {
            this.f3482n.mo16750l();
            this.f3479k = new C2143w(this);
            this.f3479k.mo16744c();
            this.f3470b.signalAll();
        } finally {
            this.f3469a.unlock();
        }
    }

    /* renamed from: a */
    public final void mo16709a() {
        if (this.f3479k.mo16742a()) {
            this.f3475g.clear();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16782a(ConnectionResult connectionResult) {
        this.f3469a.lock();
        try {
            this.f3480l = connectionResult;
            this.f3479k = new C2096k0(this);
            this.f3479k.mo16744c();
            this.f3470b.signalAll();
        } finally {
            this.f3469a.unlock();
        }
    }

    /* renamed from: f */
    public final void mo16584f(@Nullable Bundle bundle) {
        this.f3469a.lock();
        try {
            this.f3479k.mo16745f(bundle);
        } finally {
            this.f3469a.unlock();
        }
    }

    /* renamed from: a */
    public final void mo16665a(@NonNull ConnectionResult connectionResult, @NonNull C2016a<?> aVar, boolean z) {
        this.f3469a.lock();
        try {
            this.f3479k.mo16741a(connectionResult, aVar, z);
        } finally {
            this.f3469a.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16783a(C2140v0 v0Var) {
        this.f3473e.sendMessage(this.f3473e.obtainMessage(1, v0Var));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16784a(RuntimeException runtimeException) {
        this.f3473e.sendMessage(this.f3473e.obtainMessage(2, runtimeException));
    }

    /* renamed from: a */
    public final void mo16710a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        String concat = String.valueOf(str).concat("  ");
        printWriter.append((CharSequence) str).append((CharSequence) "mState=").println(this.f3479k);
        for (C2016a aVar : this.f3477i.keySet()) {
            printWriter.append((CharSequence) str).append((CharSequence) aVar.mo16522b()).println(":");
            this.f3474f.get(aVar.mo16521a()).mo16532a(concat, fileDescriptor, printWriter, strArr);
        }
    }
}
