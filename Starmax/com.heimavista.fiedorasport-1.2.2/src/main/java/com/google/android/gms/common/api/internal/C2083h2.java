package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.common.api.internal.h2 */
final class C2083h2 {

    /* renamed from: a */
    private final int f3335a;

    /* renamed from: b */
    private final ConnectionResult f3336b;

    C2083h2(ConnectionResult connectionResult, int i) {
        C2258v.m5629a(connectionResult);
        this.f3336b = connectionResult;
        this.f3335a = i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final ConnectionResult mo16717a() {
        return this.f3336b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final int mo16718b() {
        return this.f3335a;
    }
}
