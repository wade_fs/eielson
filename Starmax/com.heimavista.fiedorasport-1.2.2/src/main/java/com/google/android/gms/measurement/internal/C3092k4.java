package com.google.android.gms.measurement.internal;

import androidx.annotation.NonNull;

/* renamed from: com.google.android.gms.measurement.internal.k4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3092k4 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final String f5293a;

    public C3092k4(@NonNull String str) {
        this.f5293a = str;
    }
}
