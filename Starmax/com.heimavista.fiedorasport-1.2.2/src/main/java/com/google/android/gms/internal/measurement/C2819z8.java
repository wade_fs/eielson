package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.z8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2819z8 implements C2593l2<C2455c9> {

    /* renamed from: Q */
    private static C2819z8 f4628Q = new C2819z8();

    /* renamed from: P */
    private final C2593l2<C2455c9> f4629P;

    private C2819z8(C2593l2<C2455c9> l2Var) {
        this.f4629P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7931b() {
        return ((C2455c9) f4628Q.mo17285a()).mo17337a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4629P.mo17285a();
    }

    public C2819z8() {
        this(C2579k2.m6601a(new C2439b9()));
    }
}
