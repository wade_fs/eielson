package com.google.android.gms.common.util;

import android.os.Build;

/* renamed from: com.google.android.gms.common.util.n */
public final class C2323n {
    /* renamed from: a */
    public static boolean m5792a() {
        return true;
    }

    /* renamed from: b */
    public static boolean m5793b() {
        return true;
    }

    /* renamed from: c */
    public static boolean m5794c() {
        return true;
    }

    /* renamed from: d */
    public static boolean m5795d() {
        return Build.VERSION.SDK_INT >= 18;
    }

    /* renamed from: e */
    public static boolean m5796e() {
        return Build.VERSION.SDK_INT >= 19;
    }

    /* renamed from: f */
    public static boolean m5797f() {
        return Build.VERSION.SDK_INT >= 20;
    }

    /* renamed from: g */
    public static boolean m5798g() {
        return Build.VERSION.SDK_INT >= 21;
    }

    /* renamed from: h */
    public static boolean m5799h() {
        return Build.VERSION.SDK_INT >= 24;
    }

    /* renamed from: i */
    public static boolean m5800i() {
        return Build.VERSION.SDK_INT >= 26;
    }

    /* renamed from: j */
    public static boolean m5801j() {
        return Build.VERSION.SDK_INT >= 28;
    }
}
