package com.google.android.gms.common.internal;

import androidx.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/* renamed from: com.google.android.gms.common.internal.t */
public final class C2251t {

    /* renamed from: com.google.android.gms.common.internal.t$a */
    public static final class C2252a {

        /* renamed from: a */
        private final List<String> f3754a;

        /* renamed from: b */
        private final Object f3755b;

        private C2252a(Object obj) {
            C2258v.m5629a(obj);
            this.f3755b = obj;
            this.f3754a = new ArrayList();
        }

        /* renamed from: a */
        public final C2252a mo17037a(String str, @Nullable Object obj) {
            List<String> list = this.f3754a;
            C2258v.m5629a((Object) str);
            String str2 = str;
            String valueOf = String.valueOf(obj);
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 1 + String.valueOf(valueOf).length());
            sb.append(str2);
            sb.append("=");
            sb.append(valueOf);
            list.add(sb.toString());
            return this;
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder(100);
            sb.append(this.f3755b.getClass().getSimpleName());
            sb.append('{');
            int size = this.f3754a.size();
            for (int i = 0; i < size; i++) {
                sb.append(this.f3754a.get(i));
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
            sb.append('}');
            return sb.toString();
        }
    }

    /* renamed from: a */
    public static boolean m5617a(@Nullable Object obj, @Nullable Object obj2) {
        if (obj != obj2) {
            return obj != null && obj.equals(obj2);
        }
        return true;
    }

    /* renamed from: a */
    public static int m5615a(Object... objArr) {
        return Arrays.hashCode(objArr);
    }

    /* renamed from: a */
    public static C2252a m5616a(Object obj) {
        return new C2252a(obj);
    }
}
