package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.t7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3199t7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3188s7 f5659P;

    /* renamed from: Q */
    private final /* synthetic */ long f5660Q;

    /* renamed from: R */
    private final /* synthetic */ C3177r7 f5661R;

    C3199t7(C3177r7 r7Var, C3188s7 s7Var, long j) {
        this.f5661R = r7Var;
        this.f5659P = s7Var;
        this.f5660Q = j;
    }

    public final void run() {
        this.f5661R.m9193a(this.f5659P, false, this.f5660Q);
        C3177r7 r7Var = this.f5661R;
        r7Var.f5590c = null;
        r7Var.mo18886q().mo19379a((C3188s7) null);
    }
}
