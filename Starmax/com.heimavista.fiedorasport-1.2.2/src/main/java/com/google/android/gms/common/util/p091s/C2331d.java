package com.google.android.gms.common.util.p091s;

import android.os.Process;

/* renamed from: com.google.android.gms.common.util.s.d */
final class C2331d implements Runnable {

    /* renamed from: P */
    private final Runnable f3865P;

    /* renamed from: Q */
    private final int f3866Q;

    public C2331d(Runnable runnable, int i) {
        this.f3865P = runnable;
        this.f3866Q = i;
    }

    public final void run() {
        Process.setThreadPriority(this.f3866Q);
        this.f3865P.run();
    }
}
