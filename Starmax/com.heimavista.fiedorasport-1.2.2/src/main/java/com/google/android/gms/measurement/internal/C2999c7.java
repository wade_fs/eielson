package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.c7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2999c7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5013P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5014Q;

    C2999c7(C3154p6 p6Var, boolean z) {
        this.f5014Q = p6Var;
        this.f5013P = z;
    }

    public final void run() {
        this.f5014Q.m9127c(this.f5013P);
    }
}
