package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.facebook.GraphResponse;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.internal.auth.zzaz;
import java.util.List;
import java.util.Map;

public class zzo extends zzaz {
    public static final Parcelable.Creator<zzo> CREATOR = new C1955b();

    /* renamed from: V */
    private static final ArrayMap<String, FastJsonResponse.Field<?, ?>> f3003V;

    /* renamed from: P */
    private final int f3004P;

    /* renamed from: Q */
    private List<String> f3005Q;

    /* renamed from: R */
    private List<String> f3006R;

    /* renamed from: S */
    private List<String> f3007S;

    /* renamed from: T */
    private List<String> f3008T;

    /* renamed from: U */
    private List<String> f3009U;

    static {
        ArrayMap<String, FastJsonResponse.Field<?, ?>> arrayMap = new ArrayMap<>();
        f3003V = arrayMap;
        arrayMap.put("registered", FastJsonResponse.Field.m5716d("registered", 2));
        f3003V.put("in_progress", FastJsonResponse.Field.m5716d("in_progress", 3));
        f3003V.put(GraphResponse.SUCCESS_KEY, FastJsonResponse.Field.m5716d(GraphResponse.SUCCESS_KEY, 4));
        f3003V.put("failed", FastJsonResponse.Field.m5716d("failed", 5));
        f3003V.put("escrowed", FastJsonResponse.Field.m5716d("escrowed", 6));
    }

    public zzo() {
        this.f3004P = 1;
    }

    /* renamed from: a */
    public Map<String, FastJsonResponse.Field<?, ?>> mo16320a() {
        return f3003V;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo16321b(FastJsonResponse.Field field) {
        return true;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3004P);
        C2250b.m5612b(parcel, 2, this.f3005Q, false);
        C2250b.m5612b(parcel, 3, this.f3006R, false);
        C2250b.m5612b(parcel, 4, this.f3007S, false);
        C2250b.m5612b(parcel, 5, this.f3008T, false);
        C2250b.m5612b(parcel, 6, this.f3009U, false);
        C2250b.m5587a(parcel, a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo16319a(FastJsonResponse.Field field) {
        switch (field.mo17091c()) {
            case 1:
                return Integer.valueOf(this.f3004P);
            case 2:
                return this.f3005Q;
            case 3:
                return this.f3006R;
            case 4:
                return this.f3007S;
            case 5:
                return this.f3008T;
            case 6:
                return this.f3009U;
            default:
                int c = field.mo17091c();
                StringBuilder sb = new StringBuilder(37);
                sb.append("Unknown SafeParcelable id=");
                sb.append(c);
                throw new IllegalStateException(sb.toString());
        }
    }

    zzo(int i, @Nullable List<String> list, @Nullable List<String> list2, @Nullable List<String> list3, @Nullable List<String> list4, @Nullable List<String> list5) {
        this.f3004P = i;
        this.f3005Q = list;
        this.f3006R = list2;
        this.f3007S = list3;
        this.f3008T = list4;
        this.f3009U = list5;
    }
}
