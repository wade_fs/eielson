package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.android.datatransport.cct.b.l */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1683l implements C3536d<C1668e> {
    /* renamed from: a */
    public void mo13453a(@Nullable Object obj, @NonNull Object obj2) {
        ((C3537e) obj2).mo22173a("logRequest", ((C1668e) obj).mo13465a());
    }
}
