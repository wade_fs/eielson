package com.google.android.gms.common.stats;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.util.C2332t;
import java.util.List;

/* renamed from: com.google.android.gms.common.stats.d */
public class C2306d {

    /* renamed from: a */
    private static C2306d f3845a = new C2306d();

    /* renamed from: b */
    private static Boolean f3846b;

    /* renamed from: a */
    public static C2306d m5752a() {
        return f3845a;
    }

    /* renamed from: b */
    private static boolean m5754b() {
        if (f3846b == null) {
            f3846b = false;
        }
        return f3846b.booleanValue();
    }

    /* renamed from: a */
    public void mo17126a(Context context, String str, int i, String str2, String str3, String str4, int i2, List<String> list) {
        mo17127a(context, str, i, str2, str3, str4, i2, list, 0);
    }

    /* renamed from: a */
    public void mo17127a(Context context, String str, int i, String str2, String str3, String str4, int i2, List<String> list, long j) {
        int i3 = i;
        if (m5754b()) {
            if (TextUtils.isEmpty(str)) {
                String valueOf = String.valueOf(str);
                Log.e("WakeLockTracker", valueOf.length() != 0 ? "missing wakeLock key. ".concat(valueOf) : new String("missing wakeLock key. "));
            } else if (7 == i3 || 8 == i3 || 10 == i3 || 11 == i3) {
                WakeLockEvent wakeLockEvent = r0;
                WakeLockEvent wakeLockEvent2 = new WakeLockEvent(System.currentTimeMillis(), i, str2, i2, C2305c.m5751a(list), str, SystemClock.elapsedRealtime(), C2332t.m5822a(context), str3, C2305c.m5750a(context.getPackageName()), C2332t.m5823b(context), j, str4, false);
                m5753a(context, wakeLockEvent);
            }
        }
    }

    /* renamed from: a */
    private static void m5753a(Context context, WakeLockEvent wakeLockEvent) {
        try {
            context.startService(new Intent().setComponent(C2304b.f3844a).putExtra("com.google.android.gms.common.stats.EXTRA_LOG_EVENT", wakeLockEvent));
        } catch (Exception e) {
            Log.wtf("WakeLockTracker", e);
        }
    }
}
