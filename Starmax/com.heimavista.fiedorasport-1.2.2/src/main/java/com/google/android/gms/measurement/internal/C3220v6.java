package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.v6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3220v6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3094k6 f5701P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5702Q;

    C3220v6(C3154p6 p6Var, C3094k6 k6Var) {
        this.f5702Q = p6Var;
        this.f5701P = k6Var;
    }

    public final void run() {
        this.f5702Q.mo19262a(this.f5701P);
    }
}
