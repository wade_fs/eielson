package com.google.android.exoplayer2.drm;

import android.media.MediaDrm;
import com.google.android.exoplayer2.drm.ExoMediaDrm;

/* renamed from: com.google.android.exoplayer2.drm.e */
/* compiled from: lambda */
public final /* synthetic */ class C1791e implements MediaDrm.OnEventListener {

    /* renamed from: a */
    private final /* synthetic */ FrameworkMediaDrm f2806a;

    /* renamed from: b */
    private final /* synthetic */ ExoMediaDrm.OnEventListener f2807b;

    public /* synthetic */ C1791e(FrameworkMediaDrm frameworkMediaDrm, ExoMediaDrm.OnEventListener onEventListener) {
        this.f2806a = frameworkMediaDrm;
        this.f2807b = onEventListener;
    }

    public final void onEvent(MediaDrm mediaDrm, byte[] bArr, int i, int i2, byte[] bArr2) {
        this.f2806a.mo14318a(this.f2807b, mediaDrm, bArr, i, i2, bArr2);
    }
}
