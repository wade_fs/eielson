package com.google.android.gms.auth.api.signin.internal;

/* renamed from: com.google.android.gms.auth.api.signin.internal.a */
public class C1988a {

    /* renamed from: b */
    private static int f3138b = 31;

    /* renamed from: a */
    private int f3139a = 1;

    /* renamed from: a */
    public C1988a mo16434a(Object obj) {
        this.f3139a = (f3138b * this.f3139a) + (obj == null ? 0 : obj.hashCode());
        return this;
    }

    /* renamed from: a */
    public final C1988a mo16435a(boolean z) {
        this.f3139a = (f3138b * this.f3139a) + (z ? 1 : 0);
        return this;
    }

    /* renamed from: a */
    public int mo16433a() {
        return this.f3139a;
    }
}
