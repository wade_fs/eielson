package com.google.android.gms.common;

import android.content.Context;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.Log;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2263x0;
import com.google.android.gms.common.internal.C2265y0;
import com.google.android.gms.dynamite.DynamiteModule;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.common.m */
final class C2286m {

    /* renamed from: a */
    private static volatile C2263x0 f3777a;

    /* renamed from: b */
    private static final Object f3778b = new Object();

    /* renamed from: c */
    private static Context f3779c;

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0019, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void m5689a(android.content.Context r2) {
        /*
            java.lang.Class<com.google.android.gms.common.m> r0 = com.google.android.gms.common.C2286m.class
            monitor-enter(r0)
            android.content.Context r1 = com.google.android.gms.common.C2286m.f3779c     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0011
            if (r2 == 0) goto L_0x0018
            android.content.Context r2 = r2.getApplicationContext()     // Catch:{ all -> 0x001a }
            com.google.android.gms.common.C2286m.f3779c = r2     // Catch:{ all -> 0x001a }
            monitor-exit(r0)
            return
        L_0x0011:
            java.lang.String r2 = "GoogleCertificates"
            java.lang.String r1 = "GoogleCertificates has been initialized already"
            android.util.Log.w(r2, r1)     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r0)
            return
        L_0x001a:
            r2 = move-exception
            monitor-exit(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.C2286m.m5689a(android.content.Context):void");
    }

    /* renamed from: b */
    private static C2335v m5690b(String str, C2288o oVar, boolean z, boolean z2) {
        try {
            if (f3777a == null) {
                C2258v.m5629a(f3779c);
                synchronized (f3778b) {
                    if (f3777a == null) {
                        f3777a = C2265y0.m5660a(DynamiteModule.m5837a(f3779c, DynamiteModule.f3888k, "com.google.android.gms.googlecertificates").mo17143a("com.google.android.gms.common.GoogleCertificatesImpl"));
                    }
                }
            }
            C2258v.m5629a(f3779c);
            try {
                if (f3777a.mo17045a(new zzk(str, oVar, z, z2), C3992d.m11990a(f3779c.getPackageManager()))) {
                    return C2335v.m5830c();
                }
                return C2335v.m5828a(new C2287n(z, str, oVar));
            } catch (RemoteException e) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e);
                return C2335v.m5827a("module call", e);
            }
        } catch (DynamiteModule.C2338a e2) {
            Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            String valueOf = String.valueOf(e2.getMessage());
            return C2335v.m5827a(valueOf.length() != 0 ? "module init: ".concat(valueOf) : new String("module init: "), e2);
        }
    }

    /* renamed from: a */
    static C2335v m5687a(String str, C2288o oVar, boolean z, boolean z2) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            return m5690b(str, oVar, z, z2);
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    /* renamed from: a */
    static final /* synthetic */ String m5688a(boolean z, String str, C2288o oVar) {
        boolean z2 = true;
        if (z || !m5690b(str, oVar, true, false).f3871a) {
            z2 = false;
        }
        return C2335v.m5829a(str, oVar, z, z2);
    }
}
