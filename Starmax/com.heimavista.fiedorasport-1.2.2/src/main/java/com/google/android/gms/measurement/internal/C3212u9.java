package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.u9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C3212u9 {

    /* renamed from: a */
    final Context f5689a;

    public C3212u9(Context context) {
        C2258v.m5629a(context);
        Context applicationContext = context.getApplicationContext();
        C2258v.m5629a(applicationContext);
        this.f5689a = applicationContext;
    }
}
