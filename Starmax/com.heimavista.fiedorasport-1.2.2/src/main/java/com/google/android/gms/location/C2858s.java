package com.google.android.gms.location;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.location.s */
public final class C2858s implements Parcelable.Creator<ActivityRecognitionResult> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        long j = 0;
        long j2 = 0;
        ArrayList arrayList = null;
        Bundle bundle = null;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                arrayList = C2248a.m5562c(parcel, a, DetectedActivity.CREATOR);
            } else if (a2 == 2) {
                j = C2248a.m5546B(parcel, a);
            } else if (a2 == 3) {
                j2 = C2248a.m5546B(parcel, a);
            } else if (a2 == 4) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 != 5) {
                C2248a.m5550F(parcel, a);
            } else {
                bundle = C2248a.m5565f(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new ActivityRecognitionResult(arrayList, j, j2, i, bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ActivityRecognitionResult[i];
    }
}
