package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import androidx.annotation.MainThread;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.q4 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
class C3163q4 extends BroadcastReceiver {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final C3145o9 f5561a;

    /* renamed from: b */
    private boolean f5562b;

    /* renamed from: c */
    private boolean f5563c;

    static {
        Class<C3163q4> cls = C3163q4.class;
    }

    C3163q4(C3145o9 o9Var) {
        C2258v.m5629a(o9Var);
        this.f5561a = o9Var;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19286a() {
        this.f5561a.mo19235m();
        this.f5561a.mo19014j().mo18881c();
        if (!this.f5562b) {
            this.f5561a.mo19016n().registerReceiver(super, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
            this.f5563c = this.f5561a.mo19227d().mo19073u();
            this.f5561a.mo19015l().mo18996B().mo19043a("Registering connectivity change receiver. Network connected", Boolean.valueOf(this.f5563c));
            this.f5562b = true;
        }
    }

    @WorkerThread
    /* renamed from: b */
    public final void mo19287b() {
        this.f5561a.mo19235m();
        this.f5561a.mo19014j().mo18881c();
        this.f5561a.mo19014j().mo18881c();
        if (this.f5562b) {
            this.f5561a.mo19015l().mo18996B().mo19042a("Unregistering connectivity change receiver");
            this.f5562b = false;
            this.f5563c = false;
            try {
                this.f5561a.mo19016n().unregisterReceiver(super);
            } catch (IllegalArgumentException e) {
                this.f5561a.mo19015l().mo19001t().mo19043a("Failed to unregister the network broadcast receiver", e);
            }
        }
    }

    @MainThread
    public void onReceive(Context context, Intent intent) {
        this.f5561a.mo19235m();
        String action = intent.getAction();
        this.f5561a.mo19015l().mo18996B().mo19043a("NetworkBroadcastReceiver received action", action);
        if ("android.net.conn.CONNECTIVITY_CHANGE".equals(action)) {
            boolean u = this.f5561a.mo19227d().mo19073u();
            if (this.f5563c != u) {
                this.f5563c = u;
                this.f5561a.mo19014j().mo19028a(new C3152p4(this, u));
                return;
            }
            return;
        }
        this.f5561a.mo19015l().mo19004w().mo19043a("NetworkBroadcastReceiver received unknown action", action);
    }
}
