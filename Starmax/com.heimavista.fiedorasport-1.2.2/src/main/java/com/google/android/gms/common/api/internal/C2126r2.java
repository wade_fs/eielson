package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2020d;
import com.google.android.gms.common.api.C2033e;
import com.google.android.gms.common.api.internal.C2064e;
import com.google.android.gms.common.internal.C2211e;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.r2 */
public final class C2126r2<O extends C2016a.C2020d> extends C2033e<O> {

    /* renamed from: i */
    private final C2016a.C2027f f3431i;

    /* renamed from: j */
    private final C2102l2 f3432j;

    /* renamed from: k */
    private final C2211e f3433k;

    /* renamed from: l */
    private final C2016a.C2017a<? extends C4052e, C4047a> f3434l;

    public C2126r2(@NonNull Context context, C2016a<O> aVar, Looper looper, @NonNull C2016a.C2027f fVar, @NonNull C2102l2 l2Var, C2211e eVar, C2016a.C2017a<? extends C4052e, C4047a> aVar2) {
        super(context, aVar, looper);
        this.f3431i = fVar;
        this.f3432j = l2Var;
        this.f3433k = eVar;
        this.f3434l = aVar2;
        super.f3197h.mo16656a(super);
    }

    /* renamed from: a */
    public final C2016a.C2027f mo16547a(Looper looper, C2064e.C2065a<O> aVar) {
        this.f3432j.mo16753a(aVar);
        return this.f3431i;
    }

    /* renamed from: i */
    public final C2016a.C2027f mo16774i() {
        return this.f3431i;
    }

    /* renamed from: a */
    public final C2109n1 mo16550a(Context context, Handler handler) {
        return new C2109n1(context, handler, this.f3433k, this.f3434l);
    }
}
