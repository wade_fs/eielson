package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ic */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2556ic implements C2593l2<C2624mc> {

    /* renamed from: Q */
    private static C2556ic f4243Q = new C2556ic();

    /* renamed from: P */
    private final C2593l2<C2624mc> f4244P;

    private C2556ic(C2593l2<C2624mc> l2Var) {
        this.f4244P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6491b() {
        return ((C2624mc) f4243Q.mo17285a()).mo17716a();
    }

    /* renamed from: c */
    public static boolean m6492c() {
        return ((C2624mc) f4243Q.mo17285a()).mo17717e();
    }

    /* renamed from: d */
    public static boolean m6493d() {
        return ((C2624mc) f4243Q.mo17285a()).mo17718f();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4244P.mo17285a();
    }

    public C2556ic() {
        this(C2579k2.m6601a(new C2609lc()));
    }
}
