package com.google.android.gms.common.api.internal;

import android.app.Dialog;

/* renamed from: com.google.android.gms.common.api.internal.j2 */
final class C2094j2 extends C2078g1 {

    /* renamed from: a */
    private final /* synthetic */ Dialog f3351a;

    /* renamed from: b */
    private final /* synthetic */ C2090i2 f3352b;

    C2094j2(C2090i2 i2Var, Dialog dialog) {
        this.f3352b = i2Var;
        this.f3351a = dialog;
    }

    /* renamed from: a */
    public final void mo16700a() {
        this.f3352b.f3345Q.mo16702g();
        if (this.f3351a.isShowing()) {
            this.f3351a.dismiss();
        }
    }
}
