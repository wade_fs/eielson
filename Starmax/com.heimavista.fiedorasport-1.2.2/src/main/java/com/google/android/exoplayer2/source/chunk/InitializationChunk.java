package com.google.android.exoplayer2.source.chunk;

import androidx.annotation.Nullable;
import com.google.android.exoplayer2.C1750C;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.extractor.DefaultExtractorInput;
import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.PositionHolder;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.Util;

public final class InitializationChunk extends Chunk {
    private static final PositionHolder DUMMY_POSITION_HOLDER = new PositionHolder();
    private final ChunkExtractorWrapper extractorWrapper;
    private volatile boolean loadCanceled;
    private long nextLoadPosition;

    public InitializationChunk(DataSource dataSource, DataSpec dataSpec, Format format, int i, @Nullable Object obj, ChunkExtractorWrapper chunkExtractorWrapper) {
        super(dataSource, dataSpec, 2, format, i, obj, C1750C.TIME_UNSET, C1750C.TIME_UNSET);
        this.extractorWrapper = chunkExtractorWrapper;
    }

    public void cancelLoad() {
        this.loadCanceled = true;
    }

    public void load() {
        DefaultExtractorInput defaultExtractorInput;
        DataSpec subrange = super.dataSpec.subrange(this.nextLoadPosition);
        try {
            defaultExtractorInput = new DefaultExtractorInput(super.dataSource, subrange.absoluteStreamPosition, super.dataSource.open(subrange));
            if (this.nextLoadPosition == 0) {
                this.extractorWrapper.init(null, C1750C.TIME_UNSET, C1750C.TIME_UNSET);
            }
            Extractor extractor = this.extractorWrapper.extractor;
            int i = 0;
            while (i == 0 && !this.loadCanceled) {
                i = extractor.read(defaultExtractorInput, DUMMY_POSITION_HOLDER);
            }
            boolean z = true;
            if (i == 1) {
                z = false;
            }
            Assertions.checkState(z);
            this.nextLoadPosition = defaultExtractorInput.getPosition() - super.dataSpec.absoluteStreamPosition;
            Util.closeQuietly(super.dataSource);
        } catch (Throwable th) {
            Util.closeQuietly(super.dataSource);
            throw th;
        }
    }
}
