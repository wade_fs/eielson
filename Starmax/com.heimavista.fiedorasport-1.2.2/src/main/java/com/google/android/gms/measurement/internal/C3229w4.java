package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.util.Pair;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.w4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3229w4 {

    /* renamed from: a */
    private final String f5710a;

    /* renamed from: b */
    private final String f5711b;

    /* renamed from: c */
    private final String f5712c;

    /* renamed from: d */
    private final long f5713d;

    /* renamed from: e */
    private final /* synthetic */ C3185s4 f5714e;

    private C3229w4(C3185s4 s4Var, String str, long j) {
        this.f5714e = s4Var;
        C2258v.m5639b(str);
        C2258v.m5636a(j > 0);
        this.f5710a = String.valueOf(str).concat(":start");
        this.f5711b = String.valueOf(str).concat(":count");
        this.f5712c = String.valueOf(str).concat(":value");
        this.f5713d = j;
    }

    @WorkerThread
    /* renamed from: b */
    private final void m9309b() {
        this.f5714e.mo18881c();
        long a = this.f5714e.mo19017o().mo17132a();
        SharedPreferences.Editor edit = this.f5714e.mo19315t().edit();
        edit.remove(this.f5711b);
        edit.remove(this.f5712c);
        edit.putLong(this.f5710a, a);
        edit.apply();
    }

    @WorkerThread
    /* renamed from: c */
    private final long m9310c() {
        return this.f5714e.mo19315t().getLong(this.f5710a, 0);
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19365a(String str, long j) {
        this.f5714e.mo18881c();
        if (m9310c() == 0) {
            m9309b();
        }
        if (str == null) {
            str = "";
        }
        long j2 = this.f5714e.mo19315t().getLong(this.f5711b, 0);
        if (j2 <= 0) {
            SharedPreferences.Editor edit = this.f5714e.mo19315t().edit();
            edit.putString(this.f5712c, str);
            edit.putLong(this.f5711b, 1);
            edit.apply();
            return;
        }
        long j3 = j2 + 1;
        boolean z = (this.f5714e.mo19011f().mo19454u().nextLong() & Long.MAX_VALUE) < Long.MAX_VALUE / j3;
        SharedPreferences.Editor edit2 = this.f5714e.mo19315t().edit();
        if (z) {
            edit2.putString(this.f5712c, str);
        }
        edit2.putLong(this.f5711b, j3);
        edit2.apply();
    }

    @WorkerThread
    /* renamed from: a */
    public final Pair<String, Long> mo19364a() {
        long j;
        this.f5714e.mo18881c();
        this.f5714e.mo18881c();
        long c = m9310c();
        if (c == 0) {
            m9309b();
            j = 0;
        } else {
            j = Math.abs(c - this.f5714e.mo19017o().mo17132a());
        }
        long j2 = this.f5713d;
        if (j < j2) {
            return null;
        }
        if (j > (j2 << 1)) {
            m9309b();
            return null;
        }
        String string = this.f5714e.mo19315t().getString(this.f5712c, null);
        long j3 = this.f5714e.mo19315t().getLong(this.f5711b, 0);
        m9309b();
        if (string == null || j3 <= 0) {
            return C3185s4.f5602C;
        }
        return new Pair<>(string, Long.valueOf(j3));
    }
}
