package com.google.android.gms.auth.api.signin.internal;

import android.os.IInterface;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

/* renamed from: com.google.android.gms.auth.api.signin.internal.s */
public interface C2006s extends IInterface {
    /* renamed from: a */
    void mo16462a(C2004q qVar, GoogleSignInOptions googleSignInOptions);

    /* renamed from: b */
    void mo16463b(C2004q qVar, GoogleSignInOptions googleSignInOptions);
}
