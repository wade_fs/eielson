package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2763w0;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.v0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2748v0 extends C2595l4<C2748v0, C2749a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2748v0 zzd;
    private static volatile C2501f6<C2748v0> zze;
    private C2738u4<C2763w0> zzc = C2595l4.m6649m();

    /* renamed from: com.google.android.gms.internal.measurement.v0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2749a extends C2595l4.C2596a<C2748v0, C2749a> implements C2769w5 {
        private C2749a() {
            super(C2748v0.zzd);
        }

        /* renamed from: a */
        public final C2763w0 mo17950a(int i) {
            return ((C2748v0) super.f4288Q).mo17947b(0);
        }

        /* synthetic */ C2749a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2749a mo17949a(C2763w0.C2764a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2748v0) super.f4288Q).m7388a((C2763w0) super.mo17679i());
            return this;
        }
    }

    static {
        C2748v0 v0Var = new C2748v0();
        zzd = v0Var;
        C2595l4.m6645a(C2748v0.class, super);
    }

    private C2748v0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7388a(C2763w0 w0Var) {
        w0Var.getClass();
        if (!this.zzc.mo17938a()) {
            this.zzc = C2595l4.m6642a(this.zzc);
        }
        this.zzc.add(w0Var);
    }

    /* renamed from: o */
    public static C2749a m7389o() {
        return (C2749a) zzd.mo17668i();
    }

    /* renamed from: b */
    public final C2763w0 mo17947b(int i) {
        return this.zzc.get(0);
    }

    /* renamed from: n */
    public final List<C2763w0> mo17948n() {
        return this.zzc;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2748v0();
            case 2:
                return new C2749a(null);
            case 3:
                return C2595l4.m6643a(zzd, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001b", new Object[]{"zzc", C2763w0.class});
            case 4:
                return zzd;
            case 5:
                C2501f6<C2748v0> f6Var = zze;
                if (f6Var == null) {
                    synchronized (C2748v0.class) {
                        f6Var = zze;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzd);
                            zze = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
