package com.google.android.datatransport.cct.p084b;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1679j;

/* renamed from: com.google.android.datatransport.cct.b.u */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class C1696u {

    /* renamed from: com.google.android.datatransport.cct.b.u$a */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class C1697a {
        @NonNull
        /* renamed from: a */
        public abstract C1697a mo13521a(@Nullable C1698b bVar);

        @NonNull
        /* renamed from: a */
        public abstract C1697a mo13522a(@Nullable C1699c cVar);

        @NonNull
        /* renamed from: a */
        public abstract C1696u mo13523a();
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: com.google.android.datatransport.cct.b.u$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static final class C1698b extends Enum<C1698b> {

        /* renamed from: Q */
        public static final C1698b f2630Q = new C1698b("UNKNOWN_MOBILE_SUBTYPE", 0, 0);

        /* renamed from: R */
        public static final C1698b f2631R = new C1698b("GPRS", 1, 1);

        /* renamed from: S */
        public static final C1698b f2632S = new C1698b("EDGE", 2, 2);

        /* renamed from: T */
        public static final C1698b f2633T = new C1698b("UMTS", 3, 3);

        /* renamed from: U */
        public static final C1698b f2634U = new C1698b("CDMA", 4, 4);

        /* renamed from: V */
        public static final C1698b f2635V = new C1698b("EVDO_0", 5, 5);

        /* renamed from: W */
        public static final C1698b f2636W = new C1698b("EVDO_A", 6, 6);

        /* renamed from: X */
        public static final C1698b f2637X = new C1698b("RTT", 7, 7);

        /* renamed from: Y */
        public static final C1698b f2638Y = new C1698b("HSDPA", 8, 8);

        /* renamed from: Z */
        public static final C1698b f2639Z = new C1698b("HSUPA", 9, 9);

        /* renamed from: a0 */
        public static final C1698b f2640a0 = new C1698b("HSPA", 10, 10);

        /* renamed from: b0 */
        public static final C1698b f2641b0 = new C1698b("IDEN", 11, 11);

        /* renamed from: c0 */
        public static final C1698b f2642c0 = new C1698b("EVDO_B", 12, 12);

        /* renamed from: d0 */
        public static final C1698b f2643d0 = new C1698b("LTE", 13, 13);

        /* renamed from: e0 */
        public static final C1698b f2644e0 = new C1698b("EHRPD", 14, 14);

        /* renamed from: f0 */
        public static final C1698b f2645f0 = new C1698b("HSPAP", 15, 15);

        /* renamed from: g0 */
        public static final C1698b f2646g0 = new C1698b("GSM", 16, 16);

        /* renamed from: h0 */
        public static final C1698b f2647h0 = new C1698b("TD_SCDMA", 17, 17);

        /* renamed from: i0 */
        public static final C1698b f2648i0 = new C1698b("IWLAN", 18, 18);

        /* renamed from: j0 */
        public static final C1698b f2649j0 = new C1698b("LTE_CA", 19, 19);

        /* renamed from: k0 */
        public static final C1698b f2650k0 = new C1698b("COMBINED", 20, 100);

        /* renamed from: l0 */
        private static final SparseArray<C1698b> f2651l0 = new SparseArray<>();

        /* renamed from: P */
        private final int f2652P;

        static {
            C1698b[] bVarArr = {f2630Q, f2631R, f2632S, f2633T, f2634U, f2635V, f2636W, f2637X, f2638Y, f2639Z, f2640a0, f2641b0, f2642c0, f2643d0, f2644e0, f2645f0, f2646g0, f2647h0, f2648i0, f2649j0, f2650k0};
            f2651l0.put(0, f2630Q);
            f2651l0.put(1, f2631R);
            f2651l0.put(2, f2632S);
            f2651l0.put(3, f2633T);
            f2651l0.put(4, f2634U);
            f2651l0.put(5, f2635V);
            f2651l0.put(6, f2636W);
            f2651l0.put(7, f2637X);
            f2651l0.put(8, f2638Y);
            f2651l0.put(9, f2639Z);
            f2651l0.put(10, f2640a0);
            f2651l0.put(11, f2641b0);
            f2651l0.put(12, f2642c0);
            f2651l0.put(13, f2643d0);
            f2651l0.put(14, f2644e0);
            f2651l0.put(15, f2645f0);
            f2651l0.put(16, f2646g0);
            f2651l0.put(17, f2647h0);
            f2651l0.put(18, f2648i0);
            f2651l0.put(19, f2649j0);
        }

        private C1698b(String str, int i, int i2) {
            this.f2652P = i2;
        }

        /* renamed from: a */
        public int mo13526a() {
            return this.f2652P;
        }

        @Nullable
        /* renamed from: a */
        public static C1698b m4254a(int i) {
            return f2651l0.get(i);
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: com.google.android.datatransport.cct.b.u$c */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static final class C1699c extends Enum<C1699c> {

        /* renamed from: Q */
        public static final C1699c f2653Q = new C1699c("MOBILE", 0, 0);

        /* renamed from: R */
        public static final C1699c f2654R = new C1699c("WIFI", 1, 1);

        /* renamed from: S */
        public static final C1699c f2655S = new C1699c("MOBILE_MMS", 2, 2);

        /* renamed from: T */
        public static final C1699c f2656T = new C1699c("MOBILE_SUPL", 3, 3);

        /* renamed from: U */
        public static final C1699c f2657U = new C1699c("MOBILE_DUN", 4, 4);

        /* renamed from: V */
        public static final C1699c f2658V = new C1699c("MOBILE_HIPRI", 5, 5);

        /* renamed from: W */
        public static final C1699c f2659W = new C1699c("WIMAX", 6, 6);

        /* renamed from: X */
        public static final C1699c f2660X = new C1699c("BLUETOOTH", 7, 7);

        /* renamed from: Y */
        public static final C1699c f2661Y = new C1699c("DUMMY", 8, 8);

        /* renamed from: Z */
        public static final C1699c f2662Z = new C1699c("ETHERNET", 9, 9);

        /* renamed from: a0 */
        public static final C1699c f2663a0 = new C1699c("MOBILE_FOTA", 10, 10);

        /* renamed from: b0 */
        public static final C1699c f2664b0 = new C1699c("MOBILE_IMS", 11, 11);

        /* renamed from: c0 */
        public static final C1699c f2665c0 = new C1699c("MOBILE_CBS", 12, 12);

        /* renamed from: d0 */
        public static final C1699c f2666d0 = new C1699c("WIFI_P2P", 13, 13);

        /* renamed from: e0 */
        public static final C1699c f2667e0 = new C1699c("MOBILE_IA", 14, 14);

        /* renamed from: f0 */
        public static final C1699c f2668f0 = new C1699c("MOBILE_EMERGENCY", 15, 15);

        /* renamed from: g0 */
        public static final C1699c f2669g0 = new C1699c("PROXY", 16, 16);

        /* renamed from: h0 */
        public static final C1699c f2670h0 = new C1699c("VPN", 17, 17);

        /* renamed from: i0 */
        public static final C1699c f2671i0 = new C1699c("NONE", 18, -1);

        /* renamed from: j0 */
        private static final SparseArray<C1699c> f2672j0 = new SparseArray<>();

        /* renamed from: P */
        private final int f2673P;

        static {
            C1699c[] cVarArr = {f2653Q, f2654R, f2655S, f2656T, f2657U, f2658V, f2659W, f2660X, f2661Y, f2662Z, f2663a0, f2664b0, f2665c0, f2666d0, f2667e0, f2668f0, f2669g0, f2670h0, f2671i0};
            f2672j0.put(0, f2653Q);
            f2672j0.put(1, f2654R);
            f2672j0.put(2, f2655S);
            f2672j0.put(3, f2656T);
            f2672j0.put(4, f2657U);
            f2672j0.put(5, f2658V);
            f2672j0.put(6, f2659W);
            f2672j0.put(7, f2660X);
            f2672j0.put(8, f2661Y);
            f2672j0.put(9, f2662Z);
            f2672j0.put(10, f2663a0);
            f2672j0.put(11, f2664b0);
            f2672j0.put(12, f2665c0);
            f2672j0.put(13, f2666d0);
            f2672j0.put(14, f2667e0);
            f2672j0.put(15, f2668f0);
            f2672j0.put(16, f2669g0);
            f2672j0.put(17, f2670h0);
            f2672j0.put(-1, f2671i0);
        }

        private C1699c(String str, int i, int i2) {
            this.f2673P = i2;
        }

        /* renamed from: a */
        public int mo13527a() {
            return this.f2673P;
        }

        @Nullable
        /* renamed from: a */
        public static C1699c m4256a(int i) {
            return f2672j0.get(i);
        }
    }

    @NonNull
    /* renamed from: a */
    public static C1697a m4250a() {
        return new C1679j.C1681b();
    }
}
