package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;

/* renamed from: com.google.android.gms.common.api.internal.t0 */
public interface C2132t0 {
    /* renamed from: L */
    void mo16739L(int i);

    /* renamed from: a */
    <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16740a(C2056c cVar);

    /* renamed from: a */
    void mo16741a(ConnectionResult connectionResult, C2016a<?> aVar, boolean z);

    /* renamed from: a */
    boolean mo16742a();

    /* renamed from: b */
    void mo16743b();

    /* renamed from: c */
    void mo16744c();

    /* renamed from: f */
    void mo16745f(Bundle bundle);
}
