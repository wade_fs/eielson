package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ma */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2622ma implements C2573ja {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4337a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.lifecycle.app_background_timestamp_when_backgrounded", true);

    /* renamed from: a */
    public final boolean mo17608a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17609e() {
        return f4337a.mo18128b().booleanValue();
    }
}
