package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.g9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2521g9 implements C2471d9 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4177a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.upload.file_truncate_fix", false);

    /* renamed from: a */
    public final boolean mo17429a() {
        return f4177a.mo18128b().booleanValue();
    }
}
