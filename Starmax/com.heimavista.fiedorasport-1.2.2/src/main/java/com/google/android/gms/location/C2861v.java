package com.google.android.gms.location;

import java.util.Comparator;

/* renamed from: com.google.android.gms.location.v */
final class C2861v implements Comparator<ActivityTransition> {
    C2861v() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        ActivityTransition activityTransition = (ActivityTransition) obj;
        ActivityTransition activityTransition2 = (ActivityTransition) obj2;
        int c = activityTransition.mo18190c();
        int c2 = activityTransition2.mo18190c();
        if (c != c2) {
            return c < c2 ? -1 : 1;
        }
        int d = activityTransition.mo18191d();
        int d2 = activityTransition2.mo18191d();
        if (d == d2) {
            return 0;
        }
        return d < d2 ? -1 : 1;
    }
}
