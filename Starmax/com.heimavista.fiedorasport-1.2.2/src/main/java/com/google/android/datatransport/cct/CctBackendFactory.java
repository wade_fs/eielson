package com.google.android.datatransport.cct;

import androidx.annotation.Keep;
import com.google.android.datatransport.runtime.backends.C1712d;
import com.google.android.datatransport.runtime.backends.C1718h;
import com.google.android.datatransport.runtime.backends.C1724m;

@Keep
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public class CctBackendFactory implements C1712d {
    public C1724m create(C1718h hVar) {
        return new C1703e(hVar.mo13547a(), hVar.mo13550d(), hVar.mo13549c());
    }
}
