package com.google.android.gms.measurement;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import androidx.annotation.MainThread;
import com.google.android.gms.measurement.internal.C3211u8;
import com.google.android.gms.measurement.internal.C3255y8;

@TargetApi(24)
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class AppMeasurementJobService extends JobService implements C3255y8 {

    /* renamed from: P */
    private C3211u8<AppMeasurementJobService> f4927P;

    /* renamed from: a */
    private final C3211u8<AppMeasurementJobService> m8367a() {
        if (this.f4927P == null) {
            this.f4927P = new C3211u8<>(this);
        }
        return this.f4927P;
    }

    /* renamed from: a */
    public final void mo18705a(Intent intent) {
    }

    @MainThread
    public final void onCreate() {
        super.onCreate();
        m8367a().mo19339a();
    }

    @MainThread
    public final void onDestroy() {
        m8367a().mo19343b();
        super.onDestroy();
    }

    @MainThread
    public final void onRebind(Intent intent) {
        m8367a().mo19345c(intent);
    }

    public final boolean onStartJob(JobParameters jobParameters) {
        return m8367a().mo19342a(jobParameters);
    }

    public final boolean onStopJob(JobParameters jobParameters) {
        return false;
    }

    @MainThread
    public final boolean onUnbind(Intent intent) {
        return m8367a().mo19344b(intent);
    }

    /* renamed from: a */
    public final boolean mo18706a(int i) {
        throw new UnsupportedOperationException();
    }

    @TargetApi(24)
    /* renamed from: a */
    public final void mo18704a(JobParameters jobParameters, boolean z) {
        jobFinished(jobParameters, false);
    }
}
