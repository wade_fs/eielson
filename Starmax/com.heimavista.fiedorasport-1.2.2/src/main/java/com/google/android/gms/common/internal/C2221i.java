package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.internal.base.C2382h;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.google.android.gms.common.internal.i */
public final class C2221i implements Handler.Callback {

    /* renamed from: P */
    private final C2222a f3714P;

    /* renamed from: Q */
    private final ArrayList<C2036f.C2038b> f3715Q = new ArrayList<>();

    /* renamed from: R */
    private final ArrayList<C2036f.C2038b> f3716R = new ArrayList<>();

    /* renamed from: S */
    private final ArrayList<C2036f.C2039c> f3717S = new ArrayList<>();

    /* renamed from: T */
    private volatile boolean f3718T = false;

    /* renamed from: U */
    private final AtomicInteger f3719U = new AtomicInteger(0);

    /* renamed from: V */
    private boolean f3720V = false;

    /* renamed from: W */
    private final Handler f3721W;

    /* renamed from: X */
    private final Object f3722X = new Object();

    /* renamed from: com.google.android.gms.common.internal.i$a */
    public interface C2222a {
        /* renamed from: c */
        boolean mo16754c();

        /* renamed from: n */
        Bundle mo16755n();
    }

    public C2221i(Looper looper, C2222a aVar) {
        this.f3714P = aVar;
        this.f3721W = new C2382h(looper, this);
    }

    /* renamed from: a */
    public final void mo16979a() {
        this.f3718T = false;
        this.f3719U.incrementAndGet();
    }

    /* renamed from: b */
    public final void mo16985b() {
        this.f3718T = true;
    }

    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            C2036f.C2038b bVar = (C2036f.C2038b) message.obj;
            synchronized (this.f3722X) {
                if (this.f3718T && this.f3714P.mo16754c() && this.f3715Q.contains(bVar)) {
                    bVar.mo16584f(this.f3714P.mo16755n());
                }
            }
            return true;
        }
        StringBuilder sb = new StringBuilder(45);
        sb.append("Don't know how to handle message: ");
        sb.append(i);
        Log.wtf("GmsClientEvents", sb.toString(), new Exception());
        return false;
    }

    /* renamed from: b */
    public final void mo16986b(C2036f.C2039c cVar) {
        C2258v.m5629a(cVar);
        synchronized (this.f3722X) {
            if (!this.f3717S.remove(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 57);
                sb.append("unregisterConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" not found");
                Log.w("GmsClientEvents", sb.toString());
            }
        }
    }

    /* renamed from: a */
    public final void mo16981a(Bundle bundle) {
        C2258v.m5634a(this.f3721W, "onConnectionSuccess must only be called on the Handler thread");
        synchronized (this.f3722X) {
            boolean z = true;
            C2258v.m5640b(!this.f3720V);
            this.f3721W.removeMessages(1);
            this.f3720V = true;
            if (this.f3716R.size() != 0) {
                z = false;
            }
            C2258v.m5640b(z);
            ArrayList arrayList = new ArrayList(this.f3715Q);
            int i = this.f3719U.get();
            int size = arrayList.size();
            int i2 = 0;
            while (i2 < size) {
                Object obj = arrayList.get(i2);
                i2++;
                C2036f.C2038b bVar = (C2036f.C2038b) obj;
                if (!this.f3718T || !this.f3714P.mo16754c() || this.f3719U.get() != i) {
                    break;
                } else if (!this.f3716R.contains(bVar)) {
                    bVar.mo16584f(bundle);
                }
            }
            this.f3716R.clear();
            this.f3720V = false;
        }
    }

    /* renamed from: a */
    public final void mo16980a(int i) {
        C2258v.m5634a(this.f3721W, "onUnintentionalDisconnection must only be called on the Handler thread");
        this.f3721W.removeMessages(1);
        synchronized (this.f3722X) {
            this.f3720V = true;
            ArrayList arrayList = new ArrayList(this.f3715Q);
            int i2 = this.f3719U.get();
            int size = arrayList.size();
            int i3 = 0;
            while (i3 < size) {
                Object obj = arrayList.get(i3);
                i3++;
                C2036f.C2038b bVar = (C2036f.C2038b) obj;
                if (!this.f3718T || this.f3719U.get() != i2) {
                    break;
                } else if (this.f3715Q.contains(bVar)) {
                    bVar.mo16583L(i);
                }
            }
            this.f3716R.clear();
            this.f3720V = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0046, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo16982a(com.google.android.gms.common.ConnectionResult r8) {
        /*
            r7 = this;
            android.os.Handler r0 = r7.f3721W
            java.lang.String r1 = "onConnectionFailure must only be called on the Handler thread"
            com.google.android.gms.common.internal.C2258v.m5634a(r0, r1)
            android.os.Handler r0 = r7.f3721W
            r1 = 1
            r0.removeMessages(r1)
            java.lang.Object r0 = r7.f3722X
            monitor-enter(r0)
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x0049 }
            java.util.ArrayList<com.google.android.gms.common.api.f$c> r2 = r7.f3717S     // Catch:{ all -> 0x0049 }
            r1.<init>(r2)     // Catch:{ all -> 0x0049 }
            java.util.concurrent.atomic.AtomicInteger r2 = r7.f3719U     // Catch:{ all -> 0x0049 }
            int r2 = r2.get()     // Catch:{ all -> 0x0049 }
            int r3 = r1.size()     // Catch:{ all -> 0x0049 }
            r4 = 0
        L_0x0022:
            if (r4 >= r3) goto L_0x0047
            java.lang.Object r5 = r1.get(r4)     // Catch:{ all -> 0x0049 }
            int r4 = r4 + 1
            com.google.android.gms.common.api.f$c r5 = (com.google.android.gms.common.api.C2036f.C2039c) r5     // Catch:{ all -> 0x0049 }
            boolean r6 = r7.f3718T     // Catch:{ all -> 0x0049 }
            if (r6 == 0) goto L_0x0045
            java.util.concurrent.atomic.AtomicInteger r6 = r7.f3719U     // Catch:{ all -> 0x0049 }
            int r6 = r6.get()     // Catch:{ all -> 0x0049 }
            if (r6 == r2) goto L_0x0039
            goto L_0x0045
        L_0x0039:
            java.util.ArrayList<com.google.android.gms.common.api.f$c> r6 = r7.f3717S     // Catch:{ all -> 0x0049 }
            boolean r6 = r6.contains(r5)     // Catch:{ all -> 0x0049 }
            if (r6 == 0) goto L_0x0022
            r5.mo16585a(r8)     // Catch:{ all -> 0x0049 }
            goto L_0x0022
        L_0x0045:
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            return
        L_0x0047:
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            return
        L_0x0049:
            r8 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0049 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.C2221i.mo16982a(com.google.android.gms.common.ConnectionResult):void");
    }

    /* renamed from: a */
    public final void mo16983a(C2036f.C2038b bVar) {
        C2258v.m5629a(bVar);
        synchronized (this.f3722X) {
            if (this.f3715Q.contains(bVar)) {
                String valueOf = String.valueOf(bVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 62);
                sb.append("registerConnectionCallbacks(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.f3715Q.add(bVar);
            }
        }
        if (this.f3714P.mo16754c()) {
            Handler handler = this.f3721W;
            handler.sendMessage(handler.obtainMessage(1, bVar));
        }
    }

    /* renamed from: a */
    public final void mo16984a(C2036f.C2039c cVar) {
        C2258v.m5629a(cVar);
        synchronized (this.f3722X) {
            if (this.f3717S.contains(cVar)) {
                String valueOf = String.valueOf(cVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 67);
                sb.append("registerConnectionFailedListener(): listener ");
                sb.append(valueOf);
                sb.append(" is already registered");
                Log.w("GmsClientEvents", sb.toString());
            } else {
                this.f3717S.add(cVar);
            }
        }
    }
}
