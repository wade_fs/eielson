package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class DetectedActivity extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DetectedActivity> CREATOR = new C2865z();

    /* renamed from: R */
    private static final int[] f4654R = {0, 1, 2, 3, 7, 8, 16, 17};

    /* renamed from: P */
    private int f4655P;

    /* renamed from: Q */
    private int f4656Q;

    static {
        new C2864y();
    }

    public DetectedActivity(int i, int i2) {
        this.f4655P = i;
        this.f4656Q = i2;
    }

    /* renamed from: b */
    public static void m7945b(int i) {
        boolean z = false;
        for (int i2 : f4654R) {
            if (i2 == i) {
                z = true;
            }
        }
        if (!z) {
            StringBuilder sb = new StringBuilder(81);
            sb.append(i);
            sb.append(" is not a valid DetectedActivity supported by Activity Transition API.");
            Log.w("DetectedActivity", sb.toString());
        }
    }

    /* renamed from: c */
    public int mo18211c() {
        return this.f4656Q;
    }

    /* renamed from: d */
    public int mo18212d() {
        int i = this.f4655P;
        if (i > 19 || i < 0) {
            return 4;
        }
        return i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && DetectedActivity.class == obj.getClass()) {
            DetectedActivity detectedActivity = (DetectedActivity) obj;
            return this.f4655P == detectedActivity.f4655P && this.f4656Q == detectedActivity.f4656Q;
        }
    }

    public int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4655P), Integer.valueOf(this.f4656Q));
    }

    public String toString() {
        String str;
        int d = mo18212d();
        if (d == 0) {
            str = "IN_VEHICLE";
        } else if (d == 1) {
            str = "ON_BICYCLE";
        } else if (d == 2) {
            str = "ON_FOOT";
        } else if (d == 3) {
            str = "STILL";
        } else if (d == 4) {
            str = "UNKNOWN";
        } else if (d == 5) {
            str = "TILTING";
        } else if (d == 7) {
            str = "WALKING";
        } else if (d != 8) {
            switch (d) {
                case 16:
                    str = "IN_ROAD_VEHICLE";
                    break;
                case 17:
                    str = "IN_RAIL_VEHICLE";
                    break;
                case 18:
                    str = "IN_TWO_WHEELER_VEHICLE";
                    break;
                case 19:
                    str = "IN_FOUR_WHEELER_VEHICLE";
                    break;
                default:
                    str = Integer.toString(d);
                    break;
            }
        } else {
            str = "RUNNING";
        }
        int i = this.f4656Q;
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 48);
        sb.append("DetectedActivity [type=");
        sb.append(str);
        sb.append(", confidence=");
        sb.append(i);
        sb.append("]");
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f4655P);
        C2250b.m5591a(parcel, 2, this.f4656Q);
        C2250b.m5587a(parcel, a);
    }
}
