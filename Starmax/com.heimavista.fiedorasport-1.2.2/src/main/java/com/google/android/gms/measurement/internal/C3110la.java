package com.google.android.gms.measurement.internal;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;
import androidx.annotation.WorkerThread;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2324o;
import java.lang.reflect.InvocationTargetException;

/* renamed from: com.google.android.gms.measurement.internal.la */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3110la extends C3034f6 {

    /* renamed from: b */
    private Boolean f5334b;
    @NonNull

    /* renamed from: c */
    private C3134na f5335c = C2979b.f4959a;

    /* renamed from: d */
    private Boolean f5336d;

    C3110la(C3081j5 j5Var) {
        super(j5Var);
    }

    /* renamed from: v */
    public static long m8856v() {
        return C3135o.f5388D.mo19389a(null).longValue();
    }

    /* renamed from: w */
    public static long m8857w() {
        return C3135o.f5440d.mo19389a(null).longValue();
    }

    @Nullable
    /* renamed from: x */
    private final Bundle m8858x() {
        try {
            if (mo19016n().getPackageManager() == null) {
                mo19015l().mo19001t().mo19042a("Failed to load metadata: PackageManager is null");
                return null;
            }
            ApplicationInfo a = C2283c.m5685a(mo19016n()).mo17055a(mo19016n().getPackageName(), 128);
            if (a != null) {
                return a.metaData;
            }
            mo19015l().mo19001t().mo19042a("Failed to load metadata: ApplicationInfo is null");
            return null;
        } catch (PackageManager.NameNotFoundException e) {
            mo19015l().mo19001t().mo19043a("Failed to load metadata: Package name not found", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19145a(@NonNull C3134na naVar) {
        this.f5335c = naVar;
    }

    @WorkerThread
    /* renamed from: b */
    public final int mo19147b(String str, @NonNull C3239x3<Integer> x3Var) {
        if (str == null) {
            return x3Var.mo19389a(null).intValue();
        }
        String a = this.f5335c.mo18798a(str, x3Var.mo19390a());
        if (TextUtils.isEmpty(a)) {
            return x3Var.mo19389a(null).intValue();
        }
        try {
            return x3Var.mo19389a(Integer.valueOf(Integer.parseInt(a))).intValue();
        } catch (NumberFormatException unused) {
            return x3Var.mo19389a(null).intValue();
        }
    }

    @WorkerThread
    /* renamed from: c */
    public final double mo19149c(String str, @NonNull C3239x3<Double> x3Var) {
        if (str == null) {
            return x3Var.mo19389a(null).doubleValue();
        }
        String a = this.f5335c.mo18798a(str, x3Var.mo19390a());
        if (TextUtils.isEmpty(a)) {
            return x3Var.mo19389a(null).doubleValue();
        }
        try {
            return x3Var.mo19389a(Double.valueOf(Double.parseDouble(a))).doubleValue();
        } catch (NumberFormatException unused) {
            return x3Var.mo19389a(null).doubleValue();
        }
    }

    @WorkerThread
    /* renamed from: d */
    public final boolean mo19152d(String str, @NonNull C3239x3<Boolean> x3Var) {
        if (str == null) {
            return x3Var.mo19389a(null).booleanValue();
        }
        String a = this.f5335c.mo18798a(str, x3Var.mo19390a());
        if (TextUtils.isEmpty(a)) {
            return x3Var.mo19389a(null).booleanValue();
        }
        return x3Var.mo19389a(Boolean.valueOf(Boolean.parseBoolean(a))).booleanValue();
    }

    /* renamed from: e */
    public final boolean mo19154e(String str, C3239x3<Boolean> x3Var) {
        return mo19152d(str, x3Var);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: f */
    public final boolean mo19155f(String str) {
        return mo19152d(str, C3135o.f5396H);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: g */
    public final String mo19156g(String str) {
        C3239x3<String> x3Var = C3135o.f5398I;
        if (str == null) {
            return x3Var.mo19389a(null);
        }
        return x3Var.mo19389a(this.f5335c.mo18798a(str, x3Var.mo19390a()));
    }

    /* renamed from: i */
    public final long mo19157i() {
        mo19018r();
        return 21028;
    }

    /* renamed from: k */
    public final boolean mo19158k() {
        if (this.f5336d == null) {
            synchronized (this) {
                if (this.f5336d == null) {
                    ApplicationInfo applicationInfo = mo19016n().getApplicationInfo();
                    String a = C2324o.m5803a();
                    if (applicationInfo != null) {
                        String str = applicationInfo.processName;
                        this.f5336d = Boolean.valueOf(str != null && str.equals(a));
                    }
                    if (this.f5336d == null) {
                        this.f5336d = Boolean.TRUE;
                        mo19015l().mo19001t().mo19042a("My process not in the list of running processes");
                    }
                }
            }
        }
        return this.f5336d.booleanValue();
    }

    /* renamed from: m */
    public final boolean mo19159m() {
        mo19018r();
        Boolean b = mo19148b("firebase_analytics_collection_deactivated");
        return b != null && b.booleanValue();
    }

    /* renamed from: p */
    public final Boolean mo19160p() {
        mo19018r();
        return mo19148b("firebase_analytics_collection_enabled");
    }

    /* renamed from: q */
    public final Boolean mo19161q() {
        mo18880a();
        Boolean b = mo19148b("google_analytics_adid_collection_enabled");
        return Boolean.valueOf(b == null || b.booleanValue());
    }

    /* renamed from: s */
    public final String mo19162s() {
        return m8855a("debug.firebase.analytics.app", "");
    }

    /* renamed from: t */
    public final String mo19163t() {
        return m8855a("debug.deferred.deeplink", "");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: u */
    public final boolean mo19164u() {
        if (this.f5334b == null) {
            this.f5334b = mo19148b("app_measurement_lite");
            if (this.f5334b == null) {
                this.f5334b = false;
            }
        }
        if (this.f5334b.booleanValue() || !super.f5134a.mo19077D()) {
            return true;
        }
        return false;
    }

    @WorkerThread
    /* renamed from: a */
    public final int mo19142a(@Size(min = 1) String str) {
        return mo19147b(str, C3135o.f5462o);
    }

    /* renamed from: e */
    public final boolean mo19153e(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(this.f5335c.mo18798a(str, "measurement.event_sampling_enabled"));
    }

    @WorkerThread
    /* renamed from: a */
    public final long mo19143a(String str, @NonNull C3239x3<Long> x3Var) {
        if (str == null) {
            return x3Var.mo19389a(null).longValue();
        }
        String a = this.f5335c.mo18798a(str, x3Var.mo19390a());
        if (TextUtils.isEmpty(a)) {
            return x3Var.mo19389a(null).longValue();
        }
        try {
            return x3Var.mo19389a(Long.valueOf(Long.parseLong(a))).longValue();
        } catch (NumberFormatException unused) {
            return x3Var.mo19389a(null).longValue();
        }
    }

    /* renamed from: d */
    public final boolean mo19151d(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(this.f5335c.mo18798a(str, "gaia_collection_enabled"));
    }

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: b */
    public final Boolean mo19148b(@Size(min = 1) String str) {
        C2258v.m5639b(str);
        Bundle x = m8858x();
        if (x == null) {
            mo19015l().mo19001t().mo19042a("Failed to load metadata: Metadata bundle is null");
            return null;
        } else if (!x.containsKey(str)) {
            return null;
        } else {
            return Boolean.valueOf(x.getBoolean(str));
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002a A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:9:0x002b A[SYNTHETIC, Splitter:B:9:0x002b] */
    @androidx.annotation.Nullable
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<java.lang.String> mo19150c(@androidx.annotation.Size(min = 1) java.lang.String r4) {
        /*
            r3 = this;
            com.google.android.gms.common.internal.C2258v.m5639b(r4)
            android.os.Bundle r0 = r3.m8858x()
            r1 = 0
            if (r0 != 0) goto L_0x0019
            com.google.android.gms.measurement.internal.f4 r4 = r3.mo19015l()
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()
            java.lang.String r0 = "Failed to load metadata: Metadata bundle is null"
            r4.mo19042a(r0)
        L_0x0017:
            r4 = r1
            goto L_0x0028
        L_0x0019:
            boolean r2 = r0.containsKey(r4)
            if (r2 != 0) goto L_0x0020
            goto L_0x0017
        L_0x0020:
            int r4 = r0.getInt(r4)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
        L_0x0028:
            if (r4 != 0) goto L_0x002b
            return r1
        L_0x002b:
            android.content.Context r0 = r3.mo19016n()     // Catch:{ NotFoundException -> 0x0043 }
            android.content.res.Resources r0 = r0.getResources()     // Catch:{ NotFoundException -> 0x0043 }
            int r4 = r4.intValue()     // Catch:{ NotFoundException -> 0x0043 }
            java.lang.String[] r4 = r0.getStringArray(r4)     // Catch:{ NotFoundException -> 0x0043 }
            if (r4 != 0) goto L_0x003e
            return r1
        L_0x003e:
            java.util.List r4 = java.util.Arrays.asList(r4)     // Catch:{ NotFoundException -> 0x0043 }
            return r4
        L_0x0043:
            r4 = move-exception
            com.google.android.gms.measurement.internal.f4 r0 = r3.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()
            java.lang.String r2 = "Failed to load string array from metadata: resource not found"
            r0.mo19043a(r2, r4)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3110la.mo19150c(java.lang.String):java.util.List");
    }

    /* renamed from: a */
    public final boolean mo19146a(C3239x3<Boolean> x3Var) {
        return mo19152d(null, x3Var);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x002d, code lost:
        if (android.text.TextUtils.isEmpty(r1) != false) goto L_0x002f;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String mo19144a(com.google.android.gms.measurement.internal.C3021e5 r6) {
        /*
            r5 = this;
            android.net.Uri$Builder r0 = new android.net.Uri$Builder
            r0.<init>()
            java.lang.String r1 = r6.mo18971n()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0033
            boolean r1 = com.google.android.gms.internal.measurement.C2774wa.m7753b()
            if (r1 == 0) goto L_0x002f
            com.google.android.gms.measurement.internal.la r1 = r5.mo19013h()
            java.lang.String r2 = r6.mo18967l()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5395G0
            boolean r1 = r1.mo19152d(r2, r3)
            if (r1 == 0) goto L_0x002f
            java.lang.String r1 = r6.mo18975p()
            boolean r2 = android.text.TextUtils.isEmpty(r1)
            if (r2 == 0) goto L_0x0033
        L_0x002f:
            java.lang.String r1 = r6.mo18973o()
        L_0x0033:
            com.google.android.gms.measurement.internal.x3<java.lang.String> r2 = com.google.android.gms.measurement.internal.C3135o.f5442e
            r3 = 0
            java.lang.Object r2 = r2.mo19389a(r3)
            java.lang.String r2 = (java.lang.String) r2
            android.net.Uri$Builder r2 = r0.scheme(r2)
            com.google.android.gms.measurement.internal.x3<java.lang.String> r4 = com.google.android.gms.measurement.internal.C3135o.f5444f
            java.lang.Object r3 = r4.mo19389a(r3)
            java.lang.String r3 = (java.lang.String) r3
            android.net.Uri$Builder r2 = r2.encodedAuthority(r3)
            java.lang.String r3 = "config/app/"
            java.lang.String r1 = java.lang.String.valueOf(r1)
            int r4 = r1.length()
            if (r4 == 0) goto L_0x005d
            java.lang.String r1 = r3.concat(r1)
            goto L_0x0062
        L_0x005d:
            java.lang.String r1 = new java.lang.String
            r1.<init>(r3)
        L_0x0062:
            android.net.Uri$Builder r1 = r2.path(r1)
            java.lang.String r6 = r6.mo18969m()
            java.lang.String r2 = "app_instance_id"
            android.net.Uri$Builder r6 = r1.appendQueryParameter(r2, r6)
            java.lang.String r1 = "platform"
            java.lang.String r2 = "android"
            android.net.Uri$Builder r6 = r6.appendQueryParameter(r1, r2)
            long r1 = r5.mo19157i()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            java.lang.String r2 = "gmp_version"
            r6.appendQueryParameter(r2, r1)
            android.net.Uri r6 = r0.build()
            java.lang.String r6 = r6.toString()
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3110la.mo19144a(com.google.android.gms.measurement.internal.e5):java.lang.String");
    }

    /* renamed from: a */
    private final String m8855a(String str, String str2) {
        try {
            return (String) Class.forName("android.os.SystemProperties").getMethod("get", String.class, String.class).invoke(null, str, str2);
        } catch (ClassNotFoundException e) {
            mo19015l().mo19001t().mo19043a("Could not find SystemProperties class", e);
            return str2;
        } catch (NoSuchMethodException e2) {
            mo19015l().mo19001t().mo19043a("Could not find SystemProperties.get() method", e2);
            return str2;
        } catch (IllegalAccessException e3) {
            mo19015l().mo19001t().mo19043a("Could not access SystemProperties.get()", e3);
            return str2;
        } catch (InvocationTargetException e4) {
            mo19015l().mo19001t().mo19043a("SystemProperties.get() threw an exception", e4);
            return str2;
        }
    }
}
