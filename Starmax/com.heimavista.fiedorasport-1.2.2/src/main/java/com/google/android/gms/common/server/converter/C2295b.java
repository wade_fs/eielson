package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import com.google.android.gms.common.server.converter.StringToIntConverter;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.common.server.converter.b */
public final class C2295b implements Parcelable.Creator<StringToIntConverter> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        ArrayList arrayList = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 != 2) {
                C2248a.m5550F(parcel, a);
            } else {
                arrayList = C2248a.m5562c(parcel, a, StringToIntConverter.zaa.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new StringToIntConverter(i, arrayList);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new StringToIntConverter[i];
    }
}
