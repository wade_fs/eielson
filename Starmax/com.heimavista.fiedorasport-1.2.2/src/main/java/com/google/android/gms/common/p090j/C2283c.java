package com.google.android.gms.common.p090j;

import android.content.Context;

/* renamed from: com.google.android.gms.common.j.c */
public class C2283c {

    /* renamed from: b */
    private static C2283c f3775b = new C2283c();

    /* renamed from: a */
    private C2282b f3776a = null;

    /* renamed from: a */
    public static C2282b m5685a(Context context) {
        return f3775b.m5686b(context);
    }

    /* renamed from: b */
    private final synchronized C2282b m5686b(Context context) {
        if (this.f3776a == null) {
            if (context.getApplicationContext() != null) {
                context = context.getApplicationContext();
            }
            this.f3776a = new C2282b(context);
        }
        return this.f3776a;
    }
}
