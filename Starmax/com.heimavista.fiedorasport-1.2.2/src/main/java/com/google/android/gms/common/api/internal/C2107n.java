package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.api.internal.n */
public class C2107n extends BasePendingResult<Status> {
    public C2107n(C2036f fVar) {
        super(fVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public /* synthetic */ C2157k mo16455a(Status status) {
        return status;
    }
}
