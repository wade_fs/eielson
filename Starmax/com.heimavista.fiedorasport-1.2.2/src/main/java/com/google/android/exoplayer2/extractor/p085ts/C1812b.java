package com.google.android.exoplayer2.extractor.p085ts;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.ts.b */
/* compiled from: lambda */
public final /* synthetic */ class C1812b implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1812b f2822a = new C1812b();

    private /* synthetic */ C1812b() {
    }

    public final Extractor[] createExtractors() {
        return AdtsExtractor.m4389a();
    }
}
