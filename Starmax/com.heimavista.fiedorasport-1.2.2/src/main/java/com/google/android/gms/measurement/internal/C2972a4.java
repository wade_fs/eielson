package com.google.android.gms.measurement.internal;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.WorkerThread;
import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;

/* renamed from: com.google.android.gms.measurement.internal.a4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2972a4 extends C2995c3 {

    /* renamed from: c */
    private final C3020e4 f4944c = new C3020e4(this, mo19016n(), "google_app_measurement_local.db");

    /* renamed from: d */
    private boolean f4945d;

    C2972a4(C3081j5 j5Var) {
        super(j5Var);
    }

    @WorkerThread
    /* renamed from: E */
    private final SQLiteDatabase m8384E() {
        if (this.f4945d) {
            return null;
        }
        SQLiteDatabase writableDatabase = this.f4944c.getWritableDatabase();
        if (writableDatabase != null) {
            return writableDatabase;
        }
        this.f4945d = true;
        return null;
    }

    /* renamed from: F */
    private final boolean m8385F() {
        return mo19016n().getDatabasePath("google_app_measurement_local.db").exists();
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:71:0x0100 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:62:0x00ee */
    /* JADX WARN: Type inference failed for: r2v0 */
    /* JADX WARN: Type inference failed for: r2v1, types: [boolean, int] */
    /* JADX WARN: Type inference failed for: r7v0 */
    /* JADX WARN: Type inference failed for: r7v1 */
    /* JADX WARN: Type inference failed for: r2v4 */
    /* JADX WARN: Type inference failed for: r7v2, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v3, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r7v4, types: [android.database.sqlite.SQLiteDatabase] */
    /* JADX WARN: Type inference failed for: r7v5 */
    /* JADX WARN: Type inference failed for: r7v6 */
    /* JADX WARN: Type inference failed for: r7v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00c5 A[SYNTHETIC, Splitter:B:47:0x00c5] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00e0  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f5  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00fa  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x0112  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0117  */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x0124  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0129  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x011a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x011a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x011a A[SYNTHETIC] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m8387a(int r17, byte[] r18) {
        /*
            r16 = this;
            r1 = r16
            r16.mo18880a()
            r16.mo18881c()
            boolean r0 = r1.f4945d
            r2 = 0
            if (r0 == 0) goto L_0x000e
            return r2
        L_0x000e:
            android.content.ContentValues r3 = new android.content.ContentValues
            r3.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r17)
            java.lang.String r4 = "type"
            r3.put(r4, r0)
            java.lang.String r0 = "entry"
            r4 = r18
            r3.put(r0, r4)
            r4 = 5
            r5 = 0
            r6 = 5
        L_0x0026:
            if (r5 >= r4) goto L_0x012d
            r7 = 0
            r8 = 1
            android.database.sqlite.SQLiteDatabase r9 = r16.m8384E()     // Catch:{ SQLiteFullException -> 0x00fe, SQLiteDatabaseLockedException -> 0x00ec, SQLiteException -> 0x00c1, all -> 0x00bd }
            if (r9 != 0) goto L_0x0038
            r1.f4945d = r8     // Catch:{ SQLiteFullException -> 0x00bb, SQLiteDatabaseLockedException -> 0x00ed, SQLiteException -> 0x00b7 }
            if (r9 == 0) goto L_0x0037
            r9.close()
        L_0x0037:
            return r2
        L_0x0038:
            r9.beginTransaction()     // Catch:{ SQLiteFullException -> 0x00bb, SQLiteDatabaseLockedException -> 0x00ed, SQLiteException -> 0x00b7 }
            r10 = 0
            java.lang.String r0 = "select count(1) from messages"
            android.database.Cursor r12 = r9.rawQuery(r0, r7)     // Catch:{ SQLiteFullException -> 0x00bb, SQLiteDatabaseLockedException -> 0x00ed, SQLiteException -> 0x00b7 }
            if (r12 == 0) goto L_0x0059
            boolean r0 = r12.moveToFirst()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            if (r0 == 0) goto L_0x0059
            long r10 = r12.getLong(r2)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            goto L_0x0059
        L_0x0050:
            r0 = move-exception
            goto L_0x0122
        L_0x0053:
            r0 = move-exception
            goto L_0x00b9
        L_0x0055:
            r0 = move-exception
            r7 = r12
            goto L_0x0100
        L_0x0059:
            java.lang.String r0 = "messages"
            r13 = 100000(0x186a0, double:4.94066E-319)
            int r15 = (r10 > r13 ? 1 : (r10 == r13 ? 0 : -1))
            if (r15 < 0) goto L_0x00a0
            com.google.android.gms.measurement.internal.f4 r15 = r16.mo19015l()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            com.google.android.gms.measurement.internal.h4 r15 = r15.mo19001t()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.String r4 = "Data loss, local db full"
            r15.mo19042a(r4)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            long r13 = r13 - r10
            r10 = 1
            long r13 = r13 + r10
            java.lang.String r4 = "rowid in (select rowid from messages order by rowid asc limit ?)"
            java.lang.String[] r10 = new java.lang.String[r8]     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.String r11 = java.lang.Long.toString(r13)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r10[r2] = r11     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            int r4 = r9.delete(r0, r4, r10)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            long r10 = (long) r4     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            int r4 = (r10 > r13 ? 1 : (r10 == r13 ? 0 : -1))
            if (r4 == 0) goto L_0x00a0
            com.google.android.gms.measurement.internal.f4 r4 = r16.mo19015l()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.String r15 = "Different delete count than expected in local db. expected, received, difference"
            java.lang.Long r2 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            java.lang.Long r8 = java.lang.Long.valueOf(r10)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            long r13 = r13 - r10
            java.lang.Long r10 = java.lang.Long.valueOf(r13)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r4.mo19045a(r15, r2, r8, r10)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
        L_0x00a0:
            r9.insertOrThrow(r0, r7, r3)     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r9.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            r9.endTransaction()     // Catch:{ SQLiteFullException -> 0x0055, SQLiteDatabaseLockedException -> 0x00b5, SQLiteException -> 0x0053, all -> 0x0050 }
            if (r12 == 0) goto L_0x00ae
            r12.close()
        L_0x00ae:
            if (r9 == 0) goto L_0x00b3
            r9.close()
        L_0x00b3:
            r2 = 1
            return r2
        L_0x00b5:
            r7 = r12
            goto L_0x00ed
        L_0x00b7:
            r0 = move-exception
            r12 = r7
        L_0x00b9:
            r7 = r9
            goto L_0x00c3
        L_0x00bb:
            r0 = move-exception
            goto L_0x0100
        L_0x00bd:
            r0 = move-exception
            r9 = r7
            r12 = r9
            goto L_0x0122
        L_0x00c1:
            r0 = move-exception
            r12 = r7
        L_0x00c3:
            if (r7 == 0) goto L_0x00ce
            boolean r2 = r7.inTransaction()     // Catch:{ all -> 0x00e9 }
            if (r2 == 0) goto L_0x00ce
            r7.endTransaction()     // Catch:{ all -> 0x00e9 }
        L_0x00ce:
            com.google.android.gms.measurement.internal.f4 r2 = r16.mo19015l()     // Catch:{ all -> 0x00e9 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x00e9 }
            java.lang.String r4 = "Error writing entry to local database"
            r2.mo19043a(r4, r0)     // Catch:{ all -> 0x00e9 }
            r2 = 1
            r1.f4945d = r2     // Catch:{ all -> 0x00e9 }
            if (r12 == 0) goto L_0x00e3
            r12.close()
        L_0x00e3:
            if (r7 == 0) goto L_0x011a
            r7.close()
            goto L_0x011a
        L_0x00e9:
            r0 = move-exception
            r9 = r7
            goto L_0x0122
        L_0x00ec:
            r9 = r7
        L_0x00ed:
            long r10 = (long) r6
            android.os.SystemClock.sleep(r10)     // Catch:{ all -> 0x0120 }
            int r6 = r6 + 20
            if (r7 == 0) goto L_0x00f8
            r7.close()
        L_0x00f8:
            if (r9 == 0) goto L_0x011a
            r9.close()
            goto L_0x011a
        L_0x00fe:
            r0 = move-exception
            r9 = r7
        L_0x0100:
            com.google.android.gms.measurement.internal.f4 r2 = r16.mo19015l()     // Catch:{ all -> 0x0120 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x0120 }
            java.lang.String r4 = "Error writing entry; local database full"
            r2.mo19043a(r4, r0)     // Catch:{ all -> 0x0120 }
            r2 = 1
            r1.f4945d = r2     // Catch:{ all -> 0x0120 }
            if (r7 == 0) goto L_0x0115
            r7.close()
        L_0x0115:
            if (r9 == 0) goto L_0x011a
            r9.close()
        L_0x011a:
            int r5 = r5 + 1
            r2 = 0
            r4 = 5
            goto L_0x0026
        L_0x0120:
            r0 = move-exception
            r12 = r7
        L_0x0122:
            if (r12 == 0) goto L_0x0127
            r12.close()
        L_0x0127:
            if (r9 == 0) goto L_0x012c
            r9.close()
        L_0x012c:
            throw r0
        L_0x012d:
            com.google.android.gms.measurement.internal.f4 r0 = r16.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()
            java.lang.String r2 = "Failed to write entry to local database"
            r0.mo19042a(r2)
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C2972a4.m8387a(int, byte[]):boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public final boolean mo18725A() {
        return false;
    }

    @WorkerThread
    /* renamed from: B */
    public final void mo18726B() {
        mo18880a();
        mo18881c();
        try {
            int delete = m8384E().delete("messages", null, null) + 0;
            if (delete > 0) {
                mo19015l().mo18996B().mo19043a("Reset local analytics data. records", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19043a("Error resetting local analytics data. error", e);
        }
    }

    @WorkerThread
    /* renamed from: C */
    public final boolean mo18727C() {
        return m8387a(3, new byte[0]);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0089, code lost:
        r3 = r3 + 1;
     */
    @androidx.annotation.WorkerThread
    /* renamed from: D */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo18728D() {
        /*
            r11 = this;
            java.lang.String r0 = "Error deleting app launch break from local database"
            r11.mo18881c()
            r11.mo18880a()
            boolean r1 = r11.f4945d
            r2 = 0
            if (r1 == 0) goto L_0x000e
            return r2
        L_0x000e:
            boolean r1 = r11.m8385F()
            if (r1 != 0) goto L_0x0015
            return r2
        L_0x0015:
            r1 = 5
            r3 = 0
            r4 = 5
        L_0x0018:
            if (r3 >= r1) goto L_0x0092
            r5 = 0
            r6 = 1
            android.database.sqlite.SQLiteDatabase r5 = r11.m8384E()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            if (r5 != 0) goto L_0x002a
            r11.f4945d = r6     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            if (r5 == 0) goto L_0x0029
            r5.close()
        L_0x0029:
            return r2
        L_0x002a:
            r5.beginTransaction()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            java.lang.String r7 = "messages"
            java.lang.String r8 = "type == ?"
            java.lang.String[] r9 = new java.lang.String[r6]     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r10 = 3
            java.lang.String r10 = java.lang.Integer.toString(r10)     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r9[r2] = r10     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r5.delete(r7, r8, r9)     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r5.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            r5.endTransaction()     // Catch:{ SQLiteFullException -> 0x0076, SQLiteDatabaseLockedException -> 0x006a, SQLiteException -> 0x004b }
            if (r5 == 0) goto L_0x0048
            r5.close()
        L_0x0048:
            return r6
        L_0x0049:
            r0 = move-exception
            goto L_0x008c
        L_0x004b:
            r7 = move-exception
            if (r5 == 0) goto L_0x0057
            boolean r8 = r5.inTransaction()     // Catch:{ all -> 0x0049 }
            if (r8 == 0) goto L_0x0057
            r5.endTransaction()     // Catch:{ all -> 0x0049 }
        L_0x0057:
            com.google.android.gms.measurement.internal.f4 r8 = r11.mo19015l()     // Catch:{ all -> 0x0049 }
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19001t()     // Catch:{ all -> 0x0049 }
            r8.mo19043a(r0, r7)     // Catch:{ all -> 0x0049 }
            r11.f4945d = r6     // Catch:{ all -> 0x0049 }
            if (r5 == 0) goto L_0x0089
            r5.close()
            goto L_0x0089
        L_0x006a:
            long r6 = (long) r4
            android.os.SystemClock.sleep(r6)     // Catch:{ all -> 0x0049 }
            int r4 = r4 + 20
            if (r5 == 0) goto L_0x0089
            r5.close()
            goto L_0x0089
        L_0x0076:
            r7 = move-exception
            com.google.android.gms.measurement.internal.f4 r8 = r11.mo19015l()     // Catch:{ all -> 0x0049 }
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19001t()     // Catch:{ all -> 0x0049 }
            r8.mo19043a(r0, r7)     // Catch:{ all -> 0x0049 }
            r11.f4945d = r6     // Catch:{ all -> 0x0049 }
            if (r5 == 0) goto L_0x0089
            r5.close()
        L_0x0089:
            int r3 = r3 + 1
            goto L_0x0018
        L_0x008c:
            if (r5 == 0) goto L_0x0091
            r5.close()
        L_0x0091:
            throw r0
        L_0x0092:
            com.google.android.gms.measurement.internal.f4 r0 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()
            java.lang.String r1 = "Error deleting app launch break from local database in reasonable time"
            r0.mo19042a(r1)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C2972a4.mo18728D():boolean");
    }

    /* renamed from: a */
    public final boolean mo18730a(zzan zzan) {
        Parcel obtain = Parcel.obtain();
        zzan.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return m8387a(0, marshall);
        }
        mo19015l().mo19002u().mo19042a("Event is too long for local database. Sending event directly to service");
        return false;
    }

    /* renamed from: a */
    public final boolean mo18731a(zzkq zzkq) {
        Parcel obtain = Parcel.obtain();
        zzkq.writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        obtain.recycle();
        if (marshall.length <= 131072) {
            return m8387a(1, marshall);
        }
        mo19015l().mo19002u().mo19042a("User property too long for local database. Sending directly to service");
        return false;
    }

    /* renamed from: a */
    public final boolean mo18732a(zzv zzv) {
        mo19011f();
        byte[] a = C3267z9.m9422a((Parcelable) zzv);
        if (a.length <= 131072) {
            return m8387a(2, a);
        }
        mo19015l().mo19002u().mo19042a("Conditional user property too long for local database. Sending directly to service");
        return false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:27:0x0059 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:193:0x0254 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:191:0x0254 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:189:0x0254 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:161:? */
    /* JADX INFO: additional move instructions added (6) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:39:0x008c */
    /* JADX WARN: Type inference failed for: r24v1 */
    /* JADX WARN: Type inference failed for: r24v2 */
    /* JADX WARN: Type inference failed for: r24v3 */
    /* JADX WARN: Type inference failed for: r24v4 */
    /* JADX WARN: Type inference failed for: r24v5 */
    /* JADX WARN: Type inference failed for: r24v6 */
    /* JADX WARN: Type inference failed for: r24v7 */
    /* JADX WARN: Type inference failed for: r24v8 */
    /* JADX WARN: Type inference failed for: r24v9 */
    /* JADX WARN: Type inference failed for: r24v12 */
    /* JADX WARN: Type inference failed for: r24v13 */
    /* JADX WARN: Type inference failed for: r24v15 */
    /* JADX WARN: Type inference failed for: r24v17 */
    /* JADX WARN: Type inference failed for: r24v22 */
    /* JADX WARN: Type inference failed for: r24v23 */
    /* JADX WARN: Type inference failed for: r24v24 */
    /* JADX WARN: Type inference failed for: r24v25 */
    /* JADX WARN: Type inference failed for: r24v26 */
    /* JADX WARN: Type inference failed for: r24v27 */
    /* JADX WARN: Type inference failed for: r24v28 */
    /* JADX WARN: Type inference failed for: r24v29 */
    /* JADX WARN: Type inference failed for: r24v30 */
    /* JADX WARN: Type inference failed for: r24v31 */
    /* JADX WARN: Type inference failed for: r24v35 */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:75|76|77|78) */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:90|91|92|93) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:62|63|64|65|198) */
    /* JADX WARNING: Code restructure failed: missing block: B:132:0x01ed, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x01ee, code lost:
        r13 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x01f2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:136:0x01f3, code lost:
        r13 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x01f6, code lost:
        r13 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x01f9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x01fa, code lost:
        r13 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0091, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0092, code lost:
        r24 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0096, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0097, code lost:
        r24 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x009b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x009c, code lost:
        r24 = r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:?, code lost:
        mo19015l().mo19001t().mo19042a("Failed to load event from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        r11.recycle();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:?, code lost:
        mo19015l().mo19001t().mo19042a("Failed to load user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:?, code lost:
        r11.recycle();
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:?, code lost:
        mo19015l().mo19001t().mo19042a("Failed to load conditional user property from local database");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:?, code lost:
        r11.recycle();
        r0 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:62:0x00ee */
    /* JADX WARNING: Missing exception handler attribute for start block: B:75:0x011e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:90:0x0154 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:139:? A[ExcHandler: SQLiteDatabaseLockedException (unused android.database.sqlite.SQLiteDatabaseLockedException), SYNTHETIC, Splitter:B:12:0x0029] */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x0206 A[SYNTHETIC, Splitter:B:149:0x0206] */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x021e  */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x0223  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0231  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x024c  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0251  */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x025b  */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x0260  */
    /* JADX WARNING: Removed duplicated region for block: B:189:0x0254 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x0254 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0254 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable> mo18729a(int r24) {
        /*
            r23 = this;
            r1 = r23
            java.lang.String r2 = "Error reading entries from local database"
            r23.mo18881c()
            r23.mo18880a()
            boolean r0 = r1.f4945d
            r3 = 0
            if (r0 == 0) goto L_0x0010
            return r3
        L_0x0010:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            boolean r0 = r23.m8385F()
            if (r0 != 0) goto L_0x001c
            return r4
        L_0x001c:
            r5 = 5
            r6 = 0
            r7 = 0
            r8 = 5
        L_0x0020:
            if (r7 >= r5) goto L_0x0264
            r9 = 1
            android.database.sqlite.SQLiteDatabase r15 = r23.m8384E()     // Catch:{ SQLiteFullException -> 0x023a, SQLiteDatabaseLockedException -> 0x0227, SQLiteException -> 0x0201, all -> 0x01fd }
            if (r15 != 0) goto L_0x0040
            r1.f4945d = r9     // Catch:{ SQLiteFullException -> 0x003b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0036, all -> 0x0031 }
            if (r15 == 0) goto L_0x0030
            r15.close()
        L_0x0030:
            return r3
        L_0x0031:
            r0 = move-exception
            r10 = r3
            r13 = r15
            goto L_0x0259
        L_0x0036:
            r0 = move-exception
            r10 = r3
            r13 = r15
            goto L_0x0204
        L_0x003b:
            r0 = move-exception
            r10 = r3
            r13 = r15
            goto L_0x023d
        L_0x0040:
            r15.beginTransaction()     // Catch:{ SQLiteFullException -> 0x01f9, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x01f2, all -> 0x01ed }
            com.google.android.gms.measurement.internal.la r0 = r23.mo19013h()     // Catch:{ SQLiteFullException -> 0x01f9, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x01f2, all -> 0x01ed }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.C3135o.f5483y0     // Catch:{ SQLiteFullException -> 0x01f9, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x01f2, all -> 0x01ed }
            boolean r0 = r0.mo19146a(r10)     // Catch:{ SQLiteFullException -> 0x01f9, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x01f2, all -> 0x01ed }
            r10 = 100
            java.lang.String r11 = "entry"
            java.lang.String r12 = "type"
            java.lang.String r13 = "rowid"
            r19 = -1
            if (r0 == 0) goto L_0x00a0
            long r16 = m8386a(r15)     // Catch:{ SQLiteFullException -> 0x009b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0096, all -> 0x0091 }
            int r0 = (r16 > r19 ? 1 : (r16 == r19 ? 0 : -1))
            if (r0 == 0) goto L_0x006c
            java.lang.String r0 = "rowid<?"
            java.lang.String[] r14 = new java.lang.String[r9]     // Catch:{ SQLiteFullException -> 0x003b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0036, all -> 0x0031 }
            java.lang.String r16 = java.lang.String.valueOf(r16)     // Catch:{ SQLiteFullException -> 0x003b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0036, all -> 0x0031 }
            r14[r6] = r16     // Catch:{ SQLiteFullException -> 0x003b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0036, all -> 0x0031 }
            goto L_0x006e
        L_0x006c:
            r0 = r3
            r14 = r0
        L_0x006e:
            java.lang.String r16 = "messages"
            java.lang.String[] r12 = new java.lang.String[]{r13, r12, r11}     // Catch:{ SQLiteFullException -> 0x009b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0096, all -> 0x0091 }
            r17 = 0
            r18 = 0
            java.lang.String r21 = "rowid asc"
            java.lang.String r22 = java.lang.Integer.toString(r10)     // Catch:{ SQLiteFullException -> 0x009b, SQLiteDatabaseLockedException -> 0x01f6, SQLiteException -> 0x0096, all -> 0x0091 }
            r10 = r15
            r11 = r16
            r13 = r0
            r24 = r15
            r15 = r17
            r16 = r18
            r17 = r21
            r18 = r22
            android.database.Cursor r0 = r10.query(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteFullException -> 0x01e9, SQLiteDatabaseLockedException -> 0x01e6, SQLiteException -> 0x01e2, all -> 0x01de }
            goto L_0x00ba
        L_0x0091:
            r0 = move-exception
            r24 = r15
            goto L_0x01df
        L_0x0096:
            r0 = move-exception
            r24 = r15
            goto L_0x01e3
        L_0x009b:
            r0 = move-exception
            r24 = r15
            goto L_0x01ea
        L_0x00a0:
            r24 = r15
            java.lang.String r0 = "messages"
            java.lang.String[] r12 = new java.lang.String[]{r13, r12, r11}     // Catch:{ SQLiteFullException -> 0x01e9, SQLiteDatabaseLockedException -> 0x01e6, SQLiteException -> 0x01e2, all -> 0x01de }
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            java.lang.String r17 = "rowid asc"
            java.lang.String r18 = java.lang.Integer.toString(r10)     // Catch:{ SQLiteFullException -> 0x01e9, SQLiteDatabaseLockedException -> 0x01e6, SQLiteException -> 0x01e2, all -> 0x01de }
            r10 = r24
            r11 = r0
            android.database.Cursor r0 = r10.query(r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ SQLiteFullException -> 0x01e9, SQLiteDatabaseLockedException -> 0x01e6, SQLiteException -> 0x01e2, all -> 0x01de }
        L_0x00ba:
            r10 = r0
        L_0x00bb:
            boolean r0 = r10.moveToNext()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            if (r0 == 0) goto L_0x0191
            long r19 = r10.getLong(r6)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            int r0 = r10.getInt(r9)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            r11 = 2
            byte[] r12 = r10.getBlob(r11)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            if (r0 != 0) goto L_0x0103
            android.os.Parcel r11 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            int r0 = r12.length     // Catch:{ a -> 0x00ee }
            r11.unmarshall(r12, r6, r0)     // Catch:{ a -> 0x00ee }
            r11.setDataPosition(r6)     // Catch:{ a -> 0x00ee }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzan> r0 = com.google.android.gms.measurement.internal.zzan.CREATOR     // Catch:{ a -> 0x00ee }
            java.lang.Object r0 = r0.createFromParcel(r11)     // Catch:{ a -> 0x00ee }
            com.google.android.gms.measurement.internal.zzan r0 = (com.google.android.gms.measurement.internal.zzan) r0     // Catch:{ a -> 0x00ee }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            if (r0 == 0) goto L_0x00bb
            r4.add(r0)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x00bb
        L_0x00ec:
            r0 = move-exception
            goto L_0x00ff
        L_0x00ee:
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()     // Catch:{ all -> 0x00ec }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ all -> 0x00ec }
            java.lang.String r12 = "Failed to load event from local database"
            r0.mo19042a(r12)     // Catch:{ all -> 0x00ec }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x00bb
        L_0x00ff:
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            throw r0     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
        L_0x0103:
            if (r0 != r9) goto L_0x0139
            android.os.Parcel r11 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            int r0 = r12.length     // Catch:{ a -> 0x011e }
            r11.unmarshall(r12, r6, r0)     // Catch:{ a -> 0x011e }
            r11.setDataPosition(r6)     // Catch:{ a -> 0x011e }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzkq> r0 = com.google.android.gms.measurement.internal.zzkq.CREATOR     // Catch:{ a -> 0x011e }
            java.lang.Object r0 = r0.createFromParcel(r11)     // Catch:{ a -> 0x011e }
            com.google.android.gms.measurement.internal.zzkq r0 = (com.google.android.gms.measurement.internal.zzkq) r0     // Catch:{ a -> 0x011e }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x012f
        L_0x011c:
            r0 = move-exception
            goto L_0x0135
        L_0x011e:
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()     // Catch:{ all -> 0x011c }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ all -> 0x011c }
            java.lang.String r12 = "Failed to load user property from local database"
            r0.mo19042a(r12)     // Catch:{ all -> 0x011c }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            r0 = r3
        L_0x012f:
            if (r0 == 0) goto L_0x00bb
            r4.add(r0)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x00bb
        L_0x0135:
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            throw r0     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
        L_0x0139:
            if (r0 != r11) goto L_0x0170
            android.os.Parcel r11 = android.os.Parcel.obtain()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            int r0 = r12.length     // Catch:{ a -> 0x0154 }
            r11.unmarshall(r12, r6, r0)     // Catch:{ a -> 0x0154 }
            r11.setDataPosition(r6)     // Catch:{ a -> 0x0154 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzv> r0 = com.google.android.gms.measurement.internal.zzv.CREATOR     // Catch:{ a -> 0x0154 }
            java.lang.Object r0 = r0.createFromParcel(r11)     // Catch:{ a -> 0x0154 }
            com.google.android.gms.measurement.internal.zzv r0 = (com.google.android.gms.measurement.internal.zzv) r0     // Catch:{ a -> 0x0154 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x0165
        L_0x0152:
            r0 = move-exception
            goto L_0x016c
        L_0x0154:
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()     // Catch:{ all -> 0x0152 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ all -> 0x0152 }
            java.lang.String r12 = "Failed to load conditional user property from local database"
            r0.mo19042a(r12)     // Catch:{ all -> 0x0152 }
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            r0 = r3
        L_0x0165:
            if (r0 == 0) goto L_0x00bb
            r4.add(r0)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x00bb
        L_0x016c:
            r11.recycle()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            throw r0     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
        L_0x0170:
            r11 = 3
            if (r0 != r11) goto L_0x0182
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            java.lang.String r11 = "Skipping app launch break"
            r0.mo19042a(r11)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x00bb
        L_0x0182:
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            java.lang.String r11 = "Unknown record type in local database"
            r0.mo19042a(r11)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            goto L_0x00bb
        L_0x0191:
            java.lang.String r0 = "messages"
            java.lang.String r11 = "rowid <= ?"
            java.lang.String[] r12 = new java.lang.String[r9]     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            java.lang.String r13 = java.lang.Long.toString(r19)     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            r12[r6] = r13     // Catch:{ SQLiteFullException -> 0x01d9, SQLiteDatabaseLockedException -> 0x01d5, SQLiteException -> 0x01d1, all -> 0x01cc }
            r13 = r24
            int r0 = r13.delete(r0, r11, r12)     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
            int r11 = r4.size()     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
            if (r0 >= r11) goto L_0x01b6
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
            java.lang.String r11 = "Fewer entries removed from local database than expected"
            r0.mo19042a(r11)     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
        L_0x01b6:
            r13.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
            r13.endTransaction()     // Catch:{ SQLiteFullException -> 0x01c9, SQLiteDatabaseLockedException -> 0x0229, SQLiteException -> 0x01c7 }
            if (r10 == 0) goto L_0x01c1
            r10.close()
        L_0x01c1:
            if (r13 == 0) goto L_0x01c6
            r13.close()
        L_0x01c6:
            return r4
        L_0x01c7:
            r0 = move-exception
            goto L_0x0204
        L_0x01c9:
            r0 = move-exception
            goto L_0x023d
        L_0x01cc:
            r0 = move-exception
            r13 = r24
            goto L_0x0259
        L_0x01d1:
            r0 = move-exception
            r13 = r24
            goto L_0x0204
        L_0x01d5:
            r13 = r24
            goto L_0x0229
        L_0x01d9:
            r0 = move-exception
            r13 = r24
            goto L_0x023d
        L_0x01de:
            r0 = move-exception
        L_0x01df:
            r13 = r24
            goto L_0x01ef
        L_0x01e2:
            r0 = move-exception
        L_0x01e3:
            r13 = r24
            goto L_0x01f4
        L_0x01e6:
            r13 = r24
            goto L_0x01f7
        L_0x01e9:
            r0 = move-exception
        L_0x01ea:
            r13 = r24
            goto L_0x01fb
        L_0x01ed:
            r0 = move-exception
            r13 = r15
        L_0x01ef:
            r10 = r3
            goto L_0x0259
        L_0x01f2:
            r0 = move-exception
            r13 = r15
        L_0x01f4:
            r10 = r3
            goto L_0x0204
        L_0x01f6:
            r13 = r15
        L_0x01f7:
            r10 = r3
            goto L_0x0229
        L_0x01f9:
            r0 = move-exception
            r13 = r15
        L_0x01fb:
            r10 = r3
            goto L_0x023d
        L_0x01fd:
            r0 = move-exception
            r10 = r3
            r13 = r10
            goto L_0x0259
        L_0x0201:
            r0 = move-exception
            r10 = r3
            r13 = r10
        L_0x0204:
            if (r13 == 0) goto L_0x020f
            boolean r11 = r13.inTransaction()     // Catch:{ all -> 0x0258 }
            if (r11 == 0) goto L_0x020f
            r13.endTransaction()     // Catch:{ all -> 0x0258 }
        L_0x020f:
            com.google.android.gms.measurement.internal.f4 r11 = r23.mo19015l()     // Catch:{ all -> 0x0258 }
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo19001t()     // Catch:{ all -> 0x0258 }
            r11.mo19043a(r2, r0)     // Catch:{ all -> 0x0258 }
            r1.f4945d = r9     // Catch:{ all -> 0x0258 }
            if (r10 == 0) goto L_0x0221
            r10.close()
        L_0x0221:
            if (r13 == 0) goto L_0x0254
            r13.close()
            goto L_0x0254
        L_0x0227:
            r10 = r3
            r13 = r10
        L_0x0229:
            long r11 = (long) r8
            android.os.SystemClock.sleep(r11)     // Catch:{ all -> 0x0258 }
            int r8 = r8 + 20
            if (r10 == 0) goto L_0x0234
            r10.close()
        L_0x0234:
            if (r13 == 0) goto L_0x0254
            r13.close()
            goto L_0x0254
        L_0x023a:
            r0 = move-exception
            r10 = r3
            r13 = r10
        L_0x023d:
            com.google.android.gms.measurement.internal.f4 r11 = r23.mo19015l()     // Catch:{ all -> 0x0258 }
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo19001t()     // Catch:{ all -> 0x0258 }
            r11.mo19043a(r2, r0)     // Catch:{ all -> 0x0258 }
            r1.f4945d = r9     // Catch:{ all -> 0x0258 }
            if (r10 == 0) goto L_0x024f
            r10.close()
        L_0x024f:
            if (r13 == 0) goto L_0x0254
            r13.close()
        L_0x0254:
            int r7 = r7 + 1
            goto L_0x0020
        L_0x0258:
            r0 = move-exception
        L_0x0259:
            if (r10 == 0) goto L_0x025e
            r10.close()
        L_0x025e:
            if (r13 == 0) goto L_0x0263
            r13.close()
        L_0x0263:
            throw r0
        L_0x0264:
            com.google.android.gms.measurement.internal.f4 r0 = r23.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()
            java.lang.String r2 = "Failed to read events from database in reasonable time"
            r0.mo19042a(r2)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C2972a4.mo18729a(int):java.util.List");
    }

    /* renamed from: a */
    private static long m8386a(SQLiteDatabase sQLiteDatabase) {
        Cursor cursor = null;
        try {
            cursor = sQLiteDatabase.query("messages", new String[]{"rowid"}, "type=?", new String[]{ExifInterface.GPS_MEASUREMENT_3D}, null, null, "rowid desc", AppEventsConstants.EVENT_PARAM_VALUE_YES);
            if (cursor.moveToFirst()) {
                return cursor.getLong(0);
            }
            if (cursor == null) {
                return -1;
            }
            cursor.close();
            return -1;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
