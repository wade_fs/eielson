package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.ArrayList;
import java.util.List;

public final class PolylineOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<PolylineOptions> CREATOR = new C2947r();

    /* renamed from: P */
    private final List<LatLng> f4873P;

    /* renamed from: Q */
    private float f4874Q;

    /* renamed from: R */
    private int f4875R;

    /* renamed from: S */
    private float f4876S;

    /* renamed from: T */
    private boolean f4877T;

    /* renamed from: U */
    private boolean f4878U;

    /* renamed from: V */
    private boolean f4879V;
    @NonNull

    /* renamed from: W */
    private Cap f4880W;
    @NonNull

    /* renamed from: X */
    private Cap f4881X;

    /* renamed from: Y */
    private int f4882Y;
    @Nullable

    /* renamed from: Z */
    private List<PatternItem> f4883Z;

    public PolylineOptions() {
        this.f4874Q = 10.0f;
        this.f4875R = ViewCompat.MEASURED_STATE_MASK;
        this.f4876S = 0.0f;
        this.f4877T = true;
        this.f4878U = false;
        this.f4879V = false;
        this.f4880W = new ButtCap();
        this.f4881X = new ButtCap();
        this.f4882Y = 0;
        this.f4883Z = null;
        this.f4873P = new ArrayList();
    }

    /* renamed from: A */
    public final boolean mo18571A() {
        return this.f4879V;
    }

    /* renamed from: B */
    public final boolean mo18572B() {
        return this.f4878U;
    }

    /* renamed from: C */
    public final boolean mo18573C() {
        return this.f4877T;
    }

    /* renamed from: a */
    public final PolylineOptions mo18575a(LatLng latLng) {
        this.f4873P.add(latLng);
        return this;
    }

    /* renamed from: c */
    public final PolylineOptions mo18578c(Iterable<LatLng> iterable) {
        for (LatLng latLng : iterable) {
            this.f4873P.add(latLng);
        }
        return this;
    }

    /* renamed from: d */
    public final PolylineOptions mo18580d(int i) {
        this.f4875R = i;
        return this;
    }

    /* renamed from: e */
    public final PolylineOptions mo18581e(int i) {
        this.f4882Y = i;
        return this;
    }

    /* renamed from: u */
    public final int mo18582u() {
        return this.f4882Y;
    }

    @Nullable
    /* renamed from: v */
    public final List<PatternItem> mo18583v() {
        return this.f4883Z;
    }

    /* renamed from: w */
    public final List<LatLng> mo18584w() {
        return this.f4873P;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.Cap, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5614c(parcel, 2, mo18584w(), false);
        C2250b.m5590a(parcel, 3, mo18587y());
        C2250b.m5591a(parcel, 4, mo18577c());
        C2250b.m5590a(parcel, 5, mo18588z());
        C2250b.m5605a(parcel, 6, mo18573C());
        C2250b.m5605a(parcel, 7, mo18572B());
        C2250b.m5605a(parcel, 8, mo18571A());
        C2250b.m5596a(parcel, 9, (Parcelable) mo18586x(), i, false);
        C2250b.m5596a(parcel, 10, (Parcelable) mo18579d(), i, false);
        C2250b.m5591a(parcel, 11, mo18582u());
        C2250b.m5614c(parcel, 12, mo18583v(), false);
        C2250b.m5587a(parcel, a);
    }

    @NonNull
    /* renamed from: x */
    public final Cap mo18586x() {
        return this.f4880W;
    }

    /* renamed from: y */
    public final float mo18587y() {
        return this.f4874Q;
    }

    /* renamed from: z */
    public final float mo18588z() {
        return this.f4876S;
    }

    /* renamed from: a */
    public final PolylineOptions mo18574a(float f) {
        this.f4874Q = f;
        return this;
    }

    @NonNull
    /* renamed from: d */
    public final Cap mo18579d() {
        return this.f4881X;
    }

    /* renamed from: a */
    public final PolylineOptions mo18576a(boolean z) {
        this.f4878U = z;
        return this;
    }

    /* renamed from: c */
    public final int mo18577c() {
        return this.f4875R;
    }

    PolylineOptions(List list, float f, int i, float f2, boolean z, boolean z2, boolean z3, @Nullable Cap cap, @Nullable Cap cap2, int i2, @Nullable List<PatternItem> list2) {
        this.f4874Q = 10.0f;
        this.f4875R = ViewCompat.MEASURED_STATE_MASK;
        this.f4876S = 0.0f;
        this.f4877T = true;
        this.f4878U = false;
        this.f4879V = false;
        this.f4880W = new ButtCap();
        this.f4881X = new ButtCap();
        this.f4882Y = 0;
        this.f4883Z = null;
        this.f4873P = list;
        this.f4874Q = f;
        this.f4875R = i;
        this.f4876S = f2;
        this.f4877T = z;
        this.f4878U = z2;
        this.f4879V = z3;
        if (cap != null) {
            this.f4880W = cap;
        }
        if (cap2 != null) {
            this.f4881X = cap2;
        }
        this.f4882Y = i2;
        this.f4883Z = list2;
    }
}
