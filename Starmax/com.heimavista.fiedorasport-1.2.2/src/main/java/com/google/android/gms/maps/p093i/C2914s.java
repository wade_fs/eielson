package com.google.android.gms.maps.p093i;

import android.os.Parcel;
import p119e.p144d.p145a.p157c.p161c.p165d.C4038g;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;
import p119e.p144d.p145a.p157c.p161c.p165d.C4044m;

/* renamed from: com.google.android.gms.maps.i.s */
public abstract class C2914s extends C4038g implements C2913r {
    public C2914s() {
        super("com.google.android.gms.maps.internal.IOnMarkerClickListener");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo18485a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        boolean a = mo18488a(C4044m.m12058a(parcel.readStrongBinder()));
        parcel2.writeNoException();
        C4039h.m12045a(parcel2, a);
        return true;
    }
}
