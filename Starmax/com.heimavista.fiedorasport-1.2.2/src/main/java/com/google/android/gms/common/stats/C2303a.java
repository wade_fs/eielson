package com.google.android.gms.common.stats;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;
import com.google.android.gms.common.util.C2313d;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.common.stats.a */
public class C2303a {

    /* renamed from: a */
    private static final Object f3842a = new Object();

    /* renamed from: b */
    private static volatile C2303a f3843b;

    private C2303a() {
        List list = Collections.EMPTY_LIST;
    }

    /* renamed from: a */
    public static C2303a m5745a() {
        if (f3843b == null) {
            synchronized (f3842a) {
                if (f3843b == null) {
                    f3843b = new C2303a();
                }
            }
        }
        return f3843b;
    }

    /* renamed from: a */
    public final boolean mo17125a(Context context, String str, Intent intent, ServiceConnection serviceConnection, int i) {
        boolean z;
        ComponentName component = intent.getComponent();
        if (component == null) {
            z = false;
        } else {
            z = C2313d.m5772a(context, component.getPackageName());
        }
        if (!z) {
            return context.bindService(intent, serviceConnection, i);
        }
        Log.w("ConnectionTracker", "Attempted to bind to a service in a STOPPED package.");
        return false;
    }

    /* renamed from: a */
    public boolean mo17124a(Context context, Intent intent, ServiceConnection serviceConnection, int i) {
        return mo17125a(context, context.getClass().getName(), intent, serviceConnection, i);
    }

    @SuppressLint({"UntrackedBindService"})
    /* renamed from: a */
    public void mo17123a(Context context, ServiceConnection serviceConnection) {
        context.unbindService(serviceConnection);
    }
}
