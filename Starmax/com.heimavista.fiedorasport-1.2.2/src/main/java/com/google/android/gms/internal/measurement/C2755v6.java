package com.google.android.gms.internal.measurement;

import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.v6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2755v6 implements Comparable<C2755v6>, Map.Entry<K, V> {

    /* renamed from: P */
    private final K f4536P;

    /* renamed from: Q */
    private V f4537Q;

    /* renamed from: R */
    private final /* synthetic */ C2618m6 f4538R;

    C2755v6(C2618m6 m6Var, Map.Entry<K, V> entry) {
        this(m6Var, (Comparable) entry.getKey(), entry.getValue());
    }

    /* renamed from: a */
    private static boolean m7447a(Object obj, Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    public final /* synthetic */ int compareTo(Object obj) {
        return ((Comparable) getKey()).compareTo((Comparable) ((C2755v6) obj).getKey());
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        return m7447a(this.f4536P, entry.getKey()) && m7447a(this.f4537Q, entry.getValue());
    }

    public final /* synthetic */ Object getKey() {
        return this.f4536P;
    }

    public final V getValue() {
        return this.f4537Q;
    }

    public final int hashCode() {
        K k = this.f4536P;
        int i = 0;
        int hashCode = k == null ? 0 : k.hashCode();
        V v = this.f4537Q;
        if (v != null) {
            i = v.hashCode();
        }
        return hashCode ^ i;
    }

    public final V setValue(V v) {
        this.f4538R.m6785f();
        V v2 = this.f4537Q;
        this.f4537Q = v;
        return v2;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f4536P);
        String valueOf2 = String.valueOf(this.f4537Q);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 1 + String.valueOf(valueOf2).length());
        sb.append(valueOf);
        sb.append("=");
        sb.append(valueOf2);
        return sb.toString();
    }

    C2755v6(C2618m6 m6Var, K k, V v) {
        this.f4538R = m6Var;
        this.f4536P = k;
        this.f4537Q = v;
    }
}
