package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2556ic;

/* renamed from: com.google.android.gms.measurement.internal.h1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3053h1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5170a = new C3053h1();

    private C3053h1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2556ic.m6491b());
    }
}
