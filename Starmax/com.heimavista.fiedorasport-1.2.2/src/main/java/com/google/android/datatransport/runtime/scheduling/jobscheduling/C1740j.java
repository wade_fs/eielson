package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.runtime.backends.C1716g;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.j */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1740j implements C3969b.C3970a {

    /* renamed from: a */
    private final C1743m f2746a;

    /* renamed from: b */
    private final C1716g f2747b;

    /* renamed from: c */
    private final Iterable f2748c;

    /* renamed from: d */
    private final C3905l f2749d;

    /* renamed from: e */
    private final int f2750e;

    private C1740j(C1743m mVar, C1716g gVar, Iterable iterable, C3905l lVar, int i) {
        this.f2746a = mVar;
        this.f2747b = gVar;
        this.f2748c = iterable;
        this.f2749d = lVar;
        this.f2750e = i;
    }

    /* renamed from: a */
    public static C3969b.C3970a m4347a(C1743m mVar, C1716g gVar, Iterable iterable, C3905l lVar, int i) {
        return new C1740j(mVar, gVar, iterable, lVar, i);
    }

    /* renamed from: s */
    public Object mo13587s() {
        return C1743m.m4354a(this.f2746a, this.f2747b, this.f2748c, this.f2749d, this.f2750e);
    }
}
