package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2477e0;

/* renamed from: com.google.android.gms.internal.measurement.i0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C2544i0 implements C2661p4 {

    /* renamed from: a */
    static final C2661p4 f4225a = new C2544i0();

    private C2544i0() {
    }

    /* renamed from: a */
    public final boolean mo17363a(int i) {
        return C2477e0.C2479b.m6243a(i) != null;
    }
}
