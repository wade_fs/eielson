package com.google.android.gms.common.api.internal;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2031c;
import java.util.Collections;
import java.util.Map;
import p119e.p144d.p145a.p157c.p167e.C4057c;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.android.gms.common.api.internal.q */
final class C2119q implements C4057c<Map<C2063d2<?>, String>> {

    /* renamed from: a */
    private C2099l f3421a;

    /* renamed from: b */
    private final /* synthetic */ C2130s2 f3422b;

    C2119q(C2130s2 s2Var, C2099l lVar) {
        this.f3422b = s2Var;
        this.f3421a = lVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16765a() {
        this.f3421a.onComplete();
    }

    /* renamed from: a */
    public final void mo16766a(@NonNull C4065h<Map<C2063d2<?>, String>> hVar) {
        this.f3422b.f3450f.lock();
        try {
            if (!this.f3422b.f3458n) {
                this.f3421a.onComplete();
                return;
            }
            if (hVar.mo23714e()) {
                Map unused = this.f3422b.f3460p = new ArrayMap(this.f3422b.f3446b.size());
                for (C2126r2 r2Var : this.f3422b.f3446b.values()) {
                    this.f3422b.f3460p.put(r2Var.mo16559h(), ConnectionResult.f3156T);
                }
            } else if (hVar.mo23704a() instanceof C2031c) {
                C2031c cVar = (C2031c) hVar.mo23704a();
                if (this.f3422b.f3456l) {
                    Map unused2 = this.f3422b.f3460p = new ArrayMap(this.f3422b.f3446b.size());
                    for (C2126r2 r2Var2 : this.f3422b.f3446b.values()) {
                        C2063d2 h = r2Var2.mo16559h();
                        ConnectionResult a = cVar.mo16545a(r2Var2);
                        if (this.f3422b.m5088a(r2Var2, a)) {
                            this.f3422b.f3460p.put(h, new ConnectionResult(16));
                        } else {
                            this.f3422b.f3460p.put(h, a);
                        }
                    }
                } else {
                    Map unused3 = this.f3422b.f3460p = cVar.mo16544a();
                }
            } else {
                Log.e("ConnectionlessGAC", "Unexpected availability exception", hVar.mo23704a());
                Map unused4 = this.f3422b.f3460p = Collections.emptyMap();
            }
            if (this.f3422b.mo16713c()) {
                this.f3422b.f3459o.putAll(this.f3422b.f3460p);
                if (this.f3422b.m5105k() == null) {
                    this.f3422b.m5101i();
                    this.f3422b.m5103j();
                    this.f3422b.f3453i.signalAll();
                }
            }
            this.f3421a.onComplete();
            this.f3422b.f3450f.unlock();
        } finally {
            this.f3422b.f3450f.unlock();
        }
    }
}
