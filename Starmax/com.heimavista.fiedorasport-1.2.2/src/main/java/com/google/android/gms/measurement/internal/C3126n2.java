package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2606l9;

/* renamed from: com.google.android.gms.measurement.internal.n2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3126n2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5363a = new C3126n2();

    private C3126n2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2606l9.m6738d());
    }
}
