package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.internal.measurement.zc */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2823zc extends C2412a implements C2574jb {
    C2823zc(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IAppMeasurementDynamiteService");
    }

    public final void beginAdUnitExposure(String str, long j) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeLong(j);
        mo17246b(23, H);
    }

    public final void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7046a(H, bundle);
        mo17246b(9, H);
    }

    public final void endAdUnitExposure(String str, long j) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeLong(j);
        mo17246b(24, H);
    }

    public final void generateEventId(C2589kc kcVar) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, kcVar);
        mo17246b(22, H);
    }

    public final void getCachedAppInstanceId(C2589kc kcVar) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, kcVar);
        mo17246b(19, H);
    }

    public final void getConditionalUserProperties(String str, String str2, C2589kc kcVar) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7045a(H, kcVar);
        mo17246b(10, H);
    }

    public final void getCurrentScreenClass(C2589kc kcVar) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, kcVar);
        mo17246b(17, H);
    }

    public final void getCurrentScreenName(C2589kc kcVar) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, kcVar);
        mo17246b(16, H);
    }

    public final void getGmpAppId(C2589kc kcVar) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, kcVar);
        mo17246b(21, H);
    }

    public final void getMaxUserProperties(String str, C2589kc kcVar) {
        Parcel H = mo17243H();
        H.writeString(str);
        C2670q.m7045a(H, kcVar);
        mo17246b(6, H);
    }

    public final void getUserProperties(String str, String str2, boolean z, C2589kc kcVar) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7047a(H, z);
        C2670q.m7045a(H, kcVar);
        mo17246b(5, H);
    }

    public final void initialize(C3988b bVar, zzv zzv, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        C2670q.m7046a(H, zzv);
        H.writeLong(j);
        mo17246b(1, H);
    }

    public final void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7046a(H, bundle);
        C2670q.m7047a(H, z);
        C2670q.m7047a(H, z2);
        H.writeLong(j);
        mo17246b(2, H);
    }

    public final void logHealthData(int i, String str, C3988b bVar, C3988b bVar2, C3988b bVar3) {
        Parcel H = mo17243H();
        H.writeInt(i);
        H.writeString(str);
        C2670q.m7045a(H, bVar);
        C2670q.m7045a(H, bVar2);
        C2670q.m7045a(H, bVar3);
        mo17246b(33, H);
    }

    public final void onActivityCreated(C3988b bVar, Bundle bundle, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        C2670q.m7046a(H, bundle);
        H.writeLong(j);
        mo17246b(27, H);
    }

    public final void onActivityDestroyed(C3988b bVar, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        H.writeLong(j);
        mo17246b(28, H);
    }

    public final void onActivityPaused(C3988b bVar, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        H.writeLong(j);
        mo17246b(29, H);
    }

    public final void onActivityResumed(C3988b bVar, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        H.writeLong(j);
        mo17246b(30, H);
    }

    public final void onActivitySaveInstanceState(C3988b bVar, C2589kc kcVar, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        C2670q.m7045a(H, kcVar);
        H.writeLong(j);
        mo17246b(31, H);
    }

    public final void onActivityStarted(C3988b bVar, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        H.writeLong(j);
        mo17246b(25, H);
    }

    public final void onActivityStopped(C3988b bVar, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        H.writeLong(j);
        mo17246b(26, H);
    }

    public final void setConditionalUserProperty(Bundle bundle, long j) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, bundle);
        H.writeLong(j);
        mo17246b(8, H);
    }

    public final void setCurrentScreen(C3988b bVar, String str, String str2, long j) {
        Parcel H = mo17243H();
        C2670q.m7045a(H, bVar);
        H.writeString(str);
        H.writeString(str2);
        H.writeLong(j);
        mo17246b(15, H);
    }

    public final void setDataCollectionEnabled(boolean z) {
        Parcel H = mo17243H();
        C2670q.m7047a(H, z);
        mo17246b(39, H);
    }

    public final void setUserProperty(String str, String str2, C3988b bVar, boolean z, long j) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7045a(H, bVar);
        C2670q.m7047a(H, z);
        H.writeLong(j);
        mo17246b(4, H);
    }
}
