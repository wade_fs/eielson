package com.google.android.gms.internal.measurement;

import android.content.SharedPreferences;

/* renamed from: com.google.android.gms.internal.measurement.e2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C2481e2 implements SharedPreferences.OnSharedPreferenceChangeListener {

    /* renamed from: a */
    private final C2432b2 f4068a;

    C2481e2(C2432b2 b2Var) {
        this.f4068a = b2Var;
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.f4068a.mo17309a(sharedPreferences, str);
    }
}
