package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.internal.C2224j;
import com.google.android.gms.common.stats.C2303a;
import java.util.HashMap;
import p119e.p144d.p145a.p157c.p161c.p163b.C4012d;

/* renamed from: com.google.android.gms.common.internal.q0 */
final class C2243q0 extends C2224j implements Handler.Callback {
    /* access modifiers changed from: private */

    /* renamed from: R */
    public final HashMap<C2224j.C2225a, C2245r0> f3738R = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: S */
    public final Context f3739S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public final Handler f3740T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public final C2303a f3741U;

    /* renamed from: V */
    private final long f3742V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public final long f3743W;

    C2243q0(Context context) {
        this.f3739S = context.getApplicationContext();
        this.f3740T = new C4012d(context.getMainLooper(), this);
        this.f3741U = C2303a.m5745a();
        this.f3742V = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
        this.f3743W = 300000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo16989a(C2224j.C2225a aVar, ServiceConnection serviceConnection, String str) {
        boolean d;
        C2258v.m5630a(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.f3738R) {
            C2245r0 r0Var = this.f3738R.get(aVar);
            if (r0Var == null) {
                r0Var = new C2245r0(this, aVar);
                r0Var.mo17024a(serviceConnection, str);
                r0Var.mo17025a(str);
                this.f3738R.put(aVar, r0Var);
            } else {
                this.f3740T.removeMessages(0, aVar);
                if (!r0Var.mo17026a(serviceConnection)) {
                    r0Var.mo17024a(serviceConnection, str);
                    int c = r0Var.mo17030c();
                    if (c == 1) {
                        serviceConnection.onServiceConnected(r0Var.mo17027b(), r0Var.mo17023a());
                    } else if (c == 2) {
                        r0Var.mo17025a(str);
                    }
                } else {
                    String valueOf = String.valueOf(aVar);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 81);
                    sb.append("Trying to bind a GmsServiceConnection that was already connected before.  config=");
                    sb.append(valueOf);
                    throw new IllegalStateException(sb.toString());
                }
            }
            d = r0Var.mo17031d();
        }
        return d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo16990b(C2224j.C2225a aVar, ServiceConnection serviceConnection, String str) {
        C2258v.m5630a(serviceConnection, "ServiceConnection must not be null");
        synchronized (this.f3738R) {
            C2245r0 r0Var = this.f3738R.get(aVar);
            if (r0Var == null) {
                String valueOf = String.valueOf(aVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 50);
                sb.append("Nonexistent connection status for service config: ");
                sb.append(valueOf);
                throw new IllegalStateException(sb.toString());
            } else if (r0Var.mo17026a(serviceConnection)) {
                r0Var.mo17028b(serviceConnection, str);
                if (r0Var.mo17032e()) {
                    this.f3740T.sendMessageDelayed(this.f3740T.obtainMessage(0, aVar), this.f3742V);
                }
            } else {
                String valueOf2 = String.valueOf(aVar);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 76);
                sb2.append("Trying to unbind a GmsServiceConnection  that was not bound before.  config=");
                sb2.append(valueOf2);
                throw new IllegalStateException(sb2.toString());
            }
        }
    }

    public final boolean handleMessage(Message message) {
        int i = message.what;
        if (i == 0) {
            synchronized (this.f3738R) {
                C2224j.C2225a aVar = (C2224j.C2225a) message.obj;
                C2245r0 r0Var = this.f3738R.get(aVar);
                if (r0Var != null && r0Var.mo17032e()) {
                    if (r0Var.mo17031d()) {
                        r0Var.mo17029b("GmsClientSupervisor");
                    }
                    this.f3738R.remove(aVar);
                }
            }
            return true;
        } else if (i != 1) {
            return false;
        } else {
            synchronized (this.f3738R) {
                C2224j.C2225a aVar2 = (C2224j.C2225a) message.obj;
                C2245r0 r0Var2 = this.f3738R.get(aVar2);
                if (r0Var2 != null && r0Var2.mo17030c() == 3) {
                    String valueOf = String.valueOf(aVar2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Timeout waiting for ServiceConnection callback ");
                    sb.append(valueOf);
                    Log.e("GmsClientSupervisor", sb.toString(), new Exception());
                    ComponentName b = r0Var2.mo17027b();
                    if (b == null) {
                        b = aVar2.mo16991a();
                    }
                    if (b == null) {
                        b = new ComponentName(aVar2.mo16993b(), "unknown");
                    }
                    r0Var2.onServiceDisconnected(b);
                }
            }
            return true;
        }
    }
}
