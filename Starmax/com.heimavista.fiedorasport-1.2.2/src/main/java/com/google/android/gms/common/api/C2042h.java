package com.google.android.gms.common.api;

import com.google.android.gms.common.api.internal.BasePendingResult;
import com.google.android.gms.common.api.internal.C2107n;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.common.api.h */
public final class C2042h {

    /* renamed from: com.google.android.gms.common.api.h$a */
    private static final class C2043a<R extends C2157k> extends BasePendingResult<R> {

        /* renamed from: q */
        private final R f3221q;

        public C2043a(C2036f fVar, R r) {
            super(fVar);
            this.f3221q = r;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final R mo16455a(Status status) {
            return this.f3221q;
        }
    }

    /* renamed from: a */
    public static C2040g<Status> m4721a(Status status, C2036f fVar) {
        C2258v.m5630a(status, "Result must not be null");
        C2107n nVar = new C2107n(fVar);
        nVar.mo16593a((C2157k) status);
        return nVar;
    }

    /* renamed from: a */
    public static <R extends C2157k> C2040g<R> m4722a(C2157k kVar, C2036f fVar) {
        C2258v.m5630a(kVar, "Result must not be null");
        C2258v.m5637a(!kVar.mo16419b().mo16518v(), "Status code must not be SUCCESS");
        C2043a aVar = new C2043a(fVar, kVar);
        aVar.mo16593a(kVar);
        return aVar;
    }
}
