package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.BinderThread;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.C2032d;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2224j;
import com.google.android.gms.common.internal.C2237o;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import p119e.p144d.p145a.p157c.p161c.p163b.C4012d;

/* renamed from: com.google.android.gms.common.internal.d */
public abstract class C2197d<T extends IInterface> {

    /* renamed from: A */
    private static final Feature[] f3644A = new Feature[0];

    /* renamed from: a */
    private int f3645a;

    /* renamed from: b */
    private long f3646b;

    /* renamed from: c */
    private long f3647c;

    /* renamed from: d */
    private int f3648d;

    /* renamed from: e */
    private long f3649e;

    /* renamed from: f */
    private C2253t0 f3650f;

    /* renamed from: g */
    private final Context f3651g;

    /* renamed from: h */
    private final C2224j f3652h;

    /* renamed from: i */
    private final C2169c f3653i;

    /* renamed from: j */
    final Handler f3654j;

    /* renamed from: k */
    private final Object f3655k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public final Object f3656l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public C2242q f3657m;

    /* renamed from: n */
    protected C2200c f3658n;

    /* renamed from: o */
    private T f3659o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public final ArrayList<C2205h<?>> f3660p;

    /* renamed from: q */
    private C2207j f3661q;

    /* renamed from: r */
    private int f3662r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public final C2198a f3663s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public final C2199b f3664t;

    /* renamed from: u */
    private final int f3665u;

    /* renamed from: v */
    private final String f3666v;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public ConnectionResult f3667w;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public boolean f3668x;

    /* renamed from: y */
    private volatile zzb f3669y;

    /* renamed from: z */
    protected AtomicInteger f3670z;

    /* renamed from: com.google.android.gms.common.internal.d$a */
    public interface C2198a {
        /* renamed from: L */
        void mo16940L(int i);

        /* renamed from: f */
        void mo16941f(@Nullable Bundle bundle);
    }

    /* renamed from: com.google.android.gms.common.internal.d$b */
    public interface C2199b {
        /* renamed from: a */
        void mo16942a(@NonNull ConnectionResult connectionResult);
    }

    /* renamed from: com.google.android.gms.common.internal.d$c */
    public interface C2200c {
        /* renamed from: a */
        void mo16632a(@NonNull ConnectionResult connectionResult);
    }

    /* renamed from: com.google.android.gms.common.internal.d$d */
    protected class C2201d implements C2200c {
        public C2201d() {
        }

        /* renamed from: a */
        public void mo16632a(@NonNull ConnectionResult connectionResult) {
            if (connectionResult.mo16482w()) {
                C2197d dVar = C2197d.this;
                dVar.mo16919a((C2231m) null, dVar.mo16938w());
            } else if (C2197d.this.f3664t != null) {
                C2197d.this.f3664t.mo16942a(connectionResult);
            }
        }
    }

    /* renamed from: com.google.android.gms.common.internal.d$e */
    public interface C2202e {
        /* renamed from: a */
        void mo16644a();
    }

    /* renamed from: com.google.android.gms.common.internal.d$f */
    private abstract class C2203f extends C2205h<Boolean> {

        /* renamed from: d */
        private final int f3672d;

        /* renamed from: e */
        private final Bundle f3673e;

        @BinderThread
        protected C2203f(int i, Bundle bundle) {
            super(true);
            this.f3672d = i;
            this.f3673e = bundle;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract void mo16943a(ConnectionResult connectionResult);

        /* JADX WARN: Type inference failed for: r5v11, types: [android.os.Parcelable] */
        /* access modifiers changed from: protected */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final /* synthetic */ void mo16944a(java.lang.Object r5) {
            /*
                r4 = this;
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                r0 = 1
                r1 = 0
                if (r5 != 0) goto L_0x000c
                com.google.android.gms.common.internal.d r5 = com.google.android.gms.common.internal.C2197d.this
                r5.m5370b(r0, null)
                return
            L_0x000c:
                int r5 = r4.f3672d
                if (r5 == 0) goto L_0x0061
                r2 = 10
                if (r5 == r2) goto L_0x0031
                com.google.android.gms.common.internal.d r5 = com.google.android.gms.common.internal.C2197d.this
                r5.m5370b(r0, null)
                android.os.Bundle r5 = r4.f3673e
                if (r5 == 0) goto L_0x0026
                java.lang.String r0 = "pendingIntent"
                android.os.Parcelable r5 = r5.getParcelable(r0)
                r1 = r5
                android.app.PendingIntent r1 = (android.app.PendingIntent) r1
            L_0x0026:
                com.google.android.gms.common.ConnectionResult r5 = new com.google.android.gms.common.ConnectionResult
                int r0 = r4.f3672d
                r5.<init>(r0, r1)
                r4.mo16943a(r5)
                goto L_0x0076
            L_0x0031:
                com.google.android.gms.common.internal.d r5 = com.google.android.gms.common.internal.C2197d.this
                r5.m5370b(r0, null)
                java.lang.IllegalStateException r5 = new java.lang.IllegalStateException
                r1 = 3
                java.lang.Object[] r1 = new java.lang.Object[r1]
                r2 = 0
                java.lang.Class r3 = r4.getClass()
                java.lang.String r3 = r3.getSimpleName()
                r1[r2] = r3
                com.google.android.gms.common.internal.d r2 = com.google.android.gms.common.internal.C2197d.this
                java.lang.String r2 = r2.mo16454z()
                r1[r0] = r2
                r0 = 2
                com.google.android.gms.common.internal.d r2 = com.google.android.gms.common.internal.C2197d.this
                java.lang.String r2 = r2.mo16453y()
                r1[r0] = r2
                java.lang.String r0 = "A fatal developer error has occurred. Class name: %s. Start service action: %s. Service Descriptor: %s. "
                java.lang.String r0 = java.lang.String.format(r0, r1)
                r5.<init>(r0)
                throw r5
            L_0x0061:
                boolean r5 = r4.mo16946e()
                if (r5 != 0) goto L_0x0076
                com.google.android.gms.common.internal.d r5 = com.google.android.gms.common.internal.C2197d.this
                r5.m5370b(r0, null)
                com.google.android.gms.common.ConnectionResult r5 = new com.google.android.gms.common.ConnectionResult
                r0 = 8
                r5.<init>(r0, r1)
                r4.mo16943a(r5)
            L_0x0076:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.C2197d.C2203f.mo16944a(java.lang.Object):void");
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public final void mo16945c() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public abstract boolean mo16946e();
    }

    /* renamed from: com.google.android.gms.common.internal.d$g */
    final class C2204g extends C4012d {
        public C2204g(Looper looper) {
            super(looper);
        }

        /* renamed from: a */
        private static void m5427a(Message message) {
            C2205h hVar = (C2205h) message.obj;
            hVar.mo16945c();
            hVar.mo16949b();
        }

        /* renamed from: b */
        private static boolean m5428b(Message message) {
            int i = message.what;
            return i == 2 || i == 1 || i == 7;
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v17, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: android.app.PendingIntent} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void handleMessage(android.os.Message r8) {
            /*
                r7 = this;
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                java.util.concurrent.atomic.AtomicInteger r0 = r0.f3670z
                int r0 = r0.get()
                int r1 = r8.arg1
                if (r0 == r1) goto L_0x0016
                boolean r0 = m5428b(r8)
                if (r0 == 0) goto L_0x0015
                m5427a(r8)
            L_0x0015:
                return
            L_0x0016:
                int r0 = r8.what
                r1 = 4
                r2 = 1
                r3 = 5
                if (r0 == r2) goto L_0x002e
                r4 = 7
                if (r0 == r4) goto L_0x002e
                if (r0 != r1) goto L_0x002a
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                boolean r0 = r0.mo16932q()
                if (r0 == 0) goto L_0x002e
            L_0x002a:
                int r0 = r8.what
                if (r0 != r3) goto L_0x003a
            L_0x002e:
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                boolean r0 = r0.mo16923e()
                if (r0 != 0) goto L_0x003a
                m5427a(r8)
                return
            L_0x003a:
                int r0 = r8.what
                r4 = 8
                r5 = 3
                r6 = 0
                if (r0 != r1) goto L_0x0085
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.ConnectionResult r1 = new com.google.android.gms.common.ConnectionResult
                int r8 = r8.arg2
                r1.<init>(r8)
                com.google.android.gms.common.ConnectionResult unused = r0.f3667w = r1
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                boolean r8 = r8.m5360E()
                if (r8 == 0) goto L_0x0064
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                boolean r8 = r8.f3668x
                if (r8 != 0) goto L_0x0064
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                r8.m5370b(r5, null)
                return
            L_0x0064:
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.ConnectionResult r8 = r8.f3667w
                if (r8 == 0) goto L_0x0073
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.ConnectionResult r8 = r8.f3667w
                goto L_0x0078
            L_0x0073:
                com.google.android.gms.common.ConnectionResult r8 = new com.google.android.gms.common.ConnectionResult
                r8.<init>(r4)
            L_0x0078:
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.internal.d$c r0 = r0.f3658n
                r0.mo16632a(r8)
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                r0.mo16915a(r8)
                return
            L_0x0085:
                if (r0 != r3) goto L_0x00a8
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.ConnectionResult r8 = r8.f3667w
                if (r8 == 0) goto L_0x0096
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.ConnectionResult r8 = r8.f3667w
                goto L_0x009b
            L_0x0096:
                com.google.android.gms.common.ConnectionResult r8 = new com.google.android.gms.common.ConnectionResult
                r8.<init>(r4)
            L_0x009b:
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.internal.d$c r0 = r0.f3658n
                r0.mo16632a(r8)
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                r0.mo16915a(r8)
                return
            L_0x00a8:
                if (r0 != r5) goto L_0x00c7
                java.lang.Object r0 = r8.obj
                boolean r1 = r0 instanceof android.app.PendingIntent
                if (r1 == 0) goto L_0x00b3
                r6 = r0
                android.app.PendingIntent r6 = (android.app.PendingIntent) r6
            L_0x00b3:
                com.google.android.gms.common.ConnectionResult r0 = new com.google.android.gms.common.ConnectionResult
                int r8 = r8.arg2
                r0.<init>(r8, r6)
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.internal.d$c r8 = r8.f3658n
                r8.mo16632a(r0)
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                r8.mo16915a(r0)
                return
            L_0x00c7:
                r1 = 6
                if (r0 != r1) goto L_0x00ef
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                r0.m5370b(r3, null)
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.internal.d$a r0 = r0.f3663s
                if (r0 == 0) goto L_0x00e2
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                com.google.android.gms.common.internal.d$a r0 = r0.f3663s
                int r1 = r8.arg2
                r0.mo16940L(r1)
            L_0x00e2:
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                int r8 = r8.arg2
                r0.mo16910a(r8)
                com.google.android.gms.common.internal.d r8 = com.google.android.gms.common.internal.C2197d.this
                boolean unused = r8.m5368a(r3, r2, r6)
                return
            L_0x00ef:
                r1 = 2
                if (r0 != r1) goto L_0x00fe
                com.google.android.gms.common.internal.d r0 = com.google.android.gms.common.internal.C2197d.this
                boolean r0 = r0.mo16922c()
                if (r0 != 0) goto L_0x00fe
                m5427a(r8)
                return
            L_0x00fe:
                boolean r0 = m5428b(r8)
                if (r0 == 0) goto L_0x010c
                java.lang.Object r8 = r8.obj
                com.google.android.gms.common.internal.d$h r8 = (com.google.android.gms.common.internal.C2197d.C2205h) r8
                r8.mo16950d()
                return
            L_0x010c:
                int r8 = r8.what
                r0 = 45
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>(r0)
                java.lang.String r0 = "Don't know how to handle message: "
                r1.append(r0)
                r1.append(r8)
                java.lang.String r8 = r1.toString()
                java.lang.Exception r0 = new java.lang.Exception
                r0.<init>()
                java.lang.String r1 = "GmsClient"
                android.util.Log.wtf(r1, r8, r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.C2197d.C2204g.handleMessage(android.os.Message):void");
        }
    }

    /* renamed from: com.google.android.gms.common.internal.d$h */
    protected abstract class C2205h<TListener> {

        /* renamed from: a */
        private TListener f3676a;

        /* renamed from: b */
        private boolean f3677b = false;

        public C2205h(TListener tlistener) {
            this.f3676a = tlistener;
        }

        /* renamed from: a */
        public final void mo16948a() {
            synchronized (this) {
                this.f3676a = null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract void mo16944a(TListener tlistener);

        /* renamed from: b */
        public final void mo16949b() {
            mo16948a();
            synchronized (C2197d.this.f3660p) {
                C2197d.this.f3660p.remove(this);
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public abstract void mo16945c();

        /* renamed from: d */
        public final void mo16950d() {
            TListener tlistener;
            synchronized (this) {
                tlistener = this.f3676a;
                if (this.f3677b) {
                    String valueOf = String.valueOf(this);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 47);
                    sb.append("Callback proxy ");
                    sb.append(valueOf);
                    sb.append(" being reused. This is not safe.");
                    Log.w("GmsClient", sb.toString());
                }
            }
            if (tlistener != null) {
                try {
                    mo16944a(tlistener);
                } catch (RuntimeException e) {
                    mo16945c();
                    throw e;
                }
            } else {
                mo16945c();
            }
            synchronized (this) {
                this.f3677b = true;
            }
            mo16949b();
        }
    }

    /* renamed from: com.google.android.gms.common.internal.d$j */
    public final class C2207j implements ServiceConnection {

        /* renamed from: P */
        private final int f3681P;

        public C2207j(int i) {
            this.f3681P = i;
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            C2242q qVar;
            if (iBinder == null) {
                C2197d.this.m5372c(16);
                return;
            }
            synchronized (C2197d.this.f3656l) {
                C2197d dVar = C2197d.this;
                if (iBinder == null) {
                    qVar = null;
                } else {
                    IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGmsServiceBroker");
                    if (queryLocalInterface == null || !(queryLocalInterface instanceof C2242q)) {
                        qVar = new C2240p(iBinder);
                    } else {
                        qVar = (C2242q) queryLocalInterface;
                    }
                }
                C2242q unused = dVar.f3657m = qVar;
            }
            C2197d.this.mo16911a(0, (Bundle) null, this.f3681P);
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            synchronized (C2197d.this.f3656l) {
                C2242q unused = C2197d.this.f3657m = (C2242q) null;
            }
            Handler handler = C2197d.this.f3654j;
            handler.sendMessage(handler.obtainMessage(6, this.f3681P, 1));
        }
    }

    /* renamed from: com.google.android.gms.common.internal.d$k */
    protected final class C2208k extends C2203f {

        /* renamed from: g */
        private final IBinder f3683g;

        @BinderThread
        public C2208k(int i, IBinder iBinder, Bundle bundle) {
            super(i, bundle);
            this.f3683g = iBinder;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo16943a(ConnectionResult connectionResult) {
            if (C2197d.this.f3664t != null) {
                C2197d.this.f3664t.mo16942a(connectionResult);
            }
            C2197d.this.mo16915a(connectionResult);
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public final boolean mo16946e() {
            try {
                String interfaceDescriptor = this.f3683g.getInterfaceDescriptor();
                if (!C2197d.this.mo16453y().equals(interfaceDescriptor)) {
                    String y = C2197d.this.mo16453y();
                    StringBuilder sb = new StringBuilder(String.valueOf(y).length() + 34 + String.valueOf(interfaceDescriptor).length());
                    sb.append("service descriptor mismatch: ");
                    sb.append(y);
                    sb.append(" vs. ");
                    sb.append(interfaceDescriptor);
                    Log.e("GmsClient", sb.toString());
                    return false;
                }
                IInterface a = C2197d.this.mo16449a(this.f3683g);
                if (a == null || (!C2197d.this.m5368a(2, 4, a) && !C2197d.this.m5368a(3, 4, a))) {
                    return false;
                }
                ConnectionResult unused = C2197d.this.f3667w = (ConnectionResult) null;
                Bundle n = C2197d.this.mo16929n();
                if (C2197d.this.f3663s == null) {
                    return true;
                }
                C2197d.this.f3663s.mo16941f(n);
                return true;
            } catch (RemoteException unused2) {
                Log.w("GmsClient", "service probably died");
                return false;
            }
        }
    }

    /* renamed from: com.google.android.gms.common.internal.d$l */
    protected final class C2209l extends C2203f {
        @BinderThread
        public C2209l(int i, @Nullable Bundle bundle) {
            super(i, null);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo16943a(ConnectionResult connectionResult) {
            if (!C2197d.this.mo16932q() || !C2197d.this.m5360E()) {
                C2197d.this.f3658n.mo16632a(connectionResult);
                C2197d.this.mo16915a(connectionResult);
                return;
            }
            C2197d.this.m5372c(16);
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public final boolean mo16946e() {
            C2197d.this.f3658n.mo16632a(ConnectionResult.f3156T);
            return true;
        }
    }

    static {
        new String[]{"service_esmobile", "service_googleme"};
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected C2197d(android.content.Context r10, android.os.Looper r11, int r12, com.google.android.gms.common.internal.C2197d.C2198a r13, com.google.android.gms.common.internal.C2197d.C2199b r14, java.lang.String r15) {
        /*
            r9 = this;
            com.google.android.gms.common.internal.j r3 = com.google.android.gms.common.internal.C2224j.m5496a(r10)
            com.google.android.gms.common.c r4 = com.google.android.gms.common.C2169c.m5270a()
            com.google.android.gms.common.internal.C2258v.m5629a(r13)
            r6 = r13
            com.google.android.gms.common.internal.d$a r6 = (com.google.android.gms.common.internal.C2197d.C2198a) r6
            com.google.android.gms.common.internal.C2258v.m5629a(r14)
            r7 = r14
            com.google.android.gms.common.internal.d$b r7 = (com.google.android.gms.common.internal.C2197d.C2199b) r7
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r8 = r15
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.C2197d.<init>(android.content.Context, android.os.Looper, int, com.google.android.gms.common.internal.d$a, com.google.android.gms.common.internal.d$b, java.lang.String):void");
    }

    @Nullable
    /* renamed from: C */
    private final String mo16976C() {
        String str = this.f3666v;
        return str == null ? this.f3651g.getClass().getName() : str;
    }

    /* renamed from: D */
    private final boolean mo16448D() {
        boolean z;
        synchronized (this.f3655k) {
            z = this.f3662r == 3;
        }
        return z;
    }

    /* access modifiers changed from: private */
    /* renamed from: E */
    public final boolean m5360E() {
        if (this.f3668x || TextUtils.isEmpty(mo16453y()) || TextUtils.isEmpty(mo16937v())) {
            return false;
        }
        try {
            Class.forName(mo16453y());
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5367a(zzb zzb) {
        this.f3669y = zzb;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m5370b(int i, T t) {
        C2253t0 t0Var;
        C2258v.m5636a((i == 4) == (t != null));
        synchronized (this.f3655k) {
            this.f3662r = i;
            this.f3659o = t;
            mo16913a(i, t);
            if (i != 1) {
                if (i == 2 || i == 3) {
                    if (!(this.f3661q == null || this.f3650f == null)) {
                        String c = this.f3650f.mo17041c();
                        String a = this.f3650f.mo17039a();
                        StringBuilder sb = new StringBuilder(String.valueOf(c).length() + 70 + String.valueOf(a).length());
                        sb.append("Calling connect() while still connected, missing disconnect() for ");
                        sb.append(c);
                        sb.append(" on ");
                        sb.append(a);
                        Log.e("GmsClient", sb.toString());
                        this.f3652h.mo16988a(this.f3650f.mo17041c(), this.f3650f.mo17039a(), this.f3650f.mo17040b(), this.f3661q, mo16976C());
                        this.f3670z.incrementAndGet();
                    }
                    this.f3661q = new C2207j(this.f3670z.get());
                    if (this.f3662r != 3 || mo16937v() == null) {
                        t0Var = new C2253t0(mo16907A(), mo16454z(), false, TsExtractor.TS_STREAM_TYPE_AC3);
                    } else {
                        t0Var = new C2253t0(mo16935t().getPackageName(), mo16937v(), true, TsExtractor.TS_STREAM_TYPE_AC3);
                    }
                    this.f3650f = t0Var;
                    if (!this.f3652h.mo16989a(new C2224j.C2225a(this.f3650f.mo17041c(), this.f3650f.mo17039a(), this.f3650f.mo17040b()), this.f3661q, mo16976C())) {
                        String c2 = this.f3650f.mo17041c();
                        String a2 = this.f3650f.mo17039a();
                        StringBuilder sb2 = new StringBuilder(String.valueOf(c2).length() + 34 + String.valueOf(a2).length());
                        sb2.append("unable to connect to service: ");
                        sb2.append(c2);
                        sb2.append(" on ");
                        sb2.append(a2);
                        Log.e("GmsClient", sb2.toString());
                        mo16911a(16, (Bundle) null, this.f3670z.get());
                    }
                } else if (i == 4) {
                    mo16914a((IInterface) t);
                }
            } else if (this.f3661q != null) {
                this.f3652h.mo16988a(this.f3650f.mo17041c(), this.f3650f.mo17039a(), this.f3650f.mo17040b(), this.f3661q, mo16976C());
                this.f3661q = null;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public String mo16907A() {
        return "com.google.android.gms";
    }

    /* renamed from: B */
    public boolean mo16908B() {
        return false;
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: a */
    public abstract T mo16449a(IBinder iBinder);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo16913a(int i, T t) {
    }

    /* renamed from: c */
    public boolean mo16922c() {
        boolean z;
        synchronized (this.f3655k) {
            z = this.f3662r == 4;
        }
        return z;
    }

    /* renamed from: d */
    public boolean mo16450d() {
        return false;
    }

    /* renamed from: e */
    public boolean mo16923e() {
        boolean z;
        synchronized (this.f3655k) {
            if (this.f3662r != 2) {
                if (this.f3662r != 3) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    /* renamed from: f */
    public String mo16924f() {
        C2253t0 t0Var;
        if (mo16922c() && (t0Var = this.f3650f) != null) {
            return t0Var.mo17039a();
        }
        throw new RuntimeException("Failed to connect when checking package");
    }

    /* renamed from: h */
    public boolean mo16925h() {
        return true;
    }

    /* renamed from: i */
    public int mo16451i() {
        return C2169c.f3548a;
    }

    @Nullable
    /* renamed from: j */
    public final Feature[] mo16926j() {
        zzb zzb = this.f3669y;
        if (zzb == null) {
            return null;
        }
        return zzb.f3770Q;
    }

    /* renamed from: k */
    public Intent mo16452k() {
        throw new UnsupportedOperationException("Not a sign in API");
    }

    /* renamed from: l */
    public boolean mo16927l() {
        return false;
    }

    @Nullable
    /* renamed from: m */
    public IBinder mo16928m() {
        synchronized (this.f3656l) {
            if (this.f3657m == null) {
                return null;
            }
            IBinder asBinder = this.f3657m.asBinder();
            return asBinder;
        }
    }

    /* renamed from: n */
    public Bundle mo16929n() {
        return null;
    }

    /* renamed from: o */
    public void mo16930o() {
        int a = this.f3653i.mo16820a(this.f3651g, mo16451i());
        if (a != 0) {
            m5370b(1, null);
            mo16917a(new C2201d(), a, (PendingIntent) null);
            return;
        }
        mo16916a(new C2201d());
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public final void mo16931p() {
        if (!mo16922c()) {
            throw new IllegalStateException("Not connected. Call connect() and wait for onConnected() to be called.");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public boolean mo16932q() {
        return false;
    }

    /* renamed from: r */
    public Account mo16933r() {
        return null;
    }

    /* renamed from: s */
    public Feature[] mo16934s() {
        return f3644A;
    }

    /* renamed from: t */
    public final Context mo16935t() {
        return this.f3651g;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public Bundle mo16936u() {
        return new Bundle();
    }

    /* access modifiers changed from: protected */
    @Nullable
    /* renamed from: v */
    public String mo16937v() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: w */
    public Set<Scope> mo16938w() {
        return Collections.EMPTY_SET;
    }

    /* renamed from: x */
    public final T mo16939x() {
        T t;
        synchronized (this.f3655k) {
            if (this.f3662r != 5) {
                mo16931p();
                C2258v.m5641b(this.f3659o != null, "Client is connected but service is null");
                t = this.f3659o;
            } else {
                throw new DeadObjectException();
            }
        }
        return t;
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: y */
    public abstract String mo16453y();

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: z */
    public abstract String mo16454z();

    /* access modifiers changed from: protected */
    @CallSuper
    /* renamed from: a */
    public void mo16914a(@NonNull T t) {
        this.f3647c = System.currentTimeMillis();
    }

    /* renamed from: com.google.android.gms.common.internal.d$i */
    public static final class C2206i extends C2237o.C2238a {

        /* renamed from: a */
        private C2197d f3679a;

        /* renamed from: b */
        private final int f3680b;

        public C2206i(@NonNull C2197d dVar, int i) {
            this.f3679a = dVar;
            this.f3680b = i;
        }

        @BinderThread
        /* renamed from: a */
        public final void mo16951a(int i, @NonNull IBinder iBinder, @Nullable Bundle bundle) {
            C2258v.m5630a(this.f3679a, "onPostInitComplete can be called only once per call to getRemoteService");
            this.f3679a.mo16912a(i, iBinder, bundle, this.f3680b);
            this.f3679a = null;
        }

        @BinderThread
        /* renamed from: b */
        public final void mo16953b(int i, @Nullable Bundle bundle) {
            Log.wtf("GmsClient", "received deprecated onAccountValidationComplete callback, ignoring", new Exception());
        }

        @BinderThread
        /* renamed from: a */
        public final void mo16952a(int i, @NonNull IBinder iBinder, @NonNull zzb zzb) {
            C2258v.m5630a(this.f3679a, "onPostInitCompleteWithConnectionInfo can be called only once per call togetRemoteService");
            C2258v.m5629a(zzb);
            this.f3679a.m5367a(zzb);
            mo16951a(i, iBinder, zzb.f3769P);
        }
    }

    /* access modifiers changed from: protected */
    @CallSuper
    /* renamed from: a */
    public void mo16910a(int i) {
        this.f3645a = i;
        this.f3646b = System.currentTimeMillis();
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m5372c(int i) {
        int i2;
        if (mo16448D()) {
            i2 = 5;
            this.f3668x = true;
        } else {
            i2 = 4;
        }
        Handler handler = this.f3654j;
        handler.sendMessage(handler.obtainMessage(i2, this.f3670z.get(), 16));
    }

    /* access modifiers changed from: protected */
    @CallSuper
    /* renamed from: a */
    public void mo16915a(ConnectionResult connectionResult) {
        this.f3648d = connectionResult.mo16475c();
        this.f3649e = System.currentTimeMillis();
    }

    protected C2197d(Context context, Looper looper, C2224j jVar, C2169c cVar, int i, C2198a aVar, C2199b bVar, String str) {
        this.f3655k = new Object();
        this.f3656l = new Object();
        this.f3660p = new ArrayList<>();
        this.f3662r = 1;
        this.f3667w = null;
        this.f3668x = false;
        this.f3669y = null;
        this.f3670z = new AtomicInteger(0);
        C2258v.m5630a(context, "Context must not be null");
        this.f3651g = context;
        C2258v.m5630a(looper, "Looper must not be null");
        C2258v.m5630a(jVar, "Supervisor must not be null");
        this.f3652h = jVar;
        C2258v.m5630a(cVar, "API availability must not be null");
        this.f3653i = cVar;
        this.f3654j = new C2204g(looper);
        this.f3665u = i;
        this.f3663s = aVar;
        this.f3664t = bVar;
        this.f3666v = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final boolean m5368a(int i, int i2, T t) {
        synchronized (this.f3655k) {
            if (this.f3662r != i) {
                return false;
            }
            m5370b(i2, t);
            return true;
        }
    }

    /* renamed from: a */
    public void mo16916a(@NonNull C2200c cVar) {
        C2258v.m5630a(cVar, "Connection progress callbacks cannot be null.");
        this.f3658n = cVar;
        m5370b(2, null);
    }

    /* renamed from: a */
    public void mo16909a() {
        this.f3670z.incrementAndGet();
        synchronized (this.f3660p) {
            int size = this.f3660p.size();
            for (int i = 0; i < size; i++) {
                this.f3660p.get(i).mo16948a();
            }
            this.f3660p.clear();
        }
        synchronized (this.f3656l) {
            this.f3657m = null;
        }
        m5370b(1, null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo16917a(@NonNull C2200c cVar, int i, @Nullable PendingIntent pendingIntent) {
        C2258v.m5630a(cVar, "Connection progress callbacks cannot be null.");
        this.f3658n = cVar;
        Handler handler = this.f3654j;
        handler.sendMessage(handler.obtainMessage(3, this.f3670z.get(), i, pendingIntent));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo16912a(int i, IBinder iBinder, Bundle bundle, int i2) {
        Handler handler = this.f3654j;
        handler.sendMessage(handler.obtainMessage(1, i2, -1, new C2208k(i, iBinder, bundle)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo16911a(int i, @Nullable Bundle bundle, int i2) {
        Handler handler = this.f3654j;
        handler.sendMessage(handler.obtainMessage(7, i2, -1, new C2209l(i, null)));
    }

    @WorkerThread
    /* renamed from: a */
    public void mo16919a(C2231m mVar, Set<Scope> set) {
        Bundle u = mo16936u();
        GetServiceRequest getServiceRequest = new GetServiceRequest(this.f3665u);
        getServiceRequest.f3616S = this.f3651g.getPackageName();
        getServiceRequest.f3619V = u;
        if (set != null) {
            getServiceRequest.f3618U = (Scope[]) set.toArray(new Scope[set.size()]);
        }
        if (mo16927l()) {
            getServiceRequest.f3620W = mo16933r() != null ? mo16933r() : new Account("<<default account>>", "com.google");
            if (mVar != null) {
                getServiceRequest.f3617T = mVar.asBinder();
            }
        } else if (mo16908B()) {
            getServiceRequest.f3620W = mo16933r();
        }
        getServiceRequest.f3621X = f3644A;
        getServiceRequest.f3622Y = mo16934s();
        try {
            synchronized (this.f3656l) {
                if (this.f3657m != null) {
                    this.f3657m.mo17018a(new C2206i(this, this.f3670z.get()), getServiceRequest);
                } else {
                    Log.w("GmsClient", "mServiceBroker is null, client disconnected");
                }
            }
        } catch (DeadObjectException e) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e);
            mo16921b(1);
        } catch (SecurityException e2) {
            throw e2;
        } catch (RemoteException | RuntimeException e3) {
            Log.w("GmsClient", "IGmsServiceBroker.getService failed", e3);
            mo16912a(8, (IBinder) null, (Bundle) null, this.f3670z.get());
        }
    }

    /* renamed from: b */
    public void mo16921b(int i) {
        Handler handler = this.f3654j;
        handler.sendMessage(handler.obtainMessage(6, this.f3670z.get(), i));
    }

    /* renamed from: a */
    public void mo16918a(@NonNull C2202e eVar) {
        eVar.mo16644a();
    }

    /* renamed from: a */
    public void mo16920a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        int i;
        T t;
        C2242q qVar;
        synchronized (this.f3655k) {
            i = this.f3662r;
            t = this.f3659o;
        }
        synchronized (this.f3656l) {
            qVar = this.f3657m;
        }
        printWriter.append((CharSequence) str).append((CharSequence) "mConnectState=");
        if (i == 1) {
            printWriter.print("DISCONNECTED");
        } else if (i == 2) {
            printWriter.print("REMOTE_CONNECTING");
        } else if (i == 3) {
            printWriter.print("LOCAL_CONNECTING");
        } else if (i == 4) {
            printWriter.print("CONNECTED");
        } else if (i != 5) {
            printWriter.print("UNKNOWN");
        } else {
            printWriter.print("DISCONNECTING");
        }
        printWriter.append((CharSequence) " mService=");
        if (t == null) {
            printWriter.append((CharSequence) "null");
        } else {
            printWriter.append((CharSequence) mo16453y()).append((CharSequence) "@").append((CharSequence) Integer.toHexString(System.identityHashCode(t.asBinder())));
        }
        printWriter.append((CharSequence) " mServiceBroker=");
        if (qVar == null) {
            printWriter.println("null");
        } else {
            printWriter.append((CharSequence) "IGmsServiceBroker@").println(Integer.toHexString(System.identityHashCode(qVar.asBinder())));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
        if (this.f3647c > 0) {
            PrintWriter append = printWriter.append((CharSequence) str).append((CharSequence) "lastConnectedTime=");
            long j = this.f3647c;
            String format = simpleDateFormat.format(new Date(j));
            StringBuilder sb = new StringBuilder(String.valueOf(format).length() + 21);
            sb.append(j);
            sb.append(" ");
            sb.append(format);
            append.println(sb.toString());
        }
        if (this.f3646b > 0) {
            printWriter.append((CharSequence) str).append((CharSequence) "lastSuspendedCause=");
            int i2 = this.f3645a;
            if (i2 == 1) {
                printWriter.append((CharSequence) "CAUSE_SERVICE_DISCONNECTED");
            } else if (i2 != 2) {
                printWriter.append((CharSequence) String.valueOf(i2));
            } else {
                printWriter.append((CharSequence) "CAUSE_NETWORK_LOST");
            }
            PrintWriter append2 = printWriter.append((CharSequence) " lastSuspendedTime=");
            long j2 = this.f3646b;
            String format2 = simpleDateFormat.format(new Date(j2));
            StringBuilder sb2 = new StringBuilder(String.valueOf(format2).length() + 21);
            sb2.append(j2);
            sb2.append(" ");
            sb2.append(format2);
            append2.println(sb2.toString());
        }
        if (this.f3649e > 0) {
            printWriter.append((CharSequence) str).append((CharSequence) "lastFailedStatus=").append((CharSequence) C2032d.m4674a(this.f3648d));
            PrintWriter append3 = printWriter.append((CharSequence) " lastFailedTime=");
            long j3 = this.f3649e;
            String format3 = simpleDateFormat.format(new Date(j3));
            StringBuilder sb3 = new StringBuilder(String.valueOf(format3).length() + 21);
            sb3.append(j3);
            sb3.append(" ");
            sb3.append(format3);
            append3.println(sb3.toString());
        }
    }
}
