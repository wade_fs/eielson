package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;

/* renamed from: com.google.android.gms.common.api.internal.m2 */
public interface C2106m2 extends C2036f.C2038b {
    /* renamed from: a */
    void mo16665a(ConnectionResult connectionResult, C2016a<?> aVar, boolean z);
}
