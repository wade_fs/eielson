package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ka */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2587ka implements C2593l2<C2573ja> {

    /* renamed from: Q */
    private static C2587ka f4280Q = new C2587ka();

    /* renamed from: P */
    private final C2593l2<C2573ja> f4281P;

    private C2587ka(C2593l2<C2573ja> l2Var) {
        this.f4281P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6629b() {
        return ((C2573ja) f4280Q.mo17285a()).mo17608a();
    }

    /* renamed from: c */
    public static boolean m6630c() {
        return ((C2573ja) f4280Q.mo17285a()).mo17609e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4281P.mo17285a();
    }

    public C2587ka() {
        this(C2579k2.m6601a(new C2622ma()));
    }
}
