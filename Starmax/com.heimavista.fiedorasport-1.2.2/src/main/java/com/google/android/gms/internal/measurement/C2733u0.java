package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.u0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2733u0 extends C2595l4<C2733u0, C2734a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2733u0 zzj;
    private static volatile C2501f6<C2733u0> zzk;
    private int zzc;
    private String zzd = "";
    private String zze = "";
    private long zzf;
    private float zzg;
    private double zzh;
    private C2738u4<C2733u0> zzi = C2595l4.m6649m();

    /* renamed from: com.google.android.gms.internal.measurement.u0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2734a extends C2595l4.C2596a<C2733u0, C2734a> implements C2769w5 {
        private C2734a() {
            super(C2733u0.zzj);
        }

        /* renamed from: a */
        public final C2734a mo17933a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7342a(str);
            return this;
        }

        /* renamed from: b */
        public final C2734a mo17934b(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7345b(str);
            return this;
        }

        /* renamed from: j */
        public final C2734a mo17935j() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7333A();
            return this;
        }

        /* renamed from: k */
        public final C2734a mo17936k() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7334B();
            return this;
        }

        /* renamed from: l */
        public final C2734a mo17937l() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7335C();
            return this;
        }

        /* synthetic */ C2734a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2734a mo17932a(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7337a(j);
            return this;
        }

        /* renamed from: a */
        public final C2734a mo17931a(double d) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2733u0) super.f4288Q).m7336a(d);
            return this;
        }
    }

    static {
        C2733u0 u0Var = new C2733u0();
        zzj = u0Var;
        C2595l4.m6645a(C2733u0.class, super);
    }

    private C2733u0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: A */
    public final void m7333A() {
        this.zzc &= -3;
        this.zze = zzj.zze;
    }

    /* access modifiers changed from: private */
    /* renamed from: B */
    public final void m7334B() {
        this.zzc &= -5;
        this.zzf = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: C */
    public final void m7335C() {
        this.zzc &= -17;
        this.zzh = 0.0d;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7342a(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7345b(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    /* renamed from: y */
    public static C2734a m7347y() {
        return (C2734a) zzj.mo17668i();
    }

    /* renamed from: n */
    public final boolean mo17921n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final String mo17922o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final boolean mo17923p() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: q */
    public final String mo17924q() {
        return this.zze;
    }

    /* renamed from: s */
    public final boolean mo17925s() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: t */
    public final long mo17926t() {
        return this.zzf;
    }

    /* renamed from: u */
    public final boolean mo17927u() {
        return (this.zzc & 16) != 0;
    }

    /* renamed from: v */
    public final double mo17928v() {
        return this.zzh;
    }

    /* renamed from: w */
    public final List<C2733u0> mo17929w() {
        return this.zzi;
    }

    /* renamed from: x */
    public final int mo17930x() {
        return this.zzi.size();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7337a(long j) {
        this.zzc |= 4;
        this.zzf = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7336a(double d) {
        this.zzc |= 16;
        this.zzh = d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        Class<C2733u0> cls = C2733u0.class;
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2733u0();
            case 2:
                return new C2734a(null);
            case 3:
                return C2595l4.m6643a(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0001\u0000\u0001\b\u0000\u0002\b\u0001\u0003\u0002\u0002\u0004\u0001\u0003\u0005\u0000\u0004\u0006\u001b", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi", cls});
            case 4:
                return zzj;
            case 5:
                C2501f6<C2733u0> f6Var = zzk;
                if (f6Var == null) {
                    synchronized (cls) {
                        f6Var = zzk;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzj);
                            zzk = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
