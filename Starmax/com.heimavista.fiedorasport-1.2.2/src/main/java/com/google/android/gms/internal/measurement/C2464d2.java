package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.d2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2464d2<T> extends C2497f2<T> {

    /* renamed from: P */
    static final C2464d2<Object> f4047P = new C2464d2<>();

    private C2464d2() {
    }

    /* renamed from: a */
    public final boolean mo17397a() {
        return false;
    }

    /* renamed from: e */
    public final T mo17398e() {
        throw new IllegalStateException("Optional.get() cannot be called on an absent value");
    }

    public final boolean equals(Object obj) {
        return obj == this;
    }

    public final int hashCode() {
        return 2040732332;
    }

    public final String toString() {
        return "Optional.absent()";
    }
}
