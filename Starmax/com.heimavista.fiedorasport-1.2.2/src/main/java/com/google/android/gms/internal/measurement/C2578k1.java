package com.google.android.gms.internal.measurement;

import android.database.ContentObserver;
import android.os.Handler;

/* renamed from: com.google.android.gms.internal.measurement.k1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2578k1 extends ContentObserver {

    /* renamed from: a */
    private final /* synthetic */ C2530h1 f4277a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2578k1(C2530h1 h1Var, Handler handler) {
        super(null);
        this.f4277a = h1Var;
    }

    public final void onChange(boolean z) {
        this.f4277a.mo17536b();
    }
}
