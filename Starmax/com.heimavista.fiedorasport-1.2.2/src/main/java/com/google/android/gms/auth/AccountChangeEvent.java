package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class AccountChangeEvent extends AbstractSafeParcelable {
    public static final Parcelable.Creator<AccountChangeEvent> CREATOR = new C1950a();

    /* renamed from: P */
    private final int f2966P;

    /* renamed from: Q */
    private final long f2967Q;

    /* renamed from: R */
    private final String f2968R;

    /* renamed from: S */
    private final int f2969S;

    /* renamed from: T */
    private final int f2970T;

    /* renamed from: U */
    private final String f2971U;

    AccountChangeEvent(int i, long j, String str, int i2, int i3, String str2) {
        this.f2966P = i;
        this.f2967Q = j;
        C2258v.m5629a((Object) str);
        this.f2968R = str;
        this.f2969S = i2;
        this.f2970T = i3;
        this.f2971U = str2;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof AccountChangeEvent) {
            AccountChangeEvent accountChangeEvent = (AccountChangeEvent) obj;
            return this.f2966P == accountChangeEvent.f2966P && this.f2967Q == accountChangeEvent.f2967Q && C2251t.m5617a(this.f2968R, accountChangeEvent.f2968R) && this.f2969S == accountChangeEvent.f2969S && this.f2970T == accountChangeEvent.f2970T && C2251t.m5617a(this.f2971U, accountChangeEvent.f2971U);
        }
    }

    public int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f2966P), Long.valueOf(this.f2967Q), this.f2968R, Integer.valueOf(this.f2969S), Integer.valueOf(this.f2970T), this.f2971U);
    }

    public String toString() {
        int i = this.f2969S;
        String str = i != 1 ? i != 2 ? i != 3 ? i != 4 ? "UNKNOWN" : "RENAMED_TO" : "RENAMED_FROM" : "REMOVED" : "ADDED";
        String str2 = this.f2968R;
        String str3 = this.f2971U;
        int i2 = this.f2970T;
        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 91 + str.length() + String.valueOf(str3).length());
        sb.append("AccountChangeEvent {accountName = ");
        sb.append(str2);
        sb.append(", changeType = ");
        sb.append(str);
        sb.append(", changeData = ");
        sb.append(str3);
        sb.append(", eventIndex = ");
        sb.append(i2);
        sb.append("}");
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f2966P);
        C2250b.m5592a(parcel, 2, this.f2967Q);
        C2250b.m5602a(parcel, 3, this.f2968R, false);
        C2250b.m5591a(parcel, 4, this.f2969S);
        C2250b.m5591a(parcel, 5, this.f2970T);
        C2250b.m5602a(parcel, 6, this.f2971U, false);
        C2250b.m5587a(parcel, a);
    }
}
