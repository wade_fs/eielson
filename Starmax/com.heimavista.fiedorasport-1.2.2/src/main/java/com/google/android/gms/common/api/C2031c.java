package com.google.android.gms.common.api;

import android.text.TextUtils;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.internal.C2063d2;
import com.google.android.gms.common.internal.C2258v;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.common.api.c */
public class C2031c extends Exception {

    /* renamed from: P */
    private final ArrayMap<C2063d2<?>, ConnectionResult> f3189P;

    public C2031c(ArrayMap<C2063d2<?>, ConnectionResult> arrayMap) {
        this.f3189P = arrayMap;
    }

    /* renamed from: a */
    public ConnectionResult mo16545a(C2033e<? extends C2016a.C2020d> eVar) {
        C2063d2<? extends C2016a.C2020d> h = eVar.mo16559h();
        C2258v.m5637a(this.f3189P.get(h) != null, "The given API was not part of the availability request.");
        return this.f3189P.get(h);
    }

    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        boolean z = true;
        for (C2063d2 d2Var : this.f3189P.keySet()) {
            ConnectionResult connectionResult = this.f3189P.get(d2Var);
            if (connectionResult.mo16482w()) {
                z = false;
            }
            String a = d2Var.mo16647a();
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 2 + String.valueOf(valueOf).length());
            sb.append(a);
            sb.append(": ");
            sb.append(valueOf);
            arrayList.add(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder();
        if (z) {
            sb2.append("None of the queried APIs are available. ");
        } else {
            sb2.append("Some of the queried APIs are unavailable. ");
        }
        sb2.append(TextUtils.join("; ", arrayList));
        return sb2.toString();
    }

    /* renamed from: a */
    public final ArrayMap<C2063d2<?>, ConnectionResult> mo16544a() {
        return this.f3189P;
    }
}
