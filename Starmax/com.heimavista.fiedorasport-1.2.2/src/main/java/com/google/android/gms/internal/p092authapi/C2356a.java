package com.google.android.gms.internal.p092authapi;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.auth-api.a */
public class C2356a implements IInterface {

    /* renamed from: a */
    private final IBinder f3896a;

    /* renamed from: b */
    private final String f3897b;

    protected C2356a(IBinder iBinder, String str) {
        this.f3896a = iBinder;
        this.f3897b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public final Parcel mo17155H() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f3897b);
        return obtain;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo17156a(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            this.f3896a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f3896a;
    }
}
