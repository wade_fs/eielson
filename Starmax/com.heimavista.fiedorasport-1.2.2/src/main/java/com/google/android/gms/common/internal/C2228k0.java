package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.common.internal.k0 */
public final class C2228k0 implements Parcelable.Creator<ResolveAccountRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        Account account = null;
        int i = 0;
        GoogleSignInAccount googleSignInAccount = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                account = (Account) C2248a.m5553a(parcel, a, Account.CREATOR);
            } else if (a2 == 3) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 != 4) {
                C2248a.m5550F(parcel, a);
            } else {
                googleSignInAccount = (GoogleSignInAccount) C2248a.m5553a(parcel, a, GoogleSignInAccount.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new ResolveAccountRequest(i, account, i2, googleSignInAccount);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ResolveAccountRequest[i];
    }
}
