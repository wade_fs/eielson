package com.google.android.gms.location;

import android.os.IBinder;
import com.google.android.gms.internal.location.C2383a;

/* renamed from: com.google.android.gms.location.n0 */
public final class C2852n0 extends C2383a implements C2848l0 {
    C2852n0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.ILocationListener");
    }
}
