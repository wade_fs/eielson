package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.internal.view.SupportMenu;
import java.util.List;

/* renamed from: com.google.android.gms.common.internal.safeparcel.b */
public class C2250b {
    /* renamed from: a */
    public static int m5586a(Parcel parcel) {
        return m5610b(parcel, 20293);
    }

    /* renamed from: b */
    private static void m5611b(Parcel parcel, int i, int i2) {
        if (i2 >= 65535) {
            parcel.writeInt(i | SupportMenu.CATEGORY_MASK);
            parcel.writeInt(i2);
            return;
        }
        parcel.writeInt(i | (i2 << 16));
    }

    /* renamed from: c */
    private static void m5613c(Parcel parcel, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.setDataPosition(i - 4);
        parcel.writeInt(dataPosition - i);
        parcel.setDataPosition(dataPosition);
    }

    /* renamed from: a */
    public static void m5587a(Parcel parcel, int i) {
        m5613c(parcel, i);
    }

    /* renamed from: a */
    public static void m5605a(Parcel parcel, int i, boolean z) {
        m5611b(parcel, i, 4);
        parcel.writeInt(z ? 1 : 0);
    }

    /* renamed from: b */
    private static int m5610b(Parcel parcel, int i) {
        parcel.writeInt(i | SupportMenu.CATEGORY_MASK);
        parcel.writeInt(0);
        return parcel.dataPosition();
    }

    /* renamed from: a */
    public static void m5597a(Parcel parcel, int i, Boolean bool, boolean z) {
        if (bool != null) {
            m5611b(parcel, i, 4);
            parcel.writeInt(bool.booleanValue() ? 1 : 0);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: c */
    public static <T extends Parcelable> void m5614c(Parcel parcel, int i, List<T> list, boolean z) {
        if (list != null) {
            int b = m5610b(parcel, i);
            int size = list.size();
            parcel.writeInt(size);
            for (int i2 = 0; i2 < size; i2++) {
                Parcelable parcelable = (Parcelable) list.get(i2);
                if (parcelable == null) {
                    parcel.writeInt(0);
                } else {
                    m5609a(parcel, parcelable, 0);
                }
            }
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: b */
    public static void m5612b(Parcel parcel, int i, List<String> list, boolean z) {
        if (list != null) {
            int b = m5610b(parcel, i);
            parcel.writeStringList(list);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5588a(Parcel parcel, int i, byte b) {
        m5611b(parcel, i, 4);
        parcel.writeInt(b);
    }

    /* renamed from: a */
    public static void m5604a(Parcel parcel, int i, short s) {
        m5611b(parcel, i, 4);
        parcel.writeInt(s);
    }

    /* renamed from: a */
    public static void m5591a(Parcel parcel, int i, int i2) {
        m5611b(parcel, i, 4);
        parcel.writeInt(i2);
    }

    /* renamed from: a */
    public static void m5600a(Parcel parcel, int i, Integer num, boolean z) {
        if (num != null) {
            m5611b(parcel, i, 4);
            parcel.writeInt(num.intValue());
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5592a(Parcel parcel, int i, long j) {
        m5611b(parcel, i, 8);
        parcel.writeLong(j);
    }

    /* renamed from: a */
    public static void m5601a(Parcel parcel, int i, Long l, boolean z) {
        if (l != null) {
            m5611b(parcel, i, 8);
            parcel.writeLong(l.longValue());
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5590a(Parcel parcel, int i, float f) {
        m5611b(parcel, i, 4);
        parcel.writeFloat(f);
    }

    /* renamed from: a */
    public static void m5599a(Parcel parcel, int i, Float f, boolean z) {
        if (f != null) {
            m5611b(parcel, i, 4);
            parcel.writeFloat(f.floatValue());
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5589a(Parcel parcel, int i, double d) {
        m5611b(parcel, i, 8);
        parcel.writeDouble(d);
    }

    /* renamed from: a */
    public static void m5598a(Parcel parcel, int i, Double d, boolean z) {
        if (d != null) {
            m5611b(parcel, i, 8);
            parcel.writeDouble(d.doubleValue());
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5602a(Parcel parcel, int i, String str, boolean z) {
        if (str != null) {
            int b = m5610b(parcel, i);
            parcel.writeString(str);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5594a(Parcel parcel, int i, IBinder iBinder, boolean z) {
        if (iBinder != null) {
            int b = m5610b(parcel, i);
            parcel.writeStrongBinder(iBinder);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5596a(Parcel parcel, int i, Parcelable parcelable, int i2, boolean z) {
        if (parcelable != null) {
            int b = m5610b(parcel, i);
            parcelable.writeToParcel(parcel, i2);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5593a(Parcel parcel, int i, Bundle bundle, boolean z) {
        if (bundle != null) {
            int b = m5610b(parcel, i);
            parcel.writeBundle(bundle);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5606a(Parcel parcel, int i, byte[] bArr, boolean z) {
        if (bArr != null) {
            int b = m5610b(parcel, i);
            parcel.writeByteArray(bArr);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5608a(Parcel parcel, int i, String[] strArr, boolean z) {
        if (strArr != null) {
            int b = m5610b(parcel, i);
            parcel.writeStringArray(strArr);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static <T extends Parcelable> void m5607a(Parcel parcel, int i, T[] tArr, int i2, boolean z) {
        if (tArr != null) {
            int b = m5610b(parcel, i);
            int length = tArr.length;
            parcel.writeInt(length);
            for (T t : tArr) {
                if (t == null) {
                    parcel.writeInt(0);
                } else {
                    m5609a(parcel, t, i2);
                }
            }
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    private static <T extends Parcelable> void m5609a(Parcel parcel, Parcelable parcelable, int i) {
        int dataPosition = parcel.dataPosition();
        parcel.writeInt(1);
        int dataPosition2 = parcel.dataPosition();
        parcelable.writeToParcel(parcel, i);
        int dataPosition3 = parcel.dataPosition();
        parcel.setDataPosition(dataPosition);
        parcel.writeInt(dataPosition3 - dataPosition2);
        parcel.setDataPosition(dataPosition3);
    }

    /* renamed from: a */
    public static void m5595a(Parcel parcel, int i, Parcel parcel2, boolean z) {
        if (parcel2 != null) {
            int b = m5610b(parcel, i);
            parcel.appendFrom(parcel2, 0, parcel2.dataSize());
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }

    /* renamed from: a */
    public static void m5603a(Parcel parcel, int i, List list, boolean z) {
        if (list != null) {
            int b = m5610b(parcel, i);
            parcel.writeList(list);
            m5613c(parcel, b);
        } else if (z) {
            m5611b(parcel, i, 0);
        }
    }
}
