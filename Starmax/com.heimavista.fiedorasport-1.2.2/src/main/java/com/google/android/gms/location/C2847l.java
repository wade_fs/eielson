package com.google.android.gms.location;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.internal.location.C2401p;

/* renamed from: com.google.android.gms.location.l */
final class C2847l extends C2016a.C2017a<C2401p, Object> {
    C2847l() {
    }

    /* renamed from: a */
    public final /* synthetic */ C2016a.C2027f mo16371a(Context context, Looper looper, C2211e eVar, Object obj, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        return new C2401p(context, looper, bVar, cVar, "locationServices", eVar);
    }
}
