package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.measurement.ed */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2492ed extends C2412a implements C2459cd {
    C2492ed(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IEventHandlerProxy");
    }

    /* renamed from: a */
    public final void mo17387a(String str, String str2, Bundle bundle, long j) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7046a(H, bundle);
        H.writeLong(j);
        mo17246b(1, H);
    }

    /* renamed from: a */
    public final int mo17386a() {
        Parcel a = mo17244a(2, mo17243H());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
