package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.common.zzk;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4009a;
import p119e.p144d.p145a.p157c.p161c.p163b.C4011c;

/* renamed from: com.google.android.gms.common.internal.z0 */
public final class C2280z0 extends C4009a implements C2263x0 {
    C2280z0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    /* renamed from: a */
    public final boolean mo17045a(zzk zzk, C3988b bVar) {
        Parcel a = mo23641a();
        C4011c.m12013a(a, zzk);
        C4011c.m12012a(a, bVar);
        Parcel a2 = mo23642a(5, a);
        boolean a3 = C4011c.m12015a(a2);
        a2.recycle();
        return a3;
    }
}
