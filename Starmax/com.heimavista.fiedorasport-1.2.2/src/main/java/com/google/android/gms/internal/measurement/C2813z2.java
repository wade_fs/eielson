package com.google.android.gms.internal.measurement;

import java.util.AbstractList;
import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.z2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class C2813z2<E> extends AbstractList<E> implements C2738u4<E> {

    /* renamed from: P */
    private boolean f4624P = true;

    C2813z2() {
    }

    /* renamed from: a */
    public boolean mo17938a() {
        return this.f4624P;
    }

    public boolean add(E e) {
        mo18179b();
        return super.add(e);
    }

    public boolean addAll(Collection<? extends E> collection) {
        mo18179b();
        return super.addAll(collection);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo18179b() {
        if (!this.f4624P) {
            throw new UnsupportedOperationException();
        }
    }

    public void clear() {
        mo18179b();
        super.clear();
    }

    /* renamed from: e */
    public final void mo17939e() {
        this.f4624P = false;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        if (!(obj instanceof RandomAccess)) {
            return super.equals(obj);
        }
        List list = (List) obj;
        int size = size();
        if (size != list.size()) {
            return false;
        }
        for (int i = 0; i < size; i++) {
            if (!get(i).equals(list.get(i))) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int size = size();
        int i = 1;
        for (int i2 = 0; i2 < size; i2++) {
            i = (i * 31) + get(i2).hashCode();
        }
        return i;
    }

    public boolean remove(Object obj) {
        mo18179b();
        return super.remove(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        mo18179b();
        return super.removeAll(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        mo18179b();
        return super.retainAll(collection);
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        mo18179b();
        return super.addAll(i, collection);
    }
}
