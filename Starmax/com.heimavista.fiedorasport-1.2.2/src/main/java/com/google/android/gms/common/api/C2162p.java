package com.google.android.gms.common.api;

import com.google.android.gms.common.Feature;

/* renamed from: com.google.android.gms.common.api.p */
public final class C2162p extends UnsupportedOperationException {

    /* renamed from: P */
    private final Feature f3542P;

    public C2162p(Feature feature) {
        this.f3542P = feature;
    }

    public final String getMessage() {
        String valueOf = String.valueOf(this.f3542P);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 8);
        sb.append("Missing ");
        sb.append(valueOf);
        return sb.toString();
    }
}
