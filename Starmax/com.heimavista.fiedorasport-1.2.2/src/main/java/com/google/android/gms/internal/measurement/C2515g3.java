package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.g3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class C2515g3 implements C2580k3 {
    C2515g3() {
    }

    public /* synthetic */ Object next() {
        return Byte.valueOf(mo17442a());
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
