package com.google.android.gms.dynamite;

import android.content.Context;
import com.google.android.gms.dynamite.DynamiteModule;

/* renamed from: com.google.android.gms.dynamite.e */
final class C2348e implements DynamiteModule.C2339b {
    C2348e() {
    }

    /* renamed from: a */
    public final DynamiteModule.C2339b.C2341b mo17144a(Context context, String str, DynamiteModule.C2339b.C2340a aVar) {
        DynamiteModule.C2339b.C2341b bVar = new DynamiteModule.C2339b.C2341b();
        bVar.f3891a = aVar.mo17145a(context, str);
        if (bVar.f3891a != 0) {
            bVar.f3892b = aVar.mo17146a(context, str, false);
        } else {
            bVar.f3892b = aVar.mo17146a(context, str, true);
        }
        if (bVar.f3891a == 0 && bVar.f3892b == 0) {
            bVar.f3893c = 0;
        } else if (bVar.f3891a >= bVar.f3892b) {
            bVar.f3893c = -1;
        } else {
            bVar.f3893c = 1;
        }
        return bVar;
    }
}
