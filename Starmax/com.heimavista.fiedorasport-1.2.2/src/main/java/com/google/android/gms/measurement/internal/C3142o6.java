package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.o6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3142o6 implements Runnable {

    /* renamed from: P */
    private final C3154p6 f5499P;

    C3142o6(C3154p6 p6Var) {
        this.f5499P = p6Var;
    }

    public final void run() {
        C3154p6 p6Var = this.f5499P;
        p6Var.mo18881c();
        if (p6Var.mo19012g().f5628z.mo19333a()) {
            p6Var.mo19015l().mo18995A().mo19042a("Deferred Deep Link already retrieved. Not fetching again.");
            return;
        }
        long a = p6Var.mo19012g().f5603A.mo19326a();
        p6Var.mo19012g().f5603A.mo19327a(1 + a);
        if (a >= 5) {
            p6Var.mo19015l().mo19004w().mo19042a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
            p6Var.mo19012g().f5628z.mo19332a(true);
            return;
        }
        p6Var.f5134a.mo19096k();
    }
}
