package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.WeakHashMap;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.r */
public final class C2123r {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Map<BasePendingResult<?>, Boolean> f3428a = Collections.synchronizedMap(new WeakHashMap());

    /* renamed from: b */
    private final Map<C4066i<?>, Boolean> f3429b = Collections.synchronizedMap(new WeakHashMap());

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16768a(BasePendingResult<? extends C2157k> basePendingResult, boolean z) {
        this.f3428a.put(basePendingResult, Boolean.valueOf(z));
        basePendingResult.mo16587a(new C2127s(this, basePendingResult));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.r.a(boolean, com.google.android.gms.common.api.Status):void
     arg types: [int, com.google.android.gms.common.api.Status]
     candidates:
      com.google.android.gms.common.api.internal.r.a(com.google.android.gms.common.api.internal.BasePendingResult<? extends com.google.android.gms.common.api.k>, boolean):void
      com.google.android.gms.common.api.internal.r.a(boolean, com.google.android.gms.common.api.Status):void */
    /* renamed from: b */
    public final void mo16770b() {
        m5063a(false, C2064e.f3268c0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.r.a(boolean, com.google.android.gms.common.api.Status):void
     arg types: [int, com.google.android.gms.common.api.Status]
     candidates:
      com.google.android.gms.common.api.internal.r.a(com.google.android.gms.common.api.internal.BasePendingResult<? extends com.google.android.gms.common.api.k>, boolean):void
      com.google.android.gms.common.api.internal.r.a(boolean, com.google.android.gms.common.api.Status):void */
    /* renamed from: c */
    public final void mo16771c() {
        m5063a(true, C2141v1.f3487d);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo16769a() {
        return !this.f3428a.isEmpty() || !this.f3429b.isEmpty();
    }

    /* renamed from: a */
    private final void m5063a(boolean z, Status status) {
        HashMap hashMap;
        HashMap hashMap2;
        synchronized (this.f3428a) {
            hashMap = new HashMap(this.f3428a);
        }
        synchronized (this.f3429b) {
            hashMap2 = new HashMap(this.f3429b);
        }
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z || ((Boolean) entry.getValue()).booleanValue()) {
                ((BasePendingResult) entry.getKey()).mo16594b(status);
            }
        }
        for (Map.Entry entry2 : hashMap2.entrySet()) {
            if (z || ((Boolean) entry2.getValue()).booleanValue()) {
                ((C4066i) entry2.getKey()).mo23721b((Exception) new C2030b(status));
            }
        }
    }
}
