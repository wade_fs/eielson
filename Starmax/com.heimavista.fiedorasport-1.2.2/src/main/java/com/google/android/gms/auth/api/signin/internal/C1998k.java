package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.internal.k */
final class C1998k extends C2000m<Status> {
    C1998k(C2036f fVar) {
        super(fVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ C2157k mo16455a(Status status) {
        return status;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ void mo16456a(C2016a.C2018b bVar) {
        C1994g gVar = (C1994g) bVar;
        ((C2006s) gVar.mo16939x()).mo16463b(new C1999l(this), gVar.mo16448D());
    }
}
