package com.google.android.gms.maps;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.p093i.C2891b;
import com.google.android.gms.maps.p093i.C2912q;

/* renamed from: com.google.android.gms.maps.k */
final class C2923k extends C2912q {

    /* renamed from: a */
    private final /* synthetic */ C2885e f4795a;

    C2923k(MapFragment.C2866a aVar, C2885e eVar) {
        this.f4795a = eVar;
    }

    /* renamed from: a */
    public final void mo18487a(C2891b bVar) {
        this.f4795a.mo18411a(new C2880c(bVar));
    }
}
