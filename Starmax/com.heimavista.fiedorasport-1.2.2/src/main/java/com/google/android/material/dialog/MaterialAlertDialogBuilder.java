package com.google.android.material.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import androidx.annotation.ArrayRes;
import androidx.annotation.AttrRes;
import androidx.annotation.C0232Px;
import androidx.annotation.Dimension;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.core.view.ViewCompat;
import com.google.android.material.C3281R;
import com.google.android.material.color.MaterialColors;
import com.google.android.material.resources.MaterialAttributes;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.theme.overlay.MaterialThemeOverlay;

public class MaterialAlertDialogBuilder extends AlertDialog.Builder {
    @AttrRes
    private static final int DEF_STYLE_ATTR = C3281R.attr.alertDialogStyle;
    @StyleRes
    private static final int DEF_STYLE_RES = C3281R.C3287style.MaterialAlertDialog_MaterialComponents;
    @AttrRes
    private static final int MATERIAL_ALERT_DIALOG_THEME_OVERLAY = C3281R.attr.materialAlertDialogTheme;
    @Nullable
    private Drawable background;
    @NonNull
    @Dimension
    private final Rect backgroundInsets;

    public MaterialAlertDialogBuilder(Context context) {
        this(context, 0);
    }

    private static Context createMaterialAlertDialogThemedContext(@NonNull Context context) {
        int materialAlertDialogThemeOverlay = getMaterialAlertDialogThemeOverlay(context);
        Context wrap = MaterialThemeOverlay.wrap(context, null, DEF_STYLE_ATTR, DEF_STYLE_RES);
        if (materialAlertDialogThemeOverlay == 0) {
            return wrap;
        }
        return new ContextThemeWrapper(wrap, materialAlertDialogThemeOverlay);
    }

    private static int getMaterialAlertDialogThemeOverlay(@NonNull Context context) {
        TypedValue resolve = MaterialAttributes.resolve(context, MATERIAL_ALERT_DIALOG_THEME_OVERLAY);
        if (resolve == null) {
            return 0;
        }
        return resolve.data;
    }

    private static int getOverridingThemeResId(@NonNull Context context, int i) {
        return i == 0 ? getMaterialAlertDialogThemeOverlay(context) : i;
    }

    public AlertDialog create() {
        AlertDialog create = super.create();
        Window window = create.getWindow();
        View decorView = window.getDecorView();
        Drawable drawable = this.background;
        if (drawable instanceof MaterialShapeDrawable) {
            ((MaterialShapeDrawable) drawable).setElevation(ViewCompat.getElevation(decorView));
        }
        window.setBackgroundDrawable(MaterialDialogs.insetDrawable(this.background, this.backgroundInsets));
        decorView.setOnTouchListener(new InsetDialogOnTouchListener(create, this.backgroundInsets));
        return create;
    }

    @Nullable
    public Drawable getBackground() {
        return this.background;
    }

    @NonNull
    public MaterialAlertDialogBuilder setBackground(Drawable drawable) {
        this.background = drawable;
        return this;
    }

    @NonNull
    public MaterialAlertDialogBuilder setBackgroundInsetBottom(@C0232Px int i) {
        this.backgroundInsets.bottom = i;
        return this;
    }

    @NonNull
    public MaterialAlertDialogBuilder setBackgroundInsetEnd(@C0232Px int i) {
        if (Build.VERSION.SDK_INT < 17 || getContext().getResources().getConfiguration().getLayoutDirection() != 1) {
            this.backgroundInsets.right = i;
        } else {
            this.backgroundInsets.left = i;
        }
        return this;
    }

    @NonNull
    public MaterialAlertDialogBuilder setBackgroundInsetStart(@C0232Px int i) {
        if (Build.VERSION.SDK_INT < 17 || getContext().getResources().getConfiguration().getLayoutDirection() != 1) {
            this.backgroundInsets.left = i;
        } else {
            this.backgroundInsets.right = i;
        }
        return this;
    }

    @NonNull
    public MaterialAlertDialogBuilder setBackgroundInsetTop(@C0232Px int i) {
        this.backgroundInsets.top = i;
        return this;
    }

    public MaterialAlertDialogBuilder(Context context, int i) {
        super(createMaterialAlertDialogThemedContext(context), getOverridingThemeResId(context, i));
        Context context2 = getContext();
        Resources.Theme theme = context2.getTheme();
        this.backgroundInsets = MaterialDialogs.getDialogBackgroundInsets(context2, DEF_STYLE_ATTR, DEF_STYLE_RES);
        int color = MaterialColors.getColor(context2, C3281R.attr.colorSurface, MaterialAlertDialogBuilder.class.getCanonicalName());
        MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(context2, null, DEF_STYLE_ATTR, DEF_STYLE_RES);
        materialShapeDrawable.initializeElevationOverlay(context2);
        materialShapeDrawable.setFillColor(ColorStateList.valueOf(color));
        if (Build.VERSION.SDK_INT >= 28) {
            TypedValue typedValue = new TypedValue();
            theme.resolveAttribute(16844145, typedValue, true);
            float dimension = typedValue.getDimension(getContext().getResources().getDisplayMetrics());
            if (typedValue.type == 5 && dimension >= 0.0f) {
                materialShapeDrawable.setCornerSize(dimension);
            }
        }
        this.background = materialShapeDrawable;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setAdapter(android.widget.ListAdapter, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [android.widget.ListAdapter, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setAdapter(android.widget.ListAdapter, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setAdapter(android.widget.ListAdapter, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setAdapter(ListAdapter listAdapter, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setAdapter(listAdapter, onClickListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setCancelable(boolean z) {
        return (MaterialAlertDialogBuilder) super.setCancelable(z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setCursor(android.database.Cursor, android.content.DialogInterface$OnClickListener, java.lang.String):androidx.appcompat.app.AlertDialog$Builder
     arg types: [android.database.Cursor, android.content.DialogInterface$OnClickListener, java.lang.String]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setCursor(android.database.Cursor, android.content.DialogInterface$OnClickListener, java.lang.String):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setCursor(android.database.Cursor, android.content.DialogInterface$OnClickListener, java.lang.String):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setCursor(Cursor cursor, DialogInterface.OnClickListener onClickListener, String str) {
        return (MaterialAlertDialogBuilder) super.setCursor(cursor, onClickListener, str);
    }

    @NonNull
    public MaterialAlertDialogBuilder setCustomTitle(@Nullable View view) {
        return (MaterialAlertDialogBuilder) super.setCustomTitle(view);
    }

    @NonNull
    public MaterialAlertDialogBuilder setIconAttribute(@AttrRes int i) {
        return (MaterialAlertDialogBuilder) super.setIconAttribute(i);
    }

    @NonNull
    public MaterialAlertDialogBuilder setNegativeButtonIcon(Drawable drawable) {
        return (MaterialAlertDialogBuilder) super.setNegativeButtonIcon(drawable);
    }

    @NonNull
    public MaterialAlertDialogBuilder setNeutralButtonIcon(Drawable drawable) {
        return (MaterialAlertDialogBuilder) super.setNeutralButtonIcon(drawable);
    }

    @NonNull
    public MaterialAlertDialogBuilder setOnCancelListener(DialogInterface.OnCancelListener onCancelListener) {
        return (MaterialAlertDialogBuilder) super.setOnCancelListener(onCancelListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setOnDismissListener(DialogInterface.OnDismissListener onDismissListener) {
        return (MaterialAlertDialogBuilder) super.setOnDismissListener(onDismissListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setOnItemSelectedListener(AdapterView.OnItemSelectedListener onItemSelectedListener) {
        return (MaterialAlertDialogBuilder) super.setOnItemSelectedListener(onItemSelectedListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setOnKeyListener(DialogInterface.OnKeyListener onKeyListener) {
        return (MaterialAlertDialogBuilder) super.setOnKeyListener(onKeyListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setPositiveButtonIcon(Drawable drawable) {
        return (MaterialAlertDialogBuilder) super.setPositiveButtonIcon(drawable);
    }

    @NonNull
    public MaterialAlertDialogBuilder setIcon(@DrawableRes int i) {
        return (MaterialAlertDialogBuilder) super.setIcon(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setItems(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setItems(java.lang.CharSequence[], android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setItems(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setItems(java.lang.CharSequence[], android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setItems(java.lang.CharSequence[], android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setItems(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setItems(@ArrayRes int i, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setItems(i, onClickListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setMessage(@StringRes int i) {
        return (MaterialAlertDialogBuilder) super.setMessage(i);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setNegativeButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNegativeButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNegativeButton(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNegativeButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setNegativeButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setNegativeButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setNegativeButton(@StringRes int i, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setNegativeButton(i, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setNeutralButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNeutralButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNeutralButton(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNeutralButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setNeutralButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setNeutralButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setNeutralButton(@StringRes int i, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setNeutralButton(i, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setPositiveButton(@StringRes int i, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setPositiveButton(i, onClickListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setTitle(@StringRes int i) {
        return (MaterialAlertDialogBuilder) super.setTitle(i);
    }

    @NonNull
    public MaterialAlertDialogBuilder setView(int i) {
        return (MaterialAlertDialogBuilder) super.setView(i);
    }

    @NonNull
    public MaterialAlertDialogBuilder setIcon(@Nullable Drawable drawable) {
        return (MaterialAlertDialogBuilder) super.setIcon(drawable);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setItems(java.lang.CharSequence[], android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [java.lang.CharSequence[], android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setItems(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setItems(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setItems(java.lang.CharSequence[], android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setItems(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setItems(java.lang.CharSequence[], android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setItems(CharSequence[] charSequenceArr, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setItems(charSequenceArr, onClickListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setMessage(@Nullable CharSequence charSequence) {
        return (MaterialAlertDialogBuilder) super.setMessage(charSequence);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setMultiChoiceItems(@ArrayRes int i, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
        return (MaterialAlertDialogBuilder) super.setMultiChoiceItems(i, zArr, onMultiChoiceClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setNegativeButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [java.lang.CharSequence, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNegativeButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNegativeButton(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNegativeButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setNegativeButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setNegativeButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setNegativeButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setNegativeButton(charSequence, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setNeutralButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [java.lang.CharSequence, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNeutralButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNeutralButton(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setNeutralButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setNeutralButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setNeutralButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setNeutralButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setNeutralButton(charSequence, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [java.lang.CharSequence, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setPositiveButton(int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setPositiveButton(java.lang.CharSequence, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setPositiveButton(CharSequence charSequence, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setPositiveButton(charSequence, onClickListener);
    }

    @NonNull
    public MaterialAlertDialogBuilder setTitle(@Nullable CharSequence charSequence) {
        return (MaterialAlertDialogBuilder) super.setTitle(charSequence);
    }

    @NonNull
    public MaterialAlertDialogBuilder setView(View view) {
        return (MaterialAlertDialogBuilder) super.setView(view);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(int, boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(java.lang.CharSequence[], boolean[], android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setMultiChoiceItems(CharSequence[] charSequenceArr, boolean[] zArr, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
        return (MaterialAlertDialogBuilder) super.setMultiChoiceItems(charSequenceArr, zArr, onMultiChoiceClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [int, int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setSingleChoiceItems(@ArrayRes int i, int i2, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setSingleChoiceItems(i, i2, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(android.database.Cursor, java.lang.String, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [android.database.Cursor, java.lang.String, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setMultiChoiceItems(android.database.Cursor, java.lang.String, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setMultiChoiceItems(android.database.Cursor, java.lang.String, java.lang.String, android.content.DialogInterface$OnMultiChoiceClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setMultiChoiceItems(Cursor cursor, String str, String str2, DialogInterface.OnMultiChoiceClickListener onMultiChoiceClickListener) {
        return (MaterialAlertDialogBuilder) super.setMultiChoiceItems(cursor, str, str2, onMultiChoiceClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(android.database.Cursor, int, java.lang.String, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [android.database.Cursor, int, java.lang.String, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(android.database.Cursor, int, java.lang.String, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(android.database.Cursor, int, java.lang.String, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setSingleChoiceItems(Cursor cursor, int i, String str, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setSingleChoiceItems(cursor, i, str, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setSingleChoiceItems(CharSequence[] charSequenceArr, int i, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setSingleChoiceItems(charSequenceArr, i, onClickListener);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
     arg types: [android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener]
     candidates:
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      com.google.android.material.dialog.MaterialAlertDialogBuilder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):com.google.android.material.dialog.MaterialAlertDialogBuilder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(int, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(java.lang.CharSequence[], int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder
      androidx.appcompat.app.AlertDialog.Builder.setSingleChoiceItems(android.widget.ListAdapter, int, android.content.DialogInterface$OnClickListener):androidx.appcompat.app.AlertDialog$Builder */
    @NonNull
    public MaterialAlertDialogBuilder setSingleChoiceItems(ListAdapter listAdapter, int i, DialogInterface.OnClickListener onClickListener) {
        return (MaterialAlertDialogBuilder) super.setSingleChoiceItems(listAdapter, i, onClickListener);
    }
}
