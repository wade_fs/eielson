package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.internal.j */
final class C1997j extends C1991d {

    /* renamed from: a */
    private final /* synthetic */ C1996i f3149a;

    C1997j(C1996i iVar) {
        this.f3149a = iVar;
    }

    /* renamed from: a */
    public final void mo16445a(Status status) {
        this.f3149a.mo16593a((C2157k) status);
    }
}
