package com.google.android.exoplayer2.p086ui;

import android.content.DialogInterface;

/* renamed from: com.google.android.exoplayer2.ui.e */
/* compiled from: lambda */
public final /* synthetic */ class C1919e implements DialogInterface.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ TrackSelectionView f2929P;

    public /* synthetic */ C1919e(TrackSelectionView trackSelectionView) {
        this.f2929P = trackSelectionView;
    }

    public final void onClick(DialogInterface dialogInterface, int i) {
        this.f2929P.applySelection();
    }
}
