package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Looper;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.stats.C2303a;

/* renamed from: com.google.android.gms.measurement.internal.o8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3144o8 implements ServiceConnection, C2197d.C2198a, C2197d.C2199b {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public volatile boolean f5505P;

    /* renamed from: Q */
    private volatile C3044g4 f5506Q;

    /* renamed from: R */
    final /* synthetic */ C3232w7 f5507R;

    protected C3144o8(C3232w7 w7Var) {
        this.f5507R = w7Var;
    }

    @MainThread
    /* renamed from: L */
    public final void mo16940L(int i) {
        C2258v.m5635a("MeasurementServiceConnection.onConnectionSuspended");
        this.f5507R.mo19015l().mo18995A().mo19042a("Service connection suspended");
        this.f5507R.mo19014j().mo19028a(new C3189s8(this));
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19204a(Intent intent) {
        this.f5507R.mo18881c();
        Context n = this.f5507R.mo19016n();
        C2303a a = C2303a.m5745a();
        synchronized (this) {
            if (this.f5505P) {
                this.f5507R.mo19015l().mo18996B().mo19042a("Connection attempt already in progress");
                return;
            }
            this.f5507R.mo19015l().mo18996B().mo19042a("Using local app measurement service");
            this.f5505P = true;
            a.mo17124a(n, intent, this.f5507R.f5723c, TsExtractor.TS_STREAM_TYPE_AC3);
        }
    }

    @WorkerThread
    /* renamed from: b */
    public final void mo19205b() {
        this.f5507R.mo18881c();
        Context n = this.f5507R.mo19016n();
        synchronized (this) {
            if (this.f5505P) {
                this.f5507R.mo19015l().mo18996B().mo19042a("Connection attempt already in progress");
            } else if (this.f5506Q == null || (!this.f5506Q.mo16923e() && !this.f5506Q.mo16922c())) {
                this.f5506Q = new C3044g4(n, Looper.getMainLooper(), this, this);
                this.f5507R.mo19015l().mo18996B().mo19042a("Connecting to remote service");
                this.f5505P = true;
                this.f5506Q.mo16930o();
            } else {
                this.f5507R.mo19015l().mo18996B().mo19042a("Already awaiting connection attempt");
            }
        }
    }

    @MainThread
    /* renamed from: f */
    public final void mo16941f(@Nullable Bundle bundle) {
        C2258v.m5635a("MeasurementServiceConnection.onConnected");
        synchronized (this) {
            try {
                this.f5507R.mo19014j().mo19028a(new C3156p8(this, (C3228w3) this.f5506Q.mo16939x()));
            } catch (DeadObjectException | IllegalStateException unused) {
                this.f5506Q = null;
                this.f5505P = false;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:22|23) */
    /* JADX WARNING: Code restructure failed: missing block: B:23:?, code lost:
        r3.f5507R.mo19015l().mo19001t().mo19042a("Service connect failed to get IMeasurementService");
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0062 */
    @androidx.annotation.MainThread
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void onServiceConnected(android.content.ComponentName r4, android.os.IBinder r5) {
        /*
            r3 = this;
            java.lang.String r4 = "MeasurementServiceConnection.onServiceConnected"
            com.google.android.gms.common.internal.C2258v.m5635a(r4)
            monitor-enter(r3)
            r4 = 0
            if (r5 != 0) goto L_0x001f
            r3.f5505P = r4     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.w7 r4 = r3.f5507R     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ all -> 0x001c }
            java.lang.String r5 = "Service connected with null binder"
            r4.mo19042a(r5)     // Catch:{ all -> 0x001c }
            monitor-exit(r3)     // Catch:{ all -> 0x001c }
            return
        L_0x001c:
            r4 = move-exception
            goto L_0x0099
        L_0x001f:
            r0 = 0
            java.lang.String r1 = r5.getInterfaceDescriptor()     // Catch:{ RemoteException -> 0x0062 }
            java.lang.String r2 = "com.google.android.gms.measurement.internal.IMeasurementService"
            boolean r2 = r2.equals(r1)     // Catch:{ RemoteException -> 0x0062 }
            if (r2 == 0) goto L_0x0052
            if (r5 != 0) goto L_0x002f
            goto L_0x0042
        L_0x002f:
            java.lang.String r1 = "com.google.android.gms.measurement.internal.IMeasurementService"
            android.os.IInterface r1 = r5.queryLocalInterface(r1)     // Catch:{ RemoteException -> 0x0062 }
            boolean r2 = r1 instanceof com.google.android.gms.measurement.internal.C3228w3     // Catch:{ RemoteException -> 0x0062 }
            if (r2 == 0) goto L_0x003c
            com.google.android.gms.measurement.internal.w3 r1 = (com.google.android.gms.measurement.internal.C3228w3) r1     // Catch:{ RemoteException -> 0x0062 }
            goto L_0x0041
        L_0x003c:
            com.google.android.gms.measurement.internal.y3 r1 = new com.google.android.gms.measurement.internal.y3     // Catch:{ RemoteException -> 0x0062 }
            r1.<init>(r5)     // Catch:{ RemoteException -> 0x0062 }
        L_0x0041:
            r0 = r1
        L_0x0042:
            com.google.android.gms.measurement.internal.w7 r5 = r3.f5507R     // Catch:{ RemoteException -> 0x0062 }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ RemoteException -> 0x0062 }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo18996B()     // Catch:{ RemoteException -> 0x0062 }
            java.lang.String r1 = "Bound to IMeasurementService interface"
            r5.mo19042a(r1)     // Catch:{ RemoteException -> 0x0062 }
            goto L_0x0071
        L_0x0052:
            com.google.android.gms.measurement.internal.w7 r5 = r3.f5507R     // Catch:{ RemoteException -> 0x0062 }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ RemoteException -> 0x0062 }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19001t()     // Catch:{ RemoteException -> 0x0062 }
            java.lang.String r2 = "Got binder with a wrong descriptor"
            r5.mo19043a(r2, r1)     // Catch:{ RemoteException -> 0x0062 }
            goto L_0x0071
        L_0x0062:
            com.google.android.gms.measurement.internal.w7 r5 = r3.f5507R     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19001t()     // Catch:{ all -> 0x001c }
            java.lang.String r1 = "Service connect failed to get IMeasurementService"
            r5.mo19042a(r1)     // Catch:{ all -> 0x001c }
        L_0x0071:
            if (r0 != 0) goto L_0x0089
            r3.f5505P = r4     // Catch:{ all -> 0x001c }
            com.google.android.gms.common.stats.a r4 = com.google.android.gms.common.stats.C2303a.m5745a()     // Catch:{ IllegalArgumentException -> 0x0097 }
            com.google.android.gms.measurement.internal.w7 r5 = r3.f5507R     // Catch:{ IllegalArgumentException -> 0x0097 }
            android.content.Context r5 = r5.mo19016n()     // Catch:{ IllegalArgumentException -> 0x0097 }
            com.google.android.gms.measurement.internal.w7 r0 = r3.f5507R     // Catch:{ IllegalArgumentException -> 0x0097 }
            com.google.android.gms.measurement.internal.o8 r0 = r0.f5723c     // Catch:{ IllegalArgumentException -> 0x0097 }
            r4.mo17123a(r5, r0)     // Catch:{ IllegalArgumentException -> 0x0097 }
            goto L_0x0097
        L_0x0089:
            com.google.android.gms.measurement.internal.w7 r4 = r3.f5507R     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.g5 r4 = r4.mo19014j()     // Catch:{ all -> 0x001c }
            com.google.android.gms.measurement.internal.n8 r5 = new com.google.android.gms.measurement.internal.n8     // Catch:{ all -> 0x001c }
            r5.<init>(r3, r0)     // Catch:{ all -> 0x001c }
            r4.mo19028a(r5)     // Catch:{ all -> 0x001c }
        L_0x0097:
            monitor-exit(r3)     // Catch:{ all -> 0x001c }
            return
        L_0x0099:
            monitor-exit(r3)     // Catch:{ all -> 0x001c }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3144o8.onServiceConnected(android.content.ComponentName, android.os.IBinder):void");
    }

    @MainThread
    public final void onServiceDisconnected(ComponentName componentName) {
        C2258v.m5635a("MeasurementServiceConnection.onServiceDisconnected");
        this.f5507R.mo19015l().mo18995A().mo19042a("Service disconnected");
        this.f5507R.mo19014j().mo19028a(new C3167q8(this, componentName));
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19203a() {
        if (this.f5506Q != null && (this.f5506Q.mo16922c() || this.f5506Q.mo16923e())) {
            this.f5506Q.mo16909a();
        }
        this.f5506Q = null;
    }

    @MainThread
    /* renamed from: a */
    public final void mo16942a(@NonNull ConnectionResult connectionResult) {
        C2258v.m5635a("MeasurementServiceConnection.onConnectionFailed");
        C3032f4 q = this.f5507R.f5134a.mo19099q();
        if (q != null) {
            q.mo19004w().mo19043a("Service connection failed", connectionResult);
        }
        synchronized (this) {
            this.f5505P = false;
            this.f5506Q = null;
        }
        this.f5507R.mo19014j().mo19028a(new C3178r8(this));
    }
}
