package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2818z7;

/* renamed from: com.google.android.gms.measurement.internal.z1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3259z1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5786a = new C3259z1();

    private C3259z1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2818z7.m7929b());
    }
}
