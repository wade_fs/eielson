package com.google.android.gms.measurement.internal;

import android.os.Handler;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2520g8;

/* renamed from: com.google.android.gms.measurement.internal.g */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
abstract class C3039g {

    /* renamed from: d */
    private static volatile Handler f5142d;

    /* renamed from: a */
    private final C3058h6 f5143a;

    /* renamed from: b */
    private final Runnable f5144b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public volatile long f5145c;

    C3039g(C3058h6 h6Var) {
        C2258v.m5629a(h6Var);
        this.f5143a = h6Var;
        this.f5144b = new C3075j(this, h6Var);
    }

    /* renamed from: d */
    private final Handler m8656d() {
        Handler handler;
        if (f5142d != null) {
            return f5142d;
        }
        synchronized (C3039g.class) {
            if (f5142d == null) {
                f5142d = new C2520g8(this.f5143a.mo19016n().getMainLooper());
            }
            handler = f5142d;
        }
        return handler;
    }

    /* renamed from: a */
    public abstract void mo19022a();

    /* renamed from: a */
    public final void mo19023a(long j) {
        mo19025c();
        if (j >= 0) {
            this.f5145c = this.f5143a.mo19017o().mo17132a();
            if (!m8656d().postDelayed(this.f5144b, j)) {
                this.f5143a.mo19015l().mo19001t().mo19043a("Failed to schedule delayed post. time", Long.valueOf(j));
            }
        }
    }

    /* renamed from: b */
    public final boolean mo19024b() {
        return this.f5145c != 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final void mo19025c() {
        this.f5145c = 0;
        m8656d().removeCallbacks(this.f5144b);
    }
}
