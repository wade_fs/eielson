package com.google.android.exoplayer2.extractor.ogg;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.ogg.a */
/* compiled from: lambda */
public final /* synthetic */ class C1808a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1808a f2820a = new C1808a();

    private /* synthetic */ C1808a() {
    }

    public final Extractor[] createExtractors() {
        return OggExtractor.m4387a();
    }
}
