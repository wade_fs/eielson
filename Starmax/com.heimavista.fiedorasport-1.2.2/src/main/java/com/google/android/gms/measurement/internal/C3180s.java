package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2472da;

/* renamed from: com.google.android.gms.measurement.internal.s */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3180s implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5598a = new C3180s();

    private C3180s() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2472da.m6222c());
    }
}
