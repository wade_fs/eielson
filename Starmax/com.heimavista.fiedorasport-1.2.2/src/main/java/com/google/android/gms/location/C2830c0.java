package com.google.android.gms.location;

import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.api.internal.C2115p;
import com.google.android.gms.internal.location.C2401p;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.location.c0 */
final class C2830c0 extends C2115p<C2401p, C2833e> {

    /* renamed from: b */
    private final /* synthetic */ C2826b f4690b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2830c0(C2826b bVar, C2084i.C2085a aVar) {
        super(aVar);
        this.f4690b = bVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ void mo16763a(C2016a.C2018b bVar, C4066i iVar) {
        try {
            ((C2401p) bVar).mo17216a(mo16762a(), this.f4690b.m7968a(iVar));
        } catch (RuntimeException e) {
            iVar.mo23721b((Exception) e);
        }
    }
}
