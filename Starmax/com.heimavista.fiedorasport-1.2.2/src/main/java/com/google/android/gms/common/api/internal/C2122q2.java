package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;

/* renamed from: com.google.android.gms.common.api.internal.q2 */
final class C2122q2 implements C2089i1 {

    /* renamed from: a */
    private final /* synthetic */ C2110n2 f3427a;

    private C2122q2(C2110n2 n2Var) {
        this.f3427a = n2Var;
    }

    /* renamed from: a */
    public final void mo16729a(@Nullable Bundle bundle) {
        this.f3427a.f3412m.lock();
        try {
            ConnectionResult unused = this.f3427a.f3410k = ConnectionResult.f3156T;
            this.f3427a.m5026i();
        } finally {
            this.f3427a.f3412m.unlock();
        }
    }

    /* synthetic */ C2122q2(C2110n2 n2Var, C2114o2 o2Var) {
        this(n2Var);
    }

    /* renamed from: a */
    public final void mo16730a(@NonNull ConnectionResult connectionResult) {
        this.f3427a.f3412m.lock();
        try {
            ConnectionResult unused = this.f3427a.f3410k = connectionResult;
            this.f3427a.m5026i();
        } finally {
            this.f3427a.f3412m.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.n2, int]
     candidates:
      com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, com.google.android.gms.common.ConnectionResult):com.google.android.gms.common.ConnectionResult
      com.google.android.gms.common.api.internal.n2.a(int, boolean):void
      com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, android.os.Bundle):void
      com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, boolean):boolean */
    /* renamed from: a */
    public final void mo16728a(int i, boolean z) {
        this.f3427a.f3412m.lock();
        try {
            if (this.f3427a.f3411l) {
                boolean unused = this.f3427a.f3411l = false;
                this.f3427a.m5011a(i, z);
                return;
            }
            boolean unused2 = this.f3427a.f3411l = true;
            this.f3427a.f3403d.mo16583L(i);
            this.f3427a.f3412m.unlock();
        } finally {
            this.f3427a.f3412m.unlock();
        }
    }
}
