package com.google.android.gms.maps.p093i;

import android.os.IInterface;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4043l;
import p119e.p144d.p145a.p157c.p161c.p165d.C4046o;

/* renamed from: com.google.android.gms.maps.i.b */
public interface C2891b extends IInterface {
    /* renamed from: F */
    C2903h mo18421F();

    /* renamed from: a */
    C4043l mo18422a(MarkerOptions markerOptions);

    /* renamed from: a */
    C4046o mo18423a(PolylineOptions polylineOptions);

    /* renamed from: a */
    void mo18424a(C2907l lVar);

    /* renamed from: a */
    void mo18425a(C2909n nVar);

    /* renamed from: a */
    void mo18426a(C2913r rVar);

    /* renamed from: b */
    void mo18427b(C3988b bVar);

    void clear();

    /* renamed from: o */
    void mo18429o(boolean z);
}
