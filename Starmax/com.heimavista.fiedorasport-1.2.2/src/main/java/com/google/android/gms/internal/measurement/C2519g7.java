package com.google.android.gms.internal.measurement;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.measurement.g7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2519g7 implements Iterator<String> {

    /* renamed from: P */
    private Iterator<String> f4175P = this.f4176Q.f4074P.iterator();

    /* renamed from: Q */
    private final /* synthetic */ C2486e7 f4176Q;

    C2519g7(C2486e7 e7Var) {
        this.f4176Q = e7Var;
    }

    public final boolean hasNext() {
        return this.f4175P.hasNext();
    }

    public final /* synthetic */ Object next() {
        return this.f4175P.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
