package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.t9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3201t9 implements C3104l4 {

    /* renamed from: a */
    private final /* synthetic */ C3145o9 f5666a;

    C3201t9(C3145o9 o9Var) {
        this.f5666a = o9Var;
    }

    /* renamed from: a */
    public final void mo19135a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.f5666a.mo19218a(str, i, th, bArr, map);
    }
}
