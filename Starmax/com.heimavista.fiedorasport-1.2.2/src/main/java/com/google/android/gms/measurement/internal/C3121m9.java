package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.m9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
class C3121m9 extends C3034f6 implements C3058h6 {

    /* renamed from: b */
    protected final C3145o9 f5360b;

    C3121m9(C3145o9 o9Var) {
        super(o9Var.mo19239t());
        C2258v.m5629a(o9Var);
        this.f5360b = o9Var;
    }

    /* renamed from: i */
    public C3223v9 mo19171i() {
        return this.f5360b.mo19232h();
    }

    /* renamed from: k */
    public C3003d mo19172k() {
        return this.f5360b.mo19229e();
    }

    /* renamed from: m */
    public C3009d5 mo19173m() {
        return this.f5360b.mo19225c();
    }
}
