package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.app.job.JobParameters;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.f */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1732f implements Runnable {

    /* renamed from: P */
    private final JobInfoSchedulerService f2732P;

    /* renamed from: Q */
    private final JobParameters f2733Q;

    private C1732f(JobInfoSchedulerService jobInfoSchedulerService, JobParameters jobParameters) {
        this.f2732P = jobInfoSchedulerService;
        this.f2733Q = jobParameters;
    }

    /* renamed from: a */
    public static Runnable m4323a(JobInfoSchedulerService jobInfoSchedulerService, JobParameters jobParameters) {
        return new C1732f(jobInfoSchedulerService, jobParameters);
    }

    public void run() {
        this.f2732P.jobFinished(this.f2733Q, false);
    }
}
