package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.b2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2982b2 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f4962P;

    /* renamed from: Q */
    private final /* synthetic */ long f4963Q;

    /* renamed from: R */
    private final /* synthetic */ C3257z f4964R;

    C2982b2(C3257z zVar, String str, long j) {
        this.f4964R = zVar;
        this.f4962P = str;
        this.f4963Q = j;
    }

    public final void run() {
        this.f4964R.m9399d(this.f4962P, this.f4963Q);
    }
}
