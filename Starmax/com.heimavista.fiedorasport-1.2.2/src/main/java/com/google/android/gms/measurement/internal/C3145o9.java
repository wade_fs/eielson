package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import com.facebook.internal.AnalyticsEvents;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2314e;
import com.google.android.gms.internal.measurement.C2414a1;
import com.google.android.gms.internal.measurement.C2489ea;
import com.google.android.gms.internal.measurement.C2626n0;
import com.google.android.gms.internal.measurement.C2701s0;
import com.google.android.gms.internal.measurement.C2733u0;
import com.google.android.gms.internal.measurement.C2763w0;
import com.google.android.gms.internal.measurement.C2774wa;
import com.google.android.gms.internal.measurement.C2789x9;
import com.google.android.gms.internal.measurement.C2805y9;
import com.google.android.gms.internal.measurement.zzv;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.channels.OverlappingFileLockException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: com.google.android.gms.measurement.internal.o9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public class C3145o9 implements C3058h6 {

    /* renamed from: y */
    private static volatile C3145o9 f5508y;

    /* renamed from: a */
    private C3009d5 f5509a;

    /* renamed from: b */
    private C3080j4 f5510b;

    /* renamed from: c */
    private C3003d f5511c;

    /* renamed from: d */
    private C3163q4 f5512d;

    /* renamed from: e */
    private C3097k9 f5513e;

    /* renamed from: f */
    private C3002ca f5514f;

    /* renamed from: g */
    private final C3223v9 f5515g;

    /* renamed from: h */
    private C3166q7 f5516h;

    /* renamed from: i */
    private final C3081j5 f5517i;

    /* renamed from: j */
    private boolean f5518j;

    /* renamed from: k */
    private boolean f5519k;

    /* renamed from: l */
    private boolean f5520l;

    /* renamed from: m */
    private long f5521m;

    /* renamed from: n */
    private List<Runnable> f5522n;

    /* renamed from: o */
    private int f5523o;

    /* renamed from: p */
    private int f5524p;

    /* renamed from: q */
    private boolean f5525q;

    /* renamed from: r */
    private boolean f5526r;

    /* renamed from: s */
    private boolean f5527s;

    /* renamed from: t */
    private FileLock f5528t;

    /* renamed from: u */
    private FileChannel f5529u;

    /* renamed from: v */
    private List<Long> f5530v;

    /* renamed from: w */
    private List<Long> f5531w;

    /* renamed from: x */
    private long f5532x;

    /* renamed from: com.google.android.gms.measurement.internal.o9$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    class C3146a implements C3027f {

        /* renamed from: a */
        C2763w0 f5533a;

        /* renamed from: b */
        List<Long> f5534b;

        /* renamed from: c */
        List<C2701s0> f5535c;

        /* renamed from: d */
        private long f5536d;

        private C3146a(C3145o9 o9Var) {
        }

        /* renamed from: a */
        public final void mo18993a(C2763w0 w0Var) {
            C2258v.m5629a(w0Var);
            this.f5533a = w0Var;
        }

        /* synthetic */ C3146a(C3145o9 o9Var, C3179r9 r9Var) {
            this(o9Var);
        }

        /* renamed from: a */
        public final boolean mo18994a(long j, C2701s0 s0Var) {
            C2258v.m5629a(s0Var);
            if (this.f5535c == null) {
                this.f5535c = new ArrayList();
            }
            if (this.f5534b == null) {
                this.f5534b = new ArrayList();
            }
            if (this.f5535c.size() > 0 && m9108a(this.f5535c.get(0)) != m9108a(s0Var)) {
                return false;
            }
            long e = this.f5536d + ((long) s0Var.mo17663e());
            if (e >= ((long) Math.max(0, C3135o.f5450i.mo19389a(null).intValue()))) {
                return false;
            }
            this.f5536d = e;
            this.f5535c.add(s0Var);
            this.f5534b.add(Long.valueOf(j));
            if (this.f5535c.size() >= Math.max(1, C3135o.f5452j.mo19389a(null).intValue())) {
                return false;
            }
            return true;
        }

        /* renamed from: a */
        private static long m9108a(C2701s0 s0Var) {
            return ((s0Var.mo17851s() / 1000) / 60) / 60;
        }
    }

    private C3145o9(C3212u9 u9Var) {
        this(u9Var, null);
    }

    /* renamed from: A */
    private final long m9042A() {
        long a = this.f5517i.mo19017o().mo17132a();
        C3185s4 p = this.f5517i.mo19098p();
        p.mo18903k();
        p.mo18881c();
        long a2 = p.f5611i.mo19326a();
        if (a2 == 0) {
            a2 = 1 + ((long) p.mo19011f().mo19454u().nextInt(86400000));
            p.f5611i.mo19327a(a2);
        }
        return ((((a + a2) / 1000) / 60) / 60) / 24;
    }

    /* renamed from: B */
    private final boolean m9043B() {
        m9070z();
        mo19235m();
        return mo19229e().mo18838E() || !TextUtils.isEmpty(mo19229e().mo18876w());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x01a2  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c0  */
    @androidx.annotation.WorkerThread
    /* renamed from: C */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m9044C() {
        /*
            r21 = this;
            r0 = r21
            r21.m9070z()
            r21.mo19235m()
            boolean r1 = r21.m9067w()
            if (r1 != 0) goto L_0x001d
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.la r1 = r1.mo19097m()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.C3135o.f5439c0
            boolean r1 = r1.mo19146a(r2)
            if (r1 != 0) goto L_0x001d
            return
        L_0x001d:
            long r1 = r0.f5521m
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0062
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.common.util.e r1 = r1.mo19017o()
            long r1 = r1.elapsedRealtime()
            r5 = 3600000(0x36ee80, double:1.7786363E-317)
            long r7 = r0.f5521m
            long r1 = r1 - r7
            long r1 = java.lang.Math.abs(r1)
            long r5 = r5 - r1
            int r1 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0060
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            java.lang.Long r2 = java.lang.Long.valueOf(r5)
            java.lang.String r3 = "Upload has been suspended. Will update scheduling later in approximately ms"
            r1.mo19043a(r3, r2)
            com.google.android.gms.measurement.internal.q4 r1 = r21.m9068x()
            r1.mo19287b()
            com.google.android.gms.measurement.internal.k9 r1 = r21.m9069y()
            r1.mo19132u()
            return
        L_0x0060:
            r0.f5521m = r3
        L_0x0062:
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            boolean r1 = r1.mo19095i()
            if (r1 == 0) goto L_0x026b
            boolean r1 = r21.m9043B()
            if (r1 != 0) goto L_0x0072
            goto L_0x026b
        L_0x0072:
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.common.util.e r1 = r1.mo19017o()
            long r1 = r1.mo17132a()
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r5 = com.google.android.gms.measurement.internal.C3135o.f5382A
            r6 = 0
            java.lang.Object r5 = r5.mo19389a(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r7 = r5.longValue()
            long r7 = java.lang.Math.max(r3, r7)
            com.google.android.gms.measurement.internal.d r5 = r21.mo19229e()
            boolean r5 = r5.mo18839F()
            if (r5 != 0) goto L_0x00a4
            com.google.android.gms.measurement.internal.d r5 = r21.mo19229e()
            boolean r5 = r5.mo18834A()
            if (r5 == 0) goto L_0x00a2
            goto L_0x00a4
        L_0x00a2:
            r5 = 0
            goto L_0x00a5
        L_0x00a4:
            r5 = 1
        L_0x00a5:
            if (r5 == 0) goto L_0x00e1
            com.google.android.gms.measurement.internal.j5 r10 = r0.f5517i
            com.google.android.gms.measurement.internal.la r10 = r10.mo19097m()
            java.lang.String r10 = r10.mo19162s()
            boolean r11 = android.text.TextUtils.isEmpty(r10)
            if (r11 != 0) goto L_0x00d0
            java.lang.String r11 = ".none."
            boolean r10 = r11.equals(r10)
            if (r10 != 0) goto L_0x00d0
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r10 = com.google.android.gms.measurement.internal.C3135o.f5476v
            java.lang.Object r10 = r10.mo19389a(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00f1
        L_0x00d0:
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r10 = com.google.android.gms.measurement.internal.C3135o.f5474u
            java.lang.Object r10 = r10.mo19389a(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
            goto L_0x00f1
        L_0x00e1:
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r10 = com.google.android.gms.measurement.internal.C3135o.f5472t
            java.lang.Object r10 = r10.mo19389a(r6)
            java.lang.Long r10 = (java.lang.Long) r10
            long r10 = r10.longValue()
            long r10 = java.lang.Math.max(r3, r10)
        L_0x00f1:
            com.google.android.gms.measurement.internal.j5 r12 = r0.f5517i
            com.google.android.gms.measurement.internal.s4 r12 = r12.mo19098p()
            com.google.android.gms.measurement.internal.t4 r12 = r12.f5607e
            long r12 = r12.mo19326a()
            com.google.android.gms.measurement.internal.j5 r14 = r0.f5517i
            com.google.android.gms.measurement.internal.s4 r14 = r14.mo19098p()
            com.google.android.gms.measurement.internal.t4 r14 = r14.f5608f
            long r14 = r14.mo19326a()
            com.google.android.gms.measurement.internal.d r16 = r21.mo19229e()
            r17 = r10
            long r9 = r16.mo18836C()
            com.google.android.gms.measurement.internal.d r11 = r21.mo19229e()
            r19 = r7
            long r6 = r11.mo18837D()
            long r6 = java.lang.Math.max(r9, r6)
            int r8 = (r6 > r3 ? 1 : (r6 == r3 ? 0 : -1))
            if (r8 != 0) goto L_0x0128
        L_0x0125:
            r8 = r3
            goto L_0x019e
        L_0x0128:
            long r6 = r6 - r1
            long r6 = java.lang.Math.abs(r6)
            long r6 = r1 - r6
            long r12 = r12 - r1
            long r8 = java.lang.Math.abs(r12)
            long r8 = r1 - r8
            long r14 = r14 - r1
            long r10 = java.lang.Math.abs(r14)
            long r1 = r1 - r10
            long r8 = java.lang.Math.max(r8, r1)
            long r10 = r6 + r19
            if (r5 == 0) goto L_0x014e
            int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x014e
            long r10 = java.lang.Math.min(r6, r8)
            long r10 = r10 + r17
        L_0x014e:
            com.google.android.gms.measurement.internal.v9 r5 = r21.mo19232h()
            r12 = r17
            boolean r5 = r5.mo19359a(r8, r12)
            if (r5 != 0) goto L_0x015c
            long r8 = r8 + r12
            goto L_0x015d
        L_0x015c:
            r8 = r10
        L_0x015d:
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x019e
            int r5 = (r1 > r6 ? 1 : (r1 == r6 ? 0 : -1))
            if (r5 < 0) goto L_0x019e
            r5 = 0
        L_0x0166:
            r6 = 20
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.C3135o.f5386C
            r10 = 0
            java.lang.Object r7 = r7.mo19389a(r10)
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            r11 = 0
            int r7 = java.lang.Math.max(r11, r7)
            int r6 = java.lang.Math.min(r6, r7)
            if (r5 >= r6) goto L_0x0125
            r6 = 1
            long r6 = r6 << r5
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r12 = com.google.android.gms.measurement.internal.C3135o.f5384B
            java.lang.Object r12 = r12.mo19389a(r10)
            java.lang.Long r12 = (java.lang.Long) r12
            long r12 = r12.longValue()
            long r12 = java.lang.Math.max(r3, r12)
            long r12 = r12 * r6
            long r8 = r8 + r12
            int r6 = (r8 > r1 ? 1 : (r8 == r1 ? 0 : -1))
            if (r6 <= 0) goto L_0x019b
            goto L_0x019e
        L_0x019b:
            int r5 = r5 + 1
            goto L_0x0166
        L_0x019e:
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 != 0) goto L_0x01c0
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            java.lang.String r2 = "Next upload time is 0"
            r1.mo19042a(r2)
            com.google.android.gms.measurement.internal.q4 r1 = r21.m9068x()
            r1.mo19287b()
            com.google.android.gms.measurement.internal.k9 r1 = r21.m9069y()
            r1.mo19132u()
            return
        L_0x01c0:
            com.google.android.gms.measurement.internal.j4 r1 = r21.mo19227d()
            boolean r1 = r1.mo19073u()
            if (r1 != 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            java.lang.String r2 = "No network"
            r1.mo19042a(r2)
            com.google.android.gms.measurement.internal.q4 r1 = r21.m9068x()
            r1.mo19286a()
            com.google.android.gms.measurement.internal.k9 r1 = r21.m9069y()
            r1.mo19132u()
            return
        L_0x01e8:
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.s4 r1 = r1.mo19098p()
            com.google.android.gms.measurement.internal.t4 r1 = r1.f5609g
            long r1 = r1.mo19326a()
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r5 = com.google.android.gms.measurement.internal.C3135o.f5468r
            r6 = 0
            java.lang.Object r5 = r5.mo19389a(r6)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            long r5 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.v9 r7 = r21.mo19232h()
            boolean r7 = r7.mo19359a(r1, r5)
            if (r7 != 0) goto L_0x0214
            long r1 = r1 + r5
            long r8 = java.lang.Math.max(r8, r1)
        L_0x0214:
            com.google.android.gms.measurement.internal.q4 r1 = r21.m9068x()
            r1.mo19287b()
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.common.util.e r1 = r1.mo19017o()
            long r1 = r1.mo17132a()
            long r8 = r8 - r1
            int r1 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r1 > 0) goto L_0x0250
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r1 = com.google.android.gms.measurement.internal.C3135o.f5478w
            r2 = 0
            java.lang.Object r1 = r1.mo19389a(r2)
            java.lang.Long r1 = (java.lang.Long) r1
            long r1 = r1.longValue()
            long r8 = java.lang.Math.max(r3, r1)
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.s4 r1 = r1.mo19098p()
            com.google.android.gms.measurement.internal.t4 r1 = r1.f5607e
            com.google.android.gms.measurement.internal.j5 r2 = r0.f5517i
            com.google.android.gms.common.util.e r2 = r2.mo19017o()
            long r2 = r2.mo17132a()
            r1.mo19327a(r2)
        L_0x0250:
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            java.lang.Long r2 = java.lang.Long.valueOf(r8)
            java.lang.String r3 = "Upload scheduled in approximately ms"
            r1.mo19043a(r3, r2)
            com.google.android.gms.measurement.internal.k9 r1 = r21.m9069y()
            r1.mo19131a(r8)
            return
        L_0x026b:
            com.google.android.gms.measurement.internal.j5 r1 = r0.f5517i
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            java.lang.String r2 = "Nothing to upload or uploading impossible"
            r1.mo19042a(r2)
            com.google.android.gms.measurement.internal.q4 r1 = r21.m9068x()
            r1.mo19287b()
            com.google.android.gms.measurement.internal.k9 r1 = r21.m9069y()
            r1.mo19132u()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.m9044C():void");
    }

    /* renamed from: a */
    public static C3145o9 m9047a(Context context) {
        C2258v.m5629a(context);
        C2258v.m5629a(context.getApplicationContext());
        if (f5508y == null) {
            synchronized (C3145o9.class) {
                if (f5508y == null) {
                    f5508y = new C3145o9(new C3212u9(context));
                }
            }
        }
        return f5508y;
    }

    @WorkerThread
    /* renamed from: u */
    private final void m9065u() {
        m9070z();
        if (this.f5525q || this.f5526r || this.f5527s) {
            this.f5517i.mo19015l().mo18996B().mo19045a("Not stopping services. fetch, network, upload", Boolean.valueOf(this.f5525q), Boolean.valueOf(this.f5526r), Boolean.valueOf(this.f5527s));
            return;
        }
        this.f5517i.mo19015l().mo18996B().mo19042a("Stopping uploading service(s)");
        List<Runnable> list = this.f5522n;
        if (list != null) {
            for (Runnable runnable : list) {
                runnable.run();
            }
            this.f5522n.clear();
        }
    }

    @WorkerThread
    /* renamed from: v */
    private final boolean m9066v() {
        FileLock fileLock;
        m9070z();
        if (!this.f5517i.mo19097m().mo19146a(C3135o.f5391E0) || (fileLock = this.f5528t) == null || !fileLock.isValid()) {
            try {
                this.f5529u = new RandomAccessFile(new File(this.f5517i.mo19016n().getFilesDir(), "google_app_measurement.db"), "rw").getChannel();
                this.f5528t = this.f5529u.tryLock();
                if (this.f5528t != null) {
                    this.f5517i.mo19015l().mo18996B().mo19042a("Storage concurrent access okay");
                    return true;
                }
                this.f5517i.mo19015l().mo19001t().mo19042a("Storage concurrent data access panic");
                return false;
            } catch (FileNotFoundException e) {
                this.f5517i.mo19015l().mo19001t().mo19043a("Failed to acquire storage lock", e);
                return false;
            } catch (IOException e2) {
                this.f5517i.mo19015l().mo19001t().mo19043a("Failed to access storage lock file", e2);
                return false;
            } catch (OverlappingFileLockException e3) {
                this.f5517i.mo19015l().mo19004w().mo19043a("Storage lock already acquired", e3);
                return false;
            }
        } else {
            this.f5517i.mo19015l().mo18996B().mo19042a("Storage concurrent access okay");
            return true;
        }
    }

    @WorkerThread
    /* renamed from: w */
    private final boolean m9067w() {
        m9070z();
        mo19235m();
        return this.f5519k;
    }

    /* renamed from: x */
    private final C3163q4 m9068x() {
        C3163q4 q4Var = this.f5512d;
        if (q4Var != null) {
            return q4Var;
        }
        throw new IllegalStateException("Network broadcast receiver not created");
    }

    /* renamed from: y */
    private final C3097k9 m9069y() {
        m9062b(this.f5513e);
        return this.f5513e;
    }

    @WorkerThread
    /* renamed from: z */
    private final void m9070z() {
        this.f5517i.mo19014j().mo18881c();
    }

    /* renamed from: b */
    public final C3110la mo19220b() {
        return this.f5517i.mo19097m();
    }

    /* renamed from: c */
    public final C3009d5 mo19225c() {
        m9062b(this.f5509a);
        return this.f5509a;
    }

    /* renamed from: d */
    public final C3080j4 mo19227d() {
        m9062b(this.f5510b);
        return this.f5510b;
    }

    /* renamed from: e */
    public final C3003d mo19229e() {
        m9062b(this.f5511c);
        return this.f5511c;
    }

    /* renamed from: f */
    public final C3002ca mo19230f() {
        m9062b(this.f5514f);
        return this.f5514f;
    }

    /* renamed from: g */
    public final C3166q7 mo19231g() {
        m9062b(this.f5516h);
        return this.f5516h;
    }

    /* renamed from: h */
    public final C3223v9 mo19232h() {
        m9062b(this.f5515g);
        return this.f5515g;
    }

    /* renamed from: i */
    public final C2996c4 mo19233i() {
        return this.f5517i.mo19105x();
    }

    /* renamed from: j */
    public final C3045g5 mo19014j() {
        return this.f5517i.mo19014j();
    }

    /* renamed from: k */
    public final C3267z9 mo19234k() {
        return this.f5517i.mo19104w();
    }

    /* renamed from: l */
    public final C3032f4 mo19015l() {
        return this.f5517i.mo19015l();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public final void mo19235m() {
        if (!this.f5518j) {
            throw new IllegalStateException("UploadController is not initialized");
        }
    }

    /* renamed from: n */
    public final Context mo19016n() {
        return this.f5517i.mo19016n();
    }

    /* renamed from: o */
    public final C2314e mo19017o() {
        return this.f5517i.mo19017o();
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:89|90) */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r1.f5517i.mo19015l().mo19001t().mo19044a("Failed to parse upload URL. Not uploading. appId", com.google.android.gms.measurement.internal.C3032f4.m8621a(r5), r9);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:89:0x02b7 */
    @androidx.annotation.WorkerThread
    /* renamed from: p */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo19236p() {
        /*
            r17 = this;
            r1 = r17
            r17.m9070z()
            r17.mo19235m()
            r0 = 1
            r1.f5527s = r0
            r2 = 0
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            r3.mo19018r()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.w7 r3 = r3.mo19079F()     // Catch:{ all -> 0x02f3 }
            java.lang.Boolean r3 = r3.mo19373G()     // Catch:{ all -> 0x02f3 }
            if (r3 != 0) goto L_0x0032
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()     // Catch:{ all -> 0x02f3 }
            java.lang.String r3 = "Upload data called on the client side before use of service was decided"
            r0.mo19042a(r3)     // Catch:{ all -> 0x02f3 }
            r1.f5527s = r2
            r17.m9065u()
            return
        L_0x0032:
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x02f3 }
            if (r3 == 0) goto L_0x004d
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ all -> 0x02f3 }
            java.lang.String r3 = "Upload called in the client side when service should be used"
            r0.mo19042a(r3)     // Catch:{ all -> 0x02f3 }
            r1.f5527s = r2
            r17.m9065u()
            return
        L_0x004d:
            long r3 = r1.f5521m     // Catch:{ all -> 0x02f3 }
            r5 = 0
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x005e
            r17.m9044C()     // Catch:{ all -> 0x02f3 }
            r1.f5527s = r2
            r17.m9065u()
            return
        L_0x005e:
            r17.m9070z()     // Catch:{ all -> 0x02f3 }
            java.util.List<java.lang.Long> r3 = r1.f5530v     // Catch:{ all -> 0x02f3 }
            if (r3 == 0) goto L_0x0067
            r3 = 1
            goto L_0x0068
        L_0x0067:
            r3 = 0
        L_0x0068:
            if (r3 == 0) goto L_0x007f
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()     // Catch:{ all -> 0x02f3 }
            java.lang.String r3 = "Uploading requested multiple times"
            r0.mo19042a(r3)     // Catch:{ all -> 0x02f3 }
            r1.f5527s = r2
            r17.m9065u()
            return
        L_0x007f:
            com.google.android.gms.measurement.internal.j4 r3 = r17.mo19227d()     // Catch:{ all -> 0x02f3 }
            boolean r3 = r3.mo19073u()     // Catch:{ all -> 0x02f3 }
            if (r3 != 0) goto L_0x00a1
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()     // Catch:{ all -> 0x02f3 }
            java.lang.String r3 = "Network not connected, ignoring upload request"
            r0.mo19042a(r3)     // Catch:{ all -> 0x02f3 }
            r17.m9044C()     // Catch:{ all -> 0x02f3 }
            r1.f5527s = r2
            r17.m9065u()
            return
        L_0x00a1:
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.common.util.e r3 = r3.mo19017o()     // Catch:{ all -> 0x02f3 }
            long r3 = r3.mo17132a()     // Catch:{ all -> 0x02f3 }
            long r7 = com.google.android.gms.measurement.internal.C3110la.m8857w()     // Catch:{ all -> 0x02f3 }
            long r7 = r3 - r7
            r9 = 0
            r1.m9059a(r9, r7)     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.s4 r7 = r7.mo19098p()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.t4 r7 = r7.f5607e     // Catch:{ all -> 0x02f3 }
            long r7 = r7.mo19326a()     // Catch:{ all -> 0x02f3 }
            int r10 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r10 == 0) goto L_0x00de
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo18995A()     // Catch:{ all -> 0x02f3 }
            java.lang.String r6 = "Uploading events. Elapsed time since last upload attempt (ms)"
            long r7 = r3 - r7
            long r7 = java.lang.Math.abs(r7)     // Catch:{ all -> 0x02f3 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x02f3 }
            r5.mo19043a(r6, r7)     // Catch:{ all -> 0x02f3 }
        L_0x00de:
            com.google.android.gms.measurement.internal.d r5 = r17.mo19229e()     // Catch:{ all -> 0x02f3 }
            java.lang.String r5 = r5.mo18876w()     // Catch:{ all -> 0x02f3 }
            boolean r6 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x02f3 }
            r7 = -1
            if (r6 != 0) goto L_0x02cb
            long r10 = r1.f5532x     // Catch:{ all -> 0x02f3 }
            int r6 = (r10 > r7 ? 1 : (r10 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x00fe
            com.google.android.gms.measurement.internal.d r6 = r17.mo19229e()     // Catch:{ all -> 0x02f3 }
            long r6 = r6.mo18877x()     // Catch:{ all -> 0x02f3 }
            r1.f5532x = r6     // Catch:{ all -> 0x02f3 }
        L_0x00fe:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r7 = com.google.android.gms.measurement.internal.C3135o.f5446g     // Catch:{ all -> 0x02f3 }
            int r6 = r6.mo19147b(r5, r7)     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.la r7 = r7.mo19097m()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r8 = com.google.android.gms.measurement.internal.C3135o.f5448h     // Catch:{ all -> 0x02f3 }
            int r7 = r7.mo19147b(r5, r8)     // Catch:{ all -> 0x02f3 }
            int r7 = java.lang.Math.max(r2, r7)     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.d r8 = r17.mo19229e()     // Catch:{ all -> 0x02f3 }
            java.util.List r6 = r8.mo18846a(r5, r6, r7)     // Catch:{ all -> 0x02f3 }
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x02f3 }
            if (r7 != 0) goto L_0x02ed
            java.util.Iterator r7 = r6.iterator()     // Catch:{ all -> 0x02f3 }
        L_0x012c:
            boolean r8 = r7.hasNext()     // Catch:{ all -> 0x02f3 }
            if (r8 == 0) goto L_0x014b
            java.lang.Object r8 = r7.next()     // Catch:{ all -> 0x02f3 }
            android.util.Pair r8 = (android.util.Pair) r8     // Catch:{ all -> 0x02f3 }
            java.lang.Object r8 = r8.first     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.w0 r8 = (com.google.android.gms.internal.measurement.C2763w0) r8     // Catch:{ all -> 0x02f3 }
            java.lang.String r10 = r8.mo18057s()     // Catch:{ all -> 0x02f3 }
            boolean r10 = android.text.TextUtils.isEmpty(r10)     // Catch:{ all -> 0x02f3 }
            if (r10 != 0) goto L_0x012c
            java.lang.String r7 = r8.mo18057s()     // Catch:{ all -> 0x02f3 }
            goto L_0x014c
        L_0x014b:
            r7 = r9
        L_0x014c:
            if (r7 == 0) goto L_0x017b
            r8 = 0
        L_0x014f:
            int r10 = r6.size()     // Catch:{ all -> 0x02f3 }
            if (r8 >= r10) goto L_0x017b
            java.lang.Object r10 = r6.get(r8)     // Catch:{ all -> 0x02f3 }
            android.util.Pair r10 = (android.util.Pair) r10     // Catch:{ all -> 0x02f3 }
            java.lang.Object r10 = r10.first     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.w0 r10 = (com.google.android.gms.internal.measurement.C2763w0) r10     // Catch:{ all -> 0x02f3 }
            java.lang.String r11 = r10.mo18057s()     // Catch:{ all -> 0x02f3 }
            boolean r11 = android.text.TextUtils.isEmpty(r11)     // Catch:{ all -> 0x02f3 }
            if (r11 != 0) goto L_0x0178
            java.lang.String r10 = r10.mo18057s()     // Catch:{ all -> 0x02f3 }
            boolean r10 = r10.equals(r7)     // Catch:{ all -> 0x02f3 }
            if (r10 != 0) goto L_0x0178
            java.util.List r6 = r6.subList(r2, r8)     // Catch:{ all -> 0x02f3 }
            goto L_0x017b
        L_0x0178:
            int r8 = r8 + 1
            goto L_0x014f
        L_0x017b:
            com.google.android.gms.internal.measurement.v0$a r7 = com.google.android.gms.internal.measurement.C2748v0.m7389o()     // Catch:{ all -> 0x02f3 }
            int r8 = r6.size()     // Catch:{ all -> 0x02f3 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ all -> 0x02f3 }
            int r11 = r6.size()     // Catch:{ all -> 0x02f3 }
            r10.<init>(r11)     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.la r11 = r11.mo19097m()     // Catch:{ all -> 0x02f3 }
            boolean r11 = r11.mo19151d(r5)     // Catch:{ all -> 0x02f3 }
            r12 = 0
        L_0x0197:
            if (r12 >= r8) goto L_0x01fe
            java.lang.Object r13 = r6.get(r12)     // Catch:{ all -> 0x02f3 }
            android.util.Pair r13 = (android.util.Pair) r13     // Catch:{ all -> 0x02f3 }
            java.lang.Object r13 = r13.first     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.w0 r13 = (com.google.android.gms.internal.measurement.C2763w0) r13     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.l4$a r13 = r13.mo17669j()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.w0$a r13 = (com.google.android.gms.internal.measurement.C2763w0.C2764a) r13     // Catch:{ all -> 0x02f3 }
            java.lang.Object r14 = r6.get(r12)     // Catch:{ all -> 0x02f3 }
            android.util.Pair r14 = (android.util.Pair) r14     // Catch:{ all -> 0x02f3 }
            java.lang.Object r14 = r14.second     // Catch:{ all -> 0x02f3 }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x02f3 }
            r10.add(r14)     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.j5 r14 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.la r14 = r14.mo19097m()     // Catch:{ all -> 0x02f3 }
            long r14 = r14.mo19157i()     // Catch:{ all -> 0x02f3 }
            r13.mo18094g(r14)     // Catch:{ all -> 0x02f3 }
            r13.mo18068a(r3)     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.j5 r14 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            r14.mo19018r()     // Catch:{ all -> 0x02f3 }
            r13.mo18079b(r2)     // Catch:{ all -> 0x02f3 }
            if (r11 != 0) goto L_0x01d3
            r13.mo18125x()     // Catch:{ all -> 0x02f3 }
        L_0x01d3:
            com.google.android.gms.measurement.internal.j5 r14 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.la r14 = r14.mo19097m()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.C3135o.f5445f0     // Catch:{ all -> 0x02f3 }
            boolean r14 = r14.mo19154e(r5, r15)     // Catch:{ all -> 0x02f3 }
            if (r14 == 0) goto L_0x01f8
            com.google.android.gms.internal.measurement.u5 r14 = r13.mo17679i()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.l4 r14 = (com.google.android.gms.internal.measurement.C2595l4) r14     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.w0 r14 = (com.google.android.gms.internal.measurement.C2763w0) r14     // Catch:{ all -> 0x02f3 }
            byte[] r14 = r14.mo18129f()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.v9 r15 = r17.mo19232h()     // Catch:{ all -> 0x02f3 }
            long r14 = r15.mo19351a(r14)     // Catch:{ all -> 0x02f3 }
            r13.mo18109l(r14)     // Catch:{ all -> 0x02f3 }
        L_0x01f8:
            r7.mo17949a(r13)     // Catch:{ all -> 0x02f3 }
            int r12 = r12 + 1
            goto L_0x0197
        L_0x01fe:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r6 = r6.mo19015l()     // Catch:{ all -> 0x02f3 }
            r11 = 2
            boolean r6 = r6.mo19000a(r11)     // Catch:{ all -> 0x02f3 }
            if (r6 == 0) goto L_0x021c
            com.google.android.gms.measurement.internal.v9 r6 = r17.mo19232h()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.u5 r11 = r7.mo17679i()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.l4 r11 = (com.google.android.gms.internal.measurement.C2595l4) r11     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.v0 r11 = (com.google.android.gms.internal.measurement.C2748v0) r11     // Catch:{ all -> 0x02f3 }
            java.lang.String r6 = r6.mo19355a(r11)     // Catch:{ all -> 0x02f3 }
            goto L_0x021d
        L_0x021c:
            r6 = r9
        L_0x021d:
            r17.mo19232h()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.u5 r11 = r7.mo17679i()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.l4 r11 = (com.google.android.gms.internal.measurement.C2595l4) r11     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.internal.measurement.v0 r11 = (com.google.android.gms.internal.measurement.C2748v0) r11     // Catch:{ all -> 0x02f3 }
            byte[] r14 = r11.mo18129f()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.x3<java.lang.String> r11 = com.google.android.gms.measurement.internal.C3135o.f5466q     // Catch:{ all -> 0x02f3 }
            java.lang.Object r9 = r11.mo19389a(r9)     // Catch:{ all -> 0x02f3 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x02f3 }
            java.net.URL r13 = new java.net.URL     // Catch:{ MalformedURLException -> 0x02b7 }
            r13.<init>(r9)     // Catch:{ MalformedURLException -> 0x02b7 }
            boolean r11 = r10.isEmpty()     // Catch:{ MalformedURLException -> 0x02b7 }
            if (r11 != 0) goto L_0x0241
            r11 = 1
            goto L_0x0242
        L_0x0241:
            r11 = 0
        L_0x0242:
            com.google.android.gms.common.internal.C2258v.m5636a(r11)     // Catch:{ MalformedURLException -> 0x02b7 }
            java.util.List<java.lang.Long> r11 = r1.f5530v     // Catch:{ MalformedURLException -> 0x02b7 }
            if (r11 == 0) goto L_0x0259
            com.google.android.gms.measurement.internal.j5 r10 = r1.f5517i     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.f4 r10 = r10.mo19015l()     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.h4 r10 = r10.mo19001t()     // Catch:{ MalformedURLException -> 0x02b7 }
            java.lang.String r11 = "Set uploading progress before finishing the previous upload"
            r10.mo19042a(r11)     // Catch:{ MalformedURLException -> 0x02b7 }
            goto L_0x0260
        L_0x0259:
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ MalformedURLException -> 0x02b7 }
            r11.<init>(r10)     // Catch:{ MalformedURLException -> 0x02b7 }
            r1.f5530v = r11     // Catch:{ MalformedURLException -> 0x02b7 }
        L_0x0260:
            com.google.android.gms.measurement.internal.j5 r10 = r1.f5517i     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.s4 r10 = r10.mo19098p()     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.t4 r10 = r10.f5608f     // Catch:{ MalformedURLException -> 0x02b7 }
            r10.mo19327a(r3)     // Catch:{ MalformedURLException -> 0x02b7 }
            java.lang.String r3 = "?"
            if (r8 <= 0) goto L_0x0277
            com.google.android.gms.internal.measurement.w0 r3 = r7.mo17950a(r2)     // Catch:{ MalformedURLException -> 0x02b7 }
            java.lang.String r3 = r3.mo18052p0()     // Catch:{ MalformedURLException -> 0x02b7 }
        L_0x0277:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo18996B()     // Catch:{ MalformedURLException -> 0x02b7 }
            java.lang.String r7 = "Uploading data. app, uncompressed size, data"
            int r8 = r14.length     // Catch:{ MalformedURLException -> 0x02b7 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ MalformedURLException -> 0x02b7 }
            r4.mo19045a(r7, r3, r8, r6)     // Catch:{ MalformedURLException -> 0x02b7 }
            r1.f5526r = r0     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.j4 r11 = r17.mo19227d()     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.q9 r0 = new com.google.android.gms.measurement.internal.q9     // Catch:{ MalformedURLException -> 0x02b7 }
            r0.<init>(r1, r5)     // Catch:{ MalformedURLException -> 0x02b7 }
            r11.mo18881c()     // Catch:{ MalformedURLException -> 0x02b7 }
            r11.mo19284q()     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.common.internal.C2258v.m5629a(r13)     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.common.internal.C2258v.m5629a(r14)     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.common.internal.C2258v.m5629a(r0)     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.g5 r3 = r11.mo19014j()     // Catch:{ MalformedURLException -> 0x02b7 }
            com.google.android.gms.measurement.internal.n4 r4 = new com.google.android.gms.measurement.internal.n4     // Catch:{ MalformedURLException -> 0x02b7 }
            r15 = 0
            r10 = r4
            r12 = r5
            r16 = r0
            r10.<init>(r11, r12, r13, r14, r15, r16)     // Catch:{ MalformedURLException -> 0x02b7 }
            r3.mo19030b(r4)     // Catch:{ MalformedURLException -> 0x02b7 }
            goto L_0x02ed
        L_0x02b7:
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ all -> 0x02f3 }
            java.lang.String r3 = "Failed to parse upload URL. Not uploading. appId"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r5)     // Catch:{ all -> 0x02f3 }
            r0.mo19044a(r3, r4, r9)     // Catch:{ all -> 0x02f3 }
            goto L_0x02ed
        L_0x02cb:
            r1.f5532x = r7     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.d r0 = r17.mo19229e()     // Catch:{ all -> 0x02f3 }
            long r5 = com.google.android.gms.measurement.internal.C3110la.m8857w()     // Catch:{ all -> 0x02f3 }
            long r3 = r3 - r5
            java.lang.String r0 = r0.mo18844a(r3)     // Catch:{ all -> 0x02f3 }
            boolean r3 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x02f3 }
            if (r3 != 0) goto L_0x02ed
            com.google.android.gms.measurement.internal.d r3 = r17.mo19229e()     // Catch:{ all -> 0x02f3 }
            com.google.android.gms.measurement.internal.e5 r0 = r3.mo18858b(r0)     // Catch:{ all -> 0x02f3 }
            if (r0 == 0) goto L_0x02ed
            r1.m9054a(r0)     // Catch:{ all -> 0x02f3 }
        L_0x02ed:
            r1.f5527s = r2
            r17.m9065u()
            return
        L_0x02f3:
            r0 = move-exception
            r1.f5527s = r2
            r17.m9065u()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.mo19236p():void");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: q */
    public final void mo19237q() {
        m9070z();
        mo19235m();
        if (!this.f5520l) {
            this.f5520l = true;
            m9070z();
            mo19235m();
            if ((this.f5517i.mo19097m().mo19146a(C3135o.f5439c0) || m9067w()) && m9066v()) {
                int a = m9045a(this.f5529u);
                int F = this.f5517i.mo19081H().mo18804F();
                m9070z();
                if (a > F) {
                    this.f5517i.mo19015l().mo19001t().mo19044a("Panic: can't downgrade version. Previous, current version", Integer.valueOf(a), Integer.valueOf(F));
                } else if (a < F) {
                    if (m9057a(F, this.f5529u)) {
                        this.f5517i.mo19015l().mo18996B().mo19044a("Storage version upgraded. Previous, current version", Integer.valueOf(a), Integer.valueOf(F));
                    } else {
                        this.f5517i.mo19015l().mo19001t().mo19044a("Storage version upgrade failed. Previous, current version", Integer.valueOf(a), Integer.valueOf(F));
                    }
                }
            }
        }
        if (!this.f5519k && !this.f5517i.mo19097m().mo19146a(C3135o.f5439c0)) {
            this.f5517i.mo19015l().mo19007z().mo19042a("This instance being marked as an uploader");
            this.f5519k = true;
            m9044C();
        }
    }

    /* renamed from: r */
    public final C3098ka mo19018r() {
        return this.f5517i.mo19018r();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: s */
    public final void mo19238s() {
        this.f5524p++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: t */
    public final C3081j5 mo19239t() {
        return this.f5517i;
    }

    private C3145o9(C3212u9 u9Var, C3081j5 j5Var) {
        this.f5518j = false;
        C2258v.m5629a(u9Var);
        this.f5517i = C3081j5.m8751a(u9Var.f5689a, (zzv) null);
        this.f5532x = -1;
        C3223v9 v9Var = new C3223v9(this);
        v9Var.mo19285s();
        this.f5515g = v9Var;
        C3080j4 j4Var = new C3080j4(this);
        j4Var.mo19285s();
        this.f5510b = j4Var;
        C3009d5 d5Var = new C3009d5(this);
        d5Var.mo19285s();
        this.f5509a = d5Var;
        this.f5517i.mo19014j().mo19028a(new C3179r9(this, u9Var));
    }

    /* renamed from: b */
    private static void m9062b(C3157p9 p9Var) {
        if (p9Var == null) {
            throw new IllegalStateException("Upload Component not created");
        } else if (!p9Var.mo19283p()) {
            String valueOf = String.valueOf(p9Var.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    /* renamed from: e */
    private final boolean m9064e(zzm zzm) {
        return (!C2774wa.m7753b() || !this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5395G0)) ? !TextUtils.isEmpty(zzm.f5815Q) || !TextUtils.isEmpty(zzm.f5831g0) : !TextUtils.isEmpty(zzm.f5815Q) || !TextUtils.isEmpty(zzm.f5835k0) || !TextUtils.isEmpty(zzm.f5831g0);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: c */
    public final C3021e5 mo19226c(zzm zzm) {
        m9070z();
        mo19235m();
        C2258v.m5629a(zzm);
        C2258v.m5639b(zzm.f5814P);
        C3021e5 b = mo19229e().mo18858b(zzm.f5814P);
        String b2 = this.f5517i.mo19098p().mo19310b(zzm.f5814P);
        if (!C2805y9.m7884b() || !this.f5517i.mo19097m().mo19146a(C3135o.f5411O0)) {
            return m9046a(zzm, b, b2);
        }
        if (b == null) {
            b = new C3021e5(this.f5517i, zzm.f5814P);
            b.mo18933a(this.f5517i.mo19104w().mo19456w());
            b.mo18950e(b2);
        } else if (!b2.equals(b.mo18977q())) {
            b.mo18950e(b2);
            b.mo18933a(this.f5517i.mo19104w().mo19456w());
        }
        b.mo18939b(zzm.f5815Q);
        b.mo18943c(zzm.f5831g0);
        if (C2774wa.m7753b() && this.f5517i.mo19097m().mo19154e(b.mo18967l(), C3135o.f5395G0)) {
            b.mo18947d(zzm.f5835k0);
        }
        if (!TextUtils.isEmpty(zzm.f5824Z)) {
            b.mo18953f(zzm.f5824Z);
        }
        long j = zzm.f5818T;
        if (j != 0) {
            b.mo18946d(j);
        }
        if (!TextUtils.isEmpty(zzm.f5816R)) {
            b.mo18955g(zzm.f5816R);
        }
        b.mo18942c(zzm.f5823Y);
        String str = zzm.f5817S;
        if (str != null) {
            b.mo18958h(str);
        }
        b.mo18949e(zzm.f5819U);
        b.mo18935a(zzm.f5821W);
        if (!TextUtils.isEmpty(zzm.f5820V)) {
            b.mo18962i(zzm.f5820V);
        }
        b.mo18976p(zzm.f5825a0);
        b.mo18940b(zzm.f5828d0);
        b.mo18944c(zzm.f5829e0);
        if (this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5435a0)) {
            b.mo18932a(zzm.f5832h0);
        }
        b.mo18952f(zzm.f5833i0);
        if (b.mo18936a()) {
            mo19229e().mo18850a(b);
        }
        return b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final String mo19228d(zzm zzm) {
        try {
            return (String) this.f5517i.mo19014j().mo19027a(new C3190s9(this, zzm)).get(30000, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            this.f5517i.mo19015l().mo19001t().mo19044a("Failed to get app instance id. appId", C3032f4.m8621a(zzm.f5814P), e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
     arg types: [android.os.Bundle, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0897 A[Catch:{ SQLiteException -> 0x0238, all -> 0x0922 }] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0270 A[Catch:{ SQLiteException -> 0x0238, all -> 0x0922 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x02a7 A[Catch:{ SQLiteException -> 0x0238, all -> 0x0922 }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x02f5 A[Catch:{ SQLiteException -> 0x0238, all -> 0x0922 }] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x0322  */
    @androidx.annotation.WorkerThread
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m9063b(com.google.android.gms.measurement.internal.zzan r28, com.google.android.gms.measurement.internal.zzm r29) {
        /*
            r27 = this;
            r1 = r27
            r2 = r28
            r3 = r29
            java.lang.String r4 = "_s"
            com.google.android.gms.common.internal.C2258v.m5629a(r29)
            java.lang.String r5 = r3.f5814P
            com.google.android.gms.common.internal.C2258v.m5639b(r5)
            long r5 = java.lang.System.nanoTime()
            r27.m9070z()
            r27.mo19235m()
            java.lang.String r15 = r3.f5814P
            com.google.android.gms.measurement.internal.v9 r7 = r27.mo19232h()
            boolean r7 = r7.mo19360a(r2, r3)
            if (r7 != 0) goto L_0x0027
            return
        L_0x0027:
            boolean r7 = r3.f5821W
            if (r7 != 0) goto L_0x002f
            r1.mo19226c(r3)
            return
        L_0x002f:
            com.google.android.gms.measurement.internal.d5 r7 = r27.mo19225c()
            java.lang.String r8 = r2.f5803P
            boolean r7 = r7.mo18893b(r15, r8)
            java.lang.String r14 = "_err"
            r13 = 0
            r12 = 1
            if (r7 == 0) goto L_0x00db
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r3 = r3.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19004w()
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r15)
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i
            com.google.android.gms.measurement.internal.c4 r5 = r5.mo19105x()
            java.lang.String r6 = r2.f5803P
            java.lang.String r5 = r5.mo18822a(r6)
            java.lang.String r6 = "Dropping blacklisted event. appId"
            r3.mo19044a(r6, r4, r5)
            com.google.android.gms.measurement.internal.d5 r3 = r27.mo19225c()
            boolean r3 = r3.mo18900g(r15)
            if (r3 != 0) goto L_0x0075
            com.google.android.gms.measurement.internal.d5 r3 = r27.mo19225c()
            boolean r3 = r3.mo18901h(r15)
            if (r3 == 0) goto L_0x0073
            goto L_0x0075
        L_0x0073:
            r3 = 0
            goto L_0x0076
        L_0x0075:
            r3 = 1
        L_0x0076:
            if (r3 != 0) goto L_0x0091
            java.lang.String r4 = r2.f5803P
            boolean r4 = r14.equals(r4)
            if (r4 != 0) goto L_0x0091
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i
            com.google.android.gms.measurement.internal.z9 r7 = r4.mo19104w()
            r9 = 11
            java.lang.String r11 = r2.f5803P
            r12 = 0
            java.lang.String r10 = "_ev"
            r8 = r15
            r7.mo19441a(r8, r9, r10, r11, r12)
        L_0x0091:
            if (r3 == 0) goto L_0x00da
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            com.google.android.gms.measurement.internal.e5 r2 = r2.mo18858b(r15)
            if (r2 == 0) goto L_0x00da
            long r3 = r2.mo18925D()
            long r5 = r2.mo18924C()
            long r3 = java.lang.Math.max(r3, r5)
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i
            com.google.android.gms.common.util.e r5 = r5.mo19017o()
            long r5 = r5.mo17132a()
            long r5 = r5 - r3
            long r3 = java.lang.Math.abs(r5)
            com.google.android.gms.measurement.internal.x3<java.lang.Long> r5 = com.google.android.gms.measurement.internal.C3135o.f5484z
            java.lang.Object r5 = r5.mo19389a(r13)
            java.lang.Long r5 = (java.lang.Long) r5
            long r5 = r5.longValue()
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00da
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r3 = r3.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo18995A()
            java.lang.String r4 = "Fetching config for blacklisted app"
            r3.mo19042a(r4)
            r1.m9054a(r2)
        L_0x00da:
            return
        L_0x00db:
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()
            r10 = 2
            boolean r7 = r7.mo19000a(r10)
            if (r7 == 0) goto L_0x0101
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo18996B()
            com.google.android.gms.measurement.internal.j5 r8 = r1.f5517i
            com.google.android.gms.measurement.internal.c4 r8 = r8.mo19105x()
            java.lang.String r8 = r8.mo18821a(r2)
            java.lang.String r9 = "Logging event"
            r7.mo19043a(r9, r8)
        L_0x0101:
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()
            r7.mo18878y()
            r1.mo19226c(r3)     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = "_iap"
            java.lang.String r8 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r7 = r7.equals(r8)     // Catch:{ all -> 0x0922 }
            java.lang.String r8 = "ecommerce_purchase"
            if (r7 != 0) goto L_0x0125
            java.lang.String r7 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r7 = r8.equals(r7)     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x0120
            goto L_0x0125
        L_0x0120:
            r23 = r5
            r6 = 0
            goto L_0x02b6
        L_0x0125:
            com.google.android.gms.measurement.internal.zzam r7 = r2.f5804Q     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = "currency"
            java.lang.String r7 = r7.mo19463e(r9)     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r8 = r8.equals(r9)     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = "value"
            if (r8 == 0) goto L_0x018a
            com.google.android.gms.measurement.internal.zzam r8 = r2.f5804Q     // Catch:{ all -> 0x0922 }
            java.lang.Double r8 = r8.mo19461d(r9)     // Catch:{ all -> 0x0922 }
            double r16 = r8.doubleValue()     // Catch:{ all -> 0x0922 }
            r18 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r16 = r16 * r18
            r20 = 0
            int r8 = (r16 > r20 ? 1 : (r16 == r20 ? 0 : -1))
            if (r8 != 0) goto L_0x015c
            com.google.android.gms.measurement.internal.zzam r8 = r2.f5804Q     // Catch:{ all -> 0x0922 }
            java.lang.Long r8 = r8.mo19460c(r9)     // Catch:{ all -> 0x0922 }
            long r8 = r8.longValue()     // Catch:{ all -> 0x0922 }
            double r8 = (double) r8     // Catch:{ all -> 0x0922 }
            double r16 = r8 * r18
        L_0x015c:
            r8 = 4890909195324358656(0x43e0000000000000, double:9.223372036854776E18)
            int r18 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r18 > 0) goto L_0x016d
            r8 = -4332462841530417152(0xc3e0000000000000, double:-9.223372036854776E18)
            int r18 = (r16 > r8 ? 1 : (r16 == r8 ? 0 : -1))
            if (r18 < 0) goto L_0x016d
            long r8 = java.lang.Math.round(r16)     // Catch:{ all -> 0x0922 }
            goto L_0x0194
        L_0x016d:
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19004w()     // Catch:{ all -> 0x0922 }
            java.lang.String r8 = "Data lost. Currency value is too big. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r15)     // Catch:{ all -> 0x0922 }
            java.lang.Double r10 = java.lang.Double.valueOf(r16)     // Catch:{ all -> 0x0922 }
            r7.mo19044a(r8, r9, r10)     // Catch:{ all -> 0x0922 }
            r23 = r5
            r6 = 0
            r11 = 0
            goto L_0x02a5
        L_0x018a:
            com.google.android.gms.measurement.internal.zzam r8 = r2.f5804Q     // Catch:{ all -> 0x0922 }
            java.lang.Long r8 = r8.mo19460c(r9)     // Catch:{ all -> 0x0922 }
            long r8 = r8.longValue()     // Catch:{ all -> 0x0922 }
        L_0x0194:
            boolean r10 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x0922 }
            if (r10 != 0) goto L_0x02a1
            java.util.Locale r10 = java.util.Locale.US     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r7.toUpperCase(r10)     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = "[A-Z]{3}"
            boolean r10 = r7.matches(r10)     // Catch:{ all -> 0x0922 }
            if (r10 == 0) goto L_0x02a1
            java.lang.String r10 = "_ltv_"
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ all -> 0x0922 }
            int r16 = r7.length()     // Catch:{ all -> 0x0922 }
            if (r16 == 0) goto L_0x01b9
            java.lang.String r7 = r10.concat(r7)     // Catch:{ all -> 0x0922 }
            goto L_0x01be
        L_0x01b9:
            java.lang.String r7 = new java.lang.String     // Catch:{ all -> 0x0922 }
            r7.<init>(r10)     // Catch:{ all -> 0x0922 }
        L_0x01be:
            r10 = r7
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.w9 r7 = r7.mo18863c(r15, r10)     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x01ff
            java.lang.Object r11 = r7.f5737e     // Catch:{ all -> 0x0922 }
            boolean r11 = r11 instanceof java.lang.Long     // Catch:{ all -> 0x0922 }
            if (r11 != 0) goto L_0x01d0
            goto L_0x01ff
        L_0x01d0:
            java.lang.Object r7 = r7.f5737e     // Catch:{ all -> 0x0922 }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0922 }
            long r19 = r7.longValue()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.w9 r17 = new com.google.android.gms.measurement.internal.w9     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = r2.f5805R     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.common.util.e r7 = r7.mo19017o()     // Catch:{ all -> 0x0922 }
            long r21 = r7.mo17132a()     // Catch:{ all -> 0x0922 }
            long r19 = r19 + r8
            java.lang.Long r19 = java.lang.Long.valueOf(r19)     // Catch:{ all -> 0x0922 }
            r7 = r17
            r8 = r15
            r9 = r11
            r11 = 2
            r23 = r5
            r5 = 1
            r6 = 0
            r11 = r21
            r13 = r19
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x0922 }
            r5 = r17
            goto L_0x0266
        L_0x01ff:
            r23 = r5
            r5 = 1
            r6 = 0
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r11 = r11.mo19097m()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r12 = com.google.android.gms.measurement.internal.C3135o.f5390E     // Catch:{ all -> 0x0922 }
            int r11 = r11.mo19147b(r15, r12)     // Catch:{ all -> 0x0922 }
            int r11 = r11 - r5
            com.google.android.gms.common.internal.C2258v.m5639b(r15)     // Catch:{ all -> 0x0922 }
            r7.mo18881c()     // Catch:{ all -> 0x0922 }
            r7.mo19284q()     // Catch:{ all -> 0x0922 }
            android.database.sqlite.SQLiteDatabase r12 = r7.mo18875v()     // Catch:{ SQLiteException -> 0x0238 }
            java.lang.String r13 = "delete from user_attributes where app_id=? and name in (select name from user_attributes where app_id=? and name like '_ltv_%' order by set_timestamp desc limit ?,10);"
            r5 = 3
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0238 }
            r5[r6] = r15     // Catch:{ SQLiteException -> 0x0238 }
            r16 = 1
            r5[r16] = r15     // Catch:{ SQLiteException -> 0x0238 }
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ SQLiteException -> 0x0238 }
            r16 = 2
            r5[r16] = r11     // Catch:{ SQLiteException -> 0x0238 }
            r12.execSQL(r13, r5)     // Catch:{ SQLiteException -> 0x0238 }
            goto L_0x024b
        L_0x0238:
            r0 = move-exception
            r5 = r0
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = "Error pruning currencies. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r15)     // Catch:{ all -> 0x0922 }
            r7.mo19044a(r11, r12, r5)     // Catch:{ all -> 0x0922 }
        L_0x024b:
            com.google.android.gms.measurement.internal.w9 r5 = new com.google.android.gms.measurement.internal.w9     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = r2.f5805R     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.common.util.e r7 = r7.mo19017o()     // Catch:{ all -> 0x0922 }
            long r12 = r7.mo17132a()     // Catch:{ all -> 0x0922 }
            java.lang.Long r16 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x0922 }
            r7 = r5
            r8 = r15
            r9 = r11
            r11 = r12
            r13 = r16
            r7.<init>(r8, r9, r10, r11, r13)     // Catch:{ all -> 0x0922 }
        L_0x0266:
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            boolean r7 = r7.mo18855a(r5)     // Catch:{ all -> 0x0922 }
            if (r7 != 0) goto L_0x02a4
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r8 = "Too many unique user properties are set. Ignoring user property. appId"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r15)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r10 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.c4 r10 = r10.mo19105x()     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = r5.f5735c     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = r10.mo18824c(r11)     // Catch:{ all -> 0x0922 }
            java.lang.Object r5 = r5.f5737e     // Catch:{ all -> 0x0922 }
            r7.mo19045a(r8, r9, r10, r5)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r5.mo19104w()     // Catch:{ all -> 0x0922 }
            r9 = 9
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r15
            r7.mo19441a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0922 }
            goto L_0x02a4
        L_0x02a1:
            r23 = r5
            r6 = 0
        L_0x02a4:
            r11 = 1
        L_0x02a5:
            if (r11 != 0) goto L_0x02b6
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r2.mo18874u()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            r2.mo18879z()
            return
        L_0x02b6:
            java.lang.String r5 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r5 = com.google.android.gms.measurement.internal.C3267z9.m9429e(r5)     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r16 = r14.equals(r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            long r8 = r27.m9042A()     // Catch:{ all -> 0x0922 }
            r11 = 1
            r13 = 0
            r17 = 0
            r10 = r15
            r12 = r5
            r14 = r16
            r18 = r15
            r15 = r17
            com.google.android.gms.measurement.internal.c r7 = r7.mo18842a(r8, r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x0922 }
            long r8 = r7.f4995b     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r10 = com.google.android.gms.measurement.internal.C3135o.f5454k     // Catch:{ all -> 0x0922 }
            r14 = 0
            java.lang.Object r10 = r10.mo19389a(r14)     // Catch:{ all -> 0x0922 }
            java.lang.Integer r10 = (java.lang.Integer) r10     // Catch:{ all -> 0x0922 }
            int r10 = r10.intValue()     // Catch:{ all -> 0x0922 }
            long r10 = (long) r10     // Catch:{ all -> 0x0922 }
            long r8 = r8 - r10
            r10 = 1000(0x3e8, double:4.94E-321)
            r12 = 1
            r14 = 0
            int r17 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r17 <= 0) goto L_0x0322
            long r8 = r8 % r10
            int r2 = (r8 > r12 ? 1 : (r8 == r12 ? 0 : -1))
            if (r2 != 0) goto L_0x0313
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r3 = "Data loss. Too many events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r18)     // Catch:{ all -> 0x0922 }
            long r5 = r7.f4995b     // Catch:{ all -> 0x0922 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0922 }
            r2.mo19044a(r3, r4, r5)     // Catch:{ all -> 0x0922 }
        L_0x0313:
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r2.mo18874u()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            r2.mo18879z()
            return
        L_0x0322:
            if (r5 == 0) goto L_0x037a
            long r8 = r7.f4994a     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r6 = com.google.android.gms.measurement.internal.C3135o.f5458m     // Catch:{ all -> 0x0922 }
            r12 = 0
            java.lang.Object r6 = r6.mo19389a(r12)     // Catch:{ all -> 0x0922 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ all -> 0x0922 }
            int r6 = r6.intValue()     // Catch:{ all -> 0x0922 }
            long r12 = (long) r6     // Catch:{ all -> 0x0922 }
            long r8 = r8 - r12
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 <= 0) goto L_0x037a
            long r8 = r8 % r10
            r3 = 1
            int r5 = (r8 > r3 ? 1 : (r8 == r3 ? 0 : -1))
            if (r5 != 0) goto L_0x0359
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r3 = r3.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r4 = "Data loss. Too many public events logged. appId, count"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r18)     // Catch:{ all -> 0x0922 }
            long r6 = r7.f4994a     // Catch:{ all -> 0x0922 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0922 }
            r3.mo19044a(r4, r5, r6)     // Catch:{ all -> 0x0922 }
        L_0x0359:
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r3.mo19104w()     // Catch:{ all -> 0x0922 }
            r9 = 16
            java.lang.String r10 = "_ev"
            java.lang.String r11 = r2.f5803P     // Catch:{ all -> 0x0922 }
            r12 = 0
            r8 = r18
            r7.mo19441a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r2.mo18874u()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            r2.mo18879z()
            return
        L_0x037a:
            if (r16 == 0) goto L_0x03cc
            long r8 = r7.f4997d     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r11 = com.google.android.gms.measurement.internal.C3135o.f5456l     // Catch:{ all -> 0x0922 }
            int r6 = r6.mo19147b(r10, r11)     // Catch:{ all -> 0x0922 }
            r10 = 1000000(0xf4240, float:1.401298E-39)
            int r6 = java.lang.Math.min(r10, r6)     // Catch:{ all -> 0x0922 }
            r12 = 0
            int r6 = java.lang.Math.max(r12, r6)     // Catch:{ all -> 0x0922 }
            long r10 = (long) r6     // Catch:{ all -> 0x0922 }
            long r8 = r8 - r10
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 <= 0) goto L_0x03cd
            r10 = 1
            int r2 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r2 != 0) goto L_0x03bd
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r3 = "Too many error events logged. appId, count"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r18)     // Catch:{ all -> 0x0922 }
            long r5 = r7.f4997d     // Catch:{ all -> 0x0922 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x0922 }
            r2.mo19044a(r3, r4, r5)     // Catch:{ all -> 0x0922 }
        L_0x03bd:
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r2.mo18874u()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            r2.mo18879z()
            return
        L_0x03cc:
            r12 = 0
        L_0x03cd:
            com.google.android.gms.measurement.internal.zzam r6 = r2.f5804Q     // Catch:{ all -> 0x0922 }
            android.os.Bundle r6 = r6.mo19462e()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r7.mo19104w()     // Catch:{ all -> 0x0922 }
            java.lang.String r8 = "_o"
            java.lang.String r9 = r2.f5805R     // Catch:{ all -> 0x0922 }
            r7.mo19433a(r6, r8, r9)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r7.mo19104w()     // Catch:{ all -> 0x0922 }
            r13 = r18
            boolean r7 = r7.mo19452d(r13)     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = "_r"
            if (r7 == 0) goto L_0x040e
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r7.mo19104w()     // Catch:{ all -> 0x0922 }
            java.lang.String r8 = "_dbg"
            r9 = 1
            java.lang.Long r12 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x0922 }
            r7.mo19433a(r6, r8, r12)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r7.mo19104w()     // Catch:{ all -> 0x0922 }
            java.lang.Long r8 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x0922 }
            r7.mo19433a(r6, r11, r8)     // Catch:{ all -> 0x0922 }
        L_0x040e:
            java.lang.String r7 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r7 = r4.equals(r7)     // Catch:{ all -> 0x0922 }
            java.lang.String r8 = "_sno"
            if (r7 == 0) goto L_0x0445
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r7 = r7.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.C3135o.f5414Q     // Catch:{ all -> 0x0922 }
            boolean r7 = r7.mo19154e(r9, r10)     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x0445
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.w9 r7 = r7.mo18863c(r9, r8)     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x0445
            java.lang.Object r9 = r7.f5737e     // Catch:{ all -> 0x0922 }
            boolean r9 = r9 instanceof java.lang.Long     // Catch:{ all -> 0x0922 }
            if (r9 == 0) goto L_0x0445
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r9 = r9.mo19104w()     // Catch:{ all -> 0x0922 }
            java.lang.Object r7 = r7.f5737e     // Catch:{ all -> 0x0922 }
            r9.mo19433a(r6, r8, r7)     // Catch:{ all -> 0x0922 }
        L_0x0445:
            java.lang.String r7 = r2.f5803P     // Catch:{ all -> 0x0922 }
            boolean r4 = r4.equals(r7)     // Catch:{ all -> 0x0922 }
            if (r4 == 0) goto L_0x0477
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.C3135o.f5422U     // Catch:{ all -> 0x0922 }
            boolean r4 = r4.mo19154e(r7, r9)     // Catch:{ all -> 0x0922 }
            if (r4 == 0) goto L_0x0477
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.C3135o.f5414Q     // Catch:{ all -> 0x0922 }
            boolean r4 = r4.mo19154e(r7, r9)     // Catch:{ all -> 0x0922 }
            if (r4 != 0) goto L_0x0477
            com.google.android.gms.measurement.internal.zzkq r4 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x0922 }
            r12 = 0
            r4.<init>(r8, r14, r12)     // Catch:{ all -> 0x0922 }
            r1.mo19221b(r4, r3)     // Catch:{ all -> 0x0922 }
            goto L_0x0478
        L_0x0477:
            r12 = 0
        L_0x0478:
            com.google.android.gms.measurement.internal.d r4 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            long r7 = r4.mo18862c(r13)     // Catch:{ all -> 0x0922 }
            int r4 = (r7 > r14 ? 1 : (r7 == r14 ? 0 : -1))
            if (r4 <= 0) goto L_0x049b
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = "Data lost. Too many events stored on disk, deleted. appId"
            java.lang.Object r10 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ all -> 0x0922 }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0922 }
            r4.mo19044a(r9, r10, r7)     // Catch:{ all -> 0x0922 }
        L_0x049b:
            com.google.android.gms.measurement.internal.l r4 = new com.google.android.gms.measurement.internal.l     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r8 = r1.f5517i     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = r2.f5805R     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = r2.f5803P     // Catch:{ all -> 0x0922 }
            long r14 = r2.f5806S     // Catch:{ all -> 0x0922 }
            r19 = 0
            r7 = r4
            r2 = r10
            r10 = r13
            r26 = r11
            r11 = r2
            r16 = r12
            r2 = r13
            r25 = 0
            r12 = r14
            r28 = r16
            r14 = r19
            r16 = r6
            r7.<init>(r8, r9, r10, r11, r12, r14, r16)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r6 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r4.f5313b     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.k r6 = r6.mo18843a(r2, r7)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x052e
            com.google.android.gms.measurement.internal.d r6 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            long r6 = r6.mo18872h(r2)     // Catch:{ all -> 0x0922 }
            r8 = 500(0x1f4, double:2.47E-321)
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 < 0) goto L_0x0514
            if (r5 == 0) goto L_0x0514
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r3 = r3.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r5 = "Too many event names used, ignoring event. appId, name, supported count"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r2)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19105x()     // Catch:{ all -> 0x0922 }
            java.lang.String r4 = r4.f5313b     // Catch:{ all -> 0x0922 }
            java.lang.String r4 = r7.mo18822a(r4)     // Catch:{ all -> 0x0922 }
            r7 = 500(0x1f4, float:7.0E-43)
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x0922 }
            r3.mo19045a(r5, r6, r4, r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r3.mo19104w()     // Catch:{ all -> 0x0922 }
            r9 = 8
            r10 = 0
            r11 = 0
            r12 = 0
            r8 = r2
            r7.mo19441a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            r2.mo18879z()
            return
        L_0x0514:
            com.google.android.gms.measurement.internal.k r5 = new com.google.android.gms.measurement.internal.k     // Catch:{ all -> 0x0922 }
            java.lang.String r9 = r4.f5313b     // Catch:{ all -> 0x0922 }
            r10 = 0
            r12 = 0
            long r14 = r4.f5315d     // Catch:{ all -> 0x0922 }
            r16 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r7 = r5
            r8 = r2
            r7.<init>(r8, r9, r10, r12, r14, r16, r18, r19, r20, r21)     // Catch:{ all -> 0x0922 }
            goto L_0x053c
        L_0x052e:
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0922 }
            long r7 = r6.f5283f     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.l r4 = r4.mo19133a(r2, r7)     // Catch:{ all -> 0x0922 }
            long r7 = r4.f5315d     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.k r5 = r6.mo19124a(r7)     // Catch:{ all -> 0x0922 }
        L_0x053c:
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r2.mo18851a(r5)     // Catch:{ all -> 0x0922 }
            r27.m9070z()     // Catch:{ all -> 0x0922 }
            r27.mo19235m()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.common.internal.C2258v.m5629a(r4)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.common.internal.C2258v.m5629a(r29)     // Catch:{ all -> 0x0922 }
            java.lang.String r2 = r4.f5312a     // Catch:{ all -> 0x0922 }
            com.google.android.gms.common.internal.C2258v.m5639b(r2)     // Catch:{ all -> 0x0922 }
            java.lang.String r2 = r4.f5312a     // Catch:{ all -> 0x0922 }
            java.lang.String r5 = r3.f5814P     // Catch:{ all -> 0x0922 }
            boolean r2 = r2.equals(r5)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.common.internal.C2258v.m5636a(r2)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.internal.measurement.w0$a r2 = com.google.android.gms.internal.measurement.C2763w0.m7555s0()     // Catch:{ all -> 0x0922 }
            r5 = 1
            r2.mo18065a(r5)     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = "android"
            r2.mo18073a(r6)     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = r3.f5814P     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x0579
            java.lang.String r6 = r3.f5814P     // Catch:{ all -> 0x0922 }
            r2.mo18092f(r6)     // Catch:{ all -> 0x0922 }
        L_0x0579:
            java.lang.String r6 = r3.f5817S     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x0586
            java.lang.String r6 = r3.f5817S     // Catch:{ all -> 0x0922 }
            r2.mo18089e(r6)     // Catch:{ all -> 0x0922 }
        L_0x0586:
            java.lang.String r6 = r3.f5816R     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x0593
            java.lang.String r6 = r3.f5816R     // Catch:{ all -> 0x0922 }
            r2.mo18095g(r6)     // Catch:{ all -> 0x0922 }
        L_0x0593:
            long r6 = r3.f5823Y     // Catch:{ all -> 0x0922 }
            r8 = -2147483648(0xffffffff80000000, double:NaN)
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x05a2
            long r6 = r3.f5823Y     // Catch:{ all -> 0x0922 }
            int r7 = (int) r6     // Catch:{ all -> 0x0922 }
            r2.mo18096h(r7)     // Catch:{ all -> 0x0922 }
        L_0x05a2:
            long r6 = r3.f5818T     // Catch:{ all -> 0x0922 }
            r2.mo18091f(r6)     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = r3.f5815Q     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x05b4
            java.lang.String r6 = r3.f5815Q     // Catch:{ all -> 0x0922 }
            r2.mo18107k(r6)     // Catch:{ all -> 0x0922 }
        L_0x05b4:
            boolean r6 = com.google.android.gms.internal.measurement.C2774wa.m7753b()     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0603
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r8 = com.google.android.gms.measurement.internal.C3135o.f5395G0     // Catch:{ all -> 0x0922 }
            boolean r6 = r6.mo19154e(r7, r8)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0603
            java.lang.String r6 = r2.mo18123v()     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x05e1
            java.lang.String r6 = r3.f5835k0     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x05e1
            java.lang.String r6 = r3.f5835k0     // Catch:{ all -> 0x0922 }
            r2.mo18118p(r6)     // Catch:{ all -> 0x0922 }
        L_0x05e1:
            java.lang.String r6 = r2.mo18123v()     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0636
            java.lang.String r6 = r2.mo18126y()     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0636
            java.lang.String r6 = r3.f5831g0     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x0636
            java.lang.String r6 = r3.f5831g0     // Catch:{ all -> 0x0922 }
            r2.mo18116o(r6)     // Catch:{ all -> 0x0922 }
            goto L_0x0636
        L_0x0603:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5447g0     // Catch:{ all -> 0x0922 }
            boolean r6 = r6.mo19146a(r7)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0629
            java.lang.String r6 = r2.mo18123v()     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0636
            java.lang.String r6 = r3.f5831g0     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x0636
            java.lang.String r6 = r3.f5831g0     // Catch:{ all -> 0x0922 }
            r2.mo18116o(r6)     // Catch:{ all -> 0x0922 }
            goto L_0x0636
        L_0x0629:
            java.lang.String r6 = r3.f5831g0     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x0636
            java.lang.String r6 = r3.f5831g0     // Catch:{ all -> 0x0922 }
            r2.mo18116o(r6)     // Catch:{ all -> 0x0922 }
        L_0x0636:
            long r6 = r3.f5819U     // Catch:{ all -> 0x0922 }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0643
            long r6 = r3.f5819U     // Catch:{ all -> 0x0922 }
            r2.mo18097h(r6)     // Catch:{ all -> 0x0922 }
        L_0x0643:
            long r6 = r3.f5833i0     // Catch:{ all -> 0x0922 }
            r2.mo18106k(r6)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r10 = com.google.android.gms.measurement.internal.C3135o.f5441d0     // Catch:{ all -> 0x0922 }
            boolean r6 = r6.mo19154e(r7, r10)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0665
            com.google.android.gms.measurement.internal.v9 r6 = r27.mo19232h()     // Catch:{ all -> 0x0922 }
            java.util.List r6 = r6.mo19363u()     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0665
            r2.mo18082c(r6)     // Catch:{ all -> 0x0922 }
        L_0x0665:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.s4 r6 = r6.mo19098p()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            android.util.Pair r6 = r6.mo19307a(r7)     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x0698
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x0922 }
            java.lang.CharSequence r7 = (java.lang.CharSequence) r7     // Catch:{ all -> 0x0922 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x0922 }
            if (r7 != 0) goto L_0x0698
            boolean r7 = r3.f5828d0     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x06fa
            java.lang.Object r7 = r6.first     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ all -> 0x0922 }
            r2.mo18098h(r7)     // Catch:{ all -> 0x0922 }
            java.lang.Object r7 = r6.second     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x06fa
            java.lang.Object r6 = r6.second     // Catch:{ all -> 0x0922 }
            java.lang.Boolean r6 = (java.lang.Boolean) r6     // Catch:{ all -> 0x0922 }
            boolean r6 = r6.booleanValue()     // Catch:{ all -> 0x0922 }
            r2.mo18074a(r6)     // Catch:{ all -> 0x0922 }
            goto L_0x06fa
        L_0x0698:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.i r6 = r6.mo19080G()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            android.content.Context r7 = r7.mo19016n()     // Catch:{ all -> 0x0922 }
            boolean r6 = r6.mo19058a(r7)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x06fa
            boolean r6 = r3.f5829e0     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x06fa
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            android.content.Context r6 = r6.mo19016n()     // Catch:{ all -> 0x0922 }
            android.content.ContentResolver r6 = r6.getContentResolver()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = "android_id"
            java.lang.String r6 = android.provider.Settings.Secure.getString(r6, r7)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x06da
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r6 = r6.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19004w()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = "null secure ID. appId"
            java.lang.String r10 = r2.mo18121t()     // Catch:{ all -> 0x0922 }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r10)     // Catch:{ all -> 0x0922 }
            r6.mo19043a(r7, r10)     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = "null"
            goto L_0x06f7
        L_0x06da:
            boolean r7 = r6.isEmpty()     // Catch:{ all -> 0x0922 }
            if (r7 == 0) goto L_0x06f7
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19004w()     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = "empty secure ID. appId"
            java.lang.String r11 = r2.mo18121t()     // Catch:{ all -> 0x0922 }
            java.lang.Object r11 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r11)     // Catch:{ all -> 0x0922 }
            r7.mo19043a(r10, r11)     // Catch:{ all -> 0x0922 }
        L_0x06f7:
            r2.mo18111m(r6)     // Catch:{ all -> 0x0922 }
        L_0x06fa:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.i r6 = r6.mo19080G()     // Catch:{ all -> 0x0922 }
            r6.mo18903k()     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = android.os.Build.MODEL     // Catch:{ all -> 0x0922 }
            r2.mo18083c(r6)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.i r6 = r6.mo19080G()     // Catch:{ all -> 0x0922 }
            r6.mo18903k()     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = android.os.Build.VERSION.RELEASE     // Catch:{ all -> 0x0922 }
            r2.mo18078b(r6)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.i r6 = r6.mo19080G()     // Catch:{ all -> 0x0922 }
            long r6 = r6.mo19059t()     // Catch:{ all -> 0x0922 }
            int r7 = (int) r6     // Catch:{ all -> 0x0922 }
            r2.mo18090f(r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.i r6 = r6.mo19080G()     // Catch:{ all -> 0x0922 }
            java.lang.String r6 = r6.mo19060u()     // Catch:{ all -> 0x0922 }
            r2.mo18086d(r6)     // Catch:{ all -> 0x0922 }
            long r6 = r3.f5825a0     // Catch:{ all -> 0x0922 }
            r2.mo18102j(r6)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            boolean r6 = r6.mo19089c()     // Catch:{ all -> 0x0922 }
            if (r6 == 0) goto L_0x074c
            r2.mo18121t()     // Catch:{ all -> 0x0922 }
            boolean r6 = android.text.TextUtils.isEmpty(r28)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x074c
            r6 = r28
            r2.mo18114n(r6)     // Catch:{ all -> 0x0922 }
        L_0x074c:
            com.google.android.gms.measurement.internal.d r6 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.e5 r6 = r6.mo18858b(r7)     // Catch:{ all -> 0x0922 }
            if (r6 != 0) goto L_0x07bf
            com.google.android.gms.measurement.internal.e5 r6 = new com.google.android.gms.measurement.internal.e5     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = r3.f5814P     // Catch:{ all -> 0x0922 }
            r6.<init>(r7, r10)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.z9 r7 = r7.mo19104w()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r7.mo19456w()     // Catch:{ all -> 0x0922 }
            r6.mo18933a(r7)     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5824Z     // Catch:{ all -> 0x0922 }
            r6.mo18953f(r7)     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5815Q     // Catch:{ all -> 0x0922 }
            r6.mo18939b(r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.s4 r7 = r7.mo19098p()     // Catch:{ all -> 0x0922 }
            java.lang.String r10 = r3.f5814P     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r7.mo19310b(r10)     // Catch:{ all -> 0x0922 }
            r6.mo18950e(r7)     // Catch:{ all -> 0x0922 }
            r6.mo18954g(r8)     // Catch:{ all -> 0x0922 }
            r6.mo18931a(r8)     // Catch:{ all -> 0x0922 }
            r6.mo18938b(r8)     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5816R     // Catch:{ all -> 0x0922 }
            r6.mo18955g(r7)     // Catch:{ all -> 0x0922 }
            long r10 = r3.f5823Y     // Catch:{ all -> 0x0922 }
            r6.mo18942c(r10)     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5817S     // Catch:{ all -> 0x0922 }
            r6.mo18958h(r7)     // Catch:{ all -> 0x0922 }
            long r10 = r3.f5818T     // Catch:{ all -> 0x0922 }
            r6.mo18946d(r10)     // Catch:{ all -> 0x0922 }
            long r10 = r3.f5819U     // Catch:{ all -> 0x0922 }
            r6.mo18949e(r10)     // Catch:{ all -> 0x0922 }
            boolean r7 = r3.f5821W     // Catch:{ all -> 0x0922 }
            r6.mo18935a(r7)     // Catch:{ all -> 0x0922 }
            long r10 = r3.f5825a0     // Catch:{ all -> 0x0922 }
            r6.mo18976p(r10)     // Catch:{ all -> 0x0922 }
            long r10 = r3.f5833i0     // Catch:{ all -> 0x0922 }
            r6.mo18952f(r10)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r7 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r7.mo18850a(r6)     // Catch:{ all -> 0x0922 }
        L_0x07bf:
            java.lang.String r7 = r6.mo18969m()     // Catch:{ all -> 0x0922 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x0922 }
            if (r7 != 0) goto L_0x07d0
            java.lang.String r7 = r6.mo18969m()     // Catch:{ all -> 0x0922 }
            r2.mo18101i(r7)     // Catch:{ all -> 0x0922 }
        L_0x07d0:
            java.lang.String r7 = r6.mo18978r()     // Catch:{ all -> 0x0922 }
            boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ all -> 0x0922 }
            if (r7 != 0) goto L_0x07e1
            java.lang.String r6 = r6.mo18978r()     // Catch:{ all -> 0x0922 }
            r2.mo18110l(r6)     // Catch:{ all -> 0x0922 }
        L_0x07e1:
            com.google.android.gms.measurement.internal.d r6 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = r3.f5814P     // Catch:{ all -> 0x0922 }
            java.util.List r6 = r6.mo18845a(r7)     // Catch:{ all -> 0x0922 }
            r7 = 0
        L_0x07ec:
            int r10 = r6.size()     // Catch:{ all -> 0x0922 }
            if (r7 >= r10) goto L_0x0821
            com.google.android.gms.internal.measurement.a1$a r10 = com.google.android.gms.internal.measurement.C2414a1.m5972x()     // Catch:{ all -> 0x0922 }
            java.lang.Object r11 = r6.get(r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.w9 r11 = (com.google.android.gms.measurement.internal.C3234w9) r11     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = r11.f5735c     // Catch:{ all -> 0x0922 }
            r10.mo17259a(r11)     // Catch:{ all -> 0x0922 }
            java.lang.Object r11 = r6.get(r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.w9 r11 = (com.google.android.gms.measurement.internal.C3234w9) r11     // Catch:{ all -> 0x0922 }
            long r11 = r11.f5736d     // Catch:{ all -> 0x0922 }
            r10.mo17258a(r11)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.v9 r11 = r27.mo19232h()     // Catch:{ all -> 0x0922 }
            java.lang.Object r12 = r6.get(r7)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.w9 r12 = (com.google.android.gms.measurement.internal.C3234w9) r12     // Catch:{ all -> 0x0922 }
            java.lang.Object r12 = r12.f5737e     // Catch:{ all -> 0x0922 }
            r11.mo19357a(r10, r12)     // Catch:{ all -> 0x0922 }
            r2.mo18069a(r10)     // Catch:{ all -> 0x0922 }
            int r7 = r7 + 1
            goto L_0x07ec
        L_0x0821:
            com.google.android.gms.measurement.internal.d r6 = r27.mo19229e()     // Catch:{ IOException -> 0x089a }
            com.google.android.gms.internal.measurement.u5 r7 = r2.mo17679i()     // Catch:{ IOException -> 0x089a }
            com.google.android.gms.internal.measurement.l4 r7 = (com.google.android.gms.internal.measurement.C2595l4) r7     // Catch:{ IOException -> 0x089a }
            com.google.android.gms.internal.measurement.w0 r7 = (com.google.android.gms.internal.measurement.C2763w0) r7     // Catch:{ IOException -> 0x089a }
            long r6 = r6.mo18840a(r7)     // Catch:{ IOException -> 0x089a }
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.zzam r10 = r4.f5317f     // Catch:{ all -> 0x0922 }
            if (r10 == 0) goto L_0x0890
            com.google.android.gms.measurement.internal.zzam r10 = r4.f5317f     // Catch:{ all -> 0x0922 }
            java.util.Iterator r10 = r10.iterator()     // Catch:{ all -> 0x0922 }
        L_0x083f:
            boolean r11 = r10.hasNext()     // Catch:{ all -> 0x0922 }
            if (r11 == 0) goto L_0x0857
            java.lang.Object r11 = r10.next()     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x0922 }
            r12 = r26
            boolean r11 = r12.equals(r11)     // Catch:{ all -> 0x0922 }
            if (r11 == 0) goto L_0x0854
            goto L_0x0891
        L_0x0854:
            r26 = r12
            goto L_0x083f
        L_0x0857:
            com.google.android.gms.measurement.internal.d5 r10 = r27.mo19225c()     // Catch:{ all -> 0x0922 }
            java.lang.String r11 = r4.f5312a     // Catch:{ all -> 0x0922 }
            java.lang.String r12 = r4.f5313b     // Catch:{ all -> 0x0922 }
            boolean r10 = r10.mo18895c(r11, r12)     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.d r11 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            long r12 = r27.m9042A()     // Catch:{ all -> 0x0922 }
            java.lang.String r14 = r4.f5312a     // Catch:{ all -> 0x0922 }
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            com.google.android.gms.measurement.internal.c r11 = r11.mo18842a(r12, r14, r15, r16, r17, r18, r19)     // Catch:{ all -> 0x0922 }
            if (r10 == 0) goto L_0x0890
            long r10 = r11.f4998e     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.j5 r12 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r12 = r12.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r13 = r4.f5312a     // Catch:{ all -> 0x0922 }
            int r12 = r12.mo19142a(r13)     // Catch:{ all -> 0x0922 }
            long r12 = (long) r12     // Catch:{ all -> 0x0922 }
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 >= 0) goto L_0x0890
            goto L_0x0891
        L_0x0890:
            r5 = 0
        L_0x0891:
            boolean r2 = r2.mo18854a(r4, r6, r5)     // Catch:{ all -> 0x0922 }
            if (r2 == 0) goto L_0x08b3
            r1.f5521m = r8     // Catch:{ all -> 0x0922 }
            goto L_0x08b3
        L_0x089a:
            r0 = move-exception
            r5 = r0
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r6 = r6.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19001t()     // Catch:{ all -> 0x0922 }
            java.lang.String r7 = "Data loss. Failed to insert raw event metadata. appId"
            java.lang.String r2 = r2.mo18121t()     // Catch:{ all -> 0x0922 }
            java.lang.Object r2 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r2)     // Catch:{ all -> 0x0922 }
            r6.mo19044a(r7, r2, r5)     // Catch:{ all -> 0x0922 }
        L_0x08b3:
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()     // Catch:{ all -> 0x0922 }
            r2.mo18874u()     // Catch:{ all -> 0x0922 }
            boolean r2 = com.google.android.gms.internal.measurement.C2489ea.m6262b()     // Catch:{ all -> 0x0922 }
            if (r2 == 0) goto L_0x08d0
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.la r2 = r2.mo19097m()     // Catch:{ all -> 0x0922 }
            java.lang.String r3 = r3.f5814P     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.C3135o.f5431Y0     // Catch:{ all -> 0x0922 }
            boolean r2 = r2.mo19154e(r3, r5)     // Catch:{ all -> 0x0922 }
            if (r2 != 0) goto L_0x08f6
        L_0x08d0:
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()     // Catch:{ all -> 0x0922 }
            r3 = 2
            boolean r2 = r2.mo19000a(r3)     // Catch:{ all -> 0x0922 }
            if (r2 == 0) goto L_0x08f6
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()     // Catch:{ all -> 0x0922 }
            java.lang.String r3 = "Event recorded"
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0922 }
            com.google.android.gms.measurement.internal.c4 r5 = r5.mo19105x()     // Catch:{ all -> 0x0922 }
            java.lang.String r4 = r5.mo18820a(r4)     // Catch:{ all -> 0x0922 }
            r2.mo19043a(r3, r4)     // Catch:{ all -> 0x0922 }
        L_0x08f6:
            com.google.android.gms.measurement.internal.d r2 = r27.mo19229e()
            r2.mo18879z()
            r27.m9044C()
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18996B()
            long r3 = java.lang.System.nanoTime()
            long r3 = r3 - r23
            r5 = 500000(0x7a120, double:2.47033E-318)
            long r3 = r3 + r5
            r5 = 1000000(0xf4240, double:4.940656E-318)
            long r3 = r3 / r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            java.lang.String r4 = "Background event processing time, ms"
            r2.mo19043a(r4, r3)
            return
        L_0x0922:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.measurement.internal.d r3 = r27.mo19229e()
            r3.mo18879z()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.m9063b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void");
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: a */
    public final void m9056a(C3212u9 u9Var) {
        this.f5517i.mo19014j().mo18881c();
        C3003d dVar = new C3003d(this);
        dVar.mo19285s();
        this.f5511c = dVar;
        this.f5517i.mo19097m().mo19145a(this.f5509a);
        C3002ca caVar = new C3002ca(this);
        caVar.mo19285s();
        this.f5514f = caVar;
        C3166q7 q7Var = new C3166q7(this);
        q7Var.mo19285s();
        this.f5516h = q7Var;
        C3097k9 k9Var = new C3097k9(this);
        k9Var.mo19285s();
        this.f5513e = k9Var;
        this.f5512d = new C3163q4(this);
        if (this.f5523o != this.f5524p) {
            this.f5517i.mo19015l().mo19001t().mo19044a("Not all upload components initialized", Integer.valueOf(this.f5523o), Integer.valueOf(this.f5524p));
        }
        this.f5518j = true;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19208a() {
        this.f5517i.mo19014j().mo18881c();
        mo19229e().mo18835B();
        if (this.f5517i.mo19098p().f5607e.mo19326a() == 0) {
            this.f5517i.mo19098p().f5607e.mo19327a(this.f5517i.mo19017o().mo17132a());
        }
        m9044C();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void */
    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19212a(zzan zzan, String str) {
        zzan zzan2 = zzan;
        C3021e5 b = mo19229e().mo18858b(str);
        if (b == null || TextUtils.isEmpty(b.mo18981u())) {
            this.f5517i.mo19015l().mo18995A().mo19043a("No app data available; dropping event", str);
            return;
        }
        Boolean b2 = m9060b(b);
        if (b2 == null) {
            if (!"_ui".equals(zzan2.f5803P)) {
                this.f5517i.mo19015l().mo19004w().mo19043a("Could not find package. appId", C3032f4.m8621a(str));
            }
        } else if (!b2.booleanValue()) {
            this.f5517i.mo19015l().mo19001t().mo19043a("App version does not match; dropping event. appId", C3032f4.m8621a(str));
            return;
        }
        zzm zzm = r2;
        zzm zzm2 = new zzm(str, b.mo18971n(), b.mo18981u(), b.mo18982v(), b.mo18983w(), b.mo18984x(), b.mo18985y(), (String) null, b.mo18922A(), false, b.mo18978r(), b.mo18951f(), 0L, 0, b.mo18956g(), b.mo18959h(), false, b.mo18973o(), b.mo18960i(), b.mo18986z(), b.mo18963j(), (!C2774wa.m7753b() || !this.f5517i.mo19097m().mo19154e(b.mo18967l(), C3135o.f5395G0)) ? null : b.mo18975p());
        mo19211a(zzan2, zzm);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0139 A[Catch:{ all -> 0x0384 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0214 A[Catch:{ all -> 0x0384 }] */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo19211a(com.google.android.gms.measurement.internal.zzan r20, com.google.android.gms.measurement.internal.zzm r21) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            r2 = r21
            com.google.android.gms.common.internal.C2258v.m5629a(r21)
            java.lang.String r3 = r2.f5814P
            com.google.android.gms.common.internal.C2258v.m5639b(r3)
            r19.m9070z()
            r19.mo19235m()
            java.lang.String r3 = r2.f5814P
            long r11 = r0.f5806S
            com.google.android.gms.measurement.internal.v9 r4 = r19.mo19232h()
            boolean r4 = r4.mo19360a(r0, r2)
            if (r4 != 0) goto L_0x0023
            return
        L_0x0023:
            boolean r4 = r2.f5821W
            if (r4 != 0) goto L_0x002b
            r1.mo19226c(r2)
            return
        L_0x002b:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.C3135o.f5455k0
            boolean r4 = r4.mo19154e(r3, r5)
            if (r4 == 0) goto L_0x007d
            java.util.List<java.lang.String> r4 = r2.f5834j0
            if (r4 == 0) goto L_0x007d
            java.lang.String r5 = r0.f5803P
            boolean r4 = r4.contains(r5)
            if (r4 == 0) goto L_0x0069
            com.google.android.gms.measurement.internal.zzam r4 = r0.f5804Q
            android.os.Bundle r4 = r4.mo19462e()
            r5 = 1
            java.lang.String r7 = "ga_safelisted"
            r4.putLong(r7, r5)
            com.google.android.gms.measurement.internal.zzan r5 = new com.google.android.gms.measurement.internal.zzan
            java.lang.String r14 = r0.f5803P
            com.google.android.gms.measurement.internal.zzam r15 = new com.google.android.gms.measurement.internal.zzam
            r15.<init>(r4)
            java.lang.String r4 = r0.f5805R
            long r6 = r0.f5806S
            r13 = r5
            r16 = r4
            r17 = r6
            r13.<init>(r14, r15, r16, r17)
            r0 = r5
            goto L_0x007d
        L_0x0069:
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo18995A()
            java.lang.String r4 = r0.f5803P
            java.lang.String r0 = r0.f5805R
            java.lang.String r5 = "Dropping non-safelisted event. appId, event name, origin"
            r2.mo19045a(r5, r3, r4, r0)
            return
        L_0x007d:
            com.google.android.gms.measurement.internal.d r4 = r19.mo19229e()
            r4.mo18878y()
            com.google.android.gms.measurement.internal.d r4 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.common.internal.C2258v.m5639b(r3)     // Catch:{ all -> 0x0384 }
            r4.mo18881c()     // Catch:{ all -> 0x0384 }
            r4.mo19284q()     // Catch:{ all -> 0x0384 }
            r5 = 0
            r7 = 2
            r13 = 0
            r14 = 1
            int r8 = (r11 > r5 ? 1 : (r11 == r5 ? 0 : -1))
            if (r8 >= 0) goto L_0x00b4
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = "Invalid time querying timed out conditional properties"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ all -> 0x0384 }
            java.lang.Long r9 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0384 }
            r4.mo19044a(r5, r6, r9)     // Catch:{ all -> 0x0384 }
            java.util.List r4 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0384 }
            goto L_0x00c4
        L_0x00b4:
            java.lang.String r5 = "active=0 and app_id=? and abs(? - creation_timestamp) > trigger_timeout"
            java.lang.String[] r6 = new java.lang.String[r7]     // Catch:{ all -> 0x0384 }
            r6[r13] = r3     // Catch:{ all -> 0x0384 }
            java.lang.String r9 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0384 }
            r6[r14] = r9     // Catch:{ all -> 0x0384 }
            java.util.List r4 = r4.mo18848a(r5, r6)     // Catch:{ all -> 0x0384 }
        L_0x00c4:
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0384 }
        L_0x00c8:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x0384 }
            if (r5 == 0) goto L_0x0151
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzv r5 = (com.google.android.gms.measurement.internal.zzv) r5     // Catch:{ all -> 0x0384 }
            if (r5 == 0) goto L_0x00c8
            boolean r6 = com.google.android.gms.internal.measurement.C2489ea.m6262b()     // Catch:{ all -> 0x0384 }
            java.lang.String r9 = "User property timed out"
            if (r6 == 0) goto L_0x0112
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x0384 }
            java.lang.String r10 = r2.f5814P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.C3135o.f5431Y0     // Catch:{ all -> 0x0384 }
            boolean r6 = r6.mo19154e(r10, r15)     // Catch:{ all -> 0x0384 }
            if (r6 == 0) goto L_0x0112
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r6 = r6.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18996B()     // Catch:{ all -> 0x0384 }
            java.lang.String r10 = r5.f5836P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r15 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r15 = r15.mo19105x()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r14 = r5.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r14 = r14.f5808Q     // Catch:{ all -> 0x0384 }
            java.lang.String r14 = r15.mo18824c(r14)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r15 = r5.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.Object r15 = r15.mo19469a()     // Catch:{ all -> 0x0384 }
            r6.mo19045a(r9, r10, r14, r15)     // Catch:{ all -> 0x0384 }
            goto L_0x0135
        L_0x0112:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r6 = r6.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18995A()     // Catch:{ all -> 0x0384 }
            java.lang.String r10 = r5.f5836P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r14 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r14 = r14.mo19105x()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r15 = r5.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r15 = r15.f5808Q     // Catch:{ all -> 0x0384 }
            java.lang.String r14 = r14.mo18824c(r15)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r15 = r5.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.Object r15 = r15.mo19469a()     // Catch:{ all -> 0x0384 }
            r6.mo19045a(r9, r10, r14, r15)     // Catch:{ all -> 0x0384 }
        L_0x0135:
            com.google.android.gms.measurement.internal.zzan r6 = r5.f5842V     // Catch:{ all -> 0x0384 }
            if (r6 == 0) goto L_0x0143
            com.google.android.gms.measurement.internal.zzan r6 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzan r9 = r5.f5842V     // Catch:{ all -> 0x0384 }
            r6.<init>(r9, r11)     // Catch:{ all -> 0x0384 }
            r1.m9063b(r6, r2)     // Catch:{ all -> 0x0384 }
        L_0x0143:
            com.google.android.gms.measurement.internal.d r6 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r5 = r5.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = r5.f5808Q     // Catch:{ all -> 0x0384 }
            r6.mo18866e(r3, r5)     // Catch:{ all -> 0x0384 }
            r14 = 1
            goto L_0x00c8
        L_0x0151:
            com.google.android.gms.measurement.internal.d r4 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.common.internal.C2258v.m5639b(r3)     // Catch:{ all -> 0x0384 }
            r4.mo18881c()     // Catch:{ all -> 0x0384 }
            r4.mo19284q()     // Catch:{ all -> 0x0384 }
            if (r8 >= 0) goto L_0x017a
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = "Invalid time querying expired conditional properties"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ all -> 0x0384 }
            java.lang.Long r9 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0384 }
            r4.mo19044a(r5, r6, r9)     // Catch:{ all -> 0x0384 }
            java.util.List r4 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0384 }
            goto L_0x018b
        L_0x017a:
            java.lang.String r5 = "active<>0 and app_id=? and abs(? - triggered_timestamp) > time_to_live"
            java.lang.String[] r6 = new java.lang.String[r7]     // Catch:{ all -> 0x0384 }
            r6[r13] = r3     // Catch:{ all -> 0x0384 }
            java.lang.String r9 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0384 }
            r10 = 1
            r6[r10] = r9     // Catch:{ all -> 0x0384 }
            java.util.List r4 = r4.mo18848a(r5, r6)     // Catch:{ all -> 0x0384 }
        L_0x018b:
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0384 }
            int r6 = r4.size()     // Catch:{ all -> 0x0384 }
            r5.<init>(r6)     // Catch:{ all -> 0x0384 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0384 }
        L_0x0198:
            boolean r6 = r4.hasNext()     // Catch:{ all -> 0x0384 }
            if (r6 == 0) goto L_0x0227
            java.lang.Object r6 = r4.next()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzv r6 = (com.google.android.gms.measurement.internal.zzv) r6     // Catch:{ all -> 0x0384 }
            if (r6 == 0) goto L_0x0198
            boolean r9 = com.google.android.gms.internal.measurement.C2489ea.m6262b()     // Catch:{ all -> 0x0384 }
            java.lang.String r10 = "User property expired"
            if (r9 == 0) goto L_0x01e2
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.la r9 = r9.mo19097m()     // Catch:{ all -> 0x0384 }
            java.lang.String r14 = r2.f5814P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.C3135o.f5431Y0     // Catch:{ all -> 0x0384 }
            boolean r9 = r9.mo19154e(r14, r15)     // Catch:{ all -> 0x0384 }
            if (r9 == 0) goto L_0x01e2
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo18996B()     // Catch:{ all -> 0x0384 }
            java.lang.String r14 = r6.f5836P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r15 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r15 = r15.mo19105x()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r7 = r6.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = r7.f5808Q     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = r15.mo18824c(r7)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r15 = r6.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.Object r15 = r15.mo19469a()     // Catch:{ all -> 0x0384 }
            r9.mo19045a(r10, r14, r7, r15)     // Catch:{ all -> 0x0384 }
            goto L_0x0205
        L_0x01e2:
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo18995A()     // Catch:{ all -> 0x0384 }
            java.lang.String r9 = r6.f5836P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r14 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r14 = r14.mo19105x()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r15 = r6.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r15 = r15.f5808Q     // Catch:{ all -> 0x0384 }
            java.lang.String r14 = r14.mo18824c(r15)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r15 = r6.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.Object r15 = r15.mo19469a()     // Catch:{ all -> 0x0384 }
            r7.mo19045a(r10, r9, r14, r15)     // Catch:{ all -> 0x0384 }
        L_0x0205:
            com.google.android.gms.measurement.internal.d r7 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r9 = r6.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r9 = r9.f5808Q     // Catch:{ all -> 0x0384 }
            r7.mo18860b(r3, r9)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzan r7 = r6.f5846Z     // Catch:{ all -> 0x0384 }
            if (r7 == 0) goto L_0x0219
            com.google.android.gms.measurement.internal.zzan r7 = r6.f5846Z     // Catch:{ all -> 0x0384 }
            r5.add(r7)     // Catch:{ all -> 0x0384 }
        L_0x0219:
            com.google.android.gms.measurement.internal.d r7 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzkq r6 = r6.f5838R     // Catch:{ all -> 0x0384 }
            java.lang.String r6 = r6.f5808Q     // Catch:{ all -> 0x0384 }
            r7.mo18866e(r3, r6)     // Catch:{ all -> 0x0384 }
            r7 = 2
            goto L_0x0198
        L_0x0227:
            int r4 = r5.size()     // Catch:{ all -> 0x0384 }
            r6 = 0
        L_0x022c:
            if (r6 >= r4) goto L_0x023f
            java.lang.Object r7 = r5.get(r6)     // Catch:{ all -> 0x0384 }
            int r6 = r6 + 1
            com.google.android.gms.measurement.internal.zzan r7 = (com.google.android.gms.measurement.internal.zzan) r7     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzan r9 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x0384 }
            r9.<init>(r7, r11)     // Catch:{ all -> 0x0384 }
            r1.m9063b(r9, r2)     // Catch:{ all -> 0x0384 }
            goto L_0x022c
        L_0x023f:
            com.google.android.gms.measurement.internal.d r4 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = r0.f5803P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.common.internal.C2258v.m5639b(r3)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.common.internal.C2258v.m5639b(r5)     // Catch:{ all -> 0x0384 }
            r4.mo18881c()     // Catch:{ all -> 0x0384 }
            r4.mo19284q()     // Catch:{ all -> 0x0384 }
            if (r8 >= 0) goto L_0x0275
            com.google.android.gms.measurement.internal.f4 r6 = r4.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19004w()     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = "Invalid time querying triggered conditional properties"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r4 = r4.mo19010e()     // Catch:{ all -> 0x0384 }
            java.lang.String r4 = r4.mo18822a(r5)     // Catch:{ all -> 0x0384 }
            java.lang.Long r5 = java.lang.Long.valueOf(r11)     // Catch:{ all -> 0x0384 }
            r6.mo19045a(r7, r3, r4, r5)     // Catch:{ all -> 0x0384 }
            java.util.List r3 = java.util.Collections.emptyList()     // Catch:{ all -> 0x0384 }
            goto L_0x028a
        L_0x0275:
            java.lang.String r6 = "active=0 and app_id=? and trigger_event_name=? and abs(? - creation_timestamp) <= trigger_timeout"
            r7 = 3
            java.lang.String[] r7 = new java.lang.String[r7]     // Catch:{ all -> 0x0384 }
            r7[r13] = r3     // Catch:{ all -> 0x0384 }
            r3 = 1
            r7[r3] = r5     // Catch:{ all -> 0x0384 }
            java.lang.String r3 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x0384 }
            r5 = 2
            r7[r5] = r3     // Catch:{ all -> 0x0384 }
            java.util.List r3 = r4.mo18848a(r6, r7)     // Catch:{ all -> 0x0384 }
        L_0x028a:
            java.util.ArrayList r14 = new java.util.ArrayList     // Catch:{ all -> 0x0384 }
            int r4 = r3.size()     // Catch:{ all -> 0x0384 }
            r14.<init>(r4)     // Catch:{ all -> 0x0384 }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x0384 }
        L_0x0297:
            boolean r4 = r3.hasNext()     // Catch:{ all -> 0x0384 }
            if (r4 == 0) goto L_0x035a
            java.lang.Object r4 = r3.next()     // Catch:{ all -> 0x0384 }
            r15 = r4
            com.google.android.gms.measurement.internal.zzv r15 = (com.google.android.gms.measurement.internal.zzv) r15     // Catch:{ all -> 0x0384 }
            if (r15 == 0) goto L_0x0297
            com.google.android.gms.measurement.internal.zzkq r4 = r15.f5838R     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.w9 r10 = new com.google.android.gms.measurement.internal.w9     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = r15.f5836P     // Catch:{ all -> 0x0384 }
            java.lang.String r6 = r15.f5837Q     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = r4.f5808Q     // Catch:{ all -> 0x0384 }
            java.lang.Object r16 = r4.mo19469a()     // Catch:{ all -> 0x0384 }
            r4 = r10
            r8 = r11
            r13 = r10
            r10 = r16
            r4.<init>(r5, r6, r7, r8, r10)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.d r4 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            boolean r4 = r4.mo18855a(r13)     // Catch:{ all -> 0x0384 }
            if (r4 == 0) goto L_0x031a
            boolean r4 = com.google.android.gms.internal.measurement.C2489ea.m6262b()     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = "User property triggered"
            if (r4 == 0) goto L_0x02fc
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0384 }
            java.lang.String r6 = r2.f5814P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5431Y0     // Catch:{ all -> 0x0384 }
            boolean r4 = r4.mo19154e(r6, r7)     // Catch:{ all -> 0x0384 }
            if (r4 == 0) goto L_0x02fc
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo18996B()     // Catch:{ all -> 0x0384 }
            java.lang.String r6 = r15.f5836P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19105x()     // Catch:{ all -> 0x0384 }
            java.lang.String r8 = r13.f5735c     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = r7.mo18824c(r8)     // Catch:{ all -> 0x0384 }
            java.lang.Object r8 = r13.f5737e     // Catch:{ all -> 0x0384 }
            r4.mo19045a(r5, r6, r7, r8)     // Catch:{ all -> 0x0384 }
            goto L_0x033d
        L_0x02fc:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo18995A()     // Catch:{ all -> 0x0384 }
            java.lang.String r6 = r15.f5836P     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19105x()     // Catch:{ all -> 0x0384 }
            java.lang.String r8 = r13.f5735c     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = r7.mo18824c(r8)     // Catch:{ all -> 0x0384 }
            java.lang.Object r8 = r13.f5737e     // Catch:{ all -> 0x0384 }
            r4.mo19045a(r5, r6, r7, r8)     // Catch:{ all -> 0x0384 }
            goto L_0x033d
        L_0x031a:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ all -> 0x0384 }
            java.lang.String r5 = "Too many active user properties, ignoring"
            java.lang.String r6 = r15.f5836P     // Catch:{ all -> 0x0384 }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r6)     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19105x()     // Catch:{ all -> 0x0384 }
            java.lang.String r8 = r13.f5735c     // Catch:{ all -> 0x0384 }
            java.lang.String r7 = r7.mo18824c(r8)     // Catch:{ all -> 0x0384 }
            java.lang.Object r8 = r13.f5737e     // Catch:{ all -> 0x0384 }
            r4.mo19045a(r5, r6, r7, r8)     // Catch:{ all -> 0x0384 }
        L_0x033d:
            com.google.android.gms.measurement.internal.zzan r4 = r15.f5844X     // Catch:{ all -> 0x0384 }
            if (r4 == 0) goto L_0x0346
            com.google.android.gms.measurement.internal.zzan r4 = r15.f5844X     // Catch:{ all -> 0x0384 }
            r14.add(r4)     // Catch:{ all -> 0x0384 }
        L_0x0346:
            com.google.android.gms.measurement.internal.zzkq r4 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x0384 }
            r4.<init>(r13)     // Catch:{ all -> 0x0384 }
            r15.f5838R = r4     // Catch:{ all -> 0x0384 }
            r4 = 1
            r15.f5840T = r4     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.d r5 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            r5.mo18856a(r15)     // Catch:{ all -> 0x0384 }
            r13 = 0
            goto L_0x0297
        L_0x035a:
            r1.m9063b(r0, r2)     // Catch:{ all -> 0x0384 }
            int r0 = r14.size()     // Catch:{ all -> 0x0384 }
            r3 = 0
        L_0x0362:
            if (r3 >= r0) goto L_0x0375
            java.lang.Object r4 = r14.get(r3)     // Catch:{ all -> 0x0384 }
            int r3 = r3 + 1
            com.google.android.gms.measurement.internal.zzan r4 = (com.google.android.gms.measurement.internal.zzan) r4     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.zzan r5 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x0384 }
            r5.<init>(r4, r11)     // Catch:{ all -> 0x0384 }
            r1.m9063b(r5, r2)     // Catch:{ all -> 0x0384 }
            goto L_0x0362
        L_0x0375:
            com.google.android.gms.measurement.internal.d r0 = r19.mo19229e()     // Catch:{ all -> 0x0384 }
            r0.mo18874u()     // Catch:{ all -> 0x0384 }
            com.google.android.gms.measurement.internal.d r0 = r19.mo19229e()
            r0.mo18879z()
            return
        L_0x0384:
            r0 = move-exception
            com.google.android.gms.measurement.internal.d r2 = r19.mo19229e()
            r2.mo18879z()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.mo19211a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o9.a(com.google.android.gms.internal.measurement.w0$a, long, boolean):void
     arg types: [com.google.android.gms.internal.measurement.w0$a, long, int]
     candidates:
      com.google.android.gms.measurement.internal.o9.a(com.google.android.gms.measurement.internal.zzm, com.google.android.gms.measurement.internal.e5, java.lang.String):com.google.android.gms.measurement.internal.e5
      com.google.android.gms.measurement.internal.o9.a(com.google.android.gms.internal.measurement.s0$a, int, java.lang.String):void
      com.google.android.gms.measurement.internal.o9.a(com.google.android.gms.internal.measurement.w0$a, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.v9.a(com.google.android.gms.internal.measurement.s0$a, java.lang.String, java.lang.Object):void
     arg types: [com.google.android.gms.internal.measurement.s0$a, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.v9.a(boolean, boolean, boolean):java.lang.String
      com.google.android.gms.measurement.internal.v9.a(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.d0):void
      com.google.android.gms.measurement.internal.v9.a(java.lang.StringBuilder, int, java.util.List<com.google.android.gms.internal.measurement.u0>):void
      com.google.android.gms.measurement.internal.v9.a(com.google.android.gms.internal.measurement.s0$a, java.lang.String, java.lang.Object):void */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0040, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0041, code lost:
        r2 = r0;
        r22 = r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0046, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0047, code lost:
        r6 = null;
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0098, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0099, code lost:
        r6 = r3;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0040 A[ExcHandler: all (r0v20 'th' java.lang.Throwable A[CUSTOM_DECLARE]), PHI: r3 10  PHI: (r3v46 android.database.Cursor) = (r3v41 android.database.Cursor), (r3v41 android.database.Cursor), (r3v41 android.database.Cursor), (r3v49 android.database.Cursor), (r3v49 android.database.Cursor), (r3v49 android.database.Cursor), (r3v49 android.database.Cursor), (r3v1 android.database.Cursor), (r3v1 android.database.Cursor) binds: [B:45:0x00d8, B:51:0x00e5, B:52:?, B:23:0x007b, B:29:0x0088, B:31:0x008c, B:32:?, B:9:0x0031, B:10:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:9:0x0031] */
    /* JADX WARNING: Removed duplicated region for block: B:150:0x028b A[SYNTHETIC, Splitter:B:150:0x028b] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x0292 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x02a0 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:203:0x03d4 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:210:0x03df A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x03e0 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x05bf A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:295:0x069c A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:330:0x079f A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:336:0x07b5 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:337:0x07cf A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:470:0x0b8e A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:471:0x0ba1 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:473:0x0ba4 A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:474:0x0bcb A[Catch:{ IOException -> 0x0238, all -> 0x0f7f }] */
    /* JADX WARNING: Removed duplicated region for block: B:588:0x0f65 A[SYNTHETIC, Splitter:B:588:0x0f65] */
    /* JADX WARNING: Removed duplicated region for block: B:595:0x0f7b A[SYNTHETIC, Splitter:B:595:0x0f7b] */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m9059a(java.lang.String r64, long r65) {
        /*
            r63 = this;
            r1 = r63
            com.google.android.gms.measurement.internal.d r2 = r63.mo19229e()
            r2.mo18878y()
            com.google.android.gms.measurement.internal.o9$a r2 = new com.google.android.gms.measurement.internal.o9$a     // Catch:{ all -> 0x0f7f }
            r3 = 0
            r2.<init>(r1, r3)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d r4 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            long r5 = r1.f5532x     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.common.internal.C2258v.m5629a(r2)     // Catch:{ all -> 0x0f7f }
            r4.mo18881c()     // Catch:{ all -> 0x0f7f }
            r4.mo19284q()     // Catch:{ all -> 0x0f7f }
            r8 = -1
            r10 = 2
            r11 = 0
            r12 = 1
            android.database.sqlite.SQLiteDatabase r15 = r4.mo18875v()     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            boolean r13 = android.text.TextUtils.isEmpty(r3)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            if (r13 == 0) goto L_0x009b
            int r13 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r13 == 0) goto L_0x004c
            java.lang.String[] r14 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0046, all -> 0x0040 }
            java.lang.String r16 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0040 }
            r14[r11] = r16     // Catch:{ SQLiteException -> 0x0046, all -> 0x0040 }
            java.lang.String r16 = java.lang.String.valueOf(r65)     // Catch:{ SQLiteException -> 0x0046, all -> 0x0040 }
            r14[r12] = r16     // Catch:{ SQLiteException -> 0x0046, all -> 0x0040 }
            goto L_0x0054
        L_0x0040:
            r0 = move-exception
            r2 = r0
            r22 = r3
            goto L_0x0f79
        L_0x0046:
            r0 = move-exception
            r6 = r3
            r7 = r6
        L_0x0049:
            r3 = r0
            goto L_0x0278
        L_0x004c:
            java.lang.String[] r14 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r16 = java.lang.String.valueOf(r65)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r14[r11] = r16     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
        L_0x0054:
            if (r13 == 0) goto L_0x0059
            java.lang.String r13 = "rowid <= ? and "
            goto L_0x005b
        L_0x0059:
            java.lang.String r13 = ""
        L_0x005b:
            int r7 = r13.length()     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            int r7 = r7 + 148
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r3.<init>(r7)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r7 = "select app_id, metadata_fingerprint from raw_events where "
            r3.append(r7)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r3.append(r13)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r7 = "app_id in (select app_id from apps where config_fetched_time >= ?) order by rowid limit 1;"
            r3.append(r7)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r3 = r3.toString()     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            android.database.Cursor r3 = r15.rawQuery(r3, r14)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            boolean r7 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0269, all -> 0x0040 }
            if (r7 != 0) goto L_0x0088
            if (r3 == 0) goto L_0x028e
            r3.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x0088:
            java.lang.String r7 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0269, all -> 0x0040 }
            java.lang.String r13 = r3.getString(r12)     // Catch:{ SQLiteException -> 0x0098, all -> 0x0040 }
            r3.close()     // Catch:{ SQLiteException -> 0x0098, all -> 0x0040 }
            r23 = r3
            r3 = r7
            r7 = r13
            goto L_0x00f0
        L_0x0098:
            r0 = move-exception
            r6 = r3
            goto L_0x0049
        L_0x009b:
            int r3 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r3 == 0) goto L_0x00ac
            java.lang.String[] r7 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r13 = 0
            r7[r11] = r13     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r13 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r7[r12] = r13     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r13 = r7
            goto L_0x00b1
        L_0x00ac:
            r7 = 0
            java.lang.String[] r13 = new java.lang.String[]{r7}     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
        L_0x00b1:
            if (r3 == 0) goto L_0x00b6
            java.lang.String r3 = " and rowid <= ?"
            goto L_0x00b8
        L_0x00b6:
            java.lang.String r3 = ""
        L_0x00b8:
            int r7 = r3.length()     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            int r7 = r7 + 84
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r14.<init>(r7)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r7 = "select metadata_fingerprint from raw_events where app_id = ?"
            r14.append(r7)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            r14.append(r3)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r3 = " order by rowid limit 1;"
            r14.append(r3)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            java.lang.String r3 = r14.toString()     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            android.database.Cursor r3 = r15.rawQuery(r3, r13)     // Catch:{ SQLiteException -> 0x0274, all -> 0x026e }
            boolean r7 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x0269, all -> 0x0040 }
            if (r7 != 0) goto L_0x00e5
            if (r3 == 0) goto L_0x028e
            r3.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x00e5:
            java.lang.String r13 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x0269, all -> 0x0040 }
            r3.close()     // Catch:{ SQLiteException -> 0x0269, all -> 0x0040 }
            r23 = r3
            r7 = r13
            r3 = 0
        L_0x00f0:
            java.lang.String r14 = "raw_events_metadata"
            java.lang.String r13 = "metadata"
            java.lang.String[] r16 = new java.lang.String[]{r13}     // Catch:{ SQLiteException -> 0x0263, all -> 0x025d }
            java.lang.String r17 = "app_id = ? and metadata_fingerprint = ?"
            java.lang.String[] r13 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0263, all -> 0x025d }
            r13[r11] = r3     // Catch:{ SQLiteException -> 0x0263, all -> 0x025d }
            r13[r12] = r7     // Catch:{ SQLiteException -> 0x0263, all -> 0x025d }
            r18 = 0
            r19 = 0
            java.lang.String r20 = "rowid"
            java.lang.String r21 = "2"
            r24 = r13
            r13 = r15
            r25 = r15
            r15 = r16
            r16 = r17
            r17 = r24
            android.database.Cursor r15 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x0263, all -> 0x025d }
            boolean r13 = r15.moveToFirst()     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            if (r13 != 0) goto L_0x0140
            com.google.android.gms.measurement.internal.f4 r5 = r4.mo19015l()     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19001t()     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            java.lang.String r6 = "Raw event metadata record is missing. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            r5.mo19043a(r6, r7)     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            if (r15 == 0) goto L_0x028e
            r15.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x0135:
            r0 = move-exception
            r2 = r0
            r22 = r15
            goto L_0x0f79
        L_0x013b:
            r0 = move-exception
            r7 = r3
            r6 = r15
            goto L_0x0049
        L_0x0140:
            byte[] r13 = r15.getBlob(r11)     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            com.google.android.gms.internal.measurement.w0$a r14 = com.google.android.gms.internal.measurement.C2763w0.m7555s0()     // Catch:{ IOException -> 0x0238 }
            com.google.android.gms.measurement.internal.C3223v9.m9262a(r14, r13)     // Catch:{ IOException -> 0x0238 }
            com.google.android.gms.internal.measurement.w0$a r14 = (com.google.android.gms.internal.measurement.C2763w0.C2764a) r14     // Catch:{ IOException -> 0x0238 }
            com.google.android.gms.internal.measurement.u5 r13 = r14.mo17679i()     // Catch:{ IOException -> 0x0238 }
            com.google.android.gms.internal.measurement.l4 r13 = (com.google.android.gms.internal.measurement.C2595l4) r13     // Catch:{ IOException -> 0x0238 }
            com.google.android.gms.internal.measurement.w0 r13 = (com.google.android.gms.internal.measurement.C2763w0) r13     // Catch:{ IOException -> 0x0238 }
            boolean r14 = r15.moveToNext()     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            if (r14 == 0) goto L_0x016c
            com.google.android.gms.measurement.internal.f4 r14 = r4.mo19015l()     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            com.google.android.gms.measurement.internal.h4 r14 = r14.mo19004w()     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            java.lang.String r10 = "Get multiple raw event metadata records, expected one. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            r14.mo19043a(r10, r12)     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
        L_0x016c:
            r15.close()     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            r2.mo18993a(r13)     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            int r10 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x018c
            java.lang.String r10 = "app_id = ? and metadata_fingerprint = ? and rowid <= ?"
            r12 = 3
            java.lang.String[] r13 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            r13[r11] = r3     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            r12 = 1
            r13[r12] = r7     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            r6 = 2
            r13[r6] = r5     // Catch:{ SQLiteException -> 0x013b, all -> 0x0135 }
            r16 = r10
            r17 = r13
            goto L_0x019a
        L_0x018c:
            java.lang.String r5 = "app_id = ? and metadata_fingerprint = ?"
            r6 = 2
            java.lang.String[] r10 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            r10[r11] = r3     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            r6 = 1
            r10[r6] = r7     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            r16 = r5
            r17 = r10
        L_0x019a:
            java.lang.String r14 = "raw_events"
            java.lang.String r5 = "rowid"
            java.lang.String r6 = "name"
            java.lang.String r7 = "timestamp"
            java.lang.String r10 = "data"
            java.lang.String[] r5 = new java.lang.String[]{r5, r6, r7, r10}     // Catch:{ SQLiteException -> 0x0258, all -> 0x0254 }
            r18 = 0
            r19 = 0
            java.lang.String r20 = "rowid"
            r21 = 0
            r13 = r25
            r6 = r15
            r15 = r5
            android.database.Cursor r5 = r13.query(r14, r15, r16, r17, r18, r19, r20, r21)     // Catch:{ SQLiteException -> 0x0252 }
            boolean r6 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            if (r6 != 0) goto L_0x01d6
            com.google.android.gms.measurement.internal.f4 r6 = r4.mo19015l()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo19004w()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            java.lang.String r7 = "Raw event data disappeared while in transaction. appId"
            java.lang.Object r10 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            r6.mo19043a(r7, r10)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            if (r5 == 0) goto L_0x028e
            r5.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x01d6:
            long r6 = r5.getLong(r11)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            r10 = 3
            byte[] r12 = r5.getBlob(r10)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            com.google.android.gms.internal.measurement.s0$a r10 = com.google.android.gms.internal.measurement.C2701s0.m7158x()     // Catch:{ IOException -> 0x020d }
            com.google.android.gms.measurement.internal.C3223v9.m9262a(r10, r12)     // Catch:{ IOException -> 0x020d }
            com.google.android.gms.internal.measurement.s0$a r10 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r10     // Catch:{ IOException -> 0x020d }
            r12 = 1
            java.lang.String r13 = r5.getString(r12)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            r10.mo17862a(r13)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            r12 = 2
            long r13 = r5.getLong(r12)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            r10.mo17858a(r13)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            com.google.android.gms.internal.measurement.u5 r10 = r10.mo17679i()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            com.google.android.gms.internal.measurement.l4 r10 = (com.google.android.gms.internal.measurement.C2595l4) r10     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            com.google.android.gms.internal.measurement.s0 r10 = (com.google.android.gms.internal.measurement.C2701s0) r10     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            boolean r6 = r2.mo18994a(r6, r10)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            if (r6 != 0) goto L_0x0220
            if (r5 == 0) goto L_0x028e
            r5.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x020d:
            r0 = move-exception
            r6 = r0
            com.google.android.gms.measurement.internal.f4 r7 = r4.mo19015l()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            java.lang.String r10 = "Data loss. Failed to merge raw event. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            r7.mo19044a(r10, r12, r6)     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
        L_0x0220:
            boolean r6 = r5.moveToNext()     // Catch:{ SQLiteException -> 0x0233, all -> 0x022d }
            if (r6 != 0) goto L_0x01d6
            if (r5 == 0) goto L_0x028e
            r5.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x022d:
            r0 = move-exception
            r2 = r0
            r22 = r5
            goto L_0x0f79
        L_0x0233:
            r0 = move-exception
            r7 = r3
            r6 = r5
            goto L_0x0049
        L_0x0238:
            r0 = move-exception
            r6 = r15
            r5 = r0
            com.google.android.gms.measurement.internal.f4 r7 = r4.mo19015l()     // Catch:{ SQLiteException -> 0x0252 }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()     // Catch:{ SQLiteException -> 0x0252 }
            java.lang.String r10 = "Data loss. Failed to merge raw event metadata. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)     // Catch:{ SQLiteException -> 0x0252 }
            r7.mo19044a(r10, r12, r5)     // Catch:{ SQLiteException -> 0x0252 }
            if (r6 == 0) goto L_0x028e
            r6.close()     // Catch:{ all -> 0x0f7f }
            goto L_0x028e
        L_0x0252:
            r0 = move-exception
            goto L_0x025a
        L_0x0254:
            r0 = move-exception
            r6 = r15
            goto L_0x0f76
        L_0x0258:
            r0 = move-exception
            r6 = r15
        L_0x025a:
            r7 = r3
            goto L_0x0049
        L_0x025d:
            r0 = move-exception
            r2 = r0
            r22 = r23
            goto L_0x0f79
        L_0x0263:
            r0 = move-exception
            r7 = r3
            r6 = r23
            goto L_0x0049
        L_0x0269:
            r0 = move-exception
            r6 = r3
            r7 = 0
            goto L_0x0049
        L_0x026e:
            r0 = move-exception
            r2 = r0
            r22 = 0
            goto L_0x0f79
        L_0x0274:
            r0 = move-exception
            r3 = r0
            r6 = 0
            r7 = 0
        L_0x0278:
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f75 }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ all -> 0x0f75 }
            java.lang.String r5 = "Data loss. Error selecting raw event. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x0f75 }
            r4.mo19044a(r5, r7, r3)     // Catch:{ all -> 0x0f75 }
            if (r6 == 0) goto L_0x028e
            r6.close()     // Catch:{ all -> 0x0f7f }
        L_0x028e:
            java.util.List<com.google.android.gms.internal.measurement.s0> r3 = r2.f5535c     // Catch:{ all -> 0x0f7f }
            if (r3 == 0) goto L_0x029d
            java.util.List<com.google.android.gms.internal.measurement.s0> r3 = r2.f5535c     // Catch:{ all -> 0x0f7f }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x0f7f }
            if (r3 == 0) goto L_0x029b
            goto L_0x029d
        L_0x029b:
            r3 = 0
            goto L_0x029e
        L_0x029d:
            r3 = 1
        L_0x029e:
            if (r3 != 0) goto L_0x0f65
            com.google.android.gms.internal.measurement.w0 r3 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r3 = r3.mo17669j()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0$a r3 = (com.google.android.gms.internal.measurement.C2763w0.C2764a) r3     // Catch:{ all -> 0x0f7f }
            r3.mo18108l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r5 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r5.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5432Z     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r5, r6)     // Catch:{ all -> 0x0f7f }
            r7 = 0
            r8 = -1
            r9 = -1
            r12 = 0
            r13 = 0
            r15 = 0
            r18 = 0
            r19 = 0
        L_0x02c8:
            java.util.List<com.google.android.gms.internal.measurement.s0> r11 = r2.f5535c     // Catch:{ all -> 0x0f7f }
            int r11 = r11.size()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "_e"
            java.lang.String r6 = "_et"
            r21 = r12
            r26 = r13
            if (r7 >= r11) goto L_0x0823
            java.util.List<com.google.android.gms.internal.measurement.s0> r11 = r2.f5535c     // Catch:{ all -> 0x0f7f }
            java.lang.Object r11 = r11.get(r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r11 = (com.google.android.gms.internal.measurement.C2701s0) r11     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r11 = r11.mo17669j()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0$a r11 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r11     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d5 r14 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r10 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = r10.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.String r12 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r10 = r14.mo18893b(r10, r12)     // Catch:{ all -> 0x0f7f }
            java.lang.String r12 = "_err"
            if (r10 == 0) goto L_0x037b
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Dropping blacklisted raw event. appId"
            com.google.android.gms.internal.measurement.w0 r10 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = r10.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r10 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r10)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r13 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.c4 r13 = r13.mo19105x()     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r13 = r13.mo18822a(r14)     // Catch:{ all -> 0x0f7f }
            r5.mo19044a(r6, r10, r13)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d5 r5 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r6 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r6.mo18052p0()     // Catch:{ all -> 0x0f7f }
            boolean r5 = r5.mo18900g(r6)     // Catch:{ all -> 0x0f7f }
            if (r5 != 0) goto L_0x0346
            com.google.android.gms.measurement.internal.d5 r5 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r6 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r6.mo18052p0()     // Catch:{ all -> 0x0f7f }
            boolean r5 = r5.mo18901h(r6)     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0344
            goto L_0x0346
        L_0x0344:
            r5 = 0
            goto L_0x0347
        L_0x0346:
            r5 = 1
        L_0x0347:
            if (r5 != 0) goto L_0x036c
            java.lang.String r5 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r5 = r12.equals(r5)     // Catch:{ all -> 0x0f7f }
            if (r5 != 0) goto L_0x036c
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.z9 r28 = r5.mo19104w()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r5 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r29 = r5.mo18052p0()     // Catch:{ all -> 0x0f7f }
            r30 = 11
            java.lang.String r31 = "_ev"
            java.lang.String r32 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            r33 = 0
            r28.mo19441a(r29, r30, r31, r32, r33)     // Catch:{ all -> 0x0f7f }
        L_0x036c:
            r31 = r4
            r12 = r21
            r13 = r26
            r6 = -1
            r10 = 3
            r62 = r9
            r9 = r7
            r7 = r62
            goto L_0x081b
        L_0x037b:
            com.google.android.gms.measurement.internal.d5 r10 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r13 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r13 = r13.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r10 = r10.mo18895c(r13, r14)     // Catch:{ all -> 0x0f7f }
            java.lang.String r13 = "_c"
            if (r10 != 0) goto L_0x03e9
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.common.internal.C2258v.m5639b(r14)     // Catch:{ all -> 0x0f7f }
            r25 = r15
            int r15 = r14.hashCode()     // Catch:{ all -> 0x0f7f }
            r30 = r7
            r7 = 94660(0x171c4, float:1.32647E-40)
            if (r15 == r7) goto L_0x03c7
            r7 = 95025(0x17331, float:1.33158E-40)
            if (r15 == r7) goto L_0x03bd
            r7 = 95027(0x17333, float:1.33161E-40)
            if (r15 == r7) goto L_0x03b3
            goto L_0x03d1
        L_0x03b3:
            java.lang.String r7 = "_ui"
            boolean r7 = r14.equals(r7)     // Catch:{ all -> 0x0f7f }
            if (r7 == 0) goto L_0x03d1
            r7 = 1
            goto L_0x03d2
        L_0x03bd:
            java.lang.String r7 = "_ug"
            boolean r7 = r14.equals(r7)     // Catch:{ all -> 0x0f7f }
            if (r7 == 0) goto L_0x03d1
            r7 = 2
            goto L_0x03d2
        L_0x03c7:
            java.lang.String r7 = "_in"
            boolean r7 = r14.equals(r7)     // Catch:{ all -> 0x0f7f }
            if (r7 == 0) goto L_0x03d1
            r7 = 0
            goto L_0x03d2
        L_0x03d1:
            r7 = -1
        L_0x03d2:
            if (r7 == 0) goto L_0x03dc
            r14 = 1
            if (r7 == r14) goto L_0x03dc
            r14 = 2
            if (r7 == r14) goto L_0x03dc
            r7 = 0
            goto L_0x03dd
        L_0x03dc:
            r7 = 1
        L_0x03dd:
            if (r7 == 0) goto L_0x03e0
            goto L_0x03ed
        L_0x03e0:
            r31 = r4
            r32 = r8
            r33 = r9
            r9 = r6
            goto L_0x05bd
        L_0x03e9:
            r30 = r7
            r25 = r15
        L_0x03ed:
            r31 = r4
            r7 = 0
            r14 = 0
            r15 = 0
        L_0x03f2:
            int r4 = r11.mo17867k()     // Catch:{ all -> 0x0f7f }
            r32 = r8
            java.lang.String r8 = "_r"
            if (r7 >= r4) goto L_0x045a
            com.google.android.gms.internal.measurement.u0 r4 = r11.mo17863a(r7)     // Catch:{ all -> 0x0f7f }
            java.lang.String r4 = r4.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r13.equals(r4)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0428
            com.google.android.gms.internal.measurement.u0 r4 = r11.mo17863a(r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r4 = r4.mo17669j()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0$a r4 = (com.google.android.gms.internal.measurement.C2733u0.C2734a) r4     // Catch:{ all -> 0x0f7f }
            r33 = r9
            r8 = 1
            r4.mo17932a(r8)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r4.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r4 = (com.google.android.gms.internal.measurement.C2733u0) r4     // Catch:{ all -> 0x0f7f }
            r11.mo17857a(r7, r4)     // Catch:{ all -> 0x0f7f }
            r14 = 1
            goto L_0x0453
        L_0x0428:
            r33 = r9
            com.google.android.gms.internal.measurement.u0 r4 = r11.mo17863a(r7)     // Catch:{ all -> 0x0f7f }
            java.lang.String r4 = r4.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r8.equals(r4)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0453
            com.google.android.gms.internal.measurement.u0 r4 = r11.mo17863a(r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r4 = r4.mo17669j()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0$a r4 = (com.google.android.gms.internal.measurement.C2733u0.C2734a) r4     // Catch:{ all -> 0x0f7f }
            r8 = 1
            r4.mo17932a(r8)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r4.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r4 = (com.google.android.gms.internal.measurement.C2733u0) r4     // Catch:{ all -> 0x0f7f }
            r11.mo17857a(r7, r4)     // Catch:{ all -> 0x0f7f }
            r15 = 1
        L_0x0453:
            int r7 = r7 + 1
            r8 = r32
            r9 = r33
            goto L_0x03f2
        L_0x045a:
            r33 = r9
            if (r14 != 0) goto L_0x048e
            if (r10 == 0) goto L_0x048e
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo18996B()     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = "Marking event as conversion"
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.c4 r9 = r9.mo19105x()     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = r9.mo18822a(r14)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r7, r9)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0$a r4 = com.google.android.gms.internal.measurement.C2733u0.m7347y()     // Catch:{ all -> 0x0f7f }
            r4.mo17933a(r13)     // Catch:{ all -> 0x0f7f }
            r9 = r6
            r6 = 1
            r4.mo17932a(r6)     // Catch:{ all -> 0x0f7f }
            r11.mo17859a(r4)     // Catch:{ all -> 0x0f7f }
            goto L_0x048f
        L_0x048e:
            r9 = r6
        L_0x048f:
            if (r15 != 0) goto L_0x04bd
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo18996B()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Marking event as real-time"
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.c4 r7 = r7.mo19105x()     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo18822a(r14)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r6, r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0$a r4 = com.google.android.gms.internal.measurement.C2733u0.m7347y()     // Catch:{ all -> 0x0f7f }
            r4.mo17933a(r8)     // Catch:{ all -> 0x0f7f }
            r6 = 1
            r4.mo17932a(r6)     // Catch:{ all -> 0x0f7f }
            r11.mo17859a(r4)     // Catch:{ all -> 0x0f7f }
        L_0x04bd:
            com.google.android.gms.measurement.internal.d r34 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            long r35 = r63.m9042A()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r4 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r37 = r4.mo18052p0()     // Catch:{ all -> 0x0f7f }
            r38 = 0
            r39 = 0
            r40 = 0
            r41 = 0
            r42 = 1
            com.google.android.gms.measurement.internal.c r4 = r34.mo18842a(r35, r37, r38, r39, r40, r41, r42)     // Catch:{ all -> 0x0f7f }
            long r6 = r4.f4998e     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r14 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r14.mo18052p0()     // Catch:{ all -> 0x0f7f }
            int r4 = r4.mo19142a(r14)     // Catch:{ all -> 0x0f7f }
            long r14 = (long) r4     // Catch:{ all -> 0x0f7f }
            int r4 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r4 <= 0) goto L_0x04f4
            m9051a(r11, r8)     // Catch:{ all -> 0x0f7f }
            goto L_0x04f6
        L_0x04f4:
            r21 = 1
        L_0x04f6:
            java.lang.String r4 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r4 = com.google.android.gms.measurement.internal.C3267z9.m9429e(r4)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x05bd
            if (r10 == 0) goto L_0x05bd
            com.google.android.gms.measurement.internal.d r34 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            long r35 = r63.m9042A()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r4 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r37 = r4.mo18052p0()     // Catch:{ all -> 0x0f7f }
            r38 = 0
            r39 = 0
            r40 = 1
            r41 = 0
            r42 = 0
            com.google.android.gms.measurement.internal.c r4 = r34.mo18842a(r35, r37, r38, r39, r40, r41, r42)     // Catch:{ all -> 0x0f7f }
            long r6 = r4.f4996c     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r8 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r8 = r8.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Integer> r14 = com.google.android.gms.measurement.internal.C3135o.f5460n     // Catch:{ all -> 0x0f7f }
            int r4 = r4.mo19147b(r8, r14)     // Catch:{ all -> 0x0f7f }
            long r14 = (long) r4     // Catch:{ all -> 0x0f7f }
            int r4 = (r6 > r14 ? 1 : (r6 == r14 ? 0 : -1))
            if (r4 <= 0) goto L_0x05bd
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Too many conversions. Not logging as conversion. appId"
            com.google.android.gms.internal.measurement.w0 r7 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r6, r7)     // Catch:{ all -> 0x0f7f }
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = -1
        L_0x0554:
            int r14 = r11.mo17867k()     // Catch:{ all -> 0x0f7f }
            if (r4 >= r14) goto L_0x057e
            com.google.android.gms.internal.measurement.u0 r14 = r11.mo17863a(r4)     // Catch:{ all -> 0x0f7f }
            java.lang.String r15 = r14.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r15 = r13.equals(r15)     // Catch:{ all -> 0x0f7f }
            if (r15 == 0) goto L_0x0570
            com.google.android.gms.internal.measurement.l4$a r7 = r14.mo17669j()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0$a r7 = (com.google.android.gms.internal.measurement.C2733u0.C2734a) r7     // Catch:{ all -> 0x0f7f }
            r8 = r4
            goto L_0x057b
        L_0x0570:
            java.lang.String r14 = r14.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r14 = r12.equals(r14)     // Catch:{ all -> 0x0f7f }
            if (r14 == 0) goto L_0x057b
            r6 = 1
        L_0x057b:
            int r4 = r4 + 1
            goto L_0x0554
        L_0x057e:
            if (r6 == 0) goto L_0x0586
            if (r7 == 0) goto L_0x0586
            r11.mo17864b(r8)     // Catch:{ all -> 0x0f7f }
            goto L_0x05bd
        L_0x0586:
            if (r7 == 0) goto L_0x05a4
            java.lang.Object r4 = r7.clone()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r4 = (com.google.android.gms.internal.measurement.C2595l4.C2596a) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0$a r4 = (com.google.android.gms.internal.measurement.C2733u0.C2734a) r4     // Catch:{ all -> 0x0f7f }
            r4.mo17933a(r12)     // Catch:{ all -> 0x0f7f }
            r6 = 10
            r4.mo17932a(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r4.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r4 = (com.google.android.gms.internal.measurement.C2733u0) r4     // Catch:{ all -> 0x0f7f }
            r11.mo17857a(r8, r4)     // Catch:{ all -> 0x0f7f }
            goto L_0x05bd
        L_0x05a4:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Did not find conversion parameter. appId"
            com.google.android.gms.internal.measurement.w0 r7 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r6, r7)     // Catch:{ all -> 0x0f7f }
        L_0x05bd:
            if (r10 == 0) goto L_0x0686
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x0f7f }
            java.util.List r6 = r11.mo17866j()     // Catch:{ all -> 0x0f7f }
            r4.<init>(r6)     // Catch:{ all -> 0x0f7f }
            r6 = 0
            r7 = -1
            r8 = -1
        L_0x05cb:
            int r10 = r4.size()     // Catch:{ all -> 0x0f7f }
            if (r6 >= r10) goto L_0x05fc
            java.lang.String r10 = "value"
            java.lang.Object r12 = r4.get(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r12 = (com.google.android.gms.internal.measurement.C2733u0) r12     // Catch:{ all -> 0x0f7f }
            java.lang.String r12 = r12.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r10 = r10.equals(r12)     // Catch:{ all -> 0x0f7f }
            if (r10 == 0) goto L_0x05e6
            r7 = r6
            goto L_0x05f9
        L_0x05e6:
            java.lang.String r10 = "currency"
            java.lang.Object r12 = r4.get(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r12 = (com.google.android.gms.internal.measurement.C2733u0) r12     // Catch:{ all -> 0x0f7f }
            java.lang.String r12 = r12.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r10 = r10.equals(r12)     // Catch:{ all -> 0x0f7f }
            if (r10 == 0) goto L_0x05f9
            r8 = r6
        L_0x05f9:
            int r6 = r6 + 1
            goto L_0x05cb
        L_0x05fc:
            r6 = -1
            if (r7 == r6) goto L_0x0687
            java.lang.Object r6 = r4.get(r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r6 = (com.google.android.gms.internal.measurement.C2733u0) r6     // Catch:{ all -> 0x0f7f }
            boolean r6 = r6.mo17925s()     // Catch:{ all -> 0x0f7f }
            if (r6 != 0) goto L_0x0635
            java.lang.Object r6 = r4.get(r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r6 = (com.google.android.gms.internal.measurement.C2733u0) r6     // Catch:{ all -> 0x0f7f }
            boolean r6 = r6.mo17927u()     // Catch:{ all -> 0x0f7f }
            if (r6 != 0) goto L_0x0635
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19006y()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Value must be specified with a numeric type."
            r4.mo19042a(r6)     // Catch:{ all -> 0x0f7f }
            r11.mo17864b(r7)     // Catch:{ all -> 0x0f7f }
            m9051a(r11, r13)     // Catch:{ all -> 0x0f7f }
            r4 = 18
            java.lang.String r6 = "value"
            m9050a(r11, r4, r6)     // Catch:{ all -> 0x0f7f }
            goto L_0x0686
        L_0x0635:
            r6 = -1
            if (r8 != r6) goto L_0x063b
            r4 = 1
            r10 = 3
            goto L_0x0667
        L_0x063b:
            java.lang.Object r4 = r4.get(r8)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r4 = (com.google.android.gms.internal.measurement.C2733u0) r4     // Catch:{ all -> 0x0f7f }
            java.lang.String r4 = r4.mo17924q()     // Catch:{ all -> 0x0f7f }
            int r8 = r4.length()     // Catch:{ all -> 0x0f7f }
            r10 = 3
            if (r8 == r10) goto L_0x064e
        L_0x064c:
            r4 = 1
            goto L_0x0667
        L_0x064e:
            r8 = 0
        L_0x064f:
            int r12 = r4.length()     // Catch:{ all -> 0x0f7f }
            if (r8 >= r12) goto L_0x0666
            int r12 = r4.codePointAt(r8)     // Catch:{ all -> 0x0f7f }
            boolean r14 = java.lang.Character.isLetter(r12)     // Catch:{ all -> 0x0f7f }
            if (r14 != 0) goto L_0x0660
            goto L_0x064c
        L_0x0660:
            int r12 = java.lang.Character.charCount(r12)     // Catch:{ all -> 0x0f7f }
            int r8 = r8 + r12
            goto L_0x064f
        L_0x0666:
            r4 = 0
        L_0x0667:
            if (r4 == 0) goto L_0x0688
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19006y()     // Catch:{ all -> 0x0f7f }
            java.lang.String r8 = "Value parameter discarded. You must also supply a 3-letter ISO_4217 currency code in the currency parameter."
            r4.mo19042a(r8)     // Catch:{ all -> 0x0f7f }
            r11.mo17864b(r7)     // Catch:{ all -> 0x0f7f }
            m9051a(r11, r13)     // Catch:{ all -> 0x0f7f }
            r4 = 19
            java.lang.String r7 = "currency"
            m9050a(r11, r4, r7)     // Catch:{ all -> 0x0f7f }
            goto L_0x0688
        L_0x0686:
            r6 = -1
        L_0x0687:
            r10 = 3
        L_0x0688:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r7 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r8 = com.google.android.gms.measurement.internal.C3135o.f5430Y     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r7, r8)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x079f
            java.lang.String r4 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r5.equals(r4)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x06f7
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r11.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r4 = (com.google.android.gms.internal.measurement.C2701s0) r4     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = "_fr"
            com.google.android.gms.internal.measurement.u0 r4 = com.google.android.gms.measurement.internal.C3223v9.m9261a(r4, r7)     // Catch:{ all -> 0x0f7f }
            if (r4 != 0) goto L_0x06f1
            if (r19 == 0) goto L_0x06e9
            long r7 = r19.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r12 = r11.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r7 = r7 - r12
            long r7 = java.lang.Math.abs(r7)     // Catch:{ all -> 0x0f7f }
            r12 = 1000(0x3e8, double:4.94E-321)
            int r4 = (r7 > r12 ? 1 : (r7 == r12 ? 0 : -1))
            if (r4 > 0) goto L_0x06e9
            java.lang.Object r4 = r19.clone()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r4 = (com.google.android.gms.internal.measurement.C2595l4.C2596a) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0$a r4 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r4     // Catch:{ all -> 0x0f7f }
            boolean r7 = r1.m9058a(r11, r4)     // Catch:{ all -> 0x0f7f }
            if (r7 == 0) goto L_0x06e9
            r7 = r33
            r3.mo18067a(r7, r4)     // Catch:{ all -> 0x0f7f }
            r8 = r32
        L_0x06e3:
            r18 = 0
            r19 = 0
            goto L_0x07a3
        L_0x06e9:
            r7 = r33
            r18 = r11
            r8 = r25
            goto L_0x07a3
        L_0x06f1:
            r7 = r33
        L_0x06f3:
            r8 = r32
            goto L_0x07a3
        L_0x06f7:
            r7 = r33
            java.lang.String r4 = "_vs"
            java.lang.String r8 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.equals(r8)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0746
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r11.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r4 = (com.google.android.gms.internal.measurement.C2701s0) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r4 = com.google.android.gms.measurement.internal.C3223v9.m9261a(r4, r9)     // Catch:{ all -> 0x0f7f }
            if (r4 != 0) goto L_0x06f3
            if (r18 == 0) goto L_0x073f
            long r12 = r18.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r14 = r11.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r12 = r12 - r14
            long r12 = java.lang.Math.abs(r12)     // Catch:{ all -> 0x0f7f }
            r14 = 1000(0x3e8, double:4.94E-321)
            int r4 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r4 > 0) goto L_0x073f
            java.lang.Object r4 = r18.clone()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r4 = (com.google.android.gms.internal.measurement.C2595l4.C2596a) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0$a r4 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r4     // Catch:{ all -> 0x0f7f }
            boolean r8 = r1.m9058a(r4, r11)     // Catch:{ all -> 0x0f7f }
            if (r8 == 0) goto L_0x073f
            r8 = r32
            r3.mo18067a(r8, r4)     // Catch:{ all -> 0x0f7f }
            goto L_0x06e3
        L_0x073f:
            r8 = r32
            r19 = r11
            r7 = r25
            goto L_0x07a3
        L_0x0746:
            r8 = r32
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r12 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r12 = r12.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r13 = com.google.android.gms.measurement.internal.C3135o.f5403K0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r12, r13)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x07a3
            java.lang.String r4 = "_ab"
            java.lang.String r12 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.equals(r12)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x07a3
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r11.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r4 = (com.google.android.gms.internal.measurement.C2701s0) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r4 = com.google.android.gms.measurement.internal.C3223v9.m9261a(r4, r9)     // Catch:{ all -> 0x0f7f }
            if (r4 != 0) goto L_0x07a3
            if (r18 == 0) goto L_0x07a3
            long r12 = r18.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r14 = r11.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r12 = r12 - r14
            long r12 = java.lang.Math.abs(r12)     // Catch:{ all -> 0x0f7f }
            r14 = 4000(0xfa0, double:1.9763E-320)
            int r4 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r4 > 0) goto L_0x07a3
            java.lang.Object r4 = r18.clone()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r4 = (com.google.android.gms.internal.measurement.C2595l4.C2596a) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0$a r4 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r4     // Catch:{ all -> 0x0f7f }
            r1.m9061b(r4, r11)     // Catch:{ all -> 0x0f7f }
            r3.mo18067a(r8, r4)     // Catch:{ all -> 0x0f7f }
            r18 = 0
            goto L_0x07a3
        L_0x079f:
            r8 = r32
            r7 = r33
        L_0x07a3:
            if (r31 != 0) goto L_0x0803
            java.lang.String r4 = r11.mo17869m()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r5.equals(r4)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0803
            int r4 = r11.mo17867k()     // Catch:{ all -> 0x0f7f }
            if (r4 != 0) goto L_0x07cf
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "Engagement event does not contain any parameters. appId"
            com.google.android.gms.internal.measurement.w0 r9 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = r9.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r9)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r5, r9)     // Catch:{ all -> 0x0f7f }
            goto L_0x0803
        L_0x07cf:
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r11.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r4 = (com.google.android.gms.internal.measurement.C2701s0) r4     // Catch:{ all -> 0x0f7f }
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r4, r9)     // Catch:{ all -> 0x0f7f }
            java.lang.Long r4 = (java.lang.Long) r4     // Catch:{ all -> 0x0f7f }
            if (r4 != 0) goto L_0x07fc
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "Engagement event does not include duration. appId"
            com.google.android.gms.internal.measurement.w0 r9 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = r9.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r9)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r5, r9)     // Catch:{ all -> 0x0f7f }
            goto L_0x0803
        L_0x07fc:
            long r4 = r4.longValue()     // Catch:{ all -> 0x0f7f }
            long r13 = r26 + r4
            goto L_0x0805
        L_0x0803:
            r13 = r26
        L_0x0805:
            java.util.List<com.google.android.gms.internal.measurement.s0> r4 = r2.f5535c     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r5 = r11.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r5 = (com.google.android.gms.internal.measurement.C2595l4) r5     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r5 = (com.google.android.gms.internal.measurement.C2701s0) r5     // Catch:{ all -> 0x0f7f }
            r9 = r30
            r4.set(r9, r5)     // Catch:{ all -> 0x0f7f }
            int r15 = r25 + 1
            r3.mo18071a(r11)     // Catch:{ all -> 0x0f7f }
            r12 = r21
        L_0x081b:
            int r4 = r9 + 1
            r9 = r7
            r7 = r4
            r4 = r31
            goto L_0x02c8
        L_0x0823:
            r31 = r4
            r9 = r6
            r25 = r15
            if (r31 == 0) goto L_0x087f
            r6 = r25
            r13 = r26
            r4 = 0
        L_0x082f:
            if (r4 >= r6) goto L_0x0881
            com.google.android.gms.internal.measurement.s0 r7 = r3.mo18075b(r4)     // Catch:{ all -> 0x0f7f }
            java.lang.String r8 = r7.mo17849p()     // Catch:{ all -> 0x0f7f }
            boolean r8 = r5.equals(r8)     // Catch:{ all -> 0x0f7f }
            if (r8 == 0) goto L_0x0852
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r8 = "_fr"
            com.google.android.gms.internal.measurement.u0 r8 = com.google.android.gms.measurement.internal.C3223v9.m9261a(r7, r8)     // Catch:{ all -> 0x0f7f }
            if (r8 == 0) goto L_0x0852
            r3.mo18080c(r4)     // Catch:{ all -> 0x0f7f }
            int r6 = r6 + -1
            int r4 = r4 + -1
            goto L_0x087c
        L_0x0852:
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r7 = com.google.android.gms.measurement.internal.C3223v9.m9261a(r7, r9)     // Catch:{ all -> 0x0f7f }
            if (r7 == 0) goto L_0x087c
            boolean r8 = r7.mo17925s()     // Catch:{ all -> 0x0f7f }
            if (r8 == 0) goto L_0x086a
            long r7 = r7.mo17926t()     // Catch:{ all -> 0x0f7f }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x0f7f }
            goto L_0x086b
        L_0x086a:
            r7 = 0
        L_0x086b:
            if (r7 == 0) goto L_0x087c
            long r10 = r7.longValue()     // Catch:{ all -> 0x0f7f }
            r18 = 0
            int r8 = (r10 > r18 ? 1 : (r10 == r18 ? 0 : -1))
            if (r8 <= 0) goto L_0x087c
            long r7 = r7.longValue()     // Catch:{ all -> 0x0f7f }
            long r13 = r13 + r7
        L_0x087c:
            r7 = 1
            int r4 = r4 + r7
            goto L_0x082f
        L_0x087f:
            r13 = r26
        L_0x0881:
            r4 = 0
            r1.m9053a(r3, r13, r4)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5461n0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r5, r6)     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "_se"
            if (r4 == 0) goto L_0x094a
            java.util.List r4 = r3.mo18104j()     // Catch:{ all -> 0x0f7f }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0f7f }
        L_0x08a1:
            boolean r6 = r4.hasNext()     // Catch:{ all -> 0x0f7f }
            if (r6 == 0) goto L_0x08bb
            java.lang.Object r6 = r4.next()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r6 = (com.google.android.gms.internal.measurement.C2701s0) r6     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = "_s"
            java.lang.String r6 = r6.mo17849p()     // Catch:{ all -> 0x0f7f }
            boolean r6 = r7.equals(r6)     // Catch:{ all -> 0x0f7f }
            if (r6 == 0) goto L_0x08a1
            r4 = 1
            goto L_0x08bc
        L_0x08bb:
            r4 = 0
        L_0x08bc:
            if (r4 == 0) goto L_0x08c9
            com.google.android.gms.measurement.internal.d r4 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            r4.mo18860b(r6, r5)     // Catch:{ all -> 0x0f7f }
        L_0x08c9:
            boolean r4 = com.google.android.gms.internal.measurement.C2458cc.m6163b()     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0945
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5463o0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r6, r7)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0945
            java.lang.String r4 = "_sid"
            int r4 = com.google.android.gms.measurement.internal.C3223v9.m9260a(r3, r4)     // Catch:{ all -> 0x0f7f }
            if (r4 < 0) goto L_0x08eb
            r4 = 1
            goto L_0x08ec
        L_0x08eb:
            r4 = 0
        L_0x08ec:
            if (r4 != 0) goto L_0x0945
            int r4 = com.google.android.gms.measurement.internal.C3223v9.m9260a(r3, r5)     // Catch:{ all -> 0x0f7f }
            if (r4 < 0) goto L_0x0967
            r3.mo18087e(r4)     // Catch:{ all -> 0x0f7f }
            boolean r4 = com.google.android.gms.internal.measurement.C2489ea.m6262b()     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x092b
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r5 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r5.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5431Y0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r5, r6)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x092b
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "Session engagement user property is in the bundle without session ID. appId"
            com.google.android.gms.internal.measurement.w0 r6 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r6.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r6)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r5, r6)     // Catch:{ all -> 0x0f7f }
            goto L_0x0967
        L_0x092b:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r4 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "Session engagement user property is in the bundle without session ID. appId"
            com.google.android.gms.internal.measurement.w0 r6 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r6.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r6)     // Catch:{ all -> 0x0f7f }
            r4.mo19043a(r5, r6)     // Catch:{ all -> 0x0f7f }
            goto L_0x0967
        L_0x0945:
            r4 = 1
            r1.m9053a(r3, r13, r4)     // Catch:{ all -> 0x0f7f }
            goto L_0x0967
        L_0x094a:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5467q0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r6, r7)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0967
            com.google.android.gms.measurement.internal.d r4 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            r4.mo18860b(r6, r5)     // Catch:{ all -> 0x0f7f }
        L_0x0967:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5435a0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r5, r6)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0a09
            com.google.android.gms.measurement.internal.v9 r4 = r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r5 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo18996B()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Checking account type status for ad personalization signals"
            r5.mo19042a(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d5 r5 = r4.mo19173m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            boolean r5 = r5.mo18898e(r6)     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0a09
            com.google.android.gms.measurement.internal.d r5 = r4.mo19172k()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.e5 r5 = r5.mo18858b(r6)     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0a09
            boolean r5 = r5.mo18956g()     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0a09
            com.google.android.gms.measurement.internal.i r5 = r4.mo19009d()     // Catch:{ all -> 0x0f7f }
            boolean r5 = r5.mo19063x()     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0a09
            com.google.android.gms.measurement.internal.f4 r5 = r4.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo18995A()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Turning off ad personalization due to account type"
            r5.mo19042a(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.a1$a r5 = com.google.android.gms.internal.measurement.C2414a1.m5972x()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "_npa"
            r5.mo17259a(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.i r4 = r4.mo19009d()     // Catch:{ all -> 0x0f7f }
            long r6 = r4.mo19061v()     // Catch:{ all -> 0x0f7f }
            r5.mo17258a(r6)     // Catch:{ all -> 0x0f7f }
            r6 = 1
            r5.mo17260b(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r4 = r5.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r4 = (com.google.android.gms.internal.measurement.C2595l4) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.a1 r4 = (com.google.android.gms.internal.measurement.C2414a1) r4     // Catch:{ all -> 0x0f7f }
            r5 = 0
        L_0x09e5:
            int r6 = r3.mo18113n()     // Catch:{ all -> 0x0f7f }
            if (r5 >= r6) goto L_0x0a03
            java.lang.String r6 = "_npa"
            com.google.android.gms.internal.measurement.a1 r7 = r3.mo18084d(r5)     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo17250p()     // Catch:{ all -> 0x0f7f }
            boolean r6 = r6.equals(r7)     // Catch:{ all -> 0x0f7f }
            if (r6 == 0) goto L_0x0a00
            r3.mo18066a(r5, r4)     // Catch:{ all -> 0x0f7f }
            r5 = 1
            goto L_0x0a04
        L_0x0a00:
            int r5 = r5 + 1
            goto L_0x09e5
        L_0x0a03:
            r5 = 0
        L_0x0a04:
            if (r5 != 0) goto L_0x0a09
            r3.mo18070a(r4)     // Catch:{ all -> 0x0f7f }
        L_0x0a09:
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5393F0     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19154e(r5, r6)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0a1e
            m9052a(r3)     // Catch:{ all -> 0x0f7f }
        L_0x0a1e:
            r3.mo18124w()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.ca r5 = r63.mo19230f()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            java.util.List r7 = r3.mo18104j()     // Catch:{ all -> 0x0f7f }
            java.util.List r8 = r3.mo18112m()     // Catch:{ all -> 0x0f7f }
            long r9 = r3.mo18115o()     // Catch:{ all -> 0x0f7f }
            java.lang.Long r9 = java.lang.Long.valueOf(r9)     // Catch:{ all -> 0x0f7f }
            long r10 = r3.mo18117p()     // Catch:{ all -> 0x0f7f }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0f7f }
            java.util.List r4 = r5.mo18832a(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0f7f }
            r3.mo18077b(r4)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r4 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r4 = r4.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r5 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r5.mo18052p0()     // Catch:{ all -> 0x0f7f }
            boolean r4 = r4.mo19153e(r5)     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0dc0
            java.util.HashMap r4 = new java.util.HashMap     // Catch:{ all -> 0x0f7f }
            r4.<init>()     // Catch:{ all -> 0x0f7f }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0f7f }
            r5.<init>()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.z9 r6 = r6.mo19104w()     // Catch:{ all -> 0x0f7f }
            java.security.SecureRandom r6 = r6.mo19454u()     // Catch:{ all -> 0x0f7f }
            r7 = 0
        L_0x0a6f:
            int r8 = r3.mo18105k()     // Catch:{ all -> 0x0f7f }
            if (r7 >= r8) goto L_0x0d8c
            com.google.android.gms.internal.measurement.s0 r8 = r3.mo18075b(r7)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4$a r8 = r8.mo17669j()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0$a r8 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r8     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = "_ep"
            boolean r9 = r9.equals(r10)     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0b00
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r9 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r9 = (com.google.android.gms.internal.measurement.C2595l4) r9     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r9 = (com.google.android.gms.internal.measurement.C2701s0) r9     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = "_en"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r9, r10)     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x0f7f }
            java.lang.Object r10 = r4.get(r9)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r10 = (com.google.android.gms.measurement.internal.C3087k) r10     // Catch:{ all -> 0x0f7f }
            if (r10 != 0) goto L_0x0ab7
            com.google.android.gms.measurement.internal.d r10 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r11 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r11 = r11.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r10 = r10.mo18843a(r11, r9)     // Catch:{ all -> 0x0f7f }
            r4.put(r9, r10)     // Catch:{ all -> 0x0f7f }
        L_0x0ab7:
            java.lang.Long r9 = r10.f5286i     // Catch:{ all -> 0x0f7f }
            if (r9 != 0) goto L_0x0af6
            java.lang.Long r9 = r10.f5287j     // Catch:{ all -> 0x0f7f }
            long r11 = r9.longValue()     // Catch:{ all -> 0x0f7f }
            r13 = 1
            int r9 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r9 <= 0) goto L_0x0ad1
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = "_sr"
            java.lang.Long r11 = r10.f5287j     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.C3223v9.m9265a(r8, r9, r11)     // Catch:{ all -> 0x0f7f }
        L_0x0ad1:
            java.lang.Boolean r9 = r10.f5288k     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0aeb
            java.lang.Boolean r9 = r10.f5288k     // Catch:{ all -> 0x0f7f }
            boolean r9 = r9.booleanValue()     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0aeb
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = "_efs"
            r10 = 1
            java.lang.Long r12 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.C3223v9.m9265a(r8, r9, r12)     // Catch:{ all -> 0x0f7f }
        L_0x0aeb:
            com.google.android.gms.internal.measurement.u5 r9 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r9 = (com.google.android.gms.internal.measurement.C2595l4) r9     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r9 = (com.google.android.gms.internal.measurement.C2701s0) r9     // Catch:{ all -> 0x0f7f }
            r5.add(r9)     // Catch:{ all -> 0x0f7f }
        L_0x0af6:
            r3.mo18067a(r7, r8)     // Catch:{ all -> 0x0f7f }
        L_0x0af9:
            r64 = r2
            r23 = r6
            r2 = r7
            goto L_0x0d84
        L_0x0b00:
            com.google.android.gms.measurement.internal.d5 r9 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r10 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = r10.mo18052p0()     // Catch:{ all -> 0x0f7f }
            long r9 = r9.mo18899f(r10)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            r11.mo19104w()     // Catch:{ all -> 0x0f7f }
            long r11 = r8.mo17870n()     // Catch:{ all -> 0x0f7f }
            long r11 = com.google.android.gms.measurement.internal.C3267z9.m9409a(r11, r9)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r13 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r13 = (com.google.android.gms.internal.measurement.C2595l4) r13     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r13 = (com.google.android.gms.internal.measurement.C2701s0) r13     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = "_dbg"
            r18 = 1
            java.lang.Long r15 = java.lang.Long.valueOf(r18)     // Catch:{ all -> 0x0f7f }
            boolean r18 = android.text.TextUtils.isEmpty(r14)     // Catch:{ all -> 0x0f7f }
            if (r18 != 0) goto L_0x0b8b
            if (r15 != 0) goto L_0x0b34
            goto L_0x0b8b
        L_0x0b34:
            java.util.List r13 = r13.mo17847n()     // Catch:{ all -> 0x0f7f }
            java.util.Iterator r13 = r13.iterator()     // Catch:{ all -> 0x0f7f }
        L_0x0b3c:
            boolean r18 = r13.hasNext()     // Catch:{ all -> 0x0f7f }
            if (r18 == 0) goto L_0x0b8b
            java.lang.Object r18 = r13.next()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u0 r18 = (com.google.android.gms.internal.measurement.C2733u0) r18     // Catch:{ all -> 0x0f7f }
            r64 = r13
            java.lang.String r13 = r18.mo17922o()     // Catch:{ all -> 0x0f7f }
            boolean r13 = r14.equals(r13)     // Catch:{ all -> 0x0f7f }
            if (r13 == 0) goto L_0x0b88
            boolean r13 = r15 instanceof java.lang.Long     // Catch:{ all -> 0x0f7f }
            if (r13 == 0) goto L_0x0b66
            long r13 = r18.mo17926t()     // Catch:{ all -> 0x0f7f }
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ all -> 0x0f7f }
            boolean r13 = r15.equals(r13)     // Catch:{ all -> 0x0f7f }
            if (r13 != 0) goto L_0x0b86
        L_0x0b66:
            boolean r13 = r15 instanceof java.lang.String     // Catch:{ all -> 0x0f7f }
            if (r13 == 0) goto L_0x0b74
            java.lang.String r13 = r18.mo17924q()     // Catch:{ all -> 0x0f7f }
            boolean r13 = r15.equals(r13)     // Catch:{ all -> 0x0f7f }
            if (r13 != 0) goto L_0x0b86
        L_0x0b74:
            boolean r13 = r15 instanceof java.lang.Double     // Catch:{ all -> 0x0f7f }
            if (r13 == 0) goto L_0x0b8b
            double r13 = r18.mo17928v()     // Catch:{ all -> 0x0f7f }
            java.lang.Double r13 = java.lang.Double.valueOf(r13)     // Catch:{ all -> 0x0f7f }
            boolean r13 = r15.equals(r13)     // Catch:{ all -> 0x0f7f }
            if (r13 == 0) goto L_0x0b8b
        L_0x0b86:
            r13 = 1
            goto L_0x0b8c
        L_0x0b88:
            r13 = r64
            goto L_0x0b3c
        L_0x0b8b:
            r13 = 0
        L_0x0b8c:
            if (r13 != 0) goto L_0x0ba1
            com.google.android.gms.measurement.internal.d5 r13 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r14 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r14.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.String r15 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            int r13 = r13.mo18896d(r14, r15)     // Catch:{ all -> 0x0f7f }
            goto L_0x0ba2
        L_0x0ba1:
            r13 = 1
        L_0x0ba2:
            if (r13 > 0) goto L_0x0bcb
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = "Sample rate must be positive. event, rate"
            java.lang.String r11 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x0f7f }
            r9.mo19044a(r10, r11, r12)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r9 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r9 = (com.google.android.gms.internal.measurement.C2595l4) r9     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r9 = (com.google.android.gms.internal.measurement.C2701s0) r9     // Catch:{ all -> 0x0f7f }
            r5.add(r9)     // Catch:{ all -> 0x0f7f }
            r3.mo18067a(r7, r8)     // Catch:{ all -> 0x0f7f }
            goto L_0x0af9
        L_0x0bcb:
            java.lang.String r14 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r14 = r4.get(r14)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r14 = (com.google.android.gms.measurement.internal.C3087k) r14     // Catch:{ all -> 0x0f7f }
            if (r14 != 0) goto L_0x0c65
            com.google.android.gms.measurement.internal.d r14 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r15 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r15 = r15.mo18052p0()     // Catch:{ all -> 0x0f7f }
            r18 = r9
            java.lang.String r9 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r14 = r14.mo18843a(r15, r9)     // Catch:{ all -> 0x0f7f }
            if (r14 != 0) goto L_0x0c67
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = "Event being bundled has no eventAggregate. appId, eventName"
            com.google.android.gms.internal.measurement.w0 r14 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r14 = r14.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.String r15 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            r9.mo19044a(r10, r14, r15)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.j5 r9 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r9 = r9.mo19097m()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r10 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = r10.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.C3135o.f5459m0     // Catch:{ all -> 0x0f7f }
            boolean r9 = r9.mo19154e(r10, r14)     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0c40
            com.google.android.gms.measurement.internal.k r9 = new com.google.android.gms.measurement.internal.k     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r10 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r31 = r10.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.String r32 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            r33 = 1
            r35 = 1
            r37 = 1
            long r39 = r8.mo17870n()     // Catch:{ all -> 0x0f7f }
            r41 = 0
            r43 = 0
            r44 = 0
            r45 = 0
            r46 = 0
            r30 = r9
            r30.<init>(r31, r32, r33, r35, r37, r39, r41, r43, r44, r45, r46)     // Catch:{ all -> 0x0f7f }
            goto L_0x0c63
        L_0x0c40:
            com.google.android.gms.measurement.internal.k r9 = new com.google.android.gms.measurement.internal.k     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r10 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r48 = r10.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.String r49 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            r50 = 1
            r52 = 1
            long r54 = r8.mo17870n()     // Catch:{ all -> 0x0f7f }
            r56 = 0
            r58 = 0
            r59 = 0
            r60 = 0
            r61 = 0
            r47 = r9
            r47.<init>(r48, r49, r50, r52, r54, r56, r58, r59, r60, r61)     // Catch:{ all -> 0x0f7f }
        L_0x0c63:
            r14 = r9
            goto L_0x0c67
        L_0x0c65:
            r18 = r9
        L_0x0c67:
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r9 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r9 = (com.google.android.gms.internal.measurement.C2595l4) r9     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r9 = (com.google.android.gms.internal.measurement.C2701s0) r9     // Catch:{ all -> 0x0f7f }
            java.lang.String r10 = "_eid"
            java.lang.Object r9 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r9, r10)     // Catch:{ all -> 0x0f7f }
            java.lang.Long r9 = (java.lang.Long) r9     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0c7e
            r10 = 1
            goto L_0x0c7f
        L_0x0c7e:
            r10 = 0
        L_0x0c7f:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r10)     // Catch:{ all -> 0x0f7f }
            r15 = 1
            if (r13 != r15) goto L_0x0cb4
            com.google.android.gms.internal.measurement.u5 r9 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r9 = (com.google.android.gms.internal.measurement.C2595l4) r9     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r9 = (com.google.android.gms.internal.measurement.C2701s0) r9     // Catch:{ all -> 0x0f7f }
            r5.add(r9)     // Catch:{ all -> 0x0f7f }
            boolean r9 = r10.booleanValue()     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0caf
            java.lang.Long r9 = r14.f5286i     // Catch:{ all -> 0x0f7f }
            if (r9 != 0) goto L_0x0ca3
            java.lang.Long r9 = r14.f5287j     // Catch:{ all -> 0x0f7f }
            if (r9 != 0) goto L_0x0ca3
            java.lang.Boolean r9 = r14.f5288k     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0caf
        L_0x0ca3:
            r9 = 0
            com.google.android.gms.measurement.internal.k r10 = r14.mo19126a(r9, r9, r9)     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            r4.put(r9, r10)     // Catch:{ all -> 0x0f7f }
        L_0x0caf:
            r3.mo18067a(r7, r8)     // Catch:{ all -> 0x0f7f }
            goto L_0x0af9
        L_0x0cb4:
            int r15 = r6.nextInt(r13)     // Catch:{ all -> 0x0f7f }
            if (r15 != 0) goto L_0x0cf9
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r9 = "_sr"
            r64 = r2
            r15 = r3
            long r2 = (long) r13     // Catch:{ all -> 0x0f7f }
            java.lang.Long r13 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.C3223v9.m9265a(r8, r9, r13)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r9 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r9 = (com.google.android.gms.internal.measurement.C2595l4) r9     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r9 = (com.google.android.gms.internal.measurement.C2701s0) r9     // Catch:{ all -> 0x0f7f }
            r5.add(r9)     // Catch:{ all -> 0x0f7f }
            boolean r9 = r10.booleanValue()     // Catch:{ all -> 0x0f7f }
            if (r9 == 0) goto L_0x0ce4
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x0f7f }
            r3 = 0
            com.google.android.gms.measurement.internal.k r14 = r14.mo19126a(r3, r2, r3)     // Catch:{ all -> 0x0f7f }
        L_0x0ce4:
            java.lang.String r2 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            long r9 = r8.mo17870n()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r3 = r14.mo19125a(r9, r11)     // Catch:{ all -> 0x0f7f }
            r4.put(r2, r3)     // Catch:{ all -> 0x0f7f }
            r23 = r6
            r2 = r7
            r3 = r15
            goto L_0x0d81
        L_0x0cf9:
            r64 = r2
            r15 = r3
            java.lang.Long r2 = r14.f5285h     // Catch:{ all -> 0x0f7f }
            if (r2 == 0) goto L_0x0d0b
            java.lang.Long r2 = r14.f5285h     // Catch:{ all -> 0x0f7f }
            long r2 = r2.longValue()     // Catch:{ all -> 0x0f7f }
            r23 = r6
            r25 = r7
            goto L_0x0d1e
        L_0x0d0b:
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            r2.mo19104w()     // Catch:{ all -> 0x0f7f }
            long r2 = r8.mo17871o()     // Catch:{ all -> 0x0f7f }
            r23 = r6
            r25 = r7
            r6 = r18
            long r2 = com.google.android.gms.measurement.internal.C3267z9.m9409a(r2, r6)     // Catch:{ all -> 0x0f7f }
        L_0x0d1e:
            int r6 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r6 == 0) goto L_0x0d6c
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r2 = "_efs"
            r6 = 1
            java.lang.Long r3 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.C3223v9.m9265a(r8, r2, r3)     // Catch:{ all -> 0x0f7f }
            r63.mo19232h()     // Catch:{ all -> 0x0f7f }
            java.lang.String r2 = "_sr"
            long r6 = (long) r13     // Catch:{ all -> 0x0f7f }
            java.lang.Long r3 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.C3223v9.m9265a(r8, r2, r3)     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r2 = r8.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r2 = (com.google.android.gms.internal.measurement.C2595l4) r2     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.s0 r2 = (com.google.android.gms.internal.measurement.C2701s0) r2     // Catch:{ all -> 0x0f7f }
            r5.add(r2)     // Catch:{ all -> 0x0f7f }
            boolean r2 = r10.booleanValue()     // Catch:{ all -> 0x0f7f }
            if (r2 == 0) goto L_0x0d5c
            java.lang.Long r2 = java.lang.Long.valueOf(r6)     // Catch:{ all -> 0x0f7f }
            r3 = 1
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r3)     // Catch:{ all -> 0x0f7f }
            r3 = 0
            com.google.android.gms.measurement.internal.k r14 = r14.mo19126a(r3, r2, r6)     // Catch:{ all -> 0x0f7f }
        L_0x0d5c:
            java.lang.String r2 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            long r6 = r8.mo17870n()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r3 = r14.mo19125a(r6, r11)     // Catch:{ all -> 0x0f7f }
            r4.put(r2, r3)     // Catch:{ all -> 0x0f7f }
            goto L_0x0d7e
        L_0x0d6c:
            boolean r2 = r10.booleanValue()     // Catch:{ all -> 0x0f7f }
            if (r2 == 0) goto L_0x0d7e
            java.lang.String r2 = r8.mo17869m()     // Catch:{ all -> 0x0f7f }
            r3 = 0
            com.google.android.gms.measurement.internal.k r6 = r14.mo19126a(r9, r3, r3)     // Catch:{ all -> 0x0f7f }
            r4.put(r2, r6)     // Catch:{ all -> 0x0f7f }
        L_0x0d7e:
            r3 = r15
            r2 = r25
        L_0x0d81:
            r3.mo18067a(r2, r8)     // Catch:{ all -> 0x0f7f }
        L_0x0d84:
            int r7 = r2 + 1
            r2 = r64
            r6 = r23
            goto L_0x0a6f
        L_0x0d8c:
            r64 = r2
            int r2 = r5.size()     // Catch:{ all -> 0x0f7f }
            int r6 = r3.mo18105k()     // Catch:{ all -> 0x0f7f }
            if (r2 >= r6) goto L_0x0d9e
            r3.mo18108l()     // Catch:{ all -> 0x0f7f }
            r3.mo18072a(r5)     // Catch:{ all -> 0x0f7f }
        L_0x0d9e:
            java.util.Set r2 = r4.entrySet()     // Catch:{ all -> 0x0f7f }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0f7f }
        L_0x0da6:
            boolean r4 = r2.hasNext()     // Catch:{ all -> 0x0f7f }
            if (r4 == 0) goto L_0x0dc2
            java.lang.Object r4 = r2.next()     // Catch:{ all -> 0x0f7f }
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d r5 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r4 = r4.getValue()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.k r4 = (com.google.android.gms.measurement.internal.C3087k) r4     // Catch:{ all -> 0x0f7f }
            r5.mo18851a(r4)     // Catch:{ all -> 0x0f7f }
            goto L_0x0da6
        L_0x0dc0:
            r64 = r2
        L_0x0dc2:
            com.google.android.gms.measurement.internal.j5 r2 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.la r2 = r2.mo19097m()     // Catch:{ all -> 0x0f7f }
            java.lang.String r4 = r3.mo18121t()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.C3135o.f5393F0     // Catch:{ all -> 0x0f7f }
            boolean r2 = r2.mo19154e(r4, r5)     // Catch:{ all -> 0x0f7f }
            if (r2 != 0) goto L_0x0dd7
            m9052a(r3)     // Catch:{ all -> 0x0f7f }
        L_0x0dd7:
            r2 = r64
            com.google.android.gms.internal.measurement.w0 r4 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r4 = r4.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d r5 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.e5 r5 = r5.mo18858b(r4)     // Catch:{ all -> 0x0f7f }
            if (r5 != 0) goto L_0x0e03
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19001t()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Bundling raw events w/o app info. appId"
            com.google.android.gms.internal.measurement.w0 r7 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x0f7f }
            r5.mo19043a(r6, r7)     // Catch:{ all -> 0x0f7f }
            goto L_0x0e5e
        L_0x0e03:
            int r6 = r3.mo18105k()     // Catch:{ all -> 0x0f7f }
            if (r6 <= 0) goto L_0x0e5e
            long r6 = r5.mo18980t()     // Catch:{ all -> 0x0f7f }
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 == 0) goto L_0x0e17
            r3.mo18088e(r6)     // Catch:{ all -> 0x0f7f }
            goto L_0x0e1a
        L_0x0e17:
            r3.mo18120s()     // Catch:{ all -> 0x0f7f }
        L_0x0e1a:
            long r8 = r5.mo18979s()     // Catch:{ all -> 0x0f7f }
            r10 = 0
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x0e25
            goto L_0x0e26
        L_0x0e25:
            r6 = r8
        L_0x0e26:
            int r8 = (r6 > r10 ? 1 : (r6 == r10 ? 0 : -1))
            if (r8 == 0) goto L_0x0e2e
            r3.mo18085d(r6)     // Catch:{ all -> 0x0f7f }
            goto L_0x0e31
        L_0x0e2e:
            r3.mo18119q()     // Catch:{ all -> 0x0f7f }
        L_0x0e31:
            r5.mo18926E()     // Catch:{ all -> 0x0f7f }
            long r6 = r5.mo18923B()     // Catch:{ all -> 0x0f7f }
            int r7 = (int) r6     // Catch:{ all -> 0x0f7f }
            r3.mo18093g(r7)     // Catch:{ all -> 0x0f7f }
            long r6 = r3.mo18115o()     // Catch:{ all -> 0x0f7f }
            r5.mo18931a(r6)     // Catch:{ all -> 0x0f7f }
            long r6 = r3.mo18117p()     // Catch:{ all -> 0x0f7f }
            r5.mo18938b(r6)     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r5.mo18948e()     // Catch:{ all -> 0x0f7f }
            if (r6 == 0) goto L_0x0e54
            r3.mo18103j(r6)     // Catch:{ all -> 0x0f7f }
            goto L_0x0e57
        L_0x0e54:
            r3.mo18122u()     // Catch:{ all -> 0x0f7f }
        L_0x0e57:
            com.google.android.gms.measurement.internal.d r6 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            r6.mo18850a(r5)     // Catch:{ all -> 0x0f7f }
        L_0x0e5e:
            int r5 = r3.mo18105k()     // Catch:{ all -> 0x0f7f }
            if (r5 <= 0) goto L_0x0ec4
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            r5.mo19018r()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d5 r5 = r63.mo19225c()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r6 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = r6.mo18052p0()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.n0 r5 = r5.mo18890a(r6)     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0e88
            boolean r6 = r5.mo17761n()     // Catch:{ all -> 0x0f7f }
            if (r6 != 0) goto L_0x0e80
            goto L_0x0e88
        L_0x0e80:
            long r5 = r5.mo17762o()     // Catch:{ all -> 0x0f7f }
            r3.mo18100i(r5)     // Catch:{ all -> 0x0f7f }
            goto L_0x0eb3
        L_0x0e88:
            com.google.android.gms.internal.measurement.w0 r5 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = r5.mo18007B()     // Catch:{ all -> 0x0f7f }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ all -> 0x0f7f }
            if (r5 == 0) goto L_0x0e9a
            r5 = -1
            r3.mo18100i(r5)     // Catch:{ all -> 0x0f7f }
            goto L_0x0eb3
        L_0x0e9a:
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19004w()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Did not find measurement config or missing version info. appId"
            com.google.android.gms.internal.measurement.w0 r7 = r2.f5533a     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = r7.mo18052p0()     // Catch:{ all -> 0x0f7f }
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x0f7f }
            r5.mo19043a(r6, r7)     // Catch:{ all -> 0x0f7f }
        L_0x0eb3:
            com.google.android.gms.measurement.internal.d r5 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.u5 r3 = r3.mo17679i()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.l4 r3 = (com.google.android.gms.internal.measurement.C2595l4) r3     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.internal.measurement.w0 r3 = (com.google.android.gms.internal.measurement.C2763w0) r3     // Catch:{ all -> 0x0f7f }
            r12 = r21
            r5.mo18853a(r3, r12)     // Catch:{ all -> 0x0f7f }
        L_0x0ec4:
            com.google.android.gms.measurement.internal.d r3 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            java.util.List<java.lang.Long> r2 = r2.f5534b     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.common.internal.C2258v.m5629a(r2)     // Catch:{ all -> 0x0f7f }
            r3.mo18881c()     // Catch:{ all -> 0x0f7f }
            r3.mo19284q()     // Catch:{ all -> 0x0f7f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "rowid in ("
            r5.<init>(r6)     // Catch:{ all -> 0x0f7f }
            r6 = 0
        L_0x0edb:
            int r7 = r2.size()     // Catch:{ all -> 0x0f7f }
            if (r6 >= r7) goto L_0x0ef8
            if (r6 == 0) goto L_0x0ee8
            java.lang.String r7 = ","
            r5.append(r7)     // Catch:{ all -> 0x0f7f }
        L_0x0ee8:
            java.lang.Object r7 = r2.get(r6)     // Catch:{ all -> 0x0f7f }
            java.lang.Long r7 = (java.lang.Long) r7     // Catch:{ all -> 0x0f7f }
            long r7 = r7.longValue()     // Catch:{ all -> 0x0f7f }
            r5.append(r7)     // Catch:{ all -> 0x0f7f }
            int r6 = r6 + 1
            goto L_0x0edb
        L_0x0ef8:
            java.lang.String r6 = ")"
            r5.append(r6)     // Catch:{ all -> 0x0f7f }
            android.database.sqlite.SQLiteDatabase r6 = r3.mo18875v()     // Catch:{ all -> 0x0f7f }
            java.lang.String r7 = "raw_events"
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0f7f }
            r8 = 0
            int r5 = r6.delete(r7, r5, r8)     // Catch:{ all -> 0x0f7f }
            int r6 = r2.size()     // Catch:{ all -> 0x0f7f }
            if (r5 == r6) goto L_0x0f2b
            com.google.android.gms.measurement.internal.f4 r3 = r3.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x0f7f }
            java.lang.String r6 = "Deleted fewer rows from raw events table than expected"
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0f7f }
            int r2 = r2.size()     // Catch:{ all -> 0x0f7f }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0f7f }
            r3.mo19044a(r6, r5, r2)     // Catch:{ all -> 0x0f7f }
        L_0x0f2b:
            com.google.android.gms.measurement.internal.d r2 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            android.database.sqlite.SQLiteDatabase r3 = r2.mo18875v()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "delete from raw_events_metadata where app_id=? and metadata_fingerprint not in (select distinct metadata_fingerprint from raw_events where app_id=?)"
            r6 = 2
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x0f42 }
            r7 = 0
            r6[r7] = r4     // Catch:{ SQLiteException -> 0x0f42 }
            r7 = 1
            r6[r7] = r4     // Catch:{ SQLiteException -> 0x0f42 }
            r3.execSQL(r5, r6)     // Catch:{ SQLiteException -> 0x0f42 }
            goto L_0x0f55
        L_0x0f42:
            r0 = move-exception
            r3 = r0
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x0f7f }
            java.lang.String r5 = "Failed to remove unused event metadata. appId"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r4)     // Catch:{ all -> 0x0f7f }
            r2.mo19044a(r5, r4, r3)     // Catch:{ all -> 0x0f7f }
        L_0x0f55:
            com.google.android.gms.measurement.internal.d r2 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            r2.mo18874u()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d r2 = r63.mo19229e()
            r2.mo18879z()
            r2 = 1
            return r2
        L_0x0f65:
            com.google.android.gms.measurement.internal.d r2 = r63.mo19229e()     // Catch:{ all -> 0x0f7f }
            r2.mo18874u()     // Catch:{ all -> 0x0f7f }
            com.google.android.gms.measurement.internal.d r2 = r63.mo19229e()
            r2.mo18879z()
            r2 = 0
            return r2
        L_0x0f75:
            r0 = move-exception
        L_0x0f76:
            r2 = r0
            r22 = r6
        L_0x0f79:
            if (r22 == 0) goto L_0x0f7e
            r22.close()     // Catch:{ all -> 0x0f7f }
        L_0x0f7e:
            throw r2     // Catch:{ all -> 0x0f7f }
        L_0x0f7f:
            r0 = move-exception
            r2 = r0
            com.google.android.gms.measurement.internal.d r3 = r63.mo19229e()
            r3.mo18879z()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.m9059a(java.lang.String, long):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.v9.a(com.google.android.gms.internal.measurement.s0$a, java.lang.String, java.lang.Object):void
     arg types: [com.google.android.gms.internal.measurement.s0$a, java.lang.String, long]
     candidates:
      com.google.android.gms.measurement.internal.v9.a(boolean, boolean, boolean):java.lang.String
      com.google.android.gms.measurement.internal.v9.a(java.lang.StringBuilder, int, com.google.android.gms.internal.measurement.d0):void
      com.google.android.gms.measurement.internal.v9.a(java.lang.StringBuilder, int, java.util.List<com.google.android.gms.internal.measurement.u0>):void
      com.google.android.gms.measurement.internal.v9.a(com.google.android.gms.internal.measurement.s0$a, java.lang.String, java.lang.Object):void */
    /* renamed from: b */
    private final void m9061b(C2701s0.C2702a aVar, C2701s0.C2702a aVar2) {
        C2258v.m5636a("_e".equals(aVar.mo17869m()));
        mo19232h();
        C2733u0 a = C3223v9.m9261a((C2701s0) aVar.mo17679i(), "_et");
        if (a.mo17925s() && a.mo17926t() > 0) {
            long t = a.mo17926t();
            mo19232h();
            C2733u0 a2 = C3223v9.m9261a((C2701s0) aVar2.mo17679i(), "_et");
            if (a2 != null && a2.mo17926t() > 0) {
                t += a2.mo17926t();
            }
            mo19232h();
            C3223v9.m9265a(aVar2, "_et", Long.valueOf(t));
            mo19232h();
            C3223v9.m9265a(aVar, "_fr", (Object) 1L);
        }
    }

    @WorkerThread
    /* renamed from: b */
    private final Boolean m9060b(C3021e5 e5Var) {
        try {
            if (e5Var.mo18982v() != -2147483648L) {
                if (e5Var.mo18982v() == ((long) C2283c.m5685a(this.f5517i.mo19016n()).mo17060b(e5Var.mo18967l(), 0).versionCode)) {
                    return true;
                }
            } else {
                String str = C2283c.m5685a(this.f5517i.mo19016n()).mo17060b(e5Var.mo18967l(), 0).versionName;
                if (e5Var.mo18981u() != null && e5Var.mo18981u().equals(str)) {
                    return true;
                }
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo19221b(zzkq zzkq, zzm zzm) {
        m9070z();
        mo19235m();
        if (m9064e(zzm)) {
            if (!zzm.f5821W) {
                mo19226c(zzm);
            } else if (!this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5435a0)) {
                this.f5517i.mo19015l().mo18995A().mo19043a("Removing user property", this.f5517i.mo19105x().mo18824c(zzkq.f5808Q));
                mo19229e().mo18878y();
                try {
                    mo19226c(zzm);
                    mo19229e().mo18860b(zzm.f5814P, zzkq.f5808Q);
                    mo19229e().mo18874u();
                    this.f5517i.mo19015l().mo18995A().mo19043a("User property removed", this.f5517i.mo19105x().mo18824c(zzkq.f5808Q));
                } finally {
                    mo19229e().mo18879z();
                }
            } else if (!"_npa".equals(zzkq.f5808Q) || zzm.f5832h0 == null) {
                this.f5517i.mo19015l().mo18995A().mo19043a("Removing user property", this.f5517i.mo19105x().mo18824c(zzkq.f5808Q));
                mo19229e().mo18878y();
                try {
                    mo19226c(zzm);
                    mo19229e().mo18860b(zzm.f5814P, zzkq.f5808Q);
                    mo19229e().mo18874u();
                    this.f5517i.mo19015l().mo18995A().mo19043a("User property removed", this.f5517i.mo19105x().mo18824c(zzkq.f5808Q));
                } finally {
                    mo19229e().mo18879z();
                }
            } else {
                this.f5517i.mo19015l().mo18995A().mo19042a("Falling back to manifest metadata value for ad personalization");
                mo19213a(new zzkq("_npa", this.f5517i.mo19017o().mo17132a(), Long.valueOf(zzm.f5832h0.booleanValue() ? 1 : 0), "auto"), zzm);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x04b2 A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01eb A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:84:0x0225 A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0248 A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x024f A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:90:0x025c A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x026f A[Catch:{ NameNotFoundException -> 0x0348, all -> 0x04dd }] */
    @androidx.annotation.WorkerThread
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo19222b(com.google.android.gms.measurement.internal.zzm r22) {
        /*
            r21 = this;
            r1 = r21
            r2 = r22
            java.lang.String r3 = "_sys"
            java.lang.String r4 = "_pfo"
            java.lang.String r5 = "_uwa"
            java.lang.String r0 = "app_id=?"
            r21.m9070z()
            r21.mo19235m()
            com.google.android.gms.common.internal.C2258v.m5629a(r22)
            java.lang.String r6 = r2.f5814P
            com.google.android.gms.common.internal.C2258v.m5639b(r6)
            boolean r6 = r21.m9064e(r22)
            if (r6 != 0) goto L_0x0021
            return
        L_0x0021:
            com.google.android.gms.measurement.internal.d r6 = r21.mo19229e()
            java.lang.String r7 = r2.f5814P
            com.google.android.gms.measurement.internal.e5 r6 = r6.mo18858b(r7)
            r7 = 0
            if (r6 == 0) goto L_0x0054
            java.lang.String r9 = r6.mo18971n()
            boolean r9 = android.text.TextUtils.isEmpty(r9)
            if (r9 == 0) goto L_0x0054
            java.lang.String r9 = r2.f5815Q
            boolean r9 = android.text.TextUtils.isEmpty(r9)
            if (r9 != 0) goto L_0x0054
            r6.mo18957h(r7)
            com.google.android.gms.measurement.internal.d r9 = r21.mo19229e()
            r9.mo18850a(r6)
            com.google.android.gms.measurement.internal.d5 r6 = r21.mo19225c()
            java.lang.String r9 = r2.f5814P
            r6.mo18897d(r9)
        L_0x0054:
            boolean r6 = r2.f5821W
            if (r6 != 0) goto L_0x005c
            r21.mo19226c(r22)
            return
        L_0x005c:
            long r9 = r2.f5826b0
            int r6 = (r9 > r7 ? 1 : (r9 == r7 ? 0 : -1))
            if (r6 != 0) goto L_0x006c
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i
            com.google.android.gms.common.util.e r6 = r6.mo19017o()
            long r9 = r6.mo17132a()
        L_0x006c:
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()
            java.lang.String r11 = r2.f5814P
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.C3135o.f5435a0
            boolean r6 = r6.mo19154e(r11, r12)
            if (r6 == 0) goto L_0x0085
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i
            com.google.android.gms.measurement.internal.i r6 = r6.mo19080G()
            r6.mo19062w()
        L_0x0085:
            int r6 = r2.f5827c0
            r15 = 0
            r13 = 1
            if (r6 == 0) goto L_0x00a7
            if (r6 == r13) goto L_0x00a7
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i
            com.google.android.gms.measurement.internal.f4 r11 = r11.mo19015l()
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo19004w()
            java.lang.String r12 = r2.f5814P
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r12)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            java.lang.String r14 = "Incorrect app type, assuming installed app. appId, appType"
            r11.mo19044a(r14, r12, r6)
            r6 = 0
        L_0x00a7:
            com.google.android.gms.measurement.internal.d r11 = r21.mo19229e()
            r11.mo18878y()
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r11 = r11.mo19097m()     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.C3135o.f5435a0     // Catch:{ all -> 0x04dd }
            boolean r11 = r11.mo19154e(r12, r14)     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x0127
            com.google.android.gms.measurement.internal.d r11 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = r2.f5814P     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "_npa"
            com.google.android.gms.measurement.internal.w9 r14 = r11.mo18863c(r12, r14)     // Catch:{ all -> 0x04dd }
            if (r14 == 0) goto L_0x00d6
            java.lang.String r11 = "auto"
            java.lang.String r12 = r14.f5734b     // Catch:{ all -> 0x04dd }
            boolean r11 = r11.equals(r12)     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x0127
        L_0x00d6:
            java.lang.Boolean r11 = r2.f5832h0     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x0111
            com.google.android.gms.measurement.internal.zzkq r12 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r16 = "_npa"
            java.lang.Boolean r11 = r2.f5832h0     // Catch:{ all -> 0x04dd }
            boolean r11 = r11.booleanValue()     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x00e9
            r17 = 1
            goto L_0x00eb
        L_0x00e9:
            r17 = 0
        L_0x00eb:
            java.lang.Long r17 = java.lang.Long.valueOf(r17)     // Catch:{ all -> 0x04dd }
            java.lang.String r18 = "auto"
            r11 = r12
            r7 = r12
            r12 = r16
            r19 = r3
            r8 = r14
            r3 = 1
            r13 = r9
            r15 = r17
            r16 = r18
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            if (r8 == 0) goto L_0x010d
            java.lang.Object r8 = r8.f5737e     // Catch:{ all -> 0x04dd }
            java.lang.Long r11 = r7.f5810S     // Catch:{ all -> 0x04dd }
            boolean r8 = r8.equals(r11)     // Catch:{ all -> 0x04dd }
            if (r8 != 0) goto L_0x012a
        L_0x010d:
            r1.mo19213a(r7, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x012a
        L_0x0111:
            r19 = r3
            r8 = r14
            r3 = 1
            if (r8 == 0) goto L_0x012a
            com.google.android.gms.measurement.internal.zzkq r7 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_npa"
            r15 = 0
            java.lang.String r16 = "auto"
            r11 = r7
            r13 = r9
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.mo19221b(r7, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x012a
        L_0x0127:
            r19 = r3
            r3 = 1
        L_0x012a:
            com.google.android.gms.measurement.internal.d r7 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            java.lang.String r8 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.e5 r7 = r7.mo18858b(r8)     // Catch:{ all -> 0x04dd }
            if (r7 == 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i     // Catch:{ all -> 0x04dd }
            r11.mo19104w()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.f5815Q     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = r7.mo18971n()     // Catch:{ all -> 0x04dd }
            java.lang.String r13 = r2.f5831g0     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = r7.mo18973o()     // Catch:{ all -> 0x04dd }
            boolean r11 = com.google.android.gms.measurement.internal.C3267z9.m9420a(r11, r12, r13, r14)     // Catch:{ all -> 0x04dd }
            if (r11 == 0) goto L_0x01e8
            com.google.android.gms.measurement.internal.j5 r11 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.f4 r11 = r11.mo19015l()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo19004w()     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "New GMP App Id passed in. Removing cached database data. appId"
            java.lang.String r13 = r7.mo18967l()     // Catch:{ all -> 0x04dd }
            java.lang.Object r13 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ all -> 0x04dd }
            r11.mo19043a(r12, r13)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.d r11 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = r7.mo18967l()     // Catch:{ all -> 0x04dd }
            r11.mo19284q()     // Catch:{ all -> 0x04dd }
            r11.mo18881c()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.common.internal.C2258v.m5639b(r7)     // Catch:{ all -> 0x04dd }
            android.database.sqlite.SQLiteDatabase r12 = r11.mo18875v()     // Catch:{ SQLiteException -> 0x01d3 }
            java.lang.String[] r13 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x01d3 }
            r15 = 0
            r13[r15] = r7     // Catch:{ SQLiteException -> 0x01d1 }
            java.lang.String r14 = "events"
            int r14 = r12.delete(r14, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r15
            java.lang.String r8 = "user_attributes"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "conditional_properties"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "apps"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "raw_events"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "raw_events_metadata"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "event_filters"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "property_filters"
            int r8 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r8
            java.lang.String r8 = "audience_filter_values"
            int r0 = r12.delete(r8, r0, r13)     // Catch:{ SQLiteException -> 0x01d1 }
            int r14 = r14 + r0
            if (r14 <= 0) goto L_0x01e6
            com.google.android.gms.measurement.internal.f4 r0 = r11.mo19015l()     // Catch:{ SQLiteException -> 0x01d1 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()     // Catch:{ SQLiteException -> 0x01d1 }
            java.lang.String r8 = "Deleted application data. app, records"
            java.lang.Integer r12 = java.lang.Integer.valueOf(r14)     // Catch:{ SQLiteException -> 0x01d1 }
            r0.mo19044a(r8, r7, r12)     // Catch:{ SQLiteException -> 0x01d1 }
            goto L_0x01e6
        L_0x01d1:
            r0 = move-exception
            goto L_0x01d5
        L_0x01d3:
            r0 = move-exception
            r15 = 0
        L_0x01d5:
            com.google.android.gms.measurement.internal.f4 r8 = r11.mo19015l()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19001t()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "Error deleting application data. appId, error"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x04dd }
            r8.mo19044a(r11, r7, r0)     // Catch:{ all -> 0x04dd }
        L_0x01e6:
            r7 = 0
            goto L_0x01e9
        L_0x01e8:
            r15 = 0
        L_0x01e9:
            if (r7 == 0) goto L_0x0248
            long r11 = r7.mo18982v()     // Catch:{ all -> 0x04dd }
            r13 = -2147483648(0xffffffff80000000, double:NaN)
            int r0 = (r11 > r13 ? 1 : (r11 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x0203
            long r11 = r7.mo18982v()     // Catch:{ all -> 0x04dd }
            r8 = r4
            long r3 = r2.f5823Y     // Catch:{ all -> 0x04dd }
            int r0 = (r11 > r3 ? 1 : (r11 == r3 ? 0 : -1))
            if (r0 == 0) goto L_0x0204
            r0 = 1
            goto L_0x0205
        L_0x0203:
            r8 = r4
        L_0x0204:
            r0 = 0
        L_0x0205:
            long r3 = r7.mo18982v()     // Catch:{ all -> 0x04dd }
            int r11 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r11 != 0) goto L_0x0221
            java.lang.String r3 = r7.mo18981u()     // Catch:{ all -> 0x04dd }
            if (r3 == 0) goto L_0x0221
            java.lang.String r3 = r7.mo18981u()     // Catch:{ all -> 0x04dd }
            java.lang.String r4 = r2.f5816R     // Catch:{ all -> 0x04dd }
            boolean r3 = r3.equals(r4)     // Catch:{ all -> 0x04dd }
            if (r3 != 0) goto L_0x0221
            r3 = 1
            goto L_0x0222
        L_0x0221:
            r3 = 0
        L_0x0222:
            r0 = r0 | r3
            if (r0 == 0) goto L_0x0249
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r0.<init>()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = "_pv"
            java.lang.String r4 = r7.mo18981u()     // Catch:{ all -> 0x04dd }
            r0.putString(r3, r4)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzan r3 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_au"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r0)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r3
            r4 = 0
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.mo19211a(r3, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x024a
        L_0x0248:
            r8 = r4
        L_0x0249:
            r4 = 0
        L_0x024a:
            r21.mo19226c(r22)     // Catch:{ all -> 0x04dd }
            if (r6 != 0) goto L_0x025c
            com.google.android.gms.measurement.internal.d r0 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = r2.f5814P     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = "_f"
            com.google.android.gms.measurement.internal.k r0 = r0.mo18843a(r3, r7)     // Catch:{ all -> 0x04dd }
            goto L_0x026d
        L_0x025c:
            r3 = 1
            if (r6 != r3) goto L_0x026c
            com.google.android.gms.measurement.internal.d r0 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = r2.f5814P     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = "_v"
            com.google.android.gms.measurement.internal.k r0 = r0.mo18843a(r3, r7)     // Catch:{ all -> 0x04dd }
            goto L_0x026d
        L_0x026c:
            r0 = 0
        L_0x026d:
            if (r0 != 0) goto L_0x04b2
            r11 = 3600000(0x36ee80, double:1.7786363E-317)
            long r13 = r9 / r11
            r15 = 1
            long r13 = r13 + r15
            long r13 = r13 * r11
            java.lang.String r0 = "_dac"
            java.lang.String r3 = "_r"
            java.lang.String r7 = "_c"
            java.lang.String r15 = "_et"
            if (r6 != 0) goto L_0x0415
            com.google.android.gms.measurement.internal.zzkq r6 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_fot"
            java.lang.Long r16 = java.lang.Long.valueOf(r13)     // Catch:{ all -> 0x04dd }
            java.lang.String r20 = "auto"
            r11 = r6
            r13 = r9
            r4 = r15
            r15 = r16
            r16 = r20
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.mo19213a(r6, r2)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19097m()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.f5815Q     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.C3135o.f5410O     // Catch:{ all -> 0x04dd }
            boolean r6 = r6.mo19154e(r11, r12)     // Catch:{ all -> 0x04dd }
            if (r6 == 0) goto L_0x02b8
            r21.m9070z()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.j5 r6 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.y4 r6 = r6.mo19101t()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.f5814P     // Catch:{ all -> 0x04dd }
            r6.mo19402a(r11)     // Catch:{ all -> 0x04dd }
        L_0x02b8:
            r21.m9070z()     // Catch:{ all -> 0x04dd }
            r21.mo19235m()     // Catch:{ all -> 0x04dd }
            android.os.Bundle r6 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r6.<init>()     // Catch:{ all -> 0x04dd }
            r11 = 1
            r6.putLong(r7, r11)     // Catch:{ all -> 0x04dd }
            r6.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
            r11 = 0
            r6.putLong(r5, r11)     // Catch:{ all -> 0x04dd }
            r6.putLong(r8, r11)     // Catch:{ all -> 0x04dd }
            r3 = r19
            r6.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = "_sysu"
            r6.putLong(r7, r11)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r7 = r7.mo19097m()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r12 = com.google.android.gms.measurement.internal.C3135o.f5430Y     // Catch:{ all -> 0x04dd }
            boolean r7 = r7.mo19154e(r11, r12)     // Catch:{ all -> 0x04dd }
            r11 = 1
            if (r7 == 0) goto L_0x02f2
            r6.putLong(r4, r11)     // Catch:{ all -> 0x04dd }
        L_0x02f2:
            boolean r7 = r2.f5830f0     // Catch:{ all -> 0x04dd }
            if (r7 == 0) goto L_0x02f9
            r6.putLong(r0, r11)     // Catch:{ all -> 0x04dd }
        L_0x02f9:
            com.google.android.gms.measurement.internal.d r0 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            java.lang.String r7 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.common.internal.C2258v.m5639b(r7)     // Catch:{ all -> 0x04dd }
            r0.mo18881c()     // Catch:{ all -> 0x04dd }
            r0.mo19284q()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "first_open_count"
            long r13 = r0.mo18873h(r7, r11)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x04dd }
            android.content.Context r0 = r0.mo19016n()     // Catch:{ all -> 0x04dd }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ all -> 0x04dd }
            if (r0 != 0) goto L_0x0336
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = "PackageManager is null, first open report might be inaccurate. appId"
            java.lang.String r5 = r2.f5814P     // Catch:{ all -> 0x04dd }
            java.lang.Object r5 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r5)     // Catch:{ all -> 0x04dd }
            r0.mo19043a(r3, r5)     // Catch:{ all -> 0x04dd }
            r19 = r8
            r7 = r13
        L_0x0332:
            r11 = 0
            goto L_0x03f8
        L_0x0336:
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ NameNotFoundException -> 0x0348 }
            android.content.Context r0 = r0.mo19016n()     // Catch:{ NameNotFoundException -> 0x0348 }
            com.google.android.gms.common.j.b r0 = com.google.android.gms.common.p090j.C2283c.m5685a(r0)     // Catch:{ NameNotFoundException -> 0x0348 }
            java.lang.String r7 = r2.f5814P     // Catch:{ NameNotFoundException -> 0x0348 }
            r11 = 0
            android.content.pm.PackageInfo r0 = r0.mo17060b(r7, r11)     // Catch:{ NameNotFoundException -> 0x0348 }
            goto L_0x035f
        L_0x0348:
            r0 = move-exception
            com.google.android.gms.measurement.internal.j5 r7 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo19001t()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "Package info is null, first open report might be inaccurate. appId"
            java.lang.String r12 = r2.f5814P     // Catch:{ all -> 0x04dd }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r12)     // Catch:{ all -> 0x04dd }
            r7.mo19044a(r11, r12, r0)     // Catch:{ all -> 0x04dd }
            r0 = 0
        L_0x035f:
            if (r0 == 0) goto L_0x03b0
            long r11 = r0.firstInstallTime     // Catch:{ all -> 0x04dd }
            r15 = 0
            int r7 = (r11 > r15 ? 1 : (r11 == r15 ? 0 : -1))
            if (r7 == 0) goto L_0x03b0
            long r11 = r0.firstInstallTime     // Catch:{ all -> 0x04dd }
            r19 = r8
            long r7 = r0.lastUpdateTime     // Catch:{ all -> 0x04dd }
            int r0 = (r11 > r7 ? 1 : (r11 == r7 ? 0 : -1))
            if (r0 == 0) goto L_0x0394
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r0 = r0.mo19097m()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5407M0     // Catch:{ all -> 0x04dd }
            boolean r0 = r0.mo19146a(r7)     // Catch:{ all -> 0x04dd }
            if (r0 == 0) goto L_0x038d
            r7 = 0
            int r0 = (r13 > r7 ? 1 : (r13 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0392
            r7 = 1
            r6.putLong(r5, r7)     // Catch:{ all -> 0x04dd }
            goto L_0x0392
        L_0x038d:
            r7 = 1
            r6.putLong(r5, r7)     // Catch:{ all -> 0x04dd }
        L_0x0392:
            r0 = 0
            goto L_0x0395
        L_0x0394:
            r0 = 1
        L_0x0395:
            com.google.android.gms.measurement.internal.zzkq r5 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_fi"
            if (r0 == 0) goto L_0x039e
            r7 = 1
            goto L_0x03a0
        L_0x039e:
            r7 = 0
        L_0x03a0:
            java.lang.Long r15 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x04dd }
            java.lang.String r16 = "auto"
            r11 = r5
            r7 = r13
            r13 = r9
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.mo19213a(r5, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x03b3
        L_0x03b0:
            r19 = r8
            r7 = r13
        L_0x03b3:
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ NameNotFoundException -> 0x03c5 }
            android.content.Context r0 = r0.mo19016n()     // Catch:{ NameNotFoundException -> 0x03c5 }
            com.google.android.gms.common.j.b r0 = com.google.android.gms.common.p090j.C2283c.m5685a(r0)     // Catch:{ NameNotFoundException -> 0x03c5 }
            java.lang.String r5 = r2.f5814P     // Catch:{ NameNotFoundException -> 0x03c5 }
            r11 = 0
            android.content.pm.ApplicationInfo r0 = r0.mo17055a(r5, r11)     // Catch:{ NameNotFoundException -> 0x03c5 }
            goto L_0x03dc
        L_0x03c5:
            r0 = move-exception
            com.google.android.gms.measurement.internal.j5 r5 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.f4 r5 = r5.mo19015l()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.h4 r5 = r5.mo19001t()     // Catch:{ all -> 0x04dd }
            java.lang.String r11 = "Application info is null, first open report might be inaccurate. appId"
            java.lang.String r12 = r2.f5814P     // Catch:{ all -> 0x04dd }
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r12)     // Catch:{ all -> 0x04dd }
            r5.mo19044a(r11, r12, r0)     // Catch:{ all -> 0x04dd }
            r0 = 0
        L_0x03dc:
            if (r0 == 0) goto L_0x0332
            int r5 = r0.flags     // Catch:{ all -> 0x04dd }
            r11 = 1
            r5 = r5 & r11
            if (r5 == 0) goto L_0x03e9
            r11 = 1
            r6.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
        L_0x03e9:
            int r0 = r0.flags     // Catch:{ all -> 0x04dd }
            r0 = r0 & 128(0x80, float:1.794E-43)
            if (r0 == 0) goto L_0x0332
            java.lang.String r0 = "_sysu"
            r11 = 1
            r6.putLong(r0, r11)     // Catch:{ all -> 0x04dd }
            goto L_0x0332
        L_0x03f8:
            int r0 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r0 < 0) goto L_0x0401
            r3 = r19
            r6.putLong(r3, r7)     // Catch:{ all -> 0x04dd }
        L_0x0401:
            com.google.android.gms.measurement.internal.zzan r0 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_f"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r6)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r0
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.mo19211a(r0, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x046d
        L_0x0415:
            r4 = r15
            r5 = 1
            if (r6 != r5) goto L_0x046d
            com.google.android.gms.measurement.internal.zzkq r5 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_fvt"
            java.lang.Long r15 = java.lang.Long.valueOf(r13)     // Catch:{ all -> 0x04dd }
            java.lang.String r16 = "auto"
            r11 = r5
            r13 = r9
            r11.<init>(r12, r13, r15, r16)     // Catch:{ all -> 0x04dd }
            r1.mo19213a(r5, r2)     // Catch:{ all -> 0x04dd }
            r21.m9070z()     // Catch:{ all -> 0x04dd }
            r21.mo19235m()     // Catch:{ all -> 0x04dd }
            android.os.Bundle r5 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r5.<init>()     // Catch:{ all -> 0x04dd }
            r11 = 1
            r5.putLong(r7, r11)     // Catch:{ all -> 0x04dd }
            r5.putLong(r3, r11)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r3 = r3.mo19097m()     // Catch:{ all -> 0x04dd }
            java.lang.String r6 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5430Y     // Catch:{ all -> 0x04dd }
            boolean r3 = r3.mo19154e(r6, r7)     // Catch:{ all -> 0x04dd }
            r6 = 1
            if (r3 == 0) goto L_0x0453
            r5.putLong(r4, r6)     // Catch:{ all -> 0x04dd }
        L_0x0453:
            boolean r3 = r2.f5830f0     // Catch:{ all -> 0x04dd }
            if (r3 == 0) goto L_0x045a
            r5.putLong(r0, r6)     // Catch:{ all -> 0x04dd }
        L_0x045a:
            com.google.android.gms.measurement.internal.zzan r0 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_v"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r5)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r0
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.mo19211a(r0, r2)     // Catch:{ all -> 0x04dd }
        L_0x046d:
            com.google.android.gms.measurement.internal.j5 r0 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r0 = r0.mo19097m()     // Catch:{ all -> 0x04dd }
            java.lang.String r3 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.C3135o.f5432Z     // Catch:{ all -> 0x04dd }
            boolean r0 = r0.mo19154e(r3, r5)     // Catch:{ all -> 0x04dd }
            if (r0 != 0) goto L_0x04ce
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r0.<init>()     // Catch:{ all -> 0x04dd }
            r5 = 1
            r0.putLong(r4, r5)     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.j5 r3 = r1.f5517i     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.la r3 = r3.mo19097m()     // Catch:{ all -> 0x04dd }
            java.lang.String r4 = r2.f5814P     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.C3135o.f5430Y     // Catch:{ all -> 0x04dd }
            boolean r3 = r3.mo19154e(r4, r5)     // Catch:{ all -> 0x04dd }
            if (r3 == 0) goto L_0x049e
            java.lang.String r3 = "_fr"
            r4 = 1
            r0.putLong(r3, r4)     // Catch:{ all -> 0x04dd }
        L_0x049e:
            com.google.android.gms.measurement.internal.zzan r3 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_e"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r0)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r3
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.mo19211a(r3, r2)     // Catch:{ all -> 0x04dd }
            goto L_0x04ce
        L_0x04b2:
            boolean r0 = r2.f5822X     // Catch:{ all -> 0x04dd }
            if (r0 == 0) goto L_0x04ce
            android.os.Bundle r0 = new android.os.Bundle     // Catch:{ all -> 0x04dd }
            r0.<init>()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.zzan r3 = new com.google.android.gms.measurement.internal.zzan     // Catch:{ all -> 0x04dd }
            java.lang.String r12 = "_cd"
            com.google.android.gms.measurement.internal.zzam r13 = new com.google.android.gms.measurement.internal.zzam     // Catch:{ all -> 0x04dd }
            r13.<init>(r0)     // Catch:{ all -> 0x04dd }
            java.lang.String r14 = "auto"
            r11 = r3
            r15 = r9
            r11.<init>(r12, r13, r14, r15)     // Catch:{ all -> 0x04dd }
            r1.mo19211a(r3, r2)     // Catch:{ all -> 0x04dd }
        L_0x04ce:
            com.google.android.gms.measurement.internal.d r0 = r21.mo19229e()     // Catch:{ all -> 0x04dd }
            r0.mo18874u()     // Catch:{ all -> 0x04dd }
            com.google.android.gms.measurement.internal.d r0 = r21.mo19229e()
            r0.mo18879z()
            return
        L_0x04dd:
            r0 = move-exception
            com.google.android.gms.measurement.internal.d r2 = r21.mo19229e()
            r2.mo18879z()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.mo19222b(com.google.android.gms.measurement.internal.zzm):void");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo19223b(zzv zzv) {
        zzm a = m9049a(zzv.f5836P);
        if (a != null) {
            mo19224b(zzv, a);
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo19224b(zzv zzv, zzm zzm) {
        C2258v.m5629a(zzv);
        C2258v.m5639b(zzv.f5836P);
        C2258v.m5629a(zzv.f5838R);
        C2258v.m5639b(zzv.f5838R.f5808Q);
        m9070z();
        mo19235m();
        if (m9064e(zzm)) {
            if (!zzm.f5821W) {
                mo19226c(zzm);
                return;
            }
            mo19229e().mo18878y();
            try {
                mo19226c(zzm);
                zzv d = mo19229e().mo18864d(zzv.f5836P, zzv.f5838R.f5808Q);
                if (d != null) {
                    this.f5517i.mo19015l().mo18995A().mo19044a("Removing conditional user property", zzv.f5836P, this.f5517i.mo19105x().mo18824c(zzv.f5838R.f5808Q));
                    mo19229e().mo18866e(zzv.f5836P, zzv.f5838R.f5808Q);
                    if (d.f5840T) {
                        mo19229e().mo18860b(zzv.f5836P, zzv.f5838R.f5808Q);
                    }
                    if (zzv.f5846Z != null) {
                        Bundle bundle = null;
                        if (zzv.f5846Z.f5804Q != null) {
                            bundle = zzv.f5846Z.f5804Q.mo19462e();
                        }
                        Bundle bundle2 = bundle;
                        m9063b(this.f5517i.mo19104w().mo19428a(zzv.f5836P, zzv.f5846Z.f5803P, bundle2, d.f5837Q, zzv.f5846Z.f5806S, true, false), zzm);
                    }
                } else {
                    this.f5517i.mo19015l().mo19004w().mo19044a("Conditional user property doesn't exist", C3032f4.m8621a(zzv.f5836P), this.f5517i.mo19105x().mo18824c(zzv.f5838R.f5808Q));
                }
                mo19229e().mo18874u();
            } finally {
                mo19229e().mo18879z();
            }
        }
    }

    /* renamed from: a */
    private static void m9052a(C2763w0.C2764a aVar) {
        aVar.mo18076b(Long.MAX_VALUE);
        aVar.mo18081c(Long.MIN_VALUE);
        for (int i = 0; i < aVar.mo18105k(); i++) {
            C2701s0 b = aVar.mo18075b(i);
            if (b.mo17851s() < aVar.mo18115o()) {
                aVar.mo18076b(b.mo17851s());
            }
            if (b.mo17851s() > aVar.mo18117p()) {
                aVar.mo18081c(b.mo17851s());
            }
        }
    }

    /* renamed from: a */
    private final void m9053a(C2763w0.C2764a aVar, long j, boolean z) {
        C3234w9 w9Var;
        String str = z ? "_se" : "_lte";
        C3234w9 c = mo19229e().mo18863c(aVar.mo18121t(), str);
        if (c == null || c.f5737e == null) {
            w9Var = new C3234w9(aVar.mo18121t(), "auto", str, this.f5517i.mo19017o().mo17132a(), Long.valueOf(j));
        } else {
            w9Var = new C3234w9(aVar.mo18121t(), "auto", str, this.f5517i.mo19017o().mo17132a(), Long.valueOf(((Long) c.f5737e).longValue() + j));
        }
        C2414a1.C2415a x = C2414a1.m5972x();
        x.mo17259a(str);
        x.mo17258a(this.f5517i.mo19017o().mo17132a());
        x.mo17260b(((Long) w9Var.f5737e).longValue());
        C2414a1 a1Var = (C2414a1) x.mo17679i();
        boolean z2 = false;
        int a = C3223v9.m9260a(aVar, str);
        if (a >= 0) {
            aVar.mo18066a(a, a1Var);
            z2 = true;
        }
        if (!z2) {
            aVar.mo18070a(a1Var);
        }
        if (j > 0) {
            mo19229e().mo18855a(w9Var);
            String str2 = z ? "session-scoped" : "lifetime";
            if (!C2489ea.m6262b() || !this.f5517i.mo19097m().mo19154e(aVar.mo18121t(), C3135o.f5431Y0)) {
                this.f5517i.mo19015l().mo18995A().mo19044a("Updated engagement user property. scope, value", str2, w9Var.f5737e);
            } else {
                this.f5517i.mo19015l().mo18996B().mo19044a("Updated engagement user property. scope, value", str2, w9Var.f5737e);
            }
        }
    }

    /* renamed from: a */
    private final boolean m9058a(C2701s0.C2702a aVar, C2701s0.C2702a aVar2) {
        String str;
        C2258v.m5636a("_e".equals(aVar.mo17869m()));
        mo19232h();
        C2733u0 a = C3223v9.m9261a((C2701s0) aVar.mo17679i(), "_sc");
        String str2 = null;
        if (a == null) {
            str = null;
        } else {
            str = a.mo17924q();
        }
        mo19232h();
        C2733u0 a2 = C3223v9.m9261a((C2701s0) aVar2.mo17679i(), "_pc");
        if (a2 != null) {
            str2 = a2.mo17924q();
        }
        if (str2 == null || !str2.equals(str)) {
            return false;
        }
        m9061b(aVar, aVar2);
        return true;
    }

    /* renamed from: a */
    private static void m9051a(C2701s0.C2702a aVar, @NonNull String str) {
        List<C2733u0> j = aVar.mo17866j();
        for (int i = 0; i < j.size(); i++) {
            if (str.equals(j.get(i).mo17922o())) {
                aVar.mo17864b(i);
                return;
            }
        }
    }

    /* renamed from: a */
    private static void m9050a(C2701s0.C2702a aVar, int i, String str) {
        List<C2733u0> j = aVar.mo17866j();
        int i2 = 0;
        while (i2 < j.size()) {
            if (!"_err".equals(j.get(i2).mo17922o())) {
                i2++;
            } else {
                return;
            }
        }
        C2733u0.C2734a y = C2733u0.m7347y();
        y.mo17933a("_err");
        y.mo17932a(Long.valueOf((long) i).longValue());
        C2733u0.C2734a y2 = C2733u0.m7347y();
        y2.mo17933a("_ev");
        y2.mo17934b(str);
        aVar.mo17860a((C2733u0) y.mo17679i());
        aVar.mo17860a((C2733u0) y2.mo17679i());
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19209a(int i, Throwable th, byte[] bArr, String str) {
        C3003d e;
        m9070z();
        mo19235m();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                this.f5526r = false;
                m9065u();
                throw th2;
            }
        }
        List<Long> list = this.f5530v;
        this.f5530v = null;
        boolean z = true;
        if ((i == 200 || i == 204) && th == null) {
            try {
                this.f5517i.mo19098p().f5607e.mo19327a(this.f5517i.mo19017o().mo17132a());
                this.f5517i.mo19098p().f5608f.mo19327a(0);
                m9044C();
                this.f5517i.mo19015l().mo18996B().mo19044a("Successful upload. Got network response. code, size", Integer.valueOf(i), Integer.valueOf(bArr.length));
                mo19229e().mo18878y();
                try {
                    for (Long l : list) {
                        try {
                            e = mo19229e();
                            long longValue = l.longValue();
                            e.mo18881c();
                            e.mo19284q();
                            if (e.mo18875v().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                throw new SQLiteException("Deleted fewer rows from queue than expected");
                            }
                        } catch (SQLiteException e2) {
                            e.mo19015l().mo19001t().mo19043a("Failed to delete a bundle in a queue table", e2);
                            throw e2;
                        } catch (SQLiteException e3) {
                            if (this.f5531w == null || !this.f5531w.contains(l)) {
                                throw e3;
                            }
                        }
                    }
                    mo19229e().mo18874u();
                    mo19229e().mo18879z();
                    this.f5531w = null;
                    if (!mo19227d().mo19073u() || !m9043B()) {
                        this.f5532x = -1;
                        m9044C();
                    } else {
                        mo19236p();
                    }
                    this.f5521m = 0;
                } catch (Throwable th3) {
                    mo19229e().mo18879z();
                    throw th3;
                }
            } catch (SQLiteException e4) {
                this.f5517i.mo19015l().mo19001t().mo19043a("Database error while trying to delete uploaded bundles", e4);
                this.f5521m = this.f5517i.mo19017o().elapsedRealtime();
                this.f5517i.mo19015l().mo18996B().mo19043a("Disable upload, time", Long.valueOf(this.f5521m));
            }
        } else {
            this.f5517i.mo19015l().mo18996B().mo19044a("Network upload failed. Will retry later. code, error", Integer.valueOf(i), th);
            this.f5517i.mo19098p().f5608f.mo19327a(this.f5517i.mo19017o().mo17132a());
            if (i != 503) {
                if (i != 429) {
                    z = false;
                }
            }
            if (z) {
                this.f5517i.mo19098p().f5609g.mo19327a(this.f5517i.mo19017o().mo17132a());
            }
            mo19229e().mo18852a(list);
            m9044C();
        }
        this.f5526r = false;
        m9065u();
    }

    @WorkerThread
    /* renamed from: a */
    private final void m9054a(C3021e5 e5Var) {
        ArrayMap arrayMap;
        m9070z();
        if (!C2774wa.m7753b() || !this.f5517i.mo19097m().mo19154e(e5Var.mo18967l(), C3135o.f5395G0)) {
            if (TextUtils.isEmpty(e5Var.mo18971n()) && TextUtils.isEmpty(e5Var.mo18973o())) {
                mo19218a(e5Var.mo18967l(), 204, null, null, null);
                return;
            }
        } else if (TextUtils.isEmpty(e5Var.mo18971n()) && TextUtils.isEmpty(e5Var.mo18975p()) && TextUtils.isEmpty(e5Var.mo18973o())) {
            mo19218a(e5Var.mo18967l(), 204, null, null, null);
            return;
        }
        String a = this.f5517i.mo19097m().mo19144a(e5Var);
        try {
            URL url = new URL(a);
            this.f5517i.mo19015l().mo18996B().mo19043a("Fetching remote configuration", e5Var.mo18967l());
            C2626n0 a2 = mo19225c().mo18890a(e5Var.mo18967l());
            String b = mo19225c().mo18892b(e5Var.mo18967l());
            if (a2 == null || TextUtils.isEmpty(b)) {
                arrayMap = null;
            } else {
                ArrayMap arrayMap2 = new ArrayMap();
                arrayMap2.put("If-Modified-Since", b);
                arrayMap = arrayMap2;
            }
            this.f5525q = true;
            C3080j4 d = mo19227d();
            String l = e5Var.mo18967l();
            C3201t9 t9Var = new C3201t9(this);
            d.mo18881c();
            d.mo19284q();
            C2258v.m5629a(url);
            C2258v.m5629a(t9Var);
            d.mo19014j().mo19030b(new C3128n4(d, l, url, null, arrayMap, t9Var));
        } catch (MalformedURLException unused) {
            this.f5517i.mo19015l().mo19001t().mo19044a("Failed to parse config URL. Not fetching. appId", C3032f4.m8621a(e5Var.mo18967l()), a);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x013a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x014a A[Catch:{ all -> 0x018d, all -> 0x0196 }] */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo19218a(java.lang.String r7, int r8, java.lang.Throwable r9, byte[] r10, java.util.Map<java.lang.String, java.util.List<java.lang.String>> r11) {
        /*
            r6 = this;
            r6.m9070z()
            r6.mo19235m()
            com.google.android.gms.common.internal.C2258v.m5639b(r7)
            r0 = 0
            if (r10 != 0) goto L_0x000e
            byte[] r10 = new byte[r0]     // Catch:{ all -> 0x0196 }
        L_0x000e:
            com.google.android.gms.measurement.internal.j5 r1 = r6.f5517i     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()     // Catch:{ all -> 0x0196 }
            java.lang.String r2 = "onConfigFetched. Response size"
            int r3 = r10.length     // Catch:{ all -> 0x0196 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0196 }
            r1.mo19043a(r2, r3)     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.d r1 = r6.mo19229e()     // Catch:{ all -> 0x0196 }
            r1.mo18878y()     // Catch:{ all -> 0x0196 }
            com.google.android.gms.measurement.internal.d r1 = r6.mo19229e()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.e5 r1 = r1.mo18858b(r7)     // Catch:{ all -> 0x018d }
            r2 = 200(0xc8, float:2.8E-43)
            r3 = 304(0x130, float:4.26E-43)
            r4 = 1
            if (r8 == r2) goto L_0x003e
            r2 = 204(0xcc, float:2.86E-43)
            if (r8 == r2) goto L_0x003e
            if (r8 != r3) goto L_0x0042
        L_0x003e:
            if (r9 != 0) goto L_0x0042
            r2 = 1
            goto L_0x0043
        L_0x0042:
            r2 = 0
        L_0x0043:
            if (r1 != 0) goto L_0x005a
            com.google.android.gms.measurement.internal.j5 r8 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.f4 r8 = r8.mo19015l()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19004w()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "App does not exist in onConfigFetched. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)     // Catch:{ all -> 0x018d }
            r8.mo19043a(r9, r7)     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x005a:
            r5 = 404(0x194, float:5.66E-43)
            if (r2 != 0) goto L_0x00ca
            if (r8 != r5) goto L_0x0061
            goto L_0x00ca
        L_0x0061:
            com.google.android.gms.measurement.internal.j5 r10 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.e r10 = r10.mo19017o()     // Catch:{ all -> 0x018d }
            long r10 = r10.mo17132a()     // Catch:{ all -> 0x018d }
            r1.mo18961i(r10)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.d r10 = r6.mo19229e()     // Catch:{ all -> 0x018d }
            r10.mo18850a(r1)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.j5 r10 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.f4 r10 = r10.mo19015l()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.h4 r10 = r10.mo18996B()     // Catch:{ all -> 0x018d }
            java.lang.String r11 = "Fetching config failed. code, error"
            java.lang.Integer r1 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x018d }
            r10.mo19044a(r11, r1, r9)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.d5 r9 = r6.mo19225c()     // Catch:{ all -> 0x018d }
            r9.mo18894c(r7)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.j5 r7 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.s4 r7 = r7.mo19098p()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.t4 r7 = r7.f5608f     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.j5 r9 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.e r9 = r9.mo19017o()     // Catch:{ all -> 0x018d }
            long r9 = r9.mo17132a()     // Catch:{ all -> 0x018d }
            r7.mo19327a(r9)     // Catch:{ all -> 0x018d }
            r7 = 503(0x1f7, float:7.05E-43)
            if (r8 == r7) goto L_0x00ae
            r7 = 429(0x1ad, float:6.01E-43)
            if (r8 != r7) goto L_0x00ad
            goto L_0x00ae
        L_0x00ad:
            r4 = 0
        L_0x00ae:
            if (r4 == 0) goto L_0x00c5
            com.google.android.gms.measurement.internal.j5 r7 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.s4 r7 = r7.mo19098p()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.t4 r7 = r7.f5609g     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.j5 r8 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.e r8 = r8.mo19017o()     // Catch:{ all -> 0x018d }
            long r8 = r8.mo17132a()     // Catch:{ all -> 0x018d }
            r7.mo19327a(r8)     // Catch:{ all -> 0x018d }
        L_0x00c5:
            r6.m9044C()     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x00ca:
            r9 = 0
            if (r11 == 0) goto L_0x00d6
            java.lang.String r2 = "Last-Modified"
            java.lang.Object r11 = r11.get(r2)     // Catch:{ all -> 0x018d }
            java.util.List r11 = (java.util.List) r11     // Catch:{ all -> 0x018d }
            goto L_0x00d7
        L_0x00d6:
            r11 = r9
        L_0x00d7:
            if (r11 == 0) goto L_0x00e6
            int r2 = r11.size()     // Catch:{ all -> 0x018d }
            if (r2 <= 0) goto L_0x00e6
            java.lang.Object r11 = r11.get(r0)     // Catch:{ all -> 0x018d }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ all -> 0x018d }
            goto L_0x00e7
        L_0x00e6:
            r11 = r9
        L_0x00e7:
            if (r8 == r5) goto L_0x0103
            if (r8 != r3) goto L_0x00ec
            goto L_0x0103
        L_0x00ec:
            com.google.android.gms.measurement.internal.d5 r9 = r6.mo19225c()     // Catch:{ all -> 0x018d }
            boolean r9 = r9.mo18891a(r7, r10, r11)     // Catch:{ all -> 0x018d }
            if (r9 != 0) goto L_0x0124
            com.google.android.gms.measurement.internal.d r7 = r6.mo19229e()     // Catch:{ all -> 0x0196 }
            r7.mo18879z()     // Catch:{ all -> 0x0196 }
            r6.f5525q = r0
            r6.m9065u()
            return
        L_0x0103:
            com.google.android.gms.measurement.internal.d5 r11 = r6.mo19225c()     // Catch:{ all -> 0x018d }
            com.google.android.gms.internal.measurement.n0 r11 = r11.mo18890a(r7)     // Catch:{ all -> 0x018d }
            if (r11 != 0) goto L_0x0124
            com.google.android.gms.measurement.internal.d5 r11 = r6.mo19225c()     // Catch:{ all -> 0x018d }
            boolean r9 = r11.mo18891a(r7, r9, r9)     // Catch:{ all -> 0x018d }
            if (r9 != 0) goto L_0x0124
            com.google.android.gms.measurement.internal.d r7 = r6.mo19229e()     // Catch:{ all -> 0x0196 }
            r7.mo18879z()     // Catch:{ all -> 0x0196 }
            r6.f5525q = r0
            r6.m9065u()
            return
        L_0x0124:
            com.google.android.gms.measurement.internal.j5 r9 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.common.util.e r9 = r9.mo19017o()     // Catch:{ all -> 0x018d }
            long r2 = r9.mo17132a()     // Catch:{ all -> 0x018d }
            r1.mo18957h(r2)     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.d r9 = r6.mo19229e()     // Catch:{ all -> 0x018d }
            r9.mo18850a(r1)     // Catch:{ all -> 0x018d }
            if (r8 != r5) goto L_0x014a
            com.google.android.gms.measurement.internal.j5 r8 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.f4 r8 = r8.mo19015l()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19006y()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "Config not found. Using empty config. appId"
            r8.mo19043a(r9, r7)     // Catch:{ all -> 0x018d }
            goto L_0x0162
        L_0x014a:
            com.google.android.gms.measurement.internal.j5 r7 = r6.f5517i     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.f4 r7 = r7.mo19015l()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.h4 r7 = r7.mo18996B()     // Catch:{ all -> 0x018d }
            java.lang.String r9 = "Successfully fetched config. Got network response. code, size"
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x018d }
            int r10 = r10.length     // Catch:{ all -> 0x018d }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)     // Catch:{ all -> 0x018d }
            r7.mo19044a(r9, r8, r10)     // Catch:{ all -> 0x018d }
        L_0x0162:
            com.google.android.gms.measurement.internal.j4 r7 = r6.mo19227d()     // Catch:{ all -> 0x018d }
            boolean r7 = r7.mo19073u()     // Catch:{ all -> 0x018d }
            if (r7 == 0) goto L_0x0176
            boolean r7 = r6.m9043B()     // Catch:{ all -> 0x018d }
            if (r7 == 0) goto L_0x0176
            r6.mo19236p()     // Catch:{ all -> 0x018d }
            goto L_0x0179
        L_0x0176:
            r6.m9044C()     // Catch:{ all -> 0x018d }
        L_0x0179:
            com.google.android.gms.measurement.internal.d r7 = r6.mo19229e()     // Catch:{ all -> 0x018d }
            r7.mo18874u()     // Catch:{ all -> 0x018d }
            com.google.android.gms.measurement.internal.d r7 = r6.mo19229e()     // Catch:{ all -> 0x0196 }
            r7.mo18879z()     // Catch:{ all -> 0x0196 }
            r6.f5525q = r0
            r6.m9065u()
            return
        L_0x018d:
            r7 = move-exception
            com.google.android.gms.measurement.internal.d r8 = r6.mo19229e()     // Catch:{ all -> 0x0196 }
            r8.mo18879z()     // Catch:{ all -> 0x0196 }
            throw r7     // Catch:{ all -> 0x0196 }
        L_0x0196:
            r7 = move-exception
            r6.f5525q = r0
            r6.m9065u()
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.mo19218a(java.lang.String, int, java.lang.Throwable, byte[], java.util.Map):void");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19217a(Runnable runnable) {
        m9070z();
        if (this.f5522n == null) {
            this.f5522n = new ArrayList();
        }
        this.f5522n.add(runnable);
    }

    @WorkerThread
    /* renamed from: a */
    private final int m9045a(FileChannel fileChannel) {
        m9070z();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.f5517i.mo19015l().mo19001t().mo19042a("Bad channel to read from");
            return 0;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        try {
            fileChannel.position(0L);
            int read = fileChannel.read(allocate);
            if (read != 4) {
                if (read != -1) {
                    this.f5517i.mo19015l().mo19004w().mo19043a("Unexpected data length. Bytes read", Integer.valueOf(read));
                }
                return 0;
            }
            allocate.flip();
            return allocate.getInt();
        } catch (IOException e) {
            this.f5517i.mo19015l().mo19001t().mo19043a("Failed to read from channel", e);
            return 0;
        }
    }

    @WorkerThread
    /* renamed from: a */
    private final boolean m9057a(int i, FileChannel fileChannel) {
        m9070z();
        if (fileChannel == null || !fileChannel.isOpen()) {
            this.f5517i.mo19015l().mo19001t().mo19042a("Bad channel to read from");
            return false;
        }
        ByteBuffer allocate = ByteBuffer.allocate(4);
        allocate.putInt(i);
        allocate.flip();
        try {
            fileChannel.truncate(0L);
            if (this.f5517i.mo19097m().mo19146a(C3135o.f5421T0) && Build.VERSION.SDK_INT <= 19) {
                fileChannel.position(0L);
            }
            fileChannel.write(allocate);
            fileChannel.force(true);
            if (fileChannel.size() != 4) {
                this.f5517i.mo19015l().mo19001t().mo19043a("Error writing to channel. Bytes written", Long.valueOf(fileChannel.size()));
            }
            return true;
        } catch (IOException e) {
            this.f5517i.mo19015l().mo19001t().mo19043a("Failed to write to channel", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19214a(zzm zzm) {
        if (this.f5530v != null) {
            this.f5531w = new ArrayList();
            this.f5531w.addAll(this.f5530v);
        }
        C3003d e = mo19229e();
        String str = zzm.f5814P;
        C2258v.m5639b(str);
        e.mo18881c();
        e.mo19284q();
        try {
            SQLiteDatabase v = e.mo18875v();
            String[] strArr = {str};
            int delete = v.delete("apps", "app_id=?", strArr) + 0 + v.delete("events", "app_id=?", strArr) + v.delete("user_attributes", "app_id=?", strArr) + v.delete("conditional_properties", "app_id=?", strArr) + v.delete("raw_events", "app_id=?", strArr) + v.delete("raw_events_metadata", "app_id=?", strArr) + v.delete("queue", "app_id=?", strArr) + v.delete("audience_filter_values", "app_id=?", strArr) + v.delete("main_event_params", "app_id=?", strArr);
            if (delete > 0) {
                e.mo19015l().mo18996B().mo19044a("Reset analytics data. app, records", str, Integer.valueOf(delete));
            }
        } catch (SQLiteException e2) {
            e.mo19015l().mo19001t().mo19044a("Error resetting analytics data. appId, error", C3032f4.m8621a(str), e2);
        }
        if (!C2789x9.m7775b() || !this.f5517i.mo19097m().mo19146a(C3135o.f5405L0)) {
            zzm a = m9048a(this.f5517i.mo19016n(), zzm.f5814P, zzm.f5815Q, zzm.f5821W, zzm.f5828d0, zzm.f5829e0, zzm.f5826b0, zzm.f5831g0, zzm.f5835k0);
            if (zzm.f5821W) {
                mo19222b(a);
            }
        } else if (zzm.f5821W) {
            mo19222b(zzm);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, int, long, int, boolean, boolean, int, java.lang.String, ?[OBJECT, ARRAY], int, ?[OBJECT, ARRAY], java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void */
    /* renamed from: a */
    private final zzm m9048a(Context context, String str, String str2, boolean z, boolean z2, boolean z3, long j, String str3, String str4) {
        String str5;
        String str6;
        int i;
        String str7 = str;
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            this.f5517i.mo19015l().mo19001t().mo19042a("PackageManager is null, can not log app install information");
            return null;
        }
        try {
            str5 = packageManager.getInstallerPackageName(str7);
        } catch (IllegalArgumentException unused) {
            this.f5517i.mo19015l().mo19001t().mo19043a("Error retrieving installer package name. appId", C3032f4.m8621a(str));
            str5 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
        }
        if (str5 == null) {
            str5 = "manual_install";
        } else if ("com.android.vending".equals(str5)) {
            str5 = "";
        }
        String str8 = str5;
        try {
            PackageInfo b = C2283c.m5685a(context).mo17060b(str7, 0);
            if (b != null) {
                CharSequence b2 = C2283c.m5685a(context).mo17061b(str7);
                if (!TextUtils.isEmpty(b2)) {
                    String charSequence = b2.toString();
                }
                str6 = b.versionName;
                i = b.versionCode;
            } else {
                str6 = AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN;
                i = Integer.MIN_VALUE;
            }
            return new zzm(str, str2, str6, (long) i, str8, this.f5517i.mo19097m().mo19157i(), this.f5517i.mo19104w().mo19424a(context, str7), (String) null, z, false, "", 0L, j, 0, z2, z3, false, str3, (Boolean) null, 0L, (List<String>) null, (!C2774wa.m7753b() || !this.f5517i.mo19097m().mo19154e(str7, C3135o.f5395G0)) ? null : str4);
        } catch (PackageManager.NameNotFoundException unused2) {
            this.f5517i.mo19015l().mo19001t().mo19044a("Error retrieving newly installed package info. appId, appName", C3032f4.m8621a(str), AnalyticsEvents.PARAMETER_DIALOG_OUTCOME_VALUE_UNKNOWN);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19213a(zzkq zzkq, zzm zzm) {
        C3087k a;
        m9070z();
        mo19235m();
        if (m9064e(zzm)) {
            if (!zzm.f5821W) {
                mo19226c(zzm);
                return;
            }
            int b = this.f5517i.mo19104w().mo19447b(zzkq.f5808Q);
            if (b != 0) {
                this.f5517i.mo19104w();
                String a2 = C3267z9.m9413a(zzkq.f5808Q, 24, true);
                String str = zzkq.f5808Q;
                this.f5517i.mo19104w().mo19441a(zzm.f5814P, b, "_ev", a2, str != null ? str.length() : 0);
                return;
            }
            int b2 = this.f5517i.mo19104w().mo19448b(zzkq.f5808Q, zzkq.mo19469a());
            if (b2 != 0) {
                this.f5517i.mo19104w();
                String a3 = C3267z9.m9413a(zzkq.f5808Q, 24, true);
                Object a4 = zzkq.mo19469a();
                this.f5517i.mo19104w().mo19441a(zzm.f5814P, b2, "_ev", a3, (a4 == null || (!(a4 instanceof String) && !(a4 instanceof CharSequence))) ? 0 : String.valueOf(a4).length());
                return;
            }
            Object c = this.f5517i.mo19104w().mo19450c(zzkq.f5808Q, zzkq.mo19469a());
            if (c != null) {
                if ("_sid".equals(zzkq.f5808Q) && this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5414Q)) {
                    long j = zzkq.f5809R;
                    String str2 = zzkq.f5812U;
                    long j2 = 0;
                    C3234w9 c2 = mo19229e().mo18863c(zzm.f5814P, "_sno");
                    if (c2 != null) {
                        Object obj = c2.f5737e;
                        if (obj instanceof Long) {
                            j2 = ((Long) obj).longValue();
                            mo19213a(new zzkq("_sno", j, Long.valueOf(j2 + 1), str2), zzm);
                        }
                    }
                    if (c2 != null) {
                        this.f5517i.mo19015l().mo19004w().mo19043a("Retrieved last session number from database does not contain a valid (long) value", c2.f5737e);
                    }
                    if (this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5420T) && (a = mo19229e().mo18843a(zzm.f5814P, "_s")) != null) {
                        j2 = a.f5280c;
                        this.f5517i.mo19015l().mo18996B().mo19043a("Backfill the session number. Last used session number", Long.valueOf(j2));
                    }
                    mo19213a(new zzkq("_sno", j, Long.valueOf(j2 + 1), str2), zzm);
                }
                C3234w9 w9Var = new C3234w9(zzm.f5814P, zzkq.f5812U, zzkq.f5808Q, zzkq.f5809R, c);
                if (!C2489ea.m6262b() || !this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5431Y0)) {
                    this.f5517i.mo19015l().mo18995A().mo19044a("Setting user property", this.f5517i.mo19105x().mo18824c(w9Var.f5735c), c);
                } else {
                    this.f5517i.mo19015l().mo18996B().mo19044a("Setting user property", this.f5517i.mo19105x().mo18824c(w9Var.f5735c), c);
                }
                mo19229e().mo18878y();
                try {
                    mo19226c(zzm);
                    boolean a5 = mo19229e().mo18855a(w9Var);
                    mo19229e().mo18874u();
                    if (!a5) {
                        this.f5517i.mo19015l().mo19001t().mo19044a("Too many unique user properties are set. Ignoring user property", this.f5517i.mo19105x().mo18824c(w9Var.f5735c), w9Var.f5737e);
                        this.f5517i.mo19104w().mo19441a(zzm.f5814P, 9, (String) null, (String) null, 0);
                    } else if (!C2489ea.m6262b() || !this.f5517i.mo19097m().mo19154e(zzm.f5814P, C3135o.f5431Y0)) {
                        this.f5517i.mo19015l().mo18995A().mo19044a("User property set", this.f5517i.mo19105x().mo18824c(w9Var.f5735c), w9Var.f5737e);
                    }
                } finally {
                    mo19229e().mo18879z();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19210a(C3157p9 p9Var) {
        this.f5523o++;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, ?[OBJECT, ARRAY], boolean, int, java.lang.String, long, int, int, boolean, boolean, int, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, java.lang.String, long, long, java.lang.String, boolean, boolean, long, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void
      com.google.android.gms.measurement.internal.zzm.<init>(java.lang.String, java.lang.String, java.lang.String, long, java.lang.String, long, long, java.lang.String, boolean, boolean, java.lang.String, long, long, int, boolean, boolean, boolean, java.lang.String, java.lang.Boolean, long, java.util.List<java.lang.String>, java.lang.String):void */
    @WorkerThread
    /* renamed from: a */
    private final zzm m9049a(String str) {
        String str2 = str;
        C3021e5 b = mo19229e().mo18858b(str2);
        if (b == null || TextUtils.isEmpty(b.mo18981u())) {
            this.f5517i.mo19015l().mo18995A().mo19043a("No app data available; dropping", str2);
            return null;
        }
        Boolean b2 = m9060b(b);
        if (b2 == null || b2.booleanValue()) {
            return new zzm(str, b.mo18971n(), b.mo18981u(), b.mo18982v(), b.mo18983w(), b.mo18984x(), b.mo18985y(), (String) null, b.mo18922A(), false, b.mo18978r(), b.mo18951f(), 0L, 0, b.mo18956g(), b.mo18959h(), false, b.mo18973o(), b.mo18960i(), b.mo18986z(), b.mo18963j(), (!C2774wa.m7753b() || !this.f5517i.mo19097m().mo19154e(str2, C3135o.f5395G0)) ? null : b.mo18975p());
        }
        this.f5517i.mo19015l().mo19001t().mo19043a("App version does not match; dropping. appId", C3032f4.m8621a(str));
        return null;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19215a(zzv zzv) {
        zzm a = m9049a(zzv.f5836P);
        if (a != null) {
            mo19216a(zzv, a);
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19216a(zzv zzv, zzm zzm) {
        C2258v.m5629a(zzv);
        C2258v.m5639b(zzv.f5836P);
        C2258v.m5629a((Object) zzv.f5837Q);
        C2258v.m5629a(zzv.f5838R);
        C2258v.m5639b(zzv.f5838R.f5808Q);
        m9070z();
        mo19235m();
        if (m9064e(zzm)) {
            if (!zzm.f5821W) {
                mo19226c(zzm);
                return;
            }
            zzv zzv2 = new zzv(zzv);
            boolean z = false;
            zzv2.f5840T = false;
            mo19229e().mo18878y();
            try {
                zzv d = mo19229e().mo18864d(zzv2.f5836P, zzv2.f5838R.f5808Q);
                if (d != null && !d.f5837Q.equals(zzv2.f5837Q)) {
                    this.f5517i.mo19015l().mo19004w().mo19045a("Updating a conditional user property with different origin. name, origin, origin (from DB)", this.f5517i.mo19105x().mo18824c(zzv2.f5838R.f5808Q), zzv2.f5837Q, d.f5837Q);
                }
                if (d != null && d.f5840T) {
                    zzv2.f5837Q = d.f5837Q;
                    zzv2.f5839S = d.f5839S;
                    zzv2.f5843W = d.f5843W;
                    zzv2.f5841U = d.f5841U;
                    zzv2.f5844X = d.f5844X;
                    zzv2.f5840T = d.f5840T;
                    zzv2.f5838R = new zzkq(zzv2.f5838R.f5808Q, d.f5838R.f5809R, zzv2.f5838R.mo19469a(), d.f5838R.f5812U);
                } else if (TextUtils.isEmpty(zzv2.f5841U)) {
                    zzv2.f5838R = new zzkq(zzv2.f5838R.f5808Q, zzv2.f5839S, zzv2.f5838R.mo19469a(), zzv2.f5838R.f5812U);
                    zzv2.f5840T = true;
                    z = true;
                }
                if (zzv2.f5840T) {
                    zzkq zzkq = zzv2.f5838R;
                    C3234w9 w9Var = new C3234w9(zzv2.f5836P, zzv2.f5837Q, zzkq.f5808Q, zzkq.f5809R, zzkq.mo19469a());
                    if (mo19229e().mo18855a(w9Var)) {
                        this.f5517i.mo19015l().mo18995A().mo19045a("User property updated immediately", zzv2.f5836P, this.f5517i.mo19105x().mo18824c(w9Var.f5735c), w9Var.f5737e);
                    } else {
                        this.f5517i.mo19015l().mo19001t().mo19045a("(2)Too many active user properties, ignoring", C3032f4.m8621a(zzv2.f5836P), this.f5517i.mo19105x().mo18824c(w9Var.f5735c), w9Var.f5737e);
                    }
                    if (z && zzv2.f5844X != null) {
                        m9063b(new zzan(zzv2.f5844X, zzv2.f5839S), zzm);
                    }
                }
                if (mo19229e().mo18856a(zzv2)) {
                    this.f5517i.mo19015l().mo18995A().mo19045a("Conditional property added", zzv2.f5836P, this.f5517i.mo19105x().mo18824c(zzv2.f5838R.f5808Q), zzv2.f5838R.mo19469a());
                } else {
                    this.f5517i.mo19015l().mo19001t().mo19045a("Too many conditional properties, ignoring", C3032f4.m8621a(zzv2.f5836P), this.f5517i.mo19105x().mo18824c(zzv2.f5838R.f5808Q), zzv2.f5838R.mo19469a());
                }
                mo19229e().mo18874u();
            } finally {
                mo19229e().mo18879z();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0100  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0146  */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x018e  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.measurement.internal.C3021e5 m9046a(com.google.android.gms.measurement.internal.zzm r9, com.google.android.gms.measurement.internal.C3021e5 r10, java.lang.String r11) {
        /*
            r8 = this;
            r0 = 1
            if (r10 != 0) goto L_0x001e
            com.google.android.gms.measurement.internal.e5 r10 = new com.google.android.gms.measurement.internal.e5
            com.google.android.gms.measurement.internal.j5 r1 = r8.f5517i
            java.lang.String r2 = r9.f5814P
            r10.<init>(r1, r2)
            com.google.android.gms.measurement.internal.j5 r1 = r8.f5517i
            com.google.android.gms.measurement.internal.z9 r1 = r1.mo19104w()
            java.lang.String r1 = r1.mo19456w()
            r10.mo18933a(r1)
            r10.mo18950e(r11)
        L_0x001c:
            r11 = 1
            goto L_0x003a
        L_0x001e:
            java.lang.String r1 = r10.mo18977q()
            boolean r1 = r11.equals(r1)
            if (r1 != 0) goto L_0x0039
            r10.mo18950e(r11)
            com.google.android.gms.measurement.internal.j5 r11 = r8.f5517i
            com.google.android.gms.measurement.internal.z9 r11 = r11.mo19104w()
            java.lang.String r11 = r11.mo19456w()
            r10.mo18933a(r11)
            goto L_0x001c
        L_0x0039:
            r11 = 0
        L_0x003a:
            java.lang.String r1 = r9.f5815Q
            java.lang.String r2 = r10.mo18971n()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x004c
            java.lang.String r11 = r9.f5815Q
            r10.mo18939b(r11)
            r11 = 1
        L_0x004c:
            java.lang.String r1 = r9.f5831g0
            java.lang.String r2 = r10.mo18973o()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x005e
            java.lang.String r11 = r9.f5831g0
            r10.mo18943c(r11)
            r11 = 1
        L_0x005e:
            boolean r1 = com.google.android.gms.internal.measurement.C2774wa.m7753b()
            if (r1 == 0) goto L_0x0088
            com.google.android.gms.measurement.internal.j5 r1 = r8.f5517i
            com.google.android.gms.measurement.internal.la r1 = r1.mo19097m()
            java.lang.String r2 = r10.mo18967l()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5395G0
            boolean r1 = r1.mo19154e(r2, r3)
            if (r1 == 0) goto L_0x0088
            java.lang.String r1 = r9.f5835k0
            java.lang.String r2 = r10.mo18975p()
            boolean r1 = android.text.TextUtils.equals(r1, r2)
            if (r1 != 0) goto L_0x0088
            java.lang.String r11 = r9.f5835k0
            r10.mo18947d(r11)
            r11 = 1
        L_0x0088:
            java.lang.String r1 = r9.f5824Z
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r1 = r9.f5824Z
            java.lang.String r2 = r10.mo18978r()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00a2
            java.lang.String r11 = r9.f5824Z
            r10.mo18953f(r11)
            r11 = 1
        L_0x00a2:
            long r1 = r9.f5818T
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x00b8
            long r5 = r10.mo18984x()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00b8
            long r1 = r9.f5818T
            r10.mo18946d(r1)
            r11 = 1
        L_0x00b8:
            java.lang.String r1 = r9.f5816R
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x00d2
            java.lang.String r1 = r9.f5816R
            java.lang.String r2 = r10.mo18981u()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00d2
            java.lang.String r11 = r9.f5816R
            r10.mo18955g(r11)
            r11 = 1
        L_0x00d2:
            long r1 = r9.f5823Y
            long r5 = r10.mo18982v()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00e2
            long r1 = r9.f5823Y
            r10.mo18942c(r1)
            r11 = 1
        L_0x00e2:
            java.lang.String r1 = r9.f5817S
            if (r1 == 0) goto L_0x00f6
            java.lang.String r2 = r10.mo18983w()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x00f6
            java.lang.String r11 = r9.f5817S
            r10.mo18958h(r11)
            r11 = 1
        L_0x00f6:
            long r1 = r9.f5819U
            long r5 = r10.mo18985y()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x0106
            long r1 = r9.f5819U
            r10.mo18949e(r1)
            r11 = 1
        L_0x0106:
            boolean r1 = r9.f5821W
            boolean r2 = r10.mo18922A()
            if (r1 == r2) goto L_0x0114
            boolean r11 = r9.f5821W
            r10.mo18935a(r11)
            r11 = 1
        L_0x0114:
            java.lang.String r1 = r9.f5820V
            boolean r1 = android.text.TextUtils.isEmpty(r1)
            if (r1 != 0) goto L_0x012e
            java.lang.String r1 = r9.f5820V
            java.lang.String r2 = r10.mo18945d()
            boolean r1 = r1.equals(r2)
            if (r1 != 0) goto L_0x012e
            java.lang.String r11 = r9.f5820V
            r10.mo18962i(r11)
            r11 = 1
        L_0x012e:
            long r1 = r9.f5825a0
            long r5 = r10.mo18951f()
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x013e
            long r1 = r9.f5825a0
            r10.mo18976p(r1)
            r11 = 1
        L_0x013e:
            boolean r1 = r9.f5828d0
            boolean r2 = r10.mo18956g()
            if (r1 == r2) goto L_0x014c
            boolean r11 = r9.f5828d0
            r10.mo18940b(r11)
            r11 = 1
        L_0x014c:
            boolean r1 = r9.f5829e0
            boolean r2 = r10.mo18959h()
            if (r1 == r2) goto L_0x015a
            boolean r11 = r9.f5829e0
            r10.mo18944c(r11)
            r11 = 1
        L_0x015a:
            com.google.android.gms.measurement.internal.j5 r1 = r8.f5517i
            com.google.android.gms.measurement.internal.la r1 = r1.mo19097m()
            java.lang.String r2 = r9.f5814P
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r5 = com.google.android.gms.measurement.internal.C3135o.f5435a0
            boolean r1 = r1.mo19154e(r2, r5)
            if (r1 == 0) goto L_0x0178
            java.lang.Boolean r1 = r9.f5832h0
            java.lang.Boolean r2 = r10.mo18960i()
            if (r1 == r2) goto L_0x0178
            java.lang.Boolean r11 = r9.f5832h0
            r10.mo18932a(r11)
            r11 = 1
        L_0x0178:
            long r1 = r9.f5833i0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x018c
            long r3 = r10.mo18986z()
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 == 0) goto L_0x018c
            long r1 = r9.f5833i0
            r10.mo18952f(r1)
            r11 = 1
        L_0x018c:
            if (r11 == 0) goto L_0x0195
            com.google.android.gms.measurement.internal.d r9 = r8.mo19229e()
            r9.mo18850a(r10)
        L_0x0195:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3145o9.m9046a(com.google.android.gms.measurement.internal.zzm, com.google.android.gms.measurement.internal.e5, java.lang.String):com.google.android.gms.measurement.internal.e5");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19219a(boolean z) {
        m9044C();
    }
}
