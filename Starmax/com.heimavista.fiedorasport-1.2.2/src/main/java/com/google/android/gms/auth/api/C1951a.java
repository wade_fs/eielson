package com.google.android.gms.auth.api;

import android.os.Bundle;
import com.google.android.gms.auth.api.proxy.C1972a;
import com.google.android.gms.auth.api.signin.C1977b;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.internal.C1993f;
import com.google.android.gms.auth.api.signin.internal.C1994g;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.internal.p092authapi.C2359d;
import com.google.android.gms.internal.p092authapi.C2360e;

/* renamed from: com.google.android.gms.auth.api.a */
public final class C1951a {

    /* renamed from: a */
    public static final C2016a.C2028g<C2360e> f2985a = new C2016a.C2028g<>();

    /* renamed from: b */
    public static final C2016a.C2028g<C1994g> f2986b = new C2016a.C2028g<>();

    /* renamed from: c */
    private static final C2016a.C2017a<C2360e, C1952a> f2987c = new C1970e();

    /* renamed from: d */
    private static final C2016a.C2017a<C1994g, GoogleSignInOptions> f2988d = new C1971f();

    /* renamed from: e */
    public static final C2016a<GoogleSignInOptions> f2989e = new C2016a<>("Auth.GOOGLE_SIGN_IN_API", f2988d, f2986b);

    /* renamed from: f */
    public static final C1977b f2990f = new C1993f();

    @Deprecated
    /* renamed from: com.google.android.gms.auth.api.a$a */
    public static class C1952a implements C2016a.C2020d.C2025e {

        /* renamed from: P */
        private final boolean f2991P;

        @Deprecated
        /* renamed from: com.google.android.gms.auth.api.a$a$a */
        public static class C1953a {

            /* renamed from: a */
            protected Boolean f2992a = false;

            /* renamed from: a */
            public C1952a mo16304a() {
                return new C1952a(this);
            }
        }

        static {
            new C1953a().mo16304a();
        }

        public C1952a(C1953a aVar) {
            this.f2991P = aVar.f2992a.booleanValue();
        }

        /* renamed from: a */
        public final Bundle mo16303a() {
            Bundle bundle = new Bundle();
            bundle.putString("consumer_package", null);
            bundle.putBoolean("force_save_dialog", this.f2991P);
            return bundle;
        }
    }

    static {
        C2016a<C1960c> aVar = C1959b.f3027c;
        new C2016a("Auth.CREDENTIALS_API", f2987c, f2985a);
        C1972a aVar2 = C1959b.f3028d;
        new C2359d();
    }
}
