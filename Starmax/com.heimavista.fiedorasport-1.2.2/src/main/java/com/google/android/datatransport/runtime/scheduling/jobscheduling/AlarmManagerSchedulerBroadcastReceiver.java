package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.C3911q;
import p119e.p144d.p145a.p146a.p147i.p154y.C3977a;

/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class AlarmManagerSchedulerBroadcastReceiver extends BroadcastReceiver {
    /* renamed from: a */
    static /* synthetic */ void m4306a() {
    }

    public void onReceive(Context context, Intent intent) {
        String queryParameter = intent.getData().getQueryParameter("backendName");
        String queryParameter2 = intent.getData().getQueryParameter("extras");
        int intValue = Integer.valueOf(intent.getData().getQueryParameter("priority")).intValue();
        int i = intent.getExtras().getInt("attemptNumber");
        C3911q.m11780a(context);
        C3905l.C3906a d = C3905l.m11763d();
        d.mo23537a(queryParameter);
        d.mo23536a(C3977a.m11940a(intValue));
        if (queryParameter2 != null) {
            d.mo23538a(Base64.decode(queryParameter2, 0));
        }
        C3911q.m11781b().mo23562a().mo13589a(d.mo23539a(), i, C1726b.m4310a());
    }
}
