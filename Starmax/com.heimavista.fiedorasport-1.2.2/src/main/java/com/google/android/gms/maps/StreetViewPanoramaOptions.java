package com.google.android.gms.maps;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.StreetViewPanoramaCamera;
import com.google.android.gms.maps.model.StreetViewSource;
import com.google.android.gms.maps.p093i.C2906k;

public final class StreetViewPanoramaOptions extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<StreetViewPanoramaOptions> CREATOR = new C2956n();

    /* renamed from: P */
    private StreetViewPanoramaCamera f4754P;

    /* renamed from: Q */
    private String f4755Q;

    /* renamed from: R */
    private LatLng f4756R;

    /* renamed from: S */
    private Integer f4757S;

    /* renamed from: T */
    private Boolean f4758T = true;

    /* renamed from: U */
    private Boolean f4759U = true;

    /* renamed from: V */
    private Boolean f4760V = true;

    /* renamed from: W */
    private Boolean f4761W = true;

    /* renamed from: X */
    private Boolean f4762X;

    /* renamed from: Y */
    private StreetViewSource f4763Y = StreetViewSource.f4896Q;

    StreetViewPanoramaOptions(StreetViewPanoramaCamera streetViewPanoramaCamera, String str, LatLng latLng, Integer num, byte b, byte b2, byte b3, byte b4, byte b5, StreetViewSource streetViewSource) {
        this.f4754P = streetViewPanoramaCamera;
        this.f4756R = latLng;
        this.f4757S = num;
        this.f4755Q = str;
        this.f4758T = C2906k.m8221a(b);
        this.f4759U = C2906k.m8221a(b2);
        this.f4760V = C2906k.m8221a(b3);
        this.f4761W = C2906k.m8221a(b4);
        this.f4762X = C2906k.m8221a(b5);
        this.f4763Y = streetViewSource;
    }

    /* renamed from: c */
    public final String mo18388c() {
        return this.f4755Q;
    }

    /* renamed from: d */
    public final LatLng mo18389d() {
        return this.f4756R;
    }

    public final String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("PanoramaId", this.f4755Q);
        a.mo17037a("Position", this.f4756R);
        a.mo17037a("Radius", this.f4757S);
        a.mo17037a("Source", this.f4763Y);
        a.mo17037a("StreetViewPanoramaCamera", this.f4754P);
        a.mo17037a("UserNavigationEnabled", this.f4758T);
        a.mo17037a("ZoomGesturesEnabled", this.f4759U);
        a.mo17037a("PanningGesturesEnabled", this.f4760V);
        a.mo17037a("StreetNamesEnabled", this.f4761W);
        a.mo17037a("UseViewLifecycleInFragment", this.f4762X);
        return a.toString();
    }

    /* renamed from: u */
    public final Integer mo18391u() {
        return this.f4757S;
    }

    /* renamed from: v */
    public final StreetViewSource mo18392v() {
        return this.f4763Y;
    }

    /* renamed from: w */
    public final StreetViewPanoramaCamera mo18393w() {
        return this.f4754P;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.StreetViewPanoramaCamera, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
     arg types: [android.os.Parcel, int, java.lang.Integer, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.StreetViewSource, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 2, (Parcelable) mo18393w(), i, false);
        C2250b.m5602a(parcel, 3, mo18388c(), false);
        C2250b.m5596a(parcel, 4, (Parcelable) mo18389d(), i, false);
        C2250b.m5600a(parcel, 5, mo18391u(), false);
        C2250b.m5588a(parcel, 6, C2906k.m8220a(this.f4758T));
        C2250b.m5588a(parcel, 7, C2906k.m8220a(this.f4759U));
        C2250b.m5588a(parcel, 8, C2906k.m8220a(this.f4760V));
        C2250b.m5588a(parcel, 9, C2906k.m8220a(this.f4761W));
        C2250b.m5588a(parcel, 10, C2906k.m8220a(this.f4762X));
        C2250b.m5596a(parcel, 11, (Parcelable) mo18392v(), i, false);
        C2250b.m5587a(parcel, a);
    }

    public StreetViewPanoramaOptions() {
    }
}
