package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.k */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2576k extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4272T;

    /* renamed from: U */
    private final /* synthetic */ String f4273U;

    /* renamed from: V */
    private final /* synthetic */ boolean f4274V;

    /* renamed from: W */
    private final /* synthetic */ C2538h9 f4275W;

    /* renamed from: X */
    private final /* synthetic */ C2525gd f4276X;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2576k(C2525gd gdVar, String str, String str2, boolean z, C2538h9 h9Var) {
        super(gdVar);
        this.f4276X = gdVar;
        this.f4272T = str;
        this.f4273U = str2;
        this.f4274V = z;
        this.f4275W = h9Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4276X.f4192g.getUserProperties(this.f4272T, this.f4273U, this.f4274V, this.f4275W);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17345b() {
        this.f4275W.mo17292d(null);
    }
}
