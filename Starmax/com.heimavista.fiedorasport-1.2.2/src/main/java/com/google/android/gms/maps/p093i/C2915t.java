package com.google.android.gms.maps.p093i;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;

/* renamed from: com.google.android.gms.maps.i.t */
public final class C2915t extends C4032a implements C2889a {
    C2915t(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.ICameraUpdateFactoryDelegate");
    }

    /* renamed from: a */
    public final C3988b mo18416a(CameraPosition cameraPosition) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, cameraPosition);
        Parcel a2 = mo23665a(7, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: b */
    public final C3988b mo18419b(float f) {
        Parcel a = mo23664a();
        a.writeFloat(f);
        Parcel a2 = mo23665a(4, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: c */
    public final C3988b mo18420c(LatLng latLng) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, latLng);
        Parcel a2 = mo23665a(8, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: a */
    public final C3988b mo18417a(LatLng latLng, float f) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, latLng);
        a.writeFloat(f);
        Parcel a2 = mo23665a(9, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: a */
    public final C3988b mo18418a(LatLngBounds latLngBounds, int i) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, latLngBounds);
        a.writeInt(i);
        Parcel a2 = mo23665a(10, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
