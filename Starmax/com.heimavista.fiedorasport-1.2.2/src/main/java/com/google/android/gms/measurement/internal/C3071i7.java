package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.i7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3071i7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ long f5213P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5214Q;

    C3071i7(C3154p6 p6Var, long j) {
        this.f5214Q = p6Var;
        this.f5213P = j;
    }

    public final void run() {
        this.f5214Q.mo19012g().f5619q.mo19327a(this.f5213P);
        this.f5214Q.mo19015l().mo18995A().mo19043a("Session timeout duration set", Long.valueOf(this.f5213P));
    }
}
