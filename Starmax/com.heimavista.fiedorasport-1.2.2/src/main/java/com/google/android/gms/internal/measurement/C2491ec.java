package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ec */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2491ec implements C2507fc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4084a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4085b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4086c;

    /* renamed from: d */
    private static final C2765w1<Boolean> f4087d;

    /* renamed from: e */
    private static final C2765w1<Boolean> f4088e;

    /* renamed from: f */
    private static final C2765w1<Boolean> f4089f;

    /* renamed from: g */
    private static final C2765w1<Boolean> f4090g;

    /* renamed from: h */
    private static final C2765w1<Boolean> f4091h;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4084a = c2Var.mo17367a("measurement.service.audience.scoped_filters_v27", true);
        f4085b = c2Var.mo17367a("measurement.service.audience.session_scoped_user_engagement", true);
        f4086c = c2Var.mo17367a("measurement.client.audience.scoped_engagement_removal_when_session_expired", true);
        f4087d = c2Var.mo17367a("measurement.service.audience.scoped_engagement_removal_when_session_expired", true);
        f4088e = c2Var.mo17367a("measurement.service.audience.session_scoped_event_aggregates", true);
        f4089f = c2Var.mo17367a("measurement.service.audience.use_bundle_timestamp_for_property_filters", true);
        c2Var.mo17365a("measurement.id.scoped_audience_filters", 0L);
        f4090g = c2Var.mo17367a("measurement.service.audience.fix_prepending_previous_sequence_timestamp", true);
        f4091h = c2Var.mo17367a("measurement.service.audience.remove_disabled_session_scoped_user_engagement", false);
    }

    /* renamed from: a */
    public final boolean mo17449a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17450e() {
        return f4084a.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17451f() {
        return f4085b.mo18128b().booleanValue();
    }

    /* renamed from: g */
    public final boolean mo17452g() {
        return f4086c.mo18128b().booleanValue();
    }

    /* renamed from: h */
    public final boolean mo17453h() {
        return f4088e.mo18128b().booleanValue();
    }

    /* renamed from: i */
    public final boolean mo17454i() {
        return f4091h.mo18128b().booleanValue();
    }

    /* renamed from: p */
    public final boolean mo17455p() {
        return f4090g.mo18128b().booleanValue();
    }

    /* renamed from: q */
    public final boolean mo17456q() {
        return f4089f.mo18128b().booleanValue();
    }

    /* renamed from: t */
    public final boolean mo17457t() {
        return f4087d.mo18128b().booleanValue();
    }
}
