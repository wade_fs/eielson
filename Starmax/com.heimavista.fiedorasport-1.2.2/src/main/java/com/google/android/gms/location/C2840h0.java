package com.google.android.gms.location;

import android.os.IBinder;
import com.google.android.gms.internal.location.C2383a;

/* renamed from: com.google.android.gms.location.h0 */
public final class C2840h0 extends C2383a implements C2836f0 {
    C2840h0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.location.IDeviceOrientationListener");
    }
}
