package com.google.android.datatransport.runtime.scheduling.jobscheduling;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.o */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1745o implements Runnable {

    /* renamed from: P */
    private final C1747q f2769P;

    private C1745o(C1747q qVar) {
        this.f2769P = qVar;
    }

    /* renamed from: a */
    public static Runnable m4361a(C1747q qVar) {
        return new C1745o(qVar);
    }

    public void run() {
        this.f2769P.f2774d.mo23602a(C1746p.m4362a(this.f2769P));
    }
}
