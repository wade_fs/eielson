package com.google.android.gms.common.api.internal;

import android.os.RemoteException;
import android.os.TransactionTooLargeException;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2064e;
import com.google.android.gms.common.util.C2323n;

/* renamed from: com.google.android.gms.common.api.internal.p0 */
public abstract class C2116p0 {
    public C2116p0(int i) {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public static Status m5045a(RemoteException remoteException) {
        StringBuilder sb = new StringBuilder();
        if (C2323n.m5793b() && (remoteException instanceof TransactionTooLargeException)) {
            sb.append("TransactionTooLargeException: ");
        }
        sb.append(remoteException.getLocalizedMessage());
        return new Status(8, sb.toString());
    }

    /* renamed from: a */
    public abstract void mo16614a(@NonNull Status status);

    /* renamed from: a */
    public abstract void mo16615a(C2064e.C2065a<?> aVar);

    /* renamed from: a */
    public abstract void mo16616a(@NonNull C2123r rVar, boolean z);

    /* renamed from: a */
    public abstract void mo16617a(@NonNull RuntimeException runtimeException);
}
