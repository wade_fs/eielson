package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import com.google.android.gms.internal.measurement.C2545i1;
import com.google.android.gms.internal.measurement.C2670q;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.z3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public abstract class C3261z3 extends C2545i1 implements C3228w3 {
    public C3261z3() {
        super("com.google.android.gms.measurement.internal.IMeasurementService");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo17344a(int i, Parcel parcel, Parcel parcel2, int i2) {
        switch (i) {
            case 1:
                mo19189a((zzan) C2670q.m7044a(parcel, zzan.CREATOR), (zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                return true;
            case 2:
                mo19191a((zzkq) C2670q.m7044a(parcel, zzkq.CREATOR), (zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                return true;
            case 3:
            case 8:
            default:
                return false;
            case 4:
                mo19199d((zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                return true;
            case 5:
                mo19190a((zzan) C2670q.m7044a(parcel, zzan.CREATOR), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 6:
                mo19197b((zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                return true;
            case 7:
                List<zzkq> a = mo19183a((zzm) C2670q.m7044a(parcel, zzm.CREATOR), C2670q.m7048a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a);
                return true;
            case 9:
                byte[] a2 = mo19195a((zzan) C2670q.m7044a(parcel, zzan.CREATOR), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeByteArray(a2);
                return true;
            case 10:
                mo19188a(parcel.readLong(), parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                return true;
            case 11:
                String c = mo19198c((zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                parcel2.writeString(c);
                return true;
            case 12:
                mo19194a((zzv) C2670q.m7044a(parcel, zzv.CREATOR), (zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                return true;
            case 13:
                mo19193a((zzv) C2670q.m7044a(parcel, zzv.CREATOR));
                parcel2.writeNoException();
                return true;
            case 14:
                List<zzkq> a3 = mo19187a(parcel.readString(), parcel.readString(), C2670q.m7048a(parcel), (zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a3);
                return true;
            case 15:
                List<zzkq> a4 = mo19186a(parcel.readString(), parcel.readString(), parcel.readString(), C2670q.m7048a(parcel));
                parcel2.writeNoException();
                parcel2.writeTypedList(a4);
                return true;
            case 16:
                List<zzv> a5 = mo19184a(parcel.readString(), parcel.readString(), (zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                parcel2.writeTypedList(a5);
                return true;
            case 17:
                List<zzv> a6 = mo19185a(parcel.readString(), parcel.readString(), parcel.readString());
                parcel2.writeNoException();
                parcel2.writeTypedList(a6);
                return true;
            case 18:
                mo19192a((zzm) C2670q.m7044a(parcel, zzm.CREATOR));
                parcel2.writeNoException();
                return true;
        }
    }
}
