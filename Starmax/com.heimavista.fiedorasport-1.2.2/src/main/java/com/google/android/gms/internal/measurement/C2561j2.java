package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;

/* renamed from: com.google.android.gms.internal.measurement.j2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C2561j2 extends IInterface {
    /* renamed from: d */
    Bundle mo17498d(Bundle bundle);
}
