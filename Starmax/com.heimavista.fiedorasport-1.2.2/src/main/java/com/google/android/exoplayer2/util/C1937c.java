package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.util.SlidingPercentile;
import java.util.Comparator;

/* renamed from: com.google.android.exoplayer2.util.c */
/* compiled from: lambda */
public final /* synthetic */ class C1937c implements Comparator {

    /* renamed from: P */
    public static final /* synthetic */ C1937c f2944P = new C1937c();

    private /* synthetic */ C1937c() {
    }

    public final int compare(Object obj, Object obj2) {
        return Float.compare(((SlidingPercentile.Sample) obj).value, ((SlidingPercentile.Sample) obj2).value);
    }
}
