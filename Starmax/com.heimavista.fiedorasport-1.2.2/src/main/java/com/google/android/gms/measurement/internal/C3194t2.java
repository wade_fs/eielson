package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2457cb;

/* renamed from: com.google.android.gms.measurement.internal.t2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3194t2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5648a = new C3194t2();

    private C3194t2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2457cb.m6160f());
    }
}
