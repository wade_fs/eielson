package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.RemoteException;
import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.l8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3108l8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f5326P;

    /* renamed from: Q */
    private final /* synthetic */ String f5327Q;

    /* renamed from: R */
    private final /* synthetic */ boolean f5328R;

    /* renamed from: S */
    private final /* synthetic */ zzm f5329S;

    /* renamed from: T */
    private final /* synthetic */ C2589kc f5330T;

    /* renamed from: U */
    private final /* synthetic */ C3232w7 f5331U;

    C3108l8(C3232w7 w7Var, String str, String str2, boolean z, zzm zzm, C2589kc kcVar) {
        this.f5331U = w7Var;
        this.f5326P = str;
        this.f5327Q = str2;
        this.f5328R = z;
        this.f5329S = zzm;
        this.f5330T = kcVar;
    }

    public final void run() {
        Bundle bundle = new Bundle();
        try {
            C3228w3 d = this.f5331U.f5724d;
            if (d == null) {
                this.f5331U.mo19015l().mo19001t().mo19044a("Failed to get user properties; not connected to service", this.f5326P, this.f5327Q);
                return;
            }
            bundle = C3267z9.m9411a(d.mo19187a(this.f5326P, this.f5327Q, this.f5328R, this.f5329S));
            this.f5331U.m9314J();
            this.f5331U.mo19011f().mo19436a(this.f5330T, bundle);
        } catch (RemoteException e) {
            this.f5331U.mo19015l().mo19001t().mo19044a("Failed to get user properties; remote exception", this.f5326P, e);
        } finally {
            this.f5331U.mo19011f().mo19436a(this.f5330T, bundle);
        }
    }
}
