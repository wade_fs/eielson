package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.k5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2582k5 implements C2754v5 {
    C2582k5() {
    }

    /* renamed from: a */
    public final boolean mo17588a(Class<?> cls) {
        return false;
    }

    /* renamed from: b */
    public final C2707s5 mo17589b(Class<?> cls) {
        throw new IllegalStateException("This should never be called.");
    }
}
