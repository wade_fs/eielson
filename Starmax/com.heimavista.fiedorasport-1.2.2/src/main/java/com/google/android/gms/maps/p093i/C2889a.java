package com.google.android.gms.maps.p093i;

import android.os.IInterface;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.i.a */
public interface C2889a extends IInterface {
    /* renamed from: a */
    C3988b mo18416a(CameraPosition cameraPosition);

    /* renamed from: a */
    C3988b mo18417a(LatLng latLng, float f);

    /* renamed from: a */
    C3988b mo18418a(LatLngBounds latLngBounds, int i);

    /* renamed from: b */
    C3988b mo18419b(float f);

    /* renamed from: c */
    C3988b mo18420c(LatLng latLng);
}
