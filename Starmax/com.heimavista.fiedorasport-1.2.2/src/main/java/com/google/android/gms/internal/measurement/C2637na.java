package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.na */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2637na implements C2653oa {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4361a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4362b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4363c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4361a = c2Var.mo17367a("measurement.client.sessions.check_on_reset_and_enable", false);
        f4362b = c2Var.mo17367a("measurement.client.sessions.check_on_startup", true);
        f4363c = c2Var.mo17367a("measurement.client.sessions.start_session_before_view_screen", true);
    }

    /* renamed from: a */
    public final boolean mo17779a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17780e() {
        return f4361a.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17781f() {
        return f4362b.mo18128b().booleanValue();
    }

    /* renamed from: g */
    public final boolean mo17782g() {
        return f4363c.mo18128b().booleanValue();
    }
}
