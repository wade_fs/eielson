package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.m4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2616m4 extends C2813z2<Integer> implements C2706s4, C2485e6, RandomAccess {

    /* renamed from: S */
    private static final C2616m4 f4323S;

    /* renamed from: Q */
    private int[] f4324Q;

    /* renamed from: R */
    private int f4325R;

    static {
        C2616m4 m4Var = new C2616m4(new int[0], 0);
        f4323S = m4Var;
        super.mo17939e();
    }

    C2616m4() {
        this(new int[10], 0);
    }

    /* renamed from: e */
    private final void m6770e(int i) {
        if (i < 0 || i >= this.f4325R) {
            throw new IndexOutOfBoundsException(m6771f(i));
        }
    }

    /* renamed from: f */
    private final String m6771f(int i) {
        int i2 = this.f4325R;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: g */
    public static C2616m4 m6772g() {
        return f4323S;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        int intValue = ((Integer) obj).intValue();
        mo18179b();
        if (i < 0 || i > (i2 = this.f4325R)) {
            throw new IndexOutOfBoundsException(m6771f(i));
        }
        int[] iArr = this.f4324Q;
        if (i2 < iArr.length) {
            System.arraycopy(iArr, i, iArr, i + 1, i2 - i);
        } else {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i);
            System.arraycopy(this.f4324Q, i, iArr2, i + 1, this.f4325R - i);
            this.f4324Q = iArr2;
        }
        this.f4324Q[i] = intValue;
        this.f4325R++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Integer> collection) {
        mo18179b();
        C2647o4.m6961a(collection);
        if (!(collection instanceof C2616m4)) {
            return super.addAll(collection);
        }
        C2616m4 m4Var = (C2616m4) collection;
        int i = m4Var.f4325R;
        if (i == 0) {
            return false;
        }
        int i2 = this.f4325R;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            int[] iArr = this.f4324Q;
            if (i3 > iArr.length) {
                this.f4324Q = Arrays.copyOf(iArr, i3);
            }
            System.arraycopy(m4Var.f4324Q, 0, this.f4324Q, this.f4325R, m4Var.f4325R);
            this.f4325R = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    /* renamed from: b */
    public final C2706s4 mo17320a(int i) {
        if (i >= this.f4325R) {
            return new C2616m4(Arrays.copyOf(this.f4324Q, i), this.f4325R);
        }
        throw new IllegalArgumentException();
    }

    /* renamed from: c */
    public final int mo17733c(int i) {
        m6770e(i);
        return this.f4324Q[i];
    }

    /* renamed from: d */
    public final void mo17734d(int i) {
        mo18179b();
        int i2 = this.f4325R;
        int[] iArr = this.f4324Q;
        if (i2 == iArr.length) {
            int[] iArr2 = new int[(((i2 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            this.f4324Q = iArr2;
        }
        int[] iArr3 = this.f4324Q;
        int i3 = this.f4325R;
        this.f4325R = i3 + 1;
        iArr3[i3] = i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2616m4)) {
            return super.equals(obj);
        }
        C2616m4 m4Var = (C2616m4) obj;
        if (this.f4325R != m4Var.f4325R) {
            return false;
        }
        int[] iArr = m4Var.f4324Q;
        for (int i = 0; i < this.f4325R; i++) {
            if (this.f4324Q[i] != iArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Integer.valueOf(mo17733c(i));
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.f4325R; i2++) {
            i = (i * 31) + this.f4324Q[i2];
        }
        return i;
    }

    public final boolean remove(Object obj) {
        mo18179b();
        for (int i = 0; i < this.f4325R; i++) {
            if (obj.equals(Integer.valueOf(this.f4324Q[i]))) {
                int[] iArr = this.f4324Q;
                System.arraycopy(iArr, i + 1, iArr, i, (this.f4325R - i) - 1);
                this.f4325R--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        mo18179b();
        if (i2 >= i) {
            int[] iArr = this.f4324Q;
            System.arraycopy(iArr, i2, iArr, i, this.f4325R - i2);
            this.f4325R -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        int intValue = ((Integer) obj).intValue();
        mo18179b();
        m6770e(i);
        int[] iArr = this.f4324Q;
        int i2 = iArr[i];
        iArr[i] = intValue;
        return Integer.valueOf(i2);
    }

    public final int size() {
        return this.f4325R;
    }

    private C2616m4(int[] iArr, int i) {
        this.f4324Q = iArr;
        this.f4325R = i;
    }

    public final /* synthetic */ Object remove(int i) {
        mo18179b();
        m6770e(i);
        int[] iArr = this.f4324Q;
        int i2 = iArr[i];
        int i3 = this.f4325R;
        if (i < i3 - 1) {
            System.arraycopy(iArr, i + 1, iArr, i, (i3 - i) - 1);
        }
        this.f4325R--;
        this.modCount++;
        return Integer.valueOf(i2);
    }

    public final /* synthetic */ boolean add(Object obj) {
        mo17734d(((Integer) obj).intValue());
        return true;
    }
}
