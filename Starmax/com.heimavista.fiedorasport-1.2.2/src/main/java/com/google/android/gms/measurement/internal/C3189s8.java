package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Context;

/* renamed from: com.google.android.gms.measurement.internal.s8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3189s8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3144o8 f5642P;

    C3189s8(C3144o8 o8Var) {
        this.f5642P = o8Var;
    }

    public final void run() {
        C3232w7 w7Var = this.f5642P.f5507R;
        Context n = w7Var.mo19016n();
        this.f5642P.f5507R.mo19018r();
        w7Var.m9320a(new ComponentName(n, "com.google.android.gms.measurement.AppMeasurementService"));
    }
}
