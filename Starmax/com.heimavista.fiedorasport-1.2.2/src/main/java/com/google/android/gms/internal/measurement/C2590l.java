package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.l */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2590l extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ C2538h9 f4284T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd f4285U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2590l(C2525gd gdVar, C2538h9 h9Var) {
        super(gdVar);
        this.f4285U = gdVar;
        this.f4284T = h9Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4285U.f4192g.getCurrentScreenClass(this.f4284T);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17345b() {
        this.f4284T.mo17292d(null);
    }
}
