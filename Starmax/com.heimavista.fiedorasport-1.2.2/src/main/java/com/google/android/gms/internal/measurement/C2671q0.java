package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2794y0;

/* renamed from: com.google.android.gms.internal.measurement.q0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2671q0 extends C2595l4<C2671q0, C2672a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2671q0 zzh;
    private static volatile C2501f6<C2671q0> zzi;
    private int zzc;
    private int zzd;
    private C2794y0 zze;
    private C2794y0 zzf;
    private boolean zzg;

    /* renamed from: com.google.android.gms.internal.measurement.q0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2672a extends C2595l4.C2596a<C2671q0, C2672a> implements C2769w5 {
        private C2672a() {
            super(C2671q0.zzh);
        }

        /* renamed from: a */
        public final C2672a mo17820a(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2671q0) super.f4288Q).m7055b(i);
            return this;
        }

        /* renamed from: j */
        public final boolean mo17824j() {
            return ((C2671q0) super.f4288Q).mo17816q();
        }

        /* renamed from: k */
        public final C2794y0 mo17825k() {
            return ((C2671q0) super.f4288Q).mo17817s();
        }

        /* synthetic */ C2672a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2672a mo17821a(C2794y0.C2795a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2671q0) super.f4288Q).m7053a((C2794y0) super.mo17679i());
            return this;
        }

        /* renamed from: a */
        public final C2672a mo17822a(C2794y0 y0Var) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2671q0) super.f4288Q).m7057b(y0Var);
            return this;
        }

        /* renamed from: a */
        public final C2672a mo17823a(boolean z) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2671q0) super.f4288Q).m7054a(z);
            return this;
        }
    }

    static {
        C2671q0 q0Var = new C2671q0();
        zzh = q0Var;
        C2595l4.m6645a(C2671q0.class, super);
    }

    private C2671q0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7053a(C2794y0 y0Var) {
        y0Var.getClass();
        this.zze = y0Var;
        this.zzc |= 2;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7055b(int i) {
        this.zzc |= 1;
        this.zzd = i;
    }

    /* renamed from: v */
    public static C2672a m7058v() {
        return (C2672a) zzh.mo17668i();
    }

    /* renamed from: n */
    public final boolean mo17813n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final int mo17814o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final C2794y0 mo17815p() {
        C2794y0 y0Var = this.zze;
        return y0Var == null ? C2794y0.m7801x() : y0Var;
    }

    /* renamed from: q */
    public final boolean mo17816q() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: s */
    public final C2794y0 mo17817s() {
        C2794y0 y0Var = this.zzf;
        return y0Var == null ? C2794y0.m7801x() : y0Var;
    }

    /* renamed from: t */
    public final boolean mo17818t() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: u */
    public final boolean mo17819u() {
        return this.zzg;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7057b(C2794y0 y0Var) {
        y0Var.getClass();
        this.zzf = y0Var;
        this.zzc |= 4;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7054a(boolean z) {
        this.zzc |= 8;
        this.zzg = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2671q0();
            case 2:
                return new C2672a(null);
            case 3:
                return C2595l4.m6643a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\t\u0001\u0003\t\u0002\u0004\u0007\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                C2501f6<C2671q0> f6Var = zzi;
                if (f6Var == null) {
                    synchronized (C2671q0.class) {
                        f6Var = zzi;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzh);
                            zzi = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
