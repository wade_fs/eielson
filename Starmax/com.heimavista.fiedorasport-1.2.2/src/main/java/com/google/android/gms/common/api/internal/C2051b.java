package com.google.android.gms.common.api.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentCallbacks2;
import android.content.res.Configuration;
import android.os.Bundle;
import com.google.android.gms.common.util.C2323n;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/* renamed from: com.google.android.gms.common.api.internal.b */
public final class C2051b implements Application.ActivityLifecycleCallbacks, ComponentCallbacks2 {

    /* renamed from: T */
    private static final C2051b f3243T = new C2051b();

    /* renamed from: P */
    private final AtomicBoolean f3244P = new AtomicBoolean();

    /* renamed from: Q */
    private final AtomicBoolean f3245Q = new AtomicBoolean();

    /* renamed from: R */
    private final ArrayList<C2052a> f3246R = new ArrayList<>();

    /* renamed from: S */
    private boolean f3247S = false;

    /* renamed from: com.google.android.gms.common.api.internal.b$a */
    public interface C2052a {
        /* renamed from: a */
        void mo16631a(boolean z);
    }

    private C2051b() {
    }

    /* renamed from: a */
    public static void m4757a(Application application) {
        synchronized (f3243T) {
            if (!f3243T.f3247S) {
                application.registerActivityLifecycleCallbacks(f3243T);
                application.registerComponentCallbacks(f3243T);
                f3243T.f3247S = true;
            }
        }
    }

    /* renamed from: b */
    public static C2051b m4758b() {
        return f3243T;
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        boolean compareAndSet = this.f3244P.compareAndSet(true, false);
        this.f3245Q.set(true);
        if (compareAndSet) {
            m4759b(false);
        }
    }

    public final void onActivityDestroyed(Activity activity) {
    }

    public final void onActivityPaused(Activity activity) {
    }

    public final void onActivityResumed(Activity activity) {
        boolean compareAndSet = this.f3244P.compareAndSet(true, false);
        this.f3245Q.set(true);
        if (compareAndSet) {
            m4759b(false);
        }
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    public final void onConfigurationChanged(Configuration configuration) {
    }

    public final void onLowMemory() {
    }

    public final void onTrimMemory(int i) {
        if (i == 20 && this.f3244P.compareAndSet(false, true)) {
            this.f3245Q.set(true);
            m4759b(true);
        }
    }

    /* renamed from: b */
    private final void m4759b(boolean z) {
        synchronized (f3243T) {
            ArrayList<C2052a> arrayList = this.f3246R;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                C2052a aVar = arrayList.get(i);
                i++;
                aVar.mo16631a(z);
            }
        }
    }

    @TargetApi(16)
    /* renamed from: a */
    public final boolean mo16620a(boolean z) {
        if (!this.f3245Q.get()) {
            if (!C2323n.m5794c()) {
                return z;
            }
            ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
            if (!this.f3245Q.getAndSet(true) && runningAppProcessInfo.importance > 100) {
                this.f3244P.set(true);
            }
        }
        return mo16619a();
    }

    /* renamed from: a */
    public final boolean mo16619a() {
        return this.f3244P.get();
    }

    /* renamed from: a */
    public final void mo16618a(C2052a aVar) {
        synchronized (f3243T) {
            this.f3246R.add(aVar);
        }
    }
}
