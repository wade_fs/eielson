package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.l5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2602l5 implements C2583k6 {

    /* renamed from: b */
    private static final C2754v5 f4302b = new C2582k5();

    /* renamed from: a */
    private final C2754v5 f4303a;

    public C2602l5() {
        this(new C2632n5(C2563j4.m6502a(), m6678a()));
    }

    /* renamed from: a */
    public final <T> C2603l6<T> mo17652a(Class<T> cls) {
        Class<C2595l4> cls2 = C2595l4.class;
        C2633n6.m6888a((Class<?>) cls);
        C2707s5 b = this.f4303a.mo17589b(cls);
        if (b.mo17593e()) {
            if (cls2.isAssignableFrom(cls)) {
                return C2420a6.m6002a(C2633n6.m6900c(), C2450c4.m6131a(), b.mo17594f());
            }
            return C2420a6.m6002a(C2633n6.m6878a(), C2450c4.m6132b(), b.mo17594f());
        } else if (cls2.isAssignableFrom(cls)) {
            if (m6679a(b)) {
                return C2801y5.m7838a(cls, b, C2452c6.m6139b(), C2467d5.m6204b(), C2633n6.m6900c(), C2450c4.m6131a(), C2724t5.m7318b());
            }
            return C2801y5.m7838a(cls, b, C2452c6.m6139b(), C2467d5.m6204b(), C2633n6.m6900c(), (C2418a4<?>) null, C2724t5.m7318b());
        } else if (m6679a(b)) {
            return C2801y5.m7838a(cls, b, C2452c6.m6138a(), C2467d5.m6203a(), C2633n6.m6878a(), C2450c4.m6132b(), C2724t5.m7317a());
        } else {
            return C2801y5.m7838a(cls, b, C2452c6.m6138a(), C2467d5.m6203a(), C2633n6.m6894b(), (C2418a4<?>) null, C2724t5.m7317a());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.v5, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T */
    private C2602l5(C2754v5 v5Var) {
        C2647o4.m6963a((Object) v5Var, "messageInfoFactory");
        this.f4303a = v5Var;
    }

    /* renamed from: a */
    private static boolean m6679a(C2707s5 s5Var) {
        return s5Var.mo17590a() == C2595l4.C2601f.f4298i;
    }

    /* renamed from: a */
    private static C2754v5 m6678a() {
        try {
            return (C2754v5) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            return f4302b;
        }
    }
}
