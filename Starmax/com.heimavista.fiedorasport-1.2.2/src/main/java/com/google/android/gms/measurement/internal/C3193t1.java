package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2710s8;

/* renamed from: com.google.android.gms.measurement.internal.t1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3193t1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5647a = new C3193t1();

    private C3193t1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2710s8.m7197b());
    }
}
