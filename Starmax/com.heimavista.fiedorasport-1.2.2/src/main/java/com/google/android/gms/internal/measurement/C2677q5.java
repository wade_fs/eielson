package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.q5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2677q5 implements C2692r5 {
    C2677q5() {
    }

    /* renamed from: a */
    public final Map<?, ?> mo17829a(Object obj) {
        return (C2648o5) obj;
    }

    /* renamed from: b */
    public final boolean mo17830b(Object obj) {
        return !((C2648o5) obj).mo17794f();
    }

    /* renamed from: c */
    public final Map<?, ?> mo17831c(Object obj) {
        return (C2648o5) obj;
    }

    /* renamed from: d */
    public final C2662p5<?, ?> mo17832d(Object obj) {
        C2617m5 m5Var = (C2617m5) obj;
        throw new NoSuchMethodError();
    }

    /* renamed from: e */
    public final Object mo17833e(Object obj) {
        return C2648o5.m6968g().mo17788a();
    }

    /* renamed from: f */
    public final Object mo17834f(Object obj) {
        ((C2648o5) obj).mo17791e();
        return obj;
    }

    /* renamed from: a */
    public final Object mo17828a(Object obj, Object obj2) {
        C2648o5 o5Var = (C2648o5) obj;
        C2648o5 o5Var2 = (C2648o5) obj2;
        if (!o5Var2.isEmpty()) {
            if (!o5Var.mo17794f()) {
                o5Var = o5Var.mo17788a();
            }
            o5Var.mo17789a(o5Var2);
        }
        return o5Var;
    }

    /* renamed from: a */
    public final int mo17827a(int i, Object obj, Object obj2) {
        C2648o5 o5Var = (C2648o5) obj;
        C2617m5 m5Var = (C2617m5) obj2;
        if (o5Var.isEmpty()) {
            return 0;
        }
        Iterator it = o5Var.entrySet().iterator();
        if (!it.hasNext()) {
            return 0;
        }
        Map.Entry entry = (Map.Entry) it.next();
        entry.getKey();
        entry.getValue();
        throw new NoSuchMethodError();
    }
}
