package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class LocationSettingsStates extends AbstractSafeParcelable {
    public static final Parcelable.Creator<LocationSettingsStates> CREATOR = new C2854p();

    /* renamed from: P */
    private final boolean f4681P;

    /* renamed from: Q */
    private final boolean f4682Q;

    /* renamed from: R */
    private final boolean f4683R;

    /* renamed from: S */
    private final boolean f4684S;

    /* renamed from: T */
    private final boolean f4685T;

    /* renamed from: U */
    private final boolean f4686U;

    public LocationSettingsStates(boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6) {
        this.f4681P = z;
        this.f4682Q = z2;
        this.f4683R = z3;
        this.f4684S = z4;
        this.f4685T = z5;
        this.f4686U = z6;
    }

    /* renamed from: c */
    public final boolean mo18243c() {
        return this.f4686U;
    }

    /* renamed from: d */
    public final boolean mo18244d() {
        return this.f4683R;
    }

    /* renamed from: u */
    public final boolean mo18245u() {
        return this.f4684S;
    }

    /* renamed from: v */
    public final boolean mo18246v() {
        return this.f4681P;
    }

    /* renamed from: w */
    public final boolean mo18247w() {
        return this.f4685T;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5605a(parcel, 1, mo18246v());
        C2250b.m5605a(parcel, 2, mo18249x());
        C2250b.m5605a(parcel, 3, mo18244d());
        C2250b.m5605a(parcel, 4, mo18245u());
        C2250b.m5605a(parcel, 5, mo18247w());
        C2250b.m5605a(parcel, 6, mo18243c());
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final boolean mo18249x() {
        return this.f4682Q;
    }
}
