package com.google.android.gms.common.api.internal;

import androidx.annotation.BinderThread;
import com.google.android.gms.signin.internal.C3270c;
import com.google.android.gms.signin.internal.zaj;
import java.lang.ref.WeakReference;

/* renamed from: com.google.android.gms.common.api.internal.g0 */
final class C2077g0 extends C3270c {

    /* renamed from: a */
    private final WeakReference<C2153z> f3328a;

    C2077g0(C2153z zVar) {
        this.f3328a = new WeakReference<>(zVar);
    }

    @BinderThread
    /* renamed from: a */
    public final void mo16699a(zaj zaj) {
        C2153z zVar = this.f3328a.get();
        if (zVar != null) {
            zVar.f3509a.mo16783a(new C2081h0(this, zVar, zVar, zaj));
        }
    }
}
