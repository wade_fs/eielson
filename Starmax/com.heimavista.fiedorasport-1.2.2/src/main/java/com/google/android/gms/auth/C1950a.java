package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.auth.a */
public final class C1950a implements Parcelable.Creator<AccountChangeEvent> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        String str = null;
        String str2 = null;
        long j = 0;
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    i = C2248a.m5585z(parcel, a);
                    break;
                case 2:
                    j = C2248a.m5546B(parcel, a);
                    break;
                case 3:
                    str = C2248a.m5573n(parcel, a);
                    break;
                case 4:
                    i2 = C2248a.m5585z(parcel, a);
                    break;
                case 5:
                    i3 = C2248a.m5585z(parcel, a);
                    break;
                case 6:
                    str2 = C2248a.m5573n(parcel, a);
                    break;
                default:
                    C2248a.m5550F(parcel, a);
                    break;
            }
        }
        C2248a.m5576q(parcel, b);
        return new AccountChangeEvent(i, j, str, i2, i3, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new AccountChangeEvent[i];
    }
}
