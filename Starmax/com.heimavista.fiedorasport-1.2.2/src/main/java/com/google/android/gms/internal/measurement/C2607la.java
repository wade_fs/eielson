package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.la */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2607la implements C2593l2<C2653oa> {

    /* renamed from: Q */
    private static C2607la f4307Q = new C2607la();

    /* renamed from: P */
    private final C2593l2<C2653oa> f4308P;

    private C2607la(C2593l2<C2653oa> l2Var) {
        this.f4308P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6742b() {
        return ((C2653oa) f4307Q.mo17285a()).mo17779a();
    }

    /* renamed from: c */
    public static boolean m6743c() {
        return ((C2653oa) f4307Q.mo17285a()).mo17780e();
    }

    /* renamed from: d */
    public static boolean m6744d() {
        return ((C2653oa) f4307Q.mo17285a()).mo17781f();
    }

    /* renamed from: e */
    public static boolean m6745e() {
        return ((C2653oa) f4307Q.mo17285a()).mo17782g();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4308P.mo17285a();
    }

    public C2607la() {
        this(C2579k2.m6601a(new C2637na()));
    }
}
