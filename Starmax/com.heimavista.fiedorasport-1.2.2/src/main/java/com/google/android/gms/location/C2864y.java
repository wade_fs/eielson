package com.google.android.gms.location;

import java.util.Comparator;

/* renamed from: com.google.android.gms.location.y */
final class C2864y implements Comparator<DetectedActivity> {
    C2864y() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        DetectedActivity detectedActivity = (DetectedActivity) obj;
        DetectedActivity detectedActivity2 = (DetectedActivity) obj2;
        int compareTo = Integer.valueOf(detectedActivity2.mo18211c()).compareTo(Integer.valueOf(detectedActivity.mo18211c()));
        return compareTo == 0 ? Integer.valueOf(detectedActivity.mo18212d()).compareTo(Integer.valueOf(detectedActivity2.mo18212d())) : compareTo;
    }
}
