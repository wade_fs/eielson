package com.google.android.datatransport.cct.p084b;

/* renamed from: com.google.android.datatransport.cct.b.i */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1678i extends C1695t {

    /* renamed from: a */
    private final long f2623a;

    C1678i(long j) {
        this.f2623a = j;
    }

    /* renamed from: a */
    public long mo13512a() {
        return this.f2623a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1695t) || this.f2623a != ((C1695t) obj).mo13512a()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        long j = this.f2623a;
        return 1000003 ^ ((int) (j ^ (j >>> 32)));
    }

    public String toString() {
        return "LogResponse{nextRequestWaitMillis=" + this.f2623a + "}";
    }
}
