package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.sb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2713sb implements C2730tb {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4486a;

    /* renamed from: b */
    private static final C2765w1<Double> f4487b;

    /* renamed from: c */
    private static final C2765w1<Long> f4488c;

    /* renamed from: d */
    private static final C2765w1<Long> f4489d;

    /* renamed from: e */
    private static final C2765w1<String> f4490e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4486a = c2Var.mo17367a("measurement.test.boolean_flag", false);
        f4487b = c2Var.mo17364a("measurement.test.double_flag", -3.0d);
        f4488c = c2Var.mo17365a("measurement.test.int_flag", -2L);
        f4489d = c2Var.mo17365a("measurement.test.long_flag", -1L);
        f4490e = c2Var.mo17366a("measurement.test.string_flag", "---");
    }

    /* renamed from: a */
    public final boolean mo17876a() {
        return f4486a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final double mo17877e() {
        return f4487b.mo18128b().doubleValue();
    }

    /* renamed from: f */
    public final long mo17878f() {
        return f4488c.mo18128b().longValue();
    }

    /* renamed from: g */
    public final long mo17879g() {
        return f4489d.mo18128b().longValue();
    }

    /* renamed from: t */
    public final String mo17880t() {
        return f4490e.mo18128b();
    }
}
