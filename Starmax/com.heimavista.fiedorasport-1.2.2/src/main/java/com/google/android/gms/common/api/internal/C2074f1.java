package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2020d;
import com.google.android.gms.common.api.C2033e;
import com.google.android.gms.common.api.C2157k;

/* renamed from: com.google.android.gms.common.api.internal.f1 */
public final class C2074f1<O extends C2016a.C2020d> extends C2139v {

    /* renamed from: c */
    private final C2033e<O> f3321c;

    public C2074f1(C2033e<O> eVar) {
        super("Method is not supported by connectionless client. APIs supporting connectionless client must not call this method.");
        this.f3321c = eVar;
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16564a(@NonNull T t) {
        this.f3321c.mo16549a((C2056c) t);
        return t;
    }

    /* renamed from: a */
    public final void mo16566a(C2129s1 s1Var) {
    }

    /* renamed from: e */
    public final Context mo16573e() {
        return this.f3321c.mo16556e();
    }

    /* renamed from: f */
    public final Looper mo16574f() {
        return this.f3321c.mo16558g();
    }
}
