package com.google.android.gms.internal.location;

import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.location.C2833e;
import com.google.android.gms.location.C2844j0;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationResult;

/* renamed from: com.google.android.gms.internal.location.j */
final class C2395j extends C2844j0 {

    /* renamed from: a */
    private final C2084i<C2833e> f3926a;

    C2395j(C2084i<C2833e> iVar) {
        this.f3926a = iVar;
    }

    /* renamed from: H */
    public final synchronized void mo17212H() {
        this.f3926a.mo16719a();
    }

    /* renamed from: a */
    public final void mo17213a(LocationAvailability locationAvailability) {
        this.f3926a.mo16720a(new C2397l(this, locationAvailability));
    }

    /* renamed from: a */
    public final void mo17214a(LocationResult locationResult) {
        this.f3926a.mo16720a(new C2396k(this, locationResult));
    }
}
