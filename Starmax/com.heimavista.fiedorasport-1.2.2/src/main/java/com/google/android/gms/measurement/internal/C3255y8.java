package com.google.android.gms.measurement.internal;

import android.app.job.JobParameters;
import android.content.Intent;

/* renamed from: com.google.android.gms.measurement.internal.y8 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public interface C3255y8 {
    /* renamed from: a */
    void mo18704a(JobParameters jobParameters, boolean z);

    /* renamed from: a */
    void mo18705a(Intent intent);

    /* renamed from: a */
    boolean mo18706a(int i);
}
