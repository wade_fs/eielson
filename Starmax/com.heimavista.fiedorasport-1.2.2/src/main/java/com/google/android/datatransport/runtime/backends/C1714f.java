package com.google.android.datatransport.runtime.backends;

import androidx.annotation.Nullable;
import com.google.android.datatransport.runtime.backends.C1707a;
import p119e.p144d.p145a.p146a.p147i.C3899h;

/* renamed from: com.google.android.datatransport.runtime.backends.f */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C1714f {

    /* renamed from: com.google.android.datatransport.runtime.backends.f$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public static abstract class C1715a {
        /* renamed from: a */
        public abstract C1715a mo13539a(Iterable<C3899h> iterable);

        /* renamed from: a */
        public abstract C1715a mo13540a(@Nullable byte[] bArr);

        /* renamed from: a */
        public abstract C1714f mo13541a();
    }

    /* renamed from: c */
    public static C1715a m4281c() {
        return new C1707a.C1709b();
    }

    /* renamed from: a */
    public abstract Iterable<C3899h> mo13534a();

    @Nullable
    /* renamed from: b */
    public abstract byte[] mo13535b();
}
