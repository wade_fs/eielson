package com.google.android.exoplayer2.p086ui;

import android.graphics.Bitmap;
import com.google.android.exoplayer2.p086ui.PlayerNotificationManager;

/* renamed from: com.google.android.exoplayer2.ui.d */
/* compiled from: lambda */
public final /* synthetic */ class C1918d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ PlayerNotificationManager.BitmapCallback f2927P;

    /* renamed from: Q */
    private final /* synthetic */ Bitmap f2928Q;

    public /* synthetic */ C1918d(PlayerNotificationManager.BitmapCallback bitmapCallback, Bitmap bitmap) {
        this.f2927P = bitmapCallback;
        this.f2928Q = bitmap;
    }

    public final void run() {
        this.f2927P.mo15785a(this.f2928Q);
    }
}
