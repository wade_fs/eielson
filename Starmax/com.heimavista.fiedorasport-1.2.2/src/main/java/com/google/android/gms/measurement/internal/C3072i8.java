package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;

/* renamed from: com.google.android.gms.measurement.internal.i8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3072i8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5215P;

    /* renamed from: Q */
    private final /* synthetic */ boolean f5216Q;

    /* renamed from: R */
    private final /* synthetic */ zzan f5217R;

    /* renamed from: S */
    private final /* synthetic */ zzm f5218S;

    /* renamed from: T */
    private final /* synthetic */ String f5219T;

    /* renamed from: U */
    private final /* synthetic */ C3232w7 f5220U;

    C3072i8(C3232w7 w7Var, boolean z, boolean z2, zzan zzan, zzm zzm, String str) {
        this.f5220U = w7Var;
        this.f5215P = z;
        this.f5216Q = z2;
        this.f5217R = zzan;
        this.f5218S = zzm;
        this.f5219T = str;
    }

    public final void run() {
        C3228w3 d = this.f5220U.f5724d;
        if (d == null) {
            this.f5220U.mo19015l().mo19001t().mo19042a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.f5215P) {
            this.f5220U.mo19381a(d, this.f5216Q ? null : this.f5217R, this.f5218S);
        } else {
            try {
                if (TextUtils.isEmpty(this.f5219T)) {
                    d.mo19189a(this.f5217R, this.f5218S);
                } else {
                    d.mo19190a(this.f5217R, this.f5219T, this.f5220U.mo19015l().mo18997C());
                }
            } catch (RemoteException e) {
                this.f5220U.mo19015l().mo19001t().mo19043a("Failed to send event to the service", e);
            }
        }
        this.f5220U.m9314J();
    }
}
