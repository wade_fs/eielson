package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;

/* renamed from: com.google.android.exoplayer2.audio.e */
/* compiled from: lambda */
public final /* synthetic */ class C1775e implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f2794P;

    /* renamed from: Q */
    private final /* synthetic */ DecoderCounters f2795Q;

    public /* synthetic */ C1775e(AudioRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f2794P = eventDispatcher;
        this.f2795Q = decoderCounters;
    }

    public final void run() {
        this.f2794P.mo14095a(this.f2795Q);
    }
}
