package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.share.internal.MessengerShareContentUtility;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class StreetViewSource extends AbstractSafeParcelable {
    public static final Parcelable.Creator<StreetViewSource> CREATOR = new C2952w();

    /* renamed from: Q */
    public static final StreetViewSource f4896Q = new StreetViewSource(0);

    /* renamed from: P */
    private final int f4897P;

    static {
        Class<StreetViewSource> cls = StreetViewSource.class;
        new StreetViewSource(1);
    }

    public StreetViewSource(int i) {
        this.f4897P = i;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof StreetViewSource) && this.f4897P == ((StreetViewSource) obj).f4897P;
    }

    public final int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4897P));
    }

    public final String toString() {
        String str;
        int i = this.f4897P;
        if (i == 0) {
            str = MessengerShareContentUtility.PREVIEW_DEFAULT;
        } else if (i != 1) {
            str = String.format("UNKNOWN(%s)", Integer.valueOf(i));
        } else {
            str = "OUTDOOR";
        }
        return String.format("StreetViewSource:%s", str);
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 2, this.f4897P);
        C2250b.m5587a(parcel, a);
    }
}
