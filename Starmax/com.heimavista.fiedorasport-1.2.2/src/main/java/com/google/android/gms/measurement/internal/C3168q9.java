package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.q9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3168q9 implements C3104l4 {

    /* renamed from: a */
    private final /* synthetic */ String f5577a;

    /* renamed from: b */
    private final /* synthetic */ C3145o9 f5578b;

    C3168q9(C3145o9 o9Var, String str) {
        this.f5578b = o9Var;
        this.f5577a = str;
    }

    /* renamed from: a */
    public final void mo19135a(String str, int i, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        this.f5578b.mo19209a(i, th, bArr, this.f5577a);
    }
}
