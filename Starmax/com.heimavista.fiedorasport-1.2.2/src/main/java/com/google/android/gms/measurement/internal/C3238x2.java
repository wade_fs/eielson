package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2761vc;

/* renamed from: com.google.android.gms.measurement.internal.x2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3238x2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5741a = new C3238x2();

    private C3238x2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2761vc.m7456b());
    }
}
