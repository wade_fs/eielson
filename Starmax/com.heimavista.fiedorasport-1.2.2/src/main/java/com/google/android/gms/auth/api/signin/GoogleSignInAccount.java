package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.util.C2314e;
import com.google.android.gms.common.util.C2317h;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInAccount extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInAccount> CREATOR = new C1983f();

    /* renamed from: c0 */
    private static C2314e f3075c0 = C2317h.m5780c();

    /* renamed from: P */
    private final int f3076P;

    /* renamed from: Q */
    private String f3077Q;

    /* renamed from: R */
    private String f3078R;

    /* renamed from: S */
    private String f3079S;

    /* renamed from: T */
    private String f3080T;

    /* renamed from: U */
    private Uri f3081U;

    /* renamed from: V */
    private String f3082V;

    /* renamed from: W */
    private long f3083W;

    /* renamed from: X */
    private String f3084X;

    /* renamed from: Y */
    private List<Scope> f3085Y;

    /* renamed from: Z */
    private String f3086Z;

    /* renamed from: a0 */
    private String f3087a0;

    /* renamed from: b0 */
    private Set<Scope> f3088b0 = new HashSet();

    GoogleSignInAccount(int i, String str, String str2, String str3, String str4, Uri uri, String str5, long j, String str6, List<Scope> list, String str7, String str8) {
        this.f3076P = i;
        this.f3077Q = str;
        this.f3078R = str2;
        this.f3079S = str3;
        this.f3080T = str4;
        this.f3081U = uri;
        this.f3082V = str5;
        this.f3083W = j;
        this.f3084X = str6;
        this.f3085Y = list;
        this.f3086Z = str7;
        this.f3087a0 = str8;
    }

    /* renamed from: E */
    private final JSONObject m4505E() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (mo16392x() != null) {
                jSONObject.put("id", mo16392x());
            }
            if (mo16393y() != null) {
                jSONObject.put("tokenId", mo16393y());
            }
            if (mo16388u() != null) {
                jSONObject.put(NotificationCompat.CATEGORY_EMAIL, mo16388u());
            }
            if (mo16385d() != null) {
                jSONObject.put("displayName", mo16385d());
            }
            if (mo16390w() != null) {
                jSONObject.put("givenName", mo16390w());
            }
            if (mo16389v() != null) {
                jSONObject.put("familyName", mo16389v());
            }
            if (mo16394z() != null) {
                jSONObject.put("photoUrl", mo16394z().toString());
            }
            if (mo16381B() != null) {
                jSONObject.put("serverAuthCode", mo16381B());
            }
            jSONObject.put("expirationTime", this.f3083W);
            jSONObject.put("obfuscatedIdentifier", this.f3084X);
            JSONArray jSONArray = new JSONArray();
            Scope[] scopeArr = (Scope[]) this.f3085Y.toArray(new Scope[this.f3085Y.size()]);
            Arrays.sort(scopeArr, C1982e.f3124P);
            for (Scope scope : scopeArr) {
                jSONArray.put(scope.mo16507c());
            }
            jSONObject.put("grantedScopes", jSONArray);
            return jSONObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    private static GoogleSignInAccount m4507a(@Nullable String str, @Nullable String str2, @Nullable String str3, @Nullable String str4, @Nullable String str5, @Nullable String str6, @Nullable Uri uri, @Nullable Long l, @NonNull String str7, @NonNull Set<Scope> set) {
        long longValue = (l == null ? Long.valueOf(f3075c0.mo17132a() / 1000) : l).longValue();
        C2258v.m5639b(str7);
        C2258v.m5629a(set);
        return new GoogleSignInAccount(3, str, str2, str3, str4, uri, null, longValue, str7, new ArrayList(set), str5, str6);
    }

    @Nullable
    /* renamed from: b */
    public static GoogleSignInAccount m4508b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        String optString = jSONObject.optString("photoUrl", null);
        Uri parse = !TextUtils.isEmpty(optString) ? Uri.parse(optString) : null;
        long parseLong = Long.parseLong(jSONObject.getString("expirationTime"));
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("grantedScopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        GoogleSignInAccount a = m4507a(jSONObject.optString("id"), jSONObject.optString("tokenId", null), jSONObject.optString(NotificationCompat.CATEGORY_EMAIL, null), jSONObject.optString("displayName", null), jSONObject.optString("givenName", null), jSONObject.optString("familyName", null), parse, Long.valueOf(parseLong), jSONObject.getString("obfuscatedIdentifier"), hashSet);
        a.f3082V = jSONObject.optString("serverAuthCode", null);
        return a;
    }

    @NonNull
    /* renamed from: A */
    public Set<Scope> mo16380A() {
        HashSet hashSet = new HashSet(this.f3085Y);
        hashSet.addAll(this.f3088b0);
        return hashSet;
    }

    @Nullable
    /* renamed from: B */
    public String mo16381B() {
        return this.f3082V;
    }

    @NonNull
    /* renamed from: C */
    public final String mo16382C() {
        return this.f3084X;
    }

    /* renamed from: D */
    public final String mo16383D() {
        JSONObject E = m4505E();
        E.remove("serverAuthCode");
        return E.toString();
    }

    @Nullable
    /* renamed from: c */
    public Account mo16384c() {
        String str = this.f3079S;
        if (str == null) {
            return null;
        }
        return new Account(str, "com.google");
    }

    @Nullable
    /* renamed from: d */
    public String mo16385d() {
        return this.f3080T;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof GoogleSignInAccount)) {
            return false;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) obj;
        return googleSignInAccount.f3084X.equals(this.f3084X) && googleSignInAccount.mo16380A().equals(mo16380A());
    }

    public int hashCode() {
        return ((this.f3084X.hashCode() + 527) * 31) + mo16380A().hashCode();
    }

    @Nullable
    /* renamed from: u */
    public String mo16388u() {
        return this.f3079S;
    }

    @Nullable
    /* renamed from: v */
    public String mo16389v() {
        return this.f3087a0;
    }

    @Nullable
    /* renamed from: w */
    public String mo16390w() {
        return this.f3086Z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.net.Uri, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3076P);
        C2250b.m5602a(parcel, 2, mo16392x(), false);
        C2250b.m5602a(parcel, 3, mo16393y(), false);
        C2250b.m5602a(parcel, 4, mo16388u(), false);
        C2250b.m5602a(parcel, 5, mo16385d(), false);
        C2250b.m5596a(parcel, 6, (Parcelable) mo16394z(), i, false);
        C2250b.m5602a(parcel, 7, mo16381B(), false);
        C2250b.m5592a(parcel, 8, this.f3083W);
        C2250b.m5602a(parcel, 9, this.f3084X, false);
        C2250b.m5614c(parcel, 10, this.f3085Y, false);
        C2250b.m5602a(parcel, 11, mo16390w(), false);
        C2250b.m5602a(parcel, 12, mo16389v(), false);
        C2250b.m5587a(parcel, a);
    }

    @Nullable
    /* renamed from: x */
    public String mo16392x() {
        return this.f3077Q;
    }

    @Nullable
    /* renamed from: y */
    public String mo16393y() {
        return this.f3078R;
    }

    @Nullable
    /* renamed from: z */
    public Uri mo16394z() {
        return this.f3081U;
    }
}
