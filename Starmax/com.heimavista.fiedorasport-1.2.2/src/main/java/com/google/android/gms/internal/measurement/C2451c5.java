package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.c5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public class C2451c5 {

    /* renamed from: a */
    private volatile C2739u5 f4015a;

    /* renamed from: b */
    private volatile C2498f3 f4016b;

    static {
        C2798y3.m7828a();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:7|8|9|10|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0012 */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final com.google.android.gms.internal.measurement.C2739u5 m6134b(com.google.android.gms.internal.measurement.C2739u5 r2) {
        /*
            r1 = this;
            com.google.android.gms.internal.measurement.u5 r0 = r1.f4015a
            if (r0 != 0) goto L_0x001d
            monitor-enter(r1)
            com.google.android.gms.internal.measurement.u5 r0 = r1.f4015a     // Catch:{ all -> 0x001a }
            if (r0 == 0) goto L_0x000b
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x000b:
            r1.f4015a = r2     // Catch:{ t4 -> 0x0012 }
            com.google.android.gms.internal.measurement.f3 r0 = com.google.android.gms.internal.measurement.C2498f3.f4094Q     // Catch:{ t4 -> 0x0012 }
            r1.f4016b = r0     // Catch:{ t4 -> 0x0012 }
            goto L_0x0018
        L_0x0012:
            r1.f4015a = r2     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.measurement.f3 r2 = com.google.android.gms.internal.measurement.C2498f3.f4094Q     // Catch:{ all -> 0x001a }
            r1.f4016b = r2     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            goto L_0x001d
        L_0x001a:
            r2 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x001a }
            throw r2
        L_0x001d:
            com.google.android.gms.internal.measurement.u5 r2 = r1.f4015a
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2451c5.m6134b(com.google.android.gms.internal.measurement.u5):com.google.android.gms.internal.measurement.u5");
    }

    /* renamed from: a */
    public final C2739u5 mo17370a(C2739u5 u5Var) {
        C2739u5 u5Var2 = this.f4015a;
        this.f4016b = null;
        this.f4015a = u5Var;
        return u5Var2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2451c5)) {
            return false;
        }
        C2451c5 c5Var = (C2451c5) obj;
        C2739u5 u5Var = this.f4015a;
        C2739u5 u5Var2 = c5Var.f4015a;
        if (u5Var == null && u5Var2 == null) {
            return mo17371b().equals(c5Var.mo17371b());
        }
        if (u5Var != null && u5Var2 != null) {
            return u5Var.equals(u5Var2);
        }
        if (u5Var != null) {
            return u5Var.equals(c5Var.m6134b(u5Var.mo17658a()));
        }
        return m6134b(u5Var2.mo17658a()).equals(u5Var2);
    }

    public int hashCode() {
        return 1;
    }

    /* renamed from: a */
    public final int mo17369a() {
        if (this.f4016b != null) {
            return this.f4016b.mo17469a();
        }
        if (this.f4015a != null) {
            return this.f4015a.mo17663e();
        }
        return 0;
    }

    /* renamed from: b */
    public final C2498f3 mo17371b() {
        if (this.f4016b != null) {
            return this.f4016b;
        }
        synchronized (this) {
            if (this.f4016b != null) {
                C2498f3 f3Var = this.f4016b;
                return f3Var;
            }
            if (this.f4015a == null) {
                this.f4016b = C2498f3.f4094Q;
            } else {
                this.f4016b = this.f4015a.mo17940c();
            }
            C2498f3 f3Var2 = this.f4016b;
            return f3Var2;
        }
    }
}
