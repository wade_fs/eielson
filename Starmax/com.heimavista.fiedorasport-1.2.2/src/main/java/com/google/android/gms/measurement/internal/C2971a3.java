package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2537h8;

/* renamed from: com.google.android.gms.measurement.internal.a3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C2971a3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f4943a = new C2971a3();

    private C2971a3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2537h8.m6451b());
    }
}
