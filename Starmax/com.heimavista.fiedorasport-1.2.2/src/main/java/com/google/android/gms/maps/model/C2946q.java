package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.maps.model.q */
public final class C2946q implements Parcelable.Creator<PolygonOptions> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = null;
        ArrayList arrayList3 = null;
        float f = 0.0f;
        int i = 0;
        int i2 = 0;
        float f2 = 0.0f;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 2:
                    arrayList2 = C2248a.m5562c(parcel, a, LatLng.CREATOR);
                    break;
                case 3:
                    C2248a.m5557a(parcel, a, arrayList, C2946q.class.getClassLoader());
                    break;
                case 4:
                    f = C2248a.m5582w(parcel, a);
                    break;
                case 5:
                    i = C2248a.m5585z(parcel, a);
                    break;
                case 6:
                    i2 = C2248a.m5585z(parcel, a);
                    break;
                case 7:
                    f2 = C2248a.m5582w(parcel, a);
                    break;
                case 8:
                    z = C2248a.m5577r(parcel, a);
                    break;
                case 9:
                    z2 = C2248a.m5577r(parcel, a);
                    break;
                case 10:
                    z3 = C2248a.m5577r(parcel, a);
                    break;
                case 11:
                    i3 = C2248a.m5585z(parcel, a);
                    break;
                case 12:
                    arrayList3 = C2248a.m5562c(parcel, a, PatternItem.CREATOR);
                    break;
                default:
                    C2248a.m5550F(parcel, a);
                    break;
            }
        }
        C2248a.m5576q(parcel, b);
        return new PolygonOptions(arrayList2, arrayList, f, i, i2, f2, z, z2, z3, i3, arrayList3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new PolygonOptions[i];
    }
}
