package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import androidx.annotation.WorkerThread;

/* renamed from: com.google.android.gms.measurement.internal.n6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C3130n6 {
    @WorkerThread
    /* renamed from: a */
    void mo18721a(String str, String str2, Bundle bundle, long j);
}
