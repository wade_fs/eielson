package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2477e0;
import com.google.android.gms.internal.measurement.C2510g0;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/* renamed from: com.google.android.gms.measurement.internal.ja */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
abstract class C3086ja {

    /* renamed from: a */
    String f5272a;

    /* renamed from: b */
    int f5273b;

    /* renamed from: c */
    Boolean f5274c;

    /* renamed from: d */
    Boolean f5275d;

    /* renamed from: e */
    Long f5276e;

    /* renamed from: f */
    Long f5277f;

    C3086ja(String str, int i) {
        this.f5272a = str;
        this.f5273b = i;
    }

    /* renamed from: a */
    static Boolean m8816a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract int mo19038a();

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract boolean mo19040b();

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public abstract boolean mo19041c();

    /* renamed from: a */
    static Boolean m8819a(String str, C2510g0 g0Var, C3032f4 f4Var) {
        String str2;
        List<String> list;
        C2258v.m5629a(g0Var);
        if (str == null || !g0Var.mo17486n() || g0Var.mo17487o() == C2510g0.C2511a.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (g0Var.mo17487o() == C2510g0.C2511a.IN_LIST) {
            if (g0Var.mo17493v() == 0) {
                return null;
            }
        } else if (!g0Var.mo17488p()) {
            return null;
        }
        C2510g0.C2511a o = g0Var.mo17487o();
        boolean t = g0Var.mo17491t();
        if (t || o == C2510g0.C2511a.REGEXP || o == C2510g0.C2511a.IN_LIST) {
            str2 = g0Var.mo17489q();
        } else {
            str2 = g0Var.mo17489q().toUpperCase(Locale.ENGLISH);
        }
        String str3 = str2;
        if (g0Var.mo17493v() == 0) {
            list = null;
        } else {
            List<String> u = g0Var.mo17492u();
            if (!t) {
                ArrayList arrayList = new ArrayList(u.size());
                for (String str4 : u) {
                    arrayList.add(str4.toUpperCase(Locale.ENGLISH));
                }
                u = Collections.unmodifiableList(arrayList);
            }
            list = u;
        }
        return m8818a(str, o, t, str3, list, o == C2510g0.C2511a.REGEXP ? str3 : null, f4Var);
    }

    /* renamed from: a */
    private static Boolean m8818a(String str, C2510g0.C2511a aVar, boolean z, String str2, List<String> list, String str3, C3032f4 f4Var) {
        if (str == null) {
            return null;
        }
        if (aVar == C2510g0.C2511a.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z && aVar != C2510g0.C2511a.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (C3038fa.f5140a[aVar.ordinal()]) {
            case 1:
                try {
                    return Boolean.valueOf(Pattern.compile(str3, z ? 0 : 66).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    if (f4Var != null) {
                        f4Var.mo19004w().mo19043a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    }
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    /* renamed from: a */
    static Boolean m8815a(long j, C2477e0 e0Var) {
        try {
            return m8820a(new BigDecimal(j), e0Var, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* renamed from: a */
    static Boolean m8814a(double d, C2477e0 e0Var) {
        try {
            return m8820a(new BigDecimal(d), e0Var, Math.ulp(d));
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* renamed from: a */
    static Boolean m8817a(String str, C2477e0 e0Var) {
        if (!C3223v9.m9272a(str)) {
            return null;
        }
        try {
            return m8820a(new BigDecimal(str), e0Var, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0085, code lost:
        if (r2 != null) goto L_0x0087;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.Boolean m8820a(java.math.BigDecimal r9, com.google.android.gms.internal.measurement.C2477e0 r10, double r11) {
        /*
            com.google.android.gms.common.internal.C2258v.m5629a(r10)
            boolean r0 = r10.mo17430n()
            r1 = 0
            if (r0 == 0) goto L_0x0110
            com.google.android.gms.internal.measurement.e0$b r0 = r10.mo17431o()
            com.google.android.gms.internal.measurement.e0$b r2 = com.google.android.gms.internal.measurement.C2477e0.C2479b.UNKNOWN_COMPARISON_TYPE
            if (r0 != r2) goto L_0x0014
            goto L_0x0110
        L_0x0014:
            com.google.android.gms.internal.measurement.e0$b r0 = r10.mo17431o()
            com.google.android.gms.internal.measurement.e0$b r2 = com.google.android.gms.internal.measurement.C2477e0.C2479b.BETWEEN
            if (r0 != r2) goto L_0x0029
            boolean r0 = r10.mo17436u()
            if (r0 == 0) goto L_0x0028
            boolean r0 = r10.mo17438w()
            if (r0 != 0) goto L_0x0030
        L_0x0028:
            return r1
        L_0x0029:
            boolean r0 = r10.mo17434s()
            if (r0 != 0) goto L_0x0030
            return r1
        L_0x0030:
            com.google.android.gms.internal.measurement.e0$b r0 = r10.mo17431o()
            com.google.android.gms.internal.measurement.e0$b r2 = r10.mo17431o()
            com.google.android.gms.internal.measurement.e0$b r3 = com.google.android.gms.internal.measurement.C2477e0.C2479b.BETWEEN
            if (r2 != r3) goto L_0x0067
            java.lang.String r2 = r10.mo17437v()
            boolean r2 = com.google.android.gms.measurement.internal.C3223v9.m9272a(r2)
            if (r2 == 0) goto L_0x0066
            java.lang.String r2 = r10.mo17439x()
            boolean r2 = com.google.android.gms.measurement.internal.C3223v9.m9272a(r2)
            if (r2 != 0) goto L_0x0051
            goto L_0x0066
        L_0x0051:
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0066 }
            java.lang.String r3 = r10.mo17437v()     // Catch:{ NumberFormatException -> 0x0066 }
            r2.<init>(r3)     // Catch:{ NumberFormatException -> 0x0066 }
            java.math.BigDecimal r3 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0066 }
            java.lang.String r10 = r10.mo17439x()     // Catch:{ NumberFormatException -> 0x0066 }
            r3.<init>(r10)     // Catch:{ NumberFormatException -> 0x0066 }
            r10 = r2
            r2 = r1
            goto L_0x007d
        L_0x0066:
            return r1
        L_0x0067:
            java.lang.String r2 = r10.mo17435t()
            boolean r2 = com.google.android.gms.measurement.internal.C3223v9.m9272a(r2)
            if (r2 != 0) goto L_0x0072
            return r1
        L_0x0072:
            java.math.BigDecimal r2 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0110 }
            java.lang.String r10 = r10.mo17435t()     // Catch:{ NumberFormatException -> 0x0110 }
            r2.<init>(r10)     // Catch:{ NumberFormatException -> 0x0110 }
            r10 = r1
            r3 = r10
        L_0x007d:
            com.google.android.gms.internal.measurement.e0$b r4 = com.google.android.gms.internal.measurement.C2477e0.C2479b.BETWEEN
            if (r0 != r4) goto L_0x0085
            if (r10 == 0) goto L_0x0084
            goto L_0x0087
        L_0x0084:
            return r1
        L_0x0085:
            if (r2 == 0) goto L_0x0110
        L_0x0087:
            int[] r4 = com.google.android.gms.measurement.internal.C3038fa.f5141b
            int r0 = r0.ordinal()
            r0 = r4[r0]
            r4 = -1
            r5 = 0
            r6 = 1
            if (r0 == r6) goto L_0x0104
            r7 = 2
            if (r0 == r7) goto L_0x00f8
            r8 = 3
            if (r0 == r8) goto L_0x00b0
            r11 = 4
            if (r0 == r11) goto L_0x009e
            goto L_0x0110
        L_0x009e:
            int r10 = r9.compareTo(r10)
            if (r10 == r4) goto L_0x00ab
            int r9 = r9.compareTo(r3)
            if (r9 == r6) goto L_0x00ab
            r5 = 1
        L_0x00ab:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x00b0:
            r0 = 0
            int r10 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r10 == 0) goto L_0x00ec
            java.math.BigDecimal r10 = new java.math.BigDecimal
            r10.<init>(r11)
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r0.<init>(r7)
            java.math.BigDecimal r10 = r10.multiply(r0)
            java.math.BigDecimal r10 = r2.subtract(r10)
            int r10 = r9.compareTo(r10)
            if (r10 != r6) goto L_0x00e7
            java.math.BigDecimal r10 = new java.math.BigDecimal
            r10.<init>(r11)
            java.math.BigDecimal r11 = new java.math.BigDecimal
            r11.<init>(r7)
            java.math.BigDecimal r10 = r10.multiply(r11)
            java.math.BigDecimal r10 = r2.add(r10)
            int r9 = r9.compareTo(r10)
            if (r9 != r4) goto L_0x00e7
            r5 = 1
        L_0x00e7:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x00ec:
            int r9 = r9.compareTo(r2)
            if (r9 != 0) goto L_0x00f3
            r5 = 1
        L_0x00f3:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x00f8:
            int r9 = r9.compareTo(r2)
            if (r9 != r6) goto L_0x00ff
            r5 = 1
        L_0x00ff:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x0104:
            int r9 = r9.compareTo(r2)
            if (r9 != r4) goto L_0x010b
            r5 = 1
        L_0x010b:
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r5)
            return r9
        L_0x0110:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3086ja.m8820a(java.math.BigDecimal, com.google.android.gms.internal.measurement.e0, double):java.lang.Boolean");
    }
}
