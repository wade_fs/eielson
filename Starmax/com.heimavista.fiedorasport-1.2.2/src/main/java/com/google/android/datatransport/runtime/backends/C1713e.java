package com.google.android.datatransport.runtime.backends;

import androidx.annotation.Nullable;

/* renamed from: com.google.android.datatransport.runtime.backends.e */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public interface C1713e {
    @Nullable
    C1724m get(String str);
}
