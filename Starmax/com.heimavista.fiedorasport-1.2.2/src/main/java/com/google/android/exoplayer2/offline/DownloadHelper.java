package com.google.android.exoplayer2.offline;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.exoplayer2.source.TrackGroupArray;
import java.io.IOException;
import java.util.List;

public abstract class DownloadHelper {

    public interface Callback {
        void onPrepareError(DownloadHelper downloadHelper, IOException iOException);

        void onPrepared(DownloadHelper downloadHelper);
    }

    public abstract DownloadAction getDownloadAction(@Nullable byte[] bArr, List<TrackKey> list);

    public abstract int getPeriodCount();

    public abstract DownloadAction getRemoveAction(@Nullable byte[] bArr);

    public abstract TrackGroupArray getTrackGroups(int i);

    public void prepare(final Callback callback) {
        final Handler handler = new Handler(Looper.myLooper() != null ? Looper.myLooper() : Looper.getMainLooper());
        new Thread() {
            /* class com.google.android.exoplayer2.offline.DownloadHelper.C18391 */

            /* renamed from: a */
            public /* synthetic */ void mo14710a(Callback callback) {
                callback.onPrepared(DownloadHelper.this);
            }

            public void run() {
                try {
                    DownloadHelper.this.prepareInternal();
                    handler.post(new C1843a(this, callback));
                } catch (IOException e) {
                    handler.post(new C1844b(this, callback, e));
                }
            }

            /* renamed from: a */
            public /* synthetic */ void mo14711a(Callback callback, IOException iOException) {
                callback.onPrepareError(DownloadHelper.this, iOException);
            }
        }.start();
    }

    /* access modifiers changed from: protected */
    public abstract void prepareInternal();
}
