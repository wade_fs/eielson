package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.k */
/* compiled from: lambda */
public final /* synthetic */ class C1889k implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2909P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2910Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSourceEventListener.LoadEventInfo f2911R;

    /* renamed from: S */
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData f2912S;

    public /* synthetic */ C1889k(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f2909P = eventDispatcher;
        this.f2910Q = mediaSourceEventListener;
        this.f2911R = loadEventInfo;
        this.f2912S = mediaLoadData;
    }

    public final void run() {
        this.f2909P.mo14938c(this.f2910Q, this.f2911R, this.f2912S);
    }
}
