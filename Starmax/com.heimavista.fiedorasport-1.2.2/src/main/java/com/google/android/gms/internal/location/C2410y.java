package com.google.android.gms.internal.location;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2219h;

/* renamed from: com.google.android.gms.internal.location.y */
public class C2410y extends C2219h<C2392g> {

    /* renamed from: E */
    private final String f3932E;

    /* renamed from: F */
    protected final C2406u<C2392g> f3933F = new C2411z(this);

    public C2410y(Context context, Looper looper, C2036f.C2038b bVar, C2036f.C2039c cVar, String str, C2211e eVar) {
        super(context, looper, 23, eVar, bVar, cVar);
        this.f3932E = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.internal.IGoogleLocationManagerService");
        return queryLocalInterface instanceof C2392g ? (C2392g) queryLocalInterface : new C2393h(iBinder);
    }

    /* renamed from: i */
    public int mo16451i() {
        return 11925000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public Bundle mo16936u() {
        Bundle bundle = new Bundle();
        bundle.putString("client_name", this.f3932E);
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public String mo16453y() {
        return "com.google.android.gms.location.internal.IGoogleLocationManagerService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: z */
    public String mo16454z() {
        return "com.google.android.location.internal.GoogleLocationManagerService.START";
    }
}
