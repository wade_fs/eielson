package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;
import java.util.Map;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.internal.measurement.jb */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2574jb extends IInterface {
    void beginAdUnitExposure(String str, long j);

    void clearConditionalUserProperty(String str, String str2, Bundle bundle);

    void endAdUnitExposure(String str, long j);

    void generateEventId(C2589kc kcVar);

    void getAppInstanceId(C2589kc kcVar);

    void getCachedAppInstanceId(C2589kc kcVar);

    void getConditionalUserProperties(String str, String str2, C2589kc kcVar);

    void getCurrentScreenClass(C2589kc kcVar);

    void getCurrentScreenName(C2589kc kcVar);

    void getGmpAppId(C2589kc kcVar);

    void getMaxUserProperties(String str, C2589kc kcVar);

    void getTestFlag(C2589kc kcVar, int i);

    void getUserProperties(String str, String str2, boolean z, C2589kc kcVar);

    void initForTests(Map map);

    void initialize(C3988b bVar, zzv zzv, long j);

    void isDataCollectionEnabled(C2589kc kcVar);

    void logEvent(String str, String str2, Bundle bundle, boolean z, boolean z2, long j);

    void logEventAndBundle(String str, String str2, Bundle bundle, C2589kc kcVar, long j);

    void logHealthData(int i, String str, C3988b bVar, C3988b bVar2, C3988b bVar3);

    void onActivityCreated(C3988b bVar, Bundle bundle, long j);

    void onActivityDestroyed(C3988b bVar, long j);

    void onActivityPaused(C3988b bVar, long j);

    void onActivityResumed(C3988b bVar, long j);

    void onActivitySaveInstanceState(C3988b bVar, C2589kc kcVar, long j);

    void onActivityStarted(C3988b bVar, long j);

    void onActivityStopped(C3988b bVar, long j);

    void performAction(Bundle bundle, C2589kc kcVar, long j);

    void registerOnMeasurementEventListener(C2459cd cdVar);

    void resetAnalyticsData(long j);

    void setConditionalUserProperty(Bundle bundle, long j);

    void setCurrentScreen(C3988b bVar, String str, String str2, long j);

    void setDataCollectionEnabled(boolean z);

    void setEventInterceptor(C2459cd cdVar);

    void setInstanceIdProvider(C2475dd ddVar);

    void setMeasurementEnabled(boolean z, long j);

    void setMinimumSessionDuration(long j);

    void setSessionTimeoutDuration(long j);

    void setUserId(String str, long j);

    void setUserProperty(String str, String str2, C3988b bVar, boolean z, long j);

    void unregisterOnMeasurementEventListener(C2459cd cdVar);
}
