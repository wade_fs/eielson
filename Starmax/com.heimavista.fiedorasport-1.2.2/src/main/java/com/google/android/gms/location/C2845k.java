package com.google.android.gms.location;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.List;

/* renamed from: com.google.android.gms.location.k */
public final class C2845k implements Parcelable.Creator<LocationResult> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        List<Location> list = LocationResult.f4673Q;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            if (C2248a.m5551a(a) != 1) {
                C2248a.m5550F(parcel, a);
            } else {
                list = C2248a.m5562c(parcel, a, Location.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new LocationResult(list);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationResult[i];
    }
}
