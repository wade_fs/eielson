package com.google.android.gms.common.internal;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p161c.p163b.C4010b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4011c;

/* renamed from: com.google.android.gms.common.internal.o */
public interface C2237o extends IInterface {

    /* renamed from: com.google.android.gms.common.internal.o$a */
    public static abstract class C2238a extends C4010b implements C2237o {
        public C2238a() {
            super("com.google.android.gms.common.internal.IGmsCallbacks");
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final boolean mo17015a(int i, Parcel parcel, Parcel parcel2, int i2) {
            if (i == 1) {
                mo16951a(parcel.readInt(), parcel.readStrongBinder(), (Bundle) C4011c.m12011a(parcel, Bundle.CREATOR));
            } else if (i == 2) {
                mo16953b(parcel.readInt(), (Bundle) C4011c.m12011a(parcel, Bundle.CREATOR));
            } else if (i != 3) {
                return false;
            } else {
                mo16952a(parcel.readInt(), parcel.readStrongBinder(), (zzb) C4011c.m12011a(parcel, zzb.CREATOR));
            }
            parcel2.writeNoException();
            return true;
        }
    }

    /* renamed from: a */
    void mo16951a(int i, IBinder iBinder, Bundle bundle);

    /* renamed from: a */
    void mo16952a(int i, IBinder iBinder, zzb zzb);

    /* renamed from: b */
    void mo16953b(int i, Bundle bundle);
}
