package com.google.android.gms.common.util.p091s;

import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.google.android.gms.common.util.s.c */
public class C2330c implements ThreadFactory {

    /* renamed from: P */
    private final String f3862P;

    /* renamed from: Q */
    private final AtomicInteger f3863Q;

    /* renamed from: R */
    private final ThreadFactory f3864R;

    public C2330c(String str) {
        this(str, 0);
    }

    public Thread newThread(Runnable runnable) {
        Thread newThread = this.f3864R.newThread(new C2331d(runnable, 0));
        String str = this.f3862P;
        int andIncrement = this.f3863Q.getAndIncrement();
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 13);
        sb.append(str);
        sb.append("[");
        sb.append(andIncrement);
        sb.append("]");
        newThread.setName(sb.toString());
        return newThread;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    private C2330c(String str, int i) {
        this.f3863Q = new AtomicInteger();
        this.f3864R = Executors.defaultThreadFactory();
        C2258v.m5630a((Object) str, (Object) "Name must not be null");
        this.f3862P = str;
    }
}
