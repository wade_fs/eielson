package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.tb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C2730tb {
    /* renamed from: a */
    boolean mo17876a();

    /* renamed from: e */
    double mo17877e();

    /* renamed from: f */
    long mo17878f();

    /* renamed from: g */
    long mo17879g();

    /* renamed from: t */
    String mo17880t();
}
