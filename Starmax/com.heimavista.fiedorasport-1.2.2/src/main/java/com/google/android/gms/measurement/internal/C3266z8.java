package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.z8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3266z8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ long f5795P;

    /* renamed from: Q */
    private final /* synthetic */ C3244x8 f5796Q;

    C3266z8(C3244x8 x8Var, long j) {
        this.f5796Q = x8Var;
        this.f5795P = j;
    }

    public final void run() {
        this.f5796Q.m9360b(this.f5795P);
    }
}
