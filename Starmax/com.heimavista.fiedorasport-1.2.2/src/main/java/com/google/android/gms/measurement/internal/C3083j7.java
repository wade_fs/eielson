package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.j7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C3083j7 {
    /* renamed from: a */
    String mo19108a();

    /* renamed from: a */
    List<Bundle> mo19109a(String str, String str2);

    /* renamed from: a */
    Map<String, Object> mo19110a(String str, String str2, boolean z);

    /* renamed from: a */
    void mo19111a(String str);

    /* renamed from: a */
    void mo19112a(String str, String str2, Bundle bundle);

    /* renamed from: a */
    void mo19113a(String str, String str2, Object obj);

    /* renamed from: b */
    int mo19114b(String str);

    /* renamed from: b */
    void mo19115b(String str, String str2, Bundle bundle);

    /* renamed from: c */
    void mo19116c(String str);

    /* renamed from: d */
    void mo19117d(Bundle bundle);

    /* renamed from: d */
    void mo19118d(boolean z);

    /* renamed from: e */
    String mo19119e();

    /* renamed from: f */
    String mo19120f();

    /* renamed from: g */
    String mo19121g();

    /* renamed from: t */
    long mo19122t();
}
