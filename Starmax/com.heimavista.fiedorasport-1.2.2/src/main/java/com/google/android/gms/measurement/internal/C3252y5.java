package com.google.android.gms.measurement.internal;

import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.y5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3252y5 implements Callable<byte[]> {

    /* renamed from: a */
    private final /* synthetic */ zzan f5769a;

    /* renamed from: b */
    private final /* synthetic */ String f5770b;

    /* renamed from: c */
    private final /* synthetic */ C3141o5 f5771c;

    C3252y5(C3141o5 o5Var, zzan zzan, String str) {
        this.f5771c = o5Var;
        this.f5769a = zzan;
        this.f5770b = str;
    }

    public final /* synthetic */ Object call() {
        this.f5771c.f5496a.mo19237q();
        this.f5771c.f5496a.mo19231g().mo19291a(this.f5769a, this.f5770b);
        throw null;
    }
}
