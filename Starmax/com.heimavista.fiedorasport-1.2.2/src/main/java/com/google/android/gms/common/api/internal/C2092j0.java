package com.google.android.gms.common.api.internal;

import androidx.annotation.WorkerThread;

/* renamed from: com.google.android.gms.common.api.internal.j0 */
abstract class C2092j0 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2153z f3347P;

    private C2092j0(C2153z zVar) {
        this.f3347P = zVar;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public abstract void mo16643a();

    @WorkerThread
    public void run() {
        this.f3347P.f3510b.lock();
        try {
            if (!Thread.interrupted()) {
                mo16643a();
                this.f3347P.f3510b.unlock();
            }
        } catch (RuntimeException e) {
            this.f3347P.f3509a.mo16784a(e);
        } finally {
            this.f3347P.f3510b.unlock();
        }
    }

    /* synthetic */ C2092j0(C2153z zVar, C2048a0 a0Var) {
        this(zVar);
    }
}
