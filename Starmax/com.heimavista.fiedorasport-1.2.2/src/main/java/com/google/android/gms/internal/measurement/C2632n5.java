package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.n5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2632n5 implements C2754v5 {

    /* renamed from: a */
    private C2754v5[] f4350a;

    C2632n5(C2754v5... v5VarArr) {
        this.f4350a = v5VarArr;
    }

    /* renamed from: a */
    public final boolean mo17588a(Class<?> cls) {
        for (C2754v5 v5Var : this.f4350a) {
            if (v5Var.mo17588a(cls)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: b */
    public final C2707s5 mo17589b(Class<?> cls) {
        C2754v5[] v5VarArr = this.f4350a;
        for (C2754v5 v5Var : v5VarArr) {
            if (v5Var.mo17588a(cls)) {
                return v5Var.mo17589b(cls);
            }
        }
        String valueOf = String.valueOf(cls.getName());
        throw new UnsupportedOperationException(valueOf.length() != 0 ? "No factory is available for message type: ".concat(valueOf) : new String("No factory is available for message type: "));
    }
}
