package com.google.android.exoplayer2.source.ads;

import com.google.android.exoplayer2.source.ads.AdsMediaSource;

/* renamed from: com.google.android.exoplayer2.source.ads.c */
/* compiled from: lambda */
public final /* synthetic */ class C1861c implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AdsMediaSource.ComponentListener f2860P;

    /* renamed from: Q */
    private final /* synthetic */ AdPlaybackState f2861Q;

    public /* synthetic */ C1861c(AdsMediaSource.ComponentListener componentListener, AdPlaybackState adPlaybackState) {
        this.f2860P = componentListener;
        this.f2861Q = adPlaybackState;
    }

    public final void run() {
        this.f2860P.mo15087a(this.f2861Q);
    }
}
