package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.e9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2488e9 implements C2593l2<C2471d9> {

    /* renamed from: Q */
    private static C2488e9 f4075Q = new C2488e9();

    /* renamed from: P */
    private final C2593l2<C2471d9> f4076P;

    private C2488e9(C2593l2<C2471d9> l2Var) {
        this.f4076P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6260b() {
        return ((C2471d9) f4075Q.mo17285a()).mo17429a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4076P.mo17285a();
    }

    public C2488e9() {
        this(C2579k2.m6601a(new C2521g9()));
    }
}
