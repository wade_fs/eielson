package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1669f;

/* renamed from: com.google.android.datatransport.cct.b.m */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class C1684m {

    /* renamed from: com.google.android.datatransport.cct.b.m$a */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class C1685a {
        @NonNull
        /* renamed from: a */
        public abstract C1685a mo13474a(@Nullable C1661a aVar);

        @NonNull
        /* renamed from: a */
        public abstract C1685a mo13475a(@Nullable C1686b bVar);

        @NonNull
        /* renamed from: a */
        public abstract C1684m mo13476a();
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: com.google.android.datatransport.cct.b.m$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static final class C1686b extends Enum<C1686b> {

        /* renamed from: P */
        public static final C1686b f2628P = new C1686b("UNKNOWN", 0, 0);

        /* renamed from: Q */
        public static final C1686b f2629Q = new C1686b("ANDROID", 1, 4);

        static {
            C1686b[] bVarArr = {f2628P, f2629Q};
        }

        private C1686b(String str, int i, int i2) {
        }
    }

    @NonNull
    /* renamed from: a */
    public static C1685a m4216a() {
        return new C1669f.C1671b();
    }
}
