package com.google.android.gms.common.api.internal;

import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.internal.C2258v;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: com.google.android.gms.common.api.internal.j */
public class C2091j {

    /* renamed from: a */
    private final Set<C2084i<?>> f3346a = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: a */
    public final void mo16732a() {
        for (C2084i<?> iVar : this.f3346a) {
            iVar.mo16719a();
        }
        this.f3346a.clear();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [L, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    /* renamed from: a */
    public static <L> C2084i<L> m4935a(@NonNull L l, @NonNull Looper looper, @NonNull String str) {
        C2258v.m5630a((Object) l, (Object) "Listener must not be null");
        C2258v.m5630a(looper, "Looper must not be null");
        C2258v.m5630a((Object) str, (Object) "Listener type must not be null");
        return new C2084i<>(looper, l, str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [L, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String */
    /* renamed from: a */
    public static <L> C2084i.C2085a<L> m4934a(@NonNull L l, @NonNull String str) {
        C2258v.m5630a((Object) l, (Object) "Listener must not be null");
        C2258v.m5630a((Object) str, (Object) "Listener type must not be null");
        C2258v.m5631a(str, (Object) "Listener type must not be empty");
        return new C2084i.C2085a<>(l, str);
    }
}
