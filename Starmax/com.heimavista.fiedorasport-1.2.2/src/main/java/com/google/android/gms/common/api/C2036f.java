package com.google.android.gms.common.api;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.internal.C2056c;
import com.google.android.gms.common.api.internal.C2070e2;
import com.google.android.gms.common.api.internal.C2076g;
import com.google.android.gms.common.api.internal.C2099l;
import com.google.android.gms.common.api.internal.C2100l0;
import com.google.android.gms.common.api.internal.C2102l2;
import com.google.android.gms.common.api.internal.C2129s1;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2258v;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.ReentrantLock;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4049b;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.f */
public abstract class C2036f {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final Set<C2036f> f3202a = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: com.google.android.gms.common.api.f$b */
    public interface C2038b {
        /* renamed from: L */
        void mo16583L(int i);

        /* renamed from: f */
        void mo16584f(@Nullable Bundle bundle);
    }

    /* renamed from: com.google.android.gms.common.api.f$c */
    public interface C2039c {
        /* renamed from: a */
        void mo16585a(@NonNull ConnectionResult connectionResult);
    }

    /* renamed from: h */
    public static Set<C2036f> m4692h() {
        Set<C2036f> set;
        synchronized (f3202a) {
            set = f3202a;
        }
        return set;
    }

    /* renamed from: a */
    public abstract ConnectionResult mo16563a();

    /* renamed from: a */
    public <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16564a(@NonNull T t) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public abstract void mo16565a(@NonNull C2039c cVar);

    /* renamed from: a */
    public abstract void mo16567a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    /* renamed from: b */
    public abstract C2040g<Status> mo16569b();

    /* renamed from: b */
    public abstract void mo16570b(@NonNull C2039c cVar);

    /* renamed from: c */
    public abstract void mo16571c();

    /* renamed from: d */
    public abstract void mo16572d();

    /* renamed from: e */
    public Context mo16573e() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: f */
    public Looper mo16574f() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: g */
    public void mo16575g() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: com.google.android.gms.common.api.f$a */
    public static final class C2037a {

        /* renamed from: a */
        private Account f3203a;

        /* renamed from: b */
        private final Set<Scope> f3204b = new HashSet();

        /* renamed from: c */
        private final Set<Scope> f3205c = new HashSet();

        /* renamed from: d */
        private int f3206d;

        /* renamed from: e */
        private View f3207e;

        /* renamed from: f */
        private String f3208f;

        /* renamed from: g */
        private String f3209g;

        /* renamed from: h */
        private final Map<C2016a<?>, C2211e.C2213b> f3210h = new ArrayMap();

        /* renamed from: i */
        private final Context f3211i;

        /* renamed from: j */
        private final Map<C2016a<?>, C2016a.C2020d> f3212j = new ArrayMap();

        /* renamed from: k */
        private C2076g f3213k;

        /* renamed from: l */
        private int f3214l = -1;

        /* renamed from: m */
        private C2039c f3215m;

        /* renamed from: n */
        private Looper f3216n;

        /* renamed from: o */
        private C2167b f3217o = C2167b.m5251a();

        /* renamed from: p */
        private C2016a.C2017a<? extends C4052e, C4047a> f3218p = C4049b.f7396c;

        /* renamed from: q */
        private final ArrayList<C2038b> f3219q = new ArrayList<>();

        /* renamed from: r */
        private final ArrayList<C2039c> f3220r = new ArrayList<>();

        public C2037a(@NonNull Context context) {
            this.f3211i = context;
            this.f3216n = context.getMainLooper();
            this.f3208f = context.getPackageName();
            this.f3209g = context.getClass().getName();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
         arg types: [android.os.Handler, java.lang.String]
         candidates:
          com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
          com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
          com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
          com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
        /* renamed from: a */
        public final C2037a mo16576a(@NonNull Handler handler) {
            C2258v.m5630a((Object) handler, (Object) "Handler must not be null");
            this.f3216n = handler.getLooper();
            return this;
        }

        /* renamed from: b */
        public final C2211e mo16582b() {
            C4047a aVar = C4047a.f7385X;
            if (this.f3212j.containsKey(C4049b.f7398e)) {
                aVar = (C4047a) this.f3212j.get(C4049b.f7398e);
            }
            return new C2211e(this.f3203a, this.f3204b, this.f3210h, this.f3206d, this.f3207e, this.f3208f, this.f3209g, aVar, false);
        }

        /* renamed from: a */
        public final C2037a mo16579a(@NonNull C2038b bVar) {
            C2258v.m5630a(bVar, "Listener must not be null");
            this.f3219q.add(bVar);
            return this;
        }

        /* renamed from: a */
        public final C2037a mo16580a(@NonNull C2039c cVar) {
            C2258v.m5630a(cVar, "Listener must not be null");
            this.f3220r.add(cVar);
            return this;
        }

        /* renamed from: a */
        public final C2037a mo16577a(@NonNull C2016a<? extends C2016a.C2020d.C2024d> aVar) {
            C2258v.m5630a(aVar, "Api must not be null");
            this.f3212j.put(aVar, null);
            List<Scope> a = aVar.mo16523c().mo16372a(null);
            this.f3205c.addAll(a);
            this.f3204b.addAll(a);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
         arg types: [O, java.lang.String]
         candidates:
          com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
          com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
          com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
          com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
        /* renamed from: a */
        public final <O extends C2016a.C2020d.C2023c> C2037a mo16578a(@NonNull C2016a<O> aVar, @NonNull O o) {
            C2258v.m5630a(aVar, "Api must not be null");
            C2258v.m5630a((Object) o, (Object) "Null options are not permitted for this Api");
            this.f3212j.put(aVar, o);
            List<Scope> a = aVar.mo16523c().mo16372a(o);
            this.f3205c.addAll(a);
            this.f3204b.addAll(a);
            return this;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.api.internal.l0.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int
         arg types: [java.util.Collection, int]
         candidates:
          com.google.android.gms.common.api.internal.l0.a(int, boolean):void
          com.google.android.gms.common.api.internal.i1.a(int, boolean):void
          com.google.android.gms.common.api.internal.l0.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int */
        /* renamed from: a */
        public final C2036f mo16581a() {
            C2258v.m5637a(!this.f3212j.isEmpty(), "must call addApi() to add at least one API");
            C2211e b = mo16582b();
            C2016a aVar = null;
            Map<C2016a<?>, C2211e.C2213b> f = b.mo16963f();
            ArrayMap arrayMap = new ArrayMap();
            ArrayMap arrayMap2 = new ArrayMap();
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (C2016a aVar2 : this.f3212j.keySet()) {
                C2016a.C2020d dVar = this.f3212j.get(aVar2);
                boolean z2 = f.get(aVar2) != null;
                arrayMap.put(aVar2, Boolean.valueOf(z2));
                C2102l2 l2Var = new C2102l2(aVar2, z2);
                arrayList.add(l2Var);
                C2016a.C2017a d = aVar2.mo16524d();
                C2016a aVar3 = aVar2;
                C2016a.C2027f a = d.mo16371a(this.f3211i, this.f3216n, b, dVar, l2Var, l2Var);
                arrayMap2.put(aVar3.mo16521a(), a);
                if (d.mo16527a() == 1) {
                    z = dVar != null;
                }
                if (a.mo16450d()) {
                    if (aVar == null) {
                        aVar = aVar3;
                    } else {
                        String b2 = aVar3.mo16522b();
                        String b3 = aVar.mo16522b();
                        StringBuilder sb = new StringBuilder(String.valueOf(b2).length() + 21 + String.valueOf(b3).length());
                        sb.append(b2);
                        sb.append(" cannot be used with ");
                        sb.append(b3);
                        throw new IllegalStateException(sb.toString());
                    }
                }
            }
            if (aVar != null) {
                if (!z) {
                    C2258v.m5642b(this.f3203a == null, "Must not set an account in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead", aVar.mo16522b());
                    C2258v.m5642b(this.f3204b.equals(this.f3205c), "Must not set scopes in GoogleApiClient.Builder when using %s. Set account in GoogleSignInOptions.Builder instead.", aVar.mo16522b());
                } else {
                    String b4 = aVar.mo16522b();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b4).length() + 82);
                    sb2.append("With using ");
                    sb2.append(b4);
                    sb2.append(", GamesOptions can only be specified within GoogleSignInOptions.Builder");
                    throw new IllegalStateException(sb2.toString());
                }
            }
            C2100l0 l0Var = new C2100l0(this.f3211i, new ReentrantLock(), this.f3216n, b, this.f3217o, this.f3218p, arrayMap, this.f3219q, this.f3220r, arrayMap2, this.f3214l, C2100l0.m4951a((Iterable<C2016a.C2027f>) arrayMap2.values(), true), arrayList, false);
            synchronized (C2036f.f3202a) {
                C2036f.f3202a.add(l0Var);
            }
            if (this.f3214l >= 0) {
                C2070e2.m4870b(this.f3213k).mo16689a(this.f3214l, l0Var, this.f3215m);
            }
            return l0Var;
        }
    }

    /* renamed from: a */
    public boolean mo16568a(C2099l lVar) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void mo16566a(C2129s1 s1Var) {
        throw new UnsupportedOperationException();
    }
}
