package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.content.Context;
import com.google.android.datatransport.runtime.backends.C1713e;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.n */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C1744n implements Factory<C1743m> {

    /* renamed from: a */
    private final Provider<Context> f2762a;

    /* renamed from: b */
    private final Provider<C1713e> f2763b;

    /* renamed from: c */
    private final Provider<C3934c> f2764c;

    /* renamed from: d */
    private final Provider<C1749s> f2765d;

    /* renamed from: e */
    private final Provider<Executor> f2766e;

    /* renamed from: f */
    private final Provider<C3969b> f2767f;

    /* renamed from: g */
    private final Provider<C3971a> f2768g;

    public C1744n(Provider<Context> aVar, Provider<C1713e> aVar2, Provider<C3934c> aVar3, Provider<C1749s> aVar4, Provider<Executor> aVar5, Provider<C3969b> aVar6, Provider<C3971a> aVar7) {
        this.f2762a = aVar;
        this.f2763b = aVar2;
        this.f2764c = aVar3;
        this.f2765d = aVar4;
        this.f2766e = aVar5;
        this.f2767f = aVar6;
        this.f2768g = aVar7;
    }

    /* renamed from: a */
    public static C1744n m4360a(Provider<Context> aVar, Provider<C1713e> aVar2, Provider<C3934c> aVar3, Provider<C1749s> aVar4, Provider<Executor> aVar5, Provider<C3969b> aVar6, Provider<C3971a> aVar7) {
        return new C1744n(aVar, aVar2, aVar3, aVar4, aVar5, aVar6, aVar7);
    }

    public C1743m get() {
        return new C1743m(this.f2762a.get(), this.f2763b.get(), this.f2764c.get(), this.f2765d.get(), this.f2766e.get(), this.f2767f.get(), this.f2768g.get());
    }
}
