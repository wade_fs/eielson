package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.b8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2438b8 implements C2803y7 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4001a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.sdk.collection.validate_param_names_alphabetical", false);

    /* renamed from: a */
    public final boolean mo17336a() {
        return f4001a.mo18128b().booleanValue();
    }
}
