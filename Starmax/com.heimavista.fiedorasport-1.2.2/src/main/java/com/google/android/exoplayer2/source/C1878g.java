package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.g */
/* compiled from: lambda */
public final /* synthetic */ class C1878g implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2892P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2893Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData f2894R;

    public /* synthetic */ C1878g(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f2892P = eventDispatcher;
        this.f2893Q = mediaSourceEventListener;
        this.f2894R = mediaLoadData;
    }

    public final void run() {
        this.f2892P.mo14933a(this.f2893Q, this.f2894R);
    }
}
