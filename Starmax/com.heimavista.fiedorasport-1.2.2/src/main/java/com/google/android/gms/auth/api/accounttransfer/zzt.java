package com.google.android.gms.auth.api.accounttransfer;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.collection.ArraySet;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.internal.auth.zzaz;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class zzt extends zzaz {
    public static final Parcelable.Creator<zzt> CREATOR = new C1957d();

    /* renamed from: W */
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> f3017W;

    /* renamed from: P */
    private final Set<Integer> f3018P;

    /* renamed from: Q */
    private final int f3019Q;

    /* renamed from: R */
    private String f3020R;

    /* renamed from: S */
    private int f3021S;

    /* renamed from: T */
    private byte[] f3022T;

    /* renamed from: U */
    private PendingIntent f3023U;

    /* renamed from: V */
    private DeviceMetaData f3024V;

    static {
        HashMap<String, FastJsonResponse.Field<?, ?>> hashMap = new HashMap<>();
        f3017W = hashMap;
        hashMap.put("accountType", FastJsonResponse.Field.m5715c("accountType", 2));
        f3017W.put("status", FastJsonResponse.Field.m5713b("status", 3));
        f3017W.put("transferBytes", FastJsonResponse.Field.m5710a("transferBytes", 4));
    }

    zzt(Set<Integer> set, int i, String str, int i2, byte[] bArr, PendingIntent pendingIntent, DeviceMetaData deviceMetaData) {
        this.f3018P = set;
        this.f3019Q = i;
        this.f3020R = str;
        this.f3021S = i2;
        this.f3022T = bArr;
        this.f3023U = pendingIntent;
        this.f3024V = deviceMetaData;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo16319a(FastJsonResponse.Field field) {
        int c = field.mo17091c();
        if (c == 1) {
            return Integer.valueOf(this.f3019Q);
        }
        if (c == 2) {
            return this.f3020R;
        }
        if (c == 3) {
            return Integer.valueOf(this.f3021S);
        }
        if (c == 4) {
            return this.f3022T;
        }
        int c2 = field.mo17091c();
        StringBuilder sb = new StringBuilder(37);
        sb.append("Unknown SafeParcelable id=");
        sb.append(c2);
        throw new IllegalStateException(sb.toString());
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo16321b(FastJsonResponse.Field field) {
        return this.f3018P.contains(Integer.valueOf(field.mo17091c()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
     arg types: [android.os.Parcel, int, byte[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.app.PendingIntent, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.auth.api.accounttransfer.DeviceMetaData, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        Set<Integer> set = this.f3018P;
        if (set.contains(1)) {
            C2250b.m5591a(parcel, 1, this.f3019Q);
        }
        if (set.contains(2)) {
            C2250b.m5602a(parcel, 2, this.f3020R, true);
        }
        if (set.contains(3)) {
            C2250b.m5591a(parcel, 3, this.f3021S);
        }
        if (set.contains(4)) {
            C2250b.m5606a(parcel, 4, this.f3022T, true);
        }
        if (set.contains(5)) {
            C2250b.m5596a(parcel, 5, (Parcelable) this.f3023U, i, true);
        }
        if (set.contains(6)) {
            C2250b.m5596a(parcel, 6, (Parcelable) this.f3024V, i, true);
        }
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: a */
    public /* synthetic */ Map mo16320a() {
        return f3017W;
    }

    public zzt() {
        this.f3018P = new ArraySet(3);
        this.f3019Q = 1;
    }
}
