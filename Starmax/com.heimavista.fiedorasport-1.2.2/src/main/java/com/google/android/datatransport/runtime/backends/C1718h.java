package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import androidx.annotation.NonNull;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: com.google.android.datatransport.runtime.backends.h */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C1718h {
    /* renamed from: a */
    public static C1718h m4292a(Context context, C3971a aVar, C3971a aVar2, String str) {
        return new C1711c(context, aVar, aVar2, str);
    }

    /* renamed from: a */
    public abstract Context mo13547a();

    @NonNull
    /* renamed from: b */
    public abstract String mo13548b();

    /* renamed from: c */
    public abstract C3971a mo13549c();

    /* renamed from: d */
    public abstract C3971a mo13550d();
}
