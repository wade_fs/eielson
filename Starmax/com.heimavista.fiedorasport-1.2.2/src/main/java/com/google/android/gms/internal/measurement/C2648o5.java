package com.google.android.gms.internal.measurement;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: com.google.android.gms.internal.measurement.o5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2648o5<K, V> extends LinkedHashMap<K, V> {

    /* renamed from: Q */
    private static final C2648o5 f4369Q;

    /* renamed from: P */
    private boolean f4370P = true;

    static {
        C2648o5 o5Var = new C2648o5();
        f4369Q = o5Var;
        o5Var.f4370P = false;
    }

    private C2648o5() {
    }

    /* renamed from: g */
    public static <K, V> C2648o5<K, V> m6968g() {
        return f4369Q;
    }

    /* renamed from: h */
    private final void m6969h() {
        if (!this.f4370P) {
            throw new UnsupportedOperationException();
        }
    }

    /* renamed from: a */
    public final void mo17789a(C2648o5<K, V> o5Var) {
        m6969h();
        if (!super.isEmpty()) {
            putAll(o5Var);
        }
    }

    public final void clear() {
        m6969h();
        super.clear();
    }

    /* renamed from: e */
    public final void mo17791e() {
        this.f4370P = false;
    }

    public final Set<Map.Entry<K, V>> entrySet() {
        return isEmpty() ? Collections.emptySet() : super.entrySet();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean equals(java.lang.Object r7) {
        /*
            r6 = this;
            boolean r0 = r7 instanceof java.util.Map
            r1 = 0
            if (r0 == 0) goto L_0x005d
            java.util.Map r7 = (java.util.Map) r7
            r0 = 1
            if (r6 == r7) goto L_0x0059
            int r2 = r6.size()
            int r3 = r7.size()
            if (r2 == r3) goto L_0x0016
        L_0x0014:
            r7 = 0
            goto L_0x005a
        L_0x0016:
            java.util.Set r2 = r6.entrySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x001e:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0059
            java.lang.Object r3 = r2.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            java.lang.Object r4 = r3.getKey()
            boolean r4 = r7.containsKey(r4)
            if (r4 != 0) goto L_0x0035
            goto L_0x0014
        L_0x0035:
            java.lang.Object r4 = r3.getValue()
            java.lang.Object r3 = r3.getKey()
            java.lang.Object r3 = r7.get(r3)
            boolean r5 = r4 instanceof byte[]
            if (r5 == 0) goto L_0x0052
            boolean r5 = r3 instanceof byte[]
            if (r5 == 0) goto L_0x0052
            byte[] r4 = (byte[]) r4
            byte[] r3 = (byte[]) r3
            boolean r3 = java.util.Arrays.equals(r4, r3)
            goto L_0x0056
        L_0x0052:
            boolean r3 = r4.equals(r3)
        L_0x0056:
            if (r3 != 0) goto L_0x001e
            goto L_0x0014
        L_0x0059:
            r7 = 1
        L_0x005a:
            if (r7 == 0) goto L_0x005d
            return r0
        L_0x005d:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2648o5.equals(java.lang.Object):boolean");
    }

    /* renamed from: f */
    public final boolean mo17794f() {
        return this.f4370P;
    }

    public final int hashCode() {
        int i = 0;
        for (Map.Entry entry : entrySet()) {
            i += m6967a(entry.getValue()) ^ m6967a(entry.getKey());
        }
        return i;
    }

    public final V put(K k, V v) {
        m6969h();
        C2647o4.m6961a(k);
        C2647o4.m6961a(v);
        return super.put(k, v);
    }

    public final void putAll(Map<? extends K, ? extends V> map) {
        m6969h();
        for (Object obj : map.keySet()) {
            C2647o4.m6961a(obj);
            C2647o4.m6961a(map.get(obj));
        }
        super.putAll(map);
    }

    public final V remove(Object obj) {
        m6969h();
        return super.remove(obj);
    }

    private C2648o5(Map<K, V> map) {
        super(map);
    }

    /* renamed from: a */
    private static int m6967a(Object obj) {
        if (obj instanceof byte[]) {
            return C2647o4.m6966c((byte[]) obj);
        }
        if (!(obj instanceof C2631n4)) {
            return obj.hashCode();
        }
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public final C2648o5<K, V> mo17788a() {
        return isEmpty() ? new C2648o5<>() : new C2648o5<>(this);
    }
}
