package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.x7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3243x7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5756P;

    /* renamed from: Q */
    private final /* synthetic */ zzkq f5757Q;

    /* renamed from: R */
    private final /* synthetic */ zzm f5758R;

    /* renamed from: S */
    private final /* synthetic */ C3232w7 f5759S;

    C3243x7(C3232w7 w7Var, boolean z, zzkq zzkq, zzm zzm) {
        this.f5759S = w7Var;
        this.f5756P = z;
        this.f5757Q = zzkq;
        this.f5758R = zzm;
    }

    public final void run() {
        C3228w3 d = this.f5759S.f5724d;
        if (d == null) {
            this.f5759S.mo19015l().mo19001t().mo19042a("Discarding data. Failed to set user property");
            return;
        }
        this.f5759S.mo19381a(d, this.f5756P ? null : this.f5757Q, this.f5758R);
        this.f5759S.m9314J();
    }
}
