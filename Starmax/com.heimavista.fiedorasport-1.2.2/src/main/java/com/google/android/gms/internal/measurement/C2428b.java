package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.b */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2428b extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f3984T;

    /* renamed from: U */
    private final /* synthetic */ String f3985U;

    /* renamed from: V */
    private final /* synthetic */ Context f3986V;

    /* renamed from: W */
    private final /* synthetic */ Bundle f3987W;

    /* renamed from: X */
    private final /* synthetic */ C2525gd f3988X;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2428b(C2525gd gdVar, String str, String str2, Context context, Bundle bundle) {
        super(gdVar);
        this.f3988X = gdVar;
        this.f3984T = str;
        this.f3985U = str2;
        this.f3986V = context;
        this.f3987W = bundle;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.gd.a(com.google.android.gms.internal.measurement.gd, java.lang.Exception, boolean, boolean):void
     arg types: [com.google.android.gms.internal.measurement.gd, java.lang.Exception, int, int]
     candidates:
      com.google.android.gms.internal.measurement.gd.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
      com.google.android.gms.internal.measurement.gd.a(com.google.android.gms.internal.measurement.gd, java.lang.Exception, boolean, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0072, code lost:
        if (r4 < r3) goto L_0x0074;
     */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0054 A[Catch:{ Exception -> 0x009e }] */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0060 A[Catch:{ Exception -> 0x009e }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo17293a() {
        /*
            r14 = this;
            r0 = 0
            r1 = 1
            com.google.android.gms.internal.measurement.gd r2 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ Exception -> 0x009e }
            r3.<init>()     // Catch:{ Exception -> 0x009e }
            java.util.List unused = r2.f4189d = r3     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.gd r2 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = r14.f3984T     // Catch:{ Exception -> 0x009e }
            java.lang.String r4 = r14.f3985U     // Catch:{ Exception -> 0x009e }
            boolean r2 = com.google.android.gms.internal.measurement.C2525gd.m6399b(r3, r4)     // Catch:{ Exception -> 0x009e }
            r3 = 0
            if (r2 == 0) goto L_0x0027
            java.lang.String r3 = r14.f3985U     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r14.f3984T     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.gd r4 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            java.lang.String r4 = r4.f4186a     // Catch:{ Exception -> 0x009e }
            r10 = r2
            r11 = r3
            r9 = r4
            goto L_0x002a
        L_0x0027:
            r9 = r3
            r10 = r9
            r11 = r10
        L_0x002a:
            android.content.Context r2 = r14.f3986V     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.C2525gd.m6409i(r2)     // Catch:{ Exception -> 0x009e }
            java.lang.Boolean r2 = com.google.android.gms.internal.measurement.C2525gd.f4181j     // Catch:{ Exception -> 0x009e }
            boolean r2 = r2.booleanValue()     // Catch:{ Exception -> 0x009e }
            if (r2 != 0) goto L_0x003e
            if (r10 == 0) goto L_0x003c
            goto L_0x003e
        L_0x003c:
            r2 = 0
            goto L_0x003f
        L_0x003e:
            r2 = 1
        L_0x003f:
            com.google.android.gms.internal.measurement.gd r3 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.gd r4 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            android.content.Context r5 = r14.f3986V     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.jb r4 = r4.mo17508a(r5, r2)     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.C2574jb unused = r3.f4192g = r4     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.gd r3 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.jb r3 = r3.f4192g     // Catch:{ Exception -> 0x009e }
            if (r3 != 0) goto L_0x0060
            com.google.android.gms.internal.measurement.gd r2 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            java.lang.String r2 = r2.f4186a     // Catch:{ Exception -> 0x009e }
            java.lang.String r3 = "Failed to connect to measurement client."
            android.util.Log.w(r2, r3)     // Catch:{ Exception -> 0x009e }
            return
        L_0x0060:
            android.content.Context r3 = r14.f3986V     // Catch:{ Exception -> 0x009e }
            int r3 = com.google.android.gms.internal.measurement.C2525gd.m6408h(r3)     // Catch:{ Exception -> 0x009e }
            android.content.Context r4 = r14.f3986V     // Catch:{ Exception -> 0x009e }
            int r4 = com.google.android.gms.internal.measurement.C2525gd.m6406g(r4)     // Catch:{ Exception -> 0x009e }
            if (r2 == 0) goto L_0x0079
            int r2 = java.lang.Math.max(r3, r4)     // Catch:{ Exception -> 0x009e }
            if (r4 >= r3) goto L_0x0076
        L_0x0074:
            r3 = 1
            goto L_0x0077
        L_0x0076:
            r3 = 0
        L_0x0077:
            r8 = r3
            goto L_0x0081
        L_0x0079:
            if (r3 <= 0) goto L_0x007d
            r2 = r3
            goto L_0x007e
        L_0x007d:
            r2 = r4
        L_0x007e:
            if (r3 <= 0) goto L_0x0076
            goto L_0x0074
        L_0x0081:
            com.google.android.gms.internal.measurement.zzv r13 = new com.google.android.gms.internal.measurement.zzv     // Catch:{ Exception -> 0x009e }
            r4 = 21028(0x5224, double:1.0389E-319)
            long r6 = (long) r2     // Catch:{ Exception -> 0x009e }
            android.os.Bundle r12 = r14.f3987W     // Catch:{ Exception -> 0x009e }
            r3 = r13
            r3.<init>(r4, r6, r8, r9, r10, r11, r12)     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.gd r2 = r14.f3988X     // Catch:{ Exception -> 0x009e }
            com.google.android.gms.internal.measurement.jb r2 = r2.f4192g     // Catch:{ Exception -> 0x009e }
            android.content.Context r3 = r14.f3986V     // Catch:{ Exception -> 0x009e }
            e.d.a.c.b.b r3 = p119e.p144d.p145a.p157c.p160b.C3992d.m11990a(r3)     // Catch:{ Exception -> 0x009e }
            long r4 = r14.f4193P     // Catch:{ Exception -> 0x009e }
            r2.initialize(r3, r13, r4)     // Catch:{ Exception -> 0x009e }
            return
        L_0x009e:
            r2 = move-exception
            com.google.android.gms.internal.measurement.gd r3 = r14.f3988X
            r3.m6391a(r2, r1, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2428b.mo17293a():void");
    }
}
