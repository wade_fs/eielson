package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;
import java.lang.Thread;

/* renamed from: com.google.android.gms.measurement.internal.i5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3069i5 implements Thread.UncaughtExceptionHandler {

    /* renamed from: a */
    private final String f5209a;

    /* renamed from: b */
    private final /* synthetic */ C3045g5 f5210b;

    public C3069i5(C3045g5 g5Var, String str) {
        this.f5210b = g5Var;
        C2258v.m5629a((Object) str);
        this.f5209a = str;
    }

    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.f5210b.mo19015l().mo19001t().mo19043a(this.f5209a, th);
    }
}
