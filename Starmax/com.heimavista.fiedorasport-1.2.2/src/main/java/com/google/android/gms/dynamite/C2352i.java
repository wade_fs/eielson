package com.google.android.gms.dynamite;

import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.dynamite.i */
public interface C2352i extends IInterface {
    /* renamed from: G */
    int mo17148G();

    /* renamed from: a */
    int mo17149a(C3988b bVar, String str, boolean z);

    /* renamed from: a */
    C3988b mo17150a(C3988b bVar, String str, int i);

    /* renamed from: b */
    int mo17151b(C3988b bVar, String str, boolean z);

    /* renamed from: b */
    C3988b mo17152b(C3988b bVar, String str, int i);
}
