package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2701s0;

/* renamed from: com.google.android.gms.measurement.internal.ha */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3062ha {

    /* renamed from: a */
    private C2701s0 f5189a;

    /* renamed from: b */
    private Long f5190b;

    /* renamed from: c */
    private long f5191c;

    /* renamed from: d */
    private final /* synthetic */ C3002ca f5192d;

    private C3062ha(C3002ca caVar) {
        this.f5192d = caVar;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v7, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r13v2, resolved type: java.lang.String} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.internal.measurement.C2701s0 mo19057a(java.lang.String r19, com.google.android.gms.internal.measurement.C2701s0 r20) {
        /*
            r18 = this;
            r1 = r18
            r8 = r19
            r9 = r20
            java.lang.String r0 = r20.mo17849p()
            java.util.List r10 = r20.mo17847n()
            com.google.android.gms.measurement.internal.ca r2 = r1.f5192d
            r2.mo19171i()
            java.lang.String r2 = "_eid"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r9, r2)
            r4 = r3
            java.lang.Long r4 = (java.lang.Long) r4
            r3 = 1
            r5 = 0
            if (r4 == 0) goto L_0x0022
            r6 = 1
            goto L_0x0023
        L_0x0022:
            r6 = 0
        L_0x0023:
            if (r6 == 0) goto L_0x002f
            java.lang.String r7 = "_ep"
            boolean r7 = r0.equals(r7)
            if (r7 == 0) goto L_0x002f
            r7 = 1
            goto L_0x0030
        L_0x002f:
            r7 = 0
        L_0x0030:
            r11 = 0
            if (r7 == 0) goto L_0x01af
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            r0.mo19171i()
            java.lang.String r0 = "_en"
            java.lang.Object r0 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r9, r0)
            r13 = r0
            java.lang.String r13 = (java.lang.String) r13
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            r6 = 0
            if (r0 == 0) goto L_0x007b
            boolean r0 = com.google.android.gms.internal.measurement.C2489ea.m6262b()
            java.lang.String r2 = "Extra parameter without an event name. eventId"
            if (r0 == 0) goto L_0x006d
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.la r0 = r0.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5431Y0
            boolean r0 = r0.mo19152d(r8, r3)
            if (r0 == 0) goto L_0x006d
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19002u()
            r0.mo19043a(r2, r4)
            goto L_0x007a
        L_0x006d:
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()
            r0.mo19043a(r2, r4)
        L_0x007a:
            return r6
        L_0x007b:
            com.google.android.gms.internal.measurement.s0 r0 = r1.f5189a
            if (r0 == 0) goto L_0x0091
            java.lang.Long r0 = r1.f5190b
            if (r0 == 0) goto L_0x0091
            long r14 = r4.longValue()
            java.lang.Long r0 = r1.f5190b
            long r16 = r0.longValue()
            int r0 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r0 == 0) goto L_0x00c0
        L_0x0091:
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.d r0 = r0.mo19172k()
            android.util.Pair r0 = r0.mo18841a(r8, r4)
            if (r0 == 0) goto L_0x017d
            java.lang.Object r7 = r0.first
            if (r7 != 0) goto L_0x00a3
            goto L_0x017d
        L_0x00a3:
            com.google.android.gms.internal.measurement.s0 r7 = (com.google.android.gms.internal.measurement.C2701s0) r7
            r1.f5189a = r7
            java.lang.Object r0 = r0.second
            java.lang.Long r0 = (java.lang.Long) r0
            long r6 = r0.longValue()
            r1.f5191c = r6
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            r0.mo19171i()
            com.google.android.gms.internal.measurement.s0 r0 = r1.f5189a
            java.lang.Object r0 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r0, r2)
            java.lang.Long r0 = (java.lang.Long) r0
            r1.f5190b = r0
        L_0x00c0:
            long r6 = r1.f5191c
            r14 = 1
            long r6 = r6 - r14
            r1.f5191c = r6
            long r6 = r1.f5191c
            int r0 = (r6 > r11 ? 1 : (r6 == r11 ? 0 : -1))
            if (r0 > 0) goto L_0x0100
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.d r2 = r0.mo19172k()
            r2.mo18881c()
            com.google.android.gms.measurement.internal.f4 r0 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()
            java.lang.String r4 = "Clearing complex main event info. appId"
            r0.mo19043a(r4, r8)
            android.database.sqlite.SQLiteDatabase r0 = r2.mo18875v()     // Catch:{ SQLiteException -> 0x00f1 }
            java.lang.String r4 = "delete from main_event_params where app_id=?"
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x00f1 }
            r3[r5] = r8     // Catch:{ SQLiteException -> 0x00f1 }
            r0.execSQL(r4, r3)     // Catch:{ SQLiteException -> 0x00f1 }
            goto L_0x010f
        L_0x00f1:
            r0 = move-exception
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()
            java.lang.String r3 = "Error clearing complex main event"
            r2.mo19043a(r3, r0)
            goto L_0x010f
        L_0x0100:
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.d r2 = r0.mo19172k()
            long r5 = r1.f5191c
            com.google.android.gms.internal.measurement.s0 r7 = r1.f5189a
            r3 = r19
            r2.mo18857a(r3, r4, r5, r7)
        L_0x010f:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.google.android.gms.internal.measurement.s0 r2 = r1.f5189a
            java.util.List r2 = r2.mo17847n()
            java.util.Iterator r2 = r2.iterator()
        L_0x011e:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x013d
            java.lang.Object r3 = r2.next()
            com.google.android.gms.internal.measurement.u0 r3 = (com.google.android.gms.internal.measurement.C2733u0) r3
            com.google.android.gms.measurement.internal.ca r4 = r1.f5192d
            r4.mo19171i()
            java.lang.String r4 = r3.mo17922o()
            com.google.android.gms.internal.measurement.u0 r4 = com.google.android.gms.measurement.internal.C3223v9.m9261a(r9, r4)
            if (r4 != 0) goto L_0x011e
            r0.add(r3)
            goto L_0x011e
        L_0x013d:
            boolean r2 = r0.isEmpty()
            if (r2 != 0) goto L_0x0149
            r0.addAll(r10)
            r10 = r0
            goto L_0x0218
        L_0x0149:
            boolean r0 = com.google.android.gms.internal.measurement.C2489ea.m6262b()
            java.lang.String r2 = "No unique parameters in main event. eventName"
            if (r0 == 0) goto L_0x016e
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.la r0 = r0.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5431Y0
            boolean r0 = r0.mo19152d(r8, r3)
            if (r0 == 0) goto L_0x016e
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19002u()
            r0.mo19043a(r2, r13)
            goto L_0x0218
        L_0x016e:
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()
            r0.mo19043a(r2, r13)
            goto L_0x0218
        L_0x017d:
            boolean r0 = com.google.android.gms.internal.measurement.C2489ea.m6262b()
            java.lang.String r2 = "Extra parameter without existing main event. eventName, eventId"
            if (r0 == 0) goto L_0x01a1
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.la r0 = r0.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5431Y0
            boolean r0 = r0.mo19152d(r8, r3)
            if (r0 == 0) goto L_0x01a1
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19002u()
            r0.mo19044a(r2, r13, r4)
            goto L_0x01ae
        L_0x01a1:
            com.google.android.gms.measurement.internal.ca r0 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()
            r0.mo19044a(r2, r13, r4)
        L_0x01ae:
            return r6
        L_0x01af:
            if (r6 == 0) goto L_0x0217
            r1.f5190b = r4
            r1.f5189a = r9
            com.google.android.gms.measurement.internal.ca r2 = r1.f5192d
            r2.mo19171i()
            java.lang.Long r2 = java.lang.Long.valueOf(r11)
            java.lang.String r3 = "_epc"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3223v9.m9274b(r9, r3)
            if (r3 != 0) goto L_0x01c7
            goto L_0x01c8
        L_0x01c7:
            r2 = r3
        L_0x01c8:
            java.lang.Long r2 = (java.lang.Long) r2
            long r2 = r2.longValue()
            r1.f5191c = r2
            long r2 = r1.f5191c
            int r5 = (r2 > r11 ? 1 : (r2 == r11 ? 0 : -1))
            if (r5 > 0) goto L_0x0208
            boolean r2 = com.google.android.gms.internal.measurement.C2489ea.m6262b()
            java.lang.String r3 = "Complex event with zero extra param count. eventName"
            if (r2 == 0) goto L_0x01fa
            com.google.android.gms.measurement.internal.ca r2 = r1.f5192d
            com.google.android.gms.measurement.internal.la r2 = r2.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5431Y0
            boolean r2 = r2.mo19152d(r8, r4)
            if (r2 == 0) goto L_0x01fa
            com.google.android.gms.measurement.internal.ca r2 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19002u()
            r2.mo19043a(r3, r0)
            goto L_0x0217
        L_0x01fa:
            com.google.android.gms.measurement.internal.ca r2 = r1.f5192d
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19004w()
            r2.mo19043a(r3, r0)
            goto L_0x0217
        L_0x0208:
            com.google.android.gms.measurement.internal.ca r2 = r1.f5192d
            com.google.android.gms.measurement.internal.d r2 = r2.mo19172k()
            long r5 = r1.f5191c
            r3 = r19
            r7 = r20
            r2.mo18857a(r3, r4, r5, r7)
        L_0x0217:
            r13 = r0
        L_0x0218:
            com.google.android.gms.internal.measurement.l4$a r0 = r20.mo17669j()
            com.google.android.gms.internal.measurement.s0$a r0 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r0
            r0.mo17862a(r13)
            r0.mo17868l()
            r0.mo17861a(r10)
            com.google.android.gms.internal.measurement.u5 r0 = r0.mo17679i()
            com.google.android.gms.internal.measurement.s0 r0 = (com.google.android.gms.internal.measurement.C2701s0) r0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3062ha.mo19057a(java.lang.String, com.google.android.gms.internal.measurement.s0):com.google.android.gms.internal.measurement.s0");
    }

    /* synthetic */ C3062ha(C3002ca caVar, C3038fa faVar) {
        this(caVar);
    }
}
