package com.google.android.gms.measurement.internal;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.Size;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.internal.C2258v;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.r7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3177r7 extends C2995c3 {

    /* renamed from: c */
    protected C3188s7 f5590c;

    /* renamed from: d */
    private volatile C3188s7 f5591d;

    /* renamed from: e */
    private C3188s7 f5592e;

    /* renamed from: f */
    private final Map<Activity, C3188s7> f5593f = new ArrayMap();

    /* renamed from: g */
    private String f5594g;

    public C3177r7(C3081j5 j5Var) {
        super(j5Var);
    }

    @MainThread
    /* renamed from: d */
    private final C3188s7 m9194d(@NonNull Activity activity) {
        C2258v.m5629a(activity);
        C3188s7 s7Var = this.f5593f.get(activity);
        if (s7Var != null) {
            return s7Var;
        }
        C3188s7 s7Var2 = new C3188s7(null, m9189a(activity.getClass().getCanonicalName()), mo19011f().mo19453t());
        this.f5593f.put(activity, s7Var2);
        return s7Var2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public final boolean mo18725A() {
        return false;
    }

    @WorkerThread
    /* renamed from: B */
    public final C3188s7 mo19295B() {
        mo18816x();
        mo18881c();
        return this.f5590c;
    }

    /* renamed from: C */
    public final C3188s7 mo19296C() {
        mo18880a();
        return this.f5591d;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
     arg types: [android.app.Activity, com.google.android.gms.measurement.internal.s7, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void */
    /* renamed from: a */
    public final void mo19299a(@NonNull Activity activity, @Size(max = 36, min = 1) @Nullable String str, @Size(max = 36, min = 1) @Nullable String str2) {
        if (this.f5591d == null) {
            mo19015l().mo19006y().mo19042a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f5593f.get(activity) == null) {
            mo19015l().mo19006y().mo19042a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = m9189a(activity.getClass().getCanonicalName());
            }
            boolean equals = this.f5591d.f5639b.equals(str2);
            boolean c = C3267z9.m9428c(this.f5591d.f5638a, str);
            if (equals && c) {
                mo19015l().mo19006y().mo19042a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                mo19015l().mo19006y().mo19043a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                mo19015l().mo18996B().mo19044a("Setting current screen to name, class", str == null ? "null" : str, str2);
                C3188s7 s7Var = new C3188s7(str, str2, mo19011f().mo19453t());
                this.f5593f.put(activity, s7Var);
                m9190a(activity, s7Var, true);
            } else {
                mo19015l().mo19006y().mo19043a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    @MainThread
    /* renamed from: b */
    public final void mo19301b(Activity activity) {
        C3188s7 d = m9194d(activity);
        this.f5592e = this.f5591d;
        this.f5591d = null;
        mo19014j().mo19028a(new C3199t7(this, d, mo19017o().elapsedRealtime()));
    }

    @MainThread
    /* renamed from: c */
    public final void mo19303c(Activity activity) {
        this.f5593f.remove(activity);
    }

    @MainThread
    /* renamed from: b */
    public final void mo19302b(Activity activity, Bundle bundle) {
        C3188s7 s7Var;
        if (bundle != null && (s7Var = this.f5593f.get(activity)) != null) {
            Bundle bundle2 = new Bundle();
            bundle2.putLong("id", s7Var.f5640c);
            bundle2.putString("name", s7Var.f5638a);
            bundle2.putString("referrer_name", s7Var.f5639b);
            bundle.putBundle("com.google.app_measurement.screen_service", bundle2);
        }
    }

    @MainThread
    /* renamed from: a */
    private final void m9190a(Activity activity, C3188s7 s7Var, boolean z) {
        C3188s7 s7Var2 = this.f5591d == null ? this.f5592e : this.f5591d;
        C3188s7 s7Var3 = s7Var.f5639b == null ? new C3188s7(s7Var.f5638a, m9189a(activity.getClass().getCanonicalName()), s7Var.f5640c) : s7Var;
        this.f5592e = this.f5591d;
        this.f5591d = s7Var3;
        mo19014j().mo19028a(new C3210u7(this, z, mo19017o().elapsedRealtime(), s7Var2, s7Var3));
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: a */
    public final void m9193a(@NonNull C3188s7 s7Var, boolean z, long j) {
        mo18883k().mo19414a(mo19017o().elapsedRealtime());
        if (mo18889u().mo19397a(s7Var.f5641d, z, j)) {
            s7Var.f5641d = false;
        }
    }

    /* renamed from: a */
    public static void m9192a(C3188s7 s7Var, Bundle bundle, boolean z) {
        if (bundle != null && s7Var != null && (!bundle.containsKey("_sc") || z)) {
            String str = s7Var.f5638a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", s7Var.f5639b);
            bundle.putLong("_si", s7Var.f5640c);
        } else if (bundle != null && s7Var == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19300a(String str, C3188s7 s7Var) {
        mo18881c();
        synchronized (this) {
            if (this.f5594g == null || this.f5594g.equals(str) || s7Var != null) {
                this.f5594g = str;
            }
        }
    }

    /* renamed from: a */
    private static String m9189a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    @MainThread
    /* renamed from: a */
    public final void mo19298a(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.f5593f.put(activity, new C3188s7(bundle2.getString("name"), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
     arg types: [android.app.Activity, com.google.android.gms.measurement.internal.s7, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void */
    @MainThread
    /* renamed from: a */
    public final void mo19297a(Activity activity) {
        m9190a(activity, m9194d(activity), false);
        C3257z k = mo18883k();
        k.mo19014j().mo19028a(new C2969a1(k, k.mo19017o().elapsedRealtime()));
    }
}
