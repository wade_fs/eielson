package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.p6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2663p6 extends C2618m6<FieldDescriptorType, Object> {
    C2663p6(int i) {
        super(i, null);
    }

    /* renamed from: a */
    public final void mo17742a() {
        if (!mo17743b()) {
            if (mo17744c() <= 0) {
                Iterator it = mo17747d().iterator();
                if (it.hasNext()) {
                    ((C2466d4) ((Map.Entry) it.next()).getKey()).mo17415g();
                    throw null;
                }
            } else {
                ((C2466d4) mo17741a(0).getKey()).mo17415g();
                throw null;
            }
        }
        super.mo17742a();
    }
}
