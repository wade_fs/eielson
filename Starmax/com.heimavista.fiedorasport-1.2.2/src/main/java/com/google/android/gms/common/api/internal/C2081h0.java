package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zaj;

/* renamed from: com.google.android.gms.common.api.internal.h0 */
final class C2081h0 extends C2140v0 {

    /* renamed from: b */
    private final /* synthetic */ C2153z f3333b;

    /* renamed from: c */
    private final /* synthetic */ zaj f3334c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2081h0(C2077g0 g0Var, C2132t0 t0Var, C2153z zVar, zaj zaj) {
        super(t0Var);
        this.f3333b = zVar;
        this.f3334c = zaj;
    }

    /* renamed from: a */
    public final void mo16645a() {
        this.f3333b.m5191a(this.f3334c);
    }
}
