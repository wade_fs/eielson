package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.location.j */
public final class C2843j implements Parcelable.Creator<LocationRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        long j = 3600000;
        long j2 = 600000;
        long j3 = Long.MAX_VALUE;
        long j4 = 0;
        int i = 102;
        boolean z = false;
        int i2 = Integer.MAX_VALUE;
        float f = 0.0f;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    i = C2248a.m5585z(parcel2, a);
                    break;
                case 2:
                    j = C2248a.m5546B(parcel2, a);
                    break;
                case 3:
                    j2 = C2248a.m5546B(parcel2, a);
                    break;
                case 4:
                    z = C2248a.m5577r(parcel2, a);
                    break;
                case 5:
                    j3 = C2248a.m5546B(parcel2, a);
                    break;
                case 6:
                    i2 = C2248a.m5585z(parcel2, a);
                    break;
                case 7:
                    f = C2248a.m5582w(parcel2, a);
                    break;
                case 8:
                    j4 = C2248a.m5546B(parcel2, a);
                    break;
                default:
                    C2248a.m5550F(parcel2, a);
                    break;
            }
        }
        C2248a.m5576q(parcel2, b);
        return new LocationRequest(i, j, j2, z, j3, i2, f, j4);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationRequest[i];
    }
}
