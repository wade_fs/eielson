package com.google.android.gms.internal.measurement;

import android.os.IBinder;

/* renamed from: com.google.android.gms.internal.measurement.fd */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2508fd extends C2412a implements C2475dd {
    C2508fd(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.api.internal.IStringProvider");
    }
}
