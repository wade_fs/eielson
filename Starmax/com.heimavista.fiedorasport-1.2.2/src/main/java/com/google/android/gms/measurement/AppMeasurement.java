package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresPermission;
import androidx.annotation.Size;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.measurement.internal.C3046g6;
import com.google.android.gms.measurement.internal.C3081j5;
import com.google.android.gms.measurement.internal.C3083j7;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Deprecated
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public class AppMeasurement {

    /* renamed from: d */
    private static volatile AppMeasurement f4922d;

    /* renamed from: a */
    private final C3081j5 f4923a;

    /* renamed from: b */
    private final C3083j7 f4924b;

    /* renamed from: c */
    private final boolean f4925c;

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
    public static class ConditionalUserProperty {
        @Keep
        public boolean mActive;
        @Keep
        public String mAppId;
        @Keep
        public long mCreationTimestamp;
        @Keep
        public String mExpiredEventName;
        @Keep
        public Bundle mExpiredEventParams;
        @Keep
        public String mName;
        @Keep
        public String mOrigin;
        @Keep
        public long mTimeToLive;
        @Keep
        public String mTimedOutEventName;
        @Keep
        public Bundle mTimedOutEventParams;
        @Keep
        public String mTriggerEventName;
        @Keep
        public long mTriggerTimeout;
        @Keep
        public String mTriggeredEventName;
        @Keep
        public Bundle mTriggeredEventParams;
        @Keep
        public long mTriggeredTimestamp;
        @Keep
        public Object mValue;

        public ConditionalUserProperty() {
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public final Bundle m8363a() {
            Bundle bundle = new Bundle();
            String str = this.mAppId;
            if (str != null) {
                bundle.putString("app_id", str);
            }
            String str2 = this.mOrigin;
            if (str2 != null) {
                bundle.putString(TtmlNode.ATTR_TTS_ORIGIN, str2);
            }
            String str3 = this.mName;
            if (str3 != null) {
                bundle.putString("name", str3);
            }
            Object obj = this.mValue;
            if (obj != null) {
                C3046g6.m8688a(bundle, obj);
            }
            String str4 = this.mTriggerEventName;
            if (str4 != null) {
                bundle.putString("trigger_event_name", str4);
            }
            bundle.putLong("trigger_timeout", this.mTriggerTimeout);
            String str5 = this.mTimedOutEventName;
            if (str5 != null) {
                bundle.putString("timed_out_event_name", str5);
            }
            Bundle bundle2 = this.mTimedOutEventParams;
            if (bundle2 != null) {
                bundle.putBundle("timed_out_event_params", bundle2);
            }
            String str6 = this.mTriggeredEventName;
            if (str6 != null) {
                bundle.putString("triggered_event_name", str6);
            }
            Bundle bundle3 = this.mTriggeredEventParams;
            if (bundle3 != null) {
                bundle.putBundle("triggered_event_params", bundle3);
            }
            bundle.putLong("time_to_live", this.mTimeToLive);
            String str7 = this.mExpiredEventName;
            if (str7 != null) {
                bundle.putString("expired_event_name", str7);
            }
            Bundle bundle4 = this.mExpiredEventParams;
            if (bundle4 != null) {
                bundle.putBundle("expired_event_params", bundle4);
            }
            bundle.putLong("creation_timestamp", this.mCreationTimestamp);
            bundle.putBoolean("active", this.mActive);
            bundle.putLong("triggered_timestamp", this.mTriggeredTimestamp);
            return bundle;
        }

        private ConditionalUserProperty(@NonNull Bundle bundle) {
            C2258v.m5629a(bundle);
            this.mAppId = (String) C3046g6.m8687a(bundle, "app_id", String.class, null);
            this.mOrigin = (String) C3046g6.m8687a(bundle, TtmlNode.ATTR_TTS_ORIGIN, String.class, null);
            this.mName = (String) C3046g6.m8687a(bundle, "name", String.class, null);
            this.mValue = C3046g6.m8687a(bundle, "value", Object.class, null);
            this.mTriggerEventName = (String) C3046g6.m8687a(bundle, "trigger_event_name", String.class, null);
            this.mTriggerTimeout = ((Long) C3046g6.m8687a(bundle, "trigger_timeout", Long.class, 0L)).longValue();
            this.mTimedOutEventName = (String) C3046g6.m8687a(bundle, "timed_out_event_name", String.class, null);
            this.mTimedOutEventParams = (Bundle) C3046g6.m8687a(bundle, "timed_out_event_params", Bundle.class, null);
            this.mTriggeredEventName = (String) C3046g6.m8687a(bundle, "triggered_event_name", String.class, null);
            this.mTriggeredEventParams = (Bundle) C3046g6.m8687a(bundle, "triggered_event_params", Bundle.class, null);
            this.mTimeToLive = ((Long) C3046g6.m8687a(bundle, "time_to_live", Long.class, 0L)).longValue();
            this.mExpiredEventName = (String) C3046g6.m8687a(bundle, "expired_event_name", String.class, null);
            this.mExpiredEventParams = (Bundle) C3046g6.m8687a(bundle, "expired_event_params", Bundle.class, null);
        }
    }

    private AppMeasurement(C3081j5 j5Var) {
        C2258v.m5629a(j5Var);
        this.f4923a = j5Var;
        this.f4924b = null;
        this.f4925c = false;
    }

    /* renamed from: a */
    private static AppMeasurement m8359a(Context context, String str, String str2) {
        if (f4922d == null) {
            synchronized (AppMeasurement.class) {
                if (f4922d == null) {
                    C3083j7 b = m8360b(context, null);
                    if (b != null) {
                        f4922d = new AppMeasurement(b);
                    } else {
                        f4922d = new AppMeasurement(C3081j5.m8752a(context, null, null, null));
                    }
                }
            }
        }
        return f4922d;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.google.android.gms.measurement.internal.C3083j7 m8360b(android.content.Context r8, android.os.Bundle r9) {
        /*
            java.lang.String r0 = "com.google.firebase.analytics.FirebaseAnalytics"
            r1 = 0
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0027 }
            java.lang.String r2 = "getScionFrontendApiImplementation"
            r3 = 2
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{  }
            java.lang.Class<android.content.Context> r5 = android.content.Context.class
            r6 = 0
            r4[r6] = r5     // Catch:{  }
            java.lang.Class<android.os.Bundle> r5 = android.os.Bundle.class
            r7 = 1
            r4[r7] = r5     // Catch:{  }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r2, r4)     // Catch:{  }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{  }
            r2[r6] = r8     // Catch:{  }
            r2[r7] = r9     // Catch:{  }
            java.lang.Object r8 = r0.invoke(r1, r2)     // Catch:{  }
            com.google.android.gms.measurement.internal.j7 r8 = (com.google.android.gms.measurement.internal.C3083j7) r8     // Catch:{  }
            return r8
        L_0x0027:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.AppMeasurement.m8360b(android.content.Context, android.os.Bundle):com.google.android.gms.measurement.internal.j7");
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.WAKE_LOCK"})
    @Deprecated
    @Keep
    public static AppMeasurement getInstance(Context context) {
        return m8359a(context, (String) null, (String) null);
    }

    @Keep
    public void beginAdUnitExposure(@Size(min = 1) @NonNull String str) {
        if (this.f4925c) {
            this.f4924b.mo19111a(str);
        } else {
            this.f4923a.mo19082I().mo19415a(str, this.f4923a.mo19017o().elapsedRealtime());
        }
    }

    @Keep
    public void clearConditionalUserProperty(@Size(max = 24, min = 1) @NonNull String str, @Nullable String str2, @Nullable Bundle bundle) {
        if (this.f4925c) {
            this.f4924b.mo19112a(str, str2, bundle);
        } else {
            this.f4923a.mo19103v().mo19281c(str, str2, bundle);
        }
    }

    /* access modifiers changed from: protected */
    @Keep
    public void clearConditionalUserPropertyAs(@Size(min = 1) @NonNull String str, @Size(max = 24, min = 1) @NonNull String str2, @Nullable String str3, @Nullable Bundle bundle) {
        if (this.f4925c) {
            throw new IllegalStateException("Unexpected call on client side");
        }
        this.f4923a.mo19103v().mo19273a(str, str2, str3, bundle);
        throw null;
    }

    @Keep
    public void endAdUnitExposure(@Size(min = 1) @NonNull String str) {
        if (this.f4925c) {
            this.f4924b.mo19116c(str);
        } else {
            this.f4923a.mo19082I().mo19416b(str, this.f4923a.mo19017o().elapsedRealtime());
        }
    }

    @Keep
    public long generateEventId() {
        if (this.f4925c) {
            return this.f4924b.mo19122t();
        }
        return this.f4923a.mo19104w().mo19453t();
    }

    @Keep
    @Nullable
    public String getAppInstanceId() {
        if (this.f4925c) {
            return this.f4924b.mo19120f();
        }
        return this.f4923a.mo19103v().mo19250H();
    }

    @WorkerThread
    @Keep
    public List<ConditionalUserProperty> getConditionalUserProperties(@Nullable String str, @Size(max = 23, min = 1) @Nullable String str2) {
        List<Bundle> list;
        int i;
        if (this.f4925c) {
            list = this.f4924b.mo19109a(str, str2);
        } else {
            list = this.f4923a.mo19103v().mo19255a(str, str2);
        }
        if (list == null) {
            i = 0;
        } else {
            i = list.size();
        }
        ArrayList arrayList = new ArrayList(i);
        for (Bundle bundle : list) {
            arrayList.add(new ConditionalUserProperty(bundle));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    @Keep
    public List<ConditionalUserProperty> getConditionalUserPropertiesAs(@Size(min = 1) @NonNull String str, @Nullable String str2, @Size(max = 23, min = 1) @Nullable String str3) {
        if (this.f4925c) {
            throw new IllegalStateException("Unexpected call on client side");
        }
        this.f4923a.mo19103v().mo19256a(str, str2, str3);
        throw null;
    }

    @Keep
    @Nullable
    public String getCurrentScreenClass() {
        if (this.f4925c) {
            return this.f4924b.mo19119e();
        }
        return this.f4923a.mo19103v().mo19253K();
    }

    @Keep
    @Nullable
    public String getCurrentScreenName() {
        if (this.f4925c) {
            return this.f4924b.mo19108a();
        }
        return this.f4923a.mo19103v().mo19252J();
    }

    @Keep
    @Nullable
    public String getGmpAppId() {
        if (this.f4925c) {
            return this.f4924b.mo19121g();
        }
        return this.f4923a.mo19103v().mo19254L();
    }

    @WorkerThread
    @Keep
    public int getMaxUserProperties(@Size(min = 1) @NonNull String str) {
        if (this.f4925c) {
            return this.f4924b.mo19114b(str);
        }
        this.f4923a.mo19103v();
        C2258v.m5639b(str);
        return 25;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    @Keep
    public Map<String, Object> getUserProperties(@Nullable String str, @Size(max = 24, min = 1) @Nullable String str2, boolean z) {
        if (this.f4925c) {
            return this.f4924b.mo19110a(str, str2, z);
        }
        return this.f4923a.mo19103v().mo19258a(str, str2, z);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    @Keep
    public Map<String, Object> getUserPropertiesAs(@Size(min = 1) @NonNull String str, @Nullable String str2, @Size(max = 23, min = 1) @Nullable String str3, boolean z) {
        if (this.f4925c) {
            throw new IllegalStateException("Unexpected call on client side");
        }
        this.f4923a.mo19103v().mo19257a(str, str2, str3, z);
        throw null;
    }

    @Keep
    public void logEventInternal(String str, String str2, Bundle bundle) {
        if (this.f4925c) {
            this.f4924b.mo19115b(str, str2, bundle);
        } else {
            this.f4923a.mo19103v().mo19267a(str, str2, bundle);
        }
    }

    @Keep
    public void setConditionalUserProperty(@NonNull ConditionalUserProperty conditionalUserProperty) {
        C2258v.m5629a(conditionalUserProperty);
        if (this.f4925c) {
            this.f4924b.mo19117d(conditionalUserProperty.m8363a());
        } else {
            this.f4923a.mo19103v().mo19260a(conditionalUserProperty.m8363a());
        }
    }

    /* access modifiers changed from: protected */
    @Keep
    public void setConditionalUserPropertyAs(@NonNull ConditionalUserProperty conditionalUserProperty) {
        C2258v.m5629a(conditionalUserProperty);
        if (this.f4925c) {
            throw new IllegalStateException("Unexpected call on client side");
        }
        this.f4923a.mo19103v().mo19276b(conditionalUserProperty.m8363a());
        throw null;
    }

    private AppMeasurement(C3083j7 j7Var) {
        C2258v.m5629a(j7Var);
        this.f4924b = j7Var;
        this.f4923a = null;
        this.f4925c = true;
    }

    /* renamed from: a */
    public static AppMeasurement m8358a(Context context, Bundle bundle) {
        if (f4922d == null) {
            synchronized (AppMeasurement.class) {
                if (f4922d == null) {
                    C3083j7 b = m8360b(context, bundle);
                    if (b != null) {
                        f4922d = new AppMeasurement(b);
                    } else {
                        f4922d = new AppMeasurement(C3081j5.m8752a(context, null, null, bundle));
                    }
                }
            }
        }
        return f4922d;
    }

    /* renamed from: a */
    public final void mo18676a(boolean z) {
        if (this.f4925c) {
            this.f4924b.mo19118d(z);
        } else {
            this.f4923a.mo19103v().mo19279b(z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.Object, int]
     candidates:
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    /* renamed from: a */
    public void mo18675a(String str, String str2, Object obj) {
        C2258v.m5639b(str);
        if (this.f4925c) {
            this.f4924b.mo19113a(str, str2, obj);
        } else {
            this.f4923a.mo19103v().mo19271a(str, str2, obj, true);
        }
    }
}
