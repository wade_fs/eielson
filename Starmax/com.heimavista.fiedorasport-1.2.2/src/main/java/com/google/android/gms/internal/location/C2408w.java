package com.google.android.gms.internal.location;

import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.internal.location.w */
public final class C2408w {
    /* renamed from: a */
    public static Looper m5936a() {
        C2258v.m5641b(Looper.myLooper() != null, "Can't create handler inside thread that has not called Looper.prepare()");
        return Looper.myLooper();
    }

    /* renamed from: a */
    public static Looper m5937a(@Nullable Looper looper) {
        return looper != null ? looper : m5936a();
    }
}
