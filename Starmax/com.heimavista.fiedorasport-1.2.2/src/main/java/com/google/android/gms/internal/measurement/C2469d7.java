package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.d7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class C2469d7<T, B> {
    C2469d7() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract B mo17418a();

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract T mo17419a(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17420a(B b, int i, long j);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17421a(T t, C2771w7 w7Var);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17422a(Object obj, T t);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract void mo17423b(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract void mo17424b(Object obj, C2771w7 w7Var);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract void mo17425b(Object obj, Object obj2);

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public abstract int mo17426c(T t);

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public abstract T mo17427c(T t, T t2);

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public abstract int mo17428d(T t);
}
