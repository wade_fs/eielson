package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.aa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2424aa implements C2440ba {

    /* renamed from: a */
    private static final C2765w1<Boolean> f3983a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.service.use_appinfo_modified", false);

    /* renamed from: a */
    public final boolean mo17288a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17289e() {
        return f3983a.mo18128b().booleanValue();
    }
}
