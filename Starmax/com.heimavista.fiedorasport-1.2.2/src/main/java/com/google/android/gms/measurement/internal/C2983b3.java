package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2473db;

/* renamed from: com.google.android.gms.measurement.internal.b3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C2983b3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f4965a = new C2983b3();

    private C2983b3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2473db.m6226d());
    }
}
