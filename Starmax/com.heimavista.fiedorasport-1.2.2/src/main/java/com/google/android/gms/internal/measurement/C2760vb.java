package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.vb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2760vb implements C2593l2<C2745ub> {

    /* renamed from: Q */
    private static C2760vb f4541Q = new C2760vb();

    /* renamed from: P */
    private final C2593l2<C2745ub> f4542P;

    private C2760vb(C2593l2<C2745ub> l2Var) {
        this.f4542P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7454b() {
        return ((C2745ub) f4541Q.mo17285a()).mo17946a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4542P.mo17285a();
    }

    public C2760vb() {
        this(C2579k2.m6601a(new C2791xb()));
    }
}
