package com.google.android.gms.maps;

import android.app.Activity;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2890a0;
import com.google.android.gms.maps.p093i.C2892b0;
import com.google.android.gms.maps.p093i.C2899f;
import com.google.android.gms.maps.p093i.C2905j;
import java.util.ArrayList;
import java.util.List;
import p119e.p144d.p145a.p157c.p160b.C3986a;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p160b.C3993e;

public class SupportStreetViewPanoramaFragment extends Fragment {

    /* renamed from: P */
    private final C2877b f4779P = new C2877b(super);

    public void onActivityCreated(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(SupportStreetViewPanoramaFragment.class.getClassLoader());
        }
        super.onActivityCreated(bundle);
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.f4779P.m8099a(activity);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f4779P.mo23621a(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return this.f4779P.mo23618a(layoutInflater, viewGroup, bundle);
    }

    public void onDestroy() {
        this.f4779P.mo23623b();
        super.onDestroy();
    }

    public void onDestroyView() {
        this.f4779P.mo23625c();
        super.onDestroyView();
    }

    public void onInflate(Activity activity, AttributeSet attributeSet, Bundle bundle) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitAll().build());
        try {
            super.onInflate(activity, attributeSet, bundle);
            this.f4779P.m8099a(activity);
            this.f4779P.mo23620a(activity, new Bundle(), bundle);
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    public void onLowMemory() {
        this.f4779P.mo23626d();
        super.onLowMemory();
    }

    public void onPause() {
        this.f4779P.mo23627e();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f4779P.mo23628f();
    }

    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(SupportStreetViewPanoramaFragment.class.getClassLoader());
        }
        super.onSaveInstanceState(bundle);
        this.f4779P.mo23624b(bundle);
    }

    public void onStart() {
        super.onStart();
        this.f4779P.mo23629g();
    }

    public void onStop() {
        this.f4779P.mo23630h();
        super.onStop();
    }

    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
    }

    /* renamed from: com.google.android.gms.maps.SupportStreetViewPanoramaFragment$b */
    static class C2877b extends C3986a<C2876a> {

        /* renamed from: e */
        private final Fragment f4782e;

        /* renamed from: f */
        private C3993e<C2876a> f4783f;

        /* renamed from: g */
        private Activity f4784g;

        /* renamed from: h */
        private final List<C2886f> f4785h = new ArrayList();

        C2877b(Fragment fragment) {
            this.f4782e = fragment;
        }

        /* renamed from: i */
        private final void m8101i() {
            if (this.f4784g != null && this.f4783f != null && mo23619a() == null) {
                try {
                    C2884d.m8123a(this.f4784g);
                    this.f4783f.mo23633a(new C2876a(this.f4782e, C2892b0.m8145a(this.f4784g).mo18445c(C3992d.m11990a(this.f4784g))));
                    for (C2886f fVar : this.f4785h) {
                        ((C2876a) mo23619a()).mo18397a(fVar);
                    }
                    this.f4785h.clear();
                } catch (RemoteException e) {
                    throw new C2934e(e);
                } catch (C2170d unused) {
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo18361a(C3993e<C2876a> eVar) {
            this.f4783f = eVar;
            m8101i();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public final void m8099a(Activity activity) {
            this.f4784g = activity;
            m8101i();
        }
    }

    /* renamed from: com.google.android.gms.maps.SupportStreetViewPanoramaFragment$a */
    static class C2876a implements C2905j {

        /* renamed from: a */
        private final Fragment f4780a;

        /* renamed from: b */
        private final C2899f f4781b;

        public C2876a(Fragment fragment, C2899f fVar) {
            C2258v.m5629a(fVar);
            this.f4781b = fVar;
            C2258v.m5629a(fragment);
            this.f4780a = fragment;
        }

        /* renamed from: a */
        public final void mo18350a(Activity activity, Bundle bundle, Bundle bundle2) {
            try {
                Bundle bundle3 = new Bundle();
                C2890a0.m8135a(bundle2, bundle3);
                this.f4781b.mo18462a(C3992d.m11990a(activity), (StreetViewPanoramaOptions) null, bundle3);
                C2890a0.m8135a(bundle3, bundle2);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18354b(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                Bundle arguments = this.f4780a.getArguments();
                if (arguments != null && arguments.containsKey("StreetViewPanoramaOptions")) {
                    C2890a0.m8136a(bundle2, "StreetViewPanoramaOptions", arguments.getParcelable("StreetViewPanoramaOptions"));
                }
                this.f4781b.mo18464b(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: c */
        public final void mo18355c() {
            try {
                this.f4781b.mo18465c();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: d */
        public final void mo18356d() {
            try {
                this.f4781b.mo18466d();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: g */
        public final void mo18357g() {
            try {
                this.f4781b.mo18467g();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onLowMemory() {
            try {
                this.f4781b.onLowMemory();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onResume() {
            try {
                this.f4781b.onResume();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onStart() {
            try {
                this.f4781b.onStart();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final View mo18349a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                C3988b a = this.f4781b.mo18459a(C3992d.m11990a(layoutInflater), C3992d.m11990a(viewGroup), bundle2);
                C2890a0.m8135a(bundle2, bundle);
                return (View) C3992d.m11991e(a);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18353b() {
            try {
                this.f4781b.mo18463b();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18351a(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4781b.mo18460a(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18397a(C2886f fVar) {
            try {
                this.f4781b.mo18461a(new C2959q(this, fVar));
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }
    }
}
