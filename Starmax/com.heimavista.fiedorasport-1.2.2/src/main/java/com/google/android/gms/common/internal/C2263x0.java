package com.google.android.gms.common.internal;

import android.os.IInterface;
import com.google.android.gms.common.zzk;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.common.internal.x0 */
public interface C2263x0 extends IInterface {
    /* renamed from: a */
    boolean mo17045a(zzk zzk, C3988b bVar);
}
