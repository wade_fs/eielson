package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.u4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3207u4 {

    /* renamed from: a */
    private final String f5672a;

    /* renamed from: b */
    private final boolean f5673b;

    /* renamed from: c */
    private boolean f5674c;

    /* renamed from: d */
    private boolean f5675d;

    /* renamed from: e */
    private final /* synthetic */ C3185s4 f5676e;

    public C3207u4(C3185s4 s4Var, String str, boolean z) {
        this.f5676e = s4Var;
        C2258v.m5639b(str);
        this.f5672a = str;
        this.f5673b = z;
    }

    @WorkerThread
    /* renamed from: a */
    public final boolean mo19333a() {
        if (!this.f5674c) {
            this.f5674c = true;
            this.f5675d = this.f5676e.mo19315t().getBoolean(this.f5672a, this.f5673b);
        }
        return this.f5675d;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19332a(boolean z) {
        SharedPreferences.Editor edit = this.f5676e.mo19315t().edit();
        edit.putBoolean(this.f5672a, z);
        edit.apply();
        this.f5675d = z;
    }
}
