package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzam extends AbstractSafeParcelable implements Iterable<String> {
    public static final Parcelable.Creator<zzam> CREATOR = new C3123n();
    /* access modifiers changed from: private */

    /* renamed from: P */
    public final Bundle f5802P;

    zzam(Bundle bundle) {
        this.f5802P = bundle;
    }

    /* renamed from: a */
    public final int mo19458a() {
        return this.f5802P.size();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final Object mo19459b(String str) {
        return this.f5802P.get(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final Long mo19460c(String str) {
        return Long.valueOf(this.f5802P.getLong(str));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final Double mo19461d(String str) {
        return Double.valueOf(this.f5802P.getDouble(str));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public final String mo19463e(String str) {
        return this.f5802P.getString(str);
    }

    public final Iterator<String> iterator() {
        return new C3111m(this);
    }

    public final String toString() {
        return this.f5802P.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5593a(parcel, 2, mo19462e(), false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: e */
    public final Bundle mo19462e() {
        return new Bundle(this.f5802P);
    }
}
