package com.google.android.gms.common.p090j;

import android.content.Context;
import com.google.android.gms.common.util.C2323n;

/* renamed from: com.google.android.gms.common.j.a */
public class C2281a {

    /* renamed from: a */
    private static Context f3772a;

    /* renamed from: b */
    private static Boolean f3773b;

    /* renamed from: a */
    public static synchronized boolean m5675a(Context context) {
        synchronized (C2281a.class) {
            Context applicationContext = context.getApplicationContext();
            if (f3772a == null || f3773b == null || f3772a != applicationContext) {
                f3773b = null;
                if (C2323n.m5800i()) {
                    f3773b = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        f3773b = true;
                    } catch (ClassNotFoundException unused) {
                        f3773b = false;
                    }
                }
                f3772a = applicationContext;
                boolean booleanValue = f3773b.booleanValue();
                return booleanValue;
            }
            boolean booleanValue2 = f3773b.booleanValue();
            return booleanValue2;
        }
    }
}
