package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* renamed from: com.google.android.exoplayer2.audio.f */
/* compiled from: lambda */
public final /* synthetic */ class C1776f implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f2796P;

    /* renamed from: Q */
    private final /* synthetic */ int f2797Q;

    /* renamed from: R */
    private final /* synthetic */ long f2798R;

    /* renamed from: S */
    private final /* synthetic */ long f2799S;

    public /* synthetic */ C1776f(AudioRendererEventListener.EventDispatcher eventDispatcher, int i, long j, long j2) {
        this.f2796P = eventDispatcher;
        this.f2797Q = i;
        this.f2798R = j;
        this.f2799S = j2;
    }

    public final void run() {
        this.f2796P.mo14093a(this.f2797Q, this.f2798R, this.f2799S);
    }
}
