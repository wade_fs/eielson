package com.google.android.gms.measurement.internal;

import android.os.Binder;
import android.text.TextUtils;
import androidx.annotation.BinderThread;
import androidx.annotation.Nullable;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.C2177g;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.C2326q;
import com.google.android.gms.internal.measurement.C2489ea;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;

/* renamed from: com.google.android.gms.measurement.internal.o5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C3141o5 extends C3261z3 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final C3145o9 f5496a;

    /* renamed from: b */
    private Boolean f5497b;
    @Nullable

    /* renamed from: c */
    private String f5498c;

    public C3141o5(C3145o9 o9Var) {
        this(o9Var, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final void mo19189a(zzan zzan, zzm zzm) {
        C2258v.m5629a(zzan);
        m9015b(zzm, false);
        m9013a(new C3230w5(this, zzan, zzm));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: b */
    public final void mo19197b(zzm zzm) {
        m9015b(zzm, false);
        m9013a(new C3129n5(this, zzm));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: c */
    public final String mo19198c(zzm zzm) {
        m9015b(zzm, false);
        return this.f5496a.mo19228d(zzm);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: d */
    public final void mo19199d(zzm zzm) {
        m9015b(zzm, false);
        m9013a(new C3263z5(this, zzm));
    }

    private C3141o5(C3145o9 o9Var, @Nullable String str) {
        C2258v.m5629a(o9Var);
        this.f5496a = o9Var;
        this.f5498c = null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final zzan mo19196b(zzan zzan, zzm zzm) {
        zzam zzam;
        boolean z = false;
        if (!(!"_cmp".equals(zzan.f5803P) || (zzam = zzan.f5804Q) == null || zzam.mo19458a() == 0)) {
            String e = zzan.f5804Q.mo19463e("_cis");
            if (!TextUtils.isEmpty(e) && (("referrer broadcast".equals(e) || "referrer API".equals(e)) && this.f5496a.mo19220b().mo19154e(zzm.f5814P, C3135o.f5410O))) {
                z = true;
            }
        }
        if (!z) {
            return zzan;
        }
        this.f5496a.mo19015l().mo19007z().mo19043a("Event has been filtered ", zzan.toString());
        return new zzan("_cmpx", zzan.f5804Q, zzan.f5805R, zzan.f5806S);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final void mo19190a(zzan zzan, String str, String str2) {
        C2258v.m5629a(zzan);
        C2258v.m5639b(str);
        m9014a(str, true);
        m9013a(new C3219v5(this, zzan, str));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final byte[] mo19195a(zzan zzan, String str) {
        C2258v.m5639b(str);
        C2258v.m5629a(zzan);
        m9014a(str, true);
        this.f5496a.mo19015l().mo18995A().mo19043a("Log and bundle. event", this.f5496a.mo19233i().mo18822a(zzan.f5803P));
        long b = this.f5496a.mo19017o().mo17133b() / 1000000;
        try {
            byte[] bArr = (byte[]) this.f5496a.mo19014j().mo19029b(new C3252y5(this, zzan, str)).get();
            if (bArr == null) {
                this.f5496a.mo19015l().mo19001t().mo19043a("Log and bundle returned null. appId", C3032f4.m8621a(str));
                bArr = new byte[0];
            }
            this.f5496a.mo19015l().mo18995A().mo19045a("Log and bundle processed. event, size, time_ms", this.f5496a.mo19233i().mo18822a(zzan.f5803P), Integer.valueOf(bArr.length), Long.valueOf((this.f5496a.mo19017o().mo17133b() / 1000000) - b));
            return bArr;
        } catch (InterruptedException | ExecutionException e) {
            this.f5496a.mo19015l().mo19001t().mo19045a("Failed to log and bundle. appId, event, error", C3032f4.m8621a(str), this.f5496a.mo19233i().mo18822a(zzan.f5803P), e);
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: b */
    private final void m9015b(zzm zzm, boolean z) {
        C2258v.m5629a(zzm);
        m9014a(zzm.f5814P, false);
        this.f5496a.mo19234k().mo19445a(zzm.f5815Q, zzm.f5831g0, zzm.f5835k0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final void mo19191a(zzkq zzkq, zzm zzm) {
        C2258v.m5629a(zzkq);
        m9015b(zzm, false);
        m9013a(new C3241x5(this, zzkq, zzm));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final List<zzkq> mo19183a(zzm zzm, boolean z) {
        m9015b(zzm, false);
        try {
            List<C3234w9> list = (List) this.f5496a.mo19014j().mo19027a(new C2974a6(this, zzm)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (C3234w9 w9Var : list) {
                if (z || !C3267z9.m9430f(w9Var.f5735c)) {
                    arrayList.add(new zzkq(w9Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            if (!C2489ea.m6262b() || !this.f5496a.mo19220b().mo19154e(zzm.f5814P, C3135o.f5431Y0)) {
                this.f5496a.mo19015l().mo19001t().mo19044a("Failed to get user attributes. appId", C3032f4.m8621a(zzm.f5814P), e);
                return null;
            }
            this.f5496a.mo19015l().mo19001t().mo19044a("Failed to get user properties. appId", C3032f4.m8621a(zzm.f5814P), e);
            return null;
        }
    }

    @BinderThread
    /* renamed from: a */
    private final void m9014a(String str, boolean z) {
        boolean z2;
        if (!TextUtils.isEmpty(str)) {
            if (z) {
                try {
                    if (this.f5497b == null) {
                        if (!"com.google.android.gms".equals(this.f5498c) && !C2326q.m5806a(this.f5496a.mo19016n(), Binder.getCallingUid())) {
                            if (!C2177g.m5307a(this.f5496a.mo19016n()).mo16854a(Binder.getCallingUid())) {
                                z2 = false;
                                this.f5497b = Boolean.valueOf(z2);
                            }
                        }
                        z2 = true;
                        this.f5497b = Boolean.valueOf(z2);
                    }
                    if (this.f5497b.booleanValue()) {
                        return;
                    }
                } catch (SecurityException e) {
                    this.f5496a.mo19015l().mo19001t().mo19043a("Measurement Service called with invalid calling package. appId", C3032f4.m8621a(str));
                    throw e;
                }
            }
            if (this.f5498c == null && C2176f.m5297a(this.f5496a.mo19016n(), Binder.getCallingUid(), str)) {
                this.f5498c = str;
            }
            if (!str.equals(this.f5498c)) {
                throw new SecurityException(String.format("Unknown calling package name '%s'.", str));
            }
            return;
        }
        this.f5496a.mo19015l().mo19001t().mo19042a("Measurement Service called without app package");
        throw new SecurityException("Measurement Service called without app package");
    }

    @BinderThread
    /* renamed from: a */
    public final void mo19188a(long j, String str, String str2, String str3) {
        m9013a(new C2998c6(this, str2, str3, str, j));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final void mo19194a(zzv zzv, zzm zzm) {
        C2258v.m5629a(zzv);
        C2258v.m5629a(zzv.f5838R);
        m9015b(zzm, false);
        zzv zzv2 = new zzv(zzv);
        zzv2.f5836P = zzm.f5814P;
        m9013a(new C2986b6(this, zzv2, zzm));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final void mo19193a(zzv zzv) {
        C2258v.m5629a(zzv);
        C2258v.m5629a(zzv.f5838R);
        m9014a(zzv.f5836P, true);
        m9013a(new C3164q5(this, new zzv(zzv)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final List<zzkq> mo19187a(String str, String str2, boolean z, zzm zzm) {
        m9015b(zzm, false);
        try {
            List<C3234w9> list = (List) this.f5496a.mo19014j().mo19027a(new C3153p5(this, zzm, str, str2)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (C3234w9 w9Var : list) {
                if (z || !C3267z9.m9430f(w9Var.f5735c)) {
                    arrayList.add(new zzkq(w9Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            if (!C2489ea.m6262b() || !this.f5496a.mo19220b().mo19154e(zzm.f5814P, C3135o.f5431Y0)) {
                this.f5496a.mo19015l().mo19001t().mo19044a("Failed to get user attributes. appId", C3032f4.m8621a(zzm.f5814P), e);
            } else {
                this.f5496a.mo19015l().mo19001t().mo19044a("Failed to query user properties. appId", C3032f4.m8621a(zzm.f5814P), e);
            }
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final List<zzkq> mo19186a(String str, String str2, String str3, boolean z) {
        m9014a(str, true);
        try {
            List<C3234w9> list = (List) this.f5496a.mo19014j().mo19027a(new C3186s5(this, str, str2, str3)).get();
            ArrayList arrayList = new ArrayList(list.size());
            for (C3234w9 w9Var : list) {
                if (z || !C3267z9.m9430f(w9Var.f5735c)) {
                    arrayList.add(new zzkq(w9Var));
                }
            }
            return arrayList;
        } catch (InterruptedException | ExecutionException e) {
            if (!C2489ea.m6262b() || !this.f5496a.mo19220b().mo19154e(str, C3135o.f5431Y0)) {
                this.f5496a.mo19015l().mo19001t().mo19044a("Failed to get user attributes. appId", C3032f4.m8621a(str), e);
            } else {
                this.f5496a.mo19015l().mo19001t().mo19044a("Failed to get user properties as. appId", C3032f4.m8621a(str), e);
            }
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void
     arg types: [com.google.android.gms.measurement.internal.zzm, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):com.google.android.gms.measurement.internal.zzan
      com.google.android.gms.measurement.internal.o5.b(com.google.android.gms.measurement.internal.zzm, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final List<zzv> mo19184a(String str, String str2, zzm zzm) {
        m9015b(zzm, false);
        try {
            return (List) this.f5496a.mo19014j().mo19027a(new C3175r5(this, zzm, str, str2)).get();
        } catch (InterruptedException | ExecutionException e) {
            this.f5496a.mo19015l().mo19001t().mo19043a("Failed to get conditional user properties", e);
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final List<zzv> mo19185a(String str, String str2, String str3) {
        m9014a(str, true);
        try {
            return (List) this.f5496a.mo19014j().mo19027a(new C3208u5(this, str, str2, str3)).get();
        } catch (InterruptedException | ExecutionException e) {
            if (!C2489ea.m6262b() || !this.f5496a.mo19220b().mo19154e(str, C3135o.f5431Y0)) {
                this.f5496a.mo19015l().mo19001t().mo19043a("Failed to get conditional user properties", e);
            } else {
                this.f5496a.mo19015l().mo19001t().mo19043a("Failed to get conditional user properties as", e);
            }
            return Collections.emptyList();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.o5.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzm, boolean):java.util.List<com.google.android.gms.measurement.internal.zzkq>
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzkq, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzv, com.google.android.gms.measurement.internal.zzm):void
      com.google.android.gms.measurement.internal.w3.a(com.google.android.gms.measurement.internal.zzan, java.lang.String):byte[]
      com.google.android.gms.measurement.internal.o5.a(java.lang.String, boolean):void */
    @BinderThread
    /* renamed from: a */
    public final void mo19192a(zzm zzm) {
        m9014a(zzm.f5814P, false);
        m9013a(new C3197t5(this, zzm));
    }

    /* renamed from: a */
    private final void m9013a(Runnable runnable) {
        C2258v.m5629a(runnable);
        if (this.f5496a.mo19014j().mo19031t()) {
            runnable.run();
        } else {
            this.f5496a.mo19014j().mo19028a(runnable);
        }
    }
}
