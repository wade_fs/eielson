package com.google.android.gms.internal.measurement;

import java.lang.Comparable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/* renamed from: com.google.android.gms.internal.measurement.m6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
class C2618m6<K extends Comparable<K>, V> extends AbstractMap<K, V> {

    /* renamed from: P */
    private final int f4326P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public List<C2755v6> f4327Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public Map<K, V> f4328R;

    /* renamed from: S */
    private boolean f4329S;

    /* renamed from: T */
    private volatile C2786x6 f4330T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public Map<K, V> f4331U;

    /* renamed from: V */
    private volatile C2693r6 f4332V;

    private C2618m6(int i) {
        this.f4326P = i;
        this.f4327Q = Collections.emptyList();
        this.f4328R = Collections.emptyMap();
        this.f4331U = Collections.emptyMap();
    }

    /* renamed from: b */
    static <FieldDescriptorType extends C2466d4<FieldDescriptorType>> C2618m6<FieldDescriptorType, Object> m6780b(int i) {
        return new C2663p6(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public final void m6785f() {
        if (this.f4329S) {
            throw new UnsupportedOperationException();
        }
    }

    /* renamed from: g */
    private final SortedMap<K, V> m6786g() {
        m6785f();
        if (this.f4328R.isEmpty() && !(this.f4328R instanceof TreeMap)) {
            this.f4328R = new TreeMap();
            this.f4331U = ((TreeMap) this.f4328R).descendingMap();
        }
        return (SortedMap) this.f4328R;
    }

    /* renamed from: a */
    public void mo17742a() {
        Map<K, V> map;
        Map<K, V> map2;
        if (!this.f4329S) {
            if (this.f4328R.isEmpty()) {
                map = Collections.emptyMap();
            } else {
                map = Collections.unmodifiableMap(this.f4328R);
            }
            this.f4328R = map;
            if (this.f4331U.isEmpty()) {
                map2 = Collections.emptyMap();
            } else {
                map2 = Collections.unmodifiableMap(this.f4331U);
            }
            this.f4331U = map2;
            this.f4329S = true;
        }
    }

    /* renamed from: c */
    public final int mo17744c() {
        return this.f4327Q.size();
    }

    public void clear() {
        m6785f();
        if (!this.f4327Q.isEmpty()) {
            this.f4327Q.clear();
        }
        if (!this.f4328R.isEmpty()) {
            this.f4328R.clear();
        }
    }

    public boolean containsKey(Object obj) {
        Comparable comparable = (Comparable) obj;
        return m6777a(comparable) >= 0 || this.f4328R.containsKey(comparable);
    }

    /* renamed from: d */
    public final Iterable<Map.Entry<K, V>> mo17747d() {
        if (this.f4328R.isEmpty()) {
            return C2678q6.m7084a();
        }
        return this.f4328R.entrySet();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public final Set<Map.Entry<K, V>> mo17748e() {
        if (this.f4332V == null) {
            this.f4332V = new C2693r6(this, null);
        }
        return this.f4332V;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        if (this.f4330T == null) {
            this.f4330T = new C2786x6(this, null);
        }
        return this.f4330T;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2618m6)) {
            return super.equals(obj);
        }
        C2618m6 m6Var = (C2618m6) obj;
        int size = size();
        if (size != m6Var.size()) {
            return false;
        }
        int c = mo17744c();
        if (c != m6Var.mo17744c()) {
            return entrySet().equals(m6Var.entrySet());
        }
        for (int i = 0; i < c; i++) {
            if (!mo17741a(i).equals(m6Var.mo17741a(i))) {
                return false;
            }
        }
        if (c != size) {
            return this.f4328R.equals(m6Var.f4328R);
        }
        return true;
    }

    public V get(Object obj) {
        Comparable comparable = (Comparable) obj;
        int a = m6777a(comparable);
        if (a >= 0) {
            return this.f4327Q.get(a).getValue();
        }
        return this.f4328R.get(comparable);
    }

    public int hashCode() {
        int c = mo17744c();
        int i = 0;
        for (int i2 = 0; i2 < c; i2++) {
            i += this.f4327Q.get(i2).hashCode();
        }
        return this.f4328R.size() > 0 ? i + this.f4328R.hashCode() : i;
    }

    public V remove(Object obj) {
        m6785f();
        Comparable comparable = (Comparable) obj;
        int a = m6777a(comparable);
        if (a >= 0) {
            return m6782c(a);
        }
        if (this.f4328R.isEmpty()) {
            return null;
        }
        return this.f4328R.remove(comparable);
    }

    public int size() {
        return this.f4327Q.size() + this.f4328R.size();
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final V m6782c(int i) {
        m6785f();
        V value = this.f4327Q.remove(i).getValue();
        if (!this.f4328R.isEmpty()) {
            Iterator it = m6786g().entrySet().iterator();
            this.f4327Q.add(new C2755v6(this, (Map.Entry) it.next()));
            it.remove();
        }
        return value;
    }

    /* renamed from: b */
    public final boolean mo17743b() {
        return this.f4329S;
    }

    /* synthetic */ C2618m6(int i, C2663p6 p6Var) {
        this(i);
    }

    /* renamed from: a */
    public final Map.Entry<K, V> mo17741a(int i) {
        return this.f4327Q.get(i);
    }

    /* renamed from: a */
    public final V put(K k, V v) {
        m6785f();
        int a = m6777a((Comparable) k);
        if (a >= 0) {
            return this.f4327Q.get(a).setValue(v);
        }
        m6785f();
        if (this.f4327Q.isEmpty() && !(this.f4327Q instanceof ArrayList)) {
            this.f4327Q = new ArrayList(this.f4326P);
        }
        int i = -(a + 1);
        if (i >= this.f4326P) {
            return m6786g().put(k, v);
        }
        int size = this.f4327Q.size();
        int i2 = this.f4326P;
        if (size == i2) {
            C2755v6 remove = this.f4327Q.remove(i2 - 1);
            m6786g().put((Comparable) remove.getKey(), remove.getValue());
        }
        this.f4327Q.add(i, new C2755v6(this, k, v));
        return null;
    }

    /* renamed from: a */
    private final int m6777a(Comparable comparable) {
        int size = this.f4327Q.size() - 1;
        if (size >= 0) {
            int compareTo = comparable.compareTo((Comparable) this.f4327Q.get(size).getKey());
            if (compareTo > 0) {
                return -(size + 2);
            }
            if (compareTo == 0) {
                return size;
            }
        }
        int i = 0;
        while (i <= size) {
            int i2 = (i + size) / 2;
            int compareTo2 = comparable.compareTo((Comparable) this.f4327Q.get(i2).getKey());
            if (compareTo2 < 0) {
                size = i2 - 1;
            } else if (compareTo2 <= 0) {
                return i2;
            } else {
                i = i2 + 1;
            }
        }
        return -(i + 1);
    }
}
