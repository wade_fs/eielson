package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.i4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
enum C2548i4 {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);

    private C2548i4(boolean z) {
    }
}
