package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.List;

public final class CircleOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<CircleOptions> CREATOR = new C2938i();

    /* renamed from: P */
    private LatLng f4809P = null;

    /* renamed from: Q */
    private double f4810Q = 0.0d;

    /* renamed from: R */
    private float f4811R = 10.0f;

    /* renamed from: S */
    private int f4812S = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: T */
    private int f4813T = 0;

    /* renamed from: U */
    private float f4814U = 0.0f;

    /* renamed from: V */
    private boolean f4815V = true;

    /* renamed from: W */
    private boolean f4816W = false;
    @Nullable

    /* renamed from: X */
    private List<PatternItem> f4817X = null;

    public CircleOptions() {
    }

    /* renamed from: A */
    public final boolean mo18505A() {
        return this.f4815V;
    }

    /* renamed from: c */
    public final LatLng mo18506c() {
        return this.f4809P;
    }

    /* renamed from: d */
    public final int mo18507d() {
        return this.f4813T;
    }

    /* renamed from: u */
    public final double mo18508u() {
        return this.f4810Q;
    }

    /* renamed from: v */
    public final int mo18509v() {
        return this.f4812S;
    }

    @Nullable
    /* renamed from: w */
    public final List<PatternItem> mo18510w() {
        return this.f4817X;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 2, (Parcelable) mo18506c(), i, false);
        C2250b.m5589a(parcel, 3, mo18508u());
        C2250b.m5590a(parcel, 4, mo18512x());
        C2250b.m5591a(parcel, 5, mo18509v());
        C2250b.m5591a(parcel, 6, mo18507d());
        C2250b.m5590a(parcel, 7, mo18513y());
        C2250b.m5605a(parcel, 8, mo18505A());
        C2250b.m5605a(parcel, 9, mo18514z());
        C2250b.m5614c(parcel, 10, mo18510w(), false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final float mo18512x() {
        return this.f4811R;
    }

    /* renamed from: y */
    public final float mo18513y() {
        return this.f4814U;
    }

    /* renamed from: z */
    public final boolean mo18514z() {
        return this.f4816W;
    }

    CircleOptions(LatLng latLng, double d, float f, int i, int i2, float f2, boolean z, boolean z2, @Nullable List<PatternItem> list) {
        this.f4809P = latLng;
        this.f4810Q = d;
        this.f4811R = f;
        this.f4812S = i;
        this.f4813T = i2;
        this.f4814U = f2;
        this.f4815V = z;
        this.f4816W = z2;
        this.f4817X = list;
    }
}
