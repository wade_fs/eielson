package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.h6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2535h6<E> extends C2813z2<E> implements RandomAccess {

    /* renamed from: S */
    private static final C2535h6<Object> f4211S;

    /* renamed from: Q */
    private E[] f4212Q;

    /* renamed from: R */
    private int f4213R;

    static {
        C2535h6<Object> h6Var = new C2535h6<>(new Object[0], 0);
        f4211S = h6Var;
        super.mo17939e();
    }

    private C2535h6(E[] eArr, int i) {
        this.f4212Q = eArr;
        this.f4213R = i;
    }

    /* renamed from: b */
    private final void m6447b(int i) {
        if (i < 0 || i >= this.f4213R) {
            throw new IndexOutOfBoundsException(m6448c(i));
        }
    }

    /* renamed from: c */
    private final String m6448c(int i) {
        int i2 = this.f4213R;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: g */
    public static <E> C2535h6<E> m6449g() {
        return f4211S;
    }

    /* renamed from: a */
    public final /* synthetic */ C2738u4 mo17320a(int i) {
        if (i >= this.f4213R) {
            return new C2535h6(Arrays.copyOf(this.f4212Q, i), this.f4213R);
        }
        throw new IllegalArgumentException();
    }

    public final boolean add(E e) {
        mo18179b();
        int i = this.f4213R;
        E[] eArr = this.f4212Q;
        if (i == eArr.length) {
            this.f4212Q = Arrays.copyOf(eArr, ((i * 3) / 2) + 1);
        }
        E[] eArr2 = this.f4212Q;
        int i2 = this.f4213R;
        this.f4213R = i2 + 1;
        eArr2[i2] = e;
        this.modCount++;
        return true;
    }

    public final E get(int i) {
        m6447b(i);
        return this.f4212Q[i];
    }

    public final E remove(int i) {
        mo18179b();
        m6447b(i);
        E[] eArr = this.f4212Q;
        E e = eArr[i];
        int i2 = this.f4213R;
        if (i < i2 - 1) {
            System.arraycopy(eArr, i + 1, eArr, i, (i2 - i) - 1);
        }
        this.f4213R--;
        this.modCount++;
        return e;
    }

    public final E set(int i, E e) {
        mo18179b();
        m6447b(i);
        E[] eArr = this.f4212Q;
        E e2 = eArr[i];
        eArr[i] = e;
        this.modCount++;
        return e2;
    }

    public final int size() {
        return this.f4213R;
    }

    public final void add(int i, E e) {
        int i2;
        mo18179b();
        if (i < 0 || i > (i2 = this.f4213R)) {
            throw new IndexOutOfBoundsException(m6448c(i));
        }
        E[] eArr = this.f4212Q;
        if (i2 < eArr.length) {
            System.arraycopy(eArr, i, eArr, i + 1, i2 - i);
        } else {
            E[] eArr2 = new Object[(((i2 * 3) / 2) + 1)];
            System.arraycopy(eArr, 0, eArr2, 0, i);
            System.arraycopy(this.f4212Q, i, eArr2, i + 1, this.f4213R - i);
            this.f4212Q = eArr2;
        }
        this.f4212Q[i] = e;
        this.f4213R++;
        this.modCount++;
    }
}
