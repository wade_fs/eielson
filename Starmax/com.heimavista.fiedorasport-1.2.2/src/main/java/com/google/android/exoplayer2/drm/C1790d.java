package com.google.android.exoplayer2.drm;

import android.media.MediaDrm;
import com.google.android.exoplayer2.drm.ExoMediaDrm;
import java.util.List;

/* renamed from: com.google.android.exoplayer2.drm.d */
/* compiled from: lambda */
public final /* synthetic */ class C1790d implements MediaDrm.OnKeyStatusChangeListener {

    /* renamed from: a */
    private final /* synthetic */ FrameworkMediaDrm f2804a;

    /* renamed from: b */
    private final /* synthetic */ ExoMediaDrm.OnKeyStatusChangeListener f2805b;

    public /* synthetic */ C1790d(FrameworkMediaDrm frameworkMediaDrm, ExoMediaDrm.OnKeyStatusChangeListener onKeyStatusChangeListener) {
        this.f2804a = frameworkMediaDrm;
        this.f2805b = onKeyStatusChangeListener;
    }

    public final void onKeyStatusChange(MediaDrm mediaDrm, byte[] bArr, List list, boolean z) {
        this.f2804a.mo14319a(this.f2805b, mediaDrm, bArr, list, z);
    }
}
