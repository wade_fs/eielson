package com.google.android.exoplayer2.p086ui.spherical;

import android.graphics.SurfaceTexture;

/* renamed from: com.google.android.exoplayer2.ui.spherical.b */
/* compiled from: lambda */
public final /* synthetic */ class C1921b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ SphericalSurfaceView f2931P;

    /* renamed from: Q */
    private final /* synthetic */ SurfaceTexture f2932Q;

    public /* synthetic */ C1921b(SphericalSurfaceView sphericalSurfaceView, SurfaceTexture surfaceTexture) {
        this.f2931P = sphericalSurfaceView;
        this.f2932Q = surfaceTexture;
    }

    public final void run() {
        this.f2931P.mo15881a(this.f2932Q);
    }
}
