package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.internal.measurement.m */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2610m extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ int f4314T = 5;

    /* renamed from: U */
    private final /* synthetic */ String f4315U;

    /* renamed from: V */
    private final /* synthetic */ Object f4316V;

    /* renamed from: W */
    private final /* synthetic */ Object f4317W;

    /* renamed from: X */
    private final /* synthetic */ Object f4318X;

    /* renamed from: Y */
    private final /* synthetic */ C2525gd f4319Y;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2610m(C2525gd gdVar, boolean z, int i, String str, Object obj, Object obj2, Object obj3) {
        super(false);
        this.f4319Y = gdVar;
        this.f4315U = str;
        this.f4316V = obj;
        this.f4317W = null;
        this.f4318X = null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4319Y.f4192g.logHealthData(this.f4314T, this.f4315U, C3992d.m11990a(this.f4316V), C3992d.m11990a(this.f4317W), C3992d.m11990a(this.f4318X));
    }
}
