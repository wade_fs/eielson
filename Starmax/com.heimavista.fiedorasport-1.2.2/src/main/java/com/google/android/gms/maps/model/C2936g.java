package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.maps.model.g */
public final class C2936g implements Parcelable.Creator<CameraPosition> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        float f = 0.0f;
        LatLng latLng = null;
        float f2 = 0.0f;
        float f3 = 0.0f;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 2) {
                latLng = (LatLng) C2248a.m5553a(parcel, a, LatLng.CREATOR);
            } else if (a2 == 3) {
                f = C2248a.m5582w(parcel, a);
            } else if (a2 == 4) {
                f2 = C2248a.m5582w(parcel, a);
            } else if (a2 != 5) {
                C2248a.m5550F(parcel, a);
            } else {
                f3 = C2248a.m5582w(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new CameraPosition(latLng, f, f2, f3);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CameraPosition[i];
    }
}
