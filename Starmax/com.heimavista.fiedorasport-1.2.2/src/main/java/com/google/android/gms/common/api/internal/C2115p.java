package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2018b;
import com.google.android.gms.common.api.internal.C2084i;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.p */
public abstract class C2115p<A extends C2016a.C2018b, L> {

    /* renamed from: a */
    private final C2084i.C2085a<L> f3417a;

    protected C2115p(C2084i.C2085a<L> aVar) {
        this.f3417a = aVar;
    }

    /* renamed from: a */
    public C2084i.C2085a<L> mo16762a() {
        return this.f3417a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16763a(A a, C4066i<Boolean> iVar);
}
