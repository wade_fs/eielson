package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.o9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2652o9 implements C2666p9 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4407a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.service.audience.invalidate_config_cache_after_app_unisntall", true);

    /* renamed from: a */
    public final boolean mo17802a() {
        return f4407a.mo18128b().booleanValue();
    }
}
