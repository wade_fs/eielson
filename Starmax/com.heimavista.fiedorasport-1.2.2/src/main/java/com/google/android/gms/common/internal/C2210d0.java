package com.google.android.gms.common.internal;

import android.content.Intent;
import com.google.android.gms.common.api.internal.C2080h;

/* renamed from: com.google.android.gms.common.internal.d0 */
final class C2210d0 extends C2217g {

    /* renamed from: P */
    private final /* synthetic */ Intent f3686P;

    /* renamed from: Q */
    private final /* synthetic */ C2080h f3687Q;

    /* renamed from: R */
    private final /* synthetic */ int f3688R;

    C2210d0(Intent intent, C2080h hVar, int i) {
        this.f3686P = intent;
        this.f3687Q = hVar;
        this.f3688R = i;
    }

    /* renamed from: a */
    public final void mo16904a() {
        Intent intent = this.f3686P;
        if (intent != null) {
            this.f3687Q.startActivityForResult(intent, this.f3688R);
        }
    }
}
