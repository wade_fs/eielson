package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.internal.C2258v;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.z */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3257z extends C3008d4 {

    /* renamed from: b */
    private final Map<String, Long> f5782b = new ArrayMap();

    /* renamed from: c */
    private final Map<String, Integer> f5783c = new ArrayMap();

    /* renamed from: d */
    private long f5784d;

    public C3257z(C3081j5 j5Var) {
        super(j5Var);
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: c */
    public final void m9398c(String str, long j) {
        mo18880a();
        mo18881c();
        C2258v.m5639b(str);
        if (this.f5783c.isEmpty()) {
            this.f5784d = j;
        }
        Integer num = this.f5783c.get(str);
        if (num != null) {
            this.f5783c.put(str, Integer.valueOf(num.intValue() + 1));
        } else if (this.f5783c.size() >= 100) {
            mo19015l().mo19004w().mo19042a("Too many ads visible");
        } else {
            this.f5783c.put(str, 1);
            this.f5782b.put(str, Long.valueOf(j));
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: d */
    public final void m9399d(String str, long j) {
        mo18880a();
        mo18881c();
        C2258v.m5639b(str);
        Integer num = this.f5783c.get(str);
        if (num != null) {
            C3188s7 B = mo18887s().mo19295B();
            int intValue = num.intValue() - 1;
            if (intValue == 0) {
                this.f5783c.remove(str);
                Long l = this.f5782b.get(str);
                if (l == null) {
                    mo19015l().mo19001t().mo19042a("First ad unit exposure time was never set");
                } else {
                    this.f5782b.remove(str);
                    m9395a(str, j - l.longValue(), B);
                }
                if (this.f5783c.isEmpty()) {
                    long j2 = this.f5784d;
                    if (j2 == 0) {
                        mo19015l().mo19001t().mo19042a("First ad exposure time was never set");
                        return;
                    }
                    m9392a(j - j2, B);
                    this.f5784d = 0;
                    return;
                }
                return;
            }
            this.f5783c.put(str, Integer.valueOf(intValue));
            return;
        }
        mo19015l().mo19001t().mo19043a("Call to endAdUnitExposure for unknown ad unit id", str);
    }

    /* renamed from: a */
    public final void mo19415a(String str, long j) {
        if (str == null || str.length() == 0) {
            mo19015l().mo19001t().mo19042a("Ad unit id must be a non-empty string");
        } else {
            mo19014j().mo19028a(new C2967a(this, str, j));
        }
    }

    /* renamed from: b */
    public final void mo19416b(String str, long j) {
        if (str == null || str.length() == 0) {
            mo19015l().mo19001t().mo19042a("Ad unit id must be a non-empty string");
        } else {
            mo19014j().mo19028a(new C2982b2(this, str, j));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.s7, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void */
    @WorkerThread
    /* renamed from: a */
    private final void m9392a(long j, C3188s7 s7Var) {
        if (s7Var == null) {
            mo19015l().mo18996B().mo19042a("Not logging ad exposure. No active activity");
        } else if (j < 1000) {
            mo19015l().mo18996B().mo19043a("Not logging ad exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putLong("_xt", j);
            C3177r7.m9192a(s7Var, bundle, true);
            mo18884m().mo19267a("am", "_xa", bundle);
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: b */
    public final void m9396b(long j) {
        for (String str : this.f5782b.keySet()) {
            this.f5782b.put(str, Long.valueOf(j));
        }
        if (!this.f5782b.isEmpty()) {
            this.f5784d = j;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.s7, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void */
    @WorkerThread
    /* renamed from: a */
    private final void m9395a(String str, long j, C3188s7 s7Var) {
        if (s7Var == null) {
            mo19015l().mo18996B().mo19042a("Not logging ad unit exposure. No active activity");
        } else if (j < 1000) {
            mo19015l().mo18996B().mo19043a("Not logging ad unit exposure. Less than 1000 ms. exposure", Long.valueOf(j));
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("_ai", str);
            bundle.putLong("_xt", j);
            C3177r7.m9192a(s7Var, bundle, true);
            mo18884m().mo19267a("am", "_xu", bundle);
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19414a(long j) {
        C3188s7 B = mo18887s().mo19295B();
        for (String str : this.f5782b.keySet()) {
            m9395a(str, j - this.f5782b.get(str).longValue(), B);
        }
        if (!this.f5782b.isEmpty()) {
            m9392a(j - this.f5784d, B);
        }
        m9396b(j);
    }
}
