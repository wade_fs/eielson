package com.google.android.gms.internal.measurement;

import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.r5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
interface C2692r5 {
    /* renamed from: a */
    int mo17827a(int i, Object obj, Object obj2);

    /* renamed from: a */
    Object mo17828a(Object obj, Object obj2);

    /* renamed from: a */
    Map<?, ?> mo17829a(Object obj);

    /* renamed from: b */
    boolean mo17830b(Object obj);

    /* renamed from: c */
    Map<?, ?> mo17831c(Object obj);

    /* renamed from: d */
    C2662p5<?, ?> mo17832d(Object obj);

    /* renamed from: e */
    Object mo17833e(Object obj);

    /* renamed from: f */
    Object mo17834f(Object obj);
}
