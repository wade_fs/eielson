package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import com.google.android.gms.common.server.converter.StringToIntConverter;

/* renamed from: com.google.android.gms.common.server.converter.c */
public final class C2296c implements Parcelable.Creator<StringToIntConverter.zaa> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        String str = null;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                i2 = C2248a.m5585z(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new StringToIntConverter.zaa(i, str, i2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new StringToIntConverter.zaa[i];
    }
}
