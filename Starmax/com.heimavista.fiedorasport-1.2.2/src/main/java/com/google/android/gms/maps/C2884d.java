package com.google.android.gms.maps;

import android.content.Context;
import android.os.RemoteException;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2931b;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2892b0;
import com.google.android.gms.maps.p093i.C2894c0;

/* renamed from: com.google.android.gms.maps.d */
public final class C2884d {

    /* renamed from: a */
    private static boolean f4790a = false;

    /* renamed from: a */
    public static synchronized int m8123a(Context context) {
        synchronized (C2884d.class) {
            C2258v.m5630a(context, "Context is null");
            if (f4790a) {
                return 0;
            }
            try {
                C2894c0 a = C2892b0.m8145a(context);
                try {
                    C2879b.m8110a(a.mo18448t());
                    C2931b.m8344a(a.mo18447h());
                    f4790a = true;
                    return 0;
                } catch (RemoteException e) {
                    throw new C2934e(e);
                }
            } catch (C2170d e2) {
                return e2.f3550P;
            }
        }
    }
}
