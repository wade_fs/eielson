package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.q7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public enum C2679q7 {
    DOUBLE(C2787x7.DOUBLE, 1),
    FLOAT(C2787x7.FLOAT, 5),
    INT64(C2787x7.LONG, 0),
    UINT64(C2787x7.LONG, 0),
    INT32(C2787x7.INT, 0),
    FIXED64(C2787x7.LONG, 1),
    FIXED32(C2787x7.INT, 5),
    BOOL(C2787x7.BOOLEAN, 0),
    STRING(C2787x7.STRING, 2),
    GROUP(C2787x7.MESSAGE, 3),
    MESSAGE(C2787x7.MESSAGE, 2),
    BYTES(C2787x7.BYTE_STRING, 2),
    UINT32(C2787x7.INT, 0),
    ENUM(C2787x7.ENUM, 0),
    SFIXED32(C2787x7.INT, 5),
    SFIXED64(C2787x7.LONG, 1),
    SINT32(C2787x7.INT, 0),
    SINT64(C2787x7.LONG, 0);
    

    /* renamed from: P */
    private final C2787x7 f4449P;

    /* renamed from: Q */
    private final int f4450Q;

    private C2679q7(C2787x7 x7Var, int i) {
        this.f4449P = x7Var;
        this.f4450Q = i;
    }

    /* synthetic */ C2679q7(C2787x7 x7Var, int i, C2694r7 r7Var) {
        this(r1, r2, x7Var, i);
    }
}
