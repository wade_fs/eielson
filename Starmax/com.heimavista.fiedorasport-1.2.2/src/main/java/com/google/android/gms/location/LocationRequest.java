package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class LocationRequest extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<LocationRequest> CREATOR = new C2843j();

    /* renamed from: P */
    private int f4665P;

    /* renamed from: Q */
    private long f4666Q;

    /* renamed from: R */
    private long f4667R;

    /* renamed from: S */
    private boolean f4668S;

    /* renamed from: T */
    private long f4669T;

    /* renamed from: U */
    private int f4670U;

    /* renamed from: V */
    private float f4671V;

    /* renamed from: W */
    private long f4672W;

    public LocationRequest() {
        this.f4665P = 102;
        this.f4666Q = 3600000;
        this.f4667R = 600000;
        this.f4668S = false;
        this.f4669T = Long.MAX_VALUE;
        this.f4670U = Integer.MAX_VALUE;
        this.f4671V = 0.0f;
        this.f4672W = 0;
    }

    LocationRequest(int i, long j, long j2, boolean z, long j3, int i2, float f, long j4) {
        this.f4665P = i;
        this.f4666Q = j;
        this.f4667R = j2;
        this.f4668S = z;
        this.f4669T = j3;
        this.f4670U = i2;
        this.f4671V = f;
        this.f4672W = j4;
    }

    /* renamed from: d */
    public static LocationRequest m7950d() {
        return new LocationRequest();
    }

    /* renamed from: g */
    private static void m7951g(long j) {
        if (j < 0) {
            StringBuilder sb = new StringBuilder(38);
            sb.append("invalid interval: ");
            sb.append(j);
            throw new IllegalArgumentException(sb.toString());
        }
    }

    /* renamed from: a */
    public final LocationRequest mo18225a(float f) {
        if (f >= 0.0f) {
            this.f4671V = f;
            return this;
        }
        StringBuilder sb = new StringBuilder(37);
        sb.append("invalid displacement: ");
        sb.append(f);
        throw new IllegalArgumentException(sb.toString());
    }

    /* renamed from: c */
    public final long mo18226c() {
        long j = this.f4672W;
        long j2 = this.f4666Q;
        return j < j2 ? j2 : j;
    }

    /* renamed from: d */
    public final LocationRequest mo18227d(int i) {
        if (i == 100 || i == 102 || i == 104 || i == 105) {
            this.f4665P = i;
            return this;
        }
        StringBuilder sb = new StringBuilder(28);
        sb.append("invalid quality: ");
        sb.append(i);
        throw new IllegalArgumentException(sb.toString());
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocationRequest)) {
            return false;
        }
        LocationRequest locationRequest = (LocationRequest) obj;
        return this.f4665P == locationRequest.f4665P && this.f4666Q == locationRequest.f4666Q && this.f4667R == locationRequest.f4667R && this.f4668S == locationRequest.f4668S && this.f4669T == locationRequest.f4669T && this.f4670U == locationRequest.f4670U && this.f4671V == locationRequest.f4671V && mo18226c() == locationRequest.mo18226c();
    }

    /* renamed from: h */
    public final LocationRequest mo18229h(long j) {
        m7951g(j);
        this.f4668S = true;
        this.f4667R = j;
        return this;
    }

    public final int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4665P), Long.valueOf(this.f4666Q), Float.valueOf(this.f4671V), Long.valueOf(this.f4672W));
    }

    /* renamed from: i */
    public final LocationRequest mo18231i(long j) {
        m7951g(j);
        this.f4666Q = j;
        if (!this.f4668S) {
            this.f4667R = (long) (((double) this.f4666Q) / 6.0d);
        }
        return this;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request[");
        int i = this.f4665P;
        sb.append(i != 100 ? i != 102 ? i != 104 ? i != 105 ? "???" : "PRIORITY_NO_POWER" : "PRIORITY_LOW_POWER" : "PRIORITY_BALANCED_POWER_ACCURACY" : "PRIORITY_HIGH_ACCURACY");
        if (this.f4665P != 105) {
            sb.append(" requested=");
            sb.append(this.f4666Q);
            sb.append("ms");
        }
        sb.append(" fastest=");
        sb.append(this.f4667R);
        sb.append("ms");
        if (this.f4672W > this.f4666Q) {
            sb.append(" maxWait=");
            sb.append(this.f4672W);
            sb.append("ms");
        }
        if (this.f4671V > 0.0f) {
            sb.append(" smallestDisplacement=");
            sb.append(this.f4671V);
            sb.append("m");
        }
        long j = this.f4669T;
        if (j != Long.MAX_VALUE) {
            sb.append(" expireIn=");
            sb.append(j - SystemClock.elapsedRealtime());
            sb.append("ms");
        }
        if (this.f4670U != Integer.MAX_VALUE) {
            sb.append(" num=");
            sb.append(this.f4670U);
        }
        sb.append(']');
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f4665P);
        C2250b.m5592a(parcel, 2, this.f4666Q);
        C2250b.m5592a(parcel, 3, this.f4667R);
        C2250b.m5605a(parcel, 4, this.f4668S);
        C2250b.m5592a(parcel, 5, this.f4669T);
        C2250b.m5591a(parcel, 6, this.f4670U);
        C2250b.m5590a(parcel, 7, this.f4671V);
        C2250b.m5592a(parcel, 8, this.f4672W);
        C2250b.m5587a(parcel, a);
    }
}
