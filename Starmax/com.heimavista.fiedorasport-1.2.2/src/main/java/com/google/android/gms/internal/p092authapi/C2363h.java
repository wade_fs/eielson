package com.google.android.gms.internal.p092authapi;

import android.os.IBinder;

/* renamed from: com.google.android.gms.internal.auth-api.h */
public final class C2363h extends C2356a implements C2362g {
    C2363h(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
    }
}
