package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.w5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3230w5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzan f5715P;

    /* renamed from: Q */
    private final /* synthetic */ zzm f5716Q;

    /* renamed from: R */
    private final /* synthetic */ C3141o5 f5717R;

    C3230w5(C3141o5 o5Var, zzan zzan, zzm zzm) {
        this.f5717R = o5Var;
        this.f5715P = zzan;
        this.f5716Q = zzm;
    }

    public final void run() {
        zzan b = this.f5717R.mo19196b(this.f5715P, this.f5716Q);
        this.f5717R.f5496a.mo19237q();
        this.f5717R.f5496a.mo19211a(b, this.f5716Q);
    }
}
