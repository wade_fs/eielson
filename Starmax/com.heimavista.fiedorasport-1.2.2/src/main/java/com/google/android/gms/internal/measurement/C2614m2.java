package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.m2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2614m2<T> implements C2593l2<T> {

    /* renamed from: P */
    private volatile C2593l2<T> f4320P;

    /* renamed from: Q */
    private volatile boolean f4321Q;

    /* renamed from: R */
    private T f4322R;

    C2614m2(C2593l2<T> l2Var) {
        C2546i2.m6469a(l2Var);
        this.f4320P = l2Var;
    }

    /* renamed from: a */
    public final T mo17285a() {
        if (!this.f4321Q) {
            synchronized (this) {
                if (!this.f4321Q) {
                    T a = this.f4320P.mo17285a();
                    this.f4322R = a;
                    this.f4321Q = true;
                    this.f4320P = null;
                    return a;
                }
            }
        }
        return this.f4322R;
    }

    public final String toString() {
        Object obj = this.f4320P;
        if (obj == null) {
            String valueOf = String.valueOf(this.f4322R);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(">");
            obj = sb.toString();
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }
}
