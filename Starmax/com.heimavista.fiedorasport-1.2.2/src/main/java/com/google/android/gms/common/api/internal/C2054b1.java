package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.internal.C2064e;

/* renamed from: com.google.android.gms.common.api.internal.b1 */
final class C2054b1 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ ConnectionResult f3251P;

    /* renamed from: Q */
    private final /* synthetic */ C2064e.C2065a f3252Q;

    C2054b1(C2064e.C2065a aVar, ConnectionResult connectionResult) {
        this.f3252Q = aVar;
        this.f3251P = connectionResult;
    }

    public final void run() {
        this.f3252Q.mo16585a(this.f3251P);
    }
}
