package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.z7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2818z7 implements C2593l2<C2803y7> {

    /* renamed from: Q */
    private static C2818z7 f4626Q = new C2818z7();

    /* renamed from: P */
    private final C2593l2<C2803y7> f4627P;

    private C2818z7(C2593l2<C2803y7> l2Var) {
        this.f4627P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7929b() {
        return ((C2803y7) f4626Q.mo17285a()).mo17336a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4627P.mo17285a();
    }

    public C2818z7() {
        this(C2579k2.m6601a(new C2438b8()));
    }
}
