package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.common.api.internal.l2 */
public final class C2102l2 implements C2036f.C2038b, C2036f.C2039c {

    /* renamed from: a */
    public final C2016a<?> f3384a;

    /* renamed from: b */
    private final boolean f3385b;

    /* renamed from: c */
    private C2106m2 f3386c;

    public C2102l2(C2016a<?> aVar, boolean z) {
        this.f3384a = aVar;
        this.f3385b = z;
    }

    /* renamed from: L */
    public final void mo16583L(int i) {
        m4986a();
        this.f3386c.mo16583L(i);
    }

    /* renamed from: a */
    public final void mo16585a(@NonNull ConnectionResult connectionResult) {
        m4986a();
        this.f3386c.mo16665a(connectionResult, this.f3384a, this.f3385b);
    }

    /* renamed from: f */
    public final void mo16584f(@Nullable Bundle bundle) {
        m4986a();
        this.f3386c.mo16584f(bundle);
    }

    /* renamed from: a */
    public final void mo16753a(C2106m2 m2Var) {
        this.f3386c = m2Var;
    }

    /* renamed from: a */
    private final void m4986a() {
        C2258v.m5630a(this.f3386c, "Callbacks must be attached to a ClientConnectionHelper instance before connecting the client.");
    }
}
