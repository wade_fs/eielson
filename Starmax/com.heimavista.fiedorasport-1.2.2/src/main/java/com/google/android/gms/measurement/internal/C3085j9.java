package com.google.android.gms.measurement.internal;

import androidx.annotation.WorkerThread;

/* renamed from: com.google.android.gms.measurement.internal.j9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3085j9 extends C3039g {

    /* renamed from: e */
    private final /* synthetic */ C3049g9 f5271e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3085j9(C3049g9 g9Var, C3058h6 h6Var) {
        super(h6Var);
        this.f5271e = g9Var;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19022a() {
        this.f5271e.m8691c();
    }
}
