package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.y3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public class C2798y3 {

    /* renamed from: b */
    private static volatile C2798y3 f4590b;

    /* renamed from: c */
    private static volatile C2798y3 f4591c;

    /* renamed from: d */
    private static final C2798y3 f4592d = new C2798y3(true);

    /* renamed from: a */
    private final Map<C2799a, C2595l4.C2599d<?, ?>> f4593a;

    /* renamed from: com.google.android.gms.internal.measurement.y3$a */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class C2799a {

        /* renamed from: a */
        private final Object f4594a;

        /* renamed from: b */
        private final int f4595b;

        C2799a(Object obj, int i) {
            this.f4594a = obj;
            this.f4595b = i;
        }

        public final boolean equals(Object obj) {
            if (!(obj instanceof C2799a)) {
                return false;
            }
            C2799a aVar = (C2799a) obj;
            if (this.f4594a == aVar.f4594a && this.f4595b == aVar.f4595b) {
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return (System.identityHashCode(this.f4594a) * 65535) + this.f4595b;
        }
    }

    C2798y3() {
        this.f4593a = new HashMap();
    }

    /* renamed from: a */
    public static C2798y3 m7828a() {
        C2798y3 y3Var = f4590b;
        if (y3Var == null) {
            synchronized (C2798y3.class) {
                y3Var = f4590b;
                if (y3Var == null) {
                    y3Var = f4592d;
                    f4590b = y3Var;
                }
            }
        }
        return y3Var;
    }

    /* renamed from: b */
    public static C2798y3 m7829b() {
        Class<C2798y3> cls = C2798y3.class;
        C2798y3 y3Var = f4591c;
        if (y3Var != null) {
            return y3Var;
        }
        synchronized (cls) {
            C2798y3 y3Var2 = f4591c;
            if (y3Var2 != null) {
                return y3Var2;
            }
            C2798y3 a = C2581k4.m6603a(cls);
            f4591c = a;
            return a;
        }
    }

    private C2798y3(boolean z) {
        this.f4593a = Collections.emptyMap();
    }

    /* renamed from: a */
    public final <ContainingType extends C2739u5> C2595l4.C2599d<ContainingType, ?> mo18164a(ContainingType containingtype, int i) {
        return this.f4593a.get(new C2799a(containingtype, i));
    }
}
