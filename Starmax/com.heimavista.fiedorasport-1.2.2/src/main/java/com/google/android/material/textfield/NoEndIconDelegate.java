package com.google.android.material.textfield;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;

class NoEndIconDelegate extends EndIconDelegate {
    NoEndIconDelegate(@NonNull TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    /* access modifiers changed from: package-private */
    public void initialize() {
        super.textInputLayout.setEndIconOnClickListener(null);
        super.textInputLayout.setEndIconDrawable((Drawable) null);
        super.textInputLayout.setEndIconContentDescription((CharSequence) null);
    }
}
