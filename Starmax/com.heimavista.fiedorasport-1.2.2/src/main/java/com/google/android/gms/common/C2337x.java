package com.google.android.gms.common;

import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.common.x */
final class C2337x extends C2335v {

    /* renamed from: e */
    private final Callable<String> f3874e;

    private C2337x(Callable<String> callable) {
        super(false, null, null);
        this.f3874e = callable;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final String mo17139a() {
        try {
            return this.f3874e.call();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
