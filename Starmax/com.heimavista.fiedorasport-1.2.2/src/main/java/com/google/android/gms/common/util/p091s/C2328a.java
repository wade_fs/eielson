package com.google.android.gms.common.util.p091s;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p157c.p161c.p163b.C4012d;

/* renamed from: com.google.android.gms.common.util.s.a */
public class C2328a implements Executor {

    /* renamed from: a */
    private final Handler f3859a;

    public C2328a(Looper looper) {
        this.f3859a = new C4012d(looper);
    }

    public void execute(@NonNull Runnable runnable) {
        this.f3859a.post(runnable);
    }
}
