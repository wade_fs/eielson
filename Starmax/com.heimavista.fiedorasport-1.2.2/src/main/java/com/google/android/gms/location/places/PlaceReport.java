package com.google.android.gms.location.places;

import android.os.Parcel;
import android.os.Parcelable;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.facebook.share.internal.ShareConstants;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class PlaceReport extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<PlaceReport> CREATOR = new C2855a();

    /* renamed from: P */
    private final int f4695P;

    /* renamed from: Q */
    private final String f4696Q;

    /* renamed from: R */
    private final String f4697R;

    /* renamed from: S */
    private final String f4698S;

    PlaceReport(int i, String str, String str2, String str3) {
        this.f4695P = i;
        this.f4696Q = str;
        this.f4697R = str2;
        this.f4698S = str3;
    }

    /* renamed from: c */
    public String mo18273c() {
        return this.f4696Q;
    }

    /* renamed from: d */
    public String mo18274d() {
        return this.f4697R;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof PlaceReport)) {
            return false;
        }
        PlaceReport placeReport = (PlaceReport) obj;
        return C2251t.m5617a(this.f4696Q, placeReport.f4696Q) && C2251t.m5617a(this.f4697R, placeReport.f4697R) && C2251t.m5617a(this.f4698S, placeReport.f4698S);
    }

    public int hashCode() {
        return C2251t.m5615a(this.f4696Q, this.f4697R, this.f4698S);
    }

    public String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("placeId", this.f4696Q);
        a.mo17037a(ViewHierarchyConstants.TAG_KEY, this.f4697R);
        if (!"unknown".equals(this.f4698S)) {
            a.mo17037a(ShareConstants.FEED_SOURCE_PARAM, this.f4698S);
        }
        return a.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f4695P);
        C2250b.m5602a(parcel, 2, mo18273c(), false);
        C2250b.m5602a(parcel, 3, mo18274d(), false);
        C2250b.m5602a(parcel, 4, this.f4698S, false);
        C2250b.m5587a(parcel, a);
    }
}
