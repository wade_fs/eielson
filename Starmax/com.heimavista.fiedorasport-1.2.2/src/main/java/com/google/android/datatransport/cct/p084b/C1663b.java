package com.google.android.datatransport.cct.p084b;

import android.util.SparseArray;
import com.facebook.share.internal.MessengerShareContentUtility;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: com.google.android.datatransport.cct.b.b */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1663b extends Enum<C1663b> {

    /* renamed from: P */
    public static final C1663b f2567P = new C1663b(MessengerShareContentUtility.PREVIEW_DEFAULT, 0, 0);

    /* renamed from: Q */
    public static final C1663b f2568Q = new C1663b("UNMETERED_ONLY", 1, 1);

    /* renamed from: R */
    public static final C1663b f2569R = new C1663b("UNMETERED_OR_DAILY", 2, 2);

    /* renamed from: S */
    public static final C1663b f2570S = new C1663b("FAST_IF_RADIO_AWAKE", 3, 3);

    /* renamed from: T */
    public static final C1663b f2571T = new C1663b("NEVER", 4, 4);

    /* renamed from: U */
    public static final C1663b f2572U = new C1663b("UNRECOGNIZED", 5, -1);

    /* renamed from: V */
    private static final SparseArray<C1663b> f2573V = new SparseArray<>();

    static {
        C1663b[] bVarArr = {f2567P, f2568Q, f2569R, f2570S, f2571T, f2572U};
        f2573V.put(0, f2567P);
        f2573V.put(1, f2568Q);
        f2573V.put(2, f2569R);
        f2573V.put(3, f2570S);
        f2573V.put(4, f2571T);
        f2573V.put(-1, f2572U);
    }

    private C1663b(String str, int i, int i2) {
    }
}
