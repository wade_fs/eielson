package com.google.android.gms.location;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class LocationResult extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<LocationResult> CREATOR = new C2845k();

    /* renamed from: Q */
    static final List<Location> f4673Q = Collections.emptyList();

    /* renamed from: P */
    private final List<Location> f4674P;

    LocationResult(List<Location> list) {
        this.f4674P = list;
    }

    /* renamed from: c */
    public final Location mo18234c() {
        int size = this.f4674P.size();
        if (size == 0) {
            return null;
        }
        return this.f4674P.get(size - 1);
    }

    @NonNull
    /* renamed from: d */
    public final List<Location> mo18235d() {
        return this.f4674P;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof LocationResult)) {
            return false;
        }
        LocationResult locationResult = (LocationResult) obj;
        if (locationResult.f4674P.size() != this.f4674P.size()) {
            return false;
        }
        Iterator<Location> it = this.f4674P.iterator();
        for (Location location : locationResult.f4674P) {
            if (it.next().getTime() != location.getTime()) {
                return false;
            }
        }
        return true;
    }

    public final int hashCode() {
        int i = 17;
        for (Location location : this.f4674P) {
            long time = location.getTime();
            i = (i * 31) + ((int) (time ^ (time >>> 32)));
        }
        return i;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f4674P);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
        sb.append("LocationResult[locations: ");
        sb.append(valueOf);
        sb.append("]");
        return sb.toString();
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5614c(parcel, 1, mo18235d(), false);
        C2250b.m5587a(parcel, a);
    }
}
