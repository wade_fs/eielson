package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.MainThread;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.zzv;
import com.google.android.gms.measurement.internal.C3255y8;

/* renamed from: com.google.android.gms.measurement.internal.u8 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C3211u8<T extends Context & C3255y8> {

    /* renamed from: a */
    private final T f5688a;

    public C3211u8(T t) {
        C2258v.m5629a((Object) t);
        this.f5688a = t;
    }

    @MainThread
    /* renamed from: a */
    public final void mo19339a() {
        C3081j5 a = C3081j5.m8751a(this.f5688a, (zzv) null);
        C3032f4 l = a.mo19015l();
        a.mo19018r();
        l.mo18996B().mo19042a("Local AppMeasurementService is starting up");
    }

    @MainThread
    /* renamed from: b */
    public final void mo19343b() {
        C3081j5 a = C3081j5.m8751a(this.f5688a, (zzv) null);
        C3032f4 l = a.mo19015l();
        a.mo19018r();
        l.mo18996B().mo19042a("Local AppMeasurementService is shutting down");
    }

    @MainThread
    /* renamed from: c */
    public final void mo19345c(Intent intent) {
        if (intent == null) {
            m9242c().mo19001t().mo19042a("onRebind called with null intent");
            return;
        }
        m9242c().mo18996B().mo19043a("onRebind called. action", intent.getAction());
    }

    /* renamed from: c */
    private final C3032f4 m9242c() {
        return C3081j5.m8751a(this.f5688a, (zzv) null).mo19015l();
    }

    @MainThread
    /* renamed from: a */
    public final int mo19337a(Intent intent, int i, int i2) {
        C3081j5 a = C3081j5.m8751a(this.f5688a, (zzv) null);
        C3032f4 l = a.mo19015l();
        if (intent == null) {
            l.mo19004w().mo19042a("AppMeasurementService started with null intent");
            return 2;
        }
        String action = intent.getAction();
        a.mo19018r();
        l.mo18996B().mo19044a("Local AppMeasurementService called. startId, action", Integer.valueOf(i2), action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            m9241a(new C3200t8(this, i2, l, intent));
        }
        return 2;
    }

    @MainThread
    /* renamed from: b */
    public final boolean mo19344b(Intent intent) {
        if (intent == null) {
            m9242c().mo19001t().mo19042a("onUnbind called with null intent");
            return true;
        }
        m9242c().mo18996B().mo19043a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    /* renamed from: a */
    private final void m9241a(Runnable runnable) {
        C3145o9 a = C3145o9.m9047a((Context) this.f5688a);
        a.mo19014j().mo19028a(new C3222v8(this, a, runnable));
    }

    @MainThread
    /* renamed from: a */
    public final IBinder mo19338a(Intent intent) {
        if (intent == null) {
            m9242c().mo19001t().mo19042a("onBind called with null intent");
            return null;
        }
        String action = intent.getAction();
        if ("com.google.android.gms.measurement.START".equals(action)) {
            return new C3141o5(C3145o9.m9047a((Context) this.f5688a));
        }
        m9242c().mo19004w().mo19043a("onBind received unknown action", action);
        return null;
    }

    @TargetApi(24)
    @MainThread
    /* renamed from: a */
    public final boolean mo19342a(JobParameters jobParameters) {
        C3081j5 a = C3081j5.m8751a(this.f5688a, (zzv) null);
        C3032f4 l = a.mo19015l();
        String string = jobParameters.getExtras().getString(NativeProtocol.WEB_DIALOG_ACTION);
        a.mo19018r();
        l.mo18996B().mo19043a("Local AppMeasurementJobService called. action", string);
        if (!"com.google.android.gms.measurement.UPLOAD".equals(string)) {
            return true;
        }
        m9241a(new C3233w8(this, l, jobParameters));
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo19341a(C3032f4 f4Var, JobParameters jobParameters) {
        f4Var.mo18996B().mo19042a("AppMeasurementJobService processed last upload request.");
        ((C3255y8) this.f5688a).mo18704a(jobParameters, false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo19340a(int i, C3032f4 f4Var, Intent intent) {
        if (((C3255y8) this.f5688a).mo18706a(i)) {
            f4Var.mo18996B().mo19043a("Local AppMeasurementService processed last upload request. StartId", Integer.valueOf(i));
            m9242c().mo18996B().mo19042a("Completed wakeful intent.");
            ((C3255y8) this.f5688a).mo18705a(intent);
        }
    }
}
