package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.hc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2541hc implements C2593l2<C2524gc> {

    /* renamed from: Q */
    private static C2541hc f4221Q = new C2541hc();

    /* renamed from: P */
    private final C2593l2<C2524gc> f4222P;

    private C2541hc(C2593l2<C2524gc> l2Var) {
        this.f4222P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6461b() {
        return ((C2524gc) f4221Q.mo17285a()).mo17505a();
    }

    /* renamed from: c */
    public static boolean m6462c() {
        return ((C2524gc) f4221Q.mo17285a()).mo17506e();
    }

    /* renamed from: d */
    public static boolean m6463d() {
        return ((C2524gc) f4221Q.mo17285a()).mo17507f();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4222P.mo17285a();
    }

    public C2541hc() {
        this(C2579k2.m6601a(new C2575jc()));
    }
}
