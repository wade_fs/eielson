package com.google.android.gms.internal.measurement;

import com.google.android.exoplayer2.C1750C;
import com.google.android.gms.internal.measurement.C2414a1;
import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2701s0;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.w0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2763w0 extends C2595l4<C2763w0, C2764a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2763w0 zzav;
    private static volatile C2501f6<C2763w0> zzaw;
    private int zzaa;
    private String zzab = "";
    private String zzac = "";
    private boolean zzad;
    private C2738u4<C2671q0> zzae = C2595l4.m6649m();
    private String zzaf = "";
    private int zzag;
    private int zzah;
    private int zzai;
    private String zzaj = "";
    private long zzak;
    private long zzal;
    private String zzam = "";
    private String zzan = "";
    private int zzao;
    private String zzap = "";
    private C2778x0 zzaq;
    private C2706s4 zzar = C2595l4.m6647k();
    private long zzas;
    private long zzat;
    private String zzau = "";
    private int zzc;
    private int zzd;
    private int zze;
    private C2738u4<C2701s0> zzf = C2595l4.m6649m();
    private C2738u4<C2414a1> zzg = C2595l4.m6649m();
    private long zzh;
    private long zzi;
    private long zzj;
    private long zzk;
    private long zzl;
    private String zzm = "";
    private String zzn = "";
    private String zzo = "";
    private String zzp = "";
    private int zzq;
    private String zzr = "";
    private String zzs = "";
    private String zzt = "";
    private long zzu;
    private long zzv;
    private String zzw = "";
    private boolean zzx;
    private String zzy = "";
    private long zzz;

    /* renamed from: com.google.android.gms.internal.measurement.w0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2764a extends C2595l4.C2596a<C2763w0, C2764a> implements C2769w5 {
        private C2764a() {
            super(C2763w0.zzav);
        }

        /* renamed from: a */
        public final C2764a mo18065a(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7497d(1);
            return this;
        }

        /* renamed from: b */
        public final C2701s0 mo18075b(int i) {
            return ((C2763w0) super.f4288Q).mo18033b(i);
        }

        /* renamed from: c */
        public final C2764a mo18080c(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7504e(i);
            return this;
        }

        /* renamed from: d */
        public final C2414a1 mo18084d(int i) {
            return ((C2763w0) super.f4288Q).mo18035c(i);
        }

        /* renamed from: e */
        public final C2764a mo18087e(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7511f(i);
            return this;
        }

        /* renamed from: f */
        public final C2764a mo18090f(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7518g(i);
            return this;
        }

        /* renamed from: g */
        public final C2764a mo18095g(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7523g(str);
            return this;
        }

        /* renamed from: h */
        public final C2764a mo18098h(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7528h(str);
            return this;
        }

        /* renamed from: i */
        public final C2764a mo18101i(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7533i(str);
            return this;
        }

        /* renamed from: j */
        public final List<C2701s0> mo18104j() {
            return Collections.unmodifiableList(((C2763w0) super.f4288Q).mo18027V());
        }

        /* renamed from: k */
        public final int mo18105k() {
            return ((C2763w0) super.f4288Q).mo18028W();
        }

        /* renamed from: l */
        public final C2764a mo18108l() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7558v0();
            return this;
        }

        /* renamed from: m */
        public final List<C2414a1> mo18112m() {
            return Collections.unmodifiableList(((C2763w0) super.f4288Q).mo18029X());
        }

        /* renamed from: n */
        public final int mo18113n() {
            return ((C2763w0) super.f4288Q).mo18030Y();
        }

        /* renamed from: o */
        public final long mo18115o() {
            return ((C2763w0) super.f4288Q).mo18036c0();
        }

        /* renamed from: p */
        public final long mo18117p() {
            return ((C2763w0) super.f4288Q).mo18038e0();
        }

        /* renamed from: q */
        public final C2764a mo18119q() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7560x0();
            return this;
        }

        /* renamed from: s */
        public final C2764a mo18120s() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7561y0();
            return this;
        }

        /* renamed from: t */
        public final String mo18121t() {
            return ((C2763w0) super.f4288Q).mo18052p0();
        }

        /* renamed from: u */
        public final C2764a mo18122u() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7562z0();
            return this;
        }

        /* renamed from: v */
        public final String mo18123v() {
            return ((C2763w0) super.f4288Q).mo18007B();
        }

        /* renamed from: w */
        public final C2764a mo18124w() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7459A0();
            return this;
        }

        /* renamed from: x */
        public final C2764a mo18125x() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7460B0();
            return this;
        }

        /* renamed from: y */
        public final String mo18126y() {
            return ((C2763w0) super.f4288Q).mo18026U();
        }

        /* synthetic */ C2764a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: b */
        public final C2764a mo18076b(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7479b(j);
            return this;
        }

        /* renamed from: d */
        public final C2764a mo18085d(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7498d(j);
            return this;
        }

        /* renamed from: k */
        public final C2764a mo18107k(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7542k(str);
            return this;
        }

        /* renamed from: n */
        public final C2764a mo18114n(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7550n(null);
            return this;
        }

        /* renamed from: o */
        public final C2764a mo18116o(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7552o(str);
            return this;
        }

        /* renamed from: p */
        public final C2764a mo18118p(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7554p(str);
            return this;
        }

        /* renamed from: j */
        public final C2764a mo18103j(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7538j(str);
            return this;
        }

        /* renamed from: m */
        public final C2764a mo18111m(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7548m(str);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18067a(int i, C2701s0.C2702a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7462a(i, (C2701s0) super.mo17679i());
            return this;
        }

        /* renamed from: c */
        public final C2764a mo18081c(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7489c(j);
            return this;
        }

        /* renamed from: e */
        public final C2764a mo18088e(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7505e(j);
            return this;
        }

        /* renamed from: f */
        public final C2764a mo18092f(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7517f(str);
            return this;
        }

        /* renamed from: g */
        public final C2764a mo18094g(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7519g(j);
            return this;
        }

        /* renamed from: h */
        public final C2764a mo18097h(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7525h(j);
            return this;
        }

        /* renamed from: i */
        public final C2764a mo18100i(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7530i(j);
            return this;
        }

        /* renamed from: l */
        public final C2764a mo18110l(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7546l(str);
            return this;
        }

        /* renamed from: b */
        public final C2764a mo18078b(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7487b(str);
            return this;
        }

        /* renamed from: d */
        public final C2764a mo18086d(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7503d(str);
            return this;
        }

        /* renamed from: k */
        public final C2764a mo18106k(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7539k(j);
            return this;
        }

        /* renamed from: j */
        public final C2764a mo18102j(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7535j(j);
            return this;
        }

        /* renamed from: c */
        public final C2764a mo18083c(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7496c(str);
            return this;
        }

        /* renamed from: e */
        public final C2764a mo18089e(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7510e(str);
            return this;
        }

        /* renamed from: f */
        public final C2764a mo18091f(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7512f(j);
            return this;
        }

        /* renamed from: g */
        public final C2764a mo18093g(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7524h(i);
            return this;
        }

        /* renamed from: h */
        public final C2764a mo18096h(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7529i(i);
            return this;
        }

        /* renamed from: i */
        public final C2764a mo18099i(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7534j(i);
            return this;
        }

        /* renamed from: l */
        public final C2764a mo18109l(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7543l(j);
            return this;
        }

        /* renamed from: b */
        public final C2764a mo18079b(boolean z) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7488b(z);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18071a(C2701s0.C2702a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7465a((C2701s0) super.mo17679i());
            return this;
        }

        /* renamed from: c */
        public final C2764a mo18082c(Iterable<? extends Integer> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7495c(iterable);
            return this;
        }

        /* renamed from: b */
        public final C2764a mo18077b(Iterable<? extends C2671q0> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7486b(iterable);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18072a(Iterable<? extends C2701s0> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7476a(iterable);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18066a(int i, C2414a1 a1Var) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7461a(i, a1Var);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18070a(C2414a1 a1Var) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7464a(a1Var);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18069a(C2414a1.C2415a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7464a((C2414a1) super.mo17679i());
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18068a(long j) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7463a(j);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18073a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7477a(str);
            return this;
        }

        /* renamed from: a */
        public final C2764a mo18074a(boolean z) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2763w0) super.f4288Q).m7478a(z);
            return this;
        }
    }

    static {
        C2763w0 w0Var = new C2763w0();
        zzav = w0Var;
        C2595l4.m6645a(C2763w0.class, super);
    }

    private C2763w0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: A0 */
    public final void m7459A0() {
        this.zzae = C2595l4.m6649m();
    }

    /* access modifiers changed from: private */
    /* renamed from: B0 */
    public final void m7460B0() {
        this.zzc &= Integer.MAX_VALUE;
        this.zzam = zzav.zzam;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7462a(int i, C2701s0 s0Var) {
        s0Var.getClass();
        m7557u0();
        this.zzf.set(i, s0Var);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final void m7497d(int i) {
        this.zzc |= 1;
        this.zze = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public final void m7504e(int i) {
        m7557u0();
        this.zzf.remove(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public final void m7511f(int i) {
        m7559w0();
        this.zzg.remove(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public final void m7518g(int i) {
        this.zzc |= 1024;
        this.zzq = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public final void m7528h(String str) {
        str.getClass();
        this.zzc |= 65536;
        this.zzw = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final void m7533i(String str) {
        str.getClass();
        this.zzc |= 262144;
        this.zzy = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public final void m7538j(String str) {
        str.getClass();
        this.zzc |= 2097152;
        this.zzab = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public final void m7542k(String str) {
        str.getClass();
        this.zzc |= 4194304;
        this.zzac = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public final void m7546l(String str) {
        str.getClass();
        this.zzc |= 16777216;
        this.zzaf = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public final void m7548m(String str) {
        str.getClass();
        this.zzc |= C1750C.ENCODING_PCM_MU_LAW;
        this.zzaj = str;
    }

    /* renamed from: s0 */
    public static C2764a m7555s0() {
        return (C2764a) zzav.mo17668i();
    }

    /* renamed from: u0 */
    private final void m7557u0() {
        if (!this.zzf.mo17938a()) {
            this.zzf = C2595l4.m6642a(this.zzf);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: v0 */
    public final void m7558v0() {
        this.zzf = C2595l4.m6649m();
    }

    /* renamed from: w0 */
    private final void m7559w0() {
        if (!this.zzg.mo17938a()) {
            this.zzg = C2595l4.m6642a(this.zzg);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: x0 */
    public final void m7560x0() {
        this.zzc &= -17;
        this.zzk = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: y0 */
    public final void m7561y0() {
        this.zzc &= -33;
        this.zzl = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: z0 */
    public final void m7562z0() {
        this.zzc &= -2097153;
        this.zzab = zzav.zzab;
    }

    /* renamed from: A */
    public final String mo18006A() {
        return this.zzab;
    }

    /* renamed from: B */
    public final String mo18007B() {
        return this.zzac;
    }

    /* renamed from: C */
    public final boolean mo18008C() {
        return (this.zzc & 8388608) != 0;
    }

    /* renamed from: D */
    public final boolean mo18009D() {
        return this.zzad;
    }

    /* renamed from: E */
    public final List<C2671q0> mo18010E() {
        return this.zzae;
    }

    /* renamed from: F */
    public final String mo18011F() {
        return this.zzaf;
    }

    /* renamed from: G */
    public final boolean mo18012G() {
        return (this.zzc & 33554432) != 0;
    }

    /* renamed from: H */
    public final int mo18013H() {
        return this.zzag;
    }

    /* renamed from: I */
    public final String mo18014I() {
        return this.zzaj;
    }

    /* renamed from: J */
    public final boolean mo18015J() {
        return (this.zzc & C1750C.ENCODING_PCM_A_LAW) != 0;
    }

    /* renamed from: K */
    public final long mo18016K() {
        return this.zzak;
    }

    /* renamed from: L */
    public final boolean mo18017L() {
        return (this.zzc & 1073741824) != 0;
    }

    /* renamed from: M */
    public final long mo18018M() {
        return this.zzal;
    }

    /* renamed from: N */
    public final String mo18019N() {
        return this.zzam;
    }

    /* renamed from: O */
    public final boolean mo18020O() {
        return (this.zzd & 2) != 0;
    }

    /* renamed from: P */
    public final int mo18021P() {
        return this.zze;
    }

    /* renamed from: Q */
    public final int mo18022Q() {
        return this.zzao;
    }

    /* renamed from: R */
    public final String mo18023R() {
        return this.zzap;
    }

    /* renamed from: S */
    public final boolean mo18024S() {
        return (this.zzd & 16) != 0;
    }

    /* renamed from: T */
    public final long mo18025T() {
        return this.zzas;
    }

    /* renamed from: U */
    public final String mo18026U() {
        return this.zzau;
    }

    /* renamed from: V */
    public final List<C2701s0> mo18027V() {
        return this.zzf;
    }

    /* renamed from: W */
    public final int mo18028W() {
        return this.zzf.size();
    }

    /* renamed from: X */
    public final List<C2414a1> mo18029X() {
        return this.zzg;
    }

    /* renamed from: Y */
    public final int mo18030Y() {
        return this.zzg.size();
    }

    /* renamed from: Z */
    public final boolean mo18031Z() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: a0 */
    public final long mo18032a0() {
        return this.zzh;
    }

    /* renamed from: b */
    public final C2701s0 mo18033b(int i) {
        return this.zzf.get(i);
    }

    /* renamed from: b0 */
    public final boolean mo18034b0() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: c */
    public final C2414a1 mo18035c(int i) {
        return this.zzg.get(i);
    }

    /* renamed from: c0 */
    public final long mo18036c0() {
        return this.zzi;
    }

    /* renamed from: d0 */
    public final boolean mo18037d0() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: e0 */
    public final long mo18038e0() {
        return this.zzj;
    }

    /* renamed from: f0 */
    public final boolean mo18039f0() {
        return (this.zzc & 16) != 0;
    }

    /* renamed from: g0 */
    public final long mo18040g0() {
        return this.zzk;
    }

    /* renamed from: h0 */
    public final boolean mo18041h0() {
        return (this.zzc & 32) != 0;
    }

    /* renamed from: i0 */
    public final long mo18042i0() {
        return this.zzl;
    }

    /* renamed from: j0 */
    public final String mo18043j0() {
        return this.zzm;
    }

    /* renamed from: k0 */
    public final String mo18044k0() {
        return this.zzn;
    }

    /* renamed from: l0 */
    public final String mo18045l0() {
        return this.zzo;
    }

    /* renamed from: m0 */
    public final String mo18046m0() {
        return this.zzp;
    }

    /* renamed from: n */
    public final boolean mo18047n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: n0 */
    public final int mo18048n0() {
        return this.zzq;
    }

    /* renamed from: o */
    public final long mo18049o() {
        return this.zzu;
    }

    /* renamed from: o0 */
    public final String mo18050o0() {
        return this.zzr;
    }

    /* renamed from: p */
    public final boolean mo18051p() {
        return (this.zzc & 32768) != 0;
    }

    /* renamed from: p0 */
    public final String mo18052p0() {
        return this.zzs;
    }

    /* renamed from: q */
    public final long mo18053q() {
        return this.zzv;
    }

    /* renamed from: q0 */
    public final String mo18054q0() {
        return this.zzt;
    }

    /* renamed from: r */
    public final boolean mo18055r() {
        return (this.zzc & 1024) != 0;
    }

    /* renamed from: r0 */
    public final boolean mo18056r0() {
        return (this.zzc & 16384) != 0;
    }

    /* renamed from: s */
    public final String mo18057s() {
        return this.zzw;
    }

    /* renamed from: t */
    public final boolean mo18058t() {
        return (this.zzc & 131072) != 0;
    }

    /* renamed from: u */
    public final boolean mo18059u() {
        return this.zzx;
    }

    /* renamed from: v */
    public final String mo18060v() {
        return this.zzy;
    }

    /* renamed from: w */
    public final boolean mo18061w() {
        return (this.zzc & 524288) != 0;
    }

    /* renamed from: x */
    public final long mo18062x() {
        return this.zzz;
    }

    /* renamed from: y */
    public final boolean mo18063y() {
        return (this.zzc & 1048576) != 0;
    }

    /* renamed from: z */
    public final int mo18064z() {
        return this.zzaa;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7479b(long j) {
        this.zzc |= 4;
        this.zzi = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m7489c(long j) {
        this.zzc |= 8;
        this.zzj = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public final void m7550n(String str) {
        str.getClass();
        this.zzc |= Integer.MIN_VALUE;
        this.zzam = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: o */
    public final void m7552o(String str) {
        str.getClass();
        this.zzd |= 4;
        this.zzap = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: p */
    public final void m7554p(String str) {
        str.getClass();
        this.zzd |= 64;
        this.zzau = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final void m7498d(long j) {
        this.zzc |= 16;
        this.zzk = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public final void m7505e(long j) {
        this.zzc |= 32;
        this.zzl = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public final void m7517f(String str) {
        str.getClass();
        this.zzc |= 4096;
        this.zzs = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public final void m7523g(String str) {
        str.getClass();
        this.zzc |= 8192;
        this.zzt = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7465a(C2701s0 s0Var) {
        s0Var.getClass();
        m7557u0();
        this.zzf.add(s0Var);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7487b(String str) {
        str.getClass();
        this.zzc |= 128;
        this.zzn = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m7496c(String str) {
        str.getClass();
        this.zzc |= 256;
        this.zzo = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public final void m7525h(long j) {
        this.zzc |= 524288;
        this.zzz = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final void m7529i(int i) {
        this.zzc |= 33554432;
        this.zzag = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public final void m7535j(long j) {
        this.zzc |= 1073741824;
        this.zzal = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public final void m7539k(long j) {
        this.zzd |= 16;
        this.zzas = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public final void m7543l(long j) {
        this.zzd |= 32;
        this.zzat = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final void m7503d(String str) {
        str.getClass();
        this.zzc |= 512;
        this.zzp = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public final void m7510e(String str) {
        str.getClass();
        this.zzc |= 2048;
        this.zzr = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public final void m7512f(long j) {
        this.zzc |= 16384;
        this.zzu = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public final void m7519g(long j) {
        this.zzc |= 32768;
        this.zzv = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public final void m7524h(int i) {
        this.zzc |= 1048576;
        this.zzaa = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final void m7530i(long j) {
        this.zzc |= C1750C.ENCODING_PCM_A_LAW;
        this.zzak = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public final void m7534j(int i) {
        this.zzd |= 2;
        this.zzao = i;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7476a(Iterable<? extends C2701s0> iterable) {
        m7557u0();
        C2766w2.m7700a(iterable, this.zzf);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7488b(boolean z) {
        this.zzc |= 8388608;
        this.zzad = z;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m7495c(Iterable<? extends Integer> iterable) {
        if (!this.zzar.mo17938a()) {
            C2706s4 s4Var = this.zzar;
            int size = s4Var.size();
            this.zzar = s4Var.mo17732b(size == 0 ? 10 : size << 1);
        }
        C2766w2.m7700a(iterable, this.zzar);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7461a(int i, C2414a1 a1Var) {
        a1Var.getClass();
        m7559w0();
        this.zzg.set(i, a1Var);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7486b(Iterable<? extends C2671q0> iterable) {
        if (!this.zzae.mo17938a()) {
            this.zzae = C2595l4.m6642a(this.zzae);
        }
        C2766w2.m7700a(iterable, this.zzae);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7464a(C2414a1 a1Var) {
        a1Var.getClass();
        m7559w0();
        this.zzg.add(a1Var);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7463a(long j) {
        this.zzc |= 2;
        this.zzh = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7477a(String str) {
        str.getClass();
        this.zzc |= 64;
        this.zzm = str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7478a(boolean z) {
        this.zzc |= 131072;
        this.zzx = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2763w0();
            case 2:
                return new C2764a(null);
            case 3:
                return C2595l4.m6643a(zzav, "\u0001+\u0000\u0002\u00012+\u0000\u0004\u0000\u0001\u0004\u0000\u0002\u001b\u0003\u001b\u0004\u0002\u0001\u0005\u0002\u0002\u0006\u0002\u0003\u0007\u0002\u0005\b\b\u0006\t\b\u0007\n\b\b\u000b\b\t\f\u0004\n\r\b\u000b\u000e\b\f\u0010\b\r\u0011\u0002\u000e\u0012\u0002\u000f\u0013\b\u0010\u0014\u0007\u0011\u0015\b\u0012\u0016\u0002\u0013\u0017\u0004\u0014\u0018\b\u0015\u0019\b\u0016\u001a\u0002\u0004\u001c\u0007\u0017\u001d\u001b\u001e\b\u0018\u001f\u0004\u0019 \u0004\u001a!\u0004\u001b\"\b\u001c#\u0002\u001d$\u0002\u001e%\b\u001f&\b '\u0004!)\b\",\t#-\u001d.\u0002$/\u0002%2\b&", new Object[]{"zzc", "zzd", "zze", "zzf", C2701s0.class, "zzg", C2414a1.class, "zzh", "zzi", "zzj", "zzl", "zzm", "zzn", "zzo", "zzp", "zzq", "zzr", "zzs", "zzt", "zzu", "zzv", "zzw", "zzx", "zzy", "zzz", "zzaa", "zzab", "zzac", "zzk", "zzad", "zzae", C2671q0.class, "zzaf", "zzag", "zzah", "zzai", "zzaj", "zzak", "zzal", "zzam", "zzan", "zzao", "zzap", "zzaq", "zzar", "zzas", "zzat", "zzau"});
            case 4:
                return zzav;
            case 5:
                C2501f6<C2763w0> f6Var = zzaw;
                if (f6Var == null) {
                    synchronized (C2763w0.class) {
                        f6Var = zzaw;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzav);
                            zzaw = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
