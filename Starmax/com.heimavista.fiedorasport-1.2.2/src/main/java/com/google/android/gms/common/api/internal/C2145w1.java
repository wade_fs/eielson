package com.google.android.gms.common.api.internal;

/* renamed from: com.google.android.gms.common.api.internal.w1 */
final class C2145w1 implements C2152y1 {

    /* renamed from: a */
    private final /* synthetic */ C2141v1 f3498a;

    C2145w1(C2141v1 v1Var) {
        this.f3498a = v1Var;
    }

    /* renamed from: a */
    public final void mo16795a(BasePendingResult<?> basePendingResult) {
        this.f3498a.f3489a.remove(basePendingResult);
    }
}
