package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.qb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2683qb implements C2593l2<C2730tb> {

    /* renamed from: Q */
    private static C2683qb f4453Q = new C2683qb();

    /* renamed from: P */
    private final C2593l2<C2730tb> f4454P;

    private C2683qb(C2593l2<C2730tb> l2Var) {
        this.f4454P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7093b() {
        return ((C2730tb) f4453Q.mo17285a()).mo17876a();
    }

    /* renamed from: c */
    public static double m7094c() {
        return ((C2730tb) f4453Q.mo17285a()).mo17877e();
    }

    /* renamed from: d */
    public static long m7095d() {
        return ((C2730tb) f4453Q.mo17285a()).mo17878f();
    }

    /* renamed from: e */
    public static long m7096e() {
        return ((C2730tb) f4453Q.mo17285a()).mo17879g();
    }

    /* renamed from: f */
    public static String m7097f() {
        return ((C2730tb) f4453Q.mo17285a()).mo17880t();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4454P.mo17285a();
    }

    public C2683qb() {
        this(C2579k2.m6601a(new C2713sb()));
    }
}
