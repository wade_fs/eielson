package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.e */
/* compiled from: lambda */
public final /* synthetic */ class C1876e implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2884P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2885Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSourceEventListener.LoadEventInfo f2886R;

    /* renamed from: S */
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData f2887S;

    public /* synthetic */ C1876e(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f2884P = eventDispatcher;
        this.f2885Q = mediaSourceEventListener;
        this.f2886R = loadEventInfo;
        this.f2887S = mediaLoadData;
    }

    public final void run() {
        this.f2884P.mo14931a(this.f2885Q, this.f2886R, this.f2887S);
    }
}
