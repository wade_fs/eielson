package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Base64;
import androidx.annotation.RequiresApi;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.C3911q;
import p119e.p144d.p145a.p146a.p147i.p154y.C3977a;

@RequiresApi(api = 21)
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class JobInfoSchedulerService extends JobService {
    public boolean onStartJob(JobParameters jobParameters) {
        String string = jobParameters.getExtras().getString("backendName");
        String string2 = jobParameters.getExtras().getString("extras");
        int i = jobParameters.getExtras().getInt("priority");
        int i2 = jobParameters.getExtras().getInt("attemptNumber");
        C3911q.m11780a(getApplicationContext());
        C3905l.C3906a d = C3905l.m11763d();
        d.mo23537a(string);
        d.mo23536a(C3977a.m11940a(i));
        if (string2 != null) {
            d.mo23538a(Base64.decode(string2, 0));
        }
        C3911q.m11781b().mo23562a().mo13589a(d.mo23539a(), i2, C1732f.m4323a(this, jobParameters));
        return true;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }
}
