package com.google.android.gms.auth.api;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.auth.api.C1951a;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.internal.p092authapi.C2360e;

/* renamed from: com.google.android.gms.auth.api.e */
final class C1970e extends C2016a.C2017a<C2360e, C1951a.C1952a> {
    C1970e() {
    }

    /* renamed from: a */
    public final /* synthetic */ C2016a.C2027f mo16371a(Context context, Looper looper, C2211e eVar, Object obj, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        return new C2360e(context, looper, eVar, (C1951a.C1952a) obj, bVar, cVar);
    }
}
