package com.google.android.gms.location;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.internal.location.C2402q;
import com.google.android.gms.internal.location.C2409x;

/* renamed from: com.google.android.gms.location.j0 */
public abstract class C2844j0 extends C2402q implements C2842i0 {
    public C2844j0() {
        super("com.google.android.gms.location.ILocationCallback");
    }

    /* renamed from: a */
    public static C2842i0 m7982a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationCallback");
        return queryLocalInterface instanceof C2842i0 ? (C2842i0) queryLocalInterface : new C2846k0(iBinder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo17203a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i == 1) {
            mo17214a((LocationResult) C2409x.m5938a(parcel, LocationResult.CREATOR));
        } else if (i != 2) {
            return false;
        } else {
            mo17213a((LocationAvailability) C2409x.m5938a(parcel, LocationAvailability.CREATOR));
        }
        return true;
    }
}
