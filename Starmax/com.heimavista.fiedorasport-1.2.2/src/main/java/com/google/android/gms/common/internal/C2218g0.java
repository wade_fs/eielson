package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.base.C2375a;
import com.google.android.gms.internal.base.C2377c;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.common.internal.g0 */
public final class C2218g0 extends C2375a implements C2244r {
    C2218g0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ISignInButtonCreator");
    }

    /* renamed from: a */
    public final C3988b mo16975a(C3988b bVar, SignInButtonConfig signInButtonConfig) {
        Parcel H = mo17182H();
        C2377c.m5899a(H, bVar);
        C2377c.m5900a(H, signInButtonConfig);
        Parcel a = mo17183a(2, H);
        C3988b a2 = C3988b.C3989a.m11981a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
