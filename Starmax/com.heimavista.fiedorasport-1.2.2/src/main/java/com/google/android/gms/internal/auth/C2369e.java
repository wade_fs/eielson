package com.google.android.gms.internal.auth;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.internal.auth.e */
public final class C2369e implements Parcelable.Creator<zzah> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        String str = null;
        int i = 0;
        PendingIntent pendingIntent = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                pendingIntent = (PendingIntent) C2248a.m5553a(parcel, a, PendingIntent.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzah(i, str, pendingIntent);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzah[i];
    }
}
