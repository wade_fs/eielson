package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class zzm extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzm> CREATOR = new C3014da();

    /* renamed from: P */
    public final String f5814P;

    /* renamed from: Q */
    public final String f5815Q;

    /* renamed from: R */
    public final String f5816R;

    /* renamed from: S */
    public final String f5817S;

    /* renamed from: T */
    public final long f5818T;

    /* renamed from: U */
    public final long f5819U;

    /* renamed from: V */
    public final String f5820V;

    /* renamed from: W */
    public final boolean f5821W;

    /* renamed from: X */
    public final boolean f5822X;

    /* renamed from: Y */
    public final long f5823Y;

    /* renamed from: Z */
    public final String f5824Z;

    /* renamed from: a0 */
    public final long f5825a0;

    /* renamed from: b0 */
    public final long f5826b0;

    /* renamed from: c0 */
    public final int f5827c0;

    /* renamed from: d0 */
    public final boolean f5828d0;

    /* renamed from: e0 */
    public final boolean f5829e0;

    /* renamed from: f0 */
    public final boolean f5830f0;

    /* renamed from: g0 */
    public final String f5831g0;

    /* renamed from: h0 */
    public final Boolean f5832h0;

    /* renamed from: i0 */
    public final long f5833i0;

    /* renamed from: j0 */
    public final List<String> f5834j0;

    /* renamed from: k0 */
    public final String f5835k0;

    zzm(String str, String str2, String str3, long j, String str4, long j2, long j3, String str5, boolean z, boolean z2, String str6, long j4, long j5, int i, boolean z3, boolean z4, boolean z5, String str7, Boolean bool, long j6, List<String> list, String str8) {
        C2258v.m5639b(str);
        this.f5814P = str;
        this.f5815Q = TextUtils.isEmpty(str2) ? null : str2;
        this.f5816R = str3;
        this.f5823Y = j;
        this.f5817S = str4;
        this.f5818T = j2;
        this.f5819U = j3;
        this.f5820V = str5;
        this.f5821W = z;
        this.f5822X = z2;
        this.f5824Z = str6;
        this.f5825a0 = j4;
        this.f5826b0 = j5;
        this.f5827c0 = i;
        this.f5828d0 = z3;
        this.f5829e0 = z4;
        this.f5830f0 = z5;
        this.f5831g0 = str7;
        this.f5832h0 = bool;
        this.f5833i0 = j6;
        this.f5834j0 = list;
        this.f5835k0 = str8;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
     arg types: [android.os.Parcel, int, java.lang.Boolean, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5602a(parcel, 2, this.f5814P, false);
        C2250b.m5602a(parcel, 3, this.f5815Q, false);
        C2250b.m5602a(parcel, 4, this.f5816R, false);
        C2250b.m5602a(parcel, 5, this.f5817S, false);
        C2250b.m5592a(parcel, 6, this.f5818T);
        C2250b.m5592a(parcel, 7, this.f5819U);
        C2250b.m5602a(parcel, 8, this.f5820V, false);
        C2250b.m5605a(parcel, 9, this.f5821W);
        C2250b.m5605a(parcel, 10, this.f5822X);
        C2250b.m5592a(parcel, 11, this.f5823Y);
        C2250b.m5602a(parcel, 12, this.f5824Z, false);
        C2250b.m5592a(parcel, 13, this.f5825a0);
        C2250b.m5592a(parcel, 14, this.f5826b0);
        C2250b.m5591a(parcel, 15, this.f5827c0);
        C2250b.m5605a(parcel, 16, this.f5828d0);
        C2250b.m5605a(parcel, 17, this.f5829e0);
        C2250b.m5605a(parcel, 18, this.f5830f0);
        C2250b.m5602a(parcel, 19, this.f5831g0, false);
        C2250b.m5597a(parcel, 21, this.f5832h0, false);
        C2250b.m5592a(parcel, 22, this.f5833i0);
        C2250b.m5612b(parcel, 23, this.f5834j0, false);
        C2250b.m5602a(parcel, 24, this.f5835k0, false);
        C2250b.m5587a(parcel, a);
    }

    zzm(String str, String str2, String str3, String str4, long j, long j2, String str5, boolean z, boolean z2, long j3, String str6, long j4, long j5, int i, boolean z3, boolean z4, boolean z5, String str7, Boolean bool, long j6, List<String> list, String str8) {
        this.f5814P = str;
        this.f5815Q = str2;
        this.f5816R = str3;
        this.f5823Y = j3;
        this.f5817S = str4;
        this.f5818T = j;
        this.f5819U = j2;
        this.f5820V = str5;
        this.f5821W = z;
        this.f5822X = z2;
        this.f5824Z = str6;
        this.f5825a0 = j4;
        this.f5826b0 = j5;
        this.f5827c0 = i;
        this.f5828d0 = z3;
        this.f5829e0 = z4;
        this.f5830f0 = z5;
        this.f5831g0 = str7;
        this.f5832h0 = bool;
        this.f5833i0 = j6;
        this.f5834j0 = list;
        this.f5835k0 = str8;
    }
}
