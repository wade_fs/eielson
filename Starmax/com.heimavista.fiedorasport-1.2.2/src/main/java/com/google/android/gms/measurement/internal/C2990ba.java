package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.ba */
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
final class C2990ba implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2589kc f4992P;

    /* renamed from: Q */
    private final /* synthetic */ AppMeasurementDynamiteService f4993Q;

    C2990ba(AppMeasurementDynamiteService appMeasurementDynamiteService, C2589kc kcVar) {
        this.f4993Q = appMeasurementDynamiteService;
        this.f4992P = kcVar;
    }

    public final void run() {
        this.f4993Q.f4930a.mo19104w().mo19439a(this.f4992P, this.f4993Q.f4930a.mo19088b());
    }
}
