package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.vc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2761vc implements C2593l2<C2808yc> {

    /* renamed from: Q */
    private static C2761vc f4543Q = new C2761vc();

    /* renamed from: P */
    private final C2593l2<C2808yc> f4544P;

    private C2761vc(C2593l2<C2808yc> l2Var) {
        this.f4544P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7456b() {
        return ((C2808yc) f4543Q.mo17285a()).mo18145a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4544P.mo17285a();
    }

    public C2761vc() {
        this(C2579k2.m6601a(new C2792xc()));
    }
}
