package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2751v2;
import com.google.android.gms.internal.measurement.C2766w2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.w2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2766w2<MessageType extends C2766w2<MessageType, BuilderType>, BuilderType extends C2751v2<MessageType, BuilderType>> implements C2739u5 {
    protected int zza = 0;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo17659a(int i) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: c */
    public final C2498f3 mo17940c() {
        try {
            C2630n3 c = C2498f3.m6306c(mo17663e());
            mo17660a(c.mo17777b());
            return c.mo17776a();
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "ByteString".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("ByteString");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    /* renamed from: f */
    public final byte[] mo18129f() {
        try {
            byte[] bArr = new byte[mo17663e()];
            C2720t3 a = C2720t3.m7219a(bArr);
            mo17660a(a);
            a.mo17900b();
            return bArr;
        } catch (IOException e) {
            String name = getClass().getName();
            StringBuilder sb = new StringBuilder(String.valueOf(name).length() + 62 + "byte array".length());
            sb.append("Serializing ");
            sb.append(name);
            sb.append(" to a ");
            sb.append("byte array");
            sb.append(" threw an IOException (should never happen).");
            throw new RuntimeException(sb.toString(), e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public int mo17665g() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    protected static <T> void m7700a(Iterable iterable, List list) {
        C2647o4.m6961a(iterable);
        if (iterable instanceof C2484e5) {
            List<?> g = ((C2484e5) iterable).mo17327g();
            C2484e5 e5Var = (C2484e5) list;
            int size = list.size();
            for (Object obj : g) {
                if (obj == null) {
                    StringBuilder sb = new StringBuilder(37);
                    sb.append("Element at index ");
                    sb.append(e5Var.size() - size);
                    sb.append(" is null.");
                    String sb2 = sb.toString();
                    for (int size2 = e5Var.size() - 1; size2 >= size; size2--) {
                        e5Var.remove(size2);
                    }
                    throw new NullPointerException(sb2);
                } else if (obj instanceof C2498f3) {
                    e5Var.mo17321a((C2498f3) obj);
                } else {
                    e5Var.add((String) obj);
                }
            }
        } else if (iterable instanceof C2485e6) {
            list.addAll((Collection) iterable);
        } else {
            if ((list instanceof ArrayList) && (iterable instanceof Collection)) {
                ((ArrayList) list).ensureCapacity(list.size() + ((Collection) iterable).size());
            }
            int size3 = list.size();
            for (Object obj2 : iterable) {
                if (obj2 == null) {
                    StringBuilder sb3 = new StringBuilder(37);
                    sb3.append("Element at index ");
                    sb3.append(list.size() - size3);
                    sb3.append(" is null.");
                    String sb4 = sb3.toString();
                    for (int size4 = list.size() - 1; size4 >= size3; size4--) {
                        list.remove(size4);
                    }
                    throw new NullPointerException(sb4);
                }
                list.add(obj2);
            }
        }
    }
}
