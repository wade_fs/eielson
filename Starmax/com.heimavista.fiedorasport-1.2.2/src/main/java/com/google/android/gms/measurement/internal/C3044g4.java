package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.internal.C2197d;

/* renamed from: com.google.android.gms.measurement.internal.g4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3044g4 extends C2197d<C3228w3> {
    public C3044g4(Context context, Looper looper, C2197d.C2198a aVar, C2197d.C2199b bVar) {
        super(context, looper, 93, aVar, bVar, null);
    }

    /* renamed from: a */
    public final /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.measurement.internal.IMeasurementService");
        if (queryLocalInterface instanceof C3228w3) {
            return (C3228w3) queryLocalInterface;
        }
        return new C3250y3(iBinder);
    }

    /* renamed from: i */
    public final int mo16451i() {
        return C2176f.f3566a;
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: y */
    public final String mo16453y() {
        return "com.google.android.gms.measurement.internal.IMeasurementService";
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: z */
    public final String mo16454z() {
        return "com.google.android.gms.measurement.START";
    }
}
