package com.google.android.gms.auth.api.signin.internal;

import android.os.Parcel;
import com.google.android.gms.internal.p092authapi.C2357b;

/* renamed from: com.google.android.gms.auth.api.signin.internal.p */
public abstract class C2003p extends C2357b implements C2002o {
    public C2003p() {
        super("com.google.android.gms.auth.api.signin.internal.IRevocationService");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo16461a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i == 1) {
            mo16459k();
        } else if (i != 2) {
            return false;
        } else {
            mo16460m();
        }
        return true;
    }
}
