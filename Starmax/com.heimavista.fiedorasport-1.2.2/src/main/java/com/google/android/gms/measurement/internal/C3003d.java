package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2429b0;
import com.google.android.gms.internal.measurement.C2445c0;
import com.google.android.gms.internal.measurement.C2461d0;
import com.google.android.gms.internal.measurement.C2489ea;
import com.google.android.gms.internal.measurement.C2494f0;
import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2701s0;
import com.google.android.gms.internal.measurement.C2733u0;
import com.google.android.gms.internal.measurement.C2763w0;
import com.google.android.gms.internal.measurement.C2774wa;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.d */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3003d extends C3157p9 {
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static final String[] f5027f = {"last_bundled_timestamp", "ALTER TABLE events ADD COLUMN last_bundled_timestamp INTEGER;", "last_bundled_day", "ALTER TABLE events ADD COLUMN last_bundled_day INTEGER;", "last_sampled_complex_event_id", "ALTER TABLE events ADD COLUMN last_sampled_complex_event_id INTEGER;", "last_sampling_rate", "ALTER TABLE events ADD COLUMN last_sampling_rate INTEGER;", "last_exempt_from_sampling", "ALTER TABLE events ADD COLUMN last_exempt_from_sampling INTEGER;", "current_session_count", "ALTER TABLE events ADD COLUMN current_session_count INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static final String[] f5028g = {TtmlNode.ATTR_TTS_ORIGIN, "ALTER TABLE user_attributes ADD COLUMN origin TEXT;"};
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static final String[] f5029h = {"app_version", "ALTER TABLE apps ADD COLUMN app_version TEXT;", "app_store", "ALTER TABLE apps ADD COLUMN app_store TEXT;", "gmp_version", "ALTER TABLE apps ADD COLUMN gmp_version INTEGER;", "dev_cert_hash", "ALTER TABLE apps ADD COLUMN dev_cert_hash INTEGER;", "measurement_enabled", "ALTER TABLE apps ADD COLUMN measurement_enabled INTEGER;", "last_bundle_start_timestamp", "ALTER TABLE apps ADD COLUMN last_bundle_start_timestamp INTEGER;", "day", "ALTER TABLE apps ADD COLUMN day INTEGER;", "daily_public_events_count", "ALTER TABLE apps ADD COLUMN daily_public_events_count INTEGER;", "daily_events_count", "ALTER TABLE apps ADD COLUMN daily_events_count INTEGER;", "daily_conversions_count", "ALTER TABLE apps ADD COLUMN daily_conversions_count INTEGER;", "remote_config", "ALTER TABLE apps ADD COLUMN remote_config BLOB;", "config_fetched_time", "ALTER TABLE apps ADD COLUMN config_fetched_time INTEGER;", "failed_config_fetch_time", "ALTER TABLE apps ADD COLUMN failed_config_fetch_time INTEGER;", "app_version_int", "ALTER TABLE apps ADD COLUMN app_version_int INTEGER;", "firebase_instance_id", "ALTER TABLE apps ADD COLUMN firebase_instance_id TEXT;", "daily_error_events_count", "ALTER TABLE apps ADD COLUMN daily_error_events_count INTEGER;", "daily_realtime_events_count", "ALTER TABLE apps ADD COLUMN daily_realtime_events_count INTEGER;", "health_monitor_sample", "ALTER TABLE apps ADD COLUMN health_monitor_sample TEXT;", "android_id", "ALTER TABLE apps ADD COLUMN android_id INTEGER;", "adid_reporting_enabled", "ALTER TABLE apps ADD COLUMN adid_reporting_enabled INTEGER;", "ssaid_reporting_enabled", "ALTER TABLE apps ADD COLUMN ssaid_reporting_enabled INTEGER;", "admob_app_id", "ALTER TABLE apps ADD COLUMN admob_app_id TEXT;", "linked_admob_app_id", "ALTER TABLE apps ADD COLUMN linked_admob_app_id TEXT;", "dynamite_version", "ALTER TABLE apps ADD COLUMN dynamite_version INTEGER;", "safelisted_events", "ALTER TABLE apps ADD COLUMN safelisted_events TEXT;", "ga_app_id", "ALTER TABLE apps ADD COLUMN ga_app_id TEXT;"};
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static final String[] f5030i = {"realtime", "ALTER TABLE raw_events ADD COLUMN realtime INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static final String[] f5031j = {"has_realtime", "ALTER TABLE queue ADD COLUMN has_realtime INTEGER;", "retry_count", "ALTER TABLE queue ADD COLUMN retry_count INTEGER;"};
    /* access modifiers changed from: private */

    /* renamed from: k */
    public static final String[] f5032k = {"session_scoped", "ALTER TABLE event_filters ADD COLUMN session_scoped BOOLEAN;"};
    /* access modifiers changed from: private */

    /* renamed from: l */
    public static final String[] f5033l = {"session_scoped", "ALTER TABLE property_filters ADD COLUMN session_scoped BOOLEAN;"};
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static final String[] f5034m = {"previous_install_count", "ALTER TABLE app2 ADD COLUMN previous_install_count INTEGER;"};

    /* renamed from: d */
    private final C3015e f5035d = new C3015e(this, mo19016n(), "google_app_measurement.db");
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final C3109l9 f5036e = new C3109l9(mo19017o());

    C3003d(C3145o9 o9Var) {
        super(o9Var);
    }

    /* renamed from: O */
    private final boolean m8445O() {
        return mo19016n().getDatabasePath("google_app_measurement.db").exists();
    }

    @WorkerThread
    /* renamed from: a */
    private final long m8446a(String str, String[] strArr, long j) {
        Cursor cursor = null;
        try {
            Cursor rawQuery = mo18875v().rawQuery(str, strArr);
            if (rawQuery.moveToFirst()) {
                long j2 = rawQuery.getLong(0);
                if (rawQuery != null) {
                    rawQuery.close();
                }
                return j2;
            }
            if (rawQuery != null) {
                rawQuery.close();
            }
            return j;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    /* renamed from: b */
    private final long m8452b(String str, String[] strArr) {
        Cursor cursor = null;
        try {
            cursor = mo18875v().rawQuery(str, strArr);
            if (cursor.moveToFirst()) {
                long j = cursor.getLong(0);
                if (cursor != null) {
                    cursor.close();
                }
                return j;
            }
            throw new SQLiteException("Database returned empty set");
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Database error", str, e);
            throw e;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* renamed from: A */
    public final boolean mo18834A() {
        return m8452b("select count(1) > 0 from queue where has_realtime = 1", null) != 0;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: B */
    public final void mo18835B() {
        int delete;
        mo18881c();
        mo19284q();
        if (m8445O()) {
            long a = mo19012g().f5610h.mo19326a();
            long elapsedRealtime = mo19017o().elapsedRealtime();
            if (Math.abs(elapsedRealtime - a) > C3135o.f5482y.mo19389a(null).longValue()) {
                mo19012g().f5610h.mo19327a(elapsedRealtime);
                mo18881c();
                mo19284q();
                if (m8445O() && (delete = mo18875v().delete("queue", "abs(bundle_end_timestamp - ?) > cast(? as integer)", new String[]{String.valueOf(mo19017o().mo17132a()), String.valueOf(C3110la.m8856v())})) > 0) {
                    mo19015l().mo18996B().mo19043a("Deleted stale rows. rowsDeleted", Integer.valueOf(delete));
                }
            }
        }
    }

    @WorkerThread
    /* renamed from: C */
    public final long mo18836C() {
        return m8446a("select max(bundle_end_timestamp) from queue", (String[]) null, 0);
    }

    @WorkerThread
    /* renamed from: D */
    public final long mo18837D() {
        return m8446a("select max(timestamp) from raw_events", (String[]) null, 0);
    }

    /* renamed from: E */
    public final boolean mo18838E() {
        return m8452b("select count(1) > 0 from raw_events", null) != 0;
    }

    /* renamed from: F */
    public final boolean mo18839F() {
        return m8452b("select count(1) > 0 from raw_events where realtime = 1", null) != 0;
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00aa  */
    @androidx.annotation.WorkerThread
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.measurement.internal.C3234w9 mo18863c(java.lang.String r19, java.lang.String r20) {
        /*
            r18 = this;
            r8 = r20
            com.google.android.gms.common.internal.C2258v.m5639b(r19)
            com.google.android.gms.common.internal.C2258v.m5639b(r20)
            r18.mo18881c()
            r18.mo19284q()
            r9 = 0
            android.database.sqlite.SQLiteDatabase r10 = r18.mo18875v()     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
            java.lang.String r11 = "user_attributes"
            java.lang.String r0 = "set_timestamp"
            java.lang.String r1 = "value"
            java.lang.String r2 = "origin"
            java.lang.String[] r12 = new java.lang.String[]{r0, r1, r2}     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
            java.lang.String r13 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r14 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
            r1 = 0
            r14[r1] = r19     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
            r2 = 1
            r14[r2] = r8     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
            r15 = 0
            r16 = 0
            r17 = 0
            android.database.Cursor r10 = r10.query(r11, r12, r13, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x0084, all -> 0x007f }
            boolean r3 = r10.moveToFirst()     // Catch:{ SQLiteException -> 0x007b, all -> 0x0077 }
            if (r3 != 0) goto L_0x0040
            if (r10 == 0) goto L_0x003f
            r10.close()
        L_0x003f:
            return r9
        L_0x0040:
            long r5 = r10.getLong(r1)     // Catch:{ SQLiteException -> 0x007b, all -> 0x0077 }
            r11 = r18
            java.lang.Object r7 = r11.m8448a(r10, r2)     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r3 = r10.getString(r0)     // Catch:{ SQLiteException -> 0x0075 }
            com.google.android.gms.measurement.internal.w9 r0 = new com.google.android.gms.measurement.internal.w9     // Catch:{ SQLiteException -> 0x0075 }
            r1 = r0
            r2 = r19
            r4 = r20
            r1.<init>(r2, r3, r4, r5, r7)     // Catch:{ SQLiteException -> 0x0075 }
            boolean r1 = r10.moveToNext()     // Catch:{ SQLiteException -> 0x0075 }
            if (r1 == 0) goto L_0x006f
            com.google.android.gms.measurement.internal.f4 r1 = r18.mo19015l()     // Catch:{ SQLiteException -> 0x0075 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ SQLiteException -> 0x0075 }
            java.lang.String r2 = "Got multiple records for user property, expected one. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r19)     // Catch:{ SQLiteException -> 0x0075 }
            r1.mo19043a(r2, r3)     // Catch:{ SQLiteException -> 0x0075 }
        L_0x006f:
            if (r10 == 0) goto L_0x0074
            r10.close()
        L_0x0074:
            return r0
        L_0x0075:
            r0 = move-exception
            goto L_0x0088
        L_0x0077:
            r0 = move-exception
            r11 = r18
            goto L_0x00a8
        L_0x007b:
            r0 = move-exception
            r11 = r18
            goto L_0x0088
        L_0x007f:
            r0 = move-exception
            r11 = r18
            r10 = r9
            goto L_0x00a8
        L_0x0084:
            r0 = move-exception
            r11 = r18
            r10 = r9
        L_0x0088:
            com.google.android.gms.measurement.internal.f4 r1 = r18.mo19015l()     // Catch:{ all -> 0x00a7 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ all -> 0x00a7 }
            java.lang.String r2 = "Error querying user property. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r19)     // Catch:{ all -> 0x00a7 }
            com.google.android.gms.measurement.internal.c4 r4 = r18.mo19010e()     // Catch:{ all -> 0x00a7 }
            java.lang.String r4 = r4.mo18824c(r8)     // Catch:{ all -> 0x00a7 }
            r1.mo19045a(r2, r3, r4, r0)     // Catch:{ all -> 0x00a7 }
            if (r10 == 0) goto L_0x00a6
            r10.close()
        L_0x00a6:
            return r9
        L_0x00a7:
            r0 = move-exception
        L_0x00a8:
            if (r10 == 0) goto L_0x00ad
            r10.close()
        L_0x00ad:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18863c(java.lang.String, java.lang.String):com.google.android.gms.measurement.internal.w9");
    }

    /* JADX WARNING: Removed duplicated region for block: B:35:0x011f  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0126  */
    @androidx.annotation.WorkerThread
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.measurement.internal.zzv mo18864d(java.lang.String r30, java.lang.String r31) {
        /*
            r29 = this;
            r7 = r31
            com.google.android.gms.common.internal.C2258v.m5639b(r30)
            com.google.android.gms.common.internal.C2258v.m5639b(r31)
            r29.mo18881c()
            r29.mo19284q()
            r8 = 0
            android.database.sqlite.SQLiteDatabase r9 = r29.mo18875v()     // Catch:{ SQLiteException -> 0x0100, all -> 0x00fb }
            java.lang.String r10 = "conditional_properties"
            java.lang.String r11 = "origin"
            java.lang.String r12 = "value"
            java.lang.String r13 = "active"
            java.lang.String r14 = "trigger_event_name"
            java.lang.String r15 = "trigger_timeout"
            java.lang.String r16 = "timed_out_event"
            java.lang.String r17 = "creation_timestamp"
            java.lang.String r18 = "triggered_event"
            java.lang.String r19 = "triggered_timestamp"
            java.lang.String r20 = "time_to_live"
            java.lang.String r21 = "expired_event"
            java.lang.String[] r11 = new java.lang.String[]{r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21}     // Catch:{ SQLiteException -> 0x0100, all -> 0x00fb }
            java.lang.String r12 = "app_id=? and name=?"
            r0 = 2
            java.lang.String[] r13 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0100, all -> 0x00fb }
            r1 = 0
            r13[r1] = r30     // Catch:{ SQLiteException -> 0x0100, all -> 0x00fb }
            r2 = 1
            r13[r2] = r7     // Catch:{ SQLiteException -> 0x0100, all -> 0x00fb }
            r14 = 0
            r15 = 0
            r16 = 0
            android.database.Cursor r9 = r9.query(r10, r11, r12, r13, r14, r15, r16)     // Catch:{ SQLiteException -> 0x0100, all -> 0x00fb }
            boolean r3 = r9.moveToFirst()     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00f3 }
            if (r3 != 0) goto L_0x004f
            if (r9 == 0) goto L_0x004e
            r9.close()
        L_0x004e:
            return r8
        L_0x004f:
            java.lang.String r16 = r9.getString(r1)     // Catch:{ SQLiteException -> 0x00f7, all -> 0x00f3 }
            r10 = r29
            java.lang.Object r5 = r10.m8448a(r9, r2)     // Catch:{ SQLiteException -> 0x00f1 }
            int r0 = r9.getInt(r0)     // Catch:{ SQLiteException -> 0x00f1 }
            if (r0 == 0) goto L_0x0062
            r20 = 1
            goto L_0x0064
        L_0x0062:
            r20 = 0
        L_0x0064:
            r0 = 3
            java.lang.String r21 = r9.getString(r0)     // Catch:{ SQLiteException -> 0x00f1 }
            r0 = 4
            long r23 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.v9 r0 = r29.mo19171i()     // Catch:{ SQLiteException -> 0x00f1 }
            r1 = 5
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00f1 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzan> r2 = com.google.android.gms.measurement.internal.zzan.CREATOR     // Catch:{ SQLiteException -> 0x00f1 }
            android.os.Parcelable r0 = r0.mo19352a(r1, r2)     // Catch:{ SQLiteException -> 0x00f1 }
            r22 = r0
            com.google.android.gms.measurement.internal.zzan r22 = (com.google.android.gms.measurement.internal.zzan) r22     // Catch:{ SQLiteException -> 0x00f1 }
            r0 = 6
            long r18 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.v9 r0 = r29.mo19171i()     // Catch:{ SQLiteException -> 0x00f1 }
            r1 = 7
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00f1 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzan> r2 = com.google.android.gms.measurement.internal.zzan.CREATOR     // Catch:{ SQLiteException -> 0x00f1 }
            android.os.Parcelable r0 = r0.mo19352a(r1, r2)     // Catch:{ SQLiteException -> 0x00f1 }
            r25 = r0
            com.google.android.gms.measurement.internal.zzan r25 = (com.google.android.gms.measurement.internal.zzan) r25     // Catch:{ SQLiteException -> 0x00f1 }
            r0 = 8
            long r3 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f1 }
            r0 = 9
            long r26 = r9.getLong(r0)     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.v9 r0 = r29.mo19171i()     // Catch:{ SQLiteException -> 0x00f1 }
            r1 = 10
            byte[] r1 = r9.getBlob(r1)     // Catch:{ SQLiteException -> 0x00f1 }
            android.os.Parcelable$Creator<com.google.android.gms.measurement.internal.zzan> r2 = com.google.android.gms.measurement.internal.zzan.CREATOR     // Catch:{ SQLiteException -> 0x00f1 }
            android.os.Parcelable r0 = r0.mo19352a(r1, r2)     // Catch:{ SQLiteException -> 0x00f1 }
            r28 = r0
            com.google.android.gms.measurement.internal.zzan r28 = (com.google.android.gms.measurement.internal.zzan) r28     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.zzkq r17 = new com.google.android.gms.measurement.internal.zzkq     // Catch:{ SQLiteException -> 0x00f1 }
            r1 = r17
            r2 = r31
            r6 = r16
            r1.<init>(r2, r3, r5, r6)     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.zzv r0 = new com.google.android.gms.measurement.internal.zzv     // Catch:{ SQLiteException -> 0x00f1 }
            r14 = r0
            r15 = r30
            r14.<init>(r15, r16, r17, r18, r20, r21, r22, r23, r25, r26, r28)     // Catch:{ SQLiteException -> 0x00f1 }
            boolean r1 = r9.moveToNext()     // Catch:{ SQLiteException -> 0x00f1 }
            if (r1 == 0) goto L_0x00eb
            com.google.android.gms.measurement.internal.f4 r1 = r29.mo19015l()     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ SQLiteException -> 0x00f1 }
            java.lang.String r2 = "Got multiple records for conditional property, expected one"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r30)     // Catch:{ SQLiteException -> 0x00f1 }
            com.google.android.gms.measurement.internal.c4 r4 = r29.mo19010e()     // Catch:{ SQLiteException -> 0x00f1 }
            java.lang.String r4 = r4.mo18824c(r7)     // Catch:{ SQLiteException -> 0x00f1 }
            r1.mo19044a(r2, r3, r4)     // Catch:{ SQLiteException -> 0x00f1 }
        L_0x00eb:
            if (r9 == 0) goto L_0x00f0
            r9.close()
        L_0x00f0:
            return r0
        L_0x00f1:
            r0 = move-exception
            goto L_0x0104
        L_0x00f3:
            r0 = move-exception
            r10 = r29
            goto L_0x0124
        L_0x00f7:
            r0 = move-exception
            r10 = r29
            goto L_0x0104
        L_0x00fb:
            r0 = move-exception
            r10 = r29
            r9 = r8
            goto L_0x0124
        L_0x0100:
            r0 = move-exception
            r10 = r29
            r9 = r8
        L_0x0104:
            com.google.android.gms.measurement.internal.f4 r1 = r29.mo19015l()     // Catch:{ all -> 0x0123 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ all -> 0x0123 }
            java.lang.String r2 = "Error querying conditional property"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r30)     // Catch:{ all -> 0x0123 }
            com.google.android.gms.measurement.internal.c4 r4 = r29.mo19010e()     // Catch:{ all -> 0x0123 }
            java.lang.String r4 = r4.mo18824c(r7)     // Catch:{ all -> 0x0123 }
            r1.mo19045a(r2, r3, r4, r0)     // Catch:{ all -> 0x0123 }
            if (r9 == 0) goto L_0x0122
            r9.close()
        L_0x0122:
            return r8
        L_0x0123:
            r0 = move-exception
        L_0x0124:
            if (r9 == 0) goto L_0x0129
            r9.close()
        L_0x0129:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18864d(java.lang.String, java.lang.String):com.google.android.gms.measurement.internal.zzv");
    }

    @WorkerThread
    /* renamed from: e */
    public final int mo18866e(String str, String str2) {
        C2258v.m5639b(str);
        C2258v.m5639b(str2);
        mo18881c();
        mo19284q();
        try {
            return mo18875v().delete("conditional_properties", "app_id=? and name=?", new String[]{str, str2});
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19045a("Error deleting conditional property", C3032f4.m8621a(str), mo19010e().mo18824c(str2), e);
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b6  */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.google.android.gms.internal.measurement.C2445c0>> mo18869f(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.mo19284q()
            r12.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r13)
            com.google.android.gms.common.internal.C2258v.m5639b(r14)
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.mo18875v()
            r9 = 0
            java.lang.String r2 = "event_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            java.lang.String r4 = "app_id=? AND event_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            r10 = 0
            r5[r10] = r13     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            r11 = 1
            r5[r11] = r14     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x0095 }
            if (r1 != 0) goto L_0x0042
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0095 }
            if (r14 == 0) goto L_0x0041
            r14.close()
        L_0x0041:
            return r13
        L_0x0042:
            byte[] r1 = r14.getBlob(r11)     // Catch:{ SQLiteException -> 0x0095 }
            com.google.android.gms.internal.measurement.c0$a r2 = com.google.android.gms.internal.measurement.C2445c0.m6106z()     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.measurement.internal.C3223v9.m9262a(r2, r1)     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.c0$a r2 = (com.google.android.gms.internal.measurement.C2445c0.C2446a) r2     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.u5 r1 = r2.mo17679i()     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.l4 r1 = (com.google.android.gms.internal.measurement.C2595l4) r1     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.c0 r1 = (com.google.android.gms.internal.measurement.C2445c0) r1     // Catch:{ IOException -> 0x0077 }
            int r2 = r14.getInt(r10)     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.Object r3 = r0.get(r3)     // Catch:{ SQLiteException -> 0x0095 }
            java.util.List r3 = (java.util.List) r3     // Catch:{ SQLiteException -> 0x0095 }
            if (r3 != 0) goto L_0x0073
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0095 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0095 }
            r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x0095 }
        L_0x0073:
            r3.add(r1)     // Catch:{ SQLiteException -> 0x0095 }
            goto L_0x0089
        L_0x0077:
            r1 = move-exception
            com.google.android.gms.measurement.internal.f4 r2 = r12.mo19015l()     // Catch:{ SQLiteException -> 0x0095 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.String r3 = "Failed to merge filter. appId"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ SQLiteException -> 0x0095 }
            r2.mo19044a(r3, r4, r1)     // Catch:{ SQLiteException -> 0x0095 }
        L_0x0089:
            boolean r1 = r14.moveToNext()     // Catch:{ SQLiteException -> 0x0095 }
            if (r1 != 0) goto L_0x0042
            if (r14 == 0) goto L_0x0094
            r14.close()
        L_0x0094:
            return r0
        L_0x0095:
            r0 = move-exception
            goto L_0x009c
        L_0x0097:
            r13 = move-exception
            r14 = r9
            goto L_0x00b4
        L_0x009a:
            r0 = move-exception
            r14 = r9
        L_0x009c:
            com.google.android.gms.measurement.internal.f4 r1 = r12.mo19015l()     // Catch:{ all -> 0x00b3 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ all -> 0x00b3 }
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r13 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ all -> 0x00b3 }
            r1.mo19044a(r2, r13, r0)     // Catch:{ all -> 0x00b3 }
            if (r14 == 0) goto L_0x00b2
            r14.close()
        L_0x00b2:
            return r9
        L_0x00b3:
            r13 = move-exception
        L_0x00b4:
            if (r14 == 0) goto L_0x00b9
            r14.close()
        L_0x00b9:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18869f(java.lang.String, java.lang.String):java.util.Map");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00af  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b6  */
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<com.google.android.gms.internal.measurement.C2494f0>> mo18871g(java.lang.String r13, java.lang.String r14) {
        /*
            r12 = this;
            r12.mo19284q()
            r12.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r13)
            com.google.android.gms.common.internal.C2258v.m5639b(r14)
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r12.mo18875v()
            r9 = 0
            java.lang.String r2 = "property_filters"
            java.lang.String r3 = "audience_id"
            java.lang.String r4 = "data"
            java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            java.lang.String r4 = "app_id=? AND property_name=?"
            r5 = 2
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            r10 = 0
            r5[r10] = r13     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            r11 = 1
            r5[r11] = r14     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r14 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x009a, all -> 0x0097 }
            boolean r1 = r14.moveToFirst()     // Catch:{ SQLiteException -> 0x0095 }
            if (r1 != 0) goto L_0x0042
            java.util.Map r13 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0095 }
            if (r14 == 0) goto L_0x0041
            r14.close()
        L_0x0041:
            return r13
        L_0x0042:
            byte[] r1 = r14.getBlob(r11)     // Catch:{ SQLiteException -> 0x0095 }
            com.google.android.gms.internal.measurement.f0$a r2 = com.google.android.gms.internal.measurement.C2494f0.m6285w()     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.measurement.internal.C3223v9.m9262a(r2, r1)     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.f0$a r2 = (com.google.android.gms.internal.measurement.C2494f0.C2495a) r2     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.u5 r1 = r2.mo17679i()     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.l4 r1 = (com.google.android.gms.internal.measurement.C2595l4) r1     // Catch:{ IOException -> 0x0077 }
            com.google.android.gms.internal.measurement.f0 r1 = (com.google.android.gms.internal.measurement.C2494f0) r1     // Catch:{ IOException -> 0x0077 }
            int r2 = r14.getInt(r10)     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.Object r3 = r0.get(r3)     // Catch:{ SQLiteException -> 0x0095 }
            java.util.List r3 = (java.util.List) r3     // Catch:{ SQLiteException -> 0x0095 }
            if (r3 != 0) goto L_0x0073
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0095 }
            r3.<init>()     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x0095 }
            r0.put(r2, r3)     // Catch:{ SQLiteException -> 0x0095 }
        L_0x0073:
            r3.add(r1)     // Catch:{ SQLiteException -> 0x0095 }
            goto L_0x0089
        L_0x0077:
            r1 = move-exception
            com.google.android.gms.measurement.internal.f4 r2 = r12.mo19015l()     // Catch:{ SQLiteException -> 0x0095 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ SQLiteException -> 0x0095 }
            java.lang.String r3 = "Failed to merge filter"
            java.lang.Object r4 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ SQLiteException -> 0x0095 }
            r2.mo19044a(r3, r4, r1)     // Catch:{ SQLiteException -> 0x0095 }
        L_0x0089:
            boolean r1 = r14.moveToNext()     // Catch:{ SQLiteException -> 0x0095 }
            if (r1 != 0) goto L_0x0042
            if (r14 == 0) goto L_0x0094
            r14.close()
        L_0x0094:
            return r0
        L_0x0095:
            r0 = move-exception
            goto L_0x009c
        L_0x0097:
            r13 = move-exception
            r14 = r9
            goto L_0x00b4
        L_0x009a:
            r0 = move-exception
            r14 = r9
        L_0x009c:
            com.google.android.gms.measurement.internal.f4 r1 = r12.mo19015l()     // Catch:{ all -> 0x00b3 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ all -> 0x00b3 }
            java.lang.String r2 = "Database error querying filters. appId"
            java.lang.Object r13 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ all -> 0x00b3 }
            r1.mo19044a(r2, r13, r0)     // Catch:{ all -> 0x00b3 }
            if (r14 == 0) goto L_0x00b2
            r14.close()
        L_0x00b2:
            return r9
        L_0x00b3:
            r13 = move-exception
        L_0x00b4:
            if (r14 == 0) goto L_0x00b9
            r14.close()
        L_0x00b9:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18871g(java.lang.String, java.lang.String):java.util.Map");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: h */
    public final long mo18873h(String str, String str2) {
        long j;
        String str3 = str;
        String str4 = str2;
        C2258v.m5639b(str);
        C2258v.m5639b(str2);
        mo18881c();
        mo19284q();
        SQLiteDatabase v = mo18875v();
        v.beginTransaction();
        try {
            StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 32);
            sb.append("select ");
            sb.append(str4);
            sb.append(" from app2 where app_id=?");
            try {
                j = m8446a(sb.toString(), new String[]{str3}, -1);
                if (j == -1) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("app_id", str3);
                    contentValues.put("first_open_count", (Integer) 0);
                    contentValues.put("previous_install_count", (Integer) 0);
                    if (v.insertWithOnConflict("app2", null, contentValues, 5) == -1) {
                        mo19015l().mo19001t().mo19044a("Failed to insert column (got -1). appId", C3032f4.m8621a(str), str4);
                        v.endTransaction();
                        return -1;
                    }
                    j = 0;
                }
            } catch (SQLiteException e) {
                e = e;
                j = 0;
                try {
                    mo19015l().mo19001t().mo19045a("Error inserting column. appId", C3032f4.m8621a(str), str4, e);
                    v.endTransaction();
                    return j;
                } catch (Throwable th) {
                    th = th;
                    v.endTransaction();
                    throw th;
                }
            }
            try {
                ContentValues contentValues2 = new ContentValues();
                contentValues2.put("app_id", str3);
                contentValues2.put(str4, Long.valueOf(1 + j));
                if (((long) v.update("app2", contentValues2, "app_id = ?", new String[]{str3})) == 0) {
                    mo19015l().mo19001t().mo19044a("Failed to update column (got 0). appId", C3032f4.m8621a(str), str4);
                    v.endTransaction();
                    return -1;
                }
                v.setTransactionSuccessful();
                v.endTransaction();
                return j;
            } catch (SQLiteException e2) {
                e = e2;
                mo19015l().mo19001t().mo19045a("Error inserting column. appId", C3032f4.m8621a(str), str4, e);
                v.endTransaction();
                return j;
            }
        } catch (SQLiteException e3) {
            e = e3;
            j = 0;
            mo19015l().mo19001t().mo19045a("Error inserting column. appId", C3032f4.m8621a(str), str4, e);
            v.endTransaction();
            return j;
        } catch (Throwable th2) {
            th = th2;
            v.endTransaction();
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final boolean mo18833t() {
        return false;
    }

    @WorkerThread
    /* renamed from: u */
    public final void mo18874u() {
        mo19284q();
        mo18875v().setTransactionSuccessful();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: v */
    public final SQLiteDatabase mo18875v() {
        mo18881c();
        try {
            return this.f5035d.getWritableDatabase();
        } catch (SQLiteException e) {
            mo19015l().mo19004w().mo19043a("Error opening database", e);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0041  */
    @androidx.annotation.WorkerThread
    /* renamed from: w */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String mo18876w() {
        /*
            r6 = this;
            android.database.sqlite.SQLiteDatabase r0 = r6.mo18875v()
            r1 = 0
            java.lang.String r2 = "select app_id from queue order by has_realtime desc, rowid asc limit 1;"
            android.database.Cursor r0 = r0.rawQuery(r2, r1)     // Catch:{ SQLiteException -> 0x0029, all -> 0x0024 }
            boolean r2 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x0022 }
            if (r2 == 0) goto L_0x001c
            r2 = 0
            java.lang.String r1 = r0.getString(r2)     // Catch:{ SQLiteException -> 0x0022 }
            if (r0 == 0) goto L_0x001b
            r0.close()
        L_0x001b:
            return r1
        L_0x001c:
            if (r0 == 0) goto L_0x0021
            r0.close()
        L_0x0021:
            return r1
        L_0x0022:
            r2 = move-exception
            goto L_0x002b
        L_0x0024:
            r0 = move-exception
            r5 = r1
            r1 = r0
            r0 = r5
            goto L_0x003f
        L_0x0029:
            r2 = move-exception
            r0 = r1
        L_0x002b:
            com.google.android.gms.measurement.internal.f4 r3 = r6.mo19015l()     // Catch:{ all -> 0x003e }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x003e }
            java.lang.String r4 = "Database error getting next bundle app id"
            r3.mo19043a(r4, r2)     // Catch:{ all -> 0x003e }
            if (r0 == 0) goto L_0x003d
            r0.close()
        L_0x003d:
            return r1
        L_0x003e:
            r1 = move-exception
        L_0x003f:
            if (r0 == 0) goto L_0x0044
            r0.close()
        L_0x0044:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18876w():java.lang.String");
    }

    /* renamed from: x */
    public final long mo18877x() {
        Cursor cursor = null;
        try {
            cursor = mo18875v().rawQuery("select rowid from raw_events order by rowid desc limit 1;", null);
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return -1;
            }
            long j = cursor.getLong(0);
            if (cursor != null) {
                cursor.close();
            }
            return j;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19043a("Error querying raw events", e);
            if (cursor != null) {
                cursor.close();
            }
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    /* renamed from: y */
    public final void mo18878y() {
        mo19284q();
        mo18875v().beginTransaction();
    }

    @WorkerThread
    /* renamed from: z */
    public final void mo18879z() {
        mo19284q();
        mo18875v().endTransaction();
    }

    /* JADX WARNING: Removed duplicated region for block: B:61:0x0154  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x015b  */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.measurement.internal.C3087k mo18843a(java.lang.String r27, java.lang.String r28) {
        /*
            r26 = this;
            r15 = r27
            r14 = r28
            com.google.android.gms.common.internal.C2258v.m5639b(r27)
            com.google.android.gms.common.internal.C2258v.m5639b(r28)
            r26.mo18881c()
            r26.mo19284q()
            com.google.android.gms.measurement.internal.la r0 = r26.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.C3135o.f5459m0
            boolean r0 = r0.mo19154e(r15, r1)
            java.util.ArrayList r1 = new java.util.ArrayList
            java.lang.String r2 = "lifetime_count"
            java.lang.String r3 = "current_bundle_count"
            java.lang.String r4 = "last_fire_timestamp"
            java.lang.String r5 = "last_bundled_timestamp"
            java.lang.String r6 = "last_bundled_day"
            java.lang.String r7 = "last_sampled_complex_event_id"
            java.lang.String r8 = "last_sampling_rate"
            java.lang.String r9 = "last_exempt_from_sampling"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3, r4, r5, r6, r7, r8, r9}
            java.util.List r2 = java.util.Arrays.asList(r2)
            r1.<init>(r2)
            if (r0 == 0) goto L_0x003e
            java.lang.String r2 = "current_session_count"
            r1.add(r2)
        L_0x003e:
            r18 = 0
            android.database.sqlite.SQLiteDatabase r2 = r26.mo18875v()     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            java.lang.String r3 = "events"
            r10 = 0
            java.lang.String[] r4 = new java.lang.String[r10]     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            java.lang.Object[] r1 = r1.toArray(r4)     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            r4 = r1
            java.lang.String[] r4 = (java.lang.String[]) r4     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            java.lang.String r5 = "app_id=? and name=?"
            r1 = 2
            java.lang.String[] r6 = new java.lang.String[r1]     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            r6[r10] = r15     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            r11 = 1
            r6[r11] = r14     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            r7 = 0
            r8 = 0
            r9 = 0
            android.database.Cursor r12 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ SQLiteException -> 0x0134, all -> 0x0130 }
            boolean r2 = r12.moveToFirst()     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            if (r2 != 0) goto L_0x006d
            if (r12 == 0) goto L_0x006c
            r12.close()
        L_0x006c:
            return r18
        L_0x006d:
            long r4 = r12.getLong(r10)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            long r6 = r12.getLong(r11)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            long r16 = r12.getLong(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r1 = 3
            boolean r2 = r12.isNull(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r8 = 0
            if (r2 == 0) goto L_0x0085
            r19 = r8
            goto L_0x008b
        L_0x0085:
            long r1 = r12.getLong(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r19 = r1
        L_0x008b:
            r1 = 4
            boolean r2 = r12.isNull(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            if (r2 == 0) goto L_0x0095
            r21 = r18
            goto L_0x009f
        L_0x0095:
            long r1 = r12.getLong(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r21 = r1
        L_0x009f:
            r1 = 5
            boolean r2 = r12.isNull(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            if (r2 == 0) goto L_0x00a9
            r22 = r18
            goto L_0x00b3
        L_0x00a9:
            long r1 = r12.getLong(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r22 = r1
        L_0x00b3:
            r1 = 6
            boolean r2 = r12.isNull(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            if (r2 == 0) goto L_0x00bd
            r23 = r18
            goto L_0x00c7
        L_0x00bd:
            long r1 = r12.getLong(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r23 = r1
        L_0x00c7:
            r1 = 7
            boolean r2 = r12.isNull(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            if (r2 != 0) goto L_0x00e0
            long r1 = r12.getLong(r1)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r24 = 1
            int r3 = (r1 > r24 ? 1 : (r1 == r24 ? 0 : -1))
            if (r3 != 0) goto L_0x00d9
            r10 = 1
        L_0x00d9:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r10)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r24 = r1
            goto L_0x00e2
        L_0x00e0:
            r24 = r18
        L_0x00e2:
            if (r0 == 0) goto L_0x00f1
            r0 = 8
            boolean r1 = r12.isNull(r0)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            if (r1 != 0) goto L_0x00f1
            long r0 = r12.getLong(r0)     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r8 = r0
        L_0x00f1:
            com.google.android.gms.measurement.internal.k r0 = new com.google.android.gms.measurement.internal.k     // Catch:{ SQLiteException -> 0x012c, all -> 0x0128 }
            r1 = r0
            r2 = r27
            r3 = r28
            r10 = r16
            r25 = r12
            r12 = r19
            r14 = r21
            r15 = r22
            r16 = r23
            r17 = r24
            r1.<init>(r2, r3, r4, r6, r8, r10, r12, r14, r15, r16, r17)     // Catch:{ SQLiteException -> 0x0126 }
            boolean r1 = r25.moveToNext()     // Catch:{ SQLiteException -> 0x0126 }
            if (r1 == 0) goto L_0x0120
            com.google.android.gms.measurement.internal.f4 r1 = r26.mo19015l()     // Catch:{ SQLiteException -> 0x0126 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ SQLiteException -> 0x0126 }
            java.lang.String r2 = "Got multiple records for event aggregates, expected one. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r27)     // Catch:{ SQLiteException -> 0x0126 }
            r1.mo19043a(r2, r3)     // Catch:{ SQLiteException -> 0x0126 }
        L_0x0120:
            if (r25 == 0) goto L_0x0125
            r25.close()
        L_0x0125:
            return r0
        L_0x0126:
            r0 = move-exception
            goto L_0x0137
        L_0x0128:
            r0 = move-exception
            r25 = r12
            goto L_0x0159
        L_0x012c:
            r0 = move-exception
            r25 = r12
            goto L_0x0137
        L_0x0130:
            r0 = move-exception
            r25 = r18
            goto L_0x0159
        L_0x0134:
            r0 = move-exception
            r25 = r18
        L_0x0137:
            com.google.android.gms.measurement.internal.f4 r1 = r26.mo19015l()     // Catch:{ all -> 0x0158 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ all -> 0x0158 }
            java.lang.String r2 = "Error querying events. appId"
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r27)     // Catch:{ all -> 0x0158 }
            com.google.android.gms.measurement.internal.c4 r4 = r26.mo19010e()     // Catch:{ all -> 0x0158 }
            r5 = r28
            java.lang.String r4 = r4.mo18822a(r5)     // Catch:{ all -> 0x0158 }
            r1.mo19045a(r2, r3, r4, r0)     // Catch:{ all -> 0x0158 }
            if (r25 == 0) goto L_0x0157
            r25.close()
        L_0x0157:
            return r18
        L_0x0158:
            r0 = move-exception
        L_0x0159:
            if (r25 == 0) goto L_0x015e
            r25.close()
        L_0x015e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18843a(java.lang.String, java.lang.String):com.google.android.gms.measurement.internal.k");
    }

    @WorkerThread
    /* renamed from: b */
    public final void mo18860b(String str, String str2) {
        C2258v.m5639b(str);
        C2258v.m5639b(str2);
        mo18881c();
        mo19284q();
        try {
            int delete = mo18875v().delete("user_attributes", "app_id=? and name=?", new String[]{str, str2});
            if (!C2489ea.m6262b() || !this.f5360b.mo19220b().mo19154e(str, C3135o.f5431Y0)) {
                mo19015l().mo18996B().mo19043a("Deleted user attribute rows", Integer.valueOf(delete));
            }
        } catch (SQLiteException e) {
            if (!C2489ea.m6262b() || !this.f5360b.mo19220b().mo19154e(str, C3135o.f5431Y0)) {
                mo19015l().mo19001t().mo19045a("Error deleting user attribute. appId", C3032f4.m8621a(str), mo19010e().mo18824c(str2), e);
            } else {
                mo19015l().mo19001t().mo19045a("Error deleting user property. appId", C3032f4.m8621a(str), mo19010e().mo18824c(str2), e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public final Map<Integer, List<C2445c0>> mo18867e(String str) {
        C2258v.m5639b(str);
        ArrayMap arrayMap = new ArrayMap();
        Cursor cursor = null;
        try {
            cursor = mo18875v().query("event_filters", new String[]{"audience_id", "data"}, "app_id=?", new String[]{str}, null, null, null);
            if (!cursor.moveToFirst()) {
                Map<Integer, List<C2445c0>> emptyMap = Collections.emptyMap();
                if (cursor != null) {
                    cursor.close();
                }
                return emptyMap;
            }
            do {
                byte[] blob = cursor.getBlob(1);
                try {
                    C2445c0.C2446a z = C2445c0.m6106z();
                    C3223v9.m9262a(z, blob);
                    C2445c0 c0Var = (C2445c0) ((C2595l4) z.mo17679i());
                    if (c0Var.mo17352t()) {
                        int i = cursor.getInt(0);
                        List list = (List) arrayMap.get(Integer.valueOf(i));
                        if (list == null) {
                            list = new ArrayList();
                            arrayMap.put(Integer.valueOf(i), list);
                        }
                        list.add(c0Var);
                    }
                } catch (IOException e) {
                    mo19015l().mo19001t().mo19044a("Failed to merge filter. appId", C3032f4.m8621a(str), e);
                }
            } while (cursor.moveToNext());
            if (cursor != null) {
                cursor.close();
            }
            return arrayMap;
        } catch (SQLiteException e2) {
            mo19015l().mo19001t().mo19044a("Database error querying filters. appId", C3032f4.m8621a(str), e2);
            Map<Integer, List<C2445c0>> emptyMap2 = Collections.emptyMap();
            if (cursor != null) {
                cursor.close();
            }
            return emptyMap2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* renamed from: c */
    public final long mo18862c(String str) {
        C2258v.m5639b(str);
        mo18881c();
        mo19284q();
        try {
            return (long) mo18875v().delete("raw_events", "rowid in (select rowid from raw_events where app_id=? order by rowid desc limit -1 offset ?)", new String[]{str, String.valueOf(Math.max(0, Math.min(1000000, mo19013h().mo19147b(str, C3135o.f5464p))))});
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error deleting over the limit events. appId", C3032f4.m8621a(str), e);
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x009c  */
    /* renamed from: g */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, com.google.android.gms.internal.measurement.C2794y0> mo18870g(java.lang.String r12) {
        /*
            r11 = this;
            r11.mo19284q()
            r11.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r12)
            android.database.sqlite.SQLiteDatabase r0 = r11.mo18875v()
            r8 = 0
            java.lang.String r1 = "audience_filter_values"
            java.lang.String r2 = "audience_id"
            java.lang.String r3 = "current_results"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3}     // Catch:{ SQLiteException -> 0x0080, all -> 0x007d }
            java.lang.String r3 = "app_id=?"
            r9 = 1
            java.lang.String[] r4 = new java.lang.String[r9]     // Catch:{ SQLiteException -> 0x0080, all -> 0x007d }
            r10 = 0
            r4[r10] = r12     // Catch:{ SQLiteException -> 0x0080, all -> 0x007d }
            r5 = 0
            r6 = 0
            r7 = 0
            android.database.Cursor r0 = r0.query(r1, r2, r3, r4, r5, r6, r7)     // Catch:{ SQLiteException -> 0x0080, all -> 0x007d }
            boolean r1 = r0.moveToFirst()     // Catch:{ SQLiteException -> 0x007b }
            if (r1 != 0) goto L_0x0033
            if (r0 == 0) goto L_0x0032
            r0.close()
        L_0x0032:
            return r8
        L_0x0033:
            androidx.collection.ArrayMap r1 = new androidx.collection.ArrayMap     // Catch:{ SQLiteException -> 0x007b }
            r1.<init>()     // Catch:{ SQLiteException -> 0x007b }
        L_0x0038:
            int r2 = r0.getInt(r10)     // Catch:{ SQLiteException -> 0x007b }
            byte[] r3 = r0.getBlob(r9)     // Catch:{ SQLiteException -> 0x007b }
            com.google.android.gms.internal.measurement.y0$a r4 = com.google.android.gms.internal.measurement.C2794y0.m7800w()     // Catch:{ IOException -> 0x0059 }
            com.google.android.gms.measurement.internal.C3223v9.m9262a(r4, r3)     // Catch:{ IOException -> 0x0059 }
            com.google.android.gms.internal.measurement.y0$a r4 = (com.google.android.gms.internal.measurement.C2794y0.C2795a) r4     // Catch:{ IOException -> 0x0059 }
            com.google.android.gms.internal.measurement.u5 r3 = r4.mo17679i()     // Catch:{ IOException -> 0x0059 }
            com.google.android.gms.internal.measurement.l4 r3 = (com.google.android.gms.internal.measurement.C2595l4) r3     // Catch:{ IOException -> 0x0059 }
            com.google.android.gms.internal.measurement.y0 r3 = (com.google.android.gms.internal.measurement.C2794y0) r3     // Catch:{ IOException -> 0x0059 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x007b }
            r1.put(r2, r3)     // Catch:{ SQLiteException -> 0x007b }
            goto L_0x006f
        L_0x0059:
            r3 = move-exception
            com.google.android.gms.measurement.internal.f4 r4 = r11.mo19015l()     // Catch:{ SQLiteException -> 0x007b }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ SQLiteException -> 0x007b }
            java.lang.String r5 = "Failed to merge filter results. appId, audienceId, error"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r12)     // Catch:{ SQLiteException -> 0x007b }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ SQLiteException -> 0x007b }
            r4.mo19045a(r5, r6, r2, r3)     // Catch:{ SQLiteException -> 0x007b }
        L_0x006f:
            boolean r2 = r0.moveToNext()     // Catch:{ SQLiteException -> 0x007b }
            if (r2 != 0) goto L_0x0038
            if (r0 == 0) goto L_0x007a
            r0.close()
        L_0x007a:
            return r1
        L_0x007b:
            r1 = move-exception
            goto L_0x0082
        L_0x007d:
            r12 = move-exception
            r0 = r8
            goto L_0x009a
        L_0x0080:
            r1 = move-exception
            r0 = r8
        L_0x0082:
            com.google.android.gms.measurement.internal.f4 r2 = r11.mo19015l()     // Catch:{ all -> 0x0099 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x0099 }
            java.lang.String r3 = "Database error querying filter results. appId"
            java.lang.Object r12 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r12)     // Catch:{ all -> 0x0099 }
            r2.mo19044a(r3, r12, r1)     // Catch:{ all -> 0x0099 }
            if (r0 == 0) goto L_0x0098
            r0.close()
        L_0x0098:
            return r8
        L_0x0099:
            r12 = move-exception
        L_0x009a:
            if (r0 == 0) goto L_0x009f
            r0.close()
        L_0x009f:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18870g(java.lang.String):java.util.Map");
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x0086  */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<java.lang.Integer>> mo18868f(java.lang.String r8) {
        /*
            r7 = this;
            r7.mo19284q()
            r7.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r8)
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            android.database.sqlite.SQLiteDatabase r1 = r7.mo18875v()
            r2 = 0
            java.lang.String r3 = "select audience_id, filter_id from event_filters where app_id = ? and session_scoped = 1 UNION select audience_id, filter_id from property_filters where app_id = ? and session_scoped = 1;"
            r4 = 2
            java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ SQLiteException -> 0x006a, all -> 0x0067 }
            r5 = 0
            r4[r5] = r8     // Catch:{ SQLiteException -> 0x006a, all -> 0x0067 }
            r6 = 1
            r4[r6] = r8     // Catch:{ SQLiteException -> 0x006a, all -> 0x0067 }
            android.database.Cursor r1 = r1.rawQuery(r3, r4)     // Catch:{ SQLiteException -> 0x006a, all -> 0x0067 }
            boolean r3 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0065 }
            if (r3 != 0) goto L_0x0032
            java.util.Map r8 = java.util.Collections.emptyMap()     // Catch:{ SQLiteException -> 0x0065 }
            if (r1 == 0) goto L_0x0031
            r1.close()
        L_0x0031:
            return r8
        L_0x0032:
            int r3 = r1.getInt(r5)     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.Object r4 = r0.get(r4)     // Catch:{ SQLiteException -> 0x0065 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ SQLiteException -> 0x0065 }
            if (r4 != 0) goto L_0x004e
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0065 }
            r4.<init>()     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0065 }
            r0.put(r3, r4)     // Catch:{ SQLiteException -> 0x0065 }
        L_0x004e:
            int r3 = r1.getInt(r6)     // Catch:{ SQLiteException -> 0x0065 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x0065 }
            r4.add(r3)     // Catch:{ SQLiteException -> 0x0065 }
            boolean r3 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0065 }
            if (r3 != 0) goto L_0x0032
            if (r1 == 0) goto L_0x0064
            r1.close()
        L_0x0064:
            return r0
        L_0x0065:
            r0 = move-exception
            goto L_0x006c
        L_0x0067:
            r8 = move-exception
            r1 = r2
            goto L_0x0084
        L_0x006a:
            r0 = move-exception
            r1 = r2
        L_0x006c:
            com.google.android.gms.measurement.internal.f4 r3 = r7.mo19015l()     // Catch:{ all -> 0x0083 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x0083 }
            java.lang.String r4 = "Database error querying scoped filters. appId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r8)     // Catch:{ all -> 0x0083 }
            r3.mo19044a(r4, r8, r0)     // Catch:{ all -> 0x0083 }
            if (r1 == 0) goto L_0x0082
            r1.close()
        L_0x0082:
            return r2
        L_0x0083:
            r8 = move-exception
        L_0x0084:
            if (r1 == 0) goto L_0x0089
            r1.close()
        L_0x0089:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18868f(java.lang.String):java.util.Map");
    }

    @WorkerThread
    /* renamed from: b */
    public final List<zzv> mo18859b(String str, String str2, String str3) {
        C2258v.m5639b(str);
        mo18881c();
        mo19284q();
        ArrayList arrayList = new ArrayList(3);
        arrayList.add(str);
        StringBuilder sb = new StringBuilder("app_id=?");
        if (!TextUtils.isEmpty(str2)) {
            arrayList.add(str2);
            sb.append(" and origin=?");
        }
        if (!TextUtils.isEmpty(str3)) {
            arrayList.add(String.valueOf(str3).concat("*"));
            sb.append(" and name glob ?");
        }
        return mo18848a(sb.toString(), (String[]) arrayList.toArray(new String[arrayList.size()]));
    }

    /* renamed from: h */
    public final long mo18872h(String str) {
        C2258v.m5639b(str);
        return m8446a("select count(1) from events where app_id=? and name not like '!_%' escape '!'", new String[]{str}, 0);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x006c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0073  */
    @androidx.annotation.WorkerThread
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final byte[] mo18865d(java.lang.String r11) {
        /*
            r10 = this;
            com.google.android.gms.common.internal.C2258v.m5639b(r11)
            r10.mo18881c()
            r10.mo19284q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r10.mo18875v()     // Catch:{ SQLiteException -> 0x0057, all -> 0x0054 }
            java.lang.String r2 = "apps"
            java.lang.String r3 = "remote_config"
            java.lang.String[] r3 = new java.lang.String[]{r3}     // Catch:{ SQLiteException -> 0x0057, all -> 0x0054 }
            java.lang.String r4 = "app_id=?"
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ SQLiteException -> 0x0057, all -> 0x0054 }
            r9 = 0
            r5[r9] = r11     // Catch:{ SQLiteException -> 0x0057, all -> 0x0054 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ SQLiteException -> 0x0057, all -> 0x0054 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0052 }
            if (r2 != 0) goto L_0x0031
            if (r1 == 0) goto L_0x0030
            r1.close()
        L_0x0030:
            return r0
        L_0x0031:
            byte[] r2 = r1.getBlob(r9)     // Catch:{ SQLiteException -> 0x0052 }
            boolean r3 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x0052 }
            if (r3 == 0) goto L_0x004c
            com.google.android.gms.measurement.internal.f4 r3 = r10.mo19015l()     // Catch:{ SQLiteException -> 0x0052 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ SQLiteException -> 0x0052 }
            java.lang.String r4 = "Got multiple records for app config, expected one. appId"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r11)     // Catch:{ SQLiteException -> 0x0052 }
            r3.mo19043a(r4, r5)     // Catch:{ SQLiteException -> 0x0052 }
        L_0x004c:
            if (r1 == 0) goto L_0x0051
            r1.close()
        L_0x0051:
            return r2
        L_0x0052:
            r2 = move-exception
            goto L_0x0059
        L_0x0054:
            r11 = move-exception
            r1 = r0
            goto L_0x0071
        L_0x0057:
            r2 = move-exception
            r1 = r0
        L_0x0059:
            com.google.android.gms.measurement.internal.f4 r3 = r10.mo19015l()     // Catch:{ all -> 0x0070 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x0070 }
            java.lang.String r4 = "Error querying remote config. appId"
            java.lang.Object r11 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r11)     // Catch:{ all -> 0x0070 }
            r3.mo19044a(r4, r11, r2)     // Catch:{ all -> 0x0070 }
            if (r1 == 0) goto L_0x006f
            r1.close()
        L_0x006f:
            return r0
        L_0x0070:
            r11 = move-exception
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()
        L_0x0076:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18865d(java.lang.String):byte[]");
    }

    /* renamed from: c */
    private final boolean m8453c(String str, List<Integer> list) {
        C2258v.m5639b(str);
        mo19284q();
        mo18881c();
        SQLiteDatabase v = mo18875v();
        try {
            long b = m8452b("select count(1) from audience_filter_values where app_id=?", new String[]{str});
            int max = Math.max(0, Math.min(2000, mo19013h().mo19147b(str, C3135o.f5392F)));
            if (b <= ((long) max)) {
                return false;
            }
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < list.size(); i++) {
                Integer num = list.get(i);
                if (num == null || !(num instanceof Integer)) {
                    return false;
                }
                arrayList.add(Integer.toString(num.intValue()));
            }
            String join = TextUtils.join(",", arrayList);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 140);
            sb3.append("audience_id in (select audience_id from audience_filter_values where app_id=? and audience_id not in ");
            sb3.append(sb2);
            sb3.append(" order by rowid desc limit -1 offset ?)");
            return v.delete("audience_filter_values", sb3.toString(), new String[]{str, Integer.toString(max)}) > 0;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Database error querying filters. appId", C3032f4.m8621a(str), e);
            return false;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x011b A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x011f A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0155 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0157 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0166 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x017b A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0197 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0198 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01a7 A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01dd A[Catch:{ SQLiteException -> 0x01f4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01f0  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x021a  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0221  */
    @androidx.annotation.WorkerThread
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.measurement.internal.C3021e5 mo18858b(java.lang.String r35) {
        /*
            r34 = this;
            r1 = r35
            com.google.android.gms.common.internal.C2258v.m5639b(r35)
            r34.mo18881c()
            r34.mo19284q()
            r2 = 0
            android.database.sqlite.SQLiteDatabase r3 = r34.mo18875v()     // Catch:{ SQLiteException -> 0x0203, all -> 0x01fe }
            java.lang.String r4 = "apps"
            java.lang.String r5 = "app_instance_id"
            java.lang.String r6 = "gmp_app_id"
            java.lang.String r7 = "resettable_device_id_hash"
            java.lang.String r8 = "last_bundle_index"
            java.lang.String r9 = "last_bundle_start_timestamp"
            java.lang.String r10 = "last_bundle_end_timestamp"
            java.lang.String r11 = "app_version"
            java.lang.String r12 = "app_store"
            java.lang.String r13 = "gmp_version"
            java.lang.String r14 = "dev_cert_hash"
            java.lang.String r15 = "measurement_enabled"
            java.lang.String r16 = "day"
            java.lang.String r17 = "daily_public_events_count"
            java.lang.String r18 = "daily_events_count"
            java.lang.String r19 = "daily_conversions_count"
            java.lang.String r20 = "config_fetched_time"
            java.lang.String r21 = "failed_config_fetch_time"
            java.lang.String r22 = "app_version_int"
            java.lang.String r23 = "firebase_instance_id"
            java.lang.String r24 = "daily_error_events_count"
            java.lang.String r25 = "daily_realtime_events_count"
            java.lang.String r26 = "health_monitor_sample"
            java.lang.String r27 = "android_id"
            java.lang.String r28 = "adid_reporting_enabled"
            java.lang.String r29 = "ssaid_reporting_enabled"
            java.lang.String r30 = "admob_app_id"
            java.lang.String r31 = "dynamite_version"
            java.lang.String r32 = "safelisted_events"
            java.lang.String r33 = "ga_app_id"
            java.lang.String[] r5 = new java.lang.String[]{r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29, r30, r31, r32, r33}     // Catch:{ SQLiteException -> 0x0203, all -> 0x01fe }
            java.lang.String r6 = "app_id=?"
            r0 = 1
            java.lang.String[] r7 = new java.lang.String[r0]     // Catch:{ SQLiteException -> 0x0203, all -> 0x01fe }
            r11 = 0
            r7[r11] = r1     // Catch:{ SQLiteException -> 0x0203, all -> 0x01fe }
            r8 = 0
            r9 = 0
            r10 = 0
            android.database.Cursor r3 = r3.query(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0203, all -> 0x01fe }
            boolean r4 = r3.moveToFirst()     // Catch:{ SQLiteException -> 0x01fa, all -> 0x01f6 }
            if (r4 != 0) goto L_0x006b
            if (r3 == 0) goto L_0x006a
            r3.close()
        L_0x006a:
            return r2
        L_0x006b:
            com.google.android.gms.measurement.internal.e5 r4 = new com.google.android.gms.measurement.internal.e5     // Catch:{ SQLiteException -> 0x01fa, all -> 0x01f6 }
            r5 = r34
            com.google.android.gms.measurement.internal.o9 r6 = r5.f5360b     // Catch:{ SQLiteException -> 0x01f4 }
            com.google.android.gms.measurement.internal.j5 r6 = r6.mo19239t()     // Catch:{ SQLiteException -> 0x01f4 }
            r4.<init>(r6, r1)     // Catch:{ SQLiteException -> 0x01f4 }
            java.lang.String r6 = r3.getString(r11)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18933a(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            java.lang.String r6 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18939b(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 2
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18950e(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 3
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18954g(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 4
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18931a(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 5
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18938b(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 6
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18955g(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 7
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18958h(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 8
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18946d(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 9
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18949e(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 10
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r7 != 0) goto L_0x00d9
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r6 == 0) goto L_0x00d7
            goto L_0x00d9
        L_0x00d7:
            r6 = 0
            goto L_0x00da
        L_0x00d9:
            r6 = 1
        L_0x00da:
            r4.mo18935a(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 11
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18964j(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 12
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18966k(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 13
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18968l(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 14
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18970m(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 15
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18957h(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 16
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18961i(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 17
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r7 == 0) goto L_0x011f
            r6 = -2147483648(0xffffffff80000000, double:NaN)
            goto L_0x0124
        L_0x011f:
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            long r6 = (long) r6     // Catch:{ SQLiteException -> 0x01f4 }
        L_0x0124:
            r4.mo18942c(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 18
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18953f(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 19
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18974o(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 20
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18972n(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 21
            java.lang.String r6 = r3.getString(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18962i(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 22
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r8 = 0
            if (r7 == 0) goto L_0x0157
            r6 = r8
            goto L_0x015b
        L_0x0157:
            long r6 = r3.getLong(r6)     // Catch:{ SQLiteException -> 0x01f4 }
        L_0x015b:
            r4.mo18976p(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 23
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r7 != 0) goto L_0x016f
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r6 == 0) goto L_0x016d
            goto L_0x016f
        L_0x016d:
            r6 = 0
            goto L_0x0170
        L_0x016f:
            r6 = 1
        L_0x0170:
            r4.mo18940b(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            r6 = 24
            boolean r7 = r3.isNull(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r7 != 0) goto L_0x0183
            int r6 = r3.getInt(r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r6 == 0) goto L_0x0182
            goto L_0x0183
        L_0x0182:
            r0 = 0
        L_0x0183:
            r4.mo18944c(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            r0 = 25
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18943c(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            r0 = 26
            boolean r6 = r3.isNull(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r6 == 0) goto L_0x0198
            goto L_0x019c
        L_0x0198:
            long r8 = r3.getLong(r0)     // Catch:{ SQLiteException -> 0x01f4 }
        L_0x019c:
            r4.mo18952f(r8)     // Catch:{ SQLiteException -> 0x01f4 }
            r0 = 27
            boolean r6 = r3.isNull(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r6 != 0) goto L_0x01b9
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            java.lang.String r6 = ","
            r7 = -1
            java.lang.String[] r0 = r0.split(r6, r7)     // Catch:{ SQLiteException -> 0x01f4 }
            java.util.List r0 = java.util.Arrays.asList(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18934a(r0)     // Catch:{ SQLiteException -> 0x01f4 }
        L_0x01b9:
            boolean r0 = com.google.android.gms.internal.measurement.C2774wa.m7753b()     // Catch:{ SQLiteException -> 0x01f4 }
            if (r0 == 0) goto L_0x01d4
            com.google.android.gms.measurement.internal.la r0 = r34.mo19013h()     // Catch:{ SQLiteException -> 0x01f4 }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r6 = com.google.android.gms.measurement.internal.C3135o.f5395G0     // Catch:{ SQLiteException -> 0x01f4 }
            boolean r0 = r0.mo19154e(r1, r6)     // Catch:{ SQLiteException -> 0x01f4 }
            if (r0 == 0) goto L_0x01d4
            r0 = 28
            java.lang.String r0 = r3.getString(r0)     // Catch:{ SQLiteException -> 0x01f4 }
            r4.mo18947d(r0)     // Catch:{ SQLiteException -> 0x01f4 }
        L_0x01d4:
            r4.mo18965k()     // Catch:{ SQLiteException -> 0x01f4 }
            boolean r0 = r3.moveToNext()     // Catch:{ SQLiteException -> 0x01f4 }
            if (r0 == 0) goto L_0x01ee
            com.google.android.gms.measurement.internal.f4 r0 = r34.mo19015l()     // Catch:{ SQLiteException -> 0x01f4 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ SQLiteException -> 0x01f4 }
            java.lang.String r6 = "Got multiple records for app, expected one. appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r35)     // Catch:{ SQLiteException -> 0x01f4 }
            r0.mo19043a(r6, r7)     // Catch:{ SQLiteException -> 0x01f4 }
        L_0x01ee:
            if (r3 == 0) goto L_0x01f3
            r3.close()
        L_0x01f3:
            return r4
        L_0x01f4:
            r0 = move-exception
            goto L_0x0207
        L_0x01f6:
            r0 = move-exception
            r5 = r34
            goto L_0x021f
        L_0x01fa:
            r0 = move-exception
            r5 = r34
            goto L_0x0207
        L_0x01fe:
            r0 = move-exception
            r5 = r34
            r3 = r2
            goto L_0x021f
        L_0x0203:
            r0 = move-exception
            r5 = r34
            r3 = r2
        L_0x0207:
            com.google.android.gms.measurement.internal.f4 r4 = r34.mo19015l()     // Catch:{ all -> 0x021e }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ all -> 0x021e }
            java.lang.String r6 = "Error querying app. appId"
            java.lang.Object r1 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r35)     // Catch:{ all -> 0x021e }
            r4.mo19044a(r6, r1, r0)     // Catch:{ all -> 0x021e }
            if (r3 == 0) goto L_0x021d
            r3.close()
        L_0x021d:
            return r2
        L_0x021e:
            r0 = move-exception
        L_0x021f:
            if (r3 == 0) goto L_0x0224
            r3.close()
        L_0x0224:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18858b(java.lang.String):com.google.android.gms.measurement.internal.e5");
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18851a(C3087k kVar) {
        C2258v.m5629a(kVar);
        mo18881c();
        mo19284q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", kVar.f5278a);
        contentValues.put("name", kVar.f5279b);
        contentValues.put("lifetime_count", Long.valueOf(kVar.f5280c));
        contentValues.put("current_bundle_count", Long.valueOf(kVar.f5281d));
        contentValues.put("last_fire_timestamp", Long.valueOf(kVar.f5283f));
        contentValues.put("last_bundled_timestamp", Long.valueOf(kVar.f5284g));
        contentValues.put("last_bundled_day", kVar.f5285h);
        contentValues.put("last_sampled_complex_event_id", kVar.f5286i);
        contentValues.put("last_sampling_rate", kVar.f5287j);
        if (mo19013h().mo19154e(kVar.f5278a, C3135o.f5459m0)) {
            contentValues.put("current_session_count", Long.valueOf(kVar.f5282e));
        }
        Boolean bool = kVar.f5288k;
        contentValues.put("last_exempt_from_sampling", (bool == null || !bool.booleanValue()) ? null : 1L);
        try {
            if (mo18875v().insertWithOnConflict("events", null, contentValues, 5) == -1) {
                mo19015l().mo19001t().mo19043a("Failed to insert/update event aggregates (got -1). appId", C3032f4.m8621a(kVar.f5278a));
            }
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing event aggregates. appId", C3032f4.m8621a(kVar.f5278a), e);
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final boolean mo18855a(C3234w9 w9Var) {
        C2258v.m5629a(w9Var);
        mo18881c();
        mo19284q();
        if (mo18863c(w9Var.f5733a, w9Var.f5735c) == null) {
            if (C3267z9.m9429e(w9Var.f5735c)) {
                if (m8452b("select count(1) from user_attributes where app_id=? and name not like '!_%' escape '!'", new String[]{w9Var.f5733a}) >= 25) {
                    return false;
                }
            } else if (!mo19013h().mo19154e(w9Var.f5733a, C3135o.f5435a0)) {
                if (m8452b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{w9Var.f5733a, w9Var.f5734b}) >= 25) {
                    return false;
                }
            } else if (!"_npa".equals(w9Var.f5735c)) {
                if (m8452b("select count(1) from user_attributes where app_id=? and origin=? AND name like '!_%' escape '!'", new String[]{w9Var.f5733a, w9Var.f5734b}) >= 25) {
                    return false;
                }
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", w9Var.f5733a);
        contentValues.put(TtmlNode.ATTR_TTS_ORIGIN, w9Var.f5734b);
        contentValues.put("name", w9Var.f5735c);
        contentValues.put("set_timestamp", Long.valueOf(w9Var.f5736d));
        m8449a(contentValues, "value", w9Var.f5737e);
        try {
            if (mo18875v().insertWithOnConflict("user_attributes", null, contentValues, 5) == -1) {
                mo19015l().mo19001t().mo19043a("Failed to insert/update user property (got -1). appId", C3032f4.m8621a(w9Var.f5733a));
            }
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing user property. appId", C3032f4.m8621a(w9Var.f5733a), e);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo18861b(String str, List<C2429b0> list) {
        boolean z;
        boolean z2;
        String str2 = str;
        List<C2429b0> list2 = list;
        C2258v.m5629a(list);
        for (int i = 0; i < list.size(); i++) {
            C2429b0.C2430a aVar = (C2429b0.C2430a) list2.get(i).mo17669j();
            if (aVar.mo17307k() != 0) {
                for (int i2 = 0; i2 < aVar.mo17307k(); i2++) {
                    C2445c0.C2446a aVar2 = (C2445c0.C2446a) aVar.mo17305b(i2).mo17669j();
                    C2445c0.C2446a aVar3 = (C2445c0.C2446a) aVar2.clone();
                    String a = C3082j6.m8797a(aVar2.mo17361j());
                    if (a != null) {
                        aVar3.mo17359a(a);
                        z2 = true;
                    } else {
                        z2 = false;
                    }
                    boolean z3 = z2;
                    for (int i3 = 0; i3 < aVar2.mo17362k(); i3++) {
                        C2461d0 a2 = aVar2.mo17360a(i3);
                        String a3 = C3070i6.m8732a(a2.mo17395v());
                        if (a3 != null) {
                            C2461d0.C2462a aVar4 = (C2461d0.C2462a) a2.mo17669j();
                            aVar4.mo17396a(a3);
                            aVar3.mo17358a(i3, (C2461d0) aVar4.mo17679i());
                            z3 = true;
                        }
                    }
                    if (z3) {
                        aVar.mo17302a(i2, aVar3);
                        list2.set(i, (C2429b0) aVar.mo17679i());
                    }
                }
            }
            if (aVar.mo17306j() != 0) {
                for (int i4 = 0; i4 < aVar.mo17306j(); i4++) {
                    C2494f0 a4 = aVar.mo17304a(i4);
                    String a5 = C3106l6.m8850a(a4.mo17460p());
                    if (a5 != null) {
                        C2494f0.C2495a aVar5 = (C2494f0.C2495a) a4.mo17669j();
                        aVar5.mo17466a(a5);
                        aVar.mo17303a(i4, aVar5);
                        list2.set(i, (C2429b0) aVar.mo17679i());
                    }
                }
            }
        }
        mo19284q();
        mo18881c();
        C2258v.m5639b(str);
        C2258v.m5629a(list);
        SQLiteDatabase v = mo18875v();
        v.beginTransaction();
        try {
            mo19284q();
            mo18881c();
            C2258v.m5639b(str);
            SQLiteDatabase v2 = mo18875v();
            v2.delete("property_filters", "app_id=?", new String[]{str2});
            v2.delete("event_filters", "app_id=?", new String[]{str2});
            for (C2429b0 b0Var : list) {
                mo19284q();
                mo18881c();
                C2258v.m5639b(str);
                C2258v.m5629a(b0Var);
                if (!b0Var.mo17296n()) {
                    mo19015l().mo19004w().mo19043a("Audience with no ID. appId", C3032f4.m8621a(str));
                } else {
                    int o = b0Var.mo17297o();
                    Iterator<C2445c0> it = b0Var.mo17300s().iterator();
                    while (true) {
                        if (it.hasNext()) {
                            if (!it.next().mo17347n()) {
                                mo19015l().mo19004w().mo19044a("Event filter with no ID. Audience definition ignored. appId, audienceId", C3032f4.m8621a(str), Integer.valueOf(o));
                                break;
                            }
                        } else {
                            Iterator<C2494f0> it2 = b0Var.mo17298p().iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (!it2.next().mo17458n()) {
                                        mo19015l().mo19004w().mo19044a("Property filter with no ID. Audience definition ignored. appId, audienceId", C3032f4.m8621a(str), Integer.valueOf(o));
                                        break;
                                    }
                                } else {
                                    Iterator<C2445c0> it3 = b0Var.mo17300s().iterator();
                                    while (true) {
                                        if (it3.hasNext()) {
                                            if (!m8450a(str2, o, it3.next())) {
                                                z = false;
                                                break;
                                            }
                                        } else {
                                            z = true;
                                            break;
                                        }
                                    }
                                    if (z) {
                                        Iterator<C2494f0> it4 = b0Var.mo17298p().iterator();
                                        while (true) {
                                            if (it4.hasNext()) {
                                                if (!m8451a(str2, o, it4.next())) {
                                                    z = false;
                                                    break;
                                                }
                                            } else {
                                                break;
                                            }
                                        }
                                    }
                                    if (!z) {
                                        mo19284q();
                                        mo18881c();
                                        C2258v.m5639b(str);
                                        SQLiteDatabase v3 = mo18875v();
                                        v3.delete("property_filters", "app_id=? and audience_id=?", new String[]{str2, String.valueOf(o)});
                                        v3.delete("event_filters", "app_id=? and audience_id=?", new String[]{str2, String.valueOf(o)});
                                    }
                                }
                            }
                        }
                    }
                }
            }
            ArrayList arrayList = new ArrayList();
            for (C2429b0 b0Var2 : list) {
                arrayList.add(b0Var2.mo17296n() ? Integer.valueOf(b0Var2.mo17297o()) : null);
            }
            m8453c(str2, arrayList);
            v.setTransactionSuccessful();
        } finally {
            v.endTransaction();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:30:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a2  */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.measurement.internal.C3234w9> mo18845a(java.lang.String r14) {
        /*
            r13 = this;
            com.google.android.gms.common.internal.C2258v.m5639b(r14)
            r13.mo18881c()
            r13.mo19284q()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            android.database.sqlite.SQLiteDatabase r2 = r13.mo18875v()     // Catch:{ SQLiteException -> 0x0086, all -> 0x0083 }
            java.lang.String r3 = "user_attributes"
            java.lang.String r4 = "name"
            java.lang.String r5 = "origin"
            java.lang.String r6 = "set_timestamp"
            java.lang.String r7 = "value"
            java.lang.String[] r4 = new java.lang.String[]{r4, r5, r6, r7}     // Catch:{ SQLiteException -> 0x0086, all -> 0x0083 }
            java.lang.String r5 = "app_id=?"
            r11 = 1
            java.lang.String[] r6 = new java.lang.String[r11]     // Catch:{ SQLiteException -> 0x0086, all -> 0x0083 }
            r12 = 0
            r6[r12] = r14     // Catch:{ SQLiteException -> 0x0086, all -> 0x0083 }
            r7 = 0
            r8 = 0
            java.lang.String r9 = "rowid"
            java.lang.String r10 = "1000"
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ SQLiteException -> 0x0086, all -> 0x0083 }
            boolean r3 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x0081 }
            if (r3 != 0) goto L_0x0040
            if (r2 == 0) goto L_0x003f
            r2.close()
        L_0x003f:
            return r0
        L_0x0040:
            java.lang.String r7 = r2.getString(r12)     // Catch:{ SQLiteException -> 0x0081 }
            java.lang.String r3 = r2.getString(r11)     // Catch:{ SQLiteException -> 0x0081 }
            if (r3 != 0) goto L_0x004c
            java.lang.String r3 = ""
        L_0x004c:
            r6 = r3
            r3 = 2
            long r8 = r2.getLong(r3)     // Catch:{ SQLiteException -> 0x0081 }
            r3 = 3
            java.lang.Object r10 = r13.m8448a(r2, r3)     // Catch:{ SQLiteException -> 0x0081 }
            if (r10 != 0) goto L_0x006b
            com.google.android.gms.measurement.internal.f4 r3 = r13.mo19015l()     // Catch:{ SQLiteException -> 0x0081 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ SQLiteException -> 0x0081 }
            java.lang.String r4 = "Read invalid user property value, ignoring it. appId"
            java.lang.Object r5 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r14)     // Catch:{ SQLiteException -> 0x0081 }
            r3.mo19043a(r4, r5)     // Catch:{ SQLiteException -> 0x0081 }
            goto L_0x0075
        L_0x006b:
            com.google.android.gms.measurement.internal.w9 r3 = new com.google.android.gms.measurement.internal.w9     // Catch:{ SQLiteException -> 0x0081 }
            r4 = r3
            r5 = r14
            r4.<init>(r5, r6, r7, r8, r10)     // Catch:{ SQLiteException -> 0x0081 }
            r0.add(r3)     // Catch:{ SQLiteException -> 0x0081 }
        L_0x0075:
            boolean r3 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x0081 }
            if (r3 != 0) goto L_0x0040
            if (r2 == 0) goto L_0x0080
            r2.close()
        L_0x0080:
            return r0
        L_0x0081:
            r0 = move-exception
            goto L_0x0088
        L_0x0083:
            r14 = move-exception
            r2 = r1
            goto L_0x00a0
        L_0x0086:
            r0 = move-exception
            r2 = r1
        L_0x0088:
            com.google.android.gms.measurement.internal.f4 r3 = r13.mo19015l()     // Catch:{ all -> 0x009f }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x009f }
            java.lang.String r4 = "Error querying user properties. appId"
            java.lang.Object r14 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r14)     // Catch:{ all -> 0x009f }
            r3.mo19044a(r4, r14, r0)     // Catch:{ all -> 0x009f }
            if (r2 == 0) goto L_0x009e
            r2.close()
        L_0x009e:
            return r1
        L_0x009f:
            r14 = move-exception
        L_0x00a0:
            if (r2 == 0) goto L_0x00a5
            r2.close()
        L_0x00a5:
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18845a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00f9, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x0101, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0105, code lost:
        r0 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0101 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:1:0x000f] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0120  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x0128  */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.measurement.internal.C3234w9> mo18847a(java.lang.String r22, java.lang.String r23, java.lang.String r24) {
        /*
            r21 = this;
            com.google.android.gms.common.internal.C2258v.m5639b(r22)
            r21.mo18881c()
            r21.mo19284q()
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r1 = 0
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x0105, all -> 0x0101 }
            r3 = 3
            r2.<init>(r3)     // Catch:{ SQLiteException -> 0x0105, all -> 0x0101 }
            r11 = r22
            r2.add(r11)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x0101 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ SQLiteException -> 0x00fd, all -> 0x0101 }
            java.lang.String r5 = "app_id=?"
            r4.<init>(r5)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x0101 }
            boolean r5 = android.text.TextUtils.isEmpty(r23)     // Catch:{ SQLiteException -> 0x00fd, all -> 0x0101 }
            if (r5 != 0) goto L_0x0032
            r5 = r23
            r2.add(r5)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.String r6 = " and origin=?"
            r4.append(r6)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            goto L_0x0034
        L_0x0032:
            r5 = r23
        L_0x0034:
            boolean r6 = android.text.TextUtils.isEmpty(r24)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            if (r6 != 0) goto L_0x004c
            java.lang.String r6 = java.lang.String.valueOf(r24)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.String r7 = "*"
            java.lang.String r6 = r6.concat(r7)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            r2.add(r6)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.String r6 = " and name glob ?"
            r4.append(r6)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
        L_0x004c:
            int r6 = r2.size()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.String[] r6 = new java.lang.String[r6]     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.Object[] r2 = r2.toArray(r6)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            r16 = r2
            java.lang.String[] r16 = (java.lang.String[]) r16     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            android.database.sqlite.SQLiteDatabase r12 = r21.mo18875v()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.String r13 = "user_attributes"
            java.lang.String r2 = "name"
            java.lang.String r6 = "set_timestamp"
            java.lang.String r7 = "value"
            java.lang.String r8 = "origin"
            java.lang.String[] r14 = new java.lang.String[]{r2, r6, r7, r8}     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            java.lang.String r15 = r4.toString()     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            r17 = 0
            r18 = 0
            java.lang.String r19 = "rowid"
            java.lang.String r20 = "1001"
            android.database.Cursor r2 = r12.query(r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ SQLiteException -> 0x00f9, all -> 0x0101 }
            boolean r4 = r2.moveToFirst()     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            if (r4 != 0) goto L_0x0089
            if (r2 == 0) goto L_0x0088
            r2.close()
        L_0x0088:
            return r0
        L_0x0089:
            int r4 = r0.size()     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            r6 = 1000(0x3e8, float:1.401E-42)
            if (r4 < r6) goto L_0x00a5
            com.google.android.gms.measurement.internal.f4 r3 = r21.mo19015l()     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            java.lang.String r4 = "Read more than the max allowed user properties, ignoring excess"
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            r3.mo19043a(r4, r6)     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            r12 = r21
            goto L_0x00e4
        L_0x00a5:
            r4 = 0
            java.lang.String r7 = r2.getString(r4)     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            r4 = 1
            long r8 = r2.getLong(r4)     // Catch:{ SQLiteException -> 0x00f5, all -> 0x00f1 }
            r4 = 2
            r12 = r21
            java.lang.Object r10 = r12.m8448a(r2, r4)     // Catch:{ SQLiteException -> 0x00ef }
            java.lang.String r13 = r2.getString(r3)     // Catch:{ SQLiteException -> 0x00ef }
            if (r10 != 0) goto L_0x00d0
            com.google.android.gms.measurement.internal.f4 r4 = r21.mo19015l()     // Catch:{ SQLiteException -> 0x00ec }
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()     // Catch:{ SQLiteException -> 0x00ec }
            java.lang.String r5 = "(2)Read invalid user property value, ignoring it"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r22)     // Catch:{ SQLiteException -> 0x00ec }
            r14 = r24
            r4.mo19045a(r5, r6, r13, r14)     // Catch:{ SQLiteException -> 0x00ec }
            goto L_0x00de
        L_0x00d0:
            r14 = r24
            com.google.android.gms.measurement.internal.w9 r15 = new com.google.android.gms.measurement.internal.w9     // Catch:{ SQLiteException -> 0x00ec }
            r4 = r15
            r5 = r22
            r6 = r13
            r4.<init>(r5, r6, r7, r8, r10)     // Catch:{ SQLiteException -> 0x00ec }
            r0.add(r15)     // Catch:{ SQLiteException -> 0x00ec }
        L_0x00de:
            boolean r4 = r2.moveToNext()     // Catch:{ SQLiteException -> 0x00ec }
            if (r4 != 0) goto L_0x00ea
        L_0x00e4:
            if (r2 == 0) goto L_0x00e9
            r2.close()
        L_0x00e9:
            return r0
        L_0x00ea:
            r5 = r13
            goto L_0x0089
        L_0x00ec:
            r0 = move-exception
            r5 = r13
            goto L_0x010d
        L_0x00ef:
            r0 = move-exception
            goto L_0x010d
        L_0x00f1:
            r0 = move-exception
            r12 = r21
            goto L_0x0125
        L_0x00f5:
            r0 = move-exception
            r12 = r21
            goto L_0x010d
        L_0x00f9:
            r0 = move-exception
            r12 = r21
            goto L_0x010c
        L_0x00fd:
            r0 = move-exception
            r12 = r21
            goto L_0x010a
        L_0x0101:
            r0 = move-exception
            r12 = r21
            goto L_0x0126
        L_0x0105:
            r0 = move-exception
            r12 = r21
            r11 = r22
        L_0x010a:
            r5 = r23
        L_0x010c:
            r2 = r1
        L_0x010d:
            com.google.android.gms.measurement.internal.f4 r3 = r21.mo19015l()     // Catch:{ all -> 0x0124 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ all -> 0x0124 }
            java.lang.String r4 = "(2)Error querying user properties"
            java.lang.Object r6 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r22)     // Catch:{ all -> 0x0124 }
            r3.mo19045a(r4, r6, r5, r0)     // Catch:{ all -> 0x0124 }
            if (r2 == 0) goto L_0x0123
            r2.close()
        L_0x0123:
            return r1
        L_0x0124:
            r0 = move-exception
        L_0x0125:
            r1 = r2
        L_0x0126:
            if (r1 == 0) goto L_0x012b
            r1.close()
        L_0x012b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18847a(java.lang.String, java.lang.String, java.lang.String):java.util.List");
    }

    @WorkerThread
    /* renamed from: a */
    public final boolean mo18856a(zzv zzv) {
        C2258v.m5629a(zzv);
        mo18881c();
        mo19284q();
        if (mo18863c(zzv.f5836P, zzv.f5838R.f5808Q) == null) {
            if (m8452b("SELECT COUNT(1) FROM conditional_properties WHERE app_id=?", new String[]{zzv.f5836P}) >= 1000) {
                return false;
            }
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", zzv.f5836P);
        contentValues.put(TtmlNode.ATTR_TTS_ORIGIN, zzv.f5837Q);
        contentValues.put("name", zzv.f5838R.f5808Q);
        m8449a(contentValues, "value", zzv.f5838R.mo19469a());
        contentValues.put("active", Boolean.valueOf(zzv.f5840T));
        contentValues.put("trigger_event_name", zzv.f5841U);
        contentValues.put("trigger_timeout", Long.valueOf(zzv.f5843W));
        mo19011f();
        contentValues.put("timed_out_event", C3267z9.m9422a((Parcelable) zzv.f5842V));
        contentValues.put("creation_timestamp", Long.valueOf(zzv.f5839S));
        mo19011f();
        contentValues.put("triggered_event", C3267z9.m9422a((Parcelable) zzv.f5844X));
        contentValues.put("triggered_timestamp", Long.valueOf(zzv.f5838R.f5809R));
        contentValues.put("time_to_live", Long.valueOf(zzv.f5845Y));
        mo19011f();
        contentValues.put("expired_event", C3267z9.m9422a((Parcelable) zzv.f5846Z));
        try {
            if (mo18875v().insertWithOnConflict("conditional_properties", null, contentValues, 5) == -1) {
                mo19015l().mo19001t().mo19043a("Failed to insert/update conditional user property (got -1)", C3032f4.m8621a(zzv.f5836P));
            }
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing conditional user property", C3032f4.m8621a(zzv.f5836P), e);
        }
        return true;
    }

    /* renamed from: a */
    public final List<zzv> mo18848a(String str, String[] strArr) {
        mo18881c();
        mo19284q();
        ArrayList arrayList = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = mo18875v().query("conditional_properties", new String[]{"app_id", TtmlNode.ATTR_TTS_ORIGIN, "name", "value", "active", "trigger_event_name", "trigger_timeout", "timed_out_event", "creation_timestamp", "triggered_event", "triggered_timestamp", "time_to_live", "expired_event"}, str, strArr, null, null, "rowid", "1001");
            if (!cursor.moveToFirst()) {
                if (cursor != null) {
                    cursor.close();
                }
                return arrayList;
            }
            while (true) {
                if (arrayList.size() < 1000) {
                    boolean z = false;
                    String string = cursor.getString(0);
                    String string2 = cursor.getString(1);
                    String string3 = cursor.getString(2);
                    Object a = m8448a(cursor, 3);
                    if (cursor.getInt(4) != 0) {
                        z = true;
                    }
                    String string4 = cursor.getString(5);
                    long j = cursor.getLong(6);
                    long j2 = cursor.getLong(8);
                    long j3 = cursor.getLong(10);
                    boolean z2 = z;
                    zzv zzv = r3;
                    zzv zzv2 = new zzv(string, string2, new zzkq(string3, j3, a, string2), j2, z2, string4, (zzan) mo19171i().mo19352a(cursor.getBlob(7), zzan.CREATOR), j, (zzan) mo19171i().mo19352a(cursor.getBlob(9), zzan.CREATOR), cursor.getLong(11), (zzan) mo19171i().mo19352a(cursor.getBlob(12), zzan.CREATOR));
                    arrayList.add(zzv);
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } else {
                    mo19015l().mo19001t().mo19043a("Read more than the max allowed conditional properties, ignoring extra", 1000);
                    break;
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19043a("Error querying conditional user property value", e);
            List<zzv> emptyList = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18850a(C3021e5 e5Var) {
        C2258v.m5629a(e5Var);
        mo18881c();
        mo19284q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", e5Var.mo18967l());
        contentValues.put("app_instance_id", e5Var.mo18969m());
        contentValues.put("gmp_app_id", e5Var.mo18971n());
        contentValues.put("resettable_device_id_hash", e5Var.mo18977q());
        contentValues.put("last_bundle_index", Long.valueOf(e5Var.mo18923B()));
        contentValues.put("last_bundle_start_timestamp", Long.valueOf(e5Var.mo18979s()));
        contentValues.put("last_bundle_end_timestamp", Long.valueOf(e5Var.mo18980t()));
        contentValues.put("app_version", e5Var.mo18981u());
        contentValues.put("app_store", e5Var.mo18983w());
        contentValues.put("gmp_version", Long.valueOf(e5Var.mo18984x()));
        contentValues.put("dev_cert_hash", Long.valueOf(e5Var.mo18985y()));
        contentValues.put("measurement_enabled", Boolean.valueOf(e5Var.mo18922A()));
        contentValues.put("day", Long.valueOf(e5Var.mo18927F()));
        contentValues.put("daily_public_events_count", Long.valueOf(e5Var.mo18928G()));
        contentValues.put("daily_events_count", Long.valueOf(e5Var.mo18929H()));
        contentValues.put("daily_conversions_count", Long.valueOf(e5Var.mo18930I()));
        contentValues.put("config_fetched_time", Long.valueOf(e5Var.mo18924C()));
        contentValues.put("failed_config_fetch_time", Long.valueOf(e5Var.mo18925D()));
        contentValues.put("app_version_int", Long.valueOf(e5Var.mo18982v()));
        contentValues.put("firebase_instance_id", e5Var.mo18978r());
        contentValues.put("daily_error_events_count", Long.valueOf(e5Var.mo18941c()));
        contentValues.put("daily_realtime_events_count", Long.valueOf(e5Var.mo18937b()));
        contentValues.put("health_monitor_sample", e5Var.mo18945d());
        contentValues.put("android_id", Long.valueOf(e5Var.mo18951f()));
        contentValues.put("adid_reporting_enabled", Boolean.valueOf(e5Var.mo18956g()));
        contentValues.put("ssaid_reporting_enabled", Boolean.valueOf(e5Var.mo18959h()));
        contentValues.put("admob_app_id", e5Var.mo18973o());
        contentValues.put("dynamite_version", Long.valueOf(e5Var.mo18986z()));
        if (e5Var.mo18963j() != null) {
            if (e5Var.mo18963j().size() == 0) {
                mo19015l().mo19004w().mo19043a("Safelisted events should not be an empty list. appId", e5Var.mo18967l());
            } else {
                contentValues.put("safelisted_events", TextUtils.join(",", e5Var.mo18963j()));
            }
        }
        if (C2774wa.m7753b() && mo19013h().mo19154e(e5Var.mo18967l(), C3135o.f5395G0)) {
            contentValues.put("ga_app_id", e5Var.mo18975p());
        }
        try {
            SQLiteDatabase v = mo18875v();
            if (((long) v.update("apps", contentValues, "app_id = ?", new String[]{e5Var.mo18967l()})) == 0 && v.insertWithOnConflict("apps", null, contentValues, 5) == -1) {
                mo19015l().mo19001t().mo19043a("Failed to insert/update app (got -1). appId", C3032f4.m8621a(e5Var.mo18967l()));
            }
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing app. appId", C3032f4.m8621a(e5Var.mo18967l()), e);
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final C2991c mo18842a(long j, String str, boolean z, boolean z2, boolean z3, boolean z4, boolean z5) {
        C2258v.m5639b(str);
        mo18881c();
        mo19284q();
        String[] strArr = {str};
        C2991c cVar = new C2991c();
        Cursor cursor = null;
        try {
            SQLiteDatabase v = mo18875v();
            cursor = v.query("apps", new String[]{"day", "daily_events_count", "daily_public_events_count", "daily_conversions_count", "daily_error_events_count", "daily_realtime_events_count"}, "app_id=?", new String[]{str}, null, null, null);
            if (!cursor.moveToFirst()) {
                mo19015l().mo19004w().mo19043a("Not updating daily counts, app is not known. appId", C3032f4.m8621a(str));
                if (cursor != null) {
                    cursor.close();
                }
                return cVar;
            }
            if (cursor.getLong(0) == j) {
                cVar.f4995b = cursor.getLong(1);
                cVar.f4994a = cursor.getLong(2);
                cVar.f4996c = cursor.getLong(3);
                cVar.f4997d = cursor.getLong(4);
                cVar.f4998e = cursor.getLong(5);
            }
            if (z) {
                cVar.f4995b++;
            }
            if (z2) {
                cVar.f4994a++;
            }
            if (z3) {
                cVar.f4996c++;
            }
            if (z4) {
                cVar.f4997d++;
            }
            if (z5) {
                cVar.f4998e++;
            }
            ContentValues contentValues = new ContentValues();
            contentValues.put("day", Long.valueOf(j));
            contentValues.put("daily_public_events_count", Long.valueOf(cVar.f4994a));
            contentValues.put("daily_events_count", Long.valueOf(cVar.f4995b));
            contentValues.put("daily_conversions_count", Long.valueOf(cVar.f4996c));
            contentValues.put("daily_error_events_count", Long.valueOf(cVar.f4997d));
            contentValues.put("daily_realtime_events_count", Long.valueOf(cVar.f4998e));
            v.update("apps", contentValues, "app_id=?", strArr);
            if (cursor != null) {
                cursor.close();
            }
            return cVar;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error updating daily counts. appId", C3032f4.m8621a(str), e);
            if (cursor != null) {
                cursor.close();
            }
            return cVar;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final boolean mo18853a(C2763w0 w0Var, boolean z) {
        mo18881c();
        mo19284q();
        C2258v.m5629a(w0Var);
        C2258v.m5639b(w0Var.mo18052p0());
        C2258v.m5640b(w0Var.mo18037d0());
        mo18835B();
        long a = mo19017o().mo17132a();
        if (w0Var.mo18038e0() < a - C3110la.m8856v() || w0Var.mo18038e0() > C3110la.m8856v() + a) {
            mo19015l().mo19004w().mo19045a("Storing bundle outside of the max uploading time span. appId, now, timestamp", C3032f4.m8621a(w0Var.mo18052p0()), Long.valueOf(a), Long.valueOf(w0Var.mo18038e0()));
        }
        try {
            byte[] c = mo19171i().mo19362c(w0Var.mo18129f());
            mo19015l().mo18996B().mo19043a("Saving bundle, size", Integer.valueOf(c.length));
            ContentValues contentValues = new ContentValues();
            contentValues.put("app_id", w0Var.mo18052p0());
            contentValues.put("bundle_end_timestamp", Long.valueOf(w0Var.mo18038e0()));
            contentValues.put("data", c);
            contentValues.put("has_realtime", Integer.valueOf(z ? 1 : 0));
            if (w0Var.mo18020O()) {
                contentValues.put("retry_count", Integer.valueOf(w0Var.mo18022Q()));
            }
            try {
                if (mo18875v().insert("queue", null, contentValues) != -1) {
                    return true;
                }
                mo19015l().mo19001t().mo19043a("Failed to insert bundle (got -1). appId", C3032f4.m8621a(w0Var.mo18052p0()));
                return false;
            } catch (SQLiteException e) {
                mo19015l().mo19001t().mo19044a("Error storing bundle. appId", C3032f4.m8621a(w0Var.mo18052p0()), e);
                return false;
            }
        } catch (IOException e2) {
            mo19015l().mo19001t().mo19044a("Data loss. Failed to serialize bundle. appId", C3032f4.m8621a(w0Var.mo18052p0()), e2);
            return false;
        }
    }

    @WorkerThread
    /* renamed from: a */
    public final List<Pair<C2763w0, Long>> mo18846a(String str, int i, int i2) {
        mo18881c();
        mo19284q();
        C2258v.m5636a(i > 0);
        C2258v.m5636a(i2 > 0);
        C2258v.m5639b(str);
        Cursor cursor = null;
        try {
            cursor = mo18875v().query("queue", new String[]{"rowid", "data", "retry_count"}, "app_id=?", new String[]{str}, null, null, "rowid", String.valueOf(i));
            if (!cursor.moveToFirst()) {
                List<Pair<C2763w0, Long>> emptyList = Collections.emptyList();
                if (cursor != null) {
                    cursor.close();
                }
                return emptyList;
            }
            ArrayList arrayList = new ArrayList();
            int i3 = 0;
            do {
                long j = cursor.getLong(0);
                try {
                    byte[] b = mo19171i().mo19361b(cursor.getBlob(1));
                    if (!arrayList.isEmpty() && b.length + i3 > i2) {
                        break;
                    }
                    try {
                        C2763w0.C2764a s0 = C2763w0.m7555s0();
                        C3223v9.m9262a(s0, b);
                        C2763w0.C2764a aVar = s0;
                        if (!cursor.isNull(2)) {
                            aVar.mo18099i(cursor.getInt(2));
                        }
                        i3 += b.length;
                        arrayList.add(Pair.create((C2763w0) ((C2595l4) aVar.mo17679i()), Long.valueOf(j)));
                    } catch (IOException e) {
                        mo19015l().mo19001t().mo19044a("Failed to merge queued bundle. appId", C3032f4.m8621a(str), e);
                    }
                    if (!cursor.moveToNext()) {
                        break;
                    }
                } catch (IOException e2) {
                    mo19015l().mo19001t().mo19044a("Failed to unzip queued bundle. appId", C3032f4.m8621a(str), e2);
                }
            } while (i3 <= i2);
            if (cursor != null) {
                cursor.close();
            }
            return arrayList;
        } catch (SQLiteException e3) {
            mo19015l().mo19001t().mo19044a("Error querying bundles. appId", C3032f4.m8621a(str), e3);
            List<Pair<C2763w0, Long>> emptyList2 = Collections.emptyList();
            if (cursor != null) {
                cursor.close();
            }
            return emptyList2;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo18852a(List<Long> list) {
        mo18881c();
        mo19284q();
        C2258v.m5629a(list);
        C2258v.m5628a(list.size());
        if (m8445O()) {
            String join = TextUtils.join(",", list);
            StringBuilder sb = new StringBuilder(String.valueOf(join).length() + 2);
            sb.append("(");
            sb.append(join);
            sb.append(")");
            String sb2 = sb.toString();
            StringBuilder sb3 = new StringBuilder(String.valueOf(sb2).length() + 80);
            sb3.append("SELECT COUNT(1) FROM queue WHERE rowid IN ");
            sb3.append(sb2);
            sb3.append(" AND retry_count =  2147483647 LIMIT 1");
            if (m8452b(sb3.toString(), (String[]) null) > 0) {
                mo19015l().mo19004w().mo19042a("The number of upload retries exceeds the limit. Will remain unchanged.");
            }
            try {
                SQLiteDatabase v = mo18875v();
                StringBuilder sb4 = new StringBuilder(String.valueOf(sb2).length() + 127);
                sb4.append("UPDATE queue SET retry_count = IFNULL(retry_count, 0) + 1 WHERE rowid IN ");
                sb4.append(sb2);
                sb4.append(" AND (retry_count IS NULL OR retry_count < 2147483647)");
                v.execSQL(sb4.toString());
            } catch (SQLiteException e) {
                mo19015l().mo19001t().mo19043a("Error incrementing retry count. error", e);
            }
        }
    }

    @WorkerThread
    /* renamed from: a */
    private final boolean m8450a(String str, int i, C2445c0 c0Var) {
        mo19284q();
        mo18881c();
        C2258v.m5639b(str);
        C2258v.m5629a(c0Var);
        Integer num = null;
        if (TextUtils.isEmpty(c0Var.mo17349p())) {
            C3056h4 w = mo19015l().mo19004w();
            Object a = C3032f4.m8621a(str);
            Integer valueOf = Integer.valueOf(i);
            if (c0Var.mo17347n()) {
                num = Integer.valueOf(c0Var.mo17348o());
            }
            w.mo19045a("Event filter had no event name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] f = c0Var.mo18129f();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i));
        contentValues.put("filter_id", c0Var.mo17347n() ? Integer.valueOf(c0Var.mo17348o()) : null);
        contentValues.put("event_name", c0Var.mo17349p());
        if (mo19013h().mo19154e(str, C3135o.f5457l0)) {
            contentValues.put("session_scoped", c0Var.mo17356x() ? Boolean.valueOf(c0Var.mo17357y()) : null);
        }
        contentValues.put("data", f);
        try {
            if (mo18875v().insertWithOnConflict("event_filters", null, contentValues, 5) != -1) {
                return true;
            }
            mo19015l().mo19001t().mo19043a("Failed to insert event filter (got -1). appId", C3032f4.m8621a(str));
            return true;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing event filter. appId", C3032f4.m8621a(str), e);
            return false;
        }
    }

    @WorkerThread
    /* renamed from: a */
    private final boolean m8451a(String str, int i, C2494f0 f0Var) {
        mo19284q();
        mo18881c();
        C2258v.m5639b(str);
        C2258v.m5629a(f0Var);
        Integer num = null;
        if (TextUtils.isEmpty(f0Var.mo17460p())) {
            C3056h4 w = mo19015l().mo19004w();
            Object a = C3032f4.m8621a(str);
            Integer valueOf = Integer.valueOf(i);
            if (f0Var.mo17458n()) {
                num = Integer.valueOf(f0Var.mo17459o());
            }
            w.mo19045a("Property filter had no property name. Audience definition ignored. appId, audienceId, filterId", a, valueOf, String.valueOf(num));
            return false;
        }
        byte[] f = f0Var.mo18129f();
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("audience_id", Integer.valueOf(i));
        contentValues.put("filter_id", f0Var.mo17458n() ? Integer.valueOf(f0Var.mo17459o()) : null);
        contentValues.put("property_name", f0Var.mo17460p());
        if (mo19013h().mo19154e(str, C3135o.f5457l0)) {
            contentValues.put("session_scoped", f0Var.mo17464u() ? Boolean.valueOf(f0Var.mo17465v()) : null);
        }
        contentValues.put("data", f);
        try {
            if (mo18875v().insertWithOnConflict("property_filters", null, contentValues, 5) != -1) {
                return true;
            }
            mo19015l().mo19001t().mo19043a("Failed to insert property filter (got -1). appId", C3032f4.m8621a(str));
            return false;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing property filter. appId", C3032f4.m8621a(str), e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c3  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00ca  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.Map<java.lang.Integer, java.util.List<java.lang.Integer>> mo18849a(java.lang.String r13, java.util.List<java.lang.String> r14) {
        /*
            r12 = this;
            r12.mo19284q()
            r12.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r13)
            com.google.android.gms.common.internal.C2258v.m5629a(r14)
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            boolean r1 = r14.isEmpty()
            if (r1 == 0) goto L_0x0018
            return r0
        L_0x0018:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "app_id=? AND property_name in ("
            r1.append(r2)
            r2 = 0
            r3 = 0
        L_0x0024:
            int r4 = r14.size()
            if (r3 >= r4) goto L_0x0039
            if (r3 <= 0) goto L_0x0031
            java.lang.String r4 = ","
            r1.append(r4)
        L_0x0031:
            java.lang.String r4 = "?"
            r1.append(r4)
            int r3 = r3 + 1
            goto L_0x0024
        L_0x0039:
            java.lang.String r3 = ")"
            r1.append(r3)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>(r14)
            r3.add(r2, r13)
            android.database.sqlite.SQLiteDatabase r4 = r12.mo18875v()
            r14 = 0
            java.lang.String r5 = "property_filters"
            java.lang.String r6 = "audience_id"
            java.lang.String r7 = "filter_id"
            java.lang.String[] r6 = new java.lang.String[]{r6, r7}     // Catch:{ SQLiteException -> 0x00ae, all -> 0x00ab }
            java.lang.String r7 = r1.toString()     // Catch:{ SQLiteException -> 0x00ae, all -> 0x00ab }
            java.lang.String[] r1 = new java.lang.String[r2]     // Catch:{ SQLiteException -> 0x00ae, all -> 0x00ab }
            java.lang.Object[] r1 = r3.toArray(r1)     // Catch:{ SQLiteException -> 0x00ae, all -> 0x00ab }
            r8 = r1
            java.lang.String[] r8 = (java.lang.String[]) r8     // Catch:{ SQLiteException -> 0x00ae, all -> 0x00ab }
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r1 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ SQLiteException -> 0x00ae, all -> 0x00ab }
            boolean r3 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x00a9 }
            if (r3 != 0) goto L_0x0075
            if (r1 == 0) goto L_0x0074
            r1.close()
        L_0x0074:
            return r0
        L_0x0075:
            int r3 = r1.getInt(r2)     // Catch:{ SQLiteException -> 0x00a9 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a9 }
            java.lang.Object r4 = r0.get(r4)     // Catch:{ SQLiteException -> 0x00a9 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ SQLiteException -> 0x00a9 }
            if (r4 != 0) goto L_0x0091
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ SQLiteException -> 0x00a9 }
            r4.<init>()     // Catch:{ SQLiteException -> 0x00a9 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a9 }
            r0.put(r3, r4)     // Catch:{ SQLiteException -> 0x00a9 }
        L_0x0091:
            r3 = 1
            int r3 = r1.getInt(r3)     // Catch:{ SQLiteException -> 0x00a9 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ SQLiteException -> 0x00a9 }
            r4.add(r3)     // Catch:{ SQLiteException -> 0x00a9 }
            boolean r3 = r1.moveToNext()     // Catch:{ SQLiteException -> 0x00a9 }
            if (r3 != 0) goto L_0x0075
            if (r1 == 0) goto L_0x00a8
            r1.close()
        L_0x00a8:
            return r0
        L_0x00a9:
            r0 = move-exception
            goto L_0x00b0
        L_0x00ab:
            r13 = move-exception
            r1 = r14
            goto L_0x00c8
        L_0x00ae:
            r0 = move-exception
            r1 = r14
        L_0x00b0:
            com.google.android.gms.measurement.internal.f4 r2 = r12.mo19015l()     // Catch:{ all -> 0x00c7 }
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()     // Catch:{ all -> 0x00c7 }
            java.lang.String r3 = "Database error querying filters. appId"
            java.lang.Object r13 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r13)     // Catch:{ all -> 0x00c7 }
            r2.mo19044a(r3, r13, r0)     // Catch:{ all -> 0x00c7 }
            if (r1 == 0) goto L_0x00c6
            r1.close()
        L_0x00c6:
            return r14
        L_0x00c7:
            r13 = move-exception
        L_0x00c8:
            if (r1 == 0) goto L_0x00cd
            r1.close()
        L_0x00cd:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18849a(java.lang.String, java.util.List):java.util.Map");
    }

    @WorkerThread
    /* renamed from: a */
    private static void m8449a(ContentValues contentValues, String str, Object obj) {
        C2258v.m5639b(str);
        C2258v.m5629a(obj);
        if (obj instanceof String) {
            contentValues.put(str, (String) obj);
        } else if (obj instanceof Long) {
            contentValues.put(str, (Long) obj);
        } else if (obj instanceof Double) {
            contentValues.put(str, (Double) obj);
        } else {
            throw new IllegalArgumentException("Invalid value type");
        }
    }

    @WorkerThread
    /* renamed from: a */
    private final Object m8448a(Cursor cursor, int i) {
        int type = cursor.getType(i);
        if (type == 0) {
            mo19015l().mo19001t().mo19042a("Loaded invalid null value from database");
            return null;
        } else if (type == 1) {
            return Long.valueOf(cursor.getLong(i));
        } else {
            if (type == 2) {
                return Double.valueOf(cursor.getDouble(i));
            }
            if (type == 3) {
                return cursor.getString(i);
            }
            if (type != 4) {
                mo19015l().mo19001t().mo19043a("Loaded invalid unknown value type, ignoring it", Integer.valueOf(type));
                return null;
            }
            mo19015l().mo19001t().mo19042a("Loaded invalid blob type value, ignoring it");
            return null;
        }
    }

    /* renamed from: a */
    public final long mo18840a(C2763w0 w0Var) {
        mo18881c();
        mo19284q();
        C2258v.m5629a(w0Var);
        C2258v.m5639b(w0Var.mo18052p0());
        byte[] f = w0Var.mo18129f();
        long a = mo19171i().mo19351a(f);
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", w0Var.mo18052p0());
        contentValues.put("metadata_fingerprint", Long.valueOf(a));
        contentValues.put(TtmlNode.TAG_METADATA, f);
        try {
            mo18875v().insertWithOnConflict("raw_events_metadata", null, contentValues, 4);
            return a;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing raw event metadata. appId", C3032f4.m8621a(w0Var.mo18052p0()), e);
            throw e;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0054  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x005b  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String mo18844a(long r5) {
        /*
            r4 = this;
            r4.mo18881c()
            r4.mo19284q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r4.mo18875v()     // Catch:{ SQLiteException -> 0x0043, all -> 0x0040 }
            java.lang.String r2 = "select app_id from apps where app_id in (select distinct app_id from raw_events) and config_fetched_time < ? order by failed_config_fetch_time limit 1;"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0043, all -> 0x0040 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ SQLiteException -> 0x0043, all -> 0x0040 }
            r6 = 0
            r3[r6] = r5     // Catch:{ SQLiteException -> 0x0043, all -> 0x0040 }
            android.database.Cursor r5 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x0043, all -> 0x0040 }
            boolean r1 = r5.moveToFirst()     // Catch:{ SQLiteException -> 0x003e }
            if (r1 != 0) goto L_0x0034
            com.google.android.gms.measurement.internal.f4 r6 = r4.mo19015l()     // Catch:{ SQLiteException -> 0x003e }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18996B()     // Catch:{ SQLiteException -> 0x003e }
            java.lang.String r1 = "No expired configs for apps with pending events"
            r6.mo19042a(r1)     // Catch:{ SQLiteException -> 0x003e }
            if (r5 == 0) goto L_0x0033
            r5.close()
        L_0x0033:
            return r0
        L_0x0034:
            java.lang.String r6 = r5.getString(r6)     // Catch:{ SQLiteException -> 0x003e }
            if (r5 == 0) goto L_0x003d
            r5.close()
        L_0x003d:
            return r6
        L_0x003e:
            r6 = move-exception
            goto L_0x0045
        L_0x0040:
            r6 = move-exception
            r5 = r0
            goto L_0x0059
        L_0x0043:
            r6 = move-exception
            r5 = r0
        L_0x0045:
            com.google.android.gms.measurement.internal.f4 r1 = r4.mo19015l()     // Catch:{ all -> 0x0058 }
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19001t()     // Catch:{ all -> 0x0058 }
            java.lang.String r2 = "Error selecting expired configs"
            r1.mo19043a(r2, r6)     // Catch:{ all -> 0x0058 }
            if (r5 == 0) goto L_0x0057
            r5.close()
        L_0x0057:
            return r0
        L_0x0058:
            r6 = move-exception
        L_0x0059:
            if (r5 == 0) goto L_0x005e
            r5.close()
        L_0x005e:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18844a(long):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x008c  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0093  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.util.Pair<com.google.android.gms.internal.measurement.C2701s0, java.lang.Long> mo18841a(java.lang.String r8, java.lang.Long r9) {
        /*
            r7 = this;
            r7.mo18881c()
            r7.mo19284q()
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r7.mo18875v()     // Catch:{ SQLiteException -> 0x007b, all -> 0x0078 }
            java.lang.String r2 = "select main_event, children_to_process from main_event_params where app_id=? and event_id=?"
            r3 = 2
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x007b, all -> 0x0078 }
            r4 = 0
            r3[r4] = r8     // Catch:{ SQLiteException -> 0x007b, all -> 0x0078 }
            java.lang.String r5 = java.lang.String.valueOf(r9)     // Catch:{ SQLiteException -> 0x007b, all -> 0x0078 }
            r6 = 1
            r3[r6] = r5     // Catch:{ SQLiteException -> 0x007b, all -> 0x0078 }
            android.database.Cursor r1 = r1.rawQuery(r2, r3)     // Catch:{ SQLiteException -> 0x007b, all -> 0x0078 }
            boolean r2 = r1.moveToFirst()     // Catch:{ SQLiteException -> 0x0076 }
            if (r2 != 0) goto L_0x0037
            com.google.android.gms.measurement.internal.f4 r8 = r7.mo19015l()     // Catch:{ SQLiteException -> 0x0076 }
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo18996B()     // Catch:{ SQLiteException -> 0x0076 }
            java.lang.String r9 = "Main event not found"
            r8.mo19042a(r9)     // Catch:{ SQLiteException -> 0x0076 }
            if (r1 == 0) goto L_0x0036
            r1.close()
        L_0x0036:
            return r0
        L_0x0037:
            byte[] r2 = r1.getBlob(r4)     // Catch:{ SQLiteException -> 0x0076 }
            long r3 = r1.getLong(r6)     // Catch:{ SQLiteException -> 0x0076 }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ SQLiteException -> 0x0076 }
            com.google.android.gms.internal.measurement.s0$a r4 = com.google.android.gms.internal.measurement.C2701s0.m7158x()     // Catch:{ IOException -> 0x005e }
            com.google.android.gms.measurement.internal.C3223v9.m9262a(r4, r2)     // Catch:{ IOException -> 0x005e }
            com.google.android.gms.internal.measurement.s0$a r4 = (com.google.android.gms.internal.measurement.C2701s0.C2702a) r4     // Catch:{ IOException -> 0x005e }
            com.google.android.gms.internal.measurement.u5 r2 = r4.mo17679i()     // Catch:{ IOException -> 0x005e }
            com.google.android.gms.internal.measurement.l4 r2 = (com.google.android.gms.internal.measurement.C2595l4) r2     // Catch:{ IOException -> 0x005e }
            com.google.android.gms.internal.measurement.s0 r2 = (com.google.android.gms.internal.measurement.C2701s0) r2     // Catch:{ IOException -> 0x005e }
            android.util.Pair r8 = android.util.Pair.create(r2, r3)     // Catch:{ SQLiteException -> 0x0076 }
            if (r1 == 0) goto L_0x005d
            r1.close()
        L_0x005d:
            return r8
        L_0x005e:
            r2 = move-exception
            com.google.android.gms.measurement.internal.f4 r3 = r7.mo19015l()     // Catch:{ SQLiteException -> 0x0076 }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()     // Catch:{ SQLiteException -> 0x0076 }
            java.lang.String r4 = "Failed to merge main event. appId, eventId"
            java.lang.Object r8 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r8)     // Catch:{ SQLiteException -> 0x0076 }
            r3.mo19045a(r4, r8, r9, r2)     // Catch:{ SQLiteException -> 0x0076 }
            if (r1 == 0) goto L_0x0075
            r1.close()
        L_0x0075:
            return r0
        L_0x0076:
            r8 = move-exception
            goto L_0x007d
        L_0x0078:
            r8 = move-exception
            r1 = r0
            goto L_0x0091
        L_0x007b:
            r8 = move-exception
            r1 = r0
        L_0x007d:
            com.google.android.gms.measurement.internal.f4 r9 = r7.mo19015l()     // Catch:{ all -> 0x0090 }
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19001t()     // Catch:{ all -> 0x0090 }
            java.lang.String r2 = "Error selecting main event"
            r9.mo19043a(r2, r8)     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x008f
            r1.close()
        L_0x008f:
            return r0
        L_0x0090:
            r8 = move-exception
        L_0x0091:
            if (r1 == 0) goto L_0x0096
            r1.close()
        L_0x0096:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3003d.mo18841a(java.lang.String, java.lang.Long):android.util.Pair");
    }

    /* renamed from: a */
    public final boolean mo18857a(String str, Long l, long j, C2701s0 s0Var) {
        mo18881c();
        mo19284q();
        C2258v.m5629a(s0Var);
        C2258v.m5639b(str);
        C2258v.m5629a(l);
        byte[] f = s0Var.mo18129f();
        mo19015l().mo18996B().mo19044a("Saving complex main event, appId, data size", mo19010e().mo18822a(str), Integer.valueOf(f.length));
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", str);
        contentValues.put("event_id", l);
        contentValues.put("children_to_process", Long.valueOf(j));
        contentValues.put("main_event", f);
        try {
            if (mo18875v().insertWithOnConflict("main_event_params", null, contentValues, 5) != -1) {
                return true;
            }
            mo19015l().mo19001t().mo19043a("Failed to insert complex main event (got -1). appId", C3032f4.m8621a(str));
            return false;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing complex main event. appId", C3032f4.m8621a(str), e);
            return false;
        }
    }

    /* renamed from: a */
    public final boolean mo18854a(C3099l lVar, long j, boolean z) {
        mo18881c();
        mo19284q();
        C2258v.m5629a(lVar);
        C2258v.m5639b(lVar.f5312a);
        C2701s0.C2702a x = C2701s0.m7158x();
        x.mo17865b(lVar.f5316e);
        Iterator<String> it = lVar.f5317f.iterator();
        while (it.hasNext()) {
            String next = it.next();
            C2733u0.C2734a y = C2733u0.m7347y();
            y.mo17933a(next);
            mo19171i().mo19358a(y, lVar.f5317f.mo19459b(next));
            x.mo17859a(y);
        }
        byte[] f = ((C2701s0) x.mo17679i()).mo18129f();
        if (!C2489ea.m6262b() || !mo19013h().mo19154e(lVar.f5312a, C3135o.f5431Y0)) {
            mo19015l().mo18996B().mo19044a("Saving event, name, data size", mo19010e().mo18822a(lVar.f5313b), Integer.valueOf(f.length));
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("app_id", lVar.f5312a);
        contentValues.put("name", lVar.f5313b);
        contentValues.put("timestamp", Long.valueOf(lVar.f5315d));
        contentValues.put("metadata_fingerprint", Long.valueOf(j));
        contentValues.put("data", f);
        contentValues.put("realtime", Integer.valueOf(z ? 1 : 0));
        try {
            if (mo18875v().insert("raw_events", null, contentValues) != -1) {
                return true;
            }
            mo19015l().mo19001t().mo19043a("Failed to insert raw event (got -1). appId", C3032f4.m8621a(lVar.f5312a));
            return false;
        } catch (SQLiteException e) {
            mo19015l().mo19001t().mo19044a("Error storing raw event. appId", C3032f4.m8621a(lVar.f5312a), e);
            return false;
        }
    }
}
