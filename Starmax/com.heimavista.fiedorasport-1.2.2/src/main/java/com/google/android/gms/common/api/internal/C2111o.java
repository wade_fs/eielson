package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.Status;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.o */
public class C2111o {
    /* renamed from: a */
    public static void m5040a(Status status, C4066i<Void> iVar) {
        m5041a(status, null, iVar);
    }

    /* renamed from: a */
    public static <TResult> void m5041a(Status status, TResult tresult, C4066i<TResult> iVar) {
        if (status.mo16518v()) {
            iVar.mo23720a((Boolean) tresult);
        } else {
            iVar.mo23719a(new C2030b(status));
        }
    }

    @Deprecated
    /* renamed from: a */
    public static C4065h<Void> m5039a(C4065h<Boolean> hVar) {
        return hVar.mo23695a(new C2125r1());
    }
}
