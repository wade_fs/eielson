package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2254u;
import java.util.concurrent.TimeUnit;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.internal.i0 */
final class C2223i0 implements C2040g.C2041a {

    /* renamed from: a */
    private final /* synthetic */ C2040g f3723a;

    /* renamed from: b */
    private final /* synthetic */ C4066i f3724b;

    /* renamed from: c */
    private final /* synthetic */ C2254u.C2255a f3725c;

    /* renamed from: d */
    private final /* synthetic */ C2254u.C2256b f3726d;

    C2223i0(C2040g gVar, C4066i iVar, C2254u.C2255a aVar, C2254u.C2256b bVar) {
        this.f3723a = gVar;
        this.f3724b = iVar;
        this.f3725c = aVar;
        this.f3726d = bVar;
    }

    /* renamed from: a */
    public final void mo16589a(Status status) {
        if (status.mo16518v()) {
            this.f3724b.mo23720a((Boolean) this.f3725c.mo16417a(this.f3723a.mo16586a(0, TimeUnit.MILLISECONDS)));
            return;
        }
        this.f3724b.mo23719a(this.f3726d.mo16978a(status));
    }
}
