package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.fb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2506fb implements C2523gb {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4157a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4158b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4159c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        c2Var.mo17365a("measurement.id.lifecycle.app_in_background_parameter", 0L);
        f4157a = c2Var.mo17367a("measurement.lifecycle.app_backgrounded_engagement", false);
        f4158b = c2Var.mo17367a("measurement.lifecycle.app_backgrounded_tracking", true);
        f4159c = c2Var.mo17367a("measurement.lifecycle.app_in_background_parameter", false);
        c2Var.mo17365a("measurement.id.lifecycle.app_backgrounded_tracking", 0L);
    }

    /* renamed from: a */
    public final boolean mo17483a() {
        return f4157a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final boolean mo17484e() {
        return f4158b.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17485f() {
        return f4159c.mo18128b().booleanValue();
    }
}
