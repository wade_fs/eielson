package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.p */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1746p implements C3969b.C3970a {

    /* renamed from: a */
    private final C1747q f2770a;

    private C1746p(C1747q qVar) {
        this.f2770a = qVar;
    }

    /* renamed from: a */
    public static C3969b.C3970a m4362a(C1747q qVar) {
        return new C1746p(qVar);
    }

    /* renamed from: s */
    public Object mo13587s() {
        return C1747q.m4364a(this.f2770a);
    }
}
