package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.os.Binder;
import com.google.android.gms.auth.api.C1951a;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.api.C2036f;

/* renamed from: com.google.android.gms.auth.api.signin.internal.u */
public final class C2008u extends C2003p {

    /* renamed from: a */
    private final Context f3153a;

    public C2008u(Context context) {
        this.f3153a = context;
    }

    /* renamed from: H */
    private final void m4619H() {
        if (!C2176f.m5301b(this.f3153a, Binder.getCallingUid())) {
            int callingUid = Binder.getCallingUid();
            StringBuilder sb = new StringBuilder(52);
            sb.append("Calling UID ");
            sb.append(callingUid);
            sb.append(" is not Google Play services.");
            throw new SecurityException(sb.toString());
        }
    }

    /* renamed from: k */
    public final void mo16459k() {
        m4619H();
        C1989b a = C1989b.m4567a(this.f3153a);
        GoogleSignInAccount b = a.mo16438b();
        GoogleSignInOptions googleSignInOptions = GoogleSignInOptions.f3094d0;
        if (b != null) {
            googleSignInOptions = a.mo16439c();
        }
        C2036f.C2037a aVar = new C2036f.C2037a(this.f3153a);
        aVar.mo16578a(C1951a.f2989e, googleSignInOptions);
        C2036f a2 = aVar.mo16581a();
        try {
            if (a2.mo16563a().mo16482w()) {
                if (b != null) {
                    C1951a.f2990f.mo16414a(a2);
                } else {
                    a2.mo16569b();
                }
            }
        } finally {
            a2.mo16572d();
        }
    }

    /* renamed from: m */
    public final void mo16460m() {
        m4619H();
        C2001n.m4604a(this.f3153a).mo16457a();
    }
}
