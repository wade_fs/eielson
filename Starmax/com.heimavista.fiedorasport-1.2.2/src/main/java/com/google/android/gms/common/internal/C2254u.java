package com.google.android.gms.common.internal;

import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.internal.u */
public class C2254u {

    /* renamed from: a */
    private static final C2256b f3759a = new C2220h0();

    /* renamed from: com.google.android.gms.common.internal.u$a */
    public interface C2255a<R extends C2157k, T> {
        /* renamed from: a */
        T mo16417a(R r);
    }

    /* renamed from: com.google.android.gms.common.internal.u$b */
    public interface C2256b {
        /* renamed from: a */
        C2030b mo16978a(Status status);
    }

    /* renamed from: a */
    public static <R extends C2157k, T> C4065h<T> m5623a(C2040g<R> gVar, C2255a<R, T> aVar) {
        C2256b bVar = f3759a;
        C4066i iVar = new C4066i();
        gVar.mo16587a(new C2223i0(gVar, iVar, aVar, bVar));
        return iVar.mo23718a();
    }

    /* renamed from: a */
    public static <R extends C2157k> C4065h<Void> m5622a(C2040g<R> gVar) {
        return m5623a(gVar, new C2226j0());
    }
}
