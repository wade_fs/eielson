package com.google.android.gms.internal.measurement;

import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.e5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2484e5 extends List {
    /* renamed from: a */
    void mo17321a(C2498f3 f3Var);

    /* renamed from: b */
    Object mo17325b(int i);

    /* renamed from: g */
    List<?> mo17327g();

    /* renamed from: t */
    C2484e5 mo17332t();
}
