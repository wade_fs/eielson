package com.google.android.datatransport.cct.p084b;

import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1696u;

/* renamed from: com.google.android.datatransport.cct.b.j */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1679j extends C1696u {

    /* renamed from: a */
    private final C1696u.C1699c f2624a;

    /* renamed from: b */
    private final C1696u.C1698b f2625b;

    /* renamed from: com.google.android.datatransport.cct.b.j$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1681b extends C1696u.C1697a {

        /* renamed from: a */
        private C1696u.C1699c f2626a;

        /* renamed from: b */
        private C1696u.C1698b f2627b;

        C1681b() {
        }

        /* renamed from: a */
        public C1696u.C1697a mo13522a(@Nullable C1696u.C1699c cVar) {
            this.f2626a = cVar;
            return super;
        }

        /* renamed from: a */
        public C1696u.C1697a mo13521a(@Nullable C1696u.C1698b bVar) {
            this.f2627b = bVar;
            return super;
        }

        /* renamed from: a */
        public C1696u mo13523a() {
            return new C1679j(this.f2626a, this.f2627b, null);
        }
    }

    /* synthetic */ C1679j(C1696u.C1699c cVar, C1696u.C1698b bVar, C1680a aVar) {
        this.f2624a = cVar;
        this.f2625b = bVar;
    }

    @Nullable
    /* renamed from: b */
    public C1696u.C1698b mo13516b() {
        return this.f2625b;
    }

    @Nullable
    /* renamed from: c */
    public C1696u.C1699c mo13517c() {
        return this.f2624a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1696u)) {
            return false;
        }
        C1696u.C1699c cVar = this.f2624a;
        if (cVar != null ? cVar.equals(((C1679j) obj).f2624a) : ((C1679j) obj).f2624a == null) {
            C1696u.C1698b bVar = this.f2625b;
            if (bVar == null) {
                if (((C1679j) obj).f2625b == null) {
                    return true;
                }
            } else if (bVar.equals(((C1679j) obj).f2625b)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        C1696u.C1699c cVar = this.f2624a;
        int i = 0;
        int hashCode = ((cVar == null ? 0 : cVar.hashCode()) ^ 1000003) * 1000003;
        C1696u.C1698b bVar = this.f2625b;
        if (bVar != null) {
            i = bVar.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "NetworkConnectionInfo{networkType=" + this.f2624a + ", mobileSubtype=" + this.f2625b + "}";
    }
}
