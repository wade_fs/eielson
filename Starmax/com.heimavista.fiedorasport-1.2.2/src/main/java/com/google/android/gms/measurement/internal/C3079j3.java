package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2620m8;

/* renamed from: com.google.android.gms.measurement.internal.j3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3079j3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5229a = new C3079j3();

    private C3079j3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Integer.valueOf((int) C2620m8.m6804H());
    }
}
