package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ya */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2806ya implements C2759va {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4619a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.ga.ga_app_id", false);

    /* renamed from: a */
    public final boolean mo18004a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo18005e() {
        return f4619a.mo18128b().booleanValue();
    }
}
