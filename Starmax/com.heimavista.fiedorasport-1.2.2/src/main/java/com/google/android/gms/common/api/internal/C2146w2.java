package com.google.android.gms.common.api.internal;

/* renamed from: com.google.android.gms.common.api.internal.w2 */
final class C2146w2 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ LifecycleCallback f3499P;

    /* renamed from: Q */
    private final /* synthetic */ String f3500Q;

    /* renamed from: R */
    private final /* synthetic */ zzc f3501R;

    C2146w2(zzc zzc, LifecycleCallback lifecycleCallback, String str) {
        this.f3501R = zzc;
        this.f3499P = lifecycleCallback;
        this.f3500Q = str;
    }

    public final void run() {
        if (this.f3501R.f3540Q > 0) {
            this.f3499P.mo16605a(this.f3501R.f3541R != null ? this.f3501R.f3541R.getBundle(this.f3500Q) : null);
        }
        if (this.f3501R.f3540Q >= 2) {
            this.f3499P.mo16610d();
        }
        if (this.f3501R.f3540Q >= 3) {
            this.f3499P.mo16609c();
        }
        if (this.f3501R.f3540Q >= 4) {
            this.f3499P.mo16611e();
        }
        if (this.f3501R.f3540Q >= 5) {
            this.f3499P.mo16607b();
        }
    }
}
