package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.z3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2814z3 extends C2418a4<C2595l4.C2600e> {
    C2814z3() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo17270a(C2739u5 u5Var) {
        return u5Var instanceof C2595l4.C2597b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final C2434b4<C2595l4.C2600e> mo17271b(Object obj) {
        return ((C2595l4.C2597b) obj).mo17681n();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final void mo17272c(Object obj) {
        mo17267a(obj).mo17310a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final C2434b4<C2595l4.C2600e> mo17267a(Object obj) {
        return ((C2595l4.C2597b) obj).zzc;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final int mo17266a(Map.Entry<?, ?> entry) {
        C2595l4.C2600e eVar = (C2595l4.C2600e) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17269a(C2771w7 w7Var, Map.Entry<?, ?> entry) {
        C2595l4.C2600e eVar = (C2595l4.C2600e) entry.getKey();
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Object mo17268a(C2798y3 y3Var, C2739u5 u5Var, int i) {
        return y3Var.mo18164a(u5Var, i);
    }
}
