package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* renamed from: com.google.android.exoplayer2.audio.b */
/* compiled from: lambda */
public final /* synthetic */ class C1772b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f2786P;

    /* renamed from: Q */
    private final /* synthetic */ String f2787Q;

    /* renamed from: R */
    private final /* synthetic */ long f2788R;

    /* renamed from: S */
    private final /* synthetic */ long f2789S;

    public /* synthetic */ C1772b(AudioRendererEventListener.EventDispatcher eventDispatcher, String str, long j, long j2) {
        this.f2786P = eventDispatcher;
        this.f2787Q = str;
        this.f2788R = j;
        this.f2789S = j2;
    }

    public final void run() {
        this.f2786P.mo14096a(this.f2787Q, this.f2788R, this.f2789S);
    }
}
