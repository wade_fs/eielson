package com.google.android.gms.common.api.internal;

/* renamed from: com.google.android.gms.common.api.internal.v0 */
abstract class C2140v0 {

    /* renamed from: a */
    private final C2132t0 f3486a;

    protected C2140v0(C2132t0 t0Var) {
        this.f3486a = t0Var;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16645a();

    /* renamed from: a */
    public final void mo16788a(C2136u0 u0Var) {
        u0Var.f3469a.lock();
        try {
            if (u0Var.f3479k == this.f3486a) {
                mo16645a();
                u0Var.f3469a.unlock();
            }
        } finally {
            u0Var.f3469a.unlock();
        }
    }
}
