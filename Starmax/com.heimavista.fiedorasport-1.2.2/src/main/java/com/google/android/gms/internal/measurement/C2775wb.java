package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.wb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2775wb implements C2593l2<C2822zb> {

    /* renamed from: Q */
    private static C2775wb f4558Q = new C2775wb();

    /* renamed from: P */
    private final C2593l2<C2822zb> f4559P;

    private C2775wb(C2593l2<C2822zb> l2Var) {
        this.f4559P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7756b() {
        return ((C2822zb) f4558Q.mo17285a()).mo18169a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4559P.mo17285a();
    }

    public C2775wb() {
        this(C2579k2.m6601a(new C2807yb()));
    }
}
