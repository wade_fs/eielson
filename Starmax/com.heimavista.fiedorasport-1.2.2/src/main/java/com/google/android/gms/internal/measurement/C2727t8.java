package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.t8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2727t8 implements C2593l2<C2772w8> {

    /* renamed from: Q */
    private static C2727t8 f4504Q = new C2727t8();

    /* renamed from: P */
    private final C2593l2<C2772w8> f4505P;

    private C2727t8(C2593l2<C2772w8> l2Var) {
        this.f4505P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7320b() {
        return ((C2772w8) f4504Q.mo17285a()).mo18002a();
    }

    /* renamed from: c */
    public static boolean m7321c() {
        return ((C2772w8) f4504Q.mo17285a()).mo18003e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4505P.mo17285a();
    }

    public C2727t8() {
        this(C2579k2.m6601a(new C2757v8()));
    }
}
