package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import p119e.p144d.p145a.p146a.p147i.C3905l;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.h */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1738h implements Runnable {

    /* renamed from: P */
    private final C1743m f2740P;

    /* renamed from: Q */
    private final C3905l f2741Q;

    /* renamed from: R */
    private final int f2742R;

    /* renamed from: S */
    private final Runnable f2743S;

    private C1738h(C1743m mVar, C3905l lVar, int i, Runnable runnable) {
        this.f2740P = mVar;
        this.f2741Q = lVar;
        this.f2742R = i;
        this.f2743S = runnable;
    }

    /* renamed from: a */
    public static Runnable m4344a(C1743m mVar, C3905l lVar, int i, Runnable runnable) {
        return new C1738h(mVar, lVar, i, runnable);
    }

    public void run() {
        C1743m.m4356a(this.f2740P, this.f2741Q, this.f2742R, this.f2743S);
    }
}
