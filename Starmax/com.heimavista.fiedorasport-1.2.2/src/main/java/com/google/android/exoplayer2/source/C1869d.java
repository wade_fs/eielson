package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;
import java.io.IOException;

/* renamed from: com.google.android.exoplayer2.source.d */
/* compiled from: lambda */
public final /* synthetic */ class C1869d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2872P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2873Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSourceEventListener.LoadEventInfo f2874R;

    /* renamed from: S */
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData f2875S;

    /* renamed from: T */
    private final /* synthetic */ IOException f2876T;

    /* renamed from: U */
    private final /* synthetic */ boolean f2877U;

    public /* synthetic */ C1869d(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData, IOException iOException, boolean z) {
        this.f2872P = eventDispatcher;
        this.f2873Q = mediaSourceEventListener;
        this.f2874R = loadEventInfo;
        this.f2875S = mediaLoadData;
        this.f2876T = iOException;
        this.f2877U = z;
    }

    public final void run() {
        this.f2872P.mo14932a(this.f2873Q, this.f2874R, this.f2875S, this.f2876T, this.f2877U);
    }
}
