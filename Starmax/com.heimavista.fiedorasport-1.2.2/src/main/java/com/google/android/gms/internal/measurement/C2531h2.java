package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.h2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2531h2<T> extends C2497f2<T> {

    /* renamed from: P */
    private final T f4208P;

    C2531h2(T t) {
        this.f4208P = t;
    }

    /* renamed from: a */
    public final boolean mo17397a() {
        return true;
    }

    /* renamed from: e */
    public final T mo17398e() {
        return this.f4208P;
    }

    public final boolean equals(Object obj) {
        if (obj instanceof C2531h2) {
            return this.f4208P.equals(((C2531h2) obj).f4208P);
        }
        return false;
    }

    public final int hashCode() {
        return this.f4208P.hashCode() + 1502476572;
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f4208P);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 13);
        sb.append("Optional.of(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }
}
