package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import p119e.p144d.p145a.p157c.p160b.C3988b;

public final class GroundOverlayOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<GroundOverlayOptions> CREATOR = new C2939j();
    @NonNull

    /* renamed from: P */
    private C2929a f4822P;

    /* renamed from: Q */
    private LatLng f4823Q;

    /* renamed from: R */
    private float f4824R;

    /* renamed from: S */
    private float f4825S;

    /* renamed from: T */
    private LatLngBounds f4826T;

    /* renamed from: U */
    private float f4827U;

    /* renamed from: V */
    private float f4828V;

    /* renamed from: W */
    private boolean f4829W = true;

    /* renamed from: X */
    private float f4830X = 0.0f;

    /* renamed from: Y */
    private float f4831Y = 0.5f;

    /* renamed from: Z */
    private float f4832Z = 0.5f;

    /* renamed from: a0 */
    private boolean f4833a0 = false;

    GroundOverlayOptions(IBinder iBinder, LatLng latLng, float f, float f2, LatLngBounds latLngBounds, float f3, float f4, boolean z, float f5, float f6, float f7, boolean z2) {
        this.f4822P = new C2929a(C3988b.C3989a.m11981a(iBinder));
        this.f4823Q = latLng;
        this.f4824R = f;
        this.f4825S = f2;
        this.f4826T = latLngBounds;
        this.f4827U = f3;
        this.f4828V = f4;
        this.f4829W = z;
        this.f4830X = f5;
        this.f4831Y = f6;
        this.f4832Z = f7;
        this.f4833a0 = z2;
    }

    /* renamed from: A */
    public final float mo18516A() {
        return this.f4828V;
    }

    /* renamed from: B */
    public final boolean mo18517B() {
        return this.f4833a0;
    }

    /* renamed from: C */
    public final boolean mo18518C() {
        return this.f4829W;
    }

    /* renamed from: c */
    public final float mo18519c() {
        return this.f4831Y;
    }

    /* renamed from: d */
    public final float mo18520d() {
        return this.f4832Z;
    }

    /* renamed from: u */
    public final float mo18521u() {
        return this.f4827U;
    }

    /* renamed from: v */
    public final LatLngBounds mo18522v() {
        return this.f4826T;
    }

    /* renamed from: w */
    public final float mo18523w() {
        return this.f4825S;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5594a(parcel, 2, this.f4822P.mo18622a().asBinder(), false);
        C2250b.m5596a(parcel, 3, (Parcelable) mo18525x(), i, false);
        C2250b.m5590a(parcel, 4, mo18527z());
        C2250b.m5590a(parcel, 5, mo18523w());
        C2250b.m5596a(parcel, 6, (Parcelable) mo18522v(), i, false);
        C2250b.m5590a(parcel, 7, mo18521u());
        C2250b.m5590a(parcel, 8, mo18516A());
        C2250b.m5605a(parcel, 9, mo18518C());
        C2250b.m5590a(parcel, 10, mo18526y());
        C2250b.m5590a(parcel, 11, mo18519c());
        C2250b.m5590a(parcel, 12, mo18520d());
        C2250b.m5605a(parcel, 13, mo18517B());
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final LatLng mo18525x() {
        return this.f4823Q;
    }

    /* renamed from: y */
    public final float mo18526y() {
        return this.f4830X;
    }

    /* renamed from: z */
    public final float mo18527z() {
        return this.f4824R;
    }

    public GroundOverlayOptions() {
    }
}
