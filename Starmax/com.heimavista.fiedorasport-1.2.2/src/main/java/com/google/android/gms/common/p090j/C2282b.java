package com.google.android.gms.common.p090j;

import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.os.Binder;
import android.os.Process;
import com.google.android.gms.common.util.C2323n;

/* renamed from: com.google.android.gms.common.j.b */
public class C2282b {

    /* renamed from: a */
    private final Context f3774a;

    public C2282b(Context context) {
        this.f3774a = context;
    }

    /* renamed from: a */
    public ApplicationInfo mo17055a(String str, int i) {
        return this.f3774a.getPackageManager().getApplicationInfo(str, i);
    }

    /* renamed from: b */
    public PackageInfo mo17060b(String str, int i) {
        return this.f3774a.getPackageManager().getPackageInfo(str, i);
    }

    /* renamed from: a */
    public final PackageInfo mo17056a(String str, int i, int i2) {
        return this.f3774a.getPackageManager().getPackageInfo(str, 64);
    }

    /* renamed from: b */
    public CharSequence mo17061b(String str) {
        return this.f3774a.getPackageManager().getApplicationLabel(this.f3774a.getPackageManager().getApplicationInfo(str, 0));
    }

    /* renamed from: a */
    public final String[] mo17059a(int i) {
        return this.f3774a.getPackageManager().getPackagesForUid(i);
    }

    @TargetApi(19)
    /* renamed from: a */
    public final boolean mo17058a(int i, String str) {
        if (C2323n.m5796e()) {
            try {
                ((AppOpsManager) this.f3774a.getSystemService("appops")).checkPackage(i, str);
                return true;
            } catch (SecurityException unused) {
                return false;
            }
        } else {
            String[] packagesForUid = this.f3774a.getPackageManager().getPackagesForUid(i);
            if (!(str == null || packagesForUid == null)) {
                for (String str2 : packagesForUid) {
                    if (str.equals(str2)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }

    /* renamed from: a */
    public int mo17053a(String str) {
        return this.f3774a.checkCallingOrSelfPermission(str);
    }

    /* renamed from: a */
    public int mo17054a(String str, String str2) {
        return this.f3774a.getPackageManager().checkPermission(str, str2);
    }

    /* renamed from: a */
    public boolean mo17057a() {
        String nameForUid;
        if (Binder.getCallingUid() == Process.myUid()) {
            return C2281a.m5675a(this.f3774a);
        }
        if (!C2323n.m5800i() || (nameForUid = this.f3774a.getPackageManager().getNameForUid(Binder.getCallingUid())) == null) {
            return false;
        }
        return this.f3774a.getPackageManager().isInstantApp(nameForUid);
    }
}
