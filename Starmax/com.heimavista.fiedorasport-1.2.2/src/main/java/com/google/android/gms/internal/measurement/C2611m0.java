package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.m0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2611m0 extends C2595l4<C2611m0, C2612a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2611m0 zzh;
    private static volatile C2501f6<C2611m0> zzi;
    private int zzc;
    private String zzd = "";
    private boolean zze;
    private boolean zzf;
    private int zzg;

    /* renamed from: com.google.android.gms.internal.measurement.m0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2612a extends C2595l4.C2596a<C2611m0, C2612a> implements C2769w5 {
        private C2612a() {
            super(C2611m0.zzh);
        }

        /* renamed from: a */
        public final C2612a mo17724a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2611m0) super.f4288Q).m6754a(str);
            return this;
        }

        /* renamed from: j */
        public final String mo17725j() {
            return ((C2611m0) super.f4288Q).mo17719n();
        }

        /* renamed from: k */
        public final boolean mo17726k() {
            return ((C2611m0) super.f4288Q).mo17720o();
        }

        /* renamed from: l */
        public final boolean mo17727l() {
            return ((C2611m0) super.f4288Q).mo17721p();
        }

        /* renamed from: m */
        public final boolean mo17728m() {
            return ((C2611m0) super.f4288Q).mo17722q();
        }

        /* renamed from: n */
        public final int mo17729n() {
            return ((C2611m0) super.f4288Q).mo17723s();
        }

        /* synthetic */ C2612a(C2591l0 l0Var) {
            this();
        }
    }

    static {
        C2611m0 m0Var = new C2611m0();
        zzh = m0Var;
        C2595l4.m6645a(C2611m0.class, super);
    }

    private C2611m0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6754a(String str) {
        str.getClass();
        this.zzc |= 1;
        this.zzd = str;
    }

    /* renamed from: n */
    public final String mo17719n() {
        return this.zzd;
    }

    /* renamed from: o */
    public final boolean mo17720o() {
        return this.zze;
    }

    /* renamed from: p */
    public final boolean mo17721p() {
        return this.zzf;
    }

    /* renamed from: q */
    public final boolean mo17722q() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: s */
    public final int mo17723s() {
        return this.zzg;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2591l0.f4286a[i - 1]) {
            case 1:
                return new C2611m0();
            case 2:
                return new C2612a(null);
            case 3:
                return C2595l4.m6643a(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\b\u0000\u0002\u0007\u0001\u0003\u0007\u0002\u0004\u0004\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                C2501f6<C2611m0> f6Var = zzi;
                if (f6Var == null) {
                    synchronized (C2611m0.class) {
                        f6Var = zzi;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzh);
                            zzi = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
