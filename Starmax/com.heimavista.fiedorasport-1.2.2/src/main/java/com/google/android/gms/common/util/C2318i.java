package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;

/* renamed from: com.google.android.gms.common.util.i */
public final class C2318i {

    /* renamed from: a */
    private static Boolean f3848a;

    /* renamed from: b */
    private static Boolean f3849b;

    /* renamed from: c */
    private static Boolean f3850c;

    @TargetApi(21)
    /* renamed from: a */
    public static boolean m5784a(Context context) {
        if (f3849b == null) {
            f3849b = Boolean.valueOf(C2323n.m5798g() && context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return f3849b.booleanValue();
    }

    @TargetApi(20)
    /* renamed from: b */
    public static boolean m5785b(Context context) {
        if (f3848a == null) {
            f3848a = Boolean.valueOf(C2323n.m5797f() && context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return f3848a.booleanValue();
    }

    @TargetApi(26)
    /* renamed from: c */
    public static boolean m5786c(Context context) {
        if (!m5785b(context)) {
            return false;
        }
        if (C2323n.m5799h()) {
            return m5784a(context) && !C2323n.m5800i();
        }
        return true;
    }

    /* renamed from: d */
    public static boolean m5787d(Context context) {
        if (f3850c == null) {
            f3850c = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.iot") || context.getPackageManager().hasSystemFeature("android.hardware.type.embedded"));
        }
        return f3850c.booleanValue();
    }

    /* renamed from: a */
    public static boolean m5783a() {
        return "user".equals(Build.TYPE);
    }
}
