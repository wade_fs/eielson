package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2466d4;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.a4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class C2418a4<T extends C2466d4<T>> {
    C2418a4() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract int mo17266a(Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract C2434b4<T> mo17267a(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract Object mo17268a(C2798y3 y3Var, C2739u5 u5Var, int i);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17269a(C2771w7 w7Var, Map.Entry<?, ?> entry);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract boolean mo17270a(C2739u5 u5Var);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract C2434b4<T> mo17271b(Object obj);

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public abstract void mo17272c(Object obj);
}
