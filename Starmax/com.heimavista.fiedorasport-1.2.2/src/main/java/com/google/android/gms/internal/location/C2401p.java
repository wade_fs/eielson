package com.google.android.gms.internal.location;

import android.content.Context;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.location.C2833e;

/* renamed from: com.google.android.gms.internal.location.p */
public final class C2401p extends C2410y {

    /* renamed from: G */
    private final C2394i f3931G;

    public C2401p(Context context, Looper looper, C2036f.C2038b bVar, C2036f.C2039c cVar, String str, C2211e eVar) {
        super(context, looper, bVar, cVar, str, eVar);
        this.f3931G = new C2394i(context, super.f3933F);
    }

    /* renamed from: a */
    public final void mo16909a() {
        synchronized (this.f3931G) {
            if (mo16922c()) {
                try {
                    this.f3931G.mo17207a();
                    this.f3931G.mo17211b();
                } catch (Exception e) {
                    Log.e("LocationClientImpl", "Client disconnected before listeners could be cleaned up", e);
                }
            }
            super.mo16909a();
        }
    }

    /* renamed from: a */
    public final void mo17216a(C2084i.C2085a<C2833e> aVar, C2389d dVar) {
        this.f3931G.mo17208a(aVar, dVar);
    }

    /* renamed from: a */
    public final void mo17217a(zzbd zzbd, C2084i<C2833e> iVar, C2389d dVar) {
        synchronized (this.f3931G) {
            this.f3931G.mo17209a(zzbd, iVar, dVar);
        }
    }
}
