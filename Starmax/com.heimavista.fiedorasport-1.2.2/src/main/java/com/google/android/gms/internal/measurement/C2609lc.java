package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.lc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2609lc implements C2624mc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4311a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4312b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4313c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4311a = c2Var.mo17367a("measurement.client.sessions.background_sessions_enabled", true);
        c2Var.mo17367a("measurement.client.sessions.immediate_start_enabled_foreground", true);
        f4312b = c2Var.mo17367a("measurement.client.sessions.remove_expired_session_properties_enabled", true);
        f4313c = c2Var.mo17367a("measurement.client.sessions.session_id_enabled", true);
    }

    /* renamed from: a */
    public final boolean mo17716a() {
        return f4311a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final boolean mo17717e() {
        return f4312b.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17718f() {
        return f4313c.mo18128b().booleanValue();
    }
}
