package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.yb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2807yb implements C2822zb {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4620a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.experiment.enable_experiment_reporting", true);

    /* renamed from: a */
    public final boolean mo18169a() {
        return f4620a.mo18128b().booleanValue();
    }
}
