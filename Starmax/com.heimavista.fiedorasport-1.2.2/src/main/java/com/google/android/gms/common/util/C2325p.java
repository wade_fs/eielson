package com.google.android.gms.common.util;

import androidx.annotation.Nullable;
import java.util.regex.Pattern;

/* renamed from: com.google.android.gms.common.util.p */
public class C2325p {
    static {
        Pattern.compile("\\$\\{(.*?)\\}");
    }

    /* renamed from: a */
    public static boolean m5805a(@Nullable String str) {
        return str == null || str.trim().isEmpty();
    }
}
