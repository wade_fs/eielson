package com.google.android.gms.maps.p093i;

import p119e.p144d.p145a.p157c.p161c.p165d.C4038g;

/* renamed from: com.google.android.gms.maps.i.q */
public abstract class C2912q extends C4038g implements C2911p {
    public C2912q() {
        super("com.google.android.gms.maps.internal.IOnMapReadyCallback");
    }

    /* JADX WARN: Type inference failed for: r3v2, types: [android.os.IInterface] */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo18485a(int r2, android.os.Parcel r3, android.os.Parcel r4, int r5) {
        /*
            r1 = this;
            r5 = 1
            if (r2 != r5) goto L_0x0026
            android.os.IBinder r2 = r3.readStrongBinder()
            if (r2 != 0) goto L_0x000b
            r2 = 0
            goto L_0x001f
        L_0x000b:
            java.lang.String r3 = "com.google.android.gms.maps.internal.IGoogleMapDelegate"
            android.os.IInterface r3 = r2.queryLocalInterface(r3)
            boolean r0 = r3 instanceof com.google.android.gms.maps.p093i.C2891b
            if (r0 == 0) goto L_0x0019
            r2 = r3
            com.google.android.gms.maps.i.b r2 = (com.google.android.gms.maps.p093i.C2891b) r2
            goto L_0x001f
        L_0x0019:
            com.google.android.gms.maps.i.e0 r3 = new com.google.android.gms.maps.i.e0
            r3.<init>(r2)
            r2 = r3
        L_0x001f:
            r1.mo18487a(r2)
            r4.writeNoException()
            return r5
        L_0x0026:
            r2 = 0
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2912q.mo18485a(int, android.os.Parcel, android.os.Parcel, int):boolean");
    }
}
