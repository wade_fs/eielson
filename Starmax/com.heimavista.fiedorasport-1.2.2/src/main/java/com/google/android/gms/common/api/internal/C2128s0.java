package com.google.android.gms.common.api.internal;

import java.lang.ref.WeakReference;

/* renamed from: com.google.android.gms.common.api.internal.s0 */
final class C2128s0 extends C2078g1 {

    /* renamed from: a */
    private WeakReference<C2100l0> f3437a;

    C2128s0(C2100l0 l0Var) {
        this.f3437a = new WeakReference<>(l0Var);
    }

    /* renamed from: a */
    public final void mo16700a() {
        C2100l0 l0Var = this.f3437a.get();
        if (l0Var != null) {
            l0Var.m4959o();
        }
    }
}
