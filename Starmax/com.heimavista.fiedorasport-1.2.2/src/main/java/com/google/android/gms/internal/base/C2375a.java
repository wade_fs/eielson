package com.google.android.gms.internal.base;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.base.a */
public class C2375a implements IInterface {

    /* renamed from: a */
    private final IBinder f3916a;

    /* renamed from: b */
    private final String f3917b;

    protected C2375a(IBinder iBinder, String str) {
        this.f3916a = iBinder;
        this.f3917b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public final Parcel mo17182H() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f3917b);
        return obtain;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo17183a(int i, Parcel parcel) {
        parcel = Parcel.obtain();
        try {
            this.f3916a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f3916a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17185b(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            this.f3916a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public final void mo17186c(int i, Parcel parcel) {
        try {
            this.f3916a.transact(1, parcel, null, 1);
        } finally {
            parcel.recycle();
        }
    }
}
