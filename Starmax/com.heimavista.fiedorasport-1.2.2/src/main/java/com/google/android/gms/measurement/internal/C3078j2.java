package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2621m9;

/* renamed from: com.google.android.gms.measurement.internal.j2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3078j2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5228a = new C3078j2();

    private C3078j2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2621m9.m6831b());
    }
}
