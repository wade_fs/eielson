package com.google.android.gms.common.internal;

import androidx.annotation.NonNull;
import com.facebook.internal.ServerProtocol;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: com.google.android.gms.common.internal.s */
public class C2246s {

    /* renamed from: b */
    private static final C2227k f3751b = new C2227k("LibraryVersion", "");

    /* renamed from: c */
    private static C2246s f3752c = new C2246s();

    /* renamed from: a */
    private ConcurrentHashMap<String, String> f3753a = new ConcurrentHashMap<>();

    protected C2246s() {
    }

    /* renamed from: a */
    public static C2246s m5540a() {
        return f3752c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String */
    /* renamed from: a */
    public String mo17035a(@NonNull String str) {
        C2258v.m5631a(str, (Object) "Please provide a valid libraryName");
        if (this.f3753a.containsKey(str)) {
            return this.f3753a.get(str);
        }
        Properties properties = new Properties();
        String str2 = null;
        try {
            InputStream resourceAsStream = C2246s.class.getResourceAsStream(String.format("/%s.properties", str));
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
                str2 = properties.getProperty(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, null);
                C2227k kVar = f3751b;
                StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 12 + String.valueOf(str2).length());
                sb.append(str);
                sb.append(" version is ");
                sb.append(str2);
                kVar.mo17002c("LibraryVersion", sb.toString());
            } else {
                C2227k kVar2 = f3751b;
                String valueOf = String.valueOf(str);
                kVar2.mo17001b("LibraryVersion", valueOf.length() != 0 ? "Failed to get app version for libraryName: ".concat(valueOf) : new String("Failed to get app version for libraryName: "));
            }
        } catch (IOException e) {
            C2227k kVar3 = f3751b;
            String valueOf2 = String.valueOf(str);
            kVar3.mo16999a("LibraryVersion", valueOf2.length() != 0 ? "Failed to get app version for libraryName: ".concat(valueOf2) : new String("Failed to get app version for libraryName: "), e);
        }
        if (str2 == null) {
            f3751b.mo16998a("LibraryVersion", ".properties file is dropped during release process. Failure to read app version isexpected druing Google internal testing where locally-built libraries are used");
            str2 = "UNKNOWN";
        }
        this.f3753a.put(str, str2);
        return str2;
    }
}
