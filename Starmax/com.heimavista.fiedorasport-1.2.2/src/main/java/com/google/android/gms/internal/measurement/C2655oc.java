package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.oc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2655oc implements C2593l2<C2639nc> {

    /* renamed from: Q */
    private static C2655oc f4408Q = new C2655oc();

    /* renamed from: P */
    private final C2593l2<C2639nc> f4409P;

    private C2655oc(C2593l2<C2639nc> l2Var) {
        this.f4409P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7018b() {
        return ((C2639nc) f4408Q.mo17285a()).mo17783a();
    }

    /* renamed from: c */
    public static boolean m7019c() {
        return ((C2639nc) f4408Q.mo17285a()).mo17784e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4409P.mo17285a();
    }

    public C2655oc() {
        this(C2579k2.m6601a(new C2684qc()));
    }
}
