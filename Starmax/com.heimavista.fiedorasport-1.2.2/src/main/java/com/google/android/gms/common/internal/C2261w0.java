package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4009a;

/* renamed from: com.google.android.gms.common.internal.w0 */
public final class C2261w0 extends C4009a implements C2257u0 {
    C2261w0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.ICertData");
    }

    /* renamed from: e */
    public final C3988b mo17042e() {
        Parcel a = mo23642a(1, mo23641a());
        C3988b a2 = C3988b.C3989a.m11981a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    /* renamed from: f */
    public final int mo17043f() {
        Parcel a = mo23642a(2, mo23641a());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }
}
