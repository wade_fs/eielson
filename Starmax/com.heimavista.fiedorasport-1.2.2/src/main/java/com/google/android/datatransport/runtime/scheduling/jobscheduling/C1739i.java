package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.i */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1739i implements C3969b.C3970a {

    /* renamed from: a */
    private final C1743m f2744a;

    /* renamed from: b */
    private final C3905l f2745b;

    private C1739i(C1743m mVar, C3905l lVar) {
        this.f2744a = mVar;
        this.f2745b = lVar;
    }

    /* renamed from: a */
    public static C3969b.C3970a m4345a(C1743m mVar, C3905l lVar) {
        return new C1739i(mVar, lVar);
    }

    /* renamed from: s */
    public Object mo13587s() {
        return this.f2744a.f2757c.mo23587a(this.f2745b);
    }
}
