package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.core.view.ViewCompat;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.ArrayList;
import java.util.List;

public final class PolygonOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<PolygonOptions> CREATOR = new C2946q();

    /* renamed from: P */
    private final List<LatLng> f4862P;

    /* renamed from: Q */
    private final List<List<LatLng>> f4863Q;

    /* renamed from: R */
    private float f4864R;

    /* renamed from: S */
    private int f4865S;

    /* renamed from: T */
    private int f4866T;

    /* renamed from: U */
    private float f4867U;

    /* renamed from: V */
    private boolean f4868V;

    /* renamed from: W */
    private boolean f4869W;

    /* renamed from: X */
    private boolean f4870X;

    /* renamed from: Y */
    private int f4871Y;
    @Nullable

    /* renamed from: Z */
    private List<PatternItem> f4872Z;

    public PolygonOptions() {
        this.f4864R = 10.0f;
        this.f4865S = ViewCompat.MEASURED_STATE_MASK;
        this.f4866T = 0;
        this.f4867U = 0.0f;
        this.f4868V = true;
        this.f4869W = false;
        this.f4870X = false;
        this.f4871Y = 0;
        this.f4872Z = null;
        this.f4862P = new ArrayList();
        this.f4863Q = new ArrayList();
    }

    /* renamed from: A */
    public final boolean mo18560A() {
        return this.f4869W;
    }

    /* renamed from: B */
    public final boolean mo18561B() {
        return this.f4868V;
    }

    /* renamed from: c */
    public final int mo18562c() {
        return this.f4866T;
    }

    /* renamed from: d */
    public final List<LatLng> mo18563d() {
        return this.f4862P;
    }

    /* renamed from: u */
    public final int mo18564u() {
        return this.f4865S;
    }

    /* renamed from: v */
    public final int mo18565v() {
        return this.f4871Y;
    }

    @Nullable
    /* renamed from: w */
    public final List<PatternItem> mo18566w() {
        return this.f4872Z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
     arg types: [android.os.Parcel, int, java.util.List<java.util.List<com.google.android.gms.maps.model.LatLng>>, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5614c(parcel, 2, mo18563d(), false);
        C2250b.m5603a(parcel, 3, (List) this.f4863Q, false);
        C2250b.m5590a(parcel, 4, mo18568x());
        C2250b.m5591a(parcel, 5, mo18564u());
        C2250b.m5591a(parcel, 6, mo18562c());
        C2250b.m5590a(parcel, 7, mo18569y());
        C2250b.m5605a(parcel, 8, mo18561B());
        C2250b.m5605a(parcel, 9, mo18560A());
        C2250b.m5605a(parcel, 10, mo18570z());
        C2250b.m5591a(parcel, 11, mo18565v());
        C2250b.m5614c(parcel, 12, mo18566w(), false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final float mo18568x() {
        return this.f4864R;
    }

    /* renamed from: y */
    public final float mo18569y() {
        return this.f4867U;
    }

    /* renamed from: z */
    public final boolean mo18570z() {
        return this.f4870X;
    }

    PolygonOptions(List<LatLng> list, List list2, float f, int i, int i2, float f2, boolean z, boolean z2, boolean z3, int i3, @Nullable List<PatternItem> list3) {
        this.f4864R = 10.0f;
        this.f4865S = ViewCompat.MEASURED_STATE_MASK;
        this.f4866T = 0;
        this.f4867U = 0.0f;
        this.f4868V = true;
        this.f4869W = false;
        this.f4870X = false;
        this.f4871Y = 0;
        this.f4872Z = null;
        this.f4862P = list;
        this.f4863Q = list2;
        this.f4864R = f;
        this.f4865S = i;
        this.f4866T = i2;
        this.f4867U = f2;
        this.f4868V = z;
        this.f4869W = z2;
        this.f4870X = z3;
        this.f4871Y = i3;
        this.f4872Z = list3;
    }
}
