package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.xc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2792xc implements C2808yc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4585a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.upload.file_lock_state_check", false);

    /* renamed from: a */
    public final boolean mo18145a() {
        return f4585a.mo18128b().booleanValue();
    }
}
