package com.google.android.gms.common.api.internal;

import androidx.annotation.WorkerThread;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2231m;
import java.util.Set;

@WorkerThread
/* renamed from: com.google.android.gms.common.api.internal.q1 */
public interface C2121q1 {
    /* renamed from: a */
    void mo16685a(C2231m mVar, Set<Scope> set);

    /* renamed from: b */
    void mo16686b(ConnectionResult connectionResult);
}
