package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.util.p091s.C2330c;
import com.google.android.gms.internal.base.C2379e;
import java.util.concurrent.ExecutorService;

/* renamed from: com.google.android.gms.common.api.internal.x0 */
public final class C2148x0 {

    /* renamed from: a */
    private static final ExecutorService f3503a = C2379e.m5903a().mo17189a(2, new C2330c("GAC_Executor"), 9);

    /* renamed from: a */
    public static ExecutorService m5181a() {
        return f3503a;
    }
}
