package com.google.android.gms.common.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2197d;

/* renamed from: com.google.android.gms.common.internal.f0 */
final class C2216f0 implements C2197d.C2199b {

    /* renamed from: P */
    private final /* synthetic */ C2036f.C2039c f3710P;

    C2216f0(C2036f.C2039c cVar) {
        this.f3710P = cVar;
    }

    /* renamed from: a */
    public final void mo16942a(@NonNull ConnectionResult connectionResult) {
        this.f3710P.mo16585a(connectionResult);
    }
}
