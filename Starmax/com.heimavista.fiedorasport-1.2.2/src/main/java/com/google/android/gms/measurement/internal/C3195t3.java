package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2696r9;

/* renamed from: com.google.android.gms.measurement.internal.t3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3195t3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5649a = new C3195t3();

    private C3195t3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2696r9.m7135c());
    }
}
