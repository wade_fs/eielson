package com.google.android.gms.common.images;

import android.net.Uri;
import com.google.android.gms.common.internal.C2251t;

/* renamed from: com.google.android.gms.common.images.b */
final class C2186b {

    /* renamed from: a */
    public final Uri f3603a;

    public C2186b(Uri uri) {
        this.f3603a = uri;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C2186b)) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        return C2251t.m5617a(((C2186b) obj).f3603a, this.f3603a);
    }

    public final int hashCode() {
        return C2251t.m5615a(this.f3603a);
    }
}
