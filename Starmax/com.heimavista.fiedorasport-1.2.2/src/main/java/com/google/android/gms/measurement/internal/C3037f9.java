package com.google.android.gms.measurement.internal;

import android.os.Bundle;

/* renamed from: com.google.android.gms.measurement.internal.f9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3037f9 implements Runnable {

    /* renamed from: P */
    private final C3001c9 f5139P;

    C3037f9(C3001c9 c9Var) {
        this.f5139P = c9Var;
    }

    public final void run() {
        C3001c9 c9Var = this.f5139P;
        c9Var.f5021c.mo18881c();
        c9Var.f5021c.mo19015l().mo18995A().mo19042a("Application backgrounded");
        c9Var.f5021c.mo18884m().mo19278b("auto", "_ab", new Bundle());
    }
}
