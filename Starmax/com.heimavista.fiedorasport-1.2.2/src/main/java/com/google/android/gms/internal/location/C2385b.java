package com.google.android.gms.internal.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.internal.location.b */
public final class C2385b implements Parcelable.Creator<zzad> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        Status status = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            if (C2248a.m5551a(a) != 1) {
                C2248a.m5550F(parcel, a);
            } else {
                status = (Status) C2248a.m5553a(parcel, a, Status.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzad(status);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzad[i];
    }
}
