package com.google.android.gms.location;

import android.content.Context;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.internal.location.C2387c;
import com.google.android.gms.internal.location.C2388c0;
import com.google.android.gms.internal.location.C2401p;
import com.google.android.gms.internal.location.C2407v;

/* renamed from: com.google.android.gms.location.g */
public class C2837g {

    /* renamed from: a */
    private static final C2016a.C2028g<C2401p> f4692a = new C2016a.C2028g<>();

    /* renamed from: b */
    private static final C2016a.C2017a<C2401p, Object> f4693b = new C2847l();

    /* renamed from: c */
    public static final C2016a<Object> f4694c = new C2016a<>("LocationServices.API", f4693b, f4692a);

    static {
        new C2388c0();
        new C2387c();
        new C2407v();
    }

    /* renamed from: a */
    public static C2826b m7977a(@NonNull Context context) {
        return new C2826b(context);
    }
}
