package com.google.android.gms.maps.p093i;

import android.os.IBinder;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;

/* renamed from: com.google.android.gms.maps.i.w */
public final class C2918w extends C4032a implements C2897e {
    C2918w(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IStreetViewPanoramaDelegate");
    }
}
