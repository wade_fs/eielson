package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2504f9;

/* renamed from: com.google.android.gms.measurement.internal.k3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3091k3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5292a = new C3091k3();

    private C3091k3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2504f9.m6337c());
    }
}
