package com.google.android.gms.internal.p092authapi;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.internal.auth-api.i */
public final class C2364i implements Parcelable.Creator<zzy> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        Credential credential = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            if (C2248a.m5551a(a) != 1) {
                C2248a.m5550F(parcel, a);
            } else {
                credential = (Credential) C2248a.m5553a(parcel, a, Credential.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzy(credential);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzy[i];
    }
}
