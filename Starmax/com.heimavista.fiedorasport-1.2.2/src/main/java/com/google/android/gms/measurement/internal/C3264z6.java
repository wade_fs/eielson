package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.z6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3264z6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5791P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5792Q;

    C3264z6(C3154p6 p6Var, AtomicReference atomicReference) {
        this.f5792Q = p6Var;
        this.f5791P = atomicReference;
    }

    public final void run() {
        synchronized (this.f5791P) {
            try {
                this.f5791P.set(this.f5792Q.mo19013h().mo19156g(this.f5792Q.mo18885p().mo18800B()));
                this.f5791P.notify();
            } catch (Throwable th) {
                this.f5791P.notify();
                throw th;
            }
        }
    }
}
