package com.google.android.gms.measurement.internal;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.api.internal.C2072f;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2314e;
import com.google.android.gms.common.util.C2317h;
import com.google.android.gms.internal.measurement.C2472da;
import com.google.android.gms.internal.measurement.C2504f9;
import com.google.android.gms.internal.measurement.C2765w1;
import com.google.android.gms.internal.measurement.zzv;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.google.android.gms.measurement.internal.j5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public class C3081j5 implements C3058h6 {

    /* renamed from: G */
    private static volatile C3081j5 f5231G;

    /* renamed from: A */
    private volatile Boolean f5232A;

    /* renamed from: B */
    private Boolean f5233B;

    /* renamed from: C */
    private Boolean f5234C;

    /* renamed from: D */
    private int f5235D;

    /* renamed from: E */
    private AtomicInteger f5236E = new AtomicInteger(0);

    /* renamed from: F */
    private final long f5237F;

    /* renamed from: a */
    private final Context f5238a;

    /* renamed from: b */
    private final String f5239b;

    /* renamed from: c */
    private final String f5240c;

    /* renamed from: d */
    private final String f5241d;

    /* renamed from: e */
    private final boolean f5242e;

    /* renamed from: f */
    private final C3098ka f5243f;

    /* renamed from: g */
    private final C3110la f5244g;

    /* renamed from: h */
    private final C3185s4 f5245h;

    /* renamed from: i */
    private final C3032f4 f5246i;

    /* renamed from: j */
    private final C3045g5 f5247j;

    /* renamed from: k */
    private final C3244x8 f5248k;

    /* renamed from: l */
    private final C3267z9 f5249l;

    /* renamed from: m */
    private final C2996c4 f5250m;

    /* renamed from: n */
    private final C2314e f5251n;

    /* renamed from: o */
    private final C3177r7 f5252o;

    /* renamed from: p */
    private final C3154p6 f5253p;

    /* renamed from: q */
    private final C3257z f5254q;

    /* renamed from: r */
    private final C3119m7 f5255r;

    /* renamed from: s */
    private C2972a4 f5256s;

    /* renamed from: t */
    private C3232w7 f5257t;

    /* renamed from: u */
    private C3063i f5258u;

    /* renamed from: v */
    private C2984b4 f5259v;

    /* renamed from: w */
    private C3251y4 f5260w;

    /* renamed from: x */
    private boolean f5261x = false;

    /* renamed from: y */
    private Boolean f5262y;

    /* renamed from: z */
    private long f5263z;

    private C3081j5(C3118m6 m6Var) {
        Bundle bundle;
        boolean z = false;
        C2258v.m5629a(m6Var);
        this.f5243f = new C3098ka(m6Var.f5344a);
        C3206u3.f5671a = this.f5243f;
        this.f5238a = m6Var.f5344a;
        this.f5239b = m6Var.f5345b;
        this.f5240c = m6Var.f5346c;
        this.f5241d = m6Var.f5347d;
        this.f5242e = m6Var.f5351h;
        this.f5232A = m6Var.f5348e;
        zzv zzv = m6Var.f5350g;
        if (!(zzv == null || (bundle = zzv.f4638V) == null)) {
            Object obj = bundle.get("measurementEnabled");
            if (obj instanceof Boolean) {
                this.f5233B = (Boolean) obj;
            }
            Object obj2 = zzv.f4638V.get("measurementDeactivated");
            if (obj2 instanceof Boolean) {
                this.f5234C = (Boolean) obj2;
            }
        }
        C2765w1.m7690a(this.f5238a);
        this.f5251n = C2317h.m5780c();
        this.f5237F = this.f5251n.mo17132a();
        this.f5244g = new C3110la(this);
        C3185s4 s4Var = new C3185s4(this);
        s4Var.mo18904m();
        this.f5245h = s4Var;
        C3032f4 f4Var = new C3032f4(this);
        f4Var.mo18904m();
        this.f5246i = f4Var;
        C3267z9 z9Var = new C3267z9(this);
        z9Var.mo18904m();
        this.f5249l = z9Var;
        C2996c4 c4Var = new C2996c4(this);
        c4Var.mo18904m();
        this.f5250m = c4Var;
        this.f5254q = new C3257z(this);
        C3177r7 r7Var = new C3177r7(this);
        r7Var.mo18817y();
        this.f5252o = r7Var;
        C3154p6 p6Var = new C3154p6(this);
        p6Var.mo18817y();
        this.f5253p = p6Var;
        C3244x8 x8Var = new C3244x8(this);
        x8Var.mo18817y();
        this.f5248k = x8Var;
        C3119m7 m7Var = new C3119m7(this);
        m7Var.mo18904m();
        this.f5255r = m7Var;
        C3045g5 g5Var = new C3045g5(this);
        g5Var.mo18904m();
        this.f5247j = g5Var;
        zzv zzv2 = m6Var.f5350g;
        if (!(zzv2 == null || zzv2.f4633Q == 0)) {
            z = true;
        }
        boolean z2 = !z;
        if (this.f5238a.getApplicationContext() instanceof Application) {
            C3154p6 v = mo19103v();
            if (v.mo19016n().getApplicationContext() instanceof Application) {
                Application application = (Application) v.mo19016n().getApplicationContext();
                if (v.f5547c == null) {
                    v.f5547c = new C3059h7(v, null);
                }
                if (z2) {
                    application.unregisterActivityLifecycleCallbacks(v.f5547c);
                    application.registerActivityLifecycleCallbacks(v.f5547c);
                    v.mo19015l().mo18996B().mo19042a("Registered activity lifecycle callback");
                }
            }
        } else {
            mo19015l().mo19004w().mo19042a("Application context is not an Application");
        }
        this.f5247j.mo19028a(new C3105l5(this, m6Var));
    }

    /* renamed from: J */
    private final C3119m7 m8749J() {
        m8757b(this.f5255r);
        return this.f5255r;
    }

    /* renamed from: K */
    private final void m8750K() {
        if (!this.f5261x) {
            throw new IllegalStateException("AppMeasurement is not initialized");
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: a */
    public final void m8755a(C3118m6 m6Var) {
        String str;
        C3056h4 h4Var;
        mo19014j().mo18881c();
        C3063i iVar = new C3063i(this);
        iVar.mo18904m();
        this.f5258u = iVar;
        C2984b4 b4Var = new C2984b4(this, m6Var.f5349f);
        b4Var.mo18817y();
        this.f5259v = b4Var;
        C2972a4 a4Var = new C2972a4(this);
        a4Var.mo18817y();
        this.f5256s = a4Var;
        C3232w7 w7Var = new C3232w7(this);
        w7Var.mo18817y();
        this.f5257t = w7Var;
        this.f5249l.mo18905p();
        this.f5245h.mo18905p();
        this.f5260w = new C3251y4(this);
        this.f5259v.mo18818z();
        mo19015l().mo19007z().mo19043a("App measurement initialized, version", Long.valueOf(this.f5244g.mo19157i()));
        mo19015l().mo19007z().mo19042a("To enable debug logging run: adb shell setprop log.tag.FA VERBOSE");
        String B = b4Var.mo18800B();
        if (TextUtils.isEmpty(this.f5239b)) {
            if (mo19104w().mo19452d(B)) {
                h4Var = mo19015l().mo19007z();
                str = "Faster debug mode event logging enabled. To disable, run:\n  adb shell setprop debug.firebase.analytics.app .none.";
            } else {
                h4Var = mo19015l().mo19007z();
                String valueOf = String.valueOf(B);
                str = valueOf.length() != 0 ? "To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ".concat(valueOf) : new String("To enable faster debug mode event logging run:\n  adb shell setprop debug.firebase.analytics.app ");
            }
            h4Var.mo19042a(str);
        }
        mo19015l().mo18995A().mo19042a("Debug-level message logging enabled");
        if (this.f5235D != this.f5236E.get()) {
            mo19015l().mo19001t().mo19044a("Not all components initialized", Integer.valueOf(this.f5235D), Integer.valueOf(this.f5236E.get()));
        }
        this.f5261x = true;
    }

    /* renamed from: b */
    private static void m8757b(C3010d6 d6Var) {
        if (d6Var == null) {
            throw new IllegalStateException("Component not created");
        } else if (!d6Var.mo18906s()) {
            String valueOf = String.valueOf(d6Var.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    /* renamed from: A */
    public final String mo19074A() {
        return this.f5239b;
    }

    /* renamed from: B */
    public final String mo19075B() {
        return this.f5240c;
    }

    /* renamed from: C */
    public final String mo19076C() {
        return this.f5241d;
    }

    /* renamed from: D */
    public final boolean mo19077D() {
        return this.f5242e;
    }

    /* renamed from: E */
    public final C3177r7 mo19078E() {
        m8756b(this.f5252o);
        return this.f5252o;
    }

    /* renamed from: F */
    public final C3232w7 mo19079F() {
        m8756b(this.f5257t);
        return this.f5257t;
    }

    /* renamed from: G */
    public final C3063i mo19080G() {
        m8757b(this.f5258u);
        return this.f5258u;
    }

    /* renamed from: H */
    public final C2984b4 mo19081H() {
        m8756b(this.f5259v);
        return this.f5259v;
    }

    /* renamed from: I */
    public final C3257z mo19082I() {
        C3257z zVar = this.f5254q;
        if (zVar != null) {
            return zVar;
        }
        throw new IllegalStateException("Component not created");
    }

    @WorkerThread
    /* renamed from: c */
    public final boolean mo19089c() {
        if (C2472da.m6221b() && this.f5244g.mo19146a(C3135o.f5429X0)) {
            return mo19090d() == 0;
        }
        mo19014j().mo18881c();
        m8750K();
        if (this.f5244g.mo19159m()) {
            return false;
        }
        Boolean bool = this.f5234C;
        if (bool != null && bool.booleanValue()) {
            return false;
        }
        Boolean y = mo19098p().mo19320y();
        if (y != null) {
            return y.booleanValue();
        }
        Boolean p = this.f5244g.mo19160p();
        if (p != null) {
            return p.booleanValue();
        }
        Boolean bool2 = this.f5233B;
        if (bool2 != null) {
            return bool2.booleanValue();
        }
        if (C2072f.m4882b()) {
            return false;
        }
        if (!this.f5244g.mo19146a(C3135o.f5428X) || this.f5232A == null) {
            return true;
        }
        return this.f5232A.booleanValue();
    }

    @WorkerThread
    /* renamed from: d */
    public final int mo19090d() {
        mo19014j().mo18881c();
        if (this.f5244g.mo19159m()) {
            return 1;
        }
        Boolean bool = this.f5234C;
        if (bool != null && bool.booleanValue()) {
            return 2;
        }
        Boolean y = mo19098p().mo19320y();
        if (y == null) {
            Boolean p = this.f5244g.mo19160p();
            if (p == null) {
                Boolean bool2 = this.f5233B;
                if (bool2 != null) {
                    if (bool2.booleanValue()) {
                        return 0;
                    }
                    return 5;
                } else if (C2072f.m4882b()) {
                    return 6;
                } else {
                    if (!this.f5244g.mo19146a(C3135o.f5428X) || this.f5232A == null || this.f5232A.booleanValue()) {
                        return 0;
                    }
                    return 7;
                }
            } else if (p.booleanValue()) {
                return 0;
            } else {
                return 4;
            }
        } else if (y.booleanValue()) {
            return 0;
        } else {
            return 3;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public final long mo19091e() {
        Long valueOf = Long.valueOf(mo19098p().f5612j.mo19326a());
        if (valueOf.longValue() == 0) {
            return this.f5237F;
        }
        return Math.min(this.f5237F, valueOf.longValue());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public final void mo19092f() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public final void mo19093g() {
        throw new IllegalStateException("Unexpected call on client side");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public final void mo19094h() {
        this.f5236E.incrementAndGet();
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: i */
    public final boolean mo19095i() {
        m8750K();
        mo19014j().mo18881c();
        Boolean bool = this.f5262y;
        if (bool == null || this.f5263z == 0 || (bool != null && !bool.booleanValue() && Math.abs(this.f5251n.elapsedRealtime() - this.f5263z) > 1000)) {
            this.f5263z = this.f5251n.elapsedRealtime();
            boolean z = true;
            this.f5262y = Boolean.valueOf(mo19104w().mo19451c("android.permission.INTERNET") && mo19104w().mo19451c("android.permission.ACCESS_NETWORK_STATE") && (C2283c.m5685a(this.f5238a).mo17057a() || this.f5244g.mo19164u() || (C3262z4.m9407a(this.f5238a) && C3267z9.m9415a(this.f5238a, false))));
            if (this.f5262y.booleanValue()) {
                if (!mo19104w().mo19445a(mo19081H().mo18801C(), mo19081H().mo18802D(), mo19081H().mo18803E()) && TextUtils.isEmpty(mo19081H().mo18802D())) {
                    z = false;
                }
                this.f5262y = Boolean.valueOf(z);
            }
        }
        return this.f5262y.booleanValue();
    }

    /* renamed from: j */
    public final C3045g5 mo19014j() {
        m8757b(this.f5247j);
        return this.f5247j;
    }

    @WorkerThread
    /* renamed from: k */
    public final void mo19096k() {
        mo19014j().mo18881c();
        m8757b(m8749J());
        String B = mo19081H().mo18800B();
        Pair<String, Boolean> a = mo19098p().mo19307a(B);
        if (!this.f5244g.mo19161q().booleanValue() || ((Boolean) a.second).booleanValue() || TextUtils.isEmpty((CharSequence) a.first)) {
            mo19015l().mo18995A().mo19042a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
        } else if (!m8749J().mo19169t()) {
            mo19015l().mo19004w().mo19042a("Network is not available for Deferred Deep Link request. Skipping");
        } else {
            C3267z9 w = mo19104w();
            URL a2 = w.mo19430a(mo19081H().mo19013h().mo19157i(), B, (String) a.first, mo19098p().f5603A.mo19326a() - 1);
            C3119m7 J = m8749J();
            C3117m5 m5Var = new C3117m5(this);
            J.mo18881c();
            J.mo18903k();
            C2258v.m5629a(a2);
            C2258v.m5629a(m5Var);
            J.mo19014j().mo19030b(new C3143o7(J, B, a2, null, null, m5Var));
        }
    }

    /* renamed from: l */
    public final C3032f4 mo19015l() {
        m8757b(this.f5246i);
        return this.f5246i;
    }

    /* renamed from: m */
    public final C3110la mo19097m() {
        return this.f5244g;
    }

    /* renamed from: n */
    public final Context mo19016n() {
        return this.f5238a;
    }

    /* renamed from: o */
    public final C2314e mo19017o() {
        return this.f5251n;
    }

    /* renamed from: p */
    public final C3185s4 mo19098p() {
        m8753a((C3034f6) this.f5245h);
        return this.f5245h;
    }

    /* renamed from: q */
    public final C3032f4 mo19099q() {
        C3032f4 f4Var = this.f5246i;
        if (f4Var == null || !f4Var.mo18906s()) {
            return null;
        }
        return this.f5246i;
    }

    /* renamed from: r */
    public final C3098ka mo19018r() {
        return this.f5243f;
    }

    /* renamed from: s */
    public final C3244x8 mo19100s() {
        m8756b(this.f5248k);
        return this.f5248k;
    }

    /* renamed from: t */
    public final C3251y4 mo19101t() {
        return this.f5260w;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: u */
    public final C3045g5 mo19102u() {
        return this.f5247j;
    }

    /* renamed from: v */
    public final C3154p6 mo19103v() {
        m8756b(this.f5253p);
        return this.f5253p;
    }

    /* renamed from: w */
    public final C3267z9 mo19104w() {
        m8753a((C3034f6) this.f5249l);
        return this.f5249l;
    }

    /* renamed from: x */
    public final C2996c4 mo19105x() {
        m8753a((C3034f6) this.f5250m);
        return this.f5250m;
    }

    /* renamed from: y */
    public final C2972a4 mo19106y() {
        m8756b(this.f5256s);
        return this.f5256s;
    }

    /* renamed from: z */
    public final boolean mo19107z() {
        return TextUtils.isEmpty(this.f5239b);
    }

    /* renamed from: b */
    private static void m8756b(C2995c3 c3Var) {
        if (c3Var == null) {
            throw new IllegalStateException("Component not created");
        } else if (!c3Var.mo18815w()) {
            String valueOf = String.valueOf(c3Var.getClass());
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("Component not initialized: ");
            sb.append(valueOf);
            throw new IllegalStateException(sb.toString());
        }
    }

    @WorkerThread
    /* renamed from: b */
    public final boolean mo19088b() {
        return this.f5232A != null && this.f5232A.booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(android.content.Context, boolean):boolean
     arg types: [android.content.Context, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(long, long):long
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, int):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.Boolean, java.lang.Boolean):boolean
      com.google.android.gms.measurement.internal.z9.a(java.util.List<java.lang.String>, java.util.List<java.lang.String>):boolean
      com.google.android.gms.measurement.internal.z9.a(android.content.Context, java.lang.String):long
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.Object):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, long):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, int):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, long):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, android.os.Bundle):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, java.lang.String):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, java.util.ArrayList<android.os.Bundle>):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, boolean):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, byte[]):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, double):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(android.content.Context, boolean):boolean */
    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19083a() {
        mo19014j().mo18881c();
        if (mo19098p().f5607e.mo19326a() == 0) {
            mo19098p().f5607e.mo19327a(this.f5251n.mo17132a());
        }
        if (Long.valueOf(mo19098p().f5612j.mo19326a()).longValue() == 0) {
            mo19015l().mo18996B().mo19043a("Persisting first open", Long.valueOf(this.f5237F));
            mo19098p().f5612j.mo19327a(this.f5237F);
        }
        if (mo19095i()) {
            if (!TextUtils.isEmpty(mo19081H().mo18801C()) || !TextUtils.isEmpty(mo19081H().mo18802D())) {
                mo19104w();
                if (C3267z9.m9420a(mo19081H().mo18801C(), mo19098p().mo19316u(), mo19081H().mo18802D(), mo19098p().mo19317v())) {
                    mo19015l().mo19007z().mo19042a("Rechecking which service to use due to a GMP App Id change");
                    mo19098p().mo19319x();
                    mo19106y().mo18726B();
                    this.f5257t.mo19374H();
                    this.f5257t.mo19372F();
                    mo19098p().f5612j.mo19327a(this.f5237F);
                    mo19098p().f5614l.mo19347a(null);
                }
                mo19098p().mo19312c(mo19081H().mo18801C());
                mo19098p().mo19314d(mo19081H().mo18802D());
            }
            mo19103v().mo19264a(mo19098p().f5614l.mo19346a());
            if (C2504f9.m6336b() && this.f5244g.mo19146a(C3135o.f5413P0) && !mo19104w().mo19457x() && !TextUtils.isEmpty(mo19098p().f5604B.mo19346a())) {
                mo19015l().mo19004w().mo19042a("Remote config removed with active feature rollouts");
                mo19098p().f5604B.mo19347a(null);
            }
            if (!TextUtils.isEmpty(mo19081H().mo18801C()) || !TextUtils.isEmpty(mo19081H().mo18802D())) {
                boolean c = mo19089c();
                if (!mo19098p().mo19306A() && !this.f5244g.mo19159m()) {
                    mo19098p().mo19313c(!c);
                }
                if (c) {
                    mo19103v().mo19251I();
                }
                mo19100s().f5761d.mo19068a();
                mo19079F().mo19385a(new AtomicReference());
            }
        } else if (mo19089c()) {
            if (!mo19104w().mo19451c("android.permission.INTERNET")) {
                mo19015l().mo19001t().mo19042a("App is missing INTERNET permission");
            }
            if (!mo19104w().mo19451c("android.permission.ACCESS_NETWORK_STATE")) {
                mo19015l().mo19001t().mo19042a("App is missing ACCESS_NETWORK_STATE permission");
            }
            if (!C2283c.m5685a(this.f5238a).mo17057a() && !this.f5244g.mo19164u()) {
                if (!C3262z4.m9407a(this.f5238a)) {
                    mo19015l().mo19001t().mo19042a("AppMeasurementReceiver not registered/enabled");
                }
                if (!C3267z9.m9415a(this.f5238a, false)) {
                    mo19015l().mo19001t().mo19042a("AppMeasurementService not registered/enabled");
                }
            }
            mo19015l().mo19001t().mo19042a("Uploading is not possible. App measurement disabled");
        }
        mo19098p().f5622t.mo19332a(this.f5244g.mo19146a(C3135o.f5449h0));
        mo19098p().f5623u.mo19332a(this.f5244g.mo19146a(C3135o.f5451i0));
    }

    /* renamed from: a */
    public static C3081j5 m8752a(Context context, String str, String str2, Bundle bundle) {
        return m8751a(context, new zzv(0, 0, true, null, null, null, bundle));
    }

    /* renamed from: a */
    public static C3081j5 m8751a(Context context, zzv zzv) {
        Bundle bundle;
        if (zzv != null && (zzv.f4636T == null || zzv.f4637U == null)) {
            zzv = new zzv(zzv.f4632P, zzv.f4633Q, zzv.f4634R, zzv.f4635S, null, null, zzv.f4638V);
        }
        C2258v.m5629a(context);
        C2258v.m5629a(context.getApplicationContext());
        if (f5231G == null) {
            synchronized (C3081j5.class) {
                if (f5231G == null) {
                    f5231G = new C3081j5(new C3118m6(context, zzv));
                }
            }
        } else if (!(zzv == null || (bundle = zzv.f4638V) == null || !bundle.containsKey("dataCollectionDefaultEnabled"))) {
            f5231G.mo19087a(zzv.f4638V.getBoolean("dataCollectionDefaultEnabled"));
        }
        return f5231G;
    }

    /* renamed from: a */
    private static void m8753a(C3034f6 f6Var) {
        if (f6Var == null) {
            throw new IllegalStateException("Component not created");
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19087a(boolean z) {
        this.f5232A = Boolean.valueOf(z);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19085a(C3010d6 d6Var) {
        this.f5235D++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19084a(C2995c3 c3Var) {
        this.f5235D++;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo19086a(String str, int i, Throwable th, byte[] bArr, Map map) {
        List<ResolveInfo> queryIntentActivities;
        boolean z = true;
        if (!((i == 200 || i == 204 || i == 304) && th == null)) {
            mo19015l().mo19004w().mo19044a("Network Request for Deferred Deep Link failed. response, exception", Integer.valueOf(i), th);
            return;
        }
        mo19098p().f5628z.mo19332a(true);
        if (bArr.length == 0) {
            mo19015l().mo18995A().mo19042a("Deferred Deep Link response empty.");
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(new String(bArr));
            String optString = jSONObject.optString("deeplink", "");
            String optString2 = jSONObject.optString("gclid", "");
            double optDouble = jSONObject.optDouble("timestamp", 0.0d);
            if (TextUtils.isEmpty(optString)) {
                mo19015l().mo18995A().mo19042a("Deferred Deep Link is empty.");
                return;
            }
            C3267z9 w = mo19104w();
            w.mo18880a();
            if (TextUtils.isEmpty(optString) || (queryIntentActivities = w.mo19016n().getPackageManager().queryIntentActivities(new Intent("android.intent.action.VIEW", Uri.parse(optString)), 0)) == null || queryIntentActivities.isEmpty()) {
                z = false;
            }
            if (!z) {
                mo19015l().mo19004w().mo19044a("Deferred Deep Link validation failed. gclid, deep link", optString2, optString);
                return;
            }
            Bundle bundle = new Bundle();
            bundle.putString("gclid", optString2);
            bundle.putString("_cis", "ddp");
            this.f5253p.mo19267a("auto", "_cmp", bundle);
            C3267z9 w2 = mo19104w();
            if (!TextUtils.isEmpty(optString) && w2.mo19442a(optString, optDouble)) {
                w2.mo19016n().sendBroadcast(new Intent("android.google.analytics.action.DEEPLINK_ACTION"));
            }
        } catch (JSONException e) {
            mo19015l().mo19001t().mo19043a("Failed to parse the Deferred Deep Link response. exception", e);
        }
    }
}
