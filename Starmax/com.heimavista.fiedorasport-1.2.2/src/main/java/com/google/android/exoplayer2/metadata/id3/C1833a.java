package com.google.android.exoplayer2.metadata.id3;

import com.google.android.exoplayer2.metadata.id3.Id3Decoder;

/* renamed from: com.google.android.exoplayer2.metadata.id3.a */
/* compiled from: lambda */
public final /* synthetic */ class C1833a implements Id3Decoder.FramePredicate {

    /* renamed from: a */
    public static final /* synthetic */ C1833a f2837a = new C1833a();

    private /* synthetic */ C1833a() {
    }

    public final boolean evaluate(int i, int i2, int i3, int i4, int i5) {
        return Id3Decoder.m4393a(i, i2, i3, i4, i5);
    }
}
