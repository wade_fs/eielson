package com.google.android.exoplayer2.offline;

import android.net.Uri;
import androidx.annotation.Nullable;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.upstream.cache.CacheUtil;
import com.google.android.exoplayer2.util.Util;
import java.io.DataInputStream;
import java.io.DataOutputStream;

public final class ProgressiveDownloadAction extends DownloadAction {
    public static final DownloadAction.Deserializer DESERIALIZER = new DownloadAction.Deserializer(TYPE, 0) {
        /* class com.google.android.exoplayer2.offline.ProgressiveDownloadAction.C18421 */

        public ProgressiveDownloadAction readFromStream(int i, DataInputStream dataInputStream) {
            Uri parse = Uri.parse(dataInputStream.readUTF());
            boolean readBoolean = dataInputStream.readBoolean();
            byte[] bArr = new byte[dataInputStream.readInt()];
            dataInputStream.readFully(bArr);
            return new ProgressiveDownloadAction(parse, readBoolean, bArr, dataInputStream.readBoolean() ? dataInputStream.readUTF() : null);
        }
    };
    private static final String TYPE = "progressive";
    private static final int VERSION = 0;
    @Nullable
    private final String customCacheKey;

    @Deprecated
    public ProgressiveDownloadAction(Uri uri, boolean z, @Nullable byte[] bArr, @Nullable String str) {
        super(TYPE, 0, uri, z, bArr);
        this.customCacheKey = str;
    }

    public static ProgressiveDownloadAction createDownloadAction(Uri uri, @Nullable byte[] bArr, @Nullable String str) {
        return new ProgressiveDownloadAction(uri, false, bArr, str);
    }

    public static ProgressiveDownloadAction createRemoveAction(Uri uri, @Nullable byte[] bArr, @Nullable String str) {
        return new ProgressiveDownloadAction(uri, true, bArr, str);
    }

    private String getCacheKey() {
        String str = this.customCacheKey;
        return str != null ? str : CacheUtil.generateKey(super.uri);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        return Util.areEqual(this.customCacheKey, ((ProgressiveDownloadAction) obj).customCacheKey);
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.customCacheKey;
        return hashCode + (str != null ? str.hashCode() : 0);
    }

    public boolean isSameMedia(DownloadAction downloadAction) {
        return (downloadAction instanceof ProgressiveDownloadAction) && getCacheKey().equals(((ProgressiveDownloadAction) downloadAction).getCacheKey());
    }

    /* access modifiers changed from: protected */
    public void writeToStream(DataOutputStream dataOutputStream) {
        dataOutputStream.writeUTF(super.uri.toString());
        dataOutputStream.writeBoolean(super.isRemoveAction);
        dataOutputStream.writeInt(super.data.length);
        dataOutputStream.write(super.data);
        boolean z = this.customCacheKey != null;
        dataOutputStream.writeBoolean(z);
        if (z) {
            dataOutputStream.writeUTF(this.customCacheKey);
        }
    }

    public ProgressiveDownloader createDownloader(DownloaderConstructorHelper downloaderConstructorHelper) {
        return new ProgressiveDownloader(super.uri, this.customCacheKey, downloaderConstructorHelper);
    }
}
