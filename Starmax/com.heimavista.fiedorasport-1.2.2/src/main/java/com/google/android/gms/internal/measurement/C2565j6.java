package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.j6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2565j6 implements C2707s5 {

    /* renamed from: a */
    private final C2739u5 f4253a;

    /* renamed from: b */
    private final String f4254b;

    /* renamed from: c */
    private final Object[] f4255c;

    /* renamed from: d */
    private final int f4256d;

    C2565j6(C2739u5 u5Var, String str, Object[] objArr) {
        this.f4253a = u5Var;
        this.f4254b = str;
        this.f4255c = objArr;
        char charAt = str.charAt(0);
        if (charAt < 55296) {
            this.f4256d = charAt;
            return;
        }
        char c = charAt & 8191;
        int i = 13;
        int i2 = 1;
        while (true) {
            int i3 = i2 + 1;
            char charAt2 = str.charAt(i2);
            if (charAt2 >= 55296) {
                c |= (charAt2 & 8191) << i;
                i += 13;
                i2 = i3;
            } else {
                this.f4256d = c | (charAt2 << i);
                return;
            }
        }
    }

    /* renamed from: a */
    public final int mo17590a() {
        return (this.f4256d & 1) == 1 ? C2595l4.C2601f.f4298i : C2595l4.C2601f.f4299j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final String mo17591b() {
        return this.f4254b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final Object[] mo17592c() {
        return this.f4255c;
    }

    /* renamed from: e */
    public final boolean mo17593e() {
        return (this.f4256d & 2) == 2;
    }

    /* renamed from: f */
    public final C2739u5 mo17594f() {
        return this.f4253a;
    }
}
