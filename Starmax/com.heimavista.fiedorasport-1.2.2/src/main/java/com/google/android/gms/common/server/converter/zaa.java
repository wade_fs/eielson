package com.google.android.gms.common.server.converter;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;

public final class zaa extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zaa> CREATOR = new C2294a();

    /* renamed from: P */
    private final int f3797P;

    /* renamed from: Q */
    private final StringToIntConverter f3798Q;

    zaa(int i, StringToIntConverter stringToIntConverter) {
        this.f3797P = i;
        this.f3798Q = stringToIntConverter;
    }

    /* renamed from: a */
    public static zaa m5701a(FastJsonResponse.C2297a<?, ?> aVar) {
        if (aVar instanceof StringToIntConverter) {
            return new zaa((StringToIntConverter) aVar);
        }
        throw new IllegalArgumentException("Unsupported safe parcelable field converter class.");
    }

    /* renamed from: c */
    public final FastJsonResponse.C2297a<?, ?> mo17084c() {
        StringToIntConverter stringToIntConverter = this.f3798Q;
        if (stringToIntConverter != null) {
            return stringToIntConverter;
        }
        throw new IllegalStateException("There was no converter wrapped in this ConverterWrapper.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.server.converter.StringToIntConverter, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3797P);
        C2250b.m5596a(parcel, 2, (Parcelable) this.f3798Q, i, false);
        C2250b.m5587a(parcel, a);
    }

    private zaa(StringToIntConverter stringToIntConverter) {
        this.f3797P = 1;
        this.f3798Q = stringToIntConverter;
    }
}
