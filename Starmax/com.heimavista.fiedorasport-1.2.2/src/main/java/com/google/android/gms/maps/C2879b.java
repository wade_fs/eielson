package com.google.android.gms.maps;

import android.os.RemoteException;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.p093i.C2889a;

/* renamed from: com.google.android.gms.maps.b */
public final class C2879b {

    /* renamed from: a */
    private static C2889a f4787a;

    /* renamed from: a */
    private static C2889a m8109a() {
        C2889a aVar = f4787a;
        C2258v.m5630a(aVar, "CameraUpdateFactory is not initialized");
        return aVar;
    }

    /* renamed from: a */
    public static void m8110a(C2889a aVar) {
        C2258v.m5629a(aVar);
        f4787a = aVar;
    }

    /* renamed from: a */
    public static C2878a m8104a(float f) {
        try {
            return new C2878a(m8109a().mo18419b(f));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public static C2878a m8105a(CameraPosition cameraPosition) {
        try {
            return new C2878a(m8109a().mo18416a(cameraPosition));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public static C2878a m8106a(LatLng latLng) {
        try {
            return new C2878a(m8109a().mo18420c(latLng));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public static C2878a m8107a(LatLng latLng, float f) {
        try {
            return new C2878a(m8109a().mo18417a(latLng, f));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public static C2878a m8108a(LatLngBounds latLngBounds, int i) {
        try {
            return new C2878a(m8109a().mo18418a(latLngBounds, i));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }
}
