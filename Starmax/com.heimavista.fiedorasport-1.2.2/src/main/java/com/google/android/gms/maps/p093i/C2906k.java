package com.google.android.gms.maps.p093i;

/* renamed from: com.google.android.gms.maps.i.k */
public final class C2906k {
    /* renamed from: a */
    public static Boolean m8221a(byte b) {
        if (b == 0) {
            return Boolean.FALSE;
        }
        if (b != 1) {
            return null;
        }
        return Boolean.TRUE;
    }

    /* renamed from: a */
    public static byte m8220a(Boolean bool) {
        if (bool != null) {
            return bool.booleanValue() ? (byte) 1 : 0;
        }
        return -1;
    }
}
