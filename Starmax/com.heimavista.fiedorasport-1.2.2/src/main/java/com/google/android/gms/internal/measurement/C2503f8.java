package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.f8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2503f8 implements C2593l2<C2487e8> {

    /* renamed from: Q */
    private static C2503f8 f4152Q = new C2503f8();

    /* renamed from: P */
    private final C2593l2<C2487e8> f4153P;

    private C2503f8(C2593l2<C2487e8> l2Var) {
        this.f4153P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6334b() {
        return ((C2487e8) f4152Q.mo17285a()).mo17448a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4153P.mo17285a();
    }

    public C2503f8() {
        this(C2579k2.m6601a(new C2552i8()));
    }
}
