package com.google.android.gms.internal.measurement;

import java.io.Serializable;
import java.util.Arrays;

/* renamed from: com.google.android.gms.internal.measurement.p2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2659p2<T> implements C2593l2<T>, Serializable {

    /* renamed from: P */
    private final T f4416P;

    C2659p2(T t) {
        this.f4416P = t;
    }

    /* renamed from: a */
    public final T mo17285a() {
        return this.f4416P;
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C2659p2)) {
            return false;
        }
        T t = this.f4416P;
        T t2 = ((C2659p2) obj).f4416P;
        if (t == t2) {
            return true;
        }
        if (t == null || !t.equals(t2)) {
            return false;
        }
        return true;
    }

    public final int hashCode() {
        return Arrays.hashCode(new Object[]{this.f4416P});
    }

    public final String toString() {
        String valueOf = String.valueOf(this.f4416P);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 22);
        sb.append("Suppliers.ofInstance(");
        sb.append(valueOf);
        sb.append(")");
        return sb.toString();
    }
}
