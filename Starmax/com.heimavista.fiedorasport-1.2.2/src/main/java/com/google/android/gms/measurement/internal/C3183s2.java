package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2503f8;

/* renamed from: com.google.android.gms.measurement.internal.s2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3183s2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5601a = new C3183s2();

    private C3183s2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2503f8.m6334b());
    }
}
