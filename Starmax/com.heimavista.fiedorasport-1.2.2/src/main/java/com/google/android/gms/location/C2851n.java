package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.location.n */
public final class C2851n implements Parcelable.Creator<LocationSettingsRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        boolean z = false;
        ArrayList arrayList = null;
        zzae zzae = null;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                arrayList = C2248a.m5562c(parcel, a, LocationRequest.CREATOR);
            } else if (a2 == 2) {
                z = C2248a.m5577r(parcel, a);
            } else if (a2 == 3) {
                z2 = C2248a.m5577r(parcel, a);
            } else if (a2 != 5) {
                C2248a.m5550F(parcel, a);
            } else {
                zzae = (zzae) C2248a.m5553a(parcel, a, zzae.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new LocationSettingsRequest(arrayList, z, z2, zzae);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationSettingsRequest[i];
    }
}
