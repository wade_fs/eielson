package com.google.android.gms.maps;

import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.p093i.C2897e;

/* renamed from: com.google.android.gms.maps.g */
public class C2887g {
    public C2887g(@NonNull C2897e eVar) {
        C2258v.m5630a(eVar, "delegate");
    }
}
