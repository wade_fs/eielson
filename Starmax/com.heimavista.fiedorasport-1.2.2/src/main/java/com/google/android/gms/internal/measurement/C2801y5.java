package com.google.android.gms.internal.measurement;

import com.google.android.exoplayer2.C1750C;
import com.google.android.gms.internal.measurement.C2595l4;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

/* renamed from: com.google.android.gms.internal.measurement.y5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2801y5<T> implements C2603l6<T> {

    /* renamed from: q */
    private static final int[] f4596q = new int[0];

    /* renamed from: r */
    private static final Unsafe f4597r = C2566j7.m6532c();

    /* renamed from: a */
    private final int[] f4598a;

    /* renamed from: b */
    private final Object[] f4599b;

    /* renamed from: c */
    private final int f4600c;

    /* renamed from: d */
    private final int f4601d;

    /* renamed from: e */
    private final C2739u5 f4602e;

    /* renamed from: f */
    private final boolean f4603f;

    /* renamed from: g */
    private final boolean f4604g;

    /* renamed from: h */
    private final boolean f4605h;

    /* renamed from: i */
    private final int[] f4606i;

    /* renamed from: j */
    private final int f4607j;

    /* renamed from: k */
    private final int f4608k;

    /* renamed from: l */
    private final C2436b6 f4609l;

    /* renamed from: m */
    private final C2467d5 f4610m;

    /* renamed from: n */
    private final C2469d7<?, ?> f4611n;

    /* renamed from: o */
    private final C2418a4<?> f4612o;

    /* renamed from: p */
    private final C2692r5 f4613p;

    private C2801y5(int[] iArr, Object[] objArr, int i, int i2, C2739u5 u5Var, boolean z, boolean z2, int[] iArr2, int i3, int i4, C2436b6 b6Var, C2467d5 d5Var, C2469d7<?, ?> d7Var, C2418a4<?> a4Var, C2692r5 r5Var) {
        this.f4598a = iArr;
        this.f4599b = objArr;
        this.f4600c = i;
        this.f4601d = i2;
        boolean z3 = u5Var instanceof C2595l4;
        this.f4604g = z;
        this.f4603f = a4Var != null && a4Var.mo17270a(u5Var);
        this.f4605h = false;
        this.f4606i = iArr2;
        this.f4607j = i3;
        this.f4608k = i4;
        this.f4609l = b6Var;
        this.f4610m = d5Var;
        this.f4611n = d7Var;
        this.f4612o = a4Var;
        this.f4602e = u5Var;
        this.f4613p = r5Var;
    }

    /* renamed from: a */
    static <T> C2801y5<T> m7838a(Class<T> cls, C2707s5 s5Var, C2436b6 b6Var, C2467d5 d5Var, C2469d7<?, ?> d7Var, C2418a4<?> a4Var, C2692r5 r5Var) {
        int i;
        int i2;
        char c;
        int[] iArr;
        char c2;
        char c3;
        int i3;
        char c4;
        char c5;
        int i4;
        int i5;
        String str;
        char c6;
        int i6;
        char c7;
        int i7;
        int i8;
        int i9;
        int i10;
        Class<?> cls2;
        int i11;
        int i12;
        Field field;
        int i13;
        char charAt;
        int i14;
        char c8;
        Field field2;
        Field field3;
        int i15;
        char charAt2;
        int i16;
        char charAt3;
        int i17;
        char charAt4;
        int i18;
        int i19;
        int i20;
        int i21;
        int i22;
        int i23;
        char charAt5;
        int i24;
        char charAt6;
        int i25;
        char charAt7;
        int i26;
        char charAt8;
        char charAt9;
        char charAt10;
        char charAt11;
        char charAt12;
        char charAt13;
        char charAt14;
        C2707s5 s5Var2 = s5Var;
        if (s5Var2 instanceof C2565j6) {
            C2565j6 j6Var = (C2565j6) s5Var2;
            char c9 = 0;
            boolean z = j6Var.mo17590a() == C2595l4.C2601f.f4299j;
            String b = j6Var.mo17591b();
            int length = b.length();
            char charAt15 = b.charAt(0);
            if (charAt15 >= 55296) {
                char c10 = charAt15 & 8191;
                int i27 = 1;
                int i28 = 13;
                while (true) {
                    i = i27 + 1;
                    charAt14 = b.charAt(i27);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c10 |= (charAt14 & 8191) << i28;
                    i28 += 13;
                    i27 = i;
                }
                charAt15 = (charAt14 << i28) | c10;
            } else {
                i = 1;
            }
            int i29 = i + 1;
            char charAt16 = b.charAt(i);
            if (charAt16 >= 55296) {
                char c11 = charAt16 & 8191;
                int i30 = 13;
                while (true) {
                    i2 = i29 + 1;
                    charAt13 = b.charAt(i29);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c11 |= (charAt13 & 8191) << i30;
                    i30 += 13;
                    i29 = i2;
                }
                charAt16 = c11 | (charAt13 << i30);
            } else {
                i2 = i29;
            }
            if (charAt16 == 0) {
                iArr = f4596q;
                c5 = 0;
                c4 = 0;
                i3 = 0;
                c3 = 0;
                c2 = 0;
                c = 0;
            } else {
                int i31 = i2 + 1;
                char charAt17 = b.charAt(i2);
                if (charAt17 >= 55296) {
                    char c12 = charAt17 & 8191;
                    int i32 = 13;
                    while (true) {
                        i18 = i31 + 1;
                        charAt12 = b.charAt(i31);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c12 |= (charAt12 & 8191) << i32;
                        i32 += 13;
                        i31 = i18;
                    }
                    charAt17 = (charAt12 << i32) | c12;
                } else {
                    i18 = i31;
                }
                int i33 = i18 + 1;
                char charAt18 = b.charAt(i18);
                if (charAt18 >= 55296) {
                    char c13 = charAt18 & 8191;
                    int i34 = 13;
                    while (true) {
                        i19 = i33 + 1;
                        charAt11 = b.charAt(i33);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c13 |= (charAt11 & 8191) << i34;
                        i34 += 13;
                        i33 = i19;
                    }
                    charAt18 = c13 | (charAt11 << i34);
                } else {
                    i19 = i33;
                }
                int i35 = i19 + 1;
                char charAt19 = b.charAt(i19);
                if (charAt19 >= 55296) {
                    char c14 = charAt19 & 8191;
                    int i36 = 13;
                    while (true) {
                        i20 = i35 + 1;
                        charAt10 = b.charAt(i35);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c14 |= (charAt10 & 8191) << i36;
                        i36 += 13;
                        i35 = i20;
                    }
                    charAt19 = (charAt10 << i36) | c14;
                } else {
                    i20 = i35;
                }
                int i37 = i20 + 1;
                c3 = b.charAt(i20);
                if (c3 >= 55296) {
                    char c15 = c3 & 8191;
                    int i38 = 13;
                    while (true) {
                        i21 = i37 + 1;
                        charAt9 = b.charAt(i37);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c15 |= (charAt9 & 8191) << i38;
                        i38 += 13;
                        i37 = i21;
                    }
                    c3 = (charAt9 << i38) | c15;
                } else {
                    i21 = i37;
                }
                int i39 = i21 + 1;
                c2 = b.charAt(i21);
                if (c2 >= 55296) {
                    char c16 = c2 & 8191;
                    int i40 = 13;
                    while (true) {
                        i26 = i39 + 1;
                        charAt8 = b.charAt(i39);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c16 |= (charAt8 & 8191) << i40;
                        i40 += 13;
                        i39 = i26;
                    }
                    c2 = (charAt8 << i40) | c16;
                    i39 = i26;
                }
                int i41 = i39 + 1;
                c5 = b.charAt(i39);
                if (c5 >= 55296) {
                    char c17 = c5 & 8191;
                    int i42 = 13;
                    while (true) {
                        i25 = i41 + 1;
                        charAt7 = b.charAt(i41);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c17 |= (charAt7 & 8191) << i42;
                        i42 += 13;
                        i41 = i25;
                    }
                    c5 = c17 | (charAt7 << i42);
                    i41 = i25;
                }
                int i43 = i41 + 1;
                char charAt20 = b.charAt(i41);
                if (charAt20 >= 55296) {
                    int i44 = 13;
                    int i45 = i43;
                    char c18 = charAt20 & 8191;
                    int i46 = i45;
                    while (true) {
                        i24 = i46 + 1;
                        charAt6 = b.charAt(i46);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c18 |= (charAt6 & 8191) << i44;
                        i44 += 13;
                        i46 = i24;
                    }
                    charAt20 = c18 | (charAt6 << i44);
                    i22 = i24;
                } else {
                    i22 = i43;
                }
                int i47 = i22 + 1;
                c9 = b.charAt(i22);
                if (c9 >= 55296) {
                    int i48 = 13;
                    int i49 = i47;
                    char c19 = c9 & 8191;
                    int i50 = i49;
                    while (true) {
                        i23 = i50 + 1;
                        charAt5 = b.charAt(i50);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c19 |= (charAt5 & 8191) << i48;
                        i48 += 13;
                        i50 = i23;
                    }
                    c9 = c19 | (charAt5 << i48);
                    i47 = i23;
                }
                iArr = new int[(c9 + c5 + charAt20)];
                i3 = (charAt17 << 1) + charAt18;
                int i51 = i47;
                c = charAt17;
                c4 = charAt19;
                i2 = i51;
            }
            Unsafe unsafe = f4597r;
            Object[] c20 = j6Var.mo17592c();
            Class<?> cls3 = j6Var.mo17594f().getClass();
            int i52 = i3;
            int[] iArr2 = new int[(c2 * 3)];
            Object[] objArr = new Object[(c2 << 1)];
            int i53 = c9 + c5;
            char c21 = c9;
            int i54 = i53;
            int i55 = 0;
            int i56 = 0;
            while (i2 < length) {
                int i57 = i2 + 1;
                char charAt21 = b.charAt(i2);
                char c22 = 55296;
                if (charAt21 >= 55296) {
                    int i58 = 13;
                    int i59 = i57;
                    char c23 = charAt21 & 8191;
                    int i60 = i59;
                    while (true) {
                        i17 = i60 + 1;
                        charAt4 = b.charAt(i60);
                        if (charAt4 < c22) {
                            break;
                        }
                        c23 |= (charAt4 & 8191) << i58;
                        i58 += 13;
                        i60 = i17;
                        c22 = 55296;
                    }
                    charAt21 = c23 | (charAt4 << i58);
                    i4 = i17;
                } else {
                    i4 = i57;
                }
                int i61 = i4 + 1;
                char charAt22 = b.charAt(i4);
                int i62 = length;
                char c24 = 55296;
                if (charAt22 >= 55296) {
                    int i63 = 13;
                    int i64 = i61;
                    char c25 = charAt22 & 8191;
                    int i65 = i64;
                    while (true) {
                        i16 = i65 + 1;
                        charAt3 = b.charAt(i65);
                        if (charAt3 < c24) {
                            break;
                        }
                        c25 |= (charAt3 & 8191) << i63;
                        i63 += 13;
                        i65 = i16;
                        c24 = 55296;
                    }
                    charAt22 = c25 | (charAt3 << i63);
                    i5 = i16;
                } else {
                    i5 = i61;
                }
                char c26 = c9;
                char c27 = charAt22 & 255;
                boolean z2 = z;
                if ((charAt22 & 1024) != 0) {
                    iArr[i55] = i56;
                    i55++;
                }
                int i66 = i55;
                if (c27 >= '3') {
                    int i67 = i5 + 1;
                    char charAt23 = b.charAt(i5);
                    char c28 = 55296;
                    if (charAt23 >= 55296) {
                        char c29 = charAt23 & 8191;
                        int i68 = 13;
                        while (true) {
                            i15 = i67 + 1;
                            charAt2 = b.charAt(i67);
                            if (charAt2 < c28) {
                                break;
                            }
                            c29 |= (charAt2 & 8191) << i68;
                            i68 += 13;
                            i67 = i15;
                            c28 = 55296;
                        }
                        charAt23 = c29 | (charAt2 << i68);
                        i67 = i15;
                    }
                    int i69 = c27 - '3';
                    int i70 = i67;
                    if (i69 == 9 || i69 == 17) {
                        c8 = 1;
                        objArr[((i56 / 3) << 1) + 1] = c20[i52];
                        i52++;
                    } else {
                        if (i69 == 12 && (charAt15 & 1) == 1) {
                            objArr[((i56 / 3) << 1) + 1] = c20[i52];
                            i52++;
                        }
                        c8 = 1;
                    }
                    int i71 = charAt23 << c8;
                    Object obj = c20[i71];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = m7841a(cls3, (String) obj);
                        c20[i71] = field2;
                    }
                    char c30 = c4;
                    int objectFieldOffset = (int) unsafe.objectFieldOffset(field2);
                    int i72 = i71 + 1;
                    Object obj2 = c20[i72];
                    int i73 = objectFieldOffset;
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = m7841a(cls3, (String) obj2);
                        c20[i72] = field3;
                    }
                    str = b;
                    i9 = (int) unsafe.objectFieldOffset(field3);
                    cls2 = cls3;
                    i6 = i52;
                    i8 = i73;
                    i10 = 0;
                    c6 = c30;
                    c7 = c3;
                    i7 = charAt21;
                    i12 = i70;
                } else {
                    char c31 = c4;
                    int i74 = i52 + 1;
                    Field a = m7841a(cls3, (String) c20[i52]);
                    c7 = c3;
                    if (c27 == 9 || c27 == 17) {
                        c6 = c31;
                        objArr[((i56 / 3) << 1) + 1] = a.getType();
                    } else {
                        if (c27 == 27 || c27 == '1') {
                            c6 = c31;
                            i14 = i74 + 1;
                            objArr[((i56 / 3) << 1) + 1] = c20[i74];
                        } else if (c27 == 12 || c27 == 30 || c27 == ',') {
                            c6 = c31;
                            if ((charAt15 & 1) == 1) {
                                i14 = i74 + 1;
                                objArr[((i56 / 3) << 1) + 1] = c20[i74];
                            }
                        } else if (c27 == '2') {
                            int i75 = c21 + 1;
                            iArr[c21] = i56;
                            int i76 = (i56 / 3) << 1;
                            int i77 = i74 + 1;
                            objArr[i76] = c20[i74];
                            if ((charAt22 & 2048) != 0) {
                                i74 = i77 + 1;
                                objArr[i76 + 1] = c20[i77];
                                c6 = c31;
                                c21 = i75;
                            } else {
                                c21 = i75;
                                i74 = i77;
                                c6 = c31;
                            }
                        } else {
                            c6 = c31;
                        }
                        i7 = charAt21;
                        i74 = i14;
                        i8 = (int) unsafe.objectFieldOffset(a);
                        if ((charAt15 & 1) == 1 || c27 > 17) {
                            str = b;
                            cls2 = cls3;
                            i6 = i74;
                            i11 = i5;
                            i10 = 0;
                            i9 = 0;
                        } else {
                            i11 = i5 + 1;
                            char charAt24 = b.charAt(i5);
                            if (charAt24 >= 55296) {
                                char c32 = charAt24 & 8191;
                                int i78 = 13;
                                while (true) {
                                    i13 = i11 + 1;
                                    charAt = b.charAt(i11);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c32 |= (charAt & 8191) << i78;
                                    i78 += 13;
                                    i11 = i13;
                                }
                                charAt24 = c32 | (charAt << i78);
                                i11 = i13;
                            }
                            int i79 = (c << 1) + (charAt24 / ' ');
                            Object obj3 = c20[i79];
                            str = b;
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = m7841a(cls3, (String) obj3);
                                c20[i79] = field;
                            }
                            cls2 = cls3;
                            i6 = i74;
                            i9 = (int) unsafe.objectFieldOffset(field);
                            i10 = charAt24 % ' ';
                        }
                        if (c27 >= 18 && c27 <= '1') {
                            iArr[i54] = i8;
                            i54++;
                        }
                        i12 = i11;
                    }
                    i7 = charAt21;
                    i8 = (int) unsafe.objectFieldOffset(a);
                    if ((charAt15 & 1) == 1) {
                    }
                    str = b;
                    cls2 = cls3;
                    i6 = i74;
                    i11 = i5;
                    i10 = 0;
                    i9 = 0;
                    iArr[i54] = i8;
                    i54++;
                    i12 = i11;
                }
                int i80 = i56 + 1;
                iArr2[i56] = i7;
                int i81 = i80 + 1;
                iArr2[i80] = (c27 << 20) | ((charAt22 & 256) != 0 ? C1750C.ENCODING_PCM_MU_LAW : 0) | ((charAt22 & 512) != 0 ? C1750C.ENCODING_PCM_A_LAW : 0) | i8;
                i56 = i81 + 1;
                iArr2[i81] = (i10 << 20) | i9;
                cls3 = cls2;
                c3 = c7;
                c9 = c26;
                i52 = i6;
                length = i62;
                z = z2;
                c4 = c6;
                i55 = i66;
                b = str;
            }
            return new C2801y5(iArr2, objArr, c4, c3, j6Var.mo17594f(), z, false, iArr, c9, i53, b6Var, d5Var, d7Var, a4Var, r5Var);
        }
        ((C2770w6) s5Var2).mo17590a();
        throw null;
    }

    /* renamed from: c */
    private final C2661p4 m7859c(int i) {
        return (C2661p4) this.f4599b[((i / 3) << 1) + 1];
    }

    /* renamed from: e */
    private static C2453c7 m7865e(Object obj) {
        C2595l4 l4Var = (C2595l4) obj;
        C2453c7 c7Var = l4Var.zzb;
        if (c7Var != C2453c7.m6143d()) {
            return c7Var;
        }
        C2453c7 e = C2453c7.m6144e();
        l4Var.zzb = e;
        return e;
    }

    /* renamed from: f */
    private static <T> boolean m7867f(T t, long j) {
        return ((Boolean) C2566j7.m6544f(t, j)).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.a(int, int):int
      com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int
      com.google.android.gms.internal.measurement.y5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.b(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.y5.b(int, int):int
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.l6.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void */
    /* renamed from: b */
    public final void mo17281b(T t, T t2) {
        if (t2 != null) {
            for (int i = 0; i < this.f4598a.length; i += 3) {
                int d = m7861d(i);
                long j = (long) (1048575 & d);
                int i2 = this.f4598a[i];
                switch ((d & 267386880) >>> 20) {
                    case 0:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6518a(t, j, C2566j7.m6541e(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 1:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6519a((Object) t, j, C2566j7.m6536d(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 2:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6521a((Object) t, j, C2566j7.m6527b(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 3:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6521a((Object) t, j, C2566j7.m6527b(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 4:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 5:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6521a((Object) t, j, C2566j7.m6527b(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 6:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 7:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6523a((Object) t, j, C2566j7.m6535c(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 8:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6522a(t, j, C2566j7.m6544f(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 9:
                        m7846a(t, t2, i);
                        break;
                    case 10:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6522a(t, j, C2566j7.m6544f(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 11:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 12:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 13:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 14:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6521a((Object) t, j, C2566j7.m6527b(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 15:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 16:
                        if (!m7847a((Object) t2, i)) {
                            break;
                        } else {
                            C2566j7.m6521a((Object) t, j, C2566j7.m6527b(t2, j));
                            m7854b((Object) t, i);
                            break;
                        }
                    case 17:
                        m7846a(t, t2, i);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.f4610m.mo17417a(t, t2, j);
                        break;
                    case 50:
                        C2633n6.m6887a(this.f4613p, t, t2, j);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!m7848a(t2, i2, i)) {
                            break;
                        } else {
                            C2566j7.m6522a(t, j, C2566j7.m6544f(t2, j));
                            m7855b(t, i2, i);
                            break;
                        }
                    case 60:
                        m7857b(t, t2, i);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!m7848a(t2, i2, i)) {
                            break;
                        } else {
                            C2566j7.m6522a(t, j, C2566j7.m6544f(t2, j));
                            m7855b(t, i2, i);
                            break;
                        }
                    case 68:
                        m7857b(t, t2, i);
                        break;
                }
            }
            C2633n6.m6886a(this.f4611n, t, t2);
            if (this.f4603f) {
                C2633n6.m6885a(this.f4612o, t, t2);
                return;
            }
            return;
        }
        throw new NullPointerException();
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.h(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.h(int, int):int
      com.google.android.gms.internal.measurement.t3.h(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.b(int, boolean):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.b(int, double):int
      com.google.android.gms.internal.measurement.t3.b(int, float):int
      com.google.android.gms.internal.measurement.t3.b(int, java.lang.String):int
      com.google.android.gms.internal.measurement.t3.b(int, int):void
      com.google.android.gms.internal.measurement.t3.b(int, long):void
      com.google.android.gms.internal.measurement.t3.b(int, com.google.android.gms.internal.measurement.f3):void
      com.google.android.gms.internal.measurement.t3.b(int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.g(int, long):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.g(int, int):int
      com.google.android.gms.internal.measurement.t3.g(int, long):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.b(int, float):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.b(int, double):int
      com.google.android.gms.internal.measurement.t3.b(int, java.lang.String):int
      com.google.android.gms.internal.measurement.t3.b(int, boolean):int
      com.google.android.gms.internal.measurement.t3.b(int, int):void
      com.google.android.gms.internal.measurement.t3.b(int, long):void
      com.google.android.gms.internal.measurement.t3.b(int, com.google.android.gms.internal.measurement.f3):void
      com.google.android.gms.internal.measurement.t3.b(int, float):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.t3.b(int, double):int
     arg types: [int, int]
     candidates:
      com.google.android.gms.internal.measurement.t3.b(int, float):int
      com.google.android.gms.internal.measurement.t3.b(int, java.lang.String):int
      com.google.android.gms.internal.measurement.t3.b(int, boolean):int
      com.google.android.gms.internal.measurement.t3.b(int, int):void
      com.google.android.gms.internal.measurement.t3.b(int, long):void
      com.google.android.gms.internal.measurement.t3.b(int, com.google.android.gms.internal.measurement.f3):void
      com.google.android.gms.internal.measurement.t3.b(int, double):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.u5>, com.google.android.gms.internal.measurement.l6):int
     arg types: [int, java.util.List<?>, com.google.android.gms.internal.measurement.l6]
     candidates:
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Long>, boolean):int
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.f3>, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.u5>, com.google.android.gms.internal.measurement.l6):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List<?>, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.u5>, com.google.android.gms.internal.measurement.l6):int
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.f3>, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Long>, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List<?>, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.a(int, java.lang.Object, com.google.android.gms.internal.measurement.l6):int
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<?>, com.google.android.gms.internal.measurement.l6):int
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.String>, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.n6.a(com.google.android.gms.internal.measurement.a4, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.n6.a(com.google.android.gms.internal.measurement.d7, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Long>, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.a(int, int):int
      com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int
      com.google.android.gms.internal.measurement.y5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int
     arg types: [com.google.android.gms.internal.measurement.d7<?, ?>, T]
     candidates:
      com.google.android.gms.internal.measurement.y5.a(int, int):int
      com.google.android.gms.internal.measurement.y5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.u5>, com.google.android.gms.internal.measurement.l6):int
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<com.google.android.gms.internal.measurement.f3>, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Long>, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Long>, boolean):int
     arg types: [int, java.util.List, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.a(int, java.lang.Object, com.google.android.gms.internal.measurement.l6):int
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<?>, com.google.android.gms.internal.measurement.l6):int
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.String>, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.n6.a(com.google.android.gms.internal.measurement.a4, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.n6.a(com.google.android.gms.internal.measurement.d7, java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Long>, boolean):int */
    /* renamed from: d */
    public final int mo17284d(T t) {
        int i;
        int i2;
        long j;
        int i3;
        int i4;
        int b;
        int i5;
        int i6;
        int i7;
        int b2;
        int i8;
        int i9;
        int i10;
        T t2 = t;
        int i11 = 267386880;
        if (this.f4604g) {
            Unsafe unsafe = f4597r;
            int i12 = 0;
            int i13 = 0;
            while (i12 < this.f4598a.length) {
                int d = m7861d(i12);
                int i14 = (d & i11) >>> 20;
                int i15 = this.f4598a[i12];
                long j2 = (long) (d & 1048575);
                int i16 = (i14 < C2499f4.DOUBLE_LIST_PACKED.mo17482a() || i14 > C2499f4.SINT64_LIST_PACKED.mo17482a()) ? 0 : this.f4598a[i12 + 2] & 1048575;
                switch (i14) {
                    case 0:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7222b(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 1:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7223b(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 2:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7231d(i15, C2566j7.m6527b(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 3:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7235e(i15, C2566j7.m6527b(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 4:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7238f(i15, C2566j7.m6514a(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 5:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7243g(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 6:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7250i(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 7:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7226b(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 8:
                        if (m7847a((Object) t2, i12)) {
                            Object f = C2566j7.m6544f(t2, j2);
                            if (!(f instanceof C2498f3)) {
                                b2 = C2720t3.m7225b(i15, (String) f);
                                break;
                            } else {
                                b2 = C2720t3.m7228c(i15, (C2498f3) f);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 9:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2633n6.m6873a(i15, C2566j7.m6544f(t2, j2), m7837a(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 10:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7228c(i15, (C2498f3) C2566j7.m6544f(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 11:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7242g(i15, C2566j7.m6514a(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 12:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7255k(i15, C2566j7.m6514a(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 13:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7253j(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 14:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7247h(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 15:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7246h(i15, C2566j7.m6514a(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 16:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7239f(i15, C2566j7.m6527b(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 17:
                        if (m7847a((Object) t2, i12)) {
                            b2 = C2720t3.m7229c(i15, (C2739u5) C2566j7.m6544f(t2, j2), m7837a(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 18:
                        b2 = C2633n6.m6919i(i15, m7842a(t2, j2), false);
                        break;
                    case 19:
                        b2 = C2633n6.m6916h(i15, m7842a(t2, j2), false);
                        break;
                    case 20:
                        b2 = C2633n6.m6876a(i15, (List<Long>) m7842a(t2, j2), false);
                        break;
                    case 21:
                        b2 = C2633n6.m6892b(i15, (List<Long>) m7842a(t2, j2), false);
                        break;
                    case 22:
                        b2 = C2633n6.m6906e(i15, m7842a(t2, j2), false);
                        break;
                    case 23:
                        b2 = C2633n6.m6919i(i15, m7842a(t2, j2), false);
                        break;
                    case 24:
                        b2 = C2633n6.m6916h(i15, m7842a(t2, j2), false);
                        break;
                    case 25:
                        b2 = C2633n6.m6922j(i15, m7842a(t2, j2), false);
                        break;
                    case 26:
                        b2 = C2633n6.m6874a(i15, m7842a(t2, j2));
                        break;
                    case 27:
                        b2 = C2633n6.m6875a(i15, m7842a(t2, j2), m7837a(i12));
                        break;
                    case 28:
                        b2 = C2633n6.m6890b(i15, m7842a(t2, j2));
                        break;
                    case 29:
                        b2 = C2633n6.m6910f(i15, m7842a(t2, j2), false);
                        break;
                    case 30:
                        b2 = C2633n6.m6902d(i15, m7842a(t2, j2), false);
                        break;
                    case 31:
                        b2 = C2633n6.m6916h(i15, m7842a(t2, j2), false);
                        break;
                    case 32:
                        b2 = C2633n6.m6919i(i15, m7842a(t2, j2), false);
                        break;
                    case 33:
                        b2 = C2633n6.m6913g(i15, m7842a(t2, j2), false);
                        break;
                    case 34:
                        b2 = C2633n6.m6898c(i15, m7842a(t2, j2), false);
                        break;
                    case 35:
                        i9 = C2633n6.m6920i((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 36:
                        i9 = C2633n6.m6917h((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 37:
                        i9 = C2633n6.m6877a((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 38:
                        i9 = C2633n6.m6893b((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 39:
                        i9 = C2633n6.m6907e((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 40:
                        i9 = C2633n6.m6920i((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 41:
                        i9 = C2633n6.m6917h((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 42:
                        i9 = C2633n6.m6923j((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 43:
                        i9 = C2633n6.m6911f((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 44:
                        i9 = C2633n6.m6903d((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 45:
                        i9 = C2633n6.m6917h((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 46:
                        i9 = C2633n6.m6920i((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 47:
                        i9 = C2633n6.m6914g((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 48:
                        i9 = C2633n6.m6899c((List) unsafe.getObject(t2, j2));
                        if (i9 > 0) {
                            if (this.f4605h) {
                                unsafe.putInt(t2, (long) i16, i9);
                            }
                            i10 = C2720t3.m7234e(i15);
                            i8 = C2720t3.m7241g(i9);
                            b2 = i10 + i8 + i9;
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 49:
                        b2 = C2633n6.m6891b(i15, (List<C2739u5>) m7842a(t2, j2), m7837a(i12));
                        break;
                    case 50:
                        b2 = this.f4613p.mo17827a(i15, C2566j7.m6544f(t2, j2), m7853b(i12));
                        break;
                    case 51:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7222b(i15, 0.0d);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 52:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7223b(i15, 0.0f);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 53:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7231d(i15, m7864e(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 54:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7235e(i15, m7864e(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 55:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7238f(i15, m7862d(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 56:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7243g(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 57:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7250i(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 58:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7226b(i15, true);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 59:
                        if (m7848a(t2, i15, i12)) {
                            Object f2 = C2566j7.m6544f(t2, j2);
                            if (!(f2 instanceof C2498f3)) {
                                b2 = C2720t3.m7225b(i15, (String) f2);
                                break;
                            } else {
                                b2 = C2720t3.m7228c(i15, (C2498f3) f2);
                                break;
                            }
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 60:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2633n6.m6873a(i15, C2566j7.m6544f(t2, j2), m7837a(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 61:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7228c(i15, (C2498f3) C2566j7.m6544f(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 62:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7242g(i15, m7862d(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 63:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7255k(i15, m7862d(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 64:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7253j(i15, 0);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 65:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7247h(i15, 0L);
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 66:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7246h(i15, m7862d(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 67:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7239f(i15, m7864e(t2, j2));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    case 68:
                        if (m7848a(t2, i15, i12)) {
                            b2 = C2720t3.m7229c(i15, (C2739u5) C2566j7.m6544f(t2, j2), m7837a(i12));
                            break;
                        } else {
                            continue;
                            i12 += 3;
                            i11 = 267386880;
                        }
                    default:
                        i12 += 3;
                        i11 = 267386880;
                }
                i13 += b2;
                i12 += 3;
                i11 = 267386880;
            }
            return i13 + m7833a((C2469d7) this.f4611n, (Object) t2);
        }
        Unsafe unsafe2 = f4597r;
        int i17 = 0;
        int i18 = -1;
        int i19 = 0;
        for (int i20 = 0; i20 < this.f4598a.length; i20 += 3) {
            int d2 = m7861d(i20);
            int[] iArr = this.f4598a;
            int i21 = iArr[i20];
            int i22 = (d2 & 267386880) >>> 20;
            if (i22 <= 17) {
                i2 = iArr[i20 + 2];
                int i23 = i2 & 1048575;
                i = 1 << (i2 >>> 20);
                if (i23 != i18) {
                    i19 = unsafe2.getInt(t2, (long) i23);
                } else {
                    i23 = i18;
                }
                i18 = i23;
            } else {
                i2 = (!this.f4605h || i22 < C2499f4.DOUBLE_LIST_PACKED.mo17482a() || i22 > C2499f4.SINT64_LIST_PACKED.mo17482a()) ? 0 : this.f4598a[i20 + 2] & 1048575;
                i = 0;
            }
            long j3 = (long) (d2 & 1048575);
            switch (i22) {
                case 0:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += C2720t3.m7222b(i21, 0.0d);
                        break;
                    }
                    break;
                case 1:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i17 += C2720t3.m7223b(i21, 0.0f);
                        break;
                    }
                case 2:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = C2720t3.m7231d(i21, unsafe2.getLong(t2, j3));
                        i17 += i3;
                    }
                    break;
                case 3:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = C2720t3.m7235e(i21, unsafe2.getLong(t2, j3));
                        i17 += i3;
                    }
                    break;
                case 4:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = C2720t3.m7238f(i21, unsafe2.getInt(t2, j3));
                        i17 += i3;
                    }
                    break;
                case 5:
                    j = 0;
                    if ((i19 & i) != 0) {
                        i3 = C2720t3.m7243g(i21, 0L);
                        i17 += i3;
                    }
                    break;
                case 6:
                    if ((i19 & i) != 0) {
                        i4 = C2720t3.m7250i(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 7:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7226b(i21, true);
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 8:
                    if ((i19 & i) != 0) {
                        Object object = unsafe2.getObject(t2, j3);
                        if (object instanceof C2498f3) {
                            b = C2720t3.m7228c(i21, (C2498f3) object);
                        } else {
                            b = C2720t3.m7225b(i21, (String) object);
                        }
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 9:
                    if ((i19 & i) != 0) {
                        b = C2633n6.m6873a(i21, unsafe2.getObject(t2, j3), m7837a(i20));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 10:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7228c(i21, (C2498f3) unsafe2.getObject(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 11:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7242g(i21, unsafe2.getInt(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 12:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7255k(i21, unsafe2.getInt(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 13:
                    if ((i19 & i) != 0) {
                        i4 = C2720t3.m7253j(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 14:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7247h(i21, 0L);
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 15:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7246h(i21, unsafe2.getInt(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 16:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7239f(i21, unsafe2.getLong(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 17:
                    if ((i19 & i) != 0) {
                        b = C2720t3.m7229c(i21, (C2739u5) unsafe2.getObject(t2, j3), m7837a(i20));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 18:
                    b = C2633n6.m6919i(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 19:
                    b = C2633n6.m6916h(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 20:
                    b = C2633n6.m6876a(i21, (List<Long>) ((List) unsafe2.getObject(t2, j3)), false);
                    i17 += b;
                    j = 0;
                    break;
                case 21:
                    b = C2633n6.m6892b(i21, (List<Long>) ((List) unsafe2.getObject(t2, j3)), false);
                    i17 += b;
                    j = 0;
                    break;
                case 22:
                    b = C2633n6.m6906e(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 23:
                    b = C2633n6.m6919i(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 24:
                    b = C2633n6.m6916h(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 25:
                    b = C2633n6.m6922j(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 26:
                    b = C2633n6.m6874a(i21, (List) unsafe2.getObject(t2, j3));
                    i17 += b;
                    j = 0;
                    break;
                case 27:
                    b = C2633n6.m6875a(i21, (List<?>) ((List) unsafe2.getObject(t2, j3)), m7837a(i20));
                    i17 += b;
                    j = 0;
                    break;
                case 28:
                    b = C2633n6.m6890b(i21, (List) unsafe2.getObject(t2, j3));
                    i17 += b;
                    j = 0;
                    break;
                case 29:
                    b = C2633n6.m6910f(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 30:
                    b = C2633n6.m6902d(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 31:
                    b = C2633n6.m6916h(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 32:
                    b = C2633n6.m6919i(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 33:
                    b = C2633n6.m6913g(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 34:
                    b = C2633n6.m6898c(i21, (List) unsafe2.getObject(t2, j3), false);
                    i17 += b;
                    j = 0;
                    break;
                case 35:
                    i7 = C2633n6.m6920i((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 36:
                    i7 = C2633n6.m6917h((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 37:
                    i7 = C2633n6.m6877a((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 38:
                    i7 = C2633n6.m6893b((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 39:
                    i7 = C2633n6.m6907e((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 40:
                    i7 = C2633n6.m6920i((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 41:
                    i7 = C2633n6.m6917h((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 42:
                    i7 = C2633n6.m6923j((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 43:
                    i7 = C2633n6.m6911f((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 44:
                    i7 = C2633n6.m6903d((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 45:
                    i7 = C2633n6.m6917h((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 46:
                    i7 = C2633n6.m6920i((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 47:
                    i7 = C2633n6.m6914g((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 48:
                    i7 = C2633n6.m6899c((List) unsafe2.getObject(t2, j3));
                    if (i7 > 0) {
                        if (this.f4605h) {
                            unsafe2.putInt(t2, (long) i2, i7);
                        }
                        i6 = C2720t3.m7234e(i21);
                        i5 = C2720t3.m7241g(i7);
                        i4 = i6 + i5 + i7;
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 49:
                    b = C2633n6.m6891b(i21, (List) unsafe2.getObject(t2, j3), m7837a(i20));
                    i17 += b;
                    j = 0;
                    break;
                case 50:
                    b = this.f4613p.mo17827a(i21, unsafe2.getObject(t2, j3), m7853b(i20));
                    i17 += b;
                    j = 0;
                    break;
                case 51:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7222b(i21, 0.0d);
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 52:
                    if (m7848a(t2, i21, i20)) {
                        i4 = C2720t3.m7223b(i21, 0.0f);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 53:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7231d(i21, m7864e(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 54:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7235e(i21, m7864e(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 55:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7238f(i21, m7862d(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 56:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7243g(i21, 0L);
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 57:
                    if (m7848a(t2, i21, i20)) {
                        i4 = C2720t3.m7250i(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 58:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7226b(i21, true);
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 59:
                    if (m7848a(t2, i21, i20)) {
                        Object object2 = unsafe2.getObject(t2, j3);
                        if (object2 instanceof C2498f3) {
                            b = C2720t3.m7228c(i21, (C2498f3) object2);
                        } else {
                            b = C2720t3.m7225b(i21, (String) object2);
                        }
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 60:
                    if (m7848a(t2, i21, i20)) {
                        b = C2633n6.m6873a(i21, unsafe2.getObject(t2, j3), m7837a(i20));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 61:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7228c(i21, (C2498f3) unsafe2.getObject(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 62:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7242g(i21, m7862d(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 63:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7255k(i21, m7862d(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 64:
                    if (m7848a(t2, i21, i20)) {
                        i4 = C2720t3.m7253j(i21, 0);
                        i17 += i4;
                    }
                    j = 0;
                    break;
                case 65:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7247h(i21, 0L);
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 66:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7246h(i21, m7862d(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 67:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7239f(i21, m7864e(t2, j3));
                        i17 += b;
                    }
                    j = 0;
                    break;
                case 68:
                    if (m7848a(t2, i21, i20)) {
                        b = C2720t3.m7229c(i21, (C2739u5) unsafe2.getObject(t2, j3), m7837a(i20));
                        i17 += b;
                    }
                    j = 0;
                    break;
                default:
                    j = 0;
                    break;
            }
        }
        int a = i17 + m7833a((C2469d7) this.f4611n, (Object) t2);
        if (!this.f4603f) {
            return a;
        }
        C2434b4<?> a2 = this.f4612o.mo17267a((Object) t2);
        if (a2.f3996a.mo17744c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = a2.f3996a.mo17747d().iterator();
            if (!it.hasNext()) {
                return a + 0;
            }
            Map.Entry next = it.next();
            C2434b4.m6064a((C2466d4) next.getKey(), next.getValue());
            throw null;
        }
        Map.Entry<T, Object> a3 = a2.f3996a.mo17741a(0);
        C2434b4.m6064a((C2466d4) a3.getKey(), a3.getValue());
        throw null;
    }

    /* renamed from: f */
    private final int m7866f(int i) {
        if (i < this.f4600c || i > this.f4601d) {
            return -1;
        }
        return m7852b(i, 0);
    }

    /* renamed from: c */
    public final void mo17283c(Object obj) {
        int i;
        int i2 = this.f4607j;
        while (true) {
            i = this.f4608k;
            if (i2 >= i) {
                break;
            }
            long d = (long) (m7861d(this.f4606i[i2]) & 1048575);
            Object f = C2566j7.m6544f(obj, d);
            if (f != null) {
                this.f4613p.mo17834f(f);
                C2566j7.m6522a(obj, d, f);
            }
            i2++;
        }
        int length = this.f4606i.length;
        while (i < length) {
            this.f4610m.mo17416a(obj, (long) this.f4606i[i]);
            i++;
        }
        this.f4611n.mo17423b(obj);
        if (this.f4603f) {
            this.f4612o.mo17272c(obj);
        }
    }

    /* renamed from: e */
    private final int m7863e(int i) {
        return this.f4598a[i + 2];
    }

    /* renamed from: e */
    private static <T> long m7864e(T t, long j) {
        return ((Long) C2566j7.m6544f(t, j)).longValue();
    }

    /* renamed from: c */
    private static <T> float m7858c(T t, long j) {
        return ((Float) C2566j7.m6544f(t, j)).floatValue();
    }

    /* renamed from: c */
    private final boolean m7860c(T t, T t2, int i) {
        return m7847a(t, i) == m7847a(t2, i);
    }

    /* renamed from: b */
    private final void m7857b(T t, T t2, int i) {
        int d = m7861d(i);
        int i2 = this.f4598a[i];
        long j = (long) (d & 1048575);
        if (m7848a(t2, i2, i)) {
            Object f = C2566j7.m6544f(t, j);
            Object f2 = C2566j7.m6544f(t2, j);
            if (f != null && f2 != null) {
                C2566j7.m6522a(t, j, C2647o4.m6962a(f, f2));
                m7855b(t, i2, i);
            } else if (f2 != null) {
                C2566j7.m6522a(t, j, f2);
                m7855b(t, i2, i);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.w7, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.w7, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<?>, com.google.android.gms.internal.measurement.w7, com.google.android.gms.internal.measurement.l6):void
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.w7, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.w7, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.w7, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.a(int, int, java.lang.Object, com.google.android.gms.internal.measurement.d7):UB
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<?>, com.google.android.gms.internal.measurement.w7, com.google.android.gms.internal.measurement.l6):void
      com.google.android.gms.internal.measurement.n6.a(com.google.android.gms.internal.measurement.r5, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.w7, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x046b  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0471  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x0030  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m7856b(T r18, com.google.android.gms.internal.measurement.C2771w7 r19) {
        /*
            r17 = this;
            r0 = r17
            r1 = r18
            r2 = r19
            boolean r3 = r0.f4603f
            if (r3 == 0) goto L_0x0023
            com.google.android.gms.internal.measurement.a4<?> r3 = r0.f4612o
            com.google.android.gms.internal.measurement.b4 r3 = r3.mo17267a(r1)
            com.google.android.gms.internal.measurement.m6<T, java.lang.Object> r5 = r3.f3996a
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x0023
            java.util.Iterator r3 = r3.mo17313c()
            java.lang.Object r3 = r3.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            goto L_0x0024
        L_0x0023:
            r3 = 0
        L_0x0024:
            r5 = -1
            int[] r6 = r0.f4598a
            int r6 = r6.length
            sun.misc.Unsafe r7 = com.google.android.gms.internal.measurement.C2801y5.f4597r
            r8 = 0
            r5 = 0
            r9 = -1
            r10 = 0
        L_0x002e:
            if (r5 >= r6) goto L_0x0468
            int r11 = r0.m7861d(r5)
            int[] r12 = r0.f4598a
            r13 = r12[r5]
            r14 = 267386880(0xff00000, float:2.3665827E-29)
            r14 = r14 & r11
            int r14 = r14 >>> 20
            boolean r15 = r0.f4604g
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r4 = 1
            if (r15 != 0) goto L_0x005d
            r15 = 17
            if (r14 > r15) goto L_0x005d
            int r15 = r5 + 2
            r12 = r12[r15]
            r15 = r12 & r16
            if (r15 == r9) goto L_0x0057
            long r9 = (long) r15
            int r10 = r7.getInt(r1, r9)
            goto L_0x0058
        L_0x0057:
            r15 = r9
        L_0x0058:
            int r9 = r12 >>> 20
            int r9 = r4 << r9
            goto L_0x005f
        L_0x005d:
            r15 = r9
            r9 = 0
        L_0x005f:
            if (r3 != 0) goto L_0x0461
            r11 = r11 & r16
            long r11 = (long) r11
            switch(r14) {
                case 0: goto L_0x0451;
                case 1: goto L_0x0445;
                case 2: goto L_0x0439;
                case 3: goto L_0x042d;
                case 4: goto L_0x0421;
                case 5: goto L_0x0415;
                case 6: goto L_0x0409;
                case 7: goto L_0x03fd;
                case 8: goto L_0x03f1;
                case 9: goto L_0x03e0;
                case 10: goto L_0x03d1;
                case 11: goto L_0x03c4;
                case 12: goto L_0x03b7;
                case 13: goto L_0x03aa;
                case 14: goto L_0x039d;
                case 15: goto L_0x0390;
                case 16: goto L_0x0383;
                case 17: goto L_0x0372;
                case 18: goto L_0x0363;
                case 19: goto L_0x0354;
                case 20: goto L_0x0345;
                case 21: goto L_0x0336;
                case 22: goto L_0x0327;
                case 23: goto L_0x0318;
                case 24: goto L_0x0309;
                case 25: goto L_0x02fa;
                case 26: goto L_0x02eb;
                case 27: goto L_0x02d8;
                case 28: goto L_0x02c9;
                case 29: goto L_0x02ba;
                case 30: goto L_0x02ab;
                case 31: goto L_0x029c;
                case 32: goto L_0x028d;
                case 33: goto L_0x027e;
                case 34: goto L_0x026f;
                case 35: goto L_0x0260;
                case 36: goto L_0x0251;
                case 37: goto L_0x0242;
                case 38: goto L_0x0233;
                case 39: goto L_0x0224;
                case 40: goto L_0x0215;
                case 41: goto L_0x0206;
                case 42: goto L_0x01f7;
                case 43: goto L_0x01e8;
                case 44: goto L_0x01d9;
                case 45: goto L_0x01ca;
                case 46: goto L_0x01bb;
                case 47: goto L_0x01ac;
                case 48: goto L_0x019d;
                case 49: goto L_0x018a;
                case 50: goto L_0x0181;
                case 51: goto L_0x0172;
                case 52: goto L_0x0163;
                case 53: goto L_0x0154;
                case 54: goto L_0x0145;
                case 55: goto L_0x0136;
                case 56: goto L_0x0127;
                case 57: goto L_0x0118;
                case 58: goto L_0x0109;
                case 59: goto L_0x00fa;
                case 60: goto L_0x00e7;
                case 61: goto L_0x00d6;
                case 62: goto L_0x00c7;
                case 63: goto L_0x00b8;
                case 64: goto L_0x00a9;
                case 65: goto L_0x009a;
                case 66: goto L_0x008b;
                case 67: goto L_0x007c;
                case 68: goto L_0x0069;
                default: goto L_0x0067;
            }
        L_0x0067:
            goto L_0x045c
        L_0x0069:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            com.google.android.gms.internal.measurement.l6 r9 = r0.m7837a(r5)
            r2.mo17971b(r13, r4, r9)
            goto L_0x045c
        L_0x007c:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            long r11 = m7864e(r1, r11)
            r2.mo17982e(r13, r11)
            goto L_0x045c
        L_0x008b:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            int r4 = m7862d(r1, r11)
            r2.mo17978d(r13, r4)
            goto L_0x045c
        L_0x009a:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            long r11 = m7864e(r1, r11)
            r2.mo17959a(r13, r11)
            goto L_0x045c
        L_0x00a9:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            int r4 = m7862d(r1, r11)
            r2.mo17975c(r13, r4)
            goto L_0x045c
        L_0x00b8:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            int r4 = m7862d(r1, r11)
            r2.mo17958a(r13, r4)
            goto L_0x045c
        L_0x00c7:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            int r4 = m7862d(r1, r11)
            r2.mo17984f(r13, r4)
            goto L_0x045c
        L_0x00d6:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            com.google.android.gms.internal.measurement.f3 r4 = (com.google.android.gms.internal.measurement.C2498f3) r4
            r2.mo17960a(r13, r4)
            goto L_0x045c
        L_0x00e7:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            com.google.android.gms.internal.measurement.l6 r9 = r0.m7837a(r5)
            r2.mo17962a(r13, r4, r9)
            goto L_0x045c
        L_0x00fa:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            m7843a(r13, r4, r2)
            goto L_0x045c
        L_0x0109:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            boolean r4 = m7867f(r1, r11)
            r2.mo17967a(r13, r4)
            goto L_0x045c
        L_0x0118:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            int r4 = m7862d(r1, r11)
            r2.mo17981e(r13, r4)
            goto L_0x045c
        L_0x0127:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            long r11 = m7864e(r1, r11)
            r2.mo17979d(r13, r11)
            goto L_0x045c
        L_0x0136:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            int r4 = m7862d(r1, r11)
            r2.mo17969b(r13, r4)
            goto L_0x045c
        L_0x0145:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            long r11 = m7864e(r1, r11)
            r2.mo17970b(r13, r11)
            goto L_0x045c
        L_0x0154:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            long r11 = m7864e(r1, r11)
            r2.mo17976c(r13, r11)
            goto L_0x045c
        L_0x0163:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            float r4 = m7858c(r1, r11)
            r2.mo17957a(r13, r4)
            goto L_0x045c
        L_0x0172:
            boolean r4 = r0.m7848a(r1, r13, r5)
            if (r4 == 0) goto L_0x045c
            double r11 = m7851b(r1, r11)
            r2.mo17956a(r13, r11)
            goto L_0x045c
        L_0x0181:
            java.lang.Object r4 = r7.getObject(r1, r11)
            r0.m7845a(r2, r13, r4, r5)
            goto L_0x045c
        L_0x018a:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.l6 r11 = r0.m7837a(r5)
            com.google.android.gms.internal.measurement.C2633n6.m6896b(r4, r9, r2, r11)
            goto L_0x045c
        L_0x019d:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6909e(r9, r11, r2, r4)
            goto L_0x045c
        L_0x01ac:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6924j(r9, r11, r2, r4)
            goto L_0x045c
        L_0x01bb:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6915g(r9, r11, r2, r4)
            goto L_0x045c
        L_0x01ca:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6926l(r9, r11, r2, r4)
            goto L_0x045c
        L_0x01d9:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6927m(r9, r11, r2, r4)
            goto L_0x045c
        L_0x01e8:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6921i(r9, r11, r2, r4)
            goto L_0x045c
        L_0x01f7:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6928n(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0206:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6925k(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0215:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6912f(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0224:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6918h(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0233:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6905d(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0242:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6901c(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0251:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6897b(r9, r11, r2, r4)
            goto L_0x045c
        L_0x0260:
            int[] r9 = r0.f4598a
            r9 = r9[r5]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            com.google.android.gms.internal.measurement.C2633n6.m6884a(r9, r11, r2, r4)
            goto L_0x045c
        L_0x026f:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6909e(r4, r9, r2, r8)
            goto L_0x045c
        L_0x027e:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6924j(r4, r9, r2, r8)
            goto L_0x045c
        L_0x028d:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6915g(r4, r9, r2, r8)
            goto L_0x045c
        L_0x029c:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6926l(r4, r9, r2, r8)
            goto L_0x045c
        L_0x02ab:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6927m(r4, r9, r2, r8)
            goto L_0x045c
        L_0x02ba:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6921i(r4, r9, r2, r8)
            goto L_0x045c
        L_0x02c9:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6895b(r4, r9, r2)
            goto L_0x045c
        L_0x02d8:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.l6 r11 = r0.m7837a(r5)
            com.google.android.gms.internal.measurement.C2633n6.m6883a(r4, r9, r2, r11)
            goto L_0x045c
        L_0x02eb:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6882a(r4, r9, r2)
            goto L_0x045c
        L_0x02fa:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6928n(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0309:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6925k(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0318:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6912f(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0327:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6918h(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0336:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6905d(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0345:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6901c(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0354:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6897b(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0363:
            int[] r4 = r0.f4598a
            r4 = r4[r5]
            java.lang.Object r9 = r7.getObject(r1, r11)
            java.util.List r9 = (java.util.List) r9
            com.google.android.gms.internal.measurement.C2633n6.m6884a(r4, r9, r2, r8)
            goto L_0x045c
        L_0x0372:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            com.google.android.gms.internal.measurement.l6 r9 = r0.m7837a(r5)
            r2.mo17971b(r13, r4, r9)
            goto L_0x045c
        L_0x0383:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            long r11 = r7.getLong(r1, r11)
            r2.mo17982e(r13, r11)
            goto L_0x045c
        L_0x0390:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            int r4 = r7.getInt(r1, r11)
            r2.mo17978d(r13, r4)
            goto L_0x045c
        L_0x039d:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            long r11 = r7.getLong(r1, r11)
            r2.mo17959a(r13, r11)
            goto L_0x045c
        L_0x03aa:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            int r4 = r7.getInt(r1, r11)
            r2.mo17975c(r13, r4)
            goto L_0x045c
        L_0x03b7:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            int r4 = r7.getInt(r1, r11)
            r2.mo17958a(r13, r4)
            goto L_0x045c
        L_0x03c4:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            int r4 = r7.getInt(r1, r11)
            r2.mo17984f(r13, r4)
            goto L_0x045c
        L_0x03d1:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            com.google.android.gms.internal.measurement.f3 r4 = (com.google.android.gms.internal.measurement.C2498f3) r4
            r2.mo17960a(r13, r4)
            goto L_0x045c
        L_0x03e0:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            com.google.android.gms.internal.measurement.l6 r9 = r0.m7837a(r5)
            r2.mo17962a(r13, r4, r9)
            goto L_0x045c
        L_0x03f1:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            java.lang.Object r4 = r7.getObject(r1, r11)
            m7843a(r13, r4, r2)
            goto L_0x045c
        L_0x03fd:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            boolean r4 = com.google.android.gms.internal.measurement.C2566j7.m6535c(r1, r11)
            r2.mo17967a(r13, r4)
            goto L_0x045c
        L_0x0409:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            int r4 = r7.getInt(r1, r11)
            r2.mo17981e(r13, r4)
            goto L_0x045c
        L_0x0415:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            long r11 = r7.getLong(r1, r11)
            r2.mo17979d(r13, r11)
            goto L_0x045c
        L_0x0421:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            int r4 = r7.getInt(r1, r11)
            r2.mo17969b(r13, r4)
            goto L_0x045c
        L_0x042d:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            long r11 = r7.getLong(r1, r11)
            r2.mo17970b(r13, r11)
            goto L_0x045c
        L_0x0439:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            long r11 = r7.getLong(r1, r11)
            r2.mo17976c(r13, r11)
            goto L_0x045c
        L_0x0445:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            float r4 = com.google.android.gms.internal.measurement.C2566j7.m6536d(r1, r11)
            r2.mo17957a(r13, r4)
            goto L_0x045c
        L_0x0451:
            r4 = r10 & r9
            if (r4 == 0) goto L_0x045c
            double r11 = com.google.android.gms.internal.measurement.C2566j7.m6541e(r1, r11)
            r2.mo17956a(r13, r11)
        L_0x045c:
            int r5 = r5 + 3
            r9 = r15
            goto L_0x002e
        L_0x0461:
            com.google.android.gms.internal.measurement.a4<?> r1 = r0.f4612o
            r1.mo17266a(r3)
            r4 = 0
            throw r4
        L_0x0468:
            r4 = 0
            if (r3 != 0) goto L_0x0471
            com.google.android.gms.internal.measurement.d7<?, ?> r3 = r0.f4611n
            m7844a(r3, r1, r2)
            return
        L_0x0471:
            com.google.android.gms.internal.measurement.a4<?> r1 = r0.f4612o
            r1.mo17269a(r2, r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2801y5.m7856b(java.lang.Object, com.google.android.gms.internal.measurement.w7):void");
    }

    /* renamed from: a */
    private static Field m7841a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 40 + String.valueOf(name).length() + String.valueOf(arrays).length());
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            sb.append(" not found. Known fields are ");
            sb.append(arrays);
            throw new RuntimeException(sb.toString());
        }
    }

    /* renamed from: a */
    public final T mo17277a() {
        return this.f4609l.mo17333a(this.f4602e);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (com.google.android.gms.internal.measurement.C2633n6.m6889a(com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6), com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (com.google.android.gms.internal.measurement.C2633n6.m6889a(com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6), com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (com.google.android.gms.internal.measurement.C2633n6.m6889a(com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6), com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (com.google.android.gms.internal.measurement.C2633n6.m6889a(com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6), com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6535c(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6535c(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6) == com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.C2566j7.m6536d(r10, r6)) == java.lang.Float.floatToIntBits(com.google.android.gms.internal.measurement.C2566j7.m6536d(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.C2566j7.m6541e(r10, r6)) == java.lang.Double.doubleToLongBits(com.google.android.gms.internal.measurement.C2566j7.m6541e(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (com.google.android.gms.internal.measurement.C2633n6.m6889a(com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6), com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)) != false) goto L_0x01c2;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo17280a(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.f4598a
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.m7861d(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.m7863e(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r4)
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r3 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r10, r6)
            java.lang.Object r5 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r11, r6)
            boolean r4 = com.google.android.gms.internal.measurement.C2633n6.m6889a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = com.google.android.gms.internal.measurement.C2566j7.m6535c(r10, r6)
            boolean r5 = com.google.android.gms.internal.measurement.C2566j7.m6535c(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r10, r6)
            int r5 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r10, r6)
            long r6 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = com.google.android.gms.internal.measurement.C2566j7.m6536d(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = com.google.android.gms.internal.measurement.C2566j7.m6536d(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.m7860c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = com.google.android.gms.internal.measurement.C2566j7.m6541e(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = com.google.android.gms.internal.measurement.C2566j7.m6541e(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            com.google.android.gms.internal.measurement.d7<?, ?> r0 = r9.f4611n
            java.lang.Object r0 = r0.mo17419a(r10)
            com.google.android.gms.internal.measurement.d7<?, ?> r2 = r9.f4611n
            java.lang.Object r2 = r2.mo17419a(r11)
            boolean r0 = r0.equals(r2)
            if (r0 != 0) goto L_0x01dc
            return r1
        L_0x01dc:
            boolean r0 = r9.f4603f
            if (r0 == 0) goto L_0x01f1
            com.google.android.gms.internal.measurement.a4<?> r0 = r9.f4612o
            com.google.android.gms.internal.measurement.b4 r10 = r0.mo17267a(r10)
            com.google.android.gms.internal.measurement.a4<?> r0 = r9.f4612o
            com.google.android.gms.internal.measurement.b4 r11 = r0.mo17267a(r11)
            boolean r10 = r10.equals(r11)
            return r10
        L_0x01f1:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2801y5.mo17280a(java.lang.Object, java.lang.Object):boolean");
    }

    /* renamed from: a */
    public final int mo17276a(T t) {
        int i;
        int i2;
        int length = this.f4598a.length;
        int i3 = 0;
        for (int i4 = 0; i4 < length; i4 += 3) {
            int d = m7861d(i4);
            int i5 = this.f4598a[i4];
            long j = (long) (1048575 & d);
            int i6 = 37;
            switch ((d & 267386880) >>> 20) {
                case 0:
                    i2 = i3 * 53;
                    i = C2647o4.m6959a(Double.doubleToLongBits(C2566j7.m6541e(t, j)));
                    i3 = i2 + i;
                    break;
                case 1:
                    i2 = i3 * 53;
                    i = Float.floatToIntBits(C2566j7.m6536d(t, j));
                    i3 = i2 + i;
                    break;
                case 2:
                    i2 = i3 * 53;
                    i = C2647o4.m6959a(C2566j7.m6527b(t, j));
                    i3 = i2 + i;
                    break;
                case 3:
                    i2 = i3 * 53;
                    i = C2647o4.m6959a(C2566j7.m6527b(t, j));
                    i3 = i2 + i;
                    break;
                case 4:
                    i2 = i3 * 53;
                    i = C2566j7.m6514a(t, j);
                    i3 = i2 + i;
                    break;
                case 5:
                    i2 = i3 * 53;
                    i = C2647o4.m6959a(C2566j7.m6527b(t, j));
                    i3 = i2 + i;
                    break;
                case 6:
                    i2 = i3 * 53;
                    i = C2566j7.m6514a(t, j);
                    i3 = i2 + i;
                    break;
                case 7:
                    i2 = i3 * 53;
                    i = C2647o4.m6960a(C2566j7.m6535c(t, j));
                    i3 = i2 + i;
                    break;
                case 8:
                    i2 = i3 * 53;
                    i = ((String) C2566j7.m6544f(t, j)).hashCode();
                    i3 = i2 + i;
                    break;
                case 9:
                    Object f = C2566j7.m6544f(t, j);
                    if (f != null) {
                        i6 = f.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 10:
                    i2 = i3 * 53;
                    i = C2566j7.m6544f(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 11:
                    i2 = i3 * 53;
                    i = C2566j7.m6514a(t, j);
                    i3 = i2 + i;
                    break;
                case 12:
                    i2 = i3 * 53;
                    i = C2566j7.m6514a(t, j);
                    i3 = i2 + i;
                    break;
                case 13:
                    i2 = i3 * 53;
                    i = C2566j7.m6514a(t, j);
                    i3 = i2 + i;
                    break;
                case 14:
                    i2 = i3 * 53;
                    i = C2647o4.m6959a(C2566j7.m6527b(t, j));
                    i3 = i2 + i;
                    break;
                case 15:
                    i2 = i3 * 53;
                    i = C2566j7.m6514a(t, j);
                    i3 = i2 + i;
                    break;
                case 16:
                    i2 = i3 * 53;
                    i = C2647o4.m6959a(C2566j7.m6527b(t, j));
                    i3 = i2 + i;
                    break;
                case 17:
                    Object f2 = C2566j7.m6544f(t, j);
                    if (f2 != null) {
                        i6 = f2.hashCode();
                    }
                    i3 = (i3 * 53) + i6;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i2 = i3 * 53;
                    i = C2566j7.m6544f(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 50:
                    i2 = i3 * 53;
                    i = C2566j7.m6544f(t, j).hashCode();
                    i3 = i2 + i;
                    break;
                case 51:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6959a(Double.doubleToLongBits(m7851b(t, j)));
                        i3 = i2 + i;
                        break;
                    }
                case 52:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = Float.floatToIntBits(m7858c(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 53:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6959a(m7864e(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 54:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6959a(m7864e(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 55:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = m7862d(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 56:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6959a(m7864e(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 57:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = m7862d(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 58:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6960a(m7867f(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 59:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = ((String) C2566j7.m6544f(t, j)).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 60:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2566j7.m6544f(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 61:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2566j7.m6544f(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
                case 62:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = m7862d(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 63:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = m7862d(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 64:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = m7862d(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 65:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6959a(m7864e(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 66:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = m7862d(t, j);
                        i3 = i2 + i;
                        break;
                    }
                case 67:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2647o4.m6959a(m7864e(t, j));
                        i3 = i2 + i;
                        break;
                    }
                case 68:
                    if (!m7848a(t, i5, i4)) {
                        break;
                    } else {
                        i2 = i3 * 53;
                        i = C2566j7.m6544f(t, j).hashCode();
                        i3 = i2 + i;
                        break;
                    }
            }
        }
        int hashCode = (i3 * 53) + this.f4611n.mo17419a(t).hashCode();
        return this.f4603f ? (hashCode * 53) + this.f4612o.mo17267a((Object) t).hashCode() : hashCode;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.a(int, int):int
      com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int
      com.google.android.gms.internal.measurement.y5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.b(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.y5.b(int, int):int
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.l6.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, int):void */
    /* renamed from: a */
    private final void m7846a(T t, T t2, int i) {
        long d = (long) (m7861d(i) & 1048575);
        if (m7847a((Object) t2, i)) {
            Object f = C2566j7.m6544f(t, d);
            Object f2 = C2566j7.m6544f(t2, d);
            if (f != null && f2 != null) {
                C2566j7.m6522a(t, d, C2647o4.m6962a(f, f2));
                m7854b((Object) t, i);
            } else if (f2 != null) {
                C2566j7.m6522a(t, d, f2);
                m7854b((Object) t, i);
            }
        }
    }

    /* renamed from: a */
    private static <UT, UB> int m7833a(C2469d7<UT, UB> d7Var, T t) {
        return d7Var.mo17428d(d7Var.mo17419a(t));
    }

    /* renamed from: a */
    private static List<?> m7842a(Object obj, long j) {
        return (List) C2566j7.m6544f(obj, j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.w7, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.w7, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<?>, com.google.android.gms.internal.measurement.w7, com.google.android.gms.internal.measurement.l6):void
      com.google.android.gms.internal.measurement.n6.b(int, java.util.List<java.lang.Float>, com.google.android.gms.internal.measurement.w7, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.w7, boolean):void
     arg types: [int, java.util.List, com.google.android.gms.internal.measurement.w7, int]
     candidates:
      com.google.android.gms.internal.measurement.n6.a(int, int, java.lang.Object, com.google.android.gms.internal.measurement.d7):UB
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<?>, com.google.android.gms.internal.measurement.w7, com.google.android.gms.internal.measurement.l6):void
      com.google.android.gms.internal.measurement.n6.a(com.google.android.gms.internal.measurement.r5, java.lang.Object, java.lang.Object, long):void
      com.google.android.gms.internal.measurement.n6.a(int, java.util.List<java.lang.Double>, com.google.android.gms.internal.measurement.w7, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.a(int, int):int
      com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int
      com.google.android.gms.internal.measurement.y5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.b(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
     arg types: [T, com.google.android.gms.internal.measurement.w7]
     candidates:
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, long):double
      com.google.android.gms.internal.measurement.y5.b(int, int):int
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, int):void
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.l6.b(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.y5.b(java.lang.Object, com.google.android.gms.internal.measurement.w7):void */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:159:0x04bc A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x04bd  */
    /* JADX WARNING: Removed duplicated region for block: B:172:0x04eb  */
    /* JADX WARNING: Removed duplicated region for block: B:321:0x096d  */
    /* JADX WARNING: Removed duplicated region for block: B:323:0x0973  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo17278a(T r13, com.google.android.gms.internal.measurement.C2771w7 r14) {
        /*
            r12 = this;
            int r0 = r14.mo17954a()
            int r1 = com.google.android.gms.internal.measurement.C2595l4.C2601f.f4301l
            r2 = 267386880(0xff00000, float:2.3665827E-29)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1048575(0xfffff, float:1.469367E-39)
            if (r0 != r1) goto L_0x04c3
            com.google.android.gms.internal.measurement.d7<?, ?> r0 = r12.f4611n
            m7844a(r0, r13, r14)
            boolean r0 = r12.f4603f
            if (r0 == 0) goto L_0x0032
            com.google.android.gms.internal.measurement.a4<?> r0 = r12.f4612o
            com.google.android.gms.internal.measurement.b4 r0 = r0.mo17267a(r13)
            com.google.android.gms.internal.measurement.m6<T, java.lang.Object> r1 = r0.f3996a
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x0032
            java.util.Iterator r0 = r0.mo17315d()
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            goto L_0x0033
        L_0x0032:
            r0 = r3
        L_0x0033:
            int[] r1 = r12.f4598a
            int r1 = r1.length
            int r1 = r1 + -3
        L_0x0038:
            if (r1 < 0) goto L_0x04ba
            int r7 = r12.m7861d(r1)
            int[] r8 = r12.f4598a
            r9 = r8[r1]
            if (r0 != 0) goto L_0x04b4
            r10 = r7 & r2
            int r10 = r10 >>> 20
            switch(r10) {
                case 0: goto L_0x04a1;
                case 1: goto L_0x0491;
                case 2: goto L_0x0481;
                case 3: goto L_0x0471;
                case 4: goto L_0x0461;
                case 5: goto L_0x0451;
                case 6: goto L_0x0441;
                case 7: goto L_0x0430;
                case 8: goto L_0x041f;
                case 9: goto L_0x040a;
                case 10: goto L_0x03f7;
                case 11: goto L_0x03e6;
                case 12: goto L_0x03d5;
                case 13: goto L_0x03c4;
                case 14: goto L_0x03b3;
                case 15: goto L_0x03a2;
                case 16: goto L_0x0391;
                case 17: goto L_0x037c;
                case 18: goto L_0x036d;
                case 19: goto L_0x035e;
                case 20: goto L_0x034f;
                case 21: goto L_0x0340;
                case 22: goto L_0x0331;
                case 23: goto L_0x0322;
                case 24: goto L_0x0313;
                case 25: goto L_0x0304;
                case 26: goto L_0x02f5;
                case 27: goto L_0x02e2;
                case 28: goto L_0x02d3;
                case 29: goto L_0x02c4;
                case 30: goto L_0x02b5;
                case 31: goto L_0x02a6;
                case 32: goto L_0x0297;
                case 33: goto L_0x0288;
                case 34: goto L_0x0279;
                case 35: goto L_0x026a;
                case 36: goto L_0x025b;
                case 37: goto L_0x024c;
                case 38: goto L_0x023d;
                case 39: goto L_0x022e;
                case 40: goto L_0x021f;
                case 41: goto L_0x0210;
                case 42: goto L_0x0201;
                case 43: goto L_0x01f2;
                case 44: goto L_0x01e3;
                case 45: goto L_0x01d4;
                case 46: goto L_0x01c5;
                case 47: goto L_0x01b6;
                case 48: goto L_0x01a7;
                case 49: goto L_0x0194;
                case 50: goto L_0x0189;
                case 51: goto L_0x0178;
                case 52: goto L_0x0167;
                case 53: goto L_0x0156;
                case 54: goto L_0x0145;
                case 55: goto L_0x0134;
                case 56: goto L_0x0123;
                case 57: goto L_0x0112;
                case 58: goto L_0x0101;
                case 59: goto L_0x00f0;
                case 60: goto L_0x00db;
                case 61: goto L_0x00c8;
                case 62: goto L_0x00b7;
                case 63: goto L_0x00a6;
                case 64: goto L_0x0095;
                case 65: goto L_0x0084;
                case 66: goto L_0x0073;
                case 67: goto L_0x0062;
                case 68: goto L_0x004d;
                default: goto L_0x004b;
            }
        L_0x004b:
            goto L_0x04b0
        L_0x004d:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            com.google.android.gms.internal.measurement.l6 r8 = r12.m7837a(r1)
            r14.mo17971b(r9, r7, r8)
            goto L_0x04b0
        L_0x0062:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = m7864e(r13, r7)
            r14.mo17982e(r9, r7)
            goto L_0x04b0
        L_0x0073:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = m7862d(r13, r7)
            r14.mo17978d(r9, r7)
            goto L_0x04b0
        L_0x0084:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = m7864e(r13, r7)
            r14.mo17959a(r9, r7)
            goto L_0x04b0
        L_0x0095:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = m7862d(r13, r7)
            r14.mo17975c(r9, r7)
            goto L_0x04b0
        L_0x00a6:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = m7862d(r13, r7)
            r14.mo17958a(r9, r7)
            goto L_0x04b0
        L_0x00b7:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = m7862d(r13, r7)
            r14.mo17984f(r9, r7)
            goto L_0x04b0
        L_0x00c8:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            com.google.android.gms.internal.measurement.f3 r7 = (com.google.android.gms.internal.measurement.C2498f3) r7
            r14.mo17960a(r9, r7)
            goto L_0x04b0
        L_0x00db:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            com.google.android.gms.internal.measurement.l6 r8 = r12.m7837a(r1)
            r14.mo17962a(r9, r7, r8)
            goto L_0x04b0
        L_0x00f0:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            m7843a(r9, r7, r14)
            goto L_0x04b0
        L_0x0101:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            boolean r7 = m7867f(r13, r7)
            r14.mo17967a(r9, r7)
            goto L_0x04b0
        L_0x0112:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = m7862d(r13, r7)
            r14.mo17981e(r9, r7)
            goto L_0x04b0
        L_0x0123:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = m7864e(r13, r7)
            r14.mo17979d(r9, r7)
            goto L_0x04b0
        L_0x0134:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = m7862d(r13, r7)
            r14.mo17969b(r9, r7)
            goto L_0x04b0
        L_0x0145:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = m7864e(r13, r7)
            r14.mo17970b(r9, r7)
            goto L_0x04b0
        L_0x0156:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = m7864e(r13, r7)
            r14.mo17976c(r9, r7)
            goto L_0x04b0
        L_0x0167:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            float r7 = m7858c(r13, r7)
            r14.mo17957a(r9, r7)
            goto L_0x04b0
        L_0x0178:
            boolean r8 = r12.m7848a(r13, r9, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            double r7 = m7851b(r13, r7)
            r14.mo17956a(r9, r7)
            goto L_0x04b0
        L_0x0189:
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            r12.m7845a(r14, r9, r7, r1)
            goto L_0x04b0
        L_0x0194:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.l6 r9 = r12.m7837a(r1)
            com.google.android.gms.internal.measurement.C2633n6.m6896b(r8, r7, r14, r9)
            goto L_0x04b0
        L_0x01a7:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6909e(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01b6:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6924j(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01c5:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6915g(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01d4:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6926l(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01e3:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6927m(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x01f2:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6921i(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x0201:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6928n(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x0210:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6925k(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x021f:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6912f(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x022e:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6918h(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x023d:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6905d(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x024c:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6901c(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x025b:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6897b(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x026a:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6884a(r8, r7, r14, r4)
            goto L_0x04b0
        L_0x0279:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6909e(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0288:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6924j(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0297:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6915g(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02a6:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6926l(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02b5:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6927m(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02c4:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6921i(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x02d3:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6895b(r8, r7, r14)
            goto L_0x04b0
        L_0x02e2:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.l6 r9 = r12.m7837a(r1)
            com.google.android.gms.internal.measurement.C2633n6.m6883a(r8, r7, r14, r9)
            goto L_0x04b0
        L_0x02f5:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6882a(r8, r7, r14)
            goto L_0x04b0
        L_0x0304:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6928n(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0313:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6925k(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0322:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6912f(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0331:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6918h(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x0340:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6905d(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x034f:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6901c(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x035e:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6897b(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x036d:
            r8 = r8[r1]
            r7 = r7 & r6
            long r9 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r9)
            java.util.List r7 = (java.util.List) r7
            com.google.android.gms.internal.measurement.C2633n6.m6884a(r8, r7, r14, r5)
            goto L_0x04b0
        L_0x037c:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            com.google.android.gms.internal.measurement.l6 r8 = r12.m7837a(r1)
            r14.mo17971b(r9, r7, r8)
            goto L_0x04b0
        L_0x0391:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r7)
            r14.mo17982e(r9, r7)
            goto L_0x04b0
        L_0x03a2:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r7)
            r14.mo17978d(r9, r7)
            goto L_0x04b0
        L_0x03b3:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r7)
            r14.mo17959a(r9, r7)
            goto L_0x04b0
        L_0x03c4:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r7)
            r14.mo17975c(r9, r7)
            goto L_0x04b0
        L_0x03d5:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r7)
            r14.mo17958a(r9, r7)
            goto L_0x04b0
        L_0x03e6:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r7)
            r14.mo17984f(r9, r7)
            goto L_0x04b0
        L_0x03f7:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            com.google.android.gms.internal.measurement.f3 r7 = (com.google.android.gms.internal.measurement.C2498f3) r7
            r14.mo17960a(r9, r7)
            goto L_0x04b0
        L_0x040a:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            com.google.android.gms.internal.measurement.l6 r8 = r12.m7837a(r1)
            r14.mo17962a(r9, r7, r8)
            goto L_0x04b0
        L_0x041f:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            java.lang.Object r7 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r7)
            m7843a(r9, r7, r14)
            goto L_0x04b0
        L_0x0430:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            boolean r7 = com.google.android.gms.internal.measurement.C2566j7.m6535c(r13, r7)
            r14.mo17967a(r9, r7)
            goto L_0x04b0
        L_0x0441:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r7)
            r14.mo17981e(r9, r7)
            goto L_0x04b0
        L_0x0451:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r7)
            r14.mo17979d(r9, r7)
            goto L_0x04b0
        L_0x0461:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            int r7 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r7)
            r14.mo17969b(r9, r7)
            goto L_0x04b0
        L_0x0471:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r7)
            r14.mo17970b(r9, r7)
            goto L_0x04b0
        L_0x0481:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            long r7 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r7)
            r14.mo17976c(r9, r7)
            goto L_0x04b0
        L_0x0491:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            float r7 = com.google.android.gms.internal.measurement.C2566j7.m6536d(r13, r7)
            r14.mo17957a(r9, r7)
            goto L_0x04b0
        L_0x04a1:
            boolean r8 = r12.m7847a(r13, r1)
            if (r8 == 0) goto L_0x04b0
            r7 = r7 & r6
            long r7 = (long) r7
            double r7 = com.google.android.gms.internal.measurement.C2566j7.m6541e(r13, r7)
            r14.mo17956a(r9, r7)
        L_0x04b0:
            int r1 = r1 + -3
            goto L_0x0038
        L_0x04b4:
            com.google.android.gms.internal.measurement.a4<?> r13 = r12.f4612o
            r13.mo17266a(r0)
            throw r3
        L_0x04ba:
            if (r0 != 0) goto L_0x04bd
            return
        L_0x04bd:
            com.google.android.gms.internal.measurement.a4<?> r13 = r12.f4612o
            r13.mo17269a(r14, r0)
            throw r3
        L_0x04c3:
            boolean r0 = r12.f4604g
            if (r0 == 0) goto L_0x0979
            boolean r0 = r12.f4603f
            if (r0 == 0) goto L_0x04e4
            com.google.android.gms.internal.measurement.a4<?> r0 = r12.f4612o
            com.google.android.gms.internal.measurement.b4 r0 = r0.mo17267a(r13)
            com.google.android.gms.internal.measurement.m6<T, java.lang.Object> r1 = r0.f3996a
            boolean r1 = r1.isEmpty()
            if (r1 != 0) goto L_0x04e4
            java.util.Iterator r0 = r0.mo17313c()
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            goto L_0x04e5
        L_0x04e4:
            r0 = r3
        L_0x04e5:
            int[] r1 = r12.f4598a
            int r1 = r1.length
            r7 = 0
        L_0x04e9:
            if (r7 >= r1) goto L_0x096b
            int r8 = r12.m7861d(r7)
            int[] r9 = r12.f4598a
            r10 = r9[r7]
            if (r0 != 0) goto L_0x0965
            r11 = r8 & r2
            int r11 = r11 >>> 20
            switch(r11) {
                case 0: goto L_0x0952;
                case 1: goto L_0x0942;
                case 2: goto L_0x0932;
                case 3: goto L_0x0922;
                case 4: goto L_0x0912;
                case 5: goto L_0x0902;
                case 6: goto L_0x08f2;
                case 7: goto L_0x08e1;
                case 8: goto L_0x08d0;
                case 9: goto L_0x08bb;
                case 10: goto L_0x08a8;
                case 11: goto L_0x0897;
                case 12: goto L_0x0886;
                case 13: goto L_0x0875;
                case 14: goto L_0x0864;
                case 15: goto L_0x0853;
                case 16: goto L_0x0842;
                case 17: goto L_0x082d;
                case 18: goto L_0x081e;
                case 19: goto L_0x080f;
                case 20: goto L_0x0800;
                case 21: goto L_0x07f1;
                case 22: goto L_0x07e2;
                case 23: goto L_0x07d3;
                case 24: goto L_0x07c4;
                case 25: goto L_0x07b5;
                case 26: goto L_0x07a6;
                case 27: goto L_0x0793;
                case 28: goto L_0x0784;
                case 29: goto L_0x0775;
                case 30: goto L_0x0766;
                case 31: goto L_0x0757;
                case 32: goto L_0x0748;
                case 33: goto L_0x0739;
                case 34: goto L_0x072a;
                case 35: goto L_0x071b;
                case 36: goto L_0x070c;
                case 37: goto L_0x06fd;
                case 38: goto L_0x06ee;
                case 39: goto L_0x06df;
                case 40: goto L_0x06d0;
                case 41: goto L_0x06c1;
                case 42: goto L_0x06b2;
                case 43: goto L_0x06a3;
                case 44: goto L_0x0694;
                case 45: goto L_0x0685;
                case 46: goto L_0x0676;
                case 47: goto L_0x0667;
                case 48: goto L_0x0658;
                case 49: goto L_0x0645;
                case 50: goto L_0x063a;
                case 51: goto L_0x0629;
                case 52: goto L_0x0618;
                case 53: goto L_0x0607;
                case 54: goto L_0x05f6;
                case 55: goto L_0x05e5;
                case 56: goto L_0x05d4;
                case 57: goto L_0x05c3;
                case 58: goto L_0x05b2;
                case 59: goto L_0x05a1;
                case 60: goto L_0x058c;
                case 61: goto L_0x0579;
                case 62: goto L_0x0568;
                case 63: goto L_0x0557;
                case 64: goto L_0x0546;
                case 65: goto L_0x0535;
                case 66: goto L_0x0524;
                case 67: goto L_0x0513;
                case 68: goto L_0x04fe;
                default: goto L_0x04fc;
            }
        L_0x04fc:
            goto L_0x0961
        L_0x04fe:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            com.google.android.gms.internal.measurement.l6 r9 = r12.m7837a(r7)
            r14.mo17971b(r10, r8, r9)
            goto L_0x0961
        L_0x0513:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = m7864e(r13, r8)
            r14.mo17982e(r10, r8)
            goto L_0x0961
        L_0x0524:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = m7862d(r13, r8)
            r14.mo17978d(r10, r8)
            goto L_0x0961
        L_0x0535:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = m7864e(r13, r8)
            r14.mo17959a(r10, r8)
            goto L_0x0961
        L_0x0546:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = m7862d(r13, r8)
            r14.mo17975c(r10, r8)
            goto L_0x0961
        L_0x0557:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = m7862d(r13, r8)
            r14.mo17958a(r10, r8)
            goto L_0x0961
        L_0x0568:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = m7862d(r13, r8)
            r14.mo17984f(r10, r8)
            goto L_0x0961
        L_0x0579:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            com.google.android.gms.internal.measurement.f3 r8 = (com.google.android.gms.internal.measurement.C2498f3) r8
            r14.mo17960a(r10, r8)
            goto L_0x0961
        L_0x058c:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            com.google.android.gms.internal.measurement.l6 r9 = r12.m7837a(r7)
            r14.mo17962a(r10, r8, r9)
            goto L_0x0961
        L_0x05a1:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            m7843a(r10, r8, r14)
            goto L_0x0961
        L_0x05b2:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            boolean r8 = m7867f(r13, r8)
            r14.mo17967a(r10, r8)
            goto L_0x0961
        L_0x05c3:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = m7862d(r13, r8)
            r14.mo17981e(r10, r8)
            goto L_0x0961
        L_0x05d4:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = m7864e(r13, r8)
            r14.mo17979d(r10, r8)
            goto L_0x0961
        L_0x05e5:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = m7862d(r13, r8)
            r14.mo17969b(r10, r8)
            goto L_0x0961
        L_0x05f6:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = m7864e(r13, r8)
            r14.mo17970b(r10, r8)
            goto L_0x0961
        L_0x0607:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = m7864e(r13, r8)
            r14.mo17976c(r10, r8)
            goto L_0x0961
        L_0x0618:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            float r8 = m7858c(r13, r8)
            r14.mo17957a(r10, r8)
            goto L_0x0961
        L_0x0629:
            boolean r9 = r12.m7848a(r13, r10, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            double r8 = m7851b(r13, r8)
            r14.mo17956a(r10, r8)
            goto L_0x0961
        L_0x063a:
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            r12.m7845a(r14, r10, r8, r7)
            goto L_0x0961
        L_0x0645:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.l6 r10 = r12.m7837a(r7)
            com.google.android.gms.internal.measurement.C2633n6.m6896b(r9, r8, r14, r10)
            goto L_0x0961
        L_0x0658:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6909e(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0667:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6924j(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0676:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6915g(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0685:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6926l(r9, r8, r14, r4)
            goto L_0x0961
        L_0x0694:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6927m(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06a3:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6921i(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06b2:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6928n(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06c1:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6925k(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06d0:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6912f(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06df:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6918h(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06ee:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6905d(r9, r8, r14, r4)
            goto L_0x0961
        L_0x06fd:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6901c(r9, r8, r14, r4)
            goto L_0x0961
        L_0x070c:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6897b(r9, r8, r14, r4)
            goto L_0x0961
        L_0x071b:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6884a(r9, r8, r14, r4)
            goto L_0x0961
        L_0x072a:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6909e(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0739:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6924j(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0748:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6915g(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0757:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6926l(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0766:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6927m(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0775:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6921i(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0784:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6895b(r9, r8, r14)
            goto L_0x0961
        L_0x0793:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.l6 r10 = r12.m7837a(r7)
            com.google.android.gms.internal.measurement.C2633n6.m6883a(r9, r8, r14, r10)
            goto L_0x0961
        L_0x07a6:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6882a(r9, r8, r14)
            goto L_0x0961
        L_0x07b5:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6928n(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07c4:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6925k(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07d3:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6912f(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07e2:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6918h(r9, r8, r14, r5)
            goto L_0x0961
        L_0x07f1:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6905d(r9, r8, r14, r5)
            goto L_0x0961
        L_0x0800:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6901c(r9, r8, r14, r5)
            goto L_0x0961
        L_0x080f:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6897b(r9, r8, r14, r5)
            goto L_0x0961
        L_0x081e:
            r9 = r9[r7]
            r8 = r8 & r6
            long r10 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r10)
            java.util.List r8 = (java.util.List) r8
            com.google.android.gms.internal.measurement.C2633n6.m6884a(r9, r8, r14, r5)
            goto L_0x0961
        L_0x082d:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            com.google.android.gms.internal.measurement.l6 r9 = r12.m7837a(r7)
            r14.mo17971b(r10, r8, r9)
            goto L_0x0961
        L_0x0842:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r8)
            r14.mo17982e(r10, r8)
            goto L_0x0961
        L_0x0853:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r8)
            r14.mo17978d(r10, r8)
            goto L_0x0961
        L_0x0864:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r8)
            r14.mo17959a(r10, r8)
            goto L_0x0961
        L_0x0875:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r8)
            r14.mo17975c(r10, r8)
            goto L_0x0961
        L_0x0886:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r8)
            r14.mo17958a(r10, r8)
            goto L_0x0961
        L_0x0897:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r8)
            r14.mo17984f(r10, r8)
            goto L_0x0961
        L_0x08a8:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            com.google.android.gms.internal.measurement.f3 r8 = (com.google.android.gms.internal.measurement.C2498f3) r8
            r14.mo17960a(r10, r8)
            goto L_0x0961
        L_0x08bb:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            com.google.android.gms.internal.measurement.l6 r9 = r12.m7837a(r7)
            r14.mo17962a(r10, r8, r9)
            goto L_0x0961
        L_0x08d0:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            java.lang.Object r8 = com.google.android.gms.internal.measurement.C2566j7.m6544f(r13, r8)
            m7843a(r10, r8, r14)
            goto L_0x0961
        L_0x08e1:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            boolean r8 = com.google.android.gms.internal.measurement.C2566j7.m6535c(r13, r8)
            r14.mo17967a(r10, r8)
            goto L_0x0961
        L_0x08f2:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r8)
            r14.mo17981e(r10, r8)
            goto L_0x0961
        L_0x0902:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r8)
            r14.mo17979d(r10, r8)
            goto L_0x0961
        L_0x0912:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            int r8 = com.google.android.gms.internal.measurement.C2566j7.m6514a(r13, r8)
            r14.mo17969b(r10, r8)
            goto L_0x0961
        L_0x0922:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r8)
            r14.mo17970b(r10, r8)
            goto L_0x0961
        L_0x0932:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            long r8 = com.google.android.gms.internal.measurement.C2566j7.m6527b(r13, r8)
            r14.mo17976c(r10, r8)
            goto L_0x0961
        L_0x0942:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            float r8 = com.google.android.gms.internal.measurement.C2566j7.m6536d(r13, r8)
            r14.mo17957a(r10, r8)
            goto L_0x0961
        L_0x0952:
            boolean r9 = r12.m7847a(r13, r7)
            if (r9 == 0) goto L_0x0961
            r8 = r8 & r6
            long r8 = (long) r8
            double r8 = com.google.android.gms.internal.measurement.C2566j7.m6541e(r13, r8)
            r14.mo17956a(r10, r8)
        L_0x0961:
            int r7 = r7 + 3
            goto L_0x04e9
        L_0x0965:
            com.google.android.gms.internal.measurement.a4<?> r13 = r12.f4612o
            r13.mo17266a(r0)
            throw r3
        L_0x096b:
            if (r0 != 0) goto L_0x0973
            com.google.android.gms.internal.measurement.d7<?, ?> r0 = r12.f4611n
            m7844a(r0, r13, r14)
            return
        L_0x0973:
            com.google.android.gms.internal.measurement.a4<?> r13 = r12.f4612o
            r13.mo17269a(r14, r0)
            throw r3
        L_0x0979:
            r12.m7856b(r13, r14)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2801y5.mo17278a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void");
    }

    /* renamed from: b */
    private final Object m7853b(int i) {
        return this.f4599b[(i / 3) << 1];
    }

    /* renamed from: b */
    public final boolean mo17282b(T t) {
        int i;
        int i2 = 0;
        int i3 = -1;
        int i4 = 0;
        while (true) {
            boolean z = true;
            if (i2 >= this.f4607j) {
                return !this.f4603f || this.f4612o.mo17267a(t).mo17316e();
            }
            int i5 = this.f4606i[i2];
            int i6 = this.f4598a[i5];
            int d = m7861d(i5);
            if (!this.f4604g) {
                int i7 = this.f4598a[i5 + 2];
                int i8 = i7 & 1048575;
                i = 1 << (i7 >>> 20);
                if (i8 != i3) {
                    i4 = f4597r.getInt(t, (long) i8);
                    i3 = i8;
                }
            } else {
                i = 0;
            }
            if (((268435456 & d) != 0) && !m7849a(t, i5, i4, i)) {
                return false;
            }
            int i9 = (267386880 & d) >>> 20;
            if (i9 != 9 && i9 != 17) {
                if (i9 != 27) {
                    if (i9 == 60 || i9 == 68) {
                        if (m7848a(t, i6, i5) && !m7850a(t, d, m7837a(i5))) {
                            return false;
                        }
                    } else if (i9 != 49) {
                        if (i9 == 50 && !this.f4613p.mo17831c(C2566j7.m6544f(t, (long) (d & 1048575))).isEmpty()) {
                            this.f4613p.mo17832d(m7853b(i5));
                            throw null;
                        }
                    }
                }
                List list = (List) C2566j7.m6544f(t, (long) (d & 1048575));
                if (!list.isEmpty()) {
                    C2603l6 a = m7837a(i5);
                    int i10 = 0;
                    while (true) {
                        if (i10 >= list.size()) {
                            break;
                        } else if (!a.mo17282b(list.get(i10))) {
                            z = false;
                            break;
                        } else {
                            i10++;
                        }
                    }
                }
                if (!z) {
                    return false;
                }
            } else if (m7849a(t, i5, i4, i) && !m7850a(t, d, m7837a(i5))) {
                return false;
            }
            i2++;
        }
    }

    /* renamed from: b */
    private static <T> double m7851b(T t, long j) {
        return ((Double) C2566j7.m6544f(t, j)).doubleValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void */
    /* renamed from: b */
    private final void m7854b(T t, int i) {
        if (!this.f4604g) {
            int e = m7863e(i);
            long j = (long) (e & 1048575);
            C2566j7.m6520a((Object) t, j, C2566j7.m6514a(t, j) | (1 << (e >>> 20)));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void */
    /* renamed from: b */
    private final void m7855b(T t, int i, int i2) {
        C2566j7.m6520a((Object) t, (long) (m7863e(i2) & 1048575), i);
    }

    /* renamed from: b */
    private final int m7852b(int i, int i2) {
        int length = (this.f4598a.length / 3) - 1;
        while (i2 <= length) {
            int i3 = (length + i2) >>> 1;
            int i4 = i3 * 3;
            int i5 = this.f4598a[i4];
            if (i == i5) {
                return i4;
            }
            if (i < i5) {
                length = i3 - 1;
            } else {
                i2 = i3 + 1;
            }
        }
        return -1;
    }

    /* renamed from: d */
    private final int m7861d(int i) {
        return this.f4598a[i + 1];
    }

    /* renamed from: d */
    private static <T> int m7862d(T t, long j) {
        return ((Integer) C2566j7.m6544f(t, j)).intValue();
    }

    /* renamed from: a */
    private final <K, V> void m7845a(C2771w7 w7Var, int i, Object obj, int i2) {
        if (obj != null) {
            this.f4613p.mo17832d(m7853b(i2));
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.d7.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
     arg types: [UT, com.google.android.gms.internal.measurement.w7]
     candidates:
      com.google.android.gms.internal.measurement.d7.a(java.lang.Object, java.lang.Object):void
      com.google.android.gms.internal.measurement.d7.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void */
    /* renamed from: a */
    private static <UT, UB> void m7844a(C2469d7<UT, UB> d7Var, T t, C2771w7 w7Var) {
        d7Var.mo17421a((Object) d7Var.mo17419a(t), w7Var);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:58)
        	at jadx.core.utils.ErrorsCounter.error(ErrorsCounter.java:31)
        	at jadx.core.dex.attributes.nodes.NotificationAttrNode.addError(NotificationAttrNode.java:15)
        */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x0422 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x01eb  */
    /* renamed from: a */
    private final int m7835a(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, com.google.android.gms.internal.measurement.C2417a3 r30) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = com.google.android.gms.internal.measurement.C2801y5.f4597r
            java.lang.Object r11 = r11.getObject(r1, r9)
            com.google.android.gms.internal.measurement.u4 r11 = (com.google.android.gms.internal.measurement.C2738u4) r11
            boolean r12 = r11.mo17938a()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            com.google.android.gms.internal.measurement.u4 r11 = r11.mo17320a(r12)
            sun.misc.Unsafe r12 = com.google.android.gms.internal.measurement.C2801y5.f4597r
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r14 = 0
            r10 = 2
            switch(r27) {
                case 18: goto L_0x03e4;
                case 19: goto L_0x03a6;
                case 20: goto L_0x0365;
                case 21: goto L_0x0365;
                case 22: goto L_0x034b;
                case 23: goto L_0x030c;
                case 24: goto L_0x02cd;
                case 25: goto L_0x0276;
                case 26: goto L_0x01c3;
                case 27: goto L_0x01a9;
                case 28: goto L_0x0151;
                case 29: goto L_0x034b;
                case 30: goto L_0x0119;
                case 31: goto L_0x02cd;
                case 32: goto L_0x030c;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03e4;
                case 36: goto L_0x03a6;
                case 37: goto L_0x0365;
                case 38: goto L_0x0365;
                case 39: goto L_0x034b;
                case 40: goto L_0x030c;
                case 41: goto L_0x02cd;
                case 42: goto L_0x0276;
                case 43: goto L_0x034b;
                case 44: goto L_0x0119;
                case 45: goto L_0x02cd;
                case 46: goto L_0x030c;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x003f;
                default: goto L_0x003d;
            }
        L_0x003d:
            goto L_0x0422
        L_0x003f:
            r1 = 3
            if (r6 != r1) goto L_0x0422
            com.google.android.gms.internal.measurement.l6 r1 = r0.m7837a(r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6052a(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.f3973c
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0422
            int r8 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r9 = r7.f3971a
            if (r2 != r9) goto L_0x0422
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6052a(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.f3973c
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r10) goto L_0x00a3
            com.google.android.gms.internal.measurement.i5 r11 = (com.google.android.gms.internal.measurement.C2549i5) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r1, r7)
            long r4 = r7.f3972b
            long r4 = com.google.android.gms.internal.measurement.C2690r3.m7120a(r4)
            r11.mo17575g(r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0423
        L_0x009e:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.i5 r11 = (com.google.android.gms.internal.measurement.C2549i5) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r4, r7)
            long r8 = r7.f3972b
            long r8 = com.google.android.gms.internal.measurement.C2690r3.m7120a(r8)
            r11.mo17575g(r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r4, r7)
            long r8 = r7.f3972b
            long r8 = com.google.android.gms.internal.measurement.C2690r3.m7120a(r8)
            r11.mo17575g(r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r10) goto L_0x00f0
            com.google.android.gms.internal.measurement.m4 r11 = (com.google.android.gms.internal.measurement.C2616m4) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r4 = r7.f3971a
            int r4 = com.google.android.gms.internal.measurement.C2690r3.m7119a(r4)
            r11.mo17734d(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0423
        L_0x00eb:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.m4 r11 = (com.google.android.gms.internal.measurement.C2616m4) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r4 = r7.f3971a
            int r4 = com.google.android.gms.internal.measurement.C2690r3.m7119a(r4)
            r11.mo17734d(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r4 = r7.f3971a
            int r4 = com.google.android.gms.internal.measurement.C2690r3.m7119a(r4)
            r11.mo17734d(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r10) goto L_0x0120
            int r2 = com.google.android.gms.internal.measurement.C2433b3.m6056a(r3, r4, r11, r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0422
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = com.google.android.gms.internal.measurement.C2433b3.m6049a(r2, r3, r4, r5, r6, r7)
        L_0x0131:
            com.google.android.gms.internal.measurement.l4 r1 = (com.google.android.gms.internal.measurement.C2595l4) r1
            com.google.android.gms.internal.measurement.c7 r3 = r1.zzb
            com.google.android.gms.internal.measurement.c7 r4 = com.google.android.gms.internal.measurement.C2453c7.m6143d()
            if (r3 != r4) goto L_0x013c
            r3 = 0
        L_0x013c:
            com.google.android.gms.internal.measurement.p4 r4 = r0.m7859c(r8)
            com.google.android.gms.internal.measurement.d7<?, ?> r5 = r0.f4611n
            r6 = r22
            java.lang.Object r3 = com.google.android.gms.internal.measurement.C2633n6.m6881a(r6, r11, r4, r3, r5)
            com.google.android.gms.internal.measurement.c7 r3 = (com.google.android.gms.internal.measurement.C2453c7) r3
            if (r3 == 0) goto L_0x014e
            r1.zzb = r3
        L_0x014e:
            r1 = r2
            goto L_0x0423
        L_0x0151:
            if (r6 != r10) goto L_0x0422
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r4 = r7.f3971a
            if (r4 < 0) goto L_0x01a4
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x019f
            if (r4 != 0) goto L_0x0167
            com.google.android.gms.internal.measurement.f3 r4 = com.google.android.gms.internal.measurement.C2498f3.f4094Q
            r11.add(r4)
            goto L_0x016f
        L_0x0167:
            com.google.android.gms.internal.measurement.f3 r6 = com.google.android.gms.internal.measurement.C2498f3.m6303a(r3, r1, r4)
            r11.add(r6)
        L_0x016e:
            int r1 = r1 + r4
        L_0x016f:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r4 = r7.f3971a
            if (r4 < 0) goto L_0x019a
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0195
            if (r4 != 0) goto L_0x018d
            com.google.android.gms.internal.measurement.f3 r4 = com.google.android.gms.internal.measurement.C2498f3.f4094Q
            r11.add(r4)
            goto L_0x016f
        L_0x018d:
            com.google.android.gms.internal.measurement.f3 r6 = com.google.android.gms.internal.measurement.C2498f3.m6303a(r3, r1, r4)
            r11.add(r6)
            goto L_0x016e
        L_0x0195:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x019a:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7312e()
            throw r1
        L_0x019f:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x01a4:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7312e()
            throw r1
        L_0x01a9:
            if (r6 != r10) goto L_0x0422
            com.google.android.gms.internal.measurement.l6 r1 = r0.m7837a(r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6051a(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0423
        L_0x01c3:
            if (r6 != r10) goto L_0x0422
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            java.lang.String r1 = ""
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 != 0) goto L_0x0216
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r6 = r7.f3971a
            if (r6 < 0) goto L_0x0211
            if (r6 != 0) goto L_0x01de
            r11.add(r1)
            goto L_0x01e9
        L_0x01de:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.measurement.C2647o4.f4367a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
        L_0x01e8:
            int r4 = r4 + r6
        L_0x01e9:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r8 = r7.f3971a
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r6, r7)
            int r6 = r7.f3971a
            if (r6 < 0) goto L_0x020c
            if (r6 != 0) goto L_0x0201
            r11.add(r1)
            goto L_0x01e9
        L_0x0201:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = com.google.android.gms.internal.measurement.C2647o4.f4367a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
            goto L_0x01e8
        L_0x020c:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7312e()
            throw r1
        L_0x0211:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7312e()
            throw r1
        L_0x0216:
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r6 = r7.f3971a
            if (r6 < 0) goto L_0x0271
            if (r6 != 0) goto L_0x0224
            r11.add(r1)
            goto L_0x0237
        L_0x0224:
            int r8 = r4 + r6
            boolean r9 = com.google.android.gms.internal.measurement.C2604l7.m6696a(r3, r4, r8)
            if (r9 == 0) goto L_0x026c
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.google.android.gms.internal.measurement.C2647o4.f4367a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
        L_0x0236:
            r4 = r8
        L_0x0237:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r8 = r7.f3971a
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r6, r7)
            int r6 = r7.f3971a
            if (r6 < 0) goto L_0x0267
            if (r6 != 0) goto L_0x024f
            r11.add(r1)
            goto L_0x0237
        L_0x024f:
            int r8 = r4 + r6
            boolean r9 = com.google.android.gms.internal.measurement.C2604l7.m6696a(r3, r4, r8)
            if (r9 == 0) goto L_0x0262
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = com.google.android.gms.internal.measurement.C2647o4.f4367a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
            goto L_0x0236
        L_0x0262:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7316i()
            throw r1
        L_0x0267:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7312e()
            throw r1
        L_0x026c:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7316i()
            throw r1
        L_0x0271:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7312e()
            throw r1
        L_0x0276:
            r1 = 0
            if (r6 != r10) goto L_0x029e
            com.google.android.gms.internal.measurement.d3 r11 = (com.google.android.gms.internal.measurement.C2465d3) r11
            int r2 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r4 = r7.f3971a
            int r4 = r4 + r2
        L_0x0282:
            if (r2 >= r4) goto L_0x0295
            int r2 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r2, r7)
            long r5 = r7.f3972b
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0290
            r5 = 1
            goto L_0x0291
        L_0x0290:
            r5 = 0
        L_0x0291:
            r11.mo17402a(r5)
            goto L_0x0282
        L_0x0295:
            if (r2 != r4) goto L_0x0299
            goto L_0x014e
        L_0x0299:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x029e:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.d3 r11 = (com.google.android.gms.internal.measurement.C2465d3) r11
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r4, r7)
            long r8 = r7.f3972b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02ae
            r6 = 1
            goto L_0x02af
        L_0x02ae:
            r6 = 0
        L_0x02af:
            r11.mo17402a(r6)
        L_0x02b2:
            if (r4 >= r5) goto L_0x0422
            int r6 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r8 = r7.f3971a
            if (r2 != r8) goto L_0x0422
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r6, r7)
            long r8 = r7.f3972b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02c8
            r6 = 1
            goto L_0x02c9
        L_0x02c8:
            r6 = 0
        L_0x02c9:
            r11.mo17402a(r6)
            goto L_0x02b2
        L_0x02cd:
            if (r6 != r10) goto L_0x02ed
            com.google.android.gms.internal.measurement.m4 r11 = (com.google.android.gms.internal.measurement.C2616m4) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x02d8:
            if (r1 >= r2) goto L_0x02e4
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6054a(r3, r1)
            r11.mo17734d(r4)
            int r1 = r1 + 4
            goto L_0x02d8
        L_0x02e4:
            if (r1 != r2) goto L_0x02e8
            goto L_0x0423
        L_0x02e8:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x02ed:
            if (r6 != r9) goto L_0x0422
            com.google.android.gms.internal.measurement.m4 r11 = (com.google.android.gms.internal.measurement.C2616m4) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6054a(r18, r19)
            r11.mo17734d(r1)
        L_0x02f8:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6054a(r3, r4)
            r11.mo17734d(r1)
            goto L_0x02f8
        L_0x030c:
            if (r6 != r10) goto L_0x032c
            com.google.android.gms.internal.measurement.i5 r11 = (com.google.android.gms.internal.measurement.C2549i5) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x0317:
            if (r1 >= r2) goto L_0x0323
            long r4 = com.google.android.gms.internal.measurement.C2433b3.m6058b(r3, r1)
            r11.mo17575g(r4)
            int r1 = r1 + 8
            goto L_0x0317
        L_0x0323:
            if (r1 != r2) goto L_0x0327
            goto L_0x0423
        L_0x0327:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x032c:
            if (r6 != r13) goto L_0x0422
            com.google.android.gms.internal.measurement.i5 r11 = (com.google.android.gms.internal.measurement.C2549i5) r11
            long r8 = com.google.android.gms.internal.measurement.C2433b3.m6058b(r18, r19)
            r11.mo17575g(r8)
        L_0x0337:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            long r8 = com.google.android.gms.internal.measurement.C2433b3.m6058b(r3, r4)
            r11.mo17575g(r8)
            goto L_0x0337
        L_0x034b:
            if (r6 != r10) goto L_0x0353
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6056a(r3, r4, r11, r7)
            goto L_0x0423
        L_0x0353:
            if (r6 != 0) goto L_0x0422
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6049a(r21, r22, r23, r24, r25, r26)
            goto L_0x0423
        L_0x0365:
            if (r6 != r10) goto L_0x0385
            com.google.android.gms.internal.measurement.i5 r11 = (com.google.android.gms.internal.measurement.C2549i5) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x0370:
            if (r1 >= r2) goto L_0x037c
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r1, r7)
            long r4 = r7.f3972b
            r11.mo17575g(r4)
            goto L_0x0370
        L_0x037c:
            if (r1 != r2) goto L_0x0380
            goto L_0x0423
        L_0x0380:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x0385:
            if (r6 != 0) goto L_0x0422
            com.google.android.gms.internal.measurement.i5 r11 = (com.google.android.gms.internal.measurement.C2549i5) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r4, r7)
            long r8 = r7.f3972b
            r11.mo17575g(r8)
        L_0x0392:
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r3, r4, r7)
            long r8 = r7.f3972b
            r11.mo17575g(r8)
            goto L_0x0392
        L_0x03a6:
            if (r6 != r10) goto L_0x03c5
            com.google.android.gms.internal.measurement.h4 r11 = (com.google.android.gms.internal.measurement.C2533h4) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x03b1:
            if (r1 >= r2) goto L_0x03bd
            float r4 = com.google.android.gms.internal.measurement.C2433b3.m6061d(r3, r1)
            r11.mo17542a(r4)
            int r1 = r1 + 4
            goto L_0x03b1
        L_0x03bd:
            if (r1 != r2) goto L_0x03c0
            goto L_0x0423
        L_0x03c0:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x03c5:
            if (r6 != r9) goto L_0x0422
            com.google.android.gms.internal.measurement.h4 r11 = (com.google.android.gms.internal.measurement.C2533h4) r11
            float r1 = com.google.android.gms.internal.measurement.C2433b3.m6061d(r18, r19)
            r11.mo17542a(r1)
        L_0x03d0:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            float r1 = com.google.android.gms.internal.measurement.C2433b3.m6061d(r3, r4)
            r11.mo17542a(r1)
            goto L_0x03d0
        L_0x03e4:
            if (r6 != r10) goto L_0x0403
            com.google.android.gms.internal.measurement.x3 r11 = (com.google.android.gms.internal.measurement.C2783x3) r11
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r4, r7)
            int r2 = r7.f3971a
            int r2 = r2 + r1
        L_0x03ef:
            if (r1 >= r2) goto L_0x03fb
            double r4 = com.google.android.gms.internal.measurement.C2433b3.m6059c(r3, r1)
            r11.mo18133a(r4)
            int r1 = r1 + 8
            goto L_0x03ef
        L_0x03fb:
            if (r1 != r2) goto L_0x03fe
            goto L_0x0423
        L_0x03fe:
            com.google.android.gms.internal.measurement.t4 r1 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r1
        L_0x0403:
            if (r6 != r13) goto L_0x0422
            com.google.android.gms.internal.measurement.x3 r11 = (com.google.android.gms.internal.measurement.C2783x3) r11
            double r8 = com.google.android.gms.internal.measurement.C2433b3.m6059c(r18, r19)
            r11.mo18133a(r8)
        L_0x040e:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0423
            int r4 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r3, r1, r7)
            int r6 = r7.f3971a
            if (r2 != r6) goto L_0x0423
            double r8 = com.google.android.gms.internal.measurement.C2433b3.m6059c(r3, r4)
            r11.mo18133a(r8)
            goto L_0x040e
        L_0x0422:
            r1 = r4
        L_0x0423:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2801y5.m7835a(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, com.google.android.gms.internal.measurement.a3):int");
    }

    /* renamed from: a */
    private final <K, V> int m7836a(T t, byte[] bArr, int i, int i2, int i3, long j, C2417a3 a3Var) {
        Unsafe unsafe = f4597r;
        Object b = m7853b(i3);
        Object object = unsafe.getObject(t, j);
        if (this.f4613p.mo17830b(object)) {
            Object e = this.f4613p.mo17833e(b);
            this.f4613p.mo17828a(e, object);
            unsafe.putObject(t, j, e);
        }
        this.f4613p.mo17832d(b);
        throw null;
    }

    /* renamed from: a */
    private final int m7834a(T t, byte[] bArr, int i, int i2, int i3, int i4, int i5, int i6, int i7, long j, int i8, C2417a3 a3Var) {
        int i9;
        T t2 = t;
        byte[] bArr2 = bArr;
        int i10 = i;
        int i11 = i3;
        int i12 = i4;
        int i13 = i5;
        long j2 = j;
        int i14 = i8;
        C2417a3 a3Var2 = a3Var;
        Unsafe unsafe = f4597r;
        long j3 = (long) (this.f4598a[i14 + 2] & 1048575);
        switch (i7) {
            case 51:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Double.valueOf(C2433b3.m6059c(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 52:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Float.valueOf(C2433b3.m6061d(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 53:
            case 54:
                if (i13 == 0) {
                    i9 = C2433b3.m6057b(bArr2, i10, a3Var2);
                    unsafe.putObject(t2, j2, Long.valueOf(a3Var2.f3972b));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 55:
            case 62:
                if (i13 == 0) {
                    i9 = C2433b3.m6055a(bArr2, i10, a3Var2);
                    unsafe.putObject(t2, j2, Integer.valueOf(a3Var2.f3971a));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 56:
            case 65:
                if (i13 == 1) {
                    unsafe.putObject(t2, j2, Long.valueOf(C2433b3.m6058b(bArr, i)));
                    i9 = i10 + 8;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 57:
            case 64:
                if (i13 == 5) {
                    unsafe.putObject(t2, j2, Integer.valueOf(C2433b3.m6054a(bArr, i)));
                    i9 = i10 + 4;
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 58:
                if (i13 == 0) {
                    i9 = C2433b3.m6057b(bArr2, i10, a3Var2);
                    unsafe.putObject(t2, j2, Boolean.valueOf(a3Var2.f3972b != 0));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 59:
                if (i13 == 2) {
                    int a = C2433b3.m6055a(bArr2, i10, a3Var2);
                    int i15 = a3Var2.f3971a;
                    if (i15 == 0) {
                        unsafe.putObject(t2, j2, "");
                    } else if ((i6 & C1750C.ENCODING_PCM_A_LAW) == 0 || C2604l7.m6696a(bArr2, a, a + i15)) {
                        unsafe.putObject(t2, j2, new String(bArr2, a, i15, C2647o4.f4367a));
                        a += i15;
                    } else {
                        throw C2723t4.m7316i();
                    }
                    unsafe.putInt(t2, j3, i12);
                    return a;
                }
                return i10;
            case 60:
                if (i13 == 2) {
                    int a2 = C2433b3.m6053a(m7837a(i14), bArr2, i10, i2, a3Var2);
                    Object object = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object == null) {
                        unsafe.putObject(t2, j2, a3Var2.f3973c);
                    } else {
                        unsafe.putObject(t2, j2, C2647o4.m6962a(object, a3Var2.f3973c));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return a2;
                }
                return i10;
            case 61:
                if (i13 == 2) {
                    i9 = C2433b3.m6063e(bArr2, i10, a3Var2);
                    unsafe.putObject(t2, j2, a3Var2.f3973c);
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 63:
                if (i13 == 0) {
                    int a3 = C2433b3.m6055a(bArr2, i10, a3Var2);
                    int i16 = a3Var2.f3971a;
                    C2661p4 c = m7859c(i14);
                    if (c == null || c.mo17363a(i16)) {
                        unsafe.putObject(t2, j2, Integer.valueOf(i16));
                        i9 = a3;
                        unsafe.putInt(t2, j3, i12);
                        return i9;
                    }
                    m7865e(t).mo17375a(i11, Long.valueOf((long) i16));
                    return a3;
                }
                return i10;
            case 66:
                if (i13 == 0) {
                    i9 = C2433b3.m6055a(bArr2, i10, a3Var2);
                    unsafe.putObject(t2, j2, Integer.valueOf(C2690r3.m7119a(a3Var2.f3971a)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 67:
                if (i13 == 0) {
                    i9 = C2433b3.m6057b(bArr2, i10, a3Var2);
                    unsafe.putObject(t2, j2, Long.valueOf(C2690r3.m7120a(a3Var2.f3972b)));
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            case 68:
                if (i13 == 3) {
                    i9 = C2433b3.m6052a(m7837a(i14), bArr, i, i2, (i11 & -8) | 4, a3Var);
                    Object object2 = unsafe.getInt(t2, j3) == i12 ? unsafe.getObject(t2, j2) : null;
                    if (object2 == null) {
                        unsafe.putObject(t2, j2, a3Var2.f3973c);
                    } else {
                        unsafe.putObject(t2, j2, C2647o4.m6962a(object2, a3Var2.f3973c));
                    }
                    unsafe.putInt(t2, j3, i12);
                    return i9;
                }
                return i10;
            default:
                return i10;
        }
    }

    /* renamed from: a */
    private final C2603l6 m7837a(int i) {
        int i2 = (i / 3) << 1;
        C2603l6 l6Var = (C2603l6) this.f4599b[i2];
        if (l6Var != null) {
            return l6Var;
        }
        C2603l6 a = C2550i6.m6481a().mo17581a((Class) this.f4599b[i2 + 1]);
        this.f4599b[i2] = a;
        return a;
    }

    /* JADX WARN: Type inference failed for: r35v0, types: [int] */
    /* JADX WARN: Type inference failed for: r3v27, types: [int] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x03e4 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x03f9  */
    /* JADX WARNING: Removed duplicated region for block: B:142:0x043b  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int mo18168a(java.lang.Object r31, byte[] r32, int r33, int r34, int r35, com.google.android.gms.internal.measurement.C2417a3 r36) {
        /*
            r30 = this;
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            r9 = r36
            sun.misc.Unsafe r10 = com.google.android.gms.internal.measurement.C2801y5.f4597r
            r16 = 0
            r0 = r33
            r1 = -1
            r2 = 0
            r3 = 0
            r6 = 0
            r7 = -1
        L_0x0017:
            r17 = 0
            if (r0 >= r13) goto L_0x047d
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x002a
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6050a(r0, r12, r3, r9)
            int r3 = r9.f3971a
            r4 = r0
            r5 = r3
            goto L_0x002c
        L_0x002a:
            r5 = r0
            r4 = r3
        L_0x002c:
            int r3 = r5 >>> 3
            r0 = r5 & 7
            r8 = 3
            if (r3 <= r1) goto L_0x0039
            int r2 = r2 / r8
            int r1 = r15.m7832a(r3, r2)
            goto L_0x003d
        L_0x0039:
            int r1 = r15.m7866f(r3)
        L_0x003d:
            r2 = r1
            r1 = -1
            if (r2 != r1) goto L_0x0050
            r25 = r3
            r2 = r4
            r20 = r6
            r18 = r7
            r27 = r10
            r6 = r11
            r19 = 0
            r7 = r5
            goto L_0x03e2
        L_0x0050:
            int[] r1 = r15.f4598a
            int r19 = r2 + 1
            r8 = r1[r19]
            r19 = 267386880(0xff00000, float:2.3665827E-29)
            r19 = r8 & r19
            int r11 = r19 >>> 20
            r19 = 1048575(0xfffff, float:1.469367E-39)
            r20 = r5
            r5 = r8 & r19
            long r12 = (long) r5
            r5 = 17
            r21 = r8
            if (r11 > r5) goto L_0x02dc
            int r5 = r2 + 2
            r1 = r1[r5]
            int r5 = r1 >>> 20
            r8 = 1
            int r23 = r8 << r5
            r1 = r1 & r19
            r5 = -1
            if (r1 == r7) goto L_0x0084
            if (r7 == r5) goto L_0x007e
            long r8 = (long) r7
            r10.putInt(r14, r8, r6)
        L_0x007e:
            long r6 = (long) r1
            int r6 = r10.getInt(r14, r6)
            r7 = r1
        L_0x0084:
            r1 = 5
            switch(r11) {
                case 0: goto L_0x02a4;
                case 1: goto L_0x028a;
                case 2: goto L_0x0264;
                case 3: goto L_0x0264;
                case 4: goto L_0x0249;
                case 5: goto L_0x0224;
                case 6: goto L_0x0201;
                case 7: goto L_0x01d9;
                case 8: goto L_0x01b4;
                case 9: goto L_0x017e;
                case 10: goto L_0x0163;
                case 11: goto L_0x0249;
                case 12: goto L_0x0131;
                case 13: goto L_0x0201;
                case 14: goto L_0x0224;
                case 15: goto L_0x0116;
                case 16: goto L_0x00e9;
                case 17: goto L_0x0097;
                default: goto L_0x0088;
            }
        L_0x0088:
            r12 = r32
            r13 = r36
            r9 = r2
            r11 = r3
            r33 = r7
            r8 = r20
            r19 = -1
        L_0x0094:
            r7 = r4
            goto L_0x02cc
        L_0x0097:
            r8 = 3
            if (r0 != r8) goto L_0x00dd
            int r0 = r3 << 3
            r8 = r0 | 4
            com.google.android.gms.internal.measurement.l6 r0 = r15.m7837a(r2)
            r1 = r32
            r9 = r2
            r2 = r4
            r11 = r3
            r3 = r34
            r4 = r8
            r8 = r20
            r19 = -1
            r5 = r36
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6052a(r0, r1, r2, r3, r4, r5)
            r1 = r6 & r23
            if (r1 != 0) goto L_0x00c0
            r5 = r36
            java.lang.Object r1 = r5.f3973c
            r10.putObject(r14, r12, r1)
            goto L_0x00cf
        L_0x00c0:
            r5 = r36
            java.lang.Object r1 = r10.getObject(r14, r12)
            java.lang.Object r2 = r5.f3973c
            java.lang.Object r1 = com.google.android.gms.internal.measurement.C2647o4.m6962a(r1, r2)
            r10.putObject(r14, r12, r1)
        L_0x00cf:
            r6 = r6 | r23
            r12 = r32
            r13 = r34
            r3 = r8
            r2 = r9
            r1 = r11
            r11 = r35
            r9 = r5
            goto L_0x0017
        L_0x00dd:
            r9 = r2
            r11 = r3
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            goto L_0x0245
        L_0x00e9:
            r5 = r36
            r9 = r2
            r11 = r3
            r8 = r20
            r19 = -1
            if (r0 != 0) goto L_0x0111
            r2 = r12
            r12 = r32
            int r13 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r12, r4, r5)
            long r0 = r5.f3972b
            long r17 = com.google.android.gms.internal.measurement.C2690r3.m7120a(r0)
            r0 = r10
            r1 = r31
            r33 = r13
            r13 = r5
            r4 = r17
            r0.putLong(r1, r2, r4)
            r6 = r6 | r23
            r0 = r33
            goto L_0x02c2
        L_0x0111:
            r12 = r32
            r13 = r5
            goto L_0x0245
        L_0x0116:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != 0) goto L_0x0245
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r12, r4, r13)
            int r1 = r13.f3971a
            int r1 = com.google.android.gms.internal.measurement.C2690r3.m7119a(r1)
            r10.putInt(r14, r2, r1)
            goto L_0x017a
        L_0x0131:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != 0) goto L_0x0245
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r12, r4, r13)
            int r1 = r13.f3971a
            com.google.android.gms.internal.measurement.p4 r4 = r15.m7859c(r9)
            if (r4 == 0) goto L_0x015f
            boolean r4 = r4.mo17363a(r1)
            if (r4 == 0) goto L_0x0151
            goto L_0x015f
        L_0x0151:
            com.google.android.gms.internal.measurement.c7 r2 = m7865e(r31)
            long r3 = (long) r1
            java.lang.Long r1 = java.lang.Long.valueOf(r3)
            r2.mo17375a(r8, r1)
            goto L_0x02c2
        L_0x015f:
            r10.putInt(r14, r2, r1)
            goto L_0x017a
        L_0x0163:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r1 = 2
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != r1) goto L_0x0245
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6063e(r12, r4, r13)
            java.lang.Object r1 = r13.f3973c
            r10.putObject(r14, r2, r1)
        L_0x017a:
            r6 = r6 | r23
            goto L_0x02c2
        L_0x017e:
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r1 = 2
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != r1) goto L_0x01b0
            com.google.android.gms.internal.measurement.l6 r0 = r15.m7837a(r9)
            r5 = r34
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6053a(r0, r12, r4, r5, r13)
            r1 = r6 & r23
            if (r1 != 0) goto L_0x01a1
            java.lang.Object r1 = r13.f3973c
            r10.putObject(r14, r2, r1)
            goto L_0x0219
        L_0x01a1:
            java.lang.Object r1 = r10.getObject(r14, r2)
            java.lang.Object r4 = r13.f3973c
            java.lang.Object r1 = com.google.android.gms.internal.measurement.C2647o4.m6962a(r1, r4)
            r10.putObject(r14, r2, r1)
            goto L_0x0219
        L_0x01b0:
            r5 = r34
            goto L_0x0245
        L_0x01b4:
            r5 = r34
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r1 = 2
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != r1) goto L_0x0245
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r21 & r0
            if (r0 != 0) goto L_0x01cf
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6060c(r12, r4, r13)
            goto L_0x01d3
        L_0x01cf:
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6062d(r12, r4, r13)
        L_0x01d3:
            java.lang.Object r1 = r13.f3973c
            r10.putObject(r14, r2, r1)
            goto L_0x0219
        L_0x01d9:
            r5 = r34
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != 0) goto L_0x0245
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r12, r4, r13)
            r33 = r0
            long r0 = r13.f3972b
            r20 = 0
            int r4 = (r0 > r20 ? 1 : (r0 == r20 ? 0 : -1))
            if (r4 == 0) goto L_0x01f8
            r0 = 1
            goto L_0x01f9
        L_0x01f8:
            r0 = 0
        L_0x01f9:
            com.google.android.gms.internal.measurement.C2566j7.m6523a(r14, r2, r0)
            r6 = r6 | r23
            r0 = r33
            goto L_0x021b
        L_0x0201:
            r5 = r34
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != r1) goto L_0x0245
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6054a(r12, r4)
            r10.putInt(r14, r2, r0)
            int r0 = r4 + 4
        L_0x0219:
            r6 = r6 | r23
        L_0x021b:
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
            r11 = r35
            r13 = r5
            goto L_0x0017
        L_0x0224:
            r5 = r34
            r9 = r2
            r11 = r3
            r2 = r12
            r8 = r20
            r1 = 1
            r19 = -1
            r12 = r32
            r13 = r36
            if (r0 != r1) goto L_0x0245
            long r17 = com.google.android.gms.internal.measurement.C2433b3.m6058b(r12, r4)
            r0 = r10
            r1 = r31
            r33 = r7
            r7 = r4
            r4 = r17
            r0.putLong(r1, r2, r4)
            goto L_0x02bc
        L_0x0245:
            r33 = r7
            goto L_0x0094
        L_0x0249:
            r9 = r2
            r11 = r3
            r33 = r7
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            r7 = r4
            if (r0 != 0) goto L_0x02cc
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r12, r7, r13)
            int r1 = r13.f3971a
            r10.putInt(r14, r2, r1)
            goto L_0x02be
        L_0x0264:
            r9 = r2
            r11 = r3
            r33 = r7
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            r7 = r4
            if (r0 != 0) goto L_0x02cc
            int r7 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r12, r7, r13)
            long r4 = r13.f3972b
            r0 = r10
            r1 = r31
            r0.putLong(r1, r2, r4)
            r6 = r6 | r23
            r0 = r7
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
            r7 = r33
            goto L_0x02c6
        L_0x028a:
            r9 = r2
            r11 = r3
            r33 = r7
            r2 = r12
            r8 = r20
            r19 = -1
            r12 = r32
            r13 = r36
            r7 = r4
            if (r0 != r1) goto L_0x02cc
            float r0 = com.google.android.gms.internal.measurement.C2433b3.m6061d(r12, r7)
            com.google.android.gms.internal.measurement.C2566j7.m6519a(r14, r2, r0)
            int r0 = r7 + 4
            goto L_0x02be
        L_0x02a4:
            r9 = r2
            r11 = r3
            r33 = r7
            r2 = r12
            r8 = r20
            r1 = 1
            r19 = -1
            r12 = r32
            r13 = r36
            r7 = r4
            if (r0 != r1) goto L_0x02cc
            double r0 = com.google.android.gms.internal.measurement.C2433b3.m6059c(r12, r7)
            com.google.android.gms.internal.measurement.C2566j7.m6518a(r14, r2, r0)
        L_0x02bc:
            int r0 = r7 + 8
        L_0x02be:
            r6 = r6 | r23
            r7 = r33
        L_0x02c2:
            r3 = r8
            r2 = r9
            r1 = r11
            r9 = r13
        L_0x02c6:
            r13 = r34
            r11 = r35
            goto L_0x0017
        L_0x02cc:
            r18 = r33
            r20 = r6
            r2 = r7
            r7 = r8
            r19 = r9
            r27 = r10
            r25 = r11
            r6 = r35
            goto L_0x03e2
        L_0x02dc:
            r5 = r3
            r18 = r7
            r8 = r20
            r19 = -1
            r7 = r4
            r28 = r12
            r12 = r32
            r13 = r9
            r9 = r2
            r2 = r28
            r1 = 27
            if (r11 != r1) goto L_0x0341
            r1 = 2
            if (r0 != r1) goto L_0x0334
            java.lang.Object r0 = r10.getObject(r14, r2)
            com.google.android.gms.internal.measurement.u4 r0 = (com.google.android.gms.internal.measurement.C2738u4) r0
            boolean r1 = r0.mo17938a()
            if (r1 != 0) goto L_0x0311
            int r1 = r0.size()
            if (r1 != 0) goto L_0x0308
            r1 = 10
            goto L_0x030a
        L_0x0308:
            int r1 = r1 << 1
        L_0x030a:
            com.google.android.gms.internal.measurement.u4 r0 = r0.mo17320a(r1)
            r10.putObject(r14, r2, r0)
        L_0x0311:
            r11 = r0
            com.google.android.gms.internal.measurement.l6 r0 = r15.m7837a(r9)
            r1 = r8
            r2 = r32
            r3 = r7
            r4 = r34
            r7 = r5
            r5 = r11
            r20 = r6
            r6 = r36
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6051a(r0, r1, r2, r3, r4, r5, r6)
            r11 = r35
            r1 = r7
            r3 = r8
            r2 = r9
            r9 = r13
            r7 = r18
            r6 = r20
            r13 = r34
            goto L_0x0017
        L_0x0334:
            r20 = r6
            r25 = r5
            r15 = r7
            r26 = r8
            r19 = r9
            r27 = r10
            goto L_0x03a8
        L_0x0341:
            r20 = r6
            r6 = r5
            r1 = 49
            if (r11 > r1) goto L_0x0390
            r5 = r21
            long r4 = (long) r5
            r1 = r0
            r0 = r30
            r33 = r1
            r1 = r31
            r23 = r2
            r2 = r32
            r3 = r7
            r21 = r4
            r4 = r34
            r5 = r8
            r25 = r6
            r15 = r7
            r7 = r33
            r26 = r8
            r8 = r9
            r19 = r9
            r27 = r10
            r9 = r21
            r12 = r23
            r14 = r36
            int r0 = r0.m7835a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0376
            goto L_0x03de
        L_0x0376:
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            r9 = r36
            r7 = r18
            r2 = r19
            r6 = r20
            r1 = r25
            r3 = r26
        L_0x038c:
            r10 = r27
            goto L_0x0017
        L_0x0390:
            r33 = r0
            r23 = r2
            r25 = r6
            r15 = r7
            r26 = r8
            r19 = r9
            r27 = r10
            r5 = r21
            r0 = 50
            if (r11 != r0) goto L_0x03c1
            r7 = r33
            r0 = 2
            if (r7 == r0) goto L_0x03ae
        L_0x03a8:
            r6 = r35
            r2 = r15
        L_0x03ab:
            r7 = r26
            goto L_0x03e2
        L_0x03ae:
            r0 = r30
            r1 = r31
            r2 = r32
            r3 = r15
            r4 = r34
            r5 = r19
            r6 = r23
            r8 = r36
            r0.m7836a(r1, r2, r3, r4, r5, r6, r8)
            throw r17
        L_0x03c1:
            r7 = r33
            r0 = r30
            r1 = r31
            r2 = r32
            r3 = r15
            r4 = r34
            r8 = r5
            r5 = r26
            r6 = r25
            r9 = r11
            r10 = r23
            r12 = r19
            r13 = r36
            int r0 = r0.m7834a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0463
        L_0x03de:
            r6 = r35
            r2 = r0
            goto L_0x03ab
        L_0x03e2:
            if (r7 != r6) goto L_0x03f3
            if (r6 != 0) goto L_0x03e7
            goto L_0x03f3
        L_0x03e7:
            r4 = -1
            r8 = r30
            r11 = r31
            r3 = r7
            r0 = r18
            r1 = r20
            goto L_0x048c
        L_0x03f3:
            r8 = r30
            boolean r0 = r8.f4603f
            if (r0 == 0) goto L_0x043b
            r9 = r36
            com.google.android.gms.internal.measurement.y3 r0 = r9.f3974d
            com.google.android.gms.internal.measurement.y3 r1 = com.google.android.gms.internal.measurement.C2798y3.m7828a()
            if (r0 == r1) goto L_0x0438
            com.google.android.gms.internal.measurement.u5 r0 = r8.f4602e
            com.google.android.gms.internal.measurement.y3 r1 = r9.f3974d
            r10 = r25
            com.google.android.gms.internal.measurement.l4$d r0 = r1.mo18164a(r0, r10)
            if (r0 != 0) goto L_0x0428
            com.google.android.gms.internal.measurement.c7 r4 = m7865e(r31)
            r0 = r7
            r1 = r32
            r3 = r34
            r5 = r36
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6048a(r0, r1, r2, r3, r4, r5)
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r6
            r3 = r7
            r15 = r8
            goto L_0x0474
        L_0x0428:
            r11 = r31
            r0 = r11
            com.google.android.gms.internal.measurement.l4$b r0 = (com.google.android.gms.internal.measurement.C2595l4.C2597b) r0
            r0.mo17681n()
            com.google.android.gms.internal.measurement.b4<com.google.android.gms.internal.measurement.l4$e> r0 = r0.zzc
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x0438:
            r11 = r31
            goto L_0x043f
        L_0x043b:
            r11 = r31
            r9 = r36
        L_0x043f:
            r10 = r25
            com.google.android.gms.internal.measurement.c7 r4 = m7865e(r31)
            r0 = r7
            r1 = r32
            r3 = r34
            r5 = r36
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6048a(r0, r1, r2, r3, r4, r5)
            r12 = r32
            r13 = r34
            r3 = r7
            r15 = r8
            r1 = r10
            r14 = r11
            r7 = r18
            r2 = r19
            r10 = r27
            r11 = r6
            r6 = r20
            goto L_0x0017
        L_0x0463:
            r10 = r25
            r7 = r26
            r15 = r30
            r14 = r31
            r12 = r32
            r13 = r34
            r11 = r35
            r9 = r36
            r3 = r7
        L_0x0474:
            r1 = r10
            r7 = r18
            r2 = r19
            r6 = r20
            goto L_0x038c
        L_0x047d:
            r20 = r6
            r18 = r7
            r27 = r10
            r6 = r11
            r11 = r14
            r8 = r15
            r2 = r0
            r0 = r18
            r1 = r20
            r4 = -1
        L_0x048c:
            if (r0 == r4) goto L_0x0494
            long r4 = (long) r0
            r0 = r27
            r0.putInt(r11, r4, r1)
        L_0x0494:
            int r0 = r8.f4607j
            r1 = r17
        L_0x0498:
            int r4 = r8.f4608k
            if (r0 >= r4) goto L_0x04aa
            int[] r4 = r8.f4606i
            r4 = r4[r0]
            com.google.android.gms.internal.measurement.d7<?, ?> r5 = r8.f4611n
            r8.m7840a(r11, r4, r1, r5)
            com.google.android.gms.internal.measurement.c7 r1 = (com.google.android.gms.internal.measurement.C2453c7) r1
            int r0 = r0 + 1
            goto L_0x0498
        L_0x04aa:
            if (r1 == 0) goto L_0x04b1
            com.google.android.gms.internal.measurement.d7<?, ?> r0 = r8.f4611n
            r0.mo17425b(r11, r1)
        L_0x04b1:
            if (r6 != 0) goto L_0x04bd
            r0 = r34
            if (r2 != r0) goto L_0x04b8
            goto L_0x04c3
        L_0x04b8:
            com.google.android.gms.internal.measurement.t4 r0 = com.google.android.gms.internal.measurement.C2723t4.m7315h()
            throw r0
        L_0x04bd:
            r0 = r34
            if (r2 > r0) goto L_0x04c4
            if (r3 != r6) goto L_0x04c4
        L_0x04c3:
            return r2
        L_0x04c4:
            com.google.android.gms.internal.measurement.t4 r0 = com.google.android.gms.internal.measurement.C2723t4.m7315h()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2801y5.mo18168a(java.lang.Object, byte[], int, int, int, com.google.android.gms.internal.measurement.a3):int");
    }

    /* JADX WARN: Type inference failed for: r3v13, types: [int] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, java.lang.Object):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.a(byte[], long, byte):void
      com.google.android.gms.internal.measurement.j7.a(java.lang.Object, long, float):void */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x01e1, code lost:
        if (r0 == r15) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x022b, code lost:
        if (r0 == r15) goto L_0x022d;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo17279a(T r28, byte[] r29, int r30, int r31, com.google.android.gms.internal.measurement.C2417a3 r32) {
        /*
            r27 = this;
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            boolean r0 = r15.f4604g
            if (r0 == 0) goto L_0x025a
            sun.misc.Unsafe r9 = com.google.android.gms.internal.measurement.C2801y5.f4597r
            r10 = -1
            r16 = 0
            r0 = r30
            r1 = -1
            r2 = 0
        L_0x0017:
            if (r0 >= r13) goto L_0x0251
            int r3 = r0 + 1
            byte r0 = r12[r0]
            if (r0 >= 0) goto L_0x0029
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6050a(r0, r12, r3, r11)
            int r3 = r11.f3971a
            r8 = r0
            r17 = r3
            goto L_0x002c
        L_0x0029:
            r17 = r0
            r8 = r3
        L_0x002c:
            int r7 = r17 >>> 3
            r6 = r17 & 7
            if (r7 <= r1) goto L_0x0039
            int r2 = r2 / 3
            int r0 = r15.m7832a(r7, r2)
            goto L_0x003d
        L_0x0039:
            int r0 = r15.m7866f(r7)
        L_0x003d:
            r4 = r0
            if (r4 != r10) goto L_0x004b
            r24 = r7
            r2 = r8
            r18 = r9
            r19 = 0
            r26 = -1
            goto L_0x022e
        L_0x004b:
            int[] r0 = r15.f4598a
            int r1 = r4 + 1
            r5 = r0[r1]
            r0 = 267386880(0xff00000, float:2.3665827E-29)
            r0 = r0 & r5
            int r3 = r0 >>> 20
            r0 = 1048575(0xfffff, float:1.469367E-39)
            r0 = r0 & r5
            long r1 = (long) r0
            r0 = 17
            r10 = 2
            if (r3 > r0) goto L_0x0167
            r0 = 1
            switch(r3) {
                case 0: goto L_0x014e;
                case 1: goto L_0x013f;
                case 2: goto L_0x012d;
                case 3: goto L_0x012d;
                case 4: goto L_0x011f;
                case 5: goto L_0x010f;
                case 6: goto L_0x00fe;
                case 7: goto L_0x00e8;
                case 8: goto L_0x00d1;
                case 9: goto L_0x00b0;
                case 10: goto L_0x00a3;
                case 11: goto L_0x011f;
                case 12: goto L_0x0094;
                case 13: goto L_0x00fe;
                case 14: goto L_0x010f;
                case 15: goto L_0x0081;
                case 16: goto L_0x0066;
                default: goto L_0x0064;
            }
        L_0x0064:
            goto L_0x01a4
        L_0x0066:
            if (r6 != 0) goto L_0x01a4
            int r6 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r12, r8, r11)
            r19 = r1
            long r0 = r11.f3972b
            long r21 = com.google.android.gms.internal.measurement.C2690r3.m7120a(r0)
            r0 = r9
            r2 = r19
            r1 = r28
            r10 = r4
            r4 = r21
            r0.putLong(r1, r2, r4)
            goto L_0x013d
        L_0x0081:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r12, r8, r11)
            int r1 = r11.f3971a
            int r1 = com.google.android.gms.internal.measurement.C2690r3.m7119a(r1)
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x0094:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r12, r8, r11)
            int r1 = r11.f3971a
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x00a3:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6063e(r12, r8, r11)
            java.lang.Object r1 = r11.f3973c
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00b0:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            com.google.android.gms.internal.measurement.l6 r0 = r15.m7837a(r4)
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6053a(r0, r12, r8, r13, r11)
            java.lang.Object r1 = r9.getObject(r14, r2)
            if (r1 != 0) goto L_0x00c7
            java.lang.Object r1 = r11.f3973c
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00c7:
            java.lang.Object r5 = r11.f3973c
            java.lang.Object r1 = com.google.android.gms.internal.measurement.C2647o4.m6962a(r1, r5)
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00d1:
            r2 = r1
            if (r6 != r10) goto L_0x01a4
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r0 & r5
            if (r0 != 0) goto L_0x00de
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6060c(r12, r8, r11)
            goto L_0x00e2
        L_0x00de:
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6062d(r12, r8, r11)
        L_0x00e2:
            java.lang.Object r1 = r11.f3973c
            r9.putObject(r14, r2, r1)
            goto L_0x010b
        L_0x00e8:
            r2 = r1
            if (r6 != 0) goto L_0x01a4
            int r1 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r12, r8, r11)
            long r5 = r11.f3972b
            r19 = 0
            int r8 = (r5 > r19 ? 1 : (r5 == r19 ? 0 : -1))
            if (r8 == 0) goto L_0x00f8
            goto L_0x00f9
        L_0x00f8:
            r0 = 0
        L_0x00f9:
            com.google.android.gms.internal.measurement.C2566j7.m6523a(r14, r2, r0)
            r0 = r1
            goto L_0x010b
        L_0x00fe:
            r2 = r1
            r0 = 5
            if (r6 != r0) goto L_0x01a4
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6054a(r12, r8)
            r9.putInt(r14, r2, r0)
            int r0 = r8 + 4
        L_0x010b:
            r2 = r4
            r1 = r7
            goto L_0x024e
        L_0x010f:
            r2 = r1
            if (r6 != r0) goto L_0x01a4
            long r5 = com.google.android.gms.internal.measurement.C2433b3.m6058b(r12, r8)
            r0 = r9
            r1 = r28
            r10 = r4
            r4 = r5
            r0.putLong(r1, r2, r4)
            goto L_0x0159
        L_0x011f:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6055a(r12, r8, r11)
            int r1 = r11.f3971a
            r9.putInt(r14, r2, r1)
            goto L_0x015b
        L_0x012d:
            r2 = r1
            r10 = r4
            if (r6 != 0) goto L_0x015f
            int r6 = com.google.android.gms.internal.measurement.C2433b3.m6057b(r12, r8, r11)
            long r4 = r11.f3972b
            r0 = r9
            r1 = r28
            r0.putLong(r1, r2, r4)
        L_0x013d:
            r0 = r6
            goto L_0x015b
        L_0x013f:
            r2 = r1
            r10 = r4
            r0 = 5
            if (r6 != r0) goto L_0x015f
            float r0 = com.google.android.gms.internal.measurement.C2433b3.m6061d(r12, r8)
            com.google.android.gms.internal.measurement.C2566j7.m6519a(r14, r2, r0)
            int r0 = r8 + 4
            goto L_0x015b
        L_0x014e:
            r2 = r1
            r10 = r4
            if (r6 != r0) goto L_0x015f
            double r0 = com.google.android.gms.internal.measurement.C2433b3.m6059c(r12, r8)
            com.google.android.gms.internal.measurement.C2566j7.m6518a(r14, r2, r0)
        L_0x0159:
            int r0 = r8 + 8
        L_0x015b:
            r1 = r7
            r2 = r10
            goto L_0x024e
        L_0x015f:
            r24 = r7
            r15 = r8
            r18 = r9
            r19 = r10
            goto L_0x01ab
        L_0x0167:
            r0 = 27
            if (r3 != r0) goto L_0x01ae
            if (r6 != r10) goto L_0x01a4
            java.lang.Object r0 = r9.getObject(r14, r1)
            com.google.android.gms.internal.measurement.u4 r0 = (com.google.android.gms.internal.measurement.C2738u4) r0
            boolean r3 = r0.mo17938a()
            if (r3 != 0) goto L_0x018b
            int r3 = r0.size()
            if (r3 != 0) goto L_0x0182
            r3 = 10
            goto L_0x0184
        L_0x0182:
            int r3 = r3 << 1
        L_0x0184:
            com.google.android.gms.internal.measurement.u4 r0 = r0.mo17320a(r3)
            r9.putObject(r14, r1, r0)
        L_0x018b:
            r5 = r0
            com.google.android.gms.internal.measurement.l6 r0 = r15.m7837a(r4)
            r1 = r17
            r2 = r29
            r3 = r8
            r19 = r4
            r4 = r31
            r6 = r32
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6051a(r0, r1, r2, r3, r4, r5, r6)
            r1 = r7
            r2 = r19
            goto L_0x024e
        L_0x01a4:
            r19 = r4
            r24 = r7
            r15 = r8
            r18 = r9
        L_0x01ab:
            r26 = -1
            goto L_0x01fb
        L_0x01ae:
            r19 = r4
            r0 = 49
            if (r3 > r0) goto L_0x01e4
            long r4 = (long) r5
            r0 = r27
            r20 = r1
            r1 = r28
            r2 = r29
            r10 = r3
            r3 = r8
            r22 = r4
            r4 = r31
            r5 = r17
            r30 = r6
            r6 = r7
            r24 = r7
            r7 = r30
            r15 = r8
            r8 = r19
            r18 = r9
            r25 = r10
            r26 = -1
            r9 = r22
            r11 = r25
            r12 = r20
            r14 = r32
            int r0 = r0.m7835a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x023e
            goto L_0x022d
        L_0x01e4:
            r20 = r1
            r25 = r3
            r30 = r6
            r24 = r7
            r15 = r8
            r18 = r9
            r26 = -1
            r0 = 50
            r9 = r25
            if (r9 != r0) goto L_0x0211
            r7 = r30
            if (r7 == r10) goto L_0x01fd
        L_0x01fb:
            r2 = r15
            goto L_0x022e
        L_0x01fd:
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r5 = r19
            r6 = r20
            r8 = r32
            r0.m7836a(r1, r2, r3, r4, r5, r6, r8)
            r0 = 0
            throw r0
        L_0x0211:
            r7 = r30
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r15
            r4 = r31
            r8 = r5
            r5 = r17
            r6 = r24
            r10 = r20
            r12 = r19
            r13 = r32
            int r0 = r0.m7834a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x023e
        L_0x022d:
            r2 = r0
        L_0x022e:
            com.google.android.gms.internal.measurement.c7 r4 = m7865e(r28)
            r0 = r17
            r1 = r29
            r3 = r31
            r5 = r32
            int r0 = com.google.android.gms.internal.measurement.C2433b3.m6048a(r0, r1, r2, r3, r4, r5)
        L_0x023e:
            r15 = r27
            r14 = r28
            r12 = r29
            r13 = r31
            r11 = r32
            r9 = r18
            r2 = r19
            r1 = r24
        L_0x024e:
            r10 = -1
            goto L_0x0017
        L_0x0251:
            r4 = r13
            if (r0 != r4) goto L_0x0255
            return
        L_0x0255:
            com.google.android.gms.internal.measurement.t4 r0 = com.google.android.gms.internal.measurement.C2723t4.m7315h()
            throw r0
        L_0x025a:
            r4 = r13
            r5 = 0
            r0 = r27
            r1 = r28
            r2 = r29
            r3 = r30
            r4 = r31
            r6 = r32
            r0.mo18168a(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2801y5.mo17279a(java.lang.Object, byte[], int, int, com.google.android.gms.internal.measurement.a3):void");
    }

    /* renamed from: a */
    private final <UT, UB> UB m7840a(Object obj, int i, UB ub, C2469d7<UT, UB> d7Var) {
        C2661p4 c;
        int i2 = this.f4598a[i];
        Object f = C2566j7.m6544f(obj, (long) (m7861d(i) & 1048575));
        if (f == null || (c = m7859c(i)) == null) {
            return ub;
        }
        m7839a(i, i2, this.f4613p.mo17829a(f), c, ub, d7Var);
        throw null;
    }

    /* renamed from: a */
    private final <K, V, UT, UB> UB m7839a(int i, int i2, Map map, C2661p4 p4Var, Object obj, C2469d7 d7Var) {
        this.f4613p.mo17832d(m7853b(i));
        throw null;
    }

    /* renamed from: a */
    private static boolean m7850a(Object obj, int i, C2603l6 l6Var) {
        return l6Var.mo17282b(C2566j7.m6544f(obj, (long) (i & 1048575)));
    }

    /* renamed from: a */
    private static void m7843a(int i, Object obj, C2771w7 w7Var) {
        if (obj instanceof String) {
            w7Var.mo17963a(i, (String) obj);
        } else {
            w7Var.mo17960a(i, (C2498f3) obj);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      com.google.android.gms.internal.measurement.y5.a(int, int):int
      com.google.android.gms.internal.measurement.y5.a(com.google.android.gms.internal.measurement.d7, java.lang.Object):int
      com.google.android.gms.internal.measurement.y5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, long):java.util.List<?>
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.y5.a(java.lang.Object, int):boolean */
    /* renamed from: a */
    private final boolean m7849a(T t, int i, int i2, int i3) {
        if (this.f4604g) {
            return m7847a((Object) t, i);
        }
        return (i2 & i3) != 0;
    }

    /* renamed from: a */
    private final boolean m7847a(T t, int i) {
        if (this.f4604g) {
            int d = m7861d(i);
            long j = (long) (d & 1048575);
            switch ((d & 267386880) >>> 20) {
                case 0:
                    return C2566j7.m6541e(t, j) != 0.0d;
                case 1:
                    return C2566j7.m6536d(t, j) != 0.0f;
                case 2:
                    return C2566j7.m6527b(t, j) != 0;
                case 3:
                    return C2566j7.m6527b(t, j) != 0;
                case 4:
                    return C2566j7.m6514a(t, j) != 0;
                case 5:
                    return C2566j7.m6527b(t, j) != 0;
                case 6:
                    return C2566j7.m6514a(t, j) != 0;
                case 7:
                    return C2566j7.m6535c(t, j);
                case 8:
                    Object f = C2566j7.m6544f(t, j);
                    if (f instanceof String) {
                        return !((String) f).isEmpty();
                    }
                    if (f instanceof C2498f3) {
                        return !C2498f3.f4094Q.equals(f);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return C2566j7.m6544f(t, j) != null;
                case 10:
                    return !C2498f3.f4094Q.equals(C2566j7.m6544f(t, j));
                case 11:
                    return C2566j7.m6514a(t, j) != 0;
                case 12:
                    return C2566j7.m6514a(t, j) != 0;
                case 13:
                    return C2566j7.m6514a(t, j) != 0;
                case 14:
                    return C2566j7.m6527b(t, j) != 0;
                case 15:
                    return C2566j7.m6514a(t, j) != 0;
                case 16:
                    return C2566j7.m6527b(t, j) != 0;
                case 17:
                    return C2566j7.m6544f(t, j) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int e = m7863e(i);
            return (C2566j7.m6514a(t, (long) (e & 1048575)) & (1 << (e >>> 20))) != 0;
        }
    }

    /* renamed from: a */
    private final boolean m7848a(T t, int i, int i2) {
        return C2566j7.m6514a(t, (long) (m7863e(i2) & 1048575)) == i;
    }

    /* renamed from: a */
    private final int m7832a(int i, int i2) {
        if (i < this.f4600c || i > this.f4601d) {
            return -1;
        }
        return m7852b(i, i2);
    }
}
