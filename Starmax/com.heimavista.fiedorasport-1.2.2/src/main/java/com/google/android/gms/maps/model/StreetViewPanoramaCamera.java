package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.maps.model.StreetViewPanoramaOrientation;

public class StreetViewPanoramaCamera extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<StreetViewPanoramaCamera> CREATOR = new C2948s();

    /* renamed from: P */
    public final float f4884P;

    /* renamed from: Q */
    public final float f4885Q;

    /* renamed from: R */
    public final float f4886R;

    public StreetViewPanoramaCamera(float f, float f2, float f3) {
        boolean z = -90.0f <= f2 && f2 <= 90.0f;
        StringBuilder sb = new StringBuilder(62);
        sb.append("Tilt needs to be between -90 and 90 inclusive: ");
        sb.append(f2);
        C2258v.m5637a(z, sb.toString());
        this.f4884P = ((double) f) <= 0.0d ? 0.0f : f;
        this.f4885Q = 0.0f + f2;
        this.f4886R = (((double) f3) <= 0.0d ? (f3 % 360.0f) + 360.0f : f3) % 360.0f;
        StreetViewPanoramaOrientation.C2928a aVar = new StreetViewPanoramaOrientation.C2928a();
        aVar.mo18607b(f2);
        aVar.mo18605a(f3);
        aVar.mo18606a();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof StreetViewPanoramaCamera)) {
            return false;
        }
        StreetViewPanoramaCamera streetViewPanoramaCamera = (StreetViewPanoramaCamera) obj;
        return Float.floatToIntBits(this.f4884P) == Float.floatToIntBits(streetViewPanoramaCamera.f4884P) && Float.floatToIntBits(this.f4885Q) == Float.floatToIntBits(streetViewPanoramaCamera.f4885Q) && Float.floatToIntBits(this.f4886R) == Float.floatToIntBits(streetViewPanoramaCamera.f4886R);
    }

    public int hashCode() {
        return C2251t.m5615a(Float.valueOf(this.f4884P), Float.valueOf(this.f4885Q), Float.valueOf(this.f4886R));
    }

    public String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("zoom", Float.valueOf(this.f4884P));
        a.mo17037a("tilt", Float.valueOf(this.f4885Q));
        a.mo17037a("bearing", Float.valueOf(this.f4886R));
        return a.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5590a(parcel, 2, this.f4884P);
        C2250b.m5590a(parcel, 3, this.f4885Q);
        C2250b.m5590a(parcel, 4, this.f4886R);
        C2250b.m5587a(parcel, a);
    }
}
