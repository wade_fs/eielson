package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.p5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3153p5 implements Callable<List<C3234w9>> {

    /* renamed from: a */
    private final /* synthetic */ zzm f5543a;

    /* renamed from: b */
    private final /* synthetic */ String f5544b;

    /* renamed from: c */
    private final /* synthetic */ String f5545c;

    /* renamed from: d */
    private final /* synthetic */ C3141o5 f5546d;

    C3153p5(C3141o5 o5Var, zzm zzm, String str, String str2) {
        this.f5546d = o5Var;
        this.f5543a = zzm;
        this.f5544b = str;
        this.f5545c = str2;
    }

    public final /* synthetic */ Object call() {
        this.f5546d.f5496a.mo19237q();
        return this.f5546d.f5496a.mo19229e().mo18847a(this.f5543a.f5814P, this.f5544b, this.f5545c);
    }
}
