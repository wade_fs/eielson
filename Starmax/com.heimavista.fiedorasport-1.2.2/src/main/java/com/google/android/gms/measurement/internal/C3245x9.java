package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.measurement.internal.x9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3245x9 implements Parcelable.Creator<zzkq> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        String str = null;
        Long l = null;
        Float f = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        long j = 0;
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    i = C2248a.m5585z(parcel2, a);
                    break;
                case 2:
                    str = C2248a.m5573n(parcel2, a);
                    break;
                case 3:
                    j = C2248a.m5546B(parcel2, a);
                    break;
                case 4:
                    l = C2248a.m5547C(parcel2, a);
                    break;
                case 5:
                    f = C2248a.m5583x(parcel2, a);
                    break;
                case 6:
                    str2 = C2248a.m5573n(parcel2, a);
                    break;
                case 7:
                    str3 = C2248a.m5573n(parcel2, a);
                    break;
                case 8:
                    d = C2248a.m5581v(parcel2, a);
                    break;
                default:
                    C2248a.m5550F(parcel2, a);
                    break;
            }
        }
        C2248a.m5576q(parcel2, b);
        return new zzkq(i, str, j, l, f, str2, str3, d);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzkq[i];
    }
}
