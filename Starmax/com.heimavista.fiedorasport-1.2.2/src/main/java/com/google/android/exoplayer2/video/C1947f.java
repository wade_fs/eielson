package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.f */
/* compiled from: lambda */
public final /* synthetic */ class C1947f implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2961P;

    /* renamed from: Q */
    private final /* synthetic */ DecoderCounters f2962Q;

    public /* synthetic */ C1947f(VideoRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f2961P = eventDispatcher;
        this.f2962Q = decoderCounters;
    }

    public final void run() {
        this.f2961P.mo16269a(this.f2962Q);
    }
}
