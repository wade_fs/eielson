package com.google.android.exoplayer2.offline;

import android.os.ConditionVariable;

/* renamed from: com.google.android.exoplayer2.offline.h */
/* compiled from: lambda */
public final /* synthetic */ class C1850h implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ ConditionVariable f2852P;

    public /* synthetic */ C1850h(ConditionVariable conditionVariable) {
        this.f2852P = conditionVariable;
    }

    public final void run() {
        this.f2852P.open();
    }
}
