package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.p9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
abstract class C3157p9 extends C3121m9 {

    /* renamed from: c */
    private boolean f5555c;

    C3157p9(C3145o9 o9Var) {
        super(o9Var);
        super.f5360b.mo19210a(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: p */
    public final boolean mo19283p() {
        return this.f5555c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final void mo19284q() {
        if (!mo19283p()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    /* renamed from: s */
    public final void mo19285s() {
        if (!this.f5555c) {
            mo18833t();
            super.f5360b.mo19238s();
            this.f5555c = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public abstract boolean mo18833t();
}
