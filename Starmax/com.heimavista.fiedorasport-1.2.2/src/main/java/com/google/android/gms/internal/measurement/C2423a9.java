package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.a9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2423a9 implements C2788x8 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f3982a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f3982a = c2Var.mo17367a("measurement.gold.enhanced_ecommerce.format_logs", false);
        c2Var.mo17367a("measurement.gold.enhanced_ecommerce.nested_complex_events", false);
    }

    /* renamed from: a */
    public final boolean mo17286a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17287e() {
        return f3982a.mo18128b().booleanValue();
    }
}
