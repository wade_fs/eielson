package com.google.android.gms.internal.location;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.google.android.gms.internal.location.x */
public class C2409x {
    static {
        C2409x.class.getClassLoader();
    }

    private C2409x() {
    }

    /* renamed from: a */
    public static <T extends Parcelable> T m5938a(Parcel parcel, Parcelable.Creator creator) {
        if (parcel.readInt() == 0) {
            return null;
        }
        return (Parcelable) creator.createFromParcel(parcel);
    }

    /* renamed from: a */
    public static void m5939a(Parcel parcel, Parcelable parcelable) {
        if (parcelable == null) {
            parcel.writeInt(0);
            return;
        }
        parcel.writeInt(1);
        parcelable.writeToParcel(parcel, 0);
    }

    /* renamed from: a */
    public static void m5940a(Parcel parcel, boolean z) {
        parcel.writeInt(z ? 1 : 0);
    }
}
