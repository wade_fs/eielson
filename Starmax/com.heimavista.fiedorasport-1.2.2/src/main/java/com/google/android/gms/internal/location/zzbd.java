package com.google.android.gms.internal.location;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ClientIdentity;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.location.LocationRequest;
import java.util.Collections;
import java.util.List;

public final class zzbd extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zzbd> CREATOR = new C2403r();

    /* renamed from: W */
    static final List<ClientIdentity> f3936W = Collections.emptyList();

    /* renamed from: P */
    private LocationRequest f3937P;

    /* renamed from: Q */
    private List<ClientIdentity> f3938Q;
    @Nullable

    /* renamed from: R */
    private String f3939R;

    /* renamed from: S */
    private boolean f3940S;

    /* renamed from: T */
    private boolean f3941T;

    /* renamed from: U */
    private boolean f3942U;
    @Nullable

    /* renamed from: V */
    private String f3943V;

    zzbd(LocationRequest locationRequest, List<ClientIdentity> list, @Nullable String str, boolean z, boolean z2, boolean z3, String str2) {
        this.f3937P = locationRequest;
        this.f3938Q = list;
        this.f3939R = str;
        this.f3940S = z;
        this.f3941T = z2;
        this.f3942U = z3;
        this.f3943V = str2;
    }

    @Deprecated
    /* renamed from: a */
    public static zzbd m5950a(LocationRequest locationRequest) {
        return new zzbd(locationRequest, f3936W, null, false, false, false, null);
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof zzbd)) {
            return false;
        }
        zzbd zzbd = (zzbd) obj;
        return C2251t.m5617a(this.f3937P, zzbd.f3937P) && C2251t.m5617a(this.f3938Q, zzbd.f3938Q) && C2251t.m5617a(this.f3939R, zzbd.f3939R) && this.f3940S == zzbd.f3940S && this.f3941T == zzbd.f3941T && this.f3942U == zzbd.f3942U && C2251t.m5617a(this.f3943V, zzbd.f3943V);
    }

    public final int hashCode() {
        return this.f3937P.hashCode();
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f3937P);
        if (this.f3939R != null) {
            sb.append(" tag=");
            sb.append(this.f3939R);
        }
        if (this.f3943V != null) {
            sb.append(" moduleId=");
            sb.append(this.f3943V);
        }
        sb.append(" hideAppOps=");
        sb.append(this.f3940S);
        sb.append(" clients=");
        sb.append(this.f3938Q);
        sb.append(" forceCoarseLocation=");
        sb.append(this.f3941T);
        if (this.f3942U) {
            sb.append(" exemptFromBackgroundThrottle");
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.location.LocationRequest, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 1, (Parcelable) this.f3937P, i, false);
        C2250b.m5614c(parcel, 5, this.f3938Q, false);
        C2250b.m5602a(parcel, 6, this.f3939R, false);
        C2250b.m5605a(parcel, 7, this.f3940S);
        C2250b.m5605a(parcel, 8, this.f3941T);
        C2250b.m5605a(parcel, 9, this.f3942U);
        C2250b.m5602a(parcel, 10, this.f3943V, false);
        C2250b.m5587a(parcel, a);
    }
}
