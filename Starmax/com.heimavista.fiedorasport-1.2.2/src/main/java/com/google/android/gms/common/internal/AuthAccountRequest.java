package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class AuthAccountRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<AuthAccountRequest> CREATOR = new C2189a0();

    /* renamed from: P */
    private final int f3604P;
    @Deprecated

    /* renamed from: Q */
    private final IBinder f3605Q;

    /* renamed from: R */
    private final Scope[] f3606R;

    /* renamed from: S */
    private Integer f3607S;

    /* renamed from: T */
    private Integer f3608T;

    /* renamed from: U */
    private Account f3609U;

    AuthAccountRequest(int i, IBinder iBinder, Scope[] scopeArr, Integer num, Integer num2, Account account) {
        this.f3604P = i;
        this.f3605Q = iBinder;
        this.f3606R = scopeArr;
        this.f3607S = num;
        this.f3608T = num2;
        this.f3609U = account;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.api.Scope[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
     arg types: [android.os.Parcel, int, java.lang.Integer, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3604P);
        C2250b.m5594a(parcel, 2, this.f3605Q, false);
        C2250b.m5607a(parcel, 3, (Parcelable[]) this.f3606R, i, false);
        C2250b.m5600a(parcel, 4, this.f3607S, false);
        C2250b.m5600a(parcel, 5, this.f3608T, false);
        C2250b.m5596a(parcel, 6, (Parcelable) this.f3609U, i, false);
        C2250b.m5587a(parcel, a);
    }
}
