package com.google.android.gms.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.signin.internal.c */
public class C3270c extends C3272e {
    /* renamed from: a */
    public final void mo19479a(ConnectionResult connectionResult, zaa zaa) {
    }

    /* renamed from: a */
    public final void mo19480a(Status status, GoogleSignInAccount googleSignInAccount) {
    }

    /* renamed from: c */
    public final void mo19481c(Status status) {
    }

    /* renamed from: d */
    public final void mo19482d(Status status) {
    }
}
