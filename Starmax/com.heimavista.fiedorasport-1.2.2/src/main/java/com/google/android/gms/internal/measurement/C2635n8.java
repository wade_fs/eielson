package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.n8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2635n8 implements C2593l2<C2680q8> {

    /* renamed from: Q */
    private static C2635n8 f4355Q = new C2635n8();

    /* renamed from: P */
    private final C2593l2<C2680q8> f4356P;

    private C2635n8(C2593l2<C2680q8> l2Var) {
        this.f4356P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6933b() {
        return ((C2680q8) f4355Q.mo17285a()).mo17809a();
    }

    /* renamed from: c */
    public static boolean m6934c() {
        return ((C2680q8) f4355Q.mo17285a()).mo17810e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4356P.mo17285a();
    }

    public C2635n8() {
        this(C2579k2.m6601a(new C2665p8()));
    }
}
