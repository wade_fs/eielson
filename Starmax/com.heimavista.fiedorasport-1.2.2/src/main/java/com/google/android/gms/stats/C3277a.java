package com.google.android.gms.stats;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.PowerManager;
import android.os.WorkSource;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p088i.C2179a;
import com.google.android.gms.common.util.C2325p;
import com.google.android.gms.common.util.C2327r;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.google.android.gms.stats.a */
public class C3277a {

    /* renamed from: l */
    private static ScheduledExecutorService f5860l;

    /* renamed from: a */
    private final Object f5861a;

    /* renamed from: b */
    private final PowerManager.WakeLock f5862b;

    /* renamed from: c */
    private WorkSource f5863c;

    /* renamed from: d */
    private final int f5864d;

    /* renamed from: e */
    private final String f5865e;

    /* renamed from: f */
    private final String f5866f;

    /* renamed from: g */
    private final Context f5867g;

    /* renamed from: h */
    private boolean f5868h;

    /* renamed from: i */
    private final Map<String, Integer[]> f5869i;

    /* renamed from: j */
    private int f5870j;

    /* renamed from: k */
    private AtomicInteger f5871k;

    /* renamed from: com.google.android.gms.stats.a$a */
    public interface C3278a {
    }

    static {
        new C3279b();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public C3277a(@NonNull Context context, int i, @NonNull String str) {
        this(context, i, str, null, context == null ? null : context.getPackageName());
    }

    /* renamed from: b */
    private final List<String> m9513b() {
        return C2327r.m5812a(this.f5863c);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0054, code lost:
        if (r2 == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005c, code lost:
        if (r13.f5870j == 0) goto L_0x005e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x005e, code lost:
        com.google.android.gms.common.stats.C2306d.m5752a().mo17127a(r13.f5867g, com.google.android.gms.common.stats.C2305c.m5749a(r13.f5862b, r6), 7, r13.f5865e, r6, null, r13.f5864d, m9513b(), r14);
        r13.f5870j++;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo19496a(long r14) {
        /*
            r13 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r13.f5871k
            r0.incrementAndGet()
            r0 = 0
            java.lang.String r6 = r13.m9510a(r0)
            java.lang.Object r0 = r13.f5861a
            monitor-enter(r0)
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.f5869i     // Catch:{ all -> 0x0096 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x0096 }
            r2 = 0
            if (r1 == 0) goto L_0x001a
            int r1 = r13.f5870j     // Catch:{ all -> 0x0096 }
            if (r1 <= 0) goto L_0x0029
        L_0x001a:
            android.os.PowerManager$WakeLock r1 = r13.f5862b     // Catch:{ all -> 0x0096 }
            boolean r1 = r1.isHeld()     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0029
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.f5869i     // Catch:{ all -> 0x0096 }
            r1.clear()     // Catch:{ all -> 0x0096 }
            r13.f5870j = r2     // Catch:{ all -> 0x0096 }
        L_0x0029:
            boolean r1 = r13.f5868h     // Catch:{ all -> 0x0096 }
            r12 = 1
            if (r1 == 0) goto L_0x0056
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.f5869i     // Catch:{ all -> 0x0096 }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x0096 }
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x0047
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r13.f5869i     // Catch:{ all -> 0x0096 }
            java.lang.Integer[] r3 = new java.lang.Integer[r12]     // Catch:{ all -> 0x0096 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x0096 }
            r3[r2] = r4     // Catch:{ all -> 0x0096 }
            r1.put(r6, r3)     // Catch:{ all -> 0x0096 }
            r2 = 1
            goto L_0x0054
        L_0x0047:
            r3 = r1[r2]     // Catch:{ all -> 0x0096 }
            int r3 = r3.intValue()     // Catch:{ all -> 0x0096 }
            int r3 = r3 + r12
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x0096 }
            r1[r2] = r3     // Catch:{ all -> 0x0096 }
        L_0x0054:
            if (r2 != 0) goto L_0x005e
        L_0x0056:
            boolean r1 = r13.f5868h     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x007d
            int r1 = r13.f5870j     // Catch:{ all -> 0x0096 }
            if (r1 != 0) goto L_0x007d
        L_0x005e:
            com.google.android.gms.common.stats.d r1 = com.google.android.gms.common.stats.C2306d.m5752a()     // Catch:{ all -> 0x0096 }
            android.content.Context r2 = r13.f5867g     // Catch:{ all -> 0x0096 }
            android.os.PowerManager$WakeLock r3 = r13.f5862b     // Catch:{ all -> 0x0096 }
            java.lang.String r3 = com.google.android.gms.common.stats.C2305c.m5749a(r3, r6)     // Catch:{ all -> 0x0096 }
            r4 = 7
            java.lang.String r5 = r13.f5865e     // Catch:{ all -> 0x0096 }
            r7 = 0
            int r8 = r13.f5864d     // Catch:{ all -> 0x0096 }
            java.util.List r9 = r13.m9513b()     // Catch:{ all -> 0x0096 }
            r10 = r14
            r1.mo17127a(r2, r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0096 }
            int r1 = r13.f5870j     // Catch:{ all -> 0x0096 }
            int r1 = r1 + r12
            r13.f5870j = r1     // Catch:{ all -> 0x0096 }
        L_0x007d:
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            android.os.PowerManager$WakeLock r0 = r13.f5862b
            r0.acquire()
            r0 = 0
            int r2 = (r14 > r0 ? 1 : (r14 == r0 ? 0 : -1))
            if (r2 <= 0) goto L_0x0095
            java.util.concurrent.ScheduledExecutorService r0 = com.google.android.gms.stats.C3277a.f5860l
            com.google.android.gms.stats.c r1 = new com.google.android.gms.stats.c
            r1.<init>(r13)
            java.util.concurrent.TimeUnit r2 = java.util.concurrent.TimeUnit.MILLISECONDS
            r0.schedule(r1, r14, r2)
        L_0x0095:
            return
        L_0x0096:
            r14 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0096 }
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.stats.C3277a.mo19496a(long):void");
    }

    private C3277a(@NonNull Context context, int i, @NonNull String str, @Nullable String str2, @NonNull String str3) {
        this(context, i, str, null, str3, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
     arg types: [java.lang.String, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String */
    @SuppressLint({"UnwrappedWakeLock"})
    private C3277a(@NonNull Context context, int i, @NonNull String str, @Nullable String str2, @NonNull String str3, @Nullable String str4) {
        this.f5861a = this;
        this.f5868h = true;
        this.f5869i = new HashMap();
        Collections.synchronizedSet(new HashSet());
        this.f5871k = new AtomicInteger(0);
        C2258v.m5630a(context, "WakeLock: context must not be null");
        C2258v.m5631a(str, (Object) "WakeLock: wakeLockName must not be empty");
        this.f5864d = i;
        this.f5866f = null;
        this.f5867g = context.getApplicationContext();
        if (!"com.google.android.gms".equals(context.getPackageName())) {
            String valueOf = String.valueOf(str);
            this.f5865e = valueOf.length() != 0 ? "*gcore*:".concat(valueOf) : new String("*gcore*:");
        } else {
            this.f5865e = str;
        }
        this.f5862b = ((PowerManager) context.getSystemService("power")).newWakeLock(i, str);
        if (C2327r.m5814a(context)) {
            this.f5863c = C2327r.m5809a(context, C2325p.m5805a(str3) ? context.getPackageName() : str3);
            WorkSource workSource = this.f5863c;
            if (workSource != null && C2327r.m5814a(this.f5867g)) {
                WorkSource workSource2 = this.f5863c;
                if (workSource2 != null) {
                    workSource2.add(workSource);
                } else {
                    this.f5863c = workSource;
                }
                try {
                    this.f5862b.setWorkSource(this.f5863c);
                } catch (ArrayIndexOutOfBoundsException | IllegalArgumentException e) {
                    Log.wtf("WakeLock", e.toString());
                }
            }
        }
        if (f5860l == null) {
            f5860l = C2179a.m5317a().mo16859a();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        if (r1 != false) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0058, code lost:
        if (r12.f5870j == 1) goto L_0x005a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005a, code lost:
        com.google.android.gms.common.stats.C2306d.m5752a().mo17126a(r12.f5867g, com.google.android.gms.common.stats.C2305c.m5749a(r12.f5862b, r6), 8, r12.f5865e, r6, null, r12.f5864d, m9513b());
        r12.f5870j--;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo19495a() {
        /*
            r12 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = r12.f5871k
            int r0 = r0.decrementAndGet()
            if (r0 >= 0) goto L_0x0019
            java.lang.String r0 = r12.f5865e
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r1 = " release without a matched acquire!"
            java.lang.String r0 = r0.concat(r1)
            java.lang.String r1 = "WakeLock"
            android.util.Log.e(r1, r0)
        L_0x0019:
            r0 = 0
            java.lang.String r6 = r12.m9510a(r0)
            java.lang.Object r0 = r12.f5861a
            monitor-enter(r0)
            boolean r1 = r12.f5868h     // Catch:{ all -> 0x007e }
            r10 = 1
            r11 = 0
            if (r1 == 0) goto L_0x0052
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r12.f5869i     // Catch:{ all -> 0x007e }
            java.lang.Object r1 = r1.get(r6)     // Catch:{ all -> 0x007e }
            java.lang.Integer[] r1 = (java.lang.Integer[]) r1     // Catch:{ all -> 0x007e }
            if (r1 != 0) goto L_0x0033
        L_0x0031:
            r1 = 0
            goto L_0x0050
        L_0x0033:
            r2 = r1[r11]     // Catch:{ all -> 0x007e }
            int r2 = r2.intValue()     // Catch:{ all -> 0x007e }
            if (r2 != r10) goto L_0x0042
            java.util.Map<java.lang.String, java.lang.Integer[]> r1 = r12.f5869i     // Catch:{ all -> 0x007e }
            r1.remove(r6)     // Catch:{ all -> 0x007e }
            r1 = 1
            goto L_0x0050
        L_0x0042:
            r2 = r1[r11]     // Catch:{ all -> 0x007e }
            int r2 = r2.intValue()     // Catch:{ all -> 0x007e }
            int r2 = r2 - r10
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x007e }
            r1[r11] = r2     // Catch:{ all -> 0x007e }
            goto L_0x0031
        L_0x0050:
            if (r1 != 0) goto L_0x005a
        L_0x0052:
            boolean r1 = r12.f5868h     // Catch:{ all -> 0x007e }
            if (r1 != 0) goto L_0x0079
            int r1 = r12.f5870j     // Catch:{ all -> 0x007e }
            if (r1 != r10) goto L_0x0079
        L_0x005a:
            com.google.android.gms.common.stats.d r1 = com.google.android.gms.common.stats.C2306d.m5752a()     // Catch:{ all -> 0x007e }
            android.content.Context r2 = r12.f5867g     // Catch:{ all -> 0x007e }
            android.os.PowerManager$WakeLock r3 = r12.f5862b     // Catch:{ all -> 0x007e }
            java.lang.String r3 = com.google.android.gms.common.stats.C2305c.m5749a(r3, r6)     // Catch:{ all -> 0x007e }
            r4 = 8
            java.lang.String r5 = r12.f5865e     // Catch:{ all -> 0x007e }
            r7 = 0
            int r8 = r12.f5864d     // Catch:{ all -> 0x007e }
            java.util.List r9 = r12.m9513b()     // Catch:{ all -> 0x007e }
            r1.mo17126a(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x007e }
            int r1 = r12.f5870j     // Catch:{ all -> 0x007e }
            int r1 = r1 - r10
            r12.f5870j = r1     // Catch:{ all -> 0x007e }
        L_0x0079:
            monitor-exit(r0)     // Catch:{ all -> 0x007e }
            r12.m9511a(r11)
            return
        L_0x007e:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x007e }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.stats.C3277a.mo19495a():void");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m9511a(int i) {
        if (this.f5862b.isHeld()) {
            try {
                this.f5862b.release();
            } catch (RuntimeException e) {
                if (e.getClass().equals(RuntimeException.class)) {
                    Log.e("WakeLock", String.valueOf(this.f5865e).concat(" was already released!"), e);
                } else {
                    throw e;
                }
            }
            this.f5862b.isHeld();
        }
    }

    /* renamed from: a */
    private final String m9510a(String str) {
        if (this.f5868h) {
            return !TextUtils.isEmpty(str) ? str : this.f5866f;
        }
        return this.f5866f;
    }

    /* renamed from: a */
    public void mo19497a(boolean z) {
        this.f5862b.setReferenceCounted(z);
        this.f5868h = z;
    }
}
