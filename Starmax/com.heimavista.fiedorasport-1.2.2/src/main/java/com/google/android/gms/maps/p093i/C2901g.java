package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.i.g */
public interface C2901g extends IInterface {
    /* renamed from: a */
    void mo18471a(Bundle bundle);

    /* renamed from: a */
    void mo18472a(C2916u uVar);

    /* renamed from: b */
    void mo18473b();

    /* renamed from: b */
    void mo18474b(Bundle bundle);

    /* renamed from: c */
    void mo18475c();

    /* renamed from: d */
    void mo18476d();

    C3988b getView();

    void onLowMemory();

    void onResume();

    void onStart();
}
