package com.google.android.gms.measurement.internal;

import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import com.google.android.gms.internal.measurement.C2458cc;
import com.google.android.gms.internal.measurement.C2606l9;
import com.google.android.gms.internal.measurement.C2671q0;
import com.google.android.gms.internal.measurement.C2686r0;
import com.google.android.gms.internal.measurement.C2794y0;
import com.google.android.gms.internal.measurement.C2810z0;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.ea */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3026ea {

    /* renamed from: a */
    private String f5110a;

    /* renamed from: b */
    private boolean f5111b;

    /* renamed from: c */
    private C2794y0 f5112c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public BitSet f5113d;

    /* renamed from: e */
    private BitSet f5114e;

    /* renamed from: f */
    private Map<Integer, Long> f5115f;

    /* renamed from: g */
    private Map<Integer, List<Long>> f5116g;

    /* renamed from: h */
    private final /* synthetic */ C3002ca f5117h;

    private C3026ea(C3002ca caVar, String str) {
        this.f5117h = caVar;
        this.f5110a = str;
        this.f5111b = true;
        this.f5113d = new BitSet();
        this.f5114e = new BitSet();
        this.f5115f = new ArrayMap();
        this.f5116g = new ArrayMap();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo18992a(@NonNull C3086ja jaVar) {
        int a = jaVar.mo19038a();
        Boolean bool = jaVar.f5274c;
        if (bool != null) {
            this.f5114e.set(a, bool.booleanValue());
        }
        Boolean bool2 = jaVar.f5275d;
        if (bool2 != null) {
            this.f5113d.set(a, bool2.booleanValue());
        }
        if (jaVar.f5276e != null) {
            Long l = this.f5115f.get(Integer.valueOf(a));
            long longValue = jaVar.f5276e.longValue() / 1000;
            if (l == null || longValue > l.longValue()) {
                this.f5115f.put(Integer.valueOf(a), Long.valueOf(longValue));
            }
        }
        if (jaVar.f5277f != null) {
            List list = this.f5116g.get(Integer.valueOf(a));
            if (list == null) {
                list = new ArrayList();
                this.f5116g.put(Integer.valueOf(a), list);
            }
            if (C2458cc.m6163b() && this.f5117h.mo19013h().mo19152d(this.f5110a, C3135o.f5471s0) && jaVar.mo19040b()) {
                list.clear();
            }
            if (C2606l9.m6736b() && this.f5117h.mo19013h().mo19152d(this.f5110a, C3135o.f5479w0) && jaVar.mo19041c()) {
                list.clear();
            }
            if (!C2606l9.m6736b() || !this.f5117h.mo19013h().mo19152d(this.f5110a, C3135o.f5479w0)) {
                list.add(Long.valueOf(jaVar.f5277f.longValue() / 1000));
                return;
            }
            long longValue2 = jaVar.f5277f.longValue() / 1000;
            if (!list.contains(Long.valueOf(longValue2))) {
                list.add(Long.valueOf(longValue2));
            }
        }
    }

    private C3026ea(C3002ca caVar, String str, C2794y0 y0Var, BitSet bitSet, BitSet bitSet2, Map<Integer, Long> map, Map<Integer, Long> map2) {
        this.f5117h = caVar;
        this.f5110a = str;
        this.f5113d = bitSet;
        this.f5114e = bitSet2;
        this.f5115f = map;
        this.f5116g = new ArrayMap();
        if (map2 != null) {
            for (Integer num : map2.keySet()) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(map2.get(num));
                this.f5116g.put(num, arrayList);
            }
        }
        this.f5111b = false;
        this.f5112c = y0Var;
    }

    /* synthetic */ C3026ea(C3002ca caVar, String str, C2794y0 y0Var, BitSet bitSet, BitSet bitSet2, Map map, Map map2, C3038fa faVar) {
        this(caVar, str, y0Var, bitSet, bitSet2, map, map2);
    }

    /* synthetic */ C3026ea(C3002ca caVar, String str, C3038fa faVar) {
        this(caVar, str);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public final C2671q0 mo18991a(int i, List<Integer> list) {
        ArrayList arrayList;
        ArrayList arrayList2;
        List<Integer> list2 = list;
        C2671q0.C2672a v = C2671q0.m7058v();
        v.mo17820a(i);
        v.mo17823a(this.f5111b);
        C2794y0 y0Var = this.f5112c;
        if (y0Var != null) {
            v.mo17822a(y0Var);
        }
        C2794y0.C2795a w = C2794y0.m7800w();
        w.mo18159b(C3223v9.m9264a(this.f5113d));
        w.mo18157a(C3223v9.m9264a(this.f5114e));
        Map<Integer, Long> map = this.f5115f;
        if (map == null) {
            arrayList = null;
        } else {
            arrayList = new ArrayList(map.size());
            for (Integer num : this.f5115f.keySet()) {
                int intValue = num.intValue();
                C2686r0.C2687a s = C2686r0.m7106s();
                s.mo17841a(intValue);
                s.mo17842a(this.f5115f.get(Integer.valueOf(intValue)).longValue());
                arrayList.add((C2686r0) s.mo17679i());
            }
        }
        w.mo18160c(arrayList);
        Map<Integer, List<Long>> map2 = this.f5116g;
        if (map2 == null) {
            arrayList2 = Collections.emptyList();
        } else {
            ArrayList arrayList3 = new ArrayList(map2.size());
            for (Integer num2 : this.f5116g.keySet()) {
                C2810z0.C2811a s2 = C2810z0.m7899s();
                s2.mo18175a(num2.intValue());
                List list3 = this.f5116g.get(num2);
                if (list3 != null) {
                    Collections.sort(list3);
                    s2.mo18177a(list3);
                }
                arrayList3.add((C2810z0) s2.mo17679i());
            }
            arrayList2 = arrayList3;
        }
        if ((!C2458cc.m6163b() || !this.f5117h.mo19013h().mo19152d(this.f5110a, C3135o.f5471s0)) && v.mo17824j()) {
            List<C2810z0> u = v.mo17825k().mo18154u();
            if (!u.isEmpty()) {
                ArrayList arrayList4 = new ArrayList(arrayList2);
                ArrayMap arrayMap = new ArrayMap();
                for (C2810z0 z0Var : u) {
                    if (z0Var.mo18171n() && z0Var.mo18174q() > 0) {
                        arrayMap.put(Integer.valueOf(z0Var.mo18172o()), Long.valueOf(z0Var.mo18170b(z0Var.mo18174q() - 1)));
                    }
                }
                for (int i2 = 0; i2 < arrayList4.size(); i2++) {
                    C2810z0 z0Var2 = (C2810z0) arrayList4.get(i2);
                    Long l = (Long) arrayMap.remove(z0Var2.mo18171n() ? Integer.valueOf(z0Var2.mo18172o()) : null);
                    if (l != null && (list2 == null || !list2.contains(Integer.valueOf(z0Var2.mo18172o())))) {
                        ArrayList arrayList5 = new ArrayList();
                        if (l.longValue() < z0Var2.mo18170b(0)) {
                            arrayList5.add(l);
                        }
                        arrayList5.addAll(z0Var2.mo18173p());
                        C2810z0.C2811a aVar = (C2810z0.C2811a) z0Var2.mo17669j();
                        aVar.mo18178j();
                        aVar.mo18177a(arrayList5);
                        arrayList4.set(i2, (C2810z0) aVar.mo17679i());
                    }
                }
                for (Integer num3 : arrayMap.keySet()) {
                    C2810z0.C2811a s3 = C2810z0.m7899s();
                    s3.mo18175a(num3.intValue());
                    s3.mo18176a(((Long) arrayMap.get(num3)).longValue());
                    arrayList4.add((C2810z0) s3.mo17679i());
                }
                arrayList2 = arrayList4;
            }
        }
        w.mo18161d(arrayList2);
        v.mo17821a(w);
        return (C2671q0) v.mo17679i();
    }
}
