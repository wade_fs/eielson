package com.google.android.gms.common;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: com.google.android.gms.common.a */
public class C2015a implements ServiceConnection {

    /* renamed from: P */
    private boolean f3172P = false;

    /* renamed from: Q */
    private final BlockingQueue<IBinder> f3173Q = new LinkedBlockingQueue();

    /* renamed from: a */
    public IBinder mo16500a(long j, TimeUnit timeUnit) {
        C2258v.m5643c("BlockingServiceConnection.getServiceWithTimeout() called on main thread");
        if (!this.f3172P) {
            this.f3172P = true;
            IBinder poll = this.f3173Q.poll(j, timeUnit);
            if (poll != null) {
                return poll;
            }
            throw new TimeoutException("Timed out waiting for the service connection");
        }
        throw new IllegalStateException("Cannot call get on this connection more than once");
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        this.f3173Q.add(iBinder);
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
