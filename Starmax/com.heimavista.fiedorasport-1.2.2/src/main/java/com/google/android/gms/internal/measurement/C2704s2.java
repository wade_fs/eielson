package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.s2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2704s2 extends C2689r2 {

    /* renamed from: a */
    private final C2674q2 f4474a = new C2674q2();

    C2704s2() {
    }

    /* renamed from: a */
    public final void mo17787a(Throwable th, Throwable th2) {
        if (th2 == th) {
            throw new IllegalArgumentException("Self suppression is not allowed.", th2);
        } else if (th2 != null) {
            this.f4474a.mo17826a(th, true).add(th2);
        } else {
            throw new NullPointerException("The suppressed exception cannot be null.");
        }
    }
}
