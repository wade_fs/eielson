package com.google.android.gms.measurement.internal;

import java.util.Iterator;

/* renamed from: com.google.android.gms.measurement.internal.m */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3111m implements Iterator<String> {

    /* renamed from: P */
    private Iterator<String> f5337P = this.f5338Q.f5802P.keySet().iterator();

    /* renamed from: Q */
    private final /* synthetic */ zzam f5338Q;

    C3111m(zzam zzam) {
        this.f5338Q = zzam;
    }

    public final boolean hasNext() {
        return this.f5337P.hasNext();
    }

    public final /* synthetic */ Object next() {
        return this.f5337P.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException("Remove not supported");
    }
}
