package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.internal.measurement.hd */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2542hd implements Parcelable.Creator<zzv> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        long j = 0;
        long j2 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        Bundle bundle = null;
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    j = C2248a.m5546B(parcel2, a);
                    break;
                case 2:
                    j2 = C2248a.m5546B(parcel2, a);
                    break;
                case 3:
                    z = C2248a.m5577r(parcel2, a);
                    break;
                case 4:
                    str = C2248a.m5573n(parcel2, a);
                    break;
                case 5:
                    str2 = C2248a.m5573n(parcel2, a);
                    break;
                case 6:
                    str3 = C2248a.m5573n(parcel2, a);
                    break;
                case 7:
                    bundle = C2248a.m5565f(parcel2, a);
                    break;
                default:
                    C2248a.m5550F(parcel2, a);
                    break;
            }
        }
        C2248a.m5576q(parcel2, b);
        return new zzv(j, j2, z, str, str2, str3, bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzv[i];
    }
}
