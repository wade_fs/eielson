package com.google.android.gms.measurement;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.MainThread;
import com.google.android.gms.measurement.internal.C3033f5;
import com.google.android.gms.measurement.internal.C3262z4;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class AppMeasurementInstallReferrerReceiver extends BroadcastReceiver implements C3033f5 {

    /* renamed from: a */
    private C3262z4 f4926a;

    /* renamed from: a */
    public final BroadcastReceiver.PendingResult mo18701a() {
        return goAsync();
    }

    /* renamed from: a */
    public final void mo18702a(Context context, Intent intent) {
    }

    @MainThread
    public final void onReceive(Context context, Intent intent) {
        if (this.f4926a == null) {
            this.f4926a = new C3262z4(this);
        }
        this.f4926a.mo19417a(context, intent);
    }
}
