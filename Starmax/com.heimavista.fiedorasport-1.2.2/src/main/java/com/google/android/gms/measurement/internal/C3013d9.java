package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.d9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3013d9 implements Runnable {

    /* renamed from: P */
    long f5056P;

    /* renamed from: Q */
    final /* synthetic */ C3001c9 f5057Q;

    C3013d9(C3001c9 c9Var, long j) {
        this.f5057Q = c9Var;
        this.f5056P = j;
    }

    public final void run() {
        this.f5057Q.f5021c.mo19014j().mo19028a(new C3061h9(this));
    }
}
