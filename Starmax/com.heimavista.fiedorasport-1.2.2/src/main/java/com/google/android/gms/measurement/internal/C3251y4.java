package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.p090j.C2282b;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.stats.C2303a;
import com.google.android.gms.internal.measurement.C2561j2;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.y4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3251y4 {

    /* renamed from: a */
    final C3081j5 f5768a;

    C3251y4(C3081j5 j5Var) {
        this.f5768a = j5Var;
    }

    /* renamed from: b */
    private final boolean m9384b() {
        try {
            C2282b a = C2283c.m5685a(this.f5768a.mo19016n());
            if (a == null) {
                this.f5768a.mo19015l().mo18996B().mo19042a("Failed to get PackageManager for Install Referrer Play Store compatibility check");
                return false;
            } else if (a.mo17060b("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            this.f5768a.mo19015l().mo18996B().mo19043a("Failed to retrieve Play Store version for Install Referrer", e);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19401a() {
        this.f5768a.mo19092f();
        mo19402a(this.f5768a.mo19016n().getPackageName());
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19402a(String str) {
        if (str == null || str.isEmpty()) {
            this.f5768a.mo19015l().mo19005x().mo19042a("Install Referrer Reporter was called with invalid app package name");
            return;
        }
        this.f5768a.mo19014j().mo18881c();
        if (!m9384b()) {
            this.f5768a.mo19015l().mo19007z().mo19042a("Install Referrer Reporter is not available");
            return;
        }
        C3240x4 x4Var = new C3240x4(this, str);
        this.f5768a.mo19014j().mo18881c();
        Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
        intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
        PackageManager packageManager = this.f5768a.mo19016n().getPackageManager();
        if (packageManager == null) {
            this.f5768a.mo19015l().mo19005x().mo19042a("Failed to obtain Package Manager to verify binding conditions for Install Referrer");
            return;
        }
        List<ResolveInfo> queryIntentServices = packageManager.queryIntentServices(intent, 0);
        if (queryIntentServices == null || queryIntentServices.isEmpty()) {
            this.f5768a.mo19015l().mo19007z().mo19042a("Play Service for fetching Install Referrer is unavailable on device");
            return;
        }
        ResolveInfo resolveInfo = queryIntentServices.get(0);
        ServiceInfo serviceInfo = resolveInfo.serviceInfo;
        if (serviceInfo != null) {
            String str2 = serviceInfo.packageName;
            if (resolveInfo.serviceInfo.name == null || !"com.android.vending".equals(str2) || !m9384b()) {
                this.f5768a.mo19015l().mo19004w().mo19042a("Play Store version 8.3.73 or higher required for Install Referrer");
                return;
            }
            try {
                this.f5768a.mo19015l().mo18996B().mo19043a("Install Referrer Service is", C2303a.m5745a().mo17124a(this.f5768a.mo19016n(), new Intent(intent), x4Var, 1) ? "available" : "not available");
            } catch (Exception e) {
                this.f5768a.mo19015l().mo19001t().mo19043a("Exception occurred while binding to Install Referrer Service", e.getMessage());
            }
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    @Nullable
    /* renamed from: a */
    public final Bundle mo19400a(String str, C2561j2 j2Var) {
        this.f5768a.mo19014j().mo18881c();
        if (j2Var == null) {
            this.f5768a.mo19015l().mo19004w().mo19042a("Attempting to use Install Referrer Service while it is not initialized");
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("package_name", str);
        try {
            Bundle d = j2Var.mo17498d(bundle);
            if (d != null) {
                return d;
            }
            this.f5768a.mo19015l().mo19001t().mo19042a("Install Referrer Service returned a null response");
            return null;
        } catch (Exception e) {
            this.f5768a.mo19015l().mo19001t().mo19043a("Exception occurred while retrieving the Install Referrer", e.getMessage());
            return null;
        }
    }
}
