package com.google.android.gms.measurement.internal;

import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.n7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3131n7 implements Runnable {

    /* renamed from: P */
    private final C3143o7 f5373P;

    /* renamed from: Q */
    private final int f5374Q;

    /* renamed from: R */
    private final Exception f5375R;

    /* renamed from: S */
    private final byte[] f5376S;

    /* renamed from: T */
    private final Map f5377T;

    C3131n7(C3143o7 o7Var, int i, Exception exc, byte[] bArr, Map map) {
        this.f5373P = o7Var;
        this.f5374Q = i;
        this.f5375R = exc;
        this.f5376S = bArr;
        this.f5377T = map;
    }

    public final void run() {
        this.f5373P.mo19201a(this.f5374Q, this.f5375R, this.f5376S, this.f5377T);
    }
}
