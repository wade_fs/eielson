package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2541hc;

/* renamed from: com.google.android.gms.measurement.internal.i1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3065i1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5200a = new C3065i1();

    private C3065i1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2541hc.m6461b());
    }
}
