package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2458cc;

/* renamed from: com.google.android.gms.measurement.internal.h2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3054h2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5171a = new C3054h2();

    private C3054h2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2458cc.m6169h());
    }
}
