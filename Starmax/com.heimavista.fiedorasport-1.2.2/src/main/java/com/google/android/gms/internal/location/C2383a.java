package com.google.android.gms.internal.location;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.location.a */
public class C2383a implements IInterface {

    /* renamed from: a */
    private final IBinder f3919a;

    /* renamed from: b */
    private final String f3920b;

    protected C2383a(IBinder iBinder, String str) {
        this.f3919a = iBinder;
        this.f3920b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public final Parcel mo17193H() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f3920b);
        return obtain;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo17194a(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            this.f3919a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f3919a;
    }
}
