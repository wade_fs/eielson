package com.google.android.gms.measurement.internal;

import android.os.RemoteException;

/* renamed from: com.google.android.gms.measurement.internal.z7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3265z7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f5793P;

    /* renamed from: Q */
    private final /* synthetic */ C3232w7 f5794Q;

    C3265z7(C3232w7 w7Var, zzm zzm) {
        this.f5794Q = w7Var;
        this.f5793P = zzm;
    }

    public final void run() {
        C3228w3 d = this.f5794Q.f5724d;
        if (d == null) {
            this.f5794Q.mo19015l().mo19001t().mo19042a("Failed to reset data on the service: not connected to service");
            return;
        }
        try {
            d.mo19192a(this.f5793P);
        } catch (RemoteException e) {
            this.f5794Q.mo19015l().mo19001t().mo19043a("Failed to reset data on the service: remote exception", e);
        }
        this.f5794Q.m9314J();
    }
}
