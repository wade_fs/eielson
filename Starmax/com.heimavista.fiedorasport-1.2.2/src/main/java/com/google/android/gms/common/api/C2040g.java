package com.google.android.gms.common.api;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2157k;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.common.api.g */
public abstract class C2040g<R extends C2157k> {

    /* renamed from: com.google.android.gms.common.api.g$a */
    public interface C2041a {
        /* renamed from: a */
        void mo16589a(Status status);
    }

    @NonNull
    /* renamed from: a */
    public abstract R mo16586a(long j, @NonNull TimeUnit timeUnit);

    /* renamed from: a */
    public abstract void mo16587a(@NonNull C2041a aVar);

    /* renamed from: a */
    public abstract void mo16588a(@NonNull C2158l<? super R> lVar);
}
