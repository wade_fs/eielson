package com.google.android.gms.maps.model;

import android.os.RemoteException;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2258v;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p161c.p165d.C4043l;

/* renamed from: com.google.android.gms.maps.model.c */
public final class C2932c {

    /* renamed from: a */
    private final C4043l f4913a;

    public C2932c(C4043l lVar) {
        C2258v.m5629a(lVar);
        this.f4913a = lVar;
    }

    /* renamed from: a */
    public final void mo18627a(@NonNull LatLng latLng) {
        if (latLng != null) {
            try {
                this.f4913a.mo23677b(latLng);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        } else {
            throw new IllegalArgumentException("latlng cannot be null - a position is required.");
        }
    }

    @Nullable
    /* renamed from: b */
    public final Object mo18629b() {
        try {
            return C3992d.m11991e(this.f4913a.mo23681m());
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C2932c)) {
            return false;
        }
        try {
            return this.f4913a.mo23678b(((C2932c) obj).f4913a);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    public final int hashCode() {
        try {
            return this.f4913a.mo23680k();
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final LatLng mo18625a() {
        try {
            return this.f4913a.getPosition();
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final void mo18626a(float f) {
        try {
            this.f4913a.mo23675a(f);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: a */
    public final void mo18628a(@Nullable Object obj) {
        try {
            this.f4913a.mo23676a(C3992d.m11990a(obj));
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }
}
