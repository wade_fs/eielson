package com.google.android.gms.measurement.internal;

import java.net.InetAddress;
import java.net.Socket;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: com.google.android.gms.measurement.internal.y9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3256y9 extends SSLSocketFactory {

    /* renamed from: a */
    private final SSLSocketFactory f5781a;

    C3256y9() {
        this(HttpsURLConnection.getDefaultSSLSocketFactory());
    }

    /* renamed from: a */
    private final SSLSocket m9391a(SSLSocket sSLSocket) {
        return new C2978aa(this, sSLSocket);
    }

    public final Socket createSocket(Socket socket, String str, int i, boolean z) {
        return m9391a((SSLSocket) this.f5781a.createSocket(socket, str, i, z));
    }

    public final String[] getDefaultCipherSuites() {
        return this.f5781a.getDefaultCipherSuites();
    }

    public final String[] getSupportedCipherSuites() {
        return this.f5781a.getSupportedCipherSuites();
    }

    private C3256y9(SSLSocketFactory sSLSocketFactory) {
        this.f5781a = super;
    }

    public final Socket createSocket(String str, int i) {
        return m9391a((SSLSocket) this.f5781a.createSocket(str, i));
    }

    public final Socket createSocket(InetAddress inetAddress, int i) {
        return m9391a((SSLSocket) this.f5781a.createSocket(inetAddress, i));
    }

    public final Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        return m9391a((SSLSocket) this.f5781a.createSocket(str, i, inetAddress, i2));
    }

    public final Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return m9391a((SSLSocket) this.f5781a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    public final Socket createSocket() {
        return m9391a((SSLSocket) this.f5781a.createSocket());
    }
}
