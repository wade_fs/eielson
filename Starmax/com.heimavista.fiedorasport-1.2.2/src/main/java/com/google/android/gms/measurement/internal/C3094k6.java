package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import androidx.annotation.WorkerThread;

/* renamed from: com.google.android.gms.measurement.internal.k6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C3094k6 {
    @WorkerThread
    /* renamed from: a */
    void mo18720a(String str, String str2, Bundle bundle, long j);
}
