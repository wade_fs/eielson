package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4009a;
import p119e.p144d.p145a.p157c.p161c.p163b.C4011c;

/* renamed from: com.google.android.gms.dynamite.l */
public final class C2355l extends C4009a implements C2354k {
    C2355l(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoaderV2");
    }

    /* renamed from: a */
    public final C3988b mo17153a(C3988b bVar, String str, int i, C3988b bVar2) {
        Parcel a = mo23641a();
        C4011c.m12012a(a, bVar);
        a.writeString(str);
        a.writeInt(i);
        C4011c.m12012a(a, bVar2);
        Parcel a2 = mo23642a(2, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: b */
    public final C3988b mo17154b(C3988b bVar, String str, int i, C3988b bVar2) {
        Parcel a = mo23641a();
        C4011c.m12012a(a, bVar);
        a.writeString(str);
        a.writeInt(i);
        C4011c.m12012a(a, bVar2);
        Parcel a2 = mo23642a(3, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
