package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3535c;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.android.datatransport.cct.b.s */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1694s implements C3536d<C1675h> {
    /* renamed from: a */
    public void mo13453a(@Nullable Object obj, @NonNull Object obj2) {
        C1675h hVar = (C1675h) obj;
        C3537e eVar = (C3537e) obj2;
        eVar.mo22172a("requestTimeMs", hVar.mo13500f());
        eVar.mo22172a("requestUptimeMs", hVar.mo13501g());
        if (hVar.mo13495b() != null) {
            eVar.mo22173a("clientInfo", hVar.mo13495b());
        }
        if (hVar.mo13498e() != null) {
            eVar.mo22173a("logSourceName", hVar.mo13498e());
        } else if (hVar.mo13497d() != Integer.MIN_VALUE) {
            eVar.mo22171a("logSource", hVar.mo13497d());
        } else {
            throw new C3535c("Log request must have either LogSourceName or LogSource");
        }
        if (!hVar.mo13496c().isEmpty()) {
            eVar.mo22173a("logEvent", hVar.mo13496c());
        }
    }
}
