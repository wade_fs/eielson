package com.google.android.gms.common.p088i;

import com.google.android.gms.common.p088i.C2179a;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/* renamed from: com.google.android.gms.common.i.b */
final class C2181b implements C2179a.C2180a {
    C2181b() {
    }

    /* renamed from: a */
    public final ScheduledExecutorService mo16859a() {
        return Executors.newSingleThreadScheduledExecutor();
    }
}
