package com.google.android.gms.internal.location;

import android.location.Location;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.location.C2835f;

/* renamed from: com.google.android.gms.internal.location.o */
final class C2400o implements C2084i.C2086b<C2835f> {

    /* renamed from: a */
    private final /* synthetic */ Location f3930a;

    C2400o(C2399n nVar, Location location) {
        this.f3930a = location;
    }

    /* renamed from: a */
    public final void mo16725a() {
    }

    /* renamed from: a */
    public final /* synthetic */ void mo16726a(Object obj) {
        ((C2835f) obj).onLocationChanged(this.f3930a);
    }
}
