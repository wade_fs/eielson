package com.google.android.gms.maps;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.p093i.C2906k;

public final class GoogleMapOptions extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<GoogleMapOptions> CREATOR = new C2922j();

    /* renamed from: P */
    private Boolean f4714P;

    /* renamed from: Q */
    private Boolean f4715Q;

    /* renamed from: R */
    private int f4716R = -1;

    /* renamed from: S */
    private CameraPosition f4717S;

    /* renamed from: T */
    private Boolean f4718T;

    /* renamed from: U */
    private Boolean f4719U;

    /* renamed from: V */
    private Boolean f4720V;

    /* renamed from: W */
    private Boolean f4721W;

    /* renamed from: X */
    private Boolean f4722X;

    /* renamed from: Y */
    private Boolean f4723Y;

    /* renamed from: Z */
    private Boolean f4724Z;

    /* renamed from: a0 */
    private Boolean f4725a0;

    /* renamed from: b0 */
    private Boolean f4726b0;

    /* renamed from: c0 */
    private Float f4727c0 = null;

    /* renamed from: d0 */
    private Float f4728d0 = null;

    /* renamed from: e0 */
    private LatLngBounds f4729e0 = null;

    /* renamed from: f0 */
    private Boolean f4730f0;

    GoogleMapOptions(byte b, byte b2, int i, CameraPosition cameraPosition, byte b3, byte b4, byte b5, byte b6, byte b7, byte b8, byte b9, byte b10, byte b11, Float f, Float f2, LatLngBounds latLngBounds, byte b12) {
        this.f4714P = C2906k.m8221a(b);
        this.f4715Q = C2906k.m8221a(b2);
        this.f4716R = i;
        this.f4717S = cameraPosition;
        this.f4718T = C2906k.m8221a(b3);
        this.f4719U = C2906k.m8221a(b4);
        this.f4720V = C2906k.m8221a(b5);
        this.f4721W = C2906k.m8221a(b6);
        this.f4722X = C2906k.m8221a(b7);
        this.f4723Y = C2906k.m8221a(b8);
        this.f4724Z = C2906k.m8221a(b9);
        this.f4725a0 = C2906k.m8221a(b10);
        this.f4726b0 = C2906k.m8221a(b11);
        this.f4727c0 = f;
        this.f4728d0 = f2;
        this.f4729e0 = latLngBounds;
        this.f4730f0 = C2906k.m8221a(b12);
    }

    /* renamed from: a */
    public final GoogleMapOptions mo18310a(CameraPosition cameraPosition) {
        this.f4717S = cameraPosition;
        return this;
    }

    /* renamed from: b */
    public final GoogleMapOptions mo18314b(boolean z) {
        this.f4719U = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: c */
    public final GoogleMapOptions mo18315c(boolean z) {
        this.f4724Z = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: d */
    public final GoogleMapOptions mo18317d(int i) {
        this.f4716R = i;
        return this;
    }

    /* renamed from: e */
    public final GoogleMapOptions mo18320e(boolean z) {
        this.f4723Y = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: f */
    public final GoogleMapOptions mo18321f(boolean z) {
        this.f4720V = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: g */
    public final GoogleMapOptions mo18322g(boolean z) {
        this.f4730f0 = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: h */
    public final GoogleMapOptions mo18323h(boolean z) {
        this.f4722X = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: i */
    public final GoogleMapOptions mo18324i(boolean z) {
        this.f4715Q = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: j */
    public final GoogleMapOptions mo18325j(boolean z) {
        this.f4714P = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: k */
    public final GoogleMapOptions mo18326k(boolean z) {
        this.f4718T = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: l */
    public final GoogleMapOptions mo18327l(boolean z) {
        this.f4721W = Boolean.valueOf(z);
        return this;
    }

    public final String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("MapType", Integer.valueOf(this.f4716R));
        a.mo17037a("LiteMode", this.f4724Z);
        a.mo17037a("Camera", this.f4717S);
        a.mo17037a("CompassEnabled", this.f4719U);
        a.mo17037a("ZoomControlsEnabled", this.f4718T);
        a.mo17037a("ScrollGesturesEnabled", this.f4720V);
        a.mo17037a("ZoomGesturesEnabled", this.f4721W);
        a.mo17037a("TiltGesturesEnabled", this.f4722X);
        a.mo17037a("RotateGesturesEnabled", this.f4723Y);
        a.mo17037a("ScrollGesturesEnabledDuringRotateOrZoom", this.f4730f0);
        a.mo17037a("MapToolbarEnabled", this.f4725a0);
        a.mo17037a("AmbientEnabled", this.f4726b0);
        a.mo17037a("MinZoomPreference", this.f4727c0);
        a.mo17037a("MaxZoomPreference", this.f4728d0);
        a.mo17037a("LatLngBoundsForCameraTarget", this.f4729e0);
        a.mo17037a("ZOrderOnTop", this.f4714P);
        a.mo17037a("UseViewLifecycleInFragment", this.f4715Q);
        return a.toString();
    }

    /* renamed from: u */
    public final LatLngBounds mo18329u() {
        return this.f4729e0;
    }

    /* renamed from: v */
    public final int mo18330v() {
        return this.f4716R;
    }

    /* renamed from: w */
    public final Float mo18331w() {
        return this.f4728d0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.CameraPosition, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
     arg types: [android.os.Parcel, int, java.lang.Float, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLngBounds, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5588a(parcel, 2, C2906k.m8220a(this.f4714P));
        C2250b.m5588a(parcel, 3, C2906k.m8220a(this.f4715Q));
        C2250b.m5591a(parcel, 4, mo18330v());
        C2250b.m5596a(parcel, 5, (Parcelable) mo18316c(), i, false);
        C2250b.m5588a(parcel, 6, C2906k.m8220a(this.f4718T));
        C2250b.m5588a(parcel, 7, C2906k.m8220a(this.f4719U));
        C2250b.m5588a(parcel, 8, C2906k.m8220a(this.f4720V));
        C2250b.m5588a(parcel, 9, C2906k.m8220a(this.f4721W));
        C2250b.m5588a(parcel, 10, C2906k.m8220a(this.f4722X));
        C2250b.m5588a(parcel, 11, C2906k.m8220a(this.f4723Y));
        C2250b.m5588a(parcel, 12, C2906k.m8220a(this.f4724Z));
        C2250b.m5588a(parcel, 14, C2906k.m8220a(this.f4725a0));
        C2250b.m5588a(parcel, 15, C2906k.m8220a(this.f4726b0));
        C2250b.m5599a(parcel, 16, mo18333x(), false);
        C2250b.m5599a(parcel, 17, mo18331w(), false);
        C2250b.m5596a(parcel, 18, (Parcelable) mo18329u(), i, false);
        C2250b.m5588a(parcel, 19, C2906k.m8220a(this.f4730f0));
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final Float mo18333x() {
        return this.f4727c0;
    }

    /* renamed from: y */
    public final Boolean mo18334y() {
        return this.f4718T;
    }

    /* renamed from: a */
    public final GoogleMapOptions mo18312a(boolean z) {
        this.f4726b0 = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: b */
    public final GoogleMapOptions mo18313b(float f) {
        this.f4727c0 = Float.valueOf(f);
        return this;
    }

    /* renamed from: c */
    public final CameraPosition mo18316c() {
        return this.f4717S;
    }

    /* renamed from: d */
    public final GoogleMapOptions mo18318d(boolean z) {
        this.f4725a0 = Boolean.valueOf(z);
        return this;
    }

    /* renamed from: b */
    public static LatLngBounds m7990b(Context context, AttributeSet attributeSet) {
        if (context == null || attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, R$styleable.MapAttrs);
        Float valueOf = obtainAttributes.hasValue(R$styleable.MapAttrs_latLngBoundsSouthWestLatitude) ? Float.valueOf(obtainAttributes.getFloat(R$styleable.MapAttrs_latLngBoundsSouthWestLatitude, 0.0f)) : null;
        Float valueOf2 = obtainAttributes.hasValue(R$styleable.MapAttrs_latLngBoundsSouthWestLongitude) ? Float.valueOf(obtainAttributes.getFloat(R$styleable.MapAttrs_latLngBoundsSouthWestLongitude, 0.0f)) : null;
        Float valueOf3 = obtainAttributes.hasValue(R$styleable.MapAttrs_latLngBoundsNorthEastLatitude) ? Float.valueOf(obtainAttributes.getFloat(R$styleable.MapAttrs_latLngBoundsNorthEastLatitude, 0.0f)) : null;
        Float valueOf4 = obtainAttributes.hasValue(R$styleable.MapAttrs_latLngBoundsNorthEastLongitude) ? Float.valueOf(obtainAttributes.getFloat(R$styleable.MapAttrs_latLngBoundsNorthEastLongitude, 0.0f)) : null;
        obtainAttributes.recycle();
        if (valueOf == null || valueOf2 == null || valueOf3 == null || valueOf4 == null) {
            return null;
        }
        return new LatLngBounds(new LatLng((double) valueOf.floatValue(), (double) valueOf2.floatValue()), new LatLng((double) valueOf3.floatValue(), (double) valueOf4.floatValue()));
    }

    /* renamed from: c */
    public static CameraPosition m7991c(Context context, AttributeSet attributeSet) {
        if (context == null || attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, R$styleable.MapAttrs);
        LatLng latLng = new LatLng((double) (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraTargetLat) ? obtainAttributes.getFloat(R$styleable.MapAttrs_cameraTargetLat, 0.0f) : 0.0f), (double) (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraTargetLng) ? obtainAttributes.getFloat(R$styleable.MapAttrs_cameraTargetLng, 0.0f) : 0.0f));
        CameraPosition.C2926a c = CameraPosition.m8258c();
        c.mo18498a(latLng);
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraZoom)) {
            c.mo18501c(obtainAttributes.getFloat(R$styleable.MapAttrs_cameraZoom, 0.0f));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraBearing)) {
            c.mo18497a(obtainAttributes.getFloat(R$styleable.MapAttrs_cameraBearing, 0.0f));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraTilt)) {
            c.mo18500b(obtainAttributes.getFloat(R$styleable.MapAttrs_cameraTilt, 0.0f));
        }
        obtainAttributes.recycle();
        return c.mo18499a();
    }

    /* renamed from: a */
    public final GoogleMapOptions mo18309a(float f) {
        this.f4728d0 = Float.valueOf(f);
        return this;
    }

    /* renamed from: d */
    public final Boolean mo18319d() {
        return this.f4719U;
    }

    /* renamed from: a */
    public final GoogleMapOptions mo18311a(LatLngBounds latLngBounds) {
        this.f4729e0 = latLngBounds;
        return this;
    }

    /* renamed from: a */
    public static GoogleMapOptions m7989a(Context context, AttributeSet attributeSet) {
        if (context == null || attributeSet == null) {
            return null;
        }
        TypedArray obtainAttributes = context.getResources().obtainAttributes(attributeSet, R$styleable.MapAttrs);
        GoogleMapOptions googleMapOptions = new GoogleMapOptions();
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_mapType)) {
            googleMapOptions.mo18317d(obtainAttributes.getInt(R$styleable.MapAttrs_mapType, -1));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_zOrderOnTop)) {
            googleMapOptions.mo18325j(obtainAttributes.getBoolean(R$styleable.MapAttrs_zOrderOnTop, false));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_useViewLifecycle)) {
            googleMapOptions.mo18324i(obtainAttributes.getBoolean(R$styleable.MapAttrs_useViewLifecycle, false));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiCompass)) {
            googleMapOptions.mo18314b(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiCompass, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiRotateGestures)) {
            googleMapOptions.mo18320e(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiRotateGestures, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom)) {
            googleMapOptions.mo18322g(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiScrollGesturesDuringRotateOrZoom, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiScrollGestures)) {
            googleMapOptions.mo18321f(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiScrollGestures, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiTiltGestures)) {
            googleMapOptions.mo18323h(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiTiltGestures, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiZoomGestures)) {
            googleMapOptions.mo18327l(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiZoomGestures, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiZoomControls)) {
            googleMapOptions.mo18326k(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiZoomControls, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_liteMode)) {
            googleMapOptions.mo18315c(obtainAttributes.getBoolean(R$styleable.MapAttrs_liteMode, false));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_uiMapToolbar)) {
            googleMapOptions.mo18318d(obtainAttributes.getBoolean(R$styleable.MapAttrs_uiMapToolbar, true));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_ambientEnabled)) {
            googleMapOptions.mo18312a(obtainAttributes.getBoolean(R$styleable.MapAttrs_ambientEnabled, false));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraMinZoomPreference)) {
            googleMapOptions.mo18313b(obtainAttributes.getFloat(R$styleable.MapAttrs_cameraMinZoomPreference, Float.NEGATIVE_INFINITY));
        }
        if (obtainAttributes.hasValue(R$styleable.MapAttrs_cameraMinZoomPreference)) {
            googleMapOptions.mo18309a(obtainAttributes.getFloat(R$styleable.MapAttrs_cameraMaxZoomPreference, Float.POSITIVE_INFINITY));
        }
        googleMapOptions.mo18311a(m7990b(context, attributeSet));
        googleMapOptions.mo18310a(m7991c(context, attributeSet));
        obtainAttributes.recycle();
        return googleMapOptions;
    }

    public GoogleMapOptions() {
    }
}
