package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.location.p */
public final class C2854p implements Parcelable.Creator<LocationSettingsStates> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        boolean z4 = false;
        boolean z5 = false;
        boolean z6 = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    z = C2248a.m5577r(parcel, a);
                    break;
                case 2:
                    z2 = C2248a.m5577r(parcel, a);
                    break;
                case 3:
                    z3 = C2248a.m5577r(parcel, a);
                    break;
                case 4:
                    z4 = C2248a.m5577r(parcel, a);
                    break;
                case 5:
                    z5 = C2248a.m5577r(parcel, a);
                    break;
                case 6:
                    z6 = C2248a.m5577r(parcel, a);
                    break;
                default:
                    C2248a.m5550F(parcel, a);
                    break;
            }
        }
        C2248a.m5576q(parcel, b);
        return new LocationSettingsStates(z, z2, z3, z4, z5, z6);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationSettingsStates[i];
    }
}
