package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.internal.base.C2382h;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.common.api.internal.g2 */
public abstract class C2079g2 extends LifecycleCallback implements DialogInterface.OnCancelListener {

    /* renamed from: Q */
    protected volatile boolean f3329Q;

    /* renamed from: R */
    protected final AtomicReference<C2083h2> f3330R;

    /* renamed from: S */
    private final Handler f3331S;

    /* renamed from: T */
    protected final C2167b f3332T;

    protected C2079g2(C2080h hVar) {
        this(hVar, C2167b.m5251a());
    }

    /* renamed from: a */
    public void mo16605a(Bundle bundle) {
        super.mo16605a(bundle);
        if (bundle != null) {
            this.f3330R.set(bundle.getBoolean("resolving_error", false) ? new C2083h2(new ConnectionResult(bundle.getInt("failed_status"), (PendingIntent) bundle.getParcelable("failed_resolution")), bundle.getInt("failed_client_id", -1)) : null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16690a(ConnectionResult connectionResult, int i);

    /* renamed from: b */
    public void mo16608b(Bundle bundle) {
        super.mo16608b(bundle);
        C2083h2 h2Var = this.f3330R.get();
        if (h2Var != null) {
            bundle.putBoolean("resolving_error", true);
            bundle.putInt("failed_client_id", h2Var.mo16718b());
            bundle.putInt("failed_status", h2Var.mo16717a().mo16475c());
            bundle.putParcelable("failed_resolution", h2Var.mo16717a().mo16480u());
        }
    }

    /* renamed from: d */
    public void mo16610d() {
        super.mo16610d();
        this.f3329Q = true;
    }

    /* renamed from: e */
    public void mo16611e() {
        super.mo16611e();
        this.f3329Q = false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract void mo16691f();

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public final void mo16702g() {
        this.f3330R.set(null);
        mo16691f();
    }

    public void onCancel(DialogInterface dialogInterface) {
        mo16690a(new ConnectionResult(13, null), m4897a(this.f3330R.get()));
        mo16702g();
    }

    private C2079g2(C2080h hVar, C2167b bVar) {
        super(hVar);
        this.f3330R = new AtomicReference<>(null);
        this.f3331S = new C2382h(Looper.getMainLooper());
        this.f3332T = bVar;
    }

    /* renamed from: b */
    public final void mo16701b(ConnectionResult connectionResult, int i) {
        C2083h2 h2Var = new C2083h2(connectionResult, i);
        if (this.f3330R.compareAndSet(null, h2Var)) {
            this.f3331S.post(new C2090i2(this, h2Var));
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005c  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo16604a(int r4, int r5, android.content.Intent r6) {
        /*
            r3 = this;
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.common.api.internal.h2> r0 = r3.f3330R
            java.lang.Object r0 = r0.get()
            com.google.android.gms.common.api.internal.h2 r0 = (com.google.android.gms.common.api.internal.C2083h2) r0
            r1 = 1
            r2 = 0
            if (r4 == r1) goto L_0x0030
            r5 = 2
            if (r4 == r5) goto L_0x0010
            goto L_0x0055
        L_0x0010:
            com.google.android.gms.common.b r4 = r3.f3332T
            android.app.Activity r5 = r3.mo16603a()
            int r4 = r4.mo16831c(r5)
            if (r4 != 0) goto L_0x001d
            goto L_0x001e
        L_0x001d:
            r1 = 0
        L_0x001e:
            if (r0 != 0) goto L_0x0021
            return
        L_0x0021:
            com.google.android.gms.common.ConnectionResult r5 = r0.mo16717a()
            int r5 = r5.mo16475c()
            r6 = 18
            if (r5 != r6) goto L_0x0056
            if (r4 != r6) goto L_0x0056
            return
        L_0x0030:
            r4 = -1
            if (r5 != r4) goto L_0x0034
            goto L_0x0056
        L_0x0034:
            if (r5 != 0) goto L_0x0055
            r4 = 13
            if (r6 == 0) goto L_0x0040
            java.lang.String r5 = "<<ResolutionFailureErrorDetail>>"
            int r4 = r6.getIntExtra(r5, r4)
        L_0x0040:
            com.google.android.gms.common.api.internal.h2 r5 = new com.google.android.gms.common.api.internal.h2
            com.google.android.gms.common.ConnectionResult r6 = new com.google.android.gms.common.ConnectionResult
            r1 = 0
            r6.<init>(r4, r1)
            int r4 = m4897a(r0)
            r5.<init>(r6, r4)
            java.util.concurrent.atomic.AtomicReference<com.google.android.gms.common.api.internal.h2> r4 = r3.f3330R
            r4.set(r5)
            r0 = r5
        L_0x0055:
            r1 = 0
        L_0x0056:
            if (r1 == 0) goto L_0x005c
            r3.mo16702g()
            return
        L_0x005c:
            if (r0 == 0) goto L_0x0069
            com.google.android.gms.common.ConnectionResult r4 = r0.mo16717a()
            int r5 = r0.mo16718b()
            r3.mo16690a(r4, r5)
        L_0x0069:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.C2079g2.mo16604a(int, int, android.content.Intent):void");
    }

    /* renamed from: a */
    private static int m4897a(@Nullable C2083h2 h2Var) {
        if (h2Var == null) {
            return -1;
        }
        return h2Var.mo16718b();
    }
}
