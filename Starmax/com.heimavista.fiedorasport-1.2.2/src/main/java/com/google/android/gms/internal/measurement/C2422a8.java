package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.a8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2422a8 implements C2593l2<C2470d8> {

    /* renamed from: Q */
    private static C2422a8 f3980Q = new C2422a8();

    /* renamed from: P */
    private final C2593l2<C2470d8> f3981P;

    private C2422a8(C2593l2<C2470d8> l2Var) {
        this.f3981P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6012b() {
        return ((C2470d8) f3980Q.mo17285a()).mo17383a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f3981P.mo17285a();
    }

    public C2422a8() {
        this(C2579k2.m6601a(new C2454c8()));
    }
}
