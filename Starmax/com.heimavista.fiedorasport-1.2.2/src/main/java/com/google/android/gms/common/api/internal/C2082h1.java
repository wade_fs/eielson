package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: com.google.android.gms.common.api.internal.h1 */
public interface C2082h1 {
    /* renamed from: a */
    <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16708a(@NonNull C2056c cVar);

    /* renamed from: a */
    void mo16709a();

    /* renamed from: a */
    void mo16710a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    /* renamed from: a */
    boolean mo16711a(C2099l lVar);

    /* renamed from: b */
    void mo16712b();

    /* renamed from: c */
    boolean mo16713c();

    /* renamed from: d */
    void mo16714d();

    /* renamed from: e */
    void mo16715e();

    /* renamed from: f */
    ConnectionResult mo16716f();
}
