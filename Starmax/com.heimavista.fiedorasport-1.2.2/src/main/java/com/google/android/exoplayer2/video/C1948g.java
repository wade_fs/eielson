package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.g */
/* compiled from: lambda */
public final /* synthetic */ class C1948g implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2963P;

    /* renamed from: Q */
    private final /* synthetic */ int f2964Q;

    /* renamed from: R */
    private final /* synthetic */ long f2965R;

    public /* synthetic */ C1948g(VideoRendererEventListener.EventDispatcher eventDispatcher, int i, long j) {
        this.f2963P = eventDispatcher;
        this.f2964Q = i;
        this.f2965R = j;
    }

    public final void run() {
        this.f2963P.mo16266a(this.f2964Q, this.f2965R);
    }
}
