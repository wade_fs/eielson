package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2016a;

/* renamed from: com.google.android.gms.common.api.internal.k1 */
public final class C2097k1 {

    /* renamed from: a */
    public final C2095k<C2016a.C2018b, ?> f3357a;

    /* renamed from: b */
    public final C2115p<C2016a.C2018b, ?> f3358b;

    public C2097k1(@NonNull C2095k<C2016a.C2018b, ?> kVar, @NonNull C2115p<C2016a.C2018b, ?> pVar) {
        this.f3357a = kVar;
        this.f3358b = pVar;
    }
}
