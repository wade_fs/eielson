package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.C2197d;

/* renamed from: com.google.android.gms.common.api.internal.e0 */
final class C2068e0 extends C2140v0 {

    /* renamed from: b */
    private final /* synthetic */ C2197d.C2200c f3306b;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2068e0(C2057c0 c0Var, C2132t0 t0Var, C2197d.C2200c cVar) {
        super(t0Var);
        this.f3306b = cVar;
    }

    /* renamed from: a */
    public final void mo16645a() {
        this.f3306b.mo16632a(new ConnectionResult(16, null));
    }
}
