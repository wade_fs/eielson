package com.google.android.datatransport.cct.p084b;

import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1684m;

/* renamed from: com.google.android.datatransport.cct.b.f */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1669f extends C1684m {

    /* renamed from: a */
    private final C1684m.C1686b f2591a;

    /* renamed from: b */
    private final C1661a f2592b;

    /* renamed from: com.google.android.datatransport.cct.b.f$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1671b extends C1684m.C1685a {

        /* renamed from: a */
        private C1684m.C1686b f2593a;

        /* renamed from: b */
        private C1661a f2594b;

        C1671b() {
        }

        /* renamed from: a */
        public C1684m.C1685a mo13475a(@Nullable C1684m.C1686b bVar) {
            this.f2593a = bVar;
            return super;
        }

        /* renamed from: a */
        public C1684m.C1685a mo13474a(@Nullable C1661a aVar) {
            this.f2594b = aVar;
            return super;
        }

        /* renamed from: a */
        public C1684m mo13476a() {
            return new C1669f(this.f2593a, this.f2594b, null);
        }
    }

    /* synthetic */ C1669f(C1684m.C1686b bVar, C1661a aVar, C1670a aVar2) {
        this.f2591a = bVar;
        this.f2592b = aVar;
    }

    @Nullable
    /* renamed from: b */
    public C1661a mo13469b() {
        return this.f2592b;
    }

    @Nullable
    /* renamed from: c */
    public C1684m.C1686b mo13470c() {
        return this.f2591a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1684m)) {
            return false;
        }
        C1684m.C1686b bVar = this.f2591a;
        if (bVar != null ? bVar.equals(((C1669f) obj).f2591a) : ((C1669f) obj).f2591a == null) {
            C1661a aVar = this.f2592b;
            if (aVar == null) {
                if (((C1669f) obj).f2592b == null) {
                    return true;
                }
            } else if (aVar.equals(((C1669f) obj).f2592b)) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        C1684m.C1686b bVar = this.f2591a;
        int i = 0;
        int hashCode = ((bVar == null ? 0 : bVar.hashCode()) ^ 1000003) * 1000003;
        C1661a aVar = this.f2592b;
        if (aVar != null) {
            i = aVar.hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        return "ClientInfo{clientType=" + this.f2591a + ", androidClientInfo=" + this.f2592b + "}";
    }
}
