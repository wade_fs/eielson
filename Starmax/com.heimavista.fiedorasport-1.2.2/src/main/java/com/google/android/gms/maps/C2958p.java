package com.google.android.gms.maps;

import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.p093i.C2891b;
import com.google.android.gms.maps.p093i.C2912q;

/* renamed from: com.google.android.gms.maps.p */
final class C2958p extends C2912q {

    /* renamed from: a */
    private final /* synthetic */ C2885e f4917a;

    C2958p(SupportMapFragment.C2874a aVar, C2885e eVar) {
        this.f4917a = eVar;
    }

    /* renamed from: a */
    public final void mo18487a(C2891b bVar) {
        this.f4917a.mo18411a(new C2880c(bVar));
    }
}
