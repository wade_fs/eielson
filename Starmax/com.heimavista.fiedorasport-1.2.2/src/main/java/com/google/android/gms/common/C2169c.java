package com.google.android.gms.common;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.text.TextUtils;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2247s0;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2318i;

/* renamed from: com.google.android.gms.common.c */
public class C2169c {

    /* renamed from: a */
    public static final int f3548a = C2176f.f3566a;

    /* renamed from: b */
    private static final C2169c f3549b = new C2169c();

    C2169c() {
    }

    /* renamed from: a */
    public static C2169c m5270a() {
        return f3549b;
    }

    /* renamed from: b */
    public int mo16840b(Context context) {
        return C2176f.m5299b(context);
    }

    /* renamed from: c */
    public int mo16831c(Context context) {
        return mo16820a(context, f3548a);
    }

    /* renamed from: a */
    public int mo16820a(Context context, int i) {
        int a = C2176f.m5293a(context, i);
        if (C2176f.m5303c(context, a)) {
            return 18;
        }
        return a;
    }

    /* renamed from: b */
    public boolean mo16841b(Context context, int i) {
        return C2176f.m5303c(context, i);
    }

    /* renamed from: c */
    public boolean mo16833c(int i) {
        return C2176f.m5300b(i);
    }

    /* renamed from: b */
    public String mo16829b(int i) {
        return C2176f.m5295a(i);
    }

    /* renamed from: b */
    private static String m5271b(@Nullable Context context, @Nullable String str) {
        StringBuilder sb = new StringBuilder();
        sb.append("gcore_");
        sb.append(f3548a);
        sb.append("-");
        if (!TextUtils.isEmpty(str)) {
            sb.append(str);
        }
        sb.append("-");
        if (context != null) {
            sb.append(context.getPackageName());
        }
        sb.append("-");
        if (context != null) {
            try {
                sb.append(C2283c.m5685a(context).mo17060b(context.getPackageName(), 0).versionCode);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return sb.toString();
    }

    @Deprecated
    @Nullable
    /* renamed from: a */
    public Intent mo16837a(int i) {
        return mo16825a((Context) null, i, (String) null);
    }

    @Nullable
    /* renamed from: a */
    public Intent mo16825a(Context context, int i, @Nullable String str) {
        if (i == 1 || i == 2) {
            if (context == null || !C2318i.m5786c(context)) {
                return C2247s0.m5544a("com.google.android.gms", m5271b(context, str));
            }
            return C2247s0.m5542a();
        } else if (i != 3) {
            return null;
        } else {
            return C2247s0.m5543a("com.google.android.gms");
        }
    }

    @Nullable
    /* renamed from: a */
    public PendingIntent mo16823a(Context context, int i, int i2) {
        return mo16836a(context, i, i2, null);
    }

    @Nullable
    /* renamed from: a */
    public PendingIntent mo16836a(Context context, int i, int i2, @Nullable String str) {
        Intent a = mo16825a(context, i, str);
        if (a == null) {
            return null;
        }
        return PendingIntent.getActivity(context, i2, a, 134217728);
    }

    /* renamed from: a */
    public void mo16838a(Context context) {
        C2176f.m5296a(context);
    }

    /* renamed from: a */
    public boolean mo16839a(Context context, String str) {
        return C2176f.m5298a(context, str);
    }
}
