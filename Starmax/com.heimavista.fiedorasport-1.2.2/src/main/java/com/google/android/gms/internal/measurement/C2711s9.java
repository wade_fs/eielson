package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.s9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2711s9 implements C2593l2<C2758v9> {

    /* renamed from: Q */
    private static C2711s9 f4482Q = new C2711s9();

    /* renamed from: P */
    private final C2593l2<C2758v9> f4483P;

    private C2711s9(C2593l2<C2758v9> l2Var) {
        this.f4483P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7199b() {
        return ((C2758v9) f4482Q.mo17285a()).mo17944a();
    }

    /* renamed from: c */
    public static boolean m7200c() {
        return ((C2758v9) f4482Q.mo17285a()).mo17945e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4483P.mo17285a();
    }

    public C2711s9() {
        this(C2579k2.m6601a(new C2743u9()));
    }
}
