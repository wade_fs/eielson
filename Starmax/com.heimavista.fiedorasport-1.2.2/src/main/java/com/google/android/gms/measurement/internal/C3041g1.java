package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2556ic;

/* renamed from: com.google.android.gms.measurement.internal.g1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3041g1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5147a = new C3041g1();

    private C3041g1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2556ic.m6492c());
    }
}
