package com.google.android.gms.common.api.internal;

/* renamed from: com.google.android.gms.common.api.internal.o2 */
final class C2114o2 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2110n2 f3416P;

    C2114o2(C2110n2 n2Var) {
        this.f3416P = n2Var;
    }

    public final void run() {
        this.f3416P.f3412m.lock();
        try {
            this.f3416P.m5026i();
        } finally {
            this.f3416P.f3412m.unlock();
        }
    }
}
