package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2494f0;

/* renamed from: com.google.android.gms.measurement.internal.ia */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3074ia extends C3086ja {

    /* renamed from: g */
    private C2494f0 f5222g;

    /* renamed from: h */
    private final /* synthetic */ C3002ca f5223h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3074ia(C3002ca caVar, String str, int i, C2494f0 f0Var) {
        super(str, i);
        this.f5223h = caVar;
        this.f5222g = f0Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final int mo19038a() {
        return this.f5222g.mo17459o();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo19040b() {
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final boolean mo19041c() {
        return false;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r7v17, types: [java.lang.Integer] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean mo19070a(java.lang.Long r14, java.lang.Long r15, com.google.android.gms.internal.measurement.C2414a1 r16, boolean r17) {
        /*
            r13 = this;
            r0 = r13
            com.google.android.gms.measurement.internal.ca r1 = r0.f5223h
            com.google.android.gms.measurement.internal.la r1 = r1.mo19013h()
            java.lang.String r2 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5457l0
            boolean r1 = r1.mo19152d(r2, r3)
            com.google.android.gms.measurement.internal.ca r2 = r0.f5223h
            com.google.android.gms.measurement.internal.la r2 = r2.mo19013h()
            java.lang.String r3 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5469r0
            boolean r2 = r2.mo19152d(r3, r4)
            boolean r3 = com.google.android.gms.internal.measurement.C2606l9.m6736b()
            r4 = 0
            r5 = 1
            if (r3 == 0) goto L_0x0037
            com.google.android.gms.measurement.internal.ca r3 = r0.f5223h
            com.google.android.gms.measurement.internal.la r3 = r3.mo19013h()
            java.lang.String r6 = r0.f5272a
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r7 = com.google.android.gms.measurement.internal.C3135o.f5475u0
            boolean r3 = r3.mo19152d(r6, r7)
            if (r3 == 0) goto L_0x0037
            r3 = 1
            goto L_0x0038
        L_0x0037:
            r3 = 0
        L_0x0038:
            com.google.android.gms.internal.measurement.f0 r6 = r0.f5222g
            boolean r6 = r6.mo17462s()
            com.google.android.gms.internal.measurement.f0 r7 = r0.f5222g
            boolean r7 = r7.mo17463t()
            if (r1 == 0) goto L_0x0050
            com.google.android.gms.internal.measurement.f0 r8 = r0.f5222g
            boolean r8 = r8.mo17465v()
            if (r8 == 0) goto L_0x0050
            r8 = 1
            goto L_0x0051
        L_0x0050:
            r8 = 0
        L_0x0051:
            if (r6 != 0) goto L_0x005a
            if (r7 != 0) goto L_0x005a
            if (r8 == 0) goto L_0x0058
            goto L_0x005a
        L_0x0058:
            r6 = 0
            goto L_0x005b
        L_0x005a:
            r6 = 1
        L_0x005b:
            r7 = 0
            if (r17 == 0) goto L_0x0088
            if (r6 != 0) goto L_0x0088
            com.google.android.gms.measurement.internal.ca r1 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r1 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo18996B()
            int r2 = r0.f5273b
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            com.google.android.gms.internal.measurement.f0 r3 = r0.f5222g
            boolean r3 = r3.mo17458n()
            if (r3 == 0) goto L_0x0082
            com.google.android.gms.internal.measurement.f0 r3 = r0.f5222g
            int r3 = r3.mo17459o()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r3)
        L_0x0082:
            java.lang.String r3 = "Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            r1.mo19044a(r3, r2, r7)
            return r5
        L_0x0088:
            com.google.android.gms.internal.measurement.f0 r9 = r0.f5222g
            com.google.android.gms.internal.measurement.d0 r9 = r9.mo17461q()
            boolean r10 = r9.mo17393t()
            boolean r11 = r16.mo17253t()
            if (r11 == 0) goto L_0x00cf
            boolean r11 = r9.mo17390p()
            if (r11 != 0) goto L_0x00bd
            com.google.android.gms.measurement.internal.ca r9 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()
            com.google.android.gms.measurement.internal.ca r10 = r0.f5223h
            com.google.android.gms.measurement.internal.c4 r10 = r10.mo19010e()
            java.lang.String r11 = r16.mo17250p()
            java.lang.String r10 = r10.mo18824c(r11)
            java.lang.String r11 = "No number filter for long property. property"
            r9.mo19043a(r11, r10)
            goto L_0x01ad
        L_0x00bd:
            long r11 = r16.mo17254u()
            com.google.android.gms.internal.measurement.e0 r7 = r9.mo17391q()
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8815a(r11, r7)
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8816a(r7, r10)
            goto L_0x01ad
        L_0x00cf:
            boolean r11 = r16.mo17255v()
            if (r11 == 0) goto L_0x010c
            boolean r11 = r9.mo17390p()
            if (r11 != 0) goto L_0x00fa
            com.google.android.gms.measurement.internal.ca r9 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()
            com.google.android.gms.measurement.internal.ca r10 = r0.f5223h
            com.google.android.gms.measurement.internal.c4 r10 = r10.mo19010e()
            java.lang.String r11 = r16.mo17250p()
            java.lang.String r10 = r10.mo18824c(r11)
            java.lang.String r11 = "No number filter for double property. property"
            r9.mo19043a(r11, r10)
            goto L_0x01ad
        L_0x00fa:
            double r11 = r16.mo17256w()
            com.google.android.gms.internal.measurement.e0 r7 = r9.mo17391q()
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8814a(r11, r7)
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8816a(r7, r10)
            goto L_0x01ad
        L_0x010c:
            boolean r11 = r16.mo17251q()
            if (r11 == 0) goto L_0x0190
            boolean r11 = r9.mo17388n()
            if (r11 != 0) goto L_0x0179
            boolean r11 = r9.mo17390p()
            if (r11 != 0) goto L_0x013c
            com.google.android.gms.measurement.internal.ca r9 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()
            com.google.android.gms.measurement.internal.ca r10 = r0.f5223h
            com.google.android.gms.measurement.internal.c4 r10 = r10.mo19010e()
            java.lang.String r11 = r16.mo17250p()
            java.lang.String r10 = r10.mo18824c(r11)
            java.lang.String r11 = "No string or number filter defined. property"
            r9.mo19043a(r11, r10)
            goto L_0x01ad
        L_0x013c:
            java.lang.String r11 = r16.mo17252s()
            boolean r11 = com.google.android.gms.measurement.internal.C3223v9.m9272a(r11)
            if (r11 == 0) goto L_0x0157
            java.lang.String r7 = r16.mo17252s()
            com.google.android.gms.internal.measurement.e0 r9 = r9.mo17391q()
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8817a(r7, r9)
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8816a(r7, r10)
            goto L_0x01ad
        L_0x0157:
            com.google.android.gms.measurement.internal.ca r9 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()
            com.google.android.gms.measurement.internal.ca r10 = r0.f5223h
            com.google.android.gms.measurement.internal.c4 r10 = r10.mo19010e()
            java.lang.String r11 = r16.mo17250p()
            java.lang.String r10 = r10.mo18824c(r11)
            java.lang.String r11 = r16.mo17252s()
            java.lang.String r12 = "Invalid user property value for Numeric number filter. property, value"
            r9.mo19044a(r12, r10, r11)
            goto L_0x01ad
        L_0x0179:
            java.lang.String r7 = r16.mo17252s()
            com.google.android.gms.internal.measurement.g0 r9 = r9.mo17389o()
            com.google.android.gms.measurement.internal.ca r11 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r11 = r11.mo19015l()
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8819a(r7, r9, r11)
            java.lang.Boolean r7 = com.google.android.gms.measurement.internal.C3086ja.m8816a(r7, r10)
            goto L_0x01ad
        L_0x0190:
            com.google.android.gms.measurement.internal.ca r9 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo19004w()
            com.google.android.gms.measurement.internal.ca r10 = r0.f5223h
            com.google.android.gms.measurement.internal.c4 r10 = r10.mo19010e()
            java.lang.String r11 = r16.mo17250p()
            java.lang.String r10 = r10.mo18824c(r11)
            java.lang.String r11 = "User property has no value, property"
            r9.mo19043a(r11, r10)
        L_0x01ad:
            com.google.android.gms.measurement.internal.ca r9 = r0.f5223h
            com.google.android.gms.measurement.internal.f4 r9 = r9.mo19015l()
            com.google.android.gms.measurement.internal.h4 r9 = r9.mo18996B()
            if (r7 != 0) goto L_0x01bc
            java.lang.String r10 = "null"
            goto L_0x01bd
        L_0x01bc:
            r10 = r7
        L_0x01bd:
            java.lang.String r11 = "Property filter result"
            r9.mo19043a(r11, r10)
            if (r7 != 0) goto L_0x01c5
            return r4
        L_0x01c5:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r5)
            r0.f5274c = r4
            if (r1 == 0) goto L_0x01d6
            if (r8 == 0) goto L_0x01d6
            boolean r1 = r7.booleanValue()
            if (r1 != 0) goto L_0x01d6
            return r5
        L_0x01d6:
            if (r17 == 0) goto L_0x01e0
            com.google.android.gms.internal.measurement.f0 r1 = r0.f5222g
            boolean r1 = r1.mo17462s()
            if (r1 == 0) goto L_0x01e2
        L_0x01e0:
            r0.f5275d = r7
        L_0x01e2:
            boolean r1 = r7.booleanValue()
            if (r1 == 0) goto L_0x0229
            if (r6 == 0) goto L_0x0229
            boolean r1 = r16.mo17248n()
            if (r1 == 0) goto L_0x0229
            long r6 = r16.mo17249o()
            if (r2 == 0) goto L_0x01fc
            if (r14 == 0) goto L_0x01fc
            long r6 = r14.longValue()
        L_0x01fc:
            if (r3 == 0) goto L_0x0214
            com.google.android.gms.internal.measurement.f0 r1 = r0.f5222g
            boolean r1 = r1.mo17462s()
            if (r1 == 0) goto L_0x0214
            com.google.android.gms.internal.measurement.f0 r1 = r0.f5222g
            boolean r1 = r1.mo17463t()
            if (r1 != 0) goto L_0x0214
            if (r15 == 0) goto L_0x0214
            long r6 = r15.longValue()
        L_0x0214:
            com.google.android.gms.internal.measurement.f0 r1 = r0.f5222g
            boolean r1 = r1.mo17463t()
            if (r1 == 0) goto L_0x0223
            java.lang.Long r1 = java.lang.Long.valueOf(r6)
            r0.f5277f = r1
            goto L_0x0229
        L_0x0223:
            java.lang.Long r1 = java.lang.Long.valueOf(r6)
            r0.f5276e = r1
        L_0x0229:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3074ia.mo19070a(java.lang.Long, java.lang.Long, com.google.android.gms.internal.measurement.a1, boolean):boolean");
    }
}
