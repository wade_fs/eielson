package com.google.android.gms.common.api.internal;

import androidx.annotation.WorkerThread;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;

/* renamed from: com.google.android.gms.common.api.internal.t1 */
final class C2133t1 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2157k f3465P;

    /* renamed from: Q */
    private final /* synthetic */ C2129s1 f3466Q;

    C2133t1(C2129s1 s1Var, C2157k kVar) {
        this.f3466Q = s1Var;
        this.f3465P = kVar;
    }

    @WorkerThread
    public final void run() {
        try {
            BasePendingResult.f3222p.set(true);
            this.f3466Q.f3444g.sendMessage(this.f3466Q.f3444g.obtainMessage(0, this.f3466Q.f3438a.mo16813a(this.f3465P)));
            BasePendingResult.f3222p.set(false);
            C2129s1.m5079b(this.f3465P);
            C2036f fVar = (C2036f) this.f3466Q.f3443f.get();
            if (fVar != null) {
                fVar.mo16566a(this.f3466Q);
            }
        } catch (RuntimeException e) {
            this.f3466Q.f3444g.sendMessage(this.f3466Q.f3444g.obtainMessage(1, e));
            BasePendingResult.f3222p.set(false);
            C2129s1.m5079b(this.f3465P);
            C2036f fVar2 = (C2036f) this.f3466Q.f3443f.get();
            if (fVar2 != null) {
                fVar2.mo16566a(this.f3466Q);
            }
        } catch (Throwable th) {
            BasePendingResult.f3222p.set(false);
            C2129s1.m5079b(this.f3465P);
            C2036f fVar3 = (C2036f) this.f3466Q.f3443f.get();
            if (fVar3 != null) {
                fVar3.mo16566a(this.f3466Q);
            }
            throw th;
        }
    }
}
