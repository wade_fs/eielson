package com.google.android.gms.measurement.internal;

import android.content.ContentValues;
import android.database.sqlite.SQLiteException;
import android.text.TextUtils;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import com.facebook.appevents.AppEventsConstants;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.C2595l4;
import com.google.android.gms.internal.measurement.C2611m0;
import com.google.android.gms.internal.measurement.C2626n0;
import com.google.android.gms.internal.measurement.C2641o0;
import com.google.android.gms.internal.measurement.C2723t4;
import java.util.ArrayList;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.d5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C3009d5 extends C3157p9 implements C3134na {

    /* renamed from: j */
    private static int f5041j = 65535;

    /* renamed from: k */
    private static int f5042k = 2;

    /* renamed from: d */
    private final Map<String, Map<String, String>> f5043d = new ArrayMap();

    /* renamed from: e */
    private final Map<String, Map<String, Boolean>> f5044e = new ArrayMap();

    /* renamed from: f */
    private final Map<String, Map<String, Boolean>> f5045f = new ArrayMap();

    /* renamed from: g */
    private final Map<String, C2626n0> f5046g = new ArrayMap();

    /* renamed from: h */
    private final Map<String, Map<String, Integer>> f5047h = new ArrayMap();

    /* renamed from: i */
    private final Map<String, String> f5048i = new ArrayMap();

    C3009d5(C3145o9 o9Var) {
        super(o9Var);
    }

    @WorkerThread
    /* renamed from: i */
    private final void m8518i(String str) {
        mo19284q();
        mo18881c();
        C2258v.m5639b(str);
        if (this.f5046g.get(str) == null) {
            byte[] d = mo19172k().mo18865d(str);
            if (d == null) {
                this.f5043d.put(str, null);
                this.f5044e.put(str, null);
                this.f5045f.put(str, null);
                this.f5046g.put(str, null);
                this.f5048i.put(str, null);
                this.f5047h.put(str, null);
                return;
            }
            C2626n0.C2627a aVar = (C2626n0.C2627a) m8515a(str, d).mo17669j();
            m8517a(str, aVar);
            this.f5043d.put(str, m8516a((C2626n0) aVar.mo17679i()));
            this.f5046g.put(str, (C2626n0) aVar.mo17679i());
            this.f5048i.put(str, null);
        }
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final C2626n0 mo18890a(String str) {
        mo19284q();
        mo18881c();
        C2258v.m5639b(str);
        m8518i(str);
        return this.f5046g.get(str);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: b */
    public final String mo18892b(String str) {
        mo18881c();
        return this.f5048i.get(str);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: c */
    public final void mo18894c(String str) {
        mo18881c();
        this.f5048i.put(str, null);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: d */
    public final void mo18897d(String str) {
        mo18881c();
        this.f5046g.remove(str);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: e */
    public final boolean mo18898e(String str) {
        mo18881c();
        C2626n0 a = mo18890a(str);
        if (a == null) {
            return false;
        }
        return a.mo17768v();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: f */
    public final long mo18899f(String str) {
        String a = mo18798a(str, "measurement.account.time_zone_offset_minutes");
        if (TextUtils.isEmpty(a)) {
            return 0;
        }
        try {
            return Long.parseLong(a);
        } catch (NumberFormatException e) {
            mo19015l().mo19004w().mo19044a("Unable to parse timezone offset. appId", C3032f4.m8621a(str), e);
            return 0;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public final boolean mo18900g(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(mo18798a(str, "measurement.upload.blacklist_internal"));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public final boolean mo18901h(String str) {
        return AppEventsConstants.EVENT_PARAM_VALUE_YES.equals(mo18798a(str, "measurement.upload.blacklist_public"));
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final boolean mo18833t() {
        return false;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final boolean mo18893b(String str, String str2) {
        Boolean bool;
        mo18881c();
        m8518i(str);
        if (mo18900g(str) && C3267z9.m9430f(str2)) {
            return true;
        }
        if (mo18901h(str) && C3267z9.m9429e(str2)) {
            return true;
        }
        Map map = this.f5044e.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: c */
    public final boolean mo18895c(String str, String str2) {
        Boolean bool;
        mo18881c();
        m8518i(str);
        if ("ecommerce_purchase".equals(str2)) {
            return true;
        }
        Map map = this.f5045f.get(str);
        if (map == null || (bool = (Boolean) map.get(str2)) == null) {
            return false;
        }
        return bool.booleanValue();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: d */
    public final int mo18896d(String str, String str2) {
        Integer num;
        mo18881c();
        m8518i(str);
        Map map = this.f5047h.get(str);
        if (map == null || (num = (Integer) map.get(str2)) == null) {
            return 1;
        }
        return num.intValue();
    }

    @WorkerThread
    /* renamed from: a */
    public final String mo18798a(String str, String str2) {
        mo18881c();
        m8518i(str);
        Map map = this.f5043d.get(str);
        if (map != null) {
            return (String) map.get(str2);
        }
        return null;
    }

    /* renamed from: a */
    private static Map<String, String> m8516a(C2626n0 n0Var) {
        ArrayMap arrayMap = new ArrayMap();
        if (n0Var != null) {
            for (C2641o0 o0Var : n0Var.mo17765s()) {
                arrayMap.put(o0Var.mo17785n(), o0Var.mo17786o());
            }
        }
        return arrayMap;
    }

    /* renamed from: a */
    private final void m8517a(String str, C2626n0.C2627a aVar) {
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        ArrayMap arrayMap3 = new ArrayMap();
        if (aVar != null) {
            for (int i = 0; i < aVar.mo17771j(); i++) {
                C2611m0.C2612a aVar2 = (C2611m0.C2612a) aVar.mo17769a(i).mo17669j();
                if (TextUtils.isEmpty(aVar2.mo17725j())) {
                    mo19015l().mo19004w().mo19042a("EventConfig contained null event name");
                } else {
                    String a = C3082j6.m8797a(aVar2.mo17725j());
                    if (!TextUtils.isEmpty(a)) {
                        aVar2.mo17724a(a);
                        aVar.mo17770a(i, aVar2);
                    }
                    arrayMap.put(aVar2.mo17725j(), Boolean.valueOf(aVar2.mo17726k()));
                    arrayMap2.put(aVar2.mo17725j(), Boolean.valueOf(aVar2.mo17727l()));
                    if (aVar2.mo17728m()) {
                        if (aVar2.mo17729n() < f5042k || aVar2.mo17729n() > f5041j) {
                            mo19015l().mo19004w().mo19044a("Invalid sampling rate. Event name, sample rate", aVar2.mo17725j(), Integer.valueOf(aVar2.mo17729n()));
                        } else {
                            arrayMap3.put(aVar2.mo17725j(), Integer.valueOf(aVar2.mo17729n()));
                        }
                    }
                }
            }
        }
        this.f5044e.put(str, arrayMap);
        this.f5045f.put(str, arrayMap2);
        this.f5047h.put(str, arrayMap3);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final boolean mo18891a(String str, byte[] bArr, String str2) {
        mo19284q();
        mo18881c();
        C2258v.m5639b(str);
        C2626n0.C2627a aVar = (C2626n0.C2627a) m8515a(str, bArr).mo17669j();
        if (aVar == null) {
            return false;
        }
        m8517a(str, aVar);
        this.f5046g.put(str, (C2626n0) aVar.mo17679i());
        this.f5048i.put(str, str2);
        this.f5043d.put(str, m8516a((C2626n0) aVar.mo17679i()));
        mo19172k().mo18861b(str, new ArrayList(aVar.mo17772k()));
        try {
            aVar.mo17773l();
            bArr = ((C2626n0) ((C2595l4) aVar.mo17679i())).mo18129f();
        } catch (RuntimeException e) {
            mo19015l().mo19004w().mo19044a("Unable to serialize reduced-size config. Storing full config instead. appId", C3032f4.m8621a(str), e);
        }
        C3003d k = mo19172k();
        C2258v.m5639b(str);
        k.mo18881c();
        super.mo19284q();
        ContentValues contentValues = new ContentValues();
        contentValues.put("remote_config", bArr);
        try {
            if (((long) k.mo18875v().update("apps", contentValues, "app_id = ?", new String[]{str})) == 0) {
                k.mo19015l().mo19001t().mo19043a("Failed to update remote config (got 0). appId", C3032f4.m8621a(str));
            }
        } catch (SQLiteException e2) {
            k.mo19015l().mo19001t().mo19044a("Error storing remote config. appId", C3032f4.m8621a(str), e2);
        }
        this.f5046g.put(str, (C2626n0) aVar.mo17679i());
        return true;
    }

    @WorkerThread
    /* renamed from: a */
    private final C2626n0 m8515a(String str, byte[] bArr) {
        if (bArr == null) {
            return C2626n0.m6845x();
        }
        try {
            C2626n0.C2627a w = C2626n0.m6844w();
            C3223v9.m9262a(w, bArr);
            C2626n0 n0Var = (C2626n0) ((C2595l4) w.mo17679i());
            C3056h4 B = mo19015l().mo18996B();
            String str2 = null;
            Long valueOf = n0Var.mo17761n() ? Long.valueOf(n0Var.mo17762o()) : null;
            if (n0Var.mo17763p()) {
                str2 = n0Var.mo17764q();
            }
            B.mo19044a("Parsed config. version, gmp_app_id", valueOf, str2);
            return n0Var;
        } catch (C2723t4 e) {
            mo19015l().mo19004w().mo19044a("Unable to merge remote config. appId", C3032f4.m8621a(str), e);
            return C2626n0.m6845x();
        } catch (RuntimeException e2) {
            mo19015l().mo19004w().mo19044a("Unable to merge remote config. appId", C3032f4.m8621a(str), e2);
            return C2626n0.m6845x();
        }
    }
}
