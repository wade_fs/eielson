package com.google.android.gms.internal.measurement;

import android.os.Binder;

/* renamed from: com.google.android.gms.internal.measurement.l1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final /* synthetic */ class C2592l1 {
    /* renamed from: a */
    public static <V> V m6637a(C2643o1<V> o1Var) {
        long clearCallingIdentity;
        try {
            return o1Var.mo17495a();
        } catch (SecurityException unused) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            V a = o1Var.mo17495a();
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return a;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }
}
