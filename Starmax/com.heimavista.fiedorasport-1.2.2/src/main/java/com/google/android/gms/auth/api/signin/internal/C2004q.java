package com.google.android.gms.auth.api.signin.internal;

import android.os.IInterface;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.internal.q */
public interface C2004q extends IInterface {
    /* renamed from: a */
    void mo16444a(GoogleSignInAccount googleSignInAccount, Status status);

    /* renamed from: a */
    void mo16445a(Status status);

    /* renamed from: b */
    void mo16446b(Status status);
}
