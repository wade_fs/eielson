package com.google.android.gms.signin.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.internal.C1989b;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2219h;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ResolveAccountRequest;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.signin.internal.a */
public class C3268a extends C2219h<C3273f> implements C4052e {

    /* renamed from: E */
    private final boolean f5848E;

    /* renamed from: F */
    private final C2211e f5849F;

    /* renamed from: G */
    private final Bundle f5850G;

    /* renamed from: H */
    private Integer f5851H;

    private C3268a(Context context, Looper looper, boolean z, C2211e eVar, Bundle bundle, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        super(context, looper, 44, eVar, bVar, cVar);
        this.f5848E = true;
        this.f5849F = eVar;
        this.f5850G = bundle;
        this.f5851H = eVar.mo16962e();
    }

    /* renamed from: a */
    public final void mo19473a(C2231m mVar, boolean z) {
        try {
            ((C3273f) mo16939x()).mo19484a(mVar, this.f5851H.intValue(), z);
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when saveDefaultAccount is called");
        }
    }

    /* renamed from: b */
    public final void mo19475b() {
        mo16916a(new C2197d.C2201d());
    }

    /* renamed from: g */
    public final void mo19476g() {
        try {
            ((C3273f) mo16939x()).mo19483J(this.f5851H.intValue());
        } catch (RemoteException unused) {
            Log.w("SignInClientImpl", "Remote service probably died when clearAccountFromSessionStore is called");
        }
    }

    /* renamed from: i */
    public int mo16451i() {
        return C2176f.f3566a;
    }

    /* renamed from: l */
    public boolean mo16927l() {
        return this.f5848E;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public Bundle mo16936u() {
        if (!mo16935t().getPackageName().equals(this.f5849F.mo16965h())) {
            this.f5850G.putString("com.google.android.gms.signin.internal.realClientPackageName", this.f5849F.mo16965h());
        }
        return this.f5850G;
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public String mo16453y() {
        return "com.google.android.gms.signin.internal.ISignInService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: z */
    public String mo16454z() {
        return "com.google.android.gms.signin.service.START";
    }

    /* renamed from: a */
    public final void mo19474a(C3271d dVar) {
        C2258v.m5630a(dVar, "Expecting a valid ISignInCallbacks");
        try {
            Account c = this.f5849F.mo16960c();
            GoogleSignInAccount googleSignInAccount = null;
            if ("<<default account>>".equals(c.name)) {
                googleSignInAccount = C1989b.m4567a(mo16935t()).mo16438b();
            }
            ((C3273f) mo16939x()).mo19485a(new zah(new ResolveAccountRequest(c, this.f5851H.intValue(), googleSignInAccount)), dVar);
        } catch (RemoteException e) {
            Log.w("SignInClientImpl", "Remote service probably died when signIn is called");
            try {
                dVar.mo16699a(new zaj(8));
            } catch (RemoteException unused) {
                Log.wtf("SignInClientImpl", "ISignInCallbacks#onSignInComplete should be executed from the same process, unexpected RemoteException.", e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.signin.internal.a.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.e, android.os.Bundle, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void
     arg types: [android.content.Context, android.os.Looper, int, com.google.android.gms.common.internal.e, android.os.Bundle, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c]
     candidates:
      com.google.android.gms.signin.internal.a.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.e, e.d.a.c.d.a, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void
      com.google.android.gms.signin.internal.a.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.e, android.os.Bundle, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void */
    public C3268a(Context context, Looper looper, boolean z, C2211e eVar, C4047a aVar, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        this(context, looper, true, eVar, m9480a(eVar), bVar, cVar);
    }

    /* renamed from: a */
    public static Bundle m9480a(C2211e eVar) {
        C4047a j = eVar.mo16967j();
        Integer e = eVar.mo16962e();
        Bundle bundle = new Bundle();
        bundle.putParcelable("com.google.android.gms.signin.internal.clientRequestedAccount", eVar.mo16956a());
        if (e != null) {
            bundle.putInt("com.google.android.gms.common.internal.ClientSettings.sessionId", e.intValue());
        }
        if (j != null) {
            bundle.putBoolean("com.google.android.gms.signin.internal.offlineAccessRequested", j.mo23688g());
            bundle.putBoolean("com.google.android.gms.signin.internal.idTokenRequested", j.mo23687f());
            bundle.putString("com.google.android.gms.signin.internal.serverClientId", j.mo23685d());
            bundle.putBoolean("com.google.android.gms.signin.internal.usePromptModeForAuthCode", true);
            bundle.putBoolean("com.google.android.gms.signin.internal.forceCodeForRefreshToken", j.mo23686e());
            bundle.putString("com.google.android.gms.signin.internal.hostedDomain", j.mo23683b());
            bundle.putBoolean("com.google.android.gms.signin.internal.waitForAccessTokenRefresh", j.mo23689h());
            if (j.mo23682a() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.authApiSignInModuleVersion", j.mo23682a().longValue());
            }
            if (j.mo23684c() != null) {
                bundle.putLong("com.google.android.gms.signin.internal.realClientLibraryVersion", j.mo23684c().longValue());
            }
        }
        return bundle;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.signin.internal.ISignInService");
        if (queryLocalInterface instanceof C3273f) {
            return (C3273f) queryLocalInterface;
        }
        return new C3274g(iBinder);
    }
}
