package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.net.Uri;
import android.util.Log;
import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;

/* renamed from: com.google.android.gms.internal.measurement.u1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2735u1 {

    /* renamed from: a */
    private static volatile C2497f2<Boolean> f4510a = C2497f2.m6298f();

    /* renamed from: b */
    private static final Object f4511b = new Object();

    /* renamed from: a */
    private static boolean m7367a(Context context) {
        try {
            if ((context.getPackageManager().getApplicationInfo("com.google.android.gms", 0).flags & TsExtractor.TS_STREAM_TYPE_AC3) != 0) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    /* renamed from: a */
    public static boolean m7368a(Context context, Uri uri) {
        boolean z;
        String authority = uri.getAuthority();
        boolean z2 = false;
        if (!"com.google.android.gms.phenotype".equals(authority)) {
            StringBuilder sb = new StringBuilder(String.valueOf(authority).length() + 91);
            sb.append(authority);
            sb.append(" is an unsupported authority. Only com.google.android.gms.phenotype authority is supported.");
            Log.e("PhenotypeClientHelper", sb.toString());
            return false;
        } else if (f4510a.mo17397a()) {
            return f4510a.mo17398e().booleanValue();
        } else {
            synchronized (f4511b) {
                if (f4510a.mo17397a()) {
                    boolean booleanValue = f4510a.mo17398e().booleanValue();
                    return booleanValue;
                }
                if (!"com.google.android.gms".equals(context.getPackageName())) {
                    ProviderInfo resolveContentProvider = context.getPackageManager().resolveContentProvider("com.google.android.gms.phenotype", 0);
                    if (resolveContentProvider == null || !"com.google.android.gms".equals(resolveContentProvider.packageName)) {
                        z = false;
                        if (z && m7367a(context)) {
                            z2 = true;
                        }
                        f4510a = C2497f2.m6297a(Boolean.valueOf(z2));
                        return f4510a.mo17398e().booleanValue();
                    }
                }
                z = true;
                z2 = true;
                f4510a = C2497f2.m6297a(Boolean.valueOf(z2));
                return f4510a.mo17398e().booleanValue();
            }
        }
    }
}
