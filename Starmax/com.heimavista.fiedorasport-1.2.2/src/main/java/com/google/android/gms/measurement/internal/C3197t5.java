package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.t5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3197t5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f5655P;

    /* renamed from: Q */
    private final /* synthetic */ C3141o5 f5656Q;

    C3197t5(C3141o5 o5Var, zzm zzm) {
        this.f5656Q = o5Var;
        this.f5655P = zzm;
    }

    public final void run() {
        this.f5656Q.f5496a.mo19237q();
        this.f5656Q.f5496a.mo19214a(this.f5655P);
    }
}
