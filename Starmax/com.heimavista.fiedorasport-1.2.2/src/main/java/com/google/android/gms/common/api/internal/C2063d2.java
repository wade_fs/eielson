package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2016a.C2020d;
import com.google.android.gms.common.internal.C2251t;

/* renamed from: com.google.android.gms.common.api.internal.d2 */
public final class C2063d2<O extends C2016a.C2020d> {

    /* renamed from: a */
    private final boolean f3264a = true;

    /* renamed from: b */
    private final int f3265b;

    /* renamed from: c */
    private final C2016a<O> f3266c;

    /* renamed from: d */
    private final O f3267d;

    private C2063d2(C2016a<O> aVar, O o) {
        this.f3266c = aVar;
        this.f3267d = o;
        this.f3265b = C2251t.m5615a(this.f3266c, this.f3267d);
    }

    /* renamed from: a */
    public static <O extends C2016a.C2020d> C2063d2<O> m4787a(C2016a<O> aVar, O o) {
        return new C2063d2<>(aVar, o);
    }

    public final boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C2063d2)) {
            return false;
        }
        C2063d2 d2Var = (C2063d2) obj;
        return !this.f3264a && !d2Var.f3264a && C2251t.m5617a(this.f3266c, d2Var.f3266c) && C2251t.m5617a(this.f3267d, d2Var.f3267d);
    }

    public final int hashCode() {
        return this.f3265b;
    }

    /* renamed from: a */
    public static <O extends C2016a.C2020d> C2063d2<O> m4786a(C2016a<O> aVar) {
        return new C2063d2<>(aVar);
    }

    /* renamed from: a */
    public final String mo16647a() {
        return this.f3266c.mo16522b();
    }

    private C2063d2(C2016a<O> aVar) {
        this.f3266c = aVar;
        this.f3267d = null;
        this.f3265b = System.identityHashCode(this);
    }
}
