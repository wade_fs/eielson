package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.Nullable;

/* renamed from: com.google.android.gms.maps.i.a0 */
public final class C2890a0 {
    private C2890a0() {
    }

    /* renamed from: a */
    public static void m8135a(@Nullable Bundle bundle, @Nullable Bundle bundle2) {
        if (bundle != null && bundle2 != null) {
            Parcelable a = m8134a(bundle, "MapOptions");
            if (a != null) {
                m8136a(bundle2, "MapOptions", a);
            }
            Parcelable a2 = m8134a(bundle, "StreetViewPanoramaOptions");
            if (a2 != null) {
                m8136a(bundle2, "StreetViewPanoramaOptions", a2);
            }
            Parcelable a3 = m8134a(bundle, "camera");
            if (a3 != null) {
                m8136a(bundle2, "camera", a3);
            }
            if (bundle.containsKey("position")) {
                bundle2.putString("position", bundle.getString("position"));
            }
            if (bundle.containsKey("com.google.android.wearable.compat.extra.LOWBIT_AMBIENT")) {
                bundle2.putBoolean("com.google.android.wearable.compat.extra.LOWBIT_AMBIENT", bundle.getBoolean("com.google.android.wearable.compat.extra.LOWBIT_AMBIENT", false));
            }
        }
    }

    /* renamed from: a */
    private static <T extends Parcelable> T m8134a(@Nullable Bundle bundle, String str) {
        Class<C2890a0> cls = C2890a0.class;
        if (bundle == null) {
            return null;
        }
        bundle.setClassLoader(cls.getClassLoader());
        Bundle bundle2 = bundle.getBundle("map_state");
        if (bundle2 == null) {
            return null;
        }
        bundle2.setClassLoader(cls.getClassLoader());
        return bundle2.getParcelable(str);
    }

    /* renamed from: a */
    public static void m8136a(Bundle bundle, String str, Parcelable parcelable) {
        Class<C2890a0> cls = C2890a0.class;
        bundle.setClassLoader(cls.getClassLoader());
        Bundle bundle2 = bundle.getBundle("map_state");
        if (bundle2 == null) {
            bundle2 = new Bundle();
        }
        bundle2.setClassLoader(cls.getClassLoader());
        bundle2.putParcelable(str, parcelable);
        bundle.putBundle("map_state", bundle2);
    }
}
