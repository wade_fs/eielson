package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.Arrays;

public final class LocationAvailability extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<LocationAvailability> CREATOR = new C2841i();
    @Deprecated

    /* renamed from: P */
    private int f4660P;
    @Deprecated

    /* renamed from: Q */
    private int f4661Q;

    /* renamed from: R */
    private long f4662R;

    /* renamed from: S */
    private int f4663S;

    /* renamed from: T */
    private zzaj[] f4664T;

    LocationAvailability(int i, int i2, int i3, long j, zzaj[] zzajArr) {
        this.f4663S = i;
        this.f4660P = i2;
        this.f4661Q = i3;
        this.f4662R = j;
        this.f4664T = zzajArr;
    }

    /* renamed from: c */
    public final boolean mo18220c() {
        return this.f4663S < 1000;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && LocationAvailability.class == obj.getClass()) {
            LocationAvailability locationAvailability = (LocationAvailability) obj;
            return this.f4660P == locationAvailability.f4660P && this.f4661Q == locationAvailability.f4661Q && this.f4662R == locationAvailability.f4662R && this.f4663S == locationAvailability.f4663S && Arrays.equals(this.f4664T, locationAvailability.f4664T);
        }
    }

    public final int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4663S), Integer.valueOf(this.f4660P), Integer.valueOf(this.f4661Q), Long.valueOf(this.f4662R), this.f4664T);
    }

    public final String toString() {
        boolean c = mo18220c();
        StringBuilder sb = new StringBuilder(48);
        sb.append("LocationAvailability[isLocationAvailable: ");
        sb.append(c);
        sb.append("]");
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.location.zzaj[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f4660P);
        C2250b.m5591a(parcel, 2, this.f4661Q);
        C2250b.m5592a(parcel, 3, this.f4662R);
        C2250b.m5591a(parcel, 4, this.f4663S);
        C2250b.m5607a(parcel, 5, (Parcelable[]) this.f4664T, i, false);
        C2250b.m5587a(parcel, a);
    }
}
