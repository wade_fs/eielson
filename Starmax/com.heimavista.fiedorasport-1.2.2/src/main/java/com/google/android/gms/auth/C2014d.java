package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.auth.d */
public final class C2014d implements Parcelable.Creator<TokenData> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        String str = null;
        Long l = null;
        ArrayList<String> arrayList = null;
        String str2 = null;
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 1:
                    i = C2248a.m5585z(parcel, a);
                    break;
                case 2:
                    str = C2248a.m5573n(parcel, a);
                    break;
                case 3:
                    l = C2248a.m5547C(parcel, a);
                    break;
                case 4:
                    z = C2248a.m5577r(parcel, a);
                    break;
                case 5:
                    z2 = C2248a.m5577r(parcel, a);
                    break;
                case 6:
                    arrayList = C2248a.m5575p(parcel, a);
                    break;
                case 7:
                    str2 = C2248a.m5573n(parcel, a);
                    break;
                default:
                    C2248a.m5550F(parcel, a);
                    break;
            }
        }
        C2248a.m5576q(parcel, b);
        return new TokenData(i, str, l, z, z2, arrayList, str2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new TokenData[i];
    }
}
