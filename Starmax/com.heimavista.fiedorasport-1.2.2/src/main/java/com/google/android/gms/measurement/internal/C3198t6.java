package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import com.google.android.gms.internal.measurement.C2607la;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.t6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3198t6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ long f5657P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5658Q;

    C3198t6(C3154p6 p6Var, long j) {
        this.f5658Q = p6Var;
        this.f5657P = j;
    }

    public final void run() {
        C3154p6 p6Var = this.f5658Q;
        long j = this.f5657P;
        p6Var.mo18881c();
        p6Var.mo18880a();
        p6Var.mo18816x();
        p6Var.mo19015l().mo18995A().mo19042a("Resetting analytics data (FE)");
        C3244x8 u = p6Var.mo18889u();
        u.mo18881c();
        u.f5762e.mo19033a();
        boolean c = p6Var.f5134a.mo19089c();
        C3185s4 g = p6Var.mo19012g();
        g.f5612j.mo19327a(j);
        if (!TextUtils.isEmpty(g.mo19012g().f5604B.mo19346a())) {
            g.f5604B.mo19347a(null);
        }
        if (C2607la.m6742b() && g.mo19013h().mo19146a(C3135o.f5415Q0)) {
            g.f5624v.mo19327a(0);
        }
        if (!g.mo19013h().mo19159m()) {
            g.mo19313c(!c);
        }
        p6Var.mo18886q().mo19370D();
        if (C2607la.m6742b() && p6Var.mo19013h().mo19146a(C3135o.f5415Q0)) {
            p6Var.mo18889u().f5761d.mo19068a();
        }
        p6Var.f5552h = !c;
        this.f5658Q.mo18886q().mo19385a(new AtomicReference());
    }
}
