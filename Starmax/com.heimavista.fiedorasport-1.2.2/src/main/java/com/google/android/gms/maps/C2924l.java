package com.google.android.gms.maps;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.p093i.C2891b;
import com.google.android.gms.maps.p093i.C2912q;

/* renamed from: com.google.android.gms.maps.l */
final class C2924l extends C2912q {

    /* renamed from: a */
    private final /* synthetic */ C2885e f4796a;

    C2924l(MapView.C2868a aVar, C2885e eVar) {
        this.f4796a = eVar;
    }

    /* renamed from: a */
    public final void mo18487a(C2891b bVar) {
        this.f4796a.mo18411a(new C2880c(bVar));
    }
}
