package com.google.android.exoplayer2.upstream;

import android.text.TextUtils;
import com.google.android.exoplayer2.util.MimeTypes;
import com.google.android.exoplayer2.util.Util;

/* renamed from: com.google.android.exoplayer2.upstream.e */
/* compiled from: HttpDataSource */
public final /* synthetic */ class C1931e {
    /* renamed from: a */
    public static /* synthetic */ boolean m4434a(String str) {
        String lowerInvariant = Util.toLowerInvariant(str);
        return !TextUtils.isEmpty(lowerInvariant) && (!lowerInvariant.contains("text") || lowerInvariant.contains(MimeTypes.TEXT_VTT)) && !lowerInvariant.contains("html") && !lowerInvariant.contains("xml");
    }
}
