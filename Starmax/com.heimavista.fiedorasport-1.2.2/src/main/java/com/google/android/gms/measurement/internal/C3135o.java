package com.google.android.gms.measurement.internal;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.ItemTouchHelper;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.gms.internal.measurement.C2530h1;
import com.google.android.gms.internal.measurement.C2718t1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.measurement.internal.o */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3135o {

    /* renamed from: A */
    public static C3239x3<Long> f5382A = m8954a("measurement.upload.initial_upload_delay_time", 15000L, 15000L, C3100l0.f5318a);

    /* renamed from: A0 */
    public static C3239x3<Boolean> f5383A0 = m8954a("measurement.sdk.collection.last_deep_link_referrer_campaign2", false, false, C3205u2.f5670a);

    /* renamed from: B */
    public static C3239x3<Long> f5384B = m8954a("measurement.upload.retry_time", 1800000L, 1800000L, C3136o0.f5486a);

    /* renamed from: B0 */
    public static C3239x3<Boolean> f5385B0 = m8954a("measurement.sdk.collection.last_gclid_from_referrer2", false, false, C3194t2.f5648a);

    /* renamed from: C */
    public static C3239x3<Integer> f5386C = m8954a("measurement.upload.retry_count", 6, 6, C3124n0.f5361a);

    /* renamed from: C0 */
    public static C3239x3<Boolean> f5387C0 = m8954a("measurement.sdk.collection.worker_thread_referrer", true, true, C3227w2.f5709a);

    /* renamed from: D */
    public static C3239x3<Long> f5388D = m8954a("measurement.upload.max_queue_time", 2419200000L, 2419200000L, C3159q0.f5557a);

    /* renamed from: D0 */
    public static C3239x3<Boolean> f5389D0 = m8954a("measurement.sdk.collection.enable_extend_user_property_size", true, true, C3249y2.f5767a);

    /* renamed from: E */
    public static C3239x3<Integer> f5390E = m8954a("measurement.lifetimevalue.max_currency_tracked", 4, 4, C3148p0.f5537a);

    /* renamed from: E0 */
    public static C3239x3<Boolean> f5391E0 = m8954a("measurement.upload.file_lock_state_check", false, false, C3238x2.f5741a);

    /* renamed from: F */
    public static C3239x3<Integer> f5392F;

    /* renamed from: F0 */
    public static C3239x3<Boolean> f5393F0 = m8954a("measurement.sampling.calculate_bundle_timestamp_before_sampling", true, true, C2971a3.f4943a);

    /* renamed from: G */
    public static C3239x3<Long> f5394G;

    /* renamed from: G0 */
    public static C3239x3<Boolean> f5395G0 = m8954a("measurement.ga.ga_app_id", false, false, C3260z2.f5787a);

    /* renamed from: H */
    public static C3239x3<Boolean> f5396H = m8954a("measurement.test.boolean_flag", false, false, C3192t0.f5646a);

    /* renamed from: H0 */
    public static C3239x3<Boolean> f5397H0 = m8954a("measurement.lifecycle.app_backgrounded_tracking", true, true, C3007d3.f5040a);

    /* renamed from: I */
    public static C3239x3<String> f5398I = m8954a("measurement.test.string_flag", "---", "---", C3225w0.f5707a);

    /* renamed from: I0 */
    public static C3239x3<Boolean> f5399I0 = m8954a("measurement.lifecycle.app_in_background_parameter", false, false, C2983b3.f4965a);

    /* renamed from: J */
    public static C3239x3<Long> f5400J = m8954a("measurement.test.long_flag", -1L, -1L, C3214v0.f5691a);

    /* renamed from: J0 */
    public static C3239x3<Boolean> f5401J0 = m8954a("measurement.integration.disable_firebase_instance_id", false, false, C3031f3.f5121a);

    /* renamed from: K */
    public static C3239x3<Integer> f5402K = m8954a("measurement.test.int_flag", -2, -2, C3247y0.f5765a);

    /* renamed from: K0 */
    public static C3239x3<Boolean> f5403K0 = m8954a("measurement.lifecycle.app_backgrounded_engagement", false, false, C3019e3.f5062a);

    /* renamed from: L */
    public static C3239x3<Double> f5404L;

    /* renamed from: L0 */
    public static C3239x3<Boolean> f5405L0 = m8954a("measurement.service.fix_gmp_version", false, false, C3055h3.f5172a);

    /* renamed from: M */
    public static C3239x3<Integer> f5406M = m8954a("measurement.experiment.max_ids", 50, 50, C2981b1.f4961a);

    /* renamed from: M0 */
    public static C3239x3<Boolean> f5407M0 = m8954a("measurement.collection.service.update_with_analytics_fix", false, false, C3043g3.f5149a);

    /* renamed from: N */
    public static C3239x3<Boolean> f5408N = m8954a("measurement.validation.internal_limits_internal_event_params", false, false, C3258z0.f5785a);

    /* renamed from: N0 */
    public static C3239x3<Boolean> f5409N0 = m8954a("measurement.service.disable_install_state_reporting", false, false, C3067i3.f5202a);

    /* renamed from: O */
    public static C3239x3<Boolean> f5410O = m8954a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false, false, C3005d1.f5038a);

    /* renamed from: O0 */
    public static C3239x3<Boolean> f5411O0 = m8954a("measurement.service.use_appinfo_modified", false, false, C3103l3.f5321a);

    /* renamed from: P */
    public static C3239x3<Boolean> f5412P = m8954a("measurement.client.sessions.session_id_enabled", true, true, C3029f1.f5119a);

    /* renamed from: P0 */
    public static C3239x3<Boolean> f5413P0 = m8954a("measurement.client.firebase_feature_rollout.v1.enable", true, true, C3091k3.f5292a);

    /* renamed from: Q */
    public static C3239x3<Boolean> f5414Q = m8954a("measurement.service.sessions.session_number_enabled", true, true, C3017e1.f5060a);

    /* renamed from: Q0 */
    public static C3239x3<Boolean> f5415Q0 = m8954a("measurement.client.sessions.check_on_reset_and_enable", false, false, C3127n3.f5364a);

    /* renamed from: R */
    public static C3239x3<Boolean> f5416R = m8954a("measurement.client.sessions.background_sessions_enabled", true, true, C3053h1.f5170a);

    /* renamed from: R0 */
    public static C3239x3<Boolean> f5417R0 = m8954a("measurement.config.string.always_update_disk_on_set", false, false, C3115m3.f5342a);

    /* renamed from: S */
    public static C3239x3<Boolean> f5418S = m8954a("measurement.client.sessions.remove_expired_session_properties_enabled", true, true, C3041g1.f5147a);

    /* renamed from: S0 */
    public static C3239x3<Boolean> f5419S0 = m8954a("measurement.scheduler.task_thread.cleanup_on_exit", false, false, C3151p3.f5540a);

    /* renamed from: T */
    public static C3239x3<Boolean> f5420T = m8954a("measurement.service.sessions.session_number_backfill_enabled", true, true, C3077j1.f5227a);

    /* renamed from: T0 */
    public static C3239x3<Boolean> f5421T0 = m8954a("measurement.upload.file_truncate_fix", false, false, C3139o3.f5489a);

    /* renamed from: U */
    public static C3239x3<Boolean> f5422U = m8954a("measurement.service.sessions.remove_disabled_session_number", true, true, C3065i1.f5200a);

    /* renamed from: U0 */
    public static C3239x3<Boolean> f5423U0 = m8954a("measurement.lifecycle.app_background_timestamp_when_backgrounded", true, true, C3173r3.f5583a);

    /* renamed from: V */
    public static C3239x3<Boolean> f5424V = m8954a("measurement.client.sessions.start_session_before_view_screen", true, true, C3101l1.f5319a);

    /* renamed from: V0 */
    public static C3239x3<Boolean> f5425V0 = m8954a("measurement.engagement_time_main_thread", false, false, C3162q3.f5560a);

    /* renamed from: W */
    public static C3239x3<Boolean> f5426W = m8954a("measurement.client.sessions.check_on_startup", true, true, C3089k1.f5290a);

    /* renamed from: W0 */
    public static C3239x3<Boolean> f5427W0 = m8954a("measurement.sdk.referrer.delayed_install_referrer_api", false, false, C3195t3.f5649a);

    /* renamed from: X */
    public static C3239x3<Boolean> f5428X = m8954a("measurement.collection.firebase_global_collection_flag_enabled", true, true, C3125n1.f5362a);

    /* renamed from: X0 */
    public static C3239x3<Boolean> f5429X0 = m8954a("measurement.logging.improved_messaging_q4_2019_client", true, true, C3180s.f5598a);

    /* renamed from: Y */
    public static C3239x3<Boolean> f5430Y = m8954a("measurement.collection.efficient_engagement_reporting_enabled", false, false, C3113m1.f5340a);

    /* renamed from: Y0 */
    public static C3239x3<Boolean> f5431Y0 = m8954a("measurement.logging.improved_messaging_q4_2019_service", true, true, C3213v.f5690a);

    /* renamed from: Z */
    public static C3239x3<Boolean> f5432Z = m8954a("measurement.collection.redundant_engagement_removal_enabled", false, false, C3137o1.f5487a);

    /* renamed from: Z0 */
    public static C3239x3<Boolean> f5433Z0 = m8954a("measurement.gold.enhanced_ecommerce.format_logs", false, false, C3202u.f5667a);
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static List<C3239x3<?>> f5434a = Collections.synchronizedList(new ArrayList());

    /* renamed from: a0 */
    public static C3239x3<Boolean> f5435a0 = m8954a("measurement.personalized_ads_signals_collection_enabled", true, true, C3171r1.f5581a);

    /* renamed from: b */
    public static C3239x3<Long> f5436b = m8954a("measurement.ad_id_cache_time", 10000L, 10000L, C3169r.f5579a);

    /* renamed from: b0 */
    public static C3239x3<Boolean> f5437b0 = m8954a("measurement.personalized_ads_property_translation_enabled", true, true, C3160q1.f5558a);

    /* renamed from: c */
    public static C3239x3<Long> f5438c = m8954a("measurement.monitoring.sample_period_millis", 86400000L, 86400000L, C3158q.f5556a);

    /* renamed from: c0 */
    public static C3239x3<Boolean> f5439c0 = m8954a("measurement.upload.disable_is_uploader", true, true, C3193t1.f5647a);

    /* renamed from: d */
    public static C3239x3<Long> f5440d = m8954a("measurement.config.cache_time", 86400000L, 3600000L, C3028f0.f5118a);

    /* renamed from: d0 */
    public static C3239x3<Boolean> f5441d0 = m8954a("measurement.experiment.enable_experiment_reporting", true, true, C3182s1.f5600a);

    /* renamed from: e */
    public static C3239x3<String> f5442e = m8954a("measurement.config.url_scheme", "https", "https", C3181s0.f5599a);

    /* renamed from: e0 */
    public static C3239x3<Boolean> f5443e0 = m8954a("measurement.collection.log_event_and_bundle_v2", true, true, C3215v1.f5692a);

    /* renamed from: f */
    public static C3239x3<String> f5444f = m8954a("measurement.config.url_authority", "app-measurement.com", "app-measurement.com", C2993c1.f5000a);

    /* renamed from: f0 */
    public static C3239x3<Boolean> f5445f0 = m8954a("measurement.quality.checksum", false, false, null);

    /* renamed from: g */
    public static C3239x3<Integer> f5446g = m8954a("measurement.upload.max_bundles", 100, 100, C3149p1.f5538a);

    /* renamed from: g0 */
    public static C3239x3<Boolean> f5447g0 = m8954a("measurement.module.collection.conditionally_omit_admob_app_id", true, true, C3204u1.f5669a);

    /* renamed from: h */
    public static C3239x3<Integer> f5448h = m8954a("measurement.upload.max_batch_size", 65536, 65536, C3248y1.f5766a);

    /* renamed from: h0 */
    public static C3239x3<Boolean> f5449h0 = m8954a("measurement.sdk.dynamite.use_dynamite2", false, false, C3237x1.f5740a);

    /* renamed from: i */
    public static C3239x3<Integer> f5450i = m8954a("measurement.upload.max_bundle_size", 65536, 65536, C3114m2.f5341a);

    /* renamed from: i0 */
    public static C3239x3<Boolean> f5451i0 = m8954a("measurement.sdk.dynamite.allow_remote_dynamite", false, false, C3226w1.f5708a);

    /* renamed from: j */
    public static C3239x3<Integer> f5452j = m8954a("measurement.upload.max_events_per_bundle", 1000, 1000, C3216v2.f5693a);

    /* renamed from: j0 */
    public static C3239x3<Boolean> f5453j0 = m8954a("measurement.sdk.collection.validate_param_names_alphabetical", false, false, C3259z1.f5786a);

    /* renamed from: k */
    public static C3239x3<Integer> f5454k;

    /* renamed from: k0 */
    public static C3239x3<Boolean> f5455k0 = m8954a("measurement.collection.event_safelist", true, true, C2994c2.f5001a);

    /* renamed from: l */
    public static C3239x3<Integer> f5456l = m8954a("measurement.upload.max_error_events_per_day", 1000, 1000, C3191t.f5645a);

    /* renamed from: l0 */
    public static C3239x3<Boolean> f5457l0 = m8954a("measurement.service.audience.scoped_filters_v27", true, true, C2970a2.f4942a);

    /* renamed from: m */
    public static C3239x3<Integer> f5458m;

    /* renamed from: m0 */
    public static C3239x3<Boolean> f5459m0 = m8954a("measurement.service.audience.session_scoped_event_aggregates", true, true, C3018e2.f5061a);

    /* renamed from: n */
    public static C3239x3<Integer> f5460n = m8954a("measurement.upload.max_conversions_per_day", 500, 500, C3224w.f5706a);

    /* renamed from: n0 */
    public static C3239x3<Boolean> f5461n0 = m8954a("measurement.service.audience.session_scoped_user_engagement", true, true, C3006d2.f5039a);

    /* renamed from: o */
    public static C3239x3<Integer> f5462o = m8954a("measurement.upload.max_realtime_events_per_day", 10, 10, C2968a0.f4939a);

    /* renamed from: o0 */
    public static C3239x3<Boolean> f5463o0 = m8954a("measurement.service.audience.scoped_engagement_removal_when_session_expired", true, true, C3042g2.f5148a);

    /* renamed from: p */
    public static C3239x3<Integer> f5464p;

    /* renamed from: p0 */
    public static C3239x3<Boolean> f5465p0 = m8954a("measurement.client.audience.scoped_engagement_removal_when_session_expired", true, true, C3030f2.f5120a);

    /* renamed from: q */
    public static C3239x3<String> f5466q = m8954a("measurement.upload.url", "https://app-measurement.com/a", "https://app-measurement.com/a", C2992c0.f4999a);

    /* renamed from: q0 */
    public static C3239x3<Boolean> f5467q0 = m8954a("measurement.service.audience.remove_disabled_session_scoped_user_engagement", false, false, C3066i2.f5201a);

    /* renamed from: r */
    public static C3239x3<Long> f5468r = m8954a("measurement.upload.backoff_period", 43200000L, 43200000L, C2980b0.f4960a);

    /* renamed from: r0 */
    public static C3239x3<Boolean> f5469r0 = m8954a("measurement.service.audience.use_bundle_timestamp_for_property_filters", true, true, C3054h2.f5171a);

    /* renamed from: s */
    public static C3239x3<Long> f5470s = m8954a("measurement.upload.window_interval", 3600000L, 3600000L, C3016e0.f5059a);

    /* renamed from: s0 */
    public static C3239x3<Boolean> f5471s0 = m8954a("measurement.service.audience.fix_prepending_previous_sequence_timestamp", true, true, C3090k2.f5291a);

    /* renamed from: t */
    public static C3239x3<Long> f5472t = m8954a("measurement.upload.interval", 3600000L, 3600000L, C3004d0.f5037a);

    /* renamed from: t0 */
    public static C3239x3<Boolean> f5473t0 = m8954a("measurement.service.audience.fix_skip_audience_with_failed_filters", true, true, C3102l2.f5320a);

    /* renamed from: u */
    public static C3239x3<Long> f5474u = m8954a("measurement.upload.realtime_upload_interval", 10000L, 10000L, C3040g0.f5146a);

    /* renamed from: u0 */
    public static C3239x3<Boolean> f5475u0 = m8954a("measurement.audience.use_bundle_end_timestamp_for_non_sequence_property_filters", false, false, C3138o2.f5488a);

    /* renamed from: v */
    public static C3239x3<Long> f5476v = m8954a("measurement.upload.debug_upload_interval", 1000L, 1000L, C3064i0.f5199a);

    /* renamed from: v0 */
    public static C3239x3<Boolean> f5477v0 = m8954a("measurement.audience.refresh_event_count_filters_timestamp", false, false, C3126n2.f5363a);

    /* renamed from: w */
    public static C3239x3<Long> f5478w = m8954a("measurement.upload.minimum_delay", 500L, 500L, C3052h0.f5169a);

    /* renamed from: w0 */
    public static C3239x3<Boolean> f5479w0 = m8954a("measurement.audience.use_bundle_timestamp_for_event_count_filters", false, false, C3161q2.f5559a);

    /* renamed from: x */
    public static C3239x3<Long> f5480x;

    /* renamed from: x0 */
    public static C3239x3<Boolean> f5481x0 = m8954a("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true, true, C3150p2.f5539a);

    /* renamed from: y */
    public static C3239x3<Long> f5482y = m8954a("measurement.upload.stale_data_deletion_interval", 86400000L, 86400000L, C3076j0.f5226a);

    /* renamed from: y0 */
    public static C3239x3<Boolean> f5483y0 = m8954a("measurement.app_launch.event_ordering_fix", false, false, C3183s2.f5601a);

    /* renamed from: z */
    public static C3239x3<Long> f5484z = m8954a("measurement.upload.refresh_blacklisted_config_interval", 604800000L, 604800000L, C3112m0.f5339a);

    /* renamed from: z0 */
    public static C3239x3<Boolean> f5485z0 = m8954a("measurement.sdk.collection.last_deep_link_referrer2", false, false, C3172r2.f5582a);

    static {
        Collections.synchronizedSet(new HashSet());
        Integer valueOf = Integer.valueOf((int) DefaultOggSeeker.MATCH_BYTE_RANGE);
        f5454k = m8954a("measurement.upload.max_events_per_day", valueOf, valueOf, C3079j3.f5229a);
        Integer valueOf2 = Integer.valueOf((int) DefaultLoadControl.DEFAULT_MAX_BUFFER_MS);
        f5458m = m8954a("measurement.upload.max_public_events_per_day", valueOf2, valueOf2, C3235x.f5738a);
        Integer valueOf3 = Integer.valueOf((int) DefaultOggSeeker.MATCH_BYTE_RANGE);
        f5464p = m8954a("measurement.store.max_stored_events_per_app", valueOf3, valueOf3, C3246y.f5764a);
        Long valueOf4 = Long.valueOf((long) DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS);
        f5480x = m8954a("measurement.alarm_manager.minimum_interval", valueOf4, valueOf4, C3088k0.f5289a);
        Integer valueOf5 = Integer.valueOf((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        f5392F = m8954a("measurement.audience.filter_result_max_count", valueOf5, valueOf5, C3170r0.f5580a);
        Long valueOf6 = Long.valueOf((long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        f5394G = m8954a("measurement.service_client.idle_disconnect_millis", valueOf6, valueOf6, C3203u0.f5668a);
        Double valueOf7 = Double.valueOf(-3.0d);
        f5404L = m8954a("measurement.test.double_flag", valueOf7, valueOf7, C3236x0.f5739a);
        m8954a("measurement.service.audience.invalidate_config_cache_after_app_unisntall", true, true, C3078j2.f5228a);
    }

    /* renamed from: a */
    public static Map<String, String> m8956a(Context context) {
        C2530h1 a = C2530h1.m6433a(context.getContentResolver(), C2718t1.m7213a("com.google.android.gms.measurement"));
        return a == null ? Collections.emptyMap() : a.mo17535a();
    }

    /* renamed from: a */
    private static <V> C3239x3<V> m8954a(@Nullable String str, @Nullable V v, @Nullable V v2, @Nullable C3217v3<V> v3Var) {
        C3239x3 x3Var = new C3239x3(str, v, v2, v3Var);
        f5434a.add(x3Var);
        return x3Var;
    }
}
