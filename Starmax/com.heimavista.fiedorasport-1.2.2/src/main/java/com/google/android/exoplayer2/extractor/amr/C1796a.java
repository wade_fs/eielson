package com.google.android.exoplayer2.extractor.amr;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.amr.a */
/* compiled from: lambda */
public final /* synthetic */ class C1796a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1796a f2811a = new C1796a();

    private /* synthetic */ C1796a() {
    }

    public final Extractor[] createExtractors() {
        return AmrExtractor.m4380a();
    }
}
