package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2221i;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.p089z.C2266a;
import com.google.android.gms.common.util.C2313d;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.l0 */
public final class C2100l0 extends C2036f implements C2089i1 {

    /* renamed from: b */
    private final Lock f3359b;

    /* renamed from: c */
    private boolean f3360c;

    /* renamed from: d */
    private final C2221i f3361d;

    /* renamed from: e */
    private C2082h1 f3362e = null;

    /* renamed from: f */
    private final int f3363f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final Context f3364g;

    /* renamed from: h */
    private final Looper f3365h;

    /* renamed from: i */
    final Queue<C2056c<?, ?>> f3366i = new LinkedList();

    /* renamed from: j */
    private volatile boolean f3367j;

    /* renamed from: k */
    private long f3368k;

    /* renamed from: l */
    private long f3369l;

    /* renamed from: m */
    private final C2124r0 f3370m;

    /* renamed from: n */
    private final C2167b f3371n;

    /* renamed from: o */
    private zabq f3372o;

    /* renamed from: p */
    final Map<C2016a.C2019c<?>, C2016a.C2027f> f3373p;

    /* renamed from: q */
    Set<Scope> f3374q;

    /* renamed from: r */
    private final C2211e f3375r;

    /* renamed from: s */
    private final Map<C2016a<?>, Boolean> f3376s;

    /* renamed from: t */
    private final C2016a.C2017a<? extends C4052e, C4047a> f3377t;

    /* renamed from: u */
    private final C2091j f3378u;

    /* renamed from: v */
    private final ArrayList<C2102l2> f3379v;

    /* renamed from: w */
    private Integer f3380w;

    /* renamed from: x */
    Set<C2129s1> f3381x;

    /* renamed from: y */
    final C2141v1 f3382y;

    /* renamed from: z */
    private final C2221i.C2222a f3383z;

    public C2100l0(Context context, Lock lock, Looper looper, C2211e eVar, C2167b bVar, C2016a.C2017a<? extends C4052e, C4047a> aVar, Map<C2016a<?>, Boolean> map, List<C2036f.C2038b> list, List<C2036f.C2039c> list2, Map<C2016a.C2019c<?>, C2016a.C2027f> map2, int i, int i2, ArrayList<C2102l2> arrayList, boolean z) {
        Looper looper2 = looper;
        this.f3368k = C2313d.m5771a() ? 10000 : 120000;
        this.f3369l = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
        this.f3374q = new HashSet();
        this.f3378u = new C2091j();
        this.f3380w = null;
        this.f3381x = null;
        this.f3383z = new C2104m0(this);
        this.f3364g = context;
        this.f3359b = lock;
        this.f3360c = false;
        this.f3361d = new C2221i(looper, this.f3383z);
        this.f3365h = looper2;
        this.f3370m = new C2124r0(this, looper);
        this.f3371n = bVar;
        this.f3363f = i;
        if (this.f3363f >= 0) {
            this.f3380w = Integer.valueOf(i2);
        }
        this.f3376s = map;
        this.f3373p = map2;
        this.f3379v = arrayList;
        this.f3382y = new C2141v1(this.f3373p);
        for (C2036f.C2038b bVar2 : list) {
            this.f3361d.mo16983a(bVar2);
        }
        for (C2036f.C2039c cVar : list2) {
            this.f3361d.mo16984a(cVar);
        }
        this.f3375r = eVar;
        this.f3377t = aVar;
    }

    /* renamed from: c */
    private static String m4958c(int i) {
        return i != 1 ? i != 2 ? i != 3 ? "UNKNOWN" : "SIGN_IN_MODE_NONE" : "SIGN_IN_MODE_OPTIONAL" : "SIGN_IN_MODE_REQUIRED";
    }

    /* access modifiers changed from: private */
    /* renamed from: o */
    public final void m4959o() {
        this.f3359b.lock();
        try {
            if (this.f3367j) {
                m4960p();
            }
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: p */
    private final void m4960p() {
        this.f3361d.mo16985b();
        this.f3362e.mo16712b();
    }

    /* access modifiers changed from: private */
    /* renamed from: q */
    public final void m4961q() {
        this.f3359b.lock();
        try {
            if (mo16750l()) {
                m4960p();
            }
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16564a(@NonNull T t) {
        C2258v.m5637a(t.mo16642h() != null, "This task can not be executed (it's probably a Batch or malformed)");
        boolean containsKey = this.f3373p.containsKey(t.mo16642h());
        String b = t.mo16641g() != null ? t.mo16641g().mo16522b() : "the API";
        StringBuilder sb = new StringBuilder(String.valueOf(b).length() + 65);
        sb.append("GoogleApiClient is not configured to use ");
        sb.append(b);
        sb.append(" required for this call.");
        C2258v.m5637a(containsKey, sb.toString());
        this.f3359b.lock();
        try {
            if (this.f3362e == null) {
                throw new IllegalStateException("GoogleApiClient is not connected yet.");
            } else if (this.f3367j) {
                this.f3366i.add(t);
                while (!this.f3366i.isEmpty()) {
                    C2056c remove = this.f3366i.remove();
                    this.f3382y.mo16790a(remove);
                    remove.mo16640c(Status.f3179V);
                }
                return t;
            } else {
                T a = this.f3362e.mo16708a((C2056c) t);
                this.f3359b.unlock();
                return a;
            }
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: b */
    public final C2040g<Status> mo16569b() {
        C2258v.m5641b(mo16748j(), "GoogleApiClient is not connected yet.");
        C2258v.m5641b(this.f3380w.intValue() != 2, "Cannot use clearDefaultAccountAndReconnect with GOOGLE_SIGN_IN_API");
        C2107n nVar = new C2107n(super);
        if (this.f3373p.containsKey(C2266a.f3764a)) {
            m4952a(super, nVar, false);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            C2108n0 n0Var = new C2108n0(this, atomicReference, nVar);
            C2112o0 o0Var = new C2112o0(this, nVar);
            C2036f.C2037a aVar = new C2036f.C2037a(this.f3364g);
            aVar.mo16577a(C2266a.f3766c);
            aVar.mo16579a(n0Var);
            aVar.mo16580a(o0Var);
            aVar.mo16576a(this.f3370m);
            C2036f a = aVar.mo16581a();
            atomicReference.set(a);
            super.mo16571c();
        }
        return nVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.l0.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int
     arg types: [java.util.Collection<com.google.android.gms.common.api.a$f>, int]
     candidates:
      com.google.android.gms.common.api.internal.l0.a(int, boolean):void
      com.google.android.gms.common.api.internal.i1.a(int, boolean):void
      com.google.android.gms.common.api.internal.l0.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int */
    /* renamed from: c */
    public final void mo16571c() {
        this.f3359b.lock();
        try {
            boolean z = false;
            if (this.f3363f >= 0) {
                if (this.f3380w != null) {
                    z = true;
                }
                C2258v.m5641b(z, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.f3380w == null) {
                this.f3380w = Integer.valueOf(m4951a((Iterable<C2016a.C2027f>) this.f3373p.values(), false));
            } else if (this.f3380w.intValue() == 2) {
                throw new IllegalStateException("Cannot call connect() when SignInMode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            mo16747a(this.f3380w.intValue());
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: d */
    public final void mo16572d() {
        this.f3359b.lock();
        try {
            this.f3382y.mo16789a();
            if (this.f3362e != null) {
                this.f3362e.mo16709a();
            }
            this.f3378u.mo16732a();
            for (C2056c cVar : this.f3366i) {
                cVar.mo16592a((C2152y1) null);
                cVar.mo16591a();
            }
            this.f3366i.clear();
            if (this.f3362e != null) {
                mo16750l();
                this.f3361d.mo16979a();
                this.f3359b.unlock();
            }
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: e */
    public final Context mo16573e() {
        return this.f3364g;
    }

    /* renamed from: f */
    public final Looper mo16574f() {
        return this.f3365h;
    }

    /* renamed from: g */
    public final void mo16575g() {
        C2082h1 h1Var = this.f3362e;
        if (h1Var != null) {
            h1Var.mo16715e();
        }
    }

    /* renamed from: j */
    public final boolean mo16748j() {
        C2082h1 h1Var = this.f3362e;
        return h1Var != null && h1Var.mo16713c();
    }

    /* renamed from: k */
    public final void mo16749k() {
        mo16572d();
        mo16571c();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public final boolean mo16750l() {
        if (!this.f3367j) {
            return false;
        }
        this.f3367j = false;
        this.f3370m.removeMessages(2);
        this.f3370m.removeMessages(1);
        zabq zabq = this.f3372o;
        if (zabq != null) {
            zabq.mo16799a();
            this.f3372o = null;
        }
        return true;
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public final boolean mo16751m() {
        this.f3359b.lock();
        try {
            if (this.f3381x == null) {
                this.f3359b.unlock();
                return false;
            }
            boolean z = !this.f3381x.isEmpty();
            this.f3359b.unlock();
            return z;
        } catch (Throwable th) {
            this.f3359b.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: n */
    public final String mo16752n() {
        StringWriter stringWriter = new StringWriter();
        mo16567a("", (FileDescriptor) null, new PrintWriter(stringWriter), (String[]) null);
        return stringWriter.toString();
    }

    /* renamed from: b */
    private final void m4955b(int i) {
        Integer num = this.f3380w;
        if (num == null) {
            this.f3380w = Integer.valueOf(i);
        } else if (num.intValue() != i) {
            String c = m4958c(i);
            String c2 = m4958c(this.f3380w.intValue());
            StringBuilder sb = new StringBuilder(String.valueOf(c).length() + 51 + String.valueOf(c2).length());
            sb.append("Cannot use sign-in mode: ");
            sb.append(c);
            sb.append(". Mode was already set to ");
            sb.append(c2);
            throw new IllegalStateException(sb.toString());
        }
        if (this.f3362e == null) {
            boolean z = false;
            boolean z2 = false;
            for (C2016a.C2027f fVar : this.f3373p.values()) {
                if (fVar.mo16538l()) {
                    z = true;
                }
                if (fVar.mo16450d()) {
                    z2 = true;
                }
            }
            int intValue = this.f3380w.intValue();
            if (intValue != 1) {
                if (intValue == 2) {
                    if (z) {
                        if (this.f3360c) {
                            this.f3362e = new C2130s2(this.f3364g, this.f3359b, this.f3365h, this.f3371n, this.f3373p, this.f3375r, this.f3376s, this.f3377t, this.f3379v, this, true);
                            return;
                        } else {
                            this.f3362e = C2110n2.m5009a(this.f3364g, this, this.f3359b, this.f3365h, this.f3371n, this.f3373p, this.f3375r, this.f3376s, this.f3377t, this.f3379v);
                            return;
                        }
                    }
                }
            } else if (!z) {
                throw new IllegalStateException("SIGN_IN_MODE_REQUIRED cannot be used on a GoogleApiClient that does not contain any authenticated APIs. Use connect() instead.");
            } else if (z2) {
                throw new IllegalStateException("Cannot use SIGN_IN_MODE_REQUIRED with GOOGLE_SIGN_IN_API. Use connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            if (!this.f3360c || z2) {
                this.f3362e = new C2136u0(this.f3364g, this, this.f3359b, this.f3365h, this.f3371n, this.f3373p, this.f3375r, this.f3376s, this.f3377t, this.f3379v, this);
            } else {
                this.f3362e = new C2130s2(this.f3364g, this.f3359b, this.f3365h, this.f3371n, this.f3373p, this.f3375r, this.f3376s, this.f3377t, this.f3379v, this, false);
            }
        }
    }

    /* renamed from: a */
    public final void mo16747a(int i) {
        this.f3359b.lock();
        boolean z = true;
        if (!(i == 3 || i == 1 || i == 2)) {
            z = false;
        }
        try {
            StringBuilder sb = new StringBuilder(33);
            sb.append("Illegal sign-in mode: ");
            sb.append(i);
            C2258v.m5637a(z, sb.toString());
            m4955b(i);
            m4960p();
        } finally {
            this.f3359b.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.l0.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int
     arg types: [java.util.Collection<com.google.android.gms.common.api.a$f>, int]
     candidates:
      com.google.android.gms.common.api.internal.l0.a(int, boolean):void
      com.google.android.gms.common.api.internal.i1.a(int, boolean):void
      com.google.android.gms.common.api.internal.l0.a(java.lang.Iterable<com.google.android.gms.common.api.a$f>, boolean):int */
    /* renamed from: a */
    public final ConnectionResult mo16563a() {
        boolean z = true;
        C2258v.m5641b(Looper.myLooper() != Looper.getMainLooper(), "blockingConnect must not be called on the UI thread");
        this.f3359b.lock();
        try {
            if (this.f3363f >= 0) {
                if (this.f3380w == null) {
                    z = false;
                }
                C2258v.m5641b(z, "Sign-in mode should have been set explicitly by auto-manage.");
            } else if (this.f3380w == null) {
                this.f3380w = Integer.valueOf(m4951a((Iterable<C2016a.C2027f>) this.f3373p.values(), false));
            } else if (this.f3380w.intValue() == 2) {
                throw new IllegalStateException("Cannot call blockingConnect() when sign-in mode is set to SIGN_IN_MODE_OPTIONAL. Call connect(SIGN_IN_MODE_OPTIONAL) instead.");
            }
            m4955b(this.f3380w.intValue());
            this.f3361d.mo16985b();
            return this.f3362e.mo16716f();
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: b */
    public final void mo16570b(@NonNull C2036f.C2039c cVar) {
        this.f3361d.mo16986b(cVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m4952a(C2036f fVar, C2107n nVar, boolean z) {
        C2266a.f3767d.mo17047a(super).mo16588a(new C2120q0(this, nVar, z, super));
    }

    /* renamed from: a */
    public final void mo16565a(@NonNull C2036f.C2039c cVar) {
        this.f3361d.mo16984a(cVar);
    }

    /* renamed from: a */
    public final void mo16729a(Bundle bundle) {
        while (!this.f3366i.isEmpty()) {
            mo16564a(this.f3366i.remove());
        }
        this.f3361d.mo16981a(bundle);
    }

    /* renamed from: a */
    public final void mo16730a(ConnectionResult connectionResult) {
        if (!this.f3371n.mo16841b(this.f3364g, connectionResult.mo16475c())) {
            mo16750l();
        }
        if (!this.f3367j) {
            this.f3361d.mo16982a(connectionResult);
            this.f3361d.mo16979a();
        }
    }

    /* renamed from: a */
    public final void mo16728a(int i, boolean z) {
        if (i == 1 && !z && !this.f3367j) {
            this.f3367j = true;
            if (this.f3372o == null && !C2313d.m5771a()) {
                this.f3372o = this.f3371n.mo16826a(this.f3364g.getApplicationContext(), new C2128s0(this));
            }
            C2124r0 r0Var = this.f3370m;
            r0Var.sendMessageDelayed(r0Var.obtainMessage(1), this.f3368k);
            C2124r0 r0Var2 = this.f3370m;
            r0Var2.sendMessageDelayed(r0Var2.obtainMessage(2), this.f3369l);
        }
        this.f3382y.mo16791b();
        this.f3361d.mo16980a(i);
        this.f3361d.mo16979a();
        if (i == 2) {
            m4960p();
        }
    }

    /* renamed from: a */
    public final boolean mo16568a(C2099l lVar) {
        C2082h1 h1Var = this.f3362e;
        return h1Var != null && h1Var.mo16711a(lVar);
    }

    /* renamed from: a */
    public final void mo16566a(C2129s1 s1Var) {
        this.f3359b.lock();
        try {
            if (this.f3381x == null) {
                Log.wtf("GoogleApiClientImpl", "Attempted to remove pending transform when no transforms are registered.", new Exception());
            } else if (!this.f3381x.remove(s1Var)) {
                Log.wtf("GoogleApiClientImpl", "Failed to remove pending transform - this may lead to memory leaks!", new Exception());
            } else if (!mo16751m()) {
                this.f3362e.mo16714d();
            }
        } finally {
            this.f3359b.unlock();
        }
    }

    /* renamed from: a */
    public final void mo16567a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append((CharSequence) "mContext=").println(this.f3364g);
        printWriter.append((CharSequence) str).append((CharSequence) "mResuming=").print(this.f3367j);
        printWriter.append((CharSequence) " mWorkQueue.size()=").print(this.f3366i.size());
        printWriter.append((CharSequence) " mUnconsumedApiCalls.size()=").println(this.f3382y.f3489a.size());
        C2082h1 h1Var = this.f3362e;
        if (h1Var != null) {
            h1Var.mo16710a(str, fileDescriptor, printWriter, strArr);
        }
    }

    /* renamed from: a */
    public static int m4951a(Iterable<C2016a.C2027f> iterable, boolean z) {
        boolean z2 = false;
        boolean z3 = false;
        for (C2016a.C2027f fVar : iterable) {
            if (fVar.mo16538l()) {
                z2 = true;
            }
            if (fVar.mo16450d()) {
                z3 = true;
            }
        }
        if (!z2) {
            return 3;
        }
        if (!z3 || !z) {
            return 1;
        }
        return 2;
    }
}
