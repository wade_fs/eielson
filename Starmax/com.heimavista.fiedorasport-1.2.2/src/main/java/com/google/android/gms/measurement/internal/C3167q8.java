package com.google.android.gms.measurement.internal;

import android.content.ComponentName;

/* renamed from: com.google.android.gms.measurement.internal.q8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3167q8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ ComponentName f5575P;

    /* renamed from: Q */
    private final /* synthetic */ C3144o8 f5576Q;

    C3167q8(C3144o8 o8Var, ComponentName componentName) {
        this.f5576Q = o8Var;
        this.f5575P = componentName;
    }

    public final void run() {
        this.f5576Q.f5507R.m9320a(this.f5575P);
    }
}
