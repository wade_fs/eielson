package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;
import p119e.p144d.p145a.p157c.p161c.p163b.C4012d;

public final class zzc extends Fragment implements C2080h {

    /* renamed from: S */
    private static WeakHashMap<FragmentActivity, WeakReference<zzc>> f3538S = new WeakHashMap<>();

    /* renamed from: P */
    private Map<String, LifecycleCallback> f3539P = new ArrayMap();
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public int f3540Q = 0;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public Bundle f3541R;

    /* renamed from: a */
    public static zzc m5237a(FragmentActivity fragmentActivity) {
        zzc zzc;
        WeakReference weakReference = f3538S.get(fragmentActivity);
        if (weakReference != null && (zzc = (zzc) weakReference.get()) != null) {
            return zzc;
        }
        try {
            zzc zzc2 = (zzc) fragmentActivity.getSupportFragmentManager().findFragmentByTag("SupportLifecycleFragmentImpl");
            if (zzc2 == null || super.isRemoving()) {
                zzc2 = new zzc();
                fragmentActivity.getSupportFragmentManager().beginTransaction().add(super, "SupportLifecycleFragmentImpl").commitAllowingStateLoss();
            }
            f3538S.put(fragmentActivity, new WeakReference(zzc2));
            return zzc2;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag SupportLifecycleFragmentImpl is not a SupportLifecycleFragmentImpl", e);
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback lifecycleCallback : this.f3539P.values()) {
            lifecycleCallback.mo16606a(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback lifecycleCallback : this.f3539P.values()) {
            lifecycleCallback.mo16604a(i, i2, intent);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3540Q = 1;
        this.f3541R = bundle;
        for (Map.Entry entry : this.f3539P.entrySet()) {
            ((LifecycleCallback) entry.getValue()).mo16605a(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public final void onDestroy() {
        super.onDestroy();
        this.f3540Q = 5;
        for (LifecycleCallback lifecycleCallback : this.f3539P.values()) {
            lifecycleCallback.mo16607b();
        }
    }

    public final void onResume() {
        super.onResume();
        this.f3540Q = 3;
        for (LifecycleCallback lifecycleCallback : this.f3539P.values()) {
            lifecycleCallback.mo16609c();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry entry : this.f3539P.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) entry.getValue()).mo16608b(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public final void onStart() {
        super.onStart();
        this.f3540Q = 2;
        for (LifecycleCallback lifecycleCallback : this.f3539P.values()) {
            lifecycleCallback.mo16610d();
        }
    }

    public final void onStop() {
        super.onStop();
        this.f3540Q = 4;
        for (LifecycleCallback lifecycleCallback : this.f3539P.values()) {
            lifecycleCallback.mo16611e();
        }
    }

    /* renamed from: a */
    public final <T extends LifecycleCallback> T mo16705a(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.f3539P.get(str));
    }

    /* renamed from: a */
    public final void mo16706a(String str, @NonNull LifecycleCallback lifecycleCallback) {
        if (!this.f3539P.containsKey(str)) {
            this.f3539P.put(str, lifecycleCallback);
            if (this.f3540Q > 0) {
                new C4012d(Looper.getMainLooper()).post(new C2146w2(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    /* renamed from: a */
    public final /* synthetic */ Activity mo16704a() {
        return getActivity();
    }
}
