package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2746uc;

/* renamed from: com.google.android.gms.measurement.internal.g3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3043g3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5149a = new C3043g3();

    private C3043g3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2746uc.m7384b());
    }
}
