package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.WeakHashMap;
import p119e.p144d.p145a.p157c.p161c.p163b.C4012d;

public final class zza extends Fragment implements C2080h {

    /* renamed from: S */
    private static WeakHashMap<Activity, WeakReference<zza>> f3534S = new WeakHashMap<>();

    /* renamed from: P */
    private Map<String, LifecycleCallback> f3535P = new ArrayMap();
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public int f3536Q = 0;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public Bundle f3537R;

    /* renamed from: a */
    public static zza m5231a(Activity activity) {
        zza zza;
        WeakReference weakReference = f3534S.get(activity);
        if (weakReference != null && (zza = (zza) weakReference.get()) != null) {
            return zza;
        }
        try {
            zza zza2 = (zza) activity.getFragmentManager().findFragmentByTag("LifecycleFragmentImpl");
            if (zza2 == null || super.isRemoving()) {
                zza2 = new zza();
                activity.getFragmentManager().beginTransaction().add(super, "LifecycleFragmentImpl").commitAllowingStateLoss();
            }
            f3534S.put(activity, new WeakReference(zza2));
            return zza2;
        } catch (ClassCastException e) {
            throw new IllegalStateException("Fragment with tag LifecycleFragmentImpl is not a LifecycleFragmentImpl", e);
        }
    }

    public final void dump(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        super.dump(str, fileDescriptor, printWriter, strArr);
        for (LifecycleCallback lifecycleCallback : this.f3535P.values()) {
            lifecycleCallback.mo16606a(str, fileDescriptor, printWriter, strArr);
        }
    }

    public final void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (LifecycleCallback lifecycleCallback : this.f3535P.values()) {
            lifecycleCallback.mo16604a(i, i2, intent);
        }
    }

    public final void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f3536Q = 1;
        this.f3537R = bundle;
        for (Map.Entry entry : this.f3535P.entrySet()) {
            ((LifecycleCallback) entry.getValue()).mo16605a(bundle != null ? bundle.getBundle((String) entry.getKey()) : null);
        }
    }

    public final void onDestroy() {
        super.onDestroy();
        this.f3536Q = 5;
        for (LifecycleCallback lifecycleCallback : this.f3535P.values()) {
            lifecycleCallback.mo16607b();
        }
    }

    public final void onResume() {
        super.onResume();
        this.f3536Q = 3;
        for (LifecycleCallback lifecycleCallback : this.f3535P.values()) {
            lifecycleCallback.mo16609c();
        }
    }

    public final void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (bundle != null) {
            for (Map.Entry entry : this.f3535P.entrySet()) {
                Bundle bundle2 = new Bundle();
                ((LifecycleCallback) entry.getValue()).mo16608b(bundle2);
                bundle.putBundle((String) entry.getKey(), bundle2);
            }
        }
    }

    public final void onStart() {
        super.onStart();
        this.f3536Q = 2;
        for (LifecycleCallback lifecycleCallback : this.f3535P.values()) {
            lifecycleCallback.mo16610d();
        }
    }

    public final void onStop() {
        super.onStop();
        this.f3536Q = 4;
        for (LifecycleCallback lifecycleCallback : this.f3535P.values()) {
            lifecycleCallback.mo16611e();
        }
    }

    /* renamed from: a */
    public final <T extends LifecycleCallback> T mo16705a(String str, Class<T> cls) {
        return (LifecycleCallback) cls.cast(this.f3535P.get(str));
    }

    /* renamed from: a */
    public final void mo16706a(String str, @NonNull LifecycleCallback lifecycleCallback) {
        if (!this.f3535P.containsKey(str)) {
            this.f3535P.put(str, lifecycleCallback);
            if (this.f3536Q > 0) {
                new C4012d(Looper.getMainLooper()).post(new C2142v2(this, lifecycleCallback, str));
                return;
            }
            return;
        }
        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 59);
        sb.append("LifecycleCallback with tag ");
        sb.append(str);
        sb.append(" already added to this fragment.");
        throw new IllegalArgumentException(sb.toString());
    }

    /* renamed from: a */
    public final Activity mo16704a() {
        return getActivity();
    }
}
