package com.google.android.gms.common.api.internal;

import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.base.C2382h;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.n2 */
final class C2110n2 implements C2082h1 {

    /* renamed from: a */
    private final Context f3400a;

    /* renamed from: b */
    private final C2100l0 f3401b;

    /* renamed from: c */
    private final Looper f3402c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final C2136u0 f3403d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final C2136u0 f3404e;

    /* renamed from: f */
    private final Map<C2016a.C2019c<?>, C2136u0> f3405f;

    /* renamed from: g */
    private final Set<C2099l> f3406g = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: h */
    private final C2016a.C2027f f3407h;

    /* renamed from: i */
    private Bundle f3408i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public ConnectionResult f3409j = null;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public ConnectionResult f3410k = null;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f3411l = false;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public final Lock f3412m;

    /* renamed from: n */
    private int f3413n = 0;

    private C2110n2(Context context, C2100l0 l0Var, Lock lock, Looper looper, C2169c cVar, Map<C2016a.C2019c<?>, C2016a.C2027f> map, Map<C2016a.C2019c<?>, C2016a.C2027f> map2, C2211e eVar, C2016a.C2017a<? extends C4052e, C4047a> aVar, C2016a.C2027f fVar, ArrayList<C2102l2> arrayList, ArrayList<C2102l2> arrayList2, Map<C2016a<?>, Boolean> map3, Map<C2016a<?>, Boolean> map4) {
        this.f3400a = context;
        this.f3401b = l0Var;
        this.f3412m = lock;
        this.f3402c = looper;
        this.f3407h = fVar;
        Context context2 = context;
        Lock lock2 = lock;
        Looper looper2 = looper;
        C2169c cVar2 = cVar;
        C2136u0 u0Var = r3;
        C2136u0 u0Var2 = new C2136u0(context2, this.f3401b, lock2, looper2, cVar2, map2, null, map4, null, arrayList2, new C2118p2(this, null));
        this.f3403d = u0Var;
        this.f3404e = new C2136u0(context2, this.f3401b, lock2, looper2, cVar2, map, eVar, map3, aVar, arrayList, new C2122q2(this, null));
        ArrayMap arrayMap = new ArrayMap();
        for (C2016a.C2019c<?> cVar3 : map2.keySet()) {
            arrayMap.put(cVar3, this.f3403d);
        }
        for (C2016a.C2019c<?> cVar4 : map.keySet()) {
            arrayMap.put(cVar4, this.f3404e);
        }
        this.f3405f = Collections.unmodifiableMap(arrayMap);
    }

    /* renamed from: a */
    public static C2110n2 m5009a(Context context, C2100l0 l0Var, Lock lock, Looper looper, C2169c cVar, Map<C2016a.C2019c<?>, C2016a.C2027f> map, C2211e eVar, Map<C2016a<?>, Boolean> map2, C2016a.C2017a<? extends C4052e, C4047a> aVar, ArrayList<C2102l2> arrayList) {
        Map<C2016a<?>, Boolean> map3 = map2;
        ArrayMap arrayMap = new ArrayMap();
        ArrayMap arrayMap2 = new ArrayMap();
        C2016a.C2027f fVar = null;
        for (Map.Entry entry : map.entrySet()) {
            C2016a.C2027f fVar2 = (C2016a.C2027f) entry.getValue();
            if (fVar2.mo16450d()) {
                fVar = fVar2;
            }
            if (fVar2.mo16538l()) {
                arrayMap.put((C2016a.C2019c) entry.getKey(), fVar2);
            } else {
                arrayMap2.put((C2016a.C2019c) entry.getKey(), fVar2);
            }
        }
        C2258v.m5641b(!arrayMap.isEmpty(), "CompositeGoogleApiClient should not be used without any APIs that require sign-in.");
        ArrayMap arrayMap3 = new ArrayMap();
        ArrayMap arrayMap4 = new ArrayMap();
        for (C2016a aVar2 : map2.keySet()) {
            C2016a.C2019c<?> a = aVar2.mo16521a();
            if (arrayMap.containsKey(a)) {
                arrayMap3.put(aVar2, map3.get(aVar2));
            } else if (arrayMap2.containsKey(a)) {
                arrayMap4.put(aVar2, map3.get(aVar2));
            } else {
                throw new IllegalStateException("Each API in the isOptionalMap must have a corresponding client in the clients map.");
            }
        }
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            C2102l2 l2Var = arrayList.get(i);
            i++;
            C2102l2 l2Var2 = l2Var;
            if (arrayMap3.containsKey(l2Var2.f3384a)) {
                arrayList2.add(l2Var2);
            } else if (arrayMap4.containsKey(l2Var2.f3384a)) {
                arrayList3.add(l2Var2);
            } else {
                throw new IllegalStateException("Each ClientCallbacks must have a corresponding API in the isOptionalMap");
            }
        }
        return new C2110n2(context, l0Var, lock, looper, cVar, arrayMap, arrayMap2, eVar, aVar, fVar, arrayList2, arrayList3, arrayMap3, arrayMap4);
    }

    @Nullable
    /* renamed from: h */
    private final PendingIntent m5025h() {
        if (this.f3407h == null) {
            return null;
        }
        return PendingIntent.getActivity(this.f3400a, System.identityHashCode(this.f3401b), this.f3407h.mo16452k(), 134217728);
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final void m5026i() {
        ConnectionResult connectionResult;
        if (m5019b(this.f3409j)) {
            if (m5019b(this.f3410k) || m5028k()) {
                int i = this.f3413n;
                if (i != 1) {
                    if (i != 2) {
                        Log.wtf("CompositeGAC", "Attempted to call success callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new AssertionError());
                        this.f3413n = 0;
                        return;
                    }
                    this.f3401b.mo16729a(this.f3408i);
                }
                m5027j();
                this.f3413n = 0;
                return;
            }
            ConnectionResult connectionResult2 = this.f3410k;
            if (connectionResult2 == null) {
                return;
            }
            if (this.f3413n == 1) {
                m5027j();
                return;
            }
            m5013a(connectionResult2);
            this.f3403d.mo16709a();
        } else if (this.f3409j == null || !m5019b(this.f3410k)) {
            ConnectionResult connectionResult3 = this.f3409j;
            if (connectionResult3 != null && (connectionResult = this.f3410k) != null) {
                if (this.f3404e.f3481m < this.f3403d.f3481m) {
                    connectionResult3 = connectionResult;
                }
                m5013a(connectionResult3);
            }
        } else {
            this.f3404e.mo16709a();
            m5013a(this.f3409j);
        }
    }

    /* renamed from: j */
    private final void m5027j() {
        for (C2099l lVar : this.f3406g) {
            lVar.onComplete();
        }
        this.f3406g.clear();
    }

    /* renamed from: k */
    private final boolean m5028k() {
        ConnectionResult connectionResult = this.f3410k;
        return connectionResult != null && connectionResult.mo16475c() == 4;
    }

    /* renamed from: b */
    public final void mo16712b() {
        this.f3413n = 2;
        this.f3411l = false;
        this.f3410k = null;
        this.f3409j = null;
        this.f3403d.mo16712b();
        this.f3404e.mo16712b();
    }

    /* renamed from: c */
    public final boolean mo16713c() {
        this.f3412m.lock();
        try {
            boolean z = true;
            if (!this.f3403d.mo16713c() || (!this.f3404e.mo16713c() && !m5028k() && this.f3413n != 1)) {
                z = false;
            }
            return z;
        } finally {
            this.f3412m.unlock();
        }
    }

    /* renamed from: d */
    public final void mo16714d() {
        this.f3403d.mo16714d();
        this.f3404e.mo16714d();
    }

    /* renamed from: e */
    public final void mo16715e() {
        this.f3412m.lock();
        try {
            boolean g = mo16759g();
            this.f3404e.mo16709a();
            this.f3410k = new ConnectionResult(4);
            if (g) {
                new C2382h(this.f3402c).post(new C2114o2(this));
            } else {
                m5027j();
            }
        } finally {
            this.f3412m.unlock();
        }
    }

    /* renamed from: f */
    public final ConnectionResult mo16716f() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: g */
    public final boolean mo16759g() {
        this.f3412m.lock();
        try {
            return this.f3413n == 2;
        } finally {
            this.f3412m.unlock();
        }
    }

    /* renamed from: b */
    private final boolean m5020b(C2056c<? extends C2157k, ? extends C2016a.C2018b> cVar) {
        C2016a.C2019c<? extends C2016a.C2018b> h = cVar.mo16642h();
        C2258v.m5637a(this.f3405f.containsKey(h), "GoogleApiClient is not configured to use the API required for this call.");
        return this.f3405f.get(h).equals(this.f3404e);
    }

    /* renamed from: b */
    private static boolean m5019b(ConnectionResult connectionResult) {
        return connectionResult != null && connectionResult.mo16482w();
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16708a(@NonNull T t) {
        if (!m5020b((C2056c<? extends C2157k, ? extends C2016a.C2018b>) t)) {
            return this.f3403d.mo16708a((C2056c) t);
        }
        if (!m5028k()) {
            return this.f3404e.mo16708a((C2056c) t);
        }
        t.mo16640c(new Status(4, null, m5025h()));
        return t;
    }

    /* renamed from: a */
    public final void mo16709a() {
        this.f3410k = null;
        this.f3409j = null;
        this.f3413n = 0;
        this.f3403d.mo16709a();
        this.f3404e.mo16709a();
        m5027j();
    }

    /* renamed from: a */
    public final boolean mo16711a(C2099l lVar) {
        this.f3412m.lock();
        try {
            if ((mo16759g() || mo16713c()) && !this.f3404e.mo16713c()) {
                this.f3406g.add(lVar);
                if (this.f3413n == 0) {
                    this.f3413n = 1;
                }
                this.f3410k = null;
                this.f3404e.mo16712b();
                return true;
            }
            this.f3412m.unlock();
            return false;
        } finally {
            this.f3412m.unlock();
        }
    }

    /* renamed from: a */
    private final void m5013a(ConnectionResult connectionResult) {
        int i = this.f3413n;
        if (i != 1) {
            if (i != 2) {
                Log.wtf("CompositeGAC", "Attempted to call failure callbacks in CONNECTION_MODE_NONE. Callbacks should be disabled via GmsClientSupervisor", new Exception());
                this.f3413n = 0;
            }
            this.f3401b.mo16730a(connectionResult);
        }
        m5027j();
        this.f3413n = 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5011a(int i, boolean z) {
        this.f3401b.mo16728a(i, z);
        this.f3410k = null;
        this.f3409j = null;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5012a(Bundle bundle) {
        Bundle bundle2 = this.f3408i;
        if (bundle2 == null) {
            this.f3408i = bundle;
        } else if (bundle != null) {
            bundle2.putAll(bundle);
        }
    }

    /* renamed from: a */
    public final void mo16710a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        printWriter.append((CharSequence) str).append((CharSequence) "authClient").println(":");
        this.f3404e.mo16710a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
        printWriter.append((CharSequence) str).append((CharSequence) "anonClient").println(":");
        this.f3403d.mo16710a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
    }
}
