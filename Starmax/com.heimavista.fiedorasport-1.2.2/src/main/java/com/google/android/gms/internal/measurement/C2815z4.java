package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.z4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2815z4<K> implements Iterator<Map.Entry<K, Object>> {

    /* renamed from: P */
    private Iterator<Map.Entry<K, Object>> f4625P;

    public C2815z4(Iterator<Map.Entry<K, Object>> it) {
        this.f4625P = it;
    }

    public final boolean hasNext() {
        return this.f4625P.hasNext();
    }

    public final /* synthetic */ Object next() {
        Map.Entry next = this.f4625P.next();
        return next.getValue() instanceof C2800y4 ? new C2419a5(next) : next;
    }

    public final void remove() {
        this.f4625P.remove();
    }
}
