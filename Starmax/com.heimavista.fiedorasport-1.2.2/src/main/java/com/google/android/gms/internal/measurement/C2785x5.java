package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.x5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2785x5 extends C2769w5, Cloneable {
    /* renamed from: a */
    C2785x5 mo17951a(C2739u5 u5Var);

    /* renamed from: a */
    C2785x5 mo17952a(byte[] bArr);

    /* renamed from: a */
    C2785x5 mo17953a(byte[] bArr, C2798y3 y3Var);

    /* renamed from: r */
    C2739u5 mo17680r();
}
