package com.google.android.gms.common;

import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.common.n */
final /* synthetic */ class C2287n implements Callable {

    /* renamed from: a */
    private final boolean f3780a;

    /* renamed from: b */
    private final String f3781b;

    /* renamed from: c */
    private final C2288o f3782c;

    C2287n(boolean z, String str, C2288o oVar) {
        this.f3780a = z;
        this.f3781b = str;
        this.f3782c = oVar;
    }

    public final Object call() {
        return C2286m.m5688a(this.f3780a, this.f3781b, this.f3782c);
    }
}
