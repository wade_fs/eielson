package com.google.android.exoplayer2.extractor.mkv;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.mkv.a */
/* compiled from: lambda */
public final /* synthetic */ class C1800a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1800a f2813a = new C1800a();

    private /* synthetic */ C1800a() {
    }

    public final Extractor[] createExtractors() {
        return MatroskaExtractor.m4382a();
    }
}
