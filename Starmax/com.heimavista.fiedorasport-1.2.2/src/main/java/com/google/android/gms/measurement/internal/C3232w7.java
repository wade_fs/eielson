package com.google.android.gms.measurement.internal;

import android.content.ComponentName;
import android.os.RemoteException;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.stats.C2303a;
import com.google.android.gms.internal.measurement.C2589kc;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.w7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3232w7 extends C2995c3 {
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final C3144o8 f5723c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C3228w3 f5724d;

    /* renamed from: e */
    private volatile Boolean f5725e;

    /* renamed from: f */
    private final C3039g f5726f;

    /* renamed from: g */
    private final C3109l9 f5727g;

    /* renamed from: h */
    private final List<Runnable> f5728h = new ArrayList();

    /* renamed from: i */
    private final C3039g f5729i;

    protected C3232w7(C3081j5 j5Var) {
        super(j5Var);
        this.f5727g = new C3109l9(j5Var.mo19017o());
        this.f5723c = new C3144o8(this);
        this.f5726f = new C3221v7(this, j5Var);
        this.f5729i = new C3048g8(this, j5Var);
    }

    /* renamed from: I */
    private final boolean m9313I() {
        mo19018r();
        return true;
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: J */
    public final void m9314J() {
        mo18881c();
        this.f5727g.mo19139a();
        this.f5726f.mo19023a(C3135o.f5394G.mo19389a(null).longValue());
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: K */
    public final void m9315K() {
        mo18881c();
        if (mo19368B()) {
            mo19015l().mo18996B().mo19042a("Inactivity, disconnecting from the service");
            mo19374H();
        }
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: L */
    public final void m9316L() {
        mo18881c();
        mo19015l().mo18996B().mo19043a("Processing queued up service tasks", Integer.valueOf(this.f5728h.size()));
        for (Runnable runnable : this.f5728h) {
            try {
                runnable.run();
            } catch (Exception e) {
                mo19015l().mo19001t().mo19043a("Task exception while flushing queue", e);
            }
        }
        this.f5728h.clear();
        this.f5729i.mo19025c();
    }

    /* access modifiers changed from: protected */
    /* renamed from: A */
    public final boolean mo18725A() {
        return false;
    }

    @WorkerThread
    /* renamed from: B */
    public final boolean mo19368B() {
        mo18881c();
        mo18816x();
        return this.f5724d != null;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: C */
    public final void mo19369C() {
        mo18881c();
        mo18816x();
        m9322a(new C3036f8(this, m9319a(true)));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: D */
    public final void mo19370D() {
        mo18881c();
        mo18880a();
        mo18816x();
        zzm a = m9319a(false);
        if (m9313I()) {
            mo18888t().mo18726B();
        }
        m9322a(new C3265z7(this, a));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: E */
    public final void mo19371E() {
        mo18881c();
        mo18816x();
        zzm a = m9319a(true);
        boolean a2 = mo19013h().mo19146a(C3135o.f5483y0);
        if (a2) {
            mo18888t().mo18727C();
        }
        m9322a(new C2976a8(this, a, a2));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x010d  */
    @androidx.annotation.WorkerThread
    /* renamed from: F */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo19372F() {
        /*
            r6 = this;
            r6.mo18881c()
            r6.mo18816x()
            boolean r0 = r6.mo19368B()
            if (r0 == 0) goto L_0x000d
            return
        L_0x000d:
            java.lang.Boolean r0 = r6.f5725e
            r1 = 0
            r2 = 1
            if (r0 != 0) goto L_0x011a
            r6.mo18881c()
            r6.mo18816x()
            com.google.android.gms.measurement.internal.s4 r0 = r6.mo19012g()
            java.lang.Boolean r0 = r0.mo19318w()
            if (r0 == 0) goto L_0x002c
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x002c
            r0 = 1
            goto L_0x0114
        L_0x002c:
            r6.mo19018r()
            com.google.android.gms.measurement.internal.b4 r0 = r6.mo18885p()
            int r0 = r0.mo18805G()
            if (r0 != r2) goto L_0x003d
        L_0x0039:
            r0 = 1
        L_0x003a:
            r3 = 1
            goto L_0x00f1
        L_0x003d:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()
            java.lang.String r3 = "Checking service availability"
            r0.mo19042a(r3)
            com.google.android.gms.measurement.internal.z9 r0 = r6.mo19011f()
            r3 = 12451000(0xbdfcb8, float:1.7447567E-38)
            int r0 = r0.mo19422a(r3)
            if (r0 == 0) goto L_0x00e2
            if (r0 == r2) goto L_0x00d2
            r3 = 2
            if (r0 == r3) goto L_0x00a6
            r3 = 3
            if (r0 == r3) goto L_0x0098
            r3 = 9
            if (r0 == r3) goto L_0x008a
            r3 = 18
            if (r0 == r3) goto L_0x007c
            com.google.android.gms.measurement.internal.f4 r3 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19004w()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r4 = "Unexpected service status"
            r3.mo19043a(r4, r0)
        L_0x0078:
            r0 = 0
        L_0x0079:
            r3 = 0
            goto L_0x00f1
        L_0x007c:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()
            java.lang.String r3 = "Service updating"
            r0.mo19042a(r3)
            goto L_0x0039
        L_0x008a:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()
            java.lang.String r3 = "Service invalid"
            r0.mo19042a(r3)
            goto L_0x0078
        L_0x0098:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19004w()
            java.lang.String r3 = "Service disabled"
            r0.mo19042a(r3)
            goto L_0x0078
        L_0x00a6:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18995A()
            java.lang.String r3 = "Service container out of date"
            r0.mo19042a(r3)
            com.google.android.gms.measurement.internal.z9 r0 = r6.mo19011f()
            int r0 = r0.mo19455v()
            r3 = 17443(0x4423, float:2.4443E-41)
            if (r0 >= r3) goto L_0x00c0
            goto L_0x00df
        L_0x00c0:
            com.google.android.gms.measurement.internal.s4 r0 = r6.mo19012g()
            java.lang.Boolean r0 = r0.mo19318w()
            if (r0 == 0) goto L_0x00d0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0078
        L_0x00d0:
            r0 = 1
            goto L_0x0079
        L_0x00d2:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()
            java.lang.String r3 = "Service missing"
            r0.mo19042a(r3)
        L_0x00df:
            r0 = 0
            goto L_0x003a
        L_0x00e2:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18996B()
            java.lang.String r3 = "Service available"
            r0.mo19042a(r3)
            goto L_0x0039
        L_0x00f1:
            if (r0 != 0) goto L_0x010b
            com.google.android.gms.measurement.internal.la r4 = r6.mo19013h()
            boolean r4 = r4.mo19164u()
            if (r4 == 0) goto L_0x010b
            com.google.android.gms.measurement.internal.f4 r3 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19001t()
            java.lang.String r4 = "No way to upload. Consider using the full version of Analytics"
            r3.mo19042a(r4)
            r3 = 0
        L_0x010b:
            if (r3 == 0) goto L_0x0114
            com.google.android.gms.measurement.internal.s4 r3 = r6.mo19012g()
            r3.mo19308a(r0)
        L_0x0114:
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r6.f5725e = r0
        L_0x011a:
            java.lang.Boolean r0 = r6.f5725e
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0128
            com.google.android.gms.measurement.internal.o8 r0 = r6.f5723c
            r0.mo19205b()
            return
        L_0x0128:
            com.google.android.gms.measurement.internal.la r0 = r6.mo19013h()
            boolean r0 = r0.mo19164u()
            if (r0 != 0) goto L_0x0186
            r6.mo19018r()
            android.content.Context r0 = r6.mo19016n()
            android.content.pm.PackageManager r0 = r0.getPackageManager()
            android.content.Intent r3 = new android.content.Intent
            r3.<init>()
            android.content.Context r4 = r6.mo19016n()
            java.lang.String r5 = "com.google.android.gms.measurement.AppMeasurementService"
            android.content.Intent r3 = r3.setClassName(r4, r5)
            r4 = 65536(0x10000, float:9.18355E-41)
            java.util.List r0 = r0.queryIntentServices(r3, r4)
            if (r0 == 0) goto L_0x015b
            int r0 = r0.size()
            if (r0 <= 0) goto L_0x015b
            r1 = 1
        L_0x015b:
            if (r1 == 0) goto L_0x0179
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r1 = "com.google.android.gms.measurement.START"
            r0.<init>(r1)
            android.content.ComponentName r1 = new android.content.ComponentName
            android.content.Context r2 = r6.mo19016n()
            r6.mo19018r()
            r1.<init>(r2, r5)
            r0.setComponent(r1)
            com.google.android.gms.measurement.internal.o8 r1 = r6.f5723c
            r1.mo19204a(r0)
            return
        L_0x0179:
            com.google.android.gms.measurement.internal.f4 r0 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()
            java.lang.String r1 = "Unable to use remote or local measurement implementation. Please register the AppMeasurementService service in the app manifest"
            r0.mo19042a(r1)
        L_0x0186:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3232w7.mo19372F():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: G */
    public final Boolean mo19373G() {
        return this.f5725e;
    }

    @WorkerThread
    /* renamed from: H */
    public final void mo19374H() {
        mo18881c();
        mo18816x();
        this.f5723c.mo19203a();
        try {
            C2303a.m5745a().mo17123a(mo19016n(), this.f5723c);
        } catch (IllegalArgumentException | IllegalStateException unused) {
        }
        this.f5724d = null;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19381a(C3228w3 w3Var, AbstractSafeParcelable abstractSafeParcelable, zzm zzm) {
        int i;
        List<AbstractSafeParcelable> a;
        mo18881c();
        mo18880a();
        mo18816x();
        boolean I = m9313I();
        int i2 = 0;
        int i3 = 100;
        while (i2 < 1001 && i3 == 100) {
            ArrayList arrayList = new ArrayList();
            if (!I || (a = mo18888t().mo18729a(100)) == null) {
                i = 0;
            } else {
                arrayList.addAll(a);
                i = a.size();
            }
            if (abstractSafeParcelable != null && i < 100) {
                arrayList.add(abstractSafeParcelable);
            }
            int size = arrayList.size();
            int i4 = 0;
            while (i4 < size) {
                Object obj = arrayList.get(i4);
                i4++;
                AbstractSafeParcelable abstractSafeParcelable2 = (AbstractSafeParcelable) obj;
                if (abstractSafeParcelable2 instanceof zzan) {
                    try {
                        w3Var.mo19189a((zzan) abstractSafeParcelable2, zzm);
                    } catch (RemoteException e) {
                        mo19015l().mo19001t().mo19043a("Failed to send event to the service", e);
                    }
                } else if (abstractSafeParcelable2 instanceof zzkq) {
                    try {
                        w3Var.mo19191a((zzkq) abstractSafeParcelable2, zzm);
                    } catch (RemoteException e2) {
                        mo19015l().mo19001t().mo19043a("Failed to send user property to the service", e2);
                    }
                } else if (abstractSafeParcelable2 instanceof zzv) {
                    try {
                        w3Var.mo19194a((zzv) abstractSafeParcelable2, zzm);
                    } catch (RemoteException e3) {
                        mo19015l().mo19001t().mo19043a("Failed to send conditional user property to the service", e3);
                    }
                } else {
                    mo19015l().mo19001t().mo19042a("Discarding data. Unrecognized parcel type.");
                }
            }
            i2++;
            i3 = i;
        }
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19382a(zzan zzan, String str) {
        C2258v.m5629a(zzan);
        mo18881c();
        mo18816x();
        boolean I = m9313I();
        m9322a(new C3072i8(this, I, I && mo18888t().mo18730a(zzan), zzan, m9319a(true), str));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19384a(zzv zzv) {
        C2258v.m5629a(zzv);
        mo18881c();
        mo18816x();
        mo19018r();
        m9322a(new C3060h8(this, true, mo18888t().mo18732a(zzv), new zzv(zzv), m9319a(true), zzv));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19386a(AtomicReference<List<zzv>> atomicReference, String str, String str2, String str3) {
        mo18881c();
        mo18816x();
        m9322a(new C3096k8(this, atomicReference, str, str2, str3, m9319a(false)));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19377a(C2589kc kcVar, String str, String str2) {
        mo18881c();
        mo18816x();
        m9322a(new C3084j8(this, str, str2, m9319a(false), kcVar));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19387a(AtomicReference<List<zzkq>> atomicReference, String str, String str2, String str3, boolean z) {
        mo18881c();
        mo18816x();
        m9322a(new C3120m8(this, atomicReference, str, str2, str3, z, m9319a(false)));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19378a(C2589kc kcVar, String str, String str2, boolean z) {
        mo18881c();
        mo18816x();
        m9322a(new C3108l8(this, str, str2, z, m9319a(false), kcVar));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19383a(zzkq zzkq) {
        mo18881c();
        mo18816x();
        m9322a(new C3243x7(this, m9313I() && mo18888t().mo18731a(zzkq), zzkq, m9319a(true)));
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19385a(AtomicReference<String> atomicReference) {
        mo18881c();
        mo18816x();
        m9322a(new C3254y7(this, atomicReference, m9319a(false)));
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19375a(C2589kc kcVar) {
        mo18881c();
        mo18816x();
        m9322a(new C2988b8(this, m9319a(false), kcVar));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19379a(C3188s7 s7Var) {
        mo18881c();
        mo18816x();
        m9322a(new C3024e8(this, s7Var));
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: a */
    public final void mo19380a(C3228w3 w3Var) {
        mo18881c();
        C2258v.m5629a(w3Var);
        this.f5724d = w3Var;
        m9314J();
        m9316L();
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: a */
    public final void m9320a(ComponentName componentName) {
        mo18881c();
        if (this.f5724d != null) {
            this.f5724d = null;
            mo19015l().mo18996B().mo19043a("Disconnected from device MeasurementService", componentName);
            mo18881c();
            mo19372F();
        }
    }

    @WorkerThread
    /* renamed from: a */
    private final void m9322a(Runnable runnable) {
        mo18881c();
        if (mo19368B()) {
            runnable.run();
        } else if (((long) this.f5728h.size()) >= 1000) {
            mo19015l().mo19001t().mo19042a("Discarding data. Max runnable queue size reached");
        } else {
            this.f5728h.add(runnable);
            this.f5729i.mo19023a(DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS);
            mo19372F();
        }
    }

    @WorkerThread
    @Nullable
    /* renamed from: a */
    private final zzm m9319a(boolean z) {
        mo19018r();
        return mo18885p().mo18807a(z ? mo19015l().mo18997C() : null);
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19376a(C2589kc kcVar, zzan zzan, String str) {
        mo18881c();
        mo18816x();
        if (mo19011f().mo19422a((int) C2176f.f3566a) != 0) {
            mo19015l().mo19004w().mo19042a("Not bundling data. Service unavailable or out of date");
            mo19011f().mo19440a(kcVar, new byte[0]);
            return;
        }
        m9322a(new C3000c8(this, zzan, str, kcVar));
    }
}
