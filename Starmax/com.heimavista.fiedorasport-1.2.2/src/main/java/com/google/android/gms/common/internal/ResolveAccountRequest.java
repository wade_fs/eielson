package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class ResolveAccountRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ResolveAccountRequest> CREATOR = new C2228k0();

    /* renamed from: P */
    private final int f3624P;

    /* renamed from: Q */
    private final Account f3625Q;

    /* renamed from: R */
    private final int f3626R;

    /* renamed from: S */
    private final GoogleSignInAccount f3627S;

    ResolveAccountRequest(int i, Account account, int i2, GoogleSignInAccount googleSignInAccount) {
        this.f3624P = i;
        this.f3625Q = account;
        this.f3626R = i2;
        this.f3627S = googleSignInAccount;
    }

    /* renamed from: c */
    public Account mo16885c() {
        return this.f3625Q;
    }

    /* renamed from: d */
    public int mo16886d() {
        return this.f3626R;
    }

    @Nullable
    /* renamed from: u */
    public GoogleSignInAccount mo16887u() {
        return this.f3627S;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.auth.api.signin.GoogleSignInAccount, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3624P);
        C2250b.m5596a(parcel, 2, (Parcelable) mo16885c(), i, false);
        C2250b.m5591a(parcel, 3, mo16886d());
        C2250b.m5596a(parcel, 4, (Parcelable) mo16887u(), i, false);
        C2250b.m5587a(parcel, a);
    }

    public ResolveAccountRequest(Account account, int i, GoogleSignInAccount googleSignInAccount) {
        this(2, account, i, googleSignInAccount);
    }
}
