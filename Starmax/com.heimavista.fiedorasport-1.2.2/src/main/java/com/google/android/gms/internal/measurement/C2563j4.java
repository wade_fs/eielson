package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.j4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2563j4 implements C2754v5 {

    /* renamed from: a */
    private static final C2563j4 f4252a = new C2563j4();

    private C2563j4() {
    }

    /* renamed from: a */
    public static C2563j4 m6502a() {
        return f4252a;
    }

    /* renamed from: b */
    public final C2707s5 mo17589b(Class<?> cls) {
        Class<C2595l4> cls2 = C2595l4.class;
        if (!cls2.isAssignableFrom(cls)) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalArgumentException(valueOf.length() != 0 ? "Unsupported message type: ".concat(valueOf) : new String("Unsupported message type: "));
        }
        try {
            return (C2707s5) C2595l4.m6640a(cls.asSubclass(cls2)).mo17247a(C2595l4.C2601f.f4292c, (Object) null, (Object) null);
        } catch (Exception e) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new RuntimeException(valueOf2.length() != 0 ? "Unable to get message info for ".concat(valueOf2) : new String("Unable to get message info for "), e);
        }
    }

    /* renamed from: a */
    public final boolean mo17588a(Class<?> cls) {
        return C2595l4.class.isAssignableFrom(cls);
    }
}
