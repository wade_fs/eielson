package com.google.android.gms.common;

import android.content.Context;
import android.content.res.Resources;

/* renamed from: com.google.android.gms.common.e */
public final class C2175e extends C2176f {
    @Deprecated

    /* renamed from: f */
    public static final int f3565f = C2176f.f3566a;

    @Deprecated
    /* renamed from: a */
    public static int m5290a(Context context, int i) {
        return C2176f.m5293a(context, i);
    }

    /* renamed from: c */
    public static Context m5291c(Context context) {
        return C2176f.m5302c(context);
    }

    /* renamed from: d */
    public static Resources m5292d(Context context) {
        return C2176f.m5304d(context);
    }
}
