package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2620m8;

/* renamed from: com.google.android.gms.measurement.internal.y1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3248y1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5766a = new C3248y1();

    private C3248y1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Integer.valueOf((int) C2620m8.m6808e());
    }
}
