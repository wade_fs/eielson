package com.google.android.gms.common.api.internal;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2031c;
import java.util.Collections;
import java.util.Map;
import p119e.p144d.p145a.p157c.p167e.C4057c;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.android.gms.common.api.internal.u2 */
final class C2138u2 implements C4057c<Map<C2063d2<?>, String>> {

    /* renamed from: a */
    private final /* synthetic */ C2130s2 f3484a;

    private C2138u2(C2130s2 s2Var) {
        this.f3484a = s2Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.s2.a(com.google.android.gms.common.api.internal.s2, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.s2, int]
     candidates:
      com.google.android.gms.common.api.internal.s2.a(com.google.android.gms.common.api.internal.s2, com.google.android.gms.common.ConnectionResult):com.google.android.gms.common.ConnectionResult
      com.google.android.gms.common.api.internal.s2.a(com.google.android.gms.common.api.internal.s2, java.util.Map):java.util.Map
      com.google.android.gms.common.api.internal.s2.a(com.google.android.gms.common.api.internal.r2<?>, com.google.android.gms.common.ConnectionResult):boolean
      com.google.android.gms.common.api.internal.s2.a(com.google.android.gms.common.api.internal.s2, boolean):boolean */
    /* renamed from: a */
    public final void mo16766a(@NonNull C4065h<Map<C2063d2<?>, String>> hVar) {
        this.f3484a.f3450f.lock();
        try {
            if (this.f3484a.f3458n) {
                if (hVar.mo23714e()) {
                    Map unused = this.f3484a.f3459o = new ArrayMap(this.f3484a.f3445a.size());
                    for (C2126r2 r2Var : this.f3484a.f3445a.values()) {
                        this.f3484a.f3459o.put(r2Var.mo16559h(), ConnectionResult.f3156T);
                    }
                } else if (hVar.mo23704a() instanceof C2031c) {
                    C2031c cVar = (C2031c) hVar.mo23704a();
                    if (this.f3484a.f3456l) {
                        Map unused2 = this.f3484a.f3459o = new ArrayMap(this.f3484a.f3445a.size());
                        for (C2126r2 r2Var2 : this.f3484a.f3445a.values()) {
                            C2063d2 h = r2Var2.mo16559h();
                            ConnectionResult a = cVar.mo16545a(r2Var2);
                            if (this.f3484a.m5088a(r2Var2, a)) {
                                this.f3484a.f3459o.put(h, new ConnectionResult(16));
                            } else {
                                this.f3484a.f3459o.put(h, a);
                            }
                        }
                    } else {
                        Map unused3 = this.f3484a.f3459o = cVar.mo16544a();
                    }
                    ConnectionResult unused4 = this.f3484a.f3462r = this.f3484a.m5105k();
                } else {
                    Log.e("ConnectionlessGAC", "Unexpected availability exception", hVar.mo23704a());
                    Map unused5 = this.f3484a.f3459o = Collections.emptyMap();
                    ConnectionResult unused6 = this.f3484a.f3462r = new ConnectionResult(8);
                }
                if (this.f3484a.f3460p != null) {
                    this.f3484a.f3459o.putAll(this.f3484a.f3460p);
                    ConnectionResult unused7 = this.f3484a.f3462r = this.f3484a.m5105k();
                }
                if (this.f3484a.f3462r == null) {
                    this.f3484a.m5101i();
                    this.f3484a.m5103j();
                } else {
                    boolean unused8 = this.f3484a.f3458n = false;
                    this.f3484a.f3449e.mo16730a(this.f3484a.f3462r);
                }
                this.f3484a.f3453i.signalAll();
                this.f3484a.f3450f.unlock();
            }
        } finally {
            this.f3484a.f3450f.unlock();
        }
    }
}
