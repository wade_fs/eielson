package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.r */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2685r extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ Long f4456T;

    /* renamed from: U */
    private final /* synthetic */ String f4457U;

    /* renamed from: V */
    private final /* synthetic */ String f4458V;

    /* renamed from: W */
    private final /* synthetic */ Bundle f4459W;

    /* renamed from: X */
    private final /* synthetic */ boolean f4460X;

    /* renamed from: Y */
    private final /* synthetic */ boolean f4461Y;

    /* renamed from: Z */
    private final /* synthetic */ C2525gd f4462Z;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2685r(C2525gd gdVar, Long l, String str, String str2, Bundle bundle, boolean z, boolean z2) {
        super(gdVar);
        this.f4462Z = gdVar;
        this.f4456T = l;
        this.f4457U = str;
        this.f4458V = str2;
        this.f4459W = bundle;
        this.f4460X = z;
        this.f4461Y = z2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        Long l = this.f4456T;
        this.f4462Z.f4192g.logEvent(this.f4457U, this.f4458V, this.f4459W, this.f4460X, this.f4461Y, l == null ? super.f4193P : l.longValue());
    }
}
