package com.google.android.gms.common.internal;

import android.content.Context;
import android.os.IBinder;
import android.os.IInterface;
import android.view.View;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p160b.C3994f;

/* renamed from: com.google.android.gms.common.internal.w */
public final class C2260w extends C3994f<C2244r> {

    /* renamed from: c */
    private static final C2260w f3760c = new C2260w();

    private C2260w() {
        super("com.google.android.gms.common.ui.SignInButtonCreatorImpl");
    }

    /* renamed from: a */
    public static View m5646a(Context context, int i, int i2) {
        return f3760c.m5647b(context, i, i2);
    }

    /* renamed from: b */
    private final View m5647b(Context context, int i, int i2) {
        try {
            SignInButtonConfig signInButtonConfig = new SignInButtonConfig(i, i2, null);
            return (View) C3992d.m11991e(((C2244r) mo23634a(context)).mo16975a(C3992d.m11990a(context), signInButtonConfig));
        } catch (Exception e) {
            StringBuilder sb = new StringBuilder(64);
            sb.append("Could not get button with size ");
            sb.append(i);
            sb.append(" and color ");
            sb.append(i2);
            throw new C3994f.C3995a(sb.toString(), e);
        }
    }

    /* renamed from: a */
    public final C2244r m5649a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ISignInButtonCreator");
        if (queryLocalInterface instanceof C2244r) {
            return (C2244r) queryLocalInterface;
        }
        return new C2218g0(iBinder);
    }
}
