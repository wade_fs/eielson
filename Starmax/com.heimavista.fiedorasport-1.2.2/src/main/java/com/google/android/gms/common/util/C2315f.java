package com.google.android.gms.common.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.common.util.f */
public final class C2315f {
    @Deprecated
    /* renamed from: a */
    public static <T> List<T> m5775a() {
        return Collections.emptyList();
    }

    @Deprecated
    /* renamed from: a */
    public static <T> List<T> m5776a(T t) {
        return Collections.singletonList(t);
    }

    @Deprecated
    /* renamed from: a */
    public static <T> List<T> m5777a(T... tArr) {
        int length = tArr.length;
        if (length == 0) {
            return m5775a();
        }
        if (length != 1) {
            return Collections.unmodifiableList(Arrays.asList(tArr));
        }
        return m5776a(tArr[0]);
    }
}
