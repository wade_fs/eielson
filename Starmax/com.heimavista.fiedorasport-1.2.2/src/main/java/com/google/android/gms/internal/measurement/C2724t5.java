package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.t5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2724t5 {

    /* renamed from: a */
    private static final C2692r5 f4502a = m7319c();

    /* renamed from: b */
    private static final C2692r5 f4503b = new C2677q5();

    /* renamed from: a */
    static C2692r5 m7317a() {
        return f4502a;
    }

    /* renamed from: b */
    static C2692r5 m7318b() {
        return f4503b;
    }

    /* renamed from: c */
    private static C2692r5 m7319c() {
        try {
            return (C2692r5) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
