package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.d3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2465d3 extends C2813z2<Boolean> implements C2738u4<Boolean>, C2485e6, RandomAccess {

    /* renamed from: Q */
    private boolean[] f4048Q;

    /* renamed from: R */
    private int f4049R;

    static {
        new C2465d3(new boolean[0], 0).mo17939e();
    }

    C2465d3() {
        this(new boolean[10], 0);
    }

    /* renamed from: b */
    private final void m6196b(int i) {
        if (i < 0 || i >= this.f4049R) {
            throw new IndexOutOfBoundsException(m6197c(i));
        }
    }

    /* renamed from: c */
    private final String m6197c(int i) {
        int i2 = this.f4049R;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: a */
    public final void mo17402a(boolean z) {
        mo18179b();
        int i = this.f4049R;
        boolean[] zArr = this.f4048Q;
        if (i == zArr.length) {
            boolean[] zArr2 = new boolean[(((i * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            this.f4048Q = zArr2;
        }
        boolean[] zArr3 = this.f4048Q;
        int i2 = this.f4049R;
        this.f4049R = i2 + 1;
        zArr3[i2] = z;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        boolean booleanValue = ((Boolean) obj).booleanValue();
        mo18179b();
        if (i < 0 || i > (i2 = this.f4049R)) {
            throw new IndexOutOfBoundsException(m6197c(i));
        }
        boolean[] zArr = this.f4048Q;
        if (i2 < zArr.length) {
            System.arraycopy(zArr, i, zArr, i + 1, i2 - i);
        } else {
            boolean[] zArr2 = new boolean[(((i2 * 3) / 2) + 1)];
            System.arraycopy(zArr, 0, zArr2, 0, i);
            System.arraycopy(this.f4048Q, i, zArr2, i + 1, this.f4049R - i);
            this.f4048Q = zArr2;
        }
        this.f4048Q[i] = booleanValue;
        this.f4049R++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Boolean> collection) {
        mo18179b();
        C2647o4.m6961a(collection);
        if (!(collection instanceof C2465d3)) {
            return super.addAll(collection);
        }
        C2465d3 d3Var = (C2465d3) collection;
        int i = d3Var.f4049R;
        if (i == 0) {
            return false;
        }
        int i2 = this.f4049R;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            boolean[] zArr = this.f4048Q;
            if (i3 > zArr.length) {
                this.f4048Q = Arrays.copyOf(zArr, i3);
            }
            System.arraycopy(d3Var.f4048Q, 0, this.f4048Q, this.f4049R, d3Var.f4049R);
            this.f4049R = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2465d3)) {
            return super.equals(obj);
        }
        C2465d3 d3Var = (C2465d3) obj;
        if (this.f4049R != d3Var.f4049R) {
            return false;
        }
        boolean[] zArr = d3Var.f4048Q;
        for (int i = 0; i < this.f4049R; i++) {
            if (this.f4048Q[i] != zArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        m6196b(i);
        return Boolean.valueOf(this.f4048Q[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.f4049R; i2++) {
            i = (i * 31) + C2647o4.m6960a(this.f4048Q[i2]);
        }
        return i;
    }

    public final boolean remove(Object obj) {
        mo18179b();
        for (int i = 0; i < this.f4049R; i++) {
            if (obj.equals(Boolean.valueOf(this.f4048Q[i]))) {
                boolean[] zArr = this.f4048Q;
                System.arraycopy(zArr, i + 1, zArr, i, (this.f4049R - i) - 1);
                this.f4049R--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        mo18179b();
        if (i2 >= i) {
            boolean[] zArr = this.f4048Q;
            System.arraycopy(zArr, i2, zArr, i, this.f4049R - i2);
            this.f4049R -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        boolean booleanValue = ((Boolean) obj).booleanValue();
        mo18179b();
        m6196b(i);
        boolean[] zArr = this.f4048Q;
        boolean z = zArr[i];
        zArr[i] = booleanValue;
        return Boolean.valueOf(z);
    }

    public final int size() {
        return this.f4049R;
    }

    private C2465d3(boolean[] zArr, int i) {
        this.f4048Q = zArr;
        this.f4049R = i;
    }

    public final /* synthetic */ Object remove(int i) {
        mo18179b();
        m6196b(i);
        boolean[] zArr = this.f4048Q;
        boolean z = zArr[i];
        int i2 = this.f4049R;
        if (i < i2 - 1) {
            System.arraycopy(zArr, i + 1, zArr, i, (i2 - i) - 1);
        }
        this.f4049R--;
        this.modCount++;
        return Boolean.valueOf(z);
    }

    /* renamed from: a */
    public final /* synthetic */ C2738u4 mo17320a(int i) {
        if (i >= this.f4049R) {
            return new C2465d3(Arrays.copyOf(this.f4048Q, i), this.f4049R);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ boolean add(Object obj) {
        mo17402a(((Boolean) obj).booleanValue());
        return true;
    }
}
