package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.y2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2797y2 {

    /* renamed from: a */
    private static final Class<?> f4588a = m7825a("libcore.io.Memory");

    /* renamed from: b */
    private static final boolean f4589b = (m7825a("org.robolectric.Robolectric") != null);

    /* renamed from: a */
    static boolean m7826a() {
        return f4588a != null && !f4589b;
    }

    /* renamed from: b */
    static Class<?> m7827b() {
        return f4588a;
    }

    /* renamed from: a */
    private static <T> Class<T> m7825a(String str) {
        try {
            return Class.forName(str);
        } catch (Throwable unused) {
            return null;
        }
    }
}
