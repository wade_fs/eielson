package com.google.android.gms.common.api.internal;

import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.collection.ArrayMap;
import androidx.collection.ArraySet;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2033e;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.C2162p;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2229l;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2262x;
import com.google.android.gms.common.util.C2311b;
import com.google.android.gms.common.util.C2323n;
import com.google.android.gms.internal.base.C2382h;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import p119e.p144d.p145a.p157c.p166d.C4052e;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.e */
public class C2064e implements Handler.Callback {

    /* renamed from: c0 */
    public static final Status f3268c0 = new Status(4, "Sign-out occurred while this API call was in progress.");
    /* access modifiers changed from: private */

    /* renamed from: d0 */
    public static final Status f3269d0 = new Status(4, "The user must be signed in to make this API call.");
    /* access modifiers changed from: private */

    /* renamed from: e0 */
    public static final Object f3270e0 = new Object();

    /* renamed from: f0 */
    private static C2064e f3271f0;
    /* access modifiers changed from: private */

    /* renamed from: P */
    public long f3272P = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public long f3273Q = 120000;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public long f3274R = 10000;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public final Context f3275S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public final C2167b f3276T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public final C2229l f3277U;

    /* renamed from: V */
    private final AtomicInteger f3278V = new AtomicInteger(1);

    /* renamed from: W */
    private final AtomicInteger f3279W = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: X */
    public final Map<C2063d2<?>, C2065a<?>> f3280X = new ConcurrentHashMap(5, 0.75f, 1);
    /* access modifiers changed from: private */

    /* renamed from: Y */
    public C2131t f3281Y = null;
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public final Set<C2063d2<?>> f3282Z = new ArraySet();

    /* renamed from: a0 */
    private final Set<C2063d2<?>> f3283a0 = new ArraySet();
    /* access modifiers changed from: private */

    /* renamed from: b0 */
    public final Handler f3284b0;

    /* renamed from: com.google.android.gms.common.api.internal.e$c */
    private class C2067c implements C2121q1, C2197d.C2200c {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final C2016a.C2027f f3300a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final C2063d2<?> f3301b;

        /* renamed from: c */
        private C2231m f3302c = null;

        /* renamed from: d */
        private Set<Scope> f3303d = null;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public boolean f3304e = false;

        public C2067c(C2016a.C2027f fVar, C2063d2<?> d2Var) {
            this.f3300a = fVar;
            this.f3301b = d2Var;
        }

        /* renamed from: a */
        public final void mo16632a(@NonNull ConnectionResult connectionResult) {
            C2064e.this.f3284b0.post(new C2069e1(this, connectionResult));
        }

        @WorkerThread
        /* renamed from: b */
        public final void mo16686b(ConnectionResult connectionResult) {
            ((C2065a) C2064e.this.f3280X.get(this.f3301b)).mo16670b(connectionResult);
        }

        @WorkerThread
        /* renamed from: a */
        public final void mo16685a(C2231m mVar, Set<Scope> set) {
            if (mVar == null || set == null) {
                Log.wtf("GoogleApiManager", "Received null response from onSignInSuccess", new Exception());
                mo16686b(new ConnectionResult(4));
                return;
            }
            this.f3302c = mVar;
            this.f3303d = set;
            m4861a();
        }

        /* access modifiers changed from: private */
        @WorkerThread
        /* renamed from: a */
        public final void m4861a() {
            C2231m mVar;
            if (this.f3304e && (mVar = this.f3302c) != null) {
                this.f3300a.mo16531a(mVar, this.f3303d);
            }
        }
    }

    private C2064e(Context context, Looper looper, C2167b bVar) {
        this.f3275S = context;
        this.f3284b0 = new C2382h(looper, this);
        this.f3276T = bVar;
        this.f3277U = new C2229l(bVar);
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(6));
    }

    /* renamed from: a */
    public static C2064e m4790a(Context context) {
        C2064e eVar;
        synchronized (f3270e0) {
            if (f3271f0 == null) {
                HandlerThread handlerThread = new HandlerThread("GoogleApiHandler", 9);
                handlerThread.start();
                f3271f0 = new C2064e(context.getApplicationContext(), handlerThread.getLooper(), C2167b.m5251a());
            }
            eVar = f3271f0;
        }
        return eVar;
    }

    /* renamed from: d */
    public static void m4795d() {
        synchronized (f3270e0) {
            if (f3271f0 != null) {
                C2064e eVar = f3271f0;
                eVar.f3279W.incrementAndGet();
                eVar.f3284b0.sendMessageAtFrontOfQueue(eVar.f3284b0.obtainMessage(10));
            }
        }
    }

    /* renamed from: e */
    public static C2064e m4796e() {
        C2064e eVar;
        synchronized (f3270e0) {
            C2258v.m5630a(f3271f0, "Must guarantee manager is non-null before using getInstance");
            eVar = f3271f0;
        }
        return eVar;
    }

    /* renamed from: b */
    public final int mo16659b() {
        return this.f3278V.getAndIncrement();
    }

    /* renamed from: c */
    public final void mo16662c() {
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.e.a.a(com.google.android.gms.common.api.internal.e$a, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.e$a<?>, int]
     candidates:
      com.google.android.gms.common.api.internal.e.a.a(com.google.android.gms.common.api.internal.e$a, com.google.android.gms.common.api.internal.e$b):void
      com.google.android.gms.common.api.internal.e.a.a(com.google.android.gms.common.api.internal.e$a, boolean):boolean */
    @WorkerThread
    public boolean handleMessage(Message message) {
        C2065a aVar;
        int i = message.what;
        long j = 300000;
        switch (i) {
            case 1:
                if (((Boolean) message.obj).booleanValue()) {
                    j = 10000;
                }
                this.f3274R = j;
                this.f3284b0.removeMessages(12);
                for (C2063d2<?> d2Var : this.f3280X.keySet()) {
                    Handler handler = this.f3284b0;
                    handler.sendMessageDelayed(handler.obtainMessage(12, d2Var), this.f3274R);
                }
                break;
            case 2:
                C2075f2 f2Var = (C2075f2) message.obj;
                Iterator<C2063d2<?>> it = f2Var.mo16694b().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    } else {
                        C2063d2 next = it.next();
                        C2065a aVar2 = this.f3280X.get(next);
                        if (aVar2 == null) {
                            f2Var.mo16693a(next, new ConnectionResult(13), null);
                            break;
                        } else if (aVar2.mo16671c()) {
                            f2Var.mo16693a(next, ConnectionResult.f3156T, aVar2.mo16674f().mo16535f());
                        } else if (aVar2.mo16679k() != null) {
                            f2Var.mo16693a(next, aVar2.mo16679k(), null);
                        } else {
                            aVar2.mo16667a(f2Var);
                            aVar2.mo16664a();
                        }
                    }
                }
            case 3:
                for (C2065a aVar3 : this.f3280X.values()) {
                    aVar3.mo16678j();
                    aVar3.mo16664a();
                }
                break;
            case 4:
            case 8:
            case 13:
                C2093j1 j1Var = (C2093j1) message.obj;
                C2065a aVar4 = this.f3280X.get(j1Var.f3350c.mo16559h());
                if (aVar4 == null) {
                    m4792b(j1Var.f3350c);
                    aVar4 = this.f3280X.get(j1Var.f3350c.mo16559h());
                }
                if (aVar4.mo16672d() && this.f3279W.get() != j1Var.f3349b) {
                    j1Var.f3348a.mo16614a(f3268c0);
                    aVar4.mo16676h();
                    break;
                } else {
                    aVar4.mo16668a(j1Var.f3348a);
                    break;
                }
                break;
            case 5:
                int i2 = message.arg1;
                ConnectionResult connectionResult = (ConnectionResult) message.obj;
                Iterator<C2065a<?>> it2 = this.f3280X.values().iterator();
                while (true) {
                    if (it2.hasNext()) {
                        aVar = it2.next();
                        if (aVar.mo16669b() == i2) {
                        }
                    } else {
                        aVar = null;
                    }
                }
                if (aVar == null) {
                    StringBuilder sb = new StringBuilder(76);
                    sb.append("Could not find API instance ");
                    sb.append(i2);
                    sb.append(" while trying to fail enqueued calls.");
                    Log.wtf("GoogleApiManager", sb.toString(), new Exception());
                    break;
                } else {
                    String b = this.f3276T.mo16829b(connectionResult.mo16475c());
                    String d = connectionResult.mo16476d();
                    StringBuilder sb2 = new StringBuilder(String.valueOf(b).length() + 69 + String.valueOf(d).length());
                    sb2.append("Error resolution was canceled by the user, original error message: ");
                    sb2.append(b);
                    sb2.append(": ");
                    sb2.append(d);
                    aVar.mo16666a(new Status(17, sb2.toString()));
                    break;
                }
            case 6:
                if (C2323n.m5792a() && (this.f3275S.getApplicationContext() instanceof Application)) {
                    C2051b.m4757a((Application) this.f3275S.getApplicationContext());
                    C2051b.m4758b().mo16618a(new C2151y0(this));
                    if (!C2051b.m4758b().mo16620a(true)) {
                        this.f3274R = 300000;
                        break;
                    }
                }
                break;
            case 7:
                m4792b((C2033e) message.obj);
                break;
            case 9:
                if (this.f3280X.containsKey(message.obj)) {
                    this.f3280X.get(message.obj).mo16673e();
                    break;
                }
                break;
            case 10:
                for (C2063d2<?> d2Var2 : this.f3283a0) {
                    this.f3280X.remove(d2Var2).mo16676h();
                }
                this.f3283a0.clear();
                break;
            case 11:
                if (this.f3280X.containsKey(message.obj)) {
                    this.f3280X.get(message.obj).mo16675g();
                    break;
                }
                break;
            case 12:
                if (this.f3280X.containsKey(message.obj)) {
                    this.f3280X.get(message.obj).mo16680l();
                    break;
                }
                break;
            case 14:
                C2135u uVar = (C2135u) message.obj;
                C2063d2<?> b2 = uVar.mo16781b();
                if (this.f3280X.containsKey(b2)) {
                    uVar.mo16780a().mo23720a(Boolean.valueOf(((C2065a) this.f3280X.get(b2)).m4823a(false)));
                    break;
                } else {
                    uVar.mo16780a().mo23720a((Boolean) false);
                    break;
                }
            case 15:
                C2066b bVar = (C2066b) message.obj;
                if (this.f3280X.containsKey(bVar.f3298a)) {
                    this.f3280X.get(bVar.f3298a).m4821a(bVar);
                    break;
                }
                break;
            case 16:
                C2066b bVar2 = (C2066b) message.obj;
                if (this.f3280X.containsKey(bVar2.f3298a)) {
                    this.f3280X.get(bVar2.f3298a).m4826b(bVar2);
                    break;
                }
                break;
            default:
                StringBuilder sb3 = new StringBuilder(31);
                sb3.append("Unknown message id: ");
                sb3.append(i);
                Log.w("GoogleApiManager", sb3.toString());
                return false;
        }
        return true;
    }

    @WorkerThread
    /* renamed from: b */
    private final void m4792b(C2033e<?> eVar) {
        C2063d2<?> h = eVar.mo16559h();
        C2065a aVar = this.f3280X.get(h);
        if (aVar == null) {
            aVar = new C2065a(eVar);
            this.f3280X.put(h, aVar);
        }
        if (aVar.mo16672d()) {
            this.f3283a0.add(h);
        }
        aVar.mo16664a();
    }

    /* renamed from: com.google.android.gms.common.api.internal.e$a */
    public class C2065a<O extends C2016a.C2020d> implements C2036f.C2038b, C2036f.C2039c, C2106m2 {

        /* renamed from: a */
        private final Queue<C2116p0> f3285a = new LinkedList();
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final C2016a.C2027f f3286b;

        /* renamed from: c */
        private final C2016a.C2018b f3287c;

        /* renamed from: d */
        private final C2063d2<O> f3288d;

        /* renamed from: e */
        private final C2123r f3289e;

        /* renamed from: f */
        private final Set<C2075f2> f3290f = new HashSet();

        /* renamed from: g */
        private final Map<C2084i.C2085a<?>, C2097k1> f3291g = new HashMap();

        /* renamed from: h */
        private final int f3292h;

        /* renamed from: i */
        private final C2109n1 f3293i;

        /* renamed from: j */
        private boolean f3294j;

        /* renamed from: k */
        private final List<C2066b> f3295k = new ArrayList();

        /* renamed from: l */
        private ConnectionResult f3296l = null;

        @WorkerThread
        public C2065a(C2033e<O> eVar) {
            this.f3286b = eVar.mo16547a(C2064e.this.f3284b0.getLooper(), this);
            C2016a.C2027f fVar = this.f3286b;
            if (fVar instanceof C2262x) {
                this.f3287c = ((C2262x) fVar).mo16448D();
            } else {
                this.f3287c = fVar;
            }
            this.f3288d = eVar.mo16559h();
            this.f3289e = new C2123r();
            this.f3292h = eVar.mo16557f();
            if (this.f3286b.mo16538l()) {
                this.f3293i = eVar.mo16550a(C2064e.this.f3275S, C2064e.this.f3284b0);
            } else {
                this.f3293i = null;
            }
        }

        @WorkerThread
        /* renamed from: c */
        private final boolean m4830c(@NonNull ConnectionResult connectionResult) {
            synchronized (C2064e.f3270e0) {
                if (C2064e.this.f3281Y == null || !C2064e.this.f3282Z.contains(this.f3288d)) {
                    return false;
                }
                C2064e.this.f3281Y.mo16701b(connectionResult, this.f3292h);
                return true;
            }
        }

        @WorkerThread
        /* renamed from: d */
        private final void m4831d(ConnectionResult connectionResult) {
            for (C2075f2 f2Var : this.f3290f) {
                String str = null;
                if (C2251t.m5617a(connectionResult, ConnectionResult.f3156T)) {
                    str = this.f3286b.mo16535f();
                }
                f2Var.mo16693a(this.f3288d, connectionResult, str);
            }
            this.f3290f.clear();
        }

        /* access modifiers changed from: private */
        @WorkerThread
        /* renamed from: n */
        public final void m4832n() {
            mo16678j();
            m4831d(ConnectionResult.f3156T);
            m4835q();
            Iterator<C2097k1> it = this.f3291g.values().iterator();
            while (it.hasNext()) {
                C2097k1 next = it.next();
                if (m4818a(next.f3357a.mo16737c()) != null) {
                    it.remove();
                } else {
                    try {
                        next.f3357a.mo16735a(this.f3287c, new C4066i());
                    } catch (DeadObjectException unused) {
                        mo16583L(1);
                        this.f3286b.mo16528a();
                    } catch (RemoteException unused2) {
                        it.remove();
                    }
                }
            }
            m4834p();
            m4836r();
        }

        /* access modifiers changed from: private */
        @WorkerThread
        /* renamed from: o */
        public final void m4833o() {
            mo16678j();
            this.f3294j = true;
            this.f3289e.mo16771c();
            C2064e.this.f3284b0.sendMessageDelayed(Message.obtain(C2064e.this.f3284b0, 9, this.f3288d), C2064e.this.f3272P);
            C2064e.this.f3284b0.sendMessageDelayed(Message.obtain(C2064e.this.f3284b0, 11, this.f3288d), C2064e.this.f3273Q);
            C2064e.this.f3277U.mo17006a();
        }

        @WorkerThread
        /* renamed from: p */
        private final void m4834p() {
            ArrayList arrayList = new ArrayList(this.f3285a);
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Object obj = arrayList.get(i);
                i++;
                C2116p0 p0Var = (C2116p0) obj;
                if (!this.f3286b.mo16533c()) {
                    return;
                }
                if (m4827b(p0Var)) {
                    this.f3285a.remove(p0Var);
                }
            }
        }

        @WorkerThread
        /* renamed from: q */
        private final void m4835q() {
            if (this.f3294j) {
                C2064e.this.f3284b0.removeMessages(11, this.f3288d);
                C2064e.this.f3284b0.removeMessages(9, this.f3288d);
                this.f3294j = false;
            }
        }

        /* renamed from: r */
        private final void m4836r() {
            C2064e.this.f3284b0.removeMessages(12, this.f3288d);
            C2064e.this.f3284b0.sendMessageDelayed(C2064e.this.f3284b0.obtainMessage(12, this.f3288d), C2064e.this.f3274R);
        }

        /* renamed from: L */
        public final void mo16583L(int i) {
            if (Looper.myLooper() == C2064e.this.f3284b0.getLooper()) {
                m4833o();
            } else {
                C2064e.this.f3284b0.post(new C2049a1(this));
            }
        }

        /* renamed from: a */
        public final void mo16665a(ConnectionResult connectionResult, C2016a<?> aVar, boolean z) {
            if (Looper.myLooper() == C2064e.this.f3284b0.getLooper()) {
                mo16585a(connectionResult);
            } else {
                C2064e.this.f3284b0.post(new C2054b1(this, connectionResult));
            }
        }

        @WorkerThread
        /* renamed from: b */
        public final void mo16670b(@NonNull ConnectionResult connectionResult) {
            C2258v.m5633a(C2064e.this.f3284b0);
            this.f3286b.mo16528a();
            mo16585a(connectionResult);
        }

        @WorkerThread
        /* renamed from: e */
        public final void mo16673e() {
            C2258v.m5633a(C2064e.this.f3284b0);
            if (this.f3294j) {
                mo16664a();
            }
        }

        /* renamed from: f */
        public final void mo16584f(@Nullable Bundle bundle) {
            if (Looper.myLooper() == C2064e.this.f3284b0.getLooper()) {
                m4832n();
            } else {
                C2064e.this.f3284b0.post(new C2154z0(this));
            }
        }

        @WorkerThread
        /* renamed from: g */
        public final void mo16675g() {
            Status status;
            C2258v.m5633a(C2064e.this.f3284b0);
            if (this.f3294j) {
                m4835q();
                if (C2064e.this.f3276T.mo16831c(C2064e.this.f3275S) == 18) {
                    status = new Status(8, "Connection timed out while waiting for Google Play services update to complete.");
                } else {
                    status = new Status(8, "API failed to connect while resuming due to an unknown error.");
                }
                mo16666a(status);
                this.f3286b.mo16528a();
            }
        }

        @WorkerThread
        /* renamed from: h */
        public final void mo16676h() {
            C2258v.m5633a(C2064e.this.f3284b0);
            mo16666a(C2064e.f3268c0);
            this.f3289e.mo16770b();
            for (C2084i.C2085a aVar : (C2084i.C2085a[]) this.f3291g.keySet().toArray(new C2084i.C2085a[this.f3291g.size()])) {
                mo16668a(new C2059c2(aVar, new C4066i()));
            }
            m4831d(new ConnectionResult(4));
            if (this.f3286b.mo16533c()) {
                this.f3286b.mo16530a(new C2058c1(this));
            }
        }

        /* renamed from: i */
        public final Map<C2084i.C2085a<?>, C2097k1> mo16677i() {
            return this.f3291g;
        }

        @WorkerThread
        /* renamed from: j */
        public final void mo16678j() {
            C2258v.m5633a(C2064e.this.f3284b0);
            this.f3296l = null;
        }

        @WorkerThread
        /* renamed from: k */
        public final ConnectionResult mo16679k() {
            C2258v.m5633a(C2064e.this.f3284b0);
            return this.f3296l;
        }

        @WorkerThread
        /* renamed from: l */
        public final boolean mo16680l() {
            return m4823a(true);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: m */
        public final C4052e mo16681m() {
            C2109n1 n1Var = this.f3293i;
            if (n1Var == null) {
                return null;
            }
            return n1Var.mo16756H();
        }

        @WorkerThread
        /* renamed from: b */
        private final boolean m4827b(C2116p0 p0Var) {
            if (!(p0Var instanceof C2101l1)) {
                m4829c(p0Var);
                return true;
            }
            C2101l1 l1Var = (C2101l1) p0Var;
            Feature a = m4818a(l1Var.mo16634b(this));
            if (a == null) {
                m4829c(p0Var);
                return true;
            } else if (l1Var.mo16635c(this)) {
                C2066b bVar = new C2066b(this.f3288d, a, null);
                int indexOf = this.f3295k.indexOf(bVar);
                if (indexOf >= 0) {
                    C2066b bVar2 = this.f3295k.get(indexOf);
                    C2064e.this.f3284b0.removeMessages(15, bVar2);
                    C2064e.this.f3284b0.sendMessageDelayed(Message.obtain(C2064e.this.f3284b0, 15, bVar2), C2064e.this.f3272P);
                    return false;
                }
                this.f3295k.add(bVar);
                C2064e.this.f3284b0.sendMessageDelayed(Message.obtain(C2064e.this.f3284b0, 15, bVar), C2064e.this.f3272P);
                C2064e.this.f3284b0.sendMessageDelayed(Message.obtain(C2064e.this.f3284b0, 16, bVar), C2064e.this.f3273Q);
                ConnectionResult connectionResult = new ConnectionResult(2, null);
                if (m4830c(connectionResult)) {
                    return false;
                }
                C2064e.this.mo16661b(connectionResult, this.f3292h);
                return false;
            } else {
                l1Var.mo16617a(new C2162p(a));
                return false;
            }
        }

        @WorkerThread
        /* renamed from: a */
        public final void mo16585a(@NonNull ConnectionResult connectionResult) {
            C2258v.m5633a(C2064e.this.f3284b0);
            C2109n1 n1Var = this.f3293i;
            if (n1Var != null) {
                n1Var.mo16757I();
            }
            mo16678j();
            C2064e.this.f3277U.mo17006a();
            m4831d(connectionResult);
            if (connectionResult.mo16475c() == 4) {
                mo16666a(C2064e.f3269d0);
            } else if (this.f3285a.isEmpty()) {
                this.f3296l = connectionResult;
            } else if (!m4830c(connectionResult) && !C2064e.this.mo16661b(connectionResult, this.f3292h)) {
                if (connectionResult.mo16475c() == 18) {
                    this.f3294j = true;
                }
                if (this.f3294j) {
                    C2064e.this.f3284b0.sendMessageDelayed(Message.obtain(C2064e.this.f3284b0, 9, this.f3288d), C2064e.this.f3272P);
                    return;
                }
                String a = this.f3288d.mo16647a();
                StringBuilder sb = new StringBuilder(String.valueOf(a).length() + 38);
                sb.append("API: ");
                sb.append(a);
                sb.append(" is not available on this device.");
                mo16666a(new Status(17, sb.toString()));
            }
        }

        /* renamed from: f */
        public final C2016a.C2027f mo16674f() {
            return this.f3286b;
        }

        /* renamed from: d */
        public final boolean mo16672d() {
            return this.f3286b.mo16538l();
        }

        @WorkerThread
        /* renamed from: c */
        private final void m4829c(C2116p0 p0Var) {
            p0Var.mo16616a(this.f3289e, mo16672d());
            try {
                p0Var.mo16615a(this);
            } catch (DeadObjectException unused) {
                mo16583L(1);
                this.f3286b.mo16528a();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public final boolean mo16671c() {
            return this.f3286b.mo16533c();
        }

        @WorkerThread
        /* renamed from: a */
        public final void mo16668a(C2116p0 p0Var) {
            C2258v.m5633a(C2064e.this.f3284b0);
            if (!this.f3286b.mo16533c()) {
                this.f3285a.add(p0Var);
                ConnectionResult connectionResult = this.f3296l;
                if (connectionResult == null || !connectionResult.mo16481v()) {
                    mo16664a();
                } else {
                    mo16585a(this.f3296l);
                }
            } else if (m4827b(p0Var)) {
                m4836r();
            } else {
                this.f3285a.add(p0Var);
            }
        }

        /* renamed from: b */
        public final int mo16669b() {
            return this.f3292h;
        }

        /* access modifiers changed from: private */
        @WorkerThread
        /* renamed from: b */
        public final void m4826b(C2066b bVar) {
            Feature[] b;
            if (this.f3295k.remove(bVar)) {
                C2064e.this.f3284b0.removeMessages(15, bVar);
                C2064e.this.f3284b0.removeMessages(16, bVar);
                Feature b2 = bVar.f3299b;
                ArrayList arrayList = new ArrayList(this.f3285a.size());
                for (C2116p0 p0Var : this.f3285a) {
                    if ((p0Var instanceof C2101l1) && (b = ((C2101l1) p0Var).mo16634b(this)) != null && C2311b.m5766a(b, b2)) {
                        arrayList.add(p0Var);
                    }
                }
                int size = arrayList.size();
                int i = 0;
                while (i < size) {
                    Object obj = arrayList.get(i);
                    i++;
                    C2116p0 p0Var2 = (C2116p0) obj;
                    this.f3285a.remove(p0Var2);
                    p0Var2.mo16617a(new C2162p(b2));
                }
            }
        }

        @WorkerThread
        /* renamed from: a */
        public final void mo16666a(Status status) {
            C2258v.m5633a(C2064e.this.f3284b0);
            for (C2116p0 p0Var : this.f3285a) {
                p0Var.mo16614a(status);
            }
            this.f3285a.clear();
        }

        /* access modifiers changed from: private */
        @WorkerThread
        /* renamed from: a */
        public final boolean m4823a(boolean z) {
            C2258v.m5633a(C2064e.this.f3284b0);
            if (!this.f3286b.mo16533c() || this.f3291g.size() != 0) {
                return false;
            }
            if (this.f3289e.mo16769a()) {
                if (z) {
                    m4836r();
                }
                return false;
            }
            this.f3286b.mo16528a();
            return true;
        }

        @WorkerThread
        /* renamed from: a */
        public final void mo16664a() {
            C2258v.m5633a(C2064e.this.f3284b0);
            if (!this.f3286b.mo16533c() && !this.f3286b.mo16534e()) {
                int a = C2064e.this.f3277U.mo17005a(C2064e.this.f3275S, this.f3286b);
                if (a != 0) {
                    mo16585a(new ConnectionResult(a, null));
                    return;
                }
                C2067c cVar = new C2067c(this.f3286b, this.f3288d);
                if (this.f3286b.mo16538l()) {
                    this.f3293i.mo16758a(cVar);
                }
                this.f3286b.mo16529a(cVar);
            }
        }

        @WorkerThread
        /* renamed from: a */
        public final void mo16667a(C2075f2 f2Var) {
            C2258v.m5633a(C2064e.this.f3284b0);
            this.f3290f.add(f2Var);
        }

        @WorkerThread
        @Nullable
        /* renamed from: a */
        private final Feature m4818a(@Nullable Feature[] featureArr) {
            if (!(featureArr == null || featureArr.length == 0)) {
                Feature[] j = this.f3286b.mo16537j();
                if (j == null) {
                    j = new Feature[0];
                }
                ArrayMap arrayMap = new ArrayMap(j.length);
                for (Feature feature : j) {
                    arrayMap.put(feature.mo16487c(), Long.valueOf(feature.mo16488d()));
                }
                for (Feature feature2 : featureArr) {
                    if (!arrayMap.containsKey(feature2.mo16487c()) || ((Long) arrayMap.get(feature2.mo16487c())).longValue() < feature2.mo16488d()) {
                        return feature2;
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: private */
        @WorkerThread
        /* renamed from: a */
        public final void m4821a(C2066b bVar) {
            if (!this.f3295k.contains(bVar) || this.f3294j) {
                return;
            }
            if (!this.f3286b.mo16533c()) {
                mo16664a();
            } else {
                m4834p();
            }
        }
    }

    /* renamed from: com.google.android.gms.common.api.internal.e$b */
    private static class C2066b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final C2063d2<?> f3298a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final Feature f3299b;

        private C2066b(C2063d2<?> d2Var, Feature feature) {
            this.f3298a = d2Var;
            this.f3299b = feature;
        }

        public final boolean equals(Object obj) {
            if (obj != null && (obj instanceof C2066b)) {
                C2066b bVar = (C2066b) obj;
                if (!C2251t.m5617a(this.f3298a, bVar.f3298a) || !C2251t.m5617a(this.f3299b, bVar.f3299b)) {
                    return false;
                }
                return true;
            }
            return false;
        }

        public final int hashCode() {
            return C2251t.m5615a(this.f3298a, this.f3299b);
        }

        public final String toString() {
            C2251t.C2252a a = C2251t.m5616a(this);
            a.mo17037a("key", this.f3298a);
            a.mo17037a("feature", this.f3299b);
            return a.toString();
        }

        /* synthetic */ C2066b(C2063d2 d2Var, Feature feature, C2151y0 y0Var) {
            this(d2Var, feature);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final void mo16660b(@NonNull C2131t tVar) {
        synchronized (f3270e0) {
            if (this.f3281Y == tVar) {
                this.f3281Y = null;
                this.f3282Z.clear();
            }
        }
    }

    /* renamed from: a */
    public final void mo16656a(C2033e<?> eVar) {
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(7, eVar));
    }

    /* renamed from: a */
    public final void mo16658a(@NonNull C2131t tVar) {
        synchronized (f3270e0) {
            if (this.f3281Y != tVar) {
                this.f3281Y = tVar;
                this.f3282Z.clear();
            }
            this.f3282Z.addAll(tVar.mo16778h());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo16661b(ConnectionResult connectionResult, int i) {
        return this.f3276T.mo16828a(this.f3275S, connectionResult, i);
    }

    /* renamed from: a */
    public final C4065h<Map<C2063d2<?>, String>> mo16653a(Iterable<? extends C2033e<?>> iterable) {
        C2075f2 f2Var = new C2075f2(iterable);
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(2, f2Var));
        return f2Var.mo16692a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo16654a() {
        this.f3279W.incrementAndGet();
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(10));
    }

    /* renamed from: a */
    public final <O extends C2016a.C2020d> void mo16657a(C2033e eVar, int i, C2056c<? extends C2157k, C2016a.C2018b> cVar) {
        C2050a2 a2Var = new C2050a2(i, cVar);
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(4, new C2093j1(a2Var, this.f3279W.get(), eVar)));
    }

    /* renamed from: a */
    public final <O extends C2016a.C2020d> C4065h<Void> mo16652a(@NonNull C2033e eVar, @NonNull C2095k<C2016a.C2018b, ?> kVar, @NonNull C2115p<C2016a.C2018b, ?> pVar) {
        C4066i iVar = new C4066i();
        C2055b2 b2Var = new C2055b2(new C2097k1(kVar, pVar), iVar);
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(8, new C2093j1(b2Var, this.f3279W.get(), eVar)));
        return iVar.mo23718a();
    }

    /* renamed from: a */
    public final <O extends C2016a.C2020d> C4065h<Boolean> mo16651a(@NonNull C2033e eVar, @NonNull C2084i.C2085a<?> aVar) {
        C4066i iVar = new C4066i();
        C2059c2 c2Var = new C2059c2(aVar, iVar);
        Handler handler = this.f3284b0;
        handler.sendMessage(handler.obtainMessage(13, new C2093j1(c2Var, this.f3279W.get(), eVar)));
        return iVar.mo23718a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final PendingIntent mo16650a(C2063d2<?> d2Var, int i) {
        C4052e m;
        C2065a aVar = this.f3280X.get(d2Var);
        if (aVar == null || (m = aVar.mo16681m()) == null) {
            return null;
        }
        return PendingIntent.getActivity(this.f3275S, i, m.mo16452k(), 134217728);
    }

    /* renamed from: a */
    public final void mo16655a(ConnectionResult connectionResult, int i) {
        if (!mo16661b(connectionResult, i)) {
            Handler handler = this.f3284b0;
            handler.sendMessage(handler.obtainMessage(5, i, 0, connectionResult));
        }
    }
}
