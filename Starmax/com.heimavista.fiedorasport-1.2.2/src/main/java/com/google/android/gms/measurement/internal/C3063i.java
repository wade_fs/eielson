package com.google.android.gms.measurement.internal;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.content.pm.PackageManager;
import androidx.annotation.WorkerThread;
import androidx.core.content.ContextCompat;
import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/* renamed from: com.google.android.gms.measurement.internal.i */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3063i extends C3010d6 {

    /* renamed from: c */
    private long f5193c;

    /* renamed from: d */
    private String f5194d;

    /* renamed from: e */
    private Boolean f5195e;

    /* renamed from: f */
    private AccountManager f5196f;

    /* renamed from: g */
    private Boolean f5197g;

    /* renamed from: h */
    private long f5198h;

    C3063i(C3081j5 j5Var) {
        super(j5Var);
    }

    /* renamed from: a */
    public final boolean mo19058a(Context context) {
        if (this.f5195e == null) {
            mo19018r();
            this.f5195e = false;
            try {
                PackageManager packageManager = context.getPackageManager();
                if (packageManager != null) {
                    packageManager.getPackageInfo("com.google.android.gms", 128);
                    this.f5195e = true;
                }
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return this.f5195e.booleanValue();
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final boolean mo18825q() {
        Calendar instance = Calendar.getInstance();
        this.f5193c = TimeUnit.MINUTES.convert((long) (instance.get(15) + instance.get(16)), TimeUnit.MILLISECONDS);
        Locale locale = Locale.getDefault();
        String lowerCase = locale.getLanguage().toLowerCase(Locale.ENGLISH);
        String lowerCase2 = locale.getCountry().toLowerCase(Locale.ENGLISH);
        StringBuilder sb = new StringBuilder(String.valueOf(lowerCase).length() + 1 + String.valueOf(lowerCase2).length());
        sb.append(lowerCase);
        sb.append("-");
        sb.append(lowerCase2);
        this.f5194d = sb.toString();
        return false;
    }

    /* renamed from: t */
    public final long mo19059t() {
        mo18903k();
        return this.f5193c;
    }

    /* renamed from: u */
    public final String mo19060u() {
        mo18903k();
        return this.f5194d;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: v */
    public final long mo19061v() {
        mo18881c();
        return this.f5198h;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: w */
    public final void mo19062w() {
        mo18881c();
        this.f5197g = null;
        this.f5198h = 0;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: x */
    public final boolean mo19063x() {
        mo18881c();
        long a = mo19017o().mo17132a();
        if (a - this.f5198h > 86400000) {
            this.f5197g = null;
        }
        Boolean bool = this.f5197g;
        if (bool != null) {
            return bool.booleanValue();
        }
        if (ContextCompat.checkSelfPermission(mo19016n(), "android.permission.GET_ACCOUNTS") != 0) {
            mo19015l().mo19005x().mo19042a("Permission error checking for dasher/unicorn accounts");
            this.f5198h = a;
            this.f5197g = false;
            return false;
        }
        if (this.f5196f == null) {
            this.f5196f = AccountManager.get(mo19016n());
        }
        try {
            Account[] result = this.f5196f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_HOSTED"}, null, null).getResult();
            if (result == null || result.length <= 0) {
                Account[] result2 = this.f5196f.getAccountsByTypeAndFeatures("com.google", new String[]{"service_uca"}, null, null).getResult();
                if (result2 != null && result2.length > 0) {
                    this.f5197g = true;
                    this.f5198h = a;
                    return true;
                }
                this.f5198h = a;
                this.f5197g = false;
                return false;
            }
            this.f5197g = true;
            this.f5198h = a;
            return true;
        } catch (AuthenticatorException | OperationCanceledException | IOException e) {
            mo19015l().mo19002u().mo19043a("Exception checking account types", e);
        }
    }
}
