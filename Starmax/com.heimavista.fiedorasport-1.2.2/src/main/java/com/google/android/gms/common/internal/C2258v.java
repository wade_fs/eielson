package com.google.android.gms.common.internal;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.util.C2334v;

/* renamed from: com.google.android.gms.common.internal.v */
public final class C2258v {
    @NonNull
    /* renamed from: a */
    public static <T> T m5629a(@Nullable Object obj) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException("null reference");
    }

    /* renamed from: b */
    public static String m5639b(String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException("Given String is empty or null");
    }

    /* renamed from: c */
    public static void m5643c(String str) {
        if (C2334v.m5825a()) {
            throw new IllegalStateException(str);
        }
    }

    /* renamed from: a */
    public static String m5631a(String str, Object obj) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException(String.valueOf(obj));
    }

    /* renamed from: b */
    public static void m5640b(boolean z) {
        if (!z) {
            throw new IllegalStateException();
        }
    }

    @NonNull
    /* renamed from: a */
    public static <T> T m5630a(T t, Object obj) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(String.valueOf(obj));
    }

    /* renamed from: b */
    public static void m5641b(boolean z, Object obj) {
        if (!z) {
            throw new IllegalStateException(String.valueOf(obj));
        }
    }

    /* renamed from: a */
    public static int m5628a(int i) {
        if (i != 0) {
            return i;
        }
        throw new IllegalArgumentException("Given Integer is zero");
    }

    /* renamed from: b */
    public static void m5642b(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalStateException(String.format(str, objArr));
        }
    }

    /* renamed from: a */
    public static void m5637a(boolean z, Object obj) {
        if (!z) {
            throw new IllegalArgumentException(String.valueOf(obj));
        }
    }

    /* renamed from: a */
    public static void m5638a(boolean z, String str, Object... objArr) {
        if (!z) {
            throw new IllegalArgumentException(String.format(str, objArr));
        }
    }

    /* renamed from: a */
    public static void m5636a(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    /* renamed from: a */
    public static void m5635a(String str) {
        if (!C2334v.m5825a()) {
            throw new IllegalStateException(str);
        }
    }

    /* renamed from: a */
    public static void m5632a() {
        m5643c("Must not be called on the main application thread");
    }

    /* renamed from: a */
    public static void m5633a(Handler handler) {
        m5634a(handler, "Must be called on the handler thread");
    }

    /* renamed from: a */
    public static void m5634a(Handler handler, String str) {
        if (Looper.myLooper() != handler.getLooper()) {
            throw new IllegalStateException(str);
        }
    }
}
