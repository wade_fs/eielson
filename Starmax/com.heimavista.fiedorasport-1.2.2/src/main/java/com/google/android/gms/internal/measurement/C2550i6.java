package com.google.android.gms.internal.measurement;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/* renamed from: com.google.android.gms.internal.measurement.i6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2550i6 {

    /* renamed from: c */
    private static final C2550i6 f4236c = new C2550i6();

    /* renamed from: a */
    private final C2583k6 f4237a = new C2602l5();

    /* renamed from: b */
    private final ConcurrentMap<Class<?>, C2603l6<?>> f4238b = new ConcurrentHashMap();

    private C2550i6() {
    }

    /* renamed from: a */
    public static C2550i6 m6481a() {
        return f4236c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.l6<T>, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T */
    /* renamed from: a */
    public final <T> C2603l6<T> mo17581a(Class cls) {
        C2647o4.m6963a((Object) cls, "messageType");
        C2603l6<T> l6Var = this.f4238b.get(cls);
        if (l6Var != null) {
            return l6Var;
        }
        C2603l6<T> a = this.f4237a.mo17652a(cls);
        C2647o4.m6963a((Object) cls, "messageType");
        C2647o4.m6963a((Object) a, "schema");
        C2603l6<T> putIfAbsent = this.f4238b.putIfAbsent(cls, a);
        return putIfAbsent != null ? putIfAbsent : a;
    }

    /* renamed from: a */
    public final <T> C2603l6<T> mo17582a(Object obj) {
        return mo17581a((Class) obj.getClass());
    }
}
