package com.google.android.gms.common.api.internal;

import android.util.Log;
import android.util.SparseArray;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2258v;
import java.io.FileDescriptor;
import java.io.PrintWriter;

/* renamed from: com.google.android.gms.common.api.internal.e2 */
public class C2070e2 extends C2079g2 {

    /* renamed from: U */
    private final SparseArray<C2071a> f3309U = new SparseArray<>();

    /* renamed from: com.google.android.gms.common.api.internal.e2$a */
    private class C2071a implements C2036f.C2039c {

        /* renamed from: a */
        public final int f3310a;

        /* renamed from: b */
        public final C2036f f3311b;

        /* renamed from: c */
        public final C2036f.C2039c f3312c;

        public C2071a(int i, C2036f fVar, C2036f.C2039c cVar) {
            this.f3310a = i;
            this.f3311b = fVar;
            this.f3312c = cVar;
            fVar.mo16565a(this);
        }

        /* renamed from: a */
        public final void mo16585a(@NonNull ConnectionResult connectionResult) {
            String valueOf = String.valueOf(connectionResult);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 27);
            sb.append("beginFailureResolution for ");
            sb.append(valueOf);
            Log.d("AutoManageHelper", sb.toString());
            C2070e2.this.mo16701b(connectionResult, this.f3310a);
        }
    }

    private C2070e2(C2080h hVar) {
        super(hVar);
        this.f3239P.mo16706a("AutoManageHelper", this);
    }

    /* renamed from: b */
    public static C2070e2 m4870b(C2076g gVar) {
        C2080h a = LifecycleCallback.m4743a(gVar);
        C2070e2 e2Var = (C2070e2) a.mo16705a("AutoManageHelper", C2070e2.class);
        if (e2Var != null) {
            return e2Var;
        }
        return new C2070e2(a);
    }

    /* renamed from: a */
    public final void mo16689a(int i, C2036f fVar, C2036f.C2039c cVar) {
        C2258v.m5630a(fVar, "GoogleApiClient instance cannot be null");
        boolean z = this.f3309U.indexOfKey(i) < 0;
        StringBuilder sb = new StringBuilder(54);
        sb.append("Already managing a GoogleApiClient with id ");
        sb.append(i);
        C2258v.m5641b(z, sb.toString());
        C2083h2 h2Var = super.f3330R.get();
        boolean z2 = super.f3329Q;
        String valueOf = String.valueOf(h2Var);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf).length() + 49);
        sb2.append("starting AutoManage for client ");
        sb2.append(i);
        sb2.append(" ");
        sb2.append(z2);
        sb2.append(" ");
        sb2.append(valueOf);
        Log.d("AutoManageHelper", sb2.toString());
        this.f3309U.put(i, new C2071a(i, fVar, cVar));
        if (super.f3329Q && h2Var == null) {
            String valueOf2 = String.valueOf(fVar);
            StringBuilder sb3 = new StringBuilder(String.valueOf(valueOf2).length() + 11);
            sb3.append("connecting ");
            sb3.append(valueOf2);
            Log.d("AutoManageHelper", sb3.toString());
            fVar.mo16571c();
        }
    }

    /* renamed from: d */
    public void mo16610d() {
        super.mo16610d();
        boolean z = super.f3329Q;
        String valueOf = String.valueOf(this.f3309U);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 14);
        sb.append("onStart ");
        sb.append(z);
        sb.append(" ");
        sb.append(valueOf);
        Log.d("AutoManageHelper", sb.toString());
        if (super.f3330R.get() == null) {
            for (int i = 0; i < this.f3309U.size(); i++) {
                C2071a b = m4869b(i);
                if (b != null) {
                    b.f3311b.mo16571c();
                }
            }
        }
    }

    /* renamed from: e */
    public void mo16611e() {
        super.mo16611e();
        for (int i = 0; i < this.f3309U.size(); i++) {
            C2071a b = m4869b(i);
            if (b != null) {
                b.f3311b.mo16572d();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public final void mo16691f() {
        for (int i = 0; i < this.f3309U.size(); i++) {
            C2071a b = m4869b(i);
            if (b != null) {
                b.f3311b.mo16571c();
            }
        }
    }

    @Nullable
    /* renamed from: b */
    private final C2071a m4869b(int i) {
        if (this.f3309U.size() <= i) {
            return null;
        }
        SparseArray<C2071a> sparseArray = this.f3309U;
        return sparseArray.get(sparseArray.keyAt(i));
    }

    /* renamed from: a */
    public final void mo16688a(int i) {
        C2071a aVar = this.f3309U.get(i);
        this.f3309U.remove(i);
        if (aVar != null) {
            aVar.f3311b.mo16570b(aVar);
            aVar.f3311b.mo16572d();
        }
    }

    /* renamed from: a */
    public void mo16606a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
        for (int i = 0; i < this.f3309U.size(); i++) {
            C2071a b = m4869b(i);
            if (b != null) {
                printWriter.append((CharSequence) str).append((CharSequence) "GoogleApiClient #").print(b.f3310a);
                printWriter.println(":");
                b.f3311b.mo16567a(String.valueOf(str).concat("  "), fileDescriptor, printWriter, strArr);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo16690a(ConnectionResult connectionResult, int i) {
        Log.w("AutoManageHelper", "Unresolved error while connecting client. Stopping auto-manage.");
        if (i < 0) {
            Log.wtf("AutoManageHelper", "AutoManageLifecycleHelper received onErrorResolutionFailed callback but no failing client ID is set", new Exception());
            return;
        }
        C2071a aVar = this.f3309U.get(i);
        if (aVar != null) {
            mo16688a(i);
            C2036f.C2039c cVar = aVar.f3312c;
            if (cVar != null) {
                cVar.mo16585a(connectionResult);
            }
        }
    }
}
