package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.u5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3208u5 implements Callable<List<zzv>> {

    /* renamed from: a */
    private final /* synthetic */ String f5677a;

    /* renamed from: b */
    private final /* synthetic */ String f5678b;

    /* renamed from: c */
    private final /* synthetic */ String f5679c;

    /* renamed from: d */
    private final /* synthetic */ C3141o5 f5680d;

    C3208u5(C3141o5 o5Var, String str, String str2, String str3) {
        this.f5680d = o5Var;
        this.f5677a = str;
        this.f5678b = str2;
        this.f5679c = str3;
    }

    public final /* synthetic */ Object call() {
        this.f5680d.f5496a.mo19237q();
        return this.f5680d.f5496a.mo19229e().mo18859b(this.f5677a, this.f5678b, this.f5679c);
    }
}
