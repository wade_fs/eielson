package com.google.android.gms.auth.api.signin;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import com.google.android.gms.auth.api.C1951a;
import com.google.android.gms.auth.api.signin.internal.C1995h;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2033e;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.internal.C2047a;
import com.google.android.gms.common.api.internal.C2103m;
import com.google.android.gms.common.internal.C2254u;
import com.google.android.gms.dynamite.DynamiteModule;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.android.gms.auth.api.signin.c */
public class C1978c extends C2033e<GoogleSignInOptions> {

    /* renamed from: i */
    private static int f3116i = C1980b.f3117a;

    /* renamed from: com.google.android.gms.auth.api.signin.c$a */
    private static class C1979a implements C2254u.C2255a<C1981d, GoogleSignInAccount> {
        private C1979a() {
        }

        /* renamed from: a */
        public final /* synthetic */ Object mo16417a(C2157k kVar) {
            return ((C1981d) kVar).mo16418a();
        }

        /* synthetic */ C1979a(C1986i iVar) {
            this();
        }
    }

    /* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
    /* renamed from: com.google.android.gms.auth.api.signin.c$b */
    static final class C1980b {

        /* renamed from: a */
        public static final int f3117a = 1;

        /* renamed from: b */
        public static final int f3118b = 2;

        /* renamed from: c */
        public static final int f3119c = 3;

        /* renamed from: d */
        public static final int f3120d = 4;

        /* renamed from: e */
        private static final /* synthetic */ int[] f3121e = {f3117a, f3118b, f3119c, f3120d};

        /* renamed from: a */
        public static int[] m4554a() {
            return (int[]) f3121e.clone();
        }
    }

    static {
        new C1979a(null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.e.<init>(android.app.Activity, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.internal.m):void
     arg types: [android.app.Activity, com.google.android.gms.common.api.a<com.google.android.gms.auth.api.signin.GoogleSignInOptions>, com.google.android.gms.auth.api.signin.GoogleSignInOptions, com.google.android.gms.common.api.internal.a]
     candidates:
      com.google.android.gms.common.api.e.<init>(android.app.Activity, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.e$a):void
      com.google.android.gms.common.api.e.<init>(android.content.Context, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.e$a):void
      com.google.android.gms.common.api.e.<init>(android.content.Context, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.internal.m):void
      com.google.android.gms.common.api.e.<init>(android.app.Activity, com.google.android.gms.common.api.a, com.google.android.gms.common.api.a$d, com.google.android.gms.common.api.internal.m):void */
    C1978c(@NonNull Activity activity, GoogleSignInOptions googleSignInOptions) {
        super(activity, (C2016a) C1951a.f2989e, (C2016a.C2020d) googleSignInOptions, (C2103m) new C2047a());
    }

    /* renamed from: k */
    private final synchronized int m4550k() {
        if (f3116i == C1980b.f3117a) {
            Context e = mo16556e();
            C2167b a = C2167b.m5251a();
            int a2 = a.mo16820a(e, (int) C2176f.f3566a);
            if (a2 == 0) {
                f3116i = C1980b.f3120d;
            } else if (a.mo16825a(e, a2, (String) null) != null || DynamiteModule.m5835a(e, "com.google.android.gms.auth.api.fallback") == 0) {
                f3116i = C1980b.f3118b;
            } else {
                f3116i = C1980b.f3119c;
            }
        }
        return f3116i;
    }

    @NonNull
    /* renamed from: i */
    public Intent mo16415i() {
        Context e = mo16556e();
        int i = C1986i.f3125a[m4550k() - 1];
        if (i == 1) {
            return C1995h.m4595b(e, (GoogleSignInOptions) mo16555d());
        }
        if (i != 2) {
            return C1995h.m4597c(e, (GoogleSignInOptions) mo16555d());
        }
        return C1995h.m4591a(e, (GoogleSignInOptions) mo16555d());
    }

    /* renamed from: j */
    public C4065h<Void> mo16416j() {
        return C2254u.m5622a(C1995h.m4593a(mo16548a(), mo16556e(), m4550k() == C1980b.f3119c));
    }
}
