package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.metadata.id3.Id3Decoder;

/* renamed from: com.google.android.exoplayer2.extractor.mp3.b */
/* compiled from: lambda */
public final /* synthetic */ class C1802b implements Id3Decoder.FramePredicate {

    /* renamed from: a */
    public static final /* synthetic */ C1802b f2815a = new C1802b();

    private /* synthetic */ C1802b() {
    }

    public final boolean evaluate(int i, int i2, int i3, int i4, int i5) {
        return Mp3Extractor.m4383a(i, i2, i3, i4, i5);
    }
}
