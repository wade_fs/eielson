package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2587ka;

/* renamed from: com.google.android.gms.measurement.internal.r3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3173r3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5583a = new C3173r3();

    private C3173r3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2587ka.m6630c());
    }
}
