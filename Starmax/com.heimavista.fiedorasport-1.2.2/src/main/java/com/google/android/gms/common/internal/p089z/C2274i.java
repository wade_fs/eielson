package com.google.android.gms.common.internal.p089z;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.internal.z.i */
abstract class C2274i extends C2273h<Status> {
    public C2274i(C2036f fVar) {
        super(fVar);
    }

    /* renamed from: a */
    public /* synthetic */ C2157k mo16455a(Status status) {
        return status;
    }
}
