package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.s5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3186s5 implements Callable<List<C3234w9>> {

    /* renamed from: a */
    private final /* synthetic */ String f5629a;

    /* renamed from: b */
    private final /* synthetic */ String f5630b;

    /* renamed from: c */
    private final /* synthetic */ String f5631c;

    /* renamed from: d */
    private final /* synthetic */ C3141o5 f5632d;

    C3186s5(C3141o5 o5Var, String str, String str2, String str3) {
        this.f5632d = o5Var;
        this.f5629a = str;
        this.f5630b = str2;
        this.f5631c = str3;
    }

    public final /* synthetic */ Object call() {
        this.f5632d.f5496a.mo19237q();
        return this.f5632d.f5496a.mo19229e().mo18847a(this.f5629a, this.f5630b, this.f5631c);
    }
}
