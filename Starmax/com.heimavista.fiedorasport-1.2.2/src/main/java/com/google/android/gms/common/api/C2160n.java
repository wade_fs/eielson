package com.google.android.gms.common.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.api.C2157k;

/* renamed from: com.google.android.gms.common.api.n */
public abstract class C2160n<R extends C2157k, S extends C2157k> {
    @NonNull
    /* renamed from: a */
    public abstract Status mo16812a(@NonNull Status status);

    @WorkerThread
    @Nullable
    /* renamed from: a */
    public abstract C2040g<S> mo16813a(@NonNull C2157k kVar);
}
