package com.google.android.gms.internal.measurement;

import android.app.Activity;
import com.google.android.gms.internal.measurement.C2525gd;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.internal.measurement.x */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2777x extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ Activity f4561T;

    /* renamed from: U */
    private final /* synthetic */ C2525gd.C2527b f4562U;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2777x(C2525gd.C2527b bVar, Activity activity) {
        super(C2525gd.this);
        this.f4562U = bVar;
        this.f4561T = activity;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        C2525gd.this.f4192g.onActivityPaused(C3992d.m11990a(this.f4561T), super.f4194Q);
    }
}
