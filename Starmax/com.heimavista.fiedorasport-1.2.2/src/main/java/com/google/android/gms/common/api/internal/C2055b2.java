package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.internal.C2064e;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.b2 */
public final class C2055b2 extends C2155z1<Void> {

    /* renamed from: b */
    private final C2095k<C2016a.C2018b, ?> f3253b;

    /* renamed from: c */
    private final C2115p<C2016a.C2018b, ?> f3254c;

    public C2055b2(C2097k1 k1Var, C4066i<Void> iVar) {
        super(3, iVar);
        this.f3253b = k1Var.f3357a;
        this.f3254c = k1Var.f3358b;
    }

    /* renamed from: a */
    public final /* bridge */ /* synthetic */ void mo16616a(@NonNull C2123r rVar, boolean z) {
    }

    @Nullable
    /* renamed from: b */
    public final Feature[] mo16634b(C2064e.C2065a<?> aVar) {
        return this.f3253b.mo16737c();
    }

    /* renamed from: c */
    public final boolean mo16635c(C2064e.C2065a<?> aVar) {
        return this.f3253b.mo16738d();
    }

    /* renamed from: d */
    public final void mo16636d(C2064e.C2065a<?> aVar) {
        this.f3253b.mo16735a(aVar.mo16674f(), super.f3531a);
        if (this.f3253b.mo16736b() != null) {
            aVar.mo16677i().put(this.f3253b.mo16736b(), new C2097k1(this.f3253b, this.f3254c));
        }
    }
}
