package com.google.android.gms.common.internal.p089z;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.internal.z.e */
public final class C2270e implements C2269d {
    /* renamed from: a */
    public final C2040g<Status> mo17047a(C2036f fVar) {
        return fVar.mo16564a(new C2271f(this, fVar));
    }
}
