package com.google.android.gms.internal.measurement;

import android.app.Activity;
import com.google.android.gms.internal.measurement.C2525gd;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.internal.measurement.z */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2809z extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ Activity f4621T;

    /* renamed from: U */
    private final /* synthetic */ C2538h9 f4622U;

    /* renamed from: V */
    private final /* synthetic */ C2525gd.C2527b f4623V;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2809z(C2525gd.C2527b bVar, Activity activity, C2538h9 h9Var) {
        super(C2525gd.this);
        this.f4623V = bVar;
        this.f4621T = activity;
        this.f4622U = h9Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        C2525gd.this.f4192g.onActivitySaveInstanceState(C3992d.m11990a(this.f4621T), this.f4622U, super.f4194Q);
    }
}
