package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2682qa;

/* renamed from: com.google.android.gms.measurement.internal.o1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3137o1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5487a = new C3137o1();

    private C3137o1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2682qa.m7091c());
    }
}
