package com.google.android.gms.internal.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ClientIdentity;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import com.google.android.gms.location.zzj;
import java.util.List;

/* renamed from: com.google.android.gms.internal.location.a0 */
public final class C2384a0 implements Parcelable.Creator<zzm> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        zzj zzj = zzm.f3960T;
        List<ClientIdentity> list = zzm.f3959S;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                zzj = (zzj) C2248a.m5553a(parcel, a, zzj.CREATOR);
            } else if (a2 == 2) {
                list = C2248a.m5562c(parcel, a, ClientIdentity.CREATOR);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                str = C2248a.m5573n(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzm(zzj, list, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzm[i];
    }
}
