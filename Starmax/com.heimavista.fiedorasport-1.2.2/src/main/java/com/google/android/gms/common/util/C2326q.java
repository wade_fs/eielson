package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.google.android.gms.common.C2177g;
import com.google.android.gms.common.p090j.C2283c;

/* renamed from: com.google.android.gms.common.util.q */
public final class C2326q {
    /* renamed from: a */
    public static boolean m5806a(Context context, int i) {
        if (!m5807a(context, i, "com.google.android.gms")) {
            return false;
        }
        try {
            return C2177g.m5307a(context).mo16855a(context.getPackageManager().getPackageInfo("com.google.android.gms", 64));
        } catch (PackageManager.NameNotFoundException unused) {
            if (Log.isLoggable("UidVerifier", 3)) {
                Log.d("UidVerifier", "Package manager can't find google play services package, defaulting to false");
            }
            return false;
        }
    }

    @TargetApi(19)
    /* renamed from: a */
    public static boolean m5807a(Context context, int i, String str) {
        return C2283c.m5685a(context).mo17058a(i, str);
    }
}
