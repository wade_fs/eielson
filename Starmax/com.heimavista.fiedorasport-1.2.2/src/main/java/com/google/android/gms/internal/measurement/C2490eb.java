package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.eb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2490eb implements C2441bb {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4079a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4080b;

    /* renamed from: c */
    private static final C2765w1<Boolean> f4081c;

    /* renamed from: d */
    private static final C2765w1<Boolean> f4082d;

    /* renamed from: e */
    private static final C2765w1<Boolean> f4083e;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4079a = c2Var.mo17367a("measurement.sdk.collection.enable_extend_user_property_size", true);
        f4080b = c2Var.mo17367a("measurement.sdk.collection.last_deep_link_referrer2", false);
        f4081c = c2Var.mo17367a("measurement.sdk.collection.last_deep_link_referrer_campaign2", false);
        f4082d = c2Var.mo17367a("measurement.sdk.collection.last_gclid_from_referrer2", false);
        f4083e = c2Var.mo17367a("measurement.sdk.collection.worker_thread_referrer", true);
        c2Var.mo17365a("measurement.id.sdk.collection.last_deep_link_referrer2", 0L);
    }

    /* renamed from: a */
    public final boolean mo17338a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17339e() {
        return f4079a.mo18128b().booleanValue();
    }

    /* renamed from: f */
    public final boolean mo17340f() {
        return f4080b.mo18128b().booleanValue();
    }

    /* renamed from: g */
    public final boolean mo17341g() {
        return f4081c.mo18128b().booleanValue();
    }

    /* renamed from: h */
    public final boolean mo17342h() {
        return f4083e.mo18128b().booleanValue();
    }

    /* renamed from: t */
    public final boolean mo17343t() {
        return f4082d.mo18128b().booleanValue();
    }
}
