package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.v5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3219v5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzan f5698P;

    /* renamed from: Q */
    private final /* synthetic */ String f5699Q;

    /* renamed from: R */
    private final /* synthetic */ C3141o5 f5700R;

    C3219v5(C3141o5 o5Var, zzan zzan, String str) {
        this.f5700R = o5Var;
        this.f5698P = zzan;
        this.f5699Q = str;
    }

    public final void run() {
        this.f5700R.f5496a.mo19237q();
        this.f5700R.f5496a.mo19212a(this.f5698P, this.f5699Q);
    }
}
