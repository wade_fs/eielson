package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.a */
/* compiled from: lambda */
public final /* synthetic */ class C1942a implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2946P;

    /* renamed from: Q */
    private final /* synthetic */ Format f2947Q;

    public /* synthetic */ C1942a(VideoRendererEventListener.EventDispatcher eventDispatcher, Format format) {
        this.f2946P = eventDispatcher;
        this.f2947Q = format;
    }

    public final void run() {
        this.f2946P.mo16268a(this.f2947Q);
    }
}
