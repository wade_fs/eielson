package com.google.android.gms.internal.location;

import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.location.C2833e;
import com.google.android.gms.location.LocationResult;

/* renamed from: com.google.android.gms.internal.location.k */
final class C2396k implements C2084i.C2086b<C2833e> {

    /* renamed from: a */
    private final /* synthetic */ LocationResult f3927a;

    C2396k(C2395j jVar, LocationResult locationResult) {
        this.f3927a = locationResult;
    }

    /* renamed from: a */
    public final void mo16725a() {
    }

    /* renamed from: a */
    public final /* synthetic */ void mo16726a(Object obj) {
        ((C2833e) obj).mo18255a(this.f3927a);
    }
}
