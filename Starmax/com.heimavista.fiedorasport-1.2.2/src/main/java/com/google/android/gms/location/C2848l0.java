package com.google.android.gms.location;

import android.location.Location;
import android.os.IInterface;

/* renamed from: com.google.android.gms.location.l0 */
public interface C2848l0 extends IInterface {
    void onLocationChanged(Location location);
}
