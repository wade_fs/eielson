package com.google.android.gms.auth.api.proxy;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.auth.api.proxy.b */
public final class C1973b implements Parcelable.Creator<ProxyRequest> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        String str = null;
        byte[] bArr = null;
        Bundle bundle = null;
        long j = 0;
        int i = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                str = C2248a.m5573n(parcel, a);
            } else if (a2 == 2) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 == 3) {
                j = C2248a.m5546B(parcel, a);
            } else if (a2 == 4) {
                bArr = C2248a.m5566g(parcel, a);
            } else if (a2 == 5) {
                bundle = C2248a.m5565f(parcel, a);
            } else if (a2 != 1000) {
                C2248a.m5550F(parcel, a);
            } else {
                i = C2248a.m5585z(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new ProxyRequest(i, str, i2, j, bArr, bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new ProxyRequest[i];
    }
}
