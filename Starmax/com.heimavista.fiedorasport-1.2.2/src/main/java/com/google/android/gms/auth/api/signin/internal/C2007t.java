package com.google.android.gms.auth.api.signin.internal;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.internal.p092authapi.C2356a;
import com.google.android.gms.internal.p092authapi.C2358c;

/* renamed from: com.google.android.gms.auth.api.signin.internal.t */
public final class C2007t extends C2356a implements C2006s {
    C2007t(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.signin.internal.ISignInService");
    }

    /* renamed from: a */
    public final void mo16462a(C2004q qVar, GoogleSignInOptions googleSignInOptions) {
        Parcel H = mo17155H();
        C2358c.m5880a(H, qVar);
        C2358c.m5881a(H, googleSignInOptions);
        mo17156a(102, H);
    }

    /* renamed from: b */
    public final void mo16463b(C2004q qVar, GoogleSignInOptions googleSignInOptions) {
        Parcel H = mo17155H();
        C2358c.m5880a(H, qVar);
        C2358c.m5881a(H, googleSignInOptions);
        mo17156a(103, H);
    }
}
