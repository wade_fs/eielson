package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.base.C2382h;

/* renamed from: com.google.android.gms.common.api.internal.i */
public final class C2084i<L> {

    /* renamed from: a */
    private final C2087c f3337a;

    /* renamed from: b */
    private volatile L f3338b;

    /* renamed from: c */
    private final C2085a<L> f3339c;

    /* renamed from: com.google.android.gms.common.api.internal.i$a */
    public static final class C2085a<L> {

        /* renamed from: a */
        private final L f3340a;

        /* renamed from: b */
        private final String f3341b;

        C2085a(L l, String str) {
            this.f3340a = l;
            this.f3341b = str;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C2085a)) {
                return false;
            }
            C2085a aVar = (C2085a) obj;
            return this.f3340a == aVar.f3340a && this.f3341b.equals(aVar.f3341b);
        }

        public final int hashCode() {
            return (System.identityHashCode(this.f3340a) * 31) + this.f3341b.hashCode();
        }
    }

    /* renamed from: com.google.android.gms.common.api.internal.i$b */
    public interface C2086b<L> {
        /* renamed from: a */
        void mo16725a();

        /* renamed from: a */
        void mo16726a(L l);
    }

    /* renamed from: com.google.android.gms.common.api.internal.i$c */
    private final class C2087c extends C2382h {
        public C2087c(Looper looper) {
            super(looper);
        }

        public final void handleMessage(Message message) {
            boolean z = true;
            if (message.what != 1) {
                z = false;
            }
            C2258v.m5636a(z);
            C2084i.this.mo16722b((C2086b) message.obj);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T
     arg types: [L, java.lang.String]
     candidates:
      com.google.android.gms.common.internal.v.a(java.lang.String, java.lang.Object):java.lang.String
      com.google.android.gms.common.internal.v.a(android.os.Handler, java.lang.String):void
      com.google.android.gms.common.internal.v.a(boolean, java.lang.Object):void
      com.google.android.gms.common.internal.v.a(java.lang.Object, java.lang.Object):T */
    C2084i(@NonNull Looper looper, @NonNull L l, @NonNull String str) {
        this.f3337a = new C2087c(looper);
        C2258v.m5630a((Object) l, (Object) "Listener must not be null");
        this.f3338b = l;
        C2258v.m5639b(str);
        this.f3339c = new C2085a<>(l, str);
    }

    /* renamed from: a */
    public final void mo16720a(C2086b<? super L> bVar) {
        C2258v.m5630a(bVar, "Notifier must not be null");
        this.f3337a.sendMessage(this.f3337a.obtainMessage(1, bVar));
    }

    @NonNull
    /* renamed from: b */
    public final C2085a<L> mo16721b() {
        return this.f3339c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final void mo16722b(C2086b<? super L> bVar) {
        L l = this.f3338b;
        if (l == null) {
            bVar.mo16725a();
            return;
        }
        try {
            bVar.mo16726a(l);
        } catch (RuntimeException e) {
            bVar.mo16725a();
            throw e;
        }
    }

    /* renamed from: a */
    public final void mo16719a() {
        this.f3338b = null;
    }
}
