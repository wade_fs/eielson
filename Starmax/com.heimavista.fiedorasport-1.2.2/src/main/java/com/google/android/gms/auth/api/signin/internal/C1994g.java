package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2219h;

/* renamed from: com.google.android.gms.auth.api.signin.internal.g */
public final class C1994g extends C2219h<C2006s> {

    /* renamed from: E */
    private final GoogleSignInOptions f3147E;

    public C1994g(Context context, Looper looper, C2211e eVar, GoogleSignInOptions googleSignInOptions, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        super(context, looper, 91, eVar, bVar, cVar);
        googleSignInOptions = googleSignInOptions == null ? new GoogleSignInOptions.C1975a().mo16407a() : googleSignInOptions;
        if (!eVar.mo16961d().isEmpty()) {
            GoogleSignInOptions.C1975a aVar = new GoogleSignInOptions.C1975a(googleSignInOptions);
            for (Scope scope : eVar.mo16961d()) {
                aVar.mo16406a(scope, new Scope[0]);
            }
            googleSignInOptions = aVar.mo16407a();
        }
        this.f3147E = googleSignInOptions;
    }

    /* renamed from: D */
    public final GoogleSignInOptions mo16448D() {
        return this.f3147E;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.signin.internal.ISignInService");
        if (queryLocalInterface instanceof C2006s) {
            return (C2006s) queryLocalInterface;
        }
        return new C2007t(iBinder);
    }

    /* renamed from: d */
    public final boolean mo16450d() {
        return true;
    }

    /* renamed from: i */
    public final int mo16451i() {
        return C2176f.f3566a;
    }

    /* renamed from: k */
    public final Intent mo16452k() {
        return C1995h.m4591a(mo16935t(), this.f3147E);
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public final String mo16453y() {
        return "com.google.android.gms.auth.api.signin.internal.ISignInService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: z */
    public final String mo16454z() {
        return "com.google.android.gms.auth.api.signin.service.START";
    }
}
