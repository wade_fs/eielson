package com.google.android.exoplayer2.p086ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.support.p009v4.media.session.MediaSessionCompat;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.media.app.NotificationCompat;
import com.google.android.exoplayer2.C1750C;
import com.google.android.exoplayer2.C1778b;
import com.google.android.exoplayer2.ControlDispatcher;
import com.google.android.exoplayer2.DefaultControlDispatcher;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.util.Assertions;
import com.google.android.exoplayer2.util.NotificationUtil;
import com.google.android.exoplayer2.util.Util;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager */
public class PlayerNotificationManager {
    public static final String ACTION_FAST_FORWARD = "com.google.android.exoplayer.ffwd";
    public static final String ACTION_NEXT = "com.google.android.exoplayer.next";
    public static final String ACTION_PAUSE = "com.google.android.exoplayer.pause";
    public static final String ACTION_PLAY = "com.google.android.exoplayer.play";
    public static final String ACTION_PREVIOUS = "com.google.android.exoplayer.prev";
    public static final String ACTION_REWIND = "com.google.android.exoplayer.rewind";
    public static final String ACTION_STOP = "com.google.android.exoplayer.stop";
    public static final int DEFAULT_FAST_FORWARD_MS = 15000;
    public static final int DEFAULT_REWIND_MS = 5000;
    public static final String EXTRA_INSTANCE_ID = "INSTANCE_ID";
    private static final long MAX_POSITION_FOR_SEEK_TO_PREVIOUS = 3000;
    private static int instanceIdCounter;
    private int badgeIconType;
    private final String channelId;
    private int color;
    private boolean colorized;
    private final Context context;
    /* access modifiers changed from: private */
    public ControlDispatcher controlDispatcher;
    /* access modifiers changed from: private */
    public int currentNotificationTag;
    /* access modifiers changed from: private */
    @Nullable
    public final CustomActionReceiver customActionReceiver;
    /* access modifiers changed from: private */
    public final Map<String, NotificationCompat.Action> customActions;
    private int defaults;
    /* access modifiers changed from: private */
    public long fastForwardMs;
    /* access modifiers changed from: private */
    public final int instanceId;
    private final IntentFilter intentFilter;
    /* access modifiers changed from: private */
    public boolean isNotificationStarted;
    /* access modifiers changed from: private */
    public int lastPlaybackState;
    /* access modifiers changed from: private */
    public final Handler mainHandler;
    private final MediaDescriptionAdapter mediaDescriptionAdapter;
    @Nullable
    private MediaSessionCompat.Token mediaSessionToken;
    private final NotificationBroadcastReceiver notificationBroadcastReceiver;
    private final int notificationId;
    @Nullable
    private NotificationListener notificationListener;
    private final NotificationManagerCompat notificationManager;
    private boolean ongoing;
    private final Map<String, NotificationCompat.Action> playbackActions;
    /* access modifiers changed from: private */
    @Nullable
    public Player player;
    private final Player.EventListener playerListener;
    private int priority;
    /* access modifiers changed from: private */
    public long rewindMs;
    @DrawableRes
    private int smallIconResourceId;
    @Nullable
    private String stopAction;
    @Nullable
    private PendingIntent stopPendingIntent;
    private boolean useChronometer;
    private boolean useNavigationActions;
    private boolean usePlayPauseActions;
    private int visibility;
    /* access modifiers changed from: private */
    public boolean wasPlayWhenReady;

    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$BitmapCallback */
    public final class BitmapCallback {
        private final int notificationTag;

        /* renamed from: a */
        public /* synthetic */ void mo15785a(Bitmap bitmap) {
            if (PlayerNotificationManager.this.player != null && this.notificationTag == PlayerNotificationManager.this.currentNotificationTag && PlayerNotificationManager.this.isNotificationStarted) {
                Notification unused = PlayerNotificationManager.this.updateNotification(bitmap);
            }
        }

        public void onBitmap(Bitmap bitmap) {
            if (bitmap != null) {
                PlayerNotificationManager.this.mainHandler.post(new C1918d(this, bitmap));
            }
        }

        private BitmapCallback(int i) {
            this.notificationTag = i;
        }
    }

    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$CustomActionReceiver */
    public interface CustomActionReceiver {
        Map<String, NotificationCompat.Action> createCustomActions(Context context, int i);

        List<String> getCustomActions(Player player);

        void onCustomAction(Player player, String str, Intent intent);
    }

    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$MediaDescriptionAdapter */
    public interface MediaDescriptionAdapter {
        @Nullable
        PendingIntent createCurrentContentIntent(Player player);

        @Nullable
        String getCurrentContentText(Player player);

        String getCurrentContentTitle(Player player);

        @Nullable
        Bitmap getCurrentLargeIcon(Player player, BitmapCallback bitmapCallback);
    }

    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$NotificationBroadcastReceiver */
    private class NotificationBroadcastReceiver extends BroadcastReceiver {
        private final Timeline.Window window = new Timeline.Window();

        public NotificationBroadcastReceiver() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:28:0x009a, code lost:
            if (r0.isSeekable == false) goto L_0x009c;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onReceive(android.content.Context r8, android.content.Intent r9) {
            /*
                r7 = this;
                com.google.android.exoplayer2.ui.PlayerNotificationManager r8 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.Player r8 = r8.player
                if (r8 == 0) goto L_0x011f
                com.google.android.exoplayer2.ui.PlayerNotificationManager r0 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                boolean r0 = r0.isNotificationStarted
                if (r0 == 0) goto L_0x011f
                com.google.android.exoplayer2.ui.PlayerNotificationManager r0 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                int r0 = r0.instanceId
                java.lang.String r1 = "INSTANCE_ID"
                int r0 = r9.getIntExtra(r1, r0)
                com.google.android.exoplayer2.ui.PlayerNotificationManager r1 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                int r1 = r1.instanceId
                if (r0 == r1) goto L_0x0026
                goto L_0x011f
            L_0x0026:
                java.lang.String r0 = r9.getAction()
                java.lang.String r1 = "com.google.android.exoplayer.play"
                boolean r2 = r1.equals(r0)
                if (r2 != 0) goto L_0x0112
                java.lang.String r2 = "com.google.android.exoplayer.pause"
                boolean r2 = r2.equals(r0)
                if (r2 == 0) goto L_0x003c
                goto L_0x0112
            L_0x003c:
                java.lang.String r1 = "com.google.android.exoplayer.ffwd"
                boolean r2 = r1.equals(r0)
                if (r2 != 0) goto L_0x00eb
                java.lang.String r2 = "com.google.android.exoplayer.rewind"
                boolean r2 = r2.equals(r0)
                if (r2 == 0) goto L_0x004e
                goto L_0x00eb
            L_0x004e:
                java.lang.String r1 = "com.google.android.exoplayer.next"
                boolean r1 = r1.equals(r0)
                r2 = -1
                r3 = -9223372036854775807(0x8000000000000001, double:-4.9E-324)
                if (r1 == 0) goto L_0x006d
                int r9 = r8.getNextWindowIndex()
                if (r9 == r2) goto L_0x011f
                com.google.android.exoplayer2.ui.PlayerNotificationManager r0 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ControlDispatcher r0 = r0.controlDispatcher
                r0.dispatchSeekTo(r8, r9, r3)
                goto L_0x011f
            L_0x006d:
                java.lang.String r1 = "com.google.android.exoplayer.prev"
                boolean r1 = r1.equals(r0)
                if (r1 == 0) goto L_0x00b5
                com.google.android.exoplayer2.Timeline r9 = r8.getCurrentTimeline()
                int r0 = r8.getCurrentWindowIndex()
                com.google.android.exoplayer2.Timeline$Window r1 = r7.window
                r9.getWindow(r0, r1)
                int r9 = r8.getPreviousWindowIndex()
                if (r9 == r2) goto L_0x00a7
                long r0 = r8.getCurrentPosition()
                r5 = 3000(0xbb8, double:1.482E-320)
                int r2 = (r0 > r5 ? 1 : (r0 == r5 ? 0 : -1))
                if (r2 <= 0) goto L_0x009c
                com.google.android.exoplayer2.Timeline$Window r0 = r7.window
                boolean r1 = r0.isDynamic
                if (r1 == 0) goto L_0x00a7
                boolean r0 = r0.isSeekable
                if (r0 != 0) goto L_0x00a7
            L_0x009c:
                com.google.android.exoplayer2.ui.PlayerNotificationManager r0 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ControlDispatcher r0 = r0.controlDispatcher
                r0.dispatchSeekTo(r8, r9, r3)
                goto L_0x011f
            L_0x00a7:
                com.google.android.exoplayer2.ui.PlayerNotificationManager r9 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ControlDispatcher r9 = r9.controlDispatcher
                int r0 = r8.getCurrentWindowIndex()
                r9.dispatchSeekTo(r8, r0, r3)
                goto L_0x011f
            L_0x00b5:
                java.lang.String r1 = "com.google.android.exoplayer.stop"
                boolean r1 = r1.equals(r0)
                if (r1 == 0) goto L_0x00cd
                com.google.android.exoplayer2.ui.PlayerNotificationManager r9 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ControlDispatcher r9 = r9.controlDispatcher
                r0 = 1
                r9.dispatchStop(r8, r0)
                com.google.android.exoplayer2.ui.PlayerNotificationManager r8 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                r8.stopNotification()
                goto L_0x011f
            L_0x00cd:
                com.google.android.exoplayer2.ui.PlayerNotificationManager r1 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ui.PlayerNotificationManager$CustomActionReceiver r1 = r1.customActionReceiver
                if (r1 == 0) goto L_0x011f
                com.google.android.exoplayer2.ui.PlayerNotificationManager r1 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                java.util.Map r1 = r1.customActions
                boolean r1 = r1.containsKey(r0)
                if (r1 == 0) goto L_0x011f
                com.google.android.exoplayer2.ui.PlayerNotificationManager r1 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ui.PlayerNotificationManager$CustomActionReceiver r1 = r1.customActionReceiver
                r1.onCustomAction(r8, r0, r9)
                goto L_0x011f
            L_0x00eb:
                boolean r9 = r1.equals(r0)
                if (r9 == 0) goto L_0x00f8
                com.google.android.exoplayer2.ui.PlayerNotificationManager r9 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                long r0 = r9.fastForwardMs
                goto L_0x00ff
            L_0x00f8:
                com.google.android.exoplayer2.ui.PlayerNotificationManager r9 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                long r0 = r9.rewindMs
                long r0 = -r0
            L_0x00ff:
                com.google.android.exoplayer2.ui.PlayerNotificationManager r9 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ControlDispatcher r9 = r9.controlDispatcher
                int r2 = r8.getCurrentWindowIndex()
                long r3 = r8.getCurrentPosition()
                long r3 = r3 + r0
                r9.dispatchSeekTo(r8, r2, r3)
                goto L_0x011f
            L_0x0112:
                com.google.android.exoplayer2.ui.PlayerNotificationManager r9 = com.google.android.exoplayer2.p086ui.PlayerNotificationManager.this
                com.google.android.exoplayer2.ControlDispatcher r9 = r9.controlDispatcher
                boolean r0 = r1.equals(r0)
                r9.dispatchSetPlayWhenReady(r8, r0)
            L_0x011f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.exoplayer2.p086ui.PlayerNotificationManager.NotificationBroadcastReceiver.onReceive(android.content.Context, android.content.Intent):void");
        }
    }

    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$NotificationListener */
    public interface NotificationListener {
        void onNotificationCancelled(int i);

        void onNotificationStarted(int i, Notification notification);
    }

    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$PlayerListener */
    private class PlayerListener implements Player.EventListener {
        private PlayerListener() {
        }

        public /* synthetic */ void onLoadingChanged(boolean z) {
            C1778b.$default$onLoadingChanged(this, z);
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
            if (PlayerNotificationManager.this.player != null && PlayerNotificationManager.this.player.getPlaybackState() != 1) {
                PlayerNotificationManager.this.startOrUpdateNotification();
            }
        }

        public /* synthetic */ void onPlayerError(ExoPlaybackException exoPlaybackException) {
            C1778b.$default$onPlayerError(this, exoPlaybackException);
        }

        public void onPlayerStateChanged(boolean z, int i) {
            if (!((PlayerNotificationManager.this.wasPlayWhenReady == z || i == 1) && PlayerNotificationManager.this.lastPlaybackState == i)) {
                PlayerNotificationManager.this.startOrUpdateNotification();
            }
            boolean unused = PlayerNotificationManager.this.wasPlayWhenReady = z;
            int unused2 = PlayerNotificationManager.this.lastPlaybackState = i;
        }

        public void onPositionDiscontinuity(int i) {
            PlayerNotificationManager.this.startOrUpdateNotification();
        }

        public void onRepeatModeChanged(int i) {
            if (PlayerNotificationManager.this.player != null && PlayerNotificationManager.this.player.getPlaybackState() != 1) {
                PlayerNotificationManager.this.startOrUpdateNotification();
            }
        }

        public /* synthetic */ void onSeekProcessed() {
            C1778b.$default$onSeekProcessed(this);
        }

        public /* synthetic */ void onShuffleModeEnabledChanged(boolean z) {
            C1778b.$default$onShuffleModeEnabledChanged(this, z);
        }

        public void onTimelineChanged(Timeline timeline, @Nullable Object obj, int i) {
            if (PlayerNotificationManager.this.player != null && PlayerNotificationManager.this.player.getPlaybackState() != 1) {
                PlayerNotificationManager.this.startOrUpdateNotification();
            }
        }

        public /* synthetic */ void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            C1778b.$default$onTracksChanged(this, trackGroupArray, trackSelectionArray);
        }
    }

    @Documented
    @Retention(RetentionPolicy.SOURCE)
    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$Priority */
    public @interface Priority {
    }

    @Documented
    @Retention(RetentionPolicy.SOURCE)
    /* renamed from: com.google.android.exoplayer2.ui.PlayerNotificationManager$Visibility */
    public @interface Visibility {
    }

    public PlayerNotificationManager(Context context2, String str, int i, MediaDescriptionAdapter mediaDescriptionAdapter2) {
        this(context2, str, i, mediaDescriptionAdapter2, null);
    }

    private static PendingIntent createBroadcastIntent(String str, Context context2, int i) {
        Intent intent = new Intent(str).setPackage(context2.getPackageName());
        intent.putExtra(EXTRA_INSTANCE_ID, i);
        return PendingIntent.getBroadcast(context2, i, intent, C1750C.ENCODING_PCM_MU_LAW);
    }

    private static Map<String, NotificationCompat.Action> createPlaybackActions(Context context2, int i) {
        HashMap hashMap = new HashMap();
        hashMap.put(ACTION_PLAY, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_play, context2.getString(C1908R.string.exo_controls_play_description), createBroadcastIntent(ACTION_PLAY, context2, i)));
        hashMap.put(ACTION_PAUSE, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_pause, context2.getString(C1908R.string.exo_controls_pause_description), createBroadcastIntent(ACTION_PAUSE, context2, i)));
        hashMap.put(ACTION_STOP, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_stop, context2.getString(C1908R.string.exo_controls_stop_description), createBroadcastIntent(ACTION_STOP, context2, i)));
        hashMap.put(ACTION_REWIND, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_rewind, context2.getString(C1908R.string.exo_controls_rewind_description), createBroadcastIntent(ACTION_REWIND, context2, i)));
        hashMap.put(ACTION_FAST_FORWARD, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_fastforward, context2.getString(C1908R.string.exo_controls_fastforward_description), createBroadcastIntent(ACTION_FAST_FORWARD, context2, i)));
        hashMap.put(ACTION_PREVIOUS, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_previous, context2.getString(C1908R.string.exo_controls_previous_description), createBroadcastIntent(ACTION_PREVIOUS, context2, i)));
        hashMap.put(ACTION_NEXT, new NotificationCompat.Action(C1908R.C1910drawable.exo_notification_next, context2.getString(C1908R.string.exo_controls_next_description), createBroadcastIntent(ACTION_NEXT, context2, i)));
        return hashMap;
    }

    public static PlayerNotificationManager createWithNotificationChannel(Context context2, String str, @StringRes int i, int i2, MediaDescriptionAdapter mediaDescriptionAdapter2) {
        NotificationUtil.createNotificationChannel(context2, str, i, 2);
        return new PlayerNotificationManager(context2, str, i2, mediaDescriptionAdapter2);
    }

    /* access modifiers changed from: private */
    public void startOrUpdateNotification() {
        if (this.player != null) {
            Notification updateNotification = updateNotification(null);
            if (!this.isNotificationStarted) {
                this.isNotificationStarted = true;
                this.context.registerReceiver(this.notificationBroadcastReceiver, this.intentFilter);
                NotificationListener notificationListener2 = this.notificationListener;
                if (notificationListener2 != null) {
                    notificationListener2.onNotificationStarted(this.notificationId, updateNotification);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    public void stopNotification() {
        if (this.isNotificationStarted) {
            this.notificationManager.cancel(this.notificationId);
            this.isNotificationStarted = false;
            this.context.unregisterReceiver(this.notificationBroadcastReceiver);
            NotificationListener notificationListener2 = this.notificationListener;
            if (notificationListener2 != null) {
                notificationListener2.onNotificationCancelled(this.notificationId);
            }
        }
    }

    /* access modifiers changed from: private */
    public Notification updateNotification(@Nullable Bitmap bitmap) {
        Notification createNotification = createNotification(this.player, bitmap);
        this.notificationManager.notify(this.notificationId, createNotification);
        return createNotification;
    }

    /* access modifiers changed from: protected */
    public Notification createNotification(Player player2, @Nullable Bitmap bitmap) {
        PendingIntent pendingIntent;
        NotificationCompat.Action action;
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this.context, this.channelId);
        List<String> actions = getActions(player2);
        for (int i = 0; i < actions.size(); i++) {
            String str = actions.get(i);
            if (this.playbackActions.containsKey(str)) {
                action = this.playbackActions.get(str);
            } else {
                action = this.customActions.get(str);
            }
            if (action != null) {
                builder.addAction(action);
            }
        }
        NotificationCompat.MediaStyle mediaStyle = new NotificationCompat.MediaStyle();
        MediaSessionCompat.Token token = this.mediaSessionToken;
        if (token != null) {
            mediaStyle.setMediaSession(token);
        }
        mediaStyle.setShowActionsInCompactView(getActionIndicesForCompactView(actions, player2));
        boolean z = this.stopAction != null;
        mediaStyle.setShowCancelButton(z);
        if (z && (pendingIntent = this.stopPendingIntent) != null) {
            builder.setDeleteIntent(pendingIntent);
            mediaStyle.setCancelButtonIntent(this.stopPendingIntent);
        }
        builder.setStyle(mediaStyle);
        builder.setBadgeIconType(this.badgeIconType).setOngoing(this.ongoing).setColor(this.color).setColorized(this.colorized).setSmallIcon(this.smallIconResourceId).setVisibility(this.visibility).setPriority(this.priority).setDefaults(this.defaults);
        if (!this.useChronometer || player2.isPlayingAd() || player2.isCurrentWindowDynamic() || !player2.getPlayWhenReady() || player2.getPlaybackState() != 3) {
            builder.setShowWhen(false).setUsesChronometer(false);
        } else {
            builder.setWhen(System.currentTimeMillis() - player2.getContentPosition()).setShowWhen(true).setUsesChronometer(true);
        }
        builder.setContentTitle(this.mediaDescriptionAdapter.getCurrentContentTitle(player2));
        builder.setContentText(this.mediaDescriptionAdapter.getCurrentContentText(player2));
        if (bitmap == null) {
            MediaDescriptionAdapter mediaDescriptionAdapter2 = this.mediaDescriptionAdapter;
            int i2 = this.currentNotificationTag + 1;
            this.currentNotificationTag = i2;
            bitmap = mediaDescriptionAdapter2.getCurrentLargeIcon(player2, new BitmapCallback(i2));
        }
        if (bitmap != null) {
            builder.setLargeIcon(bitmap);
        }
        PendingIntent createCurrentContentIntent = this.mediaDescriptionAdapter.createCurrentContentIntent(player2);
        if (createCurrentContentIntent != null) {
            builder.setContentIntent(createCurrentContentIntent);
        }
        return builder.build();
    }

    /* access modifiers changed from: protected */
    public int[] getActionIndicesForCompactView(List<String> list, Player player2) {
        int indexOf = list.indexOf(ACTION_PAUSE);
        int indexOf2 = list.indexOf(ACTION_PLAY);
        if (indexOf != -1) {
            return new int[]{indexOf};
        } else if (indexOf2 == -1) {
            return new int[0];
        } else {
            return new int[]{indexOf2};
        }
    }

    /* access modifiers changed from: protected */
    public List<String> getActions(Player player2) {
        boolean isPlayingAd = player2.isPlayingAd();
        ArrayList arrayList = new ArrayList();
        if (!isPlayingAd) {
            if (this.useNavigationActions) {
                arrayList.add(ACTION_PREVIOUS);
            }
            if (this.rewindMs > 0) {
                arrayList.add(ACTION_REWIND);
            }
        }
        if (this.usePlayPauseActions) {
            if (player2.getPlayWhenReady()) {
                arrayList.add(ACTION_PAUSE);
            } else {
                arrayList.add(ACTION_PLAY);
            }
        }
        if (!isPlayingAd) {
            if (this.fastForwardMs > 0) {
                arrayList.add(ACTION_FAST_FORWARD);
            }
            if (this.useNavigationActions && player2.getNextWindowIndex() != -1) {
                arrayList.add(ACTION_NEXT);
            }
        }
        CustomActionReceiver customActionReceiver2 = this.customActionReceiver;
        if (customActionReceiver2 != null) {
            arrayList.addAll(customActionReceiver2.getCustomActions(player2));
        }
        if (ACTION_STOP.equals(this.stopAction)) {
            arrayList.add(this.stopAction);
        }
        return arrayList;
    }

    public void invalidate() {
        if (this.isNotificationStarted && this.player != null) {
            updateNotification(null);
        }
    }

    public final void setBadgeIconType(int i) {
        if (this.badgeIconType != i) {
            if (i == 0 || i == 1 || i == 2) {
                this.badgeIconType = i;
                invalidate();
                return;
            }
            throw new IllegalArgumentException();
        }
    }

    public final void setColor(int i) {
        if (this.color != i) {
            this.color = i;
            invalidate();
        }
    }

    public final void setColorized(boolean z) {
        if (this.colorized != z) {
            this.colorized = z;
            invalidate();
        }
    }

    public final void setControlDispatcher(ControlDispatcher controlDispatcher2) {
        if (controlDispatcher2 == null) {
            controlDispatcher2 = new DefaultControlDispatcher();
        }
        this.controlDispatcher = controlDispatcher2;
    }

    public final void setDefaults(int i) {
        if (this.defaults != i) {
            this.defaults = i;
            invalidate();
        }
    }

    public final void setFastForwardIncrementMs(long j) {
        if (this.fastForwardMs != j) {
            this.fastForwardMs = j;
            invalidate();
        }
    }

    public final void setMediaSessionToken(MediaSessionCompat.Token token) {
        if (!Util.areEqual(this.mediaSessionToken, token)) {
            this.mediaSessionToken = token;
            invalidate();
        }
    }

    public final void setNotificationListener(NotificationListener notificationListener2) {
        this.notificationListener = notificationListener2;
    }

    public final void setOngoing(boolean z) {
        if (this.ongoing != z) {
            this.ongoing = z;
            invalidate();
        }
    }

    public final void setPlayer(@Nullable Player player2) {
        boolean z = false;
        Assertions.checkState(Looper.myLooper() == Looper.getMainLooper());
        if (player2 == null || player2.getApplicationLooper() == Looper.getMainLooper()) {
            z = true;
        }
        Assertions.checkArgument(z);
        Player player3 = this.player;
        if (player3 != player2) {
            if (player3 != null) {
                player3.removeListener(this.playerListener);
                if (player2 == null) {
                    stopNotification();
                }
            }
            this.player = player2;
            if (player2 != null) {
                this.wasPlayWhenReady = player2.getPlayWhenReady();
                this.lastPlaybackState = player2.getPlaybackState();
                player2.addListener(this.playerListener);
                if (this.lastPlaybackState != 1) {
                    startOrUpdateNotification();
                }
            }
        }
    }

    public final void setPriority(int i) {
        if (this.priority != i) {
            if (i == -2 || i == -1 || i == 0 || i == 1 || i == 2) {
                this.priority = i;
                invalidate();
                return;
            }
            throw new IllegalArgumentException();
        }
    }

    public final void setRewindIncrementMs(long j) {
        if (this.rewindMs != j) {
            this.rewindMs = j;
            invalidate();
        }
    }

    public final void setSmallIcon(@DrawableRes int i) {
        if (this.smallIconResourceId != i) {
            this.smallIconResourceId = i;
            invalidate();
        }
    }

    public final void setStopAction(@Nullable String str) {
        if (!Util.areEqual(str, this.stopAction)) {
            this.stopAction = str;
            if (ACTION_STOP.equals(str)) {
                this.stopPendingIntent = ((NotificationCompat.Action) Assertions.checkNotNull(this.playbackActions.get(ACTION_STOP))).actionIntent;
            } else if (str != null) {
                this.stopPendingIntent = ((NotificationCompat.Action) Assertions.checkNotNull(this.customActions.get(str))).actionIntent;
            } else {
                this.stopPendingIntent = null;
            }
            invalidate();
        }
    }

    public final void setUseChronometer(boolean z) {
        if (this.useChronometer != z) {
            this.useChronometer = z;
            invalidate();
        }
    }

    public final void setUseNavigationActions(boolean z) {
        if (this.useNavigationActions != z) {
            this.useNavigationActions = z;
            invalidate();
        }
    }

    public final void setUsePlayPauseActions(boolean z) {
        if (this.usePlayPauseActions != z) {
            this.usePlayPauseActions = z;
            invalidate();
        }
    }

    public final void setVisibility(int i) {
        if (this.visibility != i) {
            if (i == -1 || i == 0 || i == 1) {
                this.visibility = i;
                invalidate();
                return;
            }
            throw new IllegalStateException();
        }
    }

    public PlayerNotificationManager(Context context2, String str, int i, MediaDescriptionAdapter mediaDescriptionAdapter2, @Nullable CustomActionReceiver customActionReceiver2) {
        Map<String, NotificationCompat.Action> map;
        this.context = context2.getApplicationContext();
        this.channelId = str;
        this.notificationId = i;
        this.mediaDescriptionAdapter = mediaDescriptionAdapter2;
        this.customActionReceiver = customActionReceiver2;
        this.controlDispatcher = new DefaultControlDispatcher();
        int i2 = instanceIdCounter;
        instanceIdCounter = i2 + 1;
        this.instanceId = i2;
        this.mainHandler = new Handler(Looper.getMainLooper());
        this.notificationManager = NotificationManagerCompat.from(context2);
        this.playerListener = new PlayerListener();
        this.notificationBroadcastReceiver = new NotificationBroadcastReceiver();
        this.intentFilter = new IntentFilter();
        this.useNavigationActions = true;
        this.usePlayPauseActions = true;
        this.ongoing = true;
        this.colorized = true;
        this.useChronometer = true;
        this.color = 0;
        this.smallIconResourceId = C1908R.C1910drawable.exo_notification_small_icon;
        this.defaults = 0;
        this.priority = -1;
        this.fastForwardMs = 15000;
        this.rewindMs = DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS;
        this.stopAction = ACTION_STOP;
        this.badgeIconType = 1;
        this.visibility = 1;
        this.playbackActions = createPlaybackActions(context2, this.instanceId);
        for (String str2 : this.playbackActions.keySet()) {
            this.intentFilter.addAction(str2);
        }
        if (customActionReceiver2 != null) {
            map = customActionReceiver2.createCustomActions(context2, this.instanceId);
        } else {
            map = Collections.emptyMap();
        }
        this.customActions = map;
        for (String str3 : this.customActions.keySet()) {
            this.intentFilter.addAction(str3);
        }
        this.stopPendingIntent = ((NotificationCompat.Action) Assertions.checkNotNull(this.playbackActions.get(ACTION_STOP))).actionIntent;
    }
}
