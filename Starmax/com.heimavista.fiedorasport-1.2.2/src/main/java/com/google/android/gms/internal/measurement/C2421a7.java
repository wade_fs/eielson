package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.a7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2421a7 extends RuntimeException {
    public C2421a7(C2739u5 u5Var) {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
