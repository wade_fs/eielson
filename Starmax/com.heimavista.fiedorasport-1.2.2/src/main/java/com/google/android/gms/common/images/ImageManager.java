package com.google.android.gms.common.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.util.Log;
import androidx.collection.LruCache;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.C2194c;
import com.google.android.gms.internal.base.zak;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

public final class ImageManager {
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static final Object f3577i = new Object();
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static HashSet<Uri> f3578j = new HashSet<>();
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Context f3579a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Handler f3580b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final ExecutorService f3581c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final C2182a f3582d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final zak f3583e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final Map<C2185a, ImageReceiver> f3584f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public final Map<Uri, ImageReceiver> f3585g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public final Map<Uri, Long> f3586h;

    @KeepName
    private final class ImageReceiver extends ResultReceiver {

        /* renamed from: P */
        private final Uri f3587P;
        /* access modifiers changed from: private */

        /* renamed from: Q */
        public final ArrayList<C2185a> f3588Q;

        /* renamed from: R */
        private final /* synthetic */ ImageManager f3589R;

        public final void onReceiveResult(int i, Bundle bundle) {
            this.f3589R.f3581c.execute(new C2183b(this.f3587P, (ParcelFileDescriptor) bundle.getParcelable("com.google.android.gms.extra.fileDescriptor")));
        }
    }

    /* renamed from: com.google.android.gms.common.images.ImageManager$a */
    private static final class C2182a extends LruCache<C2186b, Bitmap> {
    }

    /* renamed from: com.google.android.gms.common.images.ImageManager$b */
    private final class C2183b implements Runnable {

        /* renamed from: P */
        private final Uri f3590P;

        /* renamed from: Q */
        private final ParcelFileDescriptor f3591Q;

        public C2183b(Uri uri, ParcelFileDescriptor parcelFileDescriptor) {
            this.f3590P = uri;
            this.f3591Q = parcelFileDescriptor;
        }

        public final void run() {
            boolean z;
            Bitmap bitmap;
            C2194c.m5356b("LoadBitmapFromDiskRunnable can't be executed in the main thread");
            ParcelFileDescriptor parcelFileDescriptor = this.f3591Q;
            boolean z2 = false;
            Bitmap bitmap2 = null;
            if (parcelFileDescriptor != null) {
                try {
                    bitmap2 = BitmapFactory.decodeFileDescriptor(parcelFileDescriptor.getFileDescriptor());
                } catch (OutOfMemoryError e) {
                    String valueOf = String.valueOf(this.f3590P);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 34);
                    sb.append("OOM while loading bitmap for uri: ");
                    sb.append(valueOf);
                    Log.e("ImageManager", sb.toString(), e);
                    z2 = true;
                }
                try {
                    this.f3591Q.close();
                } catch (IOException e2) {
                    Log.e("ImageManager", "closed failed", e2);
                }
                z = z2;
                bitmap = bitmap2;
            } else {
                bitmap = null;
                z = false;
            }
            CountDownLatch countDownLatch = new CountDownLatch(1);
            ImageManager.this.f3580b.post(new C2184c(this.f3590P, bitmap, z, countDownLatch));
            try {
                countDownLatch.await();
            } catch (InterruptedException unused) {
                String valueOf2 = String.valueOf(this.f3590P);
                StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 32);
                sb2.append("Latch interrupted while posting ");
                sb2.append(valueOf2);
                Log.w("ImageManager", sb2.toString());
            }
        }
    }

    /* renamed from: com.google.android.gms.common.images.ImageManager$c */
    private final class C2184c implements Runnable {

        /* renamed from: P */
        private final Uri f3593P;

        /* renamed from: Q */
        private final Bitmap f3594Q;

        /* renamed from: R */
        private final CountDownLatch f3595R;

        /* renamed from: S */
        private boolean f3596S;

        public C2184c(Uri uri, Bitmap bitmap, boolean z, CountDownLatch countDownLatch) {
            this.f3593P = uri;
            this.f3594Q = bitmap;
            this.f3596S = z;
            this.f3595R = countDownLatch;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void
         arg types: [android.content.Context, android.graphics.Bitmap, int]
         candidates:
          com.google.android.gms.common.images.a.a(android.content.Context, com.google.android.gms.internal.base.zak, boolean):void
          com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.common.images.a.a(android.content.Context, com.google.android.gms.internal.base.zak, boolean):void
         arg types: [android.content.Context, com.google.android.gms.internal.base.zak, int]
         candidates:
          com.google.android.gms.common.images.a.a(android.content.Context, android.graphics.Bitmap, boolean):void
          com.google.android.gms.common.images.a.a(android.content.Context, com.google.android.gms.internal.base.zak, boolean):void */
        public final void run() {
            C2194c.m5355a("OnBitmapLoadedRunnable must be executed in the main thread");
            boolean z = this.f3594Q != null;
            if (ImageManager.this.f3582d != null) {
                if (this.f3596S) {
                    ImageManager.this.f3582d.evictAll();
                    System.gc();
                    this.f3596S = false;
                    ImageManager.this.f3580b.post(this);
                    return;
                } else if (z) {
                    ImageManager.this.f3582d.put(new C2186b(this.f3593P), this.f3594Q);
                }
            }
            ImageReceiver imageReceiver = (ImageReceiver) ImageManager.this.f3585g.remove(this.f3593P);
            if (imageReceiver != null) {
                ArrayList a = imageReceiver.f3588Q;
                int size = a.size();
                for (int i = 0; i < size; i++) {
                    C2185a aVar = (C2185a) a.get(i);
                    if (z) {
                        aVar.mo16870a(ImageManager.this.f3579a, this.f3594Q, false);
                    } else {
                        ImageManager.this.f3586h.put(this.f3593P, Long.valueOf(SystemClock.elapsedRealtime()));
                        aVar.mo16871a(ImageManager.this.f3579a, ImageManager.this.f3583e, false);
                    }
                    ImageManager.this.f3584f.remove(aVar);
                }
            }
            this.f3595R.countDown();
            synchronized (ImageManager.f3577i) {
                ImageManager.f3578j.remove(this.f3593P);
            }
        }
    }
}
