package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2588kb;

/* renamed from: com.google.android.gms.measurement.internal.z0 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3258z0 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5785a = new C3258z0();

    private C3258z0() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2588kb.m6632b());
    }
}
