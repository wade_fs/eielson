package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.mb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2623mb implements C2638nb {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4338a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.validation.internal_limits_internal_event_params", false);

    /* renamed from: a */
    public final boolean mo17759a() {
        return f4338a.mo18128b().booleanValue();
    }
}
