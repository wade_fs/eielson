package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.auth.api.credentials.d */
public final class C1965d implements Parcelable.Creator<CredentialPickerConfig> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        boolean z = false;
        boolean z2 = false;
        boolean z3 = false;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                z = C2248a.m5577r(parcel, a);
            } else if (a2 == 2) {
                z2 = C2248a.m5577r(parcel, a);
            } else if (a2 == 3) {
                z3 = C2248a.m5577r(parcel, a);
            } else if (a2 == 4) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 != 1000) {
                C2248a.m5550F(parcel, a);
            } else {
                i = C2248a.m5585z(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new CredentialPickerConfig(i, z, z2, z3, i2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new CredentialPickerConfig[i];
    }
}
