package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.w6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3231w6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5718P;

    /* renamed from: Q */
    private final /* synthetic */ String f5719Q;

    /* renamed from: R */
    private final /* synthetic */ String f5720R;

    /* renamed from: S */
    private final /* synthetic */ String f5721S;

    /* renamed from: T */
    private final /* synthetic */ C3154p6 f5722T;

    C3231w6(C3154p6 p6Var, AtomicReference atomicReference, String str, String str2, String str3) {
        this.f5722T = p6Var;
        this.f5718P = atomicReference;
        this.f5719Q = str;
        this.f5720R = str2;
        this.f5721S = str3;
    }

    public final void run() {
        this.f5722T.f5134a.mo19079F().mo19386a(this.f5718P, this.f5719Q, this.f5720R, this.f5721S);
    }
}
