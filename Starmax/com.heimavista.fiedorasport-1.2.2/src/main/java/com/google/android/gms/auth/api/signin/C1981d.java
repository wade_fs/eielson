package com.google.android.gms.auth.api.signin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.d */
public class C1981d implements C2157k {

    /* renamed from: P */
    private Status f3122P;

    /* renamed from: Q */
    private GoogleSignInAccount f3123Q;

    public C1981d(@Nullable GoogleSignInAccount googleSignInAccount, @NonNull Status status) {
        this.f3123Q = googleSignInAccount;
        this.f3122P = status;
    }

    @Nullable
    /* renamed from: a */
    public GoogleSignInAccount mo16418a() {
        return this.f3123Q;
    }

    @NonNull
    /* renamed from: b */
    public Status mo16419b() {
        return this.f3122P;
    }
}
