package com.google.android.exoplayer2.source.ads;

import com.google.android.exoplayer2.source.ads.AdsMediaSource;
import java.io.IOException;

/* renamed from: com.google.android.exoplayer2.source.ads.b */
/* compiled from: lambda */
public final /* synthetic */ class C1860b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AdsMediaSource.AdPrepareErrorListener f2858P;

    /* renamed from: Q */
    private final /* synthetic */ IOException f2859Q;

    public /* synthetic */ C1860b(AdsMediaSource.AdPrepareErrorListener adPrepareErrorListener, IOException iOException) {
        this.f2858P = adPrepareErrorListener;
        this.f2859Q = iOException;
    }

    public final void run() {
        this.f2858P.mo15085a(this.f2859Q);
    }
}
