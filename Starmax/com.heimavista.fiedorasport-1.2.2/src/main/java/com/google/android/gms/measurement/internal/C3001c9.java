package com.google.android.gms.measurement.internal;

import androidx.annotation.WorkerThread;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.gms.internal.measurement.C2587ka;

/* renamed from: com.google.android.gms.measurement.internal.c9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3001c9 {

    /* renamed from: a */
    private C3013d9 f5019a;

    /* renamed from: b */
    private final Runnable f5020b = new C2989b9(this);

    /* renamed from: c */
    final /* synthetic */ C3244x8 f5021c;

    C3001c9(C3244x8 x8Var) {
        this.f5021c = x8Var;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo18830a() {
        this.f5021c.mo18881c();
        if (!this.f5021c.mo19013h().mo19146a(C3135o.f5397H0)) {
            return;
        }
        if (!C2587ka.m6629b() || !this.f5021c.mo19013h().mo19154e(this.f5021c.mo18885p().mo18800B(), C3135o.f5423U0)) {
            this.f5021c.f5760c.removeCallbacks(this.f5020b);
        } else if (this.f5019a != null) {
            this.f5021c.f5760c.removeCallbacks(this.f5019a);
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo18831b() {
        if (!this.f5021c.mo19013h().mo19146a(C3135o.f5397H0)) {
            return;
        }
        if (!C2587ka.m6629b() || !this.f5021c.mo19013h().mo19154e(this.f5021c.mo18885p().mo18800B(), C3135o.f5423U0)) {
            this.f5021c.f5760c.postDelayed(this.f5020b, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
            return;
        }
        this.f5019a = new C3013d9(this, this.f5021c.mo19017o().mo17132a());
        this.f5021c.f5760c.postDelayed(this.f5019a, AdaptiveTrackSelection.DEFAULT_MIN_TIME_BETWEEN_BUFFER_REEVALUTATION_MS);
    }
}
