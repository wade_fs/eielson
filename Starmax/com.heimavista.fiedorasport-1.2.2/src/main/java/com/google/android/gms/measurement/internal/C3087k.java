package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.k */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3087k {

    /* renamed from: a */
    final String f5278a;

    /* renamed from: b */
    final String f5279b;

    /* renamed from: c */
    final long f5280c;

    /* renamed from: d */
    final long f5281d;

    /* renamed from: e */
    final long f5282e;

    /* renamed from: f */
    final long f5283f;

    /* renamed from: g */
    final long f5284g;

    /* renamed from: h */
    final Long f5285h;

    /* renamed from: i */
    final Long f5286i;

    /* renamed from: j */
    final Long f5287j;

    /* renamed from: k */
    final Boolean f5288k;

    C3087k(String str, String str2, long j, long j2, long j3, long j4, long j5, Long l, Long l2, Long l3, Boolean bool) {
        long j6 = j;
        long j7 = j2;
        long j8 = j3;
        long j9 = j5;
        C2258v.m5639b(str);
        C2258v.m5639b(str2);
        boolean z = true;
        C2258v.m5636a(j6 >= 0);
        C2258v.m5636a(j7 >= 0);
        C2258v.m5636a(j8 >= 0);
        C2258v.m5636a(j9 < 0 ? false : z);
        this.f5278a = str;
        this.f5279b = str2;
        this.f5280c = j6;
        this.f5281d = j7;
        this.f5282e = j8;
        this.f5283f = j4;
        this.f5284g = j9;
        this.f5285h = l;
        this.f5286i = l2;
        this.f5287j = l3;
        this.f5288k = bool;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final C3087k mo19124a(long j) {
        return new C3087k(this.f5278a, this.f5279b, this.f5280c, this.f5281d, this.f5282e, j, this.f5284g, this.f5285h, this.f5286i, this.f5287j, this.f5288k);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final C3087k mo19125a(long j, long j2) {
        return new C3087k(this.f5278a, this.f5279b, this.f5280c, this.f5281d, this.f5282e, this.f5283f, j, Long.valueOf(j2), this.f5286i, this.f5287j, this.f5288k);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final C3087k mo19126a(Long l, Long l2, Boolean bool) {
        return new C3087k(this.f5278a, this.f5279b, this.f5280c, this.f5281d, this.f5282e, this.f5283f, this.f5284g, this.f5285h, l, l2, (bool == null || bool.booleanValue()) ? bool : null);
    }

    C3087k(String str, String str2, long j, long j2, long j3, long j4, Long l, Long l2, Long l3, Boolean bool) {
        this(str, str2, j, j2, 0, j3, 0, null, null, null, null);
    }
}
