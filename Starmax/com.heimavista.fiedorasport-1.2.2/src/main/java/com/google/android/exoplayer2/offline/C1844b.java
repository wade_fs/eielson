package com.google.android.exoplayer2.offline;

import com.google.android.exoplayer2.offline.DownloadHelper;
import java.io.IOException;

/* renamed from: com.google.android.exoplayer2.offline.b */
/* compiled from: lambda */
public final /* synthetic */ class C1844b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ DownloadHelper.C18391 f2841P;

    /* renamed from: Q */
    private final /* synthetic */ DownloadHelper.Callback f2842Q;

    /* renamed from: R */
    private final /* synthetic */ IOException f2843R;

    public /* synthetic */ C1844b(DownloadHelper.C18391 r1, DownloadHelper.Callback callback, IOException iOException) {
        this.f2841P = r1;
        this.f2842Q = callback;
        this.f2843R = iOException;
    }

    public final void run() {
        this.f2841P.mo14711a(this.f2842Q, this.f2843R);
    }
}
