package com.google.android.gms.internal.measurement;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.x6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
class C2786x6 extends AbstractSet<Map.Entry<K, V>> {

    /* renamed from: P */
    private final /* synthetic */ C2618m6 f4569P;

    private C2786x6(C2618m6 m6Var) {
        this.f4569P = m6Var;
    }

    public /* synthetic */ boolean add(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (contains(entry)) {
            return false;
        }
        this.f4569P.put((Comparable) entry.getKey(), entry.getValue());
        return true;
    }

    public void clear() {
        this.f4569P.clear();
    }

    public boolean contains(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        Object obj2 = this.f4569P.get(entry.getKey());
        Object value = entry.getValue();
        if (obj2 != value) {
            return obj2 != null && obj2.equals(value);
        }
        return true;
    }

    public Iterator<Map.Entry<K, V>> iterator() {
        return new C2740u6(this.f4569P, null);
    }

    public boolean remove(Object obj) {
        Map.Entry entry = (Map.Entry) obj;
        if (!contains(entry)) {
            return false;
        }
        this.f4569P.remove(entry.getKey());
        return true;
    }

    public int size() {
        return this.f4569P.size();
    }

    /* synthetic */ C2786x6(C2618m6 m6Var, C2663p6 p6Var) {
        this(m6Var);
    }
}
