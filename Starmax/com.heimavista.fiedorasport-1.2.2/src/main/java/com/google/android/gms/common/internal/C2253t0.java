package com.google.android.gms.common.internal;

import androidx.annotation.NonNull;
import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;

/* renamed from: com.google.android.gms.common.internal.t0 */
public final class C2253t0 {
    @NonNull

    /* renamed from: a */
    private final String f3756a;
    @NonNull

    /* renamed from: b */
    private final String f3757b;

    /* renamed from: c */
    private final int f3758c = TsExtractor.TS_STREAM_TYPE_AC3;

    public C2253t0(@NonNull String str, @NonNull String str2, boolean z, int i) {
        this.f3757b = str;
        this.f3756a = str2;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public final String mo17039a() {
        return this.f3757b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final int mo17040b() {
        return this.f3758c;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: c */
    public final String mo17041c() {
        return this.f3756a;
    }
}
