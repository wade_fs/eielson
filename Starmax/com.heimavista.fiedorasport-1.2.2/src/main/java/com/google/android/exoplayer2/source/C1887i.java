package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.i */
/* compiled from: lambda */
public final /* synthetic */ class C1887i implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2903P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2904Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSource.MediaPeriodId f2905R;

    public /* synthetic */ C1887i(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId) {
        this.f2903P = eventDispatcher;
        this.f2904Q = mediaSourceEventListener;
        this.f2905R = mediaPeriodId;
    }

    public final void run() {
        this.f2903P.mo14929a(this.f2904Q, this.f2905R);
    }
}
