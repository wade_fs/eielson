package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2555ib;

/* renamed from: com.google.android.gms.measurement.internal.r1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3171r1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5581a = new C3171r1();

    private C3171r1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2555ib.m6488b());
    }
}
