package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.util.Pair;
import androidx.annotation.NonNull;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.C2044i;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.C2158l;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2235n;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.base.C2382h;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

@KeepName
public abstract class BasePendingResult<R extends C2157k> extends C2040g<R> {

    /* renamed from: p */
    static final ThreadLocal<Boolean> f3222p = new C2098k2();

    /* renamed from: a */
    private final Object f3223a;

    /* renamed from: b */
    private final C2045a<R> f3224b;

    /* renamed from: c */
    private final WeakReference<C2036f> f3225c;

    /* renamed from: d */
    private final CountDownLatch f3226d;

    /* renamed from: e */
    private final ArrayList<C2040g.C2041a> f3227e;

    /* renamed from: f */
    private C2158l<? super R> f3228f;

    /* renamed from: g */
    private final AtomicReference<C2152y1> f3229g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public R f3230h;

    /* renamed from: i */
    private Status f3231i;

    /* renamed from: j */
    private volatile boolean f3232j;

    /* renamed from: k */
    private boolean f3233k;

    /* renamed from: l */
    private boolean f3234l;

    /* renamed from: m */
    private C2235n f3235m;

    /* renamed from: n */
    private volatile C2129s1<R> f3236n;

    /* renamed from: o */
    private boolean f3237o;

    /* renamed from: com.google.android.gms.common.api.internal.BasePendingResult$a */
    public static class C2045a<R extends C2157k> extends C2382h {
        public C2045a(Looper looper) {
            super(looper);
        }

        /* renamed from: a */
        public final void mo16600a(C2158l<? super R> lVar, R r) {
            sendMessage(obtainMessage(1, new Pair(lVar, r)));
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                Pair pair = (Pair) message.obj;
                C2158l lVar = (C2158l) pair.first;
                C2157k kVar = (C2157k) pair.second;
                try {
                    lVar.mo16767a(kVar);
                } catch (RuntimeException e) {
                    BasePendingResult.m4726c(kVar);
                    throw e;
                }
            } else if (i != 2) {
                StringBuilder sb = new StringBuilder(45);
                sb.append("Don't know how to handle message: ");
                sb.append(i);
                Log.wtf("BasePendingResult", sb.toString(), new Exception());
            } else {
                ((BasePendingResult) message.obj).mo16594b(Status.f3180W);
            }
        }
    }

    /* renamed from: com.google.android.gms.common.api.internal.BasePendingResult$b */
    private final class C2046b {
        private C2046b() {
        }

        /* access modifiers changed from: protected */
        public final void finalize() {
            BasePendingResult.m4726c(BasePendingResult.this.f3230h);
            super.finalize();
        }

        /* synthetic */ C2046b(BasePendingResult basePendingResult, C2098k2 k2Var) {
            this();
        }
    }

    @Deprecated
    BasePendingResult() {
        this.f3223a = new Object();
        this.f3226d = new CountDownLatch(1);
        this.f3227e = new ArrayList<>();
        this.f3229g = new AtomicReference<>();
        this.f3237o = false;
        this.f3224b = new C2045a<>(Looper.getMainLooper());
        this.f3225c = new WeakReference<>(null);
    }

    /* renamed from: g */
    private final R mo16641g() {
        R r;
        synchronized (this.f3223a) {
            C2258v.m5641b(!this.f3232j, "Result has already been consumed.");
            C2258v.m5641b(mo16596c(), "Result is not ready.");
            r = this.f3230h;
            this.f3230h = null;
            this.f3228f = null;
            this.f3232j = true;
        }
        C2152y1 andSet = this.f3229g.getAndSet(null);
        if (andSet != null) {
            andSet.mo16795a(this);
        }
        return r;
    }

    /* renamed from: a */
    public final R mo16586a(long j, TimeUnit timeUnit) {
        if (j > 0) {
            C2258v.m5643c("await must not be called on the UI thread when time is greater than zero.");
        }
        boolean z = true;
        C2258v.m5641b(!this.f3232j, "Result has already been consumed.");
        if (this.f3236n != null) {
            z = false;
        }
        C2258v.m5641b(z, "Cannot await if then() has been called.");
        try {
            if (!this.f3226d.await(j, timeUnit)) {
                mo16594b(Status.f3180W);
            }
        } catch (InterruptedException unused) {
            mo16594b(Status.f3178U);
        }
        C2258v.m5641b(mo16596c(), "Result is not ready.");
        return mo16641g();
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: a */
    public abstract R mo16455a(Status status);

    /* renamed from: b */
    public boolean mo16595b() {
        boolean z;
        synchronized (this.f3223a) {
            z = this.f3233k;
        }
        return z;
    }

    /* renamed from: c */
    public final boolean mo16596c() {
        return this.f3226d.getCount() == 0;
    }

    /* renamed from: d */
    public final Integer mo16597d() {
        return null;
    }

    /* renamed from: e */
    public final boolean mo16598e() {
        boolean b;
        synchronized (this.f3223a) {
            if (this.f3225c.get() == null || !this.f3237o) {
                mo16591a();
            }
            b = mo16595b();
        }
        return b;
    }

    /* renamed from: f */
    public final void mo16599f() {
        this.f3237o = this.f3237o || f3222p.get().booleanValue();
    }

    /* renamed from: c */
    public static void m4726c(C2157k kVar) {
        if (kVar instanceof C2044i) {
            try {
                ((C2044i) kVar).release();
            } catch (RuntimeException e) {
                String valueOf = String.valueOf(kVar);
                StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 18);
                sb.append("Unable to release ");
                sb.append(valueOf);
                Log.w("BasePendingResult", sb.toString(), e);
            }
        }
    }

    /* renamed from: b */
    public final void mo16594b(Status status) {
        synchronized (this.f3223a) {
            if (!mo16596c()) {
                mo16593a(mo16455a(status));
                this.f3234l = true;
            }
        }
    }

    protected BasePendingResult(C2036f fVar) {
        this.f3223a = new Object();
        this.f3226d = new CountDownLatch(1);
        this.f3227e = new ArrayList<>();
        this.f3229g = new AtomicReference<>();
        this.f3237o = false;
        this.f3224b = new C2045a<>(fVar != null ? fVar.mo16574f() : Looper.getMainLooper());
        this.f3225c = new WeakReference<>(fVar);
    }

    /* renamed from: b */
    private final void mo16639b(R r) {
        this.f3230h = r;
        this.f3235m = null;
        this.f3226d.countDown();
        this.f3231i = this.f3230h.mo16419b();
        if (this.f3233k) {
            this.f3228f = null;
        } else if (this.f3228f != null) {
            this.f3224b.removeMessages(2);
            this.f3224b.mo16600a(this.f3228f, mo16641g());
        } else if (this.f3230h instanceof C2044i) {
            new C2046b(this, null);
        }
        ArrayList<C2040g.C2041a> arrayList = this.f3227e;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            C2040g.C2041a aVar = arrayList.get(i);
            i++;
            aVar.mo16589a(this.f3231i);
        }
        this.f3227e.clear();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x003e, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo16588a(com.google.android.gms.common.api.C2158l r6) {
        /*
            r5 = this;
            java.lang.Object r0 = r5.f3223a
            monitor-enter(r0)
            if (r6 != 0) goto L_0x000a
            r6 = 0
            r5.f3228f = r6     // Catch:{ all -> 0x003f }
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return
        L_0x000a:
            boolean r1 = r5.f3232j     // Catch:{ all -> 0x003f }
            r2 = 1
            r3 = 0
            if (r1 != 0) goto L_0x0012
            r1 = 1
            goto L_0x0013
        L_0x0012:
            r1 = 0
        L_0x0013:
            java.lang.String r4 = "Result has already been consumed."
            com.google.android.gms.common.internal.C2258v.m5641b(r1, r4)     // Catch:{ all -> 0x003f }
            com.google.android.gms.common.api.internal.s1<R> r1 = r5.f3236n     // Catch:{ all -> 0x003f }
            if (r1 != 0) goto L_0x001d
            goto L_0x001e
        L_0x001d:
            r2 = 0
        L_0x001e:
            java.lang.String r1 = "Cannot set callbacks if then() has been called."
            com.google.android.gms.common.internal.C2258v.m5641b(r2, r1)     // Catch:{ all -> 0x003f }
            boolean r1 = r5.mo16595b()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x002b
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return
        L_0x002b:
            boolean r1 = r5.mo16596c()     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x003b
            com.google.android.gms.common.api.internal.BasePendingResult$a<R> r1 = r5.f3224b     // Catch:{ all -> 0x003f }
            com.google.android.gms.common.api.k r2 = r5.mo16641g()     // Catch:{ all -> 0x003f }
            r1.mo16600a(r6, r2)     // Catch:{ all -> 0x003f }
            goto L_0x003d
        L_0x003b:
            r5.f3228f = r6     // Catch:{ all -> 0x003f }
        L_0x003d:
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return
        L_0x003f:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.BasePendingResult.mo16588a(com.google.android.gms.common.api.l):void");
    }

    /* renamed from: a */
    public final void mo16587a(C2040g.C2041a aVar) {
        C2258v.m5637a(aVar != null, "Callback cannot be null.");
        synchronized (this.f3223a) {
            if (mo16596c()) {
                aVar.mo16589a(this.f3231i);
            } else {
                this.f3227e.add(aVar);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|(2:10|11)|12|13|14|15) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0029, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0015 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo16591a() {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f3223a
            monitor-enter(r0)
            boolean r1 = r2.f3233k     // Catch:{ all -> 0x002a }
            if (r1 != 0) goto L_0x0028
            boolean r1 = r2.f3232j     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x000c
            goto L_0x0028
        L_0x000c:
            com.google.android.gms.common.internal.n r1 = r2.f3235m     // Catch:{ all -> 0x002a }
            if (r1 == 0) goto L_0x0015
            com.google.android.gms.common.internal.n r1 = r2.f3235m     // Catch:{ RemoteException -> 0x0015 }
            r1.cancel()     // Catch:{ RemoteException -> 0x0015 }
        L_0x0015:
            R r1 = r2.f3230h     // Catch:{ all -> 0x002a }
            m4726c(r1)     // Catch:{ all -> 0x002a }
            r1 = 1
            r2.f3233k = r1     // Catch:{ all -> 0x002a }
            com.google.android.gms.common.api.Status r1 = com.google.android.gms.common.api.Status.f3181X     // Catch:{ all -> 0x002a }
            com.google.android.gms.common.api.k r1 = r2.mo16455a(r1)     // Catch:{ all -> 0x002a }
            r2.mo16639b(r1)     // Catch:{ all -> 0x002a }
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x0028:
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            return
        L_0x002a:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002a }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.BasePendingResult.mo16591a():void");
    }

    /* renamed from: a */
    public final void mo16593a(C2157k kVar) {
        synchronized (this.f3223a) {
            if (this.f3234l || this.f3233k) {
                m4726c(kVar);
                return;
            }
            mo16596c();
            boolean z = true;
            C2258v.m5641b(!mo16596c(), "Results have already been set");
            if (this.f3232j) {
                z = false;
            }
            C2258v.m5641b(z, "Result has already been consumed");
            mo16639b(kVar);
        }
    }

    /* renamed from: a */
    public final void mo16592a(C2152y1 y1Var) {
        this.f3229g.set(y1Var);
    }
}
