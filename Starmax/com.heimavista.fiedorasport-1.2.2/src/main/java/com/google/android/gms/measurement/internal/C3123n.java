package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.measurement.internal.n */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3123n implements Parcelable.Creator<zzam> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        Bundle bundle = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            if (C2248a.m5551a(a) != 2) {
                C2248a.m5550F(parcel, a);
            } else {
                bundle = C2248a.m5565f(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzam(bundle);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzam[i];
    }
}
