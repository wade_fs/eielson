package com.google.android.gms.location;

import android.app.PendingIntent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.location.r */
public final class C2857r implements Parcelable.Creator<zzal> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        ArrayList<String> arrayList = null;
        String str = "";
        PendingIntent pendingIntent = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                arrayList = C2248a.m5575p(parcel, a);
            } else if (a2 == 2) {
                pendingIntent = (PendingIntent) C2248a.m5553a(parcel, a, PendingIntent.CREATOR);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                str = C2248a.m5573n(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzal(arrayList, pendingIntent, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzal[i];
    }
}
