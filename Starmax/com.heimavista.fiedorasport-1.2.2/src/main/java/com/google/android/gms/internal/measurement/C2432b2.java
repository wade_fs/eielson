package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.b2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2432b2 implements C2613m1 {
    @GuardedBy("SharedPreferencesLoader.class")

    /* renamed from: f */
    private static final Map<String, C2432b2> f3989f = new ArrayMap();

    /* renamed from: a */
    private final SharedPreferences f3990a;

    /* renamed from: b */
    private final SharedPreferences.OnSharedPreferenceChangeListener f3991b = new C2481e2(this);

    /* renamed from: c */
    private final Object f3992c = new Object();

    /* renamed from: d */
    private volatile Map<String, ?> f3993d;
    @GuardedBy("this")

    /* renamed from: e */
    private final List<C2560j1> f3994e = new ArrayList();

    private C2432b2(SharedPreferences sharedPreferences) {
        this.f3990a = sharedPreferences;
        this.f3990a.registerOnSharedPreferenceChangeListener(this.f3991b);
    }

    /* renamed from: a */
    static C2432b2 m6042a(Context context, String str) {
        C2432b2 b2Var;
        if (!((!C2480e1.m6245a() || str.startsWith("direct_boot:")) ? true : C2480e1.m6246a(context))) {
            return null;
        }
        synchronized (C2432b2.class) {
            b2Var = f3989f.get(str);
            if (b2Var == null) {
                b2Var = new C2432b2(m6044b(context, str));
                f3989f.put(str, b2Var);
            }
        }
        return b2Var;
    }

    /* renamed from: b */
    private static SharedPreferences m6044b(Context context, String str) {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            if (str.startsWith("direct_boot:")) {
                if (C2480e1.m6245a()) {
                    context = context.createDeviceProtectedStorageContext();
                }
                return context.getSharedPreferences(str.substring(12), 0);
            }
            SharedPreferences sharedPreferences = context.getSharedPreferences(str, 0);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return sharedPreferences;
        } finally {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public final Object mo17308a(String str) {
        Map<String, ?> map = this.f3993d;
        if (map == null) {
            synchronized (this.f3992c) {
                map = this.f3993d;
                if (map == null) {
                    StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
                    try {
                        Map<String, ?> all = this.f3990a.getAll();
                        this.f3993d = all;
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        map = all;
                    } catch (Throwable th) {
                        StrictMode.setThreadPolicy(allowThreadDiskReads);
                        throw th;
                    }
                }
            }
        }
        if (map != null) {
            return map.get(str);
        }
        return null;
    }

    /* renamed from: a */
    static synchronized void m6043a() {
        synchronized (C2432b2.class) {
            for (C2432b2 b2Var : f3989f.values()) {
                b2Var.f3990a.unregisterOnSharedPreferenceChangeListener(b2Var.f3991b);
            }
            f3989f.clear();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo17309a(SharedPreferences sharedPreferences, String str) {
        synchronized (this.f3992c) {
            this.f3993d = null;
            C2765w1.m7695c();
        }
        synchronized (this) {
            for (C2560j1 j1Var : this.f3994e) {
                j1Var.mo17586a();
            }
        }
    }
}
