package com.google.android.gms.maps.p093i;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import androidx.annotation.Nullable;
import com.google.android.gms.common.C2175e;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.dynamite.DynamiteModule;

/* renamed from: com.google.android.gms.maps.i.b0 */
public class C2892b0 {

    /* renamed from: a */
    private static final String f4792a = "b0";
    @SuppressLint({"StaticFieldLeak"})
    @Nullable

    /* renamed from: b */
    private static Context f4793b;

    /* renamed from: c */
    private static C2894c0 f4794c;

    /* JADX WARN: Type inference failed for: r1v4, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.maps.p093i.C2894c0 m8145a(android.content.Context r3) {
        /*
            com.google.android.gms.common.internal.C2258v.m5629a(r3)
            com.google.android.gms.maps.i.c0 r0 = com.google.android.gms.maps.p093i.C2892b0.f4794c
            if (r0 == 0) goto L_0x0008
            return r0
        L_0x0008:
            r0 = 13400000(0xcc77c0, float:1.87774E-38)
            int r0 = com.google.android.gms.common.C2175e.m5290a(r3, r0)
            if (r0 != 0) goto L_0x005f
            java.lang.String r0 = com.google.android.gms.maps.p093i.C2892b0.f4792a
            java.lang.String r1 = "Making Creator dynamically"
            android.util.Log.i(r0, r1)
            android.content.Context r0 = m8148b(r3)
            java.lang.ClassLoader r0 = r0.getClassLoader()
            java.lang.String r1 = "com.google.android.gms.maps.internal.CreatorImpl"
            java.lang.Object r0 = m8147a(r0, r1)
            android.os.IBinder r0 = (android.os.IBinder) r0
            if (r0 != 0) goto L_0x002c
            r0 = 0
            goto L_0x0040
        L_0x002c:
            java.lang.String r1 = "com.google.android.gms.maps.internal.ICreator"
            android.os.IInterface r1 = r0.queryLocalInterface(r1)
            boolean r2 = r1 instanceof com.google.android.gms.maps.p093i.C2894c0
            if (r2 == 0) goto L_0x003a
            r0 = r1
            com.google.android.gms.maps.i.c0 r0 = (com.google.android.gms.maps.p093i.C2894c0) r0
            goto L_0x0040
        L_0x003a:
            com.google.android.gms.maps.i.d0 r1 = new com.google.android.gms.maps.i.d0
            r1.<init>(r0)
            r0 = r1
        L_0x0040:
            com.google.android.gms.maps.p093i.C2892b0.f4794c = r0
            com.google.android.gms.maps.i.c0 r0 = com.google.android.gms.maps.p093i.C2892b0.f4794c     // Catch:{ RemoteException -> 0x0058 }
            android.content.Context r3 = m8148b(r3)     // Catch:{ RemoteException -> 0x0058 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ RemoteException -> 0x0058 }
            e.d.a.c.b.b r3 = p119e.p144d.p145a.p157c.p160b.C3992d.m11990a(r3)     // Catch:{ RemoteException -> 0x0058 }
            int r1 = com.google.android.gms.common.C2175e.f3565f     // Catch:{ RemoteException -> 0x0058 }
            r0.mo18444a(r3, r1)     // Catch:{ RemoteException -> 0x0058 }
            com.google.android.gms.maps.i.c0 r3 = com.google.android.gms.maps.p093i.C2892b0.f4794c
            return r3
        L_0x0058:
            r3 = move-exception
            com.google.android.gms.maps.model.e r0 = new com.google.android.gms.maps.model.e
            r0.<init>(r3)
            throw r0
        L_0x005f:
            com.google.android.gms.common.d r3 = new com.google.android.gms.common.d
            r3.<init>(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2892b0.m8145a(android.content.Context):com.google.android.gms.maps.i.c0");
    }

    @Nullable
    /* renamed from: b */
    private static Context m8148b(Context context) {
        Context context2 = f4793b;
        if (context2 != null) {
            return context2;
        }
        Context c = m8149c(context);
        f4793b = c;
        return c;
    }

    @Nullable
    /* renamed from: c */
    private static Context m8149c(Context context) {
        try {
            return DynamiteModule.m5837a(context, DynamiteModule.f3886i, "com.google.android.gms.maps_dynamite").mo17142a();
        } catch (Exception e) {
            Log.e(f4792a, "Failed to load maps module, use legacy", e);
            return C2175e.m5291c(context);
        }
    }

    /* renamed from: a */
    private static <T> T m8147a(ClassLoader classLoader, String str) {
        try {
            C2258v.m5629a(classLoader);
            return m8146a(classLoader.loadClass(str));
        } catch (ClassNotFoundException unused) {
            String valueOf = String.valueOf(str);
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to find dynamic class ".concat(valueOf) : new String("Unable to find dynamic class "));
        }
    }

    /* renamed from: a */
    private static <T> T m8146a(Class<?> cls) {
        try {
            return cls.newInstance();
        } catch (InstantiationException unused) {
            String valueOf = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf.length() != 0 ? "Unable to instantiate the dynamic class ".concat(valueOf) : new String("Unable to instantiate the dynamic class "));
        } catch (IllegalAccessException unused2) {
            String valueOf2 = String.valueOf(cls.getName());
            throw new IllegalStateException(valueOf2.length() != 0 ? "Unable to call the default constructor of ".concat(valueOf2) : new String("Unable to call the default constructor of "));
        }
    }
}
