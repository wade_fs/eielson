package com.google.android.gms.internal.base;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: com.google.android.gms.internal.base.h */
public class C2382h extends Handler {
    public C2382h(Looper looper) {
        super(looper);
    }

    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }

    public C2382h(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
