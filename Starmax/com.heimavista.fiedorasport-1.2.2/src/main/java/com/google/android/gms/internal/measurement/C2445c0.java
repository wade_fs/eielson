package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.c0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2445c0 extends C2595l4<C2445c0, C2446a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2445c0 zzl;
    private static volatile C2501f6<C2445c0> zzm;
    private int zzc;
    private int zzd;
    private String zze = "";
    private C2738u4<C2461d0> zzf = C2595l4.m6649m();
    private boolean zzg;
    private C2477e0 zzh;
    private boolean zzi;
    private boolean zzj;
    private boolean zzk;

    /* renamed from: com.google.android.gms.internal.measurement.c0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2446a extends C2595l4.C2596a<C2445c0, C2446a> implements C2769w5 {
        private C2446a() {
            super(C2445c0.zzl);
        }

        /* renamed from: a */
        public final C2446a mo17359a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2445c0) super.f4288Q).m6105a(str);
            return this;
        }

        /* renamed from: j */
        public final String mo17361j() {
            return ((C2445c0) super.f4288Q).mo17349p();
        }

        /* renamed from: k */
        public final int mo17362k() {
            return ((C2445c0) super.f4288Q).mo17351s();
        }

        /* synthetic */ C2446a(C2413a0 a0Var) {
            this();
        }

        /* renamed from: a */
        public final C2461d0 mo17360a(int i) {
            return ((C2445c0) super.f4288Q).mo17346b(i);
        }

        /* renamed from: a */
        public final C2446a mo17358a(int i, C2461d0 d0Var) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2445c0) super.f4288Q).m6102a(i, d0Var);
            return this;
        }
    }

    static {
        C2445c0 c0Var = new C2445c0();
        zzl = c0Var;
        C2595l4.m6645a(C2445c0.class, super);
    }

    private C2445c0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6105a(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    /* renamed from: z */
    public static C2446a m6106z() {
        return (C2446a) zzl.mo17668i();
    }

    /* renamed from: b */
    public final C2461d0 mo17346b(int i) {
        return this.zzf.get(i);
    }

    /* renamed from: n */
    public final boolean mo17347n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final int mo17348o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final String mo17349p() {
        return this.zze;
    }

    /* renamed from: q */
    public final List<C2461d0> mo17350q() {
        return this.zzf;
    }

    /* renamed from: s */
    public final int mo17351s() {
        return this.zzf.size();
    }

    /* renamed from: t */
    public final boolean mo17352t() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: u */
    public final C2477e0 mo17353u() {
        C2477e0 e0Var = this.zzh;
        return e0Var == null ? C2477e0.m6230y() : e0Var;
    }

    /* renamed from: v */
    public final boolean mo17354v() {
        return this.zzi;
    }

    /* renamed from: w */
    public final boolean mo17355w() {
        return this.zzj;
    }

    /* renamed from: x */
    public final boolean mo17356x() {
        return (this.zzc & 64) != 0;
    }

    /* renamed from: y */
    public final boolean mo17357y() {
        return this.zzk;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6102a(int i, C2461d0 d0Var) {
        d0Var.getClass();
        if (!this.zzf.mo17938a()) {
            this.zzf = C2595l4.m6642a(this.zzf);
        }
        this.zzf.set(i, d0Var);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2413a0.f3970a[i - 1]) {
            case 1:
                return new C2445c0();
            case 2:
                return new C2446a(null);
            case 3:
                return C2595l4.m6643a(zzl, "\u0001\b\u0000\u0001\u0001\b\b\u0000\u0001\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\u001b\u0004\u0007\u0002\u0005\t\u0003\u0006\u0007\u0004\u0007\u0007\u0005\b\u0007\u0006", new Object[]{"zzc", "zzd", "zze", "zzf", C2461d0.class, "zzg", "zzh", "zzi", "zzj", "zzk"});
            case 4:
                return zzl;
            case 5:
                C2501f6<C2445c0> f6Var = zzm;
                if (f6Var == null) {
                    synchronized (C2445c0.class) {
                        f6Var = zzm;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzl);
                            zzm = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
