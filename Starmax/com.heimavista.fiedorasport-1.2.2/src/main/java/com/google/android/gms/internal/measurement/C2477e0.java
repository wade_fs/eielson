package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.e0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2477e0 extends C2595l4<C2477e0, C2478a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2477e0 zzi;
    private static volatile C2501f6<C2477e0> zzj;
    private int zzc;
    private int zzd;
    private boolean zze;
    private String zzf = "";
    private String zzg = "";
    private String zzh = "";

    /* renamed from: com.google.android.gms.internal.measurement.e0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2478a extends C2595l4.C2596a<C2477e0, C2478a> implements C2769w5 {
        private C2478a() {
            super(C2477e0.zzi);
        }

        /* synthetic */ C2478a(C2413a0 a0Var) {
            this();
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.e0$b */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public enum C2479b implements C2631n4 {
        UNKNOWN_COMPARISON_TYPE(0),
        LESS_THAN(1),
        GREATER_THAN(2),
        EQUAL(3),
        BETWEEN(4);
        

        /* renamed from: P */
        private final int f4065P;

        static {
            new C2529h0();
        }

        private C2479b(int i) {
            this.f4065P = i;
        }

        /* renamed from: a */
        public static C2479b m6243a(int i) {
            if (i == 0) {
                return UNKNOWN_COMPARISON_TYPE;
            }
            if (i == 1) {
                return LESS_THAN;
            }
            if (i == 2) {
                return GREATER_THAN;
            }
            if (i == 3) {
                return EQUAL;
            }
            if (i != 4) {
                return null;
            }
            return BETWEEN;
        }

        /* renamed from: e */
        public static C2661p4 m6244e() {
            return C2544i0.f4225a;
        }

        public final String toString() {
            return "<" + C2479b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.f4065P + " name=" + name() + '>';
        }
    }

    static {
        C2477e0 e0Var = new C2477e0();
        zzi = e0Var;
        C2595l4.m6645a(C2477e0.class, super);
    }

    private C2477e0() {
    }

    /* renamed from: y */
    public static C2477e0 m6230y() {
        return zzi;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2413a0.f3970a[i - 1]) {
            case 1:
                return new C2477e0();
            case 2:
                return new C2478a(null);
            case 3:
                return C2595l4.m6643a(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0000\u0000\u0001\f\u0000\u0002\u0007\u0001\u0003\b\u0002\u0004\b\u0003\u0005\b\u0004", new Object[]{"zzc", "zzd", C2479b.m6244e(), "zze", "zzf", "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                C2501f6<C2477e0> f6Var = zzj;
                if (f6Var == null) {
                    synchronized (C2477e0.class) {
                        f6Var = zzj;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzi);
                            zzj = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /* renamed from: n */
    public final boolean mo17430n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final C2479b mo17431o() {
        C2479b a = C2479b.m6243a(this.zzd);
        return a == null ? C2479b.UNKNOWN_COMPARISON_TYPE : a;
    }

    /* renamed from: p */
    public final boolean mo17432p() {
        return (this.zzc & 2) != 0;
    }

    /* renamed from: q */
    public final boolean mo17433q() {
        return this.zze;
    }

    /* renamed from: s */
    public final boolean mo17434s() {
        return (this.zzc & 4) != 0;
    }

    /* renamed from: t */
    public final String mo17435t() {
        return this.zzf;
    }

    /* renamed from: u */
    public final boolean mo17436u() {
        return (this.zzc & 8) != 0;
    }

    /* renamed from: v */
    public final String mo17437v() {
        return this.zzg;
    }

    /* renamed from: w */
    public final boolean mo17438w() {
        return (this.zzc & 16) != 0;
    }

    /* renamed from: x */
    public final String mo17439x() {
        return this.zzh;
    }
}
