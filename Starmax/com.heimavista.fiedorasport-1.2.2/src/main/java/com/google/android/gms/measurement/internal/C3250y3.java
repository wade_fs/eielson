package com.google.android.gms.measurement.internal;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.internal.measurement.C2412a;
import com.google.android.gms.internal.measurement.C2670q;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.y3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3250y3 extends C2412a implements C3228w3 {
    C3250y3(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.measurement.internal.IMeasurementService");
    }

    /* renamed from: a */
    public final void mo19189a(zzan zzan, zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzan);
        C2670q.m7046a(H, zzm);
        mo17246b(1, H);
    }

    /* renamed from: b */
    public final void mo19197b(zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzm);
        mo17246b(6, H);
    }

    /* renamed from: c */
    public final String mo19198c(zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzm);
        Parcel a = mo17244a(11, H);
        String readString = a.readString();
        a.recycle();
        return readString;
    }

    /* renamed from: d */
    public final void mo19199d(zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzm);
        mo17246b(4, H);
    }

    /* renamed from: a */
    public final void mo19191a(zzkq zzkq, zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzkq);
        C2670q.m7046a(H, zzm);
        mo17246b(2, H);
    }

    /* renamed from: a */
    public final void mo19190a(zzan zzan, String str, String str2) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzan);
        H.writeString(str);
        H.writeString(str2);
        mo17246b(5, H);
    }

    /* renamed from: a */
    public final byte[] mo19195a(zzan zzan, String str) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzan);
        H.writeString(str);
        Parcel a = mo17244a(9, H);
        byte[] createByteArray = a.createByteArray();
        a.recycle();
        return createByteArray;
    }

    /* renamed from: a */
    public final void mo19188a(long j, String str, String str2, String str3) {
        Parcel H = mo17243H();
        H.writeLong(j);
        H.writeString(str);
        H.writeString(str2);
        H.writeString(str3);
        mo17246b(10, H);
    }

    /* renamed from: a */
    public final void mo19194a(zzv zzv, zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzv);
        C2670q.m7046a(H, zzm);
        mo17246b(12, H);
    }

    /* renamed from: a */
    public final void mo19193a(zzv zzv) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzv);
        mo17246b(13, H);
    }

    /* renamed from: a */
    public final List<zzkq> mo19187a(String str, String str2, boolean z, zzm zzm) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7047a(H, z);
        C2670q.m7046a(H, zzm);
        Parcel a = mo17244a(14, H);
        ArrayList createTypedArrayList = a.createTypedArrayList(zzkq.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    /* renamed from: a */
    public final List<zzkq> mo19186a(String str, String str2, String str3, boolean z) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        H.writeString(str3);
        C2670q.m7047a(H, z);
        Parcel a = mo17244a(15, H);
        ArrayList createTypedArrayList = a.createTypedArrayList(zzkq.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    /* renamed from: a */
    public final List<zzv> mo19184a(String str, String str2, zzm zzm) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        C2670q.m7046a(H, zzm);
        Parcel a = mo17244a(16, H);
        ArrayList createTypedArrayList = a.createTypedArrayList(zzv.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    /* renamed from: a */
    public final List<zzv> mo19185a(String str, String str2, String str3) {
        Parcel H = mo17243H();
        H.writeString(str);
        H.writeString(str2);
        H.writeString(str3);
        Parcel a = mo17244a(17, H);
        ArrayList createTypedArrayList = a.createTypedArrayList(zzv.CREATOR);
        a.recycle();
        return createTypedArrayList;
    }

    /* renamed from: a */
    public final void mo19192a(zzm zzm) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, zzm);
        mo17246b(18, H);
    }
}
