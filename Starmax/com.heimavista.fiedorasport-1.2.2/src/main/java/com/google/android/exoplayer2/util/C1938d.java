package com.google.android.exoplayer2.util;

import java.util.concurrent.ThreadFactory;

/* renamed from: com.google.android.exoplayer2.util.d */
/* compiled from: lambda */
public final /* synthetic */ class C1938d implements ThreadFactory {

    /* renamed from: P */
    private final /* synthetic */ String f2945P;

    public /* synthetic */ C1938d(String str) {
        this.f2945P = str;
    }

    public final Thread newThread(Runnable runnable) {
        return Util.m4446a(this.f2945P, runnable);
    }
}
