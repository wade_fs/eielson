package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1733g;
import java.util.Set;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.d */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C1728d extends C1733g.C1735b {

    /* renamed from: a */
    private final long f2723a;

    /* renamed from: b */
    private final long f2724b;

    /* renamed from: c */
    private final Set<C1733g.C1737c> f2725c;

    /* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.d$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static final class C1730b extends C1733g.C1735b.C1736a {

        /* renamed from: a */
        private Long f2726a;

        /* renamed from: b */
        private Long f2727b;

        /* renamed from: c */
        private Set<C1733g.C1737c> f2728c;

        C1730b() {
        }

        /* renamed from: a */
        public C1733g.C1735b.C1736a mo13575a(long j) {
            this.f2726a = Long.valueOf(j);
            return super;
        }

        /* renamed from: b */
        public C1733g.C1735b.C1736a mo13578b(long j) {
            this.f2727b = Long.valueOf(j);
            return super;
        }

        /* renamed from: a */
        public C1733g.C1735b.C1736a mo13576a(Set<C1733g.C1737c> set) {
            if (set != null) {
                this.f2728c = set;
                return super;
            }
            throw new NullPointerException("Null flags");
        }

        /* renamed from: a */
        public C1733g.C1735b mo13577a() {
            String str = "";
            if (this.f2726a == null) {
                str = str + " delta";
            }
            if (this.f2727b == null) {
                str = str + " maxAllowedDelay";
            }
            if (this.f2728c == null) {
                str = str + " flags";
            }
            if (str.isEmpty()) {
                return new C1728d(this.f2726a.longValue(), this.f2727b.longValue(), this.f2728c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public long mo13569a() {
        return this.f2723a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public Set<C1733g.C1737c> mo13570b() {
        return this.f2725c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public long mo13571c() {
        return this.f2724b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1733g.C1735b)) {
            return false;
        }
        C1733g.C1735b bVar = (C1733g.C1735b) obj;
        if (this.f2723a == super.mo13569a() && this.f2724b == super.mo13571c() && this.f2725c.equals(super.mo13570b())) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j = this.f2723a;
        long j2 = this.f2724b;
        return this.f2725c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003);
    }

    public String toString() {
        return "ConfigValue{delta=" + this.f2723a + ", maxAllowedDelay=" + this.f2724b + ", flags=" + this.f2725c + "}";
    }

    private C1728d(long j, long j2, Set<C1733g.C1737c> set) {
        this.f2723a = j;
        this.f2724b = j2;
        this.f2725c = set;
    }
}
