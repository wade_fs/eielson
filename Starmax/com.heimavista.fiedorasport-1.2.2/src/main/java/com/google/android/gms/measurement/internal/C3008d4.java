package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.d4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
class C3008d4 extends C3034f6 implements C3058h6 {
    C3008d4(C3081j5 j5Var) {
        super(j5Var);
        C2258v.m5629a(j5Var);
    }

    /* renamed from: a */
    public void mo18880a() {
        super.f5134a.mo19092f();
    }

    /* renamed from: c */
    public void mo18881c() {
        super.f5134a.mo19014j().mo18881c();
    }

    /* renamed from: i */
    public void mo18882i() {
        super.f5134a.mo19093g();
        throw null;
    }

    /* renamed from: k */
    public C3257z mo18883k() {
        return super.f5134a.mo19082I();
    }

    /* renamed from: m */
    public C3154p6 mo18884m() {
        return super.f5134a.mo19103v();
    }

    /* renamed from: p */
    public C2984b4 mo18885p() {
        return super.f5134a.mo19081H();
    }

    /* renamed from: q */
    public C3232w7 mo18886q() {
        return super.f5134a.mo19079F();
    }

    /* renamed from: s */
    public C3177r7 mo18887s() {
        return super.f5134a.mo19078E();
    }

    /* renamed from: t */
    public C2972a4 mo18888t() {
        return super.f5134a.mo19106y();
    }

    /* renamed from: u */
    public C3244x8 mo18889u() {
        return super.f5134a.mo19100s();
    }
}
