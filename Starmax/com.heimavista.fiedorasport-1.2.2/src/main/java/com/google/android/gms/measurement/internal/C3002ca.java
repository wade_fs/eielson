package com.google.android.gms.measurement.internal;

import java.util.Map;
import java.util.Set;

/* renamed from: com.google.android.gms.measurement.internal.ca */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3002ca extends C3157p9 {

    /* renamed from: d */
    private String f5022d;

    /* renamed from: e */
    private Set<Integer> f5023e;

    /* renamed from: f */
    private Map<Integer, C3026ea> f5024f;

    /* renamed from: g */
    private Long f5025g;

    /* renamed from: h */
    private Long f5026h;

    C3002ca(C3145o9 o9Var) {
        super(o9Var);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x0708, code lost:
        if (r9.mo17458n() == false) goto L_0x0713;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x070a, code lost:
        r9 = java.lang.Integer.valueOf(r9.mo17459o());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x0713, code lost:
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x0714, code lost:
        r8.mo19044a("Invalid property filter ID. appId, id", r11, java.lang.String.valueOf(r9));
        r11 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x031d  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x03e2  */
    /* JADX WARNING: Removed duplicated region for block: B:194:0x05d3  */
    /* JADX WARNING: Removed duplicated region for block: B:237:0x073e  */
    /* JADX WARNING: Removed duplicated region for block: B:241:0x0762  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0324 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x0206  */
    @androidx.annotation.WorkerThread
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<com.google.android.gms.internal.measurement.C2671q0> mo18832a(java.lang.String r53, java.util.List<com.google.android.gms.internal.measurement.C2701s0> r54, java.util.List<com.google.android.gms.internal.measurement.C2414a1> r55, java.lang.Long r56, java.lang.Long r57) {
        /*
            r52 = this;
            r10 = r52
            com.google.android.gms.common.internal.C2258v.m5639b(r53)
            com.google.android.gms.common.internal.C2258v.m5629a(r54)
            com.google.android.gms.common.internal.C2258v.m5629a(r55)
            r0 = r53
            r10.f5022d = r0
            java.util.HashSet r0 = new java.util.HashSet
            r0.<init>()
            r10.f5023e = r0
            androidx.collection.ArrayMap r0 = new androidx.collection.ArrayMap
            r0.<init>()
            r10.f5024f = r0
            r0 = r56
            r10.f5025g = r0
            r0 = r57
            r10.f5026h = r0
            com.google.android.gms.measurement.internal.la r0 = r52.mo19013h()
            java.lang.String r1 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.C3135o.f5457l0
            boolean r0 = r0.mo19152d(r1, r2)
            r11 = 0
            r12 = 1
            if (r0 != 0) goto L_0x0043
            com.google.android.gms.measurement.internal.la r0 = r52.mo19013h()
            java.lang.String r1 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.C3135o.f5459m0
            boolean r0 = r0.mo19152d(r1, r2)
            if (r0 == 0) goto L_0x0061
        L_0x0043:
            java.util.Iterator r0 = r54.iterator()
        L_0x0047:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0061
            java.lang.Object r1 = r0.next()
            com.google.android.gms.internal.measurement.s0 r1 = (com.google.android.gms.internal.measurement.C2701s0) r1
            java.lang.String r1 = r1.mo17849p()
            java.lang.String r2 = "_s"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0047
            r1 = 1
            goto L_0x0062
        L_0x0061:
            r1 = 0
        L_0x0062:
            com.google.android.gms.measurement.internal.la r0 = r52.mo19013h()
            java.lang.String r2 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5457l0
            boolean r13 = r0.mo19152d(r2, r3)
            com.google.android.gms.measurement.internal.la r0 = r52.mo19013h()
            java.lang.String r2 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5459m0
            boolean r0 = r0.mo19152d(r2, r3)
            boolean r2 = com.google.android.gms.internal.measurement.C2606l9.m6736b()
            if (r2 == 0) goto L_0x0090
            com.google.android.gms.measurement.internal.la r2 = r52.mo19013h()
            java.lang.String r3 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5479w0
            boolean r2 = r2.mo19152d(r3, r4)
            if (r2 == 0) goto L_0x0090
            r14 = 1
            goto L_0x0091
        L_0x0090:
            r14 = 0
        L_0x0091:
            boolean r2 = com.google.android.gms.internal.measurement.C2606l9.m6736b()
            if (r2 == 0) goto L_0x00a7
            com.google.android.gms.measurement.internal.la r2 = r52.mo19013h()
            java.lang.String r3 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5477v0
            boolean r2 = r2.mo19152d(r3, r4)
            if (r2 == 0) goto L_0x00a7
            r15 = 1
            goto L_0x00a8
        L_0x00a7:
            r15 = 0
        L_0x00a8:
            if (r1 == 0) goto L_0x00eb
            if (r0 == 0) goto L_0x00eb
            com.google.android.gms.measurement.internal.d r2 = r52.mo19172k()
            java.lang.String r3 = r10.f5022d
            r2.mo19284q()
            r2.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r3)
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r11)
            java.lang.String r5 = "current_session_count"
            r0.put(r5, r4)
            android.database.sqlite.SQLiteDatabase r4 = r2.mo18875v()     // Catch:{ SQLiteException -> 0x00d9 }
            java.lang.String r5 = "events"
            java.lang.String r6 = "app_id = ?"
            java.lang.String[] r7 = new java.lang.String[r12]     // Catch:{ SQLiteException -> 0x00d9 }
            r7[r11] = r3     // Catch:{ SQLiteException -> 0x00d9 }
            r4.update(r5, r0, r6, r7)     // Catch:{ SQLiteException -> 0x00d9 }
            goto L_0x00eb
        L_0x00d9:
            r0 = move-exception
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()
            java.lang.Object r3 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r3)
            java.lang.String r4 = "Error resetting session-scoped event counts. appId"
            r2.mo19044a(r4, r3, r0)
        L_0x00eb:
            java.util.Map r0 = java.util.Collections.emptyMap()
            if (r15 == 0) goto L_0x00fd
            if (r14 == 0) goto L_0x00fd
            com.google.android.gms.measurement.internal.d r0 = r52.mo19172k()
            java.lang.String r2 = r10.f5022d
            java.util.Map r0 = r0.mo18867e(r2)
        L_0x00fd:
            com.google.android.gms.measurement.internal.d r2 = r52.mo19172k()
            java.lang.String r3 = r10.f5022d
            java.util.Map r9 = r2.mo18870g(r3)
            if (r9 == 0) goto L_0x03d7
            boolean r2 = r9.isEmpty()
            if (r2 == 0) goto L_0x0111
            goto L_0x03d7
        L_0x0111:
            java.util.HashSet r2 = new java.util.HashSet
            java.util.Set r3 = r9.keySet()
            r2.<init>(r3)
            if (r13 == 0) goto L_0x01fb
            if (r1 == 0) goto L_0x01fb
            java.lang.String r1 = r10.f5022d
            com.google.android.gms.common.internal.C2258v.m5639b(r1)
            com.google.android.gms.common.internal.C2258v.m5629a(r9)
            androidx.collection.ArrayMap r3 = new androidx.collection.ArrayMap
            r3.<init>()
            boolean r4 = r9.isEmpty()
            if (r4 != 0) goto L_0x01f9
            com.google.android.gms.measurement.internal.d r4 = r52.mo19172k()
            java.util.Map r1 = r4.mo18868f(r1)
            java.util.Set r4 = r9.keySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x0141:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x01f9
            java.lang.Object r5 = r4.next()
            java.lang.Integer r5 = (java.lang.Integer) r5
            int r5 = r5.intValue()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r5)
            java.lang.Object r6 = r9.get(r6)
            com.google.android.gms.internal.measurement.y0 r6 = (com.google.android.gms.internal.measurement.C2794y0) r6
            java.lang.Integer r7 = java.lang.Integer.valueOf(r5)
            java.lang.Object r7 = r1.get(r7)
            java.util.List r7 = (java.util.List) r7
            if (r7 == 0) goto L_0x01ee
            boolean r16 = r7.isEmpty()
            if (r16 == 0) goto L_0x016f
            goto L_0x01ee
        L_0x016f:
            com.google.android.gms.measurement.internal.v9 r8 = r52.mo19171i()
            java.util.List r11 = r6.mo18150p()
            java.util.List r8 = r8.mo19356a(r11, r7)
            boolean r11 = r8.isEmpty()
            if (r11 != 0) goto L_0x01eb
            com.google.android.gms.internal.measurement.l4$a r11 = r6.mo17669j()
            com.google.android.gms.internal.measurement.y0$a r11 = (com.google.android.gms.internal.measurement.C2794y0.C2795a) r11
            r11.mo18163k()
            r11.mo18159b(r8)
            com.google.android.gms.measurement.internal.v9 r8 = r52.mo19171i()
            java.util.List r12 = r6.mo18148n()
            java.util.List r8 = r8.mo19356a(r12, r7)
            r11.mo18162j()
            r11.mo18157a(r8)
            r8 = 0
        L_0x01a0:
            int r12 = r6.mo18153t()
            if (r8 >= r12) goto L_0x01be
            com.google.android.gms.internal.measurement.r0 r12 = r6.mo18146b(r8)
            int r12 = r12.mo17838o()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            boolean r12 = r7.contains(r12)
            if (r12 == 0) goto L_0x01bb
            r11.mo18156a(r8)
        L_0x01bb:
            int r8 = r8 + 1
            goto L_0x01a0
        L_0x01be:
            r8 = 0
        L_0x01bf:
            int r12 = r6.mo18155v()
            if (r8 >= r12) goto L_0x01dd
            com.google.android.gms.internal.measurement.z0 r12 = r6.mo18147c(r8)
            int r12 = r12.mo18172o()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)
            boolean r12 = r7.contains(r12)
            if (r12 == 0) goto L_0x01da
            r11.mo18158b(r8)
        L_0x01da:
            int r8 = r8 + 1
            goto L_0x01bf
        L_0x01dd:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            com.google.android.gms.internal.measurement.u5 r6 = r11.mo17679i()
            com.google.android.gms.internal.measurement.y0 r6 = (com.google.android.gms.internal.measurement.C2794y0) r6
            r3.put(r5, r6)
            goto L_0x01f5
        L_0x01eb:
            r11 = 0
            goto L_0x0141
        L_0x01ee:
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            r3.put(r5, r6)
        L_0x01f5:
            r11 = 0
            r12 = 1
            goto L_0x0141
        L_0x01f9:
            r11 = r3
            goto L_0x01fc
        L_0x01fb:
            r11 = r9
        L_0x01fc:
            java.util.Iterator r12 = r2.iterator()
        L_0x0200:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x03d7
            java.lang.Object r1 = r12.next()
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r16 = r1.intValue()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r16)
            java.lang.Object r1 = r11.get(r1)
            com.google.android.gms.internal.measurement.y0 r1 = (com.google.android.gms.internal.measurement.C2794y0) r1
            java.util.BitSet r5 = new java.util.BitSet
            r5.<init>()
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>()
            androidx.collection.ArrayMap r7 = new androidx.collection.ArrayMap
            r7.<init>()
            if (r1 == 0) goto L_0x0268
            int r2 = r1.mo18153t()
            if (r2 != 0) goto L_0x0232
            goto L_0x0268
        L_0x0232:
            java.util.List r2 = r1.mo18152s()
            java.util.Iterator r2 = r2.iterator()
        L_0x023a:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x0268
            java.lang.Object r3 = r2.next()
            com.google.android.gms.internal.measurement.r0 r3 = (com.google.android.gms.internal.measurement.C2686r0) r3
            boolean r4 = r3.mo17837n()
            if (r4 == 0) goto L_0x023a
            int r4 = r3.mo17838o()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            boolean r8 = r3.mo17839p()
            if (r8 == 0) goto L_0x0263
            long r17 = r3.mo17840q()
            java.lang.Long r8 = java.lang.Long.valueOf(r17)
            goto L_0x0264
        L_0x0263:
            r8 = 0
        L_0x0264:
            r7.put(r4, r8)
            goto L_0x023a
        L_0x0268:
            boolean r2 = com.google.android.gms.internal.measurement.C2458cc.m6163b()
            if (r2 == 0) goto L_0x02d3
            com.google.android.gms.measurement.internal.la r2 = r52.mo19013h()
            java.lang.String r3 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5471s0
            boolean r2 = r2.mo19152d(r3, r4)
            if (r2 == 0) goto L_0x02d3
            androidx.collection.ArrayMap r2 = new androidx.collection.ArrayMap
            r2.<init>()
            if (r1 == 0) goto L_0x02cf
            int r3 = r1.mo18155v()
            if (r3 != 0) goto L_0x028a
            goto L_0x02cf
        L_0x028a:
            java.util.List r3 = r1.mo18154u()
            java.util.Iterator r3 = r3.iterator()
        L_0x0292:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x02cf
            java.lang.Object r4 = r3.next()
            com.google.android.gms.internal.measurement.z0 r4 = (com.google.android.gms.internal.measurement.C2810z0) r4
            boolean r8 = r4.mo18171n()
            if (r8 == 0) goto L_0x02c8
            int r8 = r4.mo18174q()
            if (r8 <= 0) goto L_0x02c8
            int r8 = r4.mo18172o()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            int r17 = r4.mo18174q()
            r57 = r3
            r18 = 1
            int r3 = r17 + -1
            long r3 = r4.mo18170b(r3)
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r2.put(r8, r3)
            goto L_0x02cc
        L_0x02c8:
            r57 = r3
            r18 = 1
        L_0x02cc:
            r3 = r57
            goto L_0x0292
        L_0x02cf:
            r18 = 1
            r8 = r2
            goto L_0x02d6
        L_0x02d3:
            r18 = 1
            r8 = 0
        L_0x02d6:
            if (r1 == 0) goto L_0x032b
            r2 = 0
        L_0x02d9:
            int r3 = r1.mo18149o()
            int r3 = r3 << 6
            if (r2 >= r3) goto L_0x032b
            java.util.List r3 = r1.mo18148n()
            boolean r3 = com.google.android.gms.measurement.internal.C3223v9.m9273a(r3, r2)
            if (r3 == 0) goto L_0x0316
            com.google.android.gms.measurement.internal.f4 r3 = r52.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo18996B()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r16)
            r17 = r11
            java.lang.Integer r11 = java.lang.Integer.valueOf(r2)
            r57 = r12
            java.lang.String r12 = "Filter already evaluated. audience ID, filter ID"
            r3.mo19044a(r12, r4, r11)
            r6.set(r2)
            java.util.List r3 = r1.mo18150p()
            boolean r3 = com.google.android.gms.measurement.internal.C3223v9.m9273a(r3, r2)
            if (r3 == 0) goto L_0x031a
            r5.set(r2)
            r3 = 1
            goto L_0x031b
        L_0x0316:
            r17 = r11
            r57 = r12
        L_0x031a:
            r3 = 0
        L_0x031b:
            if (r3 != 0) goto L_0x0324
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)
            r7.remove(r3)
        L_0x0324:
            int r2 = r2 + 1
            r12 = r57
            r11 = r17
            goto L_0x02d9
        L_0x032b:
            r17 = r11
            r57 = r12
            if (r13 == 0) goto L_0x033b
            java.lang.Integer r1 = java.lang.Integer.valueOf(r16)
            java.lang.Object r1 = r9.get(r1)
            com.google.android.gms.internal.measurement.y0 r1 = (com.google.android.gms.internal.measurement.C2794y0) r1
        L_0x033b:
            r4 = r1
            if (r15 == 0) goto L_0x03b3
            if (r14 == 0) goto L_0x03b3
            java.lang.Integer r1 = java.lang.Integer.valueOf(r16)
            java.lang.Object r1 = r0.get(r1)
            java.util.List r1 = (java.util.List) r1
            if (r1 == 0) goto L_0x03b3
            java.lang.Long r2 = r10.f5026h
            if (r2 == 0) goto L_0x03b3
            java.lang.Long r2 = r10.f5025g
            if (r2 != 0) goto L_0x0355
            goto L_0x03b3
        L_0x0355:
            java.util.Iterator r1 = r1.iterator()
        L_0x0359:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x03b3
            java.lang.Object r2 = r1.next()
            com.google.android.gms.internal.measurement.c0 r2 = (com.google.android.gms.internal.measurement.C2445c0) r2
            int r3 = r2.mo17348o()
            java.lang.Long r11 = r10.f5026h
            long r11 = r11.longValue()
            r19 = 1000(0x3e8, double:4.94E-321)
            long r11 = r11 / r19
            boolean r2 = r2.mo17355w()
            if (r2 == 0) goto L_0x0381
            java.lang.Long r2 = r10.f5025g
            long r11 = r2.longValue()
            long r11 = r11 / r19
        L_0x0381:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            boolean r2 = r7.containsKey(r2)
            if (r2 == 0) goto L_0x0399
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)
            r19 = r0
            java.lang.Long r0 = java.lang.Long.valueOf(r11)
            r7.put(r2, r0)
            goto L_0x039b
        L_0x0399:
            r19 = r0
        L_0x039b:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            boolean r0 = r8.containsKey(r0)
            if (r0 == 0) goto L_0x03b0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r3)
            java.lang.Long r2 = java.lang.Long.valueOf(r11)
            r8.put(r0, r2)
        L_0x03b0:
            r0 = r19
            goto L_0x0359
        L_0x03b3:
            r19 = r0
            com.google.android.gms.measurement.internal.ea r0 = new com.google.android.gms.measurement.internal.ea
            java.lang.String r3 = r10.f5022d
            r11 = 0
            r1 = r0
            r2 = r52
            r12 = 0
            r20 = r9
            r9 = r11
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            java.util.Map<java.lang.Integer, com.google.android.gms.measurement.internal.ea> r1 = r10.f5024f
            java.lang.Integer r2 = java.lang.Integer.valueOf(r16)
            r1.put(r2, r0)
            r12 = r57
            r11 = r17
            r0 = r19
            r9 = r20
            goto L_0x0200
        L_0x03d7:
            r12 = 0
            r18 = 1
            boolean r0 = r54.isEmpty()
            java.lang.String r1 = "Skipping failed audience ID"
            if (r0 != 0) goto L_0x05c8
            com.google.android.gms.measurement.internal.ha r0 = new com.google.android.gms.measurement.internal.ha
            r0.<init>(r10, r12)
            androidx.collection.ArrayMap r2 = new androidx.collection.ArrayMap
            r2.<init>()
            java.util.Iterator r3 = r54.iterator()
        L_0x03f0:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x05c8
            java.lang.Object r4 = r3.next()
            com.google.android.gms.internal.measurement.s0 r4 = (com.google.android.gms.internal.measurement.C2701s0) r4
            java.lang.String r5 = r10.f5022d
            com.google.android.gms.internal.measurement.s0 r5 = r0.mo19057a(r5, r4)
            if (r5 == 0) goto L_0x03f0
            com.google.android.gms.measurement.internal.d r6 = r52.mo19172k()
            java.lang.String r7 = r10.f5022d
            java.lang.String r8 = r5.mo17849p()
            com.google.android.gms.measurement.internal.la r9 = r6.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r11 = com.google.android.gms.measurement.internal.C3135o.f5459m0
            boolean r9 = r9.mo19152d(r7, r11)
            java.lang.String r11 = r4.mo17849p()
            com.google.android.gms.measurement.internal.k r11 = r6.mo18843a(r7, r11)
            if (r11 != 0) goto L_0x0481
            com.google.android.gms.measurement.internal.f4 r11 = r6.mo19015l()
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo19004w()
            java.lang.Object r13 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r7)
            com.google.android.gms.measurement.internal.c4 r6 = r6.mo19010e()
            java.lang.String r6 = r6.mo18822a(r8)
            java.lang.String r8 = "Event aggregate wasn't created during raw event logging. appId, event"
            r11.mo19044a(r8, r13, r6)
            if (r9 == 0) goto L_0x045f
            com.google.android.gms.measurement.internal.k r6 = new com.google.android.gms.measurement.internal.k
            r19 = r6
            java.lang.String r21 = r4.mo17849p()
            r22 = 1
            r24 = 1
            r26 = 1
            long r28 = r4.mo17851s()
            r30 = 0
            r32 = 0
            r33 = 0
            r34 = 0
            r35 = 0
            r20 = r7
            r19.<init>(r20, r21, r22, r24, r26, r28, r30, r32, r33, r34, r35)
            goto L_0x047e
        L_0x045f:
            com.google.android.gms.measurement.internal.k r6 = new com.google.android.gms.measurement.internal.k
            java.lang.String r21 = r4.mo17849p()
            r22 = 1
            r24 = 1
            long r26 = r4.mo17851s()
            r28 = 0
            r30 = 0
            r31 = 0
            r32 = 0
            r33 = 0
            r19 = r6
            r20 = r7
            r19.<init>(r20, r21, r22, r24, r26, r28, r30, r31, r32, r33)
        L_0x047e:
            r4 = r6
            goto L_0x04ec
        L_0x0481:
            r6 = 1
            if (r9 == 0) goto L_0x04b9
            com.google.android.gms.measurement.internal.k r4 = new com.google.android.gms.measurement.internal.k
            r19 = r4
            java.lang.String r8 = r11.f5278a
            r20 = r8
            java.lang.String r8 = r11.f5279b
            r21 = r8
            long r8 = r11.f5280c
            long r22 = r8 + r6
            long r8 = r11.f5281d
            long r24 = r8 + r6
            long r8 = r11.f5282e
            long r26 = r8 + r6
            long r6 = r11.f5283f
            r28 = r6
            long r6 = r11.f5284g
            r30 = r6
            java.lang.Long r6 = r11.f5285h
            r32 = r6
            java.lang.Long r6 = r11.f5286i
            r33 = r6
            java.lang.Long r6 = r11.f5287j
            r34 = r6
            java.lang.Boolean r6 = r11.f5288k
            r35 = r6
            r19.<init>(r20, r21, r22, r24, r26, r28, r30, r32, r33, r34, r35)
            goto L_0x04ec
        L_0x04b9:
            com.google.android.gms.measurement.internal.k r4 = new com.google.android.gms.measurement.internal.k
            r35 = r4
            java.lang.String r8 = r11.f5278a
            r36 = r8
            java.lang.String r8 = r11.f5279b
            r37 = r8
            long r8 = r11.f5280c
            long r38 = r8 + r6
            long r8 = r11.f5281d
            long r40 = r8 + r6
            long r6 = r11.f5282e
            r42 = r6
            long r6 = r11.f5283f
            r44 = r6
            long r6 = r11.f5284g
            r46 = r6
            java.lang.Long r6 = r11.f5285h
            r48 = r6
            java.lang.Long r6 = r11.f5286i
            r49 = r6
            java.lang.Long r6 = r11.f5287j
            r50 = r6
            java.lang.Boolean r6 = r11.f5288k
            r51 = r6
            r35.<init>(r36, r37, r38, r40, r42, r44, r46, r48, r49, r50, r51)
        L_0x04ec:
            com.google.android.gms.measurement.internal.d r6 = r52.mo19172k()
            r6.mo18851a(r4)
            long r6 = r4.f5280c
            java.lang.String r8 = r5.mo17849p()
            java.lang.Object r9 = r2.get(r8)
            java.util.Map r9 = (java.util.Map) r9
            if (r9 != 0) goto L_0x0515
            com.google.android.gms.measurement.internal.d r9 = r52.mo19172k()
            java.lang.String r11 = r10.f5022d
            java.util.Map r9 = r9.mo18869f(r11, r8)
            if (r9 != 0) goto L_0x0512
            androidx.collection.ArrayMap r9 = new androidx.collection.ArrayMap
            r9.<init>()
        L_0x0512:
            r2.put(r8, r9)
        L_0x0515:
            java.util.Set r8 = r9.keySet()
            java.util.Iterator r8 = r8.iterator()
        L_0x051d:
            boolean r11 = r8.hasNext()
            if (r11 == 0) goto L_0x03f0
            java.lang.Object r11 = r8.next()
            java.lang.Integer r11 = (java.lang.Integer) r11
            int r11 = r11.intValue()
            java.util.Set<java.lang.Integer> r13 = r10.f5023e
            java.lang.Integer r14 = java.lang.Integer.valueOf(r11)
            boolean r13 = r13.contains(r14)
            if (r13 == 0) goto L_0x0549
            com.google.android.gms.measurement.internal.f4 r13 = r52.mo19015l()
            com.google.android.gms.measurement.internal.h4 r13 = r13.mo18996B()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            r13.mo19043a(r1, r11)
            goto L_0x051d
        L_0x0549:
            java.lang.Integer r13 = java.lang.Integer.valueOf(r11)
            java.lang.Object r13 = r9.get(r13)
            java.util.List r13 = (java.util.List) r13
            java.util.Iterator r13 = r13.iterator()
            r14 = 1
        L_0x0558:
            boolean r15 = r13.hasNext()
            if (r15 == 0) goto L_0x05b2
            java.lang.Object r14 = r13.next()
            com.google.android.gms.internal.measurement.c0 r14 = (com.google.android.gms.internal.measurement.C2445c0) r14
            com.google.android.gms.measurement.internal.ga r15 = new com.google.android.gms.measurement.internal.ga
            java.lang.String r12 = r10.f5022d
            r15.<init>(r10, r12, r11, r14)
            java.lang.Long r12 = r10.f5025g
            r57 = r0
            java.lang.Long r0 = r10.f5026h
            int r14 = r14.mo17348o()
            boolean r26 = r10.m8434a(r11, r14)
            r19 = r15
            r20 = r12
            r21 = r0
            r22 = r5
            r23 = r6
            r25 = r4
            boolean r14 = r19.mo19039a(r20, r21, r22, r23, r25, r26)
            com.google.android.gms.measurement.internal.la r0 = r52.mo19013h()
            java.lang.String r12 = r10.f5022d
            r16 = r2
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r2 = com.google.android.gms.measurement.internal.C3135o.f5473t0
            boolean r0 = r0.mo19152d(r12, r2)
            if (r0 == 0) goto L_0x05a5
            if (r14 != 0) goto L_0x05a5
            java.util.Set<java.lang.Integer> r0 = r10.f5023e
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r0.add(r2)
            goto L_0x05b6
        L_0x05a5:
            com.google.android.gms.measurement.internal.ea r0 = r10.m8433a(r11)
            r0.mo18992a(r15)
            r0 = r57
            r2 = r16
            r12 = 0
            goto L_0x0558
        L_0x05b2:
            r57 = r0
            r16 = r2
        L_0x05b6:
            if (r14 != 0) goto L_0x05c1
            java.util.Set<java.lang.Integer> r0 = r10.f5023e
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r0.add(r2)
        L_0x05c1:
            r0 = r57
            r2 = r16
            r12 = 0
            goto L_0x051d
        L_0x05c8:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            boolean r2 = r55.isEmpty()
            if (r2 != 0) goto L_0x072b
            androidx.collection.ArrayMap r2 = new androidx.collection.ArrayMap
            r2.<init>()
            java.util.Iterator r3 = r55.iterator()
        L_0x05dc:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x072b
            java.lang.Object r4 = r3.next()
            com.google.android.gms.internal.measurement.a1 r4 = (com.google.android.gms.internal.measurement.C2414a1) r4
            java.lang.String r5 = r4.mo17250p()
            r0.add(r5)
            java.lang.String r5 = r4.mo17250p()
            java.lang.Object r6 = r2.get(r5)
            java.util.Map r6 = (java.util.Map) r6
            if (r6 != 0) goto L_0x060f
            com.google.android.gms.measurement.internal.d r6 = r52.mo19172k()
            java.lang.String r7 = r10.f5022d
            java.util.Map r6 = r6.mo18871g(r7, r5)
            if (r6 != 0) goto L_0x060c
            androidx.collection.ArrayMap r6 = new androidx.collection.ArrayMap
            r6.<init>()
        L_0x060c:
            r2.put(r5, r6)
        L_0x060f:
            java.util.Set r5 = r6.keySet()
            java.util.Iterator r5 = r5.iterator()
        L_0x0617:
            boolean r7 = r5.hasNext()
            if (r7 == 0) goto L_0x05dc
            java.lang.Object r7 = r5.next()
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            java.util.Set<java.lang.Integer> r8 = r10.f5023e
            java.lang.Integer r9 = java.lang.Integer.valueOf(r7)
            boolean r8 = r8.contains(r9)
            if (r8 == 0) goto L_0x0643
            com.google.android.gms.measurement.internal.f4 r4 = r52.mo19015l()
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo18996B()
            java.lang.Integer r5 = java.lang.Integer.valueOf(r7)
            r4.mo19043a(r1, r5)
            goto L_0x05dc
        L_0x0643:
            java.lang.Integer r8 = java.lang.Integer.valueOf(r7)
            java.lang.Object r8 = r6.get(r8)
            java.util.List r8 = (java.util.List) r8
            java.util.Iterator r8 = r8.iterator()
            r11 = 1
        L_0x0652:
            boolean r9 = r8.hasNext()
            if (r9 == 0) goto L_0x071e
            java.lang.Object r9 = r8.next()
            com.google.android.gms.internal.measurement.f0 r9 = (com.google.android.gms.internal.measurement.C2494f0) r9
            com.google.android.gms.measurement.internal.f4 r11 = r52.mo19015l()
            r12 = 2
            boolean r11 = r11.mo19000a(r12)
            if (r11 == 0) goto L_0x06ab
            com.google.android.gms.measurement.internal.f4 r11 = r52.mo19015l()
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo18996B()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r7)
            boolean r13 = r9.mo17458n()
            if (r13 == 0) goto L_0x0684
            int r13 = r9.mo17459o()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            goto L_0x0685
        L_0x0684:
            r13 = 0
        L_0x0685:
            com.google.android.gms.measurement.internal.c4 r14 = r52.mo19010e()
            java.lang.String r15 = r9.mo17460p()
            java.lang.String r14 = r14.mo18824c(r15)
            java.lang.String r15 = "Evaluating filter. audience, filter, property"
            r11.mo19045a(r15, r12, r13, r14)
            com.google.android.gms.measurement.internal.f4 r11 = r52.mo19015l()
            com.google.android.gms.measurement.internal.h4 r11 = r11.mo18996B()
            com.google.android.gms.measurement.internal.v9 r12 = r52.mo19171i()
            java.lang.String r12 = r12.mo19354a(r9)
            java.lang.String r13 = "Filter definition"
            r11.mo19043a(r13, r12)
        L_0x06ab:
            boolean r11 = r9.mo17458n()
            if (r11 == 0) goto L_0x06f6
            int r11 = r9.mo17459o()
            r12 = 256(0x100, float:3.59E-43)
            if (r11 <= r12) goto L_0x06ba
            goto L_0x06f6
        L_0x06ba:
            com.google.android.gms.measurement.internal.ia r11 = new com.google.android.gms.measurement.internal.ia
            java.lang.String r12 = r10.f5022d
            r11.<init>(r10, r12, r7, r9)
            java.lang.Long r12 = r10.f5025g
            java.lang.Long r13 = r10.f5026h
            int r9 = r9.mo17459o()
            boolean r9 = r10.m8434a(r7, r9)
            boolean r9 = r11.mo19070a(r12, r13, r4, r9)
            com.google.android.gms.measurement.internal.la r12 = r52.mo19013h()
            java.lang.String r13 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.C3135o.f5473t0
            boolean r12 = r12.mo19152d(r13, r14)
            if (r12 == 0) goto L_0x06ec
            if (r9 != 0) goto L_0x06ec
            java.util.Set<java.lang.Integer> r8 = r10.f5023e
            java.lang.Integer r11 = java.lang.Integer.valueOf(r7)
            r8.add(r11)
            r11 = r9
            goto L_0x071e
        L_0x06ec:
            com.google.android.gms.measurement.internal.ea r12 = r10.m8433a(r7)
            r12.mo18992a(r11)
            r11 = r9
            goto L_0x0652
        L_0x06f6:
            com.google.android.gms.measurement.internal.f4 r8 = r52.mo19015l()
            com.google.android.gms.measurement.internal.h4 r8 = r8.mo19004w()
            java.lang.String r11 = r10.f5022d
            java.lang.Object r11 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r11)
            boolean r12 = r9.mo17458n()
            if (r12 == 0) goto L_0x0713
            int r9 = r9.mo17459o()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            goto L_0x0714
        L_0x0713:
            r9 = 0
        L_0x0714:
            java.lang.String r9 = java.lang.String.valueOf(r9)
            java.lang.String r12 = "Invalid property filter ID. appId, id"
            r8.mo19044a(r12, r11, r9)
            r11 = 0
        L_0x071e:
            if (r11 != 0) goto L_0x0617
            java.util.Set<java.lang.Integer> r8 = r10.f5023e
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)
            r8.add(r7)
            goto L_0x0617
        L_0x072b:
            com.google.android.gms.measurement.internal.la r1 = r52.mo19013h()
            java.lang.String r2 = r10.f5022d
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r3 = com.google.android.gms.measurement.internal.C3135o.f5469r0
            boolean r1 = r1.mo19152d(r2, r3)
            androidx.collection.ArrayMap r2 = new androidx.collection.ArrayMap
            r2.<init>()
            if (r1 == 0) goto L_0x0748
            com.google.android.gms.measurement.internal.d r1 = r52.mo19172k()
            java.lang.String r2 = r10.f5022d
            java.util.Map r2 = r1.mo18849a(r2, r0)
        L_0x0748:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.Map<java.lang.Integer, com.google.android.gms.measurement.internal.ea> r0 = r10.f5024f
            java.util.Set r0 = r0.keySet()
            java.util.Set<java.lang.Integer> r3 = r10.f5023e
            r0.removeAll(r3)
            java.util.Iterator r3 = r0.iterator()
        L_0x075c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x07f7
            java.lang.Object r0 = r3.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.util.Map<java.lang.Integer, com.google.android.gms.measurement.internal.ea> r4 = r10.f5024f
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            java.lang.Object r4 = r4.get(r5)
            com.google.android.gms.measurement.internal.ea r4 = (com.google.android.gms.measurement.internal.C3026ea) r4
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)
            java.lang.Object r5 = r2.get(r5)
            java.util.List r5 = (java.util.List) r5
            com.google.android.gms.internal.measurement.q0 r4 = r4.mo18991a(r0, r5)
            r1.add(r4)
            com.google.android.gms.measurement.internal.d r5 = r52.mo19172k()
            java.lang.String r6 = r10.f5022d
            com.google.android.gms.internal.measurement.y0 r4 = r4.mo17815p()
            r5.mo19284q()
            r5.mo18881c()
            com.google.android.gms.common.internal.C2258v.m5639b(r6)
            com.google.android.gms.common.internal.C2258v.m5629a(r4)
            byte[] r4 = r4.mo18129f()
            android.content.ContentValues r7 = new android.content.ContentValues
            r7.<init>()
            java.lang.String r8 = "app_id"
            r7.put(r8, r6)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r8 = "audience_id"
            r7.put(r8, r0)
            java.lang.String r0 = "current_results"
            r7.put(r0, r4)
            android.database.sqlite.SQLiteDatabase r0 = r5.mo18875v()     // Catch:{ SQLiteException -> 0x07e2 }
            java.lang.String r4 = "audience_filter_values"
            r8 = 5
            r9 = 0
            long r7 = r0.insertWithOnConflict(r4, r9, r7, r8)     // Catch:{ SQLiteException -> 0x07e0 }
            r11 = -1
            int r0 = (r7 > r11 ? 1 : (r7 == r11 ? 0 : -1))
            if (r0 != 0) goto L_0x075c
            com.google.android.gms.measurement.internal.f4 r0 = r5.mo19015l()     // Catch:{ SQLiteException -> 0x07e0 }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo19001t()     // Catch:{ SQLiteException -> 0x07e0 }
            java.lang.String r4 = "Failed to insert filter results (got -1). appId"
            java.lang.Object r7 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r6)     // Catch:{ SQLiteException -> 0x07e0 }
            r0.mo19043a(r4, r7)     // Catch:{ SQLiteException -> 0x07e0 }
            goto L_0x075c
        L_0x07e0:
            r0 = move-exception
            goto L_0x07e4
        L_0x07e2:
            r0 = move-exception
            r9 = 0
        L_0x07e4:
            com.google.android.gms.measurement.internal.f4 r4 = r5.mo19015l()
            com.google.android.gms.measurement.internal.h4 r4 = r4.mo19001t()
            java.lang.Object r5 = com.google.android.gms.measurement.internal.C3032f4.m8621a(r6)
            java.lang.String r6 = "Error storing filter results. appId"
            r4.mo19044a(r6, r5, r0)
            goto L_0x075c
        L_0x07f7:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3002ca.mo18832a(java.lang.String, java.util.List, java.util.List, java.lang.Long, java.lang.Long):java.util.List");
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final boolean mo18833t() {
        return false;
    }

    /* renamed from: a */
    private final C3026ea m8433a(int i) {
        if (this.f5024f.containsKey(Integer.valueOf(i))) {
            return this.f5024f.get(Integer.valueOf(i));
        }
        C3026ea eaVar = new C3026ea(this, this.f5022d, null);
        this.f5024f.put(Integer.valueOf(i), eaVar);
        return eaVar;
    }

    /* renamed from: a */
    private final boolean m8434a(int i, int i2) {
        if (this.f5024f.get(Integer.valueOf(i)) == null) {
            return false;
        }
        return this.f5024f.get(Integer.valueOf(i)).f5113d.get(i2);
    }
}
