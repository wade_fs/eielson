package com.google.android.gms.base;

public final class R$id {
    public static final int adjust_height = 2131296349;
    public static final int adjust_width = 2131296350;
    public static final int auto = 2131296363;
    public static final int dark = 2131296482;
    public static final int icon_only = 2131296608;
    public static final int light = 2131296699;
    public static final int none = 2131296778;
    public static final int standard = 2131296908;
    public static final int wide = 2131297107;

    private R$id() {
    }
}
