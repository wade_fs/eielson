package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ra */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2697ra implements C2593l2<C2744ua> {

    /* renamed from: Q */
    private static C2697ra f4467Q = new C2697ra();

    /* renamed from: P */
    private final C2593l2<C2744ua> f4468P;

    private C2697ra(C2593l2<C2744ua> l2Var) {
        this.f4468P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7137b() {
        return ((C2744ua) f4467Q.mo17285a()).mo17919a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4468P.mo17285a();
    }

    public C2697ra() {
        this(C2579k2.m6601a(new C2729ta()));
    }
}
