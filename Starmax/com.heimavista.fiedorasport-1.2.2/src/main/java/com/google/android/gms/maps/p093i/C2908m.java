package com.google.android.gms.maps.p093i;

import android.os.Parcel;
import com.google.android.gms.maps.model.LatLng;
import p119e.p144d.p145a.p157c.p161c.p165d.C4038g;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;

/* renamed from: com.google.android.gms.maps.i.m */
public abstract class C2908m extends C4038g implements C2907l {
    public C2908m() {
        super("com.google.android.gms.maps.internal.IOnMapClickListener");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo18485a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        mo18484a((LatLng) C4039h.m12042a(parcel, LatLng.CREATOR));
        parcel2.writeNoException();
        return true;
    }
}
