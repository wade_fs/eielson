package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2445c0;
import com.google.android.gms.internal.measurement.C2494f0;
import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.b0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2429b0 extends C2595l4<C2429b0, C2430a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2429b0 zzi;
    private static volatile C2501f6<C2429b0> zzj;
    private int zzc;
    private int zzd;
    private C2738u4<C2494f0> zze = C2595l4.m6649m();
    private C2738u4<C2445c0> zzf = C2595l4.m6649m();
    private boolean zzg;
    private boolean zzh;

    /* renamed from: com.google.android.gms.internal.measurement.b0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2430a extends C2595l4.C2596a<C2429b0, C2430a> implements C2769w5 {
        private C2430a() {
            super(C2429b0.zzi);
        }

        /* renamed from: a */
        public final C2494f0 mo17304a(int i) {
            return ((C2429b0) super.f4288Q).mo17294b(i);
        }

        /* renamed from: b */
        public final C2445c0 mo17305b(int i) {
            return ((C2429b0) super.f4288Q).mo17295c(i);
        }

        /* renamed from: j */
        public final int mo17306j() {
            return ((C2429b0) super.f4288Q).mo17299q();
        }

        /* renamed from: k */
        public final int mo17307k() {
            return ((C2429b0) super.f4288Q).mo17301t();
        }

        /* synthetic */ C2430a(C2413a0 a0Var) {
            this();
        }

        /* renamed from: a */
        public final C2430a mo17303a(int i, C2494f0.C2495a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2429b0) super.f4288Q).m6023a(i, (C2494f0) super.mo17679i());
            return this;
        }

        /* renamed from: a */
        public final C2430a mo17302a(int i, C2445c0.C2446a aVar) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2429b0) super.f4288Q).m6022a(i, (C2445c0) super.mo17679i());
            return this;
        }
    }

    static {
        C2429b0 b0Var = new C2429b0();
        zzi = b0Var;
        C2595l4.m6645a(C2429b0.class, super);
    }

    private C2429b0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6023a(int i, C2494f0 f0Var) {
        f0Var.getClass();
        if (!this.zze.mo17938a()) {
            this.zze = C2595l4.m6642a(this.zze);
        }
        this.zze.set(i, f0Var);
    }

    /* renamed from: b */
    public final C2494f0 mo17294b(int i) {
        return this.zze.get(i);
    }

    /* renamed from: c */
    public final C2445c0 mo17295c(int i) {
        return this.zzf.get(i);
    }

    /* renamed from: n */
    public final boolean mo17296n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final int mo17297o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final List<C2494f0> mo17298p() {
        return this.zze;
    }

    /* renamed from: q */
    public final int mo17299q() {
        return this.zze.size();
    }

    /* renamed from: s */
    public final List<C2445c0> mo17300s() {
        return this.zzf;
    }

    /* renamed from: t */
    public final int mo17301t() {
        return this.zzf.size();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6022a(int i, C2445c0 c0Var) {
        c0Var.getClass();
        if (!this.zzf.mo17938a()) {
            this.zzf = C2595l4.m6642a(this.zzf);
        }
        this.zzf.set(i, c0Var);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2413a0.f3970a[i - 1]) {
            case 1:
                return new C2429b0();
            case 2:
                return new C2430a(null);
            case 3:
                return C2595l4.m6643a(zzi, "\u0001\u0005\u0000\u0001\u0001\u0005\u0005\u0000\u0002\u0000\u0001\u0004\u0000\u0002\u001b\u0003\u001b\u0004\u0007\u0001\u0005\u0007\u0002", new Object[]{"zzc", "zzd", "zze", C2494f0.class, "zzf", C2445c0.class, "zzg", "zzh"});
            case 4:
                return zzi;
            case 5:
                C2501f6<C2429b0> f6Var = zzj;
                if (f6Var == null) {
                    synchronized (C2429b0.class) {
                        f6Var = zzj;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzi);
                            zzj = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
