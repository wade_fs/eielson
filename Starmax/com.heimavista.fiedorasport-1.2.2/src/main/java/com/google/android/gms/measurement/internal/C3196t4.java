package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.t4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3196t4 {

    /* renamed from: a */
    private final String f5650a;

    /* renamed from: b */
    private final long f5651b;

    /* renamed from: c */
    private boolean f5652c;

    /* renamed from: d */
    private long f5653d;

    /* renamed from: e */
    private final /* synthetic */ C3185s4 f5654e;

    public C3196t4(C3185s4 s4Var, String str, long j) {
        this.f5654e = s4Var;
        C2258v.m5639b(str);
        this.f5650a = str;
        this.f5651b = j;
    }

    @WorkerThread
    /* renamed from: a */
    public final long mo19326a() {
        if (!this.f5652c) {
            this.f5652c = true;
            this.f5653d = this.f5654e.mo19315t().getLong(this.f5650a, this.f5651b);
        }
        return this.f5653d;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo19327a(long j) {
        SharedPreferences.Editor edit = this.f5654e.mo19315t().edit();
        edit.putLong(this.f5650a, j);
        edit.apply();
        this.f5653d = j;
    }
}
