package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;
import com.google.android.exoplayer2.decoder.DecoderCounters;

/* renamed from: com.google.android.exoplayer2.audio.c */
/* compiled from: lambda */
public final /* synthetic */ class C1773c implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f2790P;

    /* renamed from: Q */
    private final /* synthetic */ DecoderCounters f2791Q;

    public /* synthetic */ C1773c(AudioRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f2790P = eventDispatcher;
        this.f2791Q = decoderCounters;
    }

    public final void run() {
        this.f2790P.mo14099b(this.f2791Q);
    }
}
