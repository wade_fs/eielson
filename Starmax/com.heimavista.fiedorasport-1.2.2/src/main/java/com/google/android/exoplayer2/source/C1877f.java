package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.f */
/* compiled from: lambda */
public final /* synthetic */ class C1877f implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2888P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2889Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSource.MediaPeriodId f2890R;

    /* renamed from: S */
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData f2891S;

    public /* synthetic */ C1877f(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSource.MediaPeriodId mediaPeriodId, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f2888P = eventDispatcher;
        this.f2889Q = mediaSourceEventListener;
        this.f2890R = mediaPeriodId;
        this.f2891S = mediaLoadData;
    }

    public final void run() {
        this.f2888P.mo14930a(this.f2889Q, this.f2890R, this.f2891S);
    }
}
