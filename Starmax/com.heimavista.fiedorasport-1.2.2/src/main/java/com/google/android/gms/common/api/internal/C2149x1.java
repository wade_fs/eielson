package com.google.android.gms.common.api.internal;

import android.os.IBinder;
import com.google.android.gms.common.api.C2164r;
import java.lang.ref.WeakReference;
import java.util.NoSuchElementException;

/* renamed from: com.google.android.gms.common.api.internal.x1 */
final class C2149x1 implements IBinder.DeathRecipient, C2152y1 {

    /* renamed from: a */
    private final WeakReference<BasePendingResult<?>> f3504a;

    /* renamed from: b */
    private final WeakReference<C2164r> f3505b;

    /* renamed from: c */
    private final WeakReference<IBinder> f3506c;

    private C2149x1(BasePendingResult<?> basePendingResult, C2164r rVar, IBinder iBinder) {
        this.f3505b = new WeakReference<>(rVar);
        this.f3504a = new WeakReference<>(basePendingResult);
        this.f3506c = new WeakReference<>(iBinder);
    }

    /* renamed from: a */
    public final void mo16795a(BasePendingResult<?> basePendingResult) {
        m5182a();
    }

    public final void binderDied() {
        m5182a();
    }

    /* renamed from: a */
    private final void m5182a() {
        BasePendingResult basePendingResult = this.f3504a.get();
        C2164r rVar = this.f3505b.get();
        if (!(rVar == null || basePendingResult == null)) {
            rVar.mo16815a(basePendingResult.mo16597d().intValue());
        }
        IBinder iBinder = this.f3506c.get();
        if (iBinder != null) {
            try {
                iBinder.unlinkToDeath(this, 0);
            } catch (NoSuchElementException unused) {
            }
        }
    }

    /* synthetic */ C2149x1(BasePendingResult basePendingResult, C2164r rVar, IBinder iBinder, C2145w1 w1Var) {
        this(basePendingResult, null, iBinder);
    }
}
