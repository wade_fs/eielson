package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.d8 */
/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.2.2 */
final class C3012d8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C2589kc f5052P;

    /* renamed from: Q */
    private final /* synthetic */ String f5053Q;

    /* renamed from: R */
    private final /* synthetic */ String f5054R;

    /* renamed from: S */
    private final /* synthetic */ AppMeasurementDynamiteService f5055S;

    C3012d8(AppMeasurementDynamiteService appMeasurementDynamiteService, C2589kc kcVar, String str, String str2) {
        this.f5055S = appMeasurementDynamiteService;
        this.f5052P = kcVar;
        this.f5053Q = str;
        this.f5054R = str2;
    }

    public final void run() {
        this.f5055S.f4930a.mo19079F().mo19377a(this.f5052P, this.f5053Q, this.f5054R);
    }
}
