package com.google.android.gms.maps.p093i;

import android.os.IInterface;
import com.google.android.gms.maps.model.LatLng;

/* renamed from: com.google.android.gms.maps.i.l */
public interface C2907l extends IInterface {
    /* renamed from: a */
    void mo18484a(LatLng latLng);
}
