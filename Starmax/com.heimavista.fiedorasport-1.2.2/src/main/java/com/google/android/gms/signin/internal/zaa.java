package com.google.android.gms.signin.internal;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class zaa extends AbstractSafeParcelable implements C2157k {
    public static final Parcelable.Creator<zaa> CREATOR = new C3269b();

    /* renamed from: P */
    private final int f5852P;

    /* renamed from: Q */
    private int f5853Q;

    /* renamed from: R */
    private Intent f5854R;

    zaa(int i, int i2, Intent intent) {
        this.f5852P = i;
        this.f5853Q = i2;
        this.f5854R = intent;
    }

    /* renamed from: b */
    public final Status mo16419b() {
        if (this.f5853Q == 0) {
            return Status.f3177T;
        }
        return Status.f3181X;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.content.Intent, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f5852P);
        C2250b.m5591a(parcel, 2, this.f5853Q);
        C2250b.m5596a(parcel, 3, (Parcelable) this.f5854R, i, false);
        C2250b.m5587a(parcel, a);
    }

    public zaa() {
        this(0, null);
    }

    private zaa(int i, Intent intent) {
        this(2, 0, null);
    }
}
