package com.google.android.gms.common.api.internal;

import android.os.DeadObjectException;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2064e;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.z1 */
abstract class C2155z1<T> extends C2101l1 {

    /* renamed from: a */
    protected final C4066i<T> f3531a;

    public C2155z1(int i, C4066i<T> iVar) {
        super(i);
        this.f3531a = iVar;
    }

    /* renamed from: a */
    public void mo16614a(@NonNull Status status) {
        this.f3531a.mo23721b((Exception) new C2030b(status));
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract void mo16636d(C2064e.C2065a<?> aVar);

    /* renamed from: a */
    public void mo16617a(@NonNull RuntimeException runtimeException) {
        this.f3531a.mo23721b((Exception) runtimeException);
    }

    /* renamed from: a */
    public final void mo16615a(C2064e.C2065a<?> aVar) {
        try {
            mo16636d(aVar);
        } catch (DeadObjectException e) {
            mo16614a(C2116p0.m5045a(e));
            throw e;
        } catch (RemoteException e2) {
            mo16614a(C2116p0.m5045a(e2));
        } catch (RuntimeException e3) {
            mo16617a(e3);
        }
    }
}
