package com.google.android.gms.maps.model;

import com.google.android.gms.common.internal.C2258v;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.model.a */
public final class C2929a {

    /* renamed from: a */
    private final C3988b f4911a;

    public C2929a(C3988b bVar) {
        C2258v.m5629a(bVar);
        this.f4911a = bVar;
    }

    /* renamed from: a */
    public final C3988b mo18622a() {
        return this.f4911a;
    }
}
