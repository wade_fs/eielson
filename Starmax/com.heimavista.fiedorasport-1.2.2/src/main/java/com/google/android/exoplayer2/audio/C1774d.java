package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* renamed from: com.google.android.exoplayer2.audio.d */
/* compiled from: lambda */
public final /* synthetic */ class C1774d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f2792P;

    /* renamed from: Q */
    private final /* synthetic */ int f2793Q;

    public /* synthetic */ C1774d(AudioRendererEventListener.EventDispatcher eventDispatcher, int i) {
        this.f2792P = eventDispatcher;
        this.f2793Q = i;
    }

    public final void run() {
        this.f2792P.mo14092a(this.f2793Q);
    }
}
