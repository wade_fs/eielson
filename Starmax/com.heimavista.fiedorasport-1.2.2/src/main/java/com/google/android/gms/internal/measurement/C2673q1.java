package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.q1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C2673q1 implements C2643o1 {

    /* renamed from: a */
    private final C2628n1 f4424a;

    /* renamed from: b */
    private final String f4425b;

    C2673q1(C2628n1 n1Var, String str) {
        this.f4424a = n1Var;
        this.f4425b = str;
    }

    /* renamed from: a */
    public final Object mo17495a() {
        return this.f4424a.mo17774b(this.f4425b);
    }
}
