package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.c4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2450c4 {

    /* renamed from: a */
    private static final C2418a4<?> f4013a = new C2814z3();

    /* renamed from: b */
    private static final C2418a4<?> f4014b = m6133c();

    /* renamed from: a */
    static C2418a4<?> m6131a() {
        return f4013a;
    }

    /* renamed from: b */
    static C2418a4<?> m6132b() {
        C2418a4<?> a4Var = f4014b;
        if (a4Var != null) {
            return a4Var;
        }
        throw new IllegalStateException("Protobuf runtime is not correctly loaded.");
    }

    /* renamed from: c */
    private static C2418a4<?> m6133c() {
        try {
            return (C2418a4) Class.forName("com.google.protobuf.ExtensionSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            return null;
        }
    }
}
