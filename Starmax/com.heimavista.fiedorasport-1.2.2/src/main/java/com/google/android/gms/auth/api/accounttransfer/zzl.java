package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.internal.auth.zzaz;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public final class zzl extends zzaz {
    public static final Parcelable.Creator<zzl> CREATOR = new C1954a();

    /* renamed from: U */
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> f2997U;

    /* renamed from: P */
    private final Set<Integer> f2998P;

    /* renamed from: Q */
    private final int f2999Q;

    /* renamed from: R */
    private ArrayList<zzr> f3000R;

    /* renamed from: S */
    private int f3001S;

    /* renamed from: T */
    private zzo f3002T;

    static {
        HashMap<String, FastJsonResponse.Field<?, ?>> hashMap = new HashMap<>();
        f2997U = hashMap;
        hashMap.put("authenticatorData", FastJsonResponse.Field.m5714b("authenticatorData", 2, zzr.class));
        f2997U.put(NotificationCompat.CATEGORY_PROGRESS, FastJsonResponse.Field.m5711a(NotificationCompat.CATEGORY_PROGRESS, 4, zzo.class));
    }

    zzl(Set<Integer> set, int i, ArrayList<zzr> arrayList, int i2, zzo zzo) {
        this.f2998P = set;
        this.f2999Q = i;
        this.f3000R = arrayList;
        this.f3001S = i2;
        this.f3002T = zzo;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo16319a(FastJsonResponse.Field field) {
        int c = field.mo17091c();
        if (c == 1) {
            return Integer.valueOf(this.f2999Q);
        }
        if (c == 2) {
            return this.f3000R;
        }
        if (c == 4) {
            return this.f3002T;
        }
        int c2 = field.mo17091c();
        StringBuilder sb = new StringBuilder(37);
        sb.append("Unknown SafeParcelable id=");
        sb.append(c2);
        throw new IllegalStateException(sb.toString());
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final boolean mo16321b(FastJsonResponse.Field field) {
        return this.f2998P.contains(Integer.valueOf(field.mo17091c()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.auth.api.accounttransfer.zzo, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        Set<Integer> set = this.f2998P;
        if (set.contains(1)) {
            C2250b.m5591a(parcel, 1, this.f2999Q);
        }
        if (set.contains(2)) {
            C2250b.m5614c(parcel, 2, this.f3000R, true);
        }
        if (set.contains(3)) {
            C2250b.m5591a(parcel, 3, this.f3001S);
        }
        if (set.contains(4)) {
            C2250b.m5596a(parcel, 4, (Parcelable) this.f3002T, i, true);
        }
        C2250b.m5587a(parcel, a);
    }

    public zzl() {
        this.f2998P = new HashSet(1);
        this.f2999Q = 1;
    }

    /* renamed from: a */
    public final /* synthetic */ Map mo16320a() {
        return f2997U;
    }
}
