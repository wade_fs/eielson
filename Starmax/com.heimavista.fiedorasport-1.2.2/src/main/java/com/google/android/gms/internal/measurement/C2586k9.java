package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.k9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C2586k9 {
    /* renamed from: a */
    boolean mo17653a();

    /* renamed from: e */
    boolean mo17654e();

    /* renamed from: f */
    boolean mo17655f();

    /* renamed from: g */
    boolean mo17656g();

    /* renamed from: t */
    boolean mo17657t();
}
