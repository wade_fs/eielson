package com.google.android.gms.common.internal.p089z;

import com.google.android.gms.common.api.C2016a;

/* renamed from: com.google.android.gms.common.internal.z.a */
public final class C2266a {

    /* renamed from: a */
    public static final C2016a.C2028g<C2275j> f3764a = new C2016a.C2028g<>();

    /* renamed from: b */
    private static final C2016a.C2017a<C2275j, Object> f3765b = new C2268c();

    /* renamed from: c */
    public static final C2016a<Object> f3766c = new C2016a<>("Common.API", f3765b, f3764a);

    /* renamed from: d */
    public static final C2269d f3767d = new C2270e();
}
