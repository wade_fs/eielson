package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.location.a0 */
public final class C2825a0 implements Parcelable.Creator<zzj> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        long j = 50;
        long j2 = Long.MAX_VALUE;
        boolean z = true;
        float f = 0.0f;
        int i = Integer.MAX_VALUE;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                z = C2248a.m5577r(parcel2, a);
            } else if (a2 == 2) {
                j = C2248a.m5546B(parcel2, a);
            } else if (a2 == 3) {
                f = C2248a.m5582w(parcel2, a);
            } else if (a2 == 4) {
                j2 = C2248a.m5546B(parcel2, a);
            } else if (a2 != 5) {
                C2248a.m5550F(parcel2, a);
            } else {
                i = C2248a.m5585z(parcel2, a);
            }
        }
        C2248a.m5576q(parcel2, b);
        return new zzj(z, j, f, j2, i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzj[i];
    }
}
