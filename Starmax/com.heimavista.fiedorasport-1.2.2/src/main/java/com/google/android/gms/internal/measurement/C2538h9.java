package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.util.Log;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.internal.measurement.h9 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2538h9 extends C2443bd {

    /* renamed from: a */
    private final AtomicReference<Bundle> f4219a = new AtomicReference<>();

    /* renamed from: b */
    private boolean f4220b;

    /* renamed from: a */
    public final String mo17563a(long j) {
        return (String) m6453a(mo17564b(j), String.class);
    }

    /* renamed from: b */
    public final Bundle mo17564b(long j) {
        Bundle bundle;
        synchronized (this.f4219a) {
            if (!this.f4220b) {
                try {
                    this.f4219a.wait(j);
                } catch (InterruptedException unused) {
                    return null;
                }
            }
            bundle = this.f4219a.get();
        }
        return bundle;
    }

    /* renamed from: d */
    public final void mo17292d(Bundle bundle) {
        synchronized (this.f4219a) {
            try {
                this.f4219a.set(bundle);
                this.f4220b = true;
                this.f4219a.notify();
            } catch (Throwable th) {
                this.f4219a.notify();
                throw th;
            }
        }
    }

    /* renamed from: a */
    public static <T> T m6453a(Bundle bundle, Class<T> cls) {
        Object obj;
        if (bundle == null || (obj = bundle.get("r")) == null) {
            return null;
        }
        try {
            return cls.cast(obj);
        } catch (ClassCastException e) {
            Log.w("AM", String.format("Unexpected object type. Expected, Received".concat(": %s, %s"), cls.getCanonicalName(), obj.getClass().getCanonicalName()), e);
            throw e;
        }
    }
}
