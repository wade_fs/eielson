package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.c */
/* compiled from: lambda */
public final /* synthetic */ class C1944c implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2950P;

    /* renamed from: Q */
    private final /* synthetic */ int f2951Q;

    /* renamed from: R */
    private final /* synthetic */ int f2952R;

    /* renamed from: S */
    private final /* synthetic */ int f2953S;

    /* renamed from: T */
    private final /* synthetic */ float f2954T;

    public /* synthetic */ C1944c(VideoRendererEventListener.EventDispatcher eventDispatcher, int i, int i2, int i3, float f) {
        this.f2950P = eventDispatcher;
        this.f2951Q = i;
        this.f2952R = i2;
        this.f2953S = i3;
        this.f2954T = f;
    }

    public final void run() {
        this.f2950P.mo16265a(this.f2951Q, this.f2952R, this.f2953S, this.f2954T);
    }
}
