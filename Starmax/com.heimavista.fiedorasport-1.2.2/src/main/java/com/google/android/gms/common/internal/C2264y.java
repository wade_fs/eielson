package com.google.android.gms.common.internal;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.common.R$string;

/* renamed from: com.google.android.gms.common.internal.y */
public class C2264y {

    /* renamed from: a */
    private final Resources f3762a;

    /* renamed from: b */
    private final String f3763b = this.f3762a.getResourcePackageName(R$string.common_google_play_services_unknown_issue);

    public C2264y(Context context) {
        C2258v.m5629a(context);
        this.f3762a = context.getResources();
    }

    /* renamed from: a */
    public String mo17046a(String str) {
        int identifier = this.f3762a.getIdentifier(str, "string", this.f3763b);
        if (identifier == 0) {
            return null;
        }
        return this.f3762a.getString(identifier);
    }
}
