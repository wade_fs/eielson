package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.credentials.CredentialPickerConfig;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class CredentialRequest extends AbstractSafeParcelable {
    public static final Parcelable.Creator<CredentialRequest> CREATOR = new C1966e();

    /* renamed from: P */
    private final int f3044P;

    /* renamed from: Q */
    private final boolean f3045Q;

    /* renamed from: R */
    private final String[] f3046R;

    /* renamed from: S */
    private final CredentialPickerConfig f3047S;

    /* renamed from: T */
    private final CredentialPickerConfig f3048T;

    /* renamed from: U */
    private final boolean f3049U;

    /* renamed from: V */
    private final String f3050V;

    /* renamed from: W */
    private final String f3051W;

    /* renamed from: X */
    private final boolean f3052X;

    CredentialRequest(int i, boolean z, String[] strArr, CredentialPickerConfig credentialPickerConfig, CredentialPickerConfig credentialPickerConfig2, boolean z2, String str, String str2, boolean z3) {
        this.f3044P = i;
        this.f3045Q = z;
        C2258v.m5629a(strArr);
        this.f3046R = strArr;
        this.f3047S = credentialPickerConfig == null ? new CredentialPickerConfig.C1961a().mo16341a() : credentialPickerConfig;
        this.f3048T = credentialPickerConfig2 == null ? new CredentialPickerConfig.C1961a().mo16341a() : credentialPickerConfig2;
        if (i < 3) {
            this.f3049U = true;
            this.f3050V = null;
            this.f3051W = null;
        } else {
            this.f3049U = z2;
            this.f3050V = str;
            this.f3051W = str2;
        }
        this.f3052X = z3;
    }

    @NonNull
    /* renamed from: c */
    public final String[] mo16342c() {
        return this.f3046R;
    }

    @NonNull
    /* renamed from: d */
    public final CredentialPickerConfig mo16343d() {
        return this.f3048T;
    }

    @NonNull
    /* renamed from: u */
    public final CredentialPickerConfig mo16344u() {
        return this.f3047S;
    }

    @Nullable
    /* renamed from: v */
    public final String mo16345v() {
        return this.f3051W;
    }

    @Nullable
    /* renamed from: w */
    public final String mo16346w() {
        return this.f3050V;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
     arg types: [android.os.Parcel, int, java.lang.String[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.auth.api.credentials.CredentialPickerConfig, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5605a(parcel, 1, mo16349y());
        C2250b.m5608a(parcel, 2, mo16342c(), false);
        C2250b.m5596a(parcel, 3, (Parcelable) mo16344u(), i, false);
        C2250b.m5596a(parcel, 4, (Parcelable) mo16343d(), i, false);
        C2250b.m5605a(parcel, 5, mo16348x());
        C2250b.m5602a(parcel, 6, mo16346w(), false);
        C2250b.m5602a(parcel, 7, mo16345v(), false);
        C2250b.m5591a(parcel, 1000, this.f3044P);
        C2250b.m5605a(parcel, 8, this.f3052X);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final boolean mo16348x() {
        return this.f3049U;
    }

    /* renamed from: y */
    public final boolean mo16349y() {
        return this.f3045Q;
    }
}
