package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.util.Log;
import androidx.loader.content.AsyncTaskLoader;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.internal.C2099l;
import java.util.Set;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public final class zze extends AsyncTaskLoader<Void> implements C2099l {

    /* renamed from: a */
    private Semaphore f3154a = new Semaphore(0);

    /* renamed from: b */
    private Set<C2036f> f3155b;

    public zze(Context context, Set<C2036f> set) {
        super(context);
        this.f3155b = set;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final Void loadInBackground() {
        int i = 0;
        for (C2036f fVar : this.f3155b) {
            if (fVar.mo16568a(this)) {
                i++;
            }
        }
        try {
            this.f3154a.tryAcquire(i, 5, TimeUnit.SECONDS);
            return null;
        } catch (InterruptedException e) {
            Log.i("GACSignInLoader", "Unexpected InterruptedException", e);
            Thread.currentThread().interrupt();
            return null;
        }
    }

    public final void onComplete() {
        this.f3154a.release();
    }

    /* access modifiers changed from: protected */
    public final void onStartLoading() {
        this.f3154a.drainPermits();
        forceLoad();
    }
}
