package com.google.android.gms.auth.api.signin.internal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.signin.C1981d;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.C2042h;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2064e;
import com.google.android.gms.common.p087h.C2178a;

/* renamed from: com.google.android.gms.auth.api.signin.internal.h */
public final class C1995h {

    /* renamed from: a */
    private static C2178a f3148a = new C2178a("GoogleSignInCommon", new String[0]);

    /* renamed from: a */
    public static Intent m4591a(Context context, GoogleSignInOptions googleSignInOptions) {
        f3148a.mo16856a("getSignInIntent()", new Object[0]);
        SignInConfiguration signInConfiguration = new SignInConfiguration(context.getPackageName(), googleSignInOptions);
        Intent intent = new Intent("com.google.android.gms.auth.GOOGLE_SIGN_IN");
        intent.setPackage(context.getPackageName());
        intent.setClass(context, SignInHubActivity.class);
        Bundle bundle = new Bundle();
        bundle.putParcelable("config", signInConfiguration);
        intent.putExtra("config", bundle);
        return intent;
    }

    /* renamed from: b */
    public static Intent m4595b(Context context, GoogleSignInOptions googleSignInOptions) {
        f3148a.mo16856a("getFallbackSignInIntent()", new Object[0]);
        Intent a = m4591a(context, googleSignInOptions);
        a.setAction("com.google.android.gms.auth.APPAUTH_SIGN_IN");
        return a;
    }

    /* renamed from: c */
    public static Intent m4597c(Context context, GoogleSignInOptions googleSignInOptions) {
        f3148a.mo16856a("getNoImplementationSignInIntent()", new Object[0]);
        Intent a = m4591a(context, googleSignInOptions);
        a.setAction("com.google.android.gms.auth.NO_IMPL");
        return a;
    }

    /* renamed from: b */
    public static C2040g<Status> m4596b(C2036f fVar, Context context, boolean z) {
        f3148a.mo16856a("Revoking access", new Object[0]);
        String d = C1989b.m4567a(context).mo16440d();
        m4594a(context);
        if (z) {
            return C1992e.m4582a(d);
        }
        return fVar.mo16564a(new C1998k(fVar));
    }

    /* renamed from: a */
    public static C2040g<Status> m4593a(C2036f fVar, Context context, boolean z) {
        f3148a.mo16856a("Signing out", new Object[0]);
        m4594a(context);
        if (z) {
            return C2042h.m4721a(Status.f3177T, fVar);
        }
        return fVar.mo16564a(new C1996i(fVar));
    }

    /* renamed from: a */
    private static void m4594a(Context context) {
        C2001n.m4604a(context).mo16457a();
        for (C2036f fVar : C2036f.m4692h()) {
            fVar.mo16575g();
        }
        C2064e.m4795d();
    }

    @Nullable
    /* renamed from: a */
    public static C1981d m4592a(Intent intent) {
        if (intent == null) {
            return null;
        }
        if (!intent.hasExtra("googleSignInStatus") && !intent.hasExtra("googleSignInAccount")) {
            return null;
        }
        GoogleSignInAccount googleSignInAccount = (GoogleSignInAccount) intent.getParcelableExtra("googleSignInAccount");
        Status status = (Status) intent.getParcelableExtra("googleSignInStatus");
        if (googleSignInAccount != null) {
            status = Status.f3177T;
        }
        return new C1981d(googleSignInAccount, status);
    }
}
