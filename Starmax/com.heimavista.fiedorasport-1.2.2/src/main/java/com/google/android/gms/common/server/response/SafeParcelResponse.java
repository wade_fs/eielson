package com.google.android.gms.common.server.response;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.common.util.C2311b;
import com.google.android.gms.common.util.C2312c;
import com.google.android.gms.common.util.C2321l;
import com.google.android.gms.common.util.C2322m;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class SafeParcelResponse extends FastSafeParcelableJsonResponse {
    public static final Parcelable.Creator<SafeParcelResponse> CREATOR = new C2302e();

    /* renamed from: P */
    private final int f3810P;

    /* renamed from: Q */
    private final Parcel f3811Q;

    /* renamed from: R */
    private final int f3812R = 2;

    /* renamed from: S */
    private final zak f3813S;

    /* renamed from: T */
    private final String f3814T;

    /* renamed from: U */
    private int f3815U;

    /* renamed from: V */
    private int f3816V;

    SafeParcelResponse(int i, Parcel parcel, zak zak) {
        this.f3810P = i;
        C2258v.m5629a(parcel);
        this.f3811Q = parcel;
        this.f3813S = zak;
        zak zak2 = this.f3813S;
        if (zak2 == null) {
            this.f3814T = null;
        } else {
            this.f3814T = zak2.mo17112d();
        }
        this.f3815U = 2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0005, code lost:
        if (r0 != 1) goto L_0x001a;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final android.os.Parcel m5730b() {
        /*
            r2 = this;
            int r0 = r2.f3815U
            if (r0 == 0) goto L_0x0008
            r1 = 1
            if (r0 == r1) goto L_0x0010
            goto L_0x001a
        L_0x0008:
            android.os.Parcel r0 = r2.f3811Q
            int r0 = com.google.android.gms.common.internal.safeparcel.C2250b.m5586a(r0)
            r2.f3816V = r0
        L_0x0010:
            android.os.Parcel r0 = r2.f3811Q
            int r1 = r2.f3816V
            com.google.android.gms.common.internal.safeparcel.C2250b.m5587a(r0, r1)
            r0 = 2
            r2.f3815U = r0
        L_0x001a:
            android.os.Parcel r0 = r2.f3811Q
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.server.response.SafeParcelResponse.m5730b():android.os.Parcel");
    }

    /* renamed from: a */
    public Map<String, FastJsonResponse.Field<?, ?>> mo16320a() {
        zak zak = this.f3813S;
        if (zak == null) {
            return null;
        }
        return zak.mo17110b(this.f3814T);
    }

    public String toString() {
        C2258v.m5630a(this.f3813S, "Cannot convert to JSON on client side.");
        Parcel b = m5730b();
        b.setDataPosition(0);
        StringBuilder sb = new StringBuilder(100);
        m5729a(sb, this.f3813S.mo17110b(this.f3814T), b);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
     arg types: [android.os.Parcel, int, android.os.Parcel, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.common.server.response.zak, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        zak zak;
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3810P);
        C2250b.m5595a(parcel, 2, m5730b(), false);
        int i2 = this.f3812R;
        if (i2 == 0) {
            zak = null;
        } else if (i2 == 1) {
            zak = this.f3813S;
        } else if (i2 == 2) {
            zak = this.f3813S;
        } else {
            StringBuilder sb = new StringBuilder(34);
            sb.append("Invalid creation type: ");
            sb.append(i2);
            throw new IllegalStateException(sb.toString());
        }
        C2250b.m5596a(parcel, 3, (Parcelable) zak, i, false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: a */
    public Object mo17086a(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    /* renamed from: a */
    private final void m5729a(StringBuilder sb, Map<String, FastJsonResponse.Field<?, ?>> map, Parcel parcel) {
        SparseArray sparseArray = new SparseArray();
        for (Map.Entry entry : map.entrySet()) {
            sparseArray.put(((FastJsonResponse.Field) entry.getValue()).mo17091c(), entry);
        }
        sb.append('{');
        int b = C2248a.m5558b(parcel);
        boolean z = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            Map.Entry entry2 = (Map.Entry) sparseArray.get(C2248a.m5551a(a));
            if (entry2 != null) {
                if (z) {
                    sb.append(",");
                }
                FastJsonResponse.Field field = (FastJsonResponse.Field) entry2.getValue();
                sb.append("\"");
                sb.append((String) entry2.getKey());
                sb.append("\":");
                if (field.mo17092d()) {
                    int i = field.f3802S;
                    switch (i) {
                        case 0:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, Integer.valueOf(C2248a.m5585z(parcel, a))));
                            break;
                        case 1:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, C2248a.m5561c(parcel, a)));
                            break;
                        case 2:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, Long.valueOf(C2248a.m5546B(parcel, a))));
                            break;
                        case 3:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, Float.valueOf(C2248a.m5582w(parcel, a))));
                            break;
                        case 4:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, Double.valueOf(C2248a.m5580u(parcel, a))));
                            break;
                        case 5:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, C2248a.m5554a(parcel, a)));
                            break;
                        case 6:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, Boolean.valueOf(C2248a.m5577r(parcel, a))));
                            break;
                        case 7:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, C2248a.m5573n(parcel, a)));
                            break;
                        case 8:
                        case 9:
                            m5728a(sb, field, FastJsonResponse.m5703a(field, C2248a.m5566g(parcel, a)));
                            break;
                        case 10:
                            Bundle f = C2248a.m5565f(parcel, a);
                            HashMap hashMap = new HashMap();
                            for (String str : f.keySet()) {
                                hashMap.put(str, f.getString(str));
                            }
                            m5728a(sb, field, FastJsonResponse.m5703a(field, hashMap));
                            break;
                        case 11:
                            throw new IllegalArgumentException("Method does not accept concrete type.");
                        default:
                            StringBuilder sb2 = new StringBuilder(36);
                            sb2.append("Unknown field out type = ");
                            sb2.append(i);
                            throw new IllegalArgumentException(sb2.toString());
                    }
                } else if (field.f3803T) {
                    sb.append("[");
                    switch (field.f3802S) {
                        case 0:
                            C2311b.m5761a(sb, C2248a.m5569j(parcel, a));
                            break;
                        case 1:
                            C2311b.m5763a(sb, C2248a.m5563d(parcel, a));
                            break;
                        case 2:
                            C2311b.m5762a(sb, C2248a.m5570k(parcel, a));
                            break;
                        case 3:
                            C2311b.m5760a(sb, C2248a.m5568i(parcel, a));
                            break;
                        case 4:
                            C2311b.m5759a(sb, C2248a.m5567h(parcel, a));
                            break;
                        case 5:
                            C2311b.m5763a(sb, C2248a.m5560b(parcel, a));
                            break;
                        case 6:
                            C2311b.m5765a(sb, C2248a.m5564e(parcel, a));
                            break;
                        case 7:
                            C2311b.m5764a(sb, C2248a.m5574o(parcel, a));
                            break;
                        case 8:
                        case 9:
                        case 10:
                            throw new UnsupportedOperationException("List of type BASE64, BASE64_URL_SAFE, or STRING_MAP is not supported");
                        case 11:
                            Parcel[] m = C2248a.m5572m(parcel, a);
                            int length = m.length;
                            for (int i2 = 0; i2 < length; i2++) {
                                if (i2 > 0) {
                                    sb.append(",");
                                }
                                m[i2].setDataPosition(0);
                                m5729a(sb, field.mo17094u(), m[i2]);
                            }
                            break;
                        default:
                            throw new IllegalStateException("Unknown field type out.");
                    }
                    sb.append("]");
                } else {
                    switch (field.f3802S) {
                        case 0:
                            sb.append(C2248a.m5585z(parcel, a));
                            break;
                        case 1:
                            sb.append(C2248a.m5561c(parcel, a));
                            break;
                        case 2:
                            sb.append(C2248a.m5546B(parcel, a));
                            break;
                        case 3:
                            sb.append(C2248a.m5582w(parcel, a));
                            break;
                        case 4:
                            sb.append(C2248a.m5580u(parcel, a));
                            break;
                        case 5:
                            sb.append(C2248a.m5554a(parcel, a));
                            break;
                        case 6:
                            sb.append(C2248a.m5577r(parcel, a));
                            break;
                        case 7:
                            String n = C2248a.m5573n(parcel, a);
                            sb.append("\"");
                            sb.append(C2321l.m5790a(n));
                            sb.append("\"");
                            break;
                        case 8:
                            byte[] g = C2248a.m5566g(parcel, a);
                            sb.append("\"");
                            sb.append(C2312c.m5768a(g));
                            sb.append("\"");
                            break;
                        case 9:
                            byte[] g2 = C2248a.m5566g(parcel, a);
                            sb.append("\"");
                            sb.append(C2312c.m5769b(g2));
                            sb.append("\"");
                            break;
                        case 10:
                            Bundle f2 = C2248a.m5565f(parcel, a);
                            Set<String> keySet = f2.keySet();
                            keySet.size();
                            sb.append("{");
                            boolean z2 = true;
                            for (String str2 : keySet) {
                                if (!z2) {
                                    sb.append(",");
                                }
                                sb.append("\"");
                                sb.append(str2);
                                sb.append("\"");
                                sb.append(":");
                                sb.append("\"");
                                sb.append(C2321l.m5790a(f2.getString(str2)));
                                sb.append("\"");
                                z2 = false;
                            }
                            sb.append("}");
                            break;
                        case 11:
                            Parcel l = C2248a.m5571l(parcel, a);
                            l.setDataPosition(0);
                            m5729a(sb, field.mo17094u(), l);
                            break;
                        default:
                            throw new IllegalStateException("Unknown field type out");
                    }
                }
                z = true;
            }
        }
        if (parcel.dataPosition() == b) {
            sb.append('}');
            return;
        }
        StringBuilder sb3 = new StringBuilder(37);
        sb3.append("Overread allowed size end=");
        sb3.append(b);
        throw new C2248a.C2249a(sb3.toString(), parcel);
    }

    /* renamed from: b */
    public boolean mo17087b(String str) {
        throw new UnsupportedOperationException("Converting to JSON does not require this method.");
    }

    /* renamed from: a */
    private final void m5728a(StringBuilder sb, FastJsonResponse.Field<?, ?> field, Object obj) {
        if (field.f3801R) {
            ArrayList arrayList = (ArrayList) obj;
            sb.append("[");
            int size = arrayList.size();
            for (int i = 0; i < size; i++) {
                if (i != 0) {
                    sb.append(",");
                }
                m5727a(sb, field.f3800Q, arrayList.get(i));
            }
            sb.append("]");
            return;
        }
        m5727a(sb, field.f3800Q, obj);
    }

    /* renamed from: a */
    private static void m5727a(StringBuilder sb, int i, Object obj) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                sb.append(obj);
                return;
            case 7:
                sb.append("\"");
                sb.append(C2321l.m5790a(obj.toString()));
                sb.append("\"");
                return;
            case 8:
                sb.append("\"");
                sb.append(C2312c.m5768a((byte[]) obj));
                sb.append("\"");
                return;
            case 9:
                sb.append("\"");
                sb.append(C2312c.m5769b((byte[]) obj));
                sb.append("\"");
                return;
            case 10:
                C2322m.m5791a(sb, (HashMap) obj);
                return;
            case 11:
                throw new IllegalArgumentException("Method does not accept concrete type.");
            default:
                StringBuilder sb2 = new StringBuilder(26);
                sb2.append("Unknown type = ");
                sb2.append(i);
                throw new IllegalArgumentException(sb2.toString());
        }
    }
}
