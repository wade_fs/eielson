package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.android.datatransport.cct.b.c */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1664c implements C3536d<C1665d> {
    /* renamed from: a */
    public void mo13453a(@Nullable Object obj, @NonNull Object obj2) {
        C1665d dVar = (C1665d) obj;
        C3537e eVar = (C3537e) obj2;
        if (dVar.mo13463i() != Integer.MIN_VALUE) {
            eVar.mo22171a("sdkVersion", dVar.mo13463i());
        }
        if (dVar.mo13459f() != null) {
            eVar.mo22173a("model", dVar.mo13459f());
        }
        if (dVar.mo13456d() != null) {
            eVar.mo22173a("hardware", dVar.mo13456d());
        }
        if (dVar.mo13454b() != null) {
            eVar.mo22173a("device", dVar.mo13454b());
        }
        if (dVar.mo13461h() != null) {
            eVar.mo22173a("product", dVar.mo13461h());
        }
        if (dVar.mo13460g() != null) {
            eVar.mo22173a("osBuild", dVar.mo13460g());
        }
        if (dVar.mo13457e() != null) {
            eVar.mo22173a("manufacturer", dVar.mo13457e());
        }
        if (dVar.mo13455c() != null) {
            eVar.mo22173a("fingerprint", dVar.mo13455c());
        }
    }
}
