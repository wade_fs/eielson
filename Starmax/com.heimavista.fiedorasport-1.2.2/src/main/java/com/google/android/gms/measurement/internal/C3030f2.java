package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2458cc;

/* renamed from: com.google.android.gms.measurement.internal.f2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3030f2 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5120a = new C3030f2();

    private C3030f2() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2458cc.m6166e());
    }
}
