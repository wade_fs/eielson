package com.google.android.gms.common.api;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2157k;

/* renamed from: com.google.android.gms.common.api.l */
public interface C2158l<R extends C2157k> {
    /* renamed from: a */
    void mo16767a(@NonNull Object obj);
}
