package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class ActivityTransition extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ActivityTransition> CREATOR = new C2859t();

    /* renamed from: P */
    private final int f4644P;

    /* renamed from: Q */
    private final int f4645Q;

    ActivityTransition(int i, int i2) {
        this.f4644P = i;
        this.f4645Q = i2;
    }

    /* renamed from: a */
    public static void m7938a(int i) {
        boolean z = true;
        if (i < 0 || i > 1) {
            z = false;
        }
        StringBuilder sb = new StringBuilder(41);
        sb.append("Transition type ");
        sb.append(i);
        sb.append(" is not valid.");
        C2258v.m5637a(z, sb.toString());
    }

    /* renamed from: c */
    public int mo18190c() {
        return this.f4644P;
    }

    /* renamed from: d */
    public int mo18191d() {
        return this.f4645Q;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityTransition)) {
            return false;
        }
        ActivityTransition activityTransition = (ActivityTransition) obj;
        return this.f4644P == activityTransition.f4644P && this.f4645Q == activityTransition.f4645Q;
    }

    public int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4644P), Integer.valueOf(this.f4645Q));
    }

    public String toString() {
        int i = this.f4644P;
        int i2 = this.f4645Q;
        StringBuilder sb = new StringBuilder(75);
        sb.append("ActivityTransition [mActivityType=");
        sb.append(i);
        sb.append(", mTransitionType=");
        sb.append(i2);
        sb.append(']');
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, mo18190c());
        C2250b.m5591a(parcel, 2, mo18191d());
        C2250b.m5587a(parcel, a);
    }
}
