package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class CameraPosition extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<CameraPosition> CREATOR = new C2936g();

    /* renamed from: P */
    public final LatLng f4798P;

    /* renamed from: Q */
    public final float f4799Q;

    /* renamed from: R */
    public final float f4800R;

    /* renamed from: S */
    public final float f4801S;

    /* renamed from: com.google.android.gms.maps.model.CameraPosition$a */
    public static final class C2926a {

        /* renamed from: a */
        private LatLng f4802a;

        /* renamed from: b */
        private float f4803b;

        /* renamed from: c */
        private float f4804c;

        /* renamed from: d */
        private float f4805d;

        /* renamed from: a */
        public final C2926a mo18498a(LatLng latLng) {
            this.f4802a = latLng;
            return this;
        }

        /* renamed from: b */
        public final C2926a mo18500b(float f) {
            this.f4804c = f;
            return this;
        }

        /* renamed from: c */
        public final C2926a mo18501c(float f) {
            this.f4803b = f;
            return this;
        }

        /* renamed from: a */
        public final C2926a mo18497a(float f) {
            this.f4805d = f;
            return this;
        }

        /* renamed from: a */
        public final CameraPosition mo18499a() {
            return new CameraPosition(this.f4802a, this.f4803b, this.f4804c, this.f4805d);
        }
    }

    public CameraPosition(LatLng latLng, float f, float f2, float f3) {
        C2258v.m5630a(latLng, "null camera target");
        C2258v.m5638a(0.0f <= f2 && f2 <= 90.0f, "Tilt needs to be between 0 and 90 inclusive: %s", Float.valueOf(f2));
        this.f4798P = latLng;
        this.f4799Q = f;
        this.f4800R = f2 + 0.0f;
        this.f4801S = (((double) f3) <= 0.0d ? (f3 % 360.0f) + 360.0f : f3) % 360.0f;
    }

    /* renamed from: c */
    public static C2926a m8258c() {
        return new C2926a();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CameraPosition)) {
            return false;
        }
        CameraPosition cameraPosition = (CameraPosition) obj;
        return this.f4798P.equals(cameraPosition.f4798P) && Float.floatToIntBits(this.f4799Q) == Float.floatToIntBits(cameraPosition.f4799Q) && Float.floatToIntBits(this.f4800R) == Float.floatToIntBits(cameraPosition.f4800R) && Float.floatToIntBits(this.f4801S) == Float.floatToIntBits(cameraPosition.f4801S);
    }

    public final int hashCode() {
        return C2251t.m5615a(this.f4798P, Float.valueOf(this.f4799Q), Float.valueOf(this.f4800R), Float.valueOf(this.f4801S));
    }

    public final String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("target", this.f4798P);
        a.mo17037a("zoom", Float.valueOf(this.f4799Q));
        a.mo17037a("tilt", Float.valueOf(this.f4800R));
        a.mo17037a("bearing", Float.valueOf(this.f4801S));
        return a.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 2, (Parcelable) this.f4798P, i, false);
        C2250b.m5590a(parcel, 3, this.f4799Q);
        C2250b.m5590a(parcel, 4, this.f4800R);
        C2250b.m5590a(parcel, 5, this.f4801S);
        C2250b.m5587a(parcel, a);
    }
}
