package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2466d4;
import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.b4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2434b4<T extends C2466d4<T>> {

    /* renamed from: d */
    private static final C2434b4 f3995d = new C2434b4(true);

    /* renamed from: a */
    final C2618m6<T, Object> f3996a;

    /* renamed from: b */
    private boolean f3997b;

    /* renamed from: c */
    private boolean f3998c;

    private C2434b4() {
        this.f3996a = C2618m6.m6780b(16);
    }

    /* renamed from: g */
    public static <T extends C2466d4<T>> C2434b4<T> m6069g() {
        return f3995d;
    }

    /* renamed from: a */
    public final void mo17310a() {
        if (!this.f3997b) {
            this.f3996a.mo17742a();
            this.f3997b = true;
        }
    }

    /* renamed from: b */
    public final boolean mo17312b() {
        return this.f3997b;
    }

    /* renamed from: c */
    public final Iterator<Map.Entry<T, Object>> mo17313c() {
        if (this.f3998c) {
            return new C2815z4(this.f3996a.entrySet().iterator());
        }
        return this.f3996a.entrySet().iterator();
    }

    public final /* synthetic */ Object clone() {
        C2434b4 b4Var = new C2434b4();
        if (this.f3996a.mo17744c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = this.f3996a.mo17747d().iterator();
            if (!it.hasNext()) {
                b4Var.f3998c = this.f3998c;
                return b4Var;
            }
            Map.Entry next = it.next();
            b4Var.m6066b((C2466d4) next.getKey(), next.getValue());
            throw null;
        }
        Map.Entry<T, Object> a = this.f3996a.mo17741a(0);
        b4Var.m6066b((C2466d4) a.getKey(), a.getValue());
        throw null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final Iterator<Map.Entry<T, Object>> mo17315d() {
        if (this.f3998c) {
            return new C2815z4(this.f3996a.mo17748e().iterator());
        }
        return this.f3996a.mo17748e().iterator();
    }

    /* renamed from: e */
    public final boolean mo17316e() {
        if (this.f3996a.mo17744c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = this.f3996a.mo17747d().iterator();
            if (!it.hasNext()) {
                return true;
            }
            m6065a(it.next());
            throw null;
        }
        m6065a(this.f3996a.mo17741a(0));
        throw null;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2434b4)) {
            return false;
        }
        return this.f3996a.equals(((C2434b4) obj).f3996a);
    }

    /* renamed from: f */
    public final int mo17318f() {
        if (this.f3996a.mo17744c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = this.f3996a.mo17747d().iterator();
            if (!it.hasNext()) {
                return 0;
            }
            m6068c(it.next());
            throw null;
        }
        m6068c(this.f3996a.mo17741a(0));
        throw null;
    }

    public final int hashCode() {
        return this.f3996a.hashCode();
    }

    /* renamed from: b */
    private final void m6066b(T t, Object obj) {
        t.mo17415g();
        throw null;
    }

    private C2434b4(boolean z) {
        this(C2618m6.m6780b(0));
        mo17310a();
    }

    /* renamed from: b */
    private final void m6067b(Map.Entry<T, Object> entry) {
        C2466d4 d4Var = (C2466d4) entry.getKey();
        Object value = entry.getValue();
        if (value instanceof C2800y4) {
            C2800y4 y4Var = (C2800y4) value;
            C2800y4.m7831c();
            throw null;
        }
        d4Var.mo17415g();
        throw null;
    }

    /* renamed from: a */
    private static <T extends C2466d4<T>> boolean m6065a(Map.Entry<T, Object> entry) {
        ((C2466d4) entry.getKey()).mo17414f();
        throw null;
    }

    /* renamed from: c */
    private static int m6068c(Map.Entry<T, Object> entry) {
        entry.getValue();
        ((C2466d4) entry.getKey()).mo17414f();
        throw null;
    }

    private C2434b4(C2618m6<T, Object> m6Var) {
        this.f3996a = m6Var;
        mo17310a();
    }

    /* renamed from: a */
    public final void mo17311a(C2434b4<T> b4Var) {
        if (b4Var.f3996a.mo17744c() <= 0) {
            Iterator<Map.Entry<T, Object>> it = b4Var.f3996a.mo17747d().iterator();
            if (it.hasNext()) {
                m6067b(it.next());
                throw null;
            }
            return;
        }
        m6067b(b4Var.f3996a.mo17741a(0));
        throw null;
    }

    /* renamed from: a */
    public static int m6064a(C2466d4<?> d4Var, Object obj) {
        d4Var.mo17413e();
        throw null;
    }
}
