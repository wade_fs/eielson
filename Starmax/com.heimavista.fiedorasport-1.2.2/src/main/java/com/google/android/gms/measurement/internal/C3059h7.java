package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.MainThread;
import com.google.android.gms.internal.measurement.C2457cb;
import com.google.android.gms.internal.measurement.C2607la;

@TargetApi(14)
@MainThread
/* renamed from: com.google.android.gms.measurement.internal.h7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3059h7 implements Application.ActivityLifecycleCallbacks {

    /* renamed from: P */
    private final /* synthetic */ C3154p6 f5181P;

    private C3059h7(C3154p6 p6Var) {
        this.f5181P = p6Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, java.lang.String, int]
     candidates:
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, java.lang.Object):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, boolean):java.util.Map<java.lang.String, java.lang.Object>
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, long, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, android.os.Bundle, boolean):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, long):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void
      com.google.android.gms.measurement.internal.p6.a(java.lang.String, java.lang.String, java.lang.Object, boolean):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x009f A[SYNTHETIC, Splitter:B:33:0x009f] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e7 A[Catch:{ Exception -> 0x01ac }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00f8 A[SYNTHETIC, Splitter:B:49:0x00f8] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0127 A[Catch:{ Exception -> 0x01ac }, RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0128 A[Catch:{ Exception -> 0x01ac }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m8719a(boolean r18, android.net.Uri r19, java.lang.String r20, java.lang.String r21) {
        /*
            r17 = this;
            r1 = r17
            r0 = r20
            r2 = r21
            com.google.android.gms.measurement.internal.p6 r3 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r3 = r3.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r4 = com.google.android.gms.measurement.internal.C3135o.f5485z0     // Catch:{ Exception -> 0x01ac }
            boolean r3 = r3.mo19146a(r4)     // Catch:{ Exception -> 0x01ac }
            java.lang.String r4 = "Activity created with data 'referrer' without required params"
            java.lang.String r5 = "utm_medium"
            java.lang.String r6 = "_cis"
            java.lang.String r7 = "utm_source"
            java.lang.String r8 = "utm_campaign"
            java.lang.String r10 = "gclid"
            if (r3 != 0) goto L_0x0042
            com.google.android.gms.measurement.internal.p6 r3 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r3 = r3.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r11 = com.google.android.gms.measurement.internal.C3135o.f5385B0     // Catch:{ Exception -> 0x01ac }
            boolean r3 = r3.mo19146a(r11)     // Catch:{ Exception -> 0x01ac }
            if (r3 != 0) goto L_0x0042
            com.google.android.gms.measurement.internal.p6 r3 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r3 = r3.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r11 = com.google.android.gms.measurement.internal.C3135o.f5383A0     // Catch:{ Exception -> 0x01ac }
            boolean r3 = r3.mo19146a(r11)     // Catch:{ Exception -> 0x01ac }
            if (r3 == 0) goto L_0x0040
            goto L_0x0042
        L_0x0040:
            r3 = 0
            goto L_0x0099
        L_0x0042:
            com.google.android.gms.measurement.internal.p6 r3 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.z9 r3 = r3.mo19011f()     // Catch:{ Exception -> 0x01ac }
            boolean r11 = android.text.TextUtils.isEmpty(r21)     // Catch:{ Exception -> 0x01ac }
            if (r11 == 0) goto L_0x004f
            goto L_0x0040
        L_0x004f:
            boolean r11 = r2.contains(r10)     // Catch:{ Exception -> 0x01ac }
            if (r11 != 0) goto L_0x0073
            boolean r11 = r2.contains(r8)     // Catch:{ Exception -> 0x01ac }
            if (r11 != 0) goto L_0x0073
            boolean r11 = r2.contains(r7)     // Catch:{ Exception -> 0x01ac }
            if (r11 != 0) goto L_0x0073
            boolean r11 = r2.contains(r5)     // Catch:{ Exception -> 0x01ac }
            if (r11 != 0) goto L_0x0073
            com.google.android.gms.measurement.internal.f4 r3 = r3.mo19015l()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo18995A()     // Catch:{ Exception -> 0x01ac }
            r3.mo19042a(r4)     // Catch:{ Exception -> 0x01ac }
            goto L_0x0040
        L_0x0073:
            java.lang.String r11 = "https://google.com/search?"
            java.lang.String r12 = java.lang.String.valueOf(r21)     // Catch:{ Exception -> 0x01ac }
            int r13 = r12.length()     // Catch:{ Exception -> 0x01ac }
            if (r13 == 0) goto L_0x0084
            java.lang.String r11 = r11.concat(r12)     // Catch:{ Exception -> 0x01ac }
            goto L_0x008a
        L_0x0084:
            java.lang.String r12 = new java.lang.String     // Catch:{ Exception -> 0x01ac }
            r12.<init>(r11)     // Catch:{ Exception -> 0x01ac }
            r11 = r12
        L_0x008a:
            android.net.Uri r11 = android.net.Uri.parse(r11)     // Catch:{ Exception -> 0x01ac }
            android.os.Bundle r3 = r3.mo19425a(r11)     // Catch:{ Exception -> 0x01ac }
            if (r3 == 0) goto L_0x0099
            java.lang.String r11 = "referrer"
            r3.putString(r6, r11)     // Catch:{ Exception -> 0x01ac }
        L_0x0099:
            r11 = 0
            java.lang.String r12 = "_cmp"
            r13 = 1
            if (r18 == 0) goto L_0x00e7
            com.google.android.gms.measurement.internal.p6 r14 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.z9 r14 = r14.mo19011f()     // Catch:{ Exception -> 0x01ac }
            r15 = r19
            android.os.Bundle r14 = r14.mo19425a(r15)     // Catch:{ Exception -> 0x01ac }
            if (r14 == 0) goto L_0x00e8
            java.lang.String r15 = "intent"
            r14.putString(r6, r15)     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.C3135o.f5485z0     // Catch:{ Exception -> 0x01ac }
            boolean r6 = r6.mo19146a(r15)     // Catch:{ Exception -> 0x01ac }
            if (r6 == 0) goto L_0x00e1
            boolean r6 = r14.containsKey(r10)     // Catch:{ Exception -> 0x01ac }
            if (r6 != 0) goto L_0x00e1
            if (r3 == 0) goto L_0x00e1
            boolean r6 = r3.containsKey(r10)     // Catch:{ Exception -> 0x01ac }
            if (r6 == 0) goto L_0x00e1
            java.lang.String r6 = "_cer"
            java.lang.String r15 = "gclid=%s"
            java.lang.Object[] r9 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x01ac }
            java.lang.String r16 = r3.getString(r10)     // Catch:{ Exception -> 0x01ac }
            r9[r11] = r16     // Catch:{ Exception -> 0x01ac }
            java.lang.String r9 = java.lang.String.format(r15, r9)     // Catch:{ Exception -> 0x01ac }
            r14.putString(r6, r9)     // Catch:{ Exception -> 0x01ac }
        L_0x00e1:
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            r6.mo19267a(r0, r12, r14)     // Catch:{ Exception -> 0x01ac }
            goto L_0x00e8
        L_0x00e7:
            r14 = 0
        L_0x00e8:
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r9 = com.google.android.gms.measurement.internal.C3135o.f5385B0     // Catch:{ Exception -> 0x01ac }
            boolean r6 = r6.mo19146a(r9)     // Catch:{ Exception -> 0x01ac }
            java.lang.String r9 = "auto"
            if (r6 == 0) goto L_0x0121
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r15 = com.google.android.gms.measurement.internal.C3135o.f5383A0     // Catch:{ Exception -> 0x01ac }
            boolean r6 = r6.mo19146a(r15)     // Catch:{ Exception -> 0x01ac }
            if (r6 != 0) goto L_0x0121
            if (r3 == 0) goto L_0x0121
            boolean r6 = r3.containsKey(r10)     // Catch:{ Exception -> 0x01ac }
            if (r6 == 0) goto L_0x0121
            if (r14 == 0) goto L_0x0116
            boolean r6 = r14.containsKey(r10)     // Catch:{ Exception -> 0x01ac }
            if (r6 != 0) goto L_0x0121
        L_0x0116:
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            java.lang.String r14 = "_lgclid"
            java.lang.String r15 = r3.getString(r10)     // Catch:{ Exception -> 0x01ac }
            r6.mo19271a(r9, r14, r15, r13)     // Catch:{ Exception -> 0x01ac }
        L_0x0121:
            boolean r6 = android.text.TextUtils.isEmpty(r21)     // Catch:{ Exception -> 0x01ac }
            if (r6 == 0) goto L_0x0128
            return
        L_0x0128:
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.f4 r6 = r6.mo19015l()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.h4 r6 = r6.mo18995A()     // Catch:{ Exception -> 0x01ac }
            java.lang.String r14 = "Activity created with referrer"
            r6.mo19043a(r14, r2)     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.p6 r6 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.la r6 = r6.mo19013h()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r14 = com.google.android.gms.measurement.internal.C3135o.f5383A0     // Catch:{ Exception -> 0x01ac }
            boolean r6 = r6.mo19146a(r14)     // Catch:{ Exception -> 0x01ac }
            java.lang.String r14 = "_ldl"
            if (r6 == 0) goto L_0x0165
            if (r3 == 0) goto L_0x014f
            com.google.android.gms.measurement.internal.p6 r2 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            r2.mo19267a(r0, r12, r3)     // Catch:{ Exception -> 0x01ac }
            goto L_0x015e
        L_0x014f:
            com.google.android.gms.measurement.internal.p6 r0 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18995A()     // Catch:{ Exception -> 0x01ac }
            java.lang.String r3 = "Referrer does not contain valid parameters"
            r0.mo19043a(r3, r2)     // Catch:{ Exception -> 0x01ac }
        L_0x015e:
            com.google.android.gms.measurement.internal.p6 r0 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            r2 = 0
            r0.mo19271a(r9, r14, r2, r13)     // Catch:{ Exception -> 0x01ac }
            return
        L_0x0165:
            boolean r0 = r2.contains(r10)     // Catch:{ Exception -> 0x01ac }
            if (r0 == 0) goto L_0x0190
            boolean r0 = r2.contains(r8)     // Catch:{ Exception -> 0x01ac }
            if (r0 != 0) goto L_0x018f
            boolean r0 = r2.contains(r7)     // Catch:{ Exception -> 0x01ac }
            if (r0 != 0) goto L_0x018f
            boolean r0 = r2.contains(r5)     // Catch:{ Exception -> 0x01ac }
            if (r0 != 0) goto L_0x018f
            java.lang.String r0 = "utm_term"
            boolean r0 = r2.contains(r0)     // Catch:{ Exception -> 0x01ac }
            if (r0 != 0) goto L_0x018f
            java.lang.String r0 = "utm_content"
            boolean r0 = r2.contains(r0)     // Catch:{ Exception -> 0x01ac }
            if (r0 == 0) goto L_0x0190
        L_0x018f:
            r11 = 1
        L_0x0190:
            if (r11 != 0) goto L_0x01a0
            com.google.android.gms.measurement.internal.p6 r0 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.f4 r0 = r0.mo19015l()     // Catch:{ Exception -> 0x01ac }
            com.google.android.gms.measurement.internal.h4 r0 = r0.mo18995A()     // Catch:{ Exception -> 0x01ac }
            r0.mo19042a(r4)     // Catch:{ Exception -> 0x01ac }
            return
        L_0x01a0:
            boolean r0 = android.text.TextUtils.isEmpty(r21)     // Catch:{ Exception -> 0x01ac }
            if (r0 != 0) goto L_0x01ab
            com.google.android.gms.measurement.internal.p6 r0 = r1.f5181P     // Catch:{ Exception -> 0x01ac }
            r0.mo19271a(r9, r14, r2, r13)     // Catch:{ Exception -> 0x01ac }
        L_0x01ab:
            return
        L_0x01ac:
            r0 = move-exception
            com.google.android.gms.measurement.internal.p6 r2 = r1.f5181P
            com.google.android.gms.measurement.internal.f4 r2 = r2.mo19015l()
            com.google.android.gms.measurement.internal.h4 r2 = r2.mo19001t()
            java.lang.String r3 = "Throwable caught in handleReferrerForOnActivityCreated"
            r2.mo19043a(r3, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3059h7.m8719a(boolean, android.net.Uri, java.lang.String, java.lang.String):void");
    }

    public final void onActivityCreated(Activity activity, Bundle bundle) {
        try {
            this.f5181P.mo19015l().mo18996B().mo19042a("onActivityCreated");
            Intent intent = activity.getIntent();
            if (intent != null) {
                Uri data = intent.getData();
                if (data != null) {
                    if (data.isHierarchical()) {
                        this.f5181P.mo19011f();
                        String str = C3267z9.m9416a(intent) ? "gs" : "auto";
                        String queryParameter = data.getQueryParameter("referrer");
                        boolean z = bundle == null;
                        if (!C2457cb.m6156b() || !C3135o.f5387C0.mo19389a(null).booleanValue()) {
                            m8719a(z, data, str, queryParameter);
                        } else {
                            this.f5181P.mo19014j().mo19028a(new C3095k7(this, z, data, str, queryParameter));
                        }
                        this.f5181P.mo18887s().mo19298a(activity, bundle);
                        return;
                    }
                }
                this.f5181P.mo18887s().mo19298a(activity, bundle);
            }
        } catch (Exception e) {
            this.f5181P.mo19015l().mo19001t().mo19043a("Throwable caught in onActivityCreated", e);
        } finally {
            this.f5181P.mo18887s().mo19298a(activity, bundle);
        }
    }

    public final void onActivityDestroyed(Activity activity) {
        this.f5181P.mo18887s().mo19303c(activity);
    }

    @MainThread
    public final void onActivityPaused(Activity activity) {
        this.f5181P.mo18887s().mo19301b(activity);
        C3244x8 u = this.f5181P.mo18889u();
        u.mo19014j().mo19028a(new C3266z8(u, u.mo19017o().elapsedRealtime()));
    }

    @MainThread
    public final void onActivityResumed(Activity activity) {
        if (!C2607la.m6742b() || !C3135o.f5424V.mo19389a(null).booleanValue()) {
            this.f5181P.mo18887s().mo19297a(activity);
            this.f5181P.mo18889u().mo19396B();
            return;
        }
        this.f5181P.mo18889u().mo19396B();
        this.f5181P.mo18887s().mo19297a(activity);
    }

    public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        this.f5181P.mo18887s().mo19302b(activity, bundle);
    }

    public final void onActivityStarted(Activity activity) {
    }

    public final void onActivityStopped(Activity activity) {
    }

    /* synthetic */ C3059h7(C3154p6 p6Var, C3176r6 r6Var) {
        this(p6Var);
    }
}
