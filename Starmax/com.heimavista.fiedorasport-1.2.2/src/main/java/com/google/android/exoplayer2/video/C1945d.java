package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.d */
/* compiled from: lambda */
public final /* synthetic */ class C1945d implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2955P;

    /* renamed from: Q */
    private final /* synthetic */ String f2956Q;

    /* renamed from: R */
    private final /* synthetic */ long f2957R;

    /* renamed from: S */
    private final /* synthetic */ long f2958S;

    public /* synthetic */ C1945d(VideoRendererEventListener.EventDispatcher eventDispatcher, String str, long j, long j2) {
        this.f2955P = eventDispatcher;
        this.f2956Q = str;
        this.f2957R = j;
        this.f2958S = j2;
    }

    public final void run() {
        this.f2955P.mo16270a(this.f2956Q, this.f2957R, this.f2958S);
    }
}
