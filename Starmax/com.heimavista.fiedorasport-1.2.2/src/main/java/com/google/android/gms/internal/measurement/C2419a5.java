package com.google.android.gms.internal.measurement;

import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.a5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2419a5<K> implements Map.Entry<K, Object> {

    /* renamed from: P */
    private Map.Entry<K, C2800y4> f3975P;

    private C2419a5(Map.Entry<K, C2800y4> entry) {
        this.f3975P = entry;
    }

    public final K getKey() {
        return this.f3975P.getKey();
    }

    public final Object getValue() {
        if (this.f3975P.getValue() == null) {
            return null;
        }
        C2800y4.m7831c();
        throw null;
    }

    public final Object setValue(Object obj) {
        if (obj instanceof C2739u5) {
            return this.f3975P.getValue().mo17370a((C2739u5) obj);
        }
        throw new IllegalArgumentException("LazyField now only used for MessageSet, and the value of MessageSet must be an instance of MessageLite");
    }
}
