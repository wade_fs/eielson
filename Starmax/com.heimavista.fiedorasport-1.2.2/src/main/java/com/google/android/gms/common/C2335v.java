package com.google.android.gms.common;

import android.util.Log;
import androidx.annotation.NonNull;
import com.google.android.gms.common.util.C2310a;
import com.google.android.gms.common.util.C2319j;
import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.common.v */
class C2335v {

    /* renamed from: d */
    private static final C2335v f3870d = new C2335v(true, null, null);

    /* renamed from: a */
    final boolean f3871a;

    /* renamed from: b */
    private final String f3872b;

    /* renamed from: c */
    private final Throwable f3873c;

    C2335v(boolean z, String str, Throwable th) {
        this.f3871a = z;
        this.f3872b = str;
        this.f3873c = th;
    }

    /* renamed from: a */
    static C2335v m5828a(Callable<String> callable) {
        return new C2337x(callable);
    }

    /* renamed from: c */
    static C2335v m5830c() {
        return f3870d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final void mo17140b() {
        if (!this.f3871a && Log.isLoggable("GoogleCertificatesRslt", 3)) {
            if (this.f3873c != null) {
                Log.d("GoogleCertificatesRslt", mo17139a(), this.f3873c);
            } else {
                Log.d("GoogleCertificatesRslt", mo17139a());
            }
        }
    }

    /* renamed from: a */
    static C2335v m5826a(@NonNull String str) {
        return new C2335v(false, str, null);
    }

    /* renamed from: a */
    static C2335v m5827a(@NonNull String str, @NonNull Throwable th) {
        return new C2335v(false, str, th);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public String mo17139a() {
        return this.f3872b;
    }

    /* renamed from: a */
    static String m5829a(String str, C2288o oVar, boolean z, boolean z2) {
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", z2 ? "debug cert rejected" : "not whitelisted", str, C2319j.m5788a(C2310a.m5758a("SHA-1").digest(oVar.mo17067H())), Boolean.valueOf(z), "12451009.false");
    }
}
