package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.Scope;
import java.util.Comparator;

/* renamed from: com.google.android.gms.auth.api.signin.g */
final class C1984g implements Comparator<Scope> {
    C1984g() {
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        return ((Scope) obj).mo16507c().compareTo(((Scope) obj2).mo16507c());
    }
}
