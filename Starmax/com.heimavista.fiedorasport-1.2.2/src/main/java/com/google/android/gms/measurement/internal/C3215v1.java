package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2760vb;

/* renamed from: com.google.android.gms.measurement.internal.v1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3215v1 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5692a = new C3215v1();

    private C3215v1() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2760vb.m7454b());
    }
}
