package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.C1951a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.internal.C2056c;

/* renamed from: com.google.android.gms.auth.api.signin.internal.m */
abstract class C2000m<R extends C2157k> extends C2056c<R, C1994g> {
    public C2000m(C2036f fVar) {
        super(C1951a.f2989e, fVar);
    }
}
