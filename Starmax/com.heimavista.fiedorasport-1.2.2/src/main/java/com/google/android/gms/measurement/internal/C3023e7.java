package com.google.android.gms.measurement.internal;

import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.e7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3023e7 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5101P;

    /* renamed from: Q */
    private final /* synthetic */ C3154p6 f5102Q;

    C3023e7(C3154p6 p6Var, AtomicReference atomicReference) {
        this.f5102Q = p6Var;
        this.f5101P = atomicReference;
    }

    public final void run() {
        synchronized (this.f5101P) {
            try {
                this.f5101P.set(Double.valueOf(this.f5102Q.mo19013h().mo19149c(this.f5102Q.mo18885p().mo18800B(), C3135o.f5404L)));
                this.f5101P.notify();
            } catch (Throwable th) {
                this.f5101P.notify();
                throw th;
            }
        }
    }
}
