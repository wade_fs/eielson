package com.google.android.gms.measurement.internal;

import android.content.Intent;

/* renamed from: com.google.android.gms.measurement.internal.t8 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final /* synthetic */ class C3200t8 implements Runnable {

    /* renamed from: P */
    private final C3211u8 f5662P;

    /* renamed from: Q */
    private final int f5663Q;

    /* renamed from: R */
    private final C3032f4 f5664R;

    /* renamed from: S */
    private final Intent f5665S;

    C3200t8(C3211u8 u8Var, int i, C3032f4 f4Var, Intent intent) {
        this.f5662P = u8Var;
        this.f5663Q = i;
        this.f5664R = f4Var;
        this.f5665S = intent;
    }

    public final void run() {
        this.f5662P.mo19340a(this.f5663Q, this.f5664R, this.f5665S);
    }
}
