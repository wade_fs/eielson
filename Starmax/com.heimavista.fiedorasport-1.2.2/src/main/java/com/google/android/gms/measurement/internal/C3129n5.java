package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.n5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3129n5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f5371P;

    /* renamed from: Q */
    private final /* synthetic */ C3141o5 f5372Q;

    C3129n5(C3141o5 o5Var, zzm zzm) {
        this.f5372Q = o5Var;
        this.f5371P = zzm;
    }

    public final void run() {
        this.f5372Q.f5496a.mo19237q();
        C3145o9 a = this.f5372Q.f5496a;
        zzm zzm = this.f5371P;
        a.mo19014j().mo18881c();
        a.mo19235m();
        C2258v.m5639b(zzm.f5814P);
        a.mo19226c(zzm);
    }
}
