package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.common.internal.ResolveAccountResponse;
import com.google.android.gms.signin.internal.zaj;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.locks.Lock;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.z */
public final class C2153z implements C2132t0 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final C2136u0 f3509a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Lock f3510b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final Context f3511c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public final C2169c f3512d;

    /* renamed from: e */
    private ConnectionResult f3513e;

    /* renamed from: f */
    private int f3514f;

    /* renamed from: g */
    private int f3515g = 0;

    /* renamed from: h */
    private int f3516h;

    /* renamed from: i */
    private final Bundle f3517i = new Bundle();

    /* renamed from: j */
    private final Set<C2016a.C2019c> f3518j = new HashSet();
    /* access modifiers changed from: private */

    /* renamed from: k */
    public C4052e f3519k;

    /* renamed from: l */
    private boolean f3520l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public boolean f3521m;

    /* renamed from: n */
    private boolean f3522n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public C2231m f3523o;

    /* renamed from: p */
    private boolean f3524p;

    /* renamed from: q */
    private boolean f3525q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public final C2211e f3526r;

    /* renamed from: s */
    private final Map<C2016a<?>, Boolean> f3527s;

    /* renamed from: t */
    private final C2016a.C2017a<? extends C4052e, C4047a> f3528t;

    /* renamed from: u */
    private ArrayList<Future<?>> f3529u = new ArrayList<>();

    public C2153z(C2136u0 u0Var, C2211e eVar, Map<C2016a<?>, Boolean> map, C2169c cVar, C2016a.C2017a<? extends C4052e, C4047a> aVar, Lock lock, Context context) {
        this.f3509a = u0Var;
        this.f3526r = eVar;
        this.f3527s = map;
        this.f3512d = cVar;
        this.f3528t = aVar;
        this.f3510b = lock;
        this.f3511c = context;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m5191a(zaj zaj) {
        if (m5193a(0)) {
            ConnectionResult c = zaj.mo19492c();
            if (c.mo16482w()) {
                ResolveAccountResponse d = zaj.mo19493d();
                ConnectionResult d2 = d.mo16890d();
                if (!d2.mo16482w()) {
                    String valueOf = String.valueOf(d2);
                    StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 48);
                    sb.append("Sign-in succeeded with resolve account failure: ");
                    sb.append(valueOf);
                    Log.wtf("GoogleApiClientConnecting", sb.toString(), new Exception());
                    m5198b(d2);
                    return;
                }
                this.f3522n = true;
                this.f3523o = d.mo16889c();
                this.f3524p = d.mo16892u();
                this.f3525q = d.mo16893v();
                m5204e();
            } else if (m5194a(c)) {
                m5209g();
                m5204e();
            } else {
                m5198b(c);
            }
        }
    }

    /* renamed from: b */
    private static String m5197b(int i) {
        return i != 0 ? i != 1 ? "UNKNOWN" : "STEP_GETTING_REMOTE_SERVICE" : "STEP_SERVICE_BINDINGS_AND_SIGN_IN";
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        if (r7 != false) goto L_0x0024;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m5199b(com.google.android.gms.common.ConnectionResult r5, com.google.android.gms.common.api.C2016a<?> r6, boolean r7) {
        /*
            r4 = this;
            com.google.android.gms.common.api.a$e r0 = r6.mo16523c()
            int r0 = r0.mo16527a()
            r1 = 0
            r2 = 1
            if (r7 == 0) goto L_0x0024
            boolean r7 = r5.mo16481v()
            if (r7 == 0) goto L_0x0014
        L_0x0012:
            r7 = 1
            goto L_0x0022
        L_0x0014:
            com.google.android.gms.common.c r7 = r4.f3512d
            int r3 = r5.mo16475c()
            android.content.Intent r7 = r7.mo16837a(r3)
            if (r7 == 0) goto L_0x0021
            goto L_0x0012
        L_0x0021:
            r7 = 0
        L_0x0022:
            if (r7 == 0) goto L_0x002d
        L_0x0024:
            com.google.android.gms.common.ConnectionResult r7 = r4.f3513e
            if (r7 == 0) goto L_0x002c
            int r7 = r4.f3514f
            if (r0 >= r7) goto L_0x002d
        L_0x002c:
            r1 = 1
        L_0x002d:
            if (r1 == 0) goto L_0x0033
            r4.f3513e = r5
            r4.f3514f = r0
        L_0x0033:
            com.google.android.gms.common.api.internal.u0 r7 = r4.f3509a
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.ConnectionResult> r7 = r7.f3475g
            com.google.android.gms.common.api.a$c r6 = r6.mo16521a()
            r7.put(r6, r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.C2153z.m5199b(com.google.android.gms.common.ConnectionResult, com.google.android.gms.common.api.a, boolean):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final boolean m5203d() {
        this.f3516h--;
        int i = this.f3516h;
        if (i > 0) {
            return false;
        }
        if (i < 0) {
            Log.w("GoogleApiClientConnecting", this.f3509a.f3482n.mo16752n());
            Log.wtf("GoogleApiClientConnecting", "GoogleApiClient received too many callbacks for the given step. Clients may be in an unexpected state; GoogleApiClient will now disconnect.", new Exception());
            m5198b(new ConnectionResult(8, null));
            return false;
        }
        ConnectionResult connectionResult = this.f3513e;
        if (connectionResult == null) {
            return true;
        }
        this.f3509a.f3481m = this.f3514f;
        m5198b(connectionResult);
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public final void m5204e() {
        if (this.f3516h == 0) {
            if (!this.f3521m || this.f3522n) {
                ArrayList arrayList = new ArrayList();
                this.f3515g = 1;
                this.f3516h = this.f3509a.f3474f.size();
                for (C2016a.C2019c cVar : this.f3509a.f3474f.keySet()) {
                    if (!this.f3509a.f3475g.containsKey(cVar)) {
                        arrayList.add(this.f3509a.f3474f.get(cVar));
                    } else if (m5203d()) {
                        m5207f();
                    }
                }
                if (!arrayList.isEmpty()) {
                    this.f3529u.add(C2148x0.m5181a().submit(new C2073f0(this, arrayList)));
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public final void m5209g() {
        this.f3521m = false;
        this.f3509a.f3482n.f3374q = Collections.emptySet();
        for (C2016a.C2019c cVar : this.f3518j) {
            if (!this.f3509a.f3475g.containsKey(cVar)) {
                this.f3509a.f3475g.put(cVar, new ConnectionResult(17, null));
            }
        }
    }

    /* renamed from: h */
    private final void m5211h() {
        ArrayList<Future<?>> arrayList = this.f3529u;
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            Future<?> future = arrayList.get(i);
            i++;
            future.cancel(true);
        }
        this.f3529u.clear();
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final Set<Scope> m5213i() {
        C2211e eVar = this.f3526r;
        if (eVar == null) {
            return Collections.emptySet();
        }
        HashSet hashSet = new HashSet(eVar.mo16966i());
        Map<C2016a<?>, C2211e.C2213b> f = this.f3526r.mo16963f();
        for (C2016a aVar : f.keySet()) {
            if (!this.f3509a.f3475g.containsKey(aVar.mo16521a())) {
                hashSet.addAll(f.get(aVar).f3707a);
            }
        }
        return hashSet;
    }

    /* renamed from: L */
    public final void mo16739L(int i) {
        m5198b(new ConnectionResult(8, null));
    }

    /* renamed from: b */
    public final void mo16743b() {
    }

    /* renamed from: c */
    public final void mo16744c() {
        this.f3509a.f3475g.clear();
        this.f3521m = false;
        this.f3513e = null;
        this.f3515g = 0;
        this.f3520l = true;
        this.f3522n = false;
        this.f3524p = false;
        HashMap hashMap = new HashMap();
        boolean z = false;
        for (C2016a aVar : this.f3527s.keySet()) {
            C2016a.C2027f fVar = this.f3509a.f3474f.get(aVar.mo16521a());
            z |= aVar.mo16523c().mo16527a() == 1;
            boolean booleanValue = this.f3527s.get(aVar).booleanValue();
            if (fVar.mo16538l()) {
                this.f3521m = true;
                if (booleanValue) {
                    this.f3518j.add(aVar.mo16521a());
                } else {
                    this.f3520l = false;
                }
            }
            hashMap.put(fVar, new C2053b0(this, aVar, booleanValue));
        }
        if (z) {
            this.f3521m = false;
        }
        if (this.f3521m) {
            this.f3526r.mo16958a(Integer.valueOf(System.identityHashCode(this.f3509a.f3482n)));
            C2088i0 i0Var = new C2088i0(this, null);
            C2016a.C2017a<? extends C4052e, C4047a> aVar2 = this.f3528t;
            Context context = this.f3511c;
            Looper f = this.f3509a.f3482n.mo16574f();
            C2211e eVar = this.f3526r;
            this.f3519k = (C4052e) aVar2.mo16371a(context, f, eVar, eVar.mo16967j(), i0Var, i0Var);
        }
        this.f3516h = this.f3509a.f3474f.size();
        this.f3529u.add(C2148x0.m5181a().submit(new C2057c0(this, hashMap)));
    }

    /* renamed from: f */
    public final void mo16745f(Bundle bundle) {
        if (m5193a(1)) {
            if (bundle != null) {
                this.f3517i.putAll(bundle);
            }
            if (m5203d()) {
                m5207f();
            }
        }
    }

    /* renamed from: f */
    private final void m5207f() {
        this.f3509a.mo16787i();
        C2148x0.m5181a().execute(new C2048a0(this));
        C4052e eVar = this.f3519k;
        if (eVar != null) {
            if (this.f3524p) {
                eVar.mo19473a(this.f3523o, this.f3525q);
            }
            m5192a(false);
        }
        for (C2016a.C2019c<?> cVar : this.f3509a.f3475g.keySet()) {
            this.f3509a.f3474f.get(cVar).mo16528a();
        }
        this.f3509a.f3483o.mo16729a(this.f3517i.isEmpty() ? null : this.f3517i);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m5198b(ConnectionResult connectionResult) {
        m5211h();
        m5192a(!connectionResult.mo16481v());
        this.f3509a.mo16782a(connectionResult);
        this.f3509a.f3483o.mo16730a(connectionResult);
    }

    /* renamed from: a */
    public final void mo16741a(ConnectionResult connectionResult, C2016a<?> aVar, boolean z) {
        if (m5193a(1)) {
            m5199b(connectionResult, aVar, z);
            if (m5203d()) {
                m5207f();
            }
        }
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16740a(C2056c cVar) {
        throw new IllegalStateException("GoogleApiClient is not connected yet.");
    }

    /* renamed from: a */
    public final boolean mo16742a() {
        m5211h();
        m5192a(true);
        this.f3509a.mo16782a((ConnectionResult) null);
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final boolean m5194a(ConnectionResult connectionResult) {
        return this.f3520l && !connectionResult.mo16481v();
    }

    /* renamed from: a */
    private final void m5192a(boolean z) {
        C4052e eVar = this.f3519k;
        if (eVar != null) {
            if (eVar.mo16533c() && z) {
                this.f3519k.mo19476g();
            }
            this.f3519k.mo16528a();
            if (this.f3526r.mo16968k()) {
                this.f3519k = null;
            }
            this.f3523o = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final boolean m5193a(int i) {
        if (this.f3515g == i) {
            return true;
        }
        Log.w("GoogleApiClientConnecting", this.f3509a.f3482n.mo16752n());
        String valueOf = String.valueOf(this);
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 23);
        sb.append("Unexpected callback in ");
        sb.append(valueOf);
        Log.w("GoogleApiClientConnecting", sb.toString());
        int i2 = this.f3516h;
        StringBuilder sb2 = new StringBuilder(33);
        sb2.append("mRemainingConnections=");
        sb2.append(i2);
        Log.w("GoogleApiClientConnecting", sb2.toString());
        String b = m5197b(this.f3515g);
        String b2 = m5197b(i);
        StringBuilder sb3 = new StringBuilder(String.valueOf(b).length() + 70 + String.valueOf(b2).length());
        sb3.append("GoogleApiClient connecting is in step ");
        sb3.append(b);
        sb3.append(" but received callback for step ");
        sb3.append(b2);
        Log.wtf("GoogleApiClientConnecting", sb3.toString(), new Exception());
        m5198b(new ConnectionResult(8, null));
        return false;
    }
}
