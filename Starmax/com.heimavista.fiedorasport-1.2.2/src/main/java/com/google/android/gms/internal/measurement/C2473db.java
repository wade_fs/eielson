package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.db */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2473db implements C2593l2<C2523gb> {

    /* renamed from: Q */
    private static C2473db f4054Q = new C2473db();

    /* renamed from: P */
    private final C2593l2<C2523gb> f4055P;

    private C2473db(C2593l2<C2523gb> l2Var) {
        this.f4055P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6224b() {
        return ((C2523gb) f4054Q.mo17285a()).mo17483a();
    }

    /* renamed from: c */
    public static boolean m6225c() {
        return ((C2523gb) f4054Q.mo17285a()).mo17484e();
    }

    /* renamed from: d */
    public static boolean m6226d() {
        return ((C2523gb) f4054Q.mo17285a()).mo17485f();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4055P.mo17285a();
    }

    public C2473db() {
        this(C2579k2.m6601a(new C2506fb()));
    }
}
