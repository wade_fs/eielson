package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.s3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2705s3 extends C2690r3 {

    /* renamed from: a */
    private int f4475a;

    /* renamed from: b */
    private int f4476b;

    /* renamed from: c */
    private int f4477c;

    /* renamed from: d */
    private int f4478d;

    /* renamed from: e */
    private int f4479e;

    private C2705s3(byte[] bArr, int i, int i2, boolean z) {
        super();
        this.f4479e = Integer.MAX_VALUE;
        this.f4475a = i2 + i;
        this.f4477c = i;
        this.f4478d = this.f4477c;
    }

    /* renamed from: a */
    public final int mo17873a() {
        return this.f4477c - this.f4478d;
    }

    /* renamed from: b */
    public final int mo17874b(int i) {
        if (i >= 0) {
            int a = i + mo17873a();
            int i2 = this.f4479e;
            if (a <= i2) {
                this.f4479e = a;
                m7190b();
                return i2;
            }
            throw C2723t4.m7311a();
        }
        throw C2723t4.m7312e();
    }

    /* renamed from: b */
    private final void m7190b() {
        this.f4475a += this.f4476b;
        int i = this.f4475a;
        int i2 = i - this.f4478d;
        int i3 = this.f4479e;
        if (i2 > i3) {
            this.f4476b = i2 - i3;
            this.f4475a = i - this.f4476b;
            return;
        }
        this.f4476b = 0;
    }
}
