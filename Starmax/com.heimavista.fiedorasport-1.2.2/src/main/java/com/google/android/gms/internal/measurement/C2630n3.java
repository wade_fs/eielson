package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.n3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2630n3 {

    /* renamed from: a */
    private final C2720t3 f4348a;

    /* renamed from: b */
    private final byte[] f4349b;

    private C2630n3(int i) {
        this.f4349b = new byte[i];
        this.f4348a = C2720t3.m7219a(this.f4349b);
    }

    /* renamed from: a */
    public final C2498f3 mo17776a() {
        this.f4348a.mo17900b();
        return new C2660p3(this.f4349b);
    }

    /* renamed from: b */
    public final C2720t3 mo17777b() {
        return this.f4348a;
    }

    /* synthetic */ C2630n3(int i, C2482e3 e3Var) {
        this(i);
    }
}
