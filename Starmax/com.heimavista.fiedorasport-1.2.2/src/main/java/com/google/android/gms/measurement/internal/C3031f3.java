package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2655oc;

/* renamed from: com.google.android.gms.measurement.internal.f3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3031f3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5121a = new C3031f3();

    private C3031f3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2655oc.m7019c());
    }
}
