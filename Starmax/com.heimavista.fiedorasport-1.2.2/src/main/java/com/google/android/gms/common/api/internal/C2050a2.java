package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2056c;
import com.google.android.gms.common.api.internal.C2064e;

/* renamed from: com.google.android.gms.common.api.internal.a2 */
public final class C2050a2<A extends C2056c<? extends C2157k, C2016a.C2018b>> extends C2116p0 {

    /* renamed from: a */
    private final A f3242a;

    public C2050a2(int i, A a) {
        super(i);
        this.f3242a = a;
    }

    /* renamed from: a */
    public final void mo16615a(C2064e.C2065a<?> aVar) {
        try {
            this.f3242a.mo16638b(aVar.mo16674f());
        } catch (RuntimeException e) {
            mo16617a(e);
        }
    }

    /* renamed from: a */
    public final void mo16614a(@NonNull Status status) {
        this.f3242a.mo16640c(status);
    }

    /* renamed from: a */
    public final void mo16617a(@NonNull RuntimeException runtimeException) {
        String simpleName = runtimeException.getClass().getSimpleName();
        String localizedMessage = runtimeException.getLocalizedMessage();
        StringBuilder sb = new StringBuilder(String.valueOf(simpleName).length() + 2 + String.valueOf(localizedMessage).length());
        sb.append(simpleName);
        sb.append(": ");
        sb.append(localizedMessage);
        this.f3242a.mo16640c(new Status(10, sb.toString()));
    }

    /* renamed from: a */
    public final void mo16616a(@NonNull C2123r rVar, boolean z) {
        rVar.mo16768a(this.f3242a, z);
    }
}
