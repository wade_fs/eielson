package com.google.android.gms.auth.api.signin;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.b */
public interface C1977b {
    /* renamed from: a */
    C2040g<Status> mo16414a(C2036f fVar);
}
