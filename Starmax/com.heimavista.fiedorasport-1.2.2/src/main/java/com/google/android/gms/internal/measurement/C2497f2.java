package com.google.android.gms.internal.measurement;

import java.io.Serializable;

/* renamed from: com.google.android.gms.internal.measurement.f2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public abstract class C2497f2<T> implements Serializable {
    C2497f2() {
    }

    /* renamed from: a */
    public static <T> C2497f2<T> m6297a(T t) {
        C2546i2.m6469a(t);
        return new C2531h2(t);
    }

    /* renamed from: f */
    public static <T> C2497f2<T> m6298f() {
        return C2464d2.f4047P;
    }

    /* renamed from: a */
    public abstract boolean mo17397a();

    /* renamed from: e */
    public abstract T mo17398e();
}
