package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.o0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2641o0 extends C2595l4<C2641o0, C2642a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2641o0 zzf;
    private static volatile C2501f6<C2641o0> zzg;
    private int zzc;
    private String zzd = "";
    private String zze = "";

    /* renamed from: com.google.android.gms.internal.measurement.o0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2642a extends C2595l4.C2596a<C2641o0, C2642a> implements C2769w5 {
        private C2642a() {
            super(C2641o0.zzf);
        }

        /* synthetic */ C2642a(C2591l0 l0Var) {
            this();
        }
    }

    static {
        C2641o0 o0Var = new C2641o0();
        zzf = o0Var;
        C2595l4.m6645a(C2641o0.class, super);
    }

    private C2641o0() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2591l0.f4286a[i - 1]) {
            case 1:
                return new C2641o0();
            case 2:
                return new C2642a(null);
            case 3:
                return C2595l4.m6643a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\b\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                C2501f6<C2641o0> f6Var = zzg;
                if (f6Var == null) {
                    synchronized (C2641o0.class) {
                        f6Var = zzg;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzf);
                            zzg = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /* renamed from: n */
    public final String mo17785n() {
        return this.zzd;
    }

    /* renamed from: o */
    public final String mo17786o() {
        return this.zze;
    }
}
