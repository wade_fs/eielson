package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.g8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3048g8 extends C3039g {

    /* renamed from: e */
    private final /* synthetic */ C3232w7 f5162e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3048g8(C3232w7 w7Var, C3058h6 h6Var) {
        super(h6Var);
        this.f5162e = w7Var;
    }

    /* renamed from: a */
    public final void mo19022a() {
        this.f5162e.mo19015l().mo19004w().mo19042a("Tasks have been queued for a long time");
    }
}
