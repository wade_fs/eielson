package com.google.android.gms.common.internal.safeparcel;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.core.internal.view.SupportMenu;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.google.android.gms.common.internal.safeparcel.a */
public class C2248a {

    /* renamed from: com.google.android.gms.common.internal.safeparcel.a$a */
    public static class C2249a extends RuntimeException {
        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public C2249a(java.lang.String r4, android.os.Parcel r5) {
            /*
                r3 = this;
                int r0 = r5.dataPosition()
                int r5 = r5.dataSize()
                java.lang.String r1 = java.lang.String.valueOf(r4)
                int r1 = r1.length()
                int r1 = r1 + 41
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>(r1)
                r2.append(r4)
                java.lang.String r4 = " Parcel: pos="
                r2.append(r4)
                r2.append(r0)
                java.lang.String r4 = " size="
                r2.append(r4)
                r2.append(r5)
                java.lang.String r4 = r2.toString()
                r3.<init>(r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.safeparcel.C2248a.C2249a.<init>(java.lang.String, android.os.Parcel):void");
        }
    }

    /* renamed from: A */
    public static Integer m5545A(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        if (E == 0) {
            return null;
        }
        m5556a(parcel, i, E, 4);
        return Integer.valueOf(parcel.readInt());
    }

    /* renamed from: B */
    public static long m5546B(Parcel parcel, int i) {
        m5555a(parcel, i, 8);
        return parcel.readLong();
    }

    /* renamed from: C */
    public static Long m5547C(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        if (E == 0) {
            return null;
        }
        m5556a(parcel, i, E, 8);
        return Long.valueOf(parcel.readLong());
    }

    /* renamed from: D */
    public static short m5548D(Parcel parcel, int i) {
        m5555a(parcel, i, 4);
        return (short) parcel.readInt();
    }

    /* renamed from: E */
    public static int m5549E(Parcel parcel, int i) {
        return (i & SupportMenu.CATEGORY_MASK) != -65536 ? (i >> 16) & 65535 : parcel.readInt();
    }

    /* renamed from: F */
    public static void m5550F(Parcel parcel, int i) {
        parcel.setDataPosition(parcel.dataPosition() + m5549E(parcel, i));
    }

    /* renamed from: a */
    public static int m5551a(int i) {
        return i & 65535;
    }

    /* renamed from: a */
    public static int m5552a(Parcel parcel) {
        return parcel.readInt();
    }

    /* renamed from: b */
    public static int m5558b(Parcel parcel) {
        int a = m5552a(parcel);
        int E = m5549E(parcel, a);
        int dataPosition = parcel.dataPosition();
        if (m5551a(a) != 20293) {
            String valueOf = String.valueOf(Integer.toHexString(a));
            throw new C2249a(valueOf.length() != 0 ? "Expected object header. Got 0x".concat(valueOf) : new String("Expected object header. Got 0x"), parcel);
        }
        int i = E + dataPosition;
        if (i >= dataPosition && i <= parcel.dataSize()) {
            return i;
        }
        StringBuilder sb = new StringBuilder(54);
        sb.append("Size read is invalid start=");
        sb.append(dataPosition);
        sb.append(" end=");
        sb.append(i);
        throw new C2249a(sb.toString(), parcel);
    }

    /* renamed from: c */
    public static BigInteger m5561c(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + E);
        return new BigInteger(createByteArray);
    }

    /* renamed from: d */
    public static BigInteger[] m5563d(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        BigInteger[] bigIntegerArr = new BigInteger[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            bigIntegerArr[i2] = new BigInteger(parcel.createByteArray());
        }
        parcel.setDataPosition(dataPosition + E);
        return bigIntegerArr;
    }

    /* renamed from: e */
    public static boolean[] m5564e(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        boolean[] createBooleanArray = parcel.createBooleanArray();
        parcel.setDataPosition(dataPosition + E);
        return createBooleanArray;
    }

    /* renamed from: f */
    public static Bundle m5565f(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        Bundle readBundle = parcel.readBundle();
        parcel.setDataPosition(dataPosition + E);
        return readBundle;
    }

    /* renamed from: g */
    public static byte[] m5566g(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        parcel.setDataPosition(dataPosition + E);
        return createByteArray;
    }

    /* renamed from: h */
    public static double[] m5567h(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        double[] createDoubleArray = parcel.createDoubleArray();
        parcel.setDataPosition(dataPosition + E);
        return createDoubleArray;
    }

    /* renamed from: i */
    public static float[] m5568i(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        float[] createFloatArray = parcel.createFloatArray();
        parcel.setDataPosition(dataPosition + E);
        return createFloatArray;
    }

    /* renamed from: j */
    public static int[] m5569j(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        int[] createIntArray = parcel.createIntArray();
        parcel.setDataPosition(dataPosition + E);
        return createIntArray;
    }

    /* renamed from: k */
    public static long[] m5570k(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        long[] createLongArray = parcel.createLongArray();
        parcel.setDataPosition(dataPosition + E);
        return createLongArray;
    }

    /* renamed from: l */
    public static Parcel m5571l(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        obtain.appendFrom(parcel, dataPosition, E);
        parcel.setDataPosition(dataPosition + E);
        return obtain;
    }

    /* renamed from: m */
    public static Parcel[] m5572m(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        Parcel[] parcelArr = new Parcel[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            int readInt2 = parcel.readInt();
            if (readInt2 != 0) {
                int dataPosition2 = parcel.dataPosition();
                Parcel obtain = Parcel.obtain();
                obtain.appendFrom(parcel, dataPosition2, readInt2);
                parcelArr[i2] = obtain;
                parcel.setDataPosition(dataPosition2 + readInt2);
            } else {
                parcelArr[i2] = null;
            }
        }
        parcel.setDataPosition(dataPosition + E);
        return parcelArr;
    }

    /* renamed from: n */
    public static String m5573n(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        String readString = parcel.readString();
        parcel.setDataPosition(dataPosition + E);
        return readString;
    }

    /* renamed from: o */
    public static String[] m5574o(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        String[] createStringArray = parcel.createStringArray();
        parcel.setDataPosition(dataPosition + E);
        return createStringArray;
    }

    /* renamed from: p */
    public static ArrayList<String> m5575p(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        ArrayList<String> createStringArrayList = parcel.createStringArrayList();
        parcel.setDataPosition(dataPosition + E);
        return createStringArrayList;
    }

    /* renamed from: q */
    public static void m5576q(Parcel parcel, int i) {
        if (parcel.dataPosition() != i) {
            StringBuilder sb = new StringBuilder(37);
            sb.append("Overread allowed size end=");
            sb.append(i);
            throw new C2249a(sb.toString(), parcel);
        }
    }

    /* renamed from: r */
    public static boolean m5577r(Parcel parcel, int i) {
        m5555a(parcel, i, 4);
        return parcel.readInt() != 0;
    }

    /* renamed from: s */
    public static Boolean m5578s(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        if (E == 0) {
            return null;
        }
        m5556a(parcel, i, E, 4);
        return Boolean.valueOf(parcel.readInt() != 0);
    }

    /* renamed from: t */
    public static byte m5579t(Parcel parcel, int i) {
        m5555a(parcel, i, 4);
        return (byte) parcel.readInt();
    }

    /* renamed from: u */
    public static double m5580u(Parcel parcel, int i) {
        m5555a(parcel, i, 8);
        return parcel.readDouble();
    }

    /* renamed from: v */
    public static Double m5581v(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        if (E == 0) {
            return null;
        }
        m5556a(parcel, i, E, 8);
        return Double.valueOf(parcel.readDouble());
    }

    /* renamed from: w */
    public static float m5582w(Parcel parcel, int i) {
        m5555a(parcel, i, 4);
        return parcel.readFloat();
    }

    /* renamed from: x */
    public static Float m5583x(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        if (E == 0) {
            return null;
        }
        m5556a(parcel, i, E, 4);
        return Float.valueOf(parcel.readFloat());
    }

    /* renamed from: y */
    public static IBinder m5584y(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        IBinder readStrongBinder = parcel.readStrongBinder();
        parcel.setDataPosition(dataPosition + E);
        return readStrongBinder;
    }

    /* renamed from: z */
    public static int m5585z(Parcel parcel, int i) {
        m5555a(parcel, i, 4);
        return parcel.readInt();
    }

    /* renamed from: a */
    private static void m5555a(Parcel parcel, int i, int i2) {
        int E = m5549E(parcel, i);
        if (E != i2) {
            String hexString = Integer.toHexString(E);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i2);
            sb.append(" got ");
            sb.append(E);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new C2249a(sb.toString(), parcel);
        }
    }

    /* renamed from: a */
    private static void m5556a(Parcel parcel, int i, int i2, int i3) {
        if (i2 != i3) {
            String hexString = Integer.toHexString(i2);
            StringBuilder sb = new StringBuilder(String.valueOf(hexString).length() + 46);
            sb.append("Expected size ");
            sb.append(i3);
            sb.append(" got ");
            sb.append(i2);
            sb.append(" (0x");
            sb.append(hexString);
            sb.append(")");
            throw new C2249a(sb.toString(), parcel);
        }
    }

    /* renamed from: c */
    public static <T> ArrayList<T> m5562c(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        ArrayList<T> createTypedArrayList = parcel.createTypedArrayList(creator);
        parcel.setDataPosition(dataPosition + E);
        return createTypedArrayList;
    }

    /* renamed from: a */
    public static BigDecimal m5554a(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        byte[] createByteArray = parcel.createByteArray();
        int readInt = parcel.readInt();
        parcel.setDataPosition(dataPosition + E);
        return new BigDecimal(new BigInteger(createByteArray), readInt);
    }

    /* renamed from: b */
    public static BigDecimal[] m5560b(Parcel parcel, int i) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        int readInt = parcel.readInt();
        BigDecimal[] bigDecimalArr = new BigDecimal[readInt];
        for (int i2 = 0; i2 < readInt; i2++) {
            byte[] createByteArray = parcel.createByteArray();
            bigDecimalArr[i2] = new BigDecimal(new BigInteger(createByteArray), parcel.readInt());
        }
        parcel.setDataPosition(dataPosition + E);
        return bigDecimalArr;
    }

    /* renamed from: a */
    public static <T extends Parcelable> T m5553a(Parcel parcel, int i, Parcelable.Creator creator) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        T t = (Parcelable) creator.createFromParcel(parcel);
        parcel.setDataPosition(dataPosition + E);
        return t;
    }

    /* renamed from: b */
    public static <T> T[] m5559b(Parcel parcel, int i, Parcelable.Creator<T> creator) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E == 0) {
            return null;
        }
        T[] createTypedArray = parcel.createTypedArray(creator);
        parcel.setDataPosition(dataPosition + E);
        return createTypedArray;
    }

    /* renamed from: a */
    public static void m5557a(Parcel parcel, int i, List list, ClassLoader classLoader) {
        int E = m5549E(parcel, i);
        int dataPosition = parcel.dataPosition();
        if (E != 0) {
            parcel.readList(list, classLoader);
            parcel.setDataPosition(dataPosition + E);
        }
    }
}
