package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.p4 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3152p4 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ boolean f5541P;

    /* renamed from: Q */
    private final /* synthetic */ C3163q4 f5542Q;

    C3152p4(C3163q4 q4Var, boolean z) {
        this.f5542Q = q4Var;
        this.f5541P = z;
    }

    public final void run() {
        this.f5542Q.f5561a.mo19219a(this.f5541P);
    }
}
