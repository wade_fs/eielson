package com.google.android.gms.signin.internal;

import android.os.IInterface;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.signin.internal.d */
public interface C3271d extends IInterface {
    /* renamed from: a */
    void mo19479a(ConnectionResult connectionResult, zaa zaa);

    /* renamed from: a */
    void mo19480a(Status status, GoogleSignInAccount googleSignInAccount);

    /* renamed from: a */
    void mo16699a(zaj zaj);

    /* renamed from: c */
    void mo19481c(Status status);

    /* renamed from: d */
    void mo19482d(Status status);
}
