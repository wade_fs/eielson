package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.measurement.g4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2516g4 extends C2412a implements C2561j2 {
    C2516g4(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    /* renamed from: d */
    public final Bundle mo17498d(Bundle bundle) {
        Parcel H = mo17243H();
        C2670q.m7046a(H, bundle);
        Parcel a = mo17244a(1, H);
        Bundle bundle2 = (Bundle) C2670q.m7044a(a, Bundle.CREATOR);
        a.recycle();
        return bundle2;
    }
}
