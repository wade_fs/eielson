package com.google.android.gms.internal.auth;

import android.os.IBinder;

/* renamed from: com.google.android.gms.internal.auth.h */
public final class C2372h extends C2365a implements C2371g {
    C2372h(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.auth.api.internal.IAuthService");
    }
}
