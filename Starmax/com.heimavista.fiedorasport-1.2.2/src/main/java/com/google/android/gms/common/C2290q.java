package com.google.android.gms.common;

import java.lang.ref.WeakReference;

/* renamed from: com.google.android.gms.common.q */
abstract class C2290q extends C2288o {

    /* renamed from: c */
    private static final WeakReference<byte[]> f3785c = new WeakReference<>(null);

    /* renamed from: b */
    private WeakReference<byte[]> f3786b = f3785c;

    C2290q(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: H */
    public final byte[] mo17067H() {
        byte[] bArr;
        synchronized (this) {
            bArr = this.f3786b.get();
            if (bArr == null) {
                bArr = mo17070I();
                this.f3786b = new WeakReference<>(bArr);
            }
        }
        return bArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: I */
    public abstract byte[] mo17070I();
}
