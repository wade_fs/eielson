package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.h4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2533h4 extends C2813z2<Float> implements C2738u4<Float>, C2485e6, RandomAccess {

    /* renamed from: Q */
    private float[] f4209Q;

    /* renamed from: R */
    private int f4210R;

    static {
        new C2533h4(new float[0], 0).mo17939e();
    }

    C2533h4() {
        this(new float[10], 0);
    }

    /* renamed from: b */
    private final void m6442b(int i) {
        if (i < 0 || i >= this.f4210R) {
            throw new IndexOutOfBoundsException(m6443c(i));
        }
    }

    /* renamed from: c */
    private final String m6443c(int i) {
        int i2 = this.f4210R;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: a */
    public final void mo17542a(float f) {
        mo18179b();
        int i = this.f4210R;
        float[] fArr = this.f4209Q;
        if (i == fArr.length) {
            float[] fArr2 = new float[(((i * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            this.f4209Q = fArr2;
        }
        float[] fArr3 = this.f4209Q;
        int i2 = this.f4210R;
        this.f4210R = i2 + 1;
        fArr3[i2] = f;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        float floatValue = ((Float) obj).floatValue();
        mo18179b();
        if (i < 0 || i > (i2 = this.f4210R)) {
            throw new IndexOutOfBoundsException(m6443c(i));
        }
        float[] fArr = this.f4209Q;
        if (i2 < fArr.length) {
            System.arraycopy(fArr, i, fArr, i + 1, i2 - i);
        } else {
            float[] fArr2 = new float[(((i2 * 3) / 2) + 1)];
            System.arraycopy(fArr, 0, fArr2, 0, i);
            System.arraycopy(this.f4209Q, i, fArr2, i + 1, this.f4210R - i);
            this.f4209Q = fArr2;
        }
        this.f4209Q[i] = floatValue;
        this.f4210R++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Float> collection) {
        mo18179b();
        C2647o4.m6961a(collection);
        if (!(collection instanceof C2533h4)) {
            return super.addAll(collection);
        }
        C2533h4 h4Var = (C2533h4) collection;
        int i = h4Var.f4210R;
        if (i == 0) {
            return false;
        }
        int i2 = this.f4210R;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            float[] fArr = this.f4209Q;
            if (i3 > fArr.length) {
                this.f4209Q = Arrays.copyOf(fArr, i3);
            }
            System.arraycopy(h4Var.f4209Q, 0, this.f4209Q, this.f4210R, h4Var.f4210R);
            this.f4210R = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2533h4)) {
            return super.equals(obj);
        }
        C2533h4 h4Var = (C2533h4) obj;
        if (this.f4210R != h4Var.f4210R) {
            return false;
        }
        float[] fArr = h4Var.f4209Q;
        for (int i = 0; i < this.f4210R; i++) {
            if (Float.floatToIntBits(this.f4209Q[i]) != Float.floatToIntBits(fArr[i])) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        m6442b(i);
        return Float.valueOf(this.f4209Q[i]);
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.f4210R; i2++) {
            i = (i * 31) + Float.floatToIntBits(this.f4209Q[i2]);
        }
        return i;
    }

    public final boolean remove(Object obj) {
        mo18179b();
        for (int i = 0; i < this.f4210R; i++) {
            if (obj.equals(Float.valueOf(this.f4209Q[i]))) {
                float[] fArr = this.f4209Q;
                System.arraycopy(fArr, i + 1, fArr, i, (this.f4210R - i) - 1);
                this.f4210R--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        mo18179b();
        if (i2 >= i) {
            float[] fArr = this.f4209Q;
            System.arraycopy(fArr, i2, fArr, i, this.f4210R - i2);
            this.f4210R -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        float floatValue = ((Float) obj).floatValue();
        mo18179b();
        m6442b(i);
        float[] fArr = this.f4209Q;
        float f = fArr[i];
        fArr[i] = floatValue;
        return Float.valueOf(f);
    }

    public final int size() {
        return this.f4210R;
    }

    private C2533h4(float[] fArr, int i) {
        this.f4209Q = fArr;
        this.f4210R = i;
    }

    public final /* synthetic */ Object remove(int i) {
        mo18179b();
        m6442b(i);
        float[] fArr = this.f4209Q;
        float f = fArr[i];
        int i2 = this.f4210R;
        if (i < i2 - 1) {
            System.arraycopy(fArr, i + 1, fArr, i, (i2 - i) - 1);
        }
        this.f4210R--;
        this.modCount++;
        return Float.valueOf(f);
    }

    /* renamed from: a */
    public final /* synthetic */ C2738u4 mo17320a(int i) {
        if (i >= this.f4210R) {
            return new C2533h4(Arrays.copyOf(this.f4209Q, i), this.f4210R);
        }
        throw new IllegalArgumentException();
    }

    public final /* synthetic */ boolean add(Object obj) {
        mo17542a(((Float) obj).floatValue());
        return true;
    }
}
