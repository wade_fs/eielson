package com.google.android.gms.location;

import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2191b;
import com.google.android.gms.internal.location.C2390e;
import com.google.android.gms.internal.location.zzad;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.location.d0 */
final class C2832d0 extends C2390e {

    /* renamed from: a */
    private final /* synthetic */ C4066i f4691a;

    C2832d0(C2826b bVar, C4066i iVar) {
        this.f4691a = iVar;
    }

    /* renamed from: a */
    public final void mo17202a(zzad zzad) {
        Status b = zzad.mo16419b();
        if (b == null) {
            this.f4691a.mo23721b((Exception) new C2030b(new Status(8, "Got null status from location service")));
        } else if (b.mo16512c() == 0) {
            this.f4691a.mo23720a((Boolean) true);
        } else {
            this.f4691a.mo23721b((Exception) C2191b.m5353a(b));
        }
    }
}
