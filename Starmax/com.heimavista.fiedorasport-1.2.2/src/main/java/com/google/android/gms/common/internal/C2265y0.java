package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import p119e.p144d.p145a.p157c.p161c.p163b.C4010b;

/* renamed from: com.google.android.gms.common.internal.y0 */
public abstract class C2265y0 extends C4010b implements C2263x0 {
    /* renamed from: a */
    public static C2263x0 m5660a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.IGoogleCertificatesApi");
        if (queryLocalInterface instanceof C2263x0) {
            return (C2263x0) queryLocalInterface;
        }
        return new C2280z0(iBinder);
    }
}
