package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import com.google.android.gms.common.internal.C2224j;
import com.google.android.gms.common.stats.C2303a;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.google.android.gms.common.internal.r0 */
final class C2245r0 implements ServiceConnection {

    /* renamed from: P */
    private final Set<ServiceConnection> f3744P = new HashSet();

    /* renamed from: Q */
    private int f3745Q = 2;

    /* renamed from: R */
    private boolean f3746R;

    /* renamed from: S */
    private IBinder f3747S;

    /* renamed from: T */
    private final C2224j.C2225a f3748T;

    /* renamed from: U */
    private ComponentName f3749U;

    /* renamed from: V */
    private final /* synthetic */ C2243q0 f3750V;

    public C2245r0(C2243q0 q0Var, C2224j.C2225a aVar) {
        this.f3750V = q0Var;
        this.f3748T = aVar;
    }

    /* renamed from: a */
    public final void mo17025a(String str) {
        this.f3745Q = 3;
        this.f3746R = this.f3750V.f3741U.mo17125a(this.f3750V.f3739S, str, this.f3748T.mo16992a(this.f3750V.f3739S), this, this.f3748T.mo16994c());
        if (this.f3746R) {
            this.f3750V.f3740T.sendMessageDelayed(this.f3750V.f3740T.obtainMessage(1, this.f3748T), this.f3750V.f3743W);
            return;
        }
        this.f3745Q = 2;
        try {
            this.f3750V.f3741U.mo17123a(this.f3750V.f3739S, this);
        } catch (IllegalArgumentException unused) {
        }
    }

    /* renamed from: b */
    public final void mo17029b(String str) {
        this.f3750V.f3740T.removeMessages(1, this.f3748T);
        this.f3750V.f3741U.mo17123a(this.f3750V.f3739S, this);
        this.f3746R = false;
        this.f3745Q = 2;
    }

    /* renamed from: c */
    public final int mo17030c() {
        return this.f3745Q;
    }

    /* renamed from: d */
    public final boolean mo17031d() {
        return this.f3746R;
    }

    /* renamed from: e */
    public final boolean mo17032e() {
        return this.f3744P.isEmpty();
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.f3750V.f3738R) {
            this.f3750V.f3740T.removeMessages(1, this.f3748T);
            this.f3747S = iBinder;
            this.f3749U = componentName;
            for (ServiceConnection serviceConnection : this.f3744P) {
                serviceConnection.onServiceConnected(componentName, iBinder);
            }
            this.f3745Q = 1;
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.f3750V.f3738R) {
            this.f3750V.f3740T.removeMessages(1, this.f3748T);
            this.f3747S = null;
            this.f3749U = componentName;
            for (ServiceConnection serviceConnection : this.f3744P) {
                serviceConnection.onServiceDisconnected(componentName);
            }
            this.f3745Q = 2;
        }
    }

    /* renamed from: b */
    public final void mo17028b(ServiceConnection serviceConnection, String str) {
        C2303a unused = this.f3750V.f3741U;
        Context unused2 = this.f3750V.f3739S;
        this.f3744P.remove(serviceConnection);
    }

    /* renamed from: b */
    public final ComponentName mo17027b() {
        return this.f3749U;
    }

    /* renamed from: a */
    public final void mo17024a(ServiceConnection serviceConnection, String str) {
        C2303a unused = this.f3750V.f3741U;
        Context unused2 = this.f3750V.f3739S;
        this.f3748T.mo16992a(this.f3750V.f3739S);
        this.f3744P.add(serviceConnection);
    }

    /* renamed from: a */
    public final boolean mo17026a(ServiceConnection serviceConnection) {
        return this.f3744P.contains(serviceConnection);
    }

    /* renamed from: a */
    public final IBinder mo17023a() {
        return this.f3747S;
    }
}
