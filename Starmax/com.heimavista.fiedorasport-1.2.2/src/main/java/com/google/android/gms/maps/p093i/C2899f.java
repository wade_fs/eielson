package com.google.android.gms.maps.p093i;

import android.os.Bundle;
import android.os.IInterface;
import com.google.android.gms.maps.StreetViewPanoramaOptions;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.i.f */
public interface C2899f extends IInterface {
    /* renamed from: a */
    C3988b mo18459a(C3988b bVar, C3988b bVar2, Bundle bundle);

    /* renamed from: a */
    void mo18460a(Bundle bundle);

    /* renamed from: a */
    void mo18461a(C2916u uVar);

    /* renamed from: a */
    void mo18462a(C3988b bVar, StreetViewPanoramaOptions streetViewPanoramaOptions, Bundle bundle);

    /* renamed from: b */
    void mo18463b();

    /* renamed from: b */
    void mo18464b(Bundle bundle);

    /* renamed from: c */
    void mo18465c();

    /* renamed from: d */
    void mo18466d();

    /* renamed from: g */
    void mo18467g();

    void onLowMemory();

    void onResume();

    void onStart();
}
