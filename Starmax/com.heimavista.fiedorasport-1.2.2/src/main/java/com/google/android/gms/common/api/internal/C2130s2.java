package com.google.android.gms.common.api.internal;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.util.p091s.C2328a;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import p119e.p144d.p145a.p157c.p166d.C4047a;
import p119e.p144d.p145a.p157c.p166d.C4052e;

/* renamed from: com.google.android.gms.common.api.internal.s2 */
public final class C2130s2 implements C2082h1 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final Map<C2016a.C2019c<?>, C2126r2<?>> f3445a = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Map<C2016a.C2019c<?>, C2126r2<?>> f3446b = new HashMap();

    /* renamed from: c */
    private final Map<C2016a<?>, Boolean> f3447c;

    /* renamed from: d */
    private final C2064e f3448d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public final C2100l0 f3449e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public final Lock f3450f;

    /* renamed from: g */
    private final Looper f3451g;

    /* renamed from: h */
    private final C2169c f3452h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public final Condition f3453i;

    /* renamed from: j */
    private final C2211e f3454j;

    /* renamed from: k */
    private final boolean f3455k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public final boolean f3456l;

    /* renamed from: m */
    private final Queue<C2056c<?, ?>> f3457m = new LinkedList();
    /* access modifiers changed from: private */

    /* renamed from: n */
    public boolean f3458n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public Map<C2063d2<?>, ConnectionResult> f3459o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public Map<C2063d2<?>, ConnectionResult> f3460p;

    /* renamed from: q */
    private C2119q f3461q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public ConnectionResult f3462r;

    public C2130s2(Context context, Lock lock, Looper looper, C2169c cVar, Map<C2016a.C2019c<?>, C2016a.C2027f> map, C2211e eVar, Map<C2016a<?>, Boolean> map2, C2016a.C2017a<? extends C4052e, C4047a> aVar, ArrayList<C2102l2> arrayList, C2100l0 l0Var, boolean z) {
        boolean z2;
        boolean z3;
        boolean z4;
        this.f3450f = lock;
        this.f3451g = looper;
        this.f3453i = lock.newCondition();
        this.f3452h = cVar;
        this.f3449e = l0Var;
        this.f3447c = map2;
        this.f3454j = eVar;
        this.f3455k = z;
        HashMap hashMap = new HashMap();
        for (C2016a aVar2 : map2.keySet()) {
            hashMap.put(aVar2.mo16521a(), aVar2);
        }
        HashMap hashMap2 = new HashMap();
        int size = arrayList.size();
        int i = 0;
        while (i < size) {
            C2102l2 l2Var = arrayList.get(i);
            i++;
            C2102l2 l2Var2 = l2Var;
            hashMap2.put(l2Var2.f3384a, l2Var2);
        }
        boolean z5 = true;
        boolean z6 = false;
        boolean z7 = true;
        boolean z8 = false;
        for (Map.Entry entry : map.entrySet()) {
            C2016a aVar3 = (C2016a) hashMap.get(entry.getKey());
            C2016a.C2027f fVar = (C2016a.C2027f) entry.getValue();
            if (fVar.mo16536h()) {
                z3 = z7;
                z4 = !this.f3447c.get(aVar3).booleanValue() ? true : z8;
                z2 = true;
            } else {
                z2 = z6;
                z4 = z8;
                z3 = false;
            }
            C2126r2 r2Var = r1;
            C2126r2 r2Var2 = new C2126r2(context, aVar3, looper, fVar, (C2102l2) hashMap2.get(aVar3), eVar, aVar);
            this.f3445a.put((C2016a.C2019c) entry.getKey(), r2Var);
            if (fVar.mo16538l()) {
                this.f3446b.put((C2016a.C2019c) entry.getKey(), r2Var);
            }
            z8 = z4;
            z7 = z3;
            z6 = z2;
        }
        this.f3456l = (!z6 || z7 || z8) ? false : z5;
        this.f3448d = C2064e.m4796e();
    }

    /* renamed from: b */
    private final <T extends C2056c<? extends C2157k, ? extends C2016a.C2018b>> boolean m5092b(@NonNull T t) {
        C2016a.C2019c h = t.mo16642h();
        ConnectionResult a = m5084a(h);
        if (a == null || a.mo16475c() != 4) {
            return false;
        }
        t.mo16640c(new Status(4, null, this.f3448d.mo16650a(this.f3445a.get(h).mo16559h(), System.identityHashCode(this.f3449e))));
        return true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x001f A[Catch:{ all -> 0x0044 }] */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final boolean m5100h() {
        /*
            r3 = this;
            java.util.concurrent.locks.Lock r0 = r3.f3450f
            r0.lock()
            boolean r0 = r3.f3458n     // Catch:{ all -> 0x0044 }
            r1 = 0
            if (r0 == 0) goto L_0x003e
            boolean r0 = r3.f3455k     // Catch:{ all -> 0x0044 }
            if (r0 != 0) goto L_0x000f
            goto L_0x003e
        L_0x000f:
            java.util.Map<com.google.android.gms.common.api.a$c<?>, com.google.android.gms.common.api.internal.r2<?>> r0 = r3.f3446b     // Catch:{ all -> 0x0044 }
            java.util.Set r0 = r0.keySet()     // Catch:{ all -> 0x0044 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0044 }
        L_0x0019:
            boolean r2 = r0.hasNext()     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x0037
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x0044 }
            com.google.android.gms.common.api.a$c r2 = (com.google.android.gms.common.api.C2016a.C2019c) r2     // Catch:{ all -> 0x0044 }
            com.google.android.gms.common.ConnectionResult r2 = r3.m5084a(r2)     // Catch:{ all -> 0x0044 }
            if (r2 == 0) goto L_0x0031
            boolean r2 = r2.mo16482w()     // Catch:{ all -> 0x0044 }
            if (r2 != 0) goto L_0x0019
        L_0x0031:
            java.util.concurrent.locks.Lock r0 = r3.f3450f
            r0.unlock()
            return r1
        L_0x0037:
            java.util.concurrent.locks.Lock r0 = r3.f3450f
            r0.unlock()
            r0 = 1
            return r0
        L_0x003e:
            java.util.concurrent.locks.Lock r0 = r3.f3450f
            r0.unlock()
            return r1
        L_0x0044:
            r0 = move-exception
            java.util.concurrent.locks.Lock r1 = r3.f3450f
            r1.unlock()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.api.internal.C2130s2.m5100h():boolean");
    }

    /* access modifiers changed from: private */
    /* renamed from: i */
    public final void m5101i() {
        C2211e eVar = this.f3454j;
        if (eVar == null) {
            this.f3449e.f3374q = Collections.emptySet();
            return;
        }
        HashSet hashSet = new HashSet(eVar.mo16966i());
        Map<C2016a<?>, C2211e.C2213b> f = this.f3454j.mo16963f();
        for (C2016a aVar : f.keySet()) {
            ConnectionResult a = mo16776a(aVar);
            if (a != null && a.mo16482w()) {
                hashSet.addAll(f.get(aVar).f3707a);
            }
        }
        this.f3449e.f3374q = hashSet;
    }

    /* access modifiers changed from: private */
    /* renamed from: j */
    public final void m5103j() {
        while (!this.f3457m.isEmpty()) {
            mo16708a(this.f3457m.remove());
        }
        this.f3449e.mo16729a((Bundle) null);
    }

    /* access modifiers changed from: private */
    @Nullable
    /* renamed from: k */
    public final ConnectionResult m5105k() {
        ConnectionResult connectionResult = null;
        ConnectionResult connectionResult2 = null;
        int i = 0;
        int i2 = 0;
        for (C2126r2 r2Var : this.f3445a.values()) {
            C2016a c = r2Var.mo16554c();
            ConnectionResult connectionResult3 = this.f3459o.get(r2Var.mo16559h());
            if (!connectionResult3.mo16482w() && (!this.f3447c.get(c).booleanValue() || connectionResult3.mo16481v() || this.f3452h.mo16833c(connectionResult3.mo16475c()))) {
                if (connectionResult3.mo16475c() != 4 || !this.f3455k) {
                    int a = c.mo16523c().mo16527a();
                    if (connectionResult == null || i > a) {
                        connectionResult = connectionResult3;
                        i = a;
                    }
                } else {
                    int a2 = c.mo16523c().mo16527a();
                    if (connectionResult2 == null || i2 > a2) {
                        connectionResult2 = connectionResult3;
                        i2 = a2;
                    }
                }
            }
        }
        return (connectionResult == null || connectionResult2 == null || i <= i2) ? connectionResult : connectionResult2;
    }

    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16708a(@NonNull T t) {
        C2016a.C2019c h = t.mo16642h();
        if (this.f3455k && m5092b((C2056c) t)) {
            return t;
        }
        this.f3449e.f3382y.mo16790a(t);
        this.f3445a.get(h).mo16549a((C2056c) t);
        return t;
    }

    /* renamed from: a */
    public final void mo16710a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    /* renamed from: c */
    public final boolean mo16713c() {
        this.f3450f.lock();
        try {
            return this.f3459o != null && this.f3462r == null;
        } finally {
            this.f3450f.unlock();
        }
    }

    /* renamed from: d */
    public final void mo16714d() {
    }

    /* renamed from: e */
    public final void mo16715e() {
        this.f3450f.lock();
        try {
            this.f3448d.mo16654a();
            if (this.f3461q != null) {
                this.f3461q.mo16765a();
                this.f3461q = null;
            }
            if (this.f3460p == null) {
                this.f3460p = new ArrayMap(this.f3446b.size());
            }
            ConnectionResult connectionResult = new ConnectionResult(4);
            for (C2126r2<?> r2Var : this.f3446b.values()) {
                this.f3460p.put(r2Var.mo16559h(), connectionResult);
            }
            if (this.f3459o != null) {
                this.f3459o.putAll(this.f3460p);
            }
        } finally {
            this.f3450f.unlock();
        }
    }

    /* renamed from: f */
    public final ConnectionResult mo16716f() {
        mo16712b();
        while (mo16777g()) {
            try {
                this.f3453i.await();
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
                return new ConnectionResult(15, null);
            }
        }
        if (mo16713c()) {
            return ConnectionResult.f3156T;
        }
        ConnectionResult connectionResult = this.f3462r;
        if (connectionResult != null) {
            return connectionResult;
        }
        return new ConnectionResult(13, null);
    }

    /* renamed from: g */
    public final boolean mo16777g() {
        this.f3450f.lock();
        try {
            return this.f3459o == null && this.f3458n;
        } finally {
            this.f3450f.unlock();
        }
    }

    /* renamed from: a */
    public final void mo16709a() {
        this.f3450f.lock();
        try {
            this.f3458n = false;
            this.f3459o = null;
            this.f3460p = null;
            if (this.f3461q != null) {
                this.f3461q.mo16765a();
                this.f3461q = null;
            }
            this.f3462r = null;
            while (!this.f3457m.isEmpty()) {
                C2056c remove = this.f3457m.remove();
                remove.mo16592a((C2152y1) null);
                remove.mo16591a();
            }
            this.f3453i.signalAll();
        } finally {
            this.f3450f.unlock();
        }
    }

    /* renamed from: b */
    public final void mo16712b() {
        this.f3450f.lock();
        try {
            if (!this.f3458n) {
                this.f3458n = true;
                this.f3459o = null;
                this.f3460p = null;
                this.f3461q = null;
                this.f3462r = null;
                this.f3448d.mo16662c();
                this.f3448d.mo16653a(this.f3445a.values()).mo23700a(new C2328a(this.f3451g), new C2138u2(this));
                this.f3450f.unlock();
            }
        } finally {
            this.f3450f.unlock();
        }
    }

    @Nullable
    /* renamed from: a */
    public final ConnectionResult mo16776a(@NonNull C2016a<?> aVar) {
        return m5084a(aVar.mo16521a());
    }

    @Nullable
    /* renamed from: a */
    private final ConnectionResult m5084a(@NonNull C2016a.C2019c<?> cVar) {
        this.f3450f.lock();
        try {
            C2126r2 r2Var = this.f3445a.get(cVar);
            if (this.f3459o != null && r2Var != null) {
                return this.f3459o.get(r2Var.mo16559h());
            }
            this.f3450f.unlock();
            return null;
        } finally {
            this.f3450f.unlock();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public final boolean mo16711a(C2099l lVar) {
        this.f3450f.lock();
        try {
            if (!this.f3458n || m5100h()) {
                this.f3450f.unlock();
                return false;
            }
            this.f3448d.mo16662c();
            this.f3461q = new C2119q(this, lVar);
            this.f3448d.mo16653a(this.f3446b.values()).mo23700a(new C2328a(this.f3451g), this.f3461q);
            this.f3450f.unlock();
            return true;
        } catch (Throwable th) {
            this.f3450f.unlock();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final boolean m5088a(C2126r2<?> r2Var, ConnectionResult connectionResult) {
        return !connectionResult.mo16482w() && !connectionResult.mo16481v() && this.f3447c.get(r2Var.mo16554c()).booleanValue() && r2Var.mo16774i().mo16536h() && this.f3452h.mo16833c(connectionResult.mo16475c());
    }
}
