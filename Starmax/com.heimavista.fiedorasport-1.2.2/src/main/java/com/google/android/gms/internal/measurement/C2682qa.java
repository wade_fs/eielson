package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.qa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2682qa implements C2593l2<C2667pa> {

    /* renamed from: Q */
    private static C2682qa f4451Q = new C2682qa();

    /* renamed from: P */
    private final C2593l2<C2667pa> f4452P;

    private C2682qa(C2593l2<C2667pa> l2Var) {
        this.f4452P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7090b() {
        return ((C2667pa) f4451Q.mo17285a()).mo17811a();
    }

    /* renamed from: c */
    public static boolean m7091c() {
        return ((C2667pa) f4451Q.mo17285a()).mo17812e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4452P.mo17285a();
    }

    public C2682qa() {
        this(C2579k2.m6601a(new C2712sa()));
    }
}
