package com.google.android.gms.common.stats;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.List;

public final class WakeLockEvent extends StatsEvent {
    public static final Parcelable.Creator<WakeLockEvent> CREATOR = new C2307e();

    /* renamed from: P */
    private final int f3826P;

    /* renamed from: Q */
    private final long f3827Q;

    /* renamed from: R */
    private int f3828R;

    /* renamed from: S */
    private final String f3829S;

    /* renamed from: T */
    private final String f3830T;

    /* renamed from: U */
    private final String f3831U;

    /* renamed from: V */
    private final int f3832V;

    /* renamed from: W */
    private final List<String> f3833W;

    /* renamed from: X */
    private final String f3834X;

    /* renamed from: Y */
    private final long f3835Y;

    /* renamed from: Z */
    private int f3836Z;

    /* renamed from: a0 */
    private final String f3837a0;

    /* renamed from: b0 */
    private final float f3838b0;

    /* renamed from: c0 */
    private final long f3839c0;

    /* renamed from: d0 */
    private final boolean f3840d0;

    /* renamed from: e0 */
    private long f3841e0;

    WakeLockEvent(int i, long j, int i2, String str, int i3, List<String> list, String str2, long j2, int i4, String str3, String str4, float f, long j3, String str5, boolean z) {
        this.f3826P = i;
        this.f3827Q = j;
        this.f3828R = i2;
        this.f3829S = str;
        this.f3830T = str3;
        this.f3831U = str5;
        this.f3832V = i3;
        this.f3841e0 = -1;
        this.f3833W = list;
        this.f3834X = str2;
        this.f3835Y = j2;
        this.f3836Z = i4;
        this.f3837a0 = str4;
        this.f3838b0 = f;
        this.f3839c0 = j3;
        this.f3840d0 = z;
    }

    /* renamed from: c */
    public final int mo17117c() {
        return this.f3828R;
    }

    /* renamed from: d */
    public final long mo17118d() {
        return this.f3827Q;
    }

    /* renamed from: u */
    public final long mo17120u() {
        return this.f3841e0;
    }

    /* renamed from: v */
    public final String mo17121v() {
        String str;
        String str2 = this.f3829S;
        int i = this.f3832V;
        List<String> list = this.f3833W;
        String str3 = "";
        if (list == null) {
            str = str3;
        } else {
            str = TextUtils.join(",", list);
        }
        int i2 = this.f3836Z;
        String str4 = this.f3830T;
        if (str4 == null) {
            str4 = str3;
        }
        String str5 = this.f3837a0;
        if (str5 == null) {
            str5 = str3;
        }
        float f = this.f3838b0;
        String str6 = this.f3831U;
        if (str6 != null) {
            str3 = str6;
        }
        boolean z = this.f3840d0;
        StringBuilder sb = new StringBuilder(String.valueOf(str2).length() + 51 + String.valueOf(str).length() + String.valueOf(str4).length() + String.valueOf(str5).length() + String.valueOf(str3).length());
        sb.append("\t");
        sb.append(str2);
        sb.append("\t");
        sb.append(i);
        sb.append("\t");
        sb.append(str);
        sb.append("\t");
        sb.append(i2);
        sb.append("\t");
        sb.append(str4);
        sb.append("\t");
        sb.append(str5);
        sb.append("\t");
        sb.append(f);
        sb.append("\t");
        sb.append(str3);
        sb.append("\t");
        sb.append(z);
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3826P);
        C2250b.m5592a(parcel, 2, mo17118d());
        C2250b.m5602a(parcel, 4, this.f3829S, false);
        C2250b.m5591a(parcel, 5, this.f3832V);
        C2250b.m5612b(parcel, 6, this.f3833W, false);
        C2250b.m5592a(parcel, 8, this.f3835Y);
        C2250b.m5602a(parcel, 10, this.f3830T, false);
        C2250b.m5591a(parcel, 11, mo17117c());
        C2250b.m5602a(parcel, 12, this.f3834X, false);
        C2250b.m5602a(parcel, 13, this.f3837a0, false);
        C2250b.m5591a(parcel, 14, this.f3836Z);
        C2250b.m5590a(parcel, 15, this.f3838b0);
        C2250b.m5592a(parcel, 16, this.f3839c0);
        C2250b.m5602a(parcel, 17, this.f3831U, false);
        C2250b.m5605a(parcel, 18, this.f3840d0);
        C2250b.m5587a(parcel, a);
    }

    public WakeLockEvent(long j, int i, String str, int i2, List<String> list, String str2, long j2, int i3, String str3, String str4, float f, long j3, String str5, boolean z) {
        this(2, j, i, str, i2, list, str2, j2, i3, str3, str4, f, j3, str5, z);
    }
}
