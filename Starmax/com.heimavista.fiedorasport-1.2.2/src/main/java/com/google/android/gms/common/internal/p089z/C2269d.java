package com.google.android.gms.common.internal.p089z;

import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.internal.z.d */
public interface C2269d {
    /* renamed from: a */
    C2040g<Status> mo17047a(C2036f fVar);
}
