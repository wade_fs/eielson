package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import java.util.concurrent.Executor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.r */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C1748r implements Factory<C1747q> {

    /* renamed from: a */
    private final Provider<Executor> f2775a;

    /* renamed from: b */
    private final Provider<C3934c> f2776b;

    /* renamed from: c */
    private final Provider<C1749s> f2777c;

    /* renamed from: d */
    private final Provider<C3969b> f2778d;

    public C1748r(Provider<Executor> aVar, Provider<C3934c> aVar2, Provider<C1749s> aVar3, Provider<C3969b> aVar4) {
        this.f2775a = aVar;
        this.f2776b = aVar2;
        this.f2777c = aVar3;
        this.f2778d = aVar4;
    }

    /* renamed from: a */
    public static C1748r m4367a(Provider<Executor> aVar, Provider<C3934c> aVar2, Provider<C1749s> aVar3, Provider<C3969b> aVar4) {
        return new C1748r(aVar, aVar2, aVar3, aVar4);
    }

    public C1747q get() {
        return new C1747q(this.f2775a.get(), this.f2776b.get(), this.f2777c.get(), this.f2778d.get());
    }
}
