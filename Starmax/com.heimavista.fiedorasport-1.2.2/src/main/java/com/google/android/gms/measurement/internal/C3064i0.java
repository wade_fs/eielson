package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2620m8;

/* renamed from: com.google.android.gms.measurement.internal.i0 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3064i0 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5199a = new C3064i0();

    private C3064i0() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Long.valueOf(C2620m8.m6817n());
    }
}
