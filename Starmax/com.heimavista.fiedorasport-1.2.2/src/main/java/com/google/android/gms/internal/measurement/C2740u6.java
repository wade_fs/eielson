package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.u6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2740u6 implements Iterator<Map.Entry<K, V>> {

    /* renamed from: P */
    private int f4512P;

    /* renamed from: Q */
    private boolean f4513Q;

    /* renamed from: R */
    private Iterator<Map.Entry<K, V>> f4514R;

    /* renamed from: S */
    private final /* synthetic */ C2618m6 f4515S;

    private C2740u6(C2618m6 m6Var) {
        this.f4515S = m6Var;
        this.f4512P = -1;
    }

    /* renamed from: a */
    private final Iterator<Map.Entry<K, V>> m7378a() {
        if (this.f4514R == null) {
            this.f4514R = this.f4515S.f4328R.entrySet().iterator();
        }
        return this.f4514R;
    }

    public final boolean hasNext() {
        if (this.f4512P + 1 < this.f4515S.f4327Q.size() || (!this.f4515S.f4328R.isEmpty() && m7378a().hasNext())) {
            return true;
        }
        return false;
    }

    public final /* synthetic */ Object next() {
        this.f4513Q = true;
        int i = this.f4512P + 1;
        this.f4512P = i;
        if (i < this.f4515S.f4327Q.size()) {
            return (Map.Entry) this.f4515S.f4327Q.get(this.f4512P);
        }
        return (Map.Entry) m7378a().next();
    }

    public final void remove() {
        if (this.f4513Q) {
            this.f4513Q = false;
            this.f4515S.m6785f();
            if (this.f4512P < this.f4515S.f4327Q.size()) {
                C2618m6 m6Var = this.f4515S;
                int i = this.f4512P;
                this.f4512P = i - 1;
                Object unused = m6Var.m6782c(i);
                return;
            }
            m7378a().remove();
            return;
        }
        throw new IllegalStateException("remove() was called before next()");
    }

    /* synthetic */ C2740u6(C2618m6 m6Var, C2663p6 p6Var) {
        this(m6Var);
    }
}
