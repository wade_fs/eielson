package com.google.android.gms.internal.measurement;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.measurement.s6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2708s6 implements Iterable<Object> {
    C2708s6() {
    }

    public final Iterator<Object> iterator() {
        return C2678q6.f4428a;
    }
}
