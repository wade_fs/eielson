package com.google.android.gms.internal.measurement;

import android.os.Bundle;
import android.os.IInterface;

/* renamed from: com.google.android.gms.internal.measurement.cd */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2459cd extends IInterface {
    /* renamed from: a */
    int mo17386a();

    /* renamed from: a */
    void mo17387a(String str, String str2, Bundle bundle, long j);
}
