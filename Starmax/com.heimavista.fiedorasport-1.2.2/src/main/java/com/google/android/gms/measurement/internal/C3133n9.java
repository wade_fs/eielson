package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.n9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3133n9 extends C3039g {

    /* renamed from: e */
    private final /* synthetic */ C3145o9 f5380e;

    /* renamed from: f */
    private final /* synthetic */ C3097k9 f5381f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C3133n9(C3097k9 k9Var, C3058h6 h6Var, C3145o9 o9Var) {
        super(h6Var);
        this.f5381f = k9Var;
        this.f5380e = o9Var;
    }

    /* renamed from: a */
    public final void mo19022a() {
        this.f5381f.mo19132u();
        this.f5381f.mo19015l().mo18996B().mo19042a("Starting upload from DelayedRunnable");
        this.f5380e.mo19236p();
    }
}
