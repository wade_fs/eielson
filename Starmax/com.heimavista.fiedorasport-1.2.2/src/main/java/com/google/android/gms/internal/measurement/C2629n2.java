package com.google.android.gms.internal.measurement;

import java.io.Serializable;

/* renamed from: com.google.android.gms.internal.measurement.n2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2629n2<T> implements C2593l2<T>, Serializable {

    /* renamed from: P */
    private final C2593l2<T> f4345P;

    /* renamed from: Q */
    private volatile transient boolean f4346Q;

    /* renamed from: R */
    private transient T f4347R;

    C2629n2(C2593l2<T> l2Var) {
        C2546i2.m6469a(l2Var);
        this.f4345P = l2Var;
    }

    /* renamed from: a */
    public final T mo17285a() {
        if (!this.f4346Q) {
            synchronized (this) {
                if (!this.f4346Q) {
                    T a = this.f4345P.mo17285a();
                    this.f4347R = a;
                    this.f4346Q = true;
                    return a;
                }
            }
        }
        return this.f4347R;
    }

    public final String toString() {
        Object obj;
        if (this.f4346Q) {
            String valueOf = String.valueOf(this.f4347R);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 25);
            sb.append("<supplier that returned ");
            sb.append(valueOf);
            sb.append(">");
            obj = sb.toString();
        } else {
            obj = this.f4345P;
        }
        String valueOf2 = String.valueOf(obj);
        StringBuilder sb2 = new StringBuilder(String.valueOf(valueOf2).length() + 19);
        sb2.append("Suppliers.memoize(");
        sb2.append(valueOf2);
        sb2.append(")");
        return sb2.toString();
    }
}
