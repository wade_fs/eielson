package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.za */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2821za implements C2425ab {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4631a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.referrer.enable_logging_install_referrer_cmp_from_apk", false);

    /* renamed from: a */
    public final boolean mo17290a() {
        return f4631a.mo18128b().booleanValue();
    }
}
