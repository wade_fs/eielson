package com.google.android.gms.internal.measurement;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.Context;
import android.os.Build;
import android.os.UserHandle;
import android.util.Log;
import androidx.annotation.Nullable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

@TargetApi(24)
/* renamed from: com.google.android.gms.internal.measurement.g6 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2518g6 {
    @Nullable

    /* renamed from: b */
    private static final Method f4172b = m6372a();
    @Nullable

    /* renamed from: c */
    private static final Method f4173c = m6373b();

    /* renamed from: a */
    private final JobScheduler f4174a;

    private C2518g6(JobScheduler jobScheduler) {
        this.f4174a = jobScheduler;
    }

    @Nullable
    /* renamed from: a */
    private static Method m6372a() {
        if (Build.VERSION.SDK_INT < 24) {
            return null;
        }
        try {
            return JobScheduler.class.getDeclaredMethod("scheduleAsPackage", JobInfo.class, String.class, Integer.TYPE, String.class);
        } catch (NoSuchMethodException unused) {
            if (!Log.isLoggable("JobSchedulerCompat", 6)) {
                return null;
            }
            Log.e("JobSchedulerCompat", "No scheduleAsPackage method available, falling back to schedule");
            return null;
        }
    }

    @Nullable
    /* renamed from: b */
    private static Method m6373b() {
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                return UserHandle.class.getDeclaredMethod("myUserId", null);
            } catch (NoSuchMethodException unused) {
                if (Log.isLoggable("JobSchedulerCompat", 6)) {
                    Log.e("JobSchedulerCompat", "No myUserId method available");
                }
            }
        }
        return null;
    }

    /* renamed from: c */
    private static int m6374c() {
        Method method = f4173c;
        if (method != null) {
            try {
                return ((Integer) method.invoke(null, new Object[0])).intValue();
            } catch (IllegalAccessException | InvocationTargetException e) {
                if (Log.isLoggable("JobSchedulerCompat", 6)) {
                    Log.e("JobSchedulerCompat", "myUserId invocation illegal", e);
                }
            }
        }
        return 0;
    }

    /* renamed from: a */
    private final int m6370a(JobInfo jobInfo, String str, int i, String str2) {
        Method method = f4172b;
        if (method != null) {
            try {
                return ((Integer) method.invoke(this.f4174a, jobInfo, str, Integer.valueOf(i), str2)).intValue();
            } catch (IllegalAccessException | InvocationTargetException e) {
                Log.e(str2, "error calling scheduleAsPackage", e);
            }
        }
        return this.f4174a.schedule(jobInfo);
    }

    /* renamed from: a */
    public static int m6371a(Context context, JobInfo jobInfo, String str, String str2) {
        JobScheduler jobScheduler = (JobScheduler) context.getSystemService("jobscheduler");
        if (f4172b == null || context.checkSelfPermission("android.permission.UPDATE_DEVICE_STATS") != 0) {
            return jobScheduler.schedule(jobInfo);
        }
        return new C2518g6(jobScheduler).m6370a(jobInfo, str, m6374c(), str2);
    }
}
