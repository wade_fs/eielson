package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.internal.C2064e;
import com.google.android.gms.common.internal.C2197d;

/* renamed from: com.google.android.gms.common.api.internal.c1 */
final class C2058c1 implements C2197d.C2202e {

    /* renamed from: a */
    final /* synthetic */ C2064e.C2065a f3259a;

    C2058c1(C2064e.C2065a aVar) {
        this.f3259a = aVar;
    }

    /* renamed from: a */
    public final void mo16644a() {
        C2064e.this.f3284b0.post(new C2062d1(this));
    }
}
