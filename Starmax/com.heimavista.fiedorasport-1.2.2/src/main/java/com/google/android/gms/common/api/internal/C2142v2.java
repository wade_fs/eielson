package com.google.android.gms.common.api.internal;

/* renamed from: com.google.android.gms.common.api.internal.v2 */
final class C2142v2 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ LifecycleCallback f3492P;

    /* renamed from: Q */
    private final /* synthetic */ String f3493Q;

    /* renamed from: R */
    private final /* synthetic */ zza f3494R;

    C2142v2(zza zza, LifecycleCallback lifecycleCallback, String str) {
        this.f3494R = zza;
        this.f3492P = lifecycleCallback;
        this.f3493Q = str;
    }

    public final void run() {
        if (this.f3494R.f3536Q > 0) {
            this.f3492P.mo16605a(this.f3494R.f3537R != null ? this.f3494R.f3537R.getBundle(this.f3493Q) : null);
        }
        if (this.f3494R.f3536Q >= 2) {
            this.f3492P.mo16610d();
        }
        if (this.f3494R.f3536Q >= 3) {
            this.f3492P.mo16609c();
        }
        if (this.f3494R.f3536Q >= 4) {
            this.f3492P.mo16611e();
        }
        if (this.f3494R.f3536Q >= 5) {
            this.f3492P.mo16607b();
        }
    }
}
