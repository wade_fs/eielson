package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.f0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2494f0 extends C2595l4<C2494f0, C2495a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2494f0 zzj;
    private static volatile C2501f6<C2494f0> zzk;
    private int zzc;
    private int zzd;
    private String zze = "";
    private C2461d0 zzf;
    private boolean zzg;
    private boolean zzh;
    private boolean zzi;

    /* renamed from: com.google.android.gms.internal.measurement.f0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2495a extends C2595l4.C2596a<C2494f0, C2495a> implements C2769w5 {
        private C2495a() {
            super(C2494f0.zzj);
        }

        /* renamed from: a */
        public final C2495a mo17466a(String str) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2494f0) super.f4288Q).m6284a(str);
            return this;
        }

        /* synthetic */ C2495a(C2413a0 a0Var) {
            this();
        }
    }

    static {
        C2494f0 f0Var = new C2494f0();
        zzj = f0Var;
        C2595l4.m6645a(C2494f0.class, super);
    }

    private C2494f0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m6284a(String str) {
        str.getClass();
        this.zzc |= 2;
        this.zze = str;
    }

    /* renamed from: w */
    public static C2495a m6285w() {
        return (C2495a) zzj.mo17668i();
    }

    /* renamed from: n */
    public final boolean mo17458n() {
        return (this.zzc & 1) != 0;
    }

    /* renamed from: o */
    public final int mo17459o() {
        return this.zzd;
    }

    /* renamed from: p */
    public final String mo17460p() {
        return this.zze;
    }

    /* renamed from: q */
    public final C2461d0 mo17461q() {
        C2461d0 d0Var = this.zzf;
        return d0Var == null ? C2461d0.m6178w() : d0Var;
    }

    /* renamed from: s */
    public final boolean mo17462s() {
        return this.zzg;
    }

    /* renamed from: t */
    public final boolean mo17463t() {
        return this.zzh;
    }

    /* renamed from: u */
    public final boolean mo17464u() {
        return (this.zzc & 32) != 0;
    }

    /* renamed from: v */
    public final boolean mo17465v() {
        return this.zzi;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2413a0.f3970a[i - 1]) {
            case 1:
                return new C2494f0();
            case 2:
                return new C2495a(null);
            case 3:
                return C2595l4.m6643a(zzj, "\u0001\u0006\u0000\u0001\u0001\u0006\u0006\u0000\u0000\u0000\u0001\u0004\u0000\u0002\b\u0001\u0003\t\u0002\u0004\u0007\u0003\u0005\u0007\u0004\u0006\u0007\u0005", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg", "zzh", "zzi"});
            case 4:
                return zzj;
            case 5:
                C2501f6<C2494f0> f6Var = zzk;
                if (f6Var == null) {
                    synchronized (C2494f0.class) {
                        f6Var = zzk;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzj);
                            zzk = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
