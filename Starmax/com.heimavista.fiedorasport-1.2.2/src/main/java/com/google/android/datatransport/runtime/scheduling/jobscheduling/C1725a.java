package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Base64;
import androidx.annotation.VisibleForTesting;
import androidx.core.app.NotificationCompat;
import com.google.android.exoplayer2.C1750C;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p148t.C3915a;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p119e.p144d.p145a.p146a.p147i.p154y.C3977a;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C1725a implements C1749s {

    /* renamed from: a */
    private final Context f2715a;

    /* renamed from: b */
    private final C3934c f2716b;

    /* renamed from: c */
    private AlarmManager f2717c;

    /* renamed from: d */
    private final C1733g f2718d;

    /* renamed from: e */
    private final C3971a f2719e;

    public C1725a(Context context, C3934c cVar, C3971a aVar, C1733g gVar) {
        this(context, cVar, (AlarmManager) context.getSystemService(NotificationCompat.CATEGORY_ALARM), aVar, gVar);
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: a */
    public boolean mo13562a(Intent intent) {
        return PendingIntent.getBroadcast(this.f2715a, 0, intent, C1750C.ENCODING_PCM_A_LAW) != null;
    }

    /* renamed from: a */
    public void mo13561a(C3905l lVar, int i) {
        Uri.Builder builder = new Uri.Builder();
        builder.appendQueryParameter("backendName", lVar.mo23531a());
        builder.appendQueryParameter("priority", String.valueOf(C3977a.m11939a(lVar.mo23533c())));
        if (lVar.mo23532b() != null) {
            builder.appendQueryParameter("extras", Base64.encodeToString(lVar.mo23532b(), 0));
        }
        Intent intent = new Intent(this.f2715a, AlarmManagerSchedulerBroadcastReceiver.class);
        intent.setData(builder.build());
        intent.putExtra("attemptNumber", i);
        if (mo13562a(intent)) {
            C3915a.m11792a("AlarmManagerScheduler", "Upload for context %s is already scheduled. Returning...", lVar);
            return;
        }
        long b = this.f2716b.mo23590b(lVar);
        long a = this.f2718d.mo13581a(lVar.mo23533c(), b, i);
        C3915a.m11794a("AlarmManagerScheduler", "Scheduling upload for context %s in %dms(Backend next call timestamp %d). Attempt %d", lVar, Long.valueOf(a), Long.valueOf(b), Integer.valueOf(i));
        this.f2717c.set(3, this.f2719e.mo23604a() + a, PendingIntent.getBroadcast(this.f2715a, 0, intent, 0));
    }

    @VisibleForTesting
    C1725a(Context context, C3934c cVar, AlarmManager alarmManager, C3971a aVar, C1733g gVar) {
        this.f2715a = context;
        this.f2716b = cVar;
        this.f2717c = alarmManager;
        this.f2719e = aVar;
        this.f2718d = gVar;
    }
}
