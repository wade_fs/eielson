package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.i3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2547i3 extends C2660p3 {

    /* renamed from: T */
    private final int f4226T;

    /* renamed from: U */
    private final int f4227U;

    C2547i3(byte[] bArr, int i, int i2) {
        super(bArr);
        C2498f3.m6305b(i, i + i2, bArr.length);
        this.f4226T = i;
        this.f4227U = i2;
    }

    /* renamed from: a */
    public final byte mo17468a(int i) {
        int a = mo17469a();
        if (((a - (i + 1)) | i) >= 0) {
            return super.f4417S[this.f4226T + i];
        }
        if (i < 0) {
            StringBuilder sb = new StringBuilder(22);
            sb.append("Index < 0: ");
            sb.append(i);
            throw new ArrayIndexOutOfBoundsException(sb.toString());
        }
        StringBuilder sb2 = new StringBuilder(40);
        sb2.append("Index > length: ");
        sb2.append(i);
        sb2.append(", ");
        sb2.append(a);
        throw new ArrayIndexOutOfBoundsException(sb2.toString());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final byte mo17474b(int i) {
        return super.f4417S[this.f4226T + i];
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final int mo17571t() {
        return this.f4226T;
    }

    /* renamed from: a */
    public final int mo17469a() {
        return this.f4227U;
    }
}
