package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.sa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2712sa implements C2667pa {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4484a;

    /* renamed from: b */
    private static final C2765w1<Boolean> f4485b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4484a = c2Var.mo17367a("measurement.collection.efficient_engagement_reporting_enabled", false);
        f4485b = c2Var.mo17367a("measurement.collection.redundant_engagement_removal_enabled", false);
    }

    /* renamed from: a */
    public final boolean mo17811a() {
        return f4484a.mo18128b().booleanValue();
    }

    /* renamed from: e */
    public final boolean mo17812e() {
        return f4485b.mo18128b().booleanValue();
    }
}
