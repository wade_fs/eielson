package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class DeviceMetaData extends AbstractSafeParcelable {
    public static final Parcelable.Creator<DeviceMetaData> CREATOR = new C1958e();

    /* renamed from: P */
    private final int f2993P;

    /* renamed from: Q */
    private boolean f2994Q;

    /* renamed from: R */
    private long f2995R;

    /* renamed from: S */
    private final boolean f2996S;

    DeviceMetaData(int i, boolean z, long j, boolean z2) {
        this.f2993P = i;
        this.f2994Q = z;
        this.f2995R = j;
        this.f2996S = z2;
    }

    /* renamed from: c */
    public long mo16305c() {
        return this.f2995R;
    }

    /* renamed from: d */
    public boolean mo16306d() {
        return this.f2996S;
    }

    /* renamed from: u */
    public boolean mo16307u() {
        return this.f2994Q;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f2993P);
        C2250b.m5605a(parcel, 2, mo16307u());
        C2250b.m5592a(parcel, 3, mo16305c());
        C2250b.m5605a(parcel, 4, mo16306d());
        C2250b.m5587a(parcel, a);
    }
}
