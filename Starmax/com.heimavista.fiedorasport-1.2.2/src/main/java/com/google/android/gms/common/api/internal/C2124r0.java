package com.google.android.gms.common.api.internal;

import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.google.android.gms.internal.base.C2382h;

/* renamed from: com.google.android.gms.common.api.internal.r0 */
final class C2124r0 extends C2382h {

    /* renamed from: a */
    private final /* synthetic */ C2100l0 f3430a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2124r0(C2100l0 l0Var, Looper looper) {
        super(looper);
        this.f3430a = l0Var;
    }

    public final void handleMessage(Message message) {
        int i = message.what;
        if (i == 1) {
            this.f3430a.m4961q();
        } else if (i != 2) {
            StringBuilder sb = new StringBuilder(31);
            sb.append("Unknown message id: ");
            sb.append(i);
            Log.w("GoogleApiClientImpl", sb.toString());
        } else {
            this.f3430a.m4959o();
        }
    }
}
