package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2466d4;

/* renamed from: com.google.android.gms.internal.measurement.d4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2466d4<T extends C2466d4<T>> extends Comparable<T> {
    /* renamed from: e */
    C2679q7 mo17413e();

    /* renamed from: f */
    C2787x7 mo17414f();

    /* renamed from: g */
    boolean mo17415g();
}
