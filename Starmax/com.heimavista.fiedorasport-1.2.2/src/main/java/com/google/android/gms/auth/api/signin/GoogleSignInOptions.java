package com.google.android.gms.auth.api.signin;

import android.accounts.Account;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import com.google.android.gms.auth.api.signin.internal.C1988a;
import com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GoogleSignInOptions extends AbstractSafeParcelable implements C2016a.C2020d.C2025e, ReflectedParcelable {
    public static final Parcelable.Creator<GoogleSignInOptions> CREATOR = new C1985h();

    /* renamed from: Y */
    public static final Scope f3089Y = new Scope("profile");

    /* renamed from: Z */
    public static final Scope f3090Z = new Scope(NotificationCompat.CATEGORY_EMAIL);

    /* renamed from: a0 */
    public static final Scope f3091a0 = new Scope("openid");

    /* renamed from: b0 */
    public static final Scope f3092b0 = new Scope("https://www.googleapis.com/auth/games_lite");

    /* renamed from: c0 */
    public static final Scope f3093c0 = new Scope("https://www.googleapis.com/auth/games");

    /* renamed from: d0 */
    public static final GoogleSignInOptions f3094d0;

    /* renamed from: e0 */
    private static Comparator<Scope> f3095e0 = new C1984g();

    /* renamed from: P */
    private final int f3096P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public final ArrayList<Scope> f3097Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public Account f3098R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public boolean f3099S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public final boolean f3100T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public final boolean f3101U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public String f3102V;
    /* access modifiers changed from: private */

    /* renamed from: W */
    public String f3103W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public ArrayList<GoogleSignInOptionsExtensionParcelable> f3104X;

    static {
        C1975a aVar = new C1975a();
        aVar.mo16409c();
        aVar.mo16410d();
        f3094d0 = aVar.mo16407a();
        C1975a aVar2 = new C1975a();
        aVar2.mo16406a(f3092b0, new Scope[0]);
        aVar2.mo16407a();
    }

    GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, ArrayList<GoogleSignInOptionsExtensionParcelable> arrayList2) {
        this(i, arrayList, account, z, z2, z3, str, str2, m4523a(arrayList2));
    }

    /* renamed from: A */
    private final JSONObject m4521A() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONArray jSONArray = new JSONArray();
            Collections.sort(this.f3097Q, f3095e0);
            ArrayList<Scope> arrayList = this.f3097Q;
            int size = arrayList.size();
            int i = 0;
            while (i < size) {
                Scope scope = arrayList.get(i);
                i++;
                jSONArray.put(scope.mo16507c());
            }
            jSONObject.put("scopes", jSONArray);
            if (this.f3098R != null) {
                jSONObject.put("accountName", this.f3098R.name);
            }
            jSONObject.put("idTokenRequested", this.f3099S);
            jSONObject.put("forceCodeForRefreshToken", this.f3101U);
            jSONObject.put("serverAuthRequested", this.f3100T);
            if (!TextUtils.isEmpty(this.f3102V)) {
                jSONObject.put("serverClientId", this.f3102V);
            }
            if (!TextUtils.isEmpty(this.f3103W)) {
                jSONObject.put("hostedDomain", this.f3103W);
            }
            return jSONObject;
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public static Map<Integer, GoogleSignInOptionsExtensionParcelable> m4523a(@Nullable List<GoogleSignInOptionsExtensionParcelable> list) {
        HashMap hashMap = new HashMap();
        if (list == null) {
            return hashMap;
        }
        for (GoogleSignInOptionsExtensionParcelable googleSignInOptionsExtensionParcelable : list) {
            hashMap.put(Integer.valueOf(googleSignInOptionsExtensionParcelable.mo16426c()), googleSignInOptionsExtensionParcelable);
        }
        return hashMap;
    }

    @Nullable
    /* renamed from: b */
    public static GoogleSignInOptions m4524b(@Nullable String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        JSONObject jSONObject = new JSONObject(str);
        HashSet hashSet = new HashSet();
        JSONArray jSONArray = jSONObject.getJSONArray("scopes");
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            hashSet.add(new Scope(jSONArray.getString(i)));
        }
        String optString = jSONObject.optString("accountName", null);
        return new GoogleSignInOptions(3, new ArrayList(hashSet), !TextUtils.isEmpty(optString) ? new Account(optString, "com.google") : null, jSONObject.getBoolean("idTokenRequested"), jSONObject.getBoolean("serverAuthRequested"), jSONObject.getBoolean("forceCodeForRefreshToken"), jSONObject.optString("serverClientId", null), jSONObject.optString("hostedDomain", null), new HashMap());
    }

    /* renamed from: c */
    public Account mo16395c() {
        return this.f3098R;
    }

    /* renamed from: d */
    public ArrayList<GoogleSignInOptionsExtensionParcelable> mo16396d() {
        return this.f3104X;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004a, code lost:
        if (r3.f3098R.equals(r4.mo16395c()) != false) goto L_0x004c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0069, code lost:
        if (r3.f3102V.equals(r4.mo16400v()) != false) goto L_0x006b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean equals(java.lang.Object r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.google.android.gms.auth.api.signin.GoogleSignInOptions r4 = (com.google.android.gms.auth.api.signin.GoogleSignInOptions) r4     // Catch:{ ClassCastException -> 0x0085 }
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r3.f3104X     // Catch:{ ClassCastException -> 0x0085 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 > 0) goto L_0x0085
            java.util.ArrayList<com.google.android.gms.auth.api.signin.internal.GoogleSignInOptionsExtensionParcelable> r1 = r4.f3104X     // Catch:{ ClassCastException -> 0x0085 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 <= 0) goto L_0x0018
            goto L_0x0085
        L_0x0018:
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.f3097Q     // Catch:{ ClassCastException -> 0x0085 }
            int r1 = r1.size()     // Catch:{ ClassCastException -> 0x0085 }
            java.util.ArrayList r2 = r4.mo16399u()     // Catch:{ ClassCastException -> 0x0085 }
            int r2 = r2.size()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r2) goto L_0x0085
            java.util.ArrayList<com.google.android.gms.common.api.Scope> r1 = r3.f3097Q     // Catch:{ ClassCastException -> 0x0085 }
            java.util.ArrayList r2 = r4.mo16399u()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = r1.containsAll(r2)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != 0) goto L_0x0035
            goto L_0x0085
        L_0x0035:
            android.accounts.Account r1 = r3.f3098R     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != 0) goto L_0x0040
            android.accounts.Account r1 = r4.mo16395c()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != 0) goto L_0x0085
            goto L_0x004c
        L_0x0040:
            android.accounts.Account r1 = r3.f3098R     // Catch:{ ClassCastException -> 0x0085 }
            android.accounts.Account r2 = r4.mo16395c()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x0085
        L_0x004c:
            java.lang.String r1 = r3.f3102V     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x005f
            java.lang.String r1 = r4.mo16400v()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x0085
            goto L_0x006b
        L_0x005f:
            java.lang.String r1 = r3.f3102V     // Catch:{ ClassCastException -> 0x0085 }
            java.lang.String r2 = r4.mo16400v()     // Catch:{ ClassCastException -> 0x0085 }
            boolean r1 = r1.equals(r2)     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 == 0) goto L_0x0085
        L_0x006b:
            boolean r1 = r3.f3101U     // Catch:{ ClassCastException -> 0x0085 }
            boolean r2 = r4.mo16401w()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r2) goto L_0x0085
            boolean r1 = r3.f3099S     // Catch:{ ClassCastException -> 0x0085 }
            boolean r2 = r4.mo16403x()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r2) goto L_0x0085
            boolean r1 = r3.f3100T     // Catch:{ ClassCastException -> 0x0085 }
            boolean r4 = r4.mo16404y()     // Catch:{ ClassCastException -> 0x0085 }
            if (r1 != r4) goto L_0x0085
            r4 = 1
            return r4
        L_0x0085:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.auth.api.signin.GoogleSignInOptions.equals(java.lang.Object):boolean");
    }

    public int hashCode() {
        ArrayList arrayList = new ArrayList();
        ArrayList<Scope> arrayList2 = this.f3097Q;
        int size = arrayList2.size();
        int i = 0;
        while (i < size) {
            Scope scope = arrayList2.get(i);
            i++;
            arrayList.add(scope.mo16507c());
        }
        Collections.sort(arrayList);
        C1988a aVar = new C1988a();
        aVar.mo16434a(arrayList);
        aVar.mo16434a(this.f3098R);
        aVar.mo16434a(this.f3102V);
        aVar.mo16435a(this.f3101U);
        aVar.mo16435a(this.f3099S);
        aVar.mo16435a(this.f3100T);
        return aVar.mo16433a();
    }

    /* renamed from: u */
    public ArrayList<Scope> mo16399u() {
        return new ArrayList<>(this.f3097Q);
    }

    /* renamed from: v */
    public String mo16400v() {
        return this.f3102V;
    }

    /* renamed from: w */
    public boolean mo16401w() {
        return this.f3101U;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, android.accounts.Account, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3096P);
        C2250b.m5614c(parcel, 2, mo16399u(), false);
        C2250b.m5596a(parcel, 3, (Parcelable) mo16395c(), i, false);
        C2250b.m5605a(parcel, 4, mo16403x());
        C2250b.m5605a(parcel, 5, mo16404y());
        C2250b.m5605a(parcel, 6, mo16401w());
        C2250b.m5602a(parcel, 7, mo16400v(), false);
        C2250b.m5602a(parcel, 8, this.f3103W, false);
        C2250b.m5614c(parcel, 9, mo16396d(), false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public boolean mo16403x() {
        return this.f3099S;
    }

    /* renamed from: y */
    public boolean mo16404y() {
        return this.f3100T;
    }

    /* renamed from: z */
    public final String mo16405z() {
        return m4521A().toString();
    }

    /* renamed from: com.google.android.gms.auth.api.signin.GoogleSignInOptions$a */
    public static final class C1975a {

        /* renamed from: a */
        private Set<Scope> f3105a = new HashSet();

        /* renamed from: b */
        private boolean f3106b;

        /* renamed from: c */
        private boolean f3107c;

        /* renamed from: d */
        private boolean f3108d;

        /* renamed from: e */
        private String f3109e;

        /* renamed from: f */
        private Account f3110f;

        /* renamed from: g */
        private String f3111g;

        /* renamed from: h */
        private Map<Integer, GoogleSignInOptionsExtensionParcelable> f3112h = new HashMap();

        public C1975a() {
        }

        /* renamed from: a */
        public final C1975a mo16406a(Scope scope, Scope... scopeArr) {
            this.f3105a.add(scope);
            this.f3105a.addAll(Arrays.asList(scopeArr));
            return this;
        }

        /* renamed from: b */
        public final C1975a mo16408b() {
            this.f3105a.add(GoogleSignInOptions.f3090Z);
            return this;
        }

        /* renamed from: c */
        public final C1975a mo16409c() {
            this.f3105a.add(GoogleSignInOptions.f3091a0);
            return this;
        }

        /* renamed from: d */
        public final C1975a mo16410d() {
            this.f3105a.add(GoogleSignInOptions.f3089Y);
            return this;
        }

        /* renamed from: a */
        public final GoogleSignInOptions mo16407a() {
            if (this.f3105a.contains(GoogleSignInOptions.f3093c0) && this.f3105a.contains(GoogleSignInOptions.f3092b0)) {
                this.f3105a.remove(GoogleSignInOptions.f3092b0);
            }
            if (this.f3108d && (this.f3110f == null || !this.f3105a.isEmpty())) {
                mo16409c();
            }
            return new GoogleSignInOptions(3, new ArrayList(this.f3105a), this.f3110f, this.f3108d, this.f3106b, this.f3107c, this.f3109e, this.f3111g, this.f3112h, null);
        }

        public C1975a(@NonNull GoogleSignInOptions googleSignInOptions) {
            C2258v.m5629a(googleSignInOptions);
            this.f3105a = new HashSet(googleSignInOptions.f3097Q);
            this.f3106b = googleSignInOptions.f3100T;
            this.f3107c = googleSignInOptions.f3101U;
            this.f3108d = googleSignInOptions.f3099S;
            this.f3109e = googleSignInOptions.f3102V;
            this.f3110f = googleSignInOptions.f3098R;
            this.f3111g = googleSignInOptions.f3103W;
            this.f3112h = GoogleSignInOptions.m4523a(googleSignInOptions.f3104X);
        }
    }

    private GoogleSignInOptions(int i, ArrayList<Scope> arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map<Integer, GoogleSignInOptionsExtensionParcelable> map) {
        this.f3096P = i;
        this.f3097Q = arrayList;
        this.f3098R = account;
        this.f3099S = z;
        this.f3100T = z2;
        this.f3101U = z3;
        this.f3102V = str;
        this.f3103W = str2;
        this.f3104X = new ArrayList<>(map.values());
    }

    /* synthetic */ GoogleSignInOptions(int i, ArrayList arrayList, Account account, boolean z, boolean z2, boolean z3, String str, String str2, Map map, C1984g gVar) {
        this(3, arrayList, account, z, z2, z3, str, str2, map);
    }
}
