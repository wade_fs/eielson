package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.n */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2625n extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4339T;

    /* renamed from: U */
    private final /* synthetic */ C2538h9 f4340U;

    /* renamed from: V */
    private final /* synthetic */ C2525gd f4341V;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2625n(C2525gd gdVar, String str, C2538h9 h9Var) {
        super(gdVar);
        this.f4341V = gdVar;
        this.f4339T = str;
        this.f4340U = h9Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4341V.f4192g.getMaxUserProperties(this.f4339T, this.f4340U);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17345b() {
        this.f4340U.mo17292d(null);
    }
}
