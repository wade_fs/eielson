package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.maps.model.k */
public final class C2940k implements Parcelable.Creator<LatLngBounds> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        LatLng latLng = null;
        LatLng latLng2 = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 2) {
                latLng = (LatLng) C2248a.m5553a(parcel, a, LatLng.CREATOR);
            } else if (a2 != 3) {
                C2248a.m5550F(parcel, a);
            } else {
                latLng2 = (LatLng) C2248a.m5553a(parcel, a, LatLng.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new LatLngBounds(latLng, latLng2);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LatLngBounds[i];
    }
}
