package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.source.MediaSourceEventListener;

/* renamed from: com.google.android.exoplayer2.source.h */
/* compiled from: lambda */
public final /* synthetic */ class C1879h implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ MediaSourceEventListener.EventDispatcher f2895P;

    /* renamed from: Q */
    private final /* synthetic */ MediaSourceEventListener f2896Q;

    /* renamed from: R */
    private final /* synthetic */ MediaSourceEventListener.LoadEventInfo f2897R;

    /* renamed from: S */
    private final /* synthetic */ MediaSourceEventListener.MediaLoadData f2898S;

    public /* synthetic */ C1879h(MediaSourceEventListener.EventDispatcher eventDispatcher, MediaSourceEventListener mediaSourceEventListener, MediaSourceEventListener.LoadEventInfo loadEventInfo, MediaSourceEventListener.MediaLoadData mediaLoadData) {
        this.f2895P = eventDispatcher;
        this.f2896Q = mediaSourceEventListener;
        this.f2897R = loadEventInfo;
        this.f2898S = mediaLoadData;
    }

    public final void run() {
        this.f2895P.mo14936b(this.f2896Q, this.f2897R, this.f2898S);
    }
}
