package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.b6 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C2986b6 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzv f4983P;

    /* renamed from: Q */
    private final /* synthetic */ zzm f4984Q;

    /* renamed from: R */
    private final /* synthetic */ C3141o5 f4985R;

    C2986b6(C3141o5 o5Var, zzv zzv, zzm zzm) {
        this.f4985R = o5Var;
        this.f4983P = zzv;
        this.f4984Q = zzm;
    }

    public final void run() {
        this.f4985R.f5496a.mo19237q();
        if (this.f4983P.f5838R.mo19469a() == null) {
            this.f4985R.f5496a.mo19224b(this.f4983P, this.f4984Q);
        } else {
            this.f4985R.f5496a.mo19216a(this.f4983P, this.f4984Q);
        }
    }
}
