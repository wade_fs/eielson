package com.google.android.gms.internal.auth;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: com.google.android.gms.internal.auth.a */
public class C2365a implements IInterface {

    /* renamed from: a */
    private final IBinder f3901a;

    protected C2365a(IBinder iBinder, String str) {
        this.f3901a = iBinder;
    }

    public IBinder asBinder() {
        return this.f3901a;
    }
}
