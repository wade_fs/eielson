package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import com.google.android.gms.internal.measurement.C2589kc;

/* renamed from: com.google.android.gms.measurement.internal.b8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2988b8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f4988P;

    /* renamed from: Q */
    private final /* synthetic */ C2589kc f4989Q;

    /* renamed from: R */
    private final /* synthetic */ C3232w7 f4990R;

    C2988b8(C3232w7 w7Var, zzm zzm, C2589kc kcVar) {
        this.f4990R = w7Var;
        this.f4988P = zzm;
        this.f4989Q = kcVar;
    }

    public final void run() {
        try {
            C3228w3 d = this.f4990R.f5724d;
            if (d == null) {
                this.f4990R.mo19015l().mo19001t().mo19042a("Failed to get app instance id");
                return;
            }
            String c = d.mo19198c(this.f4988P);
            if (c != null) {
                this.f4990R.mo18884m().mo19264a(c);
                this.f4990R.mo19012g().f5614l.mo19347a(c);
            }
            this.f4990R.m9314J();
            this.f4990R.mo19011f().mo19437a(this.f4989Q, c);
        } catch (RemoteException e) {
            this.f4990R.mo19015l().mo19001t().mo19043a("Failed to get app instance id", e);
        } finally {
            this.f4990R.mo19011f().mo19437a(this.f4989Q, (String) null);
        }
    }
}
