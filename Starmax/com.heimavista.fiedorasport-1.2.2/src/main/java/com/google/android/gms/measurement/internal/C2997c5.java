package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.c5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2997c5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3081j5 f5006P;

    /* renamed from: Q */
    private final /* synthetic */ C3032f4 f5007Q;

    C2997c5(C3262z4 z4Var, C3081j5 j5Var, C3032f4 f4Var) {
        this.f5006P = j5Var;
        this.f5007Q = f4Var;
    }

    public final void run() {
        if (this.f5006P.mo19101t() == null) {
            this.f5007Q.mo19001t().mo19042a("Install Referrer Reporter is null");
        } else {
            this.f5006P.mo19101t().mo19401a();
        }
    }
}
