package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.z1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2812z1 extends C2765w1<String> {
    C2812z1(C2448c2 c2Var, String str, String str2) {
        super(c2Var, str, str2, null);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ Object mo17265a(Object obj) {
        if (obj instanceof String) {
            return (String) obj;
        }
        return null;
    }
}
