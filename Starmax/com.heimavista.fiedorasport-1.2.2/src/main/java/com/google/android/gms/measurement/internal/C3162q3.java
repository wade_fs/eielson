package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2711s9;

/* renamed from: com.google.android.gms.measurement.internal.q3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3162q3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5560a = new C3162q3();

    private C3162q3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2711s9.m7200c());
    }
}
