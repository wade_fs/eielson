package com.google.android.gms.internal.measurement;

import android.content.Context;
import android.net.Uri;

/* renamed from: com.google.android.gms.internal.measurement.c2 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2448c2 {

    /* renamed from: a */
    final Uri f4010a;

    /* renamed from: b */
    final String f4011b;

    /* renamed from: c */
    final String f4012c;

    public C2448c2(Uri uri) {
        this(null, uri, "", "", false, false, false, false, null);
    }

    /* renamed from: a */
    public final C2765w1<Long> mo17365a(String str, long j) {
        return C2765w1.m7692b(this, str, j);
    }

    private C2448c2(String str, Uri uri, String str2, String str3, boolean z, boolean z2, boolean z3, boolean z4, C2514g2<Context, Boolean> g2Var) {
        this.f4010a = uri;
        this.f4011b = str2;
        this.f4012c = str3;
    }

    /* renamed from: a */
    public final C2765w1<Boolean> mo17367a(String str, boolean z) {
        return C2765w1.m7694b(this, str, z);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.w1.a(com.google.android.gms.internal.measurement.c2, java.lang.String, double):com.google.android.gms.internal.measurement.w1
     arg types: [com.google.android.gms.internal.measurement.c2, java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.w1.a(com.google.android.gms.internal.measurement.c2, java.lang.String, long):com.google.android.gms.internal.measurement.w1
      com.google.android.gms.internal.measurement.w1.a(com.google.android.gms.internal.measurement.c2, java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1
      com.google.android.gms.internal.measurement.w1.a(com.google.android.gms.internal.measurement.c2, java.lang.String, boolean):com.google.android.gms.internal.measurement.w1
      com.google.android.gms.internal.measurement.w1.a(com.google.android.gms.internal.measurement.c2, java.lang.String, double):com.google.android.gms.internal.measurement.w1 */
    /* renamed from: a */
    public final C2765w1<Double> mo17364a(String str, double d) {
        return C2765w1.m7691b(this, str, -3.0d);
    }

    /* renamed from: a */
    public final C2765w1<String> mo17366a(String str, String str2) {
        return C2765w1.m7693b(this, str, str2);
    }
}
