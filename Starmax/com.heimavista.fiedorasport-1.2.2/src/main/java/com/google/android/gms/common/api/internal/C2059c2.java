package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.C2064e;
import com.google.android.gms.common.api.internal.C2084i;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.c2 */
public final class C2059c2 extends C2155z1<Boolean> {

    /* renamed from: b */
    private final C2084i.C2085a<?> f3260b;

    public C2059c2(C2084i.C2085a<?> aVar, C4066i<Boolean> iVar) {
        super(4, iVar);
        this.f3260b = aVar;
    }

    /* renamed from: a */
    public final /* bridge */ /* synthetic */ void mo16616a(@NonNull C2123r rVar, boolean z) {
    }

    @Nullable
    /* renamed from: b */
    public final Feature[] mo16634b(C2064e.C2065a<?> aVar) {
        C2097k1 k1Var = aVar.mo16677i().get(this.f3260b);
        if (k1Var == null) {
            return null;
        }
        return k1Var.f3357a.mo16737c();
    }

    /* renamed from: c */
    public final boolean mo16635c(C2064e.C2065a<?> aVar) {
        C2097k1 k1Var = aVar.mo16677i().get(this.f3260b);
        return k1Var != null && k1Var.f3357a.mo16738d();
    }

    /* renamed from: d */
    public final void mo16636d(C2064e.C2065a<?> aVar) {
        C2097k1 remove = aVar.mo16677i().remove(this.f3260b);
        if (remove != null) {
            remove.f3358b.mo16763a(aVar.mo16674f(), super.f3531a);
            remove.f3357a.mo16734a();
            return;
        }
        super.f3531a.mo23722b((Object) false);
    }
}
