package com.google.android.exoplayer2.audio;

import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.audio.AudioRendererEventListener;

/* renamed from: com.google.android.exoplayer2.audio.a */
/* compiled from: lambda */
public final /* synthetic */ class C1771a implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AudioRendererEventListener.EventDispatcher f2784P;

    /* renamed from: Q */
    private final /* synthetic */ Format f2785Q;

    public /* synthetic */ C1771a(AudioRendererEventListener.EventDispatcher eventDispatcher, Format format) {
        this.f2784P = eventDispatcher;
        this.f2785Q = format;
    }

    public final void run() {
        this.f2784P.mo14094a(this.f2785Q);
    }
}
