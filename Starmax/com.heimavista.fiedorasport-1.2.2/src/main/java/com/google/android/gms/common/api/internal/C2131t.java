package com.google.android.gms.common.api.internal;

import android.app.Activity;
import androidx.collection.ArraySet;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.common.api.internal.t */
public class C2131t extends C2079g2 {

    /* renamed from: U */
    private final ArraySet<C2063d2<?>> f3463U = new ArraySet<>();

    /* renamed from: V */
    private C2064e f3464V;

    private C2131t(C2080h hVar) {
        super(hVar);
        this.f3239P.mo16706a("ConnectionlessLifecycleHelper", this);
    }

    /* renamed from: a */
    public static void m5120a(Activity activity, C2064e eVar, C2063d2<?> d2Var) {
        C2080h a = LifecycleCallback.m4742a(activity);
        C2131t tVar = (C2131t) a.mo16705a("ConnectionlessLifecycleHelper", C2131t.class);
        if (tVar == null) {
            tVar = new C2131t(a);
        }
        tVar.f3464V = eVar;
        C2258v.m5630a(d2Var, "ApiKey cannot be null");
        tVar.f3463U.add(d2Var);
        eVar.mo16658a(tVar);
    }

    /* renamed from: i */
    private final void m5121i() {
        if (!this.f3463U.isEmpty()) {
            this.f3464V.mo16658a(this);
        }
    }

    /* renamed from: c */
    public void mo16609c() {
        super.mo16609c();
        m5121i();
    }

    /* renamed from: d */
    public void mo16610d() {
        super.mo16610d();
        m5121i();
    }

    /* renamed from: e */
    public void mo16611e() {
        super.mo16611e();
        this.f3464V.mo16660b(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public final void mo16691f() {
        this.f3464V.mo16662c();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public final ArraySet<C2063d2<?>> mo16778h() {
        return this.f3463U;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo16690a(ConnectionResult connectionResult, int i) {
        this.f3464V.mo16655a(connectionResult, i);
    }
}
