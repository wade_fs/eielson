package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2620m8;

/* renamed from: com.google.android.gms.measurement.internal.u0 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3203u0 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5668a = new C3203u0();

    private C3203u0() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Long.valueOf(C2620m8.m6827x());
    }
}
