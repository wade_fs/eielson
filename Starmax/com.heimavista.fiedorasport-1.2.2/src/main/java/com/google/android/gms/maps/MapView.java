package com.google.android.gms.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.StrictMode;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2890a0;
import com.google.android.gms.maps.p093i.C2892b0;
import com.google.android.gms.maps.p093i.C2895d;
import com.google.android.gms.maps.p093i.C2904i;
import java.util.ArrayList;
import java.util.List;
import p119e.p144d.p145a.p157c.p160b.C3986a;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p160b.C3993e;

public class MapView extends FrameLayout {

    /* renamed from: P */
    private final C2869b f4738P;

    /* renamed from: com.google.android.gms.maps.MapView$a */
    static class C2868a implements C2904i {

        /* renamed from: a */
        private final ViewGroup f4739a;

        /* renamed from: b */
        private final C2895d f4740b;

        /* renamed from: c */
        private View f4741c;

        public C2868a(ViewGroup viewGroup, C2895d dVar) {
            C2258v.m5629a(dVar);
            this.f4740b = dVar;
            C2258v.m5629a(viewGroup);
            this.f4739a = viewGroup;
        }

        /* renamed from: a */
        public final void mo18350a(Activity activity, Bundle bundle, Bundle bundle2) {
            throw new UnsupportedOperationException("onInflate not allowed on MapViewDelegate");
        }

        /* renamed from: b */
        public final void mo18354b(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4740b.mo18452b(bundle2);
                C2890a0.m8135a(bundle2, bundle);
                this.f4741c = (View) C3992d.m11991e(this.f4740b.getView());
                this.f4739a.removeAllViews();
                this.f4739a.addView(this.f4741c);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: c */
        public final void mo18355c() {
            try {
                this.f4740b.mo18453c();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: d */
        public final void mo18356d() {
            try {
                this.f4740b.mo18454d();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: g */
        public final void mo18357g() {
            throw new UnsupportedOperationException("onDestroyView not allowed on MapViewDelegate");
        }

        public final void onLowMemory() {
            try {
                this.f4740b.onLowMemory();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onResume() {
            try {
                this.f4740b.onResume();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onStart() {
            try {
                this.f4740b.onStart();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final View mo18349a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            throw new UnsupportedOperationException("onCreateView not allowed on MapViewDelegate");
        }

        /* renamed from: a */
        public final void mo18351a(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4740b.mo18449a(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18371a(C2885e eVar) {
            try {
                this.f4740b.mo18450a(new C2924l(this, eVar));
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18353b() {
            try {
                this.f4740b.mo18451b();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }
    }

    public MapView(Context context) {
        super(context);
        this.f4738P = new C2869b(this, context, null);
        setClickable(true);
    }

    /* renamed from: a */
    public final void mo18363a(Bundle bundle) {
        StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder(threadPolicy).permitAll().build());
        try {
            this.f4738P.mo23621a(bundle);
            if (this.f4738P.mo23619a() == null) {
                C3986a.m11965b(super);
            }
        } finally {
            StrictMode.setThreadPolicy(threadPolicy);
        }
    }

    /* renamed from: b */
    public final void mo18365b() {
        this.f4738P.mo23626d();
    }

    /* renamed from: c */
    public final void mo18367c() {
        this.f4738P.mo23627e();
    }

    /* renamed from: d */
    public final void mo18368d() {
        this.f4738P.mo23628f();
    }

    /* renamed from: e */
    public final void mo18369e() {
        this.f4738P.mo23629g();
    }

    /* renamed from: f */
    public final void mo18370f() {
        this.f4738P.mo23630h();
    }

    /* renamed from: b */
    public final void mo18366b(Bundle bundle) {
        this.f4738P.mo23624b(bundle);
    }

    public MapView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f4738P = new C2869b(this, context, GoogleMapOptions.m7989a(context, attributeSet));
        setClickable(true);
    }

    public MapView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f4738P = new C2869b(this, context, GoogleMapOptions.m7989a(context, attributeSet));
        setClickable(true);
    }

    /* renamed from: a */
    public final void mo18362a() {
        this.f4738P.mo23623b();
    }

    /* renamed from: a */
    public void mo18364a(C2885e eVar) {
        C2258v.m5635a("getMapAsync() must be called on the main thread");
        this.f4738P.mo18372a(eVar);
    }

    /* renamed from: com.google.android.gms.maps.MapView$b */
    static class C2869b extends C3986a<C2868a> {

        /* renamed from: e */
        private final ViewGroup f4742e;

        /* renamed from: f */
        private final Context f4743f;

        /* renamed from: g */
        private C3993e<C2868a> f4744g;

        /* renamed from: h */
        private final GoogleMapOptions f4745h;

        /* renamed from: i */
        private final List<C2885e> f4746i = new ArrayList();

        C2869b(ViewGroup viewGroup, Context context, GoogleMapOptions googleMapOptions) {
            this.f4742e = viewGroup;
            this.f4743f = context;
            this.f4745h = googleMapOptions;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo18361a(C3993e<C2868a> eVar) {
            this.f4744g = eVar;
            if (this.f4744g != null && mo23619a() == null) {
                try {
                    C2884d.m8123a(this.f4743f);
                    C2895d a = C2892b0.m8145a(this.f4743f).mo18442a(C3992d.m11990a(this.f4743f), this.f4745h);
                    if (a != null) {
                        this.f4744g.mo23633a(new C2868a(this.f4742e, a));
                        for (C2885e eVar2 : this.f4746i) {
                            ((C2868a) mo23619a()).mo18371a(eVar2);
                        }
                        this.f4746i.clear();
                    }
                } catch (RemoteException e) {
                    throw new C2934e(e);
                } catch (C2170d unused) {
                }
            }
        }

        /* renamed from: a */
        public final void mo18372a(C2885e eVar) {
            if (mo23619a() != null) {
                ((C2868a) mo23619a()).mo18371a(eVar);
            } else {
                this.f4746i.add(eVar);
            }
        }
    }

    public MapView(Context context, GoogleMapOptions googleMapOptions) {
        super(context);
        this.f4738P = new C2869b(this, context, googleMapOptions);
        setClickable(true);
    }
}
