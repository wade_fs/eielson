package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.pb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2668pb implements C2593l2<C2654ob> {

    /* renamed from: Q */
    private static C2668pb f4419Q = new C2668pb();

    /* renamed from: P */
    private final C2593l2<C2654ob> f4420P;

    private C2668pb(C2593l2<C2654ob> l2Var) {
        this.f4420P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7040b() {
        return ((C2654ob) f4419Q.mo17285a()).mo17803a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4420P.mo17285a();
    }

    public C2668pb() {
        this(C2579k2.m6601a(new C2698rb()));
    }
}
