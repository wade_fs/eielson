package com.google.android.gms.measurement.internal;

import com.facebook.AccessToken;

/* renamed from: com.google.android.gms.measurement.internal.l6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public class C3106l6 {

    /* renamed from: a */
    public static final String[] f5324a = {"firebase_last_notification", "first_open_time", "first_visit_time", "last_deep_link_referrer", AccessToken.USER_ID_KEY, "first_open_after_install", "lifetime_user_engagement", "session_user_engagement", "non_personalized_ads", "ga_session_number", "ga_session_id", "last_gclid", "session_number", "session_id"};

    /* renamed from: b */
    public static final String[] f5325b = {"_ln", "_fot", "_fvt", "_ldl", "_id", "_fi", "_lte", "_se", "_npa", "_sno", "_sid", "_lgclid", "_sno", "_sid"};

    /* renamed from: a */
    public static String m8850a(String str) {
        return C3155p7.m9168a(str, f5324a, f5325b);
    }
}
