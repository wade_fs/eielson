package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.pc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2669pc implements C2593l2<C2714sc> {

    /* renamed from: Q */
    private static C2669pc f4421Q = new C2669pc();

    /* renamed from: P */
    private final C2593l2<C2714sc> f4422P;

    private C2669pc(C2593l2<C2714sc> l2Var) {
        this.f4422P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7042b() {
        return ((C2714sc) f4421Q.mo17285a()).mo17845a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4422P.mo17285a();
    }

    public C2669pc() {
        this(C2579k2.m6601a(new C2699rc()));
    }
}
