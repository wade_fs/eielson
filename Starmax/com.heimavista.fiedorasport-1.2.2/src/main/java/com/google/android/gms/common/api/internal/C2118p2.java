package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.ConnectionResult;

/* renamed from: com.google.android.gms.common.api.internal.p2 */
final class C2118p2 implements C2089i1 {

    /* renamed from: a */
    private final /* synthetic */ C2110n2 f3420a;

    private C2118p2(C2110n2 n2Var) {
        this.f3420a = n2Var;
    }

    /* renamed from: a */
    public final void mo16729a(@Nullable Bundle bundle) {
        this.f3420a.f3412m.lock();
        try {
            this.f3420a.m5012a(bundle);
            ConnectionResult unused = this.f3420a.f3409j = ConnectionResult.f3156T;
            this.f3420a.m5026i();
        } finally {
            this.f3420a.f3412m.unlock();
        }
    }

    /* synthetic */ C2118p2(C2110n2 n2Var, C2114o2 o2Var) {
        this(n2Var);
    }

    /* renamed from: a */
    public final void mo16730a(@NonNull ConnectionResult connectionResult) {
        this.f3420a.f3412m.lock();
        try {
            ConnectionResult unused = this.f3420a.f3409j = connectionResult;
            this.f3420a.m5026i();
        } finally {
            this.f3420a.f3412m.unlock();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, boolean):boolean
     arg types: [com.google.android.gms.common.api.internal.n2, int]
     candidates:
      com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, com.google.android.gms.common.ConnectionResult):com.google.android.gms.common.ConnectionResult
      com.google.android.gms.common.api.internal.n2.a(int, boolean):void
      com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, android.os.Bundle):void
      com.google.android.gms.common.api.internal.n2.a(com.google.android.gms.common.api.internal.n2, boolean):boolean */
    /* renamed from: a */
    public final void mo16728a(int i, boolean z) {
        this.f3420a.f3412m.lock();
        try {
            if (!this.f3420a.f3411l && this.f3420a.f3410k != null) {
                if (this.f3420a.f3410k.mo16482w()) {
                    boolean unused = this.f3420a.f3411l = true;
                    this.f3420a.f3404e.mo16583L(i);
                    this.f3420a.f3412m.unlock();
                    return;
                }
            }
            boolean unused2 = this.f3420a.f3411l = false;
            this.f3420a.m5011a(i, z);
        } finally {
            this.f3420a.f3412m.unlock();
        }
    }
}
