package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: com.google.android.datatransport.runtime.backends.j */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C1720j implements Factory<C1719i> {

    /* renamed from: a */
    private final Provider<Context> f2705a;

    /* renamed from: b */
    private final Provider<C3971a> f2706b;

    /* renamed from: c */
    private final Provider<C3971a> f2707c;

    public C1720j(Provider<Context> aVar, Provider<C3971a> aVar2, Provider<C3971a> aVar3) {
        this.f2705a = aVar;
        this.f2706b = aVar2;
        this.f2707c = aVar3;
    }

    /* renamed from: a */
    public static C1720j m4298a(Provider<Context> aVar, Provider<C3971a> aVar2, Provider<C3971a> aVar3) {
        return new C1720j(aVar, aVar2, aVar3);
    }

    public C1719i get() {
        return new C1719i(this.f2705a.get(), this.f2706b.get(), this.f2707c.get());
    }
}
