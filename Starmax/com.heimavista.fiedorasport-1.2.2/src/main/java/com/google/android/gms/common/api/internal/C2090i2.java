package com.google.android.gms.common.api.internal;

import android.app.Dialog;
import androidx.annotation.MainThread;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiActivity;

/* renamed from: com.google.android.gms.common.api.internal.i2 */
final class C2090i2 implements Runnable {

    /* renamed from: P */
    private final C2083h2 f3344P;

    /* renamed from: Q */
    final /* synthetic */ C2079g2 f3345Q;

    C2090i2(C2079g2 g2Var, C2083h2 h2Var) {
        this.f3345Q = g2Var;
        this.f3344P = h2Var;
    }

    @MainThread
    public final void run() {
        if (this.f3345Q.f3329Q) {
            ConnectionResult a = this.f3344P.mo16717a();
            if (a.mo16481v()) {
                C2079g2 g2Var = this.f3345Q;
                g2Var.f3239P.startActivityForResult(GoogleApiActivity.m4636a(g2Var.mo16603a(), a.mo16480u(), this.f3344P.mo16718b(), false), 1);
            } else if (this.f3345Q.f3332T.mo16833c(a.mo16475c())) {
                C2079g2 g2Var2 = this.f3345Q;
                g2Var2.f3332T.mo16827a(g2Var2.mo16603a(), this.f3345Q.f3239P, a.mo16475c(), 2, this.f3345Q);
            } else if (a.mo16475c() == 18) {
                Dialog a2 = C2167b.m5249a(this.f3345Q.mo16603a(), this.f3345Q);
                C2079g2 g2Var3 = this.f3345Q;
                g2Var3.f3332T.mo16826a(g2Var3.mo16603a().getApplicationContext(), new C2094j2(this, a2));
            } else {
                this.f3345Q.mo16690a(a, this.f3344P.mo16718b());
            }
        }
    }
}
