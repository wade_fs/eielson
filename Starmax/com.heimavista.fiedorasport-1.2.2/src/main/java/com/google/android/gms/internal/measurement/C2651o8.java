package com.google.android.gms.internal.measurement;

import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;

/* renamed from: com.google.android.gms.internal.measurement.o8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2651o8 implements C2605l8 {

    /* renamed from: A */
    private static final C2765w1<Long> f4374A;

    /* renamed from: B */
    private static final C2765w1<Long> f4375B;

    /* renamed from: C */
    private static final C2765w1<Long> f4376C;

    /* renamed from: D */
    private static final C2765w1<Long> f4377D;

    /* renamed from: E */
    private static final C2765w1<Long> f4378E;

    /* renamed from: F */
    private static final C2765w1<String> f4379F;

    /* renamed from: G */
    private static final C2765w1<Long> f4380G;

    /* renamed from: a */
    private static final C2765w1<Long> f4381a;

    /* renamed from: b */
    private static final C2765w1<Long> f4382b;

    /* renamed from: c */
    private static final C2765w1<String> f4383c;

    /* renamed from: d */
    private static final C2765w1<String> f4384d;

    /* renamed from: e */
    private static final C2765w1<Long> f4385e;

    /* renamed from: f */
    private static final C2765w1<Long> f4386f;

    /* renamed from: g */
    private static final C2765w1<Long> f4387g;

    /* renamed from: h */
    private static final C2765w1<Long> f4388h;

    /* renamed from: i */
    private static final C2765w1<Long> f4389i;

    /* renamed from: j */
    private static final C2765w1<Long> f4390j;

    /* renamed from: k */
    private static final C2765w1<Long> f4391k;

    /* renamed from: l */
    private static final C2765w1<Long> f4392l;

    /* renamed from: m */
    private static final C2765w1<Long> f4393m;

    /* renamed from: n */
    private static final C2765w1<Long> f4394n;

    /* renamed from: o */
    private static final C2765w1<Long> f4395o;

    /* renamed from: p */
    private static final C2765w1<Long> f4396p;

    /* renamed from: q */
    private static final C2765w1<Long> f4397q;

    /* renamed from: r */
    private static final C2765w1<Long> f4398r;

    /* renamed from: s */
    private static final C2765w1<Long> f4399s;

    /* renamed from: t */
    private static final C2765w1<Long> f4400t;

    /* renamed from: u */
    private static final C2765w1<Long> f4401u;

    /* renamed from: v */
    private static final C2765w1<Long> f4402v;

    /* renamed from: w */
    private static final C2765w1<Long> f4403w;

    /* renamed from: x */
    private static final C2765w1<Long> f4404x;

    /* renamed from: y */
    private static final C2765w1<Long> f4405y;

    /* renamed from: z */
    private static final C2765w1<Long> f4406z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long>
     arg types: [java.lang.String, int]
     candidates:
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, double):com.google.android.gms.internal.measurement.w1<java.lang.Double>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, java.lang.String):com.google.android.gms.internal.measurement.w1<java.lang.String>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, boolean):com.google.android.gms.internal.measurement.w1<java.lang.Boolean>
      com.google.android.gms.internal.measurement.c2.a(java.lang.String, long):com.google.android.gms.internal.measurement.w1<java.lang.Long> */
    static {
        C2448c2 c2Var = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement"));
        f4381a = c2Var.mo17365a("measurement.ad_id_cache_time", 10000L);
        f4382b = c2Var.mo17365a("measurement.config.cache_time", 86400000L);
        c2Var.mo17366a("measurement.log_tag", "FA");
        f4383c = c2Var.mo17366a("measurement.config.url_authority", "app-measurement.com");
        f4384d = c2Var.mo17366a("measurement.config.url_scheme", "https");
        f4385e = c2Var.mo17365a("measurement.upload.debug_upload_interval", 1000L);
        f4386f = c2Var.mo17365a("measurement.lifetimevalue.max_currency_tracked", 4L);
        f4387g = c2Var.mo17365a("measurement.store.max_stored_events_per_app", 100000L);
        f4388h = c2Var.mo17365a("measurement.experiment.max_ids", 50L);
        f4389i = c2Var.mo17365a("measurement.audience.filter_result_max_count", 200L);
        f4390j = c2Var.mo17365a("measurement.alarm_manager.minimum_interval", (long) DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS);
        f4391k = c2Var.mo17365a("measurement.upload.minimum_delay", 500L);
        f4392l = c2Var.mo17365a("measurement.monitoring.sample_period_millis", 86400000L);
        f4393m = c2Var.mo17365a("measurement.upload.realtime_upload_interval", 10000L);
        f4394n = c2Var.mo17365a("measurement.upload.refresh_blacklisted_config_interval", 604800000L);
        c2Var.mo17365a("measurement.config.cache_time.service", 3600000L);
        f4395o = c2Var.mo17365a("measurement.service_client.idle_disconnect_millis", (long) DefaultRenderersFactory.DEFAULT_ALLOWED_VIDEO_JOINING_TIME_MS);
        c2Var.mo17366a("measurement.log_tag.service", "FA-SVC");
        f4396p = c2Var.mo17365a("measurement.upload.stale_data_deletion_interval", 86400000L);
        f4397q = c2Var.mo17365a("measurement.upload.backoff_period", 43200000L);
        f4398r = c2Var.mo17365a("measurement.upload.initial_upload_delay_time", 15000L);
        f4399s = c2Var.mo17365a("measurement.upload.interval", 3600000L);
        f4400t = c2Var.mo17365a("measurement.upload.max_bundle_size", 65536L);
        f4401u = c2Var.mo17365a("measurement.upload.max_bundles", 100L);
        f4402v = c2Var.mo17365a("measurement.upload.max_conversions_per_day", 500L);
        f4403w = c2Var.mo17365a("measurement.upload.max_error_events_per_day", 1000L);
        f4404x = c2Var.mo17365a("measurement.upload.max_events_per_bundle", 1000L);
        f4405y = c2Var.mo17365a("measurement.upload.max_events_per_day", 100000L);
        f4406z = c2Var.mo17365a("measurement.upload.max_public_events_per_day", 50000L);
        f4374A = c2Var.mo17365a("measurement.upload.max_queue_time", 2419200000L);
        f4375B = c2Var.mo17365a("measurement.upload.max_realtime_events_per_day", 10L);
        f4376C = c2Var.mo17365a("measurement.upload.max_batch_size", 65536L);
        f4377D = c2Var.mo17365a("measurement.upload.retry_count", 6L);
        f4378E = c2Var.mo17365a("measurement.upload.retry_time", 1800000L);
        f4379F = c2Var.mo17366a("measurement.upload.url", "https://app-measurement.com/a");
        f4380G = c2Var.mo17365a("measurement.upload.window_interval", 3600000L);
    }

    /* renamed from: A */
    public final long mo17683A() {
        return f4404x.mo18128b().longValue();
    }

    /* renamed from: B */
    public final String mo17684B() {
        return f4379F.mo18128b();
    }

    /* renamed from: C */
    public final long mo17685C() {
        return f4392l.mo18128b().longValue();
    }

    /* renamed from: D */
    public final long mo17686D() {
        return f4376C.mo18128b().longValue();
    }

    /* renamed from: E */
    public final long mo17687E() {
        return f4402v.mo18128b().longValue();
    }

    /* renamed from: F */
    public final long mo17688F() {
        return f4377D.mo18128b().longValue();
    }

    /* renamed from: G */
    public final long mo17689G() {
        return f4374A.mo18128b().longValue();
    }

    /* renamed from: H */
    public final long mo17690H() {
        return f4399s.mo18128b().longValue();
    }

    /* renamed from: I */
    public final long mo17691I() {
        return f4375B.mo18128b().longValue();
    }

    /* renamed from: J */
    public final long mo17692J() {
        return f4400t.mo18128b().longValue();
    }

    /* renamed from: a */
    public final long mo17693a() {
        return f4381a.mo18128b().longValue();
    }

    /* renamed from: e */
    public final long mo17694e() {
        return f4382b.mo18128b().longValue();
    }

    /* renamed from: f */
    public final String mo17695f() {
        return f4383c.mo18128b();
    }

    /* renamed from: g */
    public final String mo17696g() {
        return f4384d.mo18128b();
    }

    /* renamed from: h */
    public final long mo17697h() {
        return f4386f.mo18128b().longValue();
    }

    /* renamed from: i */
    public final long mo17698i() {
        return f4389i.mo18128b().longValue();
    }

    /* renamed from: j */
    public final long mo17699j() {
        return f4397q.mo18128b().longValue();
    }

    /* renamed from: k */
    public final long mo17700k() {
        return f4390j.mo18128b().longValue();
    }

    /* renamed from: l */
    public final long mo17701l() {
        return f4398r.mo18128b().longValue();
    }

    /* renamed from: m */
    public final long mo17702m() {
        return f4391k.mo18128b().longValue();
    }

    /* renamed from: n */
    public final long mo17703n() {
        return f4394n.mo18128b().longValue();
    }

    /* renamed from: o */
    public final long mo17704o() {
        return f4393m.mo18128b().longValue();
    }

    /* renamed from: p */
    public final long mo17705p() {
        return f4388h.mo18128b().longValue();
    }

    /* renamed from: q */
    public final long mo17706q() {
        return f4387g.mo18128b().longValue();
    }

    /* renamed from: r */
    public final long mo17707r() {
        return f4401u.mo18128b().longValue();
    }

    /* renamed from: s */
    public final long mo17708s() {
        return f4396p.mo18128b().longValue();
    }

    /* renamed from: t */
    public final long mo17709t() {
        return f4385e.mo18128b().longValue();
    }

    /* renamed from: u */
    public final long mo17710u() {
        return f4405y.mo18128b().longValue();
    }

    /* renamed from: v */
    public final long mo17711v() {
        return f4380G.mo18128b().longValue();
    }

    /* renamed from: w */
    public final long mo17712w() {
        return f4406z.mo18128b().longValue();
    }

    /* renamed from: x */
    public final long mo17713x() {
        return f4403w.mo18128b().longValue();
    }

    /* renamed from: y */
    public final long mo17714y() {
        return f4378E.mo18128b().longValue();
    }

    /* renamed from: z */
    public final long mo17715z() {
        return f4395o.mo18128b().longValue();
    }
}
