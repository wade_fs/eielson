package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.android.datatransport.cct.b.n */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1687n implements C3536d<C1669f> {
    /* renamed from: a */
    public void mo13453a(@Nullable Object obj, @NonNull Object obj2) {
        C1669f fVar = (C1669f) obj;
        C3537e eVar = (C3537e) obj2;
        if (fVar.mo13470c() != null) {
            eVar.mo22173a("clientType", fVar.mo13470c().name());
        }
        if (fVar.mo13469b() != null) {
            eVar.mo22173a("androidClientInfo", fVar.mo13469b());
        }
    }
}
