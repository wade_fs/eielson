package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.uc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2746uc implements C2593l2<C2731tc> {

    /* renamed from: Q */
    private static C2746uc f4518Q = new C2746uc();

    /* renamed from: P */
    private final C2593l2<C2731tc> f4519P;

    private C2746uc(C2593l2<C2731tc> l2Var) {
        this.f4519P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7384b() {
        return ((C2731tc) f4518Q.mo17285a()).mo17920a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4519P.mo17285a();
    }

    public C2746uc() {
        this(C2579k2.m6601a(new C2776wc()));
    }
}
