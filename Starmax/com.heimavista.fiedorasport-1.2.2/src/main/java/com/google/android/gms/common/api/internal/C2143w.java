package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import android.os.DeadObjectException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.C2262x;

/* renamed from: com.google.android.gms.common.api.internal.w */
public final class C2143w implements C2132t0 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public final C2136u0 f3495a;

    /* renamed from: b */
    private boolean f3496b = false;

    public C2143w(C2136u0 u0Var) {
        this.f3495a = u0Var;
    }

    /* renamed from: L */
    public final void mo16739L(int i) {
        this.f3495a.mo16782a((ConnectionResult) null);
        this.f3495a.f3483o.mo16728a(i, this.f3496b);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* renamed from: a */
    public final <A extends C2016a.C2018b, T extends C2056c<? extends C2157k, A>> T mo16740a(T t) {
        try {
            this.f3495a.f3482n.f3382y.mo16790a(t);
            C2100l0 l0Var = this.f3495a.f3482n;
            C2016a.C2027f fVar = l0Var.f3373p.get(t.mo16642h());
            C2258v.m5630a(fVar, "Appropriate Api was not requested.");
            if (fVar.mo16533c() || !this.f3495a.f3475g.containsKey(t.mo16642h())) {
                boolean z = fVar instanceof C2262x;
                C2016a.C2018b bVar = fVar;
                if (z) {
                    bVar = ((C2262x) fVar).mo16448D();
                }
                t.mo16638b(bVar);
                return t;
            }
            t.mo16640c(new Status(17));
            return t;
        } catch (DeadObjectException unused) {
            this.f3495a.mo16783a(new C2147x(this, this));
        }
    }

    /* renamed from: a */
    public final void mo16741a(ConnectionResult connectionResult, C2016a<?> aVar, boolean z) {
    }

    /* renamed from: b */
    public final void mo16743b() {
        if (this.f3496b) {
            this.f3496b = false;
            this.f3495a.mo16783a(new C2150y(this, this));
        }
    }

    /* renamed from: c */
    public final void mo16744c() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final void mo16793d() {
        if (this.f3496b) {
            this.f3496b = false;
            this.f3495a.f3482n.f3382y.mo16789a();
            mo16742a();
        }
    }

    /* renamed from: f */
    public final void mo16745f(Bundle bundle) {
    }

    /* renamed from: a */
    public final boolean mo16742a() {
        if (this.f3496b) {
            return false;
        }
        if (this.f3495a.f3482n.mo16751m()) {
            this.f3496b = true;
            for (C2129s1 s1Var : this.f3495a.f3482n.f3381x) {
                s1Var.mo16775a();
            }
            return false;
        }
        this.f3495a.mo16782a((ConnectionResult) null);
        return true;
    }
}
