package com.google.android.gms.internal.p092authapi;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import androidx.annotation.Nullable;
import com.google.android.gms.auth.api.C1951a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2219h;

/* renamed from: com.google.android.gms.internal.auth-api.e */
public final class C2360e extends C2219h<C2362g> {
    @Nullable

    /* renamed from: E */
    private final C1951a.C1952a f3898E;

    public C2360e(Context context, Looper looper, C2211e eVar, C1951a.C1952a aVar, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        super(context, looper, 68, eVar, bVar, cVar);
        this.f3898E = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.credentials.internal.ICredentialsService");
        if (queryLocalInterface instanceof C2362g) {
            return (C2362g) queryLocalInterface;
        }
        return new C2363h(iBinder);
    }

    /* renamed from: i */
    public final int mo16451i() {
        return 12800000;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public final Bundle mo16936u() {
        C1951a.C1952a aVar = this.f3898E;
        return aVar == null ? new Bundle() : aVar.mo16303a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public final String mo16453y() {
        return "com.google.android.gms.auth.api.credentials.internal.ICredentialsService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: z */
    public final String mo16454z() {
        return "com.google.android.gms.auth.api.credentials.service.START";
    }
}
