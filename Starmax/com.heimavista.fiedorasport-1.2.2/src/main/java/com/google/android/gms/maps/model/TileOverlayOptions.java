package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4035d;
import p119e.p144d.p145a.p157c.p161c.p165d.C4036e;

public final class TileOverlayOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<TileOverlayOptions> CREATOR = new C2955z();
    /* access modifiers changed from: private */

    /* renamed from: P */
    public C4035d f4901P;

    /* renamed from: Q */
    private boolean f4902Q = true;

    /* renamed from: R */
    private float f4903R;

    /* renamed from: S */
    private boolean f4904S = true;

    /* renamed from: T */
    private float f4905T = 0.0f;

    public TileOverlayOptions() {
    }

    /* renamed from: c */
    public final boolean mo18613c() {
        return this.f4904S;
    }

    /* renamed from: d */
    public final float mo18614d() {
        return this.f4905T;
    }

    /* renamed from: u */
    public final float mo18615u() {
        return this.f4903R;
    }

    /* renamed from: v */
    public final boolean mo18616v() {
        return this.f4902Q;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5594a(parcel, 2, this.f4901P.asBinder(), false);
        C2250b.m5605a(parcel, 3, mo18616v());
        C2250b.m5590a(parcel, 4, mo18615u());
        C2250b.m5605a(parcel, 5, mo18613c());
        C2250b.m5590a(parcel, 6, mo18614d());
        C2250b.m5587a(parcel, a);
    }

    TileOverlayOptions(IBinder iBinder, boolean z, float f, boolean z2, float f2) {
        this.f4901P = C4036e.m12040a(iBinder);
        if (this.f4901P != null) {
            new C2954y(this);
        }
        this.f4902Q = z;
        this.f4903R = f;
        this.f4904S = z2;
        this.f4905T = f2;
    }
}
