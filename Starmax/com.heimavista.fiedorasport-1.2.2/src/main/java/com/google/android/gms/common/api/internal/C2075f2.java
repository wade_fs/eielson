package com.google.android.gms.common.api.internal;

import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2031c;
import com.google.android.gms.common.api.C2033e;
import java.util.Map;
import java.util.Set;
import p119e.p144d.p145a.p157c.p167e.C4065h;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.common.api.internal.f2 */
public final class C2075f2 {

    /* renamed from: a */
    private final ArrayMap<C2063d2<?>, ConnectionResult> f3322a = new ArrayMap<>();

    /* renamed from: b */
    private final ArrayMap<C2063d2<?>, String> f3323b = new ArrayMap<>();

    /* renamed from: c */
    private final C4066i<Map<C2063d2<?>, String>> f3324c = new C4066i<>();

    /* renamed from: d */
    private int f3325d;

    /* renamed from: e */
    private boolean f3326e = false;

    public C2075f2(Iterable<? extends C2033e<?>> iterable) {
        for (C2033e eVar : iterable) {
            this.f3322a.put(eVar.mo16559h(), null);
        }
        this.f3325d = this.f3322a.keySet().size();
    }

    /* renamed from: a */
    public final C4065h<Map<C2063d2<?>, String>> mo16692a() {
        return this.f3324c.mo23718a();
    }

    /* renamed from: b */
    public final Set<C2063d2<?>> mo16694b() {
        return this.f3322a.keySet();
    }

    /* renamed from: a */
    public final void mo16693a(C2063d2<?> d2Var, ConnectionResult connectionResult, @Nullable String str) {
        this.f3322a.put(d2Var, connectionResult);
        this.f3323b.put(d2Var, str);
        this.f3325d--;
        if (!connectionResult.mo16482w()) {
            this.f3326e = true;
        }
        if (this.f3325d != 0) {
            return;
        }
        if (this.f3326e) {
            this.f3324c.mo23719a(new C2031c(this.f3322a));
            return;
        }
        this.f3324c.mo23720a((Boolean) this.f3323b);
    }
}
