package com.google.android.gms.dynamite;

import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p163b.C4009a;
import p119e.p144d.p145a.p157c.p161c.p163b.C4011c;

/* renamed from: com.google.android.gms.dynamite.j */
public final class C2353j extends C4009a implements C2352i {
    C2353j(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.dynamite.IDynamiteLoader");
    }

    /* renamed from: G */
    public final int mo17148G() {
        Parcel a = mo23642a(6, mo23641a());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    /* renamed from: a */
    public final C3988b mo17150a(C3988b bVar, String str, int i) {
        Parcel a = mo23641a();
        C4011c.m12012a(a, bVar);
        a.writeString(str);
        a.writeInt(i);
        Parcel a2 = mo23642a(2, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: b */
    public final int mo17151b(C3988b bVar, String str, boolean z) {
        Parcel a = mo23641a();
        C4011c.m12012a(a, bVar);
        a.writeString(str);
        C4011c.m12014a(a, z);
        Parcel a2 = mo23642a(3, a);
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    /* renamed from: a */
    public final int mo17149a(C3988b bVar, String str, boolean z) {
        Parcel a = mo23641a();
        C4011c.m12012a(a, bVar);
        a.writeString(str);
        C4011c.m12014a(a, z);
        Parcel a2 = mo23642a(5, a);
        int readInt = a2.readInt();
        a2.recycle();
        return readInt;
    }

    /* renamed from: b */
    public final C3988b mo17152b(C3988b bVar, String str, int i) {
        Parcel a = mo23641a();
        C4011c.m12012a(a, bVar);
        a.writeString(str);
        a.writeInt(i);
        Parcel a2 = mo23642a(4, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }
}
