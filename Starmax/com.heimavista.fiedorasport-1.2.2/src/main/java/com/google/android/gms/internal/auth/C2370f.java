package com.google.android.gms.internal.auth;

import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Looper;
import android.text.TextUtils;
import com.google.android.gms.auth.api.C1959b;
import com.google.android.gms.auth.api.C1960c;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.common.internal.C2219h;

/* renamed from: com.google.android.gms.internal.auth.f */
public final class C2370f extends C2219h<C2371g> {

    /* renamed from: E */
    private final Bundle f3902E;

    public C2370f(Context context, Looper looper, C2211e eVar, C1960c cVar, C2036f.C2038b bVar, C2036f.C2039c cVar2) {
        super(context, looper, 16, eVar, bVar, cVar2);
        if (cVar == null) {
            this.f3902E = new Bundle();
            return;
        }
        throw new NoSuchMethodError();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ IInterface mo16449a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.auth.api.internal.IAuthService");
        if (queryLocalInterface instanceof C2371g) {
            return (C2371g) queryLocalInterface;
        }
        return new C2372h(iBinder);
    }

    /* renamed from: i */
    public final int mo16451i() {
        return C2176f.f3566a;
    }

    /* renamed from: l */
    public final boolean mo16927l() {
        C2211e C = mo16976C();
        return !TextUtils.isEmpty(C.mo16959b()) && !C.mo16957a(C1959b.f3027c).isEmpty();
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public final Bundle mo16936u() {
        return this.f3902E;
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public final String mo16453y() {
        return "com.google.android.gms.auth.api.internal.IAuthService";
    }

    /* access modifiers changed from: protected */
    /* renamed from: z */
    public final String mo16454z() {
        return "com.google.android.gms.auth.service.START";
    }
}
