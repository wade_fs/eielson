package com.google.android.gms.base;

public final class R$attr {
    public static final int buttonSize = 2130968704;
    public static final int circleCrop = 2130968753;
    public static final int colorScheme = 2130968786;
    public static final int imageAspectRatio = 2130969017;
    public static final int imageAspectRatioAdjust = 2130969018;
    public static final int scopeUris = 2130969337;

    private R$attr() {
    }
}
