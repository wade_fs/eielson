package com.google.android.exoplayer2.video;

import android.view.Surface;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.b */
/* compiled from: lambda */
public final /* synthetic */ class C1943b implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2948P;

    /* renamed from: Q */
    private final /* synthetic */ Surface f2949Q;

    public /* synthetic */ C1943b(VideoRendererEventListener.EventDispatcher eventDispatcher, Surface surface) {
        this.f2948P = eventDispatcher;
        this.f2949Q = surface;
    }

    public final void run() {
        this.f2948P.mo16267a(this.f2949Q);
    }
}
