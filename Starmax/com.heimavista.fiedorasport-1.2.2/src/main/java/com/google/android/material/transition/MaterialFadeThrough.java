package com.google.android.material.transition;

import android.content.Context;
import android.transition.Transition;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import com.google.android.material.animation.AnimationUtils;

@RequiresApi(21)
public class MaterialFadeThrough extends MaterialTransitionSet<FadeThrough> {
    private static final float DEFAULT_START_SCALE = 0.92f;

    private MaterialFadeThrough() {
        setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
    }

    @NonNull
    public static MaterialFadeThrough create(@NonNull Context context) {
        MaterialFadeThrough materialFadeThrough = new MaterialFadeThrough();
        super.initialize(context);
        return materialFadeThrough;
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public Transition getDefaultSecondaryTransition() {
        Scale scale = new Scale();
        scale.setMode(1);
        scale.setIncomingStartScale(DEFAULT_START_SCALE);
        return scale;
    }

    @Nullable
    public /* bridge */ /* synthetic */ Transition getSecondaryTransition() {
        return super.getSecondaryTransition();
    }

    public /* bridge */ /* synthetic */ void setSecondaryTransition(@Nullable Transition transition) {
        super.setSecondaryTransition(transition);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public FadeThrough getDefaultPrimaryTransition() {
        return new FadeThrough();
    }
}
