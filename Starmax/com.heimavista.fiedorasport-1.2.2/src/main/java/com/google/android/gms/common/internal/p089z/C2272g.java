package com.google.android.gms.common.internal.p089z;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.api.internal.C2060d;

/* renamed from: com.google.android.gms.common.internal.z.g */
final class C2272g extends C2267b {

    /* renamed from: a */
    private final C2060d<Status> f3768a;

    public C2272g(C2060d<Status> dVar) {
        this.f3768a = dVar;
    }

    /* renamed from: K */
    public final void mo17048K(int i) {
        this.f3768a.mo16637a(new Status(i));
    }
}
