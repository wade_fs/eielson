package com.google.android.gms.internal.measurement;

import android.database.ContentObserver;
import android.os.Handler;

/* renamed from: com.google.android.gms.internal.measurement.p1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2658p1 extends ContentObserver {
    C2658p1(C2628n1 n1Var, Handler handler) {
        super(null);
    }

    public final void onChange(boolean z) {
        C2765w1.m7695c();
    }
}
