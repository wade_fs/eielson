package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2477e0;
import com.google.android.gms.internal.measurement.C2510g0;

/* renamed from: com.google.android.gms.measurement.internal.fa */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final /* synthetic */ class C3038fa {

    /* renamed from: a */
    static final /* synthetic */ int[] f5140a = new int[C2510g0.C2511a.values().length];

    /* renamed from: b */
    static final /* synthetic */ int[] f5141b = new int[C2477e0.C2479b.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(23:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|(3:27|28|30)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
    static {
        /*
            com.google.android.gms.internal.measurement.e0$b[] r0 = com.google.android.gms.internal.measurement.C2477e0.C2479b.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.google.android.gms.measurement.internal.C3038fa.f5141b = r0
            r0 = 1
            int[] r1 = com.google.android.gms.measurement.internal.C3038fa.f5141b     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.google.android.gms.internal.measurement.e0$b r2 = com.google.android.gms.internal.measurement.C2477e0.C2479b.LESS_THAN     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            r1 = 2
            int[] r2 = com.google.android.gms.measurement.internal.C3038fa.f5141b     // Catch:{ NoSuchFieldError -> 0x001f }
            com.google.android.gms.internal.measurement.e0$b r3 = com.google.android.gms.internal.measurement.C2477e0.C2479b.GREATER_THAN     // Catch:{ NoSuchFieldError -> 0x001f }
            int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            r2 = 3
            int[] r3 = com.google.android.gms.measurement.internal.C3038fa.f5141b     // Catch:{ NoSuchFieldError -> 0x002a }
            com.google.android.gms.internal.measurement.e0$b r4 = com.google.android.gms.internal.measurement.C2477e0.C2479b.EQUAL     // Catch:{ NoSuchFieldError -> 0x002a }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            r3 = 4
            int[] r4 = com.google.android.gms.measurement.internal.C3038fa.f5141b     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.google.android.gms.internal.measurement.e0$b r5 = com.google.android.gms.internal.measurement.C2477e0.C2479b.BETWEEN     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            com.google.android.gms.internal.measurement.g0$a[] r4 = com.google.android.gms.internal.measurement.C2510g0.C2511a.values()
            int r4 = r4.length
            int[] r4 = new int[r4]
            com.google.android.gms.measurement.internal.C3038fa.f5140a = r4
            int[] r4 = com.google.android.gms.measurement.internal.C3038fa.f5140a     // Catch:{ NoSuchFieldError -> 0x0048 }
            com.google.android.gms.internal.measurement.g0$a r5 = com.google.android.gms.internal.measurement.C2510g0.C2511a.REGEXP     // Catch:{ NoSuchFieldError -> 0x0048 }
            int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
            r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
        L_0x0048:
            int[] r0 = com.google.android.gms.measurement.internal.C3038fa.f5140a     // Catch:{ NoSuchFieldError -> 0x0052 }
            com.google.android.gms.internal.measurement.g0$a r4 = com.google.android.gms.internal.measurement.C2510g0.C2511a.BEGINS_WITH     // Catch:{ NoSuchFieldError -> 0x0052 }
            int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
            r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
        L_0x0052:
            int[] r0 = com.google.android.gms.measurement.internal.C3038fa.f5140a     // Catch:{ NoSuchFieldError -> 0x005c }
            com.google.android.gms.internal.measurement.g0$a r1 = com.google.android.gms.internal.measurement.C2510g0.C2511a.ENDS_WITH     // Catch:{ NoSuchFieldError -> 0x005c }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
        L_0x005c:
            int[] r0 = com.google.android.gms.measurement.internal.C3038fa.f5140a     // Catch:{ NoSuchFieldError -> 0x0066 }
            com.google.android.gms.internal.measurement.g0$a r1 = com.google.android.gms.internal.measurement.C2510g0.C2511a.PARTIAL     // Catch:{ NoSuchFieldError -> 0x0066 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
            r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
        L_0x0066:
            int[] r0 = com.google.android.gms.measurement.internal.C3038fa.f5140a     // Catch:{ NoSuchFieldError -> 0x0071 }
            com.google.android.gms.internal.measurement.g0$a r1 = com.google.android.gms.internal.measurement.C2510g0.C2511a.EXACT     // Catch:{ NoSuchFieldError -> 0x0071 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
        L_0x0071:
            int[] r0 = com.google.android.gms.measurement.internal.C3038fa.f5140a     // Catch:{ NoSuchFieldError -> 0x007c }
            com.google.android.gms.internal.measurement.g0$a r1 = com.google.android.gms.internal.measurement.C2510g0.C2511a.IN_LIST     // Catch:{ NoSuchFieldError -> 0x007c }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
            r2 = 6
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
        L_0x007c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3038fa.<clinit>():void");
    }
}
