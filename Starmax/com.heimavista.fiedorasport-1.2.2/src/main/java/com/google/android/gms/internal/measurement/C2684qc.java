package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.qc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2684qc implements C2639nc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4455a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.integration.disable_firebase_instance_id", false);

    /* renamed from: a */
    public final boolean mo17783a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17784e() {
        return f4455a.mo18128b().booleanValue();
    }
}
