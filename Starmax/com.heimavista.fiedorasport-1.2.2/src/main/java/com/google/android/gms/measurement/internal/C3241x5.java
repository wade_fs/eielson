package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.x5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3241x5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzkq f5751P;

    /* renamed from: Q */
    private final /* synthetic */ zzm f5752Q;

    /* renamed from: R */
    private final /* synthetic */ C3141o5 f5753R;

    C3241x5(C3141o5 o5Var, zzkq zzkq, zzm zzm) {
        this.f5753R = o5Var;
        this.f5751P = zzkq;
        this.f5752Q = zzm;
    }

    public final void run() {
        this.f5753R.f5496a.mo19237q();
        if (this.f5751P.mo19469a() == null) {
            this.f5753R.f5496a.mo19221b(this.f5751P, this.f5752Q);
        } else {
            this.f5753R.f5496a.mo19213a(this.f5751P, this.f5752Q);
        }
    }
}
