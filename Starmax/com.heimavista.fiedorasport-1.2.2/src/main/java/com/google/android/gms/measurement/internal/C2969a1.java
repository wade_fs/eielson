package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.a1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2969a1 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ long f4940P;

    /* renamed from: Q */
    private final /* synthetic */ C3257z f4941Q;

    C2969a1(C3257z zVar, long j) {
        this.f4941Q = zVar;
        this.f4940P = j;
    }

    public final void run() {
        this.f4941Q.m9396b(this.f4940P);
    }
}
