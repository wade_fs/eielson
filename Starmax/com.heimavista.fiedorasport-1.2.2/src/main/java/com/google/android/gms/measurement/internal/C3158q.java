package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2620m8;

/* renamed from: com.google.android.gms.measurement.internal.q */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3158q implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5556a = new C3158q();

    private C3158q() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Long.valueOf(C2620m8.m6824u());
    }
}
