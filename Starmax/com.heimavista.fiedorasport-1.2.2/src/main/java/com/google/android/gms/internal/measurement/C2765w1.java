package com.google.android.gms.internal.measurement;

import android.annotation.SuppressLint;
import android.content.Context;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.google.android.gms.internal.measurement.w1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public abstract class C2765w1<T> {

    /* renamed from: f */
    private static final Object f4547f = new Object();
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: g */
    private static Context f4548g;

    /* renamed from: h */
    private static C2593l2<C2497f2<C2703s1>> f4549h;

    /* renamed from: i */
    private static final AtomicInteger f4550i = new AtomicInteger();

    /* renamed from: a */
    private final C2448c2 f4551a;

    /* renamed from: b */
    private final String f4552b;

    /* renamed from: c */
    private final T f4553c;

    /* renamed from: d */
    private volatile int f4554d;

    /* renamed from: e */
    private volatile T f4555e;

    private C2765w1(C2448c2 c2Var, String str, T t) {
        this.f4554d = -1;
        if (c2Var.f4010a != null) {
            this.f4551a = c2Var;
            this.f4552b = str;
            this.f4553c = t;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    /* renamed from: a */
    public static void m7690a(Context context) {
        synchronized (f4547f) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (f4548g != context) {
                C2530h1.m6434d();
                C2432b2.m6043a();
                C2628n1.m6864a();
                f4550i.incrementAndGet();
                f4548g = context;
                f4549h = C2579k2.m6600a(C2750v1.f4522P);
            }
        }
    }

    /* renamed from: c */
    static void m7695c() {
        f4550i.incrementAndGet();
    }

    /* renamed from: d */
    static final /* synthetic */ C2497f2 m7696d() {
        new C2688r1();
        return C2688r1.m7115a(f4548g);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract T mo17265a(Object obj);

    /* JADX WARNING: Removed duplicated region for block: B:36:0x009b  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x009c  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00c9  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T mo18128b() {
        /*
            r6 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = com.google.android.gms.internal.measurement.C2765w1.f4550i
            int r0 = r0.get()
            int r1 = r6.f4554d
            if (r1 >= r0) goto L_0x00f8
            monitor-enter(r6)
            int r1 = r6.f4554d     // Catch:{ all -> 0x00f5 }
            if (r1 >= r0) goto L_0x00f3
            android.content.Context r1 = com.google.android.gms.internal.measurement.C2765w1.f4548g     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x00eb
            android.content.Context r1 = com.google.android.gms.internal.measurement.C2765w1.f4548g     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.n1 r1 = com.google.android.gms.internal.measurement.C2628n1.m6863a(r1)     // Catch:{ all -> 0x00f5 }
            java.lang.String r2 = "gms:phenotype:phenotype_flag:debug_bypass_phenotype"
            java.lang.Object r1 = r1.mo17308a(r2)     // Catch:{ all -> 0x00f5 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0031
            java.util.regex.Pattern r2 = com.google.android.gms.internal.measurement.C2463d1.f4036c     // Catch:{ all -> 0x00f5 }
            java.util.regex.Matcher r1 = r2.matcher(r1)     // Catch:{ all -> 0x00f5 }
            boolean r1 = r1.matches()     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0031
            r1 = 1
            goto L_0x0032
        L_0x0031:
            r1 = 0
        L_0x0032:
            r2 = 0
            if (r1 != 0) goto L_0x006f
            com.google.android.gms.internal.measurement.c2 r1 = r6.f4551a     // Catch:{ all -> 0x00f5 }
            android.net.Uri r1 = r1.f4010a     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0058
            android.content.Context r1 = com.google.android.gms.internal.measurement.C2765w1.f4548g     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.c2 r3 = r6.f4551a     // Catch:{ all -> 0x00f5 }
            android.net.Uri r3 = r3.f4010a     // Catch:{ all -> 0x00f5 }
            boolean r1 = com.google.android.gms.internal.measurement.C2735u1.m7368a(r1, r3)     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0056
            android.content.Context r1 = com.google.android.gms.internal.measurement.C2765w1.f4548g     // Catch:{ all -> 0x00f5 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.c2 r3 = r6.f4551a     // Catch:{ all -> 0x00f5 }
            android.net.Uri r3 = r3.f4010a     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.h1 r1 = com.google.android.gms.internal.measurement.C2530h1.m6433a(r1, r3)     // Catch:{ all -> 0x00f5 }
            goto L_0x005e
        L_0x0056:
            r1 = r2
            goto L_0x005e
        L_0x0058:
            android.content.Context r1 = com.google.android.gms.internal.measurement.C2765w1.f4548g     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.b2 r1 = com.google.android.gms.internal.measurement.C2432b2.m6042a(r1, r2)     // Catch:{ all -> 0x00f5 }
        L_0x005e:
            if (r1 == 0) goto L_0x0098
            java.lang.String r3 = r6.mo18127a()     // Catch:{ all -> 0x00f5 }
            java.lang.Object r1 = r1.mo17308a(r3)     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0098
            java.lang.Object r1 = r6.mo17265a(r1)     // Catch:{ all -> 0x00f5 }
            goto L_0x0099
        L_0x006f:
            java.lang.String r1 = "PhenotypeFlag"
            r3 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r3)     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x0098
            java.lang.String r1 = "PhenotypeFlag"
            java.lang.String r3 = "Bypass reading Phenotype values for flag: "
            java.lang.String r4 = r6.mo18127a()     // Catch:{ all -> 0x00f5 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x00f5 }
            int r5 = r4.length()     // Catch:{ all -> 0x00f5 }
            if (r5 == 0) goto L_0x008f
            java.lang.String r3 = r3.concat(r4)     // Catch:{ all -> 0x00f5 }
            goto L_0x0095
        L_0x008f:
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x00f5 }
            r4.<init>(r3)     // Catch:{ all -> 0x00f5 }
            r3 = r4
        L_0x0095:
            android.util.Log.d(r1, r3)     // Catch:{ all -> 0x00f5 }
        L_0x0098:
            r1 = r2
        L_0x0099:
            if (r1 == 0) goto L_0x009c
            goto L_0x00bb
        L_0x009c:
            android.content.Context r1 = com.google.android.gms.internal.measurement.C2765w1.f4548g     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.n1 r1 = com.google.android.gms.internal.measurement.C2628n1.m6863a(r1)     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.c2 r3 = r6.f4551a     // Catch:{ all -> 0x00f5 }
            java.lang.String r3 = r3.f4011b     // Catch:{ all -> 0x00f5 }
            java.lang.String r3 = r6.m7689a(r3)     // Catch:{ all -> 0x00f5 }
            java.lang.Object r1 = r1.mo17308a(r3)     // Catch:{ all -> 0x00f5 }
            if (r1 == 0) goto L_0x00b5
            java.lang.Object r1 = r6.mo17265a(r1)     // Catch:{ all -> 0x00f5 }
            goto L_0x00b6
        L_0x00b5:
            r1 = r2
        L_0x00b6:
            if (r1 == 0) goto L_0x00b9
            goto L_0x00bb
        L_0x00b9:
            T r1 = r6.f4553c     // Catch:{ all -> 0x00f5 }
        L_0x00bb:
            com.google.android.gms.internal.measurement.l2<com.google.android.gms.internal.measurement.f2<com.google.android.gms.internal.measurement.s1>> r3 = com.google.android.gms.internal.measurement.C2765w1.f4549h     // Catch:{ all -> 0x00f5 }
            java.lang.Object r3 = r3.mo17285a()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.f2 r3 = (com.google.android.gms.internal.measurement.C2497f2) r3     // Catch:{ all -> 0x00f5 }
            boolean r4 = r3.mo17397a()     // Catch:{ all -> 0x00f5 }
            if (r4 == 0) goto L_0x00e6
            java.lang.Object r1 = r3.mo17398e()     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.s1 r1 = (com.google.android.gms.internal.measurement.C2703s1) r1     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.c2 r3 = r6.f4551a     // Catch:{ all -> 0x00f5 }
            android.net.Uri r3 = r3.f4010a     // Catch:{ all -> 0x00f5 }
            com.google.android.gms.internal.measurement.c2 r4 = r6.f4551a     // Catch:{ all -> 0x00f5 }
            java.lang.String r4 = r4.f4012c     // Catch:{ all -> 0x00f5 }
            java.lang.String r5 = r6.f4552b     // Catch:{ all -> 0x00f5 }
            java.lang.String r1 = r1.mo17872a(r3, r2, r4, r5)     // Catch:{ all -> 0x00f5 }
            if (r1 != 0) goto L_0x00e2
            T r1 = r6.f4553c     // Catch:{ all -> 0x00f5 }
            goto L_0x00e6
        L_0x00e2:
            java.lang.Object r1 = r6.mo17265a(r1)     // Catch:{ all -> 0x00f5 }
        L_0x00e6:
            r6.f4555e = r1     // Catch:{ all -> 0x00f5 }
            r6.f4554d = r0     // Catch:{ all -> 0x00f5 }
            goto L_0x00f3
        L_0x00eb:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x00f5 }
            java.lang.String r1 = "Must call PhenotypeFlag.init() first"
            r0.<init>(r1)     // Catch:{ all -> 0x00f5 }
            throw r0     // Catch:{ all -> 0x00f5 }
        L_0x00f3:
            monitor-exit(r6)     // Catch:{ all -> 0x00f5 }
            goto L_0x00f8
        L_0x00f5:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x00f5 }
            throw r0
        L_0x00f8:
            T r0 = r6.f4555e
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2765w1.mo18128b():java.lang.Object");
    }

    /* synthetic */ C2765w1(C2448c2 c2Var, String str, Object obj, C2796y1 y1Var) {
        this(c2Var, str, obj);
    }

    /* renamed from: a */
    private final String m7689a(String str) {
        if (str != null && str.isEmpty()) {
            return this.f4552b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.f4552b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    /* renamed from: a */
    public final String mo18127a() {
        return m7689a(this.f4551a.f4012c);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static C2765w1<Long> m7692b(C2448c2 c2Var, String str, long j) {
        return new C2796y1(c2Var, str, Long.valueOf(j));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static C2765w1<Boolean> m7694b(C2448c2 c2Var, String str, boolean z) {
        return new C2781x1(c2Var, str, Boolean.valueOf(z));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static C2765w1<Double> m7691b(C2448c2 c2Var, String str, double d) {
        return new C2416a2(c2Var, str, Double.valueOf(d));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static C2765w1<String> m7693b(C2448c2 c2Var, String str, String str2) {
        return new C2812z1(c2Var, str, str2);
    }
}
