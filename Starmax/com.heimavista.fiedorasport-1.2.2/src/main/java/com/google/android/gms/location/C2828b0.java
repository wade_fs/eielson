package com.google.android.gms.location;

import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.internal.C2084i;
import com.google.android.gms.common.api.internal.C2095k;
import com.google.android.gms.internal.location.C2401p;
import com.google.android.gms.internal.location.zzbd;
import com.google.android.gms.location.C2826b;
import p119e.p144d.p145a.p157c.p167e.C4066i;

/* renamed from: com.google.android.gms.location.b0 */
final class C2828b0 extends C2095k<C2401p, C2833e> {

    /* renamed from: d */
    private final /* synthetic */ zzbd f4688d;

    /* renamed from: e */
    private final /* synthetic */ C2084i f4689e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2828b0(C2826b bVar, C2084i iVar, zzbd zzbd, C2084i iVar2) {
        super(iVar);
        this.f4688d = zzbd;
        this.f4689e = iVar2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final /* synthetic */ void mo16735a(C2016a.C2018b bVar, C4066i iVar) {
        ((C2401p) bVar).mo17217a(this.f4688d, this.f4689e, new C2826b.C2827a(iVar));
    }
}
