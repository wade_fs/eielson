package com.google.android.gms.common.util;

import android.content.Context;
import android.util.Log;
import com.google.android.exoplayer2.C1750C;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.common.util.g */
public final class C2316g {
    static {
        new String[]{"android.", "com.android.", "dalvik.", "java.", "javax."};
    }

    /* renamed from: a */
    public static boolean m5778a(Context context, Throwable th) {
        return m5779a(context, th, C1750C.ENCODING_PCM_A_LAW);
    }

    /* renamed from: a */
    private static boolean m5779a(Context context, Throwable th, int i) {
        try {
            C2258v.m5629a(context);
            C2258v.m5629a(th);
            return false;
        } catch (Exception e) {
            Log.e("CrashUtils", "Error adding exception to DropBox!", e);
            return false;
        }
    }
}
