package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.x0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2778x0 extends C2595l4<C2778x0, C2779a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2778x0 zzf;
    private static volatile C2501f6<C2778x0> zzg;
    private int zzc;
    private int zzd = 1;
    private C2738u4<C2716t0> zze = C2595l4.m6649m();

    /* renamed from: com.google.android.gms.internal.measurement.x0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2779a extends C2595l4.C2596a<C2778x0, C2779a> implements C2769w5 {
        private C2779a() {
            super(C2778x0.zzf);
        }

        /* synthetic */ C2779a(C2657p0 p0Var) {
            this();
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.x0$b */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public enum C2780b implements C2631n4 {
        RADS(1),
        PROVISIONING(2);
        

        /* renamed from: P */
        private final int f4566P;

        static {
            new C2431b1();
        }

        private C2780b(int i) {
            this.f4566P = i;
        }

        /* renamed from: a */
        public static C2780b m7762a(int i) {
            if (i == 1) {
                return RADS;
            }
            if (i != 2) {
                return null;
            }
            return PROVISIONING;
        }

        /* renamed from: e */
        public static C2661p4 m7763e() {
            return C2447c1.f4009a;
        }

        public final String toString() {
            return "<" + C2780b.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.f4566P + " name=" + name() + '>';
        }
    }

    static {
        C2778x0 x0Var = new C2778x0();
        zzf = x0Var;
        C2595l4.m6645a(C2778x0.class, super);
    }

    private C2778x0() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2778x0();
            case 2:
                return new C2779a(null);
            case 3:
                return C2595l4.m6643a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\f\u0000\u0002\u001b", new Object[]{"zzc", "zzd", C2780b.m7763e(), "zze", C2716t0.class});
            case 4:
                return zzf;
            case 5:
                C2501f6<C2778x0> f6Var = zzg;
                if (f6Var == null) {
                    synchronized (C2778x0.class) {
                        f6Var = zzg;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzf);
                            zzg = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
