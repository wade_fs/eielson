package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.a */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2967a implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ String f4936P;

    /* renamed from: Q */
    private final /* synthetic */ long f4937Q;

    /* renamed from: R */
    private final /* synthetic */ C3257z f4938R;

    C2967a(C3257z zVar, String str, long j) {
        this.f4938R = zVar;
        this.f4936P = str;
        this.f4937Q = j;
    }

    public final void run() {
        this.f4938R.m9398c(this.f4936P, this.f4937Q);
    }
}
