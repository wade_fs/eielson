package com.google.android.material.transition;

import android.content.Context;
import android.transition.Transition;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.RestrictTo;
import androidx.core.view.GravityCompat;
import com.google.android.material.animation.AnimationUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@RequiresApi(21)
public class MaterialSharedAxis extends MaterialTransitionSet<Transition> {

    /* renamed from: X */
    public static final int f5898X = 0;

    /* renamed from: Y */
    public static final int f5899Y = 1;

    /* renamed from: Z */
    public static final int f5900Z = 2;
    private final int axis;
    private final boolean forward;

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    @Retention(RetentionPolicy.SOURCE)
    public @interface Axis {
    }

    private MaterialSharedAxis(int i, boolean z) {
        this.axis = i;
        this.forward = z;
        setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
    }

    @NonNull
    public static MaterialSharedAxis create(@NonNull Context context, int i, boolean z) {
        MaterialSharedAxis materialSharedAxis = new MaterialSharedAxis(i, z);
        super.initialize(context);
        return materialSharedAxis;
    }

    public int getAxis() {
        return this.axis;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    public Transition getDefaultPrimaryTransition() {
        int i = this.axis;
        if (i == 0) {
            return new SlideDistance(super.context, this.forward ? GravityCompat.END : GravityCompat.START);
        } else if (i == 1) {
            return new SlideDistance(super.context, this.forward ? 80 : 48);
        } else if (i == 2) {
            return new Scale(this.forward);
        } else {
            throw new IllegalArgumentException("Invalid axis: " + this.axis);
        }
    }

    /* access modifiers changed from: package-private */
    @Nullable
    public Transition getDefaultSecondaryTransition() {
        return new FadeThrough();
    }

    @NonNull
    public /* bridge */ /* synthetic */ Transition getPrimaryTransition() {
        return super.getPrimaryTransition();
    }

    @Nullable
    public /* bridge */ /* synthetic */ Transition getSecondaryTransition() {
        return super.getSecondaryTransition();
    }

    public boolean isEntering() {
        return this.forward;
    }

    public /* bridge */ /* synthetic */ void setSecondaryTransition(@Nullable Transition transition) {
        super.setSecondaryTransition(transition);
    }
}
