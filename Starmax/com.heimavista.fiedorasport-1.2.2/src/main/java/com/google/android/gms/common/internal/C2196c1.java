package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.common.internal.c1 */
public final class C2196c1 implements Parcelable.Creator<zzr> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            if (C2248a.m5551a(a) != 1) {
                C2248a.m5550F(parcel, a);
            } else {
                i = C2248a.m5585z(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new zzr(i);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzr[i];
    }
}
