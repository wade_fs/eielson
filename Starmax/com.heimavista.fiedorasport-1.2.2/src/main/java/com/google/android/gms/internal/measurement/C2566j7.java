package com.google.android.gms.internal.measurement;

import java.lang.reflect.Field;
import java.nio.Buffer;
import java.nio.ByteOrder;
import java.security.AccessController;
import java.util.logging.Level;
import java.util.logging.Logger;
import sun.misc.Unsafe;

/* renamed from: com.google.android.gms.internal.measurement.j7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2566j7 {

    /* renamed from: a */
    private static final Logger f4257a = Logger.getLogger(C2566j7.class.getName());

    /* renamed from: b */
    private static final Unsafe f4258b = m6532c();

    /* renamed from: c */
    private static final Class<?> f4259c = C2797y2.m7827b();

    /* renamed from: d */
    private static final boolean f4260d = m6540d(Long.TYPE);

    /* renamed from: e */
    private static final boolean f4261e = m6540d(Integer.TYPE);

    /* renamed from: f */
    private static final C2569c f4262f;

    /* renamed from: g */
    private static final boolean f4263g = m6543e();

    /* renamed from: h */
    private static final boolean f4264h = m6539d();

    /* renamed from: i */
    private static final long f4265i = ((long) m6526b(byte[].class));

    /* renamed from: j */
    static final boolean f4266j = (ByteOrder.nativeOrder() == ByteOrder.BIG_ENDIAN);

    /* renamed from: com.google.android.gms.internal.measurement.j7$c */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static abstract class C2569c {

        /* renamed from: a */
        Unsafe f4267a;

        C2569c(Unsafe unsafe) {
            this.f4267a = unsafe;
        }

        /* renamed from: a */
        public abstract byte mo17595a(Object obj, long j);

        /* renamed from: a */
        public abstract void mo17596a(Object obj, long j, byte b);

        /* renamed from: a */
        public abstract void mo17597a(Object obj, long j, double d);

        /* renamed from: a */
        public abstract void mo17598a(Object obj, long j, float f);

        /* renamed from: a */
        public final void mo17603a(Object obj, long j, int i) {
            this.f4267a.putInt(obj, j, i);
        }

        /* renamed from: a */
        public abstract void mo17599a(Object obj, long j, boolean z);

        /* renamed from: b */
        public abstract boolean mo17600b(Object obj, long j);

        /* renamed from: c */
        public abstract float mo17601c(Object obj, long j);

        /* renamed from: d */
        public abstract double mo17602d(Object obj, long j);

        /* renamed from: e */
        public final int mo17605e(Object obj, long j) {
            return this.f4267a.getInt(obj, j);
        }

        /* renamed from: f */
        public final long mo17606f(Object obj, long j) {
            return this.f4267a.getLong(obj, j);
        }

        /* renamed from: a */
        public final void mo17604a(Object obj, long j, long j2) {
            this.f4267a.putLong(obj, j, j2);
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.j7$d */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class C2570d extends C2569c {
        C2570d(Unsafe unsafe) {
            super(unsafe);
        }

        /* renamed from: a */
        public final byte mo17595a(Object obj, long j) {
            return super.f4267a.getByte(obj, j);
        }

        /* renamed from: b */
        public final boolean mo17600b(Object obj, long j) {
            return super.f4267a.getBoolean(obj, j);
        }

        /* renamed from: c */
        public final float mo17601c(Object obj, long j) {
            return super.f4267a.getFloat(obj, j);
        }

        /* renamed from: d */
        public final double mo17602d(Object obj, long j) {
            return super.f4267a.getDouble(obj, j);
        }

        /* renamed from: a */
        public final void mo17596a(Object obj, long j, byte b) {
            super.f4267a.putByte(obj, j, b);
        }

        /* renamed from: a */
        public final void mo17599a(Object obj, long j, boolean z) {
            super.f4267a.putBoolean(obj, j, z);
        }

        /* renamed from: a */
        public final void mo17598a(Object obj, long j, float f) {
            super.f4267a.putFloat(obj, j, f);
        }

        /* renamed from: a */
        public final void mo17597a(Object obj, long j, double d) {
            super.f4267a.putDouble(obj, j, d);
        }
    }

    static {
        C2569c cVar;
        Class<Object[]> cls = Object[].class;
        Class<double[]> cls2 = double[].class;
        Class<float[]> cls3 = float[].class;
        Class<long[]> cls4 = long[].class;
        Class<int[]> cls5 = int[].class;
        Class<boolean[]> cls6 = boolean[].class;
        C2569c cVar2 = null;
        if (f4258b != null) {
            if (!C2797y2.m7826a()) {
                cVar2 = new C2570d(f4258b);
            } else if (f4260d) {
                cVar2 = new C2567a(f4258b);
            } else if (f4261e) {
                cVar2 = new C2568b(f4258b);
            }
        }
        f4262f = cVar2;
        m6526b(cls6);
        m6531c(cls6);
        m6526b(cls5);
        m6531c(cls5);
        m6526b(cls4);
        m6531c(cls4);
        m6526b(cls3);
        m6531c(cls3);
        m6526b(cls2);
        m6531c(cls2);
        m6526b(cls);
        m6531c(cls);
        Field f = m6545f();
        if (!(f == null || (cVar = f4262f) == null)) {
            cVar.f4267a.objectFieldOffset(f);
        }
    }

    private C2566j7() {
    }

    /* renamed from: a */
    static boolean m6525a() {
        return f4264h;
    }

    /* renamed from: b */
    static boolean m6530b() {
        return f4263g;
    }

    /* renamed from: c */
    private static int m6531c(Class<?> cls) {
        if (f4264h) {
            return f4262f.f4267a.arrayIndexScale(cls);
        }
        return -1;
    }

    /* renamed from: d */
    static float m6536d(Object obj, long j) {
        return f4262f.mo17601c(obj, j);
    }

    /* renamed from: e */
    static double m6541e(Object obj, long j) {
        return f4262f.mo17602d(obj, j);
    }

    /* renamed from: f */
    static Object m6544f(Object obj, long j) {
        return f4262f.f4267a.getObject(obj, j);
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public static byte m6550k(Object obj, long j) {
        return (byte) (m6514a(obj, -4 & j) >>> ((int) (((~j) & 3) << 3)));
    }

    /* access modifiers changed from: private */
    /* renamed from: l */
    public static byte m6551l(Object obj, long j) {
        return (byte) (m6514a(obj, -4 & j) >>> ((int) ((j & 3) << 3)));
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public static boolean m6552m(Object obj, long j) {
        return m6550k(obj, j) != 0;
    }

    /* access modifiers changed from: private */
    /* renamed from: n */
    public static boolean m6553n(Object obj, long j) {
        return m6551l(obj, j) != 0;
    }

    /* renamed from: a */
    static <T> T m6515a(Class<T> cls) {
        try {
            return f4258b.allocateInstance(cls);
        } catch (InstantiationException e) {
            throw new IllegalStateException(e);
        }
    }

    /* renamed from: b */
    private static int m6526b(Class<?> cls) {
        if (f4264h) {
            return f4262f.f4267a.arrayBaseOffset(cls);
        }
        return -1;
    }

    /* renamed from: d */
    private static boolean m6539d() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = f4258b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", Field.class);
            cls2.getMethod("arrayBaseOffset", Class.class);
            cls2.getMethod("arrayIndexScale", Class.class);
            cls2.getMethod("getInt", cls, Long.TYPE);
            cls2.getMethod("putInt", cls, Long.TYPE, Integer.TYPE);
            cls2.getMethod("getLong", cls, Long.TYPE);
            cls2.getMethod("putLong", cls, Long.TYPE, Long.TYPE);
            cls2.getMethod("getObject", cls, Long.TYPE);
            cls2.getMethod("putObject", cls, Long.TYPE, cls);
            if (C2797y2.m7826a()) {
                return true;
            }
            cls2.getMethod("getByte", cls, Long.TYPE);
            cls2.getMethod("putByte", cls, Long.TYPE, Byte.TYPE);
            cls2.getMethod("getBoolean", cls, Long.TYPE);
            cls2.getMethod("putBoolean", cls, Long.TYPE, Boolean.TYPE);
            cls2.getMethod("getFloat", cls, Long.TYPE);
            cls2.getMethod("putFloat", cls, Long.TYPE, Float.TYPE);
            cls2.getMethod("getDouble", cls, Long.TYPE);
            cls2.getMethod("putDouble", cls, Long.TYPE, Double.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = f4257a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeArrayOperations", sb.toString());
            return false;
        }
    }

    /* renamed from: e */
    private static boolean m6543e() {
        Class<Object> cls = Object.class;
        Unsafe unsafe = f4258b;
        if (unsafe == null) {
            return false;
        }
        try {
            Class<?> cls2 = unsafe.getClass();
            cls2.getMethod("objectFieldOffset", Field.class);
            cls2.getMethod("getLong", cls, Long.TYPE);
            if (m6545f() == null) {
                return false;
            }
            if (C2797y2.m7826a()) {
                return true;
            }
            cls2.getMethod("getByte", Long.TYPE);
            cls2.getMethod("putByte", Long.TYPE, Byte.TYPE);
            cls2.getMethod("getInt", Long.TYPE);
            cls2.getMethod("putInt", Long.TYPE, Integer.TYPE);
            cls2.getMethod("getLong", Long.TYPE);
            cls2.getMethod("putLong", Long.TYPE, Long.TYPE);
            cls2.getMethod("copyMemory", Long.TYPE, Long.TYPE, Long.TYPE);
            cls2.getMethod("copyMemory", cls, Long.TYPE, cls, Long.TYPE, Long.TYPE);
            return true;
        } catch (Throwable th) {
            Logger logger = f4257a;
            Level level = Level.WARNING;
            String valueOf = String.valueOf(th);
            StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 71);
            sb.append("platform method missing - proto runtime falling back to safer methods: ");
            sb.append(valueOf);
            logger.logp(level, "com.google.protobuf.UnsafeUtil", "supportsUnsafeByteBufferOperations", sb.toString());
            return false;
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.j7$a */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class C2567a extends C2569c {
        C2567a(Unsafe unsafe) {
            super(unsafe);
        }

        /* renamed from: a */
        public final byte mo17595a(Object obj, long j) {
            if (C2566j7.f4266j) {
                return C2566j7.m6550k(obj, j);
            }
            return C2566j7.m6551l(obj, j);
        }

        /* renamed from: b */
        public final boolean mo17600b(Object obj, long j) {
            if (C2566j7.f4266j) {
                return C2566j7.m6552m(obj, j);
            }
            return C2566j7.m6553n(obj, j);
        }

        /* renamed from: c */
        public final float mo17601c(Object obj, long j) {
            return Float.intBitsToFloat(mo17605e(obj, j));
        }

        /* renamed from: d */
        public final double mo17602d(Object obj, long j) {
            return Double.longBitsToDouble(mo17606f(obj, j));
        }

        /* renamed from: a */
        public final void mo17596a(Object obj, long j, byte b) {
            if (C2566j7.f4266j) {
                C2566j7.m6533c(obj, j, b);
            } else {
                C2566j7.m6537d(obj, j, b);
            }
        }

        /* renamed from: a */
        public final void mo17599a(Object obj, long j, boolean z) {
            if (C2566j7.f4266j) {
                C2566j7.m6538d(obj, j, z);
            } else {
                C2566j7.m6542e(obj, j, z);
            }
        }

        /* renamed from: a */
        public final void mo17598a(Object obj, long j, float f) {
            mo17603a(obj, j, Float.floatToIntBits(f));
        }

        /* renamed from: a */
        public final void mo17597a(Object obj, long j, double d) {
            mo17604a(obj, j, Double.doubleToLongBits(d));
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.j7$b */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static final class C2568b extends C2569c {
        C2568b(Unsafe unsafe) {
            super(unsafe);
        }

        /* renamed from: a */
        public final byte mo17595a(Object obj, long j) {
            if (C2566j7.f4266j) {
                return C2566j7.m6550k(obj, j);
            }
            return C2566j7.m6551l(obj, j);
        }

        /* renamed from: b */
        public final boolean mo17600b(Object obj, long j) {
            if (C2566j7.f4266j) {
                return C2566j7.m6552m(obj, j);
            }
            return C2566j7.m6553n(obj, j);
        }

        /* renamed from: c */
        public final float mo17601c(Object obj, long j) {
            return Float.intBitsToFloat(mo17605e(obj, j));
        }

        /* renamed from: d */
        public final double mo17602d(Object obj, long j) {
            return Double.longBitsToDouble(mo17606f(obj, j));
        }

        /* renamed from: a */
        public final void mo17596a(Object obj, long j, byte b) {
            if (C2566j7.f4266j) {
                C2566j7.m6533c(obj, j, b);
            } else {
                C2566j7.m6537d(obj, j, b);
            }
        }

        /* renamed from: a */
        public final void mo17599a(Object obj, long j, boolean z) {
            if (C2566j7.f4266j) {
                C2566j7.m6538d(obj, j, z);
            } else {
                C2566j7.m6542e(obj, j, z);
            }
        }

        /* renamed from: a */
        public final void mo17598a(Object obj, long j, float f) {
            mo17603a(obj, j, Float.floatToIntBits(f));
        }

        /* renamed from: a */
        public final void mo17597a(Object obj, long j, double d) {
            mo17604a(obj, j, Double.doubleToLongBits(d));
        }
    }

    /* renamed from: c */
    static boolean m6535c(Object obj, long j) {
        return f4262f.mo17600b(obj, j);
    }

    /* renamed from: f */
    private static Field m6545f() {
        Field a;
        if (C2797y2.m7826a() && (a = m6516a(Buffer.class, "effectiveDirectAddress")) != null) {
            return a;
        }
        Field a2 = m6516a(Buffer.class, "address");
        if (a2 == null || a2.getType() != Long.TYPE) {
            return null;
        }
        return a2;
    }

    /* renamed from: a */
    static int m6514a(Object obj, long j) {
        return f4262f.mo17605e(obj, j);
    }

    /* renamed from: b */
    static long m6527b(Object obj, long j) {
        return f4262f.mo17606f(obj, j);
    }

    /* renamed from: c */
    static Unsafe m6532c() {
        try {
            return (Unsafe) AccessController.doPrivileged(new C2551i7());
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: a */
    static void m6520a(Object obj, long j, int i) {
        f4262f.mo17603a(obj, j, i);
    }

    /* renamed from: a */
    static void m6521a(Object obj, long j, long j2) {
        f4262f.mo17604a(obj, j, j2);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m6533c(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int a = m6514a(obj, j2);
        int i = ((~((int) j)) & 3) << 3;
        m6520a(obj, j2, ((255 & b) << i) | (a & (~(255 << i))));
    }

    /* renamed from: a */
    static void m6523a(Object obj, long j, boolean z) {
        f4262f.mo17599a(obj, j, z);
    }

    /* renamed from: a */
    static void m6519a(Object obj, long j, float f) {
        f4262f.mo17598a(obj, j, f);
    }

    /* renamed from: a */
    static void m6518a(Object obj, long j, double d) {
        f4262f.mo17597a(obj, j, d);
    }

    /* renamed from: a */
    static void m6522a(Object obj, long j, Object obj2) {
        f4262f.f4267a.putObject(obj, j, obj2);
    }

    /* renamed from: a */
    static byte m6513a(byte[] bArr, long j) {
        return f4262f.mo17595a(bArr, f4265i + j);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, byte):void
     arg types: [byte[], long, byte]
     candidates:
      com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, double):void
      com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, float):void
      com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, int):void
      com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, long):void
      com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, boolean):void
      com.google.android.gms.internal.measurement.j7.c.a(java.lang.Object, long, byte):void */
    /* renamed from: a */
    static void m6524a(byte[] bArr, long j, byte b) {
        f4262f.mo17596a((Object) bArr, f4265i + j, b);
    }

    /* renamed from: a */
    private static Field m6516a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (Throwable unused) {
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public static void m6542e(Object obj, long j, boolean z) {
        m6537d(obj, j, z ? (byte) 1 : 0);
    }

    /* renamed from: d */
    private static boolean m6540d(Class<?> cls) {
        Class<byte[]> cls2 = byte[].class;
        if (!C2797y2.m7826a()) {
            return false;
        }
        try {
            Class<?> cls3 = f4259c;
            cls3.getMethod("peekLong", cls, Boolean.TYPE);
            cls3.getMethod("pokeLong", cls, Long.TYPE, Boolean.TYPE);
            cls3.getMethod("pokeInt", cls, Integer.TYPE, Boolean.TYPE);
            cls3.getMethod("peekInt", cls, Boolean.TYPE);
            cls3.getMethod("pokeByte", cls, Byte.TYPE);
            cls3.getMethod("peekByte", cls);
            cls3.getMethod("pokeByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            cls3.getMethod("peekByteArray", cls, cls2, Integer.TYPE, Integer.TYPE);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static void m6537d(Object obj, long j, byte b) {
        long j2 = -4 & j;
        int i = (((int) j) & 3) << 3;
        m6520a(obj, j2, ((255 & b) << i) | (m6514a(obj, j2) & (~(255 << i))));
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static void m6538d(Object obj, long j, boolean z) {
        m6533c(obj, j, z ? (byte) 1 : 0);
    }
}
