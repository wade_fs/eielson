package com.google.android.gms.common.api.internal;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.common.api.internal.g */
public class C2076g {

    /* renamed from: a */
    private final Object f3327a;

    public C2076g(Activity activity) {
        C2258v.m5630a(activity, "Activity must not be null");
        this.f3327a = activity;
    }

    /* renamed from: a */
    public Activity mo16695a() {
        return (Activity) this.f3327a;
    }

    /* renamed from: b */
    public FragmentActivity mo16696b() {
        return (FragmentActivity) this.f3327a;
    }

    /* renamed from: c */
    public boolean mo16697c() {
        return this.f3327a instanceof FragmentActivity;
    }

    /* renamed from: d */
    public final boolean mo16698d() {
        return this.f3327a instanceof Activity;
    }
}
