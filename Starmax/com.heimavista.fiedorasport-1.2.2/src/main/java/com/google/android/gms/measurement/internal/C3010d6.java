package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.d6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
abstract class C3010d6 extends C3034f6 {

    /* renamed from: b */
    private boolean f5049b;

    C3010d6(C3081j5 j5Var) {
        super(j5Var);
        super.f5134a.mo19085a(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: i */
    public void mo18902i() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public final void mo18903k() {
        if (!mo18906s()) {
            throw new IllegalStateException("Not initialized");
        }
    }

    /* renamed from: m */
    public final void mo18904m() {
        if (this.f5049b) {
            throw new IllegalStateException("Can't initialize twice");
        } else if (!mo18825q()) {
            super.f5134a.mo19094h();
            this.f5049b = true;
        }
    }

    /* renamed from: p */
    public final void mo18905p() {
        if (!this.f5049b) {
            mo18902i();
            super.f5134a.mo19094h();
            this.f5049b = true;
            return;
        }
        throw new IllegalStateException("Can't initialize twice");
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public abstract boolean mo18825q();

    /* access modifiers changed from: package-private */
    /* renamed from: s */
    public final boolean mo18906s() {
        return this.f5049b;
    }
}
