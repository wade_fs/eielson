package com.google.android.gms.measurement.internal;

import android.os.Bundle;
import androidx.annotation.WorkerThread;
import com.google.android.gms.internal.measurement.C2607la;
import com.google.android.gms.internal.measurement.C2711s9;

/* renamed from: com.google.android.gms.measurement.internal.g9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3049g9 {

    /* renamed from: a */
    private long f5163a;

    /* renamed from: b */
    private long f5164b;

    /* renamed from: c */
    private final C3039g f5165c = new C3085j9(this, this.f5166d.f5134a);

    /* renamed from: d */
    private final /* synthetic */ C3244x8 f5166d;

    public C3049g9(C3244x8 x8Var) {
        this.f5166d = x8Var;
        this.f5163a = x8Var.mo19017o().elapsedRealtime();
        this.f5164b = this.f5163a;
    }

    /* access modifiers changed from: private */
    @WorkerThread
    /* renamed from: c */
    public final void m8691c() {
        this.f5166d.mo18881c();
        mo19035a(false, false, this.f5166d.mo19017o().elapsedRealtime());
        this.f5166d.mo18883k().mo19414a(this.f5166d.mo19017o().elapsedRealtime());
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19034a(long j) {
        this.f5166d.mo18881c();
        this.f5165c.mo19025c();
        this.f5163a = j;
        this.f5164b = this.f5163a;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo19037b(long j) {
        this.f5165c.mo19025c();
        if (this.f5163a != 0) {
            this.f5166d.mo19012g().f5625w.mo19327a(this.f5166d.mo19012g().f5625w.mo19326a() + (j - this.f5163a));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19033a() {
        this.f5165c.mo19025c();
        this.f5163a = 0;
        this.f5164b = this.f5163a;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final long mo19036b() {
        long elapsedRealtime = this.f5166d.mo19017o().elapsedRealtime();
        long j = elapsedRealtime - this.f5164b;
        this.f5164b = elapsedRealtime;
        return j;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void
     arg types: [com.google.android.gms.measurement.internal.s7, android.os.Bundle, int]
     candidates:
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, com.google.android.gms.measurement.internal.s7, boolean):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, boolean, long):void
      com.google.android.gms.measurement.internal.r7.a(android.app.Activity, java.lang.String, java.lang.String):void
      com.google.android.gms.measurement.internal.r7.a(com.google.android.gms.measurement.internal.s7, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    @WorkerThread
    /* renamed from: a */
    public final boolean mo19035a(boolean z, boolean z2, long j) {
        this.f5166d.mo18881c();
        this.f5166d.mo18816x();
        if (!C2711s9.m7199b() || !this.f5166d.mo19013h().mo19146a(C3135o.f5425V0)) {
            j = this.f5166d.mo19017o().elapsedRealtime();
        }
        if (!C2607la.m6742b() || !this.f5166d.mo19013h().mo19146a(C3135o.f5415Q0) || this.f5166d.f5134a.mo19089c()) {
            this.f5166d.mo19012g().f5624v.mo19327a(this.f5166d.mo19017o().mo17132a());
        }
        long j2 = j - this.f5163a;
        if (z || j2 >= 1000) {
            this.f5166d.mo19012g().f5625w.mo19327a(j2);
            this.f5166d.mo19015l().mo18996B().mo19043a("Recording user engagement, ms", Long.valueOf(j2));
            Bundle bundle = new Bundle();
            bundle.putLong("_et", j2);
            C3177r7.m9192a(this.f5166d.mo18887s().mo19295B(), bundle, true);
            if (this.f5166d.mo19013h().mo19154e(this.f5166d.mo18885p().mo18800B(), C3135o.f5430Y)) {
                if (this.f5166d.mo19013h().mo19146a(C3135o.f5432Z)) {
                    if (!z2) {
                        mo19036b();
                    }
                } else if (z2) {
                    bundle.putLong("_fr", 1);
                } else {
                    mo19036b();
                }
            }
            if (!this.f5166d.mo19013h().mo19146a(C3135o.f5432Z) || !z2) {
                this.f5166d.mo18884m().mo19267a("auto", "_e", bundle);
            }
            this.f5163a = j;
            this.f5165c.mo19025c();
            this.f5165c.mo19023a(Math.max(0L, 3600000 - this.f5166d.mo19012g().f5625w.mo19326a()));
            return true;
        }
        this.f5166d.mo19015l().mo18996B().mo19043a("Screen exposed for less than 1000 ms. Event not sent. time", Long.valueOf(j2));
        return false;
    }
}
