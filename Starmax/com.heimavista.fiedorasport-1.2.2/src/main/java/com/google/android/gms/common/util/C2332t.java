package com.google.android.gms.common.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.PowerManager;
import android.os.SystemClock;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;

/* renamed from: com.google.android.gms.common.util.t */
public final class C2332t {

    /* renamed from: a */
    private static final IntentFilter f3867a = new IntentFilter("android.intent.action.BATTERY_CHANGED");

    /* renamed from: b */
    private static long f3868b;

    /* renamed from: c */
    private static float f3869c = Float.NaN;

    @TargetApi(20)
    /* renamed from: a */
    public static int m5822a(Context context) {
        int i;
        boolean z;
        if (context == null || context.getApplicationContext() == null) {
            return -1;
        }
        Intent registerReceiver = context.getApplicationContext().registerReceiver(null, f3867a);
        int i2 = 0;
        if (registerReceiver == null) {
            i = 0;
        } else {
            i = registerReceiver.getIntExtra("plugged", 0);
        }
        int i3 = (i & 7) != 0 ? 1 : 0;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return -1;
        }
        if (C2323n.m5797f()) {
            z = powerManager.isInteractive();
        } else {
            z = powerManager.isScreenOn();
        }
        if (z) {
            i2 = 2;
        }
        return i2 | i3;
    }

    /* renamed from: b */
    public static synchronized float m5823b(Context context) {
        synchronized (C2332t.class) {
            if (SystemClock.elapsedRealtime() - f3868b >= DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS || Float.isNaN(f3869c)) {
                Intent registerReceiver = context.getApplicationContext().registerReceiver(null, f3867a);
                if (registerReceiver != null) {
                    f3869c = ((float) registerReceiver.getIntExtra("level", -1)) / ((float) registerReceiver.getIntExtra("scale", -1));
                }
                f3868b = SystemClock.elapsedRealtime();
                float f = f3869c;
                return f;
            }
            float f2 = f3869c;
            return f2;
        }
    }
}
