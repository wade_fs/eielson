package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.j */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3075j implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3058h6 f5224P;

    /* renamed from: Q */
    private final /* synthetic */ C3039g f5225Q;

    C3075j(C3039g gVar, C3058h6 h6Var) {
        this.f5225Q = gVar;
        this.f5224P = h6Var;
    }

    public final void run() {
        this.f5224P.mo19018r();
        if (C3098ka.m8843a()) {
            this.f5224P.mo19014j().mo19028a(this);
            return;
        }
        boolean b = this.f5225Q.mo19024b();
        long unused = this.f5225Q.f5145c = 0;
        if (b) {
            this.f5225Q.mo19022a();
        }
    }
}
