package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.ConnectionResult;

/* renamed from: com.google.android.gms.common.api.internal.d0 */
final class C2061d0 extends C2140v0 {

    /* renamed from: b */
    private final /* synthetic */ ConnectionResult f3261b;

    /* renamed from: c */
    private final /* synthetic */ C2057c0 f3262c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2061d0(C2057c0 c0Var, C2132t0 t0Var, ConnectionResult connectionResult) {
        super(t0Var);
        this.f3262c = c0Var;
        this.f3261b = connectionResult;
    }

    /* renamed from: a */
    public final void mo16645a() {
        this.f3262c.f3258R.m5198b(this.f3261b);
    }
}
