package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;

/* renamed from: com.google.android.gms.common.api.internal.h */
public interface C2080h {
    /* renamed from: a */
    Activity mo16704a();

    /* renamed from: a */
    <T extends LifecycleCallback> T mo16705a(String str, Class cls);

    /* renamed from: a */
    void mo16706a(String str, @NonNull LifecycleCallback lifecycleCallback);

    void startActivityForResult(Intent intent, int i);
}
