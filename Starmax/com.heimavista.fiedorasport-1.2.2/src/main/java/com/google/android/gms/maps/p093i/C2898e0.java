package com.google.android.gms.maps.p093i;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import p119e.p144d.p145a.p157c.p160b.C3988b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4032a;
import p119e.p144d.p145a.p157c.p161c.p165d.C4033b;
import p119e.p144d.p145a.p157c.p161c.p165d.C4039h;
import p119e.p144d.p145a.p157c.p161c.p165d.C4043l;
import p119e.p144d.p145a.p157c.p161c.p165d.C4044m;
import p119e.p144d.p145a.p157c.p161c.p165d.C4046o;

/* renamed from: com.google.android.gms.maps.i.e0 */
public final class C2898e0 extends C4032a implements C2891b {
    C2898e0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.internal.IGoogleMapDelegate");
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [android.os.IInterface] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: F */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.google.android.gms.maps.p093i.C2903h mo18421F() {
        /*
            r4 = this;
            android.os.Parcel r0 = r4.mo23664a()
            r1 = 25
            android.os.Parcel r0 = r4.mo23665a(r1, r0)
            android.os.IBinder r1 = r0.readStrongBinder()
            if (r1 != 0) goto L_0x0012
            r1 = 0
            goto L_0x0026
        L_0x0012:
            java.lang.String r2 = "com.google.android.gms.maps.internal.IUiSettingsDelegate"
            android.os.IInterface r2 = r1.queryLocalInterface(r2)
            boolean r3 = r2 instanceof com.google.android.gms.maps.p093i.C2903h
            if (r3 == 0) goto L_0x0020
            r1 = r2
            com.google.android.gms.maps.i.h r1 = (com.google.android.gms.maps.p093i.C2903h) r1
            goto L_0x0026
        L_0x0020:
            com.google.android.gms.maps.i.z r2 = new com.google.android.gms.maps.i.z
            r2.<init>(r1)
            r1 = r2
        L_0x0026:
            r0.recycle()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.maps.p093i.C2898e0.mo18421F():com.google.android.gms.maps.i.h");
    }

    /* renamed from: a */
    public final C4046o mo18423a(PolylineOptions polylineOptions) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, polylineOptions);
        Parcel a2 = mo23665a(9, a);
        C4046o a3 = C4033b.m12037a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: b */
    public final void mo18427b(C3988b bVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, bVar);
        mo23667b(4, a);
    }

    public final void clear() {
        mo23667b(14, mo23664a());
    }

    /* renamed from: o */
    public final void mo18429o(boolean z) {
        Parcel a = mo23664a();
        C4039h.m12045a(a, z);
        mo23667b(22, a);
    }

    /* renamed from: a */
    public final C4043l mo18422a(MarkerOptions markerOptions) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, markerOptions);
        Parcel a2 = mo23665a(11, a);
        C4043l a3 = C4044m.m12058a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: a */
    public final void mo18424a(C2907l lVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, lVar);
        mo23667b(28, a);
    }

    /* renamed from: a */
    public final void mo18426a(C2913r rVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, rVar);
        mo23667b(30, a);
    }

    /* renamed from: a */
    public final void mo18425a(C2909n nVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, nVar);
        mo23667b(42, a);
    }
}
