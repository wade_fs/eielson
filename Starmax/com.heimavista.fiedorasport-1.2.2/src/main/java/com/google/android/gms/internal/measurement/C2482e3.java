package com.google.android.gms.internal.measurement;

import java.util.NoSuchElementException;

/* renamed from: com.google.android.gms.internal.measurement.e3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2482e3 extends C2515g3 {

    /* renamed from: P */
    private int f4069P = 0;

    /* renamed from: Q */
    private final int f4070Q = this.f4071R.mo17469a();

    /* renamed from: R */
    private final /* synthetic */ C2498f3 f4071R;

    C2482e3(C2498f3 f3Var) {
        this.f4071R = f3Var;
    }

    /* renamed from: a */
    public final byte mo17442a() {
        int i = this.f4069P;
        if (i < this.f4070Q) {
            this.f4069P = i + 1;
            return this.f4071R.mo17474b(i);
        }
        throw new NoSuchElementException();
    }

    public final boolean hasNext() {
        return this.f4069P < this.f4070Q;
    }
}
