package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.d5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
abstract class C2467d5 {

    /* renamed from: a */
    private static final C2467d5 f4050a = new C2500f5();

    /* renamed from: b */
    private static final C2467d5 f4051b = new C2564j5();

    private C2467d5() {
    }

    /* renamed from: a */
    static C2467d5 m6203a() {
        return f4050a;
    }

    /* renamed from: b */
    static C2467d5 m6204b() {
        return f4051b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17416a(Object obj, long j);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract <L> void mo17417a(Object obj, Object obj2, long j);
}
