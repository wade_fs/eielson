package com.google.android.exoplayer2.drm;

import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.drm.b */
/* compiled from: lambda */
public final /* synthetic */ class C1788b implements EventDispatcher.Event {

    /* renamed from: a */
    private final /* synthetic */ Exception f2802a;

    public /* synthetic */ C1788b(Exception exc) {
        this.f2802a = exc;
    }

    public final void sendTo(Object obj) {
        ((DefaultDrmSessionEventListener) obj).onDrmSessionManagerError(this.f2802a);
    }
}
