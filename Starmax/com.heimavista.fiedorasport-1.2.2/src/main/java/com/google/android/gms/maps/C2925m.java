package com.google.android.gms.maps;

import com.google.android.gms.maps.StreetViewPanoramaFragment;
import com.google.android.gms.maps.p093i.C2897e;
import com.google.android.gms.maps.p093i.C2917v;

/* renamed from: com.google.android.gms.maps.m */
final class C2925m extends C2917v {

    /* renamed from: a */
    private final /* synthetic */ C2886f f4797a;

    C2925m(StreetViewPanoramaFragment.C2870a aVar, C2886f fVar) {
        this.f4797a = fVar;
    }

    /* renamed from: a */
    public final void mo18489a(C2897e eVar) {
        this.f4797a.mo18412a(new C2887g(eVar));
    }
}
