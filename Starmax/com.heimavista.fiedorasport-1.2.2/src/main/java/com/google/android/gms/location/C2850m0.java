package com.google.android.gms.location;

import android.location.Location;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import com.google.android.gms.internal.location.C2402q;
import com.google.android.gms.internal.location.C2409x;

/* renamed from: com.google.android.gms.location.m0 */
public abstract class C2850m0 extends C2402q implements C2848l0 {
    /* renamed from: a */
    public static C2848l0 m7985a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.location.ILocationListener");
        return queryLocalInterface instanceof C2848l0 ? (C2848l0) queryLocalInterface : new C2852n0(iBinder);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo17203a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        onLocationChanged((Location) C2409x.m5938a(parcel, Location.CREATOR));
        return true;
    }
}
