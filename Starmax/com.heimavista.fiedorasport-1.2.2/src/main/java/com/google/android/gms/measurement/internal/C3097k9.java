package com.google.android.gms.measurement.internal;

import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.PersistableBundle;
import androidx.core.app.NotificationCompat;
import com.facebook.internal.NativeProtocol;
import com.google.android.gms.internal.measurement.C2489ea;
import com.google.android.gms.internal.measurement.C2518g6;

/* renamed from: com.google.android.gms.measurement.internal.k9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C3097k9 extends C3157p9 {

    /* renamed from: d */
    private final AlarmManager f5309d = ((AlarmManager) mo19016n().getSystemService(NotificationCompat.CATEGORY_ALARM));

    /* renamed from: e */
    private final C3039g f5310e;

    /* renamed from: f */
    private Integer f5311f;

    protected C3097k9(C3145o9 o9Var) {
        super(o9Var);
        this.f5310e = new C3133n9(this, o9Var.mo19239t(), o9Var);
    }

    @TargetApi(24)
    /* renamed from: v */
    private final void m8836v() {
        JobScheduler jobScheduler = (JobScheduler) mo19016n().getSystemService("jobscheduler");
        int w = m8837w();
        if (!m8839y()) {
            mo19015l().mo18996B().mo19043a("Cancelling job. JobID", Integer.valueOf(w));
        }
        jobScheduler.cancel(w);
    }

    /* renamed from: w */
    private final int m8837w() {
        if (this.f5311f == null) {
            String valueOf = String.valueOf(mo19016n().getPackageName());
            this.f5311f = Integer.valueOf((valueOf.length() != 0 ? "measurement".concat(valueOf) : new String("measurement")).hashCode());
        }
        return this.f5311f.intValue();
    }

    /* renamed from: x */
    private final PendingIntent m8838x() {
        Context n = mo19016n();
        return PendingIntent.getBroadcast(n, 0, new Intent().setClassName(n, "com.google.android.gms.measurement.AppMeasurementReceiver").setAction("com.google.android.gms.measurement.UPLOAD"), 0);
    }

    /* renamed from: y */
    private final boolean m8839y() {
        return C2489ea.m6262b() && mo19013h().mo19146a(C3135o.f5431Y0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(android.content.Context, boolean):boolean
     arg types: [android.content.Context, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(long, long):long
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, int):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.Boolean, java.lang.Boolean):boolean
      com.google.android.gms.measurement.internal.z9.a(java.util.List<java.lang.String>, java.util.List<java.lang.String>):boolean
      com.google.android.gms.measurement.internal.z9.a(android.content.Context, java.lang.String):long
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.Object):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, long):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, int):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, long):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, android.os.Bundle):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, java.lang.String):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, java.util.ArrayList<android.os.Bundle>):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, boolean):void
      com.google.android.gms.measurement.internal.z9.a(com.google.android.gms.internal.measurement.kc, byte[]):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, double):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(android.content.Context, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* renamed from: a */
    public final void mo19131a(long j) {
        mo19284q();
        mo19018r();
        Context n = mo19016n();
        if (!C3262z4.m9407a(n)) {
            mo19015l().mo18995A().mo19042a("Receiver not registered/enabled");
        }
        if (!C3267z9.m9415a(n, false)) {
            mo19015l().mo18995A().mo19042a("Service not registered/enabled");
        }
        mo19132u();
        if (m8839y()) {
            mo19015l().mo18996B().mo19043a("Scheduling upload, millis", Long.valueOf(j));
        }
        long elapsedRealtime = mo19017o().elapsedRealtime() + j;
        if (j < Math.max(0L, C3135o.f5480x.mo19389a(null).longValue()) && !this.f5310e.mo19024b()) {
            if (!m8839y()) {
                mo19015l().mo18996B().mo19042a("Scheduling upload with DelayedRunnable");
            }
            this.f5310e.mo19023a(j);
        }
        mo19018r();
        if (Build.VERSION.SDK_INT >= 24) {
            if (!m8839y()) {
                mo19015l().mo18996B().mo19042a("Scheduling upload with JobScheduler");
            }
            Context n2 = mo19016n();
            ComponentName componentName = new ComponentName(n2, "com.google.android.gms.measurement.AppMeasurementJobService");
            int w = m8837w();
            PersistableBundle persistableBundle = new PersistableBundle();
            persistableBundle.putString(NativeProtocol.WEB_DIALOG_ACTION, "com.google.android.gms.measurement.UPLOAD");
            JobInfo build = new JobInfo.Builder(w, componentName).setMinimumLatency(j).setOverrideDeadline(j << 1).setExtras(persistableBundle).build();
            if (!m8839y()) {
                mo19015l().mo18996B().mo19043a("Scheduling job. JobID", Integer.valueOf(w));
            }
            C2518g6.m6371a(n2, build, "com.google.android.gms", "UploadAlarm");
            return;
        }
        if (!m8839y()) {
            mo19015l().mo18996B().mo19042a("Scheduling upload with AlarmManager");
        }
        this.f5309d.setInexactRepeating(2, elapsedRealtime, Math.max(C3135o.f5470s.mo19389a(null).longValue(), j), m8838x());
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public final boolean mo18833t() {
        this.f5309d.cancel(m8838x());
        if (Build.VERSION.SDK_INT < 24) {
            return false;
        }
        m8836v();
        return false;
    }

    /* renamed from: u */
    public final void mo19132u() {
        mo19284q();
        if (m8839y()) {
            mo19015l().mo18996B().mo19042a("Unscheduling upload");
        }
        this.f5309d.cancel(m8838x());
        this.f5310e.mo19025c();
        if (Build.VERSION.SDK_INT >= 24) {
            m8836v();
        }
    }
}
