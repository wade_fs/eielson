package com.google.android.gms.measurement.internal;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import androidx.annotation.NonNull;
import androidx.annotation.WorkerThread;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.util.Locale;
import p119e.p144d.p145a.p157c.p158a.p159a.C3980a;

/* renamed from: com.google.android.gms.measurement.internal.s4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3185s4 extends C3010d6 {

    /* renamed from: C */
    static final Pair<String, Long> f5602C = new Pair<>("", 0L);

    /* renamed from: A */
    public C3196t4 f5603A = new C3196t4(this, "deep_link_retrieval_attempts", 0);

    /* renamed from: B */
    public final C3218v4 f5604B = new C3218v4(this, "firebase_feature_rollouts", null);

    /* renamed from: c */
    private SharedPreferences f5605c;

    /* renamed from: d */
    public C3229w4 f5606d;

    /* renamed from: e */
    public final C3196t4 f5607e = new C3196t4(this, "last_upload", 0);

    /* renamed from: f */
    public final C3196t4 f5608f = new C3196t4(this, "last_upload_attempt", 0);

    /* renamed from: g */
    public final C3196t4 f5609g = new C3196t4(this, "backoff", 0);

    /* renamed from: h */
    public final C3196t4 f5610h = new C3196t4(this, "last_delete_stale", 0);

    /* renamed from: i */
    public final C3196t4 f5611i = new C3196t4(this, "midnight_offset", 0);

    /* renamed from: j */
    public final C3196t4 f5612j = new C3196t4(this, "first_open_time", 0);

    /* renamed from: k */
    public final C3196t4 f5613k = new C3196t4(this, "app_install_time", 0);

    /* renamed from: l */
    public final C3218v4 f5614l = new C3218v4(this, "app_instance_id", null);

    /* renamed from: m */
    private String f5615m;

    /* renamed from: n */
    private boolean f5616n;

    /* renamed from: o */
    private long f5617o;

    /* renamed from: p */
    public final C3196t4 f5618p = new C3196t4(this, "time_before_start", 10000);

    /* renamed from: q */
    public final C3196t4 f5619q = new C3196t4(this, "session_timeout", 1800000);

    /* renamed from: r */
    public final C3207u4 f5620r = new C3207u4(this, "start_new_session", true);

    /* renamed from: s */
    public final C3218v4 f5621s = new C3218v4(this, "non_personalized_ads", null);

    /* renamed from: t */
    public final C3207u4 f5622t = new C3207u4(this, "use_dynamite_api", false);

    /* renamed from: u */
    public final C3207u4 f5623u = new C3207u4(this, "allow_remote_dynamite", false);

    /* renamed from: v */
    public final C3196t4 f5624v = new C3196t4(this, "last_pause_time", 0);

    /* renamed from: w */
    public final C3196t4 f5625w = new C3196t4(this, "time_active", 0);

    /* renamed from: x */
    public boolean f5626x;

    /* renamed from: y */
    public C3207u4 f5627y = new C3207u4(this, "app_backgrounded", false);

    /* renamed from: z */
    public C3207u4 f5628z = new C3207u4(this, "deep_link_retrieval_complete", false);

    C3185s4(C3081j5 j5Var) {
        super(j5Var);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: A */
    public final boolean mo19306A() {
        return this.f5605c.contains("deferred_analytics_collection");
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    @NonNull
    /* renamed from: a */
    public final Pair<String, Boolean> mo19307a(String str) {
        mo18881c();
        long elapsedRealtime = mo19017o().elapsedRealtime();
        String str2 = this.f5615m;
        if (str2 != null && elapsedRealtime < this.f5617o) {
            return new Pair<>(str2, Boolean.valueOf(this.f5616n));
        }
        this.f5617o = elapsedRealtime + mo19013h().mo19143a(str, C3135o.f5436b);
        C3980a.m11947a(true);
        try {
            C3980a.C3981a a = C3980a.m11945a(mo19016n());
            if (a != null) {
                this.f5615m = a.mo23610a();
                this.f5616n = a.mo23611b();
            }
            if (this.f5615m == null) {
                this.f5615m = "";
            }
        } catch (Exception e) {
            mo19015l().mo18995A().mo19043a("Unable to get advertising id", e);
            this.f5615m = "";
        }
        C3980a.m11947a(false);
        return new Pair<>(this.f5615m, Boolean.valueOf(this.f5616n));
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final String mo19310b(String str) {
        mo18881c();
        String str2 = (String) mo19307a(str).first;
        MessageDigest y = C3267z9.m9433y();
        if (y == null) {
            return null;
        }
        return String.format(Locale.US, "%032X", new BigInteger(1, y.digest(str2.getBytes())));
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: c */
    public final void mo19312c(String str) {
        mo18881c();
        SharedPreferences.Editor edit = mo19315t().edit();
        edit.putString("gmp_app_id", str);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: d */
    public final void mo19314d(String str) {
        mo18881c();
        SharedPreferences.Editor edit = mo19315t().edit();
        edit.putString("admob_app_id", str);
        edit.apply();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: i */
    public final void mo18902i() {
        this.f5605c = mo19016n().getSharedPreferences("com.google.android.gms.measurement.prefs", 0);
        this.f5626x = this.f5605c.getBoolean("has_been_opened", false);
        if (!this.f5626x) {
            SharedPreferences.Editor edit = this.f5605c.edit();
            edit.putBoolean("has_been_opened", true);
            edit.apply();
        }
        this.f5606d = new C3229w4(this, "health_monitor", Math.max(0L, C3135o.f5438c.mo19389a(null).longValue()));
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final boolean mo18825q() {
        return true;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: t */
    public final SharedPreferences mo19315t() {
        mo18881c();
        mo18903k();
        return this.f5605c;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: u */
    public final String mo19316u() {
        mo18881c();
        return mo19315t().getString("gmp_app_id", null);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: v */
    public final String mo19317v() {
        mo18881c();
        return mo19315t().getString("admob_app_id", null);
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: w */
    public final Boolean mo19318w() {
        mo18881c();
        if (!mo19315t().contains("use_service")) {
            return null;
        }
        return Boolean.valueOf(mo19315t().getBoolean("use_service", false));
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: x */
    public final void mo19319x() {
        mo18881c();
        Boolean y = mo19320y();
        SharedPreferences.Editor edit = mo19315t().edit();
        edit.clear();
        edit.apply();
        if (y != null) {
            mo19311b(y.booleanValue());
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: y */
    public final Boolean mo19320y() {
        mo18881c();
        if (mo19315t().contains("measurement_enabled")) {
            return Boolean.valueOf(mo19315t().getBoolean("measurement_enabled", true));
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: z */
    public final String mo19321z() {
        mo18881c();
        String string = mo19315t().getString("previous_os_version", null);
        mo19009d().mo18903k();
        String str = Build.VERSION.RELEASE;
        if (!TextUtils.isEmpty(str) && !str.equals(string)) {
            SharedPreferences.Editor edit = mo19315t().edit();
            edit.putString("previous_os_version", str);
            edit.apply();
        }
        return string;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: b */
    public final void mo19311b(boolean z) {
        mo18881c();
        SharedPreferences.Editor edit = mo19315t().edit();
        edit.putBoolean("measurement_enabled", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: c */
    public final void mo19313c(boolean z) {
        mo18881c();
        mo19015l().mo18996B().mo19043a("App measurement setting deferred collection", Boolean.valueOf(z));
        SharedPreferences.Editor edit = mo19315t().edit();
        edit.putBoolean("deferred_analytics_collection", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19308a(boolean z) {
        mo18881c();
        SharedPreferences.Editor edit = mo19315t().edit();
        edit.putBoolean("use_service", z);
        edit.apply();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo19309a(long j) {
        return j - this.f5619q.mo19326a() > this.f5624v.mo19326a();
    }
}
