package com.google.android.exoplayer2.video;

import com.google.android.exoplayer2.decoder.DecoderCounters;
import com.google.android.exoplayer2.video.VideoRendererEventListener;

/* renamed from: com.google.android.exoplayer2.video.e */
/* compiled from: lambda */
public final /* synthetic */ class C1946e implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ VideoRendererEventListener.EventDispatcher f2959P;

    /* renamed from: Q */
    private final /* synthetic */ DecoderCounters f2960Q;

    public /* synthetic */ C1946e(VideoRendererEventListener.EventDispatcher eventDispatcher, DecoderCounters decoderCounters) {
        this.f2959P = eventDispatcher;
        this.f2960Q = decoderCounters;
    }

    public final void run() {
        this.f2959P.mo16271b(this.f2960Q);
    }
}
