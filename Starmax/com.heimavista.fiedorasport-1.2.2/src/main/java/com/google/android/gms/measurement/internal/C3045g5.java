package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;
import java.lang.Thread;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicLong;

/* renamed from: com.google.android.gms.measurement.internal.g5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3045g5 extends C3010d6 {
    /* access modifiers changed from: private */

    /* renamed from: l */
    public static final AtomicLong f5150l = new AtomicLong(Long.MIN_VALUE);
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C3093k5 f5151c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C3093k5 f5152d;

    /* renamed from: e */
    private final PriorityBlockingQueue<C3057h5<?>> f5153e = new PriorityBlockingQueue<>();

    /* renamed from: f */
    private final BlockingQueue<C3057h5<?>> f5154f = new LinkedBlockingQueue();

    /* renamed from: g */
    private final Thread.UncaughtExceptionHandler f5155g = new C3069i5(this, "Thread death: Uncaught exception on worker thread");

    /* renamed from: h */
    private final Thread.UncaughtExceptionHandler f5156h = new C3069i5(this, "Thread death: Uncaught exception on network thread");
    /* access modifiers changed from: private */

    /* renamed from: i */
    public final Object f5157i = new Object();
    /* access modifiers changed from: private */

    /* renamed from: j */
    public final Semaphore f5158j = new Semaphore(2);
    /* access modifiers changed from: private */

    /* renamed from: k */
    public volatile boolean f5159k;

    C3045g5(C3081j5 j5Var) {
        super(j5Var);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.lang.Runnable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, boolean, java.lang.String):void */
    /* renamed from: a */
    public final <V> Future<V> mo19027a(Callable callable) {
        mo18903k();
        C2258v.m5629a(callable);
        C3057h5 h5Var = new C3057h5(this, callable, false, "Task exception on worker thread");
        if (Thread.currentThread() == this.f5151c) {
            if (!this.f5153e.isEmpty()) {
                mo19015l().mo19004w().mo19042a("Callable skipped the worker queue.");
            }
            h5Var.run();
        } else {
            m8671a((C3057h5<?>) h5Var);
        }
        return h5Var;
    }

    /* renamed from: b */
    public final void mo19008b() {
        if (Thread.currentThread() != this.f5152d) {
            throw new IllegalStateException("Call expected from network thread");
        }
    }

    /* renamed from: c */
    public final void mo18881c() {
        if (Thread.currentThread() != this.f5151c) {
            throw new IllegalStateException("Call expected from worker thread");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final boolean mo18825q() {
        return false;
    }

    /* renamed from: t */
    public final boolean mo19031t() {
        return Thread.currentThread() == this.f5151c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.lang.Runnable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, boolean, java.lang.String):void */
    /* renamed from: b */
    public final <V> Future<V> mo19029b(Callable callable) {
        mo18903k();
        C2258v.m5629a(callable);
        C3057h5 h5Var = new C3057h5(this, callable, true, "Task exception on worker thread");
        if (Thread.currentThread() == this.f5151c) {
            h5Var.run();
        } else {
            m8671a((C3057h5<?>) h5Var);
        }
        return h5Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.lang.Runnable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.g5, java.lang.Runnable, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.lang.Runnable, boolean, java.lang.String):void */
    /* renamed from: a */
    public final void mo19028a(Runnable runnable) {
        mo18903k();
        C2258v.m5629a(runnable);
        m8671a((C3057h5<?>) new C3057h5(this, runnable, false, "Task exception on worker thread"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.lang.Runnable, boolean, java.lang.String):void
     arg types: [com.google.android.gms.measurement.internal.g5, java.lang.Runnable, int, java.lang.String]
     candidates:
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.util.concurrent.Callable, boolean, java.lang.String):void
      com.google.android.gms.measurement.internal.h5.<init>(com.google.android.gms.measurement.internal.g5, java.lang.Runnable, boolean, java.lang.String):void */
    /* renamed from: b */
    public final void mo19030b(Runnable runnable) {
        mo18903k();
        C2258v.m5629a(runnable);
        C3057h5 h5Var = new C3057h5(this, runnable, false, "Task exception on network thread");
        synchronized (this.f5157i) {
            this.f5154f.add(h5Var);
            if (this.f5152d == null) {
                this.f5152d = new C3093k5(this, "Measurement Network", this.f5154f);
                this.f5152d.setUncaughtExceptionHandler(this.f5156h);
                this.f5152d.start();
            } else {
                this.f5152d.mo19127a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:15|16|(1:18)(1:19)|20|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0024, code lost:
        if (r5.length() == 0) goto L_0x002b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0026, code lost:
        r4 = "Timed out waiting for ".concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
        r4 = new java.lang.String("Timed out waiting for ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0031, code lost:
        r3.mo19042a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        return r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r3 = mo19015l().mo19004w();
        r5 = java.lang.String.valueOf(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0047, code lost:
        if (r5.length() != 0) goto L_0x0049;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0049, code lost:
        r4 = "Interrupted waiting for ".concat(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004e, code lost:
        r4 = new java.lang.String("Interrupted waiting for ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0054, code lost:
        r3.mo19042a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0059, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000c, code lost:
        r2 = r2.get();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0010, code lost:
        if (r2 != null) goto L_0x0034;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        r3 = mo19015l().mo19004w();
        r5 = java.lang.String.valueOf(r5);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0035 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final <T> T mo19026a(java.util.concurrent.atomic.AtomicReference<T> r2, long r3, java.lang.String r5, java.lang.Runnable r6) {
        /*
            r1 = this;
            monitor-enter(r2)
            com.google.android.gms.measurement.internal.g5 r0 = r1.mo19014j()     // Catch:{ all -> 0x005a }
            r0.mo19028a(r6)     // Catch:{ all -> 0x005a }
            r2.wait(r3)     // Catch:{ InterruptedException -> 0x0035 }
            monitor-exit(r2)     // Catch:{ all -> 0x005a }
            java.lang.Object r2 = r2.get()
            if (r2 != 0) goto L_0x0034
            com.google.android.gms.measurement.internal.f4 r3 = r1.mo19015l()
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19004w()
            java.lang.String r4 = "Timed out waiting for "
            java.lang.String r5 = java.lang.String.valueOf(r5)
            int r6 = r5.length()
            if (r6 == 0) goto L_0x002b
            java.lang.String r4 = r4.concat(r5)
            goto L_0x0031
        L_0x002b:
            java.lang.String r5 = new java.lang.String
            r5.<init>(r4)
            r4 = r5
        L_0x0031:
            r3.mo19042a(r4)
        L_0x0034:
            return r2
        L_0x0035:
            com.google.android.gms.measurement.internal.f4 r3 = r1.mo19015l()     // Catch:{ all -> 0x005a }
            com.google.android.gms.measurement.internal.h4 r3 = r3.mo19004w()     // Catch:{ all -> 0x005a }
            java.lang.String r4 = "Interrupted waiting for "
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ all -> 0x005a }
            int r6 = r5.length()     // Catch:{ all -> 0x005a }
            if (r6 == 0) goto L_0x004e
            java.lang.String r4 = r4.concat(r5)     // Catch:{ all -> 0x005a }
            goto L_0x0054
        L_0x004e:
            java.lang.String r5 = new java.lang.String     // Catch:{ all -> 0x005a }
            r5.<init>(r4)     // Catch:{ all -> 0x005a }
            r4 = r5
        L_0x0054:
            r3.mo19042a(r4)     // Catch:{ all -> 0x005a }
            r3 = 0
            monitor-exit(r2)     // Catch:{ all -> 0x005a }
            return r3
        L_0x005a:
            r3 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x005a }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3045g5.mo19026a(java.util.concurrent.atomic.AtomicReference, long, java.lang.String, java.lang.Runnable):java.lang.Object");
    }

    /* renamed from: a */
    private final void m8671a(C3057h5<?> h5Var) {
        synchronized (this.f5157i) {
            this.f5153e.add(h5Var);
            if (this.f5151c == null) {
                this.f5151c = new C3093k5(this, "Measurement Worker", this.f5153e);
                this.f5151c.setUncaughtExceptionHandler(this.f5155g);
                this.f5151c.start();
            } else {
                this.f5151c.mo19127a();
            }
        }
    }
}
