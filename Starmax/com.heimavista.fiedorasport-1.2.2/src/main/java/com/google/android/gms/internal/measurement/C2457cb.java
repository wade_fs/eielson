package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.cb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2457cb implements C2593l2<C2441bb> {

    /* renamed from: Q */
    private static C2457cb f4026Q = new C2457cb();

    /* renamed from: P */
    private final C2593l2<C2441bb> f4027P;

    private C2457cb(C2593l2<C2441bb> l2Var) {
        this.f4027P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6156b() {
        return ((C2441bb) f4026Q.mo17285a()).mo17338a();
    }

    /* renamed from: c */
    public static boolean m6157c() {
        return ((C2441bb) f4026Q.mo17285a()).mo17339e();
    }

    /* renamed from: d */
    public static boolean m6158d() {
        return ((C2441bb) f4026Q.mo17285a()).mo17340f();
    }

    /* renamed from: e */
    public static boolean m6159e() {
        return ((C2441bb) f4026Q.mo17285a()).mo17341g();
    }

    /* renamed from: f */
    public static boolean m6160f() {
        return ((C2441bb) f4026Q.mo17285a()).mo17343t();
    }

    /* renamed from: g */
    public static boolean m6161g() {
        return ((C2441bb) f4026Q.mo17285a()).mo17342h();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4027P.mo17285a();
    }

    public C2457cb() {
        this(C2579k2.m6601a(new C2490eb()));
    }
}
