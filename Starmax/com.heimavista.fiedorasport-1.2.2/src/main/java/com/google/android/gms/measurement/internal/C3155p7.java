package com.google.android.gms.measurement.internal;

import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: com.google.android.gms.measurement.internal.p7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C3155p7 {
    @Nullable
    /* renamed from: a */
    public static String m9168a(String str, String[] strArr, String[] strArr2) {
        C2258v.m5629a(strArr);
        C2258v.m5629a(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i = 0; i < min; i++) {
            String str2 = strArr[i];
            if ((str == null && str2 == null) ? true : str == null ? false : str.equals(str2)) {
                return strArr2[i];
            }
        }
        return null;
    }
}
