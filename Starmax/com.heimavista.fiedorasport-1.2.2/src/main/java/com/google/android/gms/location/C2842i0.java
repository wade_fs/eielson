package com.google.android.gms.location;

import android.os.IInterface;

/* renamed from: com.google.android.gms.location.i0 */
public interface C2842i0 extends IInterface {
    /* renamed from: a */
    void mo17213a(LocationAvailability locationAvailability);

    /* renamed from: a */
    void mo17214a(LocationResult locationResult);
}
