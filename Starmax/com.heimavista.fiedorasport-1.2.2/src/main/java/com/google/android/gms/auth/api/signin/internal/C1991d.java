package com.google.android.gms.auth.api.signin.internal;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.auth.api.signin.internal.d */
public class C1991d extends C2005r {
    /* renamed from: a */
    public void mo16444a(GoogleSignInAccount googleSignInAccount, Status status) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: b */
    public void mo16446b(Status status) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void mo16445a(Status status) {
        throw new UnsupportedOperationException();
    }
}
