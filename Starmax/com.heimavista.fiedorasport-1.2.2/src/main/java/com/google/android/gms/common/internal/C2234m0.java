package com.google.android.gms.common.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.common.internal.m0 */
public final class C2234m0 implements Parcelable.Creator<SignInButtonConfig> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        int i = 0;
        Scope[] scopeArr = null;
        int i2 = 0;
        int i3 = 0;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                i = C2248a.m5585z(parcel, a);
            } else if (a2 == 2) {
                i2 = C2248a.m5585z(parcel, a);
            } else if (a2 == 3) {
                i3 = C2248a.m5585z(parcel, a);
            } else if (a2 != 4) {
                C2248a.m5550F(parcel, a);
            } else {
                scopeArr = (Scope[]) C2248a.m5559b(parcel, a, Scope.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new SignInButtonConfig(i, i2, i3, scopeArr);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new SignInButtonConfig[i];
    }
}
