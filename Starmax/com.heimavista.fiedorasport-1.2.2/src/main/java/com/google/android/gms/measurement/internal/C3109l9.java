package com.google.android.gms.measurement.internal;

import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.util.C2314e;

/* renamed from: com.google.android.gms.measurement.internal.l9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3109l9 {

    /* renamed from: a */
    private final C2314e f5332a;

    /* renamed from: b */
    private long f5333b;

    public C3109l9(C2314e eVar) {
        C2258v.m5629a(eVar);
        this.f5332a = eVar;
    }

    /* renamed from: a */
    public final void mo19139a() {
        this.f5333b = this.f5332a.elapsedRealtime();
    }

    /* renamed from: b */
    public final void mo19141b() {
        this.f5333b = 0;
    }

    /* renamed from: a */
    public final boolean mo19140a(long j) {
        if (this.f5333b != 0 && this.f5332a.elapsedRealtime() - this.f5333b < 3600000) {
            return false;
        }
        return true;
    }
}
