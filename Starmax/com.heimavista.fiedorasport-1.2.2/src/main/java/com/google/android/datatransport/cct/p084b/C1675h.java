package com.google.android.datatransport.cct.p084b;

import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1692r;
import java.util.List;

/* renamed from: com.google.android.datatransport.cct.b.h */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1675h extends C1692r {

    /* renamed from: a */
    private final long f2609a;

    /* renamed from: b */
    private final long f2610b;

    /* renamed from: c */
    private final C1684m f2611c;

    /* renamed from: d */
    private final int f2612d;

    /* renamed from: e */
    private final String f2613e;

    /* renamed from: f */
    private final List<C1689p> f2614f;

    /* renamed from: g */
    private final C1663b f2615g;

    /* renamed from: com.google.android.datatransport.cct.b.h$b */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    static final class C1677b extends C1692r.C1693a {

        /* renamed from: a */
        private Long f2616a;

        /* renamed from: b */
        private Long f2617b;

        /* renamed from: c */
        private C1684m f2618c;

        /* renamed from: d */
        private Integer f2619d;

        /* renamed from: e */
        private String f2620e;

        /* renamed from: f */
        private List<C1689p> f2621f;

        /* renamed from: g */
        private C1663b f2622g;

        C1677b() {
        }

        /* renamed from: a */
        public C1692r.C1693a mo13505a(long j) {
            this.f2616a = Long.valueOf(j);
            return super;
        }

        /* renamed from: b */
        public C1692r.C1693a mo13511b(long j) {
            this.f2617b = Long.valueOf(j);
            return super;
        }

        /* renamed from: a */
        public C1692r.C1693a mo13507a(@Nullable C1684m mVar) {
            this.f2618c = mVar;
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C1692r.C1693a mo13504a(int i) {
            this.f2619d = Integer.valueOf(i);
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C1692r.C1693a mo13508a(@Nullable String str) {
            this.f2620e = str;
            return super;
        }

        /* renamed from: a */
        public C1692r.C1693a mo13509a(@Nullable List<C1689p> list) {
            this.f2621f = list;
            return super;
        }

        /* renamed from: a */
        public C1692r.C1693a mo13506a(@Nullable C1663b bVar) {
            this.f2622g = bVar;
            return super;
        }

        /* renamed from: a */
        public C1692r mo13510a() {
            String str = "";
            if (this.f2616a == null) {
                str = str + " requestTimeMs";
            }
            if (this.f2617b == null) {
                str = str + " requestUptimeMs";
            }
            if (this.f2619d == null) {
                str = str + " logSource";
            }
            if (str.isEmpty()) {
                return new C1675h(this.f2616a.longValue(), this.f2617b.longValue(), this.f2618c, this.f2619d.intValue(), this.f2620e, this.f2621f, this.f2622g, null);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    /* synthetic */ C1675h(long j, long j2, C1684m mVar, int i, String str, List list, C1663b bVar, C1676a aVar) {
        this.f2609a = j;
        this.f2610b = j2;
        this.f2611c = mVar;
        this.f2612d = i;
        this.f2613e = str;
        this.f2614f = list;
        this.f2615g = bVar;
    }

    @Nullable
    /* renamed from: b */
    public C1684m mo13495b() {
        return this.f2611c;
    }

    @Nullable
    /* renamed from: c */
    public List<C1689p> mo13496c() {
        return this.f2614f;
    }

    /* renamed from: d */
    public int mo13497d() {
        return this.f2612d;
    }

    @Nullable
    /* renamed from: e */
    public String mo13498e() {
        return this.f2613e;
    }

    public boolean equals(Object obj) {
        C1684m mVar;
        String str;
        List<C1689p> list;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1692r)) {
            return false;
        }
        C1675h hVar = (C1675h) obj;
        if (this.f2609a == hVar.f2609a && this.f2610b == hVar.f2610b && ((mVar = this.f2611c) != null ? mVar.equals(hVar.f2611c) : hVar.f2611c == null) && this.f2612d == hVar.f2612d && ((str = this.f2613e) != null ? str.equals(hVar.f2613e) : hVar.f2613e == null) && ((list = this.f2614f) != null ? list.equals(hVar.f2614f) : hVar.f2614f == null)) {
            C1663b bVar = this.f2615g;
            if (bVar == null) {
                if (hVar.f2615g == null) {
                    return true;
                }
            } else if (bVar.equals(hVar.f2615g)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: f */
    public long mo13500f() {
        return this.f2609a;
    }

    /* renamed from: g */
    public long mo13501g() {
        return this.f2610b;
    }

    public int hashCode() {
        long j = this.f2609a;
        long j2 = this.f2610b;
        int i = (((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ ((int) ((j2 >>> 32) ^ j2))) * 1000003;
        C1684m mVar = this.f2611c;
        int i2 = 0;
        int hashCode = (((i ^ (mVar == null ? 0 : mVar.hashCode())) * 1000003) ^ this.f2612d) * 1000003;
        String str = this.f2613e;
        int hashCode2 = (hashCode ^ (str == null ? 0 : str.hashCode())) * 1000003;
        List<C1689p> list = this.f2614f;
        int hashCode3 = (hashCode2 ^ (list == null ? 0 : list.hashCode())) * 1000003;
        C1663b bVar = this.f2615g;
        if (bVar != null) {
            i2 = bVar.hashCode();
        }
        return hashCode3 ^ i2;
    }

    public String toString() {
        return "LogRequest{requestTimeMs=" + this.f2609a + ", requestUptimeMs=" + this.f2610b + ", clientInfo=" + this.f2611c + ", logSource=" + this.f2612d + ", logSourceName=" + this.f2613e + ", logEvents=" + this.f2614f + ", qosTier=" + this.f2615g + "}";
    }
}
