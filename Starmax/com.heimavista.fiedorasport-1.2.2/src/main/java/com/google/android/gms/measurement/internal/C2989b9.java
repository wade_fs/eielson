package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.b9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C2989b9 implements Runnable {

    /* renamed from: P */
    private final C3001c9 f4991P;

    C2989b9(C3001c9 c9Var) {
        this.f4991P = c9Var;
    }

    public final void run() {
        C3001c9 c9Var = this.f4991P;
        c9Var.f5021c.mo19014j().mo19028a(new C3037f9(c9Var));
    }
}
