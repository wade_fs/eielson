package com.google.android.gms.maps;

import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.p093i.C2908m;

/* renamed from: com.google.android.gms.maps.t */
final class C2962t extends C2908m {

    /* renamed from: a */
    private final /* synthetic */ C2880c.C2881a f4921a;

    C2962t(C2880c cVar, C2880c.C2881a aVar) {
        this.f4921a = aVar;
    }

    /* renamed from: a */
    public final void mo18484a(LatLng latLng) {
        this.f4921a.mo18408a(latLng);
    }
}
