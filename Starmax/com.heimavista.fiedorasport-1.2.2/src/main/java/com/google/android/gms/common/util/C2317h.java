package com.google.android.gms.common.util;

import android.os.SystemClock;

/* renamed from: com.google.android.gms.common.util.h */
public class C2317h implements C2314e {

    /* renamed from: a */
    private static final C2317h f3847a = new C2317h();

    private C2317h() {
    }

    /* renamed from: c */
    public static C2314e m5780c() {
        return f3847a;
    }

    /* renamed from: a */
    public long mo17132a() {
        return System.currentTimeMillis();
    }

    /* renamed from: b */
    public long mo17133b() {
        return System.nanoTime();
    }

    public long elapsedRealtime() {
        return SystemClock.elapsedRealtime();
    }
}
