package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.ConnectionResult;

/* renamed from: com.google.android.gms.common.api.internal.i1 */
public interface C2089i1 {
    /* renamed from: a */
    void mo16728a(int i, boolean z);

    /* renamed from: a */
    void mo16729a(Bundle bundle);

    /* renamed from: a */
    void mo16730a(ConnectionResult connectionResult);
}
