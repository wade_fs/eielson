package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.y8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2804y8 implements C2593l2<C2788x8> {

    /* renamed from: Q */
    private static C2804y8 f4615Q = new C2804y8();

    /* renamed from: P */
    private final C2593l2<C2788x8> f4616P;

    private C2804y8(C2593l2<C2788x8> l2Var) {
        this.f4616P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7881b() {
        return ((C2788x8) f4615Q.mo17285a()).mo17286a();
    }

    /* renamed from: c */
    public static boolean m7882c() {
        return ((C2788x8) f4615Q.mo17285a()).mo17287e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4616P.mo17285a();
    }

    public C2804y8() {
        this(C2579k2.m6601a(new C2423a9()));
    }
}
