package com.google.android.gms.measurement.internal;

import androidx.exifinterface.media.ExifInterface;

/* renamed from: com.google.android.gms.measurement.internal.i4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3068i4 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ int f5203P;

    /* renamed from: Q */
    private final /* synthetic */ String f5204Q;

    /* renamed from: R */
    private final /* synthetic */ Object f5205R;

    /* renamed from: S */
    private final /* synthetic */ Object f5206S;

    /* renamed from: T */
    private final /* synthetic */ Object f5207T;

    /* renamed from: U */
    private final /* synthetic */ C3032f4 f5208U;

    C3068i4(C3032f4 f4Var, int i, String str, Object obj, Object obj2, Object obj3) {
        this.f5208U = f4Var;
        this.f5203P = i;
        this.f5204Q = str;
        this.f5205R = obj;
        this.f5206S = obj2;
        this.f5207T = obj3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.f4.a(com.google.android.gms.measurement.internal.f4, char):char
     arg types: [com.google.android.gms.measurement.internal.f4, int]
     candidates:
      com.google.android.gms.measurement.internal.f4.a(com.google.android.gms.measurement.internal.f4, long):long
      com.google.android.gms.measurement.internal.f4.a(boolean, java.lang.Object):java.lang.String
      com.google.android.gms.measurement.internal.f4.a(int, java.lang.String):void
      com.google.android.gms.measurement.internal.f4.a(com.google.android.gms.measurement.internal.f4, char):char */
    public final void run() {
        C3185s4 p = this.f5208U.f5134a.mo19098p();
        if (p.mo18906s()) {
            if (this.f5208U.f5122c == 0) {
                if (this.f5208U.mo19013h().mo19158k()) {
                    C3032f4 f4Var = this.f5208U;
                    f4Var.mo19018r();
                    char unused = f4Var.f5122c = 'C';
                } else {
                    C3032f4 f4Var2 = this.f5208U;
                    f4Var2.mo19018r();
                    char unused2 = f4Var2.f5122c = 'c';
                }
            }
            if (this.f5208U.f5123d < 0) {
                C3032f4 f4Var3 = this.f5208U;
                long unused3 = f4Var3.f5123d = f4Var3.mo19013h().mo19157i();
            }
            char charAt = "01VDIWEA?".charAt(this.f5203P);
            char a = this.f5208U.f5122c;
            long b = this.f5208U.f5123d;
            String a2 = C3032f4.m8623a(true, this.f5204Q, this.f5205R, this.f5206S, this.f5207T);
            StringBuilder sb = new StringBuilder(String.valueOf(a2).length() + 24);
            sb.append(ExifInterface.GPS_MEASUREMENT_2D);
            sb.append(charAt);
            sb.append(a);
            sb.append(b);
            sb.append(":");
            sb.append(a2);
            String sb2 = sb.toString();
            if (sb2.length() > 1024) {
                sb2 = this.f5204Q.substring(0, 1024);
            }
            p.f5606d.mo19365a(sb2, 1);
            return;
        }
        this.f5208U.mo18998a(6, "Persisted config not initialized. Not logging error/warn");
    }
}
