package com.google.android.gms.common.internal.p089z;

import android.os.Parcel;
import com.google.android.gms.internal.base.C2376b;

/* renamed from: com.google.android.gms.common.internal.z.l */
public abstract class C2277l extends C2376b implements C2276k {
    public C2277l() {
        super("com.google.android.gms.common.internal.service.ICommonCallbacks");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo17049a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        mo17048K(parcel.readInt());
        parcel2.writeNoException();
        return true;
    }
}
