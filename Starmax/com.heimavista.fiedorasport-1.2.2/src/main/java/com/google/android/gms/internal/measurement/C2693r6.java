package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.r6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2693r6 extends C2786x6 {

    /* renamed from: Q */
    private final /* synthetic */ C2618m6 f4463Q;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private C2693r6(C2618m6 m6Var) {
        super(m6Var, null);
        this.f4463Q = m6Var;
    }

    public final Iterator<Map.Entry<K, V>> iterator() {
        return new C2649o6(this.f4463Q, null);
    }

    /* synthetic */ C2693r6(C2618m6 m6Var, C2663p6 p6Var) {
        this(m6Var);
    }
}
