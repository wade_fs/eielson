package com.google.android.gms.maps.model;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import p119e.p144d.p145a.p157c.p160b.C3988b;

public final class MarkerOptions extends AbstractSafeParcelable {
    public static final Parcelable.Creator<MarkerOptions> CREATOR = new C2943n();

    /* renamed from: P */
    private LatLng f4843P;

    /* renamed from: Q */
    private String f4844Q;

    /* renamed from: R */
    private String f4845R;

    /* renamed from: S */
    private C2929a f4846S;

    /* renamed from: T */
    private float f4847T = 0.5f;

    /* renamed from: U */
    private float f4848U = 1.0f;

    /* renamed from: V */
    private boolean f4849V;

    /* renamed from: W */
    private boolean f4850W = true;

    /* renamed from: X */
    private boolean f4851X = false;

    /* renamed from: Y */
    private float f4852Y = 0.0f;

    /* renamed from: Z */
    private float f4853Z = 0.5f;

    /* renamed from: a0 */
    private float f4854a0 = 0.0f;

    /* renamed from: b0 */
    private float f4855b0 = 1.0f;

    /* renamed from: c0 */
    private float f4856c0;

    public MarkerOptions() {
    }

    /* renamed from: A */
    public final String mo18540A() {
        return this.f4844Q;
    }

    /* renamed from: B */
    public final float mo18541B() {
        return this.f4856c0;
    }

    /* renamed from: C */
    public final boolean mo18542C() {
        return this.f4849V;
    }

    /* renamed from: D */
    public final boolean mo18543D() {
        return this.f4851X;
    }

    /* renamed from: E */
    public final boolean mo18544E() {
        return this.f4850W;
    }

    /* renamed from: a */
    public final MarkerOptions mo18545a(@NonNull LatLng latLng) {
        if (latLng != null) {
            this.f4843P = latLng;
            return this;
        }
        throw new IllegalArgumentException("latlng cannot be null - a position is required.");
    }

    /* renamed from: c */
    public final float mo18547c() {
        return this.f4855b0;
    }

    /* renamed from: d */
    public final float mo18548d() {
        return this.f4847T;
    }

    /* renamed from: u */
    public final float mo18549u() {
        return this.f4848U;
    }

    /* renamed from: v */
    public final float mo18550v() {
        return this.f4853Z;
    }

    /* renamed from: w */
    public final float mo18551w() {
        return this.f4854a0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, android.os.IBinder, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        IBinder iBinder;
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 2, (Parcelable) mo18553x(), i, false);
        C2250b.m5602a(parcel, 3, mo18540A(), false);
        C2250b.m5602a(parcel, 4, mo18555z(), false);
        C2929a aVar = this.f4846S;
        if (aVar == null) {
            iBinder = null;
        } else {
            iBinder = aVar.mo18622a().asBinder();
        }
        C2250b.m5594a(parcel, 5, iBinder, false);
        C2250b.m5590a(parcel, 6, mo18548d());
        C2250b.m5590a(parcel, 7, mo18549u());
        C2250b.m5605a(parcel, 8, mo18542C());
        C2250b.m5605a(parcel, 9, mo18544E());
        C2250b.m5605a(parcel, 10, mo18543D());
        C2250b.m5590a(parcel, 11, mo18554y());
        C2250b.m5590a(parcel, 12, mo18550v());
        C2250b.m5590a(parcel, 13, mo18551w());
        C2250b.m5590a(parcel, 14, mo18547c());
        C2250b.m5590a(parcel, 15, mo18541B());
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: x */
    public final LatLng mo18553x() {
        return this.f4843P;
    }

    /* renamed from: y */
    public final float mo18554y() {
        return this.f4852Y;
    }

    /* renamed from: z */
    public final String mo18555z() {
        return this.f4845R;
    }

    /* renamed from: a */
    public final MarkerOptions mo18546a(@Nullable C2929a aVar) {
        this.f4846S = aVar;
        return this;
    }

    MarkerOptions(LatLng latLng, String str, String str2, IBinder iBinder, float f, float f2, boolean z, boolean z2, boolean z3, float f3, float f4, float f5, float f6, float f7) {
        this.f4843P = latLng;
        this.f4844Q = str;
        this.f4845R = str2;
        if (iBinder == null) {
            this.f4846S = null;
        } else {
            this.f4846S = new C2929a(C3988b.C3989a.m11981a(iBinder));
        }
        this.f4847T = f;
        this.f4848U = f2;
        this.f4849V = z;
        this.f4850W = z2;
        this.f4851X = z3;
        this.f4852Y = f3;
        this.f4853Z = f4;
        this.f4854a0 = f5;
        this.f4855b0 = f6;
        this.f4856c0 = f7;
    }
}
