package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class LatLngBounds extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<LatLngBounds> CREATOR = new C2940k();

    /* renamed from: P */
    public final LatLng f4836P;

    /* renamed from: Q */
    public final LatLng f4837Q;

    public LatLngBounds(LatLng latLng, LatLng latLng2) {
        C2258v.m5630a(latLng, "null southwest");
        C2258v.m5630a(latLng2, "null northeast");
        C2258v.m5638a(latLng2.f4834P >= latLng.f4834P, "southern latitude exceeds northern latitude (%s > %s)", Double.valueOf(latLng.f4834P), Double.valueOf(latLng2.f4834P));
        this.f4836P = latLng;
        this.f4837Q = latLng2;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public static double m8284a(double d, double d2) {
        return ((d - d2) + 360.0d) % 360.0d;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static double m8285b(double d, double d2) {
        return ((d2 - d) + 360.0d) % 360.0d;
    }

    /* renamed from: c */
    public final LatLng mo18532c() {
        LatLng latLng = this.f4836P;
        double d = latLng.f4834P;
        LatLng latLng2 = this.f4837Q;
        double d2 = (d + latLng2.f4834P) / 2.0d;
        double d3 = latLng2.f4835Q;
        double d4 = latLng.f4835Q;
        if (d4 > d3) {
            d3 += 360.0d;
        }
        return new LatLng(d2, (d3 + d4) / 2.0d);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LatLngBounds)) {
            return false;
        }
        LatLngBounds latLngBounds = (LatLngBounds) obj;
        return this.f4836P.equals(latLngBounds.f4836P) && this.f4837Q.equals(latLngBounds.f4837Q);
    }

    public final int hashCode() {
        return C2251t.m5615a(this.f4836P, this.f4837Q);
    }

    public final String toString() {
        C2251t.C2252a a = C2251t.m5616a(this);
        a.mo17037a("southwest", this.f4836P);
        a.mo17037a("northeast", this.f4837Q);
        return a.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.maps.model.LatLng, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5596a(parcel, 2, (Parcelable) this.f4836P, i, false);
        C2250b.m5596a(parcel, 3, (Parcelable) this.f4837Q, i, false);
        C2250b.m5587a(parcel, a);
    }

    /* renamed from: com.google.android.gms.maps.model.LatLngBounds$a */
    public static final class C2927a {

        /* renamed from: a */
        private double f4838a = Double.POSITIVE_INFINITY;

        /* renamed from: b */
        private double f4839b = Double.NEGATIVE_INFINITY;

        /* renamed from: c */
        private double f4840c = Double.NaN;

        /* renamed from: d */
        private double f4841d = Double.NaN;

        /* renamed from: a */
        public final C2927a mo18537a(LatLng latLng) {
            this.f4838a = Math.min(this.f4838a, latLng.f4834P);
            this.f4839b = Math.max(this.f4839b, latLng.f4834P);
            double d = latLng.f4835Q;
            if (Double.isNaN(this.f4840c)) {
                this.f4840c = d;
            } else {
                double d2 = this.f4840c;
                double d3 = this.f4841d;
                boolean z = false;
                if (d2 > d3 ? d2 <= d || d <= d3 : d2 <= d && d <= d3) {
                    z = true;
                }
                if (!z) {
                    if (LatLngBounds.m8284a(this.f4840c, d) < LatLngBounds.m8285b(this.f4841d, d)) {
                        this.f4840c = d;
                    }
                }
                return this;
            }
            this.f4841d = d;
            return this;
        }

        /* renamed from: a */
        public final LatLngBounds mo18538a() {
            C2258v.m5641b(!Double.isNaN(this.f4840c), "no included points");
            return new LatLngBounds(new LatLng(this.f4838a, this.f4840c), new LatLng(this.f4839b, this.f4841d));
        }
    }
}
