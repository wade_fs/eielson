package com.google.android.gms.internal.measurement;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.measurement.a */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public class C2412a implements IInterface {

    /* renamed from: a */
    private final IBinder f3968a;

    /* renamed from: b */
    private final String f3969b;

    protected C2412a(IBinder iBinder, String str) {
        this.f3968a = iBinder;
        this.f3969b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public final Parcel mo17243H() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f3969b);
        return obtain;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo17244a(int i, Parcel parcel) {
        parcel = Parcel.obtain();
        try {
            this.f3968a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f3968a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17246b(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            this.f3968a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }
}
