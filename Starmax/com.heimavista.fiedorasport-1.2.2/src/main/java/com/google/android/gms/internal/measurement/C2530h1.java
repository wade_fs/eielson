package com.google.android.gms.internal.measurement;

import android.content.ContentResolver;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.StrictMode;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.collection.ArrayMap;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.h1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2530h1 implements C2613m1 {
    @GuardedBy("ConfigurationContentLoader.class")

    /* renamed from: g */
    private static final Map<Uri, C2530h1> f4200g = new ArrayMap();

    /* renamed from: h */
    private static final String[] f4201h = {"key", "value"};

    /* renamed from: a */
    private final ContentResolver f4202a;

    /* renamed from: b */
    private final Uri f4203b;

    /* renamed from: c */
    private final ContentObserver f4204c = new C2578k1(this, null);

    /* renamed from: d */
    private final Object f4205d = new Object();

    /* renamed from: e */
    private volatile Map<String, String> f4206e;
    @GuardedBy("this")

    /* renamed from: f */
    private final List<C2560j1> f4207f = new ArrayList();

    private C2530h1(ContentResolver contentResolver, Uri uri) {
        this.f4202a = contentResolver;
        this.f4203b = uri;
        contentResolver.registerContentObserver(uri, false, this.f4204c);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:2|3|(5:5|6|7|8|9)|11|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0018 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.google.android.gms.internal.measurement.C2530h1 m6433a(android.content.ContentResolver r3, android.net.Uri r4) {
        /*
            java.lang.Class<com.google.android.gms.internal.measurement.h1> r0 = com.google.android.gms.internal.measurement.C2530h1.class
            monitor-enter(r0)
            java.util.Map<android.net.Uri, com.google.android.gms.internal.measurement.h1> r1 = com.google.android.gms.internal.measurement.C2530h1.f4200g     // Catch:{ all -> 0x001a }
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x001a }
            com.google.android.gms.internal.measurement.h1 r1 = (com.google.android.gms.internal.measurement.C2530h1) r1     // Catch:{ all -> 0x001a }
            if (r1 != 0) goto L_0x0018
            com.google.android.gms.internal.measurement.h1 r2 = new com.google.android.gms.internal.measurement.h1     // Catch:{ SecurityException -> 0x0018 }
            r2.<init>(r3, r4)     // Catch:{ SecurityException -> 0x0018 }
            java.util.Map<android.net.Uri, com.google.android.gms.internal.measurement.h1> r3 = com.google.android.gms.internal.measurement.C2530h1.f4200g     // Catch:{ SecurityException -> 0x0017 }
            r3.put(r4, r2)     // Catch:{ SecurityException -> 0x0017 }
        L_0x0017:
            r1 = r2
        L_0x0018:
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            return r1
        L_0x001a:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001a }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2530h1.m6433a(android.content.ContentResolver, android.net.Uri):com.google.android.gms.internal.measurement.h1");
    }

    /* renamed from: d */
    static synchronized void m6434d() {
        synchronized (C2530h1.class) {
            for (C2530h1 h1Var : f4200g.values()) {
                h1Var.f4202a.unregisterContentObserver(h1Var.f4204c);
            }
            f4200g.clear();
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: e */
    private final Map<String, String> m6435e() {
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            Map<String, String> map = (Map) C2592l1.m6637a(new C2513g1(this));
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return map;
        } catch (SQLiteException | IllegalStateException | SecurityException unused) {
            Log.e("ConfigurationContentLoader", "PhenotypeFlag unable to load ContentProvider, using default values");
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            return null;
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
    }

    /* renamed from: b */
    public final void mo17536b() {
        synchronized (this.f4205d) {
            this.f4206e = null;
            C2765w1.m7695c();
        }
        synchronized (this) {
            for (C2560j1 j1Var : this.f4207f) {
                j1Var.mo17586a();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final /* synthetic */ Map mo17537c() {
        Map map;
        Cursor query = this.f4202a.query(this.f4203b, f4201h, null, null, null);
        if (query == null) {
            return Collections.emptyMap();
        }
        try {
            int count = query.getCount();
            if (count == 0) {
                return Collections.emptyMap();
            }
            if (count <= 256) {
                map = new ArrayMap(count);
            } else {
                map = new HashMap(count, 1.0f);
            }
            while (query.moveToNext()) {
                map.put(query.getString(0), query.getString(1));
            }
            query.close();
            return map;
        } finally {
            query.close();
        }
    }

    /* renamed from: a */
    public final Map<String, String> mo17535a() {
        Map<String, String> map = this.f4206e;
        if (map == null) {
            synchronized (this.f4205d) {
                map = this.f4206e;
                if (map == null) {
                    map = m6435e();
                    this.f4206e = map;
                }
            }
        }
        if (map != null) {
            return map;
        }
        return Collections.emptyMap();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17308a(String str) {
        return mo17535a().get(str);
    }
}
