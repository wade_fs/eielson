package com.google.android.gms.internal.measurement;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.measurement.k3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2580k3 extends Iterator<Byte> {
    /* renamed from: a */
    byte mo17442a();
}
