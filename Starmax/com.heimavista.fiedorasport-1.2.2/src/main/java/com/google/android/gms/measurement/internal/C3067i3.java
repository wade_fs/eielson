package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2635n8;

/* renamed from: com.google.android.gms.measurement.internal.i3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3067i3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5202a = new C3067i3();

    private C3067i3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2635n8.m6934c());
    }
}
