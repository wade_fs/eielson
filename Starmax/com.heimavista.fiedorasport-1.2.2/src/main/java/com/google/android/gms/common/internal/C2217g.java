package com.google.android.gms.common.internal;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import androidx.annotation.NonNull;
import com.google.android.gms.common.api.internal.C2080h;

/* renamed from: com.google.android.gms.common.internal.g */
public abstract class C2217g implements DialogInterface.OnClickListener {
    /* renamed from: a */
    public static C2217g m5472a(Activity activity, Intent intent, int i) {
        return new C2195c0(intent, activity, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo16904a();

    public void onClick(DialogInterface dialogInterface, int i) {
        try {
            mo16904a();
        } catch (ActivityNotFoundException e) {
            Log.e("DialogRedirect", "Failed to start resolution intent", e);
        } finally {
            dialogInterface.dismiss();
        }
    }

    /* renamed from: a */
    public static C2217g m5473a(@NonNull C2080h hVar, Intent intent, int i) {
        return new C2210d0(intent, hVar, i);
    }
}
