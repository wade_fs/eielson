package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2473db;

/* renamed from: com.google.android.gms.measurement.internal.d3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3007d3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5040a = new C3007d3();

    private C3007d3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2473db.m6225c());
    }
}
