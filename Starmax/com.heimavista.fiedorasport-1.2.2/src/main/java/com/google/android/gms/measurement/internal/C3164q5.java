package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.q5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3164q5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzv f5564P;

    /* renamed from: Q */
    private final /* synthetic */ C3141o5 f5565Q;

    C3164q5(C3141o5 o5Var, zzv zzv) {
        this.f5565Q = o5Var;
        this.f5564P = zzv;
    }

    public final void run() {
        this.f5565Q.f5496a.mo19237q();
        if (this.f5564P.f5838R.mo19469a() == null) {
            this.f5565Q.f5496a.mo19223b(this.f5564P);
        } else {
            this.f5565Q.f5496a.mo19215a(this.f5564P);
        }
    }
}
