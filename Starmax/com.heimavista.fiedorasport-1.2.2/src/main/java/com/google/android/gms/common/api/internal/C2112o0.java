package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.api.internal.o0 */
final class C2112o0 implements C2036f.C2039c {

    /* renamed from: a */
    private final /* synthetic */ C2107n f3414a;

    C2112o0(C2100l0 l0Var, C2107n nVar) {
        this.f3414a = nVar;
    }

    /* renamed from: a */
    public final void mo16585a(@NonNull ConnectionResult connectionResult) {
        this.f3414a.mo16593a((C2157k) new Status(8));
    }
}
