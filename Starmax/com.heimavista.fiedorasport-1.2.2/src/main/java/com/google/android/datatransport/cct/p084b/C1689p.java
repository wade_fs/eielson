package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1672g;

/* renamed from: com.google.android.datatransport.cct.b.p */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class C1689p {

    /* renamed from: com.google.android.datatransport.cct.b.p$a */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class C1690a {
        @NonNull
        /* renamed from: a */
        public abstract C1690a mo13487a(int i);

        @NonNull
        /* renamed from: a */
        public abstract C1690a mo13488a(long j);

        @NonNull
        /* renamed from: a */
        public abstract C1690a mo13489a(@Nullable C1696u uVar);

        /* access modifiers changed from: package-private */
        @NonNull
        /* renamed from: a */
        public abstract C1690a mo13490a(@Nullable String str);

        /* access modifiers changed from: package-private */
        @NonNull
        /* renamed from: a */
        public abstract C1690a mo13491a(@Nullable byte[] bArr);

        @NonNull
        /* renamed from: a */
        public abstract C1689p mo13492a();

        @NonNull
        /* renamed from: b */
        public abstract C1690a mo13493b(long j);

        @NonNull
        /* renamed from: c */
        public abstract C1690a mo13494c(long j);
    }

    @NonNull
    /* renamed from: a */
    public static C1690a m4222a(@NonNull String str) {
        C1672g.C1674b bVar = new C1672g.C1674b();
        bVar.mo13487a(Integer.MIN_VALUE);
        bVar.mo13490a(str);
        return bVar;
    }

    /* renamed from: a */
    public abstract long mo13477a();

    /* renamed from: b */
    public abstract long mo13478b();

    /* renamed from: c */
    public abstract long mo13479c();

    @NonNull
    /* renamed from: a */
    public static C1690a m4223a(@NonNull byte[] bArr) {
        C1672g.C1674b bVar = new C1672g.C1674b();
        bVar.mo13487a(Integer.MIN_VALUE);
        bVar.mo13491a(bArr);
        return bVar;
    }
}
