package com.google.android.gms.maps.model;

public final class CustomCap extends Cap {

    /* renamed from: S */
    public final C2929a f4818S;

    /* renamed from: T */
    public final float f4819T;

    public final String toString() {
        String valueOf = String.valueOf(this.f4818S);
        float f = this.f4819T;
        StringBuilder sb = new StringBuilder(String.valueOf(valueOf).length() + 55);
        sb.append("[CustomCap: bitmapDescriptor=");
        sb.append(valueOf);
        sb.append(" refWidth=");
        sb.append(f);
        sb.append("]");
        return sb.toString();
    }
}
