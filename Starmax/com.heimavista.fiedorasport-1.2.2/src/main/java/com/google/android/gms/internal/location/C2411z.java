package com.google.android.gms.internal.location;

import android.os.IInterface;

/* renamed from: com.google.android.gms.internal.location.z */
final class C2411z implements C2406u<C2392g> {

    /* renamed from: a */
    private final /* synthetic */ C2410y f3934a;

    C2411z(C2410y yVar) {
        this.f3934a = yVar;
    }

    /* renamed from: a */
    public final void mo17226a() {
        this.f3934a.mo16931p();
    }

    /* renamed from: b */
    public final /* synthetic */ IInterface mo17227b() {
        return (C2392g) this.f3934a.mo16939x();
    }
}
