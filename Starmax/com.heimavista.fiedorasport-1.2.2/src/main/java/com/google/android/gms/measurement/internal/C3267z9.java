package com.google.android.gms.measurement.internal;

import android.annotation.SuppressLint;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ServiceInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.core.app.NotificationCompat;
import com.facebook.share.internal.ShareConstants;
import com.google.android.exoplayer2.upstream.DefaultLoadErrorHandlingPolicy;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.p090j.C2283c;
import com.google.android.gms.common.util.C2315f;
import com.google.android.gms.internal.measurement.C2589kc;
import com.google.android.gms.internal.measurement.C2774wa;
import java.io.ByteArrayInputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.security.auth.x500.X500Principal;

/* renamed from: com.google.android.gms.measurement.internal.z9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3267z9 extends C3010d6 {

    /* renamed from: g */
    private static final String[] f5797g = {"firebase_", "google_", "ga_"};

    /* renamed from: c */
    private SecureRandom f5798c;

    /* renamed from: d */
    private final AtomicLong f5799d = new AtomicLong(0);

    /* renamed from: e */
    private int f5800e;

    /* renamed from: f */
    private Integer f5801f = null;

    C3267z9(C3081j5 j5Var) {
        super(j5Var);
    }

    /* renamed from: e */
    static boolean m9429e(String str) {
        C2258v.m5639b(str);
        if (str.charAt(0) != '_' || str.equals("_ep")) {
            return true;
        }
        return false;
    }

    /* renamed from: f */
    static boolean m9430f(String str) {
        return !TextUtils.isEmpty(str) && str.startsWith("_");
    }

    /* renamed from: g */
    private static boolean m9431g(String str) {
        C2258v.m5629a((Object) str);
        return str.matches("^(1:\\d+:android:[a-f0-9]+|ca-app-pub-.*)$");
    }

    /* renamed from: h */
    private final int m9432h(String str) {
        if ("_ldl".equals(str)) {
            return 2048;
        }
        if ("_id".equals(str)) {
            return 256;
        }
        return (!mo19013h().mo19146a(C3135o.f5389D0) || !"_lgclid".equals(str)) ? 36 : 100;
    }

    /* renamed from: y */
    static MessageDigest m9433y() {
        int i = 0;
        while (i < 2) {
            try {
                MessageDigest instance = MessageDigest.getInstance("MD5");
                if (instance != null) {
                    return instance;
                }
                i++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Bundle mo19425a(@NonNull Uri uri) {
        String str;
        String str2;
        String str3;
        String str4;
        if (uri == null) {
            return null;
        }
        try {
            if (uri.isHierarchical()) {
                str4 = uri.getQueryParameter("utm_campaign");
                str3 = uri.getQueryParameter("utm_source");
                str2 = uri.getQueryParameter("utm_medium");
                str = uri.getQueryParameter("gclid");
            } else {
                str4 = null;
                str3 = null;
                str2 = null;
                str = null;
            }
            if (TextUtils.isEmpty(str4) && TextUtils.isEmpty(str3) && TextUtils.isEmpty(str2) && TextUtils.isEmpty(str)) {
                return null;
            }
            Bundle bundle = new Bundle();
            if (!TextUtils.isEmpty(str4)) {
                bundle.putString("campaign", str4);
            }
            if (!TextUtils.isEmpty(str3)) {
                bundle.putString(ShareConstants.FEED_SOURCE_PARAM, str3);
            }
            if (!TextUtils.isEmpty(str2)) {
                bundle.putString("medium", str2);
            }
            if (!TextUtils.isEmpty(str)) {
                bundle.putString("gclid", str);
            }
            String queryParameter = uri.getQueryParameter("utm_term");
            if (!TextUtils.isEmpty(queryParameter)) {
                bundle.putString("term", queryParameter);
            }
            String queryParameter2 = uri.getQueryParameter("utm_content");
            if (!TextUtils.isEmpty(queryParameter2)) {
                bundle.putString("content", queryParameter2);
            }
            String queryParameter3 = uri.getQueryParameter("aclid");
            if (!TextUtils.isEmpty(queryParameter3)) {
                bundle.putString("aclid", queryParameter3);
            }
            String queryParameter4 = uri.getQueryParameter("cp1");
            if (!TextUtils.isEmpty(queryParameter4)) {
                bundle.putString("cp1", queryParameter4);
            }
            String queryParameter5 = uri.getQueryParameter("anid");
            if (!TextUtils.isEmpty(queryParameter5)) {
                bundle.putString("anid", queryParameter5);
            }
            return bundle;
        } catch (UnsupportedOperationException e) {
            mo19015l().mo19004w().mo19043a("Install referrer url isn't a hierarchical URI", e);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final boolean mo19449b(String str, String str2) {
        if (str2 == null) {
            mo19015l().mo19003v().mo19043a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            mo19015l().mo19003v().mo19043a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (Character.isLetter(codePointAt) || codePointAt == 95) {
                int length = str2.length();
                int charCount = Character.charCount(codePointAt);
                while (charCount < length) {
                    int codePointAt2 = str2.codePointAt(charCount);
                    if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                        charCount += Character.charCount(codePointAt2);
                    } else {
                        mo19015l().mo19003v().mo19044a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                        return false;
                    }
                }
                return true;
            }
            mo19015l().mo19003v().mo19044a("Name must start with a letter or _ (underscore). Type, name", str, str2);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
     arg types: [int, java.lang.Object, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object */
    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final Object mo19450c(String str, Object obj) {
        if ("_ldl".equals(str)) {
            return m9412a(m9432h(str), obj, true);
        }
        return m9412a(m9432h(str), obj, false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final boolean mo19452d(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        String s = mo19013h().mo19162s();
        mo19018r();
        return s.equals(str);
    }

    /* access modifiers changed from: protected */
    @WorkerThread
    /* renamed from: i */
    public final void mo18902i() {
        mo18881c();
        SecureRandom secureRandom = new SecureRandom();
        long nextLong = secureRandom.nextLong();
        if (nextLong == 0) {
            nextLong = secureRandom.nextLong();
            if (nextLong == 0) {
                mo19015l().mo19004w().mo19042a("Utils falling back to Random for random id");
            }
        }
        this.f5799d.set(nextLong);
    }

    /* access modifiers changed from: protected */
    /* renamed from: q */
    public final boolean mo18825q() {
        return true;
    }

    /* renamed from: t */
    public final long mo19453t() {
        long andIncrement;
        long j;
        if (this.f5799d.get() == 0) {
            synchronized (this.f5799d) {
                long nextLong = new Random(System.nanoTime() ^ mo19017o().mo17132a()).nextLong();
                int i = this.f5800e + 1;
                this.f5800e = i;
                j = nextLong + ((long) i);
            }
            return j;
        }
        synchronized (this.f5799d) {
            this.f5799d.compareAndSet(-1, 1);
            andIncrement = this.f5799d.getAndIncrement();
        }
        return andIncrement;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: u */
    public final SecureRandom mo19454u() {
        mo18881c();
        if (this.f5798c == null) {
            this.f5798c = new SecureRandom();
        }
        return this.f5798c;
    }

    /* renamed from: v */
    public final int mo19455v() {
        if (this.f5801f == null) {
            this.f5801f = Integer.valueOf(C2169c.m5270a().mo16840b(mo19016n()) / 1000);
        }
        return this.f5801f.intValue();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: w */
    public final String mo19456w() {
        byte[] bArr = new byte[16];
        mo19454u().nextBytes(bArr);
        return String.format(Locale.US, "%032x", new BigInteger(1, bArr));
    }

    /* renamed from: x */
    public final boolean mo19457x() {
        try {
            mo19016n().getClassLoader().loadClass("com.google.firebase.remoteconfig.FirebaseRemoteConfig");
            return true;
        } catch (ClassNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: c */
    public final boolean mo19451c(String str) {
        mo18881c();
        if (C2283c.m5685a(mo19016n()).mo17053a(str) == 0) {
            return true;
        }
        mo19015l().mo18995A().mo19043a("Permission not granted", str);
        return false;
    }

    /* renamed from: c */
    static boolean m9428c(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null) {
            return false;
        }
        return str.equals(str2);
    }

    /* renamed from: c */
    private final boolean m9427c(Context context, String str) {
        X500Principal x500Principal = new X500Principal("CN=Android Debug,O=Android,C=US");
        try {
            PackageInfo b = C2283c.m5685a(context).mo17060b(str, 64);
            if (b == null || b.signatures == null || b.signatures.length <= 0) {
                return true;
            }
            return ((X509Certificate) CertificateFactory.getInstance("X.509").generateCertificate(new ByteArrayInputStream(b.signatures[0].toByteArray()))).getSubjectX500Principal().equals(x500Principal);
        } catch (CertificateException e) {
            mo19015l().mo19001t().mo19043a("Error obtaining certificate", e);
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            mo19015l().mo19001t().mo19043a("Package name not found", e2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final int mo19447b(String str) {
        if (!mo19449b("user property", str)) {
            return 6;
        }
        if (!mo19446a("user property", C3106l6.f5324a, str)) {
            return 15;
        }
        if (!mo19443a("user property", 24, str)) {
            return 6;
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, int, java.lang.Object, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int, java.lang.Object, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String, java.lang.String, int):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, int, java.lang.Object, boolean):boolean */
    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final int mo19448b(String str, Object obj) {
        boolean z;
        if ("_ldl".equals(str)) {
            z = m9419a("user property referrer", str, m9432h(str), obj, false);
        } else {
            z = m9419a("user property", str, m9432h(str), obj, false);
        }
        return z ? 0 : 7;
    }

    /* renamed from: b */
    private static boolean m9426b(Context context, String str) {
        ServiceInfo serviceInfo;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (serviceInfo = packageManager.getServiceInfo(new ComponentName(context, str), 0)) == null || !serviceInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    /* renamed from: b */
    public static Bundle m9424b(Bundle bundle) {
        if (bundle == null) {
            return new Bundle();
        }
        Bundle bundle2 = new Bundle(bundle);
        for (String str : bundle2.keySet()) {
            Object obj = bundle2.get(str);
            if (obj instanceof Bundle) {
                bundle2.putBundle(str, new Bundle((Bundle) obj));
            } else {
                int i = 0;
                if (obj instanceof Parcelable[]) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    while (i < parcelableArr.length) {
                        if (parcelableArr[i] instanceof Bundle) {
                            parcelableArr[i] = new Bundle((Bundle) parcelableArr[i]);
                        }
                        i++;
                    }
                } else if (obj instanceof List) {
                    List list = (List) obj;
                    while (i < list.size()) {
                        Object obj2 = list.get(i);
                        if (obj2 instanceof Bundle) {
                            list.set(i, new Bundle((Bundle) obj2));
                        }
                        i++;
                    }
                }
            }
        }
        return bundle2;
    }

    /* renamed from: a */
    static boolean m9416a(Intent intent) {
        String stringExtra = intent.getStringExtra("android.intent.extra.REFERRER_NAME");
        return "android-app://com.google.android.googlequicksearchbox/https/www.google.com".equals(stringExtra) || "https://www.google.com".equals(stringExtra) || "android-app://com.google.appcrawler".equals(stringExtra);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo19444a(String str, String str2) {
        if (str2 == null) {
            mo19015l().mo19003v().mo19043a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.length() == 0) {
            mo19015l().mo19003v().mo19043a("Name is required and can't be empty. Type", str);
            return false;
        } else {
            int codePointAt = str2.codePointAt(0);
            if (!Character.isLetter(codePointAt)) {
                mo19015l().mo19003v().mo19044a("Name must start with a letter. Type, name", str, str2);
                return false;
            }
            int length = str2.length();
            int charCount = Character.charCount(codePointAt);
            while (charCount < length) {
                int codePointAt2 = str2.codePointAt(charCount);
                if (codePointAt2 == 95 || Character.isLetterOrDigit(codePointAt2)) {
                    charCount += Character.charCount(codePointAt2);
                } else {
                    mo19015l().mo19003v().mo19044a("Name must consist of letters, digits or _ (underscores). Type, name", str, str2);
                    return false;
                }
            }
            return true;
        }
    }

    /* renamed from: b */
    public static ArrayList<Bundle> m9425b(List<zzv> list) {
        if (list == null) {
            return new ArrayList<>(0);
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(list.size());
        for (zzv zzv : list) {
            Bundle bundle = new Bundle();
            bundle.putString("app_id", zzv.f5836P);
            bundle.putString(TtmlNode.ATTR_TTS_ORIGIN, zzv.f5837Q);
            bundle.putLong("creation_timestamp", zzv.f5839S);
            bundle.putString("name", zzv.f5838R.f5808Q);
            C3046g6.m8688a(bundle, zzv.f5838R.mo19469a());
            bundle.putBoolean("active", zzv.f5840T);
            String str = zzv.f5841U;
            if (str != null) {
                bundle.putString("trigger_event_name", str);
            }
            zzan zzan = zzv.f5842V;
            if (zzan != null) {
                bundle.putString("timed_out_event_name", zzan.f5803P);
                zzam zzam = zzv.f5842V.f5804Q;
                if (zzam != null) {
                    bundle.putBundle("timed_out_event_params", zzam.mo19462e());
                }
            }
            bundle.putLong("trigger_timeout", zzv.f5843W);
            zzan zzan2 = zzv.f5844X;
            if (zzan2 != null) {
                bundle.putString("triggered_event_name", zzan2.f5803P);
                zzam zzam2 = zzv.f5844X.f5804Q;
                if (zzam2 != null) {
                    bundle.putBundle("triggered_event_params", zzam2.mo19462e());
                }
            }
            bundle.putLong("triggered_timestamp", zzv.f5838R.f5809R);
            bundle.putLong("time_to_live", zzv.f5845Y);
            zzan zzan3 = zzv.f5846Z;
            if (zzan3 != null) {
                bundle.putString("expired_event_name", zzan3.f5803P);
                zzam zzam3 = zzv.f5846Z.f5804Q;
                if (zzam3 != null) {
                    bundle.putBundle("expired_event_params", zzam3.mo19462e());
                }
            }
            arrayList.add(bundle);
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo19446a(String str, String[] strArr, String str2) {
        boolean z;
        boolean z2;
        if (str2 == null) {
            mo19015l().mo19003v().mo19043a("Name is required and can't be null. Type", str);
            return false;
        }
        C2258v.m5629a((Object) str2);
        String[] strArr2 = f5797g;
        int length = strArr2.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                z = false;
                break;
            } else if (str2.startsWith(strArr2[i])) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (z) {
            mo19015l().mo19003v().mo19044a("Name starts with reserved prefix. Type, name", str, str2);
            return false;
        }
        if (strArr != null) {
            C2258v.m5629a(strArr);
            int length2 = strArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length2) {
                    z2 = false;
                    break;
                } else if (m9428c(str2, strArr[i2])) {
                    z2 = true;
                    break;
                } else {
                    i2++;
                }
            }
            if (z2) {
                mo19015l().mo19003v().mo19044a("Name is reserved. Type, name", str, str2);
                return false;
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo19443a(String str, int i, String str2) {
        if (str2 == null) {
            mo19015l().mo19003v().mo19043a("Name is required and can't be null. Type", str);
            return false;
        } else if (str2.codePointCount(0, str2.length()) <= i) {
            return true;
        } else {
            mo19015l().mo19003v().mo19045a("Name is too long. Type, maximum supported length, name", str, Integer.valueOf(i), str2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final int mo19423a(String str) {
        if (!mo19449b(NotificationCompat.CATEGORY_EVENT, str)) {
            return 2;
        }
        if (!mo19446a(NotificationCompat.CATEGORY_EVENT, C3082j6.f5264a, str)) {
            return 13;
        }
        if (!mo19443a(NotificationCompat.CATEGORY_EVENT, 40, str)) {
            return 2;
        }
        return 0;
    }

    /* renamed from: a */
    private final boolean m9419a(String str, String str2, int i, Object obj, boolean z) {
        if (obj != null && !(obj instanceof Long) && !(obj instanceof Float) && !(obj instanceof Integer) && !(obj instanceof Byte) && !(obj instanceof Short) && !(obj instanceof Boolean) && !(obj instanceof Double)) {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                String valueOf = String.valueOf(obj);
                if (valueOf.codePointCount(0, valueOf.length()) > i) {
                    mo19015l().mo19006y().mo19045a("Value is too long; discarded. Value kind, name, value length", str, str2, Integer.valueOf(valueOf.length()));
                    return false;
                }
            } else if ((obj instanceof Bundle) && z) {
                return true;
            } else {
                if ((obj instanceof Parcelable[]) && z) {
                    Parcelable[] parcelableArr = (Parcelable[]) obj;
                    for (Parcelable parcelable : parcelableArr) {
                        if (!(parcelable instanceof Bundle)) {
                            mo19015l().mo19006y().mo19044a("All Parcelable[] elements must be of type Bundle. Value type, name", parcelable.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof ArrayList) || !z) {
                    return false;
                } else {
                    ArrayList arrayList = (ArrayList) obj;
                    int size = arrayList.size();
                    int i2 = 0;
                    while (i2 < size) {
                        Object obj2 = arrayList.get(i2);
                        i2++;
                        if (!(obj2 instanceof Bundle)) {
                            mo19015l().mo19006y().mo19044a("All ArrayList elements must be of type Bundle. Value type, name", obj2.getClass(), str2);
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final boolean mo19445a(String str, String str2, String str3) {
        if (!TextUtils.isEmpty(str)) {
            if (m9431g(str)) {
                return true;
            }
            if (this.f5134a.mo19107z()) {
                mo19015l().mo19003v().mo19043a("Invalid google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI. provided id", C3032f4.m8621a(str));
            }
            return false;
        } else if (C2774wa.m7753b() && mo19013h().mo19146a(C3135o.f5395G0) && !TextUtils.isEmpty(str3)) {
            return true;
        } else {
            if (TextUtils.isEmpty(str2)) {
                if (this.f5134a.mo19107z()) {
                    mo19015l().mo19003v().mo19042a("Missing google_app_id. Firebase Analytics disabled. See https://goo.gl/NAOOOI");
                }
                return false;
            } else if (m9431g(str2)) {
                return true;
            } else {
                mo19015l().mo19003v().mo19043a("Invalid admob_app_id. Analytics disabled.", C3032f4.m8621a(str2));
                return false;
            }
        }
    }

    /* renamed from: a */
    static boolean m9420a(String str, String str2, String str3, String str4) {
        boolean isEmpty = TextUtils.isEmpty(str);
        boolean isEmpty2 = TextUtils.isEmpty(str2);
        if (!isEmpty && !isEmpty2) {
            return !str.equals(str2);
        }
        if (isEmpty && isEmpty2) {
            return (TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) ? !TextUtils.isEmpty(str4) : !str3.equals(str4);
        }
        if (isEmpty || !isEmpty2) {
            return TextUtils.isEmpty(str3) || !str3.equals(str4);
        }
        if (TextUtils.isEmpty(str4)) {
            return false;
        }
        return TextUtils.isEmpty(str3) || !str3.equals(str4);
    }

    /* renamed from: a */
    private static Object m9412a(int i, Object obj, boolean z) {
        if (obj == null) {
            return null;
        }
        if ((obj instanceof Long) || (obj instanceof Double)) {
            return obj;
        }
        if (obj instanceof Integer) {
            return Long.valueOf((long) ((Integer) obj).intValue());
        }
        if (obj instanceof Byte) {
            return Long.valueOf((long) ((Byte) obj).byteValue());
        }
        if (obj instanceof Short) {
            return Long.valueOf((long) ((Short) obj).shortValue());
        }
        if (obj instanceof Boolean) {
            return Long.valueOf(((Boolean) obj).booleanValue() ? 1 : 0);
        } else if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        } else {
            if ((obj instanceof String) || (obj instanceof Character) || (obj instanceof CharSequence)) {
                return m9413a(String.valueOf(obj), i, z);
            }
            return null;
        }
    }

    /* renamed from: a */
    public static String m9413a(String str, int i, boolean z) {
        if (str == null) {
            return null;
        }
        if (str.codePointCount(0, str.length()) <= i) {
            return str;
        }
        if (z) {
            return String.valueOf(str.substring(0, str.offsetByCodePoints(0, i))).concat("...");
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
     arg types: [int, java.lang.Object, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Object mo19429a(String str, Object obj) {
        int i = 256;
        if ("_ev".equals(str)) {
            return m9412a(256, obj, true);
        }
        if (!m9430f(str)) {
            i = 100;
        }
        return m9412a(i, obj, false);
    }

    /* renamed from: a */
    static Bundle[] m9423a(Object obj) {
        if (obj instanceof Bundle) {
            return new Bundle[]{(Bundle) obj};
        } else if (obj instanceof Parcelable[]) {
            Parcelable[] parcelableArr = (Parcelable[]) obj;
            return (Bundle[]) Arrays.copyOf(parcelableArr, parcelableArr.length, Bundle[].class);
        } else if (!(obj instanceof ArrayList)) {
            return null;
        } else {
            ArrayList arrayList = (ArrayList) obj;
            return (Bundle[]) arrayList.toArray(new Bundle[arrayList.size()]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      com.google.android.gms.measurement.internal.z9.a(int, java.lang.Object, boolean):java.lang.Object
      com.google.android.gms.measurement.internal.z9.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String, java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      com.google.android.gms.measurement.internal.z9.a(java.lang.String, int, boolean):java.lang.String */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        if (mo19443a("event param", 40, r15) == false) goto L_0x0056;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x007e, code lost:
        if (mo19443a("event param", 40, r15) == false) goto L_0x0071;
     */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0081  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0087  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x009e  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00d7  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle mo19427a(java.lang.String r17, java.lang.String r18, android.os.Bundle r19, @androidx.annotation.Nullable java.util.List<java.lang.String> r20, boolean r21, boolean r22) {
        /*
            r16 = this;
            r6 = r16
            r7 = r17
            r8 = r19
            r9 = r20
            r10 = 0
            if (r8 == 0) goto L_0x0187
            android.os.Bundle r11 = new android.os.Bundle
            r11.<init>(r8)
            com.google.android.gms.measurement.internal.la r0 = r16.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.C3135o.f5453j0
            boolean r0 = r0.mo19154e(r7, r1)
            if (r0 == 0) goto L_0x0026
            java.util.TreeSet r0 = new java.util.TreeSet
            java.util.Set r1 = r19.keySet()
            r0.<init>(r1)
            goto L_0x002a
        L_0x0026:
            java.util.Set r0 = r19.keySet()
        L_0x002a:
            java.util.Iterator r12 = r0.iterator()
            r14 = 0
        L_0x002f:
            boolean r0 = r12.hasNext()
            if (r0 == 0) goto L_0x0188
            java.lang.Object r0 = r12.next()
            r15 = r0
            java.lang.String r15 = (java.lang.String) r15
            r5 = 40
            r0 = 3
            if (r9 == 0) goto L_0x004a
            boolean r1 = r9.contains(r15)
            if (r1 != 0) goto L_0x0048
            goto L_0x004a
        L_0x0048:
            r1 = 0
            goto L_0x0082
        L_0x004a:
            r1 = 14
            java.lang.String r2 = "event param"
            if (r21 == 0) goto L_0x0068
            boolean r3 = r6.mo19444a(r2, r15)
            if (r3 != 0) goto L_0x0058
        L_0x0056:
            r3 = 3
            goto L_0x0069
        L_0x0058:
            boolean r3 = r6.mo19446a(r2, r10, r15)
            if (r3 != 0) goto L_0x0061
            r3 = 14
            goto L_0x0069
        L_0x0061:
            boolean r3 = r6.mo19443a(r2, r5, r15)
            if (r3 != 0) goto L_0x0068
            goto L_0x0056
        L_0x0068:
            r3 = 0
        L_0x0069:
            if (r3 != 0) goto L_0x0081
            boolean r3 = r6.mo19449b(r2, r15)
            if (r3 != 0) goto L_0x0073
        L_0x0071:
            r1 = 3
            goto L_0x0082
        L_0x0073:
            boolean r3 = r6.mo19446a(r2, r10, r15)
            if (r3 != 0) goto L_0x007a
            goto L_0x0082
        L_0x007a:
            boolean r1 = r6.mo19443a(r2, r5, r15)
            if (r1 != 0) goto L_0x0048
            goto L_0x0071
        L_0x0081:
            r1 = r3
        L_0x0082:
            java.lang.String r4 = "_ev"
            r3 = 1
            if (r1 == 0) goto L_0x009e
            boolean r2 = m9417a(r11, r1)
            if (r2 == 0) goto L_0x0099
            java.lang.String r2 = m9413a(r15, r5, r3)
            r11.putString(r4, r2)
            if (r1 != r0) goto L_0x0099
            m9414a(r11, r15)
        L_0x0099:
            r11.remove(r15)
            goto L_0x0140
        L_0x009e:
            java.lang.Object r2 = r8.get(r15)
            r16.mo18881c()
            if (r22 == 0) goto L_0x00dd
            boolean r0 = r2 instanceof android.os.Parcelable[]
            if (r0 == 0) goto L_0x00b0
            r0 = r2
            android.os.Parcelable[] r0 = (android.os.Parcelable[]) r0
            int r0 = r0.length
            goto L_0x00bb
        L_0x00b0:
            boolean r0 = r2 instanceof java.util.ArrayList
            if (r0 == 0) goto L_0x00d4
            r0 = r2
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            int r0 = r0.size()
        L_0x00bb:
            r1 = 1000(0x3e8, float:1.401E-42)
            if (r0 <= r1) goto L_0x00d4
            com.google.android.gms.measurement.internal.f4 r1 = r16.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19006y()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r3 = "param"
            java.lang.String r5 = "Parameter array is too long; discarded. Value kind, name, array length"
            r1.mo19045a(r5, r3, r15, r0)
            r0 = 0
            goto L_0x00d5
        L_0x00d4:
            r0 = 1
        L_0x00d5:
            if (r0 != 0) goto L_0x00dd
            r0 = 17
            r13 = r4
            r10 = 40
            goto L_0x0120
        L_0x00dd:
            com.google.android.gms.measurement.internal.la r0 = r16.mo19013h()
            com.google.android.gms.measurement.internal.x3<java.lang.Boolean> r1 = com.google.android.gms.measurement.internal.C3135o.f5408N
            boolean r0 = r0.mo19154e(r7, r1)
            if (r0 == 0) goto L_0x00ef
            boolean r0 = m9430f(r18)
            if (r0 != 0) goto L_0x00f5
        L_0x00ef:
            boolean r0 = m9430f(r15)
            if (r0 == 0) goto L_0x0109
        L_0x00f5:
            r3 = 256(0x100, float:3.59E-43)
            java.lang.String r1 = "param"
            r0 = r16
            r5 = r2
            r2 = r15
            r10 = 1
            r13 = r4
            r4 = r5
            r10 = 40
            r5 = r22
            boolean r0 = r0.m9419a(r1, r2, r3, r4, r5)
            goto L_0x011b
        L_0x0109:
            r5 = r2
            r13 = r4
            r10 = 40
            r3 = 100
            java.lang.String r1 = "param"
            r0 = r16
            r2 = r15
            r4 = r5
            r5 = r22
            boolean r0 = r0.m9419a(r1, r2, r3, r4, r5)
        L_0x011b:
            if (r0 == 0) goto L_0x011f
            r0 = 0
            goto L_0x0120
        L_0x011f:
            r0 = 4
        L_0x0120:
            if (r0 == 0) goto L_0x0143
            boolean r1 = r13.equals(r15)
            if (r1 != 0) goto L_0x0143
            boolean r0 = m9417a(r11, r0)
            if (r0 == 0) goto L_0x013d
            r0 = 1
            java.lang.String r0 = m9413a(r15, r10, r0)
            r11.putString(r13, r0)
            java.lang.Object r0 = r8.get(r15)
            m9414a(r11, r0)
        L_0x013d:
            r11.remove(r15)
        L_0x0140:
            r10 = 0
            goto L_0x002f
        L_0x0143:
            boolean r0 = m9429e(r15)
            if (r0 == 0) goto L_0x0184
            int r14 = r14 + 1
            r0 = 25
            if (r14 <= r0) goto L_0x0184
            r0 = 48
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r0 = "Event can't contain more than 25 params"
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.google.android.gms.measurement.internal.f4 r1 = r16.mo19015l()
            com.google.android.gms.measurement.internal.h4 r1 = r1.mo19003v()
            com.google.android.gms.measurement.internal.c4 r2 = r16.mo19010e()
            r3 = r18
            java.lang.String r2 = r2.mo18822a(r3)
            com.google.android.gms.measurement.internal.c4 r4 = r16.mo19010e()
            java.lang.String r4 = r4.mo18819a(r8)
            r1.mo19044a(r0, r2, r4)
            r0 = 5
            m9417a(r11, r0)
            r11.remove(r15)
            goto L_0x0140
        L_0x0184:
            r3 = r18
            goto L_0x0140
        L_0x0187:
            r11 = 0
        L_0x0188:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.internal.C3267z9.mo19427a(java.lang.String, java.lang.String, android.os.Bundle, java.util.List, boolean, boolean):android.os.Bundle");
    }

    /* renamed from: a */
    private static boolean m9417a(Bundle bundle, int i) {
        if (bundle.getLong("_err") != 0) {
            return false;
        }
        bundle.putLong("_err", (long) i);
        return true;
    }

    /* renamed from: a */
    private static void m9414a(Bundle bundle, Object obj) {
        C2258v.m5629a(bundle);
        if (obj == null) {
            return;
        }
        if ((obj instanceof String) || (obj instanceof CharSequence)) {
            bundle.putLong("_el", (long) String.valueOf(obj).length());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19433a(Bundle bundle, String str, Object obj) {
        if (bundle != null) {
            if (obj instanceof Long) {
                bundle.putLong(str, ((Long) obj).longValue());
            } else if (obj instanceof String) {
                bundle.putString(str, String.valueOf(obj));
            } else if (obj instanceof Double) {
                bundle.putDouble(str, ((Double) obj).doubleValue());
            } else if (str != null) {
                mo19015l().mo19006y().mo19044a("Not putting event parameter. Invalid value type. name, type", mo19010e().mo18823b(str), obj != null ? obj.getClass().getSimpleName() : null);
            }
        }
    }

    /* renamed from: a */
    public final void mo19431a(int i, String str, String str2, int i2) {
        mo19441a((String) null, i, str, str2, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo19441a(String str, int i, String str2, String str3, int i2) {
        Bundle bundle = new Bundle();
        m9417a(bundle, i);
        if (!TextUtils.isEmpty(str2) && !TextUtils.isEmpty(str3)) {
            bundle.putString(str2, str3);
        }
        if (i == 6 || i == 7 || i == 2) {
            bundle.putLong("_el", (long) i2);
        }
        this.f5134a.mo19018r();
        this.f5134a.mo19103v().mo19267a("auto", "_err", bundle);
    }

    /* renamed from: a */
    static long m9410a(byte[] bArr) {
        C2258v.m5629a(bArr);
        int i = 0;
        C2258v.m5640b(bArr.length > 0);
        long j = 0;
        int length = bArr.length - 1;
        while (length >= 0 && length >= bArr.length - 8) {
            j += (((long) bArr[length]) & 255) << i;
            i += 8;
            length--;
        }
        return j;
    }

    /* renamed from: a */
    static boolean m9415a(Context context, boolean z) {
        C2258v.m5629a(context);
        if (Build.VERSION.SDK_INT >= 24) {
            return m9426b(context, "com.google.android.gms.measurement.AppMeasurementJobService");
        }
        return m9426b(context, "com.google.android.gms.measurement.AppMeasurementService");
    }

    /* renamed from: a */
    static boolean m9418a(Boolean bool, Boolean bool2) {
        if (bool == null && bool2 == null) {
            return true;
        }
        if (bool == null) {
            return false;
        }
        return bool.equals(bool2);
    }

    /* renamed from: a */
    static boolean m9421a(@Nullable List<String> list, @Nullable List<String> list2) {
        if (list == null && list2 == null) {
            return true;
        }
        if (list == null) {
            return false;
        }
        return list.equals(list2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Bundle mo19426a(Bundle bundle) {
        Bundle bundle2 = new Bundle();
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                Object a = mo19429a(str, bundle.get(str));
                if (a == null) {
                    mo19015l().mo19006y().mo19043a("Param value can't be null", mo19010e().mo18823b(str));
                } else {
                    mo19433a(bundle2, str, a);
                }
            }
        }
        return bundle2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final zzan mo19428a(String str, String str2, Bundle bundle, String str3, long j, boolean z, boolean z2) {
        Bundle bundle2;
        if (TextUtils.isEmpty(str2)) {
            return null;
        }
        if (mo19423a(str2) == 0) {
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            Bundle bundle3 = bundle2;
            bundle3.putString("_o", str3);
            return new zzan(str2, new zzam(mo19426a(mo19427a(str, str2, bundle3, C2315f.m5776a("_o"), false, false))), str3, j);
        }
        mo19015l().mo19001t().mo19043a("Invalid conditional property event name", mo19010e().mo18824c(str2));
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final long mo19424a(Context context, String str) {
        mo18881c();
        C2258v.m5629a(context);
        C2258v.m5639b(str);
        PackageManager packageManager = context.getPackageManager();
        MessageDigest y = m9433y();
        if (y == null) {
            mo19015l().mo19001t().mo19042a("Could not get MD5 instance");
            return -1;
        }
        if (packageManager != null) {
            try {
                if (!m9427c(context, str)) {
                    PackageInfo b = C2283c.m5685a(context).mo17060b(mo19016n().getPackageName(), 64);
                    if (b.signatures != null && b.signatures.length > 0) {
                        return m9410a(y.digest(b.signatures[0].toByteArray()));
                    }
                    mo19015l().mo19004w().mo19042a("Could not get signatures");
                    return -1;
                }
            } catch (PackageManager.NameNotFoundException e) {
                mo19015l().mo19001t().mo19043a("Package name not found", e);
            }
        }
        return 0;
    }

    /* renamed from: a */
    static byte[] m9422a(Parcelable parcelable) {
        if (parcelable == null) {
            return null;
        }
        Parcel obtain = Parcel.obtain();
        try {
            parcelable.writeToParcel(obtain, 0);
            return obtain.marshall();
        } finally {
            obtain.recycle();
        }
    }

    /* renamed from: a */
    public final int mo19422a(int i) {
        return C2169c.m5270a().mo16820a(mo19016n(), (int) C2176f.f3566a);
    }

    /* renamed from: a */
    public static long m9409a(long j, long j2) {
        return (j + (j2 * DefaultLoadErrorHandlingPolicy.DEFAULT_TRACK_BLACKLIST_MS)) / 86400000;
    }

    /* access modifiers changed from: package-private */
    @WorkerThread
    /* renamed from: a */
    public final void mo19432a(Bundle bundle, long j) {
        long j2 = bundle.getLong("_et");
        if (j2 != 0) {
            mo19015l().mo19004w().mo19043a("Params already contained engagement", Long.valueOf(j2));
        }
        bundle.putLong("_et", j + j2);
    }

    /* renamed from: a */
    public final void mo19437a(C2589kc kcVar, String str) {
        Bundle bundle = new Bundle();
        bundle.putString("r", str);
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning string value to wrapper", e);
        }
    }

    /* renamed from: a */
    public final void mo19435a(C2589kc kcVar, long j) {
        Bundle bundle = new Bundle();
        bundle.putLong("r", j);
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning long value to wrapper", e);
        }
    }

    /* renamed from: a */
    public final void mo19434a(C2589kc kcVar, int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("r", i);
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning int value to wrapper", e);
        }
    }

    /* renamed from: a */
    public final void mo19440a(C2589kc kcVar, byte[] bArr) {
        Bundle bundle = new Bundle();
        bundle.putByteArray("r", bArr);
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning byte array to wrapper", e);
        }
    }

    /* renamed from: a */
    public final void mo19439a(C2589kc kcVar, boolean z) {
        Bundle bundle = new Bundle();
        bundle.putBoolean("r", z);
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning boolean value to wrapper", e);
        }
    }

    /* renamed from: a */
    public final void mo19436a(C2589kc kcVar, Bundle bundle) {
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning bundle value to wrapper", e);
        }
    }

    /* renamed from: a */
    public static Bundle m9411a(List<zzkq> list) {
        Bundle bundle = new Bundle();
        if (list == null) {
            return bundle;
        }
        for (zzkq zzkq : list) {
            String str = zzkq.f5811T;
            if (str != null) {
                bundle.putString(zzkq.f5808Q, str);
            } else {
                Long l = zzkq.f5810S;
                if (l != null) {
                    bundle.putLong(zzkq.f5808Q, l.longValue());
                } else {
                    Double d = zzkq.f5813V;
                    if (d != null) {
                        bundle.putDouble(zzkq.f5808Q, d.doubleValue());
                    }
                }
            }
        }
        return bundle;
    }

    /* renamed from: a */
    public final void mo19438a(C2589kc kcVar, ArrayList<Bundle> arrayList) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("r", arrayList);
        try {
            kcVar.mo17292d(bundle);
        } catch (RemoteException e) {
            this.f5134a.mo19015l().mo19004w().mo19043a("Error returning bundle list to wrapper", e);
        }
    }

    /* renamed from: a */
    public final URL mo19430a(long j, @NonNull String str, @NonNull String str2, long j2) {
        try {
            C2258v.m5639b(str2);
            C2258v.m5639b(str);
            String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", String.format("v%s.%s", Long.valueOf(j), Integer.valueOf(mo19455v())), str2, str, Long.valueOf(j2));
            if (str.equals(mo19013h().mo19163t())) {
                format = format.concat("&ddl_test=1");
            }
            return new URL(format);
        } catch (IllegalArgumentException | MalformedURLException e) {
            mo19015l().mo19001t().mo19043a("Failed to create BOW URL for Deferred Deep Link. exception", e.getMessage());
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"ApplySharedPref"})
    /* renamed from: a */
    public final boolean mo19442a(String str, double d) {
        try {
            SharedPreferences.Editor edit = mo19016n().getSharedPreferences("google.analytics.deferred.deeplink.prefs", 0).edit();
            edit.putString("deeplink", str);
            edit.putLong("timestamp", Double.doubleToRawLongBits(d));
            return edit.commit();
        } catch (Exception e) {
            mo19015l().mo19001t().mo19043a("Failed to persist Deferred Deep Link. exception", e);
            return false;
        }
    }
}
