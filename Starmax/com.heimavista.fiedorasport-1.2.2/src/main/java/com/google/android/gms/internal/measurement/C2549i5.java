package com.google.android.gms.internal.measurement;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* renamed from: com.google.android.gms.internal.measurement.i5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2549i5 extends C2813z2<Long> implements C2691r4, C2485e6, RandomAccess {

    /* renamed from: S */
    private static final C2549i5 f4233S;

    /* renamed from: Q */
    private long[] f4234Q;

    /* renamed from: R */
    private int f4235R;

    static {
        C2549i5 i5Var = new C2549i5(new long[0], 0);
        f4233S = i5Var;
        super.mo17939e();
    }

    C2549i5() {
        this(new long[10], 0);
    }

    /* renamed from: d */
    private final void m6474d(int i) {
        if (i < 0 || i >= this.f4235R) {
            throw new IndexOutOfBoundsException(m6475e(i));
        }
    }

    /* renamed from: e */
    private final String m6475e(int i) {
        int i2 = this.f4235R;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i);
        sb.append(", Size:");
        sb.append(i2);
        return sb.toString();
    }

    /* renamed from: g */
    public static C2549i5 m6476g() {
        return f4233S;
    }

    public final /* synthetic */ void add(int i, Object obj) {
        int i2;
        long longValue = ((Long) obj).longValue();
        mo18179b();
        if (i < 0 || i > (i2 = this.f4235R)) {
            throw new IndexOutOfBoundsException(m6475e(i));
        }
        long[] jArr = this.f4234Q;
        if (i2 < jArr.length) {
            System.arraycopy(jArr, i, jArr, i + 1, i2 - i);
        } else {
            long[] jArr2 = new long[(((i2 * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            System.arraycopy(this.f4234Q, i, jArr2, i + 1, this.f4235R - i);
            this.f4234Q = jArr2;
        }
        this.f4234Q[i] = longValue;
        this.f4235R++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Long> collection) {
        mo18179b();
        C2647o4.m6961a(collection);
        if (!(collection instanceof C2549i5)) {
            return super.addAll(collection);
        }
        C2549i5 i5Var = (C2549i5) collection;
        int i = i5Var.f4235R;
        if (i == 0) {
            return false;
        }
        int i2 = this.f4235R;
        if (Integer.MAX_VALUE - i2 >= i) {
            int i3 = i2 + i;
            long[] jArr = this.f4234Q;
            if (i3 > jArr.length) {
                this.f4234Q = Arrays.copyOf(jArr, i3);
            }
            System.arraycopy(i5Var.f4234Q, 0, this.f4234Q, this.f4235R, i5Var.f4235R);
            this.f4235R = i3;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    /* renamed from: b */
    public final long mo17573b(int i) {
        m6474d(i);
        return this.f4234Q[i];
    }

    /* renamed from: c */
    public final C2691r4 mo17320a(int i) {
        if (i >= this.f4235R) {
            return new C2549i5(Arrays.copyOf(this.f4234Q, i), this.f4235R);
        }
        throw new IllegalArgumentException();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C2549i5)) {
            return super.equals(obj);
        }
        C2549i5 i5Var = (C2549i5) obj;
        if (this.f4235R != i5Var.f4235R) {
            return false;
        }
        long[] jArr = i5Var.f4234Q;
        for (int i = 0; i < this.f4235R; i++) {
            if (this.f4234Q[i] != jArr[i]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i) {
        return Long.valueOf(mo17573b(i));
    }

    public final int hashCode() {
        int i = 1;
        for (int i2 = 0; i2 < this.f4235R; i2++) {
            i = (i * 31) + C2647o4.m6959a(this.f4234Q[i2]);
        }
        return i;
    }

    public final boolean remove(Object obj) {
        mo18179b();
        for (int i = 0; i < this.f4235R; i++) {
            if (obj.equals(Long.valueOf(this.f4234Q[i]))) {
                long[] jArr = this.f4234Q;
                System.arraycopy(jArr, i + 1, jArr, i, (this.f4235R - i) - 1);
                this.f4235R--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public final void removeRange(int i, int i2) {
        mo18179b();
        if (i2 >= i) {
            long[] jArr = this.f4234Q;
            System.arraycopy(jArr, i2, jArr, i, this.f4235R - i2);
            this.f4235R -= i2 - i;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i, Object obj) {
        long longValue = ((Long) obj).longValue();
        mo18179b();
        m6474d(i);
        long[] jArr = this.f4234Q;
        long j = jArr[i];
        jArr[i] = longValue;
        return Long.valueOf(j);
    }

    public final int size() {
        return this.f4235R;
    }

    private C2549i5(long[] jArr, int i) {
        this.f4234Q = jArr;
        this.f4235R = i;
    }

    /* renamed from: g */
    public final void mo17575g(long j) {
        mo18179b();
        int i = this.f4235R;
        long[] jArr = this.f4234Q;
        if (i == jArr.length) {
            long[] jArr2 = new long[(((i * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i);
            this.f4234Q = jArr2;
        }
        long[] jArr3 = this.f4234Q;
        int i2 = this.f4235R;
        this.f4235R = i2 + 1;
        jArr3[i2] = j;
    }

    public final /* synthetic */ Object remove(int i) {
        mo18179b();
        m6474d(i);
        long[] jArr = this.f4234Q;
        long j = jArr[i];
        int i2 = this.f4235R;
        if (i < i2 - 1) {
            System.arraycopy(jArr, i + 1, jArr, i, (i2 - i) - 1);
        }
        this.f4235R--;
        this.modCount++;
        return Long.valueOf(j);
    }

    public final /* synthetic */ boolean add(Object obj) {
        mo17575g(((Long) obj).longValue());
        return true;
    }
}
