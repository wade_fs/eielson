package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.k */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C1741k implements C3969b.C3970a {

    /* renamed from: a */
    private final C3934c f2751a;

    private C1741k(C3934c cVar) {
        this.f2751a = cVar;
    }

    /* renamed from: a */
    public static C3969b.C3970a m4349a(C3934c cVar) {
        return new C1741k(cVar);
    }

    /* renamed from: s */
    public Object mo13587s() {
        return Integer.valueOf(this.f2751a.mo23593r());
    }
}
