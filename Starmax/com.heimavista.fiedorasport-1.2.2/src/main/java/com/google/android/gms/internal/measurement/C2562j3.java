package com.google.android.gms.internal.measurement;

import java.util.Arrays;

/* renamed from: com.google.android.gms.internal.measurement.j3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2562j3 implements C2594l3 {
    private C2562j3() {
    }

    /* renamed from: a */
    public final byte[] mo17587a(byte[] bArr, int i, int i2) {
        return Arrays.copyOfRange(bArr, i, i2 + i);
    }

    /* synthetic */ C2562j3(C2482e3 e3Var) {
        this();
    }
}
