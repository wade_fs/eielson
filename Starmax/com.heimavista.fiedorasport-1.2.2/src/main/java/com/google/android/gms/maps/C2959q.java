package com.google.android.gms.maps;

import com.google.android.gms.maps.SupportStreetViewPanoramaFragment;
import com.google.android.gms.maps.p093i.C2897e;
import com.google.android.gms.maps.p093i.C2917v;

/* renamed from: com.google.android.gms.maps.q */
final class C2959q extends C2917v {

    /* renamed from: a */
    private final /* synthetic */ C2886f f4918a;

    C2959q(SupportStreetViewPanoramaFragment.C2876a aVar, C2886f fVar) {
        this.f4918a = fVar;
    }

    /* renamed from: a */
    public final void mo18489a(C2897e eVar) {
        this.f4918a.mo18412a(new C2887g(eVar));
    }
}
