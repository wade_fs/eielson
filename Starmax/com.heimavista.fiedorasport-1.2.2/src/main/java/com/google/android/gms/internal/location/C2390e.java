package com.google.android.gms.internal.location;

import android.os.Parcel;

/* renamed from: com.google.android.gms.internal.location.e */
public abstract class C2390e extends C2402q implements C2389d {
    public C2390e() {
        super("com.google.android.gms.location.internal.IFusedLocationProviderCallback");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final boolean mo17203a(int i, Parcel parcel, Parcel parcel2, int i2) {
        if (i != 1) {
            return false;
        }
        mo17202a((zzad) C2409x.m5938a(parcel, zzad.CREATOR));
        return true;
    }
}
