package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ib */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2555ib implements C2593l2<C2540hb> {

    /* renamed from: Q */
    private static C2555ib f4241Q = new C2555ib();

    /* renamed from: P */
    private final C2593l2<C2540hb> f4242P;

    private C2555ib(C2593l2<C2540hb> l2Var) {
        this.f4242P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6488b() {
        return ((C2540hb) f4241Q.mo17285a()).mo17565a();
    }

    /* renamed from: c */
    public static boolean m6489c() {
        return ((C2540hb) f4241Q.mo17285a()).mo17566e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4242P.mo17285a();
    }

    public C2555ib() {
        this(C2579k2.m6601a(new C2608lb()));
    }
}
