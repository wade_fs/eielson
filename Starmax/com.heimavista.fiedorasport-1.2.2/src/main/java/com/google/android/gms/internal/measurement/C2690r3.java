package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.r3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2690r3 {
    private C2690r3() {
    }

    /* renamed from: a */
    public static int m7119a(int i) {
        return (-(i & 1)) ^ (i >>> 1);
    }

    /* renamed from: a */
    public static long m7120a(long j) {
        return (-(j & 1)) ^ (j >>> 1);
    }

    /* renamed from: a */
    static C2690r3 m7121a(byte[] bArr, int i, int i2, boolean z) {
        C2705s3 s3Var = new C2705s3(bArr, 0, i2, false);
        try {
            s3Var.mo17874b(i2);
            return s3Var;
        } catch (C2723t4 e) {
            throw new IllegalArgumentException(e);
        }
    }
}
