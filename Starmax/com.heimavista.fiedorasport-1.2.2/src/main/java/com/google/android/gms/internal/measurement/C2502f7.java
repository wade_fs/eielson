package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.f7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2502f7 extends C2469d7<C2453c7, C2453c7> {
    C2502f7() {
    }

    /* renamed from: a */
    private static void m6322a(Object obj, C2453c7 c7Var) {
        ((C2595l4) obj).zzb = c7Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final void mo17423b(Object obj) {
        ((C2595l4) obj).zzb.mo17374a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final /* synthetic */ int mo17426c(Object obj) {
        return ((C2453c7) obj).mo17378b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public final /* synthetic */ int mo17428d(Object obj) {
        return ((C2453c7) obj).mo17380c();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo17421a(Object obj, C2771w7 w7Var) {
        ((C2453c7) obj).mo17379b(w7Var);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final /* synthetic */ void mo17424b(Object obj, C2771w7 w7Var) {
        ((C2453c7) obj).mo17376a(w7Var);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public final /* synthetic */ Object mo17427c(Object obj, Object obj2) {
        C2453c7 c7Var = (C2453c7) obj;
        C2453c7 c7Var2 = (C2453c7) obj2;
        if (c7Var2.equals(C2453c7.m6143d())) {
            return c7Var;
        }
        return C2453c7.m6141a(c7Var, c7Var2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ Object mo17419a(Object obj) {
        return ((C2595l4) obj).zzb;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* bridge */ /* synthetic */ void mo17422a(Object obj, Object obj2) {
        m6322a(obj, (C2453c7) obj2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final /* synthetic */ void mo17425b(Object obj, Object obj2) {
        m6322a(obj, (C2453c7) obj2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ Object mo17418a() {
        return C2453c7.m6144e();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final /* synthetic */ void mo17420a(Object obj, int i, long j) {
        ((C2453c7) obj).mo17375a(i << 3, Long.valueOf(j));
    }
}
