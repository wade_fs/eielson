package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.ea */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2489ea implements C2593l2<C2539ha> {

    /* renamed from: Q */
    private static C2489ea f4077Q = new C2489ea();

    /* renamed from: P */
    private final C2593l2<C2539ha> f4078P;

    private C2489ea(C2593l2<C2539ha> l2Var) {
        this.f4078P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6262b() {
        return ((C2539ha) f4077Q.mo17285a()).mo17503a();
    }

    /* renamed from: c */
    public static boolean m6263c() {
        return ((C2539ha) f4077Q.mo17285a()).mo17504e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4078P.mo17285a();
    }

    public C2489ea() {
        this(C2579k2.m6601a(new C2522ga()));
    }
}
