package com.google.android.exoplayer2.trackselection;

import com.google.android.exoplayer2.source.chunk.MediaChunkIterator;
import java.util.List;

/* renamed from: com.google.android.exoplayer2.trackselection.a */
/* compiled from: TrackSelection */
public final /* synthetic */ class C1902a {
    @Deprecated
    public static void $default$updateSelectedTrack(TrackSelection _this, long j, long j2, long j3) {
        throw new UnsupportedOperationException();
    }

    /* Incorrect method signature, types: long, long, long, java.util.List<? extends com.google.android.exoplayer2.source.chunk.MediaChunk>, com.google.android.exoplayer2.source.chunk.MediaChunkIterator[] */
    public static void $default$updateSelectedTrack(TrackSelection _this, long j, long j2, long j3, List list, MediaChunkIterator[] mediaChunkIteratorArr) {
        _this.updateSelectedTrack(j, j2, j3);
    }
}
