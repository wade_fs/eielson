package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.b3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2433b3 {
    /* renamed from: a */
    static int m6055a(byte[] bArr, int i, C2417a3 a3Var) {
        int i2 = i + 1;
        byte b = bArr[i];
        if (b < 0) {
            return m6050a(b, bArr, i2, a3Var);
        }
        a3Var.f3971a = b;
        return i2;
    }

    /* renamed from: b */
    static int m6057b(byte[] bArr, int i, C2417a3 a3Var) {
        int i2 = i + 1;
        long j = (long) bArr[i];
        if (j >= 0) {
            a3Var.f3972b = j;
            return i2;
        }
        int i3 = i2 + 1;
        byte b = bArr[i2];
        long j2 = (j & 127) | (((long) (b & Byte.MAX_VALUE)) << 7);
        int i4 = 7;
        while (b < 0) {
            int i5 = i3 + 1;
            byte b2 = bArr[i3];
            i4 += 7;
            j2 |= ((long) (b2 & Byte.MAX_VALUE)) << i4;
            int i6 = i5;
            b = b2;
            i3 = i6;
        }
        a3Var.f3972b = j2;
        return i3;
    }

    /* renamed from: c */
    static double m6059c(byte[] bArr, int i) {
        return Double.longBitsToDouble(m6058b(bArr, i));
    }

    /* renamed from: d */
    static float m6061d(byte[] bArr, int i) {
        return Float.intBitsToFloat(m6054a(bArr, i));
    }

    /* renamed from: e */
    static int m6063e(byte[] bArr, int i, C2417a3 a3Var) {
        int a = m6055a(bArr, i, a3Var);
        int i2 = a3Var.f3971a;
        if (i2 < 0) {
            throw C2723t4.m7312e();
        } else if (i2 > bArr.length - a) {
            throw C2723t4.m7311a();
        } else if (i2 == 0) {
            a3Var.f3973c = C2498f3.f4094Q;
            return a;
        } else {
            a3Var.f3973c = C2498f3.m6303a(bArr, a, i2);
            return a + i2;
        }
    }

    /* renamed from: c */
    static int m6060c(byte[] bArr, int i, C2417a3 a3Var) {
        int a = m6055a(bArr, i, a3Var);
        int i2 = a3Var.f3971a;
        if (i2 < 0) {
            throw C2723t4.m7312e();
        } else if (i2 == 0) {
            a3Var.f3973c = "";
            return a;
        } else {
            a3Var.f3973c = new String(bArr, a, i2, C2647o4.f4367a);
            return a + i2;
        }
    }

    /* renamed from: d */
    static int m6062d(byte[] bArr, int i, C2417a3 a3Var) {
        int a = m6055a(bArr, i, a3Var);
        int i2 = a3Var.f3971a;
        if (i2 < 0) {
            throw C2723t4.m7312e();
        } else if (i2 == 0) {
            a3Var.f3973c = "";
            return a;
        } else {
            a3Var.f3973c = C2604l7.m6700b(bArr, a, i2);
            return a + i2;
        }
    }

    /* renamed from: a */
    static int m6050a(int i, byte[] bArr, int i2, C2417a3 a3Var) {
        int i3 = i & 127;
        int i4 = i2 + 1;
        byte b = bArr[i2];
        if (b >= 0) {
            a3Var.f3971a = i3 | (b << 7);
            return i4;
        }
        int i5 = i3 | ((b & Byte.MAX_VALUE) << 7);
        int i6 = i4 + 1;
        byte b2 = bArr[i4];
        if (b2 >= 0) {
            a3Var.f3971a = i5 | (b2 << 14);
            return i6;
        }
        int i7 = i5 | ((b2 & Byte.MAX_VALUE) << 14);
        int i8 = i6 + 1;
        byte b3 = bArr[i6];
        if (b3 >= 0) {
            a3Var.f3971a = i7 | (b3 << 21);
            return i8;
        }
        int i9 = i7 | ((b3 & Byte.MAX_VALUE) << 21);
        int i10 = i8 + 1;
        byte b4 = bArr[i8];
        if (b4 >= 0) {
            a3Var.f3971a = i9 | (b4 << 28);
            return i10;
        }
        int i11 = i9 | ((b4 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i12 = i10 + 1;
            if (bArr[i10] >= 0) {
                a3Var.f3971a = i11;
                return i12;
            }
            i10 = i12;
        }
    }

    /* renamed from: b */
    static long m6058b(byte[] bArr, int i) {
        return ((((long) bArr[i + 7]) & 255) << 56) | (((long) bArr[i]) & 255) | ((((long) bArr[i + 1]) & 255) << 8) | ((((long) bArr[i + 2]) & 255) << 16) | ((((long) bArr[i + 3]) & 255) << 24) | ((((long) bArr[i + 4]) & 255) << 32) | ((((long) bArr[i + 5]) & 255) << 40) | ((((long) bArr[i + 6]) & 255) << 48);
    }

    /* renamed from: a */
    static int m6054a(byte[] bArr, int i) {
        return ((bArr[i + 3] & 255) << 24) | (bArr[i] & 255) | ((bArr[i + 1] & 255) << 8) | ((bArr[i + 2] & 255) << 16);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v1 */
    /* JADX WARN: Type inference failed for: r8v4, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int m6053a(com.google.android.gms.internal.measurement.C2603l6 r6, byte[] r7, int r8, int r9, com.google.android.gms.internal.measurement.C2417a3 r10) {
        /*
            int r0 = r8 + 1
            byte r8 = r7[r8]
            if (r8 >= 0) goto L_0x000c
            int r0 = m6050a(r8, r7, r0, r10)
            int r8 = r10.f3971a
        L_0x000c:
            r3 = r0
            if (r8 < 0) goto L_0x0025
            int r9 = r9 - r3
            if (r8 > r9) goto L_0x0025
            java.lang.Object r9 = r6.mo17277a()
            int r8 = r8 + r3
            r0 = r6
            r1 = r9
            r2 = r7
            r4 = r8
            r5 = r10
            r0.mo17279a(r1, r2, r3, r4, r5)
            r6.mo17283c(r9)
            r10.f3973c = r9
            return r8
        L_0x0025:
            com.google.android.gms.internal.measurement.t4 r6 = com.google.android.gms.internal.measurement.C2723t4.m7311a()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2433b3.m6053a(com.google.android.gms.internal.measurement.l6, byte[], int, int, com.google.android.gms.internal.measurement.a3):int");
    }

    /* renamed from: a */
    static int m6052a(C2603l6 l6Var, byte[] bArr, int i, int i2, int i3, C2417a3 a3Var) {
        C2801y5 y5Var = (C2801y5) l6Var;
        Object a = y5Var.mo17277a();
        int a2 = y5Var.mo18168a(a, bArr, i, i2, i3, a3Var);
        y5Var.mo17283c(a);
        a3Var.f3973c = a;
        return a2;
    }

    /* renamed from: a */
    static int m6049a(int i, byte[] bArr, int i2, int i3, C2738u4<?> u4Var, C2417a3 a3Var) {
        C2616m4 m4Var = (C2616m4) u4Var;
        int a = m6055a(bArr, i2, a3Var);
        m4Var.mo17734d(a3Var.f3971a);
        while (a < i3) {
            int a2 = m6055a(bArr, a, a3Var);
            if (i != a3Var.f3971a) {
                break;
            }
            a = m6055a(bArr, a2, a3Var);
            m4Var.mo17734d(a3Var.f3971a);
        }
        return a;
    }

    /* renamed from: a */
    static int m6056a(byte[] bArr, int i, C2738u4<?> u4Var, C2417a3 a3Var) {
        C2616m4 m4Var = (C2616m4) u4Var;
        int a = m6055a(bArr, i, a3Var);
        int i2 = a3Var.f3971a + a;
        while (a < i2) {
            a = m6055a(bArr, a, a3Var);
            m4Var.mo17734d(a3Var.f3971a);
        }
        if (a == i2) {
            return a;
        }
        throw C2723t4.m7311a();
    }

    /* renamed from: a */
    static int m6051a(C2603l6<?> l6Var, int i, byte[] bArr, int i2, int i3, C2738u4<?> u4Var, C2417a3 a3Var) {
        int a = m6053a(l6Var, bArr, i2, i3, a3Var);
        u4Var.add(a3Var.f3973c);
        while (a < i3) {
            int a2 = m6055a(bArr, a, a3Var);
            if (i != a3Var.f3971a) {
                break;
            }
            a = m6053a(l6Var, bArr, a2, i3, a3Var);
            u4Var.add(a3Var.f3973c);
        }
        return a;
    }

    /* renamed from: a */
    static int m6048a(int i, byte[] bArr, int i2, int i3, C2453c7 c7Var, C2417a3 a3Var) {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                int b = m6057b(bArr, i2, a3Var);
                c7Var.mo17375a(i, Long.valueOf(a3Var.f3972b));
                return b;
            } else if (i4 == 1) {
                c7Var.mo17375a(i, Long.valueOf(m6058b(bArr, i2)));
                return i2 + 8;
            } else if (i4 == 2) {
                int a = m6055a(bArr, i2, a3Var);
                int i5 = a3Var.f3971a;
                if (i5 < 0) {
                    throw C2723t4.m7312e();
                } else if (i5 <= bArr.length - a) {
                    if (i5 == 0) {
                        c7Var.mo17375a(i, C2498f3.f4094Q);
                    } else {
                        c7Var.mo17375a(i, C2498f3.m6303a(bArr, a, i5));
                    }
                    return a + i5;
                } else {
                    throw C2723t4.m7311a();
                }
            } else if (i4 == 3) {
                C2453c7 e = C2453c7.m6144e();
                int i6 = (i & -8) | 4;
                int i7 = 0;
                while (true) {
                    if (i2 >= i3) {
                        break;
                    }
                    int a2 = m6055a(bArr, i2, a3Var);
                    int i8 = a3Var.f3971a;
                    i7 = i8;
                    if (i8 == i6) {
                        i2 = a2;
                        break;
                    }
                    int a3 = m6048a(i7, bArr, a2, i3, e, a3Var);
                    i7 = i8;
                    i2 = a3;
                }
                if (i2 > i3 || i7 != i6) {
                    throw C2723t4.m7315h();
                }
                c7Var.mo17375a(i, e);
                return i2;
            } else if (i4 == 5) {
                c7Var.mo17375a(i, Integer.valueOf(m6054a(bArr, i2)));
                return i2 + 4;
            } else {
                throw C2723t4.m7313f();
            }
        } else {
            throw C2723t4.m7313f();
        }
    }

    /* renamed from: a */
    static int m6047a(int i, byte[] bArr, int i2, int i3, C2417a3 a3Var) {
        if ((i >>> 3) != 0) {
            int i4 = i & 7;
            if (i4 == 0) {
                return m6057b(bArr, i2, a3Var);
            }
            if (i4 == 1) {
                return i2 + 8;
            }
            if (i4 == 2) {
                return m6055a(bArr, i2, a3Var) + a3Var.f3971a;
            }
            if (i4 == 3) {
                int i5 = (i & -8) | 4;
                int i6 = 0;
                while (i2 < i3) {
                    i2 = m6055a(bArr, i2, a3Var);
                    i6 = a3Var.f3971a;
                    if (i6 == i5) {
                        break;
                    }
                    i2 = m6047a(i6, bArr, i2, i3, a3Var);
                }
                if (i2 <= i3 && i6 == i5) {
                    return i2;
                }
                throw C2723t4.m7315h();
            } else if (i4 == 5) {
                return i2 + 4;
            } else {
                throw C2723t4.m7313f();
            }
        } else {
            throw C2723t4.m7313f();
        }
    }
}
