package com.google.android.gms.measurement.internal;

import android.app.job.JobParameters;

/* renamed from: com.google.android.gms.measurement.internal.w8 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final /* synthetic */ class C3233w8 implements Runnable {

    /* renamed from: P */
    private final C3211u8 f5730P;

    /* renamed from: Q */
    private final C3032f4 f5731Q;

    /* renamed from: R */
    private final JobParameters f5732R;

    C3233w8(C3211u8 u8Var, C3032f4 f4Var, JobParameters jobParameters) {
        this.f5730P = u8Var;
        this.f5731Q = f4Var;
        this.f5732R = jobParameters;
    }

    public final void run() {
        this.f5730P.mo19341a(this.f5731Q, this.f5732R);
    }
}
