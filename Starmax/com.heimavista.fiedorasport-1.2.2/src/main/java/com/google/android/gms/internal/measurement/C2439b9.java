package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.b9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2439b9 implements C2455c9 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4002a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.collection.event_safelist", true);

    /* renamed from: a */
    public final boolean mo17337a() {
        return f4002a.mo18128b().booleanValue();
    }
}
