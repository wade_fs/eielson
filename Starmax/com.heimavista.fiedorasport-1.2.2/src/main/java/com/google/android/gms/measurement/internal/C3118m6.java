package com.google.android.gms.measurement.internal;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.zzv;

/* renamed from: com.google.android.gms.measurement.internal.m6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3118m6 {

    /* renamed from: a */
    final Context f5344a;

    /* renamed from: b */
    String f5345b;

    /* renamed from: c */
    String f5346c;

    /* renamed from: d */
    String f5347d;

    /* renamed from: e */
    Boolean f5348e;

    /* renamed from: f */
    long f5349f;

    /* renamed from: g */
    zzv f5350g;

    /* renamed from: h */
    boolean f5351h = true;

    public C3118m6(Context context, zzv zzv) {
        C2258v.m5629a(context);
        Context applicationContext = context.getApplicationContext();
        C2258v.m5629a(applicationContext);
        this.f5344a = applicationContext;
        if (zzv != null) {
            this.f5350g = zzv;
            this.f5345b = zzv.f4637U;
            this.f5346c = zzv.f4636T;
            this.f5347d = zzv.f4635S;
            this.f5351h = zzv.f4634R;
            this.f5349f = zzv.f4633Q;
            Bundle bundle = zzv.f4638V;
            if (bundle != null) {
                this.f5348e = Boolean.valueOf(bundle.getBoolean("dataCollectionDefaultEnabled", true));
            }
        }
    }
}
