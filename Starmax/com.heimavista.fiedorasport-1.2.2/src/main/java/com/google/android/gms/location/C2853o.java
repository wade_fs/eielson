package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.location.o */
public final class C2853o implements Parcelable.Creator<LocationSettingsResult> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        Status status = null;
        LocationSettingsStates locationSettingsStates = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 1) {
                status = (Status) C2248a.m5553a(parcel, a, Status.CREATOR);
            } else if (a2 != 2) {
                C2248a.m5550F(parcel, a);
            } else {
                locationSettingsStates = (LocationSettingsStates) C2248a.m5553a(parcel, a, LocationSettingsStates.CREATOR);
            }
        }
        C2248a.m5576q(parcel, b);
        return new LocationSettingsResult(status, locationSettingsStates);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new LocationSettingsResult[i];
    }
}
