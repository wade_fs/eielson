package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.j5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2564j5 extends C2467d5 {
    private C2564j5() {
        super();
    }

    /* renamed from: b */
    private static <E> C2738u4<E> m6505b(Object obj, long j) {
        return (C2738u4) C2566j7.m6544f(obj, j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17416a(Object obj, long j) {
        m6505b(obj, j).mo17939e();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final <E> void mo17417a(Object obj, Object obj2, long j) {
        C2738u4 b = m6505b(obj, j);
        C2738u4 b2 = m6505b(obj2, j);
        int size = b.size();
        int size2 = b2.size();
        if (size > 0 && size2 > 0) {
            if (!b.mo17938a()) {
                b = b.mo17320a(size2 + size);
            }
            b.addAll(b2);
        }
        if (size > 0) {
            b2 = b;
        }
        C2566j7.m6522a(obj, j, b2);
    }
}
