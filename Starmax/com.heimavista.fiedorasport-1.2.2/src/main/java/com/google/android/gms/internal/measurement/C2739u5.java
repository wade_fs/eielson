package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.u5 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2739u5 extends C2769w5 {
    /* renamed from: a */
    void mo17660a(C2720t3 t3Var);

    /* renamed from: b */
    C2785x5 mo17661b();

    /* renamed from: c */
    C2498f3 mo17940c();

    /* renamed from: d */
    C2785x5 mo17662d();

    /* renamed from: e */
    int mo17663e();
}
