package com.google.android.gms.internal.measurement;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Process;
import android.os.UserManager;
import android.util.Log;
import androidx.annotation.GuardedBy;
import androidx.annotation.RequiresApi;

/* renamed from: com.google.android.gms.internal.measurement.e1 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public class C2480e1 {
    @GuardedBy("DirectBootUtils.class")

    /* renamed from: a */
    private static UserManager f4066a;

    /* renamed from: b */
    private static volatile boolean f4067b = (!m6245a());

    private C2480e1() {
    }

    /* renamed from: a */
    public static boolean m6245a() {
        return Build.VERSION.SDK_INT >= 24;
    }

    @RequiresApi(24)
    @TargetApi(24)
    @GuardedBy("DirectBootUtils.class")
    /* renamed from: b */
    private static boolean m6247b(Context context) {
        boolean z;
        int i = 1;
        while (true) {
            z = false;
            if (i > 2) {
                break;
            }
            if (f4066a == null) {
                f4066a = (UserManager) context.getSystemService(UserManager.class);
            }
            UserManager userManager = f4066a;
            if (userManager == null) {
                return true;
            }
            try {
                if (userManager.isUserUnlocked() || !userManager.isUserRunning(Process.myUserHandle())) {
                    z = true;
                }
            } catch (NullPointerException e) {
                Log.w("DirectBootUtils", "Failed to check if user is unlocked.", e);
                f4066a = null;
                i++;
            }
        }
        if (z) {
            f4066a = null;
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0018, code lost:
        return r3;
     */
    @androidx.annotation.RequiresApi(24)
    @android.annotation.TargetApi(24)
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean m6248c(android.content.Context r3) {
        /*
            boolean r0 = com.google.android.gms.internal.measurement.C2480e1.f4067b
            r1 = 1
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            java.lang.Class<com.google.android.gms.internal.measurement.e1> r0 = com.google.android.gms.internal.measurement.C2480e1.class
            monitor-enter(r0)
            boolean r2 = com.google.android.gms.internal.measurement.C2480e1.f4067b     // Catch:{ all -> 0x0019 }
            if (r2 == 0) goto L_0x000f
            monitor-exit(r0)     // Catch:{ all -> 0x0019 }
            return r1
        L_0x000f:
            boolean r3 = m6247b(r3)     // Catch:{ all -> 0x0019 }
            if (r3 == 0) goto L_0x0017
            com.google.android.gms.internal.measurement.C2480e1.f4067b = r3     // Catch:{ all -> 0x0019 }
        L_0x0017:
            monitor-exit(r0)     // Catch:{ all -> 0x0019 }
            return r3
        L_0x0019:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0019 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2480e1.m6248c(android.content.Context):boolean");
    }

    /* renamed from: a */
    public static boolean m6246a(Context context) {
        return !m6245a() || m6248c(context);
    }
}
