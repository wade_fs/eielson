package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.z5 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3263z5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zzm f5789P;

    /* renamed from: Q */
    private final /* synthetic */ C3141o5 f5790Q;

    C3263z5(C3141o5 o5Var, zzm zzm) {
        this.f5790Q = o5Var;
        this.f5789P = zzm;
    }

    public final void run() {
        this.f5790Q.f5496a.mo19237q();
        this.f5790Q.f5496a.mo19222b(this.f5789P);
    }
}
