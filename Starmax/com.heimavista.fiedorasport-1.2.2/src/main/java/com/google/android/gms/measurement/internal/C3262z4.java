package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.MainThread;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.zzv;

/* renamed from: com.google.android.gms.measurement.internal.z4 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3262z4 {

    /* renamed from: a */
    private final C3033f5 f5788a;

    public C3262z4(C3033f5 f5Var) {
        C2258v.m5629a(f5Var);
        this.f5788a = f5Var;
    }

    /* renamed from: a */
    public static boolean m9407a(Context context) {
        ActivityInfo receiverInfo;
        C2258v.m5629a(context);
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (receiverInfo = packageManager.getReceiverInfo(new ComponentName(context, "com.google.android.gms.measurement.AppMeasurementReceiver"), 0)) == null || !receiverInfo.enabled) {
                return false;
            }
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    @MainThread
    /* renamed from: a */
    public final void mo19417a(Context context, Intent intent) {
        C3081j5 a = C3081j5.m8751a(context, (zzv) null);
        C3032f4 l = a.mo19015l();
        if (intent == null) {
            l.mo19004w().mo19042a("Receiver called with null intent");
            return;
        }
        a.mo19018r();
        String action = intent.getAction();
        l.mo18996B().mo19043a("Local receiver got", action);
        if ("com.google.android.gms.measurement.UPLOAD".equals(action)) {
            Intent className = new Intent().setClassName(context, "com.google.android.gms.measurement.AppMeasurementService");
            className.setAction("com.google.android.gms.measurement.UPLOAD");
            l.mo18996B().mo19042a("Starting wakeful intent.");
            this.f5788a.mo18702a(context, className);
        } else if ("com.android.vending.INSTALL_REFERRER".equals(action)) {
            try {
                a.mo19014j().mo19028a(new C2997c5(this, a, l));
            } catch (Exception e) {
                l.mo19004w().mo19043a("Install Referrer Reporter encountered a problem", e);
            }
            BroadcastReceiver.PendingResult a2 = this.f5788a.mo18701a();
            String stringExtra = intent.getStringExtra("referrer");
            if (stringExtra == null) {
                l.mo18996B().mo19042a("Install referrer extras are null");
                if (a2 != null) {
                    a2.finish();
                    return;
                }
                return;
            }
            l.mo19007z().mo19043a("Install referrer extras are", stringExtra);
            if (!stringExtra.contains("?")) {
                String valueOf = String.valueOf(stringExtra);
                stringExtra = valueOf.length() != 0 ? "?".concat(valueOf) : new String("?");
            }
            Bundle a3 = a.mo19104w().mo19425a(Uri.parse(stringExtra));
            if (a3 == null) {
                l.mo18996B().mo19042a("No campaign defined in install referrer broadcast");
                if (a2 != null) {
                    a2.finish();
                    return;
                }
                return;
            }
            long longExtra = intent.getLongExtra("referrer_timestamp_seconds", 0) * 1000;
            if (longExtra == 0) {
                l.mo19004w().mo19042a("Install referrer is missing timestamp");
            }
            a.mo19014j().mo19028a(new C2985b5(this, a, longExtra, a3, context, l, a2));
        }
    }
}
