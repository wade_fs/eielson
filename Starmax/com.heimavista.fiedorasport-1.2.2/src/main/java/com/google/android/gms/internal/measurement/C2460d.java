package com.google.android.gms.internal.measurement;

import android.app.Activity;
import com.google.android.gms.internal.measurement.C2525gd;
import p119e.p144d.p145a.p157c.p160b.C3992d;

/* renamed from: com.google.android.gms.internal.measurement.d */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2460d extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ Activity f4030T;

    /* renamed from: U */
    private final /* synthetic */ String f4031U;

    /* renamed from: V */
    private final /* synthetic */ String f4032V;

    /* renamed from: W */
    private final /* synthetic */ C2525gd f4033W;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2460d(C2525gd gdVar, Activity activity, String str, String str2) {
        super(gdVar);
        this.f4033W = gdVar;
        this.f4030T = activity;
        this.f4031U = str;
        this.f4032V = str2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4033W.f4192g.setCurrentScreen(C3992d.m11990a(this.f4030T), this.f4031U, this.f4032V, super.f4193P);
    }
}
