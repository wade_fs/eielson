package com.google.android.gms.common.internal;

import android.os.IBinder;
import android.os.IInterface;
import com.google.android.gms.common.api.C2016a;

/* renamed from: com.google.android.gms.common.internal.x */
public class C2262x<T extends IInterface> extends C2219h<T> {

    /* renamed from: E */
    private final C2016a.C2029h<T> f3761E;

    /* renamed from: D */
    public C2016a.C2029h<T> mo16448D() {
        return this.f3761E;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public T mo16449a(IBinder iBinder) {
        return this.f3761E.mo16540a(iBinder);
    }

    /* renamed from: i */
    public int mo16451i() {
        return super.mo16451i();
    }

    /* access modifiers changed from: protected */
    /* renamed from: y */
    public String mo16453y() {
        return this.f3761E.mo16542n();
    }

    /* access modifiers changed from: protected */
    /* renamed from: z */
    public String mo16454z() {
        return this.f3761E.mo16543o();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo16913a(int i, T t) {
        this.f3761E.mo16541a(i, t);
    }
}
