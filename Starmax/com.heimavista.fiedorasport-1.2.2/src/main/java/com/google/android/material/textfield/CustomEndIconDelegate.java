package com.google.android.material.textfield;

import androidx.annotation.NonNull;

class CustomEndIconDelegate extends EndIconDelegate {
    CustomEndIconDelegate(@NonNull TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    /* access modifiers changed from: package-private */
    public void initialize() {
        super.textInputLayout.setEndIconOnClickListener(null);
        super.textInputLayout.setEndIconOnLongClickListener(null);
    }
}
