package com.google.android.gms.maps;

import android.os.RemoteException;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2903h;

/* renamed from: com.google.android.gms.maps.h */
public final class C2888h {

    /* renamed from: a */
    private final C2903h f4791a;

    C2888h(C2903h hVar) {
        this.f4791a = hVar;
    }

    /* renamed from: a */
    public final void mo18413a(boolean z) {
        try {
            this.f4791a.mo18482l(z);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: b */
    public final void mo18414b(boolean z) {
        try {
            this.f4791a.mo18481k(z);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    /* renamed from: c */
    public final void mo18415c(boolean z) {
        try {
            this.f4791a.mo18483m(z);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }
}
