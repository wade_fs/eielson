package com.google.android.gms.location;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2251t;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public class ActivityTransitionEvent extends AbstractSafeParcelable {
    public static final Parcelable.Creator<ActivityTransitionEvent> CREATOR = new C2860u();

    /* renamed from: P */
    private final int f4646P;

    /* renamed from: Q */
    private final int f4647Q;

    /* renamed from: R */
    private final long f4648R;

    public ActivityTransitionEvent(int i, int i2, long j) {
        DetectedActivity.m7945b(i);
        ActivityTransition.m7938a(i2);
        this.f4646P = i;
        this.f4647Q = i2;
        this.f4648R = j;
    }

    /* renamed from: c */
    public int mo18196c() {
        return this.f4646P;
    }

    /* renamed from: d */
    public long mo18197d() {
        return this.f4648R;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ActivityTransitionEvent)) {
            return false;
        }
        ActivityTransitionEvent activityTransitionEvent = (ActivityTransitionEvent) obj;
        return this.f4646P == activityTransitionEvent.f4646P && this.f4647Q == activityTransitionEvent.f4647Q && this.f4648R == activityTransitionEvent.f4648R;
    }

    public int hashCode() {
        return C2251t.m5615a(Integer.valueOf(this.f4646P), Integer.valueOf(this.f4647Q), Long.valueOf(this.f4648R));
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        int i = this.f4646P;
        StringBuilder sb2 = new StringBuilder(24);
        sb2.append("ActivityType ");
        sb2.append(i);
        sb.append(sb2.toString());
        sb.append(" ");
        int i2 = this.f4647Q;
        StringBuilder sb3 = new StringBuilder(26);
        sb3.append("TransitionType ");
        sb3.append(i2);
        sb.append(sb3.toString());
        sb.append(" ");
        long j = this.f4648R;
        StringBuilder sb4 = new StringBuilder(41);
        sb4.append("ElapsedRealTimeNanos ");
        sb4.append(j);
        sb.append(sb4.toString());
        return sb.toString();
    }

    /* renamed from: u */
    public int mo18201u() {
        return this.f4647Q;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, mo18196c());
        C2250b.m5591a(parcel, 2, mo18201u());
        C2250b.m5592a(parcel, 3, mo18197d());
        C2250b.m5587a(parcel, a);
    }
}
