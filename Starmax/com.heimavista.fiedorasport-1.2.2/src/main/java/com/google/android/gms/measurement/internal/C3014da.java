package com.google.android.gms.measurement.internal;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;
import java.util.ArrayList;

/* renamed from: com.google.android.gms.measurement.internal.da */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3014da implements Parcelable.Creator<zzm> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = C2248a.m5558b(parcel);
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        long j4 = 0;
        long j5 = 0;
        String str = null;
        String str2 = null;
        String str3 = null;
        String str4 = null;
        String str5 = null;
        String str6 = null;
        String str7 = null;
        Boolean bool = null;
        ArrayList<String> arrayList = null;
        String str8 = null;
        long j6 = -2147483648L;
        boolean z = true;
        boolean z2 = false;
        int i = 0;
        boolean z3 = true;
        boolean z4 = true;
        boolean z5 = false;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            switch (C2248a.m5551a(a)) {
                case 2:
                    str = C2248a.m5573n(parcel2, a);
                    break;
                case 3:
                    str2 = C2248a.m5573n(parcel2, a);
                    break;
                case 4:
                    str3 = C2248a.m5573n(parcel2, a);
                    break;
                case 5:
                    str4 = C2248a.m5573n(parcel2, a);
                    break;
                case 6:
                    j = C2248a.m5546B(parcel2, a);
                    break;
                case 7:
                    j2 = C2248a.m5546B(parcel2, a);
                    break;
                case 8:
                    str5 = C2248a.m5573n(parcel2, a);
                    break;
                case 9:
                    z = C2248a.m5577r(parcel2, a);
                    break;
                case 10:
                    z2 = C2248a.m5577r(parcel2, a);
                    break;
                case 11:
                    j6 = C2248a.m5546B(parcel2, a);
                    break;
                case 12:
                    str6 = C2248a.m5573n(parcel2, a);
                    break;
                case 13:
                    j3 = C2248a.m5546B(parcel2, a);
                    break;
                case 14:
                    j4 = C2248a.m5546B(parcel2, a);
                    break;
                case 15:
                    i = C2248a.m5585z(parcel2, a);
                    break;
                case 16:
                    z3 = C2248a.m5577r(parcel2, a);
                    break;
                case 17:
                    z4 = C2248a.m5577r(parcel2, a);
                    break;
                case 18:
                    z5 = C2248a.m5577r(parcel2, a);
                    break;
                case 19:
                    str7 = C2248a.m5573n(parcel2, a);
                    break;
                case 20:
                default:
                    C2248a.m5550F(parcel2, a);
                    break;
                case 21:
                    bool = C2248a.m5578s(parcel2, a);
                    break;
                case 22:
                    j5 = C2248a.m5546B(parcel2, a);
                    break;
                case 23:
                    arrayList = C2248a.m5575p(parcel2, a);
                    break;
                case 24:
                    str8 = C2248a.m5573n(parcel2, a);
                    break;
            }
        }
        C2248a.m5576q(parcel2, b);
        return new zzm(str, str2, str3, str4, j, j2, str5, z, z2, j6, str6, j3, j4, i, z3, z4, z5, str7, bool, j5, arrayList, str8);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new zzm[i];
    }
}
