package com.google.android.gms.maps.model;

import android.os.RemoteException;
import com.google.android.gms.common.internal.C2258v;
import p119e.p144d.p145a.p157c.p161c.p165d.C4046o;

/* renamed from: com.google.android.gms.maps.model.d */
public final class C2933d {

    /* renamed from: a */
    private final C4046o f4914a;

    public C2933d(C4046o oVar) {
        C2258v.m5629a(oVar);
        this.f4914a = oVar;
    }

    /* renamed from: a */
    public final void mo18632a() {
        try {
            this.f4914a.remove();
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    public final boolean equals(Object obj) {
        if (!(obj instanceof C2933d)) {
            return false;
        }
        try {
            return this.f4914a.mo23668a(((C2933d) obj).f4914a);
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }

    public final int hashCode() {
        try {
            return this.f4914a.mo23669k();
        } catch (RemoteException e) {
            throw new C2934e(e);
        }
    }
}
