package com.google.android.gms.maps;

import com.google.android.gms.maps.C2880c;
import com.google.android.gms.maps.model.C2932c;
import com.google.android.gms.maps.p093i.C2914s;
import p119e.p144d.p145a.p157c.p161c.p165d.C4043l;

/* renamed from: com.google.android.gms.maps.r */
final class C2960r extends C2914s {

    /* renamed from: a */
    private final /* synthetic */ C2880c.C2883c f4919a;

    C2960r(C2880c cVar, C2880c.C2883c cVar2) {
        this.f4919a = cVar2;
    }

    /* renamed from: a */
    public final boolean mo18488a(C4043l lVar) {
        return this.f4919a.mo18410a(new C2932c(lVar));
    }
}
