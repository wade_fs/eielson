package com.google.android.gms.measurement.internal;

import android.os.IInterface;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.w3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public interface C3228w3 extends IInterface {
    /* renamed from: a */
    List<zzkq> mo19183a(zzm zzm, boolean z);

    /* renamed from: a */
    List<zzv> mo19184a(String str, String str2, zzm zzm);

    /* renamed from: a */
    List<zzv> mo19185a(String str, String str2, String str3);

    /* renamed from: a */
    List<zzkq> mo19186a(String str, String str2, String str3, boolean z);

    /* renamed from: a */
    List<zzkq> mo19187a(String str, String str2, boolean z, zzm zzm);

    /* renamed from: a */
    void mo19188a(long j, String str, String str2, String str3);

    /* renamed from: a */
    void mo19189a(zzan zzan, zzm zzm);

    /* renamed from: a */
    void mo19190a(zzan zzan, String str, String str2);

    /* renamed from: a */
    void mo19191a(zzkq zzkq, zzm zzm);

    /* renamed from: a */
    void mo19192a(zzm zzm);

    /* renamed from: a */
    void mo19193a(zzv zzv);

    /* renamed from: a */
    void mo19194a(zzv zzv, zzm zzm);

    /* renamed from: a */
    byte[] mo19195a(zzan zzan, String str);

    /* renamed from: b */
    void mo19197b(zzm zzm);

    /* renamed from: c */
    String mo19198c(zzm zzm);

    /* renamed from: d */
    void mo19199d(zzm zzm);
}
