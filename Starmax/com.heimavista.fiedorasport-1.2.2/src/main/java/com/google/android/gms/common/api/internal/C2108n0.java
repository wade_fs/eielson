package com.google.android.gms.common.api.internal;

import android.os.Bundle;
import com.google.android.gms.common.api.C2036f;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.common.api.internal.n0 */
final class C2108n0 implements C2036f.C2038b {

    /* renamed from: a */
    private final /* synthetic */ AtomicReference f3389a;

    /* renamed from: b */
    private final /* synthetic */ C2107n f3390b;

    /* renamed from: c */
    private final /* synthetic */ C2100l0 f3391c;

    C2108n0(C2100l0 l0Var, AtomicReference atomicReference, C2107n nVar) {
        this.f3391c = l0Var;
        this.f3389a = atomicReference;
        this.f3390b = nVar;
    }

    /* renamed from: L */
    public final void mo16583L(int i) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.api.internal.l0.a(com.google.android.gms.common.api.internal.l0, com.google.android.gms.common.api.f, com.google.android.gms.common.api.internal.n, boolean):void
     arg types: [com.google.android.gms.common.api.internal.l0, com.google.android.gms.common.api.f, com.google.android.gms.common.api.internal.n, int]
     candidates:
      com.google.android.gms.common.api.internal.l0.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      com.google.android.gms.common.api.f.a(java.lang.String, java.io.FileDescriptor, java.io.PrintWriter, java.lang.String[]):void
      com.google.android.gms.common.api.internal.l0.a(com.google.android.gms.common.api.internal.l0, com.google.android.gms.common.api.f, com.google.android.gms.common.api.internal.n, boolean):void */
    /* renamed from: f */
    public final void mo16584f(Bundle bundle) {
        this.f3391c.m4952a((C2036f) this.f3389a.get(), this.f3390b, true);
    }
}
