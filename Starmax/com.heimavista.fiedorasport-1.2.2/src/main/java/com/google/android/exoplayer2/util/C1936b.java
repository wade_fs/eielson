package com.google.android.exoplayer2.util;

import com.google.android.exoplayer2.util.SlidingPercentile;
import java.util.Comparator;

/* renamed from: com.google.android.exoplayer2.util.b */
/* compiled from: lambda */
public final /* synthetic */ class C1936b implements Comparator {

    /* renamed from: P */
    public static final /* synthetic */ C1936b f2943P = new C1936b();

    private /* synthetic */ C1936b() {
    }

    public final int compare(Object obj, Object obj2) {
        return SlidingPercentile.m4444a((SlidingPercentile.Sample) obj, (SlidingPercentile.Sample) obj2);
    }
}
