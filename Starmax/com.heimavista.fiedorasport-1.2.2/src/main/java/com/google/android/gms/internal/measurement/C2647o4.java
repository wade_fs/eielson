package com.google.android.gms.internal.measurement;

import com.google.android.exoplayer2.C1750C;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

/* renamed from: com.google.android.gms.internal.measurement.o4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public final class C2647o4 {

    /* renamed from: a */
    static final Charset f4367a = Charset.forName(C1750C.UTF8_NAME);

    /* renamed from: b */
    public static final byte[] f4368b;

    static {
        Charset.forName("ISO-8859-1");
        byte[] bArr = new byte[0];
        f4368b = bArr;
        ByteBuffer.wrap(bArr);
        byte[] bArr2 = f4368b;
        C2690r3.m7121a(bArr2, 0, bArr2.length, false);
    }

    /* renamed from: a */
    public static int m6959a(long j) {
        return (int) (j ^ (j >>> 32));
    }

    /* renamed from: a */
    public static int m6960a(boolean z) {
        return z ? 1231 : 1237;
    }

    /* renamed from: a */
    static <T> T m6961a(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException();
    }

    /* renamed from: b */
    public static String m6965b(byte[] bArr) {
        return new String(bArr, f4367a);
    }

    /* renamed from: c */
    public static int m6966c(byte[] bArr) {
        int length = bArr.length;
        int a = m6958a(length, bArr, 0, length);
        if (a == 0) {
            return 1;
        }
        return a;
    }

    /* renamed from: a */
    static <T> T m6963a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    /* renamed from: a */
    public static boolean m6964a(byte[] bArr) {
        return C2604l7.m6695a(bArr);
    }

    /* renamed from: a */
    static int m6958a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        for (int i5 = i2; i5 < i2 + i3; i5++) {
            i4 = (i4 * 31) + bArr[i5];
        }
        return i4;
    }

    /* renamed from: a */
    static Object m6962a(Object obj, Object obj2) {
        return ((C2739u5) obj).mo17661b().mo17951a((C2739u5) obj2).mo17680r();
    }
}
