package com.google.android.datatransport.runtime.backends;

import android.content.Context;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: com.google.android.datatransport.runtime.backends.l */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C1723l implements Factory<C1721k> {

    /* renamed from: a */
    private final Provider<Context> f2713a;

    /* renamed from: b */
    private final Provider<C1719i> f2714b;

    public C1723l(Provider<Context> aVar, Provider<C1719i> aVar2) {
        this.f2713a = aVar;
        this.f2714b = aVar2;
    }

    /* renamed from: a */
    public static C1723l m4303a(Provider<Context> aVar, Provider<C1719i> aVar2) {
        return new C1723l(aVar, aVar2);
    }

    public C1721k get() {
        return new C1721k(this.f2713a.get(), this.f2714b.get());
    }
}
