package com.google.android.exoplayer2.source.ads;

import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.source.ads.AdsMediaSource;

/* renamed from: com.google.android.exoplayer2.source.ads.g */
/* compiled from: lambda */
public final /* synthetic */ class C1865g implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AdsMediaSource f2866P;

    /* renamed from: Q */
    private final /* synthetic */ ExoPlayer f2867Q;

    /* renamed from: R */
    private final /* synthetic */ AdsMediaSource.ComponentListener f2868R;

    public /* synthetic */ C1865g(AdsMediaSource adsMediaSource, ExoPlayer exoPlayer, AdsMediaSource.ComponentListener componentListener) {
        this.f2866P = adsMediaSource;
        this.f2867Q = exoPlayer;
        this.f2868R = componentListener;
    }

    public final void run() {
        this.f2866P.mo15081a(this.f2867Q, this.f2868R);
    }
}
