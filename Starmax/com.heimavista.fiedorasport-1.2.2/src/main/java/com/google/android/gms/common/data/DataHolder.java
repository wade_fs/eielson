package com.google.android.gms.common.data;

import android.database.CursorWindow;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.google.android.gms.common.annotation.KeepName;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.io.Closeable;
import java.util.ArrayList;
import java.util.HashMap;

@KeepName
public final class DataHolder extends AbstractSafeParcelable implements Closeable {
    public static final Parcelable.Creator<DataHolder> CREATOR = new C2174c();

    /* renamed from: P */
    private final int f3556P;

    /* renamed from: Q */
    private final String[] f3557Q;

    /* renamed from: R */
    private Bundle f3558R;

    /* renamed from: S */
    private final CursorWindow[] f3559S;

    /* renamed from: T */
    private final int f3560T;

    /* renamed from: U */
    private final Bundle f3561U;

    /* renamed from: V */
    private int[] f3562V;

    /* renamed from: W */
    private boolean f3563W = false;

    /* renamed from: X */
    private boolean f3564X = true;

    static {
        new C2173b(new String[0], null);
    }

    DataHolder(int i, String[] strArr, CursorWindow[] cursorWindowArr, int i2, Bundle bundle) {
        this.f3556P = i;
        this.f3557Q = strArr;
        this.f3559S = cursorWindowArr;
        this.f3560T = i2;
        this.f3561U = bundle;
    }

    /* renamed from: c */
    public final Bundle mo16843c() {
        return this.f3561U;
    }

    public final void close() {
        synchronized (this) {
            if (!this.f3563W) {
                this.f3563W = true;
                for (int i = 0; i < this.f3559S.length; i++) {
                    this.f3559S[i].close();
                }
            }
        }
    }

    /* renamed from: d */
    public final int mo16845d() {
        return this.f3560T;
    }

    /* access modifiers changed from: protected */
    public final void finalize() {
        try {
            if (this.f3564X && this.f3559S.length > 0 && !mo16847u()) {
                close();
                String obj = toString();
                StringBuilder sb = new StringBuilder(String.valueOf(obj).length() + 178);
                sb.append("Internal data leak within a DataBuffer object detected!  Be sure to explicitly call release() on all DataBuffer extending objects when you are done with them. (internal object: ");
                sb.append(obj);
                sb.append(")");
                Log.e("DataBuffer", sb.toString());
            }
        } finally {
            super.finalize();
        }
    }

    /* renamed from: u */
    public final boolean mo16847u() {
        boolean z;
        synchronized (this) {
            z = this.f3563W;
        }
        return z;
    }

    /* renamed from: v */
    public final void mo16848v() {
        this.f3558R = new Bundle();
        int i = 0;
        int i2 = 0;
        while (true) {
            String[] strArr = this.f3557Q;
            if (i2 >= strArr.length) {
                break;
            }
            this.f3558R.putInt(strArr[i2], i2);
            i2++;
        }
        this.f3562V = new int[this.f3559S.length];
        int i3 = 0;
        while (true) {
            CursorWindow[] cursorWindowArr = this.f3559S;
            if (i < cursorWindowArr.length) {
                this.f3562V[i] = i3;
                i3 += this.f3559S[i].getNumRows() - (i3 - cursorWindowArr[i].getStartPosition());
                i++;
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
     arg types: [android.os.Parcel, int, java.lang.String[], int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
     arg types: [android.os.Parcel, int, android.database.CursorWindow[], int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
     arg types: [android.os.Parcel, int, android.os.Bundle, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5608a(parcel, 1, this.f3557Q, false);
        C2250b.m5607a(parcel, 2, (Parcelable[]) this.f3559S, i, false);
        C2250b.m5591a(parcel, 3, mo16845d());
        C2250b.m5593a(parcel, 4, mo16843c(), false);
        C2250b.m5591a(parcel, 1000, this.f3556P);
        C2250b.m5587a(parcel, a);
        if ((i & 1) != 0) {
            close();
        }
    }

    /* renamed from: com.google.android.gms.common.data.DataHolder$a */
    public static class C2171a {
        private C2171a(String[] strArr, String str) {
            C2258v.m5629a(strArr);
            new ArrayList();
            new HashMap();
        }

        /* synthetic */ C2171a(String[] strArr, String str, C2173b bVar) {
            this(strArr, null);
        }
    }
}
