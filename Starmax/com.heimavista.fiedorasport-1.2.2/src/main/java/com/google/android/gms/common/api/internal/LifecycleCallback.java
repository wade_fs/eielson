package com.google.android.gms.common.api.internal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Keep;
import androidx.annotation.MainThread;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public class LifecycleCallback {

    /* renamed from: P */
    protected final C2080h f3239P;

    protected LifecycleCallback(C2080h hVar) {
        this.f3239P = hVar;
    }

    /* renamed from: a */
    protected static C2080h m4743a(C2076g gVar) {
        if (gVar.mo16697c()) {
            return zzc.m5237a(gVar.mo16696b());
        }
        if (gVar.mo16698d()) {
            return zza.m5231a(gVar.mo16695a());
        }
        throw new IllegalArgumentException("Can't get fragment for unexpected activity.");
    }

    @Keep
    private static C2080h getChimeraLifecycleFragmentImpl(C2076g gVar) {
        throw new IllegalStateException("Method not available in SDK.");
    }

    @MainThread
    /* renamed from: a */
    public void mo16604a(int i, int i2, Intent intent) {
    }

    @MainThread
    /* renamed from: a */
    public void mo16605a(Bundle bundle) {
    }

    @MainThread
    /* renamed from: a */
    public void mo16606a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr) {
    }

    @MainThread
    /* renamed from: b */
    public void mo16607b() {
    }

    @MainThread
    /* renamed from: b */
    public void mo16608b(Bundle bundle) {
    }

    @MainThread
    /* renamed from: c */
    public void mo16609c() {
    }

    @MainThread
    /* renamed from: d */
    public void mo16610d() {
    }

    @MainThread
    /* renamed from: e */
    public void mo16611e() {
    }

    /* renamed from: a */
    public static C2080h m4742a(Activity activity) {
        return m4743a(new C2076g(activity));
    }

    /* renamed from: a */
    public Activity mo16603a() {
        return this.f3239P.mo16704a();
    }
}
