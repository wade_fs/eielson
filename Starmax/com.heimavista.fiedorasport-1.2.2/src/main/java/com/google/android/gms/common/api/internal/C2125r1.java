package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.common.api.C2030b;
import com.google.android.gms.common.api.Status;
import p119e.p144d.p145a.p157c.p167e.C4053a;
import p119e.p144d.p145a.p157c.p167e.C4065h;

/* renamed from: com.google.android.gms.common.api.internal.r1 */
final class C2125r1 implements C4053a<Boolean, Void> {
    C2125r1() {
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo16773a(@NonNull C4065h hVar) {
        if (((Boolean) hVar.mo23709b()).booleanValue()) {
            return null;
        }
        throw new C2030b(new Status(13, "listener already unregistered"));
    }
}
