package com.google.android.gms.common.api.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public final class zabq extends BroadcastReceiver {

    /* renamed from: a */
    private Context f3532a;

    /* renamed from: b */
    private final C2078g1 f3533b;

    public zabq(C2078g1 g1Var) {
        this.f3533b = g1Var;
    }

    /* renamed from: a */
    public final void mo16800a(Context context) {
        this.f3532a = context;
    }

    public final void onReceive(Context context, Intent intent) {
        Uri data = intent.getData();
        if ("com.google.android.gms".equals(data != null ? data.getSchemeSpecificPart() : null)) {
            this.f3533b.mo16700a();
            mo16799a();
        }
    }

    /* renamed from: a */
    public final synchronized void mo16799a() {
        if (this.f3532a != null) {
            this.f3532a.unregisterReceiver(super);
        }
        this.f3532a = null;
    }
}
