package com.google.android.gms.maps;

import com.google.android.gms.common.internal.C2258v;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.maps.a */
public final class C2878a {

    /* renamed from: a */
    private final C3988b f4786a;

    C2878a(C3988b bVar) {
        C2258v.m5629a(bVar);
        this.f4786a = bVar;
    }

    /* renamed from: a */
    public final C3988b mo18398a() {
        return this.f4786a;
    }
}
