package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2488e9;

/* renamed from: com.google.android.gms.measurement.internal.o3 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3139o3 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5489a = new C3139o3();

    private C3139o3() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Boolean.valueOf(C2488e9.m6260b());
    }
}
