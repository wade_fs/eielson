package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.kb */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2588kb implements C2593l2<C2638nb> {

    /* renamed from: Q */
    private static C2588kb f4282Q = new C2588kb();

    /* renamed from: P */
    private final C2593l2<C2638nb> f4283P;

    private C2588kb(C2593l2<C2638nb> l2Var) {
        this.f4283P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6632b() {
        return ((C2638nb) f4282Q.mo17285a()).mo17759a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4283P.mo17285a();
    }

    public C2588kb() {
        this(C2579k2.m6601a(new C2623mb()));
    }
}
