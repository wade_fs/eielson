package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import java.util.List;

/* renamed from: com.google.android.datatransport.cct.b.e */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
final class C1668e extends C1682k {

    /* renamed from: a */
    private final List<C1692r> f2590a;

    C1668e(List<C1692r> list) {
        if (list != null) {
            this.f2590a = list;
            return;
        }
        throw new NullPointerException("Null logRequests");
    }

    @NonNull
    /* renamed from: a */
    public List<C1692r> mo13465a() {
        return this.f2590a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj instanceof C1682k) {
            return this.f2590a.equals(((C1682k) obj).mo13465a());
        }
        return false;
    }

    public int hashCode() {
        return this.f2590a.hashCode() ^ 1000003;
    }

    public String toString() {
        return "BatchedLogRequest{logRequests=" + this.f2590a + "}";
    }
}
