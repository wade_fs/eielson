package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2510g0;

/* renamed from: com.google.android.gms.internal.measurement.j0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C2559j0 implements C2661p4 {

    /* renamed from: a */
    static final C2661p4 f4251a = new C2559j0();

    private C2559j0() {
    }

    /* renamed from: a */
    public final boolean mo17363a(int i) {
        return C2510g0.C2511a.m6366a(i) != null;
    }
}
