package com.google.android.gms.common.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Process;
import android.os.WorkSource;
import android.util.Log;
import androidx.annotation.Nullable;
import com.google.android.gms.common.p090j.C2283c;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.google.android.gms.common.util.r */
public class C2327r {

    /* renamed from: a */
    private static final Method f3855a = m5819e();

    /* renamed from: b */
    private static final Method f3856b = m5820f();

    /* renamed from: c */
    private static final Method f3857c = m5821g();

    /* renamed from: d */
    private static final Method f3858d = m5816b();

    static {
        Process.myUid();
        m5811a();
        m5817c();
        m5818d();
    }

    /* renamed from: a */
    private static WorkSource m5808a(int i, String str) {
        WorkSource workSource = new WorkSource();
        m5813a(workSource, i, str);
        return workSource;
    }

    /* renamed from: b */
    private static int m5815b(WorkSource workSource) {
        Method method = f3857c;
        if (method != null) {
            try {
                return ((Integer) method.invoke(workSource, new Object[0])).intValue();
            } catch (Exception e) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
            }
        }
        return 0;
    }

    /* renamed from: c */
    private static final Method m5817c() {
        if (C2323n.m5801j()) {
            try {
                return WorkSource.class.getMethod("createWorkChain", new Class[0]);
            } catch (Exception e) {
                Log.w("WorkSourceUtil", "Missing WorkChain API createWorkChain", e);
            }
        }
        return null;
    }

    @SuppressLint({"PrivateApi"})
    /* renamed from: d */
    private static final Method m5818d() {
        if (C2323n.m5801j()) {
            try {
                return Class.forName("android.os.WorkSource$WorkChain").getMethod("addNode", Integer.TYPE, String.class);
            } catch (Exception e) {
                Log.w("WorkSourceUtil", "Missing WorkChain class", e);
            }
        }
        return null;
    }

    /* renamed from: e */
    private static Method m5819e() {
        try {
            return WorkSource.class.getMethod("add", Integer.TYPE);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: f */
    private static Method m5820f() {
        if (C2323n.m5795d()) {
            try {
                return WorkSource.class.getMethod("add", Integer.TYPE, String.class);
            } catch (Exception unused) {
            }
        }
        return null;
    }

    /* renamed from: g */
    private static Method m5821g() {
        try {
            return WorkSource.class.getMethod("size", new Class[0]);
        } catch (Exception unused) {
            return null;
        }
    }

    @Nullable
    /* renamed from: a */
    public static WorkSource m5809a(Context context, @Nullable String str) {
        if (!(context == null || context.getPackageManager() == null || str == null)) {
            try {
                ApplicationInfo a = C2283c.m5685a(context).mo17055a(str, 0);
                if (a != null) {
                    return m5808a(a.uid, str);
                }
                String valueOf = String.valueOf(str);
                Log.e("WorkSourceUtil", valueOf.length() != 0 ? "Could not get applicationInfo from package: ".concat(valueOf) : new String("Could not get applicationInfo from package: "));
                return null;
            } catch (PackageManager.NameNotFoundException unused) {
                String valueOf2 = String.valueOf(str);
                Log.e("WorkSourceUtil", valueOf2.length() != 0 ? "Could not find package: ".concat(valueOf2) : new String("Could not find package: "));
            }
        }
        return null;
    }

    /* renamed from: b */
    private static Method m5816b() {
        if (C2323n.m5795d()) {
            try {
                return WorkSource.class.getMethod("getName", Integer.TYPE);
            } catch (Exception unused) {
            }
        }
        return null;
    }

    /* renamed from: a */
    private static void m5813a(WorkSource workSource, int i, @Nullable String str) {
        if (f3856b != null) {
            if (str == null) {
                str = "";
            }
            try {
                f3856b.invoke(workSource, Integer.valueOf(i), str);
            } catch (Exception e) {
                Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
            }
        } else {
            Method method = f3855a;
            if (method != null) {
                try {
                    method.invoke(workSource, Integer.valueOf(i));
                } catch (Exception e2) {
                    Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e2);
                }
            }
        }
    }

    @Nullable
    /* renamed from: a */
    private static String m5810a(WorkSource workSource, int i) {
        Method method = f3858d;
        if (method == null) {
            return null;
        }
        try {
            return (String) method.invoke(workSource, Integer.valueOf(i));
        } catch (Exception e) {
            Log.wtf("WorkSourceUtil", "Unable to assign blame through WorkSource", e);
            return null;
        }
    }

    /* renamed from: a */
    public static List<String> m5812a(@Nullable WorkSource workSource) {
        int b = workSource == null ? 0 : m5815b(workSource);
        if (b == 0) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < b; i++) {
            String a = m5810a(workSource, i);
            if (!C2325p.m5805a(a)) {
                arrayList.add(a);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    public static boolean m5814a(Context context) {
        if (context == null || context.getPackageManager() == null || C2283c.m5685a(context).mo17054a("android.permission.UPDATE_DEVICE_STATS", context.getPackageName()) != 0) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    private static Method m5811a() {
        try {
            return WorkSource.class.getMethod("get", Integer.TYPE);
        } catch (Exception unused) {
            return null;
        }
    }
}
