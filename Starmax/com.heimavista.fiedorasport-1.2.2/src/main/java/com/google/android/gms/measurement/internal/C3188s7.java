package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.s7 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C3188s7 {

    /* renamed from: a */
    public final String f5638a;

    /* renamed from: b */
    public final String f5639b;

    /* renamed from: c */
    public final long f5640c;

    /* renamed from: d */
    boolean f5641d = false;

    public C3188s7(String str, String str2, long j) {
        this.f5638a = str;
        this.f5639b = str2;
        this.f5640c = j;
    }
}
