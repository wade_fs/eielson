package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.v4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public enum C2753v4 {
    VOID(Void.class, Void.class, null),
    INT(Integer.TYPE, Integer.class, 0),
    LONG(Long.TYPE, Long.class, 0L),
    FLOAT(Float.TYPE, Float.class, Float.valueOf(0.0f)),
    DOUBLE(Double.TYPE, Double.class, Double.valueOf(0.0d)),
    BOOLEAN(Boolean.TYPE, Boolean.class, false),
    STRING(String.class, String.class, ""),
    BYTE_STRING(C2498f3.class, C2498f3.class, C2498f3.f4094Q),
    ENUM(Integer.TYPE, Integer.class, null),
    MESSAGE(Object.class, Object.class, null);
    

    /* renamed from: P */
    private final Class<?> f4535P;

    private C2753v4(Class<?> cls, Class<?> cls2, Object obj) {
        this.f4535P = cls2;
    }

    /* renamed from: a */
    public final Class<?> mo17994a() {
        return this.f4535P;
    }
}
