package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.cc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2458cc implements C2593l2<C2507fc> {

    /* renamed from: Q */
    private static C2458cc f4028Q = new C2458cc();

    /* renamed from: P */
    private final C2593l2<C2507fc> f4029P;

    private C2458cc(C2593l2<C2507fc> l2Var) {
        this.f4029P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6163b() {
        return ((C2507fc) f4028Q.mo17285a()).mo17449a();
    }

    /* renamed from: c */
    public static boolean m6164c() {
        return ((C2507fc) f4028Q.mo17285a()).mo17450e();
    }

    /* renamed from: d */
    public static boolean m6165d() {
        return ((C2507fc) f4028Q.mo17285a()).mo17451f();
    }

    /* renamed from: e */
    public static boolean m6166e() {
        return ((C2507fc) f4028Q.mo17285a()).mo17452g();
    }

    /* renamed from: f */
    public static boolean m6167f() {
        return ((C2507fc) f4028Q.mo17285a()).mo17457t();
    }

    /* renamed from: g */
    public static boolean m6168g() {
        return ((C2507fc) f4028Q.mo17285a()).mo17453h();
    }

    /* renamed from: h */
    public static boolean m6169h() {
        return ((C2507fc) f4028Q.mo17285a()).mo17456q();
    }

    /* renamed from: i */
    public static boolean m6170i() {
        return ((C2507fc) f4028Q.mo17285a()).mo17455p();
    }

    /* renamed from: j */
    public static boolean m6171j() {
        return ((C2507fc) f4028Q.mo17285a()).mo17454i();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4029P.mo17285a();
    }

    public C2458cc() {
        this(C2579k2.m6601a(new C2491ec()));
    }
}
