package com.google.android.gms.common.api.internal;

import android.os.Looper;
import androidx.annotation.NonNull;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2258v;
import java.lang.ref.WeakReference;

/* renamed from: com.google.android.gms.common.api.internal.b0 */
final class C2053b0 implements C2197d.C2200c {

    /* renamed from: a */
    private final WeakReference<C2153z> f3248a;

    /* renamed from: b */
    private final C2016a<?> f3249b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final boolean f3250c;

    public C2053b0(C2153z zVar, C2016a<?> aVar, boolean z) {
        this.f3248a = new WeakReference<>(zVar);
        this.f3249b = aVar;
        this.f3250c = z;
    }

    /* renamed from: a */
    public final void mo16632a(@NonNull ConnectionResult connectionResult) {
        C2153z zVar = this.f3248a.get();
        if (zVar != null) {
            C2258v.m5641b(Looper.myLooper() == zVar.f3509a.f3482n.mo16574f(), "onReportServiceBinding must be called on the GoogleApiClient handler thread");
            zVar.f3510b.lock();
            try {
                if (zVar.m5193a(0)) {
                    if (!connectionResult.mo16482w()) {
                        zVar.m5199b(connectionResult, this.f3249b, this.f3250c);
                    }
                    if (zVar.m5203d()) {
                        zVar.m5204e();
                    }
                    zVar.f3510b.unlock();
                }
            } finally {
                zVar.f3510b.unlock();
            }
        }
    }
}
