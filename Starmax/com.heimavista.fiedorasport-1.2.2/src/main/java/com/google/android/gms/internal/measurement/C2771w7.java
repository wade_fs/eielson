package com.google.android.gms.internal.measurement;

import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.w7 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
interface C2771w7 {
    /* renamed from: a */
    int mo17954a();

    @Deprecated
    /* renamed from: a */
    void mo17955a(int i);

    /* renamed from: a */
    void mo17956a(int i, double d);

    /* renamed from: a */
    void mo17957a(int i, float f);

    /* renamed from: a */
    void mo17958a(int i, int i2);

    /* renamed from: a */
    void mo17959a(int i, long j);

    /* renamed from: a */
    void mo17960a(int i, C2498f3 f3Var);

    /* renamed from: a */
    void mo17961a(int i, Object obj);

    /* renamed from: a */
    void mo17962a(int i, Object obj, C2603l6 l6Var);

    /* renamed from: a */
    void mo17963a(int i, String str);

    /* renamed from: a */
    void mo17964a(int i, List<C2498f3> list);

    @Deprecated
    /* renamed from: a */
    void mo17965a(int i, List<?> list, C2603l6 l6Var);

    /* renamed from: a */
    void mo17966a(int i, List<Boolean> list, boolean z);

    /* renamed from: a */
    void mo17967a(int i, boolean z);

    @Deprecated
    /* renamed from: b */
    void mo17968b(int i);

    /* renamed from: b */
    void mo17969b(int i, int i2);

    /* renamed from: b */
    void mo17970b(int i, long j);

    @Deprecated
    /* renamed from: b */
    void mo17971b(int i, Object obj, C2603l6 l6Var);

    /* renamed from: b */
    void mo17972b(int i, List<String> list);

    /* renamed from: b */
    void mo17973b(int i, List<?> list, C2603l6 l6Var);

    /* renamed from: b */
    void mo17974b(int i, List<Integer> list, boolean z);

    /* renamed from: c */
    void mo17975c(int i, int i2);

    /* renamed from: c */
    void mo17976c(int i, long j);

    /* renamed from: c */
    void mo17977c(int i, List<Integer> list, boolean z);

    /* renamed from: d */
    void mo17978d(int i, int i2);

    /* renamed from: d */
    void mo17979d(int i, long j);

    /* renamed from: d */
    void mo17980d(int i, List<Long> list, boolean z);

    /* renamed from: e */
    void mo17981e(int i, int i2);

    /* renamed from: e */
    void mo17982e(int i, long j);

    /* renamed from: e */
    void mo17983e(int i, List<Long> list, boolean z);

    /* renamed from: f */
    void mo17984f(int i, int i2);

    /* renamed from: f */
    void mo17985f(int i, List<Integer> list, boolean z);

    /* renamed from: g */
    void mo17986g(int i, List<Integer> list, boolean z);

    /* renamed from: h */
    void mo17987h(int i, List<Integer> list, boolean z);

    /* renamed from: i */
    void mo17988i(int i, List<Long> list, boolean z);

    /* renamed from: j */
    void mo17989j(int i, List<Long> list, boolean z);

    /* renamed from: k */
    void mo17990k(int i, List<Long> list, boolean z);

    /* renamed from: l */
    void mo17991l(int i, List<Integer> list, boolean z);

    /* renamed from: m */
    void mo17992m(int i, List<Double> list, boolean z);

    /* renamed from: n */
    void mo17993n(int i, List<Float> list, boolean z);
}
