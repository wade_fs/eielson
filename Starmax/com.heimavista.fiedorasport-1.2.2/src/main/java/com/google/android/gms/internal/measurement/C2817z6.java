package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.z6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2817z6 {
    /* renamed from: a */
    static String m7928a(C2498f3 f3Var) {
        C2802y6 y6Var = new C2802y6(f3Var);
        StringBuilder sb = new StringBuilder(y6Var.mo17335a());
        for (int i = 0; i < y6Var.mo17335a(); i++) {
            byte a = y6Var.mo17334a(i);
            if (a == 34) {
                sb.append("\\\"");
            } else if (a == 39) {
                sb.append("\\'");
            } else if (a != 92) {
                switch (a) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (a < 32 || a > 126) {
                            sb.append('\\');
                            sb.append((char) (((a >>> 6) & 3) + 48));
                            sb.append((char) (((a >>> 3) & 7) + 48));
                            sb.append((char) ((a & 7) + 48));
                            break;
                        } else {
                            sb.append((char) a);
                            continue;
                        }
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }
}
