package com.google.android.exoplayer2.upstream;

import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.util.EventDispatcher;

/* renamed from: com.google.android.exoplayer2.upstream.a */
/* compiled from: lambda */
public final /* synthetic */ class C1925a implements EventDispatcher.Event {

    /* renamed from: a */
    private final /* synthetic */ int f2934a;

    /* renamed from: b */
    private final /* synthetic */ long f2935b;

    /* renamed from: c */
    private final /* synthetic */ long f2936c;

    public /* synthetic */ C1925a(int i, long j, long j2) {
        this.f2934a = i;
        this.f2935b = j;
        this.f2936c = j2;
    }

    public final void sendTo(Object obj) {
        ((BandwidthMeter.EventListener) obj).onBandwidthSample(this.f2934a, this.f2935b, this.f2936c);
    }
}
