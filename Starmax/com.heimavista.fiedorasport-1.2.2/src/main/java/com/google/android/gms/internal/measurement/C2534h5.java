package com.google.android.gms.internal.measurement;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: com.google.android.gms.internal.measurement.h5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public abstract class C2534h5 extends C2545i1 implements C2561j2 {
    /* renamed from: a */
    public static C2561j2 m6446a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
        if (queryLocalInterface instanceof C2561j2) {
            return (C2561j2) queryLocalInterface;
        }
        return new C2516g4(iBinder);
    }
}
