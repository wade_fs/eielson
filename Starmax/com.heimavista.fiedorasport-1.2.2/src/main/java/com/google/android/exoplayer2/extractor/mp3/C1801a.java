package com.google.android.exoplayer2.extractor.mp3;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.mp3.a */
/* compiled from: lambda */
public final /* synthetic */ class C1801a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1801a f2814a = new C1801a();

    private /* synthetic */ C1801a() {
    }

    public final Extractor[] createExtractors() {
        return Mp3Extractor.m4384a();
    }
}
