package com.google.android.gms.maps;

public final class R$attr {
    public static final int ambientEnabled = 2130968622;
    public static final int cameraBearing = 2130968709;
    public static final int cameraMaxZoomPreference = 2130968710;
    public static final int cameraMinZoomPreference = 2130968711;
    public static final int cameraTargetLat = 2130968712;
    public static final int cameraTargetLng = 2130968713;
    public static final int cameraTilt = 2130968714;
    public static final int cameraZoom = 2130968715;
    public static final int latLngBoundsNorthEastLatitude = 2130969067;
    public static final int latLngBoundsNorthEastLongitude = 2130969068;
    public static final int latLngBoundsSouthWestLatitude = 2130969069;
    public static final int latLngBoundsSouthWestLongitude = 2130969070;
    public static final int liteMode = 2130969168;
    public static final int mapType = 2130969172;
    public static final int uiCompass = 2130969643;
    public static final int uiMapToolbar = 2130969644;
    public static final int uiRotateGestures = 2130969645;
    public static final int uiScrollGestures = 2130969646;
    public static final int uiScrollGesturesDuringRotateOrZoom = 2130969647;
    public static final int uiTiltGestures = 2130969648;
    public static final int uiZoomControls = 2130969649;
    public static final int uiZoomGestures = 2130969650;
    public static final int useViewLifecycle = 2130969654;
    public static final int zOrderOnTop = 2130969680;

    private R$attr() {
    }
}
