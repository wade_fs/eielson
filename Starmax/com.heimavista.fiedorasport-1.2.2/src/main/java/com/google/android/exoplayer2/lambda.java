package com.google.android.exoplayer2;

/* renamed from: com.google.android.exoplayer2.a */
public final /* synthetic */ class lambda implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ ExoPlayerImplInternal f2782P;

    /* renamed from: Q */
    private final /* synthetic */ PlayerMessage f2783Q;

    public /* synthetic */ lambda(ExoPlayerImplInternal exoPlayerImplInternal, PlayerMessage playerMessage) {
        this.f2782P = exoPlayerImplInternal;
        this.f2783Q = playerMessage;
    }

    public final void run() {
        this.f2782P.mo13732a(this.f2783Q);
    }
}
