package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2525gd;

/* renamed from: com.google.android.gms.internal.measurement.c */
/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.2.2 */
final class C2444c extends C2525gd.C2526a {

    /* renamed from: T */
    private final /* synthetic */ String f4005T;

    /* renamed from: U */
    private final /* synthetic */ String f4006U;

    /* renamed from: V */
    private final /* synthetic */ C2538h9 f4007V;

    /* renamed from: W */
    private final /* synthetic */ C2525gd f4008W;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C2444c(C2525gd gdVar, String str, String str2, C2538h9 h9Var) {
        super(gdVar);
        this.f4008W = gdVar;
        this.f4005T = str;
        this.f4006U = str2;
        this.f4007V = h9Var;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17293a() {
        this.f4008W.f4192g.getConditionalUserProperties(this.f4005T, this.f4006U, this.f4007V);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo17345b() {
        this.f4007V.mo17292d(null);
    }
}
