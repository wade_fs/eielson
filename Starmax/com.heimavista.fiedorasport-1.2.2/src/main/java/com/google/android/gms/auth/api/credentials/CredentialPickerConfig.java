package com.google.android.gms.auth.api.credentials;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.ReflectedParcelable;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;

public final class CredentialPickerConfig extends AbstractSafeParcelable implements ReflectedParcelable {
    public static final Parcelable.Creator<CredentialPickerConfig> CREATOR = new C1965d();

    /* renamed from: P */
    private final int f3037P;

    /* renamed from: Q */
    private final boolean f3038Q;

    /* renamed from: R */
    private final boolean f3039R;

    /* renamed from: S */
    private final int f3040S;

    /* renamed from: com.google.android.gms.auth.api.credentials.CredentialPickerConfig$a */
    public static class C1961a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f3041a = false;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public boolean f3042b = true;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public int f3043c = 1;

        /* renamed from: a */
        public CredentialPickerConfig mo16341a() {
            return new CredentialPickerConfig(this);
        }
    }

    CredentialPickerConfig(int i, boolean z, boolean z2, boolean z3, int i2) {
        this.f3037P = i;
        this.f3038Q = z;
        this.f3039R = z2;
        int i3 = 3;
        if (i < 2) {
            this.f3040S = !z3 ? 1 : i3;
        } else {
            this.f3040S = i2;
        }
    }

    @Deprecated
    /* renamed from: c */
    public final boolean mo16337c() {
        return this.f3040S == 3;
    }

    /* renamed from: d */
    public final boolean mo16338d() {
        return this.f3038Q;
    }

    /* renamed from: u */
    public final boolean mo16339u() {
        return this.f3039R;
    }

    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5605a(parcel, 1, mo16338d());
        C2250b.m5605a(parcel, 2, mo16339u());
        C2250b.m5605a(parcel, 3, mo16337c());
        C2250b.m5591a(parcel, 4, this.f3040S);
        C2250b.m5591a(parcel, 1000, this.f3037P);
        C2250b.m5587a(parcel, a);
    }

    private CredentialPickerConfig(C1961a aVar) {
        this(2, aVar.f3041a, aVar.f3042b, false, aVar.f3043c);
    }
}
