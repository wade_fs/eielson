package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.l9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2606l9 implements C2593l2<C2586k9> {

    /* renamed from: Q */
    private static C2606l9 f4305Q = new C2606l9();

    /* renamed from: P */
    private final C2593l2<C2586k9> f4306P;

    private C2606l9(C2593l2<C2586k9> l2Var) {
        this.f4306P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6736b() {
        return ((C2586k9) f4305Q.mo17285a()).mo17653a();
    }

    /* renamed from: c */
    public static boolean m6737c() {
        return ((C2586k9) f4305Q.mo17285a()).mo17654e();
    }

    /* renamed from: d */
    public static boolean m6738d() {
        return ((C2586k9) f4305Q.mo17285a()).mo17655f();
    }

    /* renamed from: e */
    public static boolean m6739e() {
        return ((C2586k9) f4305Q.mo17285a()).mo17656g();
    }

    /* renamed from: f */
    public static boolean m6740f() {
        return ((C2586k9) f4305Q.mo17285a()).mo17657t();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4306P.mo17285a();
    }

    public C2606l9() {
        this(C2579k2.m6601a(new C2636n9()));
    }
}
