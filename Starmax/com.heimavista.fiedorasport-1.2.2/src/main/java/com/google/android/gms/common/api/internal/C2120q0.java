package com.google.android.gms.common.api.internal;

import androidx.annotation.NonNull;
import com.google.android.gms.auth.api.signin.internal.C1989b;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.C2157k;
import com.google.android.gms.common.api.C2158l;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.api.internal.q0 */
final class C2120q0 implements C2158l<Status> {

    /* renamed from: a */
    private final /* synthetic */ C2107n f3423a;

    /* renamed from: b */
    private final /* synthetic */ boolean f3424b;

    /* renamed from: c */
    private final /* synthetic */ C2036f f3425c;

    /* renamed from: d */
    private final /* synthetic */ C2100l0 f3426d;

    C2120q0(C2100l0 l0Var, C2107n nVar, boolean z, C2036f fVar) {
        this.f3426d = l0Var;
        this.f3423a = nVar;
        this.f3424b = z;
        this.f3425c = fVar;
    }

    /* renamed from: a */
    public final /* synthetic */ void mo16767a(@NonNull C2157k kVar) {
        Status status = (Status) kVar;
        C1989b.m4567a(this.f3426d.f3364g).mo16441e();
        if (status.mo16518v() && this.f3426d.mo16748j()) {
            this.f3426d.mo16749k();
        }
        this.f3423a.mo16593a((C2157k) status);
        if (this.f3424b) {
            this.f3425c.mo16572d();
        }
    }
}
