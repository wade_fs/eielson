package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.v3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2752v3 implements C2771w7 {

    /* renamed from: a */
    private final C2720t3 f4523a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.t3, java.lang.String]
     candidates:
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.Object):java.lang.Object
      com.google.android.gms.internal.measurement.o4.a(java.lang.Object, java.lang.String):T */
    private C2752v3(C2720t3 t3Var) {
        C2647o4.m6963a((Object) t3Var, "output");
        this.f4523a = t3Var;
        this.f4523a.f4498a = this;
    }

    /* renamed from: a */
    public static C2752v3 m7403a(C2720t3 t3Var) {
        C2752v3 v3Var = t3Var.f4498a;
        if (v3Var != null) {
            return v3Var;
        }
        return new C2752v3(t3Var);
    }

    /* renamed from: b */
    public final void mo17970b(int i, long j) {
        this.f4523a.mo17891a(i, j);
    }

    /* renamed from: c */
    public final void mo17975c(int i, int i2) {
        this.f4523a.mo17912e(i, i2);
    }

    /* renamed from: d */
    public final void mo17979d(int i, long j) {
        this.f4523a.mo17908c(i, j);
    }

    /* renamed from: e */
    public final void mo17981e(int i, int i2) {
        this.f4523a.mo17912e(i, i2);
    }

    /* renamed from: f */
    public final void mo17984f(int i, int i2) {
        this.f4523a.mo17907c(i, i2);
    }

    /* renamed from: g */
    public final void mo17986g(int i, List<Integer> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7241g(list.get(i4).intValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17901b(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17907c(i, list.get(i2).intValue());
            i2++;
        }
    }

    /* renamed from: h */
    public final void mo17987h(int i, List<Integer> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7252j(list.get(i4).intValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17910d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17912e(i, list.get(i2).intValue());
            i2++;
        }
    }

    /* renamed from: i */
    public final void mo17988i(int i, List<Long> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7244g(list.get(i4).longValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17909c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17908c(i, list.get(i2).longValue());
            i2++;
        }
    }

    /* renamed from: j */
    public final void mo17989j(int i, List<Long> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7240f(list.get(i4).longValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17905b(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17903b(i, list.get(i2).longValue());
            i2++;
        }
    }

    /* renamed from: k */
    public final void mo17990k(int i, List<Long> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7236e(list.get(i4).longValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17897a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17891a(i, list.get(i2).longValue());
            i2++;
        }
    }

    /* renamed from: l */
    public final void mo17991l(int i, List<Integer> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7245h(list.get(i4).intValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17906c(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17911d(i, list.get(i2).intValue());
            i2++;
        }
    }

    /* renamed from: m */
    public final void mo17992m(int i, List<Double> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7220b(list.get(i4).doubleValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17885a(list.get(i2).doubleValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17888a(i, list.get(i2).doubleValue());
            i2++;
        }
    }

    /* renamed from: n */
    public final void mo17993n(int i, List<Float> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7221b(list.get(i4).floatValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17886a(list.get(i2).floatValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17889a(i, list.get(i2).floatValue());
            i2++;
        }
    }

    /* renamed from: b */
    public final void mo17969b(int i, int i2) {
        this.f4523a.mo17902b(i, i2);
    }

    /* renamed from: d */
    public final void mo17978d(int i, int i2) {
        this.f4523a.mo17911d(i, i2);
    }

    /* renamed from: e */
    public final void mo17982e(int i, long j) {
        this.f4523a.mo17903b(i, j);
    }

    /* renamed from: f */
    public final void mo17985f(int i, List<Integer> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7249i(list.get(i4).intValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17910d(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17912e(i, list.get(i2).intValue());
            i2++;
        }
    }

    /* renamed from: a */
    public final int mo17954a() {
        return C2595l4.C2601f.f4300k;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
     arg types: [com.google.android.gms.internal.measurement.u5, com.google.android.gms.internal.measurement.v3]
     candidates:
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
      com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void */
    /* renamed from: b */
    public final void mo17971b(int i, Object obj, C2603l6 l6Var) {
        C2720t3 t3Var = this.f4523a;
        t3Var.mo17890a(i, 3);
        l6Var.mo17278a((Object) ((C2739u5) obj), (C2771w7) t3Var.f4498a);
        t3Var.mo17890a(i, 4);
    }

    /* renamed from: c */
    public final void mo17976c(int i, long j) {
        this.f4523a.mo17891a(i, j);
    }

    /* renamed from: d */
    public final void mo17980d(int i, List<Long> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7233d(list.get(i4).longValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17897a(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17891a(i, list.get(i2).longValue());
            i2++;
        }
    }

    /* renamed from: e */
    public final void mo17983e(int i, List<Long> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7248h(list.get(i4).longValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17909c(list.get(i2).longValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17908c(i, list.get(i2).longValue());
            i2++;
        }
    }

    /* renamed from: a */
    public final void mo17959a(int i, long j) {
        this.f4523a.mo17908c(i, j);
    }

    /* renamed from: c */
    public final void mo17977c(int i, List<Integer> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7254k(list.get(i4).intValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17887a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17902b(i, list.get(i2).intValue());
            i2++;
        }
    }

    /* renamed from: a */
    public final void mo17957a(int i, float f) {
        this.f4523a.mo17889a(i, f);
    }

    /* renamed from: a */
    public final void mo17956a(int i, double d) {
        this.f4523a.mo17888a(i, d);
    }

    /* renamed from: b */
    public final void mo17968b(int i) {
        this.f4523a.mo17890a(i, 4);
    }

    /* renamed from: a */
    public final void mo17958a(int i, int i2) {
        this.f4523a.mo17902b(i, i2);
    }

    /* renamed from: b */
    public final void mo17974b(int i, List<Integer> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7237f(list.get(i4).intValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17887a(list.get(i2).intValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17902b(i, list.get(i2).intValue());
            i2++;
        }
    }

    /* renamed from: a */
    public final void mo17967a(int i, boolean z) {
        this.f4523a.mo17896a(i, z);
    }

    /* renamed from: a */
    public final void mo17963a(int i, String str) {
        this.f4523a.mo17895a(i, str);
    }

    /* renamed from: a */
    public final void mo17960a(int i, C2498f3 f3Var) {
        this.f4523a.mo17892a(i, f3Var);
    }

    /* renamed from: a */
    public final void mo17962a(int i, Object obj, C2603l6 l6Var) {
        this.f4523a.mo17894a(i, (C2739u5) obj, l6Var);
    }

    /* renamed from: a */
    public final void mo17955a(int i) {
        this.f4523a.mo17890a(i, 3);
    }

    /* renamed from: a */
    public final void mo17961a(int i, Object obj) {
        if (obj instanceof C2498f3) {
            this.f4523a.mo17904b(i, (C2498f3) obj);
        } else {
            this.f4523a.mo17893a(i, (C2739u5) obj);
        }
    }

    /* renamed from: b */
    public final void mo17972b(int i, List<String> list) {
        int i2 = 0;
        if (list instanceof C2484e5) {
            C2484e5 e5Var = (C2484e5) list;
            while (i2 < list.size()) {
                Object b = e5Var.mo17325b(i2);
                if (b instanceof String) {
                    this.f4523a.mo17895a(i, (String) b);
                } else {
                    this.f4523a.mo17892a(i, (C2498f3) b);
                }
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17895a(i, list.get(i2));
            i2++;
        }
    }

    /* renamed from: a */
    public final void mo17966a(int i, List<Boolean> list, boolean z) {
        int i2 = 0;
        if (z) {
            this.f4523a.mo17890a(i, 2);
            int i3 = 0;
            for (int i4 = 0; i4 < list.size(); i4++) {
                i3 += C2720t3.m7227b(list.get(i4).booleanValue());
            }
            this.f4523a.mo17901b(i3);
            while (i2 < list.size()) {
                this.f4523a.mo17899a(list.get(i2).booleanValue());
                i2++;
            }
            return;
        }
        while (i2 < list.size()) {
            this.f4523a.mo17896a(i, list.get(i2).booleanValue());
            i2++;
        }
    }

    /* renamed from: b */
    public final void mo17973b(int i, List<?> list, C2603l6 l6Var) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            mo17962a(i, list.get(i2), l6Var);
        }
    }

    /* renamed from: a */
    public final void mo17964a(int i, List<C2498f3> list) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            this.f4523a.mo17892a(i, list.get(i2));
        }
    }

    /* renamed from: a */
    public final void mo17965a(int i, List<?> list, C2603l6 l6Var) {
        for (int i2 = 0; i2 < list.size(); i2++) {
            mo17971b(i, list.get(i2), l6Var);
        }
    }
}
