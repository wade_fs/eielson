package com.google.android.gms.common.internal;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;

/* renamed from: com.google.android.gms.common.internal.j */
public abstract class C2224j {

    /* renamed from: P */
    private static final Object f3727P = new Object();

    /* renamed from: Q */
    private static C2224j f3728Q;

    /* renamed from: com.google.android.gms.common.internal.j$a */
    protected static final class C2225a {

        /* renamed from: a */
        private final String f3729a;

        /* renamed from: b */
        private final String f3730b;

        /* renamed from: c */
        private final ComponentName f3731c = null;

        /* renamed from: d */
        private final int f3732d;

        public C2225a(String str, String str2, int i) {
            C2258v.m5639b(str);
            this.f3729a = str;
            C2258v.m5639b(str2);
            this.f3730b = str2;
            this.f3732d = i;
        }

        /* renamed from: a */
        public final ComponentName mo16991a() {
            return this.f3731c;
        }

        /* renamed from: b */
        public final String mo16993b() {
            return this.f3730b;
        }

        /* renamed from: c */
        public final int mo16994c() {
            return this.f3732d;
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof C2225a)) {
                return false;
            }
            C2225a aVar = (C2225a) obj;
            return C2251t.m5617a(this.f3729a, aVar.f3729a) && C2251t.m5617a(this.f3730b, aVar.f3730b) && C2251t.m5617a(this.f3731c, aVar.f3731c) && this.f3732d == aVar.f3732d;
        }

        public final int hashCode() {
            return C2251t.m5615a(this.f3729a, this.f3730b, this.f3731c, Integer.valueOf(this.f3732d));
        }

        public final String toString() {
            String str = this.f3729a;
            return str == null ? this.f3731c.flattenToString() : str;
        }

        /* renamed from: a */
        public final Intent mo16992a(Context context) {
            String str = this.f3729a;
            if (str != null) {
                return new Intent(str).setPackage(this.f3730b);
            }
            return new Intent().setComponent(this.f3731c);
        }
    }

    /* renamed from: a */
    public static C2224j m5496a(Context context) {
        synchronized (f3727P) {
            if (f3728Q == null) {
                f3728Q = new C2243q0(context.getApplicationContext());
            }
        }
        return f3728Q;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract boolean mo16989a(C2225a aVar, ServiceConnection serviceConnection, String str);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo16990b(C2225a aVar, ServiceConnection serviceConnection, String str);

    /* renamed from: a */
    public final void mo16988a(String str, String str2, int i, ServiceConnection serviceConnection, String str3) {
        mo16990b(new C2225a(str, str2, i), serviceConnection, str3);
    }
}
