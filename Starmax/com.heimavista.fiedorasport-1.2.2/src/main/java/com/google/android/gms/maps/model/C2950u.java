package com.google.android.gms.maps.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2248a;

/* renamed from: com.google.android.gms.maps.model.u */
public final class C2950u implements Parcelable.Creator<StreetViewPanoramaLocation> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        int b = C2248a.m5558b(parcel);
        StreetViewPanoramaLink[] streetViewPanoramaLinkArr = null;
        LatLng latLng = null;
        String str = null;
        while (parcel.dataPosition() < b) {
            int a = C2248a.m5552a(parcel);
            int a2 = C2248a.m5551a(a);
            if (a2 == 2) {
                streetViewPanoramaLinkArr = (StreetViewPanoramaLink[]) C2248a.m5559b(parcel, a, StreetViewPanoramaLink.CREATOR);
            } else if (a2 == 3) {
                latLng = (LatLng) C2248a.m5553a(parcel, a, LatLng.CREATOR);
            } else if (a2 != 4) {
                C2248a.m5550F(parcel, a);
            } else {
                str = C2248a.m5573n(parcel, a);
            }
        }
        C2248a.m5576q(parcel, b);
        return new StreetViewPanoramaLocation(streetViewPanoramaLinkArr, latLng, str);
    }

    public final /* synthetic */ Object[] newArray(int i) {
        return new StreetViewPanoramaLocation[i];
    }
}
