package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.fa */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2505fa implements C2456ca {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4156a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.logging.improved_messaging_q4_2019_client", true);

    /* renamed from: a */
    public final boolean mo17384a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17385e() {
        return f4156a.mo18128b().booleanValue();
    }
}
