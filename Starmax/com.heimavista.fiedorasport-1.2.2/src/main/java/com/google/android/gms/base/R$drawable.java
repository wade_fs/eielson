package com.google.android.gms.base;

public final class R$drawable {
    public static final int common_full_open_on_phone = 2131230869;
    public static final int common_google_signin_btn_icon_dark = 2131230870;
    public static final int common_google_signin_btn_icon_dark_focused = 2131230871;
    public static final int common_google_signin_btn_icon_dark_normal = 2131230872;
    public static final int common_google_signin_btn_icon_dark_normal_background = 2131230873;
    public static final int common_google_signin_btn_icon_disabled = 2131230874;
    public static final int common_google_signin_btn_icon_light = 2131230875;
    public static final int common_google_signin_btn_icon_light_focused = 2131230876;
    public static final int common_google_signin_btn_icon_light_normal = 2131230877;
    public static final int common_google_signin_btn_icon_light_normal_background = 2131230878;
    public static final int common_google_signin_btn_text_dark = 2131230879;
    public static final int common_google_signin_btn_text_dark_focused = 2131230880;
    public static final int common_google_signin_btn_text_dark_normal = 2131230881;
    public static final int common_google_signin_btn_text_dark_normal_background = 2131230882;
    public static final int common_google_signin_btn_text_disabled = 2131230883;
    public static final int common_google_signin_btn_text_light = 2131230884;
    public static final int common_google_signin_btn_text_light_focused = 2131230885;
    public static final int common_google_signin_btn_text_light_normal = 2131230886;
    public static final int common_google_signin_btn_text_light_normal_background = 2131230887;
    public static final int googleg_disabled_color_18 = 2131230927;
    public static final int googleg_standard_color_18 = 2131230928;

    private R$drawable() {
    }
}
