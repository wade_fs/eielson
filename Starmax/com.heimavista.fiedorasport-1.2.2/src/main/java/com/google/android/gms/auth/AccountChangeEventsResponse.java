package com.google.android.gms.auth;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import java.util.List;

public class AccountChangeEventsResponse extends AbstractSafeParcelable {
    public static final Parcelable.Creator<AccountChangeEventsResponse> CREATOR = new C2013c();

    /* renamed from: P */
    private final int f2976P;

    /* renamed from: Q */
    private final List<AccountChangeEvent> f2977Q;

    AccountChangeEventsResponse(int i, List<AccountChangeEvent> list) {
        this.f2976P = i;
        C2258v.m5629a(list);
        this.f2977Q = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f2976P);
        C2250b.m5614c(parcel, 2, this.f2977Q, false);
        C2250b.m5587a(parcel, a);
    }
}
