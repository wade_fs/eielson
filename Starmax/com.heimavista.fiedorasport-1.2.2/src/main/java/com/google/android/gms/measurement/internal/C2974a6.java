package com.google.android.gms.measurement.internal;

import java.util.List;
import java.util.concurrent.Callable;

/* renamed from: com.google.android.gms.measurement.internal.a6 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C2974a6 implements Callable<List<C3234w9>> {

    /* renamed from: a */
    private final /* synthetic */ zzm f4949a;

    /* renamed from: b */
    private final /* synthetic */ C3141o5 f4950b;

    C2974a6(C3141o5 o5Var, zzm zzm) {
        this.f4950b = o5Var;
        this.f4949a = zzm;
    }

    public final /* synthetic */ Object call() {
        this.f4950b.f5496a.mo19237q();
        return this.f4950b.f5496a.mo19229e().mo18845a(this.f4949a.f5814P);
    }
}
