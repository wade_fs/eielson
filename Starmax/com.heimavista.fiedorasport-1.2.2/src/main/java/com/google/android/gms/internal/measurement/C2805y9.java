package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.y9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2805y9 implements C2593l2<C2440ba> {

    /* renamed from: Q */
    private static C2805y9 f4617Q = new C2805y9();

    /* renamed from: P */
    private final C2593l2<C2440ba> f4618P;

    private C2805y9(C2593l2<C2440ba> l2Var) {
        this.f4618P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7884b() {
        return ((C2440ba) f4617Q.mo17285a()).mo17288a();
    }

    /* renamed from: c */
    public static boolean m7885c() {
        return ((C2440ba) f4617Q.mo17285a()).mo17289e();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4618P.mo17285a();
    }

    public C2805y9() {
        this(C2579k2.m6601a(new C2424aa()));
    }
}
