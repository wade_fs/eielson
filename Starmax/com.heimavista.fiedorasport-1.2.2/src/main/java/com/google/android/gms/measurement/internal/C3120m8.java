package com.google.android.gms.measurement.internal;

import android.os.RemoteException;
import android.text.TextUtils;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.google.android.gms.measurement.internal.m8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3120m8 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ AtomicReference f5353P;

    /* renamed from: Q */
    private final /* synthetic */ String f5354Q;

    /* renamed from: R */
    private final /* synthetic */ String f5355R;

    /* renamed from: S */
    private final /* synthetic */ String f5356S;

    /* renamed from: T */
    private final /* synthetic */ boolean f5357T;

    /* renamed from: U */
    private final /* synthetic */ zzm f5358U;

    /* renamed from: V */
    private final /* synthetic */ C3232w7 f5359V;

    C3120m8(C3232w7 w7Var, AtomicReference atomicReference, String str, String str2, String str3, boolean z, zzm zzm) {
        this.f5359V = w7Var;
        this.f5353P = atomicReference;
        this.f5354Q = str;
        this.f5355R = str2;
        this.f5356S = str3;
        this.f5357T = z;
        this.f5358U = zzm;
    }

    public final void run() {
        synchronized (this.f5353P) {
            try {
                C3228w3 d = this.f5359V.f5724d;
                if (d == null) {
                    this.f5359V.mo19015l().mo19001t().mo19045a("(legacy) Failed to get user properties; not connected to service", C3032f4.m8621a(this.f5354Q), this.f5355R, this.f5356S);
                    this.f5353P.set(Collections.emptyList());
                    this.f5353P.notify();
                    return;
                }
                if (TextUtils.isEmpty(this.f5354Q)) {
                    this.f5353P.set(d.mo19187a(this.f5355R, this.f5356S, this.f5357T, this.f5358U));
                } else {
                    this.f5353P.set(d.mo19186a(this.f5354Q, this.f5355R, this.f5356S, this.f5357T));
                }
                this.f5359V.m9314J();
                this.f5353P.notify();
            } catch (RemoteException e) {
                try {
                    this.f5359V.mo19015l().mo19001t().mo19045a("(legacy) Failed to get user properties; remote exception", C3032f4.m8621a(this.f5354Q), this.f5355R, e);
                    this.f5353P.set(Collections.emptyList());
                    this.f5353P.notify();
                } catch (Throwable th) {
                    this.f5353P.notify();
                    throw th;
                }
            }
        }
    }
}
