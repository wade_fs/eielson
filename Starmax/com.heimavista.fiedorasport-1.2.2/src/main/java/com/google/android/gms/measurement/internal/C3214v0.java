package com.google.android.gms.measurement.internal;

import com.google.android.gms.internal.measurement.C2683qb;

/* renamed from: com.google.android.gms.measurement.internal.v0 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3214v0 implements C3217v3 {

    /* renamed from: a */
    static final C3217v3 f5691a = new C3214v0();

    private C3214v0() {
    }

    /* renamed from: a */
    public final Object mo18723a() {
        return Long.valueOf(C2683qb.m7096e());
    }
}
