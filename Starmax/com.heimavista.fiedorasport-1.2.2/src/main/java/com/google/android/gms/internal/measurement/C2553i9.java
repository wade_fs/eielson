package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.i9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2553i9 implements C2572j9 {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4240a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.client.firebase_feature_rollout.v1.enable", true);

    /* renamed from: a */
    public final boolean mo17584a() {
        return true;
    }

    /* renamed from: e */
    public final boolean mo17585e() {
        return f4240a.mo18128b().booleanValue();
    }
}
