package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;

/* renamed from: com.google.android.gms.internal.measurement.t0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2716t0 extends C2595l4<C2716t0, C2717a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2716t0 zzf;
    private static volatile C2501f6<C2716t0> zzg;
    private int zzc;
    private String zzd = "";
    private long zze;

    /* renamed from: com.google.android.gms.internal.measurement.t0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2717a extends C2595l4.C2596a<C2716t0, C2717a> implements C2769w5 {
        private C2717a() {
            super(C2716t0.zzf);
        }

        /* synthetic */ C2717a(C2657p0 p0Var) {
            this();
        }
    }

    static {
        C2716t0 t0Var = new C2716t0();
        zzf = t0Var;
        C2595l4.m6645a(C2716t0.class, super);
    }

    private C2716t0() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2716t0();
            case 2:
                return new C2717a(null);
            case 3:
                return C2595l4.m6643a(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\b\u0000\u0002\u0002\u0001", new Object[]{"zzc", "zzd", "zze"});
            case 4:
                return zzf;
            case 5:
                C2501f6<C2716t0> f6Var = zzg;
                if (f6Var == null) {
                    synchronized (C2716t0.class) {
                        f6Var = zzg;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzf);
                            zzg = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
