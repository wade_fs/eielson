package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.s8 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2710s8 implements C2593l2<C2695r8> {

    /* renamed from: Q */
    private static C2710s8 f4480Q = new C2710s8();

    /* renamed from: P */
    private final C2593l2<C2695r8> f4481P;

    private C2710s8(C2593l2<C2695r8> l2Var) {
        this.f4481P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m7197b() {
        return ((C2695r8) f4480Q.mo17285a()).mo17844a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4481P.mo17285a();
    }

    public C2710s8() {
        this(C2579k2.m6601a(new C2742u8()));
    }
}
