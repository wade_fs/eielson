package com.google.android.gms.maps;

public final class R$styleable {
    public static final int[] MapAttrs = {2130968622, 2130968709, 2130968710, 2130968711, 2130968712, 2130968713, 2130968714, 2130968715, 2130969067, 2130969068, 2130969069, 2130969070, 2130969168, 2130969172, 2130969643, 2130969644, 2130969645, 2130969646, 2130969647, 2130969648, 2130969649, 2130969650, 2130969654, 2130969680};
    public static final int MapAttrs_ambientEnabled = 0;
    public static final int MapAttrs_cameraBearing = 1;
    public static final int MapAttrs_cameraMaxZoomPreference = 2;
    public static final int MapAttrs_cameraMinZoomPreference = 3;
    public static final int MapAttrs_cameraTargetLat = 4;
    public static final int MapAttrs_cameraTargetLng = 5;
    public static final int MapAttrs_cameraTilt = 6;
    public static final int MapAttrs_cameraZoom = 7;
    public static final int MapAttrs_latLngBoundsNorthEastLatitude = 8;
    public static final int MapAttrs_latLngBoundsNorthEastLongitude = 9;
    public static final int MapAttrs_latLngBoundsSouthWestLatitude = 10;
    public static final int MapAttrs_latLngBoundsSouthWestLongitude = 11;
    public static final int MapAttrs_liteMode = 12;
    public static final int MapAttrs_mapType = 13;
    public static final int MapAttrs_uiCompass = 14;
    public static final int MapAttrs_uiMapToolbar = 15;
    public static final int MapAttrs_uiRotateGestures = 16;
    public static final int MapAttrs_uiScrollGestures = 17;
    public static final int MapAttrs_uiScrollGesturesDuringRotateOrZoom = 18;
    public static final int MapAttrs_uiTiltGestures = 19;
    public static final int MapAttrs_uiZoomControls = 20;
    public static final int MapAttrs_uiZoomGestures = 21;
    public static final int MapAttrs_useViewLifecycle = 22;
    public static final int MapAttrs_zOrderOnTop = 23;

    private R$styleable() {
    }
}
