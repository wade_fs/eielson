package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.r4 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public interface C2691r4 extends C2738u4<Long> {
    /* renamed from: b */
    long mo17573b(int i);

    /* renamed from: c */
    C2691r4 mo17574c(int i);

    /* renamed from: g */
    void mo17575g(long j);
}
