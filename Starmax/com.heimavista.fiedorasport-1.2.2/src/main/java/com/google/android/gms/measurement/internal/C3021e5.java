package com.google.android.gms.measurement.internal;

import android.text.TextUtils;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.gms.common.internal.C2258v;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.google.android.gms.measurement.internal.e5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C3021e5 {

    /* renamed from: A */
    private long f5064A;

    /* renamed from: B */
    private long f5065B;

    /* renamed from: C */
    private long f5066C;

    /* renamed from: D */
    private String f5067D;

    /* renamed from: E */
    private boolean f5068E;

    /* renamed from: F */
    private long f5069F;

    /* renamed from: G */
    private long f5070G;

    /* renamed from: a */
    private final C3081j5 f5071a;

    /* renamed from: b */
    private final String f5072b;

    /* renamed from: c */
    private String f5073c;

    /* renamed from: d */
    private String f5074d;

    /* renamed from: e */
    private String f5075e;

    /* renamed from: f */
    private String f5076f;

    /* renamed from: g */
    private long f5077g;

    /* renamed from: h */
    private long f5078h;

    /* renamed from: i */
    private long f5079i;

    /* renamed from: j */
    private String f5080j;

    /* renamed from: k */
    private long f5081k;

    /* renamed from: l */
    private String f5082l;

    /* renamed from: m */
    private long f5083m;

    /* renamed from: n */
    private long f5084n;

    /* renamed from: o */
    private boolean f5085o;

    /* renamed from: p */
    private long f5086p;

    /* renamed from: q */
    private boolean f5087q;

    /* renamed from: r */
    private boolean f5088r;

    /* renamed from: s */
    private String f5089s;

    /* renamed from: t */
    private Boolean f5090t;

    /* renamed from: u */
    private long f5091u;

    /* renamed from: v */
    private List<String> f5092v;

    /* renamed from: w */
    private String f5093w;

    /* renamed from: x */
    private long f5094x;

    /* renamed from: y */
    private long f5095y;

    /* renamed from: z */
    private long f5096z;

    @WorkerThread
    C3021e5(C3081j5 j5Var, String str) {
        C2258v.m5629a(j5Var);
        C2258v.m5639b(str);
        this.f5071a = j5Var;
        this.f5072b = str;
        this.f5071a.mo19014j().mo18881c();
    }

    @WorkerThread
    /* renamed from: A */
    public final boolean mo18922A() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5085o;
    }

    @WorkerThread
    /* renamed from: B */
    public final long mo18923B() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5077g;
    }

    @WorkerThread
    /* renamed from: C */
    public final long mo18924C() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5069F;
    }

    @WorkerThread
    /* renamed from: D */
    public final long mo18925D() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5070G;
    }

    @WorkerThread
    /* renamed from: E */
    public final void mo18926E() {
        this.f5071a.mo19014j().mo18881c();
        long j = this.f5077g + 1;
        if (j > 2147483647L) {
            this.f5071a.mo19015l().mo19004w().mo19043a("Bundle index overflow. appId", C3032f4.m8621a(this.f5072b));
            j = 0;
        }
        this.f5068E = true;
        this.f5077g = j;
    }

    @WorkerThread
    /* renamed from: F */
    public final long mo18927F() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5094x;
    }

    @WorkerThread
    /* renamed from: G */
    public final long mo18928G() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5095y;
    }

    @WorkerThread
    /* renamed from: H */
    public final long mo18929H() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5096z;
    }

    @WorkerThread
    /* renamed from: I */
    public final long mo18930I() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5064A;
    }

    @WorkerThread
    /* renamed from: a */
    public final boolean mo18936a() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5068E;
    }

    @WorkerThread
    /* renamed from: b */
    public final void mo18939b(String str) {
        this.f5071a.mo19014j().mo18881c();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f5068E |= !C3267z9.m9428c(this.f5074d, str);
        this.f5074d = str;
    }

    @WorkerThread
    /* renamed from: c */
    public final void mo18943c(String str) {
        this.f5071a.mo19014j().mo18881c();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f5068E |= !C3267z9.m9428c(this.f5089s, str);
        this.f5089s = str;
    }

    @WorkerThread
    /* renamed from: d */
    public final void mo18947d(String str) {
        this.f5071a.mo19014j().mo18881c();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.f5068E |= !C3267z9.m9428c(this.f5093w, str);
        this.f5093w = str;
    }

    @WorkerThread
    /* renamed from: e */
    public final void mo18950e(String str) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9428c(this.f5075e, str);
        this.f5075e = str;
    }

    @WorkerThread
    /* renamed from: f */
    public final void mo18953f(String str) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9428c(this.f5076f, str);
        this.f5076f = str;
    }

    @WorkerThread
    /* renamed from: g */
    public final void mo18955g(String str) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9428c(this.f5080j, str);
        this.f5080j = str;
    }

    @WorkerThread
    /* renamed from: h */
    public final void mo18958h(String str) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9428c(this.f5082l, str);
        this.f5082l = str;
    }

    @WorkerThread
    /* renamed from: i */
    public final void mo18961i(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5070G != j;
        this.f5070G = j;
    }

    @WorkerThread
    /* renamed from: j */
    public final void mo18964j(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5094x != j;
        this.f5094x = j;
    }

    @WorkerThread
    /* renamed from: k */
    public final void mo18965k() {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E = false;
    }

    @WorkerThread
    /* renamed from: l */
    public final String mo18967l() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5072b;
    }

    @WorkerThread
    /* renamed from: m */
    public final String mo18969m() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5073c;
    }

    @WorkerThread
    /* renamed from: n */
    public final String mo18971n() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5074d;
    }

    @WorkerThread
    /* renamed from: o */
    public final String mo18973o() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5089s;
    }

    @WorkerThread
    /* renamed from: p */
    public final String mo18975p() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5093w;
    }

    @WorkerThread
    /* renamed from: q */
    public final String mo18977q() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5075e;
    }

    @WorkerThread
    /* renamed from: r */
    public final String mo18978r() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5076f;
    }

    @WorkerThread
    /* renamed from: s */
    public final long mo18979s() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5078h;
    }

    @WorkerThread
    /* renamed from: t */
    public final long mo18980t() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5079i;
    }

    @WorkerThread
    /* renamed from: u */
    public final String mo18981u() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5080j;
    }

    @WorkerThread
    /* renamed from: v */
    public final long mo18982v() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5081k;
    }

    @WorkerThread
    /* renamed from: w */
    public final String mo18983w() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5082l;
    }

    @WorkerThread
    /* renamed from: x */
    public final long mo18984x() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5083m;
    }

    @WorkerThread
    /* renamed from: y */
    public final long mo18985y() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5084n;
    }

    @WorkerThread
    /* renamed from: z */
    public final long mo18986z() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5091u;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18933a(String str) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9428c(this.f5073c, str);
        this.f5073c = str;
    }

    @WorkerThread
    /* renamed from: k */
    public final void mo18966k(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5095y != j;
        this.f5095y = j;
    }

    @WorkerThread
    /* renamed from: l */
    public final void mo18968l(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5096z != j;
        this.f5096z = j;
    }

    @WorkerThread
    /* renamed from: m */
    public final void mo18970m(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5064A != j;
        this.f5064A = j;
    }

    @WorkerThread
    /* renamed from: n */
    public final void mo18972n(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5066C != j;
        this.f5066C = j;
    }

    @WorkerThread
    /* renamed from: o */
    public final void mo18974o(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5065B != j;
        this.f5065B = j;
    }

    @WorkerThread
    /* renamed from: p */
    public final void mo18976p(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5086p != j;
        this.f5086p = j;
    }

    @WorkerThread
    /* renamed from: e */
    public final void mo18949e(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5084n != j;
        this.f5084n = j;
    }

    @WorkerThread
    /* renamed from: f */
    public final void mo18952f(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5091u != j;
        this.f5091u = j;
    }

    @WorkerThread
    /* renamed from: g */
    public final void mo18954g(long j) {
        boolean z = true;
        C2258v.m5636a(j >= 0);
        this.f5071a.mo19014j().mo18881c();
        boolean z2 = this.f5068E;
        if (this.f5077g == j) {
            z = false;
        }
        this.f5068E = z | z2;
        this.f5077g = j;
    }

    @WorkerThread
    /* renamed from: h */
    public final void mo18957h(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5069F != j;
        this.f5069F = j;
    }

    @WorkerThread
    /* renamed from: i */
    public final void mo18962i(String str) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9428c(this.f5067D, str);
        this.f5067D = str;
    }

    @WorkerThread
    @Nullable
    /* renamed from: j */
    public final List<String> mo18963j() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5092v;
    }

    @WorkerThread
    /* renamed from: b */
    public final void mo18938b(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5079i != j;
        this.f5079i = j;
    }

    @WorkerThread
    /* renamed from: c */
    public final void mo18942c(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5081k != j;
        this.f5081k = j;
    }

    @WorkerThread
    /* renamed from: d */
    public final void mo18946d(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5083m != j;
        this.f5083m = j;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18931a(long j) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5078h != j;
        this.f5078h = j;
    }

    @WorkerThread
    /* renamed from: e */
    public final String mo18948e() {
        this.f5071a.mo19014j().mo18881c();
        String str = this.f5067D;
        mo18962i((String) null);
        return str;
    }

    @WorkerThread
    /* renamed from: f */
    public final long mo18951f() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5086p;
    }

    @WorkerThread
    /* renamed from: h */
    public final boolean mo18959h() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5088r;
    }

    @WorkerThread
    /* renamed from: i */
    public final Boolean mo18960i() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5090t;
    }

    @WorkerThread
    /* renamed from: b */
    public final long mo18937b() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5066C;
    }

    @WorkerThread
    /* renamed from: c */
    public final long mo18941c() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5065B;
    }

    @WorkerThread
    /* renamed from: d */
    public final String mo18945d() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5067D;
    }

    @WorkerThread
    /* renamed from: g */
    public final boolean mo18956g() {
        this.f5071a.mo19014j().mo18881c();
        return this.f5087q;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18935a(boolean z) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5085o != z;
        this.f5085o = z;
    }

    @WorkerThread
    /* renamed from: b */
    public final void mo18940b(boolean z) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5087q != z;
        this.f5087q = z;
    }

    @WorkerThread
    /* renamed from: c */
    public final void mo18944c(boolean z) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= this.f5088r != z;
        this.f5088r = z;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18932a(Boolean bool) {
        this.f5071a.mo19014j().mo18881c();
        this.f5068E |= !C3267z9.m9418a(this.f5090t, bool);
        this.f5090t = bool;
    }

    @WorkerThread
    /* renamed from: a */
    public final void mo18934a(@Nullable List<String> list) {
        this.f5071a.mo19014j().mo18881c();
        if (!C3267z9.m9421a(this.f5092v, list)) {
            this.f5068E = true;
            this.f5092v = list != null ? new ArrayList(list) : null;
        }
    }
}
