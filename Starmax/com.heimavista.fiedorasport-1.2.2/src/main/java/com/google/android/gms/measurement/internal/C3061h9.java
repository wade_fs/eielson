package com.google.android.gms.measurement.internal;

import android.os.Bundle;

/* renamed from: com.google.android.gms.measurement.internal.h9 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final /* synthetic */ class C3061h9 implements Runnable {

    /* renamed from: P */
    private final C3013d9 f5188P;

    C3061h9(C3013d9 d9Var) {
        this.f5188P = d9Var;
    }

    public final void run() {
        C3013d9 d9Var = this.f5188P;
        C3001c9 c9Var = d9Var.f5057Q;
        long j = d9Var.f5056P;
        c9Var.f5021c.mo18881c();
        c9Var.f5021c.mo19015l().mo18995A().mo19042a("Application going to the background");
        c9Var.f5021c.mo18884m().mo19265a("auto", "_ab", j, new Bundle());
    }
}
