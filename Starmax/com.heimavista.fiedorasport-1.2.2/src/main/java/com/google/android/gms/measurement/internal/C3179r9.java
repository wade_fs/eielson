package com.google.android.gms.measurement.internal;

/* renamed from: com.google.android.gms.measurement.internal.r9 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
final class C3179r9 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3212u9 f5596P;

    /* renamed from: Q */
    private final /* synthetic */ C3145o9 f5597Q;

    C3179r9(C3145o9 o9Var, C3212u9 u9Var) {
        this.f5597Q = o9Var;
        this.f5596P = u9Var;
    }

    public final void run() {
        this.f5597Q.m9056a(this.f5596P);
        this.f5597Q.mo19208a();
    }
}
