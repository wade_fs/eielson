package com.google.android.gms.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.maps.model.C2934e;
import com.google.android.gms.maps.p093i.C2890a0;
import com.google.android.gms.maps.p093i.C2892b0;
import com.google.android.gms.maps.p093i.C2901g;
import com.google.android.gms.maps.p093i.C2905j;
import java.util.ArrayList;
import java.util.List;
import p119e.p144d.p145a.p157c.p160b.C3986a;
import p119e.p144d.p145a.p157c.p160b.C3992d;
import p119e.p144d.p145a.p157c.p160b.C3993e;

public class StreetViewPanoramaView extends FrameLayout {

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaView$a */
    static class C2872a implements C2905j {

        /* renamed from: a */
        private final ViewGroup f4764a;

        /* renamed from: b */
        private final C2901g f4765b;

        /* renamed from: c */
        private View f4766c;

        public C2872a(ViewGroup viewGroup, C2901g gVar) {
            C2258v.m5629a(gVar);
            this.f4765b = gVar;
            C2258v.m5629a(viewGroup);
            this.f4764a = viewGroup;
        }

        /* renamed from: a */
        public final void mo18350a(Activity activity, Bundle bundle, Bundle bundle2) {
            throw new UnsupportedOperationException("onInflate not allowed on StreetViewPanoramaViewDelegate");
        }

        /* renamed from: b */
        public final void mo18354b(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4765b.mo18474b(bundle2);
                C2890a0.m8135a(bundle2, bundle);
                this.f4766c = (View) C3992d.m11991e(this.f4765b.getView());
                this.f4764a.removeAllViews();
                this.f4764a.addView(this.f4766c);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: c */
        public final void mo18355c() {
            try {
                this.f4765b.mo18475c();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: d */
        public final void mo18356d() {
            try {
                this.f4765b.mo18476d();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: g */
        public final void mo18357g() {
            throw new UnsupportedOperationException("onDestroyView not allowed on StreetViewPanoramaViewDelegate");
        }

        public final void onLowMemory() {
            try {
                this.f4765b.onLowMemory();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onResume() {
            try {
                this.f4765b.onResume();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        public final void onStart() {
            try {
                this.f4765b.onStart();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final View mo18349a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
            throw new UnsupportedOperationException("onCreateView not allowed on StreetViewPanoramaViewDelegate");
        }

        /* renamed from: a */
        public final void mo18351a(Bundle bundle) {
            try {
                Bundle bundle2 = new Bundle();
                C2890a0.m8135a(bundle, bundle2);
                this.f4765b.mo18471a(bundle2);
                C2890a0.m8135a(bundle2, bundle);
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: a */
        public final void mo18395a(C2886f fVar) {
            try {
                this.f4765b.mo18472a(new C2957o(this, fVar));
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }

        /* renamed from: b */
        public final void mo18353b() {
            try {
                this.f4765b.mo18473b();
            } catch (RemoteException e) {
                throw new C2934e(e);
            }
        }
    }

    /* renamed from: com.google.android.gms.maps.StreetViewPanoramaView$b */
    static class C2873b extends C3986a<C2872a> {

        /* renamed from: e */
        private final ViewGroup f4767e;

        /* renamed from: f */
        private final Context f4768f;

        /* renamed from: g */
        private C3993e<C2872a> f4769g;

        /* renamed from: h */
        private final StreetViewPanoramaOptions f4770h;

        /* renamed from: i */
        private final List<C2886f> f4771i = new ArrayList();

        C2873b(ViewGroup viewGroup, Context context, StreetViewPanoramaOptions streetViewPanoramaOptions) {
            this.f4767e = viewGroup;
            this.f4768f = context;
            this.f4770h = streetViewPanoramaOptions;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public final void mo18361a(C3993e<C2872a> eVar) {
            this.f4769g = eVar;
            if (this.f4769g != null && mo23619a() == null) {
                try {
                    C2884d.m8123a(this.f4768f);
                    this.f4769g.mo23633a(new C2872a(this.f4767e, C2892b0.m8145a(this.f4768f).mo18443a(C3992d.m11990a(this.f4768f), this.f4770h)));
                    for (C2886f fVar : this.f4771i) {
                        ((C2872a) mo23619a()).mo18395a(fVar);
                    }
                    this.f4771i.clear();
                } catch (RemoteException e) {
                    throw new C2934e(e);
                } catch (C2170d unused) {
                }
            }
        }
    }

    public StreetViewPanoramaView(Context context) {
        super(context);
        new C2873b(this, context, null);
    }

    public StreetViewPanoramaView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        new C2873b(this, context, null);
    }

    public StreetViewPanoramaView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        new C2873b(this, context, null);
    }
}
