package com.google.android.exoplayer2.offline;

import com.google.android.exoplayer2.offline.DownloadHelper;

/* renamed from: com.google.android.exoplayer2.offline.a */
/* compiled from: lambda */
public final /* synthetic */ class C1843a implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ DownloadHelper.C18391 f2839P;

    /* renamed from: Q */
    private final /* synthetic */ DownloadHelper.Callback f2840Q;

    public /* synthetic */ C1843a(DownloadHelper.C18391 r1, DownloadHelper.Callback callback) {
        this.f2839P = r1;
        this.f2840Q = callback;
    }

    public final void run() {
        this.f2839P.mo14710a(this.f2840Q);
    }
}
