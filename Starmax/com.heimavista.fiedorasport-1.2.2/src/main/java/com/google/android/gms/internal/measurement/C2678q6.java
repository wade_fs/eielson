package com.google.android.gms.internal.measurement;

import java.util.Iterator;

/* renamed from: com.google.android.gms.internal.measurement.q6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2678q6 {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final Iterator<Object> f4428a = new C2725t6();

    /* renamed from: b */
    private static final Iterable<Object> f4429b = new C2708s6();

    /* renamed from: a */
    static <T> Iterable<T> m7084a() {
        return f4429b;
    }
}
