package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.datatransport.cct.p084b.C1675h;
import java.util.List;

/* renamed from: com.google.android.datatransport.cct.b.r */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public abstract class C1692r {

    /* renamed from: com.google.android.datatransport.cct.b.r$a */
    /* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
    public static abstract class C1693a {
        /* access modifiers changed from: package-private */
        @NonNull
        /* renamed from: a */
        public abstract C1693a mo13504a(int i);

        @NonNull
        /* renamed from: a */
        public abstract C1693a mo13505a(long j);

        @NonNull
        /* renamed from: a */
        public abstract C1693a mo13506a(@Nullable C1663b bVar);

        @NonNull
        /* renamed from: a */
        public abstract C1693a mo13507a(@Nullable C1684m mVar);

        /* access modifiers changed from: package-private */
        @NonNull
        /* renamed from: a */
        public abstract C1693a mo13508a(@Nullable String str);

        @NonNull
        /* renamed from: a */
        public abstract C1693a mo13509a(@Nullable List<C1689p> list);

        @NonNull
        /* renamed from: a */
        public abstract C1692r mo13510a();

        @NonNull
        /* renamed from: b */
        public C1693a mo13524b(int i) {
            mo13504a(i);
            return this;
        }

        @NonNull
        /* renamed from: b */
        public abstract C1693a mo13511b(long j);

        @NonNull
        /* renamed from: b */
        public C1693a mo13525b(@NonNull String str) {
            mo13508a(str);
            return this;
        }
    }

    @NonNull
    /* renamed from: a */
    public static C1693a m4236a() {
        C1675h.C1677b bVar = new C1675h.C1677b();
        bVar.mo13504a(Integer.MIN_VALUE);
        return bVar;
    }
}
