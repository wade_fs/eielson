package com.google.android.gms.auth.api.accounttransfer;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import com.google.android.gms.internal.auth.zzaz;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class zzr extends zzaz {
    public static final Parcelable.Creator<zzr> CREATOR = new C1956c();

    /* renamed from: V */
    private static final HashMap<String, FastJsonResponse.Field<?, ?>> f3010V;

    /* renamed from: P */
    private final Set<Integer> f3011P;

    /* renamed from: Q */
    private final int f3012Q;

    /* renamed from: R */
    private zzt f3013R;

    /* renamed from: S */
    private String f3014S;

    /* renamed from: T */
    private String f3015T;

    /* renamed from: U */
    private String f3016U;

    static {
        HashMap<String, FastJsonResponse.Field<?, ?>> hashMap = new HashMap<>();
        f3010V = hashMap;
        hashMap.put("authenticatorInfo", FastJsonResponse.Field.m5711a("authenticatorInfo", 2, zzt.class));
        f3010V.put("signature", FastJsonResponse.Field.m5715c("signature", 3));
        f3010V.put("package", FastJsonResponse.Field.m5715c("package", 4));
    }

    public zzr() {
        this.f3011P = new HashSet(3);
        this.f3012Q = 1;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Object mo16319a(FastJsonResponse.Field field) {
        int c = field.mo17091c();
        if (c == 1) {
            return Integer.valueOf(this.f3012Q);
        }
        if (c == 2) {
            return this.f3013R;
        }
        if (c == 3) {
            return this.f3014S;
        }
        if (c == 4) {
            return this.f3015T;
        }
        int c2 = field.mo17091c();
        StringBuilder sb = new StringBuilder(37);
        sb.append("Unknown SafeParcelable id=");
        sb.append(c2);
        throw new IllegalStateException(sb.toString());
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo16321b(FastJsonResponse.Field field) {
        return this.f3011P.contains(Integer.valueOf(field.mo17091c()));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void
     arg types: [android.os.Parcel, int, com.google.android.gms.auth.api.accounttransfer.zzt, int, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable[], int, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcelable, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        Set<Integer> set = this.f3011P;
        if (set.contains(1)) {
            C2250b.m5591a(parcel, 1, this.f3012Q);
        }
        if (set.contains(2)) {
            C2250b.m5596a(parcel, 2, (Parcelable) this.f3013R, i, true);
        }
        if (set.contains(3)) {
            C2250b.m5602a(parcel, 3, this.f3014S, true);
        }
        if (set.contains(4)) {
            C2250b.m5602a(parcel, 4, this.f3015T, true);
        }
        if (set.contains(5)) {
            C2250b.m5602a(parcel, 5, this.f3016U, true);
        }
        C2250b.m5587a(parcel, a);
    }

    zzr(Set<Integer> set, int i, zzt zzt, String str, String str2, String str3) {
        this.f3011P = set;
        this.f3012Q = i;
        this.f3013R = zzt;
        this.f3014S = str;
        this.f3015T = str2;
        this.f3016U = str3;
    }

    /* renamed from: a */
    public /* synthetic */ Map mo16320a() {
        return f3010V;
    }
}
