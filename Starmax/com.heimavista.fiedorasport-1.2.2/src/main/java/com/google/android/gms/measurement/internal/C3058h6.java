package com.google.android.gms.measurement.internal;

import android.content.Context;
import com.google.android.gms.common.util.C2314e;

/* renamed from: com.google.android.gms.measurement.internal.h6 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
interface C3058h6 {
    /* renamed from: j */
    C3045g5 mo19014j();

    /* renamed from: l */
    C3032f4 mo19015l();

    /* renamed from: n */
    Context mo19016n();

    /* renamed from: o */
    C2314e mo19017o();

    /* renamed from: r */
    C3098ka mo19018r();
}
