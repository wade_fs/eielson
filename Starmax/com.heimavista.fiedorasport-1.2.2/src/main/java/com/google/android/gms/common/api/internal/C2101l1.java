package com.google.android.gms.common.api.internal;

import androidx.annotation.Nullable;
import com.google.android.gms.common.Feature;
import com.google.android.gms.common.api.internal.C2064e;

/* renamed from: com.google.android.gms.common.api.internal.l1 */
public abstract class C2101l1 extends C2116p0 {
    public C2101l1(int i) {
        super(i);
    }

    @Nullable
    /* renamed from: b */
    public abstract Feature[] mo16634b(C2064e.C2065a<?> aVar);

    /* renamed from: c */
    public abstract boolean mo16635c(C2064e.C2065a<?> aVar);
}
