package com.google.android.gms.common.internal;

import android.accounts.Account;
import android.content.Context;
import android.os.IInterface;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.common.internal.C2197d;
import com.google.android.gms.common.internal.C2221i;
import java.util.Set;

/* renamed from: com.google.android.gms.common.internal.h */
public abstract class C2219h<T extends IInterface> extends C2197d<T> implements C2016a.C2027f, C2221i.C2222a {

    /* renamed from: B */
    private final C2211e f3711B;

    /* renamed from: C */
    private final Set<Scope> f3712C;

    /* renamed from: D */
    private final Account f3713D;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected C2219h(android.content.Context r10, android.os.Looper r11, int r12, com.google.android.gms.common.internal.C2211e r13, com.google.android.gms.common.api.C2036f.C2038b r14, com.google.android.gms.common.api.C2036f.C2039c r15) {
        /*
            r9 = this;
            com.google.android.gms.common.internal.j r3 = com.google.android.gms.common.internal.C2224j.m5496a(r10)
            com.google.android.gms.common.b r4 = com.google.android.gms.common.C2167b.m5251a()
            com.google.android.gms.common.internal.C2258v.m5629a(r14)
            r7 = r14
            com.google.android.gms.common.api.f$b r7 = (com.google.android.gms.common.api.C2036f.C2038b) r7
            com.google.android.gms.common.internal.C2258v.m5629a(r15)
            r8 = r15
            com.google.android.gms.common.api.f$c r8 = (com.google.android.gms.common.api.C2036f.C2039c) r8
            r0 = r9
            r1 = r10
            r2 = r11
            r5 = r12
            r6 = r13
            r0.<init>(r1, r2, r3, r4, r5, r6, r7, r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.common.internal.C2219h.<init>(android.content.Context, android.os.Looper, int, com.google.android.gms.common.internal.e, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void");
    }

    @Nullable
    /* renamed from: a */
    private static C2197d.C2198a m5476a(C2036f.C2038b bVar) {
        if (bVar == null) {
            return null;
        }
        return new C2214e0(bVar);
    }

    /* renamed from: b */
    private final Set<Scope> m5478b(@NonNull Set<Scope> set) {
        mo16977a(set);
        for (Scope scope : set) {
            if (!set.contains(scope)) {
                throw new IllegalStateException("Expanding scopes is not permitted, use implied scopes instead");
            }
        }
        return set;
    }

    /* access modifiers changed from: protected */
    /* renamed from: C */
    public final C2211e mo16976C() {
        return this.f3711B;
    }

    /* access modifiers changed from: protected */
    @NonNull
    /* renamed from: a */
    public Set<Scope> mo16977a(@NonNull Set<Scope> set) {
        return set;
    }

    /* renamed from: i */
    public int mo16451i() {
        return super.mo16451i();
    }

    /* renamed from: r */
    public final Account mo16933r() {
        return this.f3713D;
    }

    /* access modifiers changed from: protected */
    /* renamed from: w */
    public final Set<Scope> mo16938w() {
        return this.f3712C;
    }

    @Nullable
    /* renamed from: a */
    private static C2197d.C2199b m5477a(C2036f.C2039c cVar) {
        if (cVar == null) {
            return null;
        }
        return new C2216f0(cVar);
    }

    protected C2219h(Context context, Looper looper, C2224j jVar, C2167b bVar, int i, C2211e eVar, C2036f.C2038b bVar2, C2036f.C2039c cVar) {
        super(context, looper, jVar, bVar, i, m5476a(bVar2), m5477a(cVar), eVar.mo16964g());
        this.f3711B = eVar;
        this.f3713D = eVar.mo16956a();
        Set<Scope> d = eVar.mo16961d();
        m5478b(d);
        this.f3712C = d;
    }
}
