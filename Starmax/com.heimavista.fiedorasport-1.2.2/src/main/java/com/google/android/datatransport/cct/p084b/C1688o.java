package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import com.google.firebase.p096e.C3533a;
import com.google.firebase.p096e.p098i.C3543c;

/* renamed from: com.google.android.datatransport.cct.b.o */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public class C1688o {
    @NonNull
    /* renamed from: a */
    public static C3533a m4221a() {
        C3543c cVar = new C3543c();
        cVar.mo22178a(C1668e.class, new C1683l());
        cVar.mo22178a(C1675h.class, new C1694s());
        cVar.mo22178a(C1669f.class, new C1687n());
        cVar.mo22178a(C1672g.class, new C1691q());
        cVar.mo22178a(C1665d.class, new C1664c());
        cVar.mo22178a(C1679j.class, new C1700v());
        return cVar.mo22177a();
    }
}
