package com.google.android.exoplayer2;

import androidx.annotation.Nullable;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;

/* renamed from: com.google.android.exoplayer2.b */
/* compiled from: Player */
public final /* synthetic */ class C1778b {
    public static void $default$onLoadingChanged(Player.EventListener eventListener, boolean z) {
    }

    public static void $default$onPlaybackParametersChanged(Player.EventListener eventListener, PlaybackParameters playbackParameters) {
    }

    public static void $default$onPlayerError(Player.EventListener eventListener, ExoPlaybackException exoPlaybackException) {
    }

    public static void $default$onPlayerStateChanged(Player.EventListener eventListener, boolean z, int i) {
    }

    public static void $default$onPositionDiscontinuity(Player.EventListener eventListener, int i) {
    }

    public static void $default$onRepeatModeChanged(Player.EventListener eventListener, int i) {
    }

    public static void $default$onSeekProcessed(Player.EventListener eventListener) {
    }

    public static void $default$onShuffleModeEnabledChanged(Player.EventListener eventListener, boolean z) {
    }

    public static void $default$onTimelineChanged(Player.EventListener eventListener, @Nullable Timeline timeline, Object obj, int i) {
    }

    public static void $default$onTracksChanged(Player.EventListener eventListener, TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }
}
