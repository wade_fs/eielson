package com.google.android.gms.common.api.internal;

import com.google.android.gms.common.api.C2040g;
import com.google.android.gms.common.api.Status;

/* renamed from: com.google.android.gms.common.api.internal.s */
final class C2127s implements C2040g.C2041a {

    /* renamed from: a */
    private final /* synthetic */ BasePendingResult f3435a;

    /* renamed from: b */
    private final /* synthetic */ C2123r f3436b;

    C2127s(C2123r rVar, BasePendingResult basePendingResult) {
        this.f3436b = rVar;
        this.f3435a = basePendingResult;
    }

    /* renamed from: a */
    public final void mo16589a(Status status) {
        this.f3436b.f3428a.remove(this.f3435a);
    }
}
