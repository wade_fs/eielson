package com.google.android.gms.internal.measurement;

import com.google.android.gms.internal.measurement.C2595l4;
import java.util.List;

/* renamed from: com.google.android.gms.internal.measurement.y0 */
/* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
public final class C2794y0 extends C2595l4<C2794y0, C2795a> implements C2769w5 {
    /* access modifiers changed from: private */
    public static final C2794y0 zzg;
    private static volatile C2501f6<C2794y0> zzh;
    private C2691r4 zzc = C2595l4.m6648l();
    private C2691r4 zzd = C2595l4.m6648l();
    private C2738u4<C2686r0> zze = C2595l4.m6649m();
    private C2738u4<C2810z0> zzf = C2595l4.m6649m();

    /* renamed from: com.google.android.gms.internal.measurement.y0$a */
    /* compiled from: com.google.android.gms:play-services-measurement@@17.2.2 */
    public static final class C2795a extends C2595l4.C2596a<C2794y0, C2795a> implements C2769w5 {
        private C2795a() {
            super(C2794y0.zzg);
        }

        /* renamed from: a */
        public final C2795a mo18157a(Iterable<? extends Long> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7789a(iterable);
            return this;
        }

        /* renamed from: b */
        public final C2795a mo18159b(Iterable<? extends Long> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7793b(iterable);
            return this;
        }

        /* renamed from: c */
        public final C2795a mo18160c(Iterable<? extends C2686r0> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7795c(iterable);
            return this;
        }

        /* renamed from: d */
        public final C2795a mo18161d(Iterable<? extends C2810z0> iterable) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7798d(iterable);
            return this;
        }

        /* renamed from: j */
        public final C2795a mo18162j() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7803z();
            return this;
        }

        /* renamed from: k */
        public final C2795a mo18163k() {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7783A();
            return this;
        }

        /* synthetic */ C2795a(C2657p0 p0Var) {
            this();
        }

        /* renamed from: a */
        public final C2795a mo18156a(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7796d(i);
            return this;
        }

        /* renamed from: b */
        public final C2795a mo18158b(int i) {
            if (super.f4289R) {
                mo17676f();
                super.f4289R = false;
            }
            ((C2794y0) super.f4288Q).m7799e(i);
            return this;
        }
    }

    static {
        C2794y0 y0Var = new C2794y0();
        zzg = y0Var;
        C2595l4.m6645a(C2794y0.class, super);
    }

    private C2794y0() {
    }

    /* access modifiers changed from: private */
    /* renamed from: A */
    public final void m7783A() {
        this.zzd = C2595l4.m6648l();
    }

    /* renamed from: B */
    private final void m7784B() {
        if (!this.zze.mo17938a()) {
            this.zze = C2595l4.m6642a(this.zze);
        }
    }

    /* renamed from: C */
    private final void m7785C() {
        if (!this.zzf.mo17938a()) {
            this.zzf = C2595l4.m6642a(this.zzf);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public final void m7789a(Iterable<? extends Long> iterable) {
        if (!this.zzc.mo17938a()) {
            this.zzc = C2595l4.m6641a(this.zzc);
        }
        C2766w2.m7700a(iterable, this.zzc);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public final void m7793b(Iterable<? extends Long> iterable) {
        if (!this.zzd.mo17938a()) {
            this.zzd = C2595l4.m6641a(this.zzd);
        }
        C2766w2.m7700a(iterable, this.zzd);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public final void m7795c(Iterable<? extends C2686r0> iterable) {
        m7784B();
        C2766w2.m7700a(iterable, this.zze);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final void m7796d(int i) {
        m7784B();
        this.zze.remove(i);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public final void m7799e(int i) {
        m7785C();
        this.zzf.remove(i);
    }

    /* renamed from: w */
    public static C2795a m7800w() {
        return (C2795a) zzg.mo17668i();
    }

    /* renamed from: x */
    public static C2794y0 m7801x() {
        return zzg;
    }

    /* access modifiers changed from: private */
    /* renamed from: z */
    public final void m7803z() {
        this.zzc = C2595l4.m6648l();
    }

    /* renamed from: n */
    public final List<Long> mo18148n() {
        return this.zzc;
    }

    /* renamed from: o */
    public final int mo18149o() {
        return this.zzc.size();
    }

    /* renamed from: p */
    public final List<Long> mo18150p() {
        return this.zzd;
    }

    /* renamed from: q */
    public final int mo18151q() {
        return this.zzd.size();
    }

    /* renamed from: s */
    public final List<C2686r0> mo18152s() {
        return this.zze;
    }

    /* renamed from: t */
    public final int mo18153t() {
        return this.zze.size();
    }

    /* renamed from: u */
    public final List<C2810z0> mo18154u() {
        return this.zzf;
    }

    /* renamed from: v */
    public final int mo18155v() {
        return this.zzf.size();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public final void m7798d(Iterable<? extends C2810z0> iterable) {
        m7785C();
        C2766w2.m7700a(iterable, this.zzf);
    }

    /* renamed from: c */
    public final C2810z0 mo18147c(int i) {
        return this.zzf.get(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Object mo17247a(int i, Object obj, Object obj2) {
        switch (C2657p0.f4415a[i - 1]) {
            case 1:
                return new C2794y0();
            case 2:
                return new C2795a(null);
            case 3:
                return C2595l4.m6643a(zzg, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zzc", "zzd", "zze", C2686r0.class, "zzf", C2810z0.class});
            case 4:
                return zzg;
            case 5:
                C2501f6<C2794y0> f6Var = zzh;
                if (f6Var == null) {
                    synchronized (C2794y0.class) {
                        f6Var = zzh;
                        if (f6Var == null) {
                            f6Var = new C2595l4.C2598c<>(zzg);
                            zzh = f6Var;
                        }
                    }
                }
                return f6Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }

    /* renamed from: b */
    public final C2686r0 mo18146b(int i) {
        return this.zze.get(i);
    }
}
