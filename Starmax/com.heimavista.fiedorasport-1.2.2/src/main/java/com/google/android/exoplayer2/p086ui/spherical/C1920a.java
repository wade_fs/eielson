package com.google.android.exoplayer2.p086ui.spherical;

import android.graphics.SurfaceTexture;

/* renamed from: com.google.android.exoplayer2.ui.spherical.a */
/* compiled from: lambda */
public final /* synthetic */ class C1920a implements SurfaceTexture.OnFrameAvailableListener {

    /* renamed from: P */
    private final /* synthetic */ SceneRenderer f2930P;

    public /* synthetic */ C1920a(SceneRenderer sceneRenderer) {
        this.f2930P = sceneRenderer;
    }

    public final void onFrameAvailable(SurfaceTexture surfaceTexture) {
        this.f2930P.mo15873a(surfaceTexture);
    }
}
