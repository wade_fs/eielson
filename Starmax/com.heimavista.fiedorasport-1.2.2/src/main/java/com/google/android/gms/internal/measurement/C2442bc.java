package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.bc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2442bc implements C2593l2<C2426ac> {

    /* renamed from: Q */
    private static C2442bc f4003Q = new C2442bc();

    /* renamed from: P */
    private final C2593l2<C2426ac> f4004P;

    private C2442bc(C2593l2<C2426ac> l2Var) {
        this.f4004P = C2579k2.m6600a((C2593l2) l2Var);
    }

    /* renamed from: b */
    public static boolean m6096b() {
        return ((C2426ac) f4003Q.mo17285a()).mo17291a();
    }

    /* renamed from: a */
    public final /* synthetic */ Object mo17285a() {
        return this.f4004P.mo17285a();
    }

    public C2442bc() {
        this(C2579k2.m6601a(new C2474dc()));
    }
}
