package com.google.android.datatransport.runtime.scheduling.jobscheduling;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.PersistableBundle;
import android.util.Base64;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.google.android.exoplayer2.C1750C;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.zip.Adler32;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p148t.C3915a;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p154y.C3977a;

@RequiresApi(api = 21)
/* renamed from: com.google.android.datatransport.runtime.scheduling.jobscheduling.e */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C1731e implements C1749s {

    /* renamed from: a */
    private final Context f2729a;

    /* renamed from: b */
    private final C3934c f2730b;

    /* renamed from: c */
    private final C1733g f2731c;

    public C1731e(Context context, C3934c cVar, C1733g gVar) {
        this.f2729a = context;
        this.f2730b = cVar;
        this.f2731c = gVar;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: a */
    public int mo13579a(C3905l lVar) {
        Adler32 adler32 = new Adler32();
        adler32.update(this.f2729a.getPackageName().getBytes(Charset.forName(C1750C.UTF8_NAME)));
        adler32.update(lVar.mo23531a().getBytes(Charset.forName(C1750C.UTF8_NAME)));
        adler32.update(ByteBuffer.allocate(4).putInt(C3977a.m11939a(lVar.mo23533c())).array());
        if (lVar.mo23532b() != null) {
            adler32.update(lVar.mo23532b());
        }
        return (int) adler32.getValue();
    }

    /* renamed from: a */
    private boolean m4320a(JobScheduler jobScheduler, int i, int i2) {
        for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
            int i3 = jobInfo.getExtras().getInt("attemptNumber");
            if (jobInfo.getId() == i) {
                if (i3 >= i2) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    /* renamed from: a */
    public void mo13561a(C3905l lVar, int i) {
        ComponentName componentName = new ComponentName(this.f2729a, JobInfoSchedulerService.class);
        JobScheduler jobScheduler = (JobScheduler) this.f2729a.getSystemService("jobscheduler");
        int a = mo13579a(lVar);
        if (m4320a(jobScheduler, a, i)) {
            C3915a.m11792a("JobInfoScheduler", "Upload for context %s is already scheduled. Returning...", lVar);
            return;
        }
        long b = this.f2730b.mo23590b(lVar);
        C1733g gVar = this.f2731c;
        JobInfo.Builder builder = new JobInfo.Builder(a, componentName);
        gVar.mo13582a(builder, lVar.mo23533c(), b, i);
        PersistableBundle persistableBundle = new PersistableBundle();
        persistableBundle.putInt("attemptNumber", i);
        persistableBundle.putString("backendName", lVar.mo23531a());
        persistableBundle.putInt("priority", C3977a.m11939a(lVar.mo23533c()));
        if (lVar.mo23532b() != null) {
            persistableBundle.putString("extras", Base64.encodeToString(lVar.mo23532b(), 0));
        }
        builder.setExtras(persistableBundle);
        C3915a.m11794a("JobInfoScheduler", "Scheduling upload for context %s with jobId=%d in %dms(Backend next call timestamp %d). Attempt %d", lVar, Integer.valueOf(a), Long.valueOf(this.f2731c.mo13581a(lVar.mo23533c(), b, i)), Long.valueOf(b), Integer.valueOf(i));
        jobScheduler.schedule(builder.build());
    }
}
