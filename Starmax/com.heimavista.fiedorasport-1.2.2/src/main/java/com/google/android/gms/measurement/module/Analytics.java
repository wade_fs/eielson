package com.google.android.gms.measurement.module;

import android.content.Context;
import androidx.annotation.Keep;
import androidx.annotation.RequiresPermission;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.internal.measurement.zzv;
import com.google.android.gms.measurement.internal.C3081j5;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public class Analytics {

    /* renamed from: a */
    private static volatile Analytics f5847a;

    private Analytics(C3081j5 j5Var) {
        C2258v.m5629a(j5Var);
    }

    @RequiresPermission(allOf = {"android.permission.INTERNET", "android.permission.ACCESS_NETWORK_STATE", "android.permission.WAKE_LOCK"})
    @Keep
    public static Analytics getInstance(Context context) {
        if (f5847a == null) {
            synchronized (Analytics.class) {
                if (f5847a == null) {
                    f5847a = new Analytics(C3081j5.m8751a(context, (zzv) null));
                }
            }
        }
        return f5847a;
    }
}
