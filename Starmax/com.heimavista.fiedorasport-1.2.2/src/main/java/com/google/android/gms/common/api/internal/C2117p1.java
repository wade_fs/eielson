package com.google.android.gms.common.api.internal;

import com.google.android.gms.signin.internal.zaj;

/* renamed from: com.google.android.gms.common.api.internal.p1 */
final class C2117p1 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ zaj f3418P;

    /* renamed from: Q */
    private final /* synthetic */ C2109n1 f3419Q;

    C2117p1(C2109n1 n1Var, zaj zaj) {
        this.f3419Q = n1Var;
        this.f3418P = zaj;
    }

    public final void run() {
        this.f3419Q.m5000b(this.f3418P);
    }
}
