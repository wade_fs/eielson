package com.google.android.datatransport.cct.p084b;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.firebase.p096e.C3536d;
import com.google.firebase.p096e.C3537e;

/* renamed from: com.google.android.datatransport.cct.b.v */
/* compiled from: com.google.android.datatransport:transport-backend-cct@@2.2.0 */
public final class C1700v implements C3536d<C1679j> {
    /* renamed from: a */
    public void mo13453a(@Nullable Object obj, @NonNull Object obj2) {
        C1679j jVar = (C1679j) obj;
        C3537e eVar = (C3537e) obj2;
        if (jVar.mo13516b() != null) {
            eVar.mo22173a("mobileSubtype", jVar.mo13516b().name());
        }
        if (jVar.mo13517c() != null) {
            eVar.mo22173a("networkType", jVar.mo13517c().name());
        }
    }
}
