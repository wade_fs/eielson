package com.google.android.exoplayer2.extractor.flv;

import com.google.android.exoplayer2.extractor.Extractor;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;

/* renamed from: com.google.android.exoplayer2.extractor.flv.a */
/* compiled from: lambda */
public final /* synthetic */ class C1797a implements ExtractorsFactory {

    /* renamed from: a */
    public static final /* synthetic */ C1797a f2812a = new C1797a();

    private /* synthetic */ C1797a() {
    }

    public final Extractor[] createExtractors() {
        return FlvExtractor.m4381a();
    }
}
