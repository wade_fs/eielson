package com.google.android.gms.internal.measurement;

/* renamed from: com.google.android.gms.internal.measurement.rc */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
public final class C2699rc implements C2714sc {

    /* renamed from: a */
    private static final C2765w1<Boolean> f4470a = new C2448c2(C2718t1.m7213a("com.google.android.gms.measurement")).mo17367a("measurement.sdk.collection.retrieve_deeplink_from_bow_2", true);

    /* renamed from: a */
    public final boolean mo17845a() {
        return f4470a.mo18128b().booleanValue();
    }
}
