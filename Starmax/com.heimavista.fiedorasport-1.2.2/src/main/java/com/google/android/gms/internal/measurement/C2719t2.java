package com.google.android.gms.internal.measurement;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: com.google.android.gms.internal.measurement.t2 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2719t2 extends WeakReference<Throwable> {

    /* renamed from: a */
    private final int f4495a;

    public C2719t2(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.f4495a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == C2719t2.class) {
            if (this == obj) {
                return true;
            }
            C2719t2 t2Var = (C2719t2) obj;
            return this.f4495a == t2Var.f4495a && get() == super.get();
        }
    }

    public final int hashCode() {
        return this.f4495a;
    }
}
