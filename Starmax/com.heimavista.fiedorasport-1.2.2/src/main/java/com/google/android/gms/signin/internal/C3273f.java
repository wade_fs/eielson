package com.google.android.gms.signin.internal;

import android.os.IInterface;
import com.google.android.gms.common.internal.C2231m;

/* renamed from: com.google.android.gms.signin.internal.f */
public interface C3273f extends IInterface {
    /* renamed from: J */
    void mo19483J(int i);

    /* renamed from: a */
    void mo19484a(C2231m mVar, int i, boolean z);

    /* renamed from: a */
    void mo19485a(zah zah, C3271d dVar);
}
