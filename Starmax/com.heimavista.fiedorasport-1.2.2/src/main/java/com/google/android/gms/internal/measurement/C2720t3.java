package com.google.android.gms.internal.measurement;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/* renamed from: com.google.android.gms.internal.measurement.t3 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
public abstract class C2720t3 extends C2449c3 {

    /* renamed from: b */
    private static final Logger f4496b = Logger.getLogger(C2720t3.class.getName());
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static final boolean f4497c = C2566j7.m6525a();

    /* renamed from: a */
    C2752v3 f4498a;

    /* renamed from: com.google.android.gms.internal.measurement.t3$a */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    static class C2721a extends C2720t3 {

        /* renamed from: d */
        private final byte[] f4499d;

        /* renamed from: e */
        private final int f4500e;

        /* renamed from: f */
        private int f4501f;

        C2721a(byte[] bArr, int i, int i2) {
            super();
            if (bArr != null) {
                int i3 = i2 + 0;
                if ((i2 | 0 | (bArr.length - i3)) >= 0) {
                    this.f4499d = bArr;
                    this.f4501f = 0;
                    this.f4500e = i3;
                    return;
                }
                throw new IllegalArgumentException(String.format("Array range is invalid. Buffer.length=%d, offset=%d, length=%d", Integer.valueOf(bArr.length), 0, Integer.valueOf(i2)));
            }
            throw new NullPointerException("buffer");
        }

        /* renamed from: a */
        public final void mo17890a(int i, int i2) {
            mo17901b((i << 3) | i2);
        }

        /* renamed from: b */
        public final void mo17902b(int i, int i2) {
            mo17890a(i, 0);
            mo17887a(i2);
        }

        /* renamed from: c */
        public final void mo17907c(int i, int i2) {
            mo17890a(i, 0);
            mo17901b(i2);
        }

        /* renamed from: d */
        public final void mo17910d(int i) {
            try {
                byte[] bArr = this.f4499d;
                int i2 = this.f4501f;
                this.f4501f = i2 + 1;
                bArr[i2] = (byte) i;
                byte[] bArr2 = this.f4499d;
                int i3 = this.f4501f;
                this.f4501f = i3 + 1;
                bArr2[i3] = (byte) (i >> 8);
                byte[] bArr3 = this.f4499d;
                int i4 = this.f4501f;
                this.f4501f = i4 + 1;
                bArr3[i4] = (byte) (i >> 16);
                byte[] bArr4 = this.f4499d;
                int i5 = this.f4501f;
                this.f4501f = i5 + 1;
                bArr4[i5] = (byte) (i >>> 24);
            } catch (IndexOutOfBoundsException e) {
                throw new C2722b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f4501f), Integer.valueOf(this.f4500e), 1), e);
            }
        }

        /* renamed from: e */
        public final void mo17912e(int i, int i2) {
            mo17890a(i, 5);
            mo17910d(i2);
        }

        /* renamed from: a */
        public final void mo17891a(int i, long j) {
            mo17890a(i, 0);
            mo17897a(j);
        }

        /* renamed from: b */
        public final void mo17914b(C2498f3 f3Var) {
            mo17901b(f3Var.mo17469a());
            f3Var.mo17473a(this);
        }

        /* renamed from: c */
        public final void mo17908c(int i, long j) {
            mo17890a(i, 1);
            mo17909c(j);
        }

        /* renamed from: a */
        public final void mo17896a(int i, boolean z) {
            mo17890a(i, 0);
            mo17884a(z ? (byte) 1 : 0);
        }

        /* renamed from: b */
        public final void mo17904b(int i, C2498f3 f3Var) {
            mo17890a(1, 3);
            mo17907c(2, i);
            mo17892a(3, f3Var);
            mo17890a(1, 4);
        }

        /* renamed from: c */
        public final void mo17909c(long j) {
            try {
                byte[] bArr = this.f4499d;
                int i = this.f4501f;
                this.f4501f = i + 1;
                bArr[i] = (byte) ((int) j);
                byte[] bArr2 = this.f4499d;
                int i2 = this.f4501f;
                this.f4501f = i2 + 1;
                bArr2[i2] = (byte) ((int) (j >> 8));
                byte[] bArr3 = this.f4499d;
                int i3 = this.f4501f;
                this.f4501f = i3 + 1;
                bArr3[i3] = (byte) ((int) (j >> 16));
                byte[] bArr4 = this.f4499d;
                int i4 = this.f4501f;
                this.f4501f = i4 + 1;
                bArr4[i4] = (byte) ((int) (j >> 24));
                byte[] bArr5 = this.f4499d;
                int i5 = this.f4501f;
                this.f4501f = i5 + 1;
                bArr5[i5] = (byte) ((int) (j >> 32));
                byte[] bArr6 = this.f4499d;
                int i6 = this.f4501f;
                this.f4501f = i6 + 1;
                bArr6[i6] = (byte) ((int) (j >> 40));
                byte[] bArr7 = this.f4499d;
                int i7 = this.f4501f;
                this.f4501f = i7 + 1;
                bArr7[i7] = (byte) ((int) (j >> 48));
                byte[] bArr8 = this.f4499d;
                int i8 = this.f4501f;
                this.f4501f = i8 + 1;
                bArr8[i8] = (byte) ((int) (j >> 56));
            } catch (IndexOutOfBoundsException e) {
                throw new C2722b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f4501f), Integer.valueOf(this.f4500e), 1), e);
            }
        }

        /* renamed from: a */
        public final void mo17895a(int i, String str) {
            mo17890a(i, 2);
            mo17915b(str);
        }

        /* renamed from: a */
        public final void mo17892a(int i, C2498f3 f3Var) {
            mo17890a(i, 2);
            mo17914b(f3Var);
        }

        /* renamed from: b */
        public final void mo17901b(int i) {
            if (!C2720t3.f4497c || C2797y2.m7826a() || mo17883a() < 5) {
                while ((i & -128) != 0) {
                    byte[] bArr = this.f4499d;
                    int i2 = this.f4501f;
                    this.f4501f = i2 + 1;
                    bArr[i2] = (byte) ((i & 127) | 128);
                    i >>>= 7;
                }
                try {
                    byte[] bArr2 = this.f4499d;
                    int i3 = this.f4501f;
                    this.f4501f = i3 + 1;
                    bArr2[i3] = (byte) i;
                } catch (IndexOutOfBoundsException e) {
                    throw new C2722b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f4501f), Integer.valueOf(this.f4500e), 1), e);
                }
            } else if ((i & -128) == 0) {
                byte[] bArr3 = this.f4499d;
                int i4 = this.f4501f;
                this.f4501f = i4 + 1;
                C2566j7.m6524a(bArr3, (long) i4, (byte) i);
            } else {
                byte[] bArr4 = this.f4499d;
                int i5 = this.f4501f;
                this.f4501f = i5 + 1;
                C2566j7.m6524a(bArr4, (long) i5, (byte) (i | 128));
                int i6 = i >>> 7;
                if ((i6 & -128) == 0) {
                    byte[] bArr5 = this.f4499d;
                    int i7 = this.f4501f;
                    this.f4501f = i7 + 1;
                    C2566j7.m6524a(bArr5, (long) i7, (byte) i6);
                    return;
                }
                byte[] bArr6 = this.f4499d;
                int i8 = this.f4501f;
                this.f4501f = i8 + 1;
                C2566j7.m6524a(bArr6, (long) i8, (byte) (i6 | 128));
                int i9 = i6 >>> 7;
                if ((i9 & -128) == 0) {
                    byte[] bArr7 = this.f4499d;
                    int i10 = this.f4501f;
                    this.f4501f = i10 + 1;
                    C2566j7.m6524a(bArr7, (long) i10, (byte) i9);
                    return;
                }
                byte[] bArr8 = this.f4499d;
                int i11 = this.f4501f;
                this.f4501f = i11 + 1;
                C2566j7.m6524a(bArr8, (long) i11, (byte) (i9 | 128));
                int i12 = i9 >>> 7;
                if ((i12 & -128) == 0) {
                    byte[] bArr9 = this.f4499d;
                    int i13 = this.f4501f;
                    this.f4501f = i13 + 1;
                    C2566j7.m6524a(bArr9, (long) i13, (byte) i12);
                    return;
                }
                byte[] bArr10 = this.f4499d;
                int i14 = this.f4501f;
                this.f4501f = i14 + 1;
                C2566j7.m6524a(bArr10, (long) i14, (byte) (i12 | 128));
                byte[] bArr11 = this.f4499d;
                int i15 = this.f4501f;
                this.f4501f = i15 + 1;
                C2566j7.m6524a(bArr11, (long) i15, (byte) (i12 >>> 7));
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void
         arg types: [com.google.android.gms.internal.measurement.u5, com.google.android.gms.internal.measurement.v3]
         candidates:
          com.google.android.gms.internal.measurement.l6.a(java.lang.Object, java.lang.Object):boolean
          com.google.android.gms.internal.measurement.l6.a(java.lang.Object, com.google.android.gms.internal.measurement.w7):void */
        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final void mo17894a(int i, C2739u5 u5Var, C2603l6 l6Var) {
            mo17890a(i, 2);
            C2766w2 w2Var = (C2766w2) u5Var;
            int g = w2Var.mo17665g();
            if (g == -1) {
                g = l6Var.mo17284d(w2Var);
                w2Var.mo17659a(g);
            }
            mo17901b(g);
            l6Var.mo17278a((Object) u5Var, (C2771w7) super.f4498a);
        }

        /* renamed from: a */
        public final void mo17893a(int i, C2739u5 u5Var) {
            mo17890a(1, 3);
            mo17907c(2, i);
            mo17890a(3, 2);
            mo17913a(u5Var);
            mo17890a(1, 4);
        }

        /* renamed from: a */
        public final void mo17913a(C2739u5 u5Var) {
            mo17901b(u5Var.mo17663e());
            u5Var.mo17660a(super);
        }

        /* renamed from: a */
        public final void mo17884a(byte b) {
            try {
                byte[] bArr = this.f4499d;
                int i = this.f4501f;
                this.f4501f = i + 1;
                bArr[i] = b;
            } catch (IndexOutOfBoundsException e) {
                throw new C2722b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f4501f), Integer.valueOf(this.f4500e), 1), e);
            }
        }

        /* renamed from: b */
        private final void m7287b(byte[] bArr, int i, int i2) {
            try {
                System.arraycopy(bArr, i, this.f4499d, this.f4501f, i2);
                this.f4501f += i2;
            } catch (IndexOutOfBoundsException e) {
                throw new C2722b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f4501f), Integer.valueOf(this.f4500e), Integer.valueOf(i2)), e);
            }
        }

        /* renamed from: a */
        public final void mo17887a(int i) {
            if (i >= 0) {
                mo17901b(i);
            } else {
                mo17897a((long) i);
            }
        }

        /* renamed from: a */
        public final void mo17897a(long j) {
            if (!C2720t3.f4497c || mo17883a() < 10) {
                while ((j & -128) != 0) {
                    byte[] bArr = this.f4499d;
                    int i = this.f4501f;
                    this.f4501f = i + 1;
                    bArr[i] = (byte) ((((int) j) & 127) | 128);
                    j >>>= 7;
                }
                try {
                    byte[] bArr2 = this.f4499d;
                    int i2 = this.f4501f;
                    this.f4501f = i2 + 1;
                    bArr2[i2] = (byte) ((int) j);
                } catch (IndexOutOfBoundsException e) {
                    throw new C2722b(String.format("Pos: %d, limit: %d, len: %d", Integer.valueOf(this.f4501f), Integer.valueOf(this.f4500e), 1), e);
                }
            } else {
                while ((j & -128) != 0) {
                    byte[] bArr3 = this.f4499d;
                    int i3 = this.f4501f;
                    this.f4501f = i3 + 1;
                    C2566j7.m6524a(bArr3, (long) i3, (byte) ((((int) j) & 127) | 128));
                    j >>>= 7;
                }
                byte[] bArr4 = this.f4499d;
                int i4 = this.f4501f;
                this.f4501f = i4 + 1;
                C2566j7.m6524a(bArr4, (long) i4, (byte) ((int) j));
            }
        }

        /* renamed from: b */
        public final void mo17915b(String str) {
            int i = this.f4501f;
            try {
                int g = C2720t3.m7241g(str.length() * 3);
                int g2 = C2720t3.m7241g(str.length());
                if (g2 == g) {
                    this.f4501f = i + g2;
                    int a = C2604l7.m6694a(str, this.f4499d, this.f4501f, mo17883a());
                    this.f4501f = i;
                    mo17901b((a - i) - g2);
                    this.f4501f = a;
                    return;
                }
                mo17901b(C2604l7.m6693a(str));
                this.f4501f = C2604l7.m6694a(str, this.f4499d, this.f4501f, mo17883a());
            } catch (C2664p7 e) {
                this.f4501f = i;
                mo17898a(str, e);
            } catch (IndexOutOfBoundsException e2) {
                throw new C2722b(e2);
            }
        }

        /* renamed from: a */
        public final void mo17368a(byte[] bArr, int i, int i2) {
            m7287b(bArr, i, i2);
        }

        /* renamed from: a */
        public final int mo17883a() {
            return this.f4500e - this.f4501f;
        }
    }

    /* renamed from: com.google.android.gms.internal.measurement.t3$b */
    /* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
    public static class C2722b extends IOException {
        C2722b(Throwable th) {
            super("CodedOutputStream was writing to a flat byte array and ran out of space.", th);
        }

        /* JADX WARNING: Illegal instructions before constructor call */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        C2722b(java.lang.String r3, java.lang.Throwable r4) {
            /*
                r2 = this;
                java.lang.String r3 = java.lang.String.valueOf(r3)
                int r0 = r3.length()
                java.lang.String r1 = "CodedOutputStream was writing to a flat byte array and ran out of space.: "
                if (r0 == 0) goto L_0x0011
                java.lang.String r3 = r1.concat(r3)
                goto L_0x0016
            L_0x0011:
                java.lang.String r3 = new java.lang.String
                r3.<init>(r1)
            L_0x0016:
                r2.<init>(r3, r4)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.internal.measurement.C2720t3.C2722b.<init>(java.lang.String, java.lang.Throwable):void");
        }
    }

    private C2720t3() {
    }

    /* renamed from: a */
    public static C2720t3 m7219a(byte[] bArr) {
        return new C2721a(bArr, 0, bArr.length);
    }

    /* renamed from: b */
    public static int m7220b(double d) {
        return 8;
    }

    /* renamed from: b */
    public static int m7221b(float f) {
        return 4;
    }

    /* renamed from: b */
    public static int m7227b(boolean z) {
        return 1;
    }

    /* renamed from: e */
    public static int m7235e(int i, long j) {
        return m7234e(i) + m7236e(j);
    }

    /* renamed from: e */
    public static int m7236e(long j) {
        int i;
        if ((-128 & j) == 0) {
            return 1;
        }
        if (j < 0) {
            return 10;
        }
        if ((-34359738368L & j) != 0) {
            i = 6;
            j >>>= 28;
        } else {
            i = 2;
        }
        if ((-2097152 & j) != 0) {
            i += 2;
            j >>>= 14;
        }
        return (j & -16384) != 0 ? i + 1 : i;
    }

    /* renamed from: f */
    public static int m7238f(int i, int i2) {
        return m7234e(i) + m7237f(i2);
    }

    /* renamed from: g */
    public static int m7241g(int i) {
        if ((i & -128) == 0) {
            return 1;
        }
        if ((i & -16384) == 0) {
            return 2;
        }
        if ((-2097152 & i) == 0) {
            return 3;
        }
        return (i & -268435456) == 0 ? 4 : 5;
    }

    /* renamed from: g */
    public static int m7242g(int i, int i2) {
        return m7234e(i) + m7241g(i2);
    }

    /* renamed from: g */
    public static int m7244g(long j) {
        return 8;
    }

    /* renamed from: h */
    public static int m7246h(int i, int i2) {
        return m7234e(i) + m7241g(m7256l(i2));
    }

    /* renamed from: h */
    public static int m7248h(long j) {
        return 8;
    }

    /* renamed from: i */
    public static int m7249i(int i) {
        return 4;
    }

    /* renamed from: i */
    public static int m7250i(int i, int i2) {
        return m7234e(i) + 4;
    }

    /* renamed from: i */
    private static long m7251i(long j) {
        return (j >> 63) ^ (j << 1);
    }

    /* renamed from: j */
    public static int m7252j(int i) {
        return 4;
    }

    /* renamed from: j */
    public static int m7253j(int i, int i2) {
        return m7234e(i) + 4;
    }

    /* renamed from: k */
    public static int m7255k(int i, int i2) {
        return m7234e(i) + m7237f(i2);
    }

    /* renamed from: l */
    private static int m7256l(int i) {
        return (i >> 31) ^ (i << 1);
    }

    /* renamed from: a */
    public abstract int mo17883a();

    /* renamed from: a */
    public abstract void mo17884a(byte b);

    /* renamed from: a */
    public abstract void mo17887a(int i);

    /* renamed from: a */
    public abstract void mo17890a(int i, int i2);

    /* renamed from: a */
    public abstract void mo17891a(int i, long j);

    /* renamed from: a */
    public abstract void mo17892a(int i, C2498f3 f3Var);

    /* renamed from: a */
    public abstract void mo17893a(int i, C2739u5 u5Var);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo17894a(int i, C2739u5 u5Var, C2603l6 l6Var);

    /* renamed from: a */
    public abstract void mo17895a(int i, String str);

    /* renamed from: a */
    public abstract void mo17896a(int i, boolean z);

    /* renamed from: a */
    public abstract void mo17897a(long j);

    /* renamed from: b */
    public abstract void mo17901b(int i);

    /* renamed from: b */
    public abstract void mo17902b(int i, int i2);

    /* renamed from: b */
    public final void mo17903b(int i, long j) {
        mo17891a(i, m7251i(j));
    }

    /* renamed from: b */
    public abstract void mo17904b(int i, C2498f3 f3Var);

    /* renamed from: c */
    public final void mo17906c(int i) {
        mo17901b(m7256l(i));
    }

    /* renamed from: c */
    public abstract void mo17907c(int i, int i2);

    /* renamed from: c */
    public abstract void mo17908c(int i, long j);

    /* renamed from: c */
    public abstract void mo17909c(long j);

    /* renamed from: d */
    public abstract void mo17910d(int i);

    /* renamed from: d */
    public final void mo17911d(int i, int i2) {
        mo17907c(i, m7256l(i2));
    }

    /* renamed from: e */
    public abstract void mo17912e(int i, int i2);

    /* renamed from: c */
    public static int m7228c(int i, C2498f3 f3Var) {
        int e = m7234e(i);
        int a = f3Var.mo17469a();
        return e + m7241g(a) + a;
    }

    /* renamed from: d */
    public static int m7231d(int i, long j) {
        return m7234e(i) + m7236e(j);
    }

    /* renamed from: e */
    public static int m7234e(int i) {
        return m7241g(i << 3);
    }

    /* renamed from: f */
    public static int m7239f(int i, long j) {
        return m7234e(i) + m7236e(m7251i(j));
    }

    /* renamed from: g */
    public static int m7243g(int i, long j) {
        return m7234e(i) + 8;
    }

    /* renamed from: b */
    public final void mo17905b(long j) {
        mo17897a(m7251i(j));
    }

    /* renamed from: b */
    public static int m7223b(int i, float f) {
        return m7234e(i) + 4;
    }

    /* renamed from: h */
    public static int m7247h(int i, long j) {
        return m7234e(i) + 8;
    }

    /* renamed from: k */
    public static int m7254k(int i) {
        return m7237f(i);
    }

    /* renamed from: a */
    public final void mo17889a(int i, float f) {
        mo17912e(i, Float.floatToRawIntBits(f));
    }

    /* renamed from: b */
    public static int m7222b(int i, double d) {
        return m7234e(i) + 8;
    }

    /* renamed from: d */
    public static int m7232d(int i, C2498f3 f3Var) {
        return (m7234e(1) << 1) + m7242g(2, i) + m7228c(3, f3Var);
    }

    /* renamed from: f */
    public static int m7237f(int i) {
        if (i >= 0) {
            return m7241g(i);
        }
        return 10;
    }

    /* renamed from: h */
    public static int m7245h(int i) {
        return m7241g(m7256l(i));
    }

    /* renamed from: a */
    public final void mo17888a(int i, double d) {
        mo17908c(i, Double.doubleToRawLongBits(d));
    }

    /* renamed from: b */
    public static int m7226b(int i, boolean z) {
        return m7234e(i) + 1;
    }

    @Deprecated
    /* renamed from: c */
    static int m7229c(int i, C2739u5 u5Var, C2603l6 l6Var) {
        int e = m7234e(i) << 1;
        C2766w2 w2Var = (C2766w2) u5Var;
        int g = w2Var.mo17665g();
        if (g == -1) {
            g = l6Var.mo17284d(w2Var);
            w2Var.mo17659a(g);
        }
        return e + g;
    }

    /* renamed from: f */
    public static int m7240f(long j) {
        return m7236e(m7251i(j));
    }

    /* renamed from: a */
    public final void mo17886a(float f) {
        mo17910d(Float.floatToRawIntBits(f));
    }

    /* renamed from: b */
    public static int m7225b(int i, String str) {
        return m7234e(i) + m7218a(str);
    }

    /* renamed from: a */
    public final void mo17885a(double d) {
        mo17909c(Double.doubleToRawLongBits(d));
    }

    /* renamed from: b */
    static int m7224b(int i, C2739u5 u5Var, C2603l6 l6Var) {
        return m7234e(i) + m7217a(u5Var, l6Var);
    }

    /* renamed from: d */
    public static int m7233d(long j) {
        return m7236e(j);
    }

    /* renamed from: a */
    public final void mo17899a(boolean z) {
        mo17884a(z ? (byte) 1 : 0);
    }

    /* renamed from: a */
    public static int m7214a(int i, C2451c5 c5Var) {
        int e = m7234e(i);
        int a = c5Var.mo17369a();
        return e + m7241g(a) + a;
    }

    /* renamed from: b */
    public final void mo17900b() {
        if (mo17883a() != 0) {
            throw new IllegalStateException("Did not write as much data as expected.");
        }
    }

    /* renamed from: a */
    public static int m7218a(String str) {
        int i;
        try {
            i = C2604l7.m6693a(str);
        } catch (C2664p7 unused) {
            i = str.getBytes(C2647o4.f4367a).length;
        }
        return m7241g(i) + i;
    }

    /* renamed from: a */
    public static int m7215a(C2451c5 c5Var) {
        int a = c5Var.mo17369a();
        return m7241g(a) + a;
    }

    /* renamed from: a */
    public static int m7216a(C2498f3 f3Var) {
        int a = f3Var.mo17469a();
        return m7241g(a) + a;
    }

    /* renamed from: a */
    static int m7217a(C2739u5 u5Var, C2603l6 l6Var) {
        C2766w2 w2Var = (C2766w2) u5Var;
        int g = w2Var.mo17665g();
        if (g == -1) {
            g = l6Var.mo17284d(w2Var);
            w2Var.mo17659a(g);
        }
        return m7241g(g) + g;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, com.google.android.gms.internal.measurement.p7]
     candidates:
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.logp(java.util.logging.Level, java.lang.String, java.lang.String, java.lang.String, java.lang.Throwable):void} */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final void mo17898a(String str, C2664p7 p7Var) {
        f4496b.logp(Level.WARNING, "com.google.protobuf.CodedOutputStream", "inefficientWriteStringNoTag", "Converting ill-formed UTF-16. Your Protocol Buffer will not round trip correctly!", (Throwable) p7Var);
        byte[] bytes = str.getBytes(C2647o4.f4367a);
        try {
            mo17901b(bytes.length);
            mo17368a(bytes, 0, bytes.length);
        } catch (IndexOutOfBoundsException e) {
            throw new C2722b(e);
        } catch (C2722b e2) {
            throw e2;
        }
    }
}
