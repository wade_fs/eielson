package com.google.android.gms.internal.measurement;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.google.android.gms.internal.measurement.o6 */
/* compiled from: com.google.android.gms:play-services-measurement-base@@17.2.2 */
final class C2649o6 implements Iterator<Map.Entry<K, V>> {

    /* renamed from: P */
    private int f4371P;

    /* renamed from: Q */
    private Iterator<Map.Entry<K, V>> f4372Q;

    /* renamed from: R */
    private final /* synthetic */ C2618m6 f4373R;

    private C2649o6(C2618m6 m6Var) {
        this.f4373R = m6Var;
        this.f4371P = this.f4373R.f4327Q.size();
    }

    /* renamed from: a */
    private final Iterator<Map.Entry<K, V>> m6974a() {
        if (this.f4372Q == null) {
            this.f4372Q = this.f4373R.f4331U.entrySet().iterator();
        }
        return this.f4372Q;
    }

    public final boolean hasNext() {
        int i = this.f4371P;
        return (i > 0 && i <= this.f4373R.f4327Q.size()) || m6974a().hasNext();
    }

    public final /* synthetic */ Object next() {
        if (m6974a().hasNext()) {
            return (Map.Entry) m6974a().next();
        }
        List b = this.f4373R.f4327Q;
        int i = this.f4371P - 1;
        this.f4371P = i;
        return (Map.Entry) b.get(i);
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    /* synthetic */ C2649o6(C2618m6 m6Var, C2663p6 p6Var) {
        this(m6Var);
    }
}
