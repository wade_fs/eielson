package com.google.android.exoplayer2.source;

import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;

/* renamed from: com.google.android.exoplayer2.source.a */
/* compiled from: lambda */
public final /* synthetic */ class C1858a implements MediaSource.SourceInfoRefreshListener {

    /* renamed from: P */
    private final /* synthetic */ CompositeMediaSource f2855P;

    /* renamed from: Q */
    private final /* synthetic */ Object f2856Q;

    public /* synthetic */ C1858a(CompositeMediaSource compositeMediaSource, Object obj) {
        this.f2855P = compositeMediaSource;
        this.f2856Q = obj;
    }

    public final void onSourceInfoRefreshed(MediaSource mediaSource, Timeline timeline, Object obj) {
        this.f2855P.mo14853a(this.f2856Q, mediaSource, timeline, obj);
    }
}
