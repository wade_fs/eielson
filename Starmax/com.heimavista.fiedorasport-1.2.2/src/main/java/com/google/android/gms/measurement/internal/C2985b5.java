package com.google.android.gms.measurement.internal;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.internal.measurement.zzv;

/* renamed from: com.google.android.gms.measurement.internal.b5 */
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.2.2 */
final class C2985b5 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C3081j5 f4977P;

    /* renamed from: Q */
    private final /* synthetic */ long f4978Q;

    /* renamed from: R */
    private final /* synthetic */ Bundle f4979R;

    /* renamed from: S */
    private final /* synthetic */ Context f4980S;

    /* renamed from: T */
    private final /* synthetic */ C3032f4 f4981T;

    /* renamed from: U */
    private final /* synthetic */ BroadcastReceiver.PendingResult f4982U;

    C2985b5(C3262z4 z4Var, C3081j5 j5Var, long j, Bundle bundle, Context context, C3032f4 f4Var, BroadcastReceiver.PendingResult pendingResult) {
        this.f4977P = j5Var;
        this.f4978Q = j;
        this.f4979R = bundle;
        this.f4980S = context;
        this.f4981T = f4Var;
        this.f4982U = pendingResult;
    }

    public final void run() {
        long a = this.f4977P.mo19098p().f5612j.mo19326a();
        long j = this.f4978Q;
        if (a > 0 && (j >= a || j <= 0)) {
            j = a - 1;
        }
        if (j > 0) {
            this.f4979R.putLong("click_timestamp", j);
        }
        this.f4979R.putString("_cis", "referrer broadcast");
        C3081j5.m8751a(this.f4980S, (zzv) null).mo19103v().mo19267a("auto", "_cmp", this.f4979R);
        this.f4981T.mo18996B().mo19042a("Install campaign recorded");
        BroadcastReceiver.PendingResult pendingResult = this.f4982U;
        if (pendingResult != null) {
            pendingResult.finish();
        }
    }
}
