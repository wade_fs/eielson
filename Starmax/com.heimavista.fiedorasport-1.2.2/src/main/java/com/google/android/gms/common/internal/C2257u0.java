package com.google.android.gms.common.internal;

import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: com.google.android.gms.common.internal.u0 */
public interface C2257u0 extends IInterface {
    /* renamed from: e */
    C3988b mo17042e();

    /* renamed from: f */
    int mo17043f();
}
