package com.google.android.gms.common.server.response;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.internal.safeparcel.AbstractSafeParcelable;
import com.google.android.gms.common.internal.safeparcel.C2250b;
import com.google.android.gms.common.server.response.FastJsonResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public final class zak extends AbstractSafeParcelable {
    public static final Parcelable.Creator<zak> CREATOR = new C2300c();

    /* renamed from: P */
    private final int f3817P;

    /* renamed from: Q */
    private final HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> f3818Q;

    /* renamed from: R */
    private final String f3819R;

    zak(int i, ArrayList<zal> arrayList, String str) {
        this.f3817P = i;
        HashMap<String, Map<String, FastJsonResponse.Field<?, ?>>> hashMap = new HashMap<>();
        int size = arrayList.size();
        for (int i2 = 0; i2 < size; i2++) {
            zal zal = arrayList.get(i2);
            String str2 = zal.f3821Q;
            HashMap hashMap2 = new HashMap();
            int size2 = zal.f3822R.size();
            for (int i3 = 0; i3 < size2; i3++) {
                zam zam = zal.f3822R.get(i3);
                hashMap2.put(zam.f3824Q, zam.f3825R);
            }
            hashMap.put(str2, hashMap2);
        }
        this.f3818Q = hashMap;
        C2258v.m5629a((Object) str);
        this.f3819R = str;
        mo17111c();
    }

    /* renamed from: b */
    public final Map<String, FastJsonResponse.Field<?, ?>> mo17110b(String str) {
        return this.f3818Q.get(str);
    }

    /* renamed from: c */
    public final void mo17111c() {
        for (String str : this.f3818Q.keySet()) {
            Map map = this.f3818Q.get(str);
            for (String str2 : map.keySet()) {
                ((FastJsonResponse.Field) map.get(str2)).mo17090a(this);
            }
        }
    }

    /* renamed from: d */
    public final String mo17112d() {
        return this.f3819R;
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (String str : this.f3818Q.keySet()) {
            sb.append(str);
            sb.append(":\n");
            Map map = this.f3818Q.get(str);
            for (String str2 : map.keySet()) {
                sb.append("  ");
                sb.append(str2);
                sb.append(": ");
                sb.append(map.get(str2));
            }
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, android.os.Parcel, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Boolean, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Double, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Float, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Integer, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.Long, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.util.List, boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, byte[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String[], boolean):void
      com.google.android.gms.common.internal.safeparcel.b.a(android.os.Parcel, int, java.lang.String, boolean):void */
    public final void writeToParcel(Parcel parcel, int i) {
        int a = C2250b.m5586a(parcel);
        C2250b.m5591a(parcel, 1, this.f3817P);
        ArrayList arrayList = new ArrayList();
        for (String str : this.f3818Q.keySet()) {
            arrayList.add(new zal(str, this.f3818Q.get(str)));
        }
        C2250b.m5614c(parcel, 2, arrayList, false);
        C2250b.m5602a(parcel, 3, this.f3819R, false);
        C2250b.m5587a(parcel, a);
    }
}
