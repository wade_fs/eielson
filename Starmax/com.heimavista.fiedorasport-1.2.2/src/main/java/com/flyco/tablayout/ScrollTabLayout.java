package com.flyco.tablayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.LayoutRes;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.flyco.tablayout.utils.UnreadMsgUtils;
import com.flyco.tablayout.widget.MsgView;
import java.util.ArrayList;

public class ScrollTabLayout extends HorizontalScrollView {
    private static final int TEXT_BOLD_BOTH = 2;
    private static final int TEXT_BOLD_NONE = 0;
    private static final int TEXT_BOLD_WHEN_SELECT = 1;
    private Context mContext;
    /* access modifiers changed from: private */
    public int mCurrentTab;
    private int mDividerColor;
    private float mDividerPadding;
    private Paint mDividerPaint;
    private float mDividerWidth;
    private int mHeight;
    private Drawable mIndicatorDrawable;
    private int mIndicatorHeight;
    private int mIndicatorWidth;
    private SparseArray<Boolean> mInitSetMap;
    private int mLastScrollX;
    @LayoutRes
    private int mLayout;
    /* access modifiers changed from: private */
    public OnTabSelectListener mListener;
    private Paint mRectPaint;
    private int mTabCount;
    private float mTabPadding;
    private boolean mTabSpaceEqual;
    private float mTabWidth;
    /* access modifiers changed from: private */
    public LinearLayout mTabsContainer;
    private boolean mTextAllCaps;
    private int mTextBold;
    private Paint mTextPaint;
    private int mTextSelectColor;
    private float mTextSelectSize;
    private float mTextSize;
    private int mTextUnSelectColor;
    private ArrayList<String> mTitles;
    private int mUnderlineColor;
    private int mUnderlineGravity;
    private float mUnderlineHeight;
    private float margin;

    public ScrollTabLayout(Context context) {
        this(context, null, 0);
    }

    private void addTab(int i, String str, View view) {
        TextView textView = (TextView) view.findViewById(C1632R.C1635id.tv_tab_title);
        View findViewById = view.findViewById(C1632R.C1635id.indicator);
        Drawable drawable = this.mIndicatorDrawable;
        if (drawable != null) {
            findViewById.setBackground(drawable);
        }
        if (!(this.mIndicatorWidth == 0 && this.mIndicatorHeight == 0)) {
            ViewGroup.LayoutParams layoutParams = findViewById.getLayoutParams();
            layoutParams.width = this.mIndicatorWidth;
            layoutParams.height = this.mIndicatorHeight;
            findViewById.setLayoutParams(layoutParams);
        }
        if (!(textView == null || str == null)) {
            textView.setText(str);
        }
        view.setOnClickListener(new View.OnClickListener() {
            /* class com.flyco.tablayout.ScrollTabLayout.C16391 */

            public void onClick(View view) {
                int indexOfChild = ScrollTabLayout.this.mTabsContainer.indexOfChild(view);
                if (indexOfChild != -1) {
                    ScrollTabLayout.this.updateTabSelection(indexOfChild);
                    int unused = ScrollTabLayout.this.mCurrentTab = indexOfChild;
                    ScrollTabLayout.this.scrollToCurrentTab();
                    if (ScrollTabLayout.this.mListener != null) {
                        ScrollTabLayout.this.mListener.onTabSelect(indexOfChild);
                    }
                } else if (ScrollTabLayout.this.mListener != null) {
                    ScrollTabLayout.this.mListener.onTabReselect(indexOfChild);
                }
            }
        });
        LinearLayout.LayoutParams layoutParams2 = this.mTabSpaceEqual ? new LinearLayout.LayoutParams(0, -1, 1.0f) : new LinearLayout.LayoutParams(-2, -1);
        float f = this.mTabWidth;
        if (f > 0.0f) {
            layoutParams2 = new LinearLayout.LayoutParams((int) f, -1);
        }
        this.mTabsContainer.addView(view, i, layoutParams2);
    }

    private void obtainAttributes(Context context, AttributeSet attributeSet) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C1632R.styleable.ScrollTabLayout);
        this.mUnderlineColor = obtainStyledAttributes.getColor(C1632R.styleable.ScrollTabLayout_tl_underline_color, Color.parseColor("#ffffff"));
        this.mUnderlineHeight = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_underline_height, (float) dp2px(0.0f));
        this.mUnderlineGravity = obtainStyledAttributes.getInt(C1632R.styleable.ScrollTabLayout_tl_underline_gravity, 80);
        this.mDividerColor = obtainStyledAttributes.getColor(C1632R.styleable.ScrollTabLayout_tl_divider_color, Color.parseColor("#ffffff"));
        this.mDividerWidth = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_divider_width, (float) dp2px(0.0f));
        this.mDividerPadding = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_divider_padding, (float) dp2px(12.0f));
        this.mTextSize = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_textsize, (float) sp2px(14.0f));
        this.mTextSelectSize = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_textSelectSize, (float) sp2px(14.0f));
        this.mTextSelectColor = obtainStyledAttributes.getColor(C1632R.styleable.ScrollTabLayout_tl_textSelectColor, Color.parseColor("#ffffff"));
        this.mTextUnSelectColor = obtainStyledAttributes.getColor(C1632R.styleable.ScrollTabLayout_tl_textUnselectColor, Color.parseColor("#AAffffff"));
        this.mTextBold = obtainStyledAttributes.getInt(C1632R.styleable.ScrollTabLayout_tl_textBold, 0);
        this.mTextAllCaps = obtainStyledAttributes.getBoolean(C1632R.styleable.ScrollTabLayout_tl_textAllCaps, false);
        this.mTabSpaceEqual = obtainStyledAttributes.getBoolean(C1632R.styleable.ScrollTabLayout_tl_tab_space_equal, false);
        this.mTabWidth = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_tab_width, (float) dp2px(-1.0f));
        this.mTabPadding = obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_tab_padding, (float) ((this.mTabSpaceEqual || this.mTabWidth > 0.0f) ? dp2px(0.0f) : dp2px(20.0f)));
        this.mIndicatorDrawable = obtainStyledAttributes.getDrawable(C1632R.styleable.ScrollTabLayout_tl_indicator_drawable);
        this.mIndicatorHeight = (int) obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_indicator_height, (float) dp2px(3.0f));
        this.mIndicatorWidth = (int) obtainStyledAttributes.getDimension(C1632R.styleable.ScrollTabLayout_tl_indicator_width, (float) dp2px(10.0f));
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: private */
    public void scrollToCurrentTab() {
        if (this.mTabCount > 0) {
            int left = this.mTabsContainer.getChildAt(this.mCurrentTab).getLeft();
            if (this.mCurrentTab > 0) {
                left -= (getWidth() / 2) - getPaddingLeft();
            }
            if (left != this.mLastScrollX) {
                this.mLastScrollX = left;
                scrollTo(left, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateTabSelection(int i) {
        int i2 = 0;
        while (i2 < this.mTabCount) {
            View childAt = this.mTabsContainer.getChildAt(i2);
            boolean z = i2 == i;
            TextView textView = (TextView) childAt.findViewById(C1632R.C1635id.tv_tab_title);
            View findViewById = childAt.findViewById(C1632R.C1635id.indicator);
            if (textView != null) {
                textView.setTextColor(z ? this.mTextSelectColor : this.mTextUnSelectColor);
                textView.setTextSize(0, z ? this.mTextSelectSize : this.mTextSize);
                findViewById.setVisibility(z ? 0 : 8);
                if (this.mTextBold == 1) {
                    textView.getPaint().setFakeBoldText(z);
                }
            }
            i2++;
        }
    }

    private void updateTabStyles() {
        int i = 0;
        while (i < this.mTabCount) {
            View childAt = this.mTabsContainer.getChildAt(i);
            TextView textView = (TextView) childAt.findViewById(C1632R.C1635id.tv_tab_title);
            View findViewById = childAt.findViewById(C1632R.C1635id.indicator);
            if (textView != null) {
                findViewById.setVisibility(i == this.mCurrentTab ? 0 : 8);
                textView.setTextColor(i == this.mCurrentTab ? this.mTextSelectColor : this.mTextUnSelectColor);
                textView.setTextSize(0, i == this.mCurrentTab ? this.mTextSelectSize : this.mTextSize);
                float f = this.mTabPadding;
                textView.setPadding((int) f, 0, (int) f, 0);
                if (this.mTextAllCaps) {
                    textView.setText(textView.getText().toString().toUpperCase());
                }
                int i2 = this.mTextBold;
                if (i2 == 2) {
                    textView.getPaint().setFakeBoldText(true);
                } else if (i2 == 0) {
                    textView.getPaint().setFakeBoldText(false);
                }
            }
            i++;
        }
    }

    public void addNewTab(String str) {
        View inflate = View.inflate(this.mContext, this.mLayout, null);
        ArrayList<String> arrayList = this.mTitles;
        if (arrayList != null) {
            arrayList.add(str);
        }
        ArrayList<String> arrayList2 = this.mTitles;
        if (arrayList2 != null) {
            addTab(this.mTabCount, arrayList2.get(this.mTabCount).toString(), inflate);
            this.mTabCount = this.mTitles.size();
            updateTabStyles();
        }
    }

    /* access modifiers changed from: protected */
    public int dp2px(float f) {
        return (int) ((f * this.mContext.getResources().getDisplayMetrics().density) + 0.5f);
    }

    public int getCurrentTab() {
        return this.mCurrentTab;
    }

    public int getDividerColor() {
        return this.mDividerColor;
    }

    public float getDividerPadding() {
        return this.mDividerPadding;
    }

    public float getDividerWidth() {
        return this.mDividerWidth;
    }

    public MsgView getMsgView(int i) {
        int i2 = this.mTabCount;
        if (i >= i2) {
            i = i2 - 1;
        }
        return (MsgView) this.mTabsContainer.getChildAt(i).findViewById(C1632R.C1635id.rtv_msg_tip);
    }

    public int getTabCount() {
        return this.mTabCount;
    }

    public float getTabPadding() {
        return this.mTabPadding;
    }

    public float getTabWidth() {
        return this.mTabWidth;
    }

    public int getTextBold() {
        return this.mTextBold;
    }

    public int getTextSelectColor() {
        return this.mTextSelectColor;
    }

    public float getTextSelectSize() {
        return this.mTextSelectSize;
    }

    public float getTextSize() {
        return this.mTextSize;
    }

    public int getTextUnSelectColor() {
        return this.mTextUnSelectColor;
    }

    public TextView getTitleView(int i) {
        return (TextView) this.mTabsContainer.getChildAt(i).findViewById(C1632R.C1635id.tv_tab_title);
    }

    public int getUnderlineColor() {
        return this.mUnderlineColor;
    }

    public float getUnderlineHeight() {
        return this.mUnderlineHeight;
    }

    public void hideMsg(int i) {
        int i2 = this.mTabCount;
        if (i >= i2) {
            i = i2 - 1;
        }
        MsgView msgView = (MsgView) this.mTabsContainer.getChildAt(i).findViewById(C1632R.C1635id.rtv_msg_tip);
        if (msgView != null) {
            msgView.setVisibility(8);
        }
    }

    public boolean isTabSpaceEqual() {
        return this.mTabSpaceEqual;
    }

    public boolean isTextAllCaps() {
        return this.mTextAllCaps;
    }

    public void notifyDataSetChanged() {
        this.mTabsContainer.removeAllViews();
        this.mTabCount = this.mTitles.size();
        for (int i = 0; i < this.mTabCount; i++) {
            addTab(i, this.mTitles.get(i).toString(), View.inflate(this.mContext, this.mLayout, null));
        }
        updateTabStyles();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode() && this.mTabCount > 0) {
            int height = getHeight();
            int paddingLeft = getPaddingLeft();
            float f = this.mDividerWidth;
            if (f > 0.0f) {
                this.mDividerPaint.setStrokeWidth(f);
                this.mDividerPaint.setColor(this.mDividerColor);
                for (int i = 0; i < this.mTabCount - 1; i++) {
                    View childAt = this.mTabsContainer.getChildAt(i);
                    canvas.drawLine((float) (childAt.getRight() + paddingLeft), this.mDividerPadding, (float) (childAt.getRight() + paddingLeft), ((float) height) - this.mDividerPadding, this.mDividerPaint);
                }
            }
            if (this.mUnderlineHeight > 0.0f) {
                this.mRectPaint.setColor(this.mUnderlineColor);
                if (this.mUnderlineGravity == 80) {
                    float f2 = (float) height;
                    canvas.drawRect((float) paddingLeft, f2 - this.mUnderlineHeight, (float) (this.mTabsContainer.getWidth() + paddingLeft), f2, this.mRectPaint);
                    return;
                }
                canvas.drawRect((float) paddingLeft, 0.0f, (float) (this.mTabsContainer.getWidth() + paddingLeft), this.mUnderlineHeight, this.mRectPaint);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.mCurrentTab = bundle.getInt("mCurrentTab");
            parcelable = bundle.getParcelable("instanceState");
            if (this.mCurrentTab != 0 && this.mTabsContainer.getChildCount() > 0) {
                updateTabSelection(this.mCurrentTab);
                scrollToCurrentTab();
            }
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("mCurrentTab", this.mCurrentTab);
        return bundle;
    }

    public void setCurrentTab(int i) {
        this.mCurrentTab = i;
    }

    public void setDividerColor(int i) {
        this.mDividerColor = i;
        invalidate();
    }

    public void setDividerPadding(float f) {
        this.mDividerPadding = (float) dp2px(f);
        invalidate();
    }

    public void setDividerWidth(float f) {
        this.mDividerWidth = (float) dp2px(f);
        invalidate();
    }

    public void setIndicatorBackDrawable(Drawable drawable) {
        this.mIndicatorDrawable = drawable;
    }

    public void setIndicatorWidthAndHeight(int i, int i2) {
        this.mIndicatorWidth = dp2px((float) i);
        this.mIndicatorHeight = dp2px((float) i2);
    }

    public void setLayout(@LayoutRes int i) {
        this.mLayout = i;
    }

    public void setMsgMargin(int i, float f, float f2) {
        float f3;
        int i2 = this.mTabCount;
        if (i >= i2) {
            i = i2 - 1;
        }
        View childAt = this.mTabsContainer.getChildAt(i);
        MsgView msgView = (MsgView) childAt.findViewById(C1632R.C1635id.rtv_msg_tip);
        if (msgView != null) {
            this.mTextPaint.setTextSize(this.mTextSize);
            float measureText = this.mTextPaint.measureText(((TextView) childAt.findViewById(C1632R.C1635id.tv_tab_title)).getText().toString());
            float descent = this.mTextPaint.descent() - this.mTextPaint.ascent();
            ViewGroup.MarginLayoutParams marginLayoutParams = (ViewGroup.MarginLayoutParams) msgView.getLayoutParams();
            float f4 = this.mTabWidth;
            if (f4 >= 0.0f) {
                f3 = f4 / 2.0f;
                measureText /= 2.0f;
            } else {
                f3 = this.mTabPadding;
            }
            marginLayoutParams.leftMargin = (int) (f3 + measureText + ((float) dp2px(f)));
            int i3 = this.mHeight;
            marginLayoutParams.topMargin = i3 > 0 ? (((int) (((float) i3) - descent)) / 2) - dp2px(f2) : 0;
            msgView.setLayoutParams(marginLayoutParams);
        }
    }

    public void setOnTabSelectListener(OnTabSelectListener onTabSelectListener) {
        this.mListener = onTabSelectListener;
    }

    public void setTabPadding(float f) {
        this.mTabPadding = (float) dp2px(f);
        updateTabStyles();
    }

    public void setTabSpaceEqual(boolean z) {
        this.mTabSpaceEqual = z;
        updateTabStyles();
    }

    public void setTabWidth(float f) {
        this.mTabWidth = (float) dp2px(f);
        updateTabStyles();
    }

    public void setTabs(ArrayList<String> arrayList) {
        this.mTitles = arrayList;
    }

    public void setTextAllCaps(boolean z) {
        this.mTextAllCaps = z;
        updateTabStyles();
    }

    public void setTextBold(int i) {
        this.mTextBold = i;
        updateTabStyles();
    }

    public void setTextSelectColor(int i) {
        this.mTextSelectColor = i;
        updateTabStyles();
    }

    public void setTextSelectSize(float f) {
        this.mTextSelectSize = (float) sp2px(f);
        updateTabStyles();
    }

    public void setTextSize(float f) {
        this.mTextSize = (float) sp2px(f);
        updateTabStyles();
    }

    public void setTextUnSelectColor(int i) {
        this.mTextUnSelectColor = i;
        updateTabStyles();
    }

    public void setUnderlineColor(int i) {
        this.mUnderlineColor = i;
        invalidate();
    }

    public void setUnderlineGravity(int i) {
        this.mUnderlineGravity = i;
        invalidate();
    }

    public void setUnderlineHeight(float f) {
        this.mUnderlineHeight = (float) dp2px(f);
        invalidate();
    }

    public void showDot(int i) {
        int i2 = this.mTabCount;
        if (i >= i2) {
            i = i2 - 1;
        }
        showMsg(i, 0);
    }

    public void showMsg(int i, int i2) {
        int i3 = this.mTabCount;
        if (i >= i3) {
            i = i3 - 1;
        }
        MsgView msgView = (MsgView) this.mTabsContainer.getChildAt(i).findViewById(C1632R.C1635id.rtv_msg_tip);
        if (msgView != null) {
            UnreadMsgUtils.show(msgView, i2);
            if (this.mInitSetMap.get(i) == null || !this.mInitSetMap.get(i).booleanValue()) {
                setMsgMargin(i, 4.0f, 2.0f);
                this.mInitSetMap.put(i, true);
            }
        }
    }

    /* access modifiers changed from: protected */
    public int sp2px(float f) {
        return (int) ((f * this.mContext.getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }

    public ScrollTabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ScrollTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mTitles = new ArrayList<>();
        this.mRectPaint = new Paint(1);
        this.mDividerPaint = new Paint(1);
        this.mTextPaint = new Paint(1);
        this.mInitSetMap = new SparseArray<>();
        this.mLayout = C1632R.C1637layout.flyco_layout_scroll_tab;
        setFillViewport(true);
        setWillNotDraw(false);
        setClipChildren(false);
        setClipToPadding(false);
        this.mContext = context;
        this.mTabsContainer = new LinearLayout(context);
        addView(this.mTabsContainer);
        obtainAttributes(context, attributeSet);
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_height");
        if (!attributeValue.equals("-1") && !attributeValue.equals("-2")) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842997});
            this.mHeight = obtainStyledAttributes.getDimensionPixelSize(0, -2);
            obtainStyledAttributes.recycle();
        }
    }
}
