package com.flyco.tablayout;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.flyco.tablayout.listener.OnTabSelectListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class DiyTabLayout extends HorizontalScrollView {
    private static final int STYLE_BLOCK = 2;
    private static final int STYLE_NORMAL = 0;
    private static final int STYLE_TRIANGLE = 1;
    private static final int TEXT_BOLD_BOTH = 2;
    private static final int TEXT_BOLD_NONE = 0;
    private static final int TEXT_BOLD_WHEN_SELECT = 1;
    private Context mContext;
    /* access modifiers changed from: private */
    public float mCurrentPositionOffset;
    /* access modifiers changed from: private */
    public int mCurrentTab;
    private TabItemBean mDefaultTabBean;
    private int mDividerColor;
    private float mDividerPadding;
    private Paint mDividerPaint;
    private float mDividerWidth;
    private int mHeight;
    private int mIndicatorColor;
    private float mIndicatorCornerRadius;
    private GradientDrawable mIndicatorDrawable;
    private int mIndicatorGravity;
    private float mIndicatorHeight;
    private float mIndicatorMarginBottom;
    private float mIndicatorMarginLeft;
    private float mIndicatorMarginRight;
    private float mIndicatorMarginTop;
    private Rect mIndicatorRect;
    private int mIndicatorStyle;
    private float mIndicatorWidth;
    private boolean mIndicatorWidthEqualTitle;
    private int mLastScrollX;
    private OnTabSelectListener mListener;
    private ViewPager2.OnPageChangeCallback mPageChangeCallback;
    private Paint mRectPaint;
    /* access modifiers changed from: private */
    public boolean mSnapOnTabClick;
    private int mTabCount;
    private float mTabPadding;
    private Rect mTabRect;
    private boolean mTabSpaceEqual;
    private float mTabWidth;
    /* access modifiers changed from: private */
    public LinearLayout mTabsContainer;
    private boolean mTextAllCaps;
    private int mTextBold;
    private Paint mTextPaint;
    private int mTextSelectSize;
    private int mTextsize;
    private List<TabItemBean> mTitleList;
    private Paint mTrianglePaint;
    private Path mTrianglePath;
    private int mUnderlineColor;
    private int mUnderlineGravity;
    private float mUnderlineHeight;
    /* access modifiers changed from: private */
    public ViewPager2 mViewPager;
    private float margin;

    public DiyTabLayout(Context context) {
        this(context, null, 0);
    }

    private void addTab(int i, List<TabItemBean> list, View view) {
        TextView textView = (TextView) view.findViewById(C1632R.C1635id.tv_tab_title);
        if (!(textView == null || list == null)) {
            textView.setText(list.get(i).getTitle());
            if (list.size() > 0 && list.size() > i) {
                textView.setTextColor(pareColor(list.get(i).getTitleColor(), this.mDefaultTabBean.getTitleColor()));
            }
        }
        view.setOnClickListener(new View.OnClickListener() {
            /* class com.flyco.tablayout.DiyTabLayout.C16312 */

            public void onClick(View view) {
                int indexOfChild = DiyTabLayout.this.mTabsContainer.indexOfChild(view);
                if (indexOfChild != -1 && DiyTabLayout.this.mViewPager.getCurrentItem() != indexOfChild) {
                    if (DiyTabLayout.this.mSnapOnTabClick) {
                        DiyTabLayout.this.mViewPager.setCurrentItem(indexOfChild, false);
                    } else {
                        DiyTabLayout.this.mViewPager.setCurrentItem(indexOfChild, false);
                    }
                }
            }
        });
        LinearLayout.LayoutParams layoutParams = this.mTabSpaceEqual ? new LinearLayout.LayoutParams(0, -1, 1.0f) : new LinearLayout.LayoutParams(-2, -1);
        float f = this.mTabWidth;
        if (f > 0.0f) {
            layoutParams = new LinearLayout.LayoutParams((int) f, -1);
        }
        this.mTabsContainer.addView(view, i, layoutParams);
    }

    private void calcIndicatorRect() {
        View childAt = this.mTabsContainer.getChildAt(this.mCurrentTab);
        float left = (float) childAt.getLeft();
        float right = (float) childAt.getRight();
        if (this.mIndicatorStyle == 0 && this.mIndicatorWidthEqualTitle) {
            this.mTextPaint.setTextSize((float) this.mTextsize);
            this.margin = ((right - left) - this.mTextPaint.measureText(((TextView) childAt.findViewById(C1632R.C1635id.tv_tab_title)).getText().toString())) / 2.0f;
        }
        int i = this.mCurrentTab;
        if (i < this.mTabCount - 1) {
            View childAt2 = this.mTabsContainer.getChildAt(i + 1);
            float left2 = (float) childAt2.getLeft();
            float right2 = (float) childAt2.getRight();
            float f = this.mCurrentPositionOffset;
            left += (left2 - left) * f;
            right += f * (right2 - right);
            if (this.mIndicatorStyle == 0 && this.mIndicatorWidthEqualTitle) {
                this.mTextPaint.setTextSize((float) this.mTextsize);
                float f2 = this.margin;
                this.margin = f2 + (this.mCurrentPositionOffset * ((((right2 - left2) - this.mTextPaint.measureText(((TextView) childAt2.findViewById(C1632R.C1635id.tv_tab_title)).getText().toString())) / 2.0f) - f2));
            }
        }
        Rect rect = this.mIndicatorRect;
        int i2 = (int) left;
        rect.left = i2;
        int i3 = (int) right;
        rect.right = i3;
        if (this.mIndicatorStyle == 0 && this.mIndicatorWidthEqualTitle) {
            float f3 = this.margin;
            rect.left = (int) ((left + f3) - 1.0f);
            rect.right = (int) ((right - f3) - 1.0f);
        }
        Rect rect2 = this.mTabRect;
        rect2.left = i2;
        rect2.right = i3;
        if (this.mIndicatorWidth >= 0.0f) {
            float left3 = ((float) childAt.getLeft()) + ((((float) childAt.getWidth()) - this.mIndicatorWidth) / 2.0f);
            int i4 = this.mCurrentTab;
            if (i4 < this.mTabCount - 1) {
                left3 += this.mCurrentPositionOffset * ((((float) childAt.getWidth()) / 2.0f) + (((float) this.mTabsContainer.getChildAt(i4 + 1).getWidth()) / 2.0f));
            }
            Rect rect3 = this.mIndicatorRect;
            rect3.left = (int) left3;
            rect3.right = (int) (((float) rect3.left) + this.mIndicatorWidth);
        }
    }

    private void obtainAttributes(Context context, AttributeSet attributeSet) {
        float f;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, C1632R.styleable.SlidingTabLayout);
        this.mIndicatorStyle = obtainStyledAttributes.getInt(C1632R.styleable.SlidingTabLayout_tl_indicator_style, 0);
        this.mIndicatorColor = obtainStyledAttributes.getColor(C1632R.styleable.SlidingTabLayout_tl_indicator_color, Color.parseColor(this.mIndicatorStyle == 2 ? "#4B6A87" : "#ffffff"));
        int i = C1632R.styleable.SlidingTabLayout_tl_indicator_height;
        int i2 = this.mIndicatorStyle;
        if (i2 == 1) {
            f = 4.0f;
        } else {
            f = (float) (i2 == 2 ? -1 : 2);
        }
        this.mIndicatorHeight = obtainStyledAttributes.getDimension(i, (float) dp2px(f));
        this.mIndicatorWidth = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_indicator_width, (float) dp2px(this.mIndicatorStyle == 1 ? 10.0f : -1.0f));
        this.mIndicatorCornerRadius = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_indicator_corner_radius, (float) dp2px(this.mIndicatorStyle == 2 ? -1.0f : 0.0f));
        this.mIndicatorMarginLeft = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_indicator_margin_left, (float) dp2px(0.0f));
        float f2 = 7.0f;
        this.mIndicatorMarginTop = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_indicator_margin_top, (float) dp2px(this.mIndicatorStyle == 2 ? 7.0f : 0.0f));
        this.mIndicatorMarginRight = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_indicator_margin_right, (float) dp2px(0.0f));
        int i3 = C1632R.styleable.SlidingTabLayout_tl_indicator_margin_bottom;
        if (this.mIndicatorStyle != 2) {
            f2 = 0.0f;
        }
        this.mIndicatorMarginBottom = obtainStyledAttributes.getDimension(i3, (float) dp2px(f2));
        this.mIndicatorGravity = obtainStyledAttributes.getInt(C1632R.styleable.SlidingTabLayout_tl_indicator_gravity, 80);
        this.mIndicatorWidthEqualTitle = obtainStyledAttributes.getBoolean(C1632R.styleable.SlidingTabLayout_tl_indicator_width_equal_title, false);
        this.mUnderlineColor = obtainStyledAttributes.getColor(C1632R.styleable.SlidingTabLayout_tl_underline_color, Color.parseColor("#ffffff"));
        this.mUnderlineHeight = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_underline_height, (float) dp2px(0.0f));
        this.mUnderlineGravity = obtainStyledAttributes.getInt(C1632R.styleable.SlidingTabLayout_tl_underline_gravity, 80);
        this.mDividerColor = obtainStyledAttributes.getColor(C1632R.styleable.SlidingTabLayout_tl_divider_color, Color.parseColor("#ffffff"));
        this.mDividerWidth = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_divider_width, (float) dp2px(0.0f));
        this.mDividerPadding = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_divider_padding, (float) dp2px(12.0f));
        this.mTextsize = (int) obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_textsize, (float) sp2px(15.0f));
        this.mTextSelectSize = (int) obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_textSelectSize, (float) sp2px(14.0f));
        this.mTextBold = obtainStyledAttributes.getInt(C1632R.styleable.SlidingTabLayout_tl_textBold, 1);
        this.mTextAllCaps = obtainStyledAttributes.getBoolean(C1632R.styleable.SlidingTabLayout_tl_textAllCaps, false);
        this.mTabSpaceEqual = obtainStyledAttributes.getBoolean(C1632R.styleable.SlidingTabLayout_tl_tab_space_equal, false);
        this.mTabWidth = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_tab_width, (float) dp2px(-1.0f));
        this.mTabPadding = obtainStyledAttributes.getDimension(C1632R.styleable.SlidingTabLayout_tl_tab_padding, (float) ((this.mTabSpaceEqual || this.mTabWidth > 0.0f) ? dp2px(0.0f) : dp2px(8.0f)));
        obtainStyledAttributes.recycle();
    }

    /* access modifiers changed from: private */
    public void scrollToCurrentTab() {
        LinearLayout linearLayout;
        if (this.mTabCount > 0 && (linearLayout = this.mTabsContainer) != null) {
            int width = (int) (this.mCurrentPositionOffset * ((float) linearLayout.getChildAt(this.mCurrentTab).getWidth()));
            int left = this.mTabsContainer.getChildAt(this.mCurrentTab).getLeft() + width;
            if (this.mCurrentTab > 0 || width > 0) {
                int width2 = left - ((getWidth() / 2) - getPaddingLeft());
                calcIndicatorRect();
                Rect rect = this.mTabRect;
                left = width2 + ((rect.right - rect.left) / 2);
            }
            if (left != this.mLastScrollX) {
                this.mLastScrollX = left;
                scrollTo(left, 0);
            }
        }
    }

    /* access modifiers changed from: private */
    public void updateTabStyles(int i) {
        int i2 = 0;
        while (i2 < this.mTabCount) {
            View childAt = this.mTabsContainer.getChildAt(i2);
            TextView textView = (TextView) childAt.findViewById(C1632R.C1635id.tv_tab_title);
            ImageView imageView = (ImageView) childAt.findViewById(C1632R.C1635id.indicator);
            imageView.setColorFilter(pareColor(this.mTitleList.get(i2).getIndicatorColor(), this.mDefaultTabBean.getIndicatorColor()));
            if (textView != null) {
                int pareColor = pareColor(this.mTitleList.get(i2).getTitleColor(), this.mDefaultTabBean.getTitleColor());
                int pareColor2 = pareColor(this.mTitleList.get(i2).getTitleSelectedColor(), this.mDefaultTabBean.getTitleSelectedColor());
                imageView.setVisibility(i2 == i ? 0 : 8);
                if (i2 == i) {
                    pareColor = pareColor2;
                }
                textView.setTextColor(pareColor);
                textView.setTextSize(0, (float) (i2 == i ? this.mTextSelectSize : this.mTextsize));
                float f = this.mTabPadding;
                textView.setPadding((int) f, 0, (int) f, 0);
                if (this.mTextAllCaps) {
                    textView.setText(textView.getText().toString().toUpperCase());
                }
                int i3 = this.mTextBold;
                if (i3 == 2) {
                    textView.getPaint().setFakeBoldText(true);
                } else if (i3 == 0) {
                    textView.getPaint().setFakeBoldText(false);
                }
            }
            i2++;
        }
    }

    /* access modifiers changed from: protected */
    public int dp2px(float f) {
        return (int) ((f * getResources().getDisplayMetrics().density) + 0.5f);
    }

    public int getCurrentTab() {
        return this.mCurrentTab;
    }

    public float getIndicatorHeight() {
        return this.mIndicatorHeight;
    }

    public int getTabCount() {
        return this.mTabCount;
    }

    public int getTextSelectSize() {
        return this.mTextSelectSize;
    }

    public float getTextSize() {
        return (float) this.mTextsize;
    }

    public TextView getTitleView(int i) {
        return (TextView) this.mTabsContainer.getChildAt(i).findViewById(C1632R.C1635id.tv_tab_title);
    }

    public void notifyDataSetChanged() {
        this.mTabsContainer.removeAllViews();
        List<TabItemBean> list = this.mTitleList;
        this.mTabCount = list == null ? ((RecyclerView.Adapter) Objects.requireNonNull(this.mViewPager.getAdapter())).getItemCount() : list.size();
        for (int i = 0; i < this.mTabCount; i++) {
            addTab(i, this.mTitleList, View.inflate(this.mContext, C1632R.C1637layout.flyco_layout_tab_diy, null));
        }
        updateTabStyles(this.mCurrentTab);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!isInEditMode() && this.mTabCount > 0) {
            int height = getHeight();
            int paddingLeft = getPaddingLeft();
            float f = this.mDividerWidth;
            if (f > 0.0f) {
                this.mDividerPaint.setStrokeWidth(f);
                this.mDividerPaint.setColor(this.mDividerColor);
                for (int i = 0; i < this.mTabCount - 1; i++) {
                    View childAt = this.mTabsContainer.getChildAt(i);
                    canvas.drawLine((float) (childAt.getRight() + paddingLeft), this.mDividerPadding, (float) (childAt.getRight() + paddingLeft), ((float) height) - this.mDividerPadding, this.mDividerPaint);
                }
            }
            if (this.mUnderlineHeight > 0.0f) {
                this.mRectPaint.setColor(this.mUnderlineColor);
                if (this.mUnderlineGravity == 80) {
                    float f2 = (float) height;
                    canvas.drawRect((float) paddingLeft, f2 - this.mUnderlineHeight, (float) (this.mTabsContainer.getWidth() + paddingLeft), f2, this.mRectPaint);
                } else {
                    canvas.drawRect((float) paddingLeft, 0.0f, (float) (this.mTabsContainer.getWidth() + paddingLeft), this.mUnderlineHeight, this.mRectPaint);
                }
            }
            calcIndicatorRect();
            int i2 = this.mIndicatorStyle;
            if (i2 == 1) {
                if (this.mIndicatorHeight > 0.0f) {
                    this.mTrianglePaint.setColor(this.mIndicatorColor);
                    this.mTrianglePath.reset();
                    float f3 = (float) height;
                    this.mTrianglePath.moveTo((float) (this.mIndicatorRect.left + paddingLeft), f3);
                    Path path = this.mTrianglePath;
                    Rect rect = this.mIndicatorRect;
                    path.lineTo((float) ((rect.left / 2) + paddingLeft + (rect.right / 2)), f3 - this.mIndicatorHeight);
                    this.mTrianglePath.lineTo((float) (paddingLeft + this.mIndicatorRect.right), f3);
                    this.mTrianglePath.close();
                    canvas.drawPath(this.mTrianglePath, this.mTrianglePaint);
                }
            } else if (i2 == 2) {
                if (this.mIndicatorHeight < 0.0f) {
                    this.mIndicatorHeight = (((float) height) - this.mIndicatorMarginTop) - this.mIndicatorMarginBottom;
                }
                float f4 = this.mIndicatorHeight;
                if (f4 > 0.0f) {
                    float f5 = this.mIndicatorCornerRadius;
                    if (f5 < 0.0f || f5 > f4 / 2.0f) {
                        this.mIndicatorCornerRadius = this.mIndicatorHeight / 2.0f;
                    }
                    this.mIndicatorDrawable.setColor(this.mIndicatorColor);
                    GradientDrawable gradientDrawable = this.mIndicatorDrawable;
                    Rect rect2 = this.mIndicatorRect;
                    int i3 = ((int) this.mIndicatorMarginLeft) + paddingLeft + rect2.left;
                    float f6 = this.mIndicatorMarginTop;
                    gradientDrawable.setBounds(i3, (int) f6, (int) (((float) (paddingLeft + rect2.right)) - this.mIndicatorMarginRight), (int) (f6 + this.mIndicatorHeight));
                    this.mIndicatorDrawable.setCornerRadius(this.mIndicatorCornerRadius);
                    this.mIndicatorDrawable.draw(canvas);
                }
            } else if (this.mIndicatorHeight > 0.0f) {
                this.mIndicatorDrawable.setColor(this.mIndicatorColor);
                if (this.mIndicatorGravity == 80) {
                    GradientDrawable gradientDrawable2 = this.mIndicatorDrawable;
                    Rect rect3 = this.mIndicatorRect;
                    float f7 = this.mIndicatorMarginBottom;
                    gradientDrawable2.setBounds(((int) this.mIndicatorMarginLeft) + paddingLeft + rect3.left, (height - ((int) this.mIndicatorHeight)) - ((int) f7), (paddingLeft + rect3.right) - ((int) this.mIndicatorMarginRight), height - ((int) f7));
                } else {
                    GradientDrawable gradientDrawable3 = this.mIndicatorDrawable;
                    Rect rect4 = this.mIndicatorRect;
                    int i4 = ((int) this.mIndicatorMarginLeft) + paddingLeft + rect4.left;
                    float f8 = this.mIndicatorMarginTop;
                    gradientDrawable3.setBounds(i4, (int) f8, (paddingLeft + rect4.right) - ((int) this.mIndicatorMarginRight), ((int) this.mIndicatorHeight) + ((int) f8));
                }
                this.mIndicatorDrawable.setCornerRadius(this.mIndicatorCornerRadius);
                this.mIndicatorDrawable.draw(canvas);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            this.mCurrentTab = bundle.getInt("mCurrentTab");
            parcelable = bundle.getParcelable("instanceState");
            if (this.mCurrentTab != 0 && this.mTabsContainer.getChildCount() > 0) {
                updateTabStyles(this.mCurrentTab);
                scrollToCurrentTab();
            }
        }
        super.onRestoreInstanceState(parcelable);
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable("instanceState", super.onSaveInstanceState());
        bundle.putInt("mCurrentTab", this.mCurrentTab);
        return bundle;
    }

    public int pareColor(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            return Color.parseColor(str2);
        }
        try {
            return Color.parseColor(str);
        } catch (Exception unused) {
            return Color.parseColor(str2);
        }
    }

    public void setCurrentTab(int i, boolean z) {
        this.mCurrentTab = i;
        this.mViewPager.setCurrentItem(i, z);
    }

    public void setDefaultTabBean(TabItemBean tabItemBean) {
        this.mDefaultTabBean = tabItemBean;
    }

    public void setIndicatorHeight(float f) {
        this.mIndicatorHeight = (float) dp2px(f);
        invalidate();
    }

    public void setIndicatorWidthEqualTitle(boolean z) {
        this.mIndicatorWidthEqualTitle = z;
        invalidate();
    }

    public void setOnTabSelectListener(OnTabSelectListener onTabSelectListener) {
        this.mListener = onTabSelectListener;
    }

    public void setSnapOnTabClick(boolean z) {
        this.mSnapOnTabClick = z;
    }

    public void setTextSelectSize(int i) {
        this.mTextSelectSize = sp2px((float) i);
    }

    public void setTextSize(float f) {
        this.mTextsize = sp2px(f);
    }

    public void setTitleList(List<TabItemBean> list) {
        this.mTitleList = list;
    }

    public void setUnderlineGravity(int i) {
        this.mUnderlineGravity = i;
        invalidate();
    }

    public void setViewPager(ViewPager2 viewPager2, List<TabItemBean> list) {
        if (viewPager2 == null || viewPager2.getAdapter() == null) {
            throw new IllegalStateException("ViewPager or ViewPager adapter can not be NULL !");
        } else if (list == null || list.size() == 0) {
            throw new IllegalStateException("Titles can not be EMPTY !");
        } else if (list.size() == viewPager2.getAdapter().getItemCount()) {
            this.mViewPager = viewPager2;
            this.mTitleList = new ArrayList();
            this.mTitleList.addAll(list);
            this.mViewPager.unregisterOnPageChangeCallback(this.mPageChangeCallback);
            this.mViewPager.registerOnPageChangeCallback(this.mPageChangeCallback);
            notifyDataSetChanged();
        } else {
            throw new IllegalStateException("Titles length must be the same as the page count !");
        }
    }

    /* access modifiers changed from: protected */
    public int sp2px(float f) {
        return (int) ((f * getResources().getDisplayMetrics().scaledDensity) + 0.5f);
    }

    public DiyTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.mIndicatorRect = new Rect();
        this.mTabRect = new Rect();
        this.mIndicatorDrawable = new GradientDrawable();
        this.mRectPaint = new Paint(1);
        this.mDividerPaint = new Paint(1);
        this.mTrianglePaint = new Paint(1);
        this.mTrianglePath = new Path();
        this.mIndicatorStyle = 0;
        this.mDefaultTabBean = new TabItemBean("#d9000000", "#66000000", "", "#FF4FCAA4");
        this.mTextPaint = new Paint(1);
        setFillViewport(true);
        setWillNotDraw(false);
        setClipChildren(false);
        setClipToPadding(false);
        this.mContext = context;
        this.mTabsContainer = new LinearLayout(context);
        addView(this.mTabsContainer);
        obtainAttributes(context, attributeSet);
        String attributeValue = attributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "layout_height");
        if (!attributeValue.equals("-1") && !attributeValue.equals("-2")) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, new int[]{16842997});
            this.mHeight = obtainStyledAttributes.getDimensionPixelSize(0, -2);
            obtainStyledAttributes.recycle();
        }
        this.mPageChangeCallback = new ViewPager2.OnPageChangeCallback() {
            /* class com.flyco.tablayout.DiyTabLayout.C16301 */

            public void onPageScrolled(int i, float f, int i2) {
                super.onPageScrolled(i, f, i2);
                int unused = DiyTabLayout.this.mCurrentTab = i;
                float unused2 = DiyTabLayout.this.mCurrentPositionOffset = f;
                DiyTabLayout.this.scrollToCurrentTab();
                DiyTabLayout.this.invalidate();
            }

            public void onPageSelected(int i) {
                super.onPageSelected(i);
                DiyTabLayout.this.updateTabStyles(i);
            }
        };
    }

    public void setCurrentTab(int i) {
        this.mCurrentTab = i;
        this.mViewPager.setCurrentItem(i);
    }

    public DiyTabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }
}
