package com.flyco.tablayout.utils;

import android.util.DisplayMetrics;
import android.widget.RelativeLayout;
import com.flyco.tablayout.widget.MsgView;

public class UnreadMsgUtils {
    public static void setSize(MsgView msgView, int i) {
        if (msgView != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) msgView.getLayoutParams();
            layoutParams.width = i;
            layoutParams.height = i;
            msgView.setLayoutParams(layoutParams);
        }
    }

    public static void show(MsgView msgView, int i) {
        if (msgView != null) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) msgView.getLayoutParams();
            DisplayMetrics displayMetrics = msgView.getResources().getDisplayMetrics();
            msgView.setVisibility(0);
            if (i <= 0) {
                msgView.setStrokeWidth(0);
                msgView.setText("");
                float f = displayMetrics.density;
                layoutParams.width = (int) (f * 5.0f);
                layoutParams.height = (int) (f * 5.0f);
                msgView.setLayoutParams(layoutParams);
                return;
            }
            layoutParams.height = (int) (displayMetrics.density * 18.0f);
            String str = i + "";
            if (i < 10) {
                layoutParams.width = (int) (displayMetrics.density * 18.0f);
            } else if (i < 100) {
                layoutParams.width = -2;
                float f2 = displayMetrics.density;
                msgView.setPadding((int) (f2 * 6.0f), 0, (int) (f2 * 6.0f), 0);
            } else {
                layoutParams.width = -2;
                float f3 = displayMetrics.density;
                msgView.setPadding((int) (f3 * 6.0f), 0, (int) (f3 * 6.0f), 0);
                str = "99+";
            }
            msgView.setText(str);
            msgView.setLayoutParams(layoutParams);
        }
    }
}
