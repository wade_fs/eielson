package com.flyco.tablayout;

import android.text.TextUtils;

public class TabItemBean {
    private String indicatorColor = "#FF4FCAA4";
    private String title = "";
    private String titleColor = "#d9000000";
    private String titleSelectedColor = "#66000000";

    public TabItemBean() {
    }

    public String getIndicatorColor() {
        String str = this.indicatorColor;
        return str == null ? "" : str;
    }

    public String getTitle() {
        String str = this.title;
        return str == null ? "" : str;
    }

    public String getTitleColor() {
        String str = this.titleColor;
        return str == null ? "" : str;
    }

    public String getTitleSelectedColor() {
        String str = this.titleSelectedColor;
        return str == null ? "" : str;
    }

    public void setIndicatorColor(String str) {
        this.indicatorColor = str;
    }

    public void setTitle(String str) {
        this.title = str;
    }

    public void setTitleColor(String str) {
        this.titleColor = str;
    }

    public void setTitleSelectedColor(String str) {
        this.titleSelectedColor = str;
    }

    public TabItemBean(String str, String str2, String str3, String str4) {
        this.titleColor = TextUtils.isEmpty(str4) ? "#d9000000" : str;
        this.titleSelectedColor = TextUtils.isEmpty(str4) ? "#66000000" : str2;
        this.title = str3;
        this.indicatorColor = TextUtils.isEmpty(str4) ? "#FF4FCAA4" : str4;
    }
}
