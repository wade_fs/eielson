package com.bumptech.glide.load.p028o;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p028o.ModelLoader;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.bumptech.glide.load.o.x */
public class UrlUriLoader<Data> implements ModelLoader<Uri, Data> {

    /* renamed from: b */
    private static final Set<String> f1370b = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", "https")));

    /* renamed from: a */
    private final ModelLoader<GlideUrl, Data> f1371a;

    /* renamed from: com.bumptech.glide.load.o.x$a */
    /* compiled from: UrlUriLoader */
    public static class C1056a implements ModelLoaderFactory<Uri, InputStream> {
        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new UrlUriLoader(rVar.mo10313a(GlideUrl.class, InputStream.class));
        }
    }

    public UrlUriLoader(ModelLoader<GlideUrl, Data> nVar) {
        this.f1371a = nVar;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        return this.f1371a.mo10261a(new GlideUrl(uri.toString()), i, i2, iVar);
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        return f1370b.contains(uri.getScheme());
    }
}
