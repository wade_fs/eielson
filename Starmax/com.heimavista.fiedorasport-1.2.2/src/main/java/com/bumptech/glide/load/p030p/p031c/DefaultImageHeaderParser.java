package com.bumptech.glide.load.p030p.p031c;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.internal.view.SupportMenu;
import androidx.core.view.InputDeviceCompat;
import androidx.core.view.MotionEventCompat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.util.Preconditions;
import com.google.android.exoplayer2.C1750C;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;

/* renamed from: com.bumptech.glide.load.p.c.j */
public final class DefaultImageHeaderParser implements ImageHeaderParser {

    /* renamed from: a */
    static final byte[] f1397a = "Exif\u0000\u0000".getBytes(Charset.forName(C1750C.UTF8_NAME));

    /* renamed from: b */
    private static final int[] f1398b = {0, 1, 1, 2, 4, 8, 1, 1, 2, 4, 8, 4, 8};

    /* renamed from: com.bumptech.glide.load.p.c.j$a */
    /* compiled from: DefaultImageHeaderParser */
    private static final class C1062a implements C1064c {

        /* renamed from: a */
        private final ByteBuffer f1399a;

        C1062a(ByteBuffer byteBuffer) {
            this.f1399a = byteBuffer;
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
        }

        /* renamed from: a */
        public int mo10345a() {
            return ((mo10348c() << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK) | (mo10348c() & 255);
        }

        /* renamed from: b */
        public short mo10347b() {
            return (short) (mo10348c() & 255);
        }

        /* renamed from: c */
        public int mo10348c() {
            if (this.f1399a.remaining() < 1) {
                return -1;
            }
            return this.f1399a.get();
        }

        public long skip(long j) {
            int min = (int) Math.min((long) this.f1399a.remaining(), j);
            ByteBuffer byteBuffer = this.f1399a;
            byteBuffer.position(byteBuffer.position() + min);
            return (long) min;
        }

        /* renamed from: a */
        public int mo10346a(byte[] bArr, int i) {
            int min = Math.min(i, this.f1399a.remaining());
            if (min == 0) {
                return -1;
            }
            this.f1399a.get(bArr, 0, min);
            return min;
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.j$b */
    /* compiled from: DefaultImageHeaderParser */
    private static final class C1063b {

        /* renamed from: a */
        private final ByteBuffer f1400a;

        C1063b(byte[] bArr, int i) {
            this.f1400a = (ByteBuffer) ByteBuffer.wrap(bArr).order(ByteOrder.BIG_ENDIAN).limit(i);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10352a(ByteOrder byteOrder) {
            this.f1400a.order(byteOrder);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public int mo10353b(int i) {
            if (m2122a(i, 4)) {
                return this.f1400a.getInt(i);
            }
            return -1;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public int mo10350a() {
            return this.f1400a.remaining();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public short mo10351a(int i) {
            if (m2122a(i, 2)) {
                return this.f1400a.getShort(i);
            }
            return -1;
        }

        /* renamed from: a */
        private boolean m2122a(int i, int i2) {
            return this.f1400a.remaining() - i >= i2;
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.j$c */
    /* compiled from: DefaultImageHeaderParser */
    private interface C1064c {
        /* renamed from: a */
        int mo10345a();

        /* renamed from: a */
        int mo10346a(byte[] bArr, int i);

        /* renamed from: b */
        short mo10347b();

        /* renamed from: c */
        int mo10348c();

        long skip(long j);
    }

    /* renamed from: com.bumptech.glide.load.p.c.j$d */
    /* compiled from: DefaultImageHeaderParser */
    private static final class C1065d implements C1064c {

        /* renamed from: a */
        private final InputStream f1401a;

        C1065d(InputStream inputStream) {
            this.f1401a = inputStream;
        }

        /* renamed from: a */
        public int mo10345a() {
            return ((this.f1401a.read() << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK) | (this.f1401a.read() & 255);
        }

        /* renamed from: b */
        public short mo10347b() {
            return (short) (this.f1401a.read() & 255);
        }

        /* renamed from: c */
        public int mo10348c() {
            return this.f1401a.read();
        }

        public long skip(long j) {
            if (j < 0) {
                return 0;
            }
            long j2 = j;
            while (j2 > 0) {
                long skip = this.f1401a.skip(j2);
                if (skip <= 0) {
                    if (this.f1401a.read() == -1) {
                        break;
                    }
                    skip = 1;
                }
                j2 -= skip;
            }
            return j - j2;
        }

        /* renamed from: a */
        public int mo10346a(byte[] bArr, int i) {
            int i2 = i;
            while (i2 > 0) {
                int read = this.f1401a.read(bArr, i - i2, i2);
                if (read == -1) {
                    break;
                }
                i2 -= read;
            }
            return i - i2;
        }
    }

    /* renamed from: a */
    private static int m2107a(int i, int i2) {
        return i + 2 + (i2 * 12);
    }

    /* renamed from: a */
    private static boolean m2112a(int i) {
        return (i & 65496) == 65496 || i == 19789 || i == 18761;
    }

    /* renamed from: b */
    private int m2114b(C1064c cVar) {
        short b;
        int a;
        long j;
        long skip;
        do {
            short b2 = cVar.mo10347b();
            if (b2 != 255) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Unknown segmentId=" + ((int) b2));
                }
                return -1;
            }
            b = cVar.mo10347b();
            if (b == 218) {
                return -1;
            }
            if (b == 217) {
                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Found MARKER_EOI in exif segment");
                }
                return -1;
            }
            a = cVar.mo10345a() - 2;
            if (b == 225) {
                return a;
            }
            j = (long) a;
            skip = cVar.skip(j);
        } while (skip == j);
        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
            Log.d("DfltImageHeaderParser", "Unable to skip enough data, type: " + ((int) b) + ", wanted to skip: " + a + ", but actually skipped: " + skip);
        }
        return -1;
    }

    @NonNull
    /* renamed from: a */
    public ImageHeaderParser.ImageType mo9962a(@NonNull InputStream inputStream) {
        Preconditions.m2828a(inputStream);
        return m2111a(new C1065d(inputStream));
    }

    @NonNull
    /* renamed from: a */
    public ImageHeaderParser.ImageType mo9963a(@NonNull ByteBuffer byteBuffer) {
        Preconditions.m2828a(byteBuffer);
        return m2111a(new C1062a(byteBuffer));
    }

    /* renamed from: a */
    public int mo9961a(@NonNull InputStream inputStream, @NonNull ArrayPool bVar) {
        Preconditions.m2828a(inputStream);
        C1065d dVar = new C1065d(inputStream);
        Preconditions.m2828a(bVar);
        return m2109a(dVar, bVar);
    }

    @NonNull
    /* renamed from: a */
    private ImageHeaderParser.ImageType m2111a(C1064c cVar) {
        int a = cVar.mo10345a();
        if (a == 65496) {
            return ImageHeaderParser.ImageType.JPEG;
        }
        int a2 = ((a << 16) & SupportMenu.CATEGORY_MASK) | (cVar.mo10345a() & 65535);
        if (a2 == -1991225785) {
            cVar.skip(21);
            return cVar.mo10348c() >= 3 ? ImageHeaderParser.ImageType.PNG_A : ImageHeaderParser.ImageType.PNG;
        } else if ((a2 >> 8) == 4671814) {
            return ImageHeaderParser.ImageType.GIF;
        } else {
            if (a2 != 1380533830) {
                return ImageHeaderParser.ImageType.UNKNOWN;
            }
            cVar.skip(4);
            if ((((cVar.mo10345a() << 16) & SupportMenu.CATEGORY_MASK) | (cVar.mo10345a() & 65535)) != 1464156752) {
                return ImageHeaderParser.ImageType.UNKNOWN;
            }
            int a3 = ((cVar.mo10345a() << 16) & SupportMenu.CATEGORY_MASK) | (cVar.mo10345a() & 65535);
            if ((a3 & InputDeviceCompat.SOURCE_ANY) != 1448097792) {
                return ImageHeaderParser.ImageType.UNKNOWN;
            }
            int i = a3 & 255;
            if (i == 88) {
                cVar.skip(4);
                return (cVar.mo10348c() & 16) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
            } else if (i != 76) {
                return ImageHeaderParser.ImageType.WEBP;
            } else {
                cVar.skip(4);
                return (cVar.mo10348c() & 8) != 0 ? ImageHeaderParser.ImageType.WEBP_A : ImageHeaderParser.ImageType.WEBP;
            }
        }
    }

    /* renamed from: a */
    private int m2109a(C1064c cVar, ArrayPool bVar) {
        int a = cVar.mo10345a();
        if (!m2112a(a)) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Parser doesn't handle magic number: " + a);
            }
            return -1;
        }
        int b = m2114b(cVar);
        if (b == -1) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Failed to parse exif segment length, or exif segment not found");
            }
            return -1;
        }
        byte[] bArr = (byte[]) bVar.mo10042b(b, byte[].class);
        try {
            return m2110a(cVar, bArr, b);
        } finally {
            bVar.put(bArr);
        }
    }

    /* renamed from: a */
    private int m2110a(C1064c cVar, byte[] bArr, int i) {
        int a = cVar.mo10346a(bArr, i);
        if (a != i) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unable to read exif segment data, length: " + i + ", actually read: " + a);
            }
            return -1;
        } else if (m2113a(bArr, i)) {
            return m2108a(new C1063b(bArr, i));
        } else {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Missing jpeg exif preamble");
            }
            return -1;
        }
    }

    /* renamed from: a */
    private boolean m2113a(byte[] bArr, int i) {
        boolean z = bArr != null && i > f1397a.length;
        if (!z) {
            return z;
        }
        int i2 = 0;
        while (true) {
            byte[] bArr2 = f1397a;
            if (i2 >= bArr2.length) {
                return z;
            }
            if (bArr[i2] != bArr2[i2]) {
                return false;
            }
            i2++;
        }
    }

    /* renamed from: a */
    private static int m2108a(C1063b bVar) {
        ByteOrder byteOrder;
        short a = bVar.mo10351a(6);
        if (a == 18761) {
            byteOrder = ByteOrder.LITTLE_ENDIAN;
        } else if (a != 19789) {
            if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                Log.d("DfltImageHeaderParser", "Unknown endianness = " + ((int) a));
            }
            byteOrder = ByteOrder.BIG_ENDIAN;
        } else {
            byteOrder = ByteOrder.BIG_ENDIAN;
        }
        bVar.mo10352a(byteOrder);
        int b = bVar.mo10353b(10) + 6;
        short a2 = bVar.mo10351a(b);
        for (int i = 0; i < a2; i++) {
            int a3 = m2107a(b, i);
            short a4 = bVar.mo10351a(a3);
            if (a4 == 274) {
                short a5 = bVar.mo10351a(a3 + 2);
                if (a5 >= 1 && a5 <= 12) {
                    int b2 = bVar.mo10353b(a3 + 4);
                    if (b2 >= 0) {
                        if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got tagIndex=" + i + " tagType=" + ((int) a4) + " formatCode=" + ((int) a5) + " componentCount=" + b2);
                        }
                        int i2 = b2 + f1398b[a5];
                        if (i2 <= 4) {
                            int i3 = a3 + 8;
                            if (i3 < 0 || i3 > bVar.mo10350a()) {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal tagValueOffset=" + i3 + " tagType=" + ((int) a4));
                                }
                            } else if (i2 >= 0 && i2 + i3 <= bVar.mo10350a()) {
                                return bVar.mo10351a(i3);
                            } else {
                                if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                                    Log.d("DfltImageHeaderParser", "Illegal number of bytes for TI tag data tagType=" + ((int) a4));
                                }
                            }
                        } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                            Log.d("DfltImageHeaderParser", "Got byte count > 4, not orientation, continuing, formatCode=" + ((int) a5));
                        }
                    } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                        Log.d("DfltImageHeaderParser", "Negative tiff component count");
                    }
                } else if (Log.isLoggable("DfltImageHeaderParser", 3)) {
                    Log.d("DfltImageHeaderParser", "Got invalid format code = " + ((int) a5));
                }
            }
        }
        return -1;
    }
}
