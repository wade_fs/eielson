package com.bumptech.glide.load.p030p.p034f;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import java.io.File;

/* renamed from: com.bumptech.glide.load.p.f.a */
public class FileDecoder implements ResourceDecoder<File, File> {
    /* renamed from: a */
    public boolean mo9980a(@NonNull File file, @NonNull Options iVar) {
        return true;
    }

    /* renamed from: a */
    public Resource<File> mo9979a(@NonNull File file, int i, int i2, @NonNull Options iVar) {
        return new FileResource(file);
    }
}
