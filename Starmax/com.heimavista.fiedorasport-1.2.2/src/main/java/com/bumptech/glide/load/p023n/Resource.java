package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.load.n.v */
public interface Resource<Z> {
    /* renamed from: a */
    int mo10226a();

    @NonNull
    /* renamed from: b */
    Class<Z> mo10228b();

    @NonNull
    Z get();

    void recycle();
}
