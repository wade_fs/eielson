package com.bumptech.glide.p040q.p041l;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.manager.LifecycleListener;
import com.bumptech.glide.p040q.Request;
import com.bumptech.glide.p040q.p042m.Transition;

/* renamed from: com.bumptech.glide.q.l.i */
public interface Target<R> extends LifecycleListener {
    @Nullable
    /* renamed from: a */
    Request mo10638a();

    /* renamed from: a */
    void mo10639a(@Nullable Drawable drawable);

    /* renamed from: a */
    void mo10640a(@Nullable Request dVar);

    /* renamed from: a */
    void mo10641a(@NonNull SizeReadyCallback hVar);

    /* renamed from: a */
    void mo10453a(@NonNull R r, @Nullable Transition<? super R> bVar);

    /* renamed from: b */
    void mo10644b(@Nullable Drawable drawable);

    /* renamed from: b */
    void mo10645b(@NonNull SizeReadyCallback hVar);

    /* renamed from: c */
    void mo10646c(@Nullable Drawable drawable);
}
