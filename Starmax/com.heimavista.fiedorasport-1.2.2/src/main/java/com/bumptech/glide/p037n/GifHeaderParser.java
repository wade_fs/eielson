package com.bumptech.glide.p037n;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

/* renamed from: com.bumptech.glide.n.d */
public class GifHeaderParser {

    /* renamed from: a */
    private final byte[] f1619a = new byte[256];

    /* renamed from: b */
    private ByteBuffer f1620b;

    /* renamed from: c */
    private GifHeader f1621c;

    /* renamed from: d */
    private int f1622d = 0;

    /* renamed from: c */
    private boolean m2511c() {
        return this.f1621c.f1607b != 0;
    }

    /* renamed from: d */
    private int m2512d() {
        try {
            return this.f1620b.get() & 255;
        } catch (Exception unused) {
            this.f1621c.f1607b = 1;
            return 0;
        }
    }

    /* renamed from: e */
    private void m2513e() {
        this.f1621c.f1609d.f1595a = m2520l();
        this.f1621c.f1609d.f1596b = m2520l();
        this.f1621c.f1609d.f1597c = m2520l();
        this.f1621c.f1609d.f1598d = m2520l();
        int d = m2512d();
        boolean z = false;
        boolean z2 = (d & 128) != 0;
        int pow = (int) Math.pow(2.0d, (double) ((d & 7) + 1));
        GifFrame bVar = this.f1621c.f1609d;
        if ((d & 64) != 0) {
            z = true;
        }
        bVar.f1599e = z;
        if (z2) {
            this.f1621c.f1609d.f1605k = m2509a(pow);
        } else {
            this.f1621c.f1609d.f1605k = null;
        }
        this.f1621c.f1609d.f1604j = this.f1620b.position();
        m2523o();
        if (!m2511c()) {
            GifHeader cVar = this.f1621c;
            cVar.f1608c++;
            cVar.f1610e.add(cVar.f1609d);
        }
    }

    /* renamed from: f */
    private void m2514f() {
        this.f1622d = m2512d();
        if (this.f1622d > 0) {
            int i = 0;
            int i2 = 0;
            while (i < this.f1622d) {
                try {
                    i2 = this.f1622d - i;
                    this.f1620b.get(this.f1619a, i, i2);
                    i += i2;
                } catch (Exception e) {
                    if (Log.isLoggable("GifHeaderParser", 3)) {
                        Log.d("GifHeaderParser", "Error Reading Block n: " + i + " count: " + i2 + " blockSize: " + this.f1622d, e);
                    }
                    this.f1621c.f1607b = 1;
                    return;
                }
            }
        }
    }

    /* renamed from: g */
    private void m2515g() {
        m2510b(Integer.MAX_VALUE);
    }

    /* renamed from: h */
    private void m2516h() {
        m2512d();
        int d = m2512d();
        GifFrame bVar = this.f1621c.f1609d;
        bVar.f1601g = (d & 28) >> 2;
        boolean z = true;
        if (bVar.f1601g == 0) {
            bVar.f1601g = 1;
        }
        GifFrame bVar2 = this.f1621c.f1609d;
        if ((d & 1) == 0) {
            z = false;
        }
        bVar2.f1600f = z;
        int l = m2520l();
        if (l < 2) {
            l = 10;
        }
        GifFrame bVar3 = this.f1621c.f1609d;
        bVar3.f1603i = l * 10;
        bVar3.f1602h = m2512d();
        m2512d();
    }

    /* renamed from: i */
    private void m2517i() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            sb.append((char) m2512d());
        }
        if (!sb.toString().startsWith("GIF")) {
            this.f1621c.f1607b = 1;
            return;
        }
        m2518j();
        if (this.f1621c.f1613h && !m2511c()) {
            GifHeader cVar = this.f1621c;
            cVar.f1606a = m2509a(cVar.f1614i);
            GifHeader cVar2 = this.f1621c;
            cVar2.f1617l = cVar2.f1606a[cVar2.f1615j];
        }
    }

    /* renamed from: j */
    private void m2518j() {
        this.f1621c.f1611f = m2520l();
        this.f1621c.f1612g = m2520l();
        int d = m2512d();
        this.f1621c.f1613h = (d & 128) != 0;
        this.f1621c.f1614i = (int) Math.pow(2.0d, (double) ((d & 7) + 1));
        this.f1621c.f1615j = m2512d();
        this.f1621c.f1616k = m2512d();
    }

    /* renamed from: k */
    private void m2519k() {
        do {
            m2514f();
            byte[] bArr = this.f1619a;
            if (bArr[0] == 1) {
                this.f1621c.f1618m = ((bArr[2] & 255) << 8) | (bArr[1] & 255);
            }
            if (this.f1622d <= 0) {
                return;
            }
        } while (!m2511c());
    }

    /* renamed from: l */
    private int m2520l() {
        return this.f1620b.getShort();
    }

    /* renamed from: m */
    private void m2521m() {
        this.f1620b = null;
        Arrays.fill(this.f1619a, (byte) 0);
        this.f1621c = new GifHeader();
        this.f1622d = 0;
    }

    /* renamed from: n */
    private void m2522n() {
        int d;
        do {
            d = m2512d();
            this.f1620b.position(Math.min(this.f1620b.position() + d, this.f1620b.limit()));
        } while (d > 0);
    }

    /* renamed from: o */
    private void m2523o() {
        m2512d();
        m2522n();
    }

    /* renamed from: a */
    public GifHeaderParser mo10541a(@NonNull ByteBuffer byteBuffer) {
        m2521m();
        this.f1620b = byteBuffer.asReadOnlyBuffer();
        this.f1620b.position(0);
        this.f1620b.order(ByteOrder.LITTLE_ENDIAN);
        return this;
    }

    @NonNull
    /* renamed from: b */
    public GifHeader mo10543b() {
        if (this.f1620b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (m2511c()) {
            return this.f1621c;
        } else {
            m2517i();
            if (!m2511c()) {
                m2515g();
                GifHeader cVar = this.f1621c;
                if (cVar.f1608c < 0) {
                    cVar.f1607b = 1;
                }
            }
            return this.f1621c;
        }
    }

    /* renamed from: a */
    public void mo10542a() {
        this.f1620b = null;
        this.f1621c = null;
    }

    @Nullable
    /* renamed from: a */
    private int[] m2509a(int i) {
        byte[] bArr = new byte[(i * 3)];
        int[] iArr = null;
        try {
            this.f1620b.get(bArr);
            iArr = new int[256];
            int i2 = 0;
            int i3 = 0;
            while (i2 < i) {
                int i4 = i3 + 1;
                int i5 = i4 + 1;
                int i6 = i5 + 1;
                int i7 = i2 + 1;
                iArr[i2] = ((bArr[i3] & 255) << 16) | -16777216 | ((bArr[i4] & 255) << 8) | (bArr[i5] & 255);
                i3 = i6;
                i2 = i7;
            }
        } catch (BufferUnderflowException e) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e);
            }
            this.f1621c.f1607b = 1;
        }
        return iArr;
    }

    /* renamed from: b */
    private void m2510b(int i) {
        boolean z = false;
        while (!z && !m2511c() && this.f1621c.f1608c <= i) {
            int d = m2512d();
            if (d == 33) {
                int d2 = m2512d();
                if (d2 == 1) {
                    m2522n();
                } else if (d2 == 249) {
                    this.f1621c.f1609d = new GifFrame();
                    m2516h();
                } else if (d2 == 254) {
                    m2522n();
                } else if (d2 != 255) {
                    m2522n();
                } else {
                    m2514f();
                    StringBuilder sb = new StringBuilder();
                    for (int i2 = 0; i2 < 11; i2++) {
                        sb.append((char) this.f1619a[i2]);
                    }
                    if (sb.toString().equals("NETSCAPE2.0")) {
                        m2519k();
                    } else {
                        m2522n();
                    }
                }
            } else if (d == 44) {
                GifHeader cVar = this.f1621c;
                if (cVar.f1609d == null) {
                    cVar.f1609d = new GifFrame();
                }
                m2513e();
            } else if (d != 59) {
                this.f1621c.f1607b = 1;
            } else {
                z = true;
            }
        }
    }
}
