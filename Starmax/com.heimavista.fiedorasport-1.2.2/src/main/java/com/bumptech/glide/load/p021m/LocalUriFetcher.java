package com.bumptech.glide.load.p021m;

import android.content.ContentResolver;
import android.net.Uri;
import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p021m.DataFetcher;
import java.io.FileNotFoundException;
import java.io.IOException;

/* renamed from: com.bumptech.glide.load.m.l */
public abstract class LocalUriFetcher<T> implements DataFetcher<T> {

    /* renamed from: P */
    private final Uri f959P;

    /* renamed from: Q */
    private final ContentResolver f960Q;

    /* renamed from: R */
    private T f961R;

    public LocalUriFetcher(ContentResolver contentResolver, Uri uri) {
        this.f960Q = contentResolver;
        this.f959P = uri;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract T mo9983a(Uri uri, ContentResolver contentResolver);

    /* renamed from: a */
    public final void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a aVar) {
        try {
            this.f961R = mo9983a(this.f959P, this.f960Q);
            aVar.mo9999a((Object) this.f961R);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("LocalUriFetcher", 3)) {
                Log.d("LocalUriFetcher", "Failed to open Uri", e);
            }
            aVar.mo9998a((Exception) e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo9986a(Object obj);

    /* renamed from: b */
    public void mo9990b() {
        T t = this.f961R;
        if (t != null) {
            try {
                mo9986a(t);
            } catch (IOException unused) {
            }
        }
    }

    @NonNull
    /* renamed from: c */
    public DataSource mo9991c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }
}
