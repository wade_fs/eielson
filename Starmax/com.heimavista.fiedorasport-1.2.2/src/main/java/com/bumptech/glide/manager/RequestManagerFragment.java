package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import java.util.HashSet;
import java.util.Set;

@Deprecated
public class RequestManagerFragment extends Fragment {

    /* renamed from: P */
    private final ActivityFragmentLifecycle f1559P;

    /* renamed from: Q */
    private final RequestManagerTreeNode f1560Q;

    /* renamed from: R */
    private final Set<RequestManagerFragment> f1561R;
    @Nullable

    /* renamed from: S */
    private RequestManager f1562S;
    @Nullable

    /* renamed from: T */
    private RequestManagerFragment f1563T;
    @Nullable

    /* renamed from: U */
    private Fragment f1564U;

    /* renamed from: com.bumptech.glide.manager.RequestManagerFragment$a */
    private class C1101a implements RequestManagerTreeNode {
        C1101a() {
        }

        public String toString() {
            return super.toString() + "{fragment=" + RequestManagerFragment.this + "}";
        }
    }

    public RequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    @TargetApi(17)
    @Nullable
    /* renamed from: d */
    private Fragment m2417d() {
        Fragment parentFragment = Build.VERSION.SDK_INT >= 17 ? getParentFragment() : null;
        return parentFragment != null ? super : this.f1564U;
    }

    /* renamed from: e */
    private void m2418e() {
        RequestManagerFragment requestManagerFragment = this.f1563T;
        if (requestManagerFragment != null) {
            requestManagerFragment.m2416b(this);
            this.f1563T = null;
        }
    }

    /* renamed from: a */
    public void mo10482a(@Nullable RequestManager kVar) {
        this.f1562S = kVar;
    }

    @Nullable
    /* renamed from: b */
    public RequestManager mo10483b() {
        return this.f1562S;
    }

    @NonNull
    /* renamed from: c */
    public RequestManagerTreeNode mo10484c() {
        return this.f1560Q;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            m2414a(activity);
        } catch (IllegalStateException e) {
            if (Log.isLoggable("RMFragment", 5)) {
                Log.w("RMFragment", "Unable to register fragment with root", e);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f1559P.mo10498a();
        m2418e();
    }

    public void onDetach() {
        super.onDetach();
        m2418e();
    }

    public void onStart() {
        super.onStart();
        this.f1559P.mo10500b();
    }

    public void onStop() {
        super.onStop();
        this.f1559P.mo10502c();
    }

    public String toString() {
        return super.toString() + "{parent=" + m2417d() + "}";
    }

    @VisibleForTesting
    @SuppressLint({"ValidFragment"})
    RequestManagerFragment(@NonNull ActivityFragmentLifecycle aVar) {
        this.f1560Q = new C1101a();
        this.f1561R = new HashSet();
        this.f1559P = aVar;
    }

    /* renamed from: b */
    private void m2416b(RequestManagerFragment requestManagerFragment) {
        this.f1561R.remove(requestManagerFragment);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public ActivityFragmentLifecycle mo10480a() {
        return this.f1559P;
    }

    /* renamed from: a */
    private void m2415a(RequestManagerFragment requestManagerFragment) {
        this.f1561R.add(requestManagerFragment);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10481a(@Nullable Fragment fragment) {
        this.f1564U = super;
        if (fragment != null && super.getActivity() != null) {
            m2414a(super.getActivity());
        }
    }

    /* renamed from: a */
    private void m2414a(@NonNull Activity activity) {
        m2418e();
        this.f1563T = Glide.m1279b(activity).mo9899h().mo10512b(activity);
        if (!equals(this.f1563T)) {
            this.f1563T.m2415a(this);
        }
    }
}
