package com.bumptech.glide.util;

import androidx.collection.ArrayMap;
import androidx.collection.SimpleArrayMap;

public final class CachedHashCodeArrayMap<K, V> extends ArrayMap<K, V> {

    /* renamed from: P */
    private int f1766P;

    public void clear() {
        this.f1766P = 0;
        super.clear();
    }

    public int hashCode() {
        if (this.f1766P == 0) {
            this.f1766P = super.hashCode();
        }
        return this.f1766P;
    }

    public V put(K k, V v) {
        this.f1766P = 0;
        return super.put(k, v);
    }

    public void putAll(SimpleArrayMap<? extends K, ? extends V> simpleArrayMap) {
        this.f1766P = 0;
        super.putAll(simpleArrayMap);
    }

    public V removeAt(int i) {
        this.f1766P = 0;
        return super.removeAt(i);
    }

    public V setValueAt(int i, V v) {
        this.f1766P = 0;
        return super.setValueAt(i, v);
    }
}
