package com.bumptech.glide.load;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: com.bumptech.glide.load.b */
public final class DecodeFormat extends Enum<DecodeFormat> {

    /* renamed from: P */
    public static final DecodeFormat f920P = new DecodeFormat("PREFER_ARGB_8888", 0);

    /* renamed from: Q */
    public static final DecodeFormat f921Q = new DecodeFormat("PREFER_RGB_565", 1);

    /* renamed from: R */
    public static final DecodeFormat f922R;

    /* renamed from: S */
    private static final /* synthetic */ DecodeFormat[] f923S;

    static {
        DecodeFormat bVar = f920P;
        f923S = new DecodeFormat[]{bVar, f921Q};
        f922R = bVar;
    }

    private DecodeFormat(String str, int i) {
    }

    public static DecodeFormat valueOf(String str) {
        return (DecodeFormat) Enum.valueOf(DecodeFormat.class, str);
    }

    public static DecodeFormat[] values() {
        return (DecodeFormat[]) f923S.clone();
    }
}
