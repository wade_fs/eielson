package com.bumptech.glide.load.resource.gif;

import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.File;
import java.io.IOException;

/* renamed from: com.bumptech.glide.load.resource.gif.c */
public class GifDrawableEncoder implements ResourceEncoder<GifDrawable> {
    /* renamed from: a */
    public /* bridge */ /* synthetic */ boolean mo9965a(@NonNull Object obj, @NonNull File file, @NonNull Options iVar) {
        return mo10438a((Resource<GifDrawable>) ((Resource) obj), file, iVar);
    }

    @NonNull
    /* renamed from: a */
    public EncodeStrategy mo9981a(@NonNull Options iVar) {
        return EncodeStrategy.SOURCE;
    }

    /* renamed from: a */
    public boolean mo10438a(@NonNull Resource<GifDrawable> vVar, @NonNull File file, @NonNull Options iVar) {
        try {
            ByteBufferUtil.m2803a(vVar.get().mo10406b(), file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("GifEncoder", 5)) {
                Log.w("GifEncoder", "Failed to encode GIF drawable data", e);
            }
            return false;
        }
    }
}
