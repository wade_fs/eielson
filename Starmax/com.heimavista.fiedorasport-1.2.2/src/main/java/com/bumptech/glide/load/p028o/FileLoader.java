package com.bumptech.glide.load.p028o;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import com.google.android.exoplayer2.C1750C;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.f */
public class FileLoader<Data> implements ModelLoader<File, Data> {

    /* renamed from: a */
    private final C1026d<Data> f1300a;

    /* renamed from: com.bumptech.glide.load.o.f$a */
    /* compiled from: FileLoader */
    public static class C1022a<Data> implements ModelLoaderFactory<File, Data> {

        /* renamed from: a */
        private final C1026d<Data> f1301a;

        public C1022a(C1026d<Data> dVar) {
            this.f1301a = dVar;
        }

        @NonNull
        /* renamed from: a */
        public final ModelLoader<File, Data> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new FileLoader(this.f1301a);
        }
    }

    /* renamed from: com.bumptech.glide.load.o.f$b */
    /* compiled from: FileLoader */
    public static class C1023b extends C1022a<ParcelFileDescriptor> {
        public C1023b() {
            super(new C1024a());
        }

        /* renamed from: com.bumptech.glide.load.o.f$b$a */
        /* compiled from: FileLoader */
        class C1024a implements C1026d<ParcelFileDescriptor> {
            C1024a() {
            }

            /* renamed from: a */
            public ParcelFileDescriptor m1926a(File file) {
                return ParcelFileDescriptor.open(file, C1750C.ENCODING_PCM_MU_LAW);
            }

            /* renamed from: a */
            public void mo10282a(ParcelFileDescriptor parcelFileDescriptor) {
                parcelFileDescriptor.close();
            }

            /* renamed from: a */
            public Class<ParcelFileDescriptor> mo10280a() {
                return ParcelFileDescriptor.class;
            }
        }
    }

    /* renamed from: com.bumptech.glide.load.o.f$d */
    /* compiled from: FileLoader */
    public interface C1026d<Data> {
        /* renamed from: a */
        Class<Data> mo10280a();

        /* renamed from: a */
        Data mo10279a(File file);

        /* renamed from: a */
        void mo10282a(Data data);
    }

    /* renamed from: com.bumptech.glide.load.o.f$e */
    /* compiled from: FileLoader */
    public static class C1027e extends C1022a<InputStream> {
        public C1027e() {
            super(new C1028a());
        }

        /* renamed from: com.bumptech.glide.load.o.f$e$a */
        /* compiled from: FileLoader */
        class C1028a implements C1026d<InputStream> {
            C1028a() {
            }

            /* renamed from: a */
            public InputStream m1938a(File file) {
                return new FileInputStream(file);
            }

            /* renamed from: a */
            public void mo10282a(InputStream inputStream) {
                inputStream.close();
            }

            /* renamed from: a */
            public Class<InputStream> mo10280a() {
                return InputStream.class;
            }
        }
    }

    public FileLoader(C1026d<Data> dVar) {
        this.f1300a = dVar;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull File file) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull File file, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(file), new C1025c(file, this.f1300a));
    }

    /* renamed from: com.bumptech.glide.load.o.f$c */
    /* compiled from: FileLoader */
    private static final class C1025c<Data> implements DataFetcher<Data> {

        /* renamed from: P */
        private final File f1302P;

        /* renamed from: Q */
        private final C1026d<Data> f1303Q;

        /* renamed from: R */
        private Data f1304R;

        C1025c(File file, C1026d<Data> dVar) {
            this.f1302P = file;
            this.f1303Q = dVar;
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super Data> aVar) {
            try {
                this.f1304R = this.f1303Q.mo10279a(this.f1302P);
                aVar.mo9999a((Object) this.f1304R);
            } catch (FileNotFoundException e) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e);
                }
                aVar.mo9998a((Exception) e);
            }
        }

        /* renamed from: b */
        public void mo9990b() {
            Data data = this.f1304R;
            if (data != null) {
                try {
                    this.f1303Q.mo10282a((Object) data);
                } catch (IOException unused) {
                }
            }
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        @NonNull
        /* renamed from: a */
        public Class<Data> mo9984a() {
            return this.f1303Q.mo10280a();
        }
    }
}
