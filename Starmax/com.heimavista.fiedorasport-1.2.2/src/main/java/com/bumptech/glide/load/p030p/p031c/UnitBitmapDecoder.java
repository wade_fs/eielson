package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.C1122j;

/* renamed from: com.bumptech.glide.load.p.c.w */
public final class UnitBitmapDecoder implements ResourceDecoder<Bitmap, Bitmap> {

    /* renamed from: com.bumptech.glide.load.p.c.w$a */
    /* compiled from: UnitBitmapDecoder */
    private static final class C1079a implements Resource<Bitmap> {

        /* renamed from: P */
        private final Bitmap f1450P;

        C1079a(@NonNull Bitmap bitmap) {
            this.f1450P = bitmap;
        }

        /* renamed from: a */
        public int mo10226a() {
            return C1122j.m2838a(this.f1450P);
        }

        @NonNull
        /* renamed from: b */
        public Class<Bitmap> mo10228b() {
            return Bitmap.class;
        }

        public void recycle() {
        }

        @NonNull
        public Bitmap get() {
            return this.f1450P;
        }
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull Bitmap bitmap, @NonNull Options iVar) {
        return true;
    }

    /* renamed from: a */
    public Resource<Bitmap> mo9979a(@NonNull Bitmap bitmap, int i, int i2, @NonNull Options iVar) {
        return new C1079a(bitmap);
    }
}
