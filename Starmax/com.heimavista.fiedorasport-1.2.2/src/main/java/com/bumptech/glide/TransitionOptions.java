package com.bumptech.glide;

import com.bumptech.glide.TransitionOptions;
import com.bumptech.glide.p040q.p042m.NoTransition;
import com.bumptech.glide.p040q.p042m.TransitionFactory;

/* renamed from: com.bumptech.glide.l */
public abstract class TransitionOptions<CHILD extends TransitionOptions<CHILD, TranscodeType>, TranscodeType> implements Cloneable {

    /* renamed from: P */
    private TransitionFactory<? super TranscodeType> f912P = NoTransition.m2793a();

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final TransitionFactory<? super TranscodeType> mo9959a() {
        return this.f912P;
    }

    public final CHILD clone() {
        try {
            return (TransitionOptions) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }
}
