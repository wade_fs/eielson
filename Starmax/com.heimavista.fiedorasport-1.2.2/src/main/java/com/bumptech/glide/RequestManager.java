package com.bumptech.glide;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import androidx.annotation.CheckResult;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.DiskCacheStrategy;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.Lifecycle;
import com.bumptech.glide.manager.LifecycleListener;
import com.bumptech.glide.manager.RequestManagerTreeNode;
import com.bumptech.glide.manager.RequestTracker;
import com.bumptech.glide.manager.TargetTracker;
import com.bumptech.glide.p040q.BaseRequestOptions;
import com.bumptech.glide.p040q.Request;
import com.bumptech.glide.p040q.RequestListener;
import com.bumptech.glide.p040q.RequestOptions;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.util.C1122j;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* renamed from: com.bumptech.glide.k */
public class RequestManager implements LifecycleListener, ModelTypes<RequestBuilder<Drawable>> {

    /* renamed from: a0 */
    private static final RequestOptions f897a0;

    /* renamed from: P */
    protected final Glide f898P;

    /* renamed from: Q */
    protected final Context f899Q;

    /* renamed from: R */
    final Lifecycle f900R;
    @GuardedBy("this")

    /* renamed from: S */
    private final RequestTracker f901S;
    @GuardedBy("this")

    /* renamed from: T */
    private final RequestManagerTreeNode f902T;
    @GuardedBy("this")

    /* renamed from: U */
    private final TargetTracker f903U;

    /* renamed from: V */
    private final Runnable f904V;

    /* renamed from: W */
    private final Handler f905W;

    /* renamed from: X */
    private final ConnectivityMonitor f906X;

    /* renamed from: Y */
    private final CopyOnWriteArrayList<RequestListener<Object>> f907Y;
    @GuardedBy("this")

    /* renamed from: Z */
    private RequestOptions f908Z;

    /* renamed from: com.bumptech.glide.k$a */
    /* compiled from: RequestManager */
    class C0930a implements Runnable {
        C0930a() {
        }

        public void run() {
            RequestManager kVar = RequestManager.this;
            kVar.f900R.mo10499a(kVar);
        }
    }

    /* renamed from: com.bumptech.glide.k$b */
    /* compiled from: RequestManager */
    private class C0931b implements ConnectivityMonitor.C1103a {
        @GuardedBy("RequestManager.this")

        /* renamed from: a */
        private final RequestTracker f910a;

        C0931b(@NonNull RequestTracker mVar) {
            this.f910a = mVar;
        }

        /* renamed from: a */
        public void mo9958a(boolean z) {
            if (z) {
                synchronized (RequestManager.this) {
                    this.f910a.mo10520c();
                }
            }
        }
    }

    static {
        RequestOptions b = RequestOptions.m2679b(Bitmap.class);
        b.mo10574E();
        f897a0 = b;
        RequestOptions.m2679b(GifDrawable.class).mo10574E();
        RequestOptions hVar = (RequestOptions) ((RequestOptions) RequestOptions.m2678b(DiskCacheStrategy.f1166b).mo10582a(Priority.LOW)).mo10592a(true);
    }

    public RequestManager(@NonNull Glide cVar, @NonNull Lifecycle hVar, @NonNull RequestManagerTreeNode lVar, @NonNull Context context) {
        this(cVar, hVar, lVar, new RequestTracker(), cVar.mo9895d(), context);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized void mo9943a(@NonNull RequestOptions hVar) {
        RequestOptions hVar2 = (RequestOptions) hVar.clone();
        hVar2.mo10578a();
        this.f908Z = hVar2;
    }

    /* renamed from: b */
    public synchronized void mo9947b() {
        this.f903U.mo9947b();
        for (Target<?> iVar : this.f903U.mo10526e()) {
            mo9944a(iVar);
        }
        this.f903U.mo10525d();
        this.f901S.mo10516a();
        this.f900R.mo10501b(this);
        this.f900R.mo10501b(this.f906X);
        this.f905W.removeCallbacks(this.f904V);
        this.f898P.mo9893b(this);
    }

    /* renamed from: c */
    public synchronized void mo9949c() {
        mo9953g();
        this.f903U.mo9949c();
    }

    @CheckResult
    @NonNull
    /* renamed from: d */
    public RequestBuilder<Bitmap> mo9950d() {
        return mo9942a(Bitmap.class).mo9932a((BaseRequestOptions<?>) f897a0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public List<RequestListener<Object>> mo9951e() {
        return this.f907Y;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public synchronized RequestOptions mo9952f() {
        return this.f908Z;
    }

    /* renamed from: g */
    public synchronized void mo9953g() {
        this.f901S.mo10518b();
    }

    /* renamed from: h */
    public synchronized void mo9954h() {
        this.f901S.mo10521d();
    }

    public synchronized void onStart() {
        mo9954h();
        this.f903U.onStart();
    }

    public synchronized String toString() {
        return super.toString() + "{tracker=" + this.f901S + ", treeNode=" + this.f902T + "}";
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public <ResourceType> RequestBuilder<ResourceType> mo9942a(@NonNull Class<ResourceType> cls) {
        return new RequestBuilder<>(this.f898P, this, cls, this.f899Q);
    }

    RequestManager(Glide cVar, Lifecycle hVar, RequestManagerTreeNode lVar, RequestTracker mVar, ConnectivityMonitorFactory dVar, Context context) {
        this.f903U = new TargetTracker();
        this.f904V = new C0930a();
        this.f905W = new Handler(Looper.getMainLooper());
        this.f898P = cVar;
        this.f900R = hVar;
        this.f902T = lVar;
        this.f901S = mVar;
        this.f899Q = context;
        this.f906X = dVar.mo10503a(context.getApplicationContext(), new C0931b(mVar));
        if (C1122j.m2851c()) {
            this.f905W.post(this.f904V);
        } else {
            hVar.mo10499a(this);
        }
        hVar.mo10499a(this.f906X);
        this.f907Y = new CopyOnWriteArrayList<>(cVar.mo9897f().mo9908b());
        mo9943a(cVar.mo9897f().mo9909c());
        cVar.mo9890a(this);
    }

    /* renamed from: c */
    private void m1345c(@NonNull Target<?> iVar) {
        if (!mo9948b(iVar) && !this.f898P.mo9891a(iVar) && iVar.mo10638a() != null) {
            Request a = iVar.mo10638a();
            iVar.mo10640a((Request) null);
            a.clear();
        }
    }

    /* renamed from: a */
    public synchronized void mo9944a(@Nullable Target<?> iVar) {
        if (iVar != null) {
            m1345c(iVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo9945a(@NonNull Target<?> iVar, @NonNull Request dVar) {
        this.f903U.mo10523a(iVar);
        this.f901S.mo10519b(dVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public synchronized boolean mo9948b(@NonNull Target<?> iVar) {
        Request a = iVar.mo10638a();
        if (a == null) {
            return true;
        }
        if (!this.f901S.mo10517a(a)) {
            return false;
        }
        this.f903U.mo10524b(iVar);
        iVar.mo10640a((Request) null);
        return true;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: b */
    public <T> TransitionOptions<?, T> mo9946b(Class cls) {
        return this.f898P.mo9897f().mo9905a(cls);
    }
}
