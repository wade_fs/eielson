package com.bumptech.glide.util.p044k;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.util.Pools;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.util.k.a */
public final class FactoryPools {

    /* renamed from: a */
    private static final C1130g<Object> f1791a = new C1124a();

    /* renamed from: com.bumptech.glide.util.k.a$a */
    /* compiled from: FactoryPools */
    class C1124a implements C1130g<Object> {
        C1124a() {
        }

        /* renamed from: a */
        public void mo10714a(@NonNull Object obj) {
        }
    }

    /* renamed from: com.bumptech.glide.util.k.a$b */
    /* compiled from: FactoryPools */
    class C1125b implements C1127d<List<T>> {
        C1125b() {
        }

        @NonNull
        /* renamed from: a */
        public List<T> mo10114a() {
            return new ArrayList();
        }
    }

    /* renamed from: com.bumptech.glide.util.k.a$c */
    /* compiled from: FactoryPools */
    class C1126c implements C1130g<List<T>> {
        C1126c() {
        }

        /* renamed from: a */
        public void mo10714a(@NonNull List<T> list) {
            list.clear();
        }
    }

    /* renamed from: com.bumptech.glide.util.k.a$d */
    /* compiled from: FactoryPools */
    public interface C1127d<T> {
        /* renamed from: a */
        T mo10114a();
    }

    /* renamed from: com.bumptech.glide.util.k.a$e */
    /* compiled from: FactoryPools */
    private static final class C1128e<T> implements Pools.Pool<T> {

        /* renamed from: a */
        private final C1127d<T> f1792a;

        /* renamed from: b */
        private final C1130g<T> f1793b;

        /* renamed from: c */
        private final Pools.Pool<T> f1794c;

        C1128e(@NonNull Pools.Pool<T> pool, @NonNull C1127d<T> dVar, @NonNull C1130g<T> gVar) {
            this.f1794c = pool;
            this.f1792a = dVar;
            this.f1793b = gVar;
        }

        public T acquire() {
            T acquire = this.f1794c.acquire();
            if (acquire == null) {
                acquire = this.f1792a.mo10114a();
                if (Log.isLoggable("FactoryPools", 2)) {
                    Log.v("FactoryPools", "Created new " + acquire.getClass());
                }
            }
            if (acquire instanceof C1129f) {
                ((C1129f) acquire).mo10115d().mo10717a(false);
            }
            return acquire;
        }

        public boolean release(@NonNull T t) {
            if (t instanceof C1129f) {
                ((C1129f) t).mo10115d().mo10717a(true);
            }
            this.f1793b.mo10714a(t);
            return this.f1794c.release(t);
        }
    }

    /* renamed from: com.bumptech.glide.util.k.a$f */
    /* compiled from: FactoryPools */
    public interface C1129f {
        @NonNull
        /* renamed from: d */
        StateVerifier mo10115d();
    }

    /* renamed from: com.bumptech.glide.util.k.a$g */
    /* compiled from: FactoryPools */
    public interface C1130g<T> {
        /* renamed from: a */
        void mo10714a(@NonNull T t);
    }

    @NonNull
    /* renamed from: a */
    public static <T extends C1129f> Pools.Pool<T> m2854a(int i, @NonNull C1127d dVar) {
        return m2855a(new Pools.SynchronizedPool(i), dVar);
    }

    @NonNull
    /* renamed from: b */
    public static <T> Pools.Pool<List<T>> m2858b() {
        return m2853a(20);
    }

    @NonNull
    /* renamed from: a */
    public static <T> Pools.Pool<List<T>> m2853a(int i) {
        return m2856a(new Pools.SynchronizedPool(i), new C1125b(), new C1126c());
    }

    @NonNull
    /* renamed from: a */
    private static <T extends C1129f> Pools.Pool<T> m2855a(@NonNull Pools.Pool pool, @NonNull C1127d dVar) {
        return m2856a(pool, dVar, m2857a());
    }

    @NonNull
    /* renamed from: a */
    private static <T> Pools.Pool<T> m2856a(@NonNull Pools.Pool<T> pool, @NonNull C1127d<T> dVar, @NonNull C1130g<T> gVar) {
        return new C1128e(pool, dVar, gVar);
    }

    @NonNull
    /* renamed from: a */
    private static <T> C1130g<T> m2857a() {
        return f1791a;
    }
}
