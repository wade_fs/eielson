package com.bumptech.glide.p039p;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Encoder;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.p.a */
public class EncoderRegistry {

    /* renamed from: a */
    private final List<C1108a<?>> f1645a = new ArrayList();

    /* renamed from: com.bumptech.glide.p.a$a */
    /* compiled from: EncoderRegistry */
    private static final class C1108a<T> {

        /* renamed from: a */
        private final Class<T> f1646a;

        /* renamed from: b */
        final Encoder<T> f1647b;

        C1108a(@NonNull Class<T> cls, @NonNull Encoder<T> dVar) {
            this.f1646a = cls;
            this.f1647b = dVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo10554a(@NonNull Class<?> cls) {
            return this.f1646a.isAssignableFrom(cls);
        }
    }

    @Nullable
    /* renamed from: a */
    public synchronized <T> Encoder<T> mo10552a(@NonNull Class<T> cls) {
        for (C1108a aVar : this.f1645a) {
            if (aVar.mo10554a(cls)) {
                return aVar.f1647b;
            }
        }
        return null;
    }

    /* renamed from: a */
    public synchronized <T> void mo10553a(@NonNull Class<T> cls, @NonNull Encoder<T> dVar) {
        this.f1645a.add(new C1108a(cls, dVar));
    }
}
