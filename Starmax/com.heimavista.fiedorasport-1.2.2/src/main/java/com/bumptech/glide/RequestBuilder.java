package com.bumptech.glide;

import android.annotation.SuppressLint;
import android.content.Context;
import android.widget.ImageView;
import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.DiskCacheStrategy;
import com.bumptech.glide.p040q.BaseRequestOptions;
import com.bumptech.glide.p040q.ErrorRequestCoordinator;
import com.bumptech.glide.p040q.FutureTarget;
import com.bumptech.glide.p040q.Request;
import com.bumptech.glide.p040q.RequestCoordinator;
import com.bumptech.glide.p040q.RequestFutureTarget;
import com.bumptech.glide.p040q.RequestListener;
import com.bumptech.glide.p040q.RequestOptions;
import com.bumptech.glide.p040q.ThumbnailRequestCoordinator;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.p040q.p041l.ViewTarget;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.Executors;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: com.bumptech.glide.j */
public class RequestBuilder<TranscodeType> extends BaseRequestOptions<RequestBuilder<TranscodeType>> implements Cloneable, ModelTypes<RequestBuilder<TranscodeType>> {

    /* renamed from: A0 */
    private boolean f882A0;

    /* renamed from: B0 */
    private boolean f883B0;

    /* renamed from: p0 */
    private final Context f884p0;

    /* renamed from: q0 */
    private final RequestManager f885q0;

    /* renamed from: r0 */
    private final Class<TranscodeType> f886r0;

    /* renamed from: s0 */
    private final GlideContext f887s0;
    @NonNull

    /* renamed from: t0 */
    private TransitionOptions<?, ? super TranscodeType> f888t0;
    @Nullable

    /* renamed from: u0 */
    private Object f889u0;
    @Nullable

    /* renamed from: v0 */
    private List<RequestListener<TranscodeType>> f890v0;
    @Nullable

    /* renamed from: w0 */
    private RequestBuilder<TranscodeType> f891w0;
    @Nullable

    /* renamed from: x0 */
    private RequestBuilder<TranscodeType> f892x0;
    @Nullable

    /* renamed from: y0 */
    private Float f893y0;

    /* renamed from: z0 */
    private boolean f894z0 = true;

    /* renamed from: com.bumptech.glide.j$a */
    /* compiled from: RequestBuilder */
    static /* synthetic */ class C0929a {

        /* renamed from: a */
        static final /* synthetic */ int[] f895a = new int[ImageView.ScaleType.values().length];

        /* renamed from: b */
        static final /* synthetic */ int[] f896b = new int[Priority.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|(3:31|32|34)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Can't wrap try/catch for region: R(29:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|34) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        static {
            /*
                com.bumptech.glide.h[] r0 = com.bumptech.glide.Priority.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.bumptech.glide.RequestBuilder.C0929a.f896b = r0
                r0 = 1
                int[] r1 = com.bumptech.glide.RequestBuilder.C0929a.f896b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.bumptech.glide.h r2 = com.bumptech.glide.Priority.LOW     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.bumptech.glide.RequestBuilder.C0929a.f896b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.bumptech.glide.h r3 = com.bumptech.glide.Priority.NORMAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = com.bumptech.glide.RequestBuilder.C0929a.f896b     // Catch:{ NoSuchFieldError -> 0x002a }
                com.bumptech.glide.h r4 = com.bumptech.glide.Priority.HIGH     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = com.bumptech.glide.RequestBuilder.C0929a.f896b     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.bumptech.glide.h r5 = com.bumptech.glide.Priority.IMMEDIATE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                android.widget.ImageView$ScaleType[] r4 = android.widget.ImageView.ScaleType.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                com.bumptech.glide.RequestBuilder.C0929a.f895a = r4
                int[] r4 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x0048 }
                android.widget.ImageView$ScaleType r5 = android.widget.ImageView.ScaleType.CENTER_CROP     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x0052 }
                android.widget.ImageView$ScaleType r4 = android.widget.ImageView.ScaleType.CENTER_INSIDE     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x005c }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x0066 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_START     // Catch:{ NoSuchFieldError -> 0x0066 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
            L_0x0066:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x0071 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_END     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x007c }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.FIT_XY     // Catch:{ NoSuchFieldError -> 0x007c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
            L_0x007c:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x0087 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.CENTER     // Catch:{ NoSuchFieldError -> 0x0087 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0087 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0087 }
            L_0x0087:
                int[] r0 = com.bumptech.glide.RequestBuilder.C0929a.f895a     // Catch:{ NoSuchFieldError -> 0x0093 }
                android.widget.ImageView$ScaleType r1 = android.widget.ImageView.ScaleType.MATRIX     // Catch:{ NoSuchFieldError -> 0x0093 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0093 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0093 }
            L_0x0093:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.RequestBuilder.C0929a.<clinit>():void");
        }
    }

    static {
        RequestOptions hVar = (RequestOptions) ((RequestOptions) ((RequestOptions) new RequestOptions().mo10587a(DiskCacheStrategy.f1166b)).mo10582a(Priority.LOW)).mo10592a(true);
    }

    @SuppressLint({"CheckResult"})
    protected RequestBuilder(@NonNull Glide cVar, RequestManager kVar, Class<TranscodeType> cls, Context context) {
        this.f885q0 = kVar;
        this.f886r0 = cls;
        this.f884p0 = context;
        this.f888t0 = kVar.mo9946b(cls);
        this.f887s0 = cVar.mo9897f();
        m1328a(kVar.mo9951e());
        mo9932a((BaseRequestOptions<?>) kVar.mo9952f());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.j.b(int, int):com.bumptech.glide.q.c<TranscodeType>
     arg types: [int, int]
     candidates:
      com.bumptech.glide.q.a.b(int, int):boolean
      com.bumptech.glide.q.a.b(com.bumptech.glide.load.p.c.k, com.bumptech.glide.load.l<android.graphics.Bitmap>):T
      com.bumptech.glide.j.b(int, int):com.bumptech.glide.q.c<TranscodeType> */
    @NonNull
    /* renamed from: I */
    public FutureTarget<TranscodeType> mo9931I() {
        return mo9940b(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    @CheckResult
    @NonNull
    /* renamed from: b */
    public RequestBuilder<TranscodeType> mo9939b(@Nullable RequestListener<TranscodeType> gVar) {
        this.f890v0 = null;
        mo9933a((RequestListener) gVar);
        return this;
    }

    @SuppressLint({"CheckResult"})
    /* renamed from: a */
    private void m1328a(List<RequestListener<Object>> list) {
        for (RequestListener<Object> gVar : list) {
            mo9933a((RequestListener) gVar);
        }
    }

    @NonNull
    /* renamed from: b */
    private RequestBuilder<TranscodeType> m1331b(@Nullable Object obj) {
        this.f889u0 = obj;
        this.f882A0 = true;
        return this;
    }

    @CheckResult
    public RequestBuilder<TranscodeType> clone() {
        RequestBuilder<TranscodeType> jVar = (RequestBuilder) super.clone();
        jVar.f888t0 = jVar.f888t0.clone();
        return jVar;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public RequestBuilder<TranscodeType> mo9932a(@NonNull BaseRequestOptions<?> aVar) {
        Preconditions.m2828a(aVar);
        return (RequestBuilder) super.mo9932a(super);
    }

    /* renamed from: b */
    private <Y extends Target<TranscodeType>> Y m1333b(@NonNull Y y, @Nullable RequestListener<TranscodeType> gVar, BaseRequestOptions<?> aVar, Executor executor) {
        Preconditions.m2828a((Object) y);
        if (this.f882A0) {
            Request a = m1326a(y, gVar, super, executor);
            Request a2 = y.mo10638a();
            if (!a.mo10624a(a2) || m1329a(super, a2)) {
                this.f885q0.mo9944a((Target<?>) y);
                y.mo10640a(a);
                this.f885q0.mo9945a(y, a);
                return y;
            }
            a.recycle();
            Preconditions.m2828a(a2);
            if (!a2.isRunning()) {
                a2.mo10626c();
            }
            return y;
        }
        throw new IllegalArgumentException("You must call #load() before calling #into()");
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public RequestBuilder<TranscodeType> mo9933a(@Nullable RequestListener gVar) {
        if (gVar != null) {
            if (this.f890v0 == null) {
                this.f890v0 = new ArrayList();
            }
            this.f890v0.add(gVar);
        }
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public RequestBuilder<TranscodeType> mo9934a(@Nullable Object obj) {
        m1331b(obj);
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public RequestBuilder<TranscodeType> mo9935a(@Nullable String str) {
        m1331b(str);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public <Y extends Target<TranscodeType>> Y mo9936a(@NonNull Target iVar) {
        mo9937a(iVar, (RequestListener) null, Executors.m2813b());
        return iVar;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public <Y extends Target<TranscodeType>> Y mo9937a(@NonNull Y y, @Nullable RequestListener<TranscodeType> gVar, Executor executor) {
        m1333b(y, gVar, super, executor);
        return y;
    }

    /* renamed from: a */
    private boolean m1329a(BaseRequestOptions<?> aVar, Request dVar) {
        return !super.mo10619x() && dVar.mo10635h();
    }

    @NonNull
    /* renamed from: a */
    public ViewTarget<ImageView, TranscodeType> mo9938a(@NonNull ImageView imageView) {
        BaseRequestOptions aVar;
        C1122j.m2847b();
        Preconditions.m2828a(imageView);
        if (!mo10572C() && mo10570A() && imageView.getScaleType() != null) {
            switch (C0929a.f895a[imageView.getScaleType().ordinal()]) {
                case 1:
                    aVar = clone().mo10575F();
                    break;
                case 2:
                    aVar = clone().mo10576G();
                    break;
                case 3:
                case 4:
                case 5:
                    aVar = clone().mo10577H();
                    break;
                case 6:
                    aVar = clone().mo10576G();
                    break;
            }
            ViewTarget<ImageView, TranscodeType> a = this.f887s0.mo9907a(imageView, this.f886r0);
            m1333b(a, null, super, Executors.m2813b());
            return a;
        }
        aVar = this;
        ViewTarget<ImageView, TranscodeType> a2 = this.f887s0.mo9907a(imageView, this.f886r0);
        m1333b(a2, null, super, Executors.m2813b());
        return a2;
    }

    @NonNull
    /* renamed from: b */
    public FutureTarget<TranscodeType> mo9940b(int i, int i2) {
        RequestFutureTarget fVar = new RequestFutureTarget(i, i2);
        mo9937a(fVar, fVar, Executors.m2812a());
        return fVar;
    }

    @NonNull
    /* renamed from: b */
    private Priority m1330b(@NonNull Priority hVar) {
        int i = C0929a.f896b[hVar.ordinal()];
        if (i == 1) {
            return Priority.NORMAL;
        }
        if (i == 2) {
            return Priority.HIGH;
        }
        if (i == 3 || i == 4) {
            return Priority.IMMEDIATE;
        }
        throw new IllegalArgumentException("unknown priority: " + mo10611n());
    }

    /* JADX WARN: Type inference failed for: r0v5, types: [com.bumptech.glide.q.a] */
    /* renamed from: b */
    private Request m1332b(Target<TranscodeType> iVar, RequestListener<TranscodeType> gVar, @Nullable RequestCoordinator eVar, TransitionOptions<?, ? super TranscodeType> lVar, Priority hVar, int i, int i2, BaseRequestOptions<?> superR, Executor executor) {
        RequestCoordinator eVar2 = eVar;
        Priority hVar2 = hVar;
        RequestBuilder<TranscodeType> jVar = this.f891w0;
        if (jVar != null) {
            if (!this.f883B0) {
                TransitionOptions<?, ? super TranscodeType> lVar2 = jVar.f894z0 ? lVar : jVar.f888t0;
                Priority n = this.f891w0.mo10620y() ? this.f891w0.mo10611n() : m1330b(hVar2);
                int k = this.f891w0.mo10608k();
                int j = this.f891w0.mo10607j();
                if (C1122j.m2849b(i, i2) && !this.f891w0.mo10573D()) {
                    k = super.mo10608k();
                    j = super.mo10607j();
                }
                int i3 = k;
                int i4 = j;
                ThumbnailRequestCoordinator kVar = new ThumbnailRequestCoordinator(eVar2);
                Request a = m1325a(iVar, gVar, superR, kVar, lVar, hVar, i, i2, executor);
                this.f883B0 = true;
                RequestBuilder<TranscodeType> superR2 = this.f891w0;
                ThumbnailRequestCoordinator kVar2 = kVar;
                Request a2 = superR2.m1327a(iVar, gVar, kVar, lVar2, n, i3, i4, superR2, executor);
                this.f883B0 = false;
                kVar2.mo10657a(a, a2);
                return kVar2;
            }
            throw new IllegalStateException("You cannot use a request as both the main request and a thumbnail, consider using clone() on the request(s) passed to thumbnail()");
        } else if (this.f893y0 == null) {
            return m1325a(iVar, gVar, superR, eVar, lVar, hVar, i, i2, executor);
        } else {
            ThumbnailRequestCoordinator kVar3 = new ThumbnailRequestCoordinator(eVar2);
            RequestListener<TranscodeType> gVar2 = gVar;
            ThumbnailRequestCoordinator kVar4 = kVar3;
            TransitionOptions<?, ? super TranscodeType> lVar3 = lVar;
            int i5 = i;
            int i6 = i2;
            Executor executor2 = executor;
            kVar3.mo10657a(m1325a(iVar, gVar2, superR, kVar4, lVar3, hVar, i5, i6, executor2), m1325a(iVar, gVar2, super.clone().mo10579a(this.f893y0.floatValue()), kVar4, lVar3, m1330b(hVar2), i5, i6, executor2));
            return kVar3;
        }
    }

    /* renamed from: a */
    private Request m1326a(Target<TranscodeType> iVar, @Nullable RequestListener<TranscodeType> gVar, BaseRequestOptions<?> superR, Executor executor) {
        return m1327a(iVar, gVar, (RequestCoordinator) null, this.f888t0, super.mo10611n(), super.mo10608k(), super.mo10607j(), superR, executor);
    }

    /* renamed from: a */
    private Request m1327a(Target<TranscodeType> iVar, @Nullable RequestListener<TranscodeType> gVar, @Nullable RequestCoordinator eVar, TransitionOptions<?, ? super TranscodeType> lVar, Priority hVar, int i, int i2, BaseRequestOptions<?> superR, Executor executor) {
        ErrorRequestCoordinator bVar;
        ErrorRequestCoordinator bVar2;
        if (this.f892x0 != null) {
            bVar2 = new ErrorRequestCoordinator(eVar);
            bVar = bVar2;
        } else {
            bVar = null;
            bVar2 = eVar;
        }
        Request b = m1332b(iVar, gVar, bVar2, lVar, hVar, i, i2, superR, executor);
        if (bVar == null) {
            return b;
        }
        int k = this.f892x0.mo10608k();
        int j = this.f892x0.mo10607j();
        if (C1122j.m2849b(i, i2) && !this.f892x0.mo10573D()) {
            k = super.mo10608k();
            j = super.mo10607j();
        }
        RequestBuilder<TranscodeType> jVar = this.f892x0;
        ErrorRequestCoordinator bVar3 = bVar;
        bVar3.mo10622a(b, jVar.m1327a(iVar, gVar, bVar, jVar.f888t0, super.mo10611n(), k, j, this.f892x0, executor));
        return bVar3;
    }

    /* JADX WARN: Type inference failed for: r21v0, types: [com.bumptech.glide.l<?, ? super TranscodeType>, com.bumptech.glide.l] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.bumptech.glide.p040q.Request m1325a(com.bumptech.glide.p040q.p041l.Target<TranscodeType> r17, com.bumptech.glide.p040q.RequestListener<TranscodeType> r18, com.bumptech.glide.p040q.BaseRequestOptions<?> r19, com.bumptech.glide.p040q.RequestCoordinator r20, com.bumptech.glide.TransitionOptions<?, ? super TranscodeType> r21, com.bumptech.glide.Priority r22, int r23, int r24, java.util.concurrent.Executor r25) {
        /*
            r16 = this;
            r0 = r16
            android.content.Context r1 = r0.f884p0
            com.bumptech.glide.e r2 = r0.f887s0
            java.lang.Object r3 = r0.f889u0
            java.lang.Class<TranscodeType> r4 = r0.f886r0
            java.util.List<com.bumptech.glide.q.g<TranscodeType>> r11 = r0.f890v0
            com.bumptech.glide.load.n.k r13 = r2.mo9910d()
            com.bumptech.glide.q.m.c r14 = r21.mo9959a()
            r5 = r19
            r6 = r23
            r7 = r24
            r8 = r22
            r9 = r17
            r10 = r18
            r12 = r20
            r15 = r25
            com.bumptech.glide.q.j r1 = com.bumptech.glide.p040q.SingleRequest.m2691b(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.RequestBuilder.m1325a(com.bumptech.glide.q.l.i, com.bumptech.glide.q.g, com.bumptech.glide.q.a, com.bumptech.glide.q.e, com.bumptech.glide.l, com.bumptech.glide.h, int, int, java.util.concurrent.Executor):com.bumptech.glide.q.d");
    }
}
