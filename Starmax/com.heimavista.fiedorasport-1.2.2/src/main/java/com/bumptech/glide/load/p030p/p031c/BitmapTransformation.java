package com.bumptech.glide.load.p030p.p031c;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.util.C1122j;

/* renamed from: com.bumptech.glide.load.p.c.e */
public abstract class BitmapTransformation implements Transformation<Bitmap> {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Bitmap mo10342a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2);

    @NonNull
    /* renamed from: a */
    public final Resource<Bitmap> mo9982a(@NonNull Context context, @NonNull Resource<Bitmap> vVar, int i, int i2) {
        if (C1122j.m2849b(i, i2)) {
            BitmapPool c = Glide.m1279b(context).mo9894c();
            Bitmap bitmap = vVar.get();
            if (i == Integer.MIN_VALUE) {
                i = bitmap.getWidth();
            }
            if (i2 == Integer.MIN_VALUE) {
                i2 = bitmap.getHeight();
            }
            Bitmap a = mo10342a(c, bitmap, i, i2);
            return bitmap.equals(a) ? vVar : BitmapResource.m2091a(a, c);
        }
        throw new IllegalArgumentException("Cannot apply transformation on width: " + i + " or height: " + i2 + " less than or equal to zero and not Target.SIZE_ORIGINAL");
    }
}
