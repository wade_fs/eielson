package com.bumptech.glide.load.p021m.p022o;

import android.database.Cursor;
import android.net.Uri;

/* renamed from: com.bumptech.glide.load.m.o.d */
interface ThumbnailQuery {
    /* renamed from: a */
    Cursor mo10021a(Uri uri);
}
