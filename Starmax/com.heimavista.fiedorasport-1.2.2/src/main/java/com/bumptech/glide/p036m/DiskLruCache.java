package com.bumptech.glide.p036m;

import com.facebook.appevents.AppEventsConstants;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: com.bumptech.glide.m.a */
public final class DiskLruCache implements Closeable {
    /* access modifiers changed from: private */

    /* renamed from: P */
    public final File f1524P;

    /* renamed from: Q */
    private final File f1525Q;

    /* renamed from: R */
    private final File f1526R;

    /* renamed from: S */
    private final File f1527S;

    /* renamed from: T */
    private final int f1528T;

    /* renamed from: U */
    private long f1529U;
    /* access modifiers changed from: private */

    /* renamed from: V */
    public final int f1530V;

    /* renamed from: W */
    private long f1531W = 0;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public Writer f1532X;

    /* renamed from: Y */
    private final LinkedHashMap<String, C1098d> f1533Y = new LinkedHashMap<>(0, 0.75f, true);
    /* access modifiers changed from: private */

    /* renamed from: Z */
    public int f1534Z;

    /* renamed from: a0 */
    private long f1535a0 = 0;

    /* renamed from: b0 */
    final ThreadPoolExecutor f1536b0 = new ThreadPoolExecutor(0, 1, 60, TimeUnit.SECONDS, new LinkedBlockingQueue(), new C1096b(null));

    /* renamed from: c0 */
    private final Callable<Void> f1537c0 = new C1095a();

    /* renamed from: com.bumptech.glide.m.a$a */
    /* compiled from: DiskLruCache */
    class C1095a implements Callable<Void> {
        C1095a() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0027, code lost:
            return null;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Void call() {
            /*
                r4 = this;
                com.bumptech.glide.m.a r0 = com.bumptech.glide.p036m.DiskLruCache.this
                monitor-enter(r0)
                com.bumptech.glide.m.a r1 = com.bumptech.glide.p036m.DiskLruCache.this     // Catch:{ all -> 0x0028 }
                java.io.Writer r1 = r1.f1532X     // Catch:{ all -> 0x0028 }
                r2 = 0
                if (r1 != 0) goto L_0x000e
                monitor-exit(r0)     // Catch:{ all -> 0x0028 }
                return r2
            L_0x000e:
                com.bumptech.glide.m.a r1 = com.bumptech.glide.p036m.DiskLruCache.this     // Catch:{ all -> 0x0028 }
                r1.m2382t()     // Catch:{ all -> 0x0028 }
                com.bumptech.glide.m.a r1 = com.bumptech.glide.p036m.DiskLruCache.this     // Catch:{ all -> 0x0028 }
                boolean r1 = r1.m2374c()     // Catch:{ all -> 0x0028 }
                if (r1 == 0) goto L_0x0026
                com.bumptech.glide.m.a r1 = com.bumptech.glide.p036m.DiskLruCache.this     // Catch:{ all -> 0x0028 }
                r1.m2381g()     // Catch:{ all -> 0x0028 }
                com.bumptech.glide.m.a r1 = com.bumptech.glide.p036m.DiskLruCache.this     // Catch:{ all -> 0x0028 }
                r3 = 0
                int unused = r1.f1534Z = r3     // Catch:{ all -> 0x0028 }
            L_0x0026:
                monitor-exit(r0)     // Catch:{ all -> 0x0028 }
                return r2
            L_0x0028:
                r1 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x0028 }
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p036m.DiskLruCache.C1095a.call():java.lang.Void");
        }
    }

    /* renamed from: com.bumptech.glide.m.a$b */
    /* compiled from: DiskLruCache */
    private static final class C1096b implements ThreadFactory {
        private C1096b() {
        }

        public synchronized Thread newThread(Runnable runnable) {
            Thread thread;
            thread = new Thread(runnable, "glide-disk-lru-cache-thread");
            thread.setPriority(1);
            return thread;
        }

        /* synthetic */ C1096b(C1095a aVar) {
            this();
        }
    }

    /* renamed from: com.bumptech.glide.m.a$c */
    /* compiled from: DiskLruCache */
    public final class C1097c {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final C1098d f1539a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final boolean[] f1540b;

        /* renamed from: c */
        private boolean f1541c;

        /* synthetic */ C1097c(DiskLruCache aVar, C1098d dVar, C1095a aVar2) {
            this(dVar);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.bumptech.glide.m.a.a(com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, boolean):void
         arg types: [com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, int]
         candidates:
          com.bumptech.glide.m.a.a(java.io.File, java.io.File, boolean):void
          com.bumptech.glide.m.a.a(com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, boolean):void */
        /* renamed from: c */
        public void mo10471c() {
            DiskLruCache.this.m2367a(this, true);
            this.f1541c = true;
        }

        private C1097c(C1098d dVar) {
            this.f1539a = dVar;
            this.f1540b = dVar.f1547e ? null : new boolean[DiskLruCache.this.f1530V];
        }

        /* renamed from: a */
        public File mo10468a(int i) {
            File b;
            synchronized (DiskLruCache.this) {
                if (this.f1539a.f1548f == this) {
                    if (!this.f1539a.f1547e) {
                        this.f1540b[i] = true;
                    }
                    b = this.f1539a.mo10474b(i);
                    if (!DiskLruCache.this.f1524P.exists()) {
                        DiskLruCache.this.f1524P.mkdirs();
                    }
                } else {
                    throw new IllegalStateException();
                }
            }
            return b;
        }

        /* renamed from: b */
        public void mo10470b() {
            if (!this.f1541c) {
                try {
                    mo10469a();
                } catch (IOException unused) {
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.bumptech.glide.m.a.a(com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, boolean):void
         arg types: [com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, int]
         candidates:
          com.bumptech.glide.m.a.a(java.io.File, java.io.File, boolean):void
          com.bumptech.glide.m.a.a(com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, boolean):void */
        /* renamed from: a */
        public void mo10469a() {
            DiskLruCache.this.m2367a(this, false);
        }
    }

    /* renamed from: com.bumptech.glide.m.a$d */
    /* compiled from: DiskLruCache */
    private final class C1098d {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public final String f1543a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final long[] f1544b;

        /* renamed from: c */
        File[] f1545c;

        /* renamed from: d */
        File[] f1546d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public boolean f1547e;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public C1097c f1548f;
        /* access modifiers changed from: private */

        /* renamed from: g */
        public long f1549g;

        /* synthetic */ C1098d(DiskLruCache aVar, String str, C1095a aVar2) {
            this(str);
        }

        private C1098d(String str) {
            this.f1543a = str;
            this.f1544b = new long[DiskLruCache.this.f1530V];
            this.f1545c = new File[DiskLruCache.this.f1530V];
            this.f1546d = new File[DiskLruCache.this.f1530V];
            StringBuilder sb = new StringBuilder(str);
            sb.append((char) JwtParser.SEPARATOR_CHAR);
            int length = sb.length();
            for (int i = 0; i < DiskLruCache.this.f1530V; i++) {
                sb.append(i);
                this.f1545c[i] = new File(DiskLruCache.this.f1524P, sb.toString());
                sb.append(".tmp");
                this.f1546d[i] = new File(DiskLruCache.this.f1524P, sb.toString());
                sb.setLength(length);
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public void m2400b(String[] strArr) {
            if (strArr.length == DiskLruCache.this.f1530V) {
                int i = 0;
                while (i < strArr.length) {
                    try {
                        this.f1544b[i] = Long.parseLong(strArr[i]);
                        i++;
                    } catch (NumberFormatException unused) {
                        m2395a(strArr);
                        throw null;
                    }
                }
                return;
            }
            m2395a(strArr);
            throw null;
        }

        /* renamed from: a */
        public String mo10473a() {
            StringBuilder sb = new StringBuilder();
            long[] jArr = this.f1544b;
            for (long j : jArr) {
                sb.append(' ');
                sb.append(j);
            }
            return sb.toString();
        }

        /* renamed from: b */
        public File mo10474b(int i) {
            return this.f1546d[i];
        }

        /* renamed from: a */
        private IOException m2395a(String[] strArr) {
            throw new IOException("unexpected journal line: " + Arrays.toString(strArr));
        }

        /* renamed from: a */
        public File mo10472a(int i) {
            return this.f1545c[i];
        }
    }

    /* renamed from: com.bumptech.glide.m.a$e */
    /* compiled from: DiskLruCache */
    public final class C1099e {

        /* renamed from: a */
        private final File[] f1551a;

        /* synthetic */ C1099e(DiskLruCache aVar, String str, long j, File[] fileArr, long[] jArr, C1095a aVar2) {
            this(aVar, str, j, fileArr, jArr);
        }

        /* renamed from: a */
        public File mo10475a(int i) {
            return this.f1551a[i];
        }

        private C1099e(DiskLruCache aVar, String str, long j, File[] fileArr, long[] jArr) {
            this.f1551a = fileArr;
        }
    }

    private DiskLruCache(File file, int i, int i2, long j) {
        File file2 = file;
        this.f1524P = file2;
        this.f1528T = i;
        this.f1525Q = new File(file2, "journal");
        this.f1526R = new File(file2, "journal.tmp");
        this.f1527S = new File(file2, "journal.bkp");
        this.f1530V = i2;
        this.f1529U = j;
    }

    /* JADX INFO: finally extract failed */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.m.a.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.bumptech.glide.m.a.a(com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, boolean):void
      com.bumptech.glide.m.a.a(java.io.File, java.io.File, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* access modifiers changed from: private */
    /* renamed from: g */
    public synchronized void m2381g() {
        if (this.f1532X != null) {
            this.f1532X.close();
        }
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f1526R), Util.f1558a));
        try {
            bufferedWriter.write("libcore.io.DiskLruCache");
            bufferedWriter.write("\n");
            bufferedWriter.write(AppEventsConstants.EVENT_PARAM_VALUE_YES);
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.f1528T));
            bufferedWriter.write("\n");
            bufferedWriter.write(Integer.toString(this.f1530V));
            bufferedWriter.write("\n");
            bufferedWriter.write("\n");
            for (C1098d dVar : this.f1533Y.values()) {
                if (dVar.f1548f != null) {
                    bufferedWriter.write("DIRTY " + dVar.f1543a + 10);
                } else {
                    bufferedWriter.write("CLEAN " + dVar.f1543a + dVar.mo10473a() + 10);
                }
            }
            bufferedWriter.close();
            if (this.f1525Q.exists()) {
                m2370a(this.f1525Q, this.f1527S, true);
            }
            m2370a(this.f1526R, this.f1525Q, false);
            this.f1527S.delete();
            this.f1532X = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.f1525Q, true), Util.f1558a));
        } catch (Throwable th) {
            bufferedWriter.close();
            throw th;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: t */
    public void m2382t() {
        while (this.f1531W > this.f1529U) {
            mo10465d((String) this.f1533Y.entrySet().iterator().next().getKey());
        }
    }

    public synchronized void close() {
        if (this.f1532X != null) {
            Iterator it = new ArrayList(this.f1533Y.values()).iterator();
            while (it.hasNext()) {
                C1098d dVar = (C1098d) it.next();
                if (dVar.f1548f != null) {
                    dVar.f1548f.mo10469a();
                }
            }
            m2382t();
            this.f1532X.close();
            this.f1532X = null;
        }
    }

    /* renamed from: d */
    private void m2376d() {
        m2369a(this.f1526R);
        Iterator<C1098d> it = this.f1533Y.values().iterator();
        while (it.hasNext()) {
            C1098d next = it.next();
            int i = 0;
            if (next.f1548f == null) {
                while (i < this.f1530V) {
                    this.f1531W += next.f1544b[i];
                    i++;
                }
            } else {
                C1097c unused = next.f1548f = (C1097c) null;
                while (i < this.f1530V) {
                    m2369a(next.mo10472a(i));
                    m2369a(next.mo10474b(i));
                    i++;
                }
                it.remove();
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
      ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:16|17|(1:19)(1:20)|21|22) */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r9.f1534Z = r0 - r9.f1533Y.size();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006c, code lost:
        if (r1.mo10476a() != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x006e, code lost:
        m2381g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0072, code lost:
        r9.f1532X = new java.io.BufferedWriter(new java.io.OutputStreamWriter(new java.io.FileOutputStream(r9.f1525Q, true), com.bumptech.glide.p036m.Util.f1558a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x008b, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x005f */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:23:0x008c=Splitter:B:23:0x008c, B:16:0x005f=Splitter:B:16:0x005f} */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2377e() {
        /*
            r9 = this;
            java.lang.String r0 = ", "
            com.bumptech.glide.m.b r1 = new com.bumptech.glide.m.b
            java.io.FileInputStream r2 = new java.io.FileInputStream
            java.io.File r3 = r9.f1525Q
            r2.<init>(r3)
            java.nio.charset.Charset r3 = com.bumptech.glide.p036m.Util.f1558a
            r1.<init>(r2, r3)
            java.lang.String r2 = r1.mo10477b()     // Catch:{ all -> 0x00ba }
            java.lang.String r3 = r1.mo10477b()     // Catch:{ all -> 0x00ba }
            java.lang.String r4 = r1.mo10477b()     // Catch:{ all -> 0x00ba }
            java.lang.String r5 = r1.mo10477b()     // Catch:{ all -> 0x00ba }
            java.lang.String r6 = r1.mo10477b()     // Catch:{ all -> 0x00ba }
            java.lang.String r7 = "libcore.io.DiskLruCache"
            boolean r7 = r7.equals(r2)     // Catch:{ all -> 0x00ba }
            if (r7 == 0) goto L_0x008c
            java.lang.String r7 = "1"
            boolean r7 = r7.equals(r3)     // Catch:{ all -> 0x00ba }
            if (r7 == 0) goto L_0x008c
            int r7 = r9.f1528T     // Catch:{ all -> 0x00ba }
            java.lang.String r7 = java.lang.Integer.toString(r7)     // Catch:{ all -> 0x00ba }
            boolean r4 = r7.equals(r4)     // Catch:{ all -> 0x00ba }
            if (r4 == 0) goto L_0x008c
            int r4 = r9.f1530V     // Catch:{ all -> 0x00ba }
            java.lang.String r4 = java.lang.Integer.toString(r4)     // Catch:{ all -> 0x00ba }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x00ba }
            if (r4 == 0) goto L_0x008c
            java.lang.String r4 = ""
            boolean r4 = r4.equals(r6)     // Catch:{ all -> 0x00ba }
            if (r4 == 0) goto L_0x008c
            r0 = 0
        L_0x0055:
            java.lang.String r2 = r1.mo10477b()     // Catch:{ EOFException -> 0x005f }
            r9.m2378e(r2)     // Catch:{ EOFException -> 0x005f }
            int r0 = r0 + 1
            goto L_0x0055
        L_0x005f:
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.m.a$d> r2 = r9.f1533Y     // Catch:{ all -> 0x00ba }
            int r2 = r2.size()     // Catch:{ all -> 0x00ba }
            int r0 = r0 - r2
            r9.f1534Z = r0     // Catch:{ all -> 0x00ba }
            boolean r0 = r1.mo10476a()     // Catch:{ all -> 0x00ba }
            if (r0 == 0) goto L_0x0072
            r9.m2381g()     // Catch:{ all -> 0x00ba }
            goto L_0x0088
        L_0x0072:
            java.io.BufferedWriter r0 = new java.io.BufferedWriter     // Catch:{ all -> 0x00ba }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x00ba }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ all -> 0x00ba }
            java.io.File r4 = r9.f1525Q     // Catch:{ all -> 0x00ba }
            r5 = 1
            r3.<init>(r4, r5)     // Catch:{ all -> 0x00ba }
            java.nio.charset.Charset r4 = com.bumptech.glide.p036m.Util.f1558a     // Catch:{ all -> 0x00ba }
            r2.<init>(r3, r4)     // Catch:{ all -> 0x00ba }
            r0.<init>(r2)     // Catch:{ all -> 0x00ba }
            r9.f1532X = r0     // Catch:{ all -> 0x00ba }
        L_0x0088:
            com.bumptech.glide.p036m.Util.m2412a(r1)
            return
        L_0x008c:
            java.io.IOException r4 = new java.io.IOException     // Catch:{ all -> 0x00ba }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ba }
            r7.<init>()     // Catch:{ all -> 0x00ba }
            java.lang.String r8 = "unexpected journal header: ["
            r7.append(r8)     // Catch:{ all -> 0x00ba }
            r7.append(r2)     // Catch:{ all -> 0x00ba }
            r7.append(r0)     // Catch:{ all -> 0x00ba }
            r7.append(r3)     // Catch:{ all -> 0x00ba }
            r7.append(r0)     // Catch:{ all -> 0x00ba }
            r7.append(r5)     // Catch:{ all -> 0x00ba }
            r7.append(r0)     // Catch:{ all -> 0x00ba }
            r7.append(r6)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = "]"
            r7.append(r0)     // Catch:{ all -> 0x00ba }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x00ba }
            r4.<init>(r0)     // Catch:{ all -> 0x00ba }
            throw r4     // Catch:{ all -> 0x00ba }
        L_0x00ba:
            r0 = move-exception
            com.bumptech.glide.p036m.Util.m2412a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p036m.DiskLruCache.m2377e():void");
    }

    /* renamed from: b */
    public C1097c mo10462b(String str) {
        return m2364a(str, -1);
    }

    /* renamed from: c */
    public synchronized C1099e mo10463c(String str) {
        m2372b();
        C1098d dVar = this.f1533Y.get(str);
        if (dVar == null) {
            return null;
        }
        if (!dVar.f1547e) {
            return null;
        }
        for (File file : dVar.f1545c) {
            if (!file.exists()) {
                return null;
            }
        }
        this.f1534Z++;
        this.f1532X.append((CharSequence) "READ");
        this.f1532X.append(' ');
        this.f1532X.append((CharSequence) str);
        this.f1532X.append(10);
        if (m2374c()) {
            this.f1536b0.submit(this.f1537c0);
        }
        return new C1099e(this, str, dVar.f1549g, dVar.f1545c, dVar.f1544b, null);
    }

    /* renamed from: b */
    private void m2372b() {
        if (this.f1532X == null) {
            throw new IllegalStateException("cache is closed");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.m.a.a(java.io.File, java.io.File, boolean):void
     arg types: [java.io.File, java.io.File, int]
     candidates:
      com.bumptech.glide.m.a.a(com.bumptech.glide.m.a, com.bumptech.glide.m.a$c, boolean):void
      com.bumptech.glide.m.a.a(java.io.File, java.io.File, boolean):void */
    /* renamed from: a */
    public static DiskLruCache m2365a(File file, int i, int i2, long j) {
        if (j <= 0) {
            throw new IllegalArgumentException("maxSize <= 0");
        } else if (i2 > 0) {
            File file2 = new File(file, "journal.bkp");
            if (file2.exists()) {
                File file3 = new File(file, "journal");
                if (file3.exists()) {
                    file2.delete();
                } else {
                    m2370a(file2, file3, false);
                }
            }
            DiskLruCache aVar = new DiskLruCache(file, i, i2, j);
            if (aVar.f1525Q.exists()) {
                try {
                    aVar.m2377e();
                    aVar.m2376d();
                    return aVar;
                } catch (IOException e) {
                    PrintStream printStream = System.out;
                    printStream.println("DiskLruCache " + file + " is corrupt: " + e.getMessage() + ", removing");
                    aVar.mo10461a();
                }
            }
            file.mkdirs();
            DiskLruCache aVar2 = new DiskLruCache(file, i, i2, j);
            aVar2.m2381g();
            return aVar2;
        } else {
            throw new IllegalArgumentException("valueCount <= 0");
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x008c, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x008e, code lost:
        return false;
     */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean mo10465d(java.lang.String r8) {
        /*
            r7 = this;
            monitor-enter(r7)
            r7.m2372b()     // Catch:{ all -> 0x008f }
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.m.a$d> r0 = r7.f1533Y     // Catch:{ all -> 0x008f }
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x008f }
            com.bumptech.glide.m.a$d r0 = (com.bumptech.glide.p036m.DiskLruCache.C1098d) r0     // Catch:{ all -> 0x008f }
            r1 = 0
            if (r0 == 0) goto L_0x008d
            com.bumptech.glide.m.a$c r2 = r0.f1548f     // Catch:{ all -> 0x008f }
            if (r2 == 0) goto L_0x0017
            goto L_0x008d
        L_0x0017:
            int r2 = r7.f1530V     // Catch:{ all -> 0x008f }
            if (r1 >= r2) goto L_0x0059
            java.io.File r2 = r0.mo10472a(r1)     // Catch:{ all -> 0x008f }
            boolean r3 = r2.exists()     // Catch:{ all -> 0x008f }
            if (r3 == 0) goto L_0x0043
            boolean r3 = r2.delete()     // Catch:{ all -> 0x008f }
            if (r3 == 0) goto L_0x002c
            goto L_0x0043
        L_0x002c:
            java.io.IOException r8 = new java.io.IOException     // Catch:{ all -> 0x008f }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x008f }
            r0.<init>()     // Catch:{ all -> 0x008f }
            java.lang.String r1 = "failed to delete "
            r0.append(r1)     // Catch:{ all -> 0x008f }
            r0.append(r2)     // Catch:{ all -> 0x008f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x008f }
            r8.<init>(r0)     // Catch:{ all -> 0x008f }
            throw r8     // Catch:{ all -> 0x008f }
        L_0x0043:
            long r2 = r7.f1531W     // Catch:{ all -> 0x008f }
            long[] r4 = r0.f1544b     // Catch:{ all -> 0x008f }
            r5 = r4[r1]     // Catch:{ all -> 0x008f }
            long r2 = r2 - r5
            r7.f1531W = r2     // Catch:{ all -> 0x008f }
            long[] r2 = r0.f1544b     // Catch:{ all -> 0x008f }
            r3 = 0
            r2[r1] = r3     // Catch:{ all -> 0x008f }
            int r1 = r1 + 1
            goto L_0x0017
        L_0x0059:
            int r0 = r7.f1534Z     // Catch:{ all -> 0x008f }
            r1 = 1
            int r0 = r0 + r1
            r7.f1534Z = r0     // Catch:{ all -> 0x008f }
            java.io.Writer r0 = r7.f1532X     // Catch:{ all -> 0x008f }
            java.lang.String r2 = "REMOVE"
            r0.append(r2)     // Catch:{ all -> 0x008f }
            java.io.Writer r0 = r7.f1532X     // Catch:{ all -> 0x008f }
            r2 = 32
            r0.append(r2)     // Catch:{ all -> 0x008f }
            java.io.Writer r0 = r7.f1532X     // Catch:{ all -> 0x008f }
            r0.append(r8)     // Catch:{ all -> 0x008f }
            java.io.Writer r0 = r7.f1532X     // Catch:{ all -> 0x008f }
            r2 = 10
            r0.append(r2)     // Catch:{ all -> 0x008f }
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.m.a$d> r0 = r7.f1533Y     // Catch:{ all -> 0x008f }
            r0.remove(r8)     // Catch:{ all -> 0x008f }
            boolean r8 = r7.m2374c()     // Catch:{ all -> 0x008f }
            if (r8 == 0) goto L_0x008b
            java.util.concurrent.ThreadPoolExecutor r8 = r7.f1536b0     // Catch:{ all -> 0x008f }
            java.util.concurrent.Callable<java.lang.Void> r0 = r7.f1537c0     // Catch:{ all -> 0x008f }
            r8.submit(r0)     // Catch:{ all -> 0x008f }
        L_0x008b:
            monitor-exit(r7)
            return r1
        L_0x008d:
            monitor-exit(r7)
            return r1
        L_0x008f:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p036m.DiskLruCache.mo10465d(java.lang.String):boolean");
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m2374c() {
        int i = this.f1534Z;
        return i >= 2000 && i >= this.f1533Y.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, boolean):boolean
     arg types: [com.bumptech.glide.m.a$d, int]
     candidates:
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, long):long
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, com.bumptech.glide.m.a$c):com.bumptech.glide.m.a$c
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, java.lang.String[]):void
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, boolean):boolean */
    /* renamed from: e */
    private void m2378e(String str) {
        String str2;
        int indexOf = str.indexOf(32);
        if (indexOf != -1) {
            int i = indexOf + 1;
            int indexOf2 = str.indexOf(32, i);
            if (indexOf2 == -1) {
                str2 = str.substring(i);
                if (indexOf == 6 && str.startsWith("REMOVE")) {
                    this.f1533Y.remove(str2);
                    return;
                }
            } else {
                str2 = str.substring(i, indexOf2);
            }
            C1098d dVar = this.f1533Y.get(str2);
            if (dVar == null) {
                dVar = new C1098d(this, str2, null);
                this.f1533Y.put(str2, dVar);
            }
            if (indexOf2 != -1 && indexOf == 5 && str.startsWith("CLEAN")) {
                String[] split = str.substring(indexOf2 + 1).split(" ");
                boolean unused = dVar.f1547e = true;
                C1097c unused2 = dVar.f1548f = (C1097c) null;
                dVar.m2400b(split);
            } else if (indexOf2 == -1 && indexOf == 5 && str.startsWith("DIRTY")) {
                C1097c unused3 = dVar.f1548f = new C1097c(this, dVar, null);
            } else if (indexOf2 != -1 || indexOf != 4 || !str.startsWith("READ")) {
                throw new IOException("unexpected journal line: " + str);
            }
        } else {
            throw new IOException("unexpected journal line: " + str);
        }
    }

    /* renamed from: a */
    private static void m2369a(File file) {
        if (file.exists() && !file.delete()) {
            throw new IOException();
        }
    }

    /* renamed from: a */
    private static void m2370a(File file, File file2, boolean z) {
        if (z) {
            m2369a(file2);
        }
        if (!file.renameTo(file2)) {
            throw new IOException();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001e, code lost:
        return null;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.bumptech.glide.p036m.DiskLruCache.C1097c m2364a(java.lang.String r6, long r7) {
        /*
            r5 = this;
            monitor-enter(r5)
            r5.m2372b()     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.m.a$d> r0 = r5.f1533Y     // Catch:{ all -> 0x005d }
            java.lang.Object r0 = r0.get(r6)     // Catch:{ all -> 0x005d }
            com.bumptech.glide.m.a$d r0 = (com.bumptech.glide.p036m.DiskLruCache.C1098d) r0     // Catch:{ all -> 0x005d }
            r1 = -1
            r3 = 0
            int r4 = (r7 > r1 ? 1 : (r7 == r1 ? 0 : -1))
            if (r4 == 0) goto L_0x001f
            if (r0 == 0) goto L_0x001d
            long r1 = r0.f1549g     // Catch:{ all -> 0x005d }
            int r4 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r4 == 0) goto L_0x001f
        L_0x001d:
            monitor-exit(r5)
            return r3
        L_0x001f:
            if (r0 != 0) goto L_0x002c
            com.bumptech.glide.m.a$d r0 = new com.bumptech.glide.m.a$d     // Catch:{ all -> 0x005d }
            r0.<init>(r5, r6, r3)     // Catch:{ all -> 0x005d }
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.m.a$d> r7 = r5.f1533Y     // Catch:{ all -> 0x005d }
            r7.put(r6, r0)     // Catch:{ all -> 0x005d }
            goto L_0x0034
        L_0x002c:
            com.bumptech.glide.m.a$c r7 = r0.f1548f     // Catch:{ all -> 0x005d }
            if (r7 == 0) goto L_0x0034
            monitor-exit(r5)
            return r3
        L_0x0034:
            com.bumptech.glide.m.a$c r7 = new com.bumptech.glide.m.a$c     // Catch:{ all -> 0x005d }
            r7.<init>(r5, r0, r3)     // Catch:{ all -> 0x005d }
            com.bumptech.glide.p036m.DiskLruCache.C1097c unused = r0.f1548f = r7     // Catch:{ all -> 0x005d }
            java.io.Writer r8 = r5.f1532X     // Catch:{ all -> 0x005d }
            java.lang.String r0 = "DIRTY"
            r8.append(r0)     // Catch:{ all -> 0x005d }
            java.io.Writer r8 = r5.f1532X     // Catch:{ all -> 0x005d }
            r0 = 32
            r8.append(r0)     // Catch:{ all -> 0x005d }
            java.io.Writer r8 = r5.f1532X     // Catch:{ all -> 0x005d }
            r8.append(r6)     // Catch:{ all -> 0x005d }
            java.io.Writer r6 = r5.f1532X     // Catch:{ all -> 0x005d }
            r8 = 10
            r6.append(r8)     // Catch:{ all -> 0x005d }
            java.io.Writer r6 = r5.f1532X     // Catch:{ all -> 0x005d }
            r6.flush()     // Catch:{ all -> 0x005d }
            monitor-exit(r5)
            return r7
        L_0x005d:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p036m.DiskLruCache.m2364a(java.lang.String, long):com.bumptech.glide.m.a$c");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, boolean):boolean
     arg types: [com.bumptech.glide.m.a$d, int]
     candidates:
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, long):long
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, com.bumptech.glide.m.a$c):com.bumptech.glide.m.a$c
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, java.lang.String[]):void
      com.bumptech.glide.m.a.d.a(com.bumptech.glide.m.a$d, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0107, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void m2367a(com.bumptech.glide.p036m.DiskLruCache.C1097c r10, boolean r11) {
        /*
            r9 = this;
            monitor-enter(r9)
            com.bumptech.glide.m.a$d r0 = r10.f1539a     // Catch:{ all -> 0x010e }
            com.bumptech.glide.m.a$c r1 = r0.f1548f     // Catch:{ all -> 0x010e }
            if (r1 != r10) goto L_0x0108
            r1 = 0
            if (r11 == 0) goto L_0x004d
            boolean r2 = r0.f1547e     // Catch:{ all -> 0x010e }
            if (r2 != 0) goto L_0x004d
            r2 = 0
        L_0x0015:
            int r3 = r9.f1530V     // Catch:{ all -> 0x010e }
            if (r2 >= r3) goto L_0x004d
            boolean[] r3 = r10.f1540b     // Catch:{ all -> 0x010e }
            boolean r3 = r3[r2]     // Catch:{ all -> 0x010e }
            if (r3 == 0) goto L_0x0033
            java.io.File r3 = r0.mo10474b(r2)     // Catch:{ all -> 0x010e }
            boolean r3 = r3.exists()     // Catch:{ all -> 0x010e }
            if (r3 != 0) goto L_0x0030
            r10.mo10469a()     // Catch:{ all -> 0x010e }
            monitor-exit(r9)
            return
        L_0x0030:
            int r2 = r2 + 1
            goto L_0x0015
        L_0x0033:
            r10.mo10469a()     // Catch:{ all -> 0x010e }
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x010e }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x010e }
            r11.<init>()     // Catch:{ all -> 0x010e }
            java.lang.String r0 = "Newly created entry didn't create value for index "
            r11.append(r0)     // Catch:{ all -> 0x010e }
            r11.append(r2)     // Catch:{ all -> 0x010e }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x010e }
            r10.<init>(r11)     // Catch:{ all -> 0x010e }
            throw r10     // Catch:{ all -> 0x010e }
        L_0x004d:
            int r10 = r9.f1530V     // Catch:{ all -> 0x010e }
            if (r1 >= r10) goto L_0x0081
            java.io.File r10 = r0.mo10474b(r1)     // Catch:{ all -> 0x010e }
            if (r11 == 0) goto L_0x007b
            boolean r2 = r10.exists()     // Catch:{ all -> 0x010e }
            if (r2 == 0) goto L_0x007e
            java.io.File r2 = r0.mo10472a(r1)     // Catch:{ all -> 0x010e }
            r10.renameTo(r2)     // Catch:{ all -> 0x010e }
            long[] r10 = r0.f1544b     // Catch:{ all -> 0x010e }
            r3 = r10[r1]     // Catch:{ all -> 0x010e }
            long r5 = r2.length()     // Catch:{ all -> 0x010e }
            long[] r10 = r0.f1544b     // Catch:{ all -> 0x010e }
            r10[r1] = r5     // Catch:{ all -> 0x010e }
            long r7 = r9.f1531W     // Catch:{ all -> 0x010e }
            long r7 = r7 - r3
            long r7 = r7 + r5
            r9.f1531W = r7     // Catch:{ all -> 0x010e }
            goto L_0x007e
        L_0x007b:
            m2369a(r10)     // Catch:{ all -> 0x010e }
        L_0x007e:
            int r1 = r1 + 1
            goto L_0x004d
        L_0x0081:
            int r10 = r9.f1534Z     // Catch:{ all -> 0x010e }
            r1 = 1
            int r10 = r10 + r1
            r9.f1534Z = r10     // Catch:{ all -> 0x010e }
            r10 = 0
            com.bumptech.glide.p036m.DiskLruCache.C1097c unused = r0.f1548f = r10     // Catch:{ all -> 0x010e }
            boolean r10 = r0.f1547e     // Catch:{ all -> 0x010e }
            r10 = r10 | r11
            r2 = 10
            r3 = 32
            if (r10 == 0) goto L_0x00c9
            boolean unused = r0.f1547e = r1     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            java.lang.String r1 = "CLEAN"
            r10.append(r1)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            r10.append(r3)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            java.lang.String r1 = r0.f1543a     // Catch:{ all -> 0x010e }
            r10.append(r1)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            java.lang.String r1 = r0.mo10473a()     // Catch:{ all -> 0x010e }
            r10.append(r1)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            r10.append(r2)     // Catch:{ all -> 0x010e }
            if (r11 == 0) goto L_0x00ec
            long r10 = r9.f1535a0     // Catch:{ all -> 0x010e }
            r1 = 1
            long r1 = r1 + r10
            r9.f1535a0 = r1     // Catch:{ all -> 0x010e }
            long unused = r0.f1549g = r10     // Catch:{ all -> 0x010e }
            goto L_0x00ec
        L_0x00c9:
            java.util.LinkedHashMap<java.lang.String, com.bumptech.glide.m.a$d> r10 = r9.f1533Y     // Catch:{ all -> 0x010e }
            java.lang.String r11 = r0.f1543a     // Catch:{ all -> 0x010e }
            r10.remove(r11)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            java.lang.String r11 = "REMOVE"
            r10.append(r11)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            r10.append(r3)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            java.lang.String r11 = r0.f1543a     // Catch:{ all -> 0x010e }
            r10.append(r11)     // Catch:{ all -> 0x010e }
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            r10.append(r2)     // Catch:{ all -> 0x010e }
        L_0x00ec:
            java.io.Writer r10 = r9.f1532X     // Catch:{ all -> 0x010e }
            r10.flush()     // Catch:{ all -> 0x010e }
            long r10 = r9.f1531W     // Catch:{ all -> 0x010e }
            long r0 = r9.f1529U     // Catch:{ all -> 0x010e }
            int r2 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r2 > 0) goto L_0x00ff
            boolean r10 = r9.m2374c()     // Catch:{ all -> 0x010e }
            if (r10 == 0) goto L_0x0106
        L_0x00ff:
            java.util.concurrent.ThreadPoolExecutor r10 = r9.f1536b0     // Catch:{ all -> 0x010e }
            java.util.concurrent.Callable<java.lang.Void> r11 = r9.f1537c0     // Catch:{ all -> 0x010e }
            r10.submit(r11)     // Catch:{ all -> 0x010e }
        L_0x0106:
            monitor-exit(r9)
            return
        L_0x0108:
            java.lang.IllegalStateException r10 = new java.lang.IllegalStateException     // Catch:{ all -> 0x010e }
            r10.<init>()     // Catch:{ all -> 0x010e }
            throw r10     // Catch:{ all -> 0x010e }
        L_0x010e:
            r10 = move-exception
            monitor-exit(r9)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p036m.DiskLruCache.m2367a(com.bumptech.glide.m.a$c, boolean):void");
    }

    /* renamed from: a */
    public void mo10461a() {
        close();
        Util.m2413a(this.f1524P);
    }
}
