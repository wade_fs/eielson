package com.bumptech.glide.load.p030p;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.a */
public class SimpleResource<T> implements Resource<T> {

    /* renamed from: P */
    protected final T f1382P;

    public SimpleResource(@NonNull T t) {
        Preconditions.m2828a((Object) t);
        this.f1382P = t;
    }

    /* renamed from: a */
    public final int mo10226a() {
        return 1;
    }

    @NonNull
    /* renamed from: b */
    public Class<T> mo10228b() {
        return this.f1382P.getClass();
    }

    @NonNull
    public final T get() {
        return this.f1382P;
    }

    public void recycle() {
    }
}
