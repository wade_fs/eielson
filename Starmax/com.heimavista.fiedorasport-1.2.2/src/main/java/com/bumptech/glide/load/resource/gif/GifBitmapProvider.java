package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.p037n.GifDecoder;

/* renamed from: com.bumptech.glide.load.resource.gif.b */
public final class GifBitmapProvider implements GifDecoder.C1107a {

    /* renamed from: a */
    private final BitmapPool f1495a;
    @Nullable

    /* renamed from: b */
    private final ArrayPool f1496b;

    public GifBitmapProvider(BitmapPool eVar, @Nullable ArrayPool bVar) {
        this.f1495a = eVar;
        this.f1496b = bVar;
    }

    @NonNull
    /* renamed from: a */
    public Bitmap mo10432a(int i, int i2, @NonNull Bitmap.Config config) {
        return this.f1495a.mo10064b(i, i2, config);
    }

    @NonNull
    /* renamed from: b */
    public byte[] mo10437b(int i) {
        ArrayPool bVar = this.f1496b;
        if (bVar == null) {
            return new byte[i];
        }
        return (byte[]) bVar.mo10042b(i, byte[].class);
    }

    /* renamed from: a */
    public void mo10433a(@NonNull Bitmap bitmap) {
        this.f1495a.mo10063a(bitmap);
    }

    /* renamed from: a */
    public void mo10434a(@NonNull byte[] bArr) {
        ArrayPool bVar = this.f1496b;
        if (bVar != null) {
            bVar.put(bArr);
        }
    }

    @NonNull
    /* renamed from: a */
    public int[] mo10436a(int i) {
        ArrayPool bVar = this.f1496b;
        if (bVar == null) {
            return new int[i];
        }
        return (int[]) bVar.mo10042b(i, int[].class);
    }

    /* renamed from: a */
    public void mo10435a(@NonNull int[] iArr) {
        ArrayPool bVar = this.f1496b;
        if (bVar != null) {
            bVar.put(iArr);
        }
    }
}
