package com.bumptech.glide.load.p028o;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.AssetFileDescriptorLocalUriFetcher;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p021m.FileDescriptorLocalUriFetcher;
import com.bumptech.glide.load.p021m.StreamLocalUriFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.bumptech.glide.load.o.w */
public class UriLoader<Data> implements ModelLoader<Uri, Data> {

    /* renamed from: b */
    private static final Set<String> f1365b = Collections.unmodifiableSet(new HashSet(Arrays.asList("file", "android.resource", "content")));

    /* renamed from: a */
    private final C1054c<Data> f1366a;

    /* renamed from: com.bumptech.glide.load.o.w$a */
    /* compiled from: UriLoader */
    public static final class C1052a implements ModelLoaderFactory<Uri, AssetFileDescriptor>, C1054c<AssetFileDescriptor> {

        /* renamed from: a */
        private final ContentResolver f1367a;

        public C1052a(ContentResolver contentResolver) {
            this.f1367a = contentResolver;
        }

        /* renamed from: a */
        public ModelLoader<Uri, AssetFileDescriptor> mo10265a(MultiModelLoaderFactory rVar) {
            return new UriLoader(this);
        }

        /* renamed from: a */
        public DataFetcher<AssetFileDescriptor> mo10327a(Uri uri) {
            return new AssetFileDescriptorLocalUriFetcher(this.f1367a, uri);
        }
    }

    /* renamed from: com.bumptech.glide.load.o.w$b */
    /* compiled from: UriLoader */
    public static class C1053b implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, C1054c<ParcelFileDescriptor> {

        /* renamed from: a */
        private final ContentResolver f1368a;

        public C1053b(ContentResolver contentResolver) {
            this.f1368a = contentResolver;
        }

        /* renamed from: a */
        public DataFetcher<ParcelFileDescriptor> mo10327a(Uri uri) {
            return new FileDescriptorLocalUriFetcher(this.f1368a, uri);
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, ParcelFileDescriptor> mo10265a(MultiModelLoaderFactory rVar) {
            return new UriLoader(this);
        }
    }

    /* renamed from: com.bumptech.glide.load.o.w$c */
    /* compiled from: UriLoader */
    public interface C1054c<Data> {
        /* renamed from: a */
        DataFetcher<Data> mo10327a(Uri uri);
    }

    /* renamed from: com.bumptech.glide.load.o.w$d */
    /* compiled from: UriLoader */
    public static class C1055d implements ModelLoaderFactory<Uri, InputStream>, C1054c<InputStream> {

        /* renamed from: a */
        private final ContentResolver f1369a;

        public C1055d(ContentResolver contentResolver) {
            this.f1369a = contentResolver;
        }

        /* renamed from: a */
        public DataFetcher<InputStream> mo10327a(Uri uri) {
            return new StreamLocalUriFetcher(this.f1369a, uri);
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new UriLoader(this);
        }
    }

    public UriLoader(C1054c<Data> cVar) {
        this.f1366a = cVar;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(uri), this.f1366a.mo10327a(uri));
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        return f1365b.contains(uri.getScheme());
    }
}
