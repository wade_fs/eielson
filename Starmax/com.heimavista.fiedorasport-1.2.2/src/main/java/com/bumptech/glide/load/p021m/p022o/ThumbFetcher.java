package com.bumptech.glide.load.p021m.p022o;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p021m.ExifOrientationStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.m.o.c */
public class ThumbFetcher implements DataFetcher<InputStream> {

    /* renamed from: P */
    private final Uri f963P;

    /* renamed from: Q */
    private final ThumbnailStreamOpener f964Q;

    /* renamed from: R */
    private InputStream f965R;

    /* renamed from: com.bumptech.glide.load.m.o.c$a */
    /* compiled from: ThumbFetcher */
    static class C0941a implements ThumbnailQuery {

        /* renamed from: b */
        private static final String[] f966b = {"_data"};

        /* renamed from: a */
        private final ContentResolver f967a;

        C0941a(ContentResolver contentResolver) {
            this.f967a = contentResolver;
        }

        /* renamed from: a */
        public Cursor mo10021a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f967a.query(MediaStore.Images.Thumbnails.EXTERNAL_CONTENT_URI, f966b, "kind = 1 AND image_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    /* renamed from: com.bumptech.glide.load.m.o.c$b */
    /* compiled from: ThumbFetcher */
    static class C0942b implements ThumbnailQuery {

        /* renamed from: b */
        private static final String[] f968b = {"_data"};

        /* renamed from: a */
        private final ContentResolver f969a;

        C0942b(ContentResolver contentResolver) {
            this.f969a = contentResolver;
        }

        /* renamed from: a */
        public Cursor mo10021a(Uri uri) {
            String lastPathSegment = uri.getLastPathSegment();
            return this.f969a.query(MediaStore.Video.Thumbnails.EXTERNAL_CONTENT_URI, f968b, "kind = 1 AND video_id = ?", new String[]{lastPathSegment}, null);
        }
    }

    @VisibleForTesting
    ThumbFetcher(Uri uri, ThumbnailStreamOpener eVar) {
        this.f963P = uri;
        this.f964Q = eVar;
    }

    /* renamed from: a */
    public static ThumbFetcher m1467a(Context context, Uri uri) {
        return m1468a(context, uri, new C0941a(context.getContentResolver()));
    }

    /* renamed from: b */
    public static ThumbFetcher m1469b(Context context, Uri uri) {
        return m1468a(context, uri, new C0942b(context.getContentResolver()));
    }

    /* renamed from: d */
    private InputStream m1470d() {
        InputStream b = this.f964Q.mo10023b(this.f963P);
        int a = b != null ? this.f964Q.mo10022a(this.f963P) : -1;
        return a != -1 ? new ExifOrientationStream(b, a) : b;
    }

    @NonNull
    /* renamed from: c */
    public DataSource mo9991c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }

    /* renamed from: a */
    private static ThumbFetcher m1468a(Context context, Uri uri, ThumbnailQuery dVar) {
        return new ThumbFetcher(uri, new ThumbnailStreamOpener(Glide.m1279b(context).mo9898g().mo9925a(), dVar, Glide.m1279b(context).mo9892b(), context.getContentResolver()));
    }

    /* renamed from: b */
    public void mo9990b() {
        InputStream inputStream = this.f965R;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
    }

    /* renamed from: a */
    public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super InputStream> aVar) {
        try {
            this.f965R = m1470d();
            aVar.mo9999a(this.f965R);
        } catch (FileNotFoundException e) {
            if (Log.isLoggable("MediaStoreThumbFetcher", 3)) {
                Log.d("MediaStoreThumbFetcher", "Failed to find thumbnail file", e);
            }
            aVar.mo9998a((Exception) e);
        }
    }

    @NonNull
    /* renamed from: a */
    public Class<InputStream> mo9984a() {
        return InputStream.class;
    }
}
