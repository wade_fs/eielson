package com.bumptech.glide.load;

import android.content.Context;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.l */
public interface Transformation<T> extends Key {
    @NonNull
    /* renamed from: a */
    Resource<T> mo9982a(@NonNull Context context, @NonNull Resource<T> vVar, int i, int i2);
}
