package com.bumptech.glide.manager;

import android.content.Context;
import androidx.annotation.NonNull;
import com.bumptech.glide.manager.ConnectivityMonitor;

/* renamed from: com.bumptech.glide.manager.d */
public interface ConnectivityMonitorFactory {
    @NonNull
    /* renamed from: a */
    ConnectivityMonitor mo10503a(@NonNull Context context, @NonNull ConnectivityMonitor.C1103a aVar);
}
