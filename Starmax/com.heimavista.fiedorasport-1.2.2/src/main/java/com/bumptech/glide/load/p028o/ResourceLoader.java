package com.bumptech.glide.load.p028o;

import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p028o.ModelLoader;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.s */
public class ResourceLoader<Data> implements ModelLoader<Integer, Data> {

    /* renamed from: a */
    private final ModelLoader<Uri, Data> f1354a;

    /* renamed from: b */
    private final Resources f1355b;

    /* renamed from: com.bumptech.glide.load.o.s$a */
    /* compiled from: ResourceLoader */
    public static final class C1043a implements ModelLoaderFactory<Integer, AssetFileDescriptor> {

        /* renamed from: a */
        private final Resources f1356a;

        public C1043a(Resources resources) {
            this.f1356a = resources;
        }

        /* renamed from: a */
        public ModelLoader<Integer, AssetFileDescriptor> mo10265a(MultiModelLoaderFactory rVar) {
            return new ResourceLoader(this.f1356a, rVar.mo10313a(Uri.class, AssetFileDescriptor.class));
        }
    }

    /* renamed from: com.bumptech.glide.load.o.s$b */
    /* compiled from: ResourceLoader */
    public static class C1044b implements ModelLoaderFactory<Integer, ParcelFileDescriptor> {

        /* renamed from: a */
        private final Resources f1357a;

        public C1044b(Resources resources) {
            this.f1357a = resources;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Integer, ParcelFileDescriptor> mo10265a(MultiModelLoaderFactory rVar) {
            return new ResourceLoader(this.f1357a, rVar.mo10313a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    /* renamed from: com.bumptech.glide.load.o.s$c */
    /* compiled from: ResourceLoader */
    public static class C1045c implements ModelLoaderFactory<Integer, InputStream> {

        /* renamed from: a */
        private final Resources f1358a;

        public C1045c(Resources resources) {
            this.f1358a = resources;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Integer, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new ResourceLoader(this.f1358a, rVar.mo10313a(Uri.class, InputStream.class));
        }
    }

    /* renamed from: com.bumptech.glide.load.o.s$d */
    /* compiled from: ResourceLoader */
    public static class C1046d implements ModelLoaderFactory<Integer, Uri> {

        /* renamed from: a */
        private final Resources f1359a;

        public C1046d(Resources resources) {
            this.f1359a = resources;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Integer, Uri> mo10265a(MultiModelLoaderFactory rVar) {
            return new ResourceLoader(this.f1359a, UnitModelLoader.m2026a());
        }
    }

    public ResourceLoader(Resources resources, ModelLoader<Uri, Data> nVar) {
        this.f1355b = resources;
        this.f1354a = nVar;
    }

    @Nullable
    /* renamed from: b */
    private Uri m2006b(Integer num) {
        try {
            return Uri.parse("android.resource://" + this.f1355b.getResourcePackageName(num.intValue()) + '/' + this.f1355b.getResourceTypeName(num.intValue()) + '/' + this.f1355b.getResourceEntryName(num.intValue()));
        } catch (Resources.NotFoundException e) {
            if (!Log.isLoggable("ResourceLoader", 5)) {
                return null;
            }
            Log.w("ResourceLoader", "Received invalid resource id: " + num, e);
            return null;
        }
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Integer num) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull Integer num, int i, int i2, @NonNull Options iVar) {
        Uri b = m2006b(num);
        if (b == null) {
            return null;
        }
        return this.f1354a.mo10261a(b, i, i2, iVar);
    }
}
