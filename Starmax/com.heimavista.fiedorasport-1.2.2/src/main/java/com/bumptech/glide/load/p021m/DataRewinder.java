package com.bumptech.glide.load.p021m;

import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.load.m.e */
public interface DataRewinder<T> {

    /* renamed from: com.bumptech.glide.load.m.e$a */
    /* compiled from: DataRewinder */
    public interface C0935a<T> {
        @NonNull
        /* renamed from: a */
        DataRewinder<T> mo10002a(@NonNull T t);

        @NonNull
        /* renamed from: a */
        Class<T> mo10003a();
    }

    @NonNull
    /* renamed from: a */
    T mo10000a();

    /* renamed from: b */
    void mo10001b();
}
