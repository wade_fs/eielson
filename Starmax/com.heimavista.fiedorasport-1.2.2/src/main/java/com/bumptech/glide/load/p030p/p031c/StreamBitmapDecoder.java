package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.p031c.Downsampler;
import com.bumptech.glide.util.ExceptionCatchingInputStream;
import com.bumptech.glide.util.MarkEnforcingInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.p.c.u */
public class StreamBitmapDecoder implements ResourceDecoder<InputStream, Bitmap> {

    /* renamed from: a */
    private final Downsampler f1441a;

    /* renamed from: b */
    private final ArrayPool f1442b;

    /* renamed from: com.bumptech.glide.load.p.c.u$a */
    /* compiled from: StreamBitmapDecoder */
    static class C1077a implements Downsampler.C1074b {

        /* renamed from: a */
        private final RecyclableBufferedInputStream f1443a;

        /* renamed from: b */
        private final ExceptionCatchingInputStream f1444b;

        C1077a(RecyclableBufferedInputStream sVar, ExceptionCatchingInputStream cVar) {
            this.f1443a = sVar;
            this.f1444b = cVar;
        }

        /* renamed from: a */
        public void mo10360a() {
            this.f1443a.mo10364a();
        }

        /* renamed from: a */
        public void mo10361a(BitmapPool eVar, Bitmap bitmap) {
            IOException a = this.f1444b.mo10684a();
            if (a != null) {
                if (bitmap != null) {
                    eVar.mo10063a(bitmap);
                }
                throw a;
            }
        }
    }

    public StreamBitmapDecoder(Downsampler lVar, ArrayPool bVar) {
        this.f1441a = lVar;
        this.f1442b = bVar;
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull InputStream inputStream, @NonNull Options iVar) {
        return this.f1441a.mo10358a(inputStream);
    }

    /* renamed from: a */
    public Resource<Bitmap> mo9979a(@NonNull InputStream inputStream, int i, int i2, @NonNull Options iVar) {
        RecyclableBufferedInputStream sVar;
        boolean z;
        if (inputStream instanceof RecyclableBufferedInputStream) {
            sVar = (RecyclableBufferedInputStream) inputStream;
            z = false;
        } else {
            sVar = new RecyclableBufferedInputStream(inputStream, this.f1442b);
            z = true;
        }
        ExceptionCatchingInputStream b = ExceptionCatchingInputStream.m2808b(sVar);
        try {
            return this.f1441a.mo10357a(new MarkEnforcingInputStream(b), i, i2, iVar, new C1077a(sVar, b));
        } finally {
            b.mo10687b();
            if (z) {
                sVar.mo10366b();
            }
        }
    }
}
