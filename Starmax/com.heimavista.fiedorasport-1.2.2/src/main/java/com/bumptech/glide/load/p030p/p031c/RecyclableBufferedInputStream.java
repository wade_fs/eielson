package com.bumptech.glide.load.p030p.p031c;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.p.c.s */
public class RecyclableBufferedInputStream extends FilterInputStream {

    /* renamed from: P */
    private volatile byte[] f1433P;

    /* renamed from: Q */
    private int f1434Q;

    /* renamed from: R */
    private int f1435R;

    /* renamed from: S */
    private int f1436S;

    /* renamed from: T */
    private int f1437T;

    /* renamed from: U */
    private final ArrayPool f1438U;

    /* renamed from: com.bumptech.glide.load.p.c.s$a */
    /* compiled from: RecyclableBufferedInputStream */
    static class C1076a extends IOException {
        private static final long serialVersionUID = -4338378848813561757L;

        C1076a(String str) {
            super(str);
        }
    }

    public RecyclableBufferedInputStream(@NonNull InputStream inputStream, @NonNull ArrayPool bVar) {
        this(inputStream, bVar, 65536);
    }

    /* renamed from: c */
    private static IOException m2195c() {
        throw new IOException("BufferedInputStream is closed");
    }

    /* renamed from: a */
    public synchronized void mo10364a() {
        this.f1435R = this.f1433P.length;
    }

    public synchronized int available() {
        InputStream inputStream;
        inputStream = super.in;
        if (this.f1433P == null || inputStream == null) {
            m2195c();
            throw null;
        }
        return (this.f1434Q - this.f1437T) + inputStream.available();
    }

    /* renamed from: b */
    public synchronized void mo10366b() {
        if (this.f1433P != null) {
            this.f1438U.put(this.f1433P);
            this.f1433P = null;
        }
    }

    public void close() {
        if (this.f1433P != null) {
            this.f1438U.put(this.f1433P);
            this.f1433P = null;
        }
        InputStream inputStream = super.in;
        super.in = null;
        if (inputStream != null) {
            inputStream.close();
        }
    }

    public synchronized void mark(int i) {
        this.f1435R = Math.max(this.f1435R, i);
        this.f1436S = this.f1437T;
    }

    public boolean markSupported() {
        return true;
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:19:0x0026=Splitter:B:19:0x0026, B:11:0x0019=Splitter:B:11:0x0019, B:28:0x003b=Splitter:B:28:0x003b} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int read() {
        /*
            r6 = this;
            monitor-enter(r6)
            byte[] r0 = r6.f1433P     // Catch:{ all -> 0x003f }
            java.io.InputStream r1 = r6.in     // Catch:{ all -> 0x003f }
            r2 = 0
            if (r0 == 0) goto L_0x003b
            if (r1 == 0) goto L_0x003b
            int r3 = r6.f1437T     // Catch:{ all -> 0x003f }
            int r4 = r6.f1434Q     // Catch:{ all -> 0x003f }
            r5 = -1
            if (r3 < r4) goto L_0x0019
            int r1 = r6.m2194a(r1, r0)     // Catch:{ all -> 0x003f }
            if (r1 != r5) goto L_0x0019
            monitor-exit(r6)
            return r5
        L_0x0019:
            byte[] r1 = r6.f1433P     // Catch:{ all -> 0x003f }
            if (r0 == r1) goto L_0x0026
            byte[] r0 = r6.f1433P     // Catch:{ all -> 0x003f }
            if (r0 == 0) goto L_0x0022
            goto L_0x0026
        L_0x0022:
            m2195c()     // Catch:{ all -> 0x003f }
            throw r2
        L_0x0026:
            int r1 = r6.f1434Q     // Catch:{ all -> 0x003f }
            int r2 = r6.f1437T     // Catch:{ all -> 0x003f }
            int r1 = r1 - r2
            if (r1 <= 0) goto L_0x0039
            int r1 = r6.f1437T     // Catch:{ all -> 0x003f }
            int r2 = r1 + 1
            r6.f1437T = r2     // Catch:{ all -> 0x003f }
            byte r0 = r0[r1]     // Catch:{ all -> 0x003f }
            r0 = r0 & 255(0xff, float:3.57E-43)
            monitor-exit(r6)
            return r0
        L_0x0039:
            monitor-exit(r6)
            return r5
        L_0x003b:
            m2195c()     // Catch:{ all -> 0x003f }
            throw r2
        L_0x003f:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p030p.p031c.RecyclableBufferedInputStream.read():int");
    }

    public synchronized void reset() {
        if (this.f1433P == null) {
            throw new IOException("Stream is closed");
        } else if (-1 != this.f1436S) {
            this.f1437T = this.f1436S;
        } else {
            throw new C1076a("Mark has been invalidated, pos: " + this.f1437T + " markLimit: " + this.f1435R);
        }
    }

    public synchronized long skip(long j) {
        if (j < 1) {
            return 0;
        }
        byte[] bArr = this.f1433P;
        if (bArr != null) {
            InputStream inputStream = super.in;
            if (inputStream == null) {
                m2195c();
                throw null;
            } else if (((long) (this.f1434Q - this.f1437T)) >= j) {
                this.f1437T = (int) (((long) this.f1437T) + j);
                return j;
            } else {
                long j2 = ((long) this.f1434Q) - ((long) this.f1437T);
                this.f1437T = this.f1434Q;
                if (this.f1436S == -1 || j > ((long) this.f1435R)) {
                    return j2 + inputStream.skip(j - j2);
                } else if (m2194a(inputStream, bArr) == -1) {
                    return j2;
                } else {
                    if (((long) (this.f1434Q - this.f1437T)) >= j - j2) {
                        this.f1437T = (int) ((((long) this.f1437T) + j) - j2);
                        return j;
                    }
                    long j3 = (j2 + ((long) this.f1434Q)) - ((long) this.f1437T);
                    this.f1437T = this.f1434Q;
                    return j3;
                }
            }
        } else {
            m2195c();
            throw null;
        }
    }

    @VisibleForTesting
    RecyclableBufferedInputStream(@NonNull InputStream inputStream, @NonNull ArrayPool bVar, int i) {
        super(inputStream);
        this.f1436S = -1;
        this.f1438U = bVar;
        this.f1433P = (byte[]) bVar.mo10042b(i, byte[].class);
    }

    /* renamed from: a */
    private int m2194a(InputStream inputStream, byte[] bArr) {
        int i;
        int i2 = this.f1436S;
        if (i2 == -1 || this.f1437T - i2 >= (i = this.f1435R)) {
            int read = inputStream.read(bArr);
            if (read > 0) {
                this.f1436S = -1;
                this.f1437T = 0;
                this.f1434Q = read;
            }
            return read;
        }
        if (i2 == 0 && i > bArr.length && this.f1434Q == bArr.length) {
            int length = bArr.length * 2;
            if (length > i) {
                length = i;
            }
            byte[] bArr2 = (byte[]) this.f1438U.mo10042b(length, byte[].class);
            System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
            this.f1433P = bArr2;
            this.f1438U.put(bArr);
            bArr = bArr2;
        } else {
            int i3 = this.f1436S;
            if (i3 > 0) {
                System.arraycopy(bArr, i3, bArr, 0, bArr.length - i3);
            }
        }
        this.f1437T -= this.f1436S;
        this.f1436S = 0;
        this.f1434Q = 0;
        int i4 = this.f1437T;
        int read2 = inputStream.read(bArr, i4, bArr.length - i4);
        int i5 = this.f1437T;
        if (read2 > 0) {
            i5 += read2;
        }
        this.f1434Q = i5;
        return read2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003b, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0051, code lost:
        return r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x005e, code lost:
        return r5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int read(@androidx.annotation.NonNull byte[] r7, int r8, int r9) {
        /*
            r6 = this;
            monitor-enter(r6)
            byte[] r0 = r6.f1433P     // Catch:{ all -> 0x009c }
            r1 = 0
            if (r0 == 0) goto L_0x0098
            if (r9 != 0) goto L_0x000b
            r7 = 0
            monitor-exit(r6)
            return r7
        L_0x000b:
            java.io.InputStream r2 = r6.in     // Catch:{ all -> 0x009c }
            if (r2 == 0) goto L_0x0094
            int r3 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r4 = r6.f1434Q     // Catch:{ all -> 0x009c }
            if (r3 >= r4) goto L_0x003c
            int r3 = r6.f1434Q     // Catch:{ all -> 0x009c }
            int r4 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r3 = r3 - r4
            if (r3 < r9) goto L_0x001e
            r3 = r9
            goto L_0x0023
        L_0x001e:
            int r3 = r6.f1434Q     // Catch:{ all -> 0x009c }
            int r4 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r3 = r3 - r4
        L_0x0023:
            int r4 = r6.f1437T     // Catch:{ all -> 0x009c }
            java.lang.System.arraycopy(r0, r4, r7, r8, r3)     // Catch:{ all -> 0x009c }
            int r4 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r4 = r4 + r3
            r6.f1437T = r4     // Catch:{ all -> 0x009c }
            if (r3 == r9) goto L_0x003a
            int r4 = r2.available()     // Catch:{ all -> 0x009c }
            if (r4 != 0) goto L_0x0036
            goto L_0x003a
        L_0x0036:
            int r8 = r8 + r3
            int r3 = r9 - r3
            goto L_0x003d
        L_0x003a:
            monitor-exit(r6)
            return r3
        L_0x003c:
            r3 = r9
        L_0x003d:
            int r4 = r6.f1436S     // Catch:{ all -> 0x009c }
            r5 = -1
            if (r4 != r5) goto L_0x0052
            int r4 = r0.length     // Catch:{ all -> 0x009c }
            if (r3 < r4) goto L_0x0052
            int r4 = r2.read(r7, r8, r3)     // Catch:{ all -> 0x009c }
            if (r4 != r5) goto L_0x0084
            if (r3 != r9) goto L_0x004e
            goto L_0x0050
        L_0x004e:
            int r5 = r9 - r3
        L_0x0050:
            monitor-exit(r6)
            return r5
        L_0x0052:
            int r4 = r6.m2194a(r2, r0)     // Catch:{ all -> 0x009c }
            if (r4 != r5) goto L_0x005f
            if (r3 != r9) goto L_0x005b
            goto L_0x005d
        L_0x005b:
            int r5 = r9 - r3
        L_0x005d:
            monitor-exit(r6)
            return r5
        L_0x005f:
            byte[] r4 = r6.f1433P     // Catch:{ all -> 0x009c }
            if (r0 == r4) goto L_0x006c
            byte[] r0 = r6.f1433P     // Catch:{ all -> 0x009c }
            if (r0 == 0) goto L_0x0068
            goto L_0x006c
        L_0x0068:
            m2195c()     // Catch:{ all -> 0x009c }
            throw r1
        L_0x006c:
            int r4 = r6.f1434Q     // Catch:{ all -> 0x009c }
            int r5 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r4 = r4 - r5
            if (r4 < r3) goto L_0x0075
            r4 = r3
            goto L_0x007a
        L_0x0075:
            int r4 = r6.f1434Q     // Catch:{ all -> 0x009c }
            int r5 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r4 = r4 - r5
        L_0x007a:
            int r5 = r6.f1437T     // Catch:{ all -> 0x009c }
            java.lang.System.arraycopy(r0, r5, r7, r8, r4)     // Catch:{ all -> 0x009c }
            int r5 = r6.f1437T     // Catch:{ all -> 0x009c }
            int r5 = r5 + r4
            r6.f1437T = r5     // Catch:{ all -> 0x009c }
        L_0x0084:
            int r3 = r3 - r4
            if (r3 != 0) goto L_0x0089
            monitor-exit(r6)
            return r9
        L_0x0089:
            int r5 = r2.available()     // Catch:{ all -> 0x009c }
            if (r5 != 0) goto L_0x0092
            int r9 = r9 - r3
            monitor-exit(r6)
            return r9
        L_0x0092:
            int r8 = r8 + r4
            goto L_0x003d
        L_0x0094:
            m2195c()     // Catch:{ all -> 0x009c }
            throw r1
        L_0x0098:
            m2195c()     // Catch:{ all -> 0x009c }
            throw r1
        L_0x009c:
            r7 = move-exception
            monitor-exit(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p030p.p031c.RecyclableBufferedInputStream.read(byte[], int, int):int");
    }
}
