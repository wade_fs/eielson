package com.bumptech.glide.p038o;

import android.content.Context;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;

@Deprecated
/* renamed from: com.bumptech.glide.o.c */
public interface GlideModule extends RegistersComponents, AppliesOptions {
    /* renamed from: a */
    /* synthetic */ void mo10548a(@NonNull Context context, @NonNull Glide cVar, @NonNull Registry iVar);

    /* renamed from: a */
    /* synthetic */ void mo10549a(@NonNull Context context, @NonNull GlideBuilder dVar);
}
