package com.bumptech.glide.manager;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.app.FragmentManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.collection.ArrayMap;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.Preconditions;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.bumptech.glide.manager.k */
public class RequestManagerRetriever implements Handler.Callback {

    /* renamed from: X */
    private static final C1106b f1582X = new C1105a();

    /* renamed from: P */
    private volatile RequestManager f1583P;
    @VisibleForTesting

    /* renamed from: Q */
    final Map<FragmentManager, RequestManagerFragment> f1584Q = new HashMap();
    @VisibleForTesting

    /* renamed from: R */
    final Map<androidx.fragment.app.FragmentManager, SupportRequestManagerFragment> f1585R = new HashMap();

    /* renamed from: S */
    private final Handler f1586S;

    /* renamed from: T */
    private final C1106b f1587T;

    /* renamed from: U */
    private final ArrayMap<View, Fragment> f1588U = new ArrayMap<>();

    /* renamed from: V */
    private final ArrayMap<View, android.app.Fragment> f1589V = new ArrayMap<>();

    /* renamed from: W */
    private final Bundle f1590W = new Bundle();

    /* renamed from: com.bumptech.glide.manager.k$a */
    /* compiled from: RequestManagerRetriever */
    class C1105a implements C1106b {
        C1105a() {
        }

        @NonNull
        /* renamed from: a */
        public RequestManager mo10515a(@NonNull Glide cVar, @NonNull Lifecycle hVar, @NonNull RequestManagerTreeNode lVar, @NonNull Context context) {
            return new RequestManager(cVar, hVar, lVar, context);
        }
    }

    /* renamed from: com.bumptech.glide.manager.k$b */
    /* compiled from: RequestManagerRetriever */
    public interface C1106b {
        @NonNull
        /* renamed from: a */
        RequestManager mo10515a(@NonNull Glide cVar, @NonNull Lifecycle hVar, @NonNull RequestManagerTreeNode lVar, @NonNull Context context);
    }

    public RequestManagerRetriever(@Nullable C1106b bVar) {
        this.f1587T = bVar == null ? f1582X : bVar;
        this.f1586S = new Handler(Looper.getMainLooper(), this);
    }

    @Deprecated
    /* renamed from: b */
    private void m2464b(@NonNull FragmentManager fragmentManager, @NonNull ArrayMap<View, android.app.Fragment> arrayMap) {
        int i = 0;
        while (true) {
            int i2 = i + 1;
            this.f1590W.putInt("key", i);
            android.app.Fragment fragment = null;
            try {
                fragment = fragmentManager.getFragment(this.f1590W, "key");
            } catch (Exception unused) {
            }
            if (fragment != null) {
                if (fragment.getView() != null) {
                    arrayMap.put(fragment.getView(), fragment);
                    if (Build.VERSION.SDK_INT >= 17) {
                        m2461a(fragment.getChildFragmentManager(), arrayMap);
                    }
                }
                i = i2;
            } else {
                return;
            }
        }
    }

    @NonNull
    /* renamed from: c */
    private RequestManager m2465c(@NonNull Context context) {
        if (this.f1583P == null) {
            synchronized (this) {
                if (this.f1583P == null) {
                    this.f1583P = this.f1587T.mo10515a(Glide.m1279b(context.getApplicationContext()), new ApplicationLifecycle(), new EmptyRequestManagerTreeNode(), context.getApplicationContext());
                }
            }
        }
        return this.f1583P;
    }

    /* renamed from: d */
    private static boolean m2467d(Activity activity) {
        return !activity.isFinishing();
    }

    @NonNull
    /* renamed from: a */
    public RequestManager mo10508a(@NonNull Context context) {
        if (context != null) {
            if (C1122j.m2852d() && !(context instanceof Application)) {
                if (context instanceof FragmentActivity) {
                    return mo10511a((FragmentActivity) context);
                }
                if (context instanceof Activity) {
                    return mo10506a((Activity) context);
                }
                if (context instanceof ContextWrapper) {
                    return mo10508a(((ContextWrapper) context).getBaseContext());
                }
            }
            return m2465c(context);
        }
        throw new IllegalArgumentException("You cannot start a load on a null Context");
    }

    public boolean handleMessage(Message message) {
        Object obj;
        int i = message.what;
        Object obj2 = null;
        boolean z = true;
        if (i == 1) {
            obj2 = (FragmentManager) message.obj;
            obj = this.f1584Q.remove(obj2);
        } else if (i != 2) {
            z = false;
            obj = null;
        } else {
            obj2 = (androidx.fragment.app.FragmentManager) message.obj;
            obj = this.f1585R.remove(obj2);
        }
        if (z && obj == null && Log.isLoggable("RMRetriever", 5)) {
            Log.w("RMRetriever", "Failed to remove expected request manager fragment, manager: " + obj2);
        }
        return z;
    }

    @Nullable
    /* renamed from: b */
    private Activity m2463b(@NonNull Context context) {
        if (context instanceof Activity) {
            return (Activity) context;
        }
        if (context instanceof ContextWrapper) {
            return m2463b(((ContextWrapper) context).getBaseContext());
        }
        return null;
    }

    @TargetApi(17)
    /* renamed from: c */
    private static void m2466c(@NonNull Activity activity) {
        if (Build.VERSION.SDK_INT >= 17 && activity.isDestroyed()) {
            throw new IllegalArgumentException("You cannot start a load for a destroyed activity");
        }
    }

    @NonNull
    /* renamed from: a */
    public RequestManager mo10511a(@NonNull FragmentActivity fragmentActivity) {
        if (C1122j.m2851c()) {
            return mo10508a(fragmentActivity.getApplicationContext());
        }
        m2466c((Activity) fragmentActivity);
        return m2458a(fragmentActivity, fragmentActivity.getSupportFragmentManager(), (Fragment) null, m2467d(fragmentActivity));
    }

    /* access modifiers changed from: package-private */
    @NonNull
    @Deprecated
    /* renamed from: b */
    public RequestManagerFragment mo10512b(Activity activity) {
        return m2459a(activity.getFragmentManager(), (android.app.Fragment) null, m2467d(activity));
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: b */
    public SupportRequestManagerFragment mo10513b(FragmentActivity fragmentActivity) {
        return m2460a(fragmentActivity.getSupportFragmentManager(), (Fragment) null, m2467d(fragmentActivity));
    }

    @NonNull
    /* renamed from: a */
    public RequestManager mo10510a(@NonNull Fragment fragment) {
        Preconditions.m2829a(fragment.getActivity(), "You cannot start a load on a fragment before it is attached or after it is destroyed");
        if (C1122j.m2851c()) {
            return mo10508a(fragment.getActivity().getApplicationContext());
        }
        return m2458a(fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
    }

    @NonNull
    /* renamed from: a */
    public RequestManager mo10506a(@NonNull Activity activity) {
        if (C1122j.m2851c()) {
            return mo10508a(activity.getApplicationContext());
        }
        m2466c(activity);
        return m2457a(activity, activity.getFragmentManager(), (android.app.Fragment) null, m2467d(activity));
    }

    @NonNull
    /* renamed from: a */
    public RequestManager mo10509a(@NonNull View view) {
        if (C1122j.m2851c()) {
            return mo10508a(view.getContext().getApplicationContext());
        }
        Preconditions.m2828a(view);
        Preconditions.m2829a(view.getContext(), "Unable to obtain a request manager for a view without a Context");
        Activity b = m2463b(view.getContext());
        if (b == null) {
            return mo10508a(view.getContext().getApplicationContext());
        }
        if (b instanceof FragmentActivity) {
            Fragment a = m2456a(view, (FragmentActivity) b);
            return a != null ? mo10510a(a) : mo10506a(b);
        }
        android.app.Fragment a2 = m2455a(view, b);
        if (a2 == null) {
            return mo10506a(b);
        }
        return mo10507a(a2);
    }

    /* renamed from: a */
    private static void m2462a(@Nullable Collection<Fragment> collection, @NonNull Map<View, Fragment> map) {
        if (collection != null) {
            for (Fragment fragment : collection) {
                if (!(fragment == null || fragment.getView() == null)) {
                    map.put(fragment.getView(), fragment);
                    m2462a(fragment.getChildFragmentManager().getFragments(), map);
                }
            }
        }
    }

    @Nullable
    /* renamed from: a */
    private Fragment m2456a(@NonNull View view, @NonNull FragmentActivity fragmentActivity) {
        this.f1588U.clear();
        m2462a(fragmentActivity.getSupportFragmentManager().getFragments(), this.f1588U);
        View findViewById = fragmentActivity.findViewById(16908290);
        Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.f1588U.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.f1588U.clear();
        return fragment;
    }

    @Deprecated
    @Nullable
    /* renamed from: a */
    private android.app.Fragment m2455a(@NonNull View view, @NonNull Activity activity) {
        this.f1589V.clear();
        m2461a(activity.getFragmentManager(), this.f1589V);
        View findViewById = activity.findViewById(16908290);
        android.app.Fragment fragment = null;
        while (!view.equals(findViewById) && (fragment = this.f1589V.get(view)) == null && (view.getParent() instanceof View)) {
            view = (View) view.getParent();
        }
        this.f1589V.clear();
        return fragment;
    }

    @TargetApi(26)
    @Deprecated
    /* renamed from: a */
    private void m2461a(@NonNull FragmentManager fragmentManager, @NonNull ArrayMap<View, android.app.Fragment> arrayMap) {
        if (Build.VERSION.SDK_INT >= 26) {
            for (android.app.Fragment fragment : fragmentManager.getFragments()) {
                if (fragment.getView() != null) {
                    arrayMap.put(fragment.getView(), fragment);
                    m2461a(fragment.getChildFragmentManager(), arrayMap);
                }
            }
            return;
        }
        m2464b(fragmentManager, arrayMap);
    }

    @TargetApi(17)
    @NonNull
    @Deprecated
    /* renamed from: a */
    public RequestManager mo10507a(@NonNull android.app.Fragment fragment) {
        if (fragment.getActivity() == null) {
            throw new IllegalArgumentException("You cannot start a load on a fragment before it is attached");
        } else if (C1122j.m2851c() || Build.VERSION.SDK_INT < 17) {
            return mo10508a(fragment.getActivity().getApplicationContext());
        } else {
            return m2457a(fragment.getActivity(), fragment.getChildFragmentManager(), fragment, fragment.isVisible());
        }
    }

    @NonNull
    /* renamed from: a */
    private RequestManagerFragment m2459a(@NonNull FragmentManager fragmentManager, @Nullable android.app.Fragment fragment, boolean z) {
        RequestManagerFragment requestManagerFragment = (RequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (requestManagerFragment == null && (requestManagerFragment = this.f1584Q.get(fragmentManager)) == null) {
            requestManagerFragment = new RequestManagerFragment();
            requestManagerFragment.mo10481a(fragment);
            if (z) {
                requestManagerFragment.mo10480a().mo10500b();
            }
            this.f1584Q.put(fragmentManager, requestManagerFragment);
            fragmentManager.beginTransaction().add(requestManagerFragment, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.f1586S.obtainMessage(1, fragmentManager).sendToTarget();
        }
        return requestManagerFragment;
    }

    @NonNull
    @Deprecated
    /* renamed from: a */
    private RequestManager m2457a(@NonNull Context context, @NonNull FragmentManager fragmentManager, @Nullable android.app.Fragment fragment, boolean z) {
        RequestManagerFragment a = m2459a(fragmentManager, fragment, z);
        RequestManager b = a.mo10483b();
        if (b != null) {
            return b;
        }
        RequestManager a2 = this.f1587T.mo10515a(Glide.m1279b(context), a.mo10480a(), a.mo10484c(), context);
        a.mo10482a(a2);
        return a2;
    }

    @NonNull
    /* renamed from: a */
    private SupportRequestManagerFragment m2460a(@NonNull androidx.fragment.app.FragmentManager fragmentManager, @Nullable Fragment fragment, boolean z) {
        SupportRequestManagerFragment supportRequestManagerFragment = (SupportRequestManagerFragment) fragmentManager.findFragmentByTag("com.bumptech.glide.manager");
        if (supportRequestManagerFragment == null && (supportRequestManagerFragment = this.f1585R.get(fragmentManager)) == null) {
            supportRequestManagerFragment = new SupportRequestManagerFragment();
            supportRequestManagerFragment.mo10492a(fragment);
            if (z) {
                supportRequestManagerFragment.mo10494b().mo10500b();
            }
            this.f1585R.put(fragmentManager, supportRequestManagerFragment);
            fragmentManager.beginTransaction().add(supportRequestManagerFragment, "com.bumptech.glide.manager").commitAllowingStateLoss();
            this.f1586S.obtainMessage(2, fragmentManager).sendToTarget();
        }
        return supportRequestManagerFragment;
    }

    @NonNull
    /* renamed from: a */
    private RequestManager m2458a(@NonNull Context context, @NonNull androidx.fragment.app.FragmentManager fragmentManager, @Nullable Fragment fragment, boolean z) {
        SupportRequestManagerFragment a = m2460a(fragmentManager, fragment, z);
        RequestManager c = a.mo10495c();
        if (c != null) {
            return c;
        }
        RequestManager a2 = this.f1587T.mo10515a(Glide.m1279b(context), a.mo10494b(), a.mo10496d(), context);
        a.mo10493a(a2);
        return a2;
    }
}
