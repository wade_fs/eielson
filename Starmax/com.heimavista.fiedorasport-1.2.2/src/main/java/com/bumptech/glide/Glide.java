package com.bumptech.glide;

import android.content.ComponentCallbacks2;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.load.p021m.InputStreamRewinder;
import com.bumptech.glide.load.p023n.Engine;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p023n.p025b0.MemoryCache;
import com.bumptech.glide.load.p023n.p027d0.BitmapPreFiller;
import com.bumptech.glide.load.p028o.AssetUriLoader;
import com.bumptech.glide.load.p028o.ByteArrayLoader;
import com.bumptech.glide.load.p028o.ByteBufferEncoder;
import com.bumptech.glide.load.p028o.ByteBufferFileLoader;
import com.bumptech.glide.load.p028o.DataUrlLoader;
import com.bumptech.glide.load.p028o.FileLoader;
import com.bumptech.glide.load.p028o.GlideUrl;
import com.bumptech.glide.load.p028o.MediaStoreFileLoader;
import com.bumptech.glide.load.p028o.ResourceLoader;
import com.bumptech.glide.load.p028o.StreamEncoder;
import com.bumptech.glide.load.p028o.StringLoader;
import com.bumptech.glide.load.p028o.UnitModelLoader;
import com.bumptech.glide.load.p028o.UriLoader;
import com.bumptech.glide.load.p028o.UrlUriLoader;
import com.bumptech.glide.load.p028o.p029y.HttpGlideUrlLoader;
import com.bumptech.glide.load.p028o.p029y.HttpUriLoader;
import com.bumptech.glide.load.p028o.p029y.MediaStoreImageThumbLoader;
import com.bumptech.glide.load.p028o.p029y.MediaStoreVideoThumbLoader;
import com.bumptech.glide.load.p028o.p029y.UrlLoader;
import com.bumptech.glide.load.p030p.p031c.BitmapDrawableDecoder;
import com.bumptech.glide.load.p030p.p031c.BitmapDrawableEncoder;
import com.bumptech.glide.load.p030p.p031c.BitmapEncoder;
import com.bumptech.glide.load.p030p.p031c.ByteBufferBitmapDecoder;
import com.bumptech.glide.load.p030p.p031c.DefaultImageHeaderParser;
import com.bumptech.glide.load.p030p.p031c.Downsampler;
import com.bumptech.glide.load.p030p.p031c.ExifInterfaceImageHeaderParser;
import com.bumptech.glide.load.p030p.p031c.ResourceBitmapDecoder;
import com.bumptech.glide.load.p030p.p031c.StreamBitmapDecoder;
import com.bumptech.glide.load.p030p.p031c.UnitBitmapDecoder;
import com.bumptech.glide.load.p030p.p031c.VideoDecoder;
import com.bumptech.glide.load.p030p.p032d.ByteBufferRewinder;
import com.bumptech.glide.load.p030p.p033e.ResourceDrawableDecoder;
import com.bumptech.glide.load.p030p.p033e.UnitDrawableDecoder;
import com.bumptech.glide.load.p030p.p034f.FileDecoder;
import com.bumptech.glide.load.p030p.p035g.BitmapBytesTranscoder;
import com.bumptech.glide.load.p030p.p035g.BitmapDrawableTranscoder;
import com.bumptech.glide.load.p030p.p035g.DrawableBytesTranscoder;
import com.bumptech.glide.load.p030p.p035g.GifDrawableBytesTranscoder;
import com.bumptech.glide.load.resource.gif.ByteBufferGifDecoder;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawableEncoder;
import com.bumptech.glide.load.resource.gif.GifFrameResourceDecoder;
import com.bumptech.glide.load.resource.gif.StreamGifDecoder;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.p037n.GifDecoder;
import com.bumptech.glide.p038o.GlideModule;
import com.bumptech.glide.p038o.ManifestParser;
import com.bumptech.glide.p040q.RequestListener;
import com.bumptech.glide.p040q.RequestOptions;
import com.bumptech.glide.p040q.p041l.ImageViewTargetFactory;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.Preconditions;
import java.io.File;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/* renamed from: com.bumptech.glide.c */
public class Glide implements ComponentCallbacks2 {

    /* renamed from: X */
    private static volatile Glide f826X;

    /* renamed from: Y */
    private static volatile boolean f827Y;

    /* renamed from: P */
    private final BitmapPool f828P;

    /* renamed from: Q */
    private final MemoryCache f829Q;

    /* renamed from: R */
    private final GlideContext f830R;

    /* renamed from: S */
    private final Registry f831S;

    /* renamed from: T */
    private final ArrayPool f832T;

    /* renamed from: U */
    private final RequestManagerRetriever f833U;

    /* renamed from: V */
    private final ConnectivityMonitorFactory f834V;

    /* renamed from: W */
    private final List<RequestManager> f835W = new ArrayList();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.k):com.bumptech.glide.i
     arg types: [java.lang.Class, com.bumptech.glide.load.p.c.c]
     candidates:
      com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.d):com.bumptech.glide.i
      com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.k):com.bumptech.glide.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.k):com.bumptech.glide.i
     arg types: [java.lang.Class, com.bumptech.glide.load.p.c.b]
     candidates:
      com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.d):com.bumptech.glide.i
      com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.k):com.bumptech.glide.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.k):com.bumptech.glide.i
     arg types: [java.lang.Class, com.bumptech.glide.load.resource.gif.c]
     candidates:
      com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.d):com.bumptech.glide.i
      com.bumptech.glide.i.a(java.lang.Class, com.bumptech.glide.load.k):com.bumptech.glide.i */
    Glide(@NonNull Context context, @NonNull Engine kVar, @NonNull MemoryCache hVar, @NonNull BitmapPool eVar, @NonNull ArrayPool bVar, @NonNull RequestManagerRetriever kVar2, @NonNull ConnectivityMonitorFactory dVar, int i, @NonNull RequestOptions hVar2, @NonNull Map<Class<?>, TransitionOptions<?, ?>> map, @NonNull List<RequestListener<Object>> list, boolean z) {
        Context context2 = context;
        MemoryCache hVar3 = hVar;
        BitmapPool eVar2 = eVar;
        ArrayPool bVar2 = bVar;
        Class<GifDecoder> cls = GifDecoder.class;
        Class<byte[]> cls2 = byte[].class;
        MemoryCategory fVar = MemoryCategory.NORMAL;
        this.f828P = eVar2;
        this.f832T = bVar2;
        this.f829Q = hVar3;
        this.f833U = kVar2;
        this.f834V = dVar;
        new BitmapPreFiller(hVar3, eVar2, (DecodeFormat) hVar2.mo10606i().mo9976a(Downsampler.f1411f));
        Resources resources = context.getResources();
        this.f831S = new Registry();
        this.f831S.mo9914a((ImageHeaderParser) new DefaultImageHeaderParser());
        if (Build.VERSION.SDK_INT >= 27) {
            this.f831S.mo9914a((ImageHeaderParser) new ExifInterfaceImageHeaderParser());
        }
        List<ImageHeaderParser> a = this.f831S.mo9925a();
        Downsampler lVar = new Downsampler(a, resources.getDisplayMetrics(), eVar2, bVar2);
        ByteBufferGifDecoder aVar = new ByteBufferGifDecoder(context2, a, eVar2, bVar2);
        ResourceDecoder<ParcelFileDescriptor, Bitmap> b = VideoDecoder.m2233b(eVar);
        ByteBufferBitmapDecoder fVar2 = new ByteBufferBitmapDecoder(lVar);
        StreamBitmapDecoder uVar = new StreamBitmapDecoder(lVar, bVar2);
        ResourceDrawableDecoder dVar2 = new ResourceDrawableDecoder(context2);
        ResourceLoader.C1045c cVar = new ResourceLoader.C1045c(resources);
        ResourceLoader.C1046d dVar3 = new ResourceLoader.C1046d(resources);
        ResourceLoader.C1044b bVar3 = new ResourceLoader.C1044b(resources);
        Class<byte[]> cls3 = cls2;
        ResourceLoader.C1043a aVar2 = new ResourceLoader.C1043a(resources);
        BitmapEncoder cVar2 = new BitmapEncoder(bVar2);
        ResourceLoader.C1043a aVar3 = aVar2;
        BitmapBytesTranscoder aVar4 = new BitmapBytesTranscoder();
        GifDrawableBytesTranscoder dVar4 = new GifDrawableBytesTranscoder();
        ContentResolver contentResolver = context.getContentResolver();
        Registry iVar = this.f831S;
        iVar.mo9916a(ByteBuffer.class, new ByteBufferEncoder());
        iVar.mo9916a(InputStream.class, new StreamEncoder(bVar2));
        iVar.mo9921a("Bitmap", ByteBuffer.class, Bitmap.class, fVar2);
        iVar.mo9921a("Bitmap", InputStream.class, Bitmap.class, uVar);
        iVar.mo9921a("Bitmap", ParcelFileDescriptor.class, Bitmap.class, b);
        iVar.mo9921a("Bitmap", AssetFileDescriptor.class, Bitmap.class, VideoDecoder.m2231a(eVar));
        iVar.mo9919a(Bitmap.class, Bitmap.class, UnitModelLoader.C1050a.m2029a());
        iVar.mo9921a("Bitmap", Bitmap.class, Bitmap.class, new UnitBitmapDecoder());
        iVar.mo9917a(Bitmap.class, (ResourceEncoder) cVar2);
        iVar.mo9921a("BitmapDrawable", ByteBuffer.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, fVar2));
        iVar.mo9921a("BitmapDrawable", InputStream.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, uVar));
        iVar.mo9921a("BitmapDrawable", ParcelFileDescriptor.class, BitmapDrawable.class, new BitmapDrawableDecoder(resources, b));
        iVar.mo9917a(BitmapDrawable.class, (ResourceEncoder) new BitmapDrawableEncoder(eVar2, cVar2));
        iVar.mo9921a("Gif", InputStream.class, GifDrawable.class, new StreamGifDecoder(a, aVar, bVar2));
        iVar.mo9921a("Gif", ByteBuffer.class, GifDrawable.class, aVar);
        iVar.mo9917a(GifDrawable.class, (ResourceEncoder) new GifDrawableEncoder());
        iVar.mo9919a(cls, cls, UnitModelLoader.C1050a.m2029a());
        iVar.mo9921a("Bitmap", cls, Bitmap.class, new GifFrameResourceDecoder(eVar2));
        iVar.mo9918a(Uri.class, Drawable.class, dVar2);
        iVar.mo9918a(Uri.class, Bitmap.class, new ResourceBitmapDecoder(dVar2, eVar2));
        iVar.mo9915a((DataRewinder.C0935a<?>) new ByteBufferRewinder.C1086a());
        iVar.mo9919a(File.class, ByteBuffer.class, new ByteBufferFileLoader.C1017b());
        iVar.mo9919a(File.class, InputStream.class, new FileLoader.C1027e());
        iVar.mo9918a(File.class, File.class, new FileDecoder());
        iVar.mo9919a(File.class, ParcelFileDescriptor.class, new FileLoader.C1023b());
        iVar.mo9919a(File.class, File.class, UnitModelLoader.C1050a.m2029a());
        iVar.mo9915a((DataRewinder.C0935a<?>) new InputStreamRewinder.C0940a(bVar2));
        ResourceLoader.C1045c cVar3 = cVar;
        iVar.mo9919a(Integer.TYPE, InputStream.class, cVar3);
        ResourceLoader.C1044b bVar4 = bVar3;
        iVar.mo9919a(Integer.TYPE, ParcelFileDescriptor.class, bVar4);
        iVar.mo9919a(Integer.class, InputStream.class, cVar3);
        iVar.mo9919a(Integer.class, ParcelFileDescriptor.class, bVar4);
        ResourceLoader.C1046d dVar5 = dVar3;
        iVar.mo9919a(Integer.class, Uri.class, dVar5);
        ResourceLoader.C1043a aVar5 = aVar3;
        iVar.mo9919a(Integer.TYPE, AssetFileDescriptor.class, aVar5);
        iVar.mo9919a(Integer.class, AssetFileDescriptor.class, aVar5);
        iVar.mo9919a(Integer.TYPE, Uri.class, dVar5);
        iVar.mo9919a(String.class, InputStream.class, new DataUrlLoader.C1020c());
        iVar.mo9919a(Uri.class, InputStream.class, new DataUrlLoader.C1020c());
        iVar.mo9919a(String.class, InputStream.class, new StringLoader.C1049c());
        iVar.mo9919a(String.class, ParcelFileDescriptor.class, new StringLoader.C1048b());
        iVar.mo9919a(String.class, AssetFileDescriptor.class, new StringLoader.C1047a());
        iVar.mo9919a(Uri.class, InputStream.class, new HttpUriLoader.C1058a());
        iVar.mo9919a(Uri.class, InputStream.class, new AssetUriLoader.C1009c(context.getAssets()));
        iVar.mo9919a(Uri.class, ParcelFileDescriptor.class, new AssetUriLoader.C1008b(context.getAssets()));
        Context context3 = context;
        iVar.mo9919a(Uri.class, InputStream.class, new MediaStoreImageThumbLoader.C1059a(context3));
        iVar.mo9919a(Uri.class, InputStream.class, new MediaStoreVideoThumbLoader.C1060a(context3));
        ContentResolver contentResolver2 = contentResolver;
        iVar.mo9919a(Uri.class, InputStream.class, new UriLoader.C1055d(contentResolver2));
        iVar.mo9919a(Uri.class, ParcelFileDescriptor.class, new UriLoader.C1053b(contentResolver2));
        iVar.mo9919a(Uri.class, AssetFileDescriptor.class, new UriLoader.C1052a(contentResolver2));
        iVar.mo9919a(Uri.class, InputStream.class, new UrlUriLoader.C1056a());
        iVar.mo9919a(URL.class, InputStream.class, new UrlLoader.C1061a());
        iVar.mo9919a(Uri.class, File.class, new MediaStoreFileLoader.C1032a(context3));
        iVar.mo9919a(GlideUrl.class, InputStream.class, new HttpGlideUrlLoader.C1057a());
        Class<byte[]> cls4 = cls3;
        iVar.mo9919a(cls4, ByteBuffer.class, new ByteArrayLoader.C1010a());
        iVar.mo9919a(cls4, InputStream.class, new ByteArrayLoader.C1014d());
        iVar.mo9919a(Uri.class, Uri.class, UnitModelLoader.C1050a.m2029a());
        iVar.mo9919a(Drawable.class, Drawable.class, UnitModelLoader.C1050a.m2029a());
        iVar.mo9918a(Drawable.class, Drawable.class, new UnitDrawableDecoder());
        iVar.mo9920a(Bitmap.class, BitmapDrawable.class, new BitmapDrawableTranscoder(resources));
        BitmapBytesTranscoder aVar6 = aVar4;
        iVar.mo9920a(Bitmap.class, cls4, aVar6);
        GifDrawableBytesTranscoder dVar6 = dVar4;
        iVar.mo9920a(Drawable.class, cls4, new DrawableBytesTranscoder(eVar2, aVar6, dVar6));
        iVar.mo9920a(GifDrawable.class, cls4, dVar6);
        this.f830R = new GlideContext(context, bVar, this.f831S, new ImageViewTargetFactory(), hVar2, map, list, kVar, z, i);
    }

    /* renamed from: a */
    private static void m1276a(@NonNull Context context) {
        if (!f827Y) {
            f827Y = true;
            m1281d(context);
            f827Y = false;
            return;
        }
        throw new IllegalStateException("You cannot call Glide.get() in registerComponents(), use the provided Glide instance instead");
    }

    @NonNull
    /* renamed from: b */
    public static Glide m1279b(@NonNull Context context) {
        if (f826X == null) {
            synchronized (Glide.class) {
                if (f826X == null) {
                    m1276a(context);
                }
            }
        }
        return f826X;
    }

    /* renamed from: d */
    private static void m1281d(@NonNull Context context) {
        m1277a(context, new GlideBuilder());
    }

    @Nullable
    /* renamed from: i */
    private static GeneratedAppGlideModule m1283i() {
        try {
            return (GeneratedAppGlideModule) Class.forName("com.bumptech.glide.GeneratedAppGlideModuleImpl").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (ClassNotFoundException unused) {
            if (Log.isLoggable("Glide", 5)) {
                Log.w("Glide", "Failed to find GeneratedAppGlideModule. You should include an annotationProcessor compile dependency on com.github.bumptech.glide:compiler in your application and a @GlideModule annotated AppGlideModule implementation or LibraryGlideModules will be silently ignored");
            }
            return null;
        } catch (InstantiationException e) {
            m1278a(e);
            throw null;
        } catch (IllegalAccessException e2) {
            m1278a(e2);
            throw null;
        } catch (NoSuchMethodException e3) {
            m1278a(e3);
            throw null;
        } catch (InvocationTargetException e4) {
            m1278a(e4);
            throw null;
        }
    }

    @NonNull
    /* renamed from: c */
    public BitmapPool mo9894c() {
        return this.f828P;
    }

    @NonNull
    /* renamed from: e */
    public Context mo9896e() {
        return this.f830R.getBaseContext();
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: f */
    public GlideContext mo9897f() {
        return this.f830R;
    }

    @NonNull
    /* renamed from: g */
    public Registry mo9898g() {
        return this.f831S;
    }

    @NonNull
    /* renamed from: h */
    public RequestManagerRetriever mo9899h() {
        return this.f833U;
    }

    public void onConfigurationChanged(Configuration configuration) {
    }

    public void onLowMemory() {
        mo9888a();
    }

    public void onTrimMemory(int i) {
        mo9889a(i);
    }

    @NonNull
    /* renamed from: c */
    private static RequestManagerRetriever m1280c(@Nullable Context context) {
        Preconditions.m2829a(context, "You cannot start a load on a not yet attached View or a Fragment where getActivity() returns null (which usually occurs when getActivity() is called before the Fragment is attached or after the Fragment is destroyed).");
        return m1279b(context).mo9899h();
    }

    @NonNull
    /* renamed from: e */
    public static RequestManager m1282e(@NonNull Context context) {
        return m1280c(context).mo10508a(context);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public ConnectivityMonitorFactory mo9895d() {
        return this.f834V;
    }

    /* renamed from: a */
    private static void m1277a(@NonNull Context context, @NonNull GlideBuilder dVar) {
        Context applicationContext = context.getApplicationContext();
        GeneratedAppGlideModule i = m1283i();
        List<GlideModule> emptyList = Collections.emptyList();
        if (i == null || i.mo10547a()) {
            emptyList = new ManifestParser(applicationContext).mo10551a();
        }
        if (i != null && !i.mo9886b().isEmpty()) {
            Set<Class<?>> b = i.mo9886b();
            Iterator<GlideModule> it = emptyList.iterator();
            while (it.hasNext()) {
                GlideModule next = it.next();
                if (b.contains(next.getClass())) {
                    if (Log.isLoggable("Glide", 3)) {
                        Log.d("Glide", "AppGlideModule excludes manifest GlideModule: " + next);
                    }
                    it.remove();
                }
            }
        }
        if (Log.isLoggable("Glide", 3)) {
            for (GlideModule cVar : emptyList) {
                Log.d("Glide", "Discovered GlideModule from manifest: " + cVar.getClass());
            }
        }
        dVar.mo9904a(i != null ? i.mo9887c() : null);
        for (GlideModule cVar2 : emptyList) {
            cVar2.mo10549a(applicationContext, dVar);
        }
        if (i != null) {
            i.mo10546a(applicationContext, dVar);
        }
        Glide a = dVar.mo9903a(applicationContext);
        for (GlideModule cVar3 : emptyList) {
            cVar3.mo10548a(applicationContext, a, a.f831S);
        }
        if (i != null) {
            i.mo10550a(applicationContext, a, a.f831S);
        }
        applicationContext.registerComponentCallbacks(a);
        f826X = a;
    }

    @NonNull
    /* renamed from: b */
    public ArrayPool mo9892b() {
        return this.f832T;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo9893b(RequestManager kVar) {
        synchronized (this.f835W) {
            if (this.f835W.contains(kVar)) {
                this.f835W.remove(kVar);
            } else {
                throw new IllegalStateException("Cannot unregister not yet registered manager");
            }
        }
    }

    /* renamed from: a */
    private static void m1278a(Exception exc) {
        throw new IllegalStateException("GeneratedAppGlideModuleImpl is implemented incorrectly. If you've manually implemented this class, remove your implementation. The Annotation processor will generate a correct implementation.", exc);
    }

    /* renamed from: a */
    public void mo9888a() {
        C1122j.m2847b();
        this.f829Q.mo10105a();
        this.f828P.mo10061a();
        this.f832T.mo10040a();
    }

    /* renamed from: a */
    public void mo9889a(int i) {
        C1122j.m2847b();
        this.f829Q.mo10100a(i);
        this.f828P.mo10062a(i);
        this.f832T.mo10041a(i);
    }

    @NonNull
    /* renamed from: a */
    public static RequestManager m1275a(@NonNull View view) {
        return m1280c(view.getContext()).mo10509a(view);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo9891a(@NonNull Target<?> iVar) {
        synchronized (this.f835W) {
            for (RequestManager kVar : this.f835W) {
                if (kVar.mo9948b(iVar)) {
                    return true;
                }
            }
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo9890a(RequestManager kVar) {
        synchronized (this.f835W) {
            if (!this.f835W.contains(kVar)) {
                this.f835W.add(kVar);
            } else {
                throw new IllegalStateException("Cannot register already registered manager");
            }
        }
    }
}
