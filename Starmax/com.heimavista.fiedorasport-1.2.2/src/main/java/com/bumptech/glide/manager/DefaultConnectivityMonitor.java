package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.manager.ConnectivityMonitor;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.manager.e */
final class DefaultConnectivityMonitor implements ConnectivityMonitor {

    /* renamed from: P */
    private final Context f1576P;

    /* renamed from: Q */
    final ConnectivityMonitor.C1103a f1577Q;

    /* renamed from: R */
    boolean f1578R;

    /* renamed from: S */
    private boolean f1579S;

    /* renamed from: T */
    private final BroadcastReceiver f1580T = new C1104a();

    /* renamed from: com.bumptech.glide.manager.e$a */
    /* compiled from: DefaultConnectivityMonitor */
    class C1104a extends BroadcastReceiver {
        C1104a() {
        }

        public void onReceive(@NonNull Context context, Intent intent) {
            DefaultConnectivityMonitor eVar = DefaultConnectivityMonitor.this;
            boolean z = eVar.f1578R;
            eVar.f1578R = eVar.mo10504a(context);
            if (z != DefaultConnectivityMonitor.this.f1578R) {
                if (Log.isLoggable("ConnectivityMonitor", 3)) {
                    Log.d("ConnectivityMonitor", "connectivity changed, isConnected: " + DefaultConnectivityMonitor.this.f1578R);
                }
                DefaultConnectivityMonitor eVar2 = DefaultConnectivityMonitor.this;
                eVar2.f1577Q.mo9958a(eVar2.f1578R);
            }
        }
    }

    DefaultConnectivityMonitor(@NonNull Context context, @NonNull ConnectivityMonitor.C1103a aVar) {
        this.f1576P = context.getApplicationContext();
        this.f1577Q = aVar;
    }

    /* renamed from: a */
    private void m2443a() {
        if (!this.f1579S) {
            this.f1578R = mo10504a(this.f1576P);
            try {
                this.f1576P.registerReceiver(this.f1580T, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                this.f1579S = true;
            } catch (SecurityException e) {
                if (Log.isLoggable("ConnectivityMonitor", 5)) {
                    Log.w("ConnectivityMonitor", "Failed to register", e);
                }
            }
        }
    }

    /* renamed from: d */
    private void m2444d() {
        if (this.f1579S) {
            this.f1576P.unregisterReceiver(this.f1580T);
            this.f1579S = false;
        }
    }

    /* renamed from: b */
    public void mo9947b() {
    }

    /* renamed from: c */
    public void mo9949c() {
        m2444d();
    }

    public void onStart() {
        m2443a();
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"MissingPermission"})
    /* renamed from: a */
    public boolean mo10504a(@NonNull Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        Preconditions.m2828a(connectivityManager);
        try {
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (RuntimeException e) {
            if (Log.isLoggable("ConnectivityMonitor", 5)) {
                Log.w("ConnectivityMonitor", "Failed to determine connectivity status when connectivity changed", e);
            }
            return true;
        }
    }
}
