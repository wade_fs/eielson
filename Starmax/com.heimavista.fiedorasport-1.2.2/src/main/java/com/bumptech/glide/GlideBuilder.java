package com.bumptech.glide;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.bumptech.glide.load.p023n.Engine;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPoolAdapter;
import com.bumptech.glide.load.p023n.p024a0.LruArrayPool;
import com.bumptech.glide.load.p023n.p024a0.LruBitmapPool;
import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import com.bumptech.glide.load.p023n.p025b0.InternalCacheDiskCacheFactory;
import com.bumptech.glide.load.p023n.p025b0.LruResourceCache;
import com.bumptech.glide.load.p023n.p025b0.MemoryCache;
import com.bumptech.glide.load.p023n.p025b0.MemorySizeCalculator;
import com.bumptech.glide.load.p023n.p026c0.GlideExecutor;
import com.bumptech.glide.manager.ConnectivityMonitorFactory;
import com.bumptech.glide.manager.DefaultConnectivityMonitorFactory;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.p040q.RequestListener;
import com.bumptech.glide.p040q.RequestOptions;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.d */
public final class GlideBuilder {

    /* renamed from: a */
    private final Map<Class<?>, TransitionOptions<?, ?>> f836a = new ArrayMap();

    /* renamed from: b */
    private Engine f837b;

    /* renamed from: c */
    private BitmapPool f838c;

    /* renamed from: d */
    private ArrayPool f839d;

    /* renamed from: e */
    private MemoryCache f840e;

    /* renamed from: f */
    private GlideExecutor f841f;

    /* renamed from: g */
    private GlideExecutor f842g;

    /* renamed from: h */
    private DiskCache.C0958a f843h;

    /* renamed from: i */
    private MemorySizeCalculator f844i;

    /* renamed from: j */
    private ConnectivityMonitorFactory f845j;

    /* renamed from: k */
    private int f846k = 4;

    /* renamed from: l */
    private RequestOptions f847l = new RequestOptions();
    @Nullable

    /* renamed from: m */
    private RequestManagerRetriever.C1106b f848m;

    /* renamed from: n */
    private GlideExecutor f849n;

    /* renamed from: o */
    private boolean f850o;
    @Nullable

    /* renamed from: p */
    private List<RequestListener<Object>> f851p;

    /* renamed from: q */
    private boolean f852q;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo9904a(@Nullable RequestManagerRetriever.C1106b bVar) {
        this.f848m = bVar;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public Glide mo9903a(@NonNull Context context) {
        if (this.f841f == null) {
            this.f841f = GlideExecutor.m1660d();
        }
        if (this.f842g == null) {
            this.f842g = GlideExecutor.m1659c();
        }
        if (this.f849n == null) {
            this.f849n = GlideExecutor.m1657b();
        }
        if (this.f844i == null) {
            this.f844i = new MemorySizeCalculator.C0965a(context).mo10110a();
        }
        if (this.f845j == null) {
            this.f845j = new DefaultConnectivityMonitorFactory();
        }
        if (this.f838c == null) {
            int b = this.f844i.mo10108b();
            if (b > 0) {
                this.f838c = new LruBitmapPool((long) b);
            } else {
                this.f838c = new BitmapPoolAdapter();
            }
        }
        if (this.f839d == null) {
            this.f839d = new LruArrayPool(this.f844i.mo10107a());
        }
        if (this.f840e == null) {
            this.f840e = new LruResourceCache((long) this.f844i.mo10109c());
        }
        if (this.f843h == null) {
            this.f843h = new InternalCacheDiskCacheFactory(context);
        }
        if (this.f837b == null) {
            this.f837b = new Engine(this.f840e, this.f843h, this.f842g, this.f841f, GlideExecutor.m1661e(), GlideExecutor.m1657b(), this.f850o);
        }
        List<RequestListener<Object>> list = this.f851p;
        if (list == null) {
            this.f851p = Collections.emptyList();
        } else {
            this.f851p = Collections.unmodifiableList(list);
        }
        RequestManagerRetriever kVar = new RequestManagerRetriever(this.f848m);
        Engine kVar2 = this.f837b;
        MemoryCache hVar = this.f840e;
        BitmapPool eVar = this.f838c;
        ArrayPool bVar = this.f839d;
        ConnectivityMonitorFactory dVar = this.f845j;
        int i = this.f846k;
        RequestOptions hVar2 = this.f847l;
        hVar2.mo10574E();
        return new Glide(context, kVar2, hVar, eVar, bVar, kVar, dVar, i, hVar2, this.f836a, this.f851p, this.f852q);
    }
}
