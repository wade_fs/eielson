package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import androidx.core.util.Pools;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.p044k.FactoryPools;
import com.bumptech.glide.util.p044k.StateVerifier;

/* renamed from: com.bumptech.glide.load.n.u */
final class LockedResource<Z> implements Resource<Z>, FactoryPools.C1129f {

    /* renamed from: T */
    private static final Pools.Pool<LockedResource<?>> f1253T = FactoryPools.m2854a(20, new C1005a());

    /* renamed from: P */
    private final StateVerifier f1254P = StateVerifier.m2871b();

    /* renamed from: Q */
    private Resource<Z> f1255Q;

    /* renamed from: R */
    private boolean f1256R;

    /* renamed from: S */
    private boolean f1257S;

    /* renamed from: com.bumptech.glide.load.n.u$a */
    /* compiled from: LockedResource */
    class C1005a implements FactoryPools.C1127d<LockedResource<?>> {
        C1005a() {
        }

        /* renamed from: a */
        public LockedResource<?> m1851a() {
            return new LockedResource<>();
        }
    }

    LockedResource() {
    }

    /* renamed from: a */
    private void m1843a(Resource<Z> vVar) {
        this.f1257S = false;
        this.f1256R = true;
        this.f1255Q = vVar;
    }

    @NonNull
    /* renamed from: b */
    static <Z> LockedResource<Z> m1844b(Resource<Z> vVar) {
        LockedResource<Z> acquire = f1253T.acquire();
        Preconditions.m2828a(acquire);
        LockedResource<Z> uVar = acquire;
        uVar.m1843a(vVar);
        return uVar;
    }

    /* renamed from: e */
    private void m1845e() {
        this.f1255Q = null;
        f1253T.release(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public synchronized void mo10256c() {
        this.f1254P.mo10716a();
        if (this.f1256R) {
            this.f1256R = false;
            if (this.f1257S) {
                recycle();
            }
        } else {
            throw new IllegalStateException("Already unlocked");
        }
    }

    @NonNull
    /* renamed from: d */
    public StateVerifier mo10115d() {
        return this.f1254P;
    }

    @NonNull
    public Z get() {
        return this.f1255Q.get();
    }

    public synchronized void recycle() {
        this.f1254P.mo10716a();
        this.f1257S = true;
        if (!this.f1256R) {
            this.f1255Q.recycle();
            m1845e();
        }
    }

    @NonNull
    /* renamed from: b */
    public Class<Z> mo10228b() {
        return this.f1255Q.mo10228b();
    }

    /* renamed from: a */
    public int mo10226a() {
        return this.f1255Q.mo10226a();
    }
}
