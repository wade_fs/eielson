package com.bumptech.glide.load.p021m.p022o;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/* renamed from: com.bumptech.glide.load.m.o.e */
class ThumbnailStreamOpener {

    /* renamed from: f */
    private static final FileService f970f = new FileService();

    /* renamed from: a */
    private final FileService f971a;

    /* renamed from: b */
    private final ThumbnailQuery f972b;

    /* renamed from: c */
    private final ArrayPool f973c;

    /* renamed from: d */
    private final ContentResolver f974d;

    /* renamed from: e */
    private final List<ImageHeaderParser> f975e;

    ThumbnailStreamOpener(List<ImageHeaderParser> list, ThumbnailQuery dVar, ArrayPool bVar, ContentResolver contentResolver) {
        this(list, f970f, dVar, bVar, contentResolver);
    }

    @Nullable
    /* renamed from: c */
    private String m1479c(@NonNull Uri uri) {
        Cursor a = this.f972b.mo10021a(uri);
        if (a != null) {
            try {
                if (a.moveToFirst()) {
                    return a.getString(0);
                }
            } finally {
                if (a != null) {
                    a.close();
                }
            }
        }
        if (a != null) {
            a.close();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo10022a(Uri uri) {
        InputStream inputStream = null;
        try {
            InputStream openInputStream = this.f974d.openInputStream(uri);
            int a = ImageHeaderParserUtils.m1365a(this.f975e, openInputStream, this.f973c);
            if (openInputStream != null) {
                try {
                    openInputStream.close();
                } catch (IOException unused) {
                }
            }
            return a;
        } catch (IOException | NullPointerException e) {
            if (Log.isLoggable("ThumbStreamOpener", 3)) {
                Log.d("ThumbStreamOpener", "Failed to open uri: " + uri, e);
            }
            if (inputStream == null) {
                return -1;
            }
            try {
                inputStream.close();
                return -1;
            } catch (IOException unused2) {
                return -1;
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused3) {
                }
            }
            throw th;
        }
    }

    /* renamed from: b */
    public InputStream mo10023b(Uri uri) {
        String c = m1479c(uri);
        if (TextUtils.isEmpty(c)) {
            return null;
        }
        File a = this.f971a.mo10018a(c);
        if (!m1478a(a)) {
            return null;
        }
        Uri fromFile = Uri.fromFile(a);
        try {
            return this.f974d.openInputStream(fromFile);
        } catch (NullPointerException e) {
            throw ((FileNotFoundException) new FileNotFoundException("NPE opening uri: " + uri + " -> " + fromFile).initCause(e));
        }
    }

    ThumbnailStreamOpener(List<ImageHeaderParser> list, FileService aVar, ThumbnailQuery dVar, ArrayPool bVar, ContentResolver contentResolver) {
        this.f971a = aVar;
        this.f972b = dVar;
        this.f973c = bVar;
        this.f974d = contentResolver;
        this.f975e = list;
    }

    /* renamed from: a */
    private boolean m1478a(File file) {
        return this.f971a.mo10019a(file) && 0 < this.f971a.mo10020b(file);
    }
}
