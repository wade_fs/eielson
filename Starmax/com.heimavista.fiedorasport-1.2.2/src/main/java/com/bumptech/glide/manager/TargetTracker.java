package com.bumptech.glide.manager;

import androidx.annotation.NonNull;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.util.C1122j;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: com.bumptech.glide.manager.n */
public final class TargetTracker implements LifecycleListener {

    /* renamed from: P */
    private final Set<Target<?>> f1594P = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: a */
    public void mo10523a(@NonNull Target<?> iVar) {
        this.f1594P.add(iVar);
    }

    /* renamed from: b */
    public void mo10524b(@NonNull Target<?> iVar) {
        this.f1594P.remove(iVar);
    }

    /* renamed from: c */
    public void mo9949c() {
        for (Target iVar : C1122j.m2843a(this.f1594P)) {
            iVar.mo9949c();
        }
    }

    /* renamed from: d */
    public void mo10525d() {
        this.f1594P.clear();
    }

    @NonNull
    /* renamed from: e */
    public List<Target<?>> mo10526e() {
        return C1122j.m2843a(this.f1594P);
    }

    public void onStart() {
        for (Target iVar : C1122j.m2843a(this.f1594P)) {
            iVar.onStart();
        }
    }

    /* renamed from: b */
    public void mo9947b() {
        for (Target iVar : C1122j.m2843a(this.f1594P)) {
            iVar.mo9947b();
        }
    }
}
