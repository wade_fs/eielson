package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import androidx.core.util.Pools;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.load.p023n.DecodePath;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/* renamed from: com.bumptech.glide.load.n.t */
public class LoadPath<Data, ResourceType, Transcode> {

    /* renamed from: a */
    private final Pools.Pool<List<Throwable>> f1250a;

    /* renamed from: b */
    private final List<? extends DecodePath<Data, ResourceType, Transcode>> f1251b;

    /* renamed from: c */
    private final String f1252c;

    public LoadPath(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<DecodePath<Data, ResourceType, Transcode>> list, Pools.Pool<List<Throwable>> pool) {
        this.f1250a = pool;
        Preconditions.m2831a((Collection) list);
        this.f1251b = list;
        this.f1252c = "Failed LoadPath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    /* renamed from: a */
    public Resource<Transcode> mo10254a(DataRewinder<Data> eVar, @NonNull Options iVar, int i, int i2, DecodePath.C0986a<ResourceType> aVar) {
        List<Throwable> acquire = this.f1250a.acquire();
        Preconditions.m2828a((Object) acquire);
        List list = acquire;
        try {
            return m1841a(eVar, iVar, i, i2, aVar, list);
        } finally {
            this.f1250a.release(list);
        }
    }

    public String toString() {
        return "LoadPath{decodePaths=" + Arrays.toString(this.f1251b.toArray()) + '}';
    }

    /* renamed from: a */
    private Resource<Transcode> m1841a(DataRewinder<Data> eVar, @NonNull Options iVar, int i, int i2, DecodePath.C0986a<ResourceType> aVar, List<Throwable> list) {
        List<Throwable> list2 = list;
        int size = this.f1251b.size();
        Resource<Transcode> vVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            try {
                vVar = ((DecodePath) this.f1251b.get(i3)).mo10185a(eVar, i, i2, iVar, aVar);
            } catch (GlideException e) {
                list2.add(e);
            }
            if (vVar != null) {
                break;
            }
        }
        if (vVar != null) {
            return vVar;
        }
        throw new GlideException(this.f1252c, new ArrayList(list2));
    }
}
