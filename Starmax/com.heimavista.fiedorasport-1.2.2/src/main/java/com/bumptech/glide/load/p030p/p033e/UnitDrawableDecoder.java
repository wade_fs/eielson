package com.bumptech.glide.load.p030p.p033e;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.p.e.e */
public class UnitDrawableDecoder implements ResourceDecoder<Drawable, Drawable> {
    /* renamed from: a */
    public boolean mo9980a(@NonNull Drawable drawable, @NonNull Options iVar) {
        return true;
    }

    @Nullable
    /* renamed from: a */
    public Resource<Drawable> mo9979a(@NonNull Drawable drawable, int i, int i2, @NonNull Options iVar) {
        return NonOwnedDrawableResource.m2260a(drawable);
    }
}
