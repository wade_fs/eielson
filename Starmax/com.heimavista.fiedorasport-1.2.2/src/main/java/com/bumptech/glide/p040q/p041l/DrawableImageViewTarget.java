package com.bumptech.glide.p040q.p041l;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.annotation.Nullable;

/* renamed from: com.bumptech.glide.q.l.d */
public class DrawableImageViewTarget extends ImageViewTarget<Drawable> {
    public DrawableImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo10659a(@Nullable Drawable drawable) {
        ((ImageView) this.f1751Q).setImageDrawable(drawable);
    }
}
