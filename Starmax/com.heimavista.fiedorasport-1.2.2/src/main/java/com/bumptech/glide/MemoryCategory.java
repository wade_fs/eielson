package com.bumptech.glide;

/* renamed from: com.bumptech.glide.f */
public enum MemoryCategory {
    LOW(0.5f),
    NORMAL(1.0f),
    HIGH(1.5f);

    private MemoryCategory(float f) {
    }
}
