package com.bumptech.glide.load.p023n.p024a0;

import com.bumptech.glide.load.p023n.p024a0.Poolable;
import com.bumptech.glide.util.C1122j;
import java.util.Queue;

/* renamed from: com.bumptech.glide.load.n.a0.d */
abstract class BaseKeyPool<T extends Poolable> {

    /* renamed from: a */
    private final Queue<T> f993a = C1122j.m2844a(20);

    BaseKeyPool() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract T mo10056a();

    /* renamed from: a */
    public void mo10058a(T t) {
        if (this.f993a.size() < 20) {
            this.f993a.offer(t);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public T mo10059b() {
        T t = (Poolable) this.f993a.poll();
        return t == null ? mo10056a() : t;
    }
}
