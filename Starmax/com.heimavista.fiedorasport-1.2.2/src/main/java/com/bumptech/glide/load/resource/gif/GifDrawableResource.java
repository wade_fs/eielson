package com.bumptech.glide.load.resource.gif;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.Initializable;
import com.bumptech.glide.load.p030p.p033e.DrawableResource;

/* renamed from: com.bumptech.glide.load.resource.gif.d */
public class GifDrawableResource extends DrawableResource<GifDrawable> implements Initializable {
    public GifDrawableResource(GifDrawable gifDrawable) {
        super(gifDrawable);
    }

    /* renamed from: a */
    public int mo10226a() {
        return ((GifDrawable) super.f1462P).mo10411f();
    }

    @NonNull
    /* renamed from: b */
    public Class<GifDrawable> mo10228b() {
        return GifDrawable.class;
    }

    /* renamed from: c */
    public void mo10250c() {
        ((GifDrawable) super.f1462P).mo10407c().prepareToDraw();
    }

    public void recycle() {
        ((GifDrawable) super.f1462P).stop();
        ((GifDrawable) super.f1462P).mo10412g();
    }
}
