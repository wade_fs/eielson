package com.bumptech.glide.p037n;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.nio.ByteBuffer;

/* renamed from: com.bumptech.glide.n.a */
public interface GifDecoder {

    /* renamed from: com.bumptech.glide.n.a$a */
    /* compiled from: GifDecoder */
    public interface C1107a {
        @NonNull
        /* renamed from: a */
        Bitmap mo10432a(int i, int i2, @NonNull Bitmap.Config config);

        /* renamed from: a */
        void mo10433a(@NonNull Bitmap bitmap);

        /* renamed from: a */
        void mo10434a(@NonNull byte[] bArr);

        /* renamed from: a */
        void mo10435a(@NonNull int[] iArr);

        @NonNull
        /* renamed from: a */
        int[] mo10436a(int i);

        @NonNull
        /* renamed from: b */
        byte[] mo10437b(int i);
    }

    @Nullable
    /* renamed from: a */
    Bitmap mo10527a();

    /* renamed from: a */
    void mo10528a(@NonNull Bitmap.Config config);

    /* renamed from: b */
    void mo10529b();

    /* renamed from: c */
    int mo10530c();

    void clear();

    /* renamed from: d */
    int mo10532d();

    /* renamed from: e */
    void mo10533e();

    /* renamed from: f */
    int mo10534f();

    /* renamed from: g */
    int mo10535g();

    @NonNull
    ByteBuffer getData();
}
