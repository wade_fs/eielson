package com.bumptech.glide.load.p023n.p024a0;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.bumptech.glide.load.n.a0.k */
public class LruBitmapPool implements BitmapPool {

    /* renamed from: j */
    private static final Bitmap.Config f1009j = Bitmap.Config.ARGB_8888;

    /* renamed from: a */
    private final LruPoolStrategy f1010a;

    /* renamed from: b */
    private final Set<Bitmap.Config> f1011b;

    /* renamed from: c */
    private final C0953a f1012c;

    /* renamed from: d */
    private long f1013d;

    /* renamed from: e */
    private long f1014e;

    /* renamed from: f */
    private int f1015f;

    /* renamed from: g */
    private int f1016g;

    /* renamed from: h */
    private int f1017h;

    /* renamed from: i */
    private int f1018i;

    /* renamed from: com.bumptech.glide.load.n.a0.k$a */
    /* compiled from: LruBitmapPool */
    private interface C0953a {
        /* renamed from: a */
        void mo10080a(Bitmap bitmap);

        /* renamed from: b */
        void mo10081b(Bitmap bitmap);
    }

    /* renamed from: com.bumptech.glide.load.n.a0.k$b */
    /* compiled from: LruBitmapPool */
    private static final class C0954b implements C0953a {
        C0954b() {
        }

        /* renamed from: a */
        public void mo10080a(Bitmap bitmap) {
        }

        /* renamed from: b */
        public void mo10081b(Bitmap bitmap) {
        }
    }

    LruBitmapPool(long j, LruPoolStrategy lVar, Set<Bitmap.Config> set) {
        this.f1013d = j;
        this.f1010a = lVar;
        this.f1011b = set;
        this.f1012c = new C0954b();
    }

    @NonNull
    /* renamed from: c */
    private static Bitmap m1564c(int i, int i2, @Nullable Bitmap.Config config) {
        if (config == null) {
            config = f1009j;
        }
        return Bitmap.createBitmap(i, i2, config);
    }

    @Nullable
    /* renamed from: d */
    private synchronized Bitmap m1567d(int i, int i2, @Nullable Bitmap.Config config) {
        Bitmap a;
        m1562a(config);
        a = this.f1010a.mo10045a(i, i2, config != null ? config : f1009j);
        if (a == null) {
            if (Log.isLoggable("LruBitmapPool", 3)) {
                Log.d("LruBitmapPool", "Missing bitmap=" + this.f1010a.mo10048b(i, i2, config));
            }
            this.f1016g++;
        } else {
            this.f1015f++;
            this.f1014e -= (long) this.f1010a.mo10047b(a);
            this.f1012c.mo10080a(a);
            m1566c(a);
        }
        if (Log.isLoggable("LruBitmapPool", 2)) {
            Log.v("LruBitmapPool", "Get bitmap=" + this.f1010a.mo10048b(i, i2, config));
        }
        m1565c();
        return a;
    }

    /* renamed from: e */
    private void m1569e() {
        m1561a(this.f1013d);
    }

    @TargetApi(26)
    /* renamed from: f */
    private static Set<Bitmap.Config> m1570f() {
        HashSet hashSet = new HashSet(Arrays.asList(Bitmap.Config.values()));
        if (Build.VERSION.SDK_INT >= 19) {
            hashSet.add(null);
        }
        if (Build.VERSION.SDK_INT >= 26) {
            hashSet.remove(Bitmap.Config.HARDWARE);
        }
        return Collections.unmodifiableSet(hashSet);
    }

    /* renamed from: g */
    private static LruPoolStrategy m1571g() {
        if (Build.VERSION.SDK_INT >= 19) {
            return new SizeConfigStrategy();
        }
        return new AttributeStrategy();
    }

    /* renamed from: a */
    public synchronized void mo10063a(Bitmap bitmap) {
        if (bitmap != null) {
            try {
                if (!bitmap.isRecycled()) {
                    if (bitmap.isMutable() && ((long) this.f1010a.mo10047b(bitmap)) <= this.f1013d) {
                        if (this.f1011b.contains(bitmap.getConfig())) {
                            int b = this.f1010a.mo10047b(bitmap);
                            this.f1010a.mo10046a(bitmap);
                            this.f1012c.mo10081b(bitmap);
                            this.f1017h++;
                            this.f1014e += (long) b;
                            if (Log.isLoggable("LruBitmapPool", 2)) {
                                Log.v("LruBitmapPool", "Put bitmap in pool=" + this.f1010a.mo10049c(bitmap));
                            }
                            m1565c();
                            m1569e();
                            return;
                        }
                    }
                    if (Log.isLoggable("LruBitmapPool", 2)) {
                        Log.v("LruBitmapPool", "Reject bitmap from pool, bitmap: " + this.f1010a.mo10049c(bitmap) + ", is mutable: " + bitmap.isMutable() + ", is allowed config: " + this.f1011b.contains(bitmap.getConfig()));
                    }
                    bitmap.recycle();
                    return;
                }
                throw new IllegalStateException("Cannot pool recycled bitmap");
            } catch (Throwable th) {
                throw th;
            }
        } else {
            throw new NullPointerException("Bitmap must not be null");
        }
    }

    /* renamed from: b */
    public long mo10079b() {
        return this.f1013d;
    }

    /* renamed from: c */
    private static void m1566c(Bitmap bitmap) {
        bitmap.setHasAlpha(true);
        m1563b(bitmap);
    }

    @NonNull
    /* renamed from: b */
    public Bitmap mo10064b(int i, int i2, Bitmap.Config config) {
        Bitmap d = m1567d(i, i2, config);
        return d == null ? m1564c(i, i2, config) : d;
    }

    @TargetApi(19)
    /* renamed from: b */
    private static void m1563b(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= 19) {
            bitmap.setPremultiplied(true);
        }
    }

    /* renamed from: c */
    private void m1565c() {
        if (Log.isLoggable("LruBitmapPool", 2)) {
            m1568d();
        }
    }

    public LruBitmapPool(long j) {
        this(j, m1571g(), m1570f());
    }

    /* renamed from: d */
    private void m1568d() {
        Log.v("LruBitmapPool", "Hits=" + this.f1015f + ", misses=" + this.f1016g + ", puts=" + this.f1017h + ", evictions=" + this.f1018i + ", currentSize=" + this.f1014e + ", maxSize=" + this.f1013d + "\nStrategy=" + this.f1010a);
    }

    @NonNull
    /* renamed from: a */
    public Bitmap mo10060a(int i, int i2, Bitmap.Config config) {
        Bitmap d = m1567d(i, i2, config);
        if (d == null) {
            return m1564c(i, i2, config);
        }
        d.eraseColor(0);
        return d;
    }

    @TargetApi(26)
    /* renamed from: a */
    private static void m1562a(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && config == Bitmap.Config.HARDWARE) {
            throw new IllegalArgumentException("Cannot create a mutable Bitmap with config: " + config + ". Consider setting Downsampler#ALLOW_HARDWARE_CONFIG to false in your RequestOptions and/or in GlideBuilder.setDefaultRequestOptions");
        }
    }

    /* renamed from: a */
    public void mo10061a() {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "clearMemory");
        }
        m1561a(0L);
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: a */
    public void mo10062a(int i) {
        if (Log.isLoggable("LruBitmapPool", 3)) {
            Log.d("LruBitmapPool", "trimMemory, level=" + i);
        }
        if (i >= 40) {
            mo10061a();
        } else if (i >= 20 || i == 15) {
            m1561a(mo10079b() / 2);
        }
    }

    /* renamed from: a */
    private synchronized void m1561a(long j) {
        while (this.f1014e > j) {
            Bitmap a = this.f1010a.mo10044a();
            if (a == null) {
                if (Log.isLoggable("LruBitmapPool", 5)) {
                    Log.w("LruBitmapPool", "Size mismatch, resetting");
                    m1568d();
                }
                this.f1014e = 0;
                return;
            }
            this.f1012c.mo10080a(a);
            this.f1014e -= (long) this.f1010a.mo10047b(a);
            this.f1018i++;
            if (Log.isLoggable("LruBitmapPool", 3)) {
                Log.d("LruBitmapPool", "Evicting bitmap=" + this.f1010a.mo10049c(a));
            }
            m1565c();
            a.recycle();
        }
    }
}
