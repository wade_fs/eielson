package com.bumptech.glide.load.p030p.p031c;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.Log;
import com.bumptech.glide.load.DecodeFormat;
import java.io.File;

/* renamed from: com.bumptech.glide.load.p.c.q */
final class HardwareConfigState {

    /* renamed from: c */
    private static final File f1427c = new File("/proc/self/fd");

    /* renamed from: d */
    private static volatile HardwareConfigState f1428d;

    /* renamed from: a */
    private volatile int f1429a;

    /* renamed from: b */
    private volatile boolean f1430b = true;

    private HardwareConfigState() {
    }

    /* renamed from: a */
    static HardwareConfigState m2187a() {
        if (f1428d == null) {
            synchronized (HardwareConfigState.class) {
                if (f1428d == null) {
                    f1428d = new HardwareConfigState();
                }
            }
        }
        return f1428d;
    }

    /* renamed from: b */
    private synchronized boolean m2188b() {
        int i = this.f1429a + 1;
        this.f1429a = i;
        if (i >= 50) {
            boolean z = false;
            this.f1429a = 0;
            int length = f1427c.list().length;
            if (length < 700) {
                z = true;
            }
            this.f1430b = z;
            if (!this.f1430b && Log.isLoggable("Downsampler", 5)) {
                Log.w("Downsampler", "Excluding HARDWARE bitmap config because we're over the file descriptor limit, file descriptors " + length + ", limit " + 700);
            }
        }
        return this.f1430b;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(26)
    /* renamed from: a */
    public boolean mo10363a(int i, int i2, BitmapFactory.Options options, DecodeFormat bVar, boolean z, boolean z2) {
        if (!z || Build.VERSION.SDK_INT < 26 || z2) {
            return false;
        }
        boolean z3 = i >= 128 && i2 >= 128 && m2188b();
        if (z3) {
            options.inPreferredConfig = Bitmap.Config.HARDWARE;
            options.inMutable = false;
        }
        return z3;
    }
}
