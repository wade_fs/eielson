package com.bumptech.glide.load.p030p.p031c;

import android.annotation.TargetApi;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.p.c.x */
public class VideoDecoder<T> implements ResourceDecoder<T, Bitmap> {

    /* renamed from: d */
    public static final Option<Long> f1451d = Option.m1371a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.TargetFrame", -1L, new C1080a());

    /* renamed from: e */
    public static final Option<Integer> f1452e = Option.m1371a("com.bumptech.glide.load.resource.bitmap.VideoBitmapDecode.FrameOption", 2, new C1081b());

    /* renamed from: f */
    private static final C1083d f1453f = new C1083d();

    /* renamed from: a */
    private final C1084e<T> f1454a;

    /* renamed from: b */
    private final BitmapPool f1455b;

    /* renamed from: c */
    private final C1083d f1456c;

    /* renamed from: com.bumptech.glide.load.p.c.x$a */
    /* compiled from: VideoDecoder */
    class C1080a implements Option.C0933b<Long> {

        /* renamed from: a */
        private final ByteBuffer f1457a = ByteBuffer.allocate(8);

        C1080a() {
        }

        /* renamed from: a */
        public void mo9974a(@NonNull byte[] bArr, @NonNull Long l, @NonNull MessageDigest messageDigest) {
            messageDigest.update(bArr);
            synchronized (this.f1457a) {
                this.f1457a.position(0);
                messageDigest.update(this.f1457a.putLong(l.longValue()).array());
            }
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.x$b */
    /* compiled from: VideoDecoder */
    class C1081b implements Option.C0933b<Integer> {

        /* renamed from: a */
        private final ByteBuffer f1458a = ByteBuffer.allocate(4);

        C1081b() {
        }

        /* renamed from: a */
        public void mo9974a(@NonNull byte[] bArr, @NonNull Integer num, @NonNull MessageDigest messageDigest) {
            if (num != null) {
                messageDigest.update(bArr);
                synchronized (this.f1458a) {
                    this.f1458a.position(0);
                    messageDigest.update(this.f1458a.putInt(num.intValue()).array());
                }
            }
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.x$c */
    /* compiled from: VideoDecoder */
    private static final class C1082c implements C1084e<AssetFileDescriptor> {
        private C1082c() {
        }

        /* synthetic */ C1082c(C1080a aVar) {
            this();
        }

        /* renamed from: a */
        public void mo10389a(MediaMetadataRetriever mediaMetadataRetriever, AssetFileDescriptor assetFileDescriptor) {
            mediaMetadataRetriever.setDataSource(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength());
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.p.c.x$d */
    /* compiled from: VideoDecoder */
    static class C1083d {
        C1083d() {
        }

        /* renamed from: a */
        public MediaMetadataRetriever mo10390a() {
            return new MediaMetadataRetriever();
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.p.c.x$e */
    /* compiled from: VideoDecoder */
    interface C1084e<T> {
        /* renamed from: a */
        void mo10389a(MediaMetadataRetriever mediaMetadataRetriever, T t);
    }

    /* renamed from: com.bumptech.glide.load.p.c.x$f */
    /* compiled from: VideoDecoder */
    static final class C1085f implements C1084e<ParcelFileDescriptor> {
        C1085f() {
        }

        /* renamed from: a */
        public void mo10389a(MediaMetadataRetriever mediaMetadataRetriever, ParcelFileDescriptor parcelFileDescriptor) {
            mediaMetadataRetriever.setDataSource(parcelFileDescriptor.getFileDescriptor());
        }
    }

    VideoDecoder(BitmapPool eVar, C1084e<T> eVar2) {
        this(eVar, eVar2, f1453f);
    }

    /* renamed from: a */
    public static ResourceDecoder<AssetFileDescriptor, Bitmap> m2231a(BitmapPool eVar) {
        return new VideoDecoder(eVar, new C1082c(null));
    }

    /* renamed from: b */
    public static ResourceDecoder<ParcelFileDescriptor, Bitmap> m2233b(BitmapPool eVar) {
        return new VideoDecoder(eVar, new C1085f());
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull T t, @NonNull Options iVar) {
        return true;
    }

    @VisibleForTesting
    VideoDecoder(BitmapPool eVar, C1084e<T> eVar2, C1083d dVar) {
        this.f1455b = eVar;
        this.f1454a = eVar2;
        this.f1456c = dVar;
    }

    @TargetApi(27)
    /* renamed from: b */
    private static Bitmap m2232b(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, DownsampleStrategy kVar) {
        try {
            int parseInt = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
            int parseInt2 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
            int parseInt3 = Integer.parseInt(mediaMetadataRetriever.extractMetadata(24));
            if (parseInt3 == 90 || parseInt3 == 270) {
                int i4 = parseInt2;
                parseInt2 = parseInt;
                parseInt = i4;
            }
            float b = kVar.mo10355b(parseInt, parseInt2, i2, i3);
            return mediaMetadataRetriever.getScaledFrameAtTime(j, i, Math.round(((float) parseInt) * b), Math.round(b * ((float) parseInt2)));
        } catch (Throwable th) {
            if (!Log.isLoggable("VideoDecoder", 3)) {
                return null;
            }
            Log.d("VideoDecoder", "Exception trying to decode frame on oreo+", th);
            return null;
        }
    }

    /* renamed from: a */
    public Resource<Bitmap> mo9979a(@NonNull T t, int i, int i2, @NonNull Options iVar) {
        long longValue = ((Long) iVar.mo9976a(f1451d)).longValue();
        if (longValue >= 0 || longValue == -1) {
            Integer num = (Integer) iVar.mo9976a(f1452e);
            if (num == null) {
                num = 2;
            }
            DownsampleStrategy kVar = (DownsampleStrategy) iVar.mo9976a(DownsampleStrategy.f1407f);
            if (kVar == null) {
                kVar = DownsampleStrategy.f1406e;
            }
            DownsampleStrategy kVar2 = kVar;
            MediaMetadataRetriever a = this.f1456c.mo10390a();
            try {
                this.f1454a.mo10389a(a, t);
                Bitmap a2 = m2230a(a, longValue, num.intValue(), i, i2, kVar2);
                a.release();
                return BitmapResource.m2091a(a2, this.f1455b);
            } catch (RuntimeException e) {
                throw new IOException(e);
            } catch (Throwable th) {
                a.release();
                throw th;
            }
        } else {
            throw new IllegalArgumentException("Requested frame must be non-negative, or DEFAULT_FRAME, given: " + longValue);
        }
    }

    @Nullable
    /* renamed from: a */
    private static Bitmap m2230a(MediaMetadataRetriever mediaMetadataRetriever, long j, int i, int i2, int i3, DownsampleStrategy kVar) {
        Bitmap b = (Build.VERSION.SDK_INT < 27 || i2 == Integer.MIN_VALUE || i3 == Integer.MIN_VALUE || kVar == DownsampleStrategy.f1405d) ? null : m2232b(mediaMetadataRetriever, j, i, i2, i3, kVar);
        return b == null ? m2229a(mediaMetadataRetriever, j, i) : b;
    }

    /* renamed from: a */
    private static Bitmap m2229a(MediaMetadataRetriever mediaMetadataRetriever, long j, int i) {
        return mediaMetadataRetriever.getFrameAtTime(j, i);
    }
}
