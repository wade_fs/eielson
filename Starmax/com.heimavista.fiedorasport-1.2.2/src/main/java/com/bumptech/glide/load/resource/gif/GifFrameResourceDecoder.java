package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.p031c.BitmapResource;
import com.bumptech.glide.p037n.GifDecoder;

/* renamed from: com.bumptech.glide.load.resource.gif.g */
public final class GifFrameResourceDecoder implements ResourceDecoder<GifDecoder, Bitmap> {

    /* renamed from: a */
    private final BitmapPool f1518a;

    public GifFrameResourceDecoder(BitmapPool eVar) {
        this.f1518a = eVar;
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull GifDecoder aVar, @NonNull Options iVar) {
        return true;
    }

    /* renamed from: a */
    public Resource<Bitmap> mo9979a(@NonNull GifDecoder aVar, int i, int i2, @NonNull Options iVar) {
        return BitmapResource.m2091a(aVar.mo10527a(), this.f1518a);
    }
}
