package com.bumptech.glide.util;

import androidx.annotation.NonNull;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.util.b */
public final class ContentLengthInputStream extends FilterInputStream {

    /* renamed from: P */
    private final long f1772P;

    /* renamed from: Q */
    private int f1773Q;

    private ContentLengthInputStream(@NonNull InputStream inputStream, long j) {
        super(inputStream);
        this.f1772P = j;
    }

    @NonNull
    /* renamed from: a */
    public static InputStream m2807a(@NonNull InputStream inputStream, long j) {
        return new ContentLengthInputStream(inputStream, j);
    }

    public synchronized int available() {
        return (int) Math.max(this.f1772P - ((long) this.f1773Q), (long) super.in.available());
    }

    public synchronized int read() {
        int read;
        read = super.read();
        m2806a(read >= 0 ? 1 : -1);
        return read;
    }

    /* renamed from: a */
    private int m2806a(int i) {
        if (i >= 0) {
            this.f1773Q += i;
        } else if (this.f1772P - ((long) this.f1773Q) > 0) {
            throw new IOException("Failed to read all expected data, expected: " + this.f1772P + ", but read: " + this.f1773Q);
        }
        return i;
    }

    public int read(byte[] bArr) {
        return read(bArr, 0, bArr.length);
    }

    public synchronized int read(byte[] bArr, int i, int i2) {
        int read;
        read = super.read(bArr, i, i2);
        m2806a(read);
        return read;
    }
}
