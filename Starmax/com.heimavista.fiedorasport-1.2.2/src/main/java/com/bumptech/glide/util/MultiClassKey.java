package com.bumptech.glide.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: com.bumptech.glide.util.h */
public class MultiClassKey {

    /* renamed from: a */
    private Class<?> f1785a;

    /* renamed from: b */
    private Class<?> f1786b;

    /* renamed from: c */
    private Class<?> f1787c;

    public MultiClassKey() {
    }

    /* renamed from: a */
    public void mo10710a(@NonNull Class<?> cls, @NonNull Class<?> cls2, @Nullable Class<?> cls3) {
        this.f1785a = cls;
        this.f1786b = cls2;
        this.f1787c = cls3;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || MultiClassKey.class != obj.getClass()) {
            return false;
        }
        MultiClassKey hVar = (MultiClassKey) obj;
        return this.f1785a.equals(hVar.f1785a) && this.f1786b.equals(hVar.f1786b) && C1122j.m2850b(this.f1787c, hVar.f1787c);
    }

    public int hashCode() {
        int hashCode = ((this.f1785a.hashCode() * 31) + this.f1786b.hashCode()) * 31;
        Class<?> cls = this.f1787c;
        return hashCode + (cls != null ? cls.hashCode() : 0);
    }

    public String toString() {
        return "MultiClassKey{first=" + this.f1785a + ", second=" + this.f1786b + '}';
    }

    public MultiClassKey(@NonNull Class<?> cls, @NonNull Class<?> cls2, @Nullable Class<?> cls3) {
        mo10710a(cls, cls2, cls3);
    }
}
