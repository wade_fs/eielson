package com.bumptech.glide.load.p023n.p024a0;

/* renamed from: com.bumptech.glide.load.n.a0.g */
public final class ByteArrayAdapter implements ArrayAdapterInterface<byte[]> {
    /* renamed from: a */
    public int mo10035a() {
        return 1;
    }

    /* renamed from: b */
    public String mo10037b() {
        return "ByteArrayPool";
    }

    /* renamed from: a */
    public int mo10036a(byte[] bArr) {
        return bArr.length;
    }

    public byte[] newArray(int i) {
        return new byte[i];
    }
}
