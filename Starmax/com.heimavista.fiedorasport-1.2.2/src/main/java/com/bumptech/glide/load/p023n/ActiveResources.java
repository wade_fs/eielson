package com.bumptech.glide.load.p023n;

import android.os.Process;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.EngineResource;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

/* renamed from: com.bumptech.glide.load.n.a */
final class ActiveResources {

    /* renamed from: a */
    private final boolean f976a;
    @VisibleForTesting

    /* renamed from: b */
    final Map<Key, C0947d> f977b;

    /* renamed from: c */
    private final ReferenceQueue<EngineResource<?>> f978c;

    /* renamed from: d */
    private EngineResource.C1003a f979d;

    /* renamed from: e */
    private volatile boolean f980e;
    @Nullable

    /* renamed from: f */
    private volatile C0946c f981f;

    /* renamed from: com.bumptech.glide.load.n.a$a */
    /* compiled from: ActiveResources */
    class C0943a implements ThreadFactory {

        /* renamed from: com.bumptech.glide.load.n.a$a$a */
        /* compiled from: ActiveResources */
        class C0944a implements Runnable {

            /* renamed from: P */
            final /* synthetic */ Runnable f982P;

            C0944a(C0943a aVar, Runnable runnable) {
                this.f982P = runnable;
            }

            public void run() {
                Process.setThreadPriority(10);
                this.f982P.run();
            }
        }

        C0943a() {
        }

        public Thread newThread(@NonNull Runnable runnable) {
            return new Thread(new C0944a(this, runnable), "glide-active-resources");
        }
    }

    /* renamed from: com.bumptech.glide.load.n.a$b */
    /* compiled from: ActiveResources */
    class C0945b implements Runnable {
        C0945b() {
        }

        public void run() {
            ActiveResources.this.mo10024a();
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.a$c */
    /* compiled from: ActiveResources */
    interface C0946c {
        /* renamed from: a */
        void mo10033a();
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.a$d */
    /* compiled from: ActiveResources */
    static final class C0947d extends WeakReference<EngineResource<?>> {

        /* renamed from: a */
        final Key f984a;

        /* renamed from: b */
        final boolean f985b;
        @Nullable

        /* renamed from: c */
        Resource<?> f986c;

        C0947d(@NonNull Key gVar, @NonNull EngineResource<?> pVar, @NonNull ReferenceQueue<? super EngineResource<?>> referenceQueue, boolean z) {
            super(pVar, referenceQueue);
            Resource<?> vVar;
            Preconditions.m2828a(gVar);
            this.f984a = gVar;
            if (!pVar.mo10231e() || !z) {
                vVar = null;
            } else {
                Resource<?> d = pVar.mo10230d();
                Preconditions.m2828a(d);
                vVar = d;
            }
            this.f986c = vVar;
            this.f985b = pVar.mo10231e();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10034a() {
            this.f986c = null;
            clear();
        }
    }

    ActiveResources(boolean z) {
        this(z, Executors.newSingleThreadExecutor(new C0943a()));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10028a(EngineResource.C1003a aVar) {
        synchronized (aVar) {
            synchronized (this) {
                this.f979d = aVar;
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001a, code lost:
        return r0;
     */
    @androidx.annotation.Nullable
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized com.bumptech.glide.load.p023n.EngineResource<?> mo10029b(com.bumptech.glide.load.Key r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            java.util.Map<com.bumptech.glide.load.g, com.bumptech.glide.load.n.a$d> r0 = r1.f977b     // Catch:{ all -> 0x001b }
            java.lang.Object r2 = r0.get(r2)     // Catch:{ all -> 0x001b }
            com.bumptech.glide.load.n.a$d r2 = (com.bumptech.glide.load.p023n.ActiveResources.C0947d) r2     // Catch:{ all -> 0x001b }
            if (r2 != 0) goto L_0x000e
            r2 = 0
            monitor-exit(r1)
            return r2
        L_0x000e:
            java.lang.Object r0 = r2.get()     // Catch:{ all -> 0x001b }
            com.bumptech.glide.load.n.p r0 = (com.bumptech.glide.load.p023n.EngineResource) r0     // Catch:{ all -> 0x001b }
            if (r0 != 0) goto L_0x0019
            r1.mo10027a(r2)     // Catch:{ all -> 0x001b }
        L_0x0019:
            monitor-exit(r1)
            return r0
        L_0x001b:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.ActiveResources.mo10029b(com.bumptech.glide.load.g):com.bumptech.glide.load.n.p");
    }

    @VisibleForTesting
    ActiveResources(boolean z, Executor executor) {
        this.f977b = new HashMap();
        this.f978c = new ReferenceQueue<>();
        this.f976a = z;
        executor.execute(new C0945b());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10026a(Key gVar, EngineResource<?> pVar) {
        C0947d put = this.f977b.put(gVar, new C0947d(gVar, pVar, this.f978c, this.f976a));
        if (put != null) {
            put.mo10034a();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10025a(Key gVar) {
        C0947d remove = this.f977b.remove(gVar);
        if (remove != null) {
            remove.mo10034a();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10027a(@NonNull C0947d dVar) {
        synchronized (this.f979d) {
            synchronized (this) {
                this.f977b.remove(dVar.f984a);
                if (dVar.f985b) {
                    if (dVar.f986c != null) {
                        EngineResource pVar = new EngineResource(dVar.f986c, true, false);
                        pVar.mo10227a(dVar.f984a, this.f979d);
                        this.f979d.mo10192a(dVar.f984a, pVar);
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10024a() {
        while (!this.f980e) {
            try {
                mo10027a((C0947d) this.f978c.remove());
                C0946c cVar = this.f981f;
                if (cVar != null) {
                    cVar.mo10033a();
                }
            } catch (InterruptedException unused) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
