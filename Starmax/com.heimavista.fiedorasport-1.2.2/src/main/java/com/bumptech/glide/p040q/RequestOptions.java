package com.bumptech.glide.p040q;

import androidx.annotation.CheckResult;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.DiskCacheStrategy;

/* renamed from: com.bumptech.glide.q.h */
public class RequestOptions extends BaseRequestOptions<RequestOptions> {
    @CheckResult
    @NonNull
    /* renamed from: b */
    public static RequestOptions m2678b(@NonNull DiskCacheStrategy jVar) {
        return (RequestOptions) new RequestOptions().mo10587a(jVar);
    }

    @CheckResult
    @NonNull
    /* renamed from: b */
    public static RequestOptions m2677b(@NonNull Key gVar) {
        return (RequestOptions) new RequestOptions().mo10583a(gVar);
    }

    @CheckResult
    @NonNull
    /* renamed from: b */
    public static RequestOptions m2679b(@NonNull Class<?> cls) {
        return (RequestOptions) new RequestOptions().mo10590a(cls);
    }
}
