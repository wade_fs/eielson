package com.bumptech.glide.load.p023n;

import androidx.annotation.Nullable;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p021m.DataFetcher;

/* renamed from: com.bumptech.glide.load.n.f */
interface DataFetcherGenerator {

    /* renamed from: com.bumptech.glide.load.n.f$a */
    /* compiled from: DataFetcherGenerator */
    public interface C0977a {
        /* renamed from: a */
        void mo10137a(Key gVar, Exception exc, DataFetcher<?> dVar, DataSource aVar);

        /* renamed from: a */
        void mo10138a(Key gVar, @Nullable Object obj, DataFetcher<?> dVar, DataSource aVar, Key gVar2);

        /* renamed from: b */
        void mo10139b();
    }

    /* renamed from: a */
    boolean mo10116a();

    void cancel();
}
