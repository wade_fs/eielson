package com.bumptech.glide.load.p023n;

import com.bumptech.glide.load.Key;

/* renamed from: com.bumptech.glide.load.n.m */
interface EngineJobListener {
    /* renamed from: a */
    void mo10193a(EngineJob<?> lVar, Key gVar);

    /* renamed from: a */
    void mo10194a(EngineJob<?> lVar, Key gVar, EngineResource<?> pVar);
}
