package com.bumptech.glide.p040q.p041l;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.p040q.Request;
import com.bumptech.glide.util.C1122j;

/* renamed from: com.bumptech.glide.q.l.c */
public abstract class CustomTarget<T> implements Target<T> {

    /* renamed from: P */
    private final int f1744P;

    /* renamed from: Q */
    private final int f1745Q;
    @Nullable

    /* renamed from: R */
    private Request f1746R;

    public CustomTarget() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    /* renamed from: a */
    public void mo10639a(@Nullable Drawable drawable) {
    }

    /* renamed from: a */
    public final void mo10640a(@Nullable Request dVar) {
        this.f1746R = dVar;
    }

    /* renamed from: a */
    public final void mo10641a(@NonNull SizeReadyCallback hVar) {
    }

    /* renamed from: b */
    public void mo9947b() {
    }

    /* renamed from: b */
    public void mo10644b(@Nullable Drawable drawable) {
    }

    /* renamed from: b */
    public final void mo10645b(@NonNull SizeReadyCallback hVar) {
        hVar.mo10656a(this.f1744P, this.f1745Q);
    }

    /* renamed from: c */
    public void mo9949c() {
    }

    public void onStart() {
    }

    public CustomTarget(int i, int i2) {
        if (C1122j.m2849b(i, i2)) {
            this.f1744P = i;
            this.f1745Q = i2;
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + i + " and height: " + i2);
    }

    @Nullable
    /* renamed from: a */
    public final Request mo10638a() {
        return this.f1746R;
    }
}
