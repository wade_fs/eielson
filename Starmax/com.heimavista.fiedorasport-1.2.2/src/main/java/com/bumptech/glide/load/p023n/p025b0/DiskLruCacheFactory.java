package com.bumptech.glide.load.p023n.p025b0;

import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import java.io.File;

/* renamed from: com.bumptech.glide.load.n.b0.d */
public class DiskLruCacheFactory implements DiskCache.C0958a {

    /* renamed from: a */
    private final long f1036a;

    /* renamed from: b */
    private final C0962a f1037b;

    /* renamed from: com.bumptech.glide.load.n.b0.d$a */
    /* compiled from: DiskLruCacheFactory */
    public interface C0962a {
        /* renamed from: a */
        File mo10096a();
    }

    public DiskLruCacheFactory(C0962a aVar, long j) {
        this.f1036a = j;
        this.f1037b = aVar;
    }

    public DiskCache build() {
        File a = this.f1037b.mo10096a();
        if (a == null) {
            return null;
        }
        if (a.mkdirs() || (a.exists() && a.isDirectory())) {
            return DiskLruCacheWrapper.m1615a(a, this.f1036a);
        }
        return null;
    }
}
