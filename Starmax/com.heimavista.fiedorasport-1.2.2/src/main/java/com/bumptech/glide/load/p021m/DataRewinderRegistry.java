package com.bumptech.glide.load.p021m;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.util.Preconditions;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.m.f */
public class DataRewinderRegistry {

    /* renamed from: b */
    private static final DataRewinder.C0935a<?> f942b = new C0936a();

    /* renamed from: a */
    private final Map<Class<?>, DataRewinder.C0935a<?>> f943a = new HashMap();

    /* renamed from: com.bumptech.glide.load.m.f$a */
    /* compiled from: DataRewinderRegistry */
    class C0936a implements DataRewinder.C0935a<Object> {
        C0936a() {
        }

        @NonNull
        /* renamed from: a */
        public DataRewinder<Object> mo10002a(@NonNull Object obj) {
            return new C0937b(obj);
        }

        @NonNull
        /* renamed from: a */
        public Class<Object> mo10003a() {
            throw new UnsupportedOperationException("Not implemented");
        }
    }

    /* renamed from: com.bumptech.glide.load.m.f$b */
    /* compiled from: DataRewinderRegistry */
    private static final class C0937b implements DataRewinder<Object> {

        /* renamed from: a */
        private final Object f944a;

        C0937b(@NonNull Object obj) {
            this.f944a = obj;
        }

        @NonNull
        /* renamed from: a */
        public Object mo10000a() {
            return this.f944a;
        }

        /* renamed from: b */
        public void mo10001b() {
        }
    }

    /* renamed from: a */
    public synchronized void mo10005a(@NonNull DataRewinder.C0935a<?> aVar) {
        this.f943a.put(aVar.mo10003a(), aVar);
    }

    @NonNull
    /* renamed from: a */
    public synchronized <T> DataRewinder<T> mo10004a(@NonNull Object obj) {
        DataRewinder.C0935a<?> aVar;
        Preconditions.m2828a(obj);
        aVar = this.f943a.get(obj.getClass());
        if (aVar == null) {
            Iterator<DataRewinder.C0935a<?>> it = this.f943a.values().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                DataRewinder.C0935a<?> next = it.next();
                if (next.mo10003a().isAssignableFrom(obj.getClass())) {
                    aVar = next;
                    break;
                }
            }
        }
        if (aVar == null) {
            aVar = f942b;
        }
        return aVar.mo10002a(obj);
    }
}
