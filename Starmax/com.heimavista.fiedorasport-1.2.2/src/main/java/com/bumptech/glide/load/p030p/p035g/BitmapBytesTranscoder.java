package com.bumptech.glide.load.p030p.p035g;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p030p.p032d.BytesResource;
import java.io.ByteArrayOutputStream;

/* renamed from: com.bumptech.glide.load.p.g.a */
public class BitmapBytesTranscoder implements ResourceTranscoder<Bitmap, byte[]> {

    /* renamed from: a */
    private final Bitmap.CompressFormat f1464a;

    /* renamed from: b */
    private final int f1465b;

    public BitmapBytesTranscoder() {
        this(Bitmap.CompressFormat.JPEG, 100);
    }

    @Nullable
    /* renamed from: a */
    public Resource<byte[]> mo10399a(@NonNull Resource<Bitmap> vVar, @NonNull Options iVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        vVar.get().compress(this.f1464a, this.f1465b, byteArrayOutputStream);
        vVar.recycle();
        return new BytesResource(byteArrayOutputStream.toByteArray());
    }

    public BitmapBytesTranscoder(@NonNull Bitmap.CompressFormat compressFormat, int i) {
        this.f1464a = compressFormat;
        this.f1465b = i;
    }
}
