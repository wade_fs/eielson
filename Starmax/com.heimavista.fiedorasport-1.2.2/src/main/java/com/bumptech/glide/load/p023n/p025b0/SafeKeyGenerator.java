package com.bumptech.glide.load.p023n.p025b0;

import androidx.annotation.NonNull;
import androidx.core.util.Pools;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.LruCache;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.p044k.FactoryPools;
import com.bumptech.glide.util.p044k.StateVerifier;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: com.bumptech.glide.load.n.b0.j */
public class SafeKeyGenerator {

    /* renamed from: a */
    private final LruCache<Key, String> f1060a = new LruCache<>(1000);

    /* renamed from: b */
    private final Pools.Pool<C0969b> f1061b = FactoryPools.m2854a(10, new C0968a(this));

    /* renamed from: com.bumptech.glide.load.n.b0.j$a */
    /* compiled from: SafeKeyGenerator */
    class C0968a implements FactoryPools.C1127d<C0969b> {
        C0968a(SafeKeyGenerator jVar) {
        }

        /* renamed from: a */
        public C0969b m1648a() {
            try {
                return new C0969b(MessageDigest.getInstance("SHA-256"));
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /* renamed from: com.bumptech.glide.load.n.b0.j$b */
    /* compiled from: SafeKeyGenerator */
    private static final class C0969b implements FactoryPools.C1129f {

        /* renamed from: P */
        final MessageDigest f1062P;

        /* renamed from: Q */
        private final StateVerifier f1063Q = StateVerifier.m2871b();

        C0969b(MessageDigest messageDigest) {
            this.f1062P = messageDigest;
        }

        @NonNull
        /* renamed from: d */
        public StateVerifier mo10115d() {
            return this.f1063Q;
        }
    }

    /* renamed from: b */
    private String m1645b(Key gVar) {
        C0969b acquire = this.f1061b.acquire();
        Preconditions.m2828a(acquire);
        C0969b bVar = acquire;
        try {
            gVar.mo9966a(bVar.f1062P);
            return C1122j.m2841a(bVar.f1062P.digest());
        } finally {
            this.f1061b.release(bVar);
        }
    }

    /* renamed from: a */
    public String mo10113a(Key gVar) {
        String a;
        synchronized (this.f1060a) {
            a = this.f1060a.mo10698a(gVar);
        }
        if (a == null) {
            a = m1645b(gVar);
        }
        synchronized (this.f1060a) {
            this.f1060a.mo10702b(gVar, a);
        }
        return a;
    }
}
