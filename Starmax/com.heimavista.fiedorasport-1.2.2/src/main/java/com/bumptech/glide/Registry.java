package com.bumptech.glide;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pools;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.load.p021m.DataRewinderRegistry;
import com.bumptech.glide.load.p023n.DecodePath;
import com.bumptech.glide.load.p023n.LoadPath;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p028o.ModelLoaderFactory;
import com.bumptech.glide.load.p028o.ModelLoaderRegistry;
import com.bumptech.glide.load.p030p.p035g.ResourceTranscoder;
import com.bumptech.glide.load.p030p.p035g.TranscoderRegistry;
import com.bumptech.glide.p039p.EncoderRegistry;
import com.bumptech.glide.p039p.ImageHeaderParserRegistry;
import com.bumptech.glide.p039p.LoadPathCache;
import com.bumptech.glide.p039p.ModelToResourceClassCache;
import com.bumptech.glide.p039p.ResourceDecoderRegistry;
import com.bumptech.glide.p039p.ResourceEncoderRegistry;
import com.bumptech.glide.util.p044k.FactoryPools;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/* renamed from: com.bumptech.glide.i */
public class Registry {

    /* renamed from: a */
    private final ModelLoaderRegistry f872a = new ModelLoaderRegistry(this.f881j);

    /* renamed from: b */
    private final EncoderRegistry f873b = new EncoderRegistry();

    /* renamed from: c */
    private final ResourceDecoderRegistry f874c = new ResourceDecoderRegistry();

    /* renamed from: d */
    private final ResourceEncoderRegistry f875d = new ResourceEncoderRegistry();

    /* renamed from: e */
    private final DataRewinderRegistry f876e = new DataRewinderRegistry();

    /* renamed from: f */
    private final TranscoderRegistry f877f = new TranscoderRegistry();

    /* renamed from: g */
    private final ImageHeaderParserRegistry f878g = new ImageHeaderParserRegistry();

    /* renamed from: h */
    private final ModelToResourceClassCache f879h = new ModelToResourceClassCache();

    /* renamed from: i */
    private final LoadPathCache f880i = new LoadPathCache();

    /* renamed from: j */
    private final Pools.Pool<List<Throwable>> f881j = FactoryPools.m2858b();

    /* renamed from: com.bumptech.glide.i$a */
    /* compiled from: Registry */
    public static class C0924a extends RuntimeException {
        public C0924a(@NonNull String str) {
            super(str);
        }
    }

    /* renamed from: com.bumptech.glide.i$b */
    /* compiled from: Registry */
    public static final class C0925b extends C0924a {
        public C0925b() {
            super("Failed to find image header parser.");
        }
    }

    /* renamed from: com.bumptech.glide.i$c */
    /* compiled from: Registry */
    public static class C0926c extends C0924a {
        public C0926c(@NonNull Object obj) {
            super("Failed to find any ModelLoaders for model: " + obj);
        }

        public C0926c(@NonNull Class<?> cls, @NonNull Class<?> cls2) {
            super("Failed to find any ModelLoaders for model: " + cls + " and data: " + cls2);
        }
    }

    /* renamed from: com.bumptech.glide.i$d */
    /* compiled from: Registry */
    public static class C0927d extends C0924a {
        public C0927d(@NonNull Class<?> cls) {
            super("Failed to find result encoder for resource class: " + cls + ", you may need to consider registering a new Encoder for the requested type or DiskCacheStrategy.DATA/DiskCacheStrategy.NONE if caching your transformed resource is unnecessary.");
        }
    }

    /* renamed from: com.bumptech.glide.i$e */
    /* compiled from: Registry */
    public static class C0928e extends C0924a {
        public C0928e(@NonNull Class<?> cls) {
            super("Failed to find source encoder for data class: " + cls);
        }
    }

    public Registry() {
        mo9922a((List<String>) Arrays.asList("Gif", "Bitmap", "BitmapDrawable"));
    }

    @NonNull
    /* renamed from: c */
    private <Data, TResource, Transcode> List<DecodePath<Data, TResource, Transcode>> m1307c(@NonNull Class<Data> cls, @NonNull Class<TResource> cls2, @NonNull Class<Transcode> cls3) {
        ArrayList arrayList = new ArrayList();
        for (Class cls4 : this.f874c.mo10565b(cls, cls2)) {
            for (Class cls5 : this.f877f.mo10402b(cls4, cls3)) {
                arrayList.add(new DecodePath(cls, cls4, cls5, this.f874c.mo10562a(cls, cls4), this.f877f.mo10400a(cls4, cls5), this.f881j));
            }
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public <Data> Registry mo9916a(@NonNull Class cls, @NonNull Encoder dVar) {
        this.f873b.mo10553a(cls, dVar);
        return this;
    }

    @NonNull
    /* renamed from: b */
    public <Model, TResource, Transcode> List<Class<?>> mo9928b(@NonNull Class<Model> cls, @NonNull Class<TResource> cls2, @NonNull Class<Transcode> cls3) {
        List<Class<?>> a = this.f879h.mo10560a(cls, cls2, cls3);
        if (a == null) {
            a = new ArrayList<>();
            for (Class<?> cls4 : this.f872a.mo10306a((Class<?>) cls)) {
                for (Class cls5 : this.f874c.mo10565b(cls4, cls2)) {
                    if (!this.f877f.mo10402b(cls5, cls3).isEmpty() && !a.contains(cls5)) {
                        a.add(cls5);
                    }
                }
            }
            this.f879h.mo10561a(cls, cls2, cls3, Collections.unmodifiableList(a));
        }
        return a;
    }

    @NonNull
    /* renamed from: a */
    public <Data, TResource> Registry mo9918a(@NonNull Class cls, @NonNull Class cls2, @NonNull ResourceDecoder jVar) {
        mo9921a("legacy_append", cls, cls2, jVar);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public <Data, TResource> Registry mo9921a(@NonNull String str, @NonNull Class<Data> cls, @NonNull Class<TResource> cls2, @NonNull ResourceDecoder<Data, TResource> jVar) {
        this.f874c.mo10563a(str, jVar, cls, cls2);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public final Registry mo9922a(@NonNull List<String> list) {
        ArrayList arrayList = new ArrayList(list.size());
        arrayList.addAll(list);
        arrayList.add(0, "legacy_prepend_all");
        arrayList.add("legacy_append");
        this.f874c.mo10564a(arrayList);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public <TResource> Registry mo9917a(@NonNull Class cls, @NonNull ResourceEncoder kVar) {
        this.f875d.mo10568a(cls, kVar);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public Registry mo9915a(@NonNull DataRewinder.C0935a<?> aVar) {
        this.f876e.mo10005a(aVar);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public <TResource, Transcode> Registry mo9920a(@NonNull Class cls, @NonNull Class cls2, @NonNull ResourceTranscoder eVar) {
        this.f877f.mo10401a(cls, cls2, eVar);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public Registry mo9914a(@NonNull ImageHeaderParser imageHeaderParser) {
        this.f878g.mo10556a(imageHeaderParser);
        return this;
    }

    @NonNull
    /* renamed from: a */
    public <Model, Data> Registry mo9919a(@NonNull Class cls, @NonNull Class cls2, @NonNull ModelLoaderFactory oVar) {
        this.f872a.mo10308a(cls, cls2, oVar);
        return this;
    }

    @Nullable
    /* renamed from: a */
    public <Data, TResource, Transcode> LoadPath<Data, TResource, Transcode> mo9924a(@NonNull Class cls, @NonNull Class cls2, @NonNull Class cls3) {
        LoadPath<Data, TResource, Transcode> a = this.f880i.mo10557a(cls, cls2, cls3);
        if (this.f880i.mo10559a(a)) {
            return null;
        }
        if (a == null) {
            List c = m1307c(cls, cls2, cls3);
            if (c.isEmpty()) {
                a = null;
            } else {
                a = new LoadPath<>(cls, cls2, cls3, c, this.f881j);
            }
            this.f880i.mo10558a(cls, cls2, cls3, a);
        }
        return a;
    }

    @NonNull
    /* renamed from: c */
    public <X> Encoder<X> mo9930c(@NonNull X x) {
        Encoder<X> a = this.f873b.mo10552a(x.getClass());
        if (a != null) {
            return a;
        }
        throw new C0928e(x.getClass());
    }

    /* renamed from: b */
    public boolean mo9929b(@NonNull Resource<?> vVar) {
        return this.f875d.mo10567a(vVar.mo10228b()) != null;
    }

    @NonNull
    /* renamed from: b */
    public <X> DataRewinder<X> mo9927b(@NonNull X x) {
        return this.f876e.mo10004a((Object) x);
    }

    @NonNull
    /* renamed from: a */
    public <X> ResourceEncoder<X> mo9923a(@NonNull Resource vVar) {
        ResourceEncoder<X> a = this.f875d.mo10567a(vVar.mo10228b());
        if (a != null) {
            return a;
        }
        throw new C0927d(vVar.mo10228b());
    }

    @NonNull
    /* renamed from: a */
    public <Model> List<ModelLoader<Model, ?>> mo9926a(@NonNull Object obj) {
        List<ModelLoader<Model, ?>> a = this.f872a.mo10307a(obj);
        if (!a.isEmpty()) {
            return a;
        }
        throw new C0926c(obj);
    }

    @NonNull
    /* renamed from: a */
    public List<ImageHeaderParser> mo9925a() {
        List<ImageHeaderParser> a = this.f878g.mo10555a();
        if (!a.isEmpty()) {
            return a;
        }
        throw new C0925b();
    }
}
