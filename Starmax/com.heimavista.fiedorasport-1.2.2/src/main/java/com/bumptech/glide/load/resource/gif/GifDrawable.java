package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.vectordrawable.graphics.drawable.Animatable2Compat;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.gif.GifFrameLoader;
import com.bumptech.glide.p037n.GifDecoder;
import com.bumptech.glide.util.Preconditions;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class GifDrawable extends Drawable implements GifFrameLoader.C1092b, Animatable, Animatable2Compat {

    /* renamed from: P */
    private final C1088a f1475P;

    /* renamed from: Q */
    private boolean f1476Q;

    /* renamed from: R */
    private boolean f1477R;

    /* renamed from: S */
    private boolean f1478S;

    /* renamed from: T */
    private boolean f1479T;

    /* renamed from: U */
    private int f1480U;

    /* renamed from: V */
    private int f1481V;

    /* renamed from: W */
    private boolean f1482W;

    /* renamed from: X */
    private Paint f1483X;

    /* renamed from: Y */
    private Rect f1484Y;

    /* renamed from: Z */
    private List<Animatable2Compat.AnimationCallback> f1485Z;

    /* renamed from: com.bumptech.glide.load.resource.gif.GifDrawable$a */
    static final class C1088a extends Drawable.ConstantState {
        @VisibleForTesting

        /* renamed from: a */
        final GifFrameLoader f1486a;

        C1088a(GifFrameLoader fVar) {
            this.f1486a = fVar;
        }

        public int getChangingConfigurations() {
            return 0;
        }

        @NonNull
        public Drawable newDrawable(Resources resources) {
            return newDrawable();
        }

        @NonNull
        public Drawable newDrawable() {
            return new GifDrawable(this);
        }
    }

    public GifDrawable(Context context, GifDecoder aVar, Transformation<Bitmap> lVar, int i, int i2, Bitmap bitmap) {
        this(new C1088a(new GifFrameLoader(Glide.m1279b(context), aVar, i, i2, lVar, bitmap)));
    }

    /* renamed from: h */
    private Drawable.Callback m2291h() {
        Drawable.Callback callback = getCallback();
        while (callback instanceof Drawable) {
            callback = ((Drawable) callback).getCallback();
        }
        return callback;
    }

    /* renamed from: i */
    private Rect m2292i() {
        if (this.f1484Y == null) {
            this.f1484Y = new Rect();
        }
        return this.f1484Y;
    }

    /* renamed from: j */
    private Paint m2293j() {
        if (this.f1483X == null) {
            this.f1483X = new Paint(2);
        }
        return this.f1483X;
    }

    /* renamed from: k */
    private void m2294k() {
        List<Animatable2Compat.AnimationCallback> list = this.f1485Z;
        if (list != null) {
            int size = list.size();
            for (int i = 0; i < size; i++) {
                this.f1485Z.get(i).onAnimationEnd(super);
            }
        }
    }

    /* renamed from: l */
    private void m2295l() {
        this.f1480U = 0;
    }

    /* renamed from: m */
    private void m2296m() {
        Preconditions.m2832a(!this.f1478S, "You cannot start a recycled Drawable. Ensure thatyou clear any references to the Drawable when clearing the corresponding request.");
        if (this.f1475P.f1486a.mo10448f() == 1) {
            invalidateSelf();
        } else if (!this.f1476Q) {
            this.f1476Q = true;
            this.f1475P.f1486a.mo10442a(this);
            invalidateSelf();
        }
    }

    /* renamed from: n */
    private void m2297n() {
        this.f1476Q = false;
        this.f1475P.f1486a.mo10444b(this);
    }

    /* renamed from: a */
    public void mo10405a(Transformation<Bitmap> lVar, Bitmap bitmap) {
        this.f1475P.f1486a.mo10440a(lVar, bitmap);
    }

    /* renamed from: b */
    public ByteBuffer mo10406b() {
        return this.f1475P.f1486a.mo10443b();
    }

    /* renamed from: c */
    public Bitmap mo10407c() {
        return this.f1475P.f1486a.mo10447e();
    }

    public void clearAnimationCallbacks() {
        List<Animatable2Compat.AnimationCallback> list = this.f1485Z;
        if (list != null) {
            list.clear();
        }
    }

    /* renamed from: d */
    public int mo10408d() {
        return this.f1475P.f1486a.mo10448f();
    }

    public void draw(@NonNull Canvas canvas) {
        if (!this.f1478S) {
            if (this.f1482W) {
                Gravity.apply(119, getIntrinsicWidth(), getIntrinsicHeight(), getBounds(), m2292i());
                this.f1482W = false;
            }
            canvas.drawBitmap(this.f1475P.f1486a.mo10445c(), (Rect) null, m2292i(), m2293j());
        }
    }

    /* renamed from: e */
    public int mo10410e() {
        return this.f1475P.f1486a.mo10446d();
    }

    /* renamed from: f */
    public int mo10411f() {
        return this.f1475P.f1486a.mo10450h();
    }

    /* renamed from: g */
    public void mo10412g() {
        this.f1478S = true;
        this.f1475P.f1486a.mo10439a();
    }

    public Drawable.ConstantState getConstantState() {
        return this.f1475P;
    }

    public int getIntrinsicHeight() {
        return this.f1475P.f1486a.mo10449g();
    }

    public int getIntrinsicWidth() {
        return this.f1475P.f1486a.mo10451i();
    }

    public int getOpacity() {
        return -2;
    }

    public boolean isRunning() {
        return this.f1476Q;
    }

    /* access modifiers changed from: protected */
    public void onBoundsChange(Rect rect) {
        super.onBoundsChange(rect);
        this.f1482W = true;
    }

    public void registerAnimationCallback(@NonNull Animatable2Compat.AnimationCallback animationCallback) {
        if (animationCallback != null) {
            if (this.f1485Z == null) {
                this.f1485Z = new ArrayList();
            }
            this.f1485Z.add(animationCallback);
        }
    }

    public void setAlpha(int i) {
        m2293j().setAlpha(i);
    }

    public void setColorFilter(ColorFilter colorFilter) {
        m2293j().setColorFilter(colorFilter);
    }

    public boolean setVisible(boolean z, boolean z2) {
        Preconditions.m2832a(!this.f1478S, "Cannot change the visibility of a recycled resource. Ensure that you unset the Drawable from your View before changing the View's visibility.");
        this.f1479T = z;
        if (!z) {
            m2297n();
        } else if (this.f1477R) {
            m2296m();
        }
        return super.setVisible(z, z2);
    }

    public void start() {
        this.f1477R = true;
        m2295l();
        if (this.f1479T) {
            m2296m();
        }
    }

    public void stop() {
        this.f1477R = false;
        m2297n();
    }

    public boolean unregisterAnimationCallback(@NonNull Animatable2Compat.AnimationCallback animationCallback) {
        List<Animatable2Compat.AnimationCallback> list = this.f1485Z;
        if (list == null || animationCallback == null) {
            return false;
        }
        return list.remove(animationCallback);
    }

    /* renamed from: a */
    public void mo10404a() {
        if (m2291h() == null) {
            stop();
            invalidateSelf();
            return;
        }
        invalidateSelf();
        if (mo10410e() == mo10408d() - 1) {
            this.f1480U++;
        }
        int i = this.f1481V;
        if (i != -1 && this.f1480U >= i) {
            m2294k();
            stop();
        }
    }

    GifDrawable(C1088a aVar) {
        this.f1479T = true;
        this.f1481V = -1;
        Preconditions.m2828a(aVar);
        this.f1475P = aVar;
    }
}
