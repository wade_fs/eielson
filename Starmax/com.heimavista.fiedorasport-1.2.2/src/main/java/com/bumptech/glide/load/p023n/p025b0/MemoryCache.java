package com.bumptech.glide.load.p023n.p025b0;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.n.b0.h */
public interface MemoryCache {

    /* renamed from: com.bumptech.glide.load.n.b0.h$a */
    /* compiled from: MemoryCache */
    public interface C0964a {
        /* renamed from: a */
        void mo10106a(@NonNull Resource<?> vVar);
    }

    @Nullable
    /* renamed from: a */
    Resource<?> mo10098a(@NonNull Key gVar);

    @Nullable
    /* renamed from: a */
    Resource<?> mo10099a(@NonNull Key gVar, @Nullable Resource<?> vVar);

    /* renamed from: a */
    void mo10105a();

    /* renamed from: a */
    void mo10100a(int i);

    /* renamed from: a */
    void mo10101a(@NonNull C0964a aVar);
}
