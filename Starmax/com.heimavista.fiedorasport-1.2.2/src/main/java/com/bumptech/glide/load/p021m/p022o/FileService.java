package com.bumptech.glide.load.p021m.p022o;

import java.io.File;

/* renamed from: com.bumptech.glide.load.m.o.a */
class FileService {
    FileService() {
    }

    /* renamed from: a */
    public boolean mo10019a(File file) {
        return file.exists();
    }

    /* renamed from: b */
    public long mo10020b(File file) {
        return file.length();
    }

    /* renamed from: a */
    public File mo10018a(String str) {
        return new File(str);
    }
}
