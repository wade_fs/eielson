package com.bumptech.glide.p040q;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p023n.GlideException;
import com.bumptech.glide.p040q.p041l.SizeReadyCallback;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.p040q.p042m.Transition;
import com.bumptech.glide.util.C1122j;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: com.bumptech.glide.q.f */
public class RequestFutureTarget<R> implements FutureTarget<R>, RequestListener<R> {

    /* renamed from: Z */
    private static final C1111a f1691Z = new C1111a();

    /* renamed from: P */
    private final int f1692P;

    /* renamed from: Q */
    private final int f1693Q;

    /* renamed from: R */
    private final boolean f1694R;

    /* renamed from: S */
    private final C1111a f1695S;
    @Nullable

    /* renamed from: T */
    private R f1696T;
    @Nullable

    /* renamed from: U */
    private Request f1697U;

    /* renamed from: V */
    private boolean f1698V;

    /* renamed from: W */
    private boolean f1699W;

    /* renamed from: X */
    private boolean f1700X;
    @Nullable

    /* renamed from: Y */
    private GlideException f1701Y;

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.q.f$a */
    /* compiled from: RequestFutureTarget */
    static class C1111a {
        C1111a() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10653a(Object obj, long j) {
            obj.wait(j);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10652a(Object obj) {
            obj.notifyAll();
        }
    }

    public RequestFutureTarget(int i, int i2) {
        this(i, i2, true, f1691Z);
    }

    /* renamed from: a */
    public synchronized void mo10640a(@Nullable Request dVar) {
        this.f1697U = dVar;
    }

    /* renamed from: a */
    public void mo10641a(@NonNull SizeReadyCallback hVar) {
    }

    /* renamed from: b */
    public void mo9947b() {
    }

    /* renamed from: b */
    public void mo10644b(@Nullable Drawable drawable) {
    }

    /* renamed from: b */
    public void mo10645b(@NonNull SizeReadyCallback hVar) {
        hVar.mo10656a(this.f1692P, this.f1693Q);
    }

    /* renamed from: c */
    public void mo9949c() {
    }

    /* renamed from: c */
    public void mo10646c(@Nullable Drawable drawable) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean cancel(boolean r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            boolean r0 = r2.isDone()     // Catch:{ all -> 0x0022 }
            if (r0 == 0) goto L_0x000a
            r3 = 0
            monitor-exit(r2)
            return r3
        L_0x000a:
            r0 = 1
            r2.f1698V = r0     // Catch:{ all -> 0x0022 }
            com.bumptech.glide.q.f$a r1 = r2.f1695S     // Catch:{ all -> 0x0022 }
            r1.mo10652a(r2)     // Catch:{ all -> 0x0022 }
            if (r3 == 0) goto L_0x0020
            com.bumptech.glide.q.d r3 = r2.f1697U     // Catch:{ all -> 0x0022 }
            if (r3 == 0) goto L_0x0020
            com.bumptech.glide.q.d r3 = r2.f1697U     // Catch:{ all -> 0x0022 }
            r3.clear()     // Catch:{ all -> 0x0022 }
            r3 = 0
            r2.f1697U = r3     // Catch:{ all -> 0x0022 }
        L_0x0020:
            monitor-exit(r2)
            return r0
        L_0x0022:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.RequestFutureTarget.cancel(boolean):boolean");
    }

    public R get() {
        try {
            return m2660a((Long) null);
        } catch (TimeoutException e) {
            throw new AssertionError(e);
        }
    }

    public synchronized boolean isCancelled() {
        return this.f1698V;
    }

    public synchronized boolean isDone() {
        return this.f1698V || this.f1699W || this.f1700X;
    }

    public void onStart() {
    }

    RequestFutureTarget(int i, int i2, boolean z, C1111a aVar) {
        this.f1692P = i;
        this.f1693Q = i2;
        this.f1694R = z;
        this.f1695S = aVar;
    }

    @Nullable
    /* renamed from: a */
    public synchronized Request mo10638a() {
        return this.f1697U;
    }

    public R get(long j, @NonNull TimeUnit timeUnit) {
        return m2660a(Long.valueOf(timeUnit.toMillis(j)));
    }

    /* renamed from: a */
    public synchronized void mo10639a(@Nullable Drawable drawable) {
    }

    /* renamed from: a */
    public synchronized void mo10453a(@NonNull R r, @Nullable Transition<? super R> bVar) {
    }

    /* renamed from: a */
    private synchronized R m2660a(Long l) {
        if (this.f1694R && !isDone()) {
            C1122j.m2845a();
        }
        if (this.f1698V) {
            throw new CancellationException();
        } else if (this.f1700X) {
            throw new ExecutionException(this.f1701Y);
        } else if (this.f1699W) {
            return this.f1696T;
        } else {
            if (l == null) {
                this.f1695S.mo10653a(this, 0);
            } else if (l.longValue() > 0) {
                long currentTimeMillis = System.currentTimeMillis();
                long longValue = l.longValue() + currentTimeMillis;
                while (!isDone() && currentTimeMillis < longValue) {
                    this.f1695S.mo10653a(this, longValue - currentTimeMillis);
                    currentTimeMillis = System.currentTimeMillis();
                }
            }
            if (Thread.interrupted()) {
                throw new InterruptedException();
            } else if (this.f1700X) {
                throw new ExecutionException(this.f1701Y);
            } else if (this.f1698V) {
                throw new CancellationException();
            } else if (this.f1699W) {
                return this.f1696T;
            } else {
                throw new TimeoutException();
            }
        }
    }

    /* renamed from: a */
    public synchronized boolean mo10642a(@Nullable GlideException qVar, Object obj, Target<R> iVar, boolean z) {
        this.f1700X = true;
        this.f1701Y = qVar;
        this.f1695S.mo10652a(this);
        return false;
    }

    /* renamed from: a */
    public synchronized boolean mo10643a(R r, Object obj, Target<R> iVar, DataSource aVar, boolean z) {
        this.f1699W = true;
        this.f1696T = r;
        this.f1695S.mo10652a(this);
        return false;
    }
}
