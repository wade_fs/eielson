package com.bumptech.glide.util;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.SystemClock;

/* renamed from: com.bumptech.glide.util.e */
public final class LogTime {

    /* renamed from: a */
    private static final double f1780a;

    static {
        double d = 1.0d;
        if (Build.VERSION.SDK_INT >= 17) {
            d = 1.0d / Math.pow(10.0d, 6.0d);
        }
        f1780a = d;
    }

    @TargetApi(17)
    /* renamed from: a */
    public static long m2815a() {
        if (Build.VERSION.SDK_INT >= 17) {
            return SystemClock.elapsedRealtimeNanos();
        }
        return SystemClock.uptimeMillis();
    }

    /* renamed from: a */
    public static double m2814a(long j) {
        return ((double) (m2815a() - j)) * f1780a;
    }
}
