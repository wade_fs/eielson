package com.bumptech.glide.load.p028o;

import android.content.res.AssetManager;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p021m.FileDescriptorAssetPathFetcher;
import com.bumptech.glide.load.p021m.StreamAssetPathFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.a */
public class AssetUriLoader<Data> implements ModelLoader<Uri, Data> {

    /* renamed from: c */
    private static final int f1286c = 22;

    /* renamed from: a */
    private final AssetManager f1287a;

    /* renamed from: b */
    private final C1007a<Data> f1288b;

    /* renamed from: com.bumptech.glide.load.o.a$a */
    /* compiled from: AssetUriLoader */
    public interface C1007a<Data> {
        /* renamed from: a */
        DataFetcher<Data> mo10264a(AssetManager assetManager, String str);
    }

    /* renamed from: com.bumptech.glide.load.o.a$b */
    /* compiled from: AssetUriLoader */
    public static class C1008b implements ModelLoaderFactory<Uri, ParcelFileDescriptor>, C1007a<ParcelFileDescriptor> {

        /* renamed from: a */
        private final AssetManager f1289a;

        public C1008b(AssetManager assetManager) {
            this.f1289a = assetManager;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, ParcelFileDescriptor> mo10265a(MultiModelLoaderFactory rVar) {
            return new AssetUriLoader(this.f1289a, this);
        }

        /* renamed from: a */
        public DataFetcher<ParcelFileDescriptor> mo10264a(AssetManager assetManager, String str) {
            return new FileDescriptorAssetPathFetcher(assetManager, str);
        }
    }

    /* renamed from: com.bumptech.glide.load.o.a$c */
    /* compiled from: AssetUriLoader */
    public static class C1009c implements ModelLoaderFactory<Uri, InputStream>, C1007a<InputStream> {

        /* renamed from: a */
        private final AssetManager f1290a;

        public C1009c(AssetManager assetManager) {
            this.f1290a = assetManager;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new AssetUriLoader(this.f1290a, this);
        }

        /* renamed from: a */
        public DataFetcher<InputStream> mo10264a(AssetManager assetManager, String str) {
            return new StreamAssetPathFetcher(assetManager, str);
        }
    }

    public AssetUriLoader(AssetManager assetManager, C1007a<Data> aVar) {
        this.f1287a = assetManager;
        this.f1288b = aVar;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(uri), this.f1288b.mo10264a(this.f1287a, uri.toString().substring(f1286c)));
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        if (!"file".equals(uri.getScheme()) || uri.getPathSegments().isEmpty() || !"android_asset".equals(uri.getPathSegments().get(0))) {
            return false;
        }
        return true;
    }
}
