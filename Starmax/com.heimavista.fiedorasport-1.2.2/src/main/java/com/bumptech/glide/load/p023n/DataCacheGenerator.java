package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p023n.DataFetcherGenerator;
import com.bumptech.glide.load.p028o.ModelLoader;
import java.io.File;
import java.util.List;

/* renamed from: com.bumptech.glide.load.n.c */
class DataCacheGenerator implements DataFetcherGenerator, DataFetcher.C0934a<Object> {

    /* renamed from: P */
    private final List<Key> f1064P;

    /* renamed from: Q */
    private final DecodeHelper<?> f1065Q;

    /* renamed from: R */
    private final DataFetcherGenerator.C0977a f1066R;

    /* renamed from: S */
    private int f1067S;

    /* renamed from: T */
    private Key f1068T;

    /* renamed from: U */
    private List<ModelLoader<File, ?>> f1069U;

    /* renamed from: V */
    private int f1070V;

    /* renamed from: W */
    private volatile ModelLoader.C1036a<?> f1071W;

    /* renamed from: X */
    private File f1072X;

    DataCacheGenerator(DecodeHelper<?> gVar, DataFetcherGenerator.C0977a aVar) {
        this(gVar.mo10150c(), gVar, aVar);
    }

    /* renamed from: b */
    private boolean m1650b() {
        return this.f1070V < this.f1069U.size();
    }

    /* renamed from: a */
    public boolean mo10116a() {
        while (true) {
            boolean z = false;
            if (this.f1069U == null || !m1650b()) {
                this.f1067S++;
                if (this.f1067S >= this.f1064P.size()) {
                    return false;
                }
                Key gVar = this.f1064P.get(this.f1067S);
                this.f1072X = this.f1065Q.mo10152d().mo10088a(new DataCacheKey(gVar, this.f1065Q.mo10160l()));
                File file = this.f1072X;
                if (file != null) {
                    this.f1068T = gVar;
                    this.f1069U = this.f1065Q.mo10143a(file);
                    this.f1070V = 0;
                }
            } else {
                this.f1071W = null;
                while (!z && m1650b()) {
                    List<ModelLoader<File, ?>> list = this.f1069U;
                    int i = this.f1070V;
                    this.f1070V = i + 1;
                    this.f1071W = list.get(i).mo10261a(this.f1072X, this.f1065Q.mo10162n(), this.f1065Q.mo10154f(), this.f1065Q.mo10157i());
                    if (this.f1071W != null && this.f1065Q.mo10151c(this.f1071W.f1331c.mo9984a())) {
                        this.f1071W.f1331c.mo9988a(this.f1065Q.mo10158j(), this);
                        z = true;
                    }
                }
                return z;
            }
        }
    }

    public void cancel() {
        ModelLoader.C1036a<?> aVar = this.f1071W;
        if (aVar != null) {
            aVar.f1331c.cancel();
        }
    }

    DataCacheGenerator(List<Key> list, DecodeHelper<?> gVar, DataFetcherGenerator.C0977a aVar) {
        this.f1067S = -1;
        this.f1064P = list;
        this.f1065Q = gVar;
        this.f1066R = aVar;
    }

    /* renamed from: a */
    public void mo9999a(Object obj) {
        this.f1066R.mo10138a(this.f1068T, obj, this.f1071W.f1331c, DataSource.DATA_DISK_CACHE, this.f1068T);
    }

    /* renamed from: a */
    public void mo9998a(@NonNull Exception exc) {
        this.f1066R.mo10137a(this.f1068T, exc, this.f1071W.f1331c, DataSource.DATA_DISK_CACHE);
    }
}
