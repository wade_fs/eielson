package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import com.google.android.exoplayer2.C1750C;
import java.nio.charset.Charset;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.g */
public interface Key {

    /* renamed from: a */
    public static final Charset f928a = Charset.forName(C1750C.UTF8_NAME);

    /* renamed from: a */
    void mo9966a(@NonNull MessageDigest messageDigest);

    boolean equals(Object obj);

    int hashCode();
}
