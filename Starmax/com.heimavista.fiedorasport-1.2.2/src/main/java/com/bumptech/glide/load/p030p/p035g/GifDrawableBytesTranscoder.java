package com.bumptech.glide.load.p030p.p035g;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p030p.p032d.BytesResource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.ByteBufferUtil;

/* renamed from: com.bumptech.glide.load.p.g.d */
public class GifDrawableBytesTranscoder implements ResourceTranscoder<GifDrawable, byte[]> {
    @Nullable
    /* renamed from: a */
    public Resource<byte[]> mo10399a(@NonNull Resource<GifDrawable> vVar, @NonNull Options iVar) {
        return new BytesResource(ByteBufferUtil.m2804b(vVar.get().mo10406b()));
    }
}
