package com.bumptech.glide.load.p030p.p035g;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.p031c.BitmapResource;
import com.bumptech.glide.load.resource.gif.GifDrawable;

/* renamed from: com.bumptech.glide.load.p.g.c */
public final class DrawableBytesTranscoder implements ResourceTranscoder<Drawable, byte[]> {

    /* renamed from: a */
    private final BitmapPool f1467a;

    /* renamed from: b */
    private final ResourceTranscoder<Bitmap, byte[]> f1468b;

    /* renamed from: c */
    private final ResourceTranscoder<GifDrawable, byte[]> f1469c;

    public DrawableBytesTranscoder(@NonNull BitmapPool eVar, @NonNull ResourceTranscoder<Bitmap, byte[]> eVar2, @NonNull ResourceTranscoder<GifDrawable, byte[]> eVar3) {
        this.f1467a = eVar;
        this.f1468b = eVar2;
        this.f1469c = eVar3;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [com.bumptech.glide.load.n.v<android.graphics.drawable.Drawable>, com.bumptech.glide.load.n.v<com.bumptech.glide.load.resource.gif.GifDrawable>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @androidx.annotation.NonNull
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.bumptech.glide.load.p023n.Resource<com.bumptech.glide.load.resource.gif.GifDrawable> m2281a(@androidx.annotation.NonNull com.bumptech.glide.load.p023n.Resource<android.graphics.drawable.Drawable> r0) {
        /*
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p030p.p035g.DrawableBytesTranscoder.m2281a(com.bumptech.glide.load.n.v):com.bumptech.glide.load.n.v");
    }

    @Nullable
    /* renamed from: a */
    public Resource<byte[]> mo10399a(@NonNull Resource<Drawable> vVar, @NonNull Options iVar) {
        Drawable drawable = vVar.get();
        if (drawable instanceof BitmapDrawable) {
            return this.f1468b.mo10399a(BitmapResource.m2091a(((BitmapDrawable) drawable).getBitmap(), this.f1467a), iVar);
        }
        if (!(drawable instanceof GifDrawable)) {
            return null;
        }
        ResourceTranscoder<GifDrawable, byte[]> eVar = this.f1469c;
        m2281a(vVar);
        return eVar.mo10399a(vVar, iVar);
    }
}
