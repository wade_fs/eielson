package com.bumptech.glide.load.p030p.p031c;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.p031c.DownsampleStrategy;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Preconditions;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;

/* renamed from: com.bumptech.glide.load.p.c.l */
public final class Downsampler {

    /* renamed from: f */
    public static final Option<DecodeFormat> f1411f = Option.m1370a("com.bumptech.glide.load.resource.bitmap.Downsampler.DecodeFormat", DecodeFormat.f922R);

    /* renamed from: g */
    public static final Option<Boolean> f1412g = Option.m1370a("com.bumptech.glide.load.resource.bitmap.Downsampler.FixBitmapSize", (Object) false);

    /* renamed from: h */
    public static final Option<Boolean> f1413h = Option.m1370a("com.bumptech.glide.load.resource.bitmap.Downsampler.AllowHardwareDecode", (Object) false);

    /* renamed from: i */
    private static final Set<String> f1414i = Collections.unmodifiableSet(new HashSet(Arrays.asList("image/vnd.wap.wbmp", "image/x-ico")));

    /* renamed from: j */
    private static final C1074b f1415j = new C1073a();

    /* renamed from: k */
    private static final Set<ImageHeaderParser.ImageType> f1416k = Collections.unmodifiableSet(EnumSet.of(ImageHeaderParser.ImageType.JPEG, ImageHeaderParser.ImageType.PNG_A, ImageHeaderParser.ImageType.PNG));

    /* renamed from: l */
    private static final Queue<BitmapFactory.Options> f1417l = C1122j.m2844a(0);

    /* renamed from: a */
    private final BitmapPool f1418a;

    /* renamed from: b */
    private final DisplayMetrics f1419b;

    /* renamed from: c */
    private final ArrayPool f1420c;

    /* renamed from: d */
    private final List<ImageHeaderParser> f1421d;

    /* renamed from: e */
    private final HardwareConfigState f1422e = HardwareConfigState.m2187a();

    /* renamed from: com.bumptech.glide.load.p.c.l$a */
    /* compiled from: Downsampler */
    class C1073a implements C1074b {
        C1073a() {
        }

        /* renamed from: a */
        public void mo10360a() {
        }

        /* renamed from: a */
        public void mo10361a(BitmapPool eVar, Bitmap bitmap) {
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.l$b */
    /* compiled from: Downsampler */
    public interface C1074b {
        /* renamed from: a */
        void mo10360a();

        /* renamed from: a */
        void mo10361a(BitmapPool eVar, Bitmap bitmap);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.load.h.a(java.lang.String, java.lang.Object):com.bumptech.glide.load.h<T>
     arg types: [java.lang.String, boolean]
     candidates:
      com.bumptech.glide.load.h.a(java.lang.Object, java.security.MessageDigest):void
      com.bumptech.glide.load.h.a(java.lang.String, java.lang.Object):com.bumptech.glide.load.h<T> */
    static {
        Option<DownsampleStrategy> hVar = DownsampleStrategy.f1407f;
    }

    public Downsampler(List<ImageHeaderParser> list, DisplayMetrics displayMetrics, BitmapPool eVar, ArrayPool bVar) {
        this.f1421d = list;
        Preconditions.m2828a(displayMetrics);
        this.f1419b = displayMetrics;
        Preconditions.m2828a(eVar);
        this.f1418a = eVar;
        Preconditions.m2828a(bVar);
        this.f1420c = bVar;
    }

    /* renamed from: b */
    private static int m2161b(double d) {
        if (d > 1.0d) {
            d = 1.0d / d;
        }
        return (int) Math.round(d * 2.147483647E9d);
    }

    /* renamed from: c */
    private static int m2164c(double d) {
        return (int) (d + 0.5d);
    }

    /* renamed from: c */
    private static void m2165c(BitmapFactory.Options options) {
        m2166d(options);
        synchronized (f1417l) {
            f1417l.offer(options);
        }
    }

    /* renamed from: d */
    private static void m2166d(BitmapFactory.Options options) {
        options.inTempStorage = null;
        options.inDither = false;
        options.inScaled = false;
        options.inSampleSize = 1;
        options.inPreferredConfig = null;
        options.inJustDecodeBounds = false;
        options.inDensity = 0;
        options.inTargetDensity = 0;
        options.outWidth = 0;
        options.outHeight = 0;
        options.outMimeType = null;
        options.inBitmap = null;
        options.inMutable = true;
    }

    /* renamed from: a */
    public Resource<Bitmap> mo10356a(InputStream inputStream, int i, int i2, Options iVar) {
        return mo10357a(inputStream, i, i2, iVar, f1415j);
    }

    /* renamed from: a */
    public boolean mo10358a(InputStream inputStream) {
        return true;
    }

    /* renamed from: a */
    public boolean mo10359a(ByteBuffer byteBuffer) {
        return true;
    }

    /* renamed from: b */
    private static int[] m2163b(InputStream inputStream, BitmapFactory.Options options, C1074b bVar, BitmapPool eVar) {
        options.inJustDecodeBounds = true;
        m2151a(inputStream, options, bVar, eVar);
        options.inJustDecodeBounds = false;
        return new int[]{options.outWidth, options.outHeight};
    }

    /* renamed from: a */
    public Resource<Bitmap> mo10357a(InputStream inputStream, int i, int i2, Options iVar, C1074b bVar) {
        Options iVar2 = iVar;
        Preconditions.m2832a(inputStream.markSupported(), "You must provide an InputStream that supports mark()");
        byte[] bArr = (byte[]) this.f1420c.mo10042b(65536, byte[].class);
        BitmapFactory.Options a = m2152a();
        a.inTempStorage = bArr;
        DecodeFormat bVar2 = (DecodeFormat) iVar2.mo9976a(f1411f);
        try {
            return BitmapResource.m2091a(m2150a(inputStream, a, (DownsampleStrategy) iVar2.mo9976a(DownsampleStrategy.f1407f), bVar2, iVar2.mo9976a(f1413h) != null && ((Boolean) iVar2.mo9976a(f1413h)).booleanValue(), i, i2, ((Boolean) iVar2.mo9976a(f1412g)).booleanValue(), bVar), this.f1418a);
        } finally {
            m2165c(a);
            this.f1420c.put(bArr);
        }
    }

    /* renamed from: b */
    private static boolean m2162b(BitmapFactory.Options options) {
        int i;
        int i2 = options.inTargetDensity;
        return i2 > 0 && (i = options.inDensity) > 0 && i2 != i;
    }

    /* renamed from: a */
    private Bitmap m2150a(InputStream inputStream, BitmapFactory.Options options, DownsampleStrategy kVar, DecodeFormat bVar, boolean z, int i, int i2, boolean z2, C1074b bVar2) {
        Downsampler lVar;
        int i3;
        int i4;
        int i5;
        InputStream inputStream2 = inputStream;
        BitmapFactory.Options options2 = options;
        C1074b bVar3 = bVar2;
        long a = LogTime.m2815a();
        int[] b = m2163b(inputStream2, options2, bVar3, this.f1418a);
        boolean z3 = false;
        int i6 = b[0];
        int i7 = b[1];
        String str = options2.outMimeType;
        boolean z4 = (i6 == -1 || i7 == -1) ? false : z;
        int a2 = ImageHeaderParserUtils.m1365a(this.f1421d, inputStream2, this.f1420c);
        int a3 = TransformationUtils.m2208a(a2);
        boolean b2 = TransformationUtils.m2220b(a2);
        int i8 = i;
        int i9 = i2;
        int i10 = i8 == Integer.MIN_VALUE ? i6 : i8;
        int i11 = i9 == Integer.MIN_VALUE ? i7 : i9;
        ImageHeaderParser.ImageType b3 = ImageHeaderParserUtils.m1367b(this.f1421d, inputStream2, this.f1420c);
        BitmapPool eVar = this.f1418a;
        ImageHeaderParser.ImageType imageType = b3;
        m2158a(b3, inputStream, bVar2, eVar, kVar, a3, i6, i7, i10, i11, options);
        int i12 = a2;
        String str2 = str;
        int i13 = i7;
        int i14 = i6;
        C1074b bVar4 = bVar3;
        BitmapFactory.Options options3 = options2;
        m2159a(inputStream, bVar, z4, b2, options, i10, i11);
        if (Build.VERSION.SDK_INT >= 19) {
            z3 = true;
        }
        if (options3.inSampleSize == 1 || z3) {
            lVar = this;
            if (lVar.m2160a(imageType)) {
                if (i14 < 0 || i13 < 0 || !z2 || !z3) {
                    float f = m2162b(options) ? ((float) options3.inTargetDensity) / ((float) options3.inDensity) : 1.0f;
                    int i15 = options3.inSampleSize;
                    float f2 = (float) i15;
                    i5 = Math.round(((float) ((int) Math.ceil((double) (((float) i14) / f2)))) * f);
                    i4 = Math.round(((float) ((int) Math.ceil((double) (((float) i13) / f2)))) * f);
                    if (Log.isLoggable("Downsampler", 2)) {
                        Log.v("Downsampler", "Calculated target [" + i5 + "x" + i4 + "] for source [" + i14 + "x" + i13 + "], sampleSize: " + i15 + ", targetDensity: " + options3.inTargetDensity + ", density: " + options3.inDensity + ", density multiplier: " + f);
                    }
                } else {
                    i5 = i10;
                    i4 = i11;
                }
                if (i5 > 0 && i4 > 0) {
                    m2157a(options3, lVar.f1418a, i5, i4);
                }
            }
        } else {
            lVar = this;
        }
        Bitmap a4 = m2151a(inputStream, options3, bVar4, lVar.f1418a);
        bVar4.mo10361a(lVar.f1418a, a4);
        if (Log.isLoggable("Downsampler", 2)) {
            i3 = i12;
            m2156a(i14, i13, str2, options, a4, i, i2, a);
        } else {
            i3 = i12;
        }
        Bitmap bitmap = null;
        if (a4 != null) {
            a4.setDensity(lVar.f1419b.densityDpi);
            bitmap = TransformationUtils.m2211a(lVar.f1418a, a4, i3);
            if (!a4.equals(bitmap)) {
                lVar.f1418a.mo10063a(a4);
            }
        }
        return bitmap;
    }

    /* renamed from: a */
    private static void m2158a(ImageHeaderParser.ImageType imageType, InputStream inputStream, C1074b bVar, BitmapPool eVar, DownsampleStrategy kVar, int i, int i2, int i3, int i4, int i5, BitmapFactory.Options options) {
        float f;
        int i6;
        int i7;
        int i8;
        int i9;
        double d;
        ImageHeaderParser.ImageType imageType2 = imageType;
        DownsampleStrategy kVar2 = kVar;
        int i10 = i;
        int i11 = i2;
        int i12 = i3;
        int i13 = i4;
        int i14 = i5;
        BitmapFactory.Options options2 = options;
        if (i11 <= 0 || i12 <= 0) {
            String str = "Downsampler";
            String str2 = "x";
            if (Log.isLoggable(str, 3)) {
                Log.d(str, "Unable to determine dimensions for: " + imageType2 + " with target [" + i13 + str2 + i14 + "]");
                return;
            }
            return;
        }
        if (i10 == 90 || i10 == 270) {
            f = kVar2.mo10355b(i12, i11, i13, i14);
        } else {
            f = kVar2.mo10355b(i11, i12, i13, i14);
        }
        if (f > 0.0f) {
            DownsampleStrategy.C1072g a = kVar2.mo10354a(i11, i12, i13, i14);
            if (a != null) {
                float f2 = (float) i11;
                float f3 = (float) i12;
                String str3 = "Downsampler";
                String str4 = "x";
                int c = i11 / m2164c((double) (f * f2));
                int c2 = i12 / m2164c((double) (f * f3));
                if (a == DownsampleStrategy.C1072g.MEMORY) {
                    i6 = Math.max(c, c2);
                } else {
                    i6 = Math.min(c, c2);
                }
                if (Build.VERSION.SDK_INT > 23 || !f1414i.contains(options2.outMimeType)) {
                    int max = Math.max(1, Integer.highestOneBit(i6));
                    i7 = (a != DownsampleStrategy.C1072g.MEMORY || ((float) max) >= 1.0f / f) ? max : max << 1;
                } else {
                    i7 = 1;
                }
                options2.inSampleSize = i7;
                if (imageType2 == ImageHeaderParser.ImageType.JPEG) {
                    float min = (float) Math.min(i7, 8);
                    i8 = (int) Math.ceil((double) (f2 / min));
                    i9 = (int) Math.ceil((double) (f3 / min));
                    int i15 = i7 / 8;
                    if (i15 > 0) {
                        i8 /= i15;
                        i9 /= i15;
                    }
                } else {
                    if (imageType2 == ImageHeaderParser.ImageType.PNG || imageType2 == ImageHeaderParser.ImageType.PNG_A) {
                        float f4 = (float) i7;
                        i8 = (int) Math.floor((double) (f2 / f4));
                        d = Math.floor((double) (f3 / f4));
                    } else if (imageType2 == ImageHeaderParser.ImageType.WEBP || imageType2 == ImageHeaderParser.ImageType.WEBP_A) {
                        if (Build.VERSION.SDK_INT >= 24) {
                            float f5 = (float) i7;
                            i8 = Math.round(f2 / f5);
                            i9 = Math.round(f3 / f5);
                        } else {
                            float f6 = (float) i7;
                            i8 = (int) Math.floor((double) (f2 / f6));
                            d = Math.floor((double) (f3 / f6));
                        }
                    } else if (i11 % i7 == 0 && i12 % i7 == 0) {
                        i8 = i11 / i7;
                        i9 = i12 / i7;
                    } else {
                        int[] b = m2163b(inputStream, options2, bVar, eVar);
                        int i16 = b[0];
                        i9 = b[1];
                        i8 = i16;
                    }
                    i9 = (int) d;
                }
                double b2 = (double) kVar2.mo10355b(i8, i9, i13, i14);
                if (Build.VERSION.SDK_INT >= 19) {
                    options2.inTargetDensity = m2149a(b2);
                    options2.inDensity = m2161b(b2);
                }
                if (m2162b(options)) {
                    options2.inScaled = true;
                } else {
                    options2.inTargetDensity = 0;
                    options2.inDensity = 0;
                }
                String str5 = str3;
                if (Log.isLoggable(str5, 2)) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("Calculate scaling, source: [");
                    sb.append(i11);
                    String str6 = str4;
                    sb.append(str6);
                    sb.append(i12);
                    sb.append("], target: [");
                    sb.append(i13);
                    sb.append(str6);
                    sb.append(i14);
                    sb.append("], power of two scaled: [");
                    sb.append(i8);
                    sb.append(str6);
                    sb.append(i9);
                    sb.append("], exact scale factor: ");
                    sb.append(f);
                    sb.append(", power of 2 sample size: ");
                    sb.append(i7);
                    sb.append(", adjusted scale factor: ");
                    sb.append(b2);
                    sb.append(", target density: ");
                    sb.append(options2.inTargetDensity);
                    sb.append(", density: ");
                    sb.append(options2.inDensity);
                    Log.v(str5, sb.toString());
                    return;
                }
                return;
            }
            throw new IllegalArgumentException("Cannot round with null rounding");
        }
        String str7 = "x";
        throw new IllegalArgumentException("Cannot scale with factor: " + f + " from: " + kVar2 + ", source: [" + i11 + str7 + i12 + "], target: [" + i13 + str7 + i14 + "]");
    }

    /* renamed from: a */
    private static int m2149a(double d) {
        int b = m2161b(d);
        int c = m2164c(((double) b) * d);
        return m2164c((d / ((double) (((float) c) / ((float) b)))) * ((double) c));
    }

    /* renamed from: a */
    private boolean m2160a(ImageHeaderParser.ImageType imageType) {
        if (Build.VERSION.SDK_INT >= 19) {
            return true;
        }
        return f1416k.contains(imageType);
    }

    /* renamed from: a */
    private void m2159a(InputStream inputStream, DecodeFormat bVar, boolean z, boolean z2, BitmapFactory.Options options, int i, int i2) {
        if (!this.f1422e.mo10363a(i, i2, options, bVar, z, z2)) {
            if (bVar == DecodeFormat.f920P || Build.VERSION.SDK_INT == 16) {
                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                return;
            }
            boolean z3 = false;
            try {
                z3 = ImageHeaderParserUtils.m1367b(this.f1421d, inputStream, this.f1420c).hasAlpha();
            } catch (IOException e) {
                if (Log.isLoggable("Downsampler", 3)) {
                    Log.d("Downsampler", "Cannot determine whether the image has alpha or not from header, format " + bVar, e);
                }
            }
            options.inPreferredConfig = z3 ? Bitmap.Config.ARGB_8888 : Bitmap.Config.RGB_565;
            if (options.inPreferredConfig == Bitmap.Config.RGB_565) {
                options.inDither = true;
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:23|24) */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        throw r1;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005d */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.Bitmap m2151a(java.io.InputStream r6, android.graphics.BitmapFactory.Options r7, com.bumptech.glide.load.p030p.p031c.Downsampler.C1074b r8, com.bumptech.glide.load.p023n.p024a0.BitmapPool r9) {
        /*
            java.lang.String r0 = "Downsampler"
            boolean r1 = r7.inJustDecodeBounds
            if (r1 == 0) goto L_0x000c
            r1 = 10485760(0xa00000, float:1.469368E-38)
            r6.mark(r1)
            goto L_0x000f
        L_0x000c:
            r8.mo10360a()
        L_0x000f:
            int r1 = r7.outWidth
            int r2 = r7.outHeight
            java.lang.String r3 = r7.outMimeType
            java.util.concurrent.locks.Lock r4 = com.bumptech.glide.load.p030p.p031c.TransformationUtils.m2213a()
            r4.lock()
            r4 = 0
            android.graphics.Bitmap r8 = android.graphics.BitmapFactory.decodeStream(r6, r4, r7)     // Catch:{ IllegalArgumentException -> 0x0032 }
            java.util.concurrent.locks.Lock r9 = com.bumptech.glide.load.p030p.p031c.TransformationUtils.m2213a()
            r9.unlock()
            boolean r7 = r7.inJustDecodeBounds
            if (r7 == 0) goto L_0x002f
            r6.reset()
        L_0x002f:
            return r8
        L_0x0030:
            r6 = move-exception
            goto L_0x005f
        L_0x0032:
            r5 = move-exception
            java.io.IOException r1 = m2153a(r5, r1, r2, r3, r7)     // Catch:{ all -> 0x0030 }
            r2 = 3
            boolean r2 = android.util.Log.isLoggable(r0, r2)     // Catch:{ all -> 0x0030 }
            if (r2 == 0) goto L_0x0043
            java.lang.String r2 = "Failed to decode with inBitmap, trying again without Bitmap re-use"
            android.util.Log.d(r0, r2, r1)     // Catch:{ all -> 0x0030 }
        L_0x0043:
            android.graphics.Bitmap r0 = r7.inBitmap     // Catch:{ all -> 0x0030 }
            if (r0 == 0) goto L_0x005e
            r6.reset()     // Catch:{ IOException -> 0x005d }
            android.graphics.Bitmap r0 = r7.inBitmap     // Catch:{ IOException -> 0x005d }
            r9.mo10063a(r0)     // Catch:{ IOException -> 0x005d }
            r7.inBitmap = r4     // Catch:{ IOException -> 0x005d }
            android.graphics.Bitmap r6 = m2151a(r6, r7, r8, r9)     // Catch:{ IOException -> 0x005d }
            java.util.concurrent.locks.Lock r7 = com.bumptech.glide.load.p030p.p031c.TransformationUtils.m2213a()
            r7.unlock()
            return r6
        L_0x005d:
            throw r1     // Catch:{ all -> 0x0030 }
        L_0x005e:
            throw r1     // Catch:{ all -> 0x0030 }
        L_0x005f:
            java.util.concurrent.locks.Lock r7 = com.bumptech.glide.load.p030p.p031c.TransformationUtils.m2213a()
            r7.unlock()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p030p.p031c.Downsampler.m2151a(java.io.InputStream, android.graphics.BitmapFactory$Options, com.bumptech.glide.load.p.c.l$b, com.bumptech.glide.load.n.a0.e):android.graphics.Bitmap");
    }

    /* renamed from: a */
    private static void m2156a(int i, int i2, String str, BitmapFactory.Options options, Bitmap bitmap, int i3, int i4, long j) {
        Log.v("Downsampler", "Decoded " + m2154a(bitmap) + " from [" + i + "x" + i2 + "] " + str + " with inBitmap " + m2155a(options) + " for [" + i3 + "x" + i4 + "], sample size: " + options.inSampleSize + ", density: " + options.inDensity + ", target density: " + options.inTargetDensity + ", thread: " + Thread.currentThread().getName() + ", duration: " + LogTime.m2814a(j));
    }

    /* renamed from: a */
    private static String m2155a(BitmapFactory.Options options) {
        return m2154a(options.inBitmap);
    }

    @TargetApi(19)
    @Nullable
    /* renamed from: a */
    private static String m2154a(Bitmap bitmap) {
        String str;
        if (bitmap == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT >= 19) {
            str = " (" + bitmap.getAllocationByteCount() + ")";
        } else {
            str = "";
        }
        return "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig() + str;
    }

    /* renamed from: a */
    private static IOException m2153a(IllegalArgumentException illegalArgumentException, int i, int i2, String str, BitmapFactory.Options options) {
        return new IOException("Exception decoding bitmap, outWidth: " + i + ", outHeight: " + i2 + ", outMimeType: " + str + ", inBitmap: " + m2155a(options), illegalArgumentException);
    }

    @TargetApi(26)
    /* renamed from: a */
    private static void m2157a(BitmapFactory.Options options, BitmapPool eVar, int i, int i2) {
        Bitmap.Config config;
        if (Build.VERSION.SDK_INT < 26) {
            config = null;
        } else if (options.inPreferredConfig != Bitmap.Config.HARDWARE) {
            config = options.outConfig;
        } else {
            return;
        }
        if (config == null) {
            config = options.inPreferredConfig;
        }
        options.inBitmap = eVar.mo10064b(i, i2, config);
    }

    /* renamed from: a */
    private static synchronized BitmapFactory.Options m2152a() {
        BitmapFactory.Options poll;
        synchronized (Downsampler.class) {
            synchronized (f1417l) {
                poll = f1417l.poll();
            }
            if (poll == null) {
                poll = new BitmapFactory.Options();
                m2166d(poll);
            }
        }
        return poll;
    }
}
