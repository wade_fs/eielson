package com.bumptech.glide.p039p;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.ResourceDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.p.e */
public class ResourceDecoderRegistry {

    /* renamed from: a */
    private final List<String> f1654a = new ArrayList();

    /* renamed from: b */
    private final Map<String, List<C1109a<?, ?>>> f1655b = new HashMap();

    /* renamed from: com.bumptech.glide.p.e$a */
    /* compiled from: ResourceDecoderRegistry */
    private static class C1109a<T, R> {

        /* renamed from: a */
        private final Class<T> f1656a;

        /* renamed from: b */
        final Class<R> f1657b;

        /* renamed from: c */
        final ResourceDecoder<T, R> f1658c;

        public C1109a(@NonNull Class<T> cls, @NonNull Class<R> cls2, ResourceDecoder<T, R> jVar) {
            this.f1656a = cls;
            this.f1657b = cls2;
            this.f1658c = jVar;
        }

        /* renamed from: a */
        public boolean mo10566a(@NonNull Class<?> cls, @NonNull Class<?> cls2) {
            return this.f1656a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.f1657b);
        }
    }

    /* renamed from: a */
    public synchronized void mo10564a(@NonNull List<String> list) {
        ArrayList<String> arrayList = new ArrayList<>(this.f1654a);
        this.f1654a.clear();
        this.f1654a.addAll(list);
        for (String str : arrayList) {
            if (!list.contains(str)) {
                this.f1654a.add(str);
            }
        }
    }

    @NonNull
    /* renamed from: b */
    public synchronized <T, R> List<Class<R>> mo10565b(@NonNull Class<T> cls, @NonNull Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.f1654a) {
            List<C1109a> list = this.f1655b.get(str);
            if (list != null) {
                for (C1109a aVar : list) {
                    if (aVar.mo10566a(cls, cls2) && !arrayList.contains(aVar.f1657b)) {
                        arrayList.add(aVar.f1657b);
                    }
                }
            }
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public synchronized <T, R> List<ResourceDecoder<T, R>> mo10562a(@NonNull Class<T> cls, @NonNull Class<R> cls2) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (String str : this.f1654a) {
            List<C1109a> list = this.f1655b.get(str);
            if (list != null) {
                for (C1109a aVar : list) {
                    if (aVar.mo10566a(cls, cls2)) {
                        arrayList.add(aVar.f1658c);
                    }
                }
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    public synchronized <T, R> void mo10563a(@NonNull String str, @NonNull ResourceDecoder<T, R> jVar, @NonNull Class<T> cls, @NonNull Class<R> cls2) {
        m2564a(str).add(new C1109a(cls, cls2, jVar));
    }

    @NonNull
    /* renamed from: a */
    private synchronized List<C1109a<?, ?>> m2564a(@NonNull String str) {
        List<C1109a<?, ?>> list;
        if (!this.f1654a.contains(str)) {
            this.f1654a.add(str);
        }
        list = this.f1655b.get(str);
        if (list == null) {
            list = new ArrayList<>();
            this.f1655b.put(str, list);
        }
        return list;
    }
}
