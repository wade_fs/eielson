package com.bumptech.glide.p043r;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.r.a */
public final class EmptySignature implements Key {

    /* renamed from: b */
    private static final EmptySignature f1764b = new EmptySignature();

    private EmptySignature() {
    }

    @NonNull
    /* renamed from: a */
    public static EmptySignature m2798a() {
        return f1764b;
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
    }

    public String toString() {
        return "EmptySignature";
    }
}
