package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.n.p */
class EngineResource<Z> implements Resource<Z> {

    /* renamed from: P */
    private final boolean f1233P;

    /* renamed from: Q */
    private final boolean f1234Q;

    /* renamed from: R */
    private final Resource<Z> f1235R;

    /* renamed from: S */
    private C1003a f1236S;

    /* renamed from: T */
    private Key f1237T;

    /* renamed from: U */
    private int f1238U;

    /* renamed from: V */
    private boolean f1239V;

    /* renamed from: com.bumptech.glide.load.n.p$a */
    /* compiled from: EngineResource */
    interface C1003a {
        /* renamed from: a */
        void mo10192a(Key gVar, EngineResource<?> pVar);
    }

    EngineResource(Resource<Z> vVar, boolean z, boolean z2) {
        Preconditions.m2828a(vVar);
        this.f1235R = vVar;
        this.f1233P = z;
        this.f1234Q = z2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10227a(Key gVar, C1003a aVar) {
        this.f1237T = gVar;
        this.f1236S = aVar;
    }

    @NonNull
    /* renamed from: b */
    public Class<Z> mo10228b() {
        return this.f1235R.mo10228b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public synchronized void mo10229c() {
        if (!this.f1239V) {
            this.f1238U++;
        } else {
            throw new IllegalStateException("Cannot acquire a recycled resource");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public Resource<Z> mo10230d() {
        return this.f1235R;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public boolean mo10231e() {
        return this.f1233P;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public void mo10232f() {
        synchronized (this.f1236S) {
            synchronized (this) {
                if (this.f1238U > 0) {
                    int i = this.f1238U - 1;
                    this.f1238U = i;
                    if (i == 0) {
                        this.f1236S.mo10192a(this.f1237T, this);
                    }
                } else {
                    throw new IllegalStateException("Cannot release a recycled or not yet acquired resource");
                }
            }
        }
    }

    @NonNull
    public Z get() {
        return this.f1235R.get();
    }

    public synchronized void recycle() {
        if (this.f1238U > 0) {
            throw new IllegalStateException("Cannot recycle a resource while it is still acquired");
        } else if (!this.f1239V) {
            this.f1239V = true;
            if (this.f1234Q) {
                this.f1235R.recycle();
            }
        } else {
            throw new IllegalStateException("Cannot recycle a resource that has already been recycled");
        }
    }

    public synchronized String toString() {
        return "EngineResource{isCacheable=" + this.f1233P + ", listener=" + this.f1236S + ", key=" + this.f1237T + ", acquired=" + this.f1238U + ", isRecycled=" + this.f1239V + ", resource=" + this.f1235R + '}';
    }

    /* renamed from: a */
    public int mo10226a() {
        return this.f1235R.mo10226a();
    }
}
