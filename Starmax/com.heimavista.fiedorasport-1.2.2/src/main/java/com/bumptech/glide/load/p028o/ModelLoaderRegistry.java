package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pools;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.o.p */
public class ModelLoaderRegistry {

    /* renamed from: a */
    private final MultiModelLoaderFactory f1332a;

    /* renamed from: b */
    private final C1037a f1333b;

    /* renamed from: com.bumptech.glide.load.o.p$a */
    /* compiled from: ModelLoaderRegistry */
    private static class C1037a {

        /* renamed from: a */
        private final Map<Class<?>, C1038a<?>> f1334a = new HashMap();

        /* renamed from: com.bumptech.glide.load.o.p$a$a */
        /* compiled from: ModelLoaderRegistry */
        private static class C1038a<Model> {

            /* renamed from: a */
            final List<ModelLoader<Model, ?>> f1335a;

            public C1038a(List<ModelLoader<Model, ?>> list) {
                this.f1335a = list;
            }
        }

        C1037a() {
        }

        /* renamed from: a */
        public void mo10310a() {
            this.f1334a.clear();
        }

        /* renamed from: a */
        public <Model> void mo10311a(Class<Model> cls, List<ModelLoader<Model, ?>> list) {
            if (this.f1334a.put(cls, new C1038a(list)) != null) {
                throw new IllegalStateException("Already cached loaders for model: " + cls);
            }
        }

        @Nullable
        /* renamed from: a */
        public <Model> List<ModelLoader<Model, ?>> mo10309a(Class<Model> cls) {
            C1038a aVar = this.f1334a.get(cls);
            if (aVar == null) {
                return null;
            }
            return aVar.f1335a;
        }
    }

    public ModelLoaderRegistry(@NonNull Pools.Pool<List<Throwable>> pool) {
        this(new MultiModelLoaderFactory(pool));
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class<A>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @androidx.annotation.NonNull
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized <A> java.util.List<com.bumptech.glide.load.p028o.ModelLoader<A, ?>> m1978b(@androidx.annotation.NonNull java.lang.Class<A> r3) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.bumptech.glide.load.o.p$a r0 = r2.f1333b     // Catch:{ all -> 0x001a }
            java.util.List r0 = r0.mo10309a(r3)     // Catch:{ all -> 0x001a }
            if (r0 != 0) goto L_0x0018
            com.bumptech.glide.load.o.r r0 = r2.f1332a     // Catch:{ all -> 0x001a }
            java.util.List r0 = r0.mo10314a(r3)     // Catch:{ all -> 0x001a }
            java.util.List r0 = java.util.Collections.unmodifiableList(r0)     // Catch:{ all -> 0x001a }
            com.bumptech.glide.load.o.p$a r1 = r2.f1333b     // Catch:{ all -> 0x001a }
            r1.mo10311a(r3, r0)     // Catch:{ all -> 0x001a }
        L_0x0018:
            monitor-exit(r2)
            return r0
        L_0x001a:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p028o.ModelLoaderRegistry.m1978b(java.lang.Class):java.util.List");
    }

    /* renamed from: a */
    public synchronized <Model, Data> void mo10308a(@NonNull Class<Model> cls, @NonNull Class<Data> cls2, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> oVar) {
        this.f1332a.mo10315a(cls, cls2, oVar);
        this.f1333b.mo10310a();
    }

    private ModelLoaderRegistry(@NonNull MultiModelLoaderFactory rVar) {
        this.f1333b = new C1037a();
        this.f1332a = rVar;
    }

    @NonNull
    /* renamed from: a */
    public <A> List<ModelLoader<A, ?>> mo10307a(@NonNull Object obj) {
        List b = m1978b(m1977b(obj));
        int size = b.size();
        List<ModelLoader<A, ?>> emptyList = Collections.emptyList();
        boolean z = true;
        for (int i = 0; i < size; i++) {
            ModelLoader nVar = (ModelLoader) b.get(i);
            if (nVar.mo10263a(obj)) {
                if (z) {
                    emptyList = new ArrayList<>(size - i);
                    z = false;
                }
                emptyList.add(nVar);
            }
        }
        return emptyList;
    }

    @NonNull
    /* renamed from: b */
    private static <A> Class<A> m1977b(@NonNull A a) {
        return a.getClass();
    }

    @NonNull
    /* renamed from: a */
    public synchronized List<Class<?>> mo10306a(@NonNull Class<?> cls) {
        return this.f1332a.mo10316b(cls);
    }
}
