package com.bumptech.glide.load.p021m;

import android.content.res.AssetManager;
import androidx.annotation.NonNull;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.m.m */
public class StreamAssetPathFetcher extends AssetPathFetcher<InputStream> {
    public StreamAssetPathFetcher(AssetManager assetManager, String str) {
        super(assetManager, str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public InputStream m1449a(AssetManager assetManager, String str) {
        return assetManager.open(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo9989a(InputStream inputStream) {
        inputStream.close();
    }

    @NonNull
    /* renamed from: a */
    public Class<InputStream> mo9984a() {
        return InputStream.class;
    }
}
