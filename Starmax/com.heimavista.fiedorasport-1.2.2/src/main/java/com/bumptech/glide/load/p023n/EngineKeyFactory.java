package com.bumptech.glide.load.p023n;

import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.o */
class EngineKeyFactory {
    EngineKeyFactory() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public EngineKey mo10225a(Object obj, Key gVar, int i, int i2, Map<Class<?>, Transformation<?>> map, Class<?> cls, Class<?> cls2, Options iVar) {
        return new EngineKey(obj, gVar, i, i2, map, cls, cls2, iVar);
    }
}
