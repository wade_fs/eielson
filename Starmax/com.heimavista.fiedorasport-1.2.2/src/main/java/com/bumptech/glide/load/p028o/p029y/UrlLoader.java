package com.bumptech.glide.load.p028o.p029y;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p028o.GlideUrl;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p028o.ModelLoaderFactory;
import com.bumptech.glide.load.p028o.MultiModelLoaderFactory;
import java.io.InputStream;
import java.net.URL;

/* renamed from: com.bumptech.glide.load.o.y.e */
public class UrlLoader implements ModelLoader<URL, InputStream> {

    /* renamed from: a */
    private final ModelLoader<GlideUrl, InputStream> f1381a;

    /* renamed from: com.bumptech.glide.load.o.y.e$a */
    /* compiled from: UrlLoader */
    public static class C1061a implements ModelLoaderFactory<URL, InputStream> {
        @NonNull
        /* renamed from: a */
        public ModelLoader<URL, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new UrlLoader(rVar.mo10313a(GlideUrl.class, InputStream.class));
        }
    }

    public UrlLoader(ModelLoader<GlideUrl, InputStream> nVar) {
        this.f1381a = nVar;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull URL url) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<InputStream> mo10261a(@NonNull URL url, int i, int i2, @NonNull Options iVar) {
        return this.f1381a.mo10261a(new GlideUrl(url), i, i2, iVar);
    }
}
