package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.h */
public final class Option<T> {

    /* renamed from: e */
    private static final C0933b<Object> f929e = new C0932a();

    /* renamed from: a */
    private final T f930a;

    /* renamed from: b */
    private final C0933b<T> f931b;

    /* renamed from: c */
    private final String f932c;

    /* renamed from: d */
    private volatile byte[] f933d;

    /* renamed from: com.bumptech.glide.load.h$a */
    /* compiled from: Option */
    class C0932a implements C0933b<Object> {
        C0932a() {
        }

        /* renamed from: a */
        public void mo9974a(@NonNull byte[] bArr, @NonNull Object obj, @NonNull MessageDigest messageDigest) {
        }
    }

    /* renamed from: com.bumptech.glide.load.h$b */
    /* compiled from: Option */
    public interface C0933b<T> {
        /* renamed from: a */
        void mo9974a(@NonNull byte[] bArr, @NonNull T t, @NonNull MessageDigest messageDigest);
    }

    private Option(@NonNull String str, @Nullable T t, @NonNull C0933b<T> bVar) {
        Preconditions.m2830a(str);
        this.f932c = str;
        this.f930a = t;
        Preconditions.m2828a(bVar);
        this.f931b = bVar;
    }

    @NonNull
    /* renamed from: a */
    public static <T> Option<T> m1369a(@NonNull String str) {
        return new Option<>(str, null, m1372b());
    }

    @NonNull
    /* renamed from: b */
    private static <T> C0933b<T> m1372b() {
        return f929e;
    }

    @NonNull
    /* renamed from: c */
    private byte[] m1373c() {
        if (this.f933d == null) {
            this.f933d = this.f932c.getBytes(Key.f928a);
        }
        return this.f933d;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Option) {
            return this.f932c.equals(((Option) obj).f932c);
        }
        return false;
    }

    public int hashCode() {
        return this.f932c.hashCode();
    }

    public String toString() {
        return "Option{key='" + this.f932c + '\'' + '}';
    }

    @NonNull
    /* renamed from: a */
    public static <T> Option<T> m1370a(@NonNull String str, @NonNull T t) {
        return new Option<>(str, t, m1372b());
    }

    @NonNull
    /* renamed from: a */
    public static <T> Option<T> m1371a(@NonNull String str, @Nullable T t, @NonNull C0933b<T> bVar) {
        return new Option<>(str, t, bVar);
    }

    @Nullable
    /* renamed from: a */
    public T mo9969a() {
        return this.f930a;
    }

    /* renamed from: a */
    public void mo9970a(@NonNull T t, @NonNull MessageDigest messageDigest) {
        this.f931b.mo9974a(m1373c(), t, messageDigest);
    }
}
