package com.bumptech.glide.load.p028o;

import com.bumptech.glide.load.p028o.LazyHeaders;
import java.util.Collections;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.o.h */
public interface Headers {

    /* renamed from: a */
    public static final Headers f1312a = new LazyHeaders.C1030a().mo10293a();

    /* renamed from: com.bumptech.glide.load.o.h$a */
    /* compiled from: Headers */
    class C1029a implements Headers {
        C1029a() {
        }

        /* renamed from: a */
        public Map<String, String> mo10288a() {
            return Collections.emptyMap();
        }
    }

    static {
        new C1029a();
    }

    /* renamed from: a */
    Map<String, String> mo10288a();
}
