package com.bumptech.glide.load.p023n.p025b0;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Key;
import java.io.File;

/* renamed from: com.bumptech.glide.load.n.b0.a */
public interface DiskCache {

    /* renamed from: com.bumptech.glide.load.n.b0.a$a */
    /* compiled from: DiskCache */
    public interface C0958a {
        @Nullable
        DiskCache build();
    }

    /* renamed from: com.bumptech.glide.load.n.b0.a$b */
    /* compiled from: DiskCache */
    public interface C0959b {
        /* renamed from: a */
        boolean mo10091a(@NonNull File file);
    }

    @Nullable
    /* renamed from: a */
    File mo10088a(Key gVar);

    /* renamed from: a */
    void mo10089a(Key gVar, C0959b bVar);
}
