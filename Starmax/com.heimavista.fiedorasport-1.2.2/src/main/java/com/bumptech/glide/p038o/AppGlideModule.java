package com.bumptech.glide.p038o;

import android.content.Context;
import androidx.annotation.NonNull;
import com.bumptech.glide.GlideBuilder;

/* renamed from: com.bumptech.glide.o.a */
public abstract class AppGlideModule extends LibraryGlideModule implements AppliesOptions {
    /* renamed from: a */
    public void mo10546a(@NonNull Context context, @NonNull GlideBuilder dVar) {
    }

    /* renamed from: a */
    public boolean mo10547a() {
        return true;
    }
}
