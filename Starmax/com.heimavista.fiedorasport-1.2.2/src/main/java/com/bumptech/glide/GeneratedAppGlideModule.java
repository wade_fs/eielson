package com.bumptech.glide;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.manager.RequestManagerRetriever;
import com.bumptech.glide.p038o.AppGlideModule;
import java.util.Set;

/* renamed from: com.bumptech.glide.a */
abstract class GeneratedAppGlideModule extends AppGlideModule {
    GeneratedAppGlideModule() {
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: b */
    public abstract Set<Class<?>> mo9886b();

    /* access modifiers changed from: package-private */
    @Nullable
    /* renamed from: c */
    public RequestManagerRetriever.C1106b mo9887c() {
        return null;
    }
}
