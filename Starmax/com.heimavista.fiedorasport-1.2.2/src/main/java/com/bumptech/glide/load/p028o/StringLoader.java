package com.bumptech.glide.load.p028o;

import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p028o.ModelLoader;
import java.io.File;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.u */
public class StringLoader<Data> implements ModelLoader<String, Data> {

    /* renamed from: a */
    private final ModelLoader<Uri, Data> f1361a;

    /* renamed from: com.bumptech.glide.load.o.u$a */
    /* compiled from: StringLoader */
    public static final class C1047a implements ModelLoaderFactory<String, AssetFileDescriptor> {
        /* renamed from: a */
        public ModelLoader<String, AssetFileDescriptor> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new StringLoader(rVar.mo10313a(Uri.class, AssetFileDescriptor.class));
        }
    }

    /* renamed from: com.bumptech.glide.load.o.u$b */
    /* compiled from: StringLoader */
    public static class C1048b implements ModelLoaderFactory<String, ParcelFileDescriptor> {
        @NonNull
        /* renamed from: a */
        public ModelLoader<String, ParcelFileDescriptor> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new StringLoader(rVar.mo10313a(Uri.class, ParcelFileDescriptor.class));
        }
    }

    /* renamed from: com.bumptech.glide.load.o.u$c */
    /* compiled from: StringLoader */
    public static class C1049c implements ModelLoaderFactory<String, InputStream> {
        @NonNull
        /* renamed from: a */
        public ModelLoader<String, InputStream> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new StringLoader(rVar.mo10313a(Uri.class, InputStream.class));
        }
    }

    public StringLoader(ModelLoader<Uri, Data> nVar) {
        this.f1361a = nVar;
    }

    @Nullable
    /* renamed from: b */
    private static Uri m2017b(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.charAt(0) == '/') {
            return m2018c(str);
        }
        Uri parse = Uri.parse(str);
        return parse.getScheme() == null ? m2018c(str) : parse;
    }

    /* renamed from: c */
    private static Uri m2018c(String str) {
        return Uri.fromFile(new File(str));
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull String str) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull String str, int i, int i2, @NonNull Options iVar) {
        Uri b = m2017b(str);
        if (b == null || !this.f1361a.mo10263a(b)) {
            return null;
        }
        return this.f1361a.mo10261a(b, i, i2, iVar);
    }
}
