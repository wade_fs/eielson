package com.bumptech.glide.util.p044k;

import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.util.k.c */
public abstract class StateVerifier {
    @NonNull
    /* renamed from: b */
    public static StateVerifier m2871b() {
        return new C1132b();
    }

    /* renamed from: a */
    public abstract void mo10716a();

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo10717a(boolean z);

    /* renamed from: com.bumptech.glide.util.k.c$b */
    /* compiled from: StateVerifier */
    private static class C1132b extends StateVerifier {

        /* renamed from: a */
        private volatile boolean f1795a;

        C1132b() {
            super();
        }

        /* renamed from: a */
        public void mo10716a() {
            if (this.f1795a) {
                throw new IllegalStateException("Already released");
            }
        }

        /* renamed from: a */
        public void mo10717a(boolean z) {
            this.f1795a = z;
        }
    }

    private StateVerifier() {
    }
}
