package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.ByteBufferUtil;
import java.nio.ByteBuffer;

/* renamed from: com.bumptech.glide.load.p.c.f */
public class ByteBufferBitmapDecoder implements ResourceDecoder<ByteBuffer, Bitmap> {

    /* renamed from: a */
    private final Downsampler f1393a;

    public ByteBufferBitmapDecoder(Downsampler lVar) {
        this.f1393a = lVar;
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull ByteBuffer byteBuffer, @NonNull Options iVar) {
        return this.f1393a.mo10359a(byteBuffer);
    }

    /* renamed from: a */
    public Resource<Bitmap> mo9979a(@NonNull ByteBuffer byteBuffer, int i, int i2, @NonNull Options iVar) {
        return this.f1393a.mo10356a(ByteBufferUtil.m2805c(byteBuffer), i, i2, iVar);
    }
}
