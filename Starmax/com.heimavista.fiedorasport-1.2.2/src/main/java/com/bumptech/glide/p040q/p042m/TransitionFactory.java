package com.bumptech.glide.p040q.p042m;

import com.bumptech.glide.load.DataSource;

/* renamed from: com.bumptech.glide.q.m.c */
public interface TransitionFactory<R> {
    /* renamed from: a */
    Transition<R> mo10670a(DataSource aVar, boolean z);
}
