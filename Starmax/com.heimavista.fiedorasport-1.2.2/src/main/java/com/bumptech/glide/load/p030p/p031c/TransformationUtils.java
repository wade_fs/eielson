package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: com.bumptech.glide.load.p.c.v */
public final class TransformationUtils {

    /* renamed from: a */
    private static final Paint f1445a = new Paint(6);

    /* renamed from: b */
    private static final Paint f1446b = new Paint(7);

    /* renamed from: c */
    private static final Paint f1447c = new Paint(7);

    /* renamed from: d */
    private static final Set<String> f1448d = new HashSet(Arrays.asList("XT1085", "XT1092", "XT1093", "XT1094", "XT1095", "XT1096", "XT1097", "XT1098", "XT1031", "XT1028", "XT937C", "XT1032", "XT1008", "XT1033", "XT1035", "XT1034", "XT939G", "XT1039", "XT1040", "XT1042", "XT1045", "XT1063", "XT1064", "XT1068", "XT1069", "XT1072", "XT1077", "XT1078", "XT1079"));

    /* renamed from: e */
    private static final Lock f1449e = (f1448d.contains(Build.MODEL) ? new ReentrantLock() : new C1078a());

    /* renamed from: com.bumptech.glide.load.p.c.v$a */
    /* compiled from: TransformationUtils */
    private static final class C1078a implements Lock {
        C1078a() {
        }

        public void lock() {
        }

        public void lockInterruptibly() {
        }

        @NonNull
        public Condition newCondition() {
            throw new UnsupportedOperationException("Should not be called");
        }

        public boolean tryLock() {
            return true;
        }

        public boolean tryLock(long j, @NonNull TimeUnit timeUnit) {
            return true;
        }

        public void unlock() {
        }
    }

    static {
        f1447c.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
    }

    /* renamed from: a */
    public static int m2208a(int i) {
        switch (i) {
            case 3:
            case 4:
                return 180;
            case 5:
            case 6:
                return 90;
            case 7:
            case 8:
                return 270;
            default:
                return 0;
        }
    }

    /* renamed from: a */
    public static Lock m2213a() {
        return f1449e;
    }

    /* renamed from: b */
    public static Bitmap m2219b(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        if (bitmap.getWidth() > i || bitmap.getHeight() > i2) {
            if (Log.isLoggable("TransformationUtils", 2)) {
                Log.v("TransformationUtils", "requested target size too big for input, fit centering instead");
            }
            return m2222d(eVar, bitmap, i, i2);
        }
        if (Log.isLoggable("TransformationUtils", 2)) {
            Log.v("TransformationUtils", "requested target size larger or equal to input, returning input");
        }
        return bitmap;
    }

    /* renamed from: b */
    public static boolean m2220b(int i) {
        switch (i) {
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
                return true;
            default:
                return false;
        }
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: c */
    public static Bitmap m2221c(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        int min = Math.min(i, i2);
        float f = (float) min;
        float f2 = f / 2.0f;
        float width = (float) bitmap.getWidth();
        float height = (float) bitmap.getHeight();
        float max = Math.max(f / width, f / height);
        float f3 = width * max;
        float f4 = max * height;
        float f5 = (f - f3) / 2.0f;
        float f6 = (f - f4) / 2.0f;
        RectF rectF = new RectF(f5, f6, f3 + f5, f4 + f6);
        Bitmap a = m2210a(eVar, bitmap);
        Bitmap a2 = eVar.mo10060a(min, min, m2209a(bitmap));
        a2.setHasAlpha(true);
        f1449e.lock();
        try {
            Canvas canvas = new Canvas(a2);
            canvas.drawCircle(f2, f2, f2, f1446b);
            canvas.drawBitmap(a, (Rect) null, rectF, f1447c);
            m2217a(canvas);
            f1449e.unlock();
            if (!a.equals(bitmap)) {
                eVar.mo10063a(a);
            }
            return a2;
        } catch (Throwable th) {
            f1449e.unlock();
            throw th;
        }
    }

    /* renamed from: d */
    public static Bitmap m2222d(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        if (bitmap.getWidth() == i && bitmap.getHeight() == i2) {
            if (Log.isLoggable("TransformationUtils", 2)) {
                Log.v("TransformationUtils", "requested target size matches input, returning input");
            }
            return bitmap;
        }
        float min = Math.min(((float) i) / ((float) bitmap.getWidth()), ((float) i2) / ((float) bitmap.getHeight()));
        int round = Math.round(((float) bitmap.getWidth()) * min);
        int round2 = Math.round(((float) bitmap.getHeight()) * min);
        if (bitmap.getWidth() == round && bitmap.getHeight() == round2) {
            if (Log.isLoggable("TransformationUtils", 2)) {
                Log.v("TransformationUtils", "adjusted target size matches input, returning input");
            }
            return bitmap;
        }
        Bitmap a = eVar.mo10060a((int) (((float) bitmap.getWidth()) * min), (int) (((float) bitmap.getHeight()) * min), m2218b(bitmap));
        m2215a(bitmap, a);
        if (Log.isLoggable("TransformationUtils", 2)) {
            Log.v("TransformationUtils", "request: " + i + "x" + i2);
            Log.v("TransformationUtils", "toFit:   " + bitmap.getWidth() + "x" + bitmap.getHeight());
            Log.v("TransformationUtils", "toReuse: " + a.getWidth() + "x" + a.getHeight());
            StringBuilder sb = new StringBuilder();
            sb.append("minPct:   ");
            sb.append(min);
            Log.v("TransformationUtils", sb.toString());
        }
        Matrix matrix = new Matrix();
        matrix.setScale(min, min);
        m2216a(bitmap, a, matrix);
        return a;
    }

    /* renamed from: a */
    public static Bitmap m2212a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        float f;
        float f2;
        if (bitmap.getWidth() == i && bitmap.getHeight() == i2) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        float f3 = 0.0f;
        if (bitmap.getWidth() * i2 > bitmap.getHeight() * i) {
            f2 = ((float) i2) / ((float) bitmap.getHeight());
            f = (((float) i) - (((float) bitmap.getWidth()) * f2)) * 0.5f;
        } else {
            f2 = ((float) i) / ((float) bitmap.getWidth());
            f3 = (((float) i2) - (((float) bitmap.getHeight()) * f2)) * 0.5f;
            f = 0.0f;
        }
        matrix.setScale(f2, f2);
        matrix.postTranslate((float) ((int) (f + 0.5f)), (float) ((int) (f3 + 0.5f)));
        Bitmap a = eVar.mo10060a(i, i2, m2218b(bitmap));
        m2215a(bitmap, a);
        m2216a(bitmap, a, matrix);
        return a;
    }

    @NonNull
    /* renamed from: b */
    private static Bitmap.Config m2218b(@NonNull Bitmap bitmap) {
        return bitmap.getConfig() != null ? bitmap.getConfig() : Bitmap.Config.ARGB_8888;
    }

    /* renamed from: a */
    public static void m2215a(Bitmap bitmap, Bitmap bitmap2) {
        bitmap2.setHasAlpha(bitmap.hasAlpha());
    }

    /* renamed from: a */
    public static Bitmap m2211a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i) {
        if (!m2220b(i)) {
            return bitmap;
        }
        Matrix matrix = new Matrix();
        m2214a(i, matrix);
        RectF rectF = new RectF(0.0f, 0.0f, (float) bitmap.getWidth(), (float) bitmap.getHeight());
        matrix.mapRect(rectF);
        Bitmap a = eVar.mo10060a(Math.round(rectF.width()), Math.round(rectF.height()), m2218b(bitmap));
        matrix.postTranslate(-rectF.left, -rectF.top);
        a.setHasAlpha(bitmap.hasAlpha());
        m2216a(bitmap, a, matrix);
        return a;
    }

    /* renamed from: a */
    private static Bitmap m2210a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap) {
        Bitmap.Config a = m2209a(bitmap);
        if (a.equals(bitmap.getConfig())) {
            return bitmap;
        }
        Bitmap a2 = eVar.mo10060a(bitmap.getWidth(), bitmap.getHeight(), a);
        new Canvas(a2).drawBitmap(bitmap, 0.0f, 0.0f, (Paint) null);
        return a2;
    }

    @NonNull
    /* renamed from: a */
    private static Bitmap.Config m2209a(@NonNull Bitmap bitmap) {
        if (Build.VERSION.SDK_INT < 26 || !Bitmap.Config.RGBA_F16.equals(bitmap.getConfig())) {
            return Bitmap.Config.ARGB_8888;
        }
        return Bitmap.Config.RGBA_F16;
    }

    /* renamed from: a */
    private static void m2217a(Canvas canvas) {
        canvas.setBitmap(null);
    }

    /* renamed from: a */
    private static void m2216a(@NonNull Bitmap bitmap, @NonNull Bitmap bitmap2, Matrix matrix) {
        f1449e.lock();
        try {
            Canvas canvas = new Canvas(bitmap2);
            canvas.drawBitmap(bitmap, matrix, f1445a);
            m2217a(canvas);
        } finally {
            f1449e.unlock();
        }
    }

    @VisibleForTesting
    /* renamed from: a */
    static void m2214a(int i, Matrix matrix) {
        switch (i) {
            case 2:
                matrix.setScale(-1.0f, 1.0f);
                return;
            case 3:
                matrix.setRotate(180.0f);
                return;
            case 4:
                matrix.setRotate(180.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 5:
                matrix.setRotate(90.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 6:
                matrix.setRotate(90.0f);
                return;
            case 7:
                matrix.setRotate(-90.0f);
                matrix.postScale(-1.0f, 1.0f);
                return;
            case 8:
                matrix.setRotate(-90.0f);
                return;
            default:
                return;
        }
    }
}
