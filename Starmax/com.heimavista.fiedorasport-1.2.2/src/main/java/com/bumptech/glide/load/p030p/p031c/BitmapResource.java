package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.Initializable;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.c.d */
public class BitmapResource implements Resource<Bitmap>, Initializable {

    /* renamed from: P */
    private final Bitmap f1391P;

    /* renamed from: Q */
    private final BitmapPool f1392Q;

    public BitmapResource(@NonNull Bitmap bitmap, @NonNull BitmapPool eVar) {
        Preconditions.m2829a(bitmap, "Bitmap must not be null");
        this.f1391P = bitmap;
        Preconditions.m2829a(eVar, "BitmapPool must not be null");
        this.f1392Q = eVar;
    }

    @Nullable
    /* renamed from: a */
    public static BitmapResource m2091a(@Nullable Bitmap bitmap, @NonNull BitmapPool eVar) {
        if (bitmap == null) {
            return null;
        }
        return new BitmapResource(bitmap, eVar);
    }

    @NonNull
    /* renamed from: b */
    public Class<Bitmap> mo10228b() {
        return Bitmap.class;
    }

    /* renamed from: c */
    public void mo10250c() {
        this.f1391P.prepareToDraw();
    }

    public void recycle() {
        this.f1392Q.mo10063a(this.f1391P);
    }

    /* renamed from: a */
    public int mo10226a() {
        return C1122j.m2838a(this.f1391P);
    }

    @NonNull
    public Bitmap get() {
        return this.f1391P;
    }
}
