package com.bumptech.glide.p040q;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import androidx.annotation.DrawableRes;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pools;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p023n.Engine;
import com.bumptech.glide.load.p023n.GlideException;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p030p.p033e.DrawableDecoderCompat;
import com.bumptech.glide.p040q.p041l.SizeReadyCallback;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.p040q.p042m.TransitionFactory;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.p044k.FactoryPools;
import com.bumptech.glide.util.p044k.StateVerifier;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: com.bumptech.glide.q.j */
public final class SingleRequest<R> implements Request, SizeReadyCallback, ResourceCallback, FactoryPools.C1129f {

    /* renamed from: r0 */
    private static final Pools.Pool<SingleRequest<?>> f1702r0 = FactoryPools.m2854a(150, new C1112a());

    /* renamed from: s0 */
    private static final boolean f1703s0 = Log.isLoggable("Request", 2);

    /* renamed from: P */
    private boolean f1704P;
    @Nullable

    /* renamed from: Q */
    private final String f1705Q;

    /* renamed from: R */
    private final StateVerifier f1706R;
    @Nullable

    /* renamed from: S */
    private RequestListener<R> f1707S;

    /* renamed from: T */
    private RequestCoordinator f1708T;

    /* renamed from: U */
    private Context f1709U;

    /* renamed from: V */
    private GlideContext f1710V;
    @Nullable

    /* renamed from: W */
    private Object f1711W;

    /* renamed from: X */
    private Class<R> f1712X;

    /* renamed from: Y */
    private BaseRequestOptions<?> f1713Y;

    /* renamed from: Z */
    private int f1714Z;

    /* renamed from: a0 */
    private int f1715a0;

    /* renamed from: b0 */
    private Priority f1716b0;

    /* renamed from: c0 */
    private Target<R> f1717c0;
    @Nullable

    /* renamed from: d0 */
    private List<RequestListener<R>> f1718d0;

    /* renamed from: e0 */
    private Engine f1719e0;

    /* renamed from: f0 */
    private TransitionFactory<? super R> f1720f0;

    /* renamed from: g0 */
    private Executor f1721g0;

    /* renamed from: h0 */
    private Resource<R> f1722h0;

    /* renamed from: i0 */
    private Engine.C0997d f1723i0;

    /* renamed from: j0 */
    private long f1724j0;
    @GuardedBy("this")

    /* renamed from: k0 */
    private C1113b f1725k0;

    /* renamed from: l0 */
    private Drawable f1726l0;

    /* renamed from: m0 */
    private Drawable f1727m0;

    /* renamed from: n0 */
    private Drawable f1728n0;

    /* renamed from: o0 */
    private int f1729o0;

    /* renamed from: p0 */
    private int f1730p0;
    @Nullable

    /* renamed from: q0 */
    private RuntimeException f1731q0;

    /* renamed from: com.bumptech.glide.q.j$a */
    /* compiled from: SingleRequest */
    class C1112a implements FactoryPools.C1127d<SingleRequest<?>> {
        C1112a() {
        }

        /* renamed from: a */
        public SingleRequest<?> m2714a() {
            return new SingleRequest<>();
        }
    }

    /* renamed from: com.bumptech.glide.q.j$b */
    /* compiled from: SingleRequest */
    private enum C1113b {
        PENDING,
        RUNNING,
        WAITING_FOR_SIZE,
        COMPLETE,
        FAILED,
        CLEARED
    }

    SingleRequest() {
        this.f1705Q = f1703s0 ? String.valueOf(super.hashCode()) : null;
        this.f1706R = StateVerifier.m2871b();
    }

    /* renamed from: a */
    private synchronized void m2685a(Context context, GlideContext eVar, Object obj, Class<R> cls, BaseRequestOptions<?> aVar, int i, int i2, Priority hVar, Target<R> iVar, RequestListener<R> gVar, @Nullable List<RequestListener<R>> list, RequestCoordinator eVar2, Engine kVar, TransitionFactory<? super R> cVar, Executor executor) {
        this.f1709U = context;
        this.f1710V = eVar;
        this.f1711W = obj;
        this.f1712X = cls;
        this.f1713Y = aVar;
        this.f1714Z = i;
        this.f1715a0 = i2;
        this.f1716b0 = hVar;
        this.f1717c0 = iVar;
        this.f1707S = gVar;
        this.f1718d0 = list;
        this.f1708T = eVar2;
        this.f1719e0 = kVar;
        this.f1720f0 = cVar;
        this.f1721g0 = executor;
        this.f1725k0 = C1113b.PENDING;
        if (this.f1731q0 == null && eVar.mo9913g()) {
            this.f1731q0 = new RuntimeException("Glide request origin trace");
        }
    }

    /* renamed from: b */
    public static <R> SingleRequest<R> m2691b(Context context, GlideContext eVar, Object obj, Class<R> cls, BaseRequestOptions<?> aVar, int i, int i2, Priority hVar, Target<R> iVar, RequestListener<R> gVar, @Nullable List<RequestListener<R>> list, RequestCoordinator eVar2, Engine kVar, TransitionFactory<? super R> cVar, Executor executor) {
        SingleRequest<R> acquire = f1702r0.acquire();
        if (acquire == null) {
            acquire = new SingleRequest<>();
        }
        acquire.m2685a(context, eVar, obj, cls, aVar, i, i2, hVar, iVar, gVar, list, eVar2, kVar, cVar, executor);
        return acquire;
    }

    /* renamed from: i */
    private boolean m2693i() {
        RequestCoordinator eVar = this.f1708T;
        return eVar == null || eVar.mo10627c(this);
    }

    /* renamed from: j */
    private boolean m2694j() {
        RequestCoordinator eVar = this.f1708T;
        return eVar == null || eVar.mo10629d(this);
    }

    /* renamed from: k */
    private void m2695k() {
        m2684a();
        this.f1706R.mo10716a();
        this.f1717c0.mo10641a((SizeReadyCallback) this);
        Engine.C0997d dVar = this.f1723i0;
        if (dVar != null) {
            dVar.mo10198a();
            this.f1723i0 = null;
        }
    }

    /* renamed from: l */
    private Drawable m2696l() {
        if (this.f1726l0 == null) {
            this.f1726l0 = this.f1713Y.mo10600e();
            if (this.f1726l0 == null && this.f1713Y.mo10599d() > 0) {
                this.f1726l0 = m2683a(this.f1713Y.mo10599d());
            }
        }
        return this.f1726l0;
    }

    /* renamed from: m */
    private Drawable m2697m() {
        if (this.f1728n0 == null) {
            this.f1728n0 = this.f1713Y.mo10602f();
            if (this.f1728n0 == null && this.f1713Y.mo10603g() > 0) {
                this.f1728n0 = m2683a(this.f1713Y.mo10603g());
            }
        }
        return this.f1728n0;
    }

    /* renamed from: n */
    private Drawable m2698n() {
        if (this.f1727m0 == null) {
            this.f1727m0 = this.f1713Y.mo10609l();
            if (this.f1727m0 == null && this.f1713Y.mo10610m() > 0) {
                this.f1727m0 = m2683a(this.f1713Y.mo10610m());
            }
        }
        return this.f1727m0;
    }

    /* renamed from: o */
    private boolean m2699o() {
        RequestCoordinator eVar = this.f1708T;
        return eVar == null || !eVar.mo10623a();
    }

    /* renamed from: p */
    private void m2700p() {
        RequestCoordinator eVar = this.f1708T;
        if (eVar != null) {
            eVar.mo10625b(this);
        }
    }

    /* renamed from: q */
    private void m2701q() {
        RequestCoordinator eVar = this.f1708T;
        if (eVar != null) {
            eVar.mo10630e(this);
        }
    }

    /* renamed from: r */
    private synchronized void m2702r() {
        if (m2693i()) {
            Drawable drawable = null;
            if (this.f1711W == null) {
                drawable = m2697m();
            }
            if (drawable == null) {
                drawable = m2696l();
            }
            if (drawable == null) {
                drawable = m2698n();
            }
            this.f1717c0.mo10639a(drawable);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.q.j.a(com.bumptech.glide.load.n.v<?>, com.bumptech.glide.load.a):void
     arg types: [com.bumptech.glide.load.n.v<R>, com.bumptech.glide.load.a]
     candidates:
      com.bumptech.glide.q.j.a(int, float):int
      com.bumptech.glide.q.j.a(com.bumptech.glide.load.n.q, int):void
      com.bumptech.glide.q.j.a(int, int):void
      com.bumptech.glide.q.l.h.a(int, int):void
      com.bumptech.glide.q.j.a(com.bumptech.glide.load.n.v<?>, com.bumptech.glide.load.a):void */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00a4, code lost:
        return;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo10626c() {
        /*
            r3 = this;
            monitor-enter(r3)
            r3.m2684a()     // Catch:{ all -> 0x00ad }
            com.bumptech.glide.util.k.c r0 = r3.f1706R     // Catch:{ all -> 0x00ad }
            r0.mo10716a()     // Catch:{ all -> 0x00ad }
            long r0 = com.bumptech.glide.util.LogTime.m2815a()     // Catch:{ all -> 0x00ad }
            r3.f1724j0 = r0     // Catch:{ all -> 0x00ad }
            java.lang.Object r0 = r3.f1711W     // Catch:{ all -> 0x00ad }
            if (r0 != 0) goto L_0x003a
            int r0 = r3.f1714Z     // Catch:{ all -> 0x00ad }
            int r1 = r3.f1715a0     // Catch:{ all -> 0x00ad }
            boolean r0 = com.bumptech.glide.util.C1122j.m2849b(r0, r1)     // Catch:{ all -> 0x00ad }
            if (r0 == 0) goto L_0x0025
            int r0 = r3.f1714Z     // Catch:{ all -> 0x00ad }
            r3.f1729o0 = r0     // Catch:{ all -> 0x00ad }
            int r0 = r3.f1715a0     // Catch:{ all -> 0x00ad }
            r3.f1730p0 = r0     // Catch:{ all -> 0x00ad }
        L_0x0025:
            android.graphics.drawable.Drawable r0 = r3.m2697m()     // Catch:{ all -> 0x00ad }
            if (r0 != 0) goto L_0x002d
            r0 = 5
            goto L_0x002e
        L_0x002d:
            r0 = 3
        L_0x002e:
            com.bumptech.glide.load.n.q r1 = new com.bumptech.glide.load.n.q     // Catch:{ all -> 0x00ad }
            java.lang.String r2 = "Received null model"
            r1.<init>(r2)     // Catch:{ all -> 0x00ad }
            r3.m2686a(r1, r0)     // Catch:{ all -> 0x00ad }
            monitor-exit(r3)
            return
        L_0x003a:
            com.bumptech.glide.q.j$b r0 = r3.f1725k0     // Catch:{ all -> 0x00ad }
            com.bumptech.glide.q.j$b r1 = com.bumptech.glide.p040q.SingleRequest.C1113b.RUNNING     // Catch:{ all -> 0x00ad }
            if (r0 == r1) goto L_0x00a5
            com.bumptech.glide.q.j$b r0 = r3.f1725k0     // Catch:{ all -> 0x00ad }
            com.bumptech.glide.q.j$b r1 = com.bumptech.glide.p040q.SingleRequest.C1113b.COMPLETE     // Catch:{ all -> 0x00ad }
            if (r0 != r1) goto L_0x004f
            com.bumptech.glide.load.n.v<R> r0 = r3.f1722h0     // Catch:{ all -> 0x00ad }
            com.bumptech.glide.load.a r1 = com.bumptech.glide.load.DataSource.MEMORY_CACHE     // Catch:{ all -> 0x00ad }
            r3.mo10655a(r0, r1)     // Catch:{ all -> 0x00ad }
            monitor-exit(r3)
            return
        L_0x004f:
            com.bumptech.glide.q.j$b r0 = com.bumptech.glide.p040q.SingleRequest.C1113b.WAITING_FOR_SIZE     // Catch:{ all -> 0x00ad }
            r3.f1725k0 = r0     // Catch:{ all -> 0x00ad }
            int r0 = r3.f1714Z     // Catch:{ all -> 0x00ad }
            int r1 = r3.f1715a0     // Catch:{ all -> 0x00ad }
            boolean r0 = com.bumptech.glide.util.C1122j.m2849b(r0, r1)     // Catch:{ all -> 0x00ad }
            if (r0 == 0) goto L_0x0065
            int r0 = r3.f1714Z     // Catch:{ all -> 0x00ad }
            int r1 = r3.f1715a0     // Catch:{ all -> 0x00ad }
            r3.mo10656a(r0, r1)     // Catch:{ all -> 0x00ad }
            goto L_0x006a
        L_0x0065:
            com.bumptech.glide.q.l.i<R> r0 = r3.f1717c0     // Catch:{ all -> 0x00ad }
            r0.mo10645b(r3)     // Catch:{ all -> 0x00ad }
        L_0x006a:
            com.bumptech.glide.q.j$b r0 = r3.f1725k0     // Catch:{ all -> 0x00ad }
            com.bumptech.glide.q.j$b r1 = com.bumptech.glide.p040q.SingleRequest.C1113b.RUNNING     // Catch:{ all -> 0x00ad }
            if (r0 == r1) goto L_0x0076
            com.bumptech.glide.q.j$b r0 = r3.f1725k0     // Catch:{ all -> 0x00ad }
            com.bumptech.glide.q.j$b r1 = com.bumptech.glide.p040q.SingleRequest.C1113b.WAITING_FOR_SIZE     // Catch:{ all -> 0x00ad }
            if (r0 != r1) goto L_0x0085
        L_0x0076:
            boolean r0 = r3.m2693i()     // Catch:{ all -> 0x00ad }
            if (r0 == 0) goto L_0x0085
            com.bumptech.glide.q.l.i<R> r0 = r3.f1717c0     // Catch:{ all -> 0x00ad }
            android.graphics.drawable.Drawable r1 = r3.m2698n()     // Catch:{ all -> 0x00ad }
            r0.mo10644b(r1)     // Catch:{ all -> 0x00ad }
        L_0x0085:
            boolean r0 = com.bumptech.glide.p040q.SingleRequest.f1703s0     // Catch:{ all -> 0x00ad }
            if (r0 == 0) goto L_0x00a3
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ad }
            r0.<init>()     // Catch:{ all -> 0x00ad }
            java.lang.String r1 = "finished run method in "
            r0.append(r1)     // Catch:{ all -> 0x00ad }
            long r1 = r3.f1724j0     // Catch:{ all -> 0x00ad }
            double r1 = com.bumptech.glide.util.LogTime.m2814a(r1)     // Catch:{ all -> 0x00ad }
            r0.append(r1)     // Catch:{ all -> 0x00ad }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00ad }
            r3.m2689a(r0)     // Catch:{ all -> 0x00ad }
        L_0x00a3:
            monitor-exit(r3)
            return
        L_0x00a5:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ all -> 0x00ad }
            java.lang.String r1 = "Cannot restart a running request"
            r0.<init>(r1)     // Catch:{ all -> 0x00ad }
            throw r0     // Catch:{ all -> 0x00ad }
        L_0x00ad:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.SingleRequest.mo10626c():void");
    }

    public synchronized void clear() {
        m2684a();
        this.f1706R.mo10716a();
        if (this.f1725k0 != C1113b.CLEARED) {
            m2695k();
            if (this.f1722h0 != null) {
                m2687a((Resource<?>) this.f1722h0);
            }
            if (m2692b()) {
                this.f1717c0.mo10646c(m2698n());
            }
            this.f1725k0 = C1113b.CLEARED;
        }
    }

    @NonNull
    /* renamed from: d */
    public StateVerifier mo10115d() {
        return this.f1706R;
    }

    /* renamed from: e */
    public synchronized boolean mo10631e() {
        return mo10635h();
    }

    /* renamed from: f */
    public synchronized boolean mo10632f() {
        return this.f1725k0 == C1113b.FAILED;
    }

    /* renamed from: g */
    public synchronized boolean mo10634g() {
        return this.f1725k0 == C1113b.CLEARED;
    }

    /* renamed from: h */
    public synchronized boolean mo10635h() {
        return this.f1725k0 == C1113b.COMPLETE;
    }

    public synchronized boolean isRunning() {
        return this.f1725k0 == C1113b.RUNNING || this.f1725k0 == C1113b.WAITING_FOR_SIZE;
    }

    public synchronized void recycle() {
        m2684a();
        this.f1709U = null;
        this.f1710V = null;
        this.f1711W = null;
        this.f1712X = null;
        this.f1713Y = null;
        this.f1714Z = -1;
        this.f1715a0 = -1;
        this.f1717c0 = null;
        this.f1718d0 = null;
        this.f1707S = null;
        this.f1708T = null;
        this.f1720f0 = null;
        this.f1723i0 = null;
        this.f1726l0 = null;
        this.f1727m0 = null;
        this.f1728n0 = null;
        this.f1729o0 = -1;
        this.f1730p0 = -1;
        this.f1731q0 = null;
        f1702r0.release(this);
    }

    /* renamed from: b */
    private boolean m2692b() {
        RequestCoordinator eVar = this.f1708T;
        return eVar == null || eVar.mo10633f(this);
    }

    /* renamed from: a */
    private void m2684a() {
        if (this.f1704P) {
            throw new IllegalStateException("You can't start or clear loads in RequestListener or Target callbacks. If you're trying to start a fallback request when a load fails, use RequestBuilder#error(RequestBuilder). Otherwise consider posting your into() or clear() calls to the main thread using a Handler instead.");
        }
    }

    /* renamed from: a */
    private void m2687a(Resource<?> vVar) {
        this.f1719e0.mo10195b(vVar);
        this.f1722h0 = null;
    }

    /* renamed from: a */
    private Drawable m2683a(@DrawableRes int i) {
        return DrawableDecoderCompat.m2254a(this.f1710V, i, this.f1713Y.mo10615t() != null ? this.f1713Y.mo10615t() : this.f1709U.getTheme());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00f0, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo10656a(int r22, int r23) {
        /*
            r21 = this;
            r15 = r21
            monitor-enter(r21)
            com.bumptech.glide.util.k.c r0 = r15.f1706R     // Catch:{ all -> 0x00f7 }
            r0.mo10716a()     // Catch:{ all -> 0x00f7 }
            boolean r0 = com.bumptech.glide.p040q.SingleRequest.f1703s0     // Catch:{ all -> 0x00f7 }
            if (r0 == 0) goto L_0x0026
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f7 }
            r0.<init>()     // Catch:{ all -> 0x00f7 }
            java.lang.String r1 = "Got onSizeReady in "
            r0.append(r1)     // Catch:{ all -> 0x00f7 }
            long r1 = r15.f1724j0     // Catch:{ all -> 0x00f7 }
            double r1 = com.bumptech.glide.util.LogTime.m2814a(r1)     // Catch:{ all -> 0x00f7 }
            r0.append(r1)     // Catch:{ all -> 0x00f7 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f7 }
            r15.m2689a(r0)     // Catch:{ all -> 0x00f7 }
        L_0x0026:
            com.bumptech.glide.q.j$b r0 = r15.f1725k0     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.j$b r1 = com.bumptech.glide.p040q.SingleRequest.C1113b.WAITING_FOR_SIZE     // Catch:{ all -> 0x00f7 }
            if (r0 == r1) goto L_0x002e
            monitor-exit(r21)
            return
        L_0x002e:
            com.bumptech.glide.q.j$b r0 = com.bumptech.glide.p040q.SingleRequest.C1113b.RUNNING     // Catch:{ all -> 0x00f7 }
            r15.f1725k0 = r0     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            float r0 = r0.mo10614q()     // Catch:{ all -> 0x00f7 }
            r1 = r22
            int r1 = m2682a(r1, r0)     // Catch:{ all -> 0x00f7 }
            r15.f1729o0 = r1     // Catch:{ all -> 0x00f7 }
            r1 = r23
            int r0 = m2682a(r1, r0)     // Catch:{ all -> 0x00f7 }
            r15.f1730p0 = r0     // Catch:{ all -> 0x00f7 }
            boolean r0 = com.bumptech.glide.p040q.SingleRequest.f1703s0     // Catch:{ all -> 0x00f7 }
            if (r0 == 0) goto L_0x0066
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f7 }
            r0.<init>()     // Catch:{ all -> 0x00f7 }
            java.lang.String r1 = "finished setup for calling load in "
            r0.append(r1)     // Catch:{ all -> 0x00f7 }
            long r1 = r15.f1724j0     // Catch:{ all -> 0x00f7 }
            double r1 = com.bumptech.glide.util.LogTime.m2814a(r1)     // Catch:{ all -> 0x00f7 }
            r0.append(r1)     // Catch:{ all -> 0x00f7 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f7 }
            r15.m2689a(r0)     // Catch:{ all -> 0x00f7 }
        L_0x0066:
            com.bumptech.glide.load.n.k r1 = r15.f1719e0     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.e r2 = r15.f1710V     // Catch:{ all -> 0x00f7 }
            java.lang.Object r3 = r15.f1711W     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.load.g r4 = r0.mo10613p()     // Catch:{ all -> 0x00f7 }
            int r5 = r15.f1729o0     // Catch:{ all -> 0x00f7 }
            int r6 = r15.f1730p0     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            java.lang.Class r7 = r0.mo10612o()     // Catch:{ all -> 0x00f7 }
            java.lang.Class<R> r8 = r15.f1712X     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.h r9 = r15.f1716b0     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.load.n.j r10 = r0.mo10597c()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            java.util.Map r11 = r0.mo10616u()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            boolean r12 = r0.mo10571B()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            boolean r13 = r0.mo10621z()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.load.i r14 = r0.mo10606i()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            boolean r0 = r0.mo10619x()     // Catch:{ all -> 0x00f7 }
            r22 = r0
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            boolean r16 = r0.mo10618w()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            boolean r17 = r0.mo10617v()     // Catch:{ all -> 0x00f7 }
            com.bumptech.glide.q.a<?> r0 = r15.f1713Y     // Catch:{ all -> 0x00f7 }
            boolean r18 = r0.mo10604h()     // Catch:{ all -> 0x00f7 }
            java.util.concurrent.Executor r0 = r15.f1721g0     // Catch:{ all -> 0x00f7 }
            r15 = r22
            r19 = r21
            r20 = r0
            com.bumptech.glide.load.n.k$d r0 = r1.mo10191a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ all -> 0x00f3 }
            r1 = r21
            r1.f1723i0 = r0     // Catch:{ all -> 0x00f1 }
            com.bumptech.glide.q.j$b r0 = r1.f1725k0     // Catch:{ all -> 0x00f1 }
            com.bumptech.glide.q.j$b r2 = com.bumptech.glide.p040q.SingleRequest.C1113b.RUNNING     // Catch:{ all -> 0x00f1 }
            if (r0 == r2) goto L_0x00d1
            r0 = 0
            r1.f1723i0 = r0     // Catch:{ all -> 0x00f1 }
        L_0x00d1:
            boolean r0 = com.bumptech.glide.p040q.SingleRequest.f1703s0     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x00ef
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f1 }
            r0.<init>()     // Catch:{ all -> 0x00f1 }
            java.lang.String r2 = "finished onSizeReady in "
            r0.append(r2)     // Catch:{ all -> 0x00f1 }
            long r2 = r1.f1724j0     // Catch:{ all -> 0x00f1 }
            double r2 = com.bumptech.glide.util.LogTime.m2814a(r2)     // Catch:{ all -> 0x00f1 }
            r0.append(r2)     // Catch:{ all -> 0x00f1 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00f1 }
            r1.m2689a(r0)     // Catch:{ all -> 0x00f1 }
        L_0x00ef:
            monitor-exit(r21)
            return
        L_0x00f1:
            r0 = move-exception
            goto L_0x00f9
        L_0x00f3:
            r0 = move-exception
            r1 = r21
            goto L_0x00f9
        L_0x00f7:
            r0 = move-exception
            r1 = r15
        L_0x00f9:
            monitor-exit(r21)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.SingleRequest.mo10656a(int, int):void");
    }

    /* renamed from: a */
    private static int m2682a(int i, float f) {
        return i == Integer.MIN_VALUE ? i : Math.round(f * ((float) i));
    }

    /* renamed from: a */
    public synchronized void mo10655a(Resource<?> vVar, DataSource aVar) {
        this.f1706R.mo10716a();
        this.f1723i0 = null;
        if (vVar == null) {
            mo10654a(new GlideException("Expected to receive a Resource<R> with an object of " + this.f1712X + " inside, but instead got null."));
            return;
        }
        Object obj = vVar.get();
        if (obj != null) {
            if (this.f1712X.isAssignableFrom(obj.getClass())) {
                if (!m2694j()) {
                    m2687a(vVar);
                    this.f1725k0 = C1113b.COMPLETE;
                    return;
                }
                m2688a(vVar, obj, aVar);
                return;
            }
        }
        m2687a(vVar);
        StringBuilder sb = new StringBuilder();
        sb.append("Expected to receive an object of ");
        sb.append(this.f1712X);
        sb.append(" but instead got ");
        sb.append(obj != null ? obj.getClass() : "");
        sb.append("{");
        sb.append(obj);
        sb.append("} inside Resource{");
        sb.append(vVar);
        sb.append("}.");
        sb.append(obj != null ? "" : " To indicate failure return a null Resource object, rather than a Resource object containing null data.");
        mo10654a(new GlideException(sb.toString()));
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00ad A[Catch:{ all -> 0x00bf }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m2688a(com.bumptech.glide.load.p023n.Resource<R> r11, R r12, com.bumptech.glide.load.DataSource r13) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r6 = r10.m2699o()     // Catch:{ all -> 0x00c3 }
            com.bumptech.glide.q.j$b r0 = com.bumptech.glide.p040q.SingleRequest.C1113b.COMPLETE     // Catch:{ all -> 0x00c3 }
            r10.f1725k0 = r0     // Catch:{ all -> 0x00c3 }
            r10.f1722h0 = r11     // Catch:{ all -> 0x00c3 }
            com.bumptech.glide.e r11 = r10.f1710V     // Catch:{ all -> 0x00c3 }
            int r11 = r11.mo9911e()     // Catch:{ all -> 0x00c3 }
            r0 = 3
            if (r11 > r0) goto L_0x006c
            java.lang.String r11 = "Glide"
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c3 }
            r0.<init>()     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = "Finished loading "
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.Class r1 = r12.getClass()     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = r1.getSimpleName()     // Catch:{ all -> 0x00c3 }
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = " from "
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            r0.append(r13)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = " for "
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.Object r1 = r10.f1711W     // Catch:{ all -> 0x00c3 }
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = " with size ["
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            int r1 = r10.f1729o0     // Catch:{ all -> 0x00c3 }
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = "x"
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            int r1 = r10.f1730p0     // Catch:{ all -> 0x00c3 }
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = "] in "
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            long r1 = r10.f1724j0     // Catch:{ all -> 0x00c3 }
            double r1 = com.bumptech.glide.util.LogTime.m2814a(r1)     // Catch:{ all -> 0x00c3 }
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r1 = " ms"
            r0.append(r1)     // Catch:{ all -> 0x00c3 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c3 }
            android.util.Log.d(r11, r0)     // Catch:{ all -> 0x00c3 }
        L_0x006c:
            r11 = 1
            r10.f1704P = r11     // Catch:{ all -> 0x00c3 }
            r7 = 0
            java.util.List<com.bumptech.glide.q.g<R>> r0 = r10.f1718d0     // Catch:{ all -> 0x00bf }
            if (r0 == 0) goto L_0x0094
            java.util.List<com.bumptech.glide.q.g<R>> r0 = r10.f1718d0     // Catch:{ all -> 0x00bf }
            java.util.Iterator r8 = r0.iterator()     // Catch:{ all -> 0x00bf }
            r9 = 0
        L_0x007b:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x00bf }
            if (r0 == 0) goto L_0x0095
            java.lang.Object r0 = r8.next()     // Catch:{ all -> 0x00bf }
            com.bumptech.glide.q.g r0 = (com.bumptech.glide.p040q.RequestListener) r0     // Catch:{ all -> 0x00bf }
            java.lang.Object r2 = r10.f1711W     // Catch:{ all -> 0x00bf }
            com.bumptech.glide.q.l.i<R> r3 = r10.f1717c0     // Catch:{ all -> 0x00bf }
            r1 = r12
            r4 = r13
            r5 = r6
            boolean r0 = r0.mo10643a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00bf }
            r9 = r9 | r0
            goto L_0x007b
        L_0x0094:
            r9 = 0
        L_0x0095:
            com.bumptech.glide.q.g<R> r0 = r10.f1707S     // Catch:{ all -> 0x00bf }
            if (r0 == 0) goto L_0x00a9
            com.bumptech.glide.q.g<R> r0 = r10.f1707S     // Catch:{ all -> 0x00bf }
            java.lang.Object r2 = r10.f1711W     // Catch:{ all -> 0x00bf }
            com.bumptech.glide.q.l.i<R> r3 = r10.f1717c0     // Catch:{ all -> 0x00bf }
            r1 = r12
            r4 = r13
            r5 = r6
            boolean r0 = r0.mo10643a(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00bf }
            if (r0 == 0) goto L_0x00a9
            goto L_0x00aa
        L_0x00a9:
            r11 = 0
        L_0x00aa:
            r11 = r11 | r9
            if (r11 != 0) goto L_0x00b8
            com.bumptech.glide.q.m.c<? super R> r11 = r10.f1720f0     // Catch:{ all -> 0x00bf }
            com.bumptech.glide.q.m.b r11 = r11.mo10670a(r13, r6)     // Catch:{ all -> 0x00bf }
            com.bumptech.glide.q.l.i<R> r13 = r10.f1717c0     // Catch:{ all -> 0x00bf }
            r13.mo10453a(r12, r11)     // Catch:{ all -> 0x00bf }
        L_0x00b8:
            r10.f1704P = r7     // Catch:{ all -> 0x00c3 }
            r10.m2701q()     // Catch:{ all -> 0x00c3 }
            monitor-exit(r10)
            return
        L_0x00bf:
            r11 = move-exception
            r10.f1704P = r7     // Catch:{ all -> 0x00c3 }
            throw r11     // Catch:{ all -> 0x00c3 }
        L_0x00c3:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.SingleRequest.m2688a(com.bumptech.glide.load.n.v, java.lang.Object, com.bumptech.glide.load.a):void");
    }

    /* renamed from: a */
    public synchronized void mo10654a(GlideException qVar) {
        m2686a(qVar, 5);
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    private synchronized void m2686a(GlideException qVar, int i) {
        boolean z;
        this.f1706R.mo10716a();
        qVar.mo10239a(this.f1731q0);
        int e = this.f1710V.mo9911e();
        if (e <= i) {
            Log.w("Glide", "Load failed for " + this.f1711W + " with size [" + this.f1729o0 + "x" + this.f1730p0 + "]", qVar);
            if (e <= 4) {
                qVar.mo10240a("Glide");
            }
        }
        this.f1723i0 = null;
        this.f1725k0 = C1113b.FAILED;
        boolean z2 = true;
        this.f1704P = true;
        try {
            if (this.f1718d0 != null) {
                z = false;
                for (RequestListener<R> gVar : this.f1718d0) {
                    z |= gVar.mo10642a(qVar, this.f1711W, this.f1717c0, m2699o());
                }
            } else {
                z = false;
            }
            if (this.f1707S == null || !this.f1707S.mo10642a(qVar, this.f1711W, this.f1717c0, m2699o())) {
                z2 = false;
            }
            if (!z && !z2) {
                m2702r();
            }
            this.f1704P = false;
            m2700p();
        } catch (Throwable th) {
            this.f1704P = false;
            throw th;
        }
    }

    /* renamed from: a */
    public synchronized boolean mo10624a(Request dVar) {
        boolean z = false;
        if (!(dVar instanceof SingleRequest)) {
            return false;
        }
        SingleRequest jVar = (SingleRequest) dVar;
        synchronized (jVar) {
            if (this.f1714Z == jVar.f1714Z && this.f1715a0 == jVar.f1715a0 && C1122j.m2846a(this.f1711W, jVar.f1711W) && this.f1712X.equals(jVar.f1712X) && this.f1713Y.equals(jVar.f1713Y) && this.f1716b0 == jVar.f1716b0 && m2690a((SingleRequest<?>) jVar)) {
                z = true;
            }
        }
        return z;
    }

    /* renamed from: a */
    private synchronized boolean m2690a(SingleRequest<?> jVar) {
        boolean z;
        synchronized (jVar) {
            z = false;
            if ((this.f1718d0 == null ? 0 : this.f1718d0.size()) == (jVar.f1718d0 == null ? 0 : jVar.f1718d0.size())) {
                z = true;
            }
        }
        return z;
    }

    /* renamed from: a */
    private void m2689a(String str) {
        Log.v("Request", str + " this: " + this.f1705Q);
    }
}
