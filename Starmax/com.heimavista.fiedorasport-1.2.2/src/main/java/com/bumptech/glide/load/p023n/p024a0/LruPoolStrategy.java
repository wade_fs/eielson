package com.bumptech.glide.load.p023n.p024a0;

import android.graphics.Bitmap;
import androidx.annotation.Nullable;

/* renamed from: com.bumptech.glide.load.n.a0.l */
interface LruPoolStrategy {
    @Nullable
    /* renamed from: a */
    Bitmap mo10044a();

    @Nullable
    /* renamed from: a */
    Bitmap mo10045a(int i, int i2, Bitmap.Config config);

    /* renamed from: a */
    void mo10046a(Bitmap bitmap);

    /* renamed from: b */
    int mo10047b(Bitmap bitmap);

    /* renamed from: b */
    String mo10048b(int i, int i2, Bitmap.Config config);

    /* renamed from: c */
    String mo10049c(Bitmap bitmap);
}
