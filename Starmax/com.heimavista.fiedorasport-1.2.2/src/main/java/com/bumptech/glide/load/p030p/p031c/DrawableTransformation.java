package com.bumptech.glide.load.p030p.p031c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.p.c.n */
public class DrawableTransformation implements Transformation<Drawable> {

    /* renamed from: b */
    private final Transformation<Bitmap> f1424b;

    /* renamed from: c */
    private final boolean f1425c;

    public DrawableTransformation(Transformation<Bitmap> lVar, boolean z) {
        this.f1424b = lVar;
        this.f1425c = z;
    }

    /* renamed from: a */
    public Transformation<BitmapDrawable> mo10362a() {
        return this;
    }

    @NonNull
    /* renamed from: a */
    public Resource<Drawable> mo9982a(@NonNull Context context, @NonNull Resource<Drawable> vVar, int i, int i2) {
        BitmapPool c = Glide.m1279b(context).mo9894c();
        Drawable drawable = vVar.get();
        Resource<Bitmap> a = DrawableToBitmapConverter.m2175a(c, drawable, i, i2);
        if (a != null) {
            Resource<Bitmap> a2 = this.f1424b.mo9982a(context, a, i, i2);
            if (!a2.equals(a)) {
                return m2178a(context, a2);
            }
            a2.recycle();
            return vVar;
        } else if (!this.f1425c) {
            return vVar;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof DrawableTransformation) {
            return this.f1424b.equals(((DrawableTransformation) obj).f1424b);
        }
        return false;
    }

    public int hashCode() {
        return this.f1424b.hashCode();
    }

    /* renamed from: a */
    private Resource<Drawable> m2178a(Context context, Resource<Bitmap> vVar) {
        return LazyBitmapDrawableResource.m2190a(context.getResources(), vVar);
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        this.f1424b.mo9966a(messageDigest);
    }
}
