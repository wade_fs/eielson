package com.bumptech.glide.p040q;

import androidx.annotation.Nullable;

/* renamed from: com.bumptech.glide.q.b */
public final class ErrorRequestCoordinator implements RequestCoordinator, Request {
    @Nullable

    /* renamed from: P */
    private final RequestCoordinator f1688P;

    /* renamed from: Q */
    private Request f1689Q;

    /* renamed from: R */
    private Request f1690R;

    public ErrorRequestCoordinator(@Nullable RequestCoordinator eVar) {
        this.f1688P = eVar;
    }

    /* renamed from: b */
    private boolean m2630b() {
        RequestCoordinator eVar = this.f1688P;
        return eVar == null || eVar.mo10633f(this);
    }

    /* renamed from: i */
    private boolean m2633i() {
        RequestCoordinator eVar = this.f1688P;
        return eVar == null || eVar.mo10629d(this);
    }

    /* renamed from: j */
    private boolean m2634j() {
        RequestCoordinator eVar = this.f1688P;
        return eVar != null && eVar.mo10623a();
    }

    /* renamed from: a */
    public void mo10622a(Request dVar, Request dVar2) {
        this.f1689Q = dVar;
        this.f1690R = dVar2;
    }

    /* renamed from: c */
    public void mo10626c() {
        if (!this.f1689Q.isRunning()) {
            this.f1689Q.mo10626c();
        }
    }

    public void clear() {
        this.f1689Q.clear();
        if (this.f1690R.isRunning()) {
            this.f1690R.clear();
        }
    }

    /* renamed from: d */
    public boolean mo10629d(Request dVar) {
        return m2633i() && m2632g(dVar);
    }

    /* renamed from: e */
    public boolean mo10631e() {
        return (this.f1689Q.mo10632f() ? this.f1690R : this.f1689Q).mo10631e();
    }

    /* renamed from: f */
    public boolean mo10632f() {
        return this.f1689Q.mo10632f() && this.f1690R.mo10632f();
    }

    /* renamed from: g */
    public boolean mo10634g() {
        return (this.f1689Q.mo10632f() ? this.f1690R : this.f1689Q).mo10634g();
    }

    /* renamed from: h */
    public boolean mo10635h() {
        return (this.f1689Q.mo10632f() ? this.f1690R : this.f1689Q).mo10635h();
    }

    public boolean isRunning() {
        return (this.f1689Q.mo10632f() ? this.f1690R : this.f1689Q).isRunning();
    }

    public void recycle() {
        this.f1689Q.recycle();
        this.f1690R.recycle();
    }

    /* renamed from: d */
    private boolean m2631d() {
        RequestCoordinator eVar = this.f1688P;
        return eVar == null || eVar.mo10627c(this);
    }

    /* renamed from: g */
    private boolean m2632g(Request dVar) {
        return dVar.equals(this.f1689Q) || (this.f1689Q.mo10632f() && dVar.equals(this.f1690R));
    }

    /* renamed from: b */
    public void mo10625b(Request dVar) {
        if (dVar.equals(this.f1690R)) {
            RequestCoordinator eVar = this.f1688P;
            if (eVar != null) {
                eVar.mo10625b(this);
            }
        } else if (!this.f1690R.isRunning()) {
            this.f1690R.mo10626c();
        }
    }

    /* renamed from: e */
    public void mo10630e(Request dVar) {
        RequestCoordinator eVar = this.f1688P;
        if (eVar != null) {
            eVar.mo10630e(this);
        }
    }

    /* renamed from: f */
    public boolean mo10633f(Request dVar) {
        return m2630b() && m2632g(dVar);
    }

    /* renamed from: a */
    public boolean mo10624a(Request dVar) {
        if (!(dVar instanceof ErrorRequestCoordinator)) {
            return false;
        }
        ErrorRequestCoordinator bVar = (ErrorRequestCoordinator) dVar;
        if (!this.f1689Q.mo10624a(bVar.f1689Q) || !this.f1690R.mo10624a(bVar.f1690R)) {
            return false;
        }
        return true;
    }

    /* renamed from: c */
    public boolean mo10627c(Request dVar) {
        return m2631d() && m2632g(dVar);
    }

    /* renamed from: a */
    public boolean mo10623a() {
        return m2634j() || mo10631e();
    }
}
