package com.bumptech.glide.load.p030p.p035g;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.p.g.g */
public class UnitTranscoder<Z> implements ResourceTranscoder<Z, Z> {

    /* renamed from: a */
    private static final UnitTranscoder<?> f1474a = new UnitTranscoder<>();

    /* renamed from: a */
    public static <Z> ResourceTranscoder<Z, Z> m2289a() {
        return f1474a;
    }

    @Nullable
    /* renamed from: a */
    public Resource<Z> mo10399a(@NonNull Resource<Z> vVar, @NonNull Options iVar) {
        return vVar;
    }
}
