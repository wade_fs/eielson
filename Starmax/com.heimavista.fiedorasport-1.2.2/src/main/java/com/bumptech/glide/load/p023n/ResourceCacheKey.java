package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.LruCache;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.n.x */
final class ResourceCacheKey implements Key {

    /* renamed from: j */
    private static final LruCache<Class<?>, byte[]> f1268j = new LruCache<>(50);

    /* renamed from: b */
    private final ArrayPool f1269b;

    /* renamed from: c */
    private final Key f1270c;

    /* renamed from: d */
    private final Key f1271d;

    /* renamed from: e */
    private final int f1272e;

    /* renamed from: f */
    private final int f1273f;

    /* renamed from: g */
    private final Class<?> f1274g;

    /* renamed from: h */
    private final Options f1275h;

    /* renamed from: i */
    private final Transformation<?> f1276i;

    ResourceCacheKey(ArrayPool bVar, Key gVar, Key gVar2, int i, int i2, Transformation<?> lVar, Class<?> cls, Options iVar) {
        this.f1269b = bVar;
        this.f1270c = gVar;
        this.f1271d = gVar2;
        this.f1272e = i;
        this.f1273f = i2;
        this.f1276i = lVar;
        this.f1274g = cls;
        this.f1275h = iVar;
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.f1269b.mo10039a(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.f1272e).putInt(this.f1273f).array();
        this.f1271d.mo9966a(messageDigest);
        this.f1270c.mo9966a(messageDigest);
        messageDigest.update(bArr);
        Transformation<?> lVar = this.f1276i;
        if (lVar != null) {
            lVar.mo9966a(messageDigest);
        }
        this.f1275h.mo9966a(messageDigest);
        messageDigest.update(m1858a());
        this.f1269b.put(bArr);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ResourceCacheKey)) {
            return false;
        }
        ResourceCacheKey xVar = (ResourceCacheKey) obj;
        if (this.f1273f != xVar.f1273f || this.f1272e != xVar.f1272e || !C1122j.m2850b(this.f1276i, xVar.f1276i) || !this.f1274g.equals(xVar.f1274g) || !this.f1270c.equals(xVar.f1270c) || !this.f1271d.equals(xVar.f1271d) || !this.f1275h.equals(xVar.f1275h)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = (((((this.f1270c.hashCode() * 31) + this.f1271d.hashCode()) * 31) + this.f1272e) * 31) + this.f1273f;
        Transformation<?> lVar = this.f1276i;
        if (lVar != null) {
            hashCode = (hashCode * 31) + lVar.hashCode();
        }
        return (((hashCode * 31) + this.f1274g.hashCode()) * 31) + this.f1275h.hashCode();
    }

    public String toString() {
        return "ResourceCacheKey{sourceKey=" + this.f1270c + ", signature=" + this.f1271d + ", width=" + this.f1272e + ", height=" + this.f1273f + ", decodedResourceClass=" + this.f1274g + ", transformation='" + this.f1276i + '\'' + ", options=" + this.f1275h + '}';
    }

    /* renamed from: a */
    private byte[] m1858a() {
        byte[] a = f1268j.mo10698a(this.f1274g);
        if (a != null) {
            return a;
        }
        byte[] bytes = this.f1274g.getName().getBytes(Key.f928a);
        f1268j.mo10702b(this.f1274g, bytes);
        return bytes;
    }
}
