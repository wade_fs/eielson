package com.bumptech.glide.load.p023n.p024a0;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.load.n.a0.e */
public interface BitmapPool {
    @NonNull
    /* renamed from: a */
    Bitmap mo10060a(int i, int i2, Bitmap.Config config);

    /* renamed from: a */
    void mo10061a();

    /* renamed from: a */
    void mo10062a(int i);

    /* renamed from: a */
    void mo10063a(Bitmap bitmap);

    @NonNull
    /* renamed from: b */
    Bitmap mo10064b(int i, int i2, Bitmap.Config config);
}
