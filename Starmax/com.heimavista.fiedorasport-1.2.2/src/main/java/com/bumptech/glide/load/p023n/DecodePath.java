package com.bumptech.glide.load.p023n;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.util.Pools;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.load.p030p.p035g.ResourceTranscoder;
import com.bumptech.glide.util.Preconditions;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.load.n.i */
public class DecodePath<DataType, ResourceType, Transcode> {

    /* renamed from: a */
    private final Class<DataType> f1160a;

    /* renamed from: b */
    private final List<? extends ResourceDecoder<DataType, ResourceType>> f1161b;

    /* renamed from: c */
    private final ResourceTranscoder<ResourceType, Transcode> f1162c;

    /* renamed from: d */
    private final Pools.Pool<List<Throwable>> f1163d;

    /* renamed from: e */
    private final String f1164e;

    /* renamed from: com.bumptech.glide.load.n.i$a */
    /* compiled from: DecodePath */
    interface C0986a<ResourceType> {
        @NonNull
        /* renamed from: a */
        Resource<ResourceType> mo10175a(@NonNull Resource<ResourceType> vVar);
    }

    public DecodePath(Class<DataType> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<? extends ResourceDecoder<DataType, ResourceType>> list, ResourceTranscoder<ResourceType, Transcode> eVar, Pools.Pool<List<Throwable>> pool) {
        this.f1160a = cls;
        this.f1161b = list;
        this.f1162c = eVar;
        this.f1163d = pool;
        this.f1164e = "Failed DecodePath{" + cls.getSimpleName() + "->" + cls2.getSimpleName() + "->" + cls3.getSimpleName() + "}";
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [com.bumptech.glide.load.m.e<DataType>, com.bumptech.glide.load.m.e] */
    /* JADX WARN: Type inference failed for: r5v0, types: [com.bumptech.glide.load.n.i$a, com.bumptech.glide.load.n.i$a<ResourceType>] */
    /* JADX WARNING: Unknown variable types count: 2 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.bumptech.glide.load.p023n.Resource<Transcode> mo10185a(com.bumptech.glide.load.p021m.DataRewinder<DataType> r1, int r2, int r3, @androidx.annotation.NonNull com.bumptech.glide.load.Options r4, com.bumptech.glide.load.p023n.DecodePath.C0986a<ResourceType> r5) {
        /*
            r0 = this;
            com.bumptech.glide.load.n.v r1 = r0.m1741a(r1, r2, r3, r4)
            com.bumptech.glide.load.n.v r1 = r5.mo10175a(r1)
            com.bumptech.glide.load.p.g.e<ResourceType, Transcode> r2 = r0.f1162c
            com.bumptech.glide.load.n.v r1 = r2.mo10399a(r1, r4)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.DecodePath.mo10185a(com.bumptech.glide.load.m.e, int, int, com.bumptech.glide.load.i, com.bumptech.glide.load.n.i$a):com.bumptech.glide.load.n.v");
    }

    public String toString() {
        return "DecodePath{ dataClass=" + this.f1160a + ", decoders=" + this.f1161b + ", transcoder=" + this.f1162c + '}';
    }

    @NonNull
    /* renamed from: a */
    private Resource<ResourceType> m1741a(DataRewinder<DataType> eVar, int i, int i2, @NonNull Options iVar) {
        List<Throwable> acquire = this.f1163d.acquire();
        Preconditions.m2828a((Object) acquire);
        List list = acquire;
        try {
            return m1742a(eVar, i, i2, iVar, list);
        } finally {
            this.f1163d.release(list);
        }
    }

    @NonNull
    /* renamed from: a */
    private Resource<ResourceType> m1742a(DataRewinder<DataType> eVar, int i, int i2, @NonNull Options iVar, List<Throwable> list) {
        int size = this.f1161b.size();
        Resource<ResourceType> vVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            ResourceDecoder jVar = (ResourceDecoder) this.f1161b.get(i3);
            try {
                if (jVar.mo9980a(eVar.mo10000a(), iVar)) {
                    vVar = jVar.mo9979a(eVar.mo10000a(), i, i2, iVar);
                }
            } catch (IOException | OutOfMemoryError | RuntimeException e) {
                if (Log.isLoggable("DecodePath", 2)) {
                    Log.v("DecodePath", "Failed to decode data for " + jVar, e);
                }
                list.add(e);
            }
            if (vVar != null) {
                break;
            }
        }
        if (vVar != null) {
            return vVar;
        }
        throw new GlideException(this.f1164e, new ArrayList(list));
    }
}
