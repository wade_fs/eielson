package com.bumptech.glide.load.p028o;

import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/* renamed from: com.bumptech.glide.load.o.d */
public class ByteBufferFileLoader implements ModelLoader<File, ByteBuffer> {

    /* renamed from: com.bumptech.glide.load.o.d$b */
    /* compiled from: ByteBufferFileLoader */
    public static class C1017b implements ModelLoaderFactory<File, ByteBuffer> {
        @NonNull
        /* renamed from: a */
        public ModelLoader<File, ByteBuffer> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new ByteBufferFileLoader();
        }
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull File file) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<ByteBuffer> mo10261a(@NonNull File file, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(file), new C1016a(file));
    }

    /* renamed from: com.bumptech.glide.load.o.d$a */
    /* compiled from: ByteBufferFileLoader */
    private static final class C1016a implements DataFetcher<ByteBuffer> {

        /* renamed from: P */
        private final File f1294P;

        C1016a(File file) {
            this.f1294P = file;
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super ByteBuffer> aVar) {
            try {
                aVar.mo9999a(ByteBufferUtil.m2802a(this.f1294P));
            } catch (IOException e) {
                if (Log.isLoggable("ByteBufferFileLoader", 3)) {
                    Log.d("ByteBufferFileLoader", "Failed to obtain ByteBuffer for file", e);
                }
                aVar.mo9998a((Exception) e);
            }
        }

        /* renamed from: b */
        public void mo9990b() {
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        @NonNull
        /* renamed from: a */
        public Class<ByteBuffer> mo9984a() {
            return ByteBuffer.class;
        }
    }
}
