package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.p.c.g */
public class CenterCrop extends BitmapTransformation {

    /* renamed from: b */
    private static final byte[] f1394b = "com.bumptech.glide.load.resource.bitmap.CenterCrop".getBytes(Key.f928a);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap mo10342a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        return TransformationUtils.m2212a(eVar, bitmap, i, i2);
    }

    public boolean equals(Object obj) {
        return obj instanceof CenterCrop;
    }

    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterCrop".hashCode();
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        messageDigest.update(f1394b);
    }
}
