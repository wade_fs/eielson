package com.bumptech.glide.p040q.p041l;

import android.graphics.Bitmap;
import android.widget.ImageView;

/* renamed from: com.bumptech.glide.q.l.b */
public class BitmapImageViewTarget extends ImageViewTarget<Bitmap> {
    public BitmapImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo10659a(Bitmap bitmap) {
        ((ImageView) this.f1751Q).setImageBitmap(bitmap);
    }
}
