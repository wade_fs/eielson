package com.bumptech.glide.p037n;

import android.graphics.Bitmap;
import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.p037n.GifDecoder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Iterator;

/* renamed from: com.bumptech.glide.n.e */
public class StandardGifDecoder implements GifDecoder {

    /* renamed from: u */
    private static final String f1623u = "e";
    @ColorInt

    /* renamed from: a */
    private int[] f1624a;
    @ColorInt

    /* renamed from: b */
    private final int[] f1625b;

    /* renamed from: c */
    private final GifDecoder.C1107a f1626c;

    /* renamed from: d */
    private ByteBuffer f1627d;

    /* renamed from: e */
    private byte[] f1628e;

    /* renamed from: f */
    private short[] f1629f;

    /* renamed from: g */
    private byte[] f1630g;

    /* renamed from: h */
    private byte[] f1631h;

    /* renamed from: i */
    private byte[] f1632i;
    @ColorInt

    /* renamed from: j */
    private int[] f1633j;

    /* renamed from: k */
    private int f1634k;

    /* renamed from: l */
    private GifHeader f1635l;

    /* renamed from: m */
    private Bitmap f1636m;

    /* renamed from: n */
    private boolean f1637n;

    /* renamed from: o */
    private int f1638o;

    /* renamed from: p */
    private int f1639p;

    /* renamed from: q */
    private int f1640q;

    /* renamed from: r */
    private int f1641r;
    @Nullable

    /* renamed from: s */
    private Boolean f1642s;
    @NonNull

    /* renamed from: t */
    private Bitmap.Config f1643t;

    public StandardGifDecoder(@NonNull GifDecoder.C1107a aVar, GifHeader cVar, ByteBuffer byteBuffer, int i) {
        this(aVar);
        mo10545a(cVar, byteBuffer, i);
    }

    /* renamed from: h */
    private Bitmap m2532h() {
        Boolean bool = this.f1642s;
        Bitmap a = this.f1626c.mo10432a(this.f1641r, this.f1640q, (bool == null || bool.booleanValue()) ? Bitmap.Config.ARGB_8888 : this.f1643t);
        a.setHasAlpha(true);
        return a;
    }

    /* renamed from: i */
    private int m2533i() {
        int j = m2534j();
        if (j <= 0) {
            return j;
        }
        ByteBuffer byteBuffer = this.f1627d;
        byteBuffer.get(this.f1628e, 0, Math.min(j, byteBuffer.remaining()));
        return j;
    }

    /* renamed from: j */
    private int m2534j() {
        return this.f1627d.get() & 255;
    }

    /* renamed from: a */
    public int mo10544a(int i) {
        if (i >= 0) {
            GifHeader cVar = this.f1635l;
            if (i < cVar.f1608c) {
                return cVar.f1610e.get(i).f1603i;
            }
        }
        return -1;
    }

    /* renamed from: b */
    public void mo10529b() {
        this.f1634k = (this.f1634k + 1) % this.f1635l.f1608c;
    }

    /* renamed from: c */
    public int mo10530c() {
        return this.f1635l.f1608c;
    }

    public void clear() {
        this.f1635l = null;
        byte[] bArr = this.f1632i;
        if (bArr != null) {
            this.f1626c.mo10434a(bArr);
        }
        int[] iArr = this.f1633j;
        if (iArr != null) {
            this.f1626c.mo10435a(iArr);
        }
        Bitmap bitmap = this.f1636m;
        if (bitmap != null) {
            this.f1626c.mo10433a(bitmap);
        }
        this.f1636m = null;
        this.f1627d = null;
        this.f1642s = null;
        byte[] bArr2 = this.f1628e;
        if (bArr2 != null) {
            this.f1626c.mo10434a(bArr2);
        }
    }

    /* renamed from: d */
    public int mo10532d() {
        int i;
        if (this.f1635l.f1608c <= 0 || (i = this.f1634k) < 0) {
            return 0;
        }
        return mo10544a(i);
    }

    /* renamed from: e */
    public void mo10533e() {
        this.f1634k = -1;
    }

    /* renamed from: f */
    public int mo10534f() {
        return this.f1634k;
    }

    /* renamed from: g */
    public int mo10535g() {
        return this.f1627d.limit() + this.f1632i.length + (this.f1633j.length * 4);
    }

    @NonNull
    public ByteBuffer getData() {
        return this.f1627d;
    }

    /* renamed from: b */
    private void m2530b(GifFrame bVar) {
        GifFrame bVar2 = bVar;
        int[] iArr = this.f1633j;
        int i = bVar2.f1598d;
        int i2 = bVar2.f1596b;
        int i3 = bVar2.f1597c;
        int i4 = bVar2.f1595a;
        boolean z = this.f1634k == 0;
        int i5 = this.f1641r;
        byte[] bArr = this.f1632i;
        int[] iArr2 = this.f1624a;
        int i6 = 0;
        byte b = -1;
        while (i6 < i) {
            int i7 = (i6 + i2) * i5;
            int i8 = i7 + i4;
            int i9 = i8 + i3;
            int i10 = i7 + i5;
            if (i10 < i9) {
                i9 = i10;
            }
            int i11 = bVar2.f1597c * i6;
            for (int i12 = i8; i12 < i9; i12++) {
                byte b2 = bArr[i11];
                byte b3 = b2 & 255;
                if (b3 != b) {
                    int i13 = iArr2[b3];
                    if (i13 != 0) {
                        iArr[i12] = i13;
                    } else {
                        b = b2;
                    }
                }
                i11++;
            }
            i6++;
            bVar2 = bVar;
        }
        this.f1642s = Boolean.valueOf(this.f1642s == null && z && b != -1);
    }

    /* JADX WARN: Type inference failed for: r17v2, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2531c(com.bumptech.glide.p037n.GifFrame r30) {
        /*
            r29 = this;
            r0 = r29
            r1 = r30
            if (r1 == 0) goto L_0x000d
            java.nio.ByteBuffer r2 = r0.f1627d
            int r3 = r1.f1604j
            r2.position(r3)
        L_0x000d:
            if (r1 != 0) goto L_0x0016
            com.bumptech.glide.n.c r1 = r0.f1635l
            int r2 = r1.f1611f
            int r1 = r1.f1612g
            goto L_0x001a
        L_0x0016:
            int r2 = r1.f1597c
            int r1 = r1.f1598d
        L_0x001a:
            int r2 = r2 * r1
            byte[] r1 = r0.f1632i
            if (r1 == 0) goto L_0x0023
            int r1 = r1.length
            if (r1 >= r2) goto L_0x002b
        L_0x0023:
            com.bumptech.glide.n.a$a r1 = r0.f1626c
            byte[] r1 = r1.mo10437b(r2)
            r0.f1632i = r1
        L_0x002b:
            byte[] r1 = r0.f1632i
            short[] r3 = r0.f1629f
            r4 = 4096(0x1000, float:5.74E-42)
            if (r3 != 0) goto L_0x0037
            short[] r3 = new short[r4]
            r0.f1629f = r3
        L_0x0037:
            short[] r3 = r0.f1629f
            byte[] r5 = r0.f1630g
            if (r5 != 0) goto L_0x0041
            byte[] r5 = new byte[r4]
            r0.f1630g = r5
        L_0x0041:
            byte[] r5 = r0.f1630g
            byte[] r6 = r0.f1631h
            if (r6 != 0) goto L_0x004d
            r6 = 4097(0x1001, float:5.741E-42)
            byte[] r6 = new byte[r6]
            r0.f1631h = r6
        L_0x004d:
            byte[] r6 = r0.f1631h
            int r7 = r29.m2534j()
            r8 = 1
            int r9 = r8 << r7
            int r10 = r9 + 1
            int r11 = r9 + 2
            int r7 = r7 + r8
            int r12 = r8 << r7
            int r12 = r12 - r8
            r13 = 0
            r14 = 0
        L_0x0060:
            if (r14 >= r9) goto L_0x006a
            r3[r14] = r13
            byte r15 = (byte) r14
            r5[r14] = r15
            int r14 = r14 + 1
            goto L_0x0060
        L_0x006a:
            byte[] r14 = r0.f1628e
            r15 = -1
            r26 = r7
            r24 = r11
            r25 = r12
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = -1
            r22 = 0
            r23 = 0
        L_0x0083:
            if (r13 >= r2) goto L_0x0159
            if (r16 != 0) goto L_0x0094
            int r16 = r29.m2533i()
            if (r16 > 0) goto L_0x0092
            r3 = 3
            r0.f1638o = r3
            goto L_0x0159
        L_0x0092:
            r19 = 0
        L_0x0094:
            byte r4 = r14[r19]
            r4 = r4 & 255(0xff, float:3.57E-43)
            int r4 = r4 << r17
            int r18 = r18 + r4
            int r17 = r17 + 8
            int r19 = r19 + 1
            int r16 = r16 + -1
            r4 = r17
            r8 = r21
            r28 = r22
            r27 = r24
            r21 = r20
            r20 = r13
            r13 = r26
        L_0x00b0:
            if (r4 < r13) goto L_0x013d
            r15 = r18 & r25
            int r18 = r18 >> r13
            int r4 = r4 - r13
            if (r15 != r9) goto L_0x00c1
            r13 = r7
            r27 = r11
            r25 = r12
            r8 = -1
        L_0x00bf:
            r15 = -1
            goto L_0x00b0
        L_0x00c1:
            if (r15 != r10) goto L_0x00d6
            r17 = r4
            r26 = r13
            r13 = r20
            r20 = r21
            r24 = r27
            r22 = r28
            r4 = 4096(0x1000, float:5.74E-42)
            r15 = -1
            r21 = r8
            r8 = 1
            goto L_0x0083
        L_0x00d6:
            r0 = -1
            if (r8 != r0) goto L_0x00e7
            byte r8 = r5[r15]
            r1[r21] = r8
            int r21 = r21 + 1
            int r20 = r20 + 1
            r0 = r29
            r8 = r15
            r28 = r8
            goto L_0x00bf
        L_0x00e7:
            r0 = r27
            r24 = r4
            if (r15 < r0) goto L_0x00f6
            r4 = r28
            byte r4 = (byte) r4
            r6[r23] = r4
            int r23 = r23 + 1
            r4 = r8
            goto L_0x00f7
        L_0x00f6:
            r4 = r15
        L_0x00f7:
            if (r4 < r9) goto L_0x0102
            byte r26 = r5[r4]
            r6[r23] = r26
            int r23 = r23 + 1
            short r4 = r3[r4]
            goto L_0x00f7
        L_0x0102:
            byte r4 = r5[r4]
            r4 = r4 & 255(0xff, float:3.57E-43)
            r26 = r7
            byte r7 = (byte) r4
            r1[r21] = r7
        L_0x010b:
            int r21 = r21 + 1
            int r20 = r20 + 1
            if (r23 <= 0) goto L_0x0118
            int r23 = r23 + -1
            byte r27 = r6[r23]
            r1[r21] = r27
            goto L_0x010b
        L_0x0118:
            r27 = r4
            r4 = 4096(0x1000, float:5.74E-42)
            if (r0 >= r4) goto L_0x012f
            short r8 = (short) r8
            r3[r0] = r8
            r5[r0] = r7
            int r0 = r0 + 1
            r7 = r0 & r25
            if (r7 != 0) goto L_0x012f
            if (r0 >= r4) goto L_0x012f
            int r13 = r13 + 1
            int r25 = r25 + r0
        L_0x012f:
            r8 = r15
            r4 = r24
            r7 = r26
            r28 = r27
            r15 = -1
            r27 = r0
            r0 = r29
            goto L_0x00b0
        L_0x013d:
            r24 = r4
            r0 = r27
            r15 = r28
            r26 = r13
            r22 = r15
            r13 = r20
            r20 = r21
            r17 = r24
            r4 = 4096(0x1000, float:5.74E-42)
            r15 = -1
            r24 = r0
            r21 = r8
            r8 = 1
            r0 = r29
            goto L_0x0083
        L_0x0159:
            r13 = r20
            r0 = 0
            java.util.Arrays.fill(r1, r13, r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p037n.StandardGifDecoder.m2531c(com.bumptech.glide.n.b):void");
    }

    public StandardGifDecoder(@NonNull GifDecoder.C1107a aVar) {
        this.f1625b = new int[256];
        this.f1643t = Bitmap.Config.ARGB_8888;
        this.f1626c = aVar;
        this.f1635l = new GifHeader();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00e9, code lost:
        return null;
     */
    @androidx.annotation.Nullable
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.graphics.Bitmap mo10527a() {
        /*
            r7 = this;
            monitor-enter(r7)
            com.bumptech.glide.n.c r0 = r7.f1635l     // Catch:{ all -> 0x00ea }
            int r0 = r0.f1608c     // Catch:{ all -> 0x00ea }
            r1 = 3
            r2 = 1
            if (r0 <= 0) goto L_0x000d
            int r0 = r7.f1634k     // Catch:{ all -> 0x00ea }
            if (r0 >= 0) goto L_0x003b
        L_0x000d:
            java.lang.String r0 = com.bumptech.glide.p037n.StandardGifDecoder.f1623u     // Catch:{ all -> 0x00ea }
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00ea }
            if (r0 == 0) goto L_0x0039
            java.lang.String r0 = com.bumptech.glide.p037n.StandardGifDecoder.f1623u     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            r3.<init>()     // Catch:{ all -> 0x00ea }
            java.lang.String r4 = "Unable to decode frame, frameCount="
            r3.append(r4)     // Catch:{ all -> 0x00ea }
            com.bumptech.glide.n.c r4 = r7.f1635l     // Catch:{ all -> 0x00ea }
            int r4 = r4.f1608c     // Catch:{ all -> 0x00ea }
            r3.append(r4)     // Catch:{ all -> 0x00ea }
            java.lang.String r4 = ", framePointer="
            r3.append(r4)     // Catch:{ all -> 0x00ea }
            int r4 = r7.f1634k     // Catch:{ all -> 0x00ea }
            r3.append(r4)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x00ea }
            android.util.Log.d(r0, r3)     // Catch:{ all -> 0x00ea }
        L_0x0039:
            r7.f1638o = r2     // Catch:{ all -> 0x00ea }
        L_0x003b:
            int r0 = r7.f1638o     // Catch:{ all -> 0x00ea }
            r3 = 0
            if (r0 == r2) goto L_0x00c8
            int r0 = r7.f1638o     // Catch:{ all -> 0x00ea }
            r4 = 2
            if (r0 != r4) goto L_0x0047
            goto L_0x00c8
        L_0x0047:
            r0 = 0
            r7.f1638o = r0     // Catch:{ all -> 0x00ea }
            byte[] r4 = r7.f1628e     // Catch:{ all -> 0x00ea }
            if (r4 != 0) goto L_0x0058
            com.bumptech.glide.n.a$a r4 = r7.f1626c     // Catch:{ all -> 0x00ea }
            r5 = 255(0xff, float:3.57E-43)
            byte[] r4 = r4.mo10437b(r5)     // Catch:{ all -> 0x00ea }
            r7.f1628e = r4     // Catch:{ all -> 0x00ea }
        L_0x0058:
            com.bumptech.glide.n.c r4 = r7.f1635l     // Catch:{ all -> 0x00ea }
            java.util.List<com.bumptech.glide.n.b> r4 = r4.f1610e     // Catch:{ all -> 0x00ea }
            int r5 = r7.f1634k     // Catch:{ all -> 0x00ea }
            java.lang.Object r4 = r4.get(r5)     // Catch:{ all -> 0x00ea }
            com.bumptech.glide.n.b r4 = (com.bumptech.glide.p037n.GifFrame) r4     // Catch:{ all -> 0x00ea }
            int r5 = r7.f1634k     // Catch:{ all -> 0x00ea }
            int r5 = r5 - r2
            if (r5 < 0) goto L_0x0074
            com.bumptech.glide.n.c r6 = r7.f1635l     // Catch:{ all -> 0x00ea }
            java.util.List<com.bumptech.glide.n.b> r6 = r6.f1610e     // Catch:{ all -> 0x00ea }
            java.lang.Object r5 = r6.get(r5)     // Catch:{ all -> 0x00ea }
            com.bumptech.glide.n.b r5 = (com.bumptech.glide.p037n.GifFrame) r5     // Catch:{ all -> 0x00ea }
            goto L_0x0075
        L_0x0074:
            r5 = r3
        L_0x0075:
            int[] r6 = r4.f1605k     // Catch:{ all -> 0x00ea }
            if (r6 == 0) goto L_0x007c
            int[] r6 = r4.f1605k     // Catch:{ all -> 0x00ea }
            goto L_0x0080
        L_0x007c:
            com.bumptech.glide.n.c r6 = r7.f1635l     // Catch:{ all -> 0x00ea }
            int[] r6 = r6.f1606a     // Catch:{ all -> 0x00ea }
        L_0x0080:
            r7.f1624a = r6     // Catch:{ all -> 0x00ea }
            int[] r6 = r7.f1624a     // Catch:{ all -> 0x00ea }
            if (r6 != 0) goto L_0x00aa
            java.lang.String r0 = com.bumptech.glide.p037n.StandardGifDecoder.f1623u     // Catch:{ all -> 0x00ea }
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00ea }
            if (r0 == 0) goto L_0x00a6
            java.lang.String r0 = com.bumptech.glide.p037n.StandardGifDecoder.f1623u     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            r1.<init>()     // Catch:{ all -> 0x00ea }
            java.lang.String r4 = "No valid color table found for frame #"
            r1.append(r4)     // Catch:{ all -> 0x00ea }
            int r4 = r7.f1634k     // Catch:{ all -> 0x00ea }
            r1.append(r4)     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ea }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00ea }
        L_0x00a6:
            r7.f1638o = r2     // Catch:{ all -> 0x00ea }
            monitor-exit(r7)
            return r3
        L_0x00aa:
            boolean r1 = r4.f1600f     // Catch:{ all -> 0x00ea }
            if (r1 == 0) goto L_0x00c2
            int[] r1 = r7.f1624a     // Catch:{ all -> 0x00ea }
            int[] r2 = r7.f1625b     // Catch:{ all -> 0x00ea }
            int[] r3 = r7.f1624a     // Catch:{ all -> 0x00ea }
            int r3 = r3.length     // Catch:{ all -> 0x00ea }
            java.lang.System.arraycopy(r1, r0, r2, r0, r3)     // Catch:{ all -> 0x00ea }
            int[] r1 = r7.f1625b     // Catch:{ all -> 0x00ea }
            r7.f1624a = r1     // Catch:{ all -> 0x00ea }
            int[] r1 = r7.f1624a     // Catch:{ all -> 0x00ea }
            int r2 = r4.f1602h     // Catch:{ all -> 0x00ea }
            r1[r2] = r0     // Catch:{ all -> 0x00ea }
        L_0x00c2:
            android.graphics.Bitmap r0 = r7.m2528a(r4, r5)     // Catch:{ all -> 0x00ea }
            monitor-exit(r7)
            return r0
        L_0x00c8:
            java.lang.String r0 = com.bumptech.glide.p037n.StandardGifDecoder.f1623u     // Catch:{ all -> 0x00ea }
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00ea }
            if (r0 == 0) goto L_0x00e8
            java.lang.String r0 = com.bumptech.glide.p037n.StandardGifDecoder.f1623u     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            r1.<init>()     // Catch:{ all -> 0x00ea }
            java.lang.String r2 = "Unable to decode frame, status="
            r1.append(r2)     // Catch:{ all -> 0x00ea }
            int r2 = r7.f1638o     // Catch:{ all -> 0x00ea }
            r1.append(r2)     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00ea }
            android.util.Log.d(r0, r1)     // Catch:{ all -> 0x00ea }
        L_0x00e8:
            monitor-exit(r7)
            return r3
        L_0x00ea:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p037n.StandardGifDecoder.mo10527a():android.graphics.Bitmap");
    }

    /* renamed from: a */
    public synchronized void mo10545a(@NonNull GifHeader cVar, @NonNull ByteBuffer byteBuffer, int i) {
        if (i > 0) {
            int highestOneBit = Integer.highestOneBit(i);
            this.f1638o = 0;
            this.f1635l = cVar;
            this.f1634k = -1;
            this.f1627d = byteBuffer.asReadOnlyBuffer();
            this.f1627d.position(0);
            this.f1627d.order(ByteOrder.LITTLE_ENDIAN);
            this.f1637n = false;
            Iterator<GifFrame> it = cVar.f1610e.iterator();
            while (true) {
                if (it.hasNext()) {
                    if (it.next().f1601g == 3) {
                        this.f1637n = true;
                        break;
                    }
                } else {
                    break;
                }
            }
            this.f1639p = highestOneBit;
            this.f1641r = cVar.f1611f / highestOneBit;
            this.f1640q = cVar.f1612g / highestOneBit;
            this.f1632i = this.f1626c.mo10437b(cVar.f1611f * cVar.f1612g);
            this.f1633j = this.f1626c.mo10436a(this.f1641r * this.f1640q);
        } else {
            throw new IllegalArgumentException("Sample size must be >=0, not: " + i);
        }
    }

    /* renamed from: a */
    public void mo10528a(@NonNull Bitmap.Config config) {
        if (config == Bitmap.Config.ARGB_8888 || config == Bitmap.Config.RGB_565) {
            this.f1643t = config;
            return;
        }
        throw new IllegalArgumentException("Unsupported format: " + config + ", must be one of " + Bitmap.Config.ARGB_8888 + " or " + Bitmap.Config.RGB_565);
    }

    /* renamed from: a */
    private Bitmap m2528a(GifFrame bVar, GifFrame bVar2) {
        int i;
        int i2;
        Bitmap bitmap;
        int[] iArr = this.f1633j;
        int i3 = 0;
        if (bVar2 == null) {
            Bitmap bitmap2 = this.f1636m;
            if (bitmap2 != null) {
                this.f1626c.mo10433a(bitmap2);
            }
            this.f1636m = null;
            Arrays.fill(iArr, 0);
        }
        if (bVar2 != null && bVar2.f1601g == 3 && this.f1636m == null) {
            Arrays.fill(iArr, 0);
        }
        if (bVar2 != null && (i2 = bVar2.f1601g) > 0) {
            if (i2 == 2) {
                if (!bVar.f1600f) {
                    GifHeader cVar = this.f1635l;
                    int i4 = cVar.f1617l;
                    if (bVar.f1605k == null || cVar.f1615j != bVar.f1602h) {
                        i3 = i4;
                    }
                } else if (this.f1634k == 0) {
                    this.f1642s = true;
                }
                int i5 = bVar2.f1598d;
                int i6 = this.f1639p;
                int i7 = i5 / i6;
                int i8 = bVar2.f1596b / i6;
                int i9 = bVar2.f1597c / i6;
                int i10 = bVar2.f1595a / i6;
                int i11 = this.f1641r;
                int i12 = (i8 * i11) + i10;
                int i13 = (i7 * i11) + i12;
                while (i12 < i13) {
                    int i14 = i12 + i9;
                    for (int i15 = i12; i15 < i14; i15++) {
                        iArr[i15] = i3;
                    }
                    i12 += this.f1641r;
                }
            } else if (i2 == 3 && (bitmap = this.f1636m) != null) {
                int i16 = this.f1641r;
                bitmap.getPixels(iArr, 0, i16, 0, 0, i16, this.f1640q);
            }
        }
        m2531c(bVar);
        if (bVar.f1599e || this.f1639p != 1) {
            m2529a(bVar);
        } else {
            m2530b(bVar);
        }
        if (this.f1637n && ((i = bVar.f1601g) == 0 || i == 1)) {
            if (this.f1636m == null) {
                this.f1636m = m2532h();
            }
            Bitmap bitmap3 = this.f1636m;
            int i17 = this.f1641r;
            bitmap3.setPixels(iArr, 0, i17, 0, 0, i17, this.f1640q);
        }
        Bitmap h = m2532h();
        int i18 = this.f1641r;
        h.setPixels(iArr, 0, i18, 0, 0, i18, this.f1640q);
        return h;
    }

    /* renamed from: a */
    private void m2529a(GifFrame bVar) {
        boolean z;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        GifFrame bVar2 = bVar;
        int[] iArr = this.f1633j;
        int i6 = bVar2.f1598d;
        int i7 = this.f1639p;
        int i8 = i6 / i7;
        int i9 = bVar2.f1596b / i7;
        int i10 = bVar2.f1597c / i7;
        int i11 = bVar2.f1595a / i7;
        boolean z2 = true;
        boolean z3 = this.f1634k == 0;
        int i12 = this.f1639p;
        int i13 = this.f1641r;
        int i14 = this.f1640q;
        byte[] bArr = this.f1632i;
        int[] iArr2 = this.f1624a;
        Boolean bool = this.f1642s;
        int i15 = 0;
        int i16 = 0;
        int i17 = 1;
        int i18 = 8;
        while (i15 < i8) {
            Boolean bool2 = z2;
            if (bVar2.f1599e) {
                if (i16 >= i8) {
                    i = i8;
                    i5 = i17 + 1;
                    if (i5 == 2) {
                        i16 = 4;
                    } else if (i5 == 3) {
                        i16 = 2;
                        i18 = 4;
                    } else if (i5 == 4) {
                        i16 = 1;
                        i18 = 2;
                    }
                } else {
                    i = i8;
                    i5 = i17;
                }
                i2 = i16 + i18;
                i17 = i5;
            } else {
                i = i8;
                i2 = i16;
                i16 = i15;
            }
            int i19 = i16 + i9;
            boolean z4 = i12 == 1;
            if (i19 < i14) {
                int i20 = i19 * i13;
                int i21 = i20 + i11;
                int i22 = i21 + i10;
                int i23 = i20 + i13;
                if (i23 < i22) {
                    i22 = i23;
                }
                i3 = i9;
                int i24 = i15 * i12 * bVar2.f1597c;
                if (z4) {
                    int i25 = i21;
                    while (i25 < i22) {
                        int i26 = i10;
                        int i27 = iArr2[bArr[i24] & 255];
                        if (i27 != 0) {
                            iArr[i25] = i27;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i24 += i12;
                        i25++;
                        i10 = i26;
                    }
                } else {
                    i4 = i10;
                    int i28 = ((i22 - i21) * i12) + i24;
                    int i29 = i21;
                    while (i29 < i22) {
                        int i30 = i22;
                        int a = m2527a(i24, i28, bVar2.f1597c);
                        if (a != 0) {
                            iArr[i29] = a;
                        } else if (z3 && bool == null) {
                            bool = bool2;
                        }
                        i24 += i12;
                        i29++;
                        i22 = i30;
                    }
                    i15++;
                    i16 = i2;
                    i10 = i4;
                    z2 = bool2;
                    i8 = i;
                    i9 = i3;
                }
            } else {
                i3 = i9;
            }
            i4 = i10;
            i15++;
            i16 = i2;
            i10 = i4;
            z2 = bool2;
            i8 = i;
            i9 = i3;
        }
        if (this.f1642s == null) {
            if (bool == null) {
                z = false;
            } else {
                z = bool.booleanValue();
            }
            this.f1642s = Boolean.valueOf(z);
        }
    }

    @ColorInt
    /* renamed from: a */
    private int m2527a(int i, int i2, int i3) {
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        int i8 = 0;
        for (int i9 = i; i9 < this.f1639p + i; i9++) {
            byte[] bArr = this.f1632i;
            if (i9 >= bArr.length || i9 >= i2) {
                break;
            }
            int i10 = this.f1624a[bArr[i9] & 255];
            if (i10 != 0) {
                i4 += (i10 >> 24) & 255;
                i5 += (i10 >> 16) & 255;
                i6 += (i10 >> 8) & 255;
                i7 += i10 & 255;
                i8++;
            }
        }
        int i11 = i + i3;
        for (int i12 = i11; i12 < this.f1639p + i11; i12++) {
            byte[] bArr2 = this.f1632i;
            if (i12 >= bArr2.length || i12 >= i2) {
                break;
            }
            int i13 = this.f1624a[bArr2[i12] & 255];
            if (i13 != 0) {
                i4 += (i13 >> 24) & 255;
                i5 += (i13 >> 16) & 255;
                i6 += (i13 >> 8) & 255;
                i7 += i13 & 255;
                i8++;
            }
        }
        if (i8 == 0) {
            return 0;
        }
        return ((i4 / i8) << 24) | ((i5 / i8) << 16) | ((i6 / i8) << 8) | (i7 / i8);
    }
}
