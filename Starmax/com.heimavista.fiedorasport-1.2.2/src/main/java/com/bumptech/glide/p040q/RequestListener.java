package com.bumptech.glide.p040q;

import androidx.annotation.Nullable;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p023n.GlideException;
import com.bumptech.glide.p040q.p041l.Target;

/* renamed from: com.bumptech.glide.q.g */
public interface RequestListener<R> {
    /* renamed from: a */
    boolean mo10642a(@Nullable GlideException qVar, Object obj, Target<R> iVar, boolean z);

    /* renamed from: a */
    boolean mo10643a(R r, Object obj, Target<R> iVar, DataSource aVar, boolean z);
}
