package com.bumptech.glide.load.p030p.p031c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.Initializable;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.c.r */
public final class LazyBitmapDrawableResource implements Resource<BitmapDrawable>, Initializable {

    /* renamed from: P */
    private final Resources f1431P;

    /* renamed from: Q */
    private final Resource<Bitmap> f1432Q;

    private LazyBitmapDrawableResource(@NonNull Resources resources, @NonNull Resource<Bitmap> vVar) {
        Preconditions.m2828a(resources);
        this.f1431P = resources;
        Preconditions.m2828a(vVar);
        this.f1432Q = vVar;
    }

    @Nullable
    /* renamed from: a */
    public static Resource<BitmapDrawable> m2190a(@NonNull Resources resources, @Nullable Resource<Bitmap> vVar) {
        if (vVar == null) {
            return null;
        }
        return new LazyBitmapDrawableResource(resources, vVar);
    }

    @NonNull
    /* renamed from: b */
    public Class<BitmapDrawable> mo10228b() {
        return BitmapDrawable.class;
    }

    /* renamed from: c */
    public void mo10250c() {
        Resource<Bitmap> vVar = this.f1432Q;
        if (vVar instanceof Initializable) {
            ((Initializable) vVar).mo10250c();
        }
    }

    public void recycle() {
        this.f1432Q.recycle();
    }

    /* renamed from: a */
    public int mo10226a() {
        return this.f1432Q.mo10226a();
    }

    @NonNull
    public BitmapDrawable get() {
        return new BitmapDrawable(this.f1431P, this.f1432Q.get());
    }
}
