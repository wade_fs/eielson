package com.bumptech.glide.p040q.p041l;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.p040q.p042m.Transition;

/* renamed from: com.bumptech.glide.q.l.e */
public abstract class ImageViewTarget<Z> extends ViewTarget<ImageView, Z> implements Transition.C1117a {
    @Nullable

    /* renamed from: W */
    private Animatable f1747W;

    public ImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    /* renamed from: a */
    public void mo10639a(@Nullable Drawable drawable) {
        super.mo10639a(drawable);
        m2752c((Object) null);
        mo10661d(drawable);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo10659a(@Nullable Object obj);

    /* renamed from: b */
    public void mo10644b(@Nullable Drawable drawable) {
        super.mo10644b(drawable);
        m2752c((Object) null);
        mo10661d(drawable);
    }

    /* renamed from: c */
    public void mo10646c(@Nullable Drawable drawable) {
        super.mo10646c(drawable);
        Animatable animatable = this.f1747W;
        if (animatable != null) {
            animatable.stop();
        }
        m2752c((Object) null);
        mo10661d(drawable);
    }

    /* renamed from: d */
    public void mo10661d(Drawable drawable) {
        ((ImageView) super.f1751Q).setImageDrawable(drawable);
    }

    public void onStart() {
        Animatable animatable = this.f1747W;
        if (animatable != null) {
            animatable.start();
        }
    }

    /* renamed from: b */
    private void m2751b(@Nullable Z z) {
        if (z instanceof Animatable) {
            this.f1747W = (Animatable) z;
            this.f1747W.start();
            return;
        }
        this.f1747W = null;
    }

    /* renamed from: a */
    public void mo10453a(@NonNull Z z, @Nullable Transition<? super Z> bVar) {
        if (bVar == null || !bVar.mo10669a(z, this)) {
            m2752c((Object) z);
        } else {
            m2751b((Object) z);
        }
    }

    /* renamed from: c */
    public void mo9949c() {
        Animatable animatable = this.f1747W;
        if (animatable != null) {
            animatable.stop();
        }
    }

    /* renamed from: c */
    private void m2752c(@Nullable Z z) {
        mo10659a((Object) z);
        m2751b((Object) z);
    }
}
