package com.bumptech.glide;

import android.content.Context;
import android.content.ContextWrapper;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.p023n.Engine;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.p040q.RequestListener;
import com.bumptech.glide.p040q.RequestOptions;
import com.bumptech.glide.p040q.p041l.ImageViewTargetFactory;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.e */
public class GlideContext extends ContextWrapper {
    @VisibleForTesting

    /* renamed from: j */
    static final TransitionOptions<?, ?> f853j = new GenericTransitionOptions();

    /* renamed from: a */
    private final ArrayPool f854a;

    /* renamed from: b */
    private final Registry f855b;

    /* renamed from: c */
    private final ImageViewTargetFactory f856c;

    /* renamed from: d */
    private final RequestOptions f857d;

    /* renamed from: e */
    private final List<RequestListener<Object>> f858e;

    /* renamed from: f */
    private final Map<Class<?>, TransitionOptions<?, ?>> f859f;

    /* renamed from: g */
    private final Engine f860g;

    /* renamed from: h */
    private final boolean f861h;

    /* renamed from: i */
    private final int f862i;

    public GlideContext(@NonNull Context context, @NonNull ArrayPool bVar, @NonNull Registry iVar, @NonNull ImageViewTargetFactory fVar, @NonNull RequestOptions hVar, @NonNull Map<Class<?>, TransitionOptions<?, ?>> map, @NonNull List<RequestListener<Object>> list, @NonNull Engine kVar, boolean z, int i) {
        super(context.getApplicationContext());
        this.f854a = bVar;
        this.f855b = iVar;
        this.f856c = fVar;
        this.f857d = hVar;
        this.f858e = list;
        this.f859f = map;
        this.f860g = kVar;
        this.f861h = z;
        this.f862i = i;
    }

    @NonNull
    /* renamed from: a */
    public <T> TransitionOptions<?, T> mo9905a(@NonNull Class<T> cls) {
        TransitionOptions<?, T> lVar = this.f859f.get(cls);
        if (lVar == null) {
            for (Map.Entry entry : this.f859f.entrySet()) {
                if (((Class) entry.getKey()).isAssignableFrom(cls)) {
                    lVar = (TransitionOptions) entry.getValue();
                }
            }
        }
        return lVar == null ? f853j : lVar;
    }

    /* renamed from: b */
    public List<RequestListener<Object>> mo9908b() {
        return this.f858e;
    }

    /* renamed from: c */
    public RequestOptions mo9909c() {
        return this.f857d;
    }

    @NonNull
    /* renamed from: d */
    public Engine mo9910d() {
        return this.f860g;
    }

    /* renamed from: e */
    public int mo9911e() {
        return this.f862i;
    }

    @NonNull
    /* renamed from: f */
    public Registry mo9912f() {
        return this.f855b;
    }

    /* renamed from: g */
    public boolean mo9913g() {
        return this.f861h;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [java.lang.Class<X>, java.lang.Class] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @androidx.annotation.NonNull
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <X> com.bumptech.glide.p040q.p041l.ViewTarget<android.widget.ImageView, X> mo9907a(@androidx.annotation.NonNull android.widget.ImageView r2, @androidx.annotation.NonNull java.lang.Class<X> r3) {
        /*
            r1 = this;
            com.bumptech.glide.q.l.f r0 = r1.f856c
            com.bumptech.glide.q.l.j r2 = r0.mo10662a(r2, r3)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.GlideContext.mo9907a(android.widget.ImageView, java.lang.Class):com.bumptech.glide.q.l.j");
    }

    @NonNull
    /* renamed from: a */
    public ArrayPool mo9906a() {
        return this.f854a;
    }
}
