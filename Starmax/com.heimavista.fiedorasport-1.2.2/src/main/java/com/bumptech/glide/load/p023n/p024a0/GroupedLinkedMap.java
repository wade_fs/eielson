package com.bumptech.glide.load.p023n.p024a0;

import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.p024a0.LruArrayPool;
import com.bumptech.glide.load.p023n.p024a0.Poolable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.a0.h */
class GroupedLinkedMap<K extends Poolable, V> {

    /* renamed from: a */
    private final C0950a<K, V> f994a = new C0950a<>();

    /* renamed from: b */
    private final Map<K, C0950a<K, V>> f995b = new HashMap();

    /* renamed from: com.bumptech.glide.load.n.a0.h$a */
    /* compiled from: GroupedLinkedMap */
    private static class C0950a<K, V> {

        /* renamed from: a */
        final K f996a;

        /* renamed from: b */
        private List<V> f997b;

        /* renamed from: c */
        C0950a<K, V> f998c;

        /* renamed from: d */
        C0950a<K, V> f999d;

        C0950a() {
            this(null);
        }

        @Nullable
        /* renamed from: a */
        public V mo10070a() {
            int b = mo10072b();
            if (b > 0) {
                return this.f997b.remove(b - 1);
            }
            return null;
        }

        /* renamed from: b */
        public int mo10072b() {
            List<V> list = this.f997b;
            if (list != null) {
                return list.size();
            }
            return 0;
        }

        C0950a(K k) {
            this.f999d = this;
            this.f998c = this;
            this.f996a = k;
        }

        /* renamed from: a */
        public void mo10071a(V v) {
            if (this.f997b == null) {
                this.f997b = new ArrayList();
            }
            this.f997b.add(v);
        }
    }

    GroupedLinkedMap() {
    }

    /* renamed from: b */
    private void m1528b(C0950a<K, V> aVar) {
        m1529c(aVar);
        C0950a<K, V> aVar2 = this.f994a;
        aVar.f999d = aVar2.f999d;
        aVar.f998c = aVar2;
        m1530d(aVar);
    }

    /* renamed from: c */
    private static <K, V> void m1529c(C0950a<K, V> aVar) {
        C0950a<K, V> aVar2 = aVar.f999d;
        aVar2.f998c = aVar.f998c;
        aVar.f998c.f999d = aVar2;
    }

    /* renamed from: d */
    private static <K, V> void m1530d(C0950a<K, V> aVar) {
        aVar.f998c.f999d = aVar;
        aVar.f999d.f998c = aVar;
    }

    /* renamed from: a */
    public void mo10068a(K k, V v) {
        C0950a aVar = this.f995b.get(k);
        if (aVar == null) {
            aVar = new C0950a(k);
            m1528b(aVar);
            this.f995b.put(k, aVar);
        } else {
            k.mo10051a();
        }
        aVar.mo10071a(v);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("GroupedLinkedMap( ");
        boolean z = false;
        for (C0950a<K, V> aVar = this.f994a.f998c; !aVar.equals(this.f994a); aVar = aVar.f998c) {
            z = true;
            sb.append('{');
            sb.append((Object) aVar.f996a);
            sb.append(':');
            sb.append(aVar.mo10072b());
            sb.append("}, ");
        }
        if (z) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append(" )");
        return sb.toString();
    }

    @Nullable
    /* renamed from: a */
    public V mo10067a(LruArrayPool.C0951a aVar) {
        C0950a aVar2 = this.f995b.get(aVar);
        if (aVar2 == null) {
            aVar2 = new C0950a(aVar);
            this.f995b.put(aVar, aVar2);
        } else {
            aVar.mo10051a();
        }
        m1527a(aVar2);
        return aVar2.mo10070a();
    }

    @Nullable
    /* renamed from: a */
    public V mo10066a() {
        for (C0950a<K, V> aVar = this.f994a.f999d; !aVar.equals(this.f994a); aVar = aVar.f999d) {
            V a = aVar.mo10070a();
            if (a != null) {
                return a;
            }
            m1529c(aVar);
            this.f995b.remove(aVar.f996a);
            ((Poolable) aVar.f996a).mo10051a();
        }
        return null;
    }

    /* renamed from: a */
    private void m1527a(C0950a<LruArrayPool.C0951a, Object> aVar) {
        m1529c(aVar);
        C0950a<K, V> aVar2 = this.f994a;
        aVar.f999d = aVar2;
        aVar.f998c = aVar2.f998c;
        m1530d(aVar);
    }
}
