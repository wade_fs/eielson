package com.bumptech.glide.load.p028o.p029y;

import android.net.Uri;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p028o.GlideUrl;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p028o.ModelLoaderFactory;
import com.bumptech.glide.load.p028o.MultiModelLoaderFactory;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/* renamed from: com.bumptech.glide.load.o.y.b */
public class HttpUriLoader implements ModelLoader<Uri, InputStream> {

    /* renamed from: b */
    private static final Set<String> f1375b = Collections.unmodifiableSet(new HashSet(Arrays.asList("http", "https")));

    /* renamed from: a */
    private final ModelLoader<GlideUrl, InputStream> f1376a;

    /* renamed from: com.bumptech.glide.load.o.y.b$a */
    /* compiled from: HttpUriLoader */
    public static class C1058a implements ModelLoaderFactory<Uri, InputStream> {
        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new HttpUriLoader(rVar.mo10313a(GlideUrl.class, InputStream.class));
        }
    }

    public HttpUriLoader(ModelLoader<GlideUrl, InputStream> nVar) {
        this.f1376a = nVar;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<InputStream> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        return this.f1376a.mo10261a(new GlideUrl(uri.toString()), i, i2, iVar);
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        return f1375b.contains(uri.getScheme());
    }
}
