package com.bumptech.glide.load.p030p.p032d;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.d.b */
public class BytesResource implements Resource<byte[]> {

    /* renamed from: P */
    private final byte[] f1460P;

    public BytesResource(byte[] bArr) {
        Preconditions.m2828a(bArr);
        this.f1460P = bArr;
    }

    /* renamed from: a */
    public int mo10226a() {
        return this.f1460P.length;
    }

    @NonNull
    /* renamed from: b */
    public Class<byte[]> mo10228b() {
        return byte[].class;
    }

    public void recycle() {
    }

    @NonNull
    public byte[] get() {
        return this.f1460P;
    }
}
