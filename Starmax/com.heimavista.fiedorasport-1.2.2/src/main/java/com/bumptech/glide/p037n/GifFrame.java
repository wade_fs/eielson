package com.bumptech.glide.p037n;

import androidx.annotation.ColorInt;

/* renamed from: com.bumptech.glide.n.b */
class GifFrame {

    /* renamed from: a */
    int f1595a;

    /* renamed from: b */
    int f1596b;

    /* renamed from: c */
    int f1597c;

    /* renamed from: d */
    int f1598d;

    /* renamed from: e */
    boolean f1599e;

    /* renamed from: f */
    boolean f1600f;

    /* renamed from: g */
    int f1601g;

    /* renamed from: h */
    int f1602h;

    /* renamed from: i */
    int f1603i;

    /* renamed from: j */
    int f1604j;
    @ColorInt

    /* renamed from: k */
    int[] f1605k;

    GifFrame() {
    }
}
