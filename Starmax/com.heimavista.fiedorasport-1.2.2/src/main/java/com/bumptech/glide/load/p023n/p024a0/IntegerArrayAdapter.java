package com.bumptech.glide.load.p023n.p024a0;

/* renamed from: com.bumptech.glide.load.n.a0.i */
public final class IntegerArrayAdapter implements ArrayAdapterInterface<int[]> {
    /* renamed from: a */
    public int mo10035a() {
        return 4;
    }

    /* renamed from: b */
    public String mo10037b() {
        return "IntegerArrayPool";
    }

    /* renamed from: a */
    public int mo10036a(int[] iArr) {
        return iArr.length;
    }

    public int[] newArray(int i) {
        return new int[i];
    }
}
