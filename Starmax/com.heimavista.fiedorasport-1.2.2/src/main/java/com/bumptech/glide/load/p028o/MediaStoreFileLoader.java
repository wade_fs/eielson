package com.bumptech.glide.load.p028o;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p021m.p022o.MediaStoreUtil;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.File;
import java.io.FileNotFoundException;

/* renamed from: com.bumptech.glide.load.o.k */
public final class MediaStoreFileLoader implements ModelLoader<Uri, File> {

    /* renamed from: a */
    private final Context f1319a;

    /* renamed from: com.bumptech.glide.load.o.k$a */
    /* compiled from: MediaStoreFileLoader */
    public static final class C1032a implements ModelLoaderFactory<Uri, File> {

        /* renamed from: a */
        private final Context f1320a;

        public C1032a(Context context) {
            this.f1320a = context;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, File> mo10265a(MultiModelLoaderFactory rVar) {
            return new MediaStoreFileLoader(this.f1320a);
        }
    }

    public MediaStoreFileLoader(Context context) {
        this.f1319a = context;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<File> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(uri), new C1033b(this.f1319a, uri));
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        return MediaStoreUtil.m1464b(uri);
    }

    /* renamed from: com.bumptech.glide.load.o.k$b */
    /* compiled from: MediaStoreFileLoader */
    private static class C1033b implements DataFetcher<File> {

        /* renamed from: R */
        private static final String[] f1321R = {"_data"};

        /* renamed from: P */
        private final Context f1322P;

        /* renamed from: Q */
        private final Uri f1323Q;

        C1033b(Context context, Uri uri) {
            this.f1322P = context;
            this.f1323Q = uri;
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super File> aVar) {
            Cursor query = this.f1322P.getContentResolver().query(this.f1323Q, f1321R, null, null, null);
            String str = null;
            if (query != null) {
                try {
                    if (query.moveToFirst()) {
                        str = query.getString(query.getColumnIndexOrThrow("_data"));
                    }
                } finally {
                    query.close();
                }
            }
            if (TextUtils.isEmpty(str)) {
                aVar.mo9998a((Exception) new FileNotFoundException("Failed to find file path for: " + this.f1323Q));
                return;
            }
            aVar.mo9999a(new File(str));
        }

        /* renamed from: b */
        public void mo9990b() {
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        @NonNull
        /* renamed from: a */
        public Class<File> mo9984a() {
            return File.class;
        }
    }
}
