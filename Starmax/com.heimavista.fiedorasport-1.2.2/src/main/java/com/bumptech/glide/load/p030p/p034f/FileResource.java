package com.bumptech.glide.load.p030p.p034f;

import com.bumptech.glide.load.p030p.SimpleResource;
import java.io.File;

/* renamed from: com.bumptech.glide.load.p.f.b */
public class FileResource extends SimpleResource<File> {
    public FileResource(File file) {
        super(file);
    }
}
