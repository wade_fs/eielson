package com.bumptech.glide.load.p023n.p024a0;

/* renamed from: com.bumptech.glide.load.n.a0.a */
interface ArrayAdapterInterface<T> {
    /* renamed from: a */
    int mo10035a();

    /* renamed from: a */
    int mo10036a(T t);

    /* renamed from: b */
    String mo10037b();

    T newArray(int i);
}
