package com.bumptech.glide.load.p023n.p025b0;

import android.annotation.SuppressLint;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p025b0.MemoryCache;
import com.bumptech.glide.util.LruCache;

/* renamed from: com.bumptech.glide.load.n.b0.g */
public class LruResourceCache extends LruCache<Key, Resource<?>> implements MemoryCache {

    /* renamed from: d */
    private MemoryCache.C0964a f1045d;

    public LruResourceCache(long j) {
        super(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public /* bridge */ /* synthetic */ void mo10102a(@NonNull Object obj, @Nullable Object obj2) {
        mo10104b((Key) obj, (Resource<?>) ((Resource) obj2));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public /* bridge */ /* synthetic */ int mo10103b(@Nullable Object obj) {
        return mo10097a((Resource<?>) ((Resource) obj));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.util.f.b(java.lang.Object, java.lang.Object):Y
     arg types: [com.bumptech.glide.load.g, com.bumptech.glide.load.n.v]
     candidates:
      com.bumptech.glide.load.n.b0.g.b(com.bumptech.glide.load.g, com.bumptech.glide.load.n.v<?>):void
      com.bumptech.glide.util.f.b(java.lang.Object, java.lang.Object):Y */
    @Nullable
    /* renamed from: a */
    public /* bridge */ /* synthetic */ Resource mo10099a(@NonNull Key gVar, @Nullable Resource vVar) {
        return (Resource) super.mo10702b((Object) gVar, (Object) vVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo10104b(@NonNull Key gVar, @Nullable Resource<?> vVar) {
        MemoryCache.C0964a aVar = this.f1045d;
        if (aVar != null && vVar != null) {
            aVar.mo10106a(vVar);
        }
    }

    @Nullable
    /* renamed from: a */
    public /* bridge */ /* synthetic */ Resource mo10098a(@NonNull Key gVar) {
        return (Resource) super.mo10703c(gVar);
    }

    /* renamed from: a */
    public void mo10101a(@NonNull MemoryCache.C0964a aVar) {
        this.f1045d = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo10097a(@Nullable Resource<?> vVar) {
        if (vVar == null) {
            return super.mo10103b(null);
        }
        return vVar.mo10226a();
    }

    @SuppressLint({"InlinedApi"})
    /* renamed from: a */
    public void mo10100a(int i) {
        if (i >= 40) {
            mo10699a();
        } else if (i >= 20 || i == 15) {
            mo10700a(mo10701b() / 2);
        }
    }
}
