package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;

/* renamed from: com.bumptech.glide.load.o.v */
public class UnitModelLoader<Model> implements ModelLoader<Model, Model> {

    /* renamed from: a */
    private static final UnitModelLoader<?> f1362a = new UnitModelLoader<>();

    /* renamed from: com.bumptech.glide.load.o.v$a */
    /* compiled from: UnitModelLoader */
    public static class C1050a<Model> implements ModelLoaderFactory<Model, Model> {

        /* renamed from: a */
        private static final C1050a<?> f1363a = new C1050a<>();

        /* renamed from: a */
        public static <T> C1050a<T> m2029a() {
            return f1363a;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Model, Model> mo10265a(MultiModelLoaderFactory rVar) {
            return UnitModelLoader.m2026a();
        }
    }

    /* renamed from: com.bumptech.glide.load.o.v$b */
    /* compiled from: UnitModelLoader */
    private static class C1051b<Model> implements DataFetcher<Model> {

        /* renamed from: P */
        private final Model f1364P;

        C1051b(Model model) {
            this.f1364P = model;
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super Model> aVar) {
            aVar.mo9999a((Object) this.f1364P);
        }

        /* renamed from: b */
        public void mo9990b() {
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        @NonNull
        /* renamed from: a */
        public Class<Model> mo9984a() {
            return this.f1364P.getClass();
        }
    }

    /* renamed from: a */
    public static <T> UnitModelLoader<T> m2026a() {
        return f1362a;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Model model) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Model> mo10261a(@NonNull Model model, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(model), new C1051b(model));
    }
}
