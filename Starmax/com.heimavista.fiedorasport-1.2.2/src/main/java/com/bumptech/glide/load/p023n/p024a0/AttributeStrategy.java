package com.bumptech.glide.load.p023n.p024a0;

import android.graphics.Bitmap;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.p023n.p024a0.LruArrayPool;
import com.bumptech.glide.util.C1122j;

/* renamed from: com.bumptech.glide.load.n.a0.c */
class AttributeStrategy implements LruPoolStrategy {

    /* renamed from: a */
    private final C0949b f987a = new C0949b();

    /* renamed from: b */
    private final GroupedLinkedMap<C0948a, Bitmap> f988b = new GroupedLinkedMap<>();

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.a0.c$b */
    /* compiled from: AttributeStrategy */
    static class C0949b extends BaseKeyPool<C0948a> {
        C0949b() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C0948a mo10057a(int i, int i2, Bitmap.Config config) {
            C0948a aVar = (C0948a) mo10059b();
            aVar.mo10052a(i, i2, config);
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0948a m1509a() {
            return new C0948a(this);
        }
    }

    AttributeStrategy() {
    }

    /* renamed from: d */
    private static String m1498d(Bitmap bitmap) {
        return m1497c(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());
    }

    /* renamed from: a */
    public void mo10046a(Bitmap bitmap) {
        this.f988b.mo10068a(this.f987a.mo10057a(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig()), bitmap);
    }

    /* renamed from: b */
    public String mo10048b(int i, int i2, Bitmap.Config config) {
        return m1497c(i, i2, config);
    }

    /* renamed from: c */
    public String mo10049c(Bitmap bitmap) {
        return m1498d(bitmap);
    }

    public String toString() {
        return "AttributeStrategy:\n  " + this.f988b;
    }

    /* renamed from: c */
    static String m1497c(int i, int i2, Bitmap.Config config) {
        return "[" + i + "x" + i2 + "], " + config;
    }

    /* renamed from: b */
    public int mo10047b(Bitmap bitmap) {
        return C1122j.m2838a(bitmap);
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.a0.c$a */
    /* compiled from: AttributeStrategy */
    static class C0948a implements Poolable {

        /* renamed from: a */
        private final C0949b f989a;

        /* renamed from: b */
        private int f990b;

        /* renamed from: c */
        private int f991c;

        /* renamed from: d */
        private Bitmap.Config f992d;

        public C0948a(C0949b bVar) {
            this.f989a = bVar;
        }

        /* renamed from: a */
        public void mo10052a(int i, int i2, Bitmap.Config config) {
            this.f990b = i;
            this.f991c = i2;
            this.f992d = config;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0948a)) {
                return false;
            }
            C0948a aVar = (C0948a) obj;
            if (this.f990b == aVar.f990b && this.f991c == aVar.f991c && this.f992d == aVar.f992d) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i = ((this.f990b * 31) + this.f991c) * 31;
            Bitmap.Config config = this.f992d;
            return i + (config != null ? config.hashCode() : 0);
        }

        public String toString() {
            return AttributeStrategy.m1497c(this.f990b, this.f991c, this.f992d);
        }

        /* renamed from: a */
        public void mo10051a() {
            this.f989a.mo10058a(this);
        }
    }

    /* renamed from: a */
    public Bitmap mo10045a(int i, int i2, Bitmap.Config config) {
        return this.f988b.mo10067a((LruArrayPool.C0951a) this.f987a.mo10057a(i, i2, config));
    }

    /* renamed from: a */
    public Bitmap mo10044a() {
        return this.f988b.mo10066a();
    }
}
