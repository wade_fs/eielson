package com.bumptech.glide.load.p021m;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p030p.p031c.RecyclableBufferedInputStream;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.m.k */
public final class InputStreamRewinder implements DataRewinder<InputStream> {

    /* renamed from: a */
    private final RecyclableBufferedInputStream f957a;

    /* renamed from: com.bumptech.glide.load.m.k$a */
    /* compiled from: InputStreamRewinder */
    public static final class C0940a implements DataRewinder.C0935a<InputStream> {

        /* renamed from: a */
        private final ArrayPool f958a;

        public C0940a(ArrayPool bVar) {
            this.f958a = bVar;
        }

        @NonNull
        /* renamed from: a */
        public DataRewinder<InputStream> mo10002a(InputStream inputStream) {
            return new InputStreamRewinder(inputStream, this.f958a);
        }

        @NonNull
        /* renamed from: a */
        public Class<InputStream> mo10003a() {
            return InputStream.class;
        }
    }

    InputStreamRewinder(InputStream inputStream, ArrayPool bVar) {
        this.f957a = new RecyclableBufferedInputStream(inputStream, bVar);
        this.f957a.mark(5242880);
    }

    /* renamed from: b */
    public void mo10001b() {
        this.f957a.mo10366b();
    }

    @NonNull
    /* renamed from: a */
    public InputStream m1437a() {
        this.f957a.reset();
        return this.f957a;
    }
}
