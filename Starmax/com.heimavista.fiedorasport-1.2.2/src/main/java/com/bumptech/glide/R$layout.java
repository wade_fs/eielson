package com.bumptech.glide;

public final class R$layout {
    public static final int notification_action = 2131493086;
    public static final int notification_action_tombstone = 2131493087;
    public static final int notification_template_custom_big = 2131493094;
    public static final int notification_template_icon_group = 2131493095;
    public static final int notification_template_part_chronometer = 2131493099;
    public static final int notification_template_part_time = 2131493100;

    private R$layout() {
    }
}
