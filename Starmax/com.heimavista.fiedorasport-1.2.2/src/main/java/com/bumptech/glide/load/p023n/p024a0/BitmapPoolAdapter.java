package com.bumptech.glide.load.p023n.p024a0;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.load.n.a0.f */
public class BitmapPoolAdapter implements BitmapPool {
    /* renamed from: a */
    public void mo10061a() {
    }

    /* renamed from: a */
    public void mo10062a(int i) {
    }

    /* renamed from: a */
    public void mo10063a(Bitmap bitmap) {
        bitmap.recycle();
    }

    @NonNull
    /* renamed from: b */
    public Bitmap mo10064b(int i, int i2, Bitmap.Config config) {
        return mo10060a(i, i2, config);
    }

    @NonNull
    /* renamed from: a */
    public Bitmap mo10060a(int i, int i2, Bitmap.Config config) {
        return Bitmap.createBitmap(i, i2, config);
    }
}
