package com.bumptech.glide.manager;

import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.manager.h */
public interface Lifecycle {
    /* renamed from: a */
    void mo10499a(@NonNull LifecycleListener iVar);

    /* renamed from: b */
    void mo10501b(@NonNull LifecycleListener iVar);
}
