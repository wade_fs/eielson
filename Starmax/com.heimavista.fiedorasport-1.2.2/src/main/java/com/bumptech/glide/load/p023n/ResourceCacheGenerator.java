package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p023n.DataFetcherGenerator;
import com.bumptech.glide.load.p028o.ModelLoader;
import java.io.File;
import java.util.List;

/* renamed from: com.bumptech.glide.load.n.w */
class ResourceCacheGenerator implements DataFetcherGenerator, DataFetcher.C0934a<Object> {

    /* renamed from: P */
    private final DataFetcherGenerator.C0977a f1258P;

    /* renamed from: Q */
    private final DecodeHelper<?> f1259Q;

    /* renamed from: R */
    private int f1260R;

    /* renamed from: S */
    private int f1261S = -1;

    /* renamed from: T */
    private Key f1262T;

    /* renamed from: U */
    private List<ModelLoader<File, ?>> f1263U;

    /* renamed from: V */
    private int f1264V;

    /* renamed from: W */
    private volatile ModelLoader.C1036a<?> f1265W;

    /* renamed from: X */
    private File f1266X;

    /* renamed from: Y */
    private ResourceCacheKey f1267Y;

    ResourceCacheGenerator(DecodeHelper<?> gVar, DataFetcherGenerator.C0977a aVar) {
        this.f1259Q = gVar;
        this.f1258P = aVar;
    }

    /* renamed from: b */
    private boolean m1854b() {
        return this.f1264V < this.f1263U.size();
    }

    /* renamed from: a */
    public boolean mo10116a() {
        List<Key> c = this.f1259Q.mo10150c();
        boolean z = false;
        if (c.isEmpty()) {
            return false;
        }
        List<Class<?>> k = this.f1259Q.mo10159k();
        if (!k.isEmpty()) {
            while (true) {
                if (this.f1263U == null || !m1854b()) {
                    this.f1261S++;
                    if (this.f1261S >= k.size()) {
                        this.f1260R++;
                        if (this.f1260R >= c.size()) {
                            return false;
                        }
                        this.f1261S = 0;
                    }
                    Key gVar = c.get(this.f1260R);
                    Class cls = k.get(this.f1261S);
                    this.f1267Y = new ResourceCacheKey(this.f1259Q.mo10148b(), gVar, this.f1259Q.mo10160l(), this.f1259Q.mo10162n(), this.f1259Q.mo10154f(), this.f1259Q.mo10147b(cls), cls, this.f1259Q.mo10157i());
                    this.f1266X = this.f1259Q.mo10152d().mo10088a(this.f1267Y);
                    File file = this.f1266X;
                    if (file != null) {
                        this.f1262T = gVar;
                        this.f1263U = this.f1259Q.mo10143a(file);
                        this.f1264V = 0;
                    }
                } else {
                    this.f1265W = null;
                    while (!z && m1854b()) {
                        List<ModelLoader<File, ?>> list = this.f1263U;
                        int i = this.f1264V;
                        this.f1264V = i + 1;
                        this.f1265W = list.get(i).mo10261a(this.f1266X, this.f1259Q.mo10162n(), this.f1259Q.mo10154f(), this.f1259Q.mo10157i());
                        if (this.f1265W != null && this.f1259Q.mo10151c(this.f1265W.f1331c.mo9984a())) {
                            this.f1265W.f1331c.mo9988a(this.f1259Q.mo10158j(), this);
                            z = true;
                        }
                    }
                    return z;
                }
            }
        } else if (File.class.equals(this.f1259Q.mo10161m())) {
            return false;
        } else {
            throw new IllegalStateException("Failed to find any load path from " + this.f1259Q.mo10156h() + " to " + this.f1259Q.mo10161m());
        }
    }

    public void cancel() {
        ModelLoader.C1036a<?> aVar = this.f1265W;
        if (aVar != null) {
            aVar.f1331c.cancel();
        }
    }

    /* renamed from: a */
    public void mo9999a(Object obj) {
        this.f1258P.mo10138a(this.f1262T, obj, this.f1265W.f1331c, DataSource.RESOURCE_DISK_CACHE, this.f1267Y);
    }

    /* renamed from: a */
    public void mo9998a(@NonNull Exception exc) {
        this.f1258P.mo10137a(this.f1267Y, exc, this.f1265W.f1331c, DataSource.RESOURCE_DISK_CACHE);
    }
}
