package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.load.o.o */
public interface ModelLoaderFactory<T, Y> {
    @NonNull
    /* renamed from: a */
    ModelLoader<T, Y> mo10265a(@NonNull MultiModelLoaderFactory rVar);
}
