package com.bumptech.glide;

public final class R$styleable {
    public static final int[] CoordinatorLayout = {2130969059, 2130969463};
    public static final int[] CoordinatorLayout_Layout = {16842931, 2130969075, 2130969076, 2130969077, 2130969122, 2130969131, 2130969132};
    public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
    public static final int CoordinatorLayout_Layout_layout_anchor = 1;
    public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
    public static final int CoordinatorLayout_Layout_layout_behavior = 3;
    public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
    public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
    public static final int CoordinatorLayout_Layout_layout_keyline = 6;
    public static final int CoordinatorLayout_keylines = 0;
    public static final int CoordinatorLayout_statusBarBackground = 1;
    public static final int[] FontFamily = {2130968974, 2130968975, 2130968976, 2130968977, 2130968978, 2130968979};
    public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, 2130968972, 2130968980, 2130968981, 2130968982, 2130969641};
    public static final int FontFamilyFont_android_font = 0;
    public static final int FontFamilyFont_android_fontStyle = 2;
    public static final int FontFamilyFont_android_fontVariationSettings = 4;
    public static final int FontFamilyFont_android_fontWeight = 1;
    public static final int FontFamilyFont_android_ttcIndex = 3;
    public static final int FontFamilyFont_font = 5;
    public static final int FontFamilyFont_fontStyle = 6;
    public static final int FontFamilyFont_fontVariationSettings = 7;
    public static final int FontFamilyFont_fontWeight = 8;
    public static final int FontFamilyFont_ttcIndex = 9;
    public static final int FontFamily_fontProviderAuthority = 0;
    public static final int FontFamily_fontProviderCerts = 1;
    public static final int FontFamily_fontProviderFetchStrategy = 2;
    public static final int FontFamily_fontProviderFetchTimeout = 3;
    public static final int FontFamily_fontProviderPackage = 4;
    public static final int FontFamily_fontProviderQuery = 5;

    private R$styleable() {
    }
}
