package com.bumptech.glide.load.p030p.p033e;

import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.Initializable;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.e.b */
public abstract class DrawableResource<T extends Drawable> implements Resource<T>, Initializable {

    /* renamed from: P */
    protected final T f1462P;

    public DrawableResource(T t) {
        Preconditions.m2828a((Object) t);
        this.f1462P = (Drawable) t;
    }

    /* renamed from: c */
    public void mo10250c() {
        T t = this.f1462P;
        if (t instanceof BitmapDrawable) {
            ((BitmapDrawable) t).getBitmap().prepareToDraw();
        } else if (t instanceof GifDrawable) {
            ((GifDrawable) t).mo10407c().prepareToDraw();
        }
    }

    @NonNull
    public final T get() {
        Drawable.ConstantState constantState = this.f1462P.getConstantState();
        if (constantState == null) {
            return this.f1462P;
        }
        return constantState.newDrawable();
    }
}
