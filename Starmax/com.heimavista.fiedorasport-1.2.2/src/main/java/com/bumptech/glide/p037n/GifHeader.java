package com.bumptech.glide.p037n;

import androidx.annotation.ColorInt;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.n.c */
public class GifHeader {
    @ColorInt

    /* renamed from: a */
    int[] f1606a = null;

    /* renamed from: b */
    int f1607b = 0;

    /* renamed from: c */
    int f1608c = 0;

    /* renamed from: d */
    GifFrame f1609d;

    /* renamed from: e */
    final List<GifFrame> f1610e = new ArrayList();

    /* renamed from: f */
    int f1611f;

    /* renamed from: g */
    int f1612g;

    /* renamed from: h */
    boolean f1613h;

    /* renamed from: i */
    int f1614i;

    /* renamed from: j */
    int f1615j;

    /* renamed from: k */
    int f1616k;
    @ColorInt

    /* renamed from: l */
    int f1617l;

    /* renamed from: m */
    int f1618m;

    /* renamed from: a */
    public int mo10537a() {
        return this.f1612g;
    }

    /* renamed from: b */
    public int mo10538b() {
        return this.f1608c;
    }

    /* renamed from: c */
    public int mo10539c() {
        return this.f1607b;
    }

    /* renamed from: d */
    public int mo10540d() {
        return this.f1611f;
    }
}
