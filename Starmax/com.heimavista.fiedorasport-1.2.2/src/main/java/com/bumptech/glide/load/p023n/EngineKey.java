package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.n */
class EngineKey implements Key {

    /* renamed from: b */
    private final Object f1224b;

    /* renamed from: c */
    private final int f1225c;

    /* renamed from: d */
    private final int f1226d;

    /* renamed from: e */
    private final Class<?> f1227e;

    /* renamed from: f */
    private final Class<?> f1228f;

    /* renamed from: g */
    private final Key f1229g;

    /* renamed from: h */
    private final Map<Class<?>, Transformation<?>> f1230h;

    /* renamed from: i */
    private final Options f1231i;

    /* renamed from: j */
    private int f1232j;

    EngineKey(Object obj, Key gVar, int i, int i2, Map<Class<?>, Transformation<?>> map, Class<?> cls, Class<?> cls2, Options iVar) {
        Preconditions.m2828a(obj);
        this.f1224b = obj;
        Preconditions.m2829a(gVar, "Signature must not be null");
        this.f1229g = gVar;
        this.f1225c = i;
        this.f1226d = i2;
        Preconditions.m2828a(map);
        this.f1230h = map;
        Preconditions.m2829a(cls, "Resource class must not be null");
        this.f1227e = cls;
        Preconditions.m2829a(cls2, "Transcode class must not be null");
        this.f1228f = cls2;
        Preconditions.m2828a(iVar);
        this.f1231i = iVar;
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        throw new UnsupportedOperationException();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof EngineKey)) {
            return false;
        }
        EngineKey nVar = (EngineKey) obj;
        if (!this.f1224b.equals(nVar.f1224b) || !this.f1229g.equals(nVar.f1229g) || this.f1226d != nVar.f1226d || this.f1225c != nVar.f1225c || !this.f1230h.equals(nVar.f1230h) || !this.f1227e.equals(nVar.f1227e) || !this.f1228f.equals(nVar.f1228f) || !this.f1231i.equals(nVar.f1231i)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.f1232j == 0) {
            this.f1232j = this.f1224b.hashCode();
            this.f1232j = (this.f1232j * 31) + this.f1229g.hashCode();
            this.f1232j = (this.f1232j * 31) + this.f1225c;
            this.f1232j = (this.f1232j * 31) + this.f1226d;
            this.f1232j = (this.f1232j * 31) + this.f1230h.hashCode();
            this.f1232j = (this.f1232j * 31) + this.f1227e.hashCode();
            this.f1232j = (this.f1232j * 31) + this.f1228f.hashCode();
            this.f1232j = (this.f1232j * 31) + this.f1231i.hashCode();
        }
        return this.f1232j;
    }

    public String toString() {
        return "EngineKey{model=" + this.f1224b + ", width=" + this.f1225c + ", height=" + this.f1226d + ", resourceClass=" + this.f1227e + ", transcodeClass=" + this.f1228f + ", signature=" + this.f1229g + ", hashCode=" + this.f1232j + ", transformations=" + this.f1230h + ", options=" + this.f1231i + '}';
    }
}
