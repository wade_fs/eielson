package com.bumptech.glide.manager;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.p040q.Request;
import com.bumptech.glide.util.C1122j;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: com.bumptech.glide.manager.m */
public class RequestTracker {

    /* renamed from: a */
    private final Set<Request> f1591a = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: b */
    private final List<Request> f1592b = new ArrayList();

    /* renamed from: c */
    private boolean f1593c;

    /* renamed from: a */
    public boolean mo10517a(@Nullable Request dVar) {
        return m2478a(dVar, true);
    }

    /* renamed from: b */
    public void mo10519b(@NonNull Request dVar) {
        this.f1591a.add(dVar);
        if (!this.f1593c) {
            dVar.mo10626c();
            return;
        }
        dVar.clear();
        if (Log.isLoggable("RequestTracker", 2)) {
            Log.v("RequestTracker", "Paused, delaying request");
        }
        this.f1592b.add(dVar);
    }

    /* renamed from: c */
    public void mo10520c() {
        for (Request dVar : C1122j.m2843a(this.f1591a)) {
            if (!dVar.mo10635h() && !dVar.mo10634g()) {
                dVar.clear();
                if (!this.f1593c) {
                    dVar.mo10626c();
                } else {
                    this.f1592b.add(dVar);
                }
            }
        }
    }

    /* renamed from: d */
    public void mo10521d() {
        this.f1593c = false;
        for (Request dVar : C1122j.m2843a(this.f1591a)) {
            if (!dVar.mo10635h() && !dVar.isRunning()) {
                dVar.mo10626c();
            }
        }
        this.f1592b.clear();
    }

    public String toString() {
        return super.toString() + "{numRequests=" + this.f1591a.size() + ", isPaused=" + this.f1593c + "}";
    }

    /* renamed from: a */
    private boolean m2478a(@Nullable Request dVar, boolean z) {
        boolean z2 = true;
        if (dVar == null) {
            return true;
        }
        boolean remove = this.f1591a.remove(dVar);
        if (!this.f1592b.remove(dVar) && !remove) {
            z2 = false;
        }
        if (z2) {
            dVar.clear();
            if (z) {
                dVar.recycle();
            }
        }
        return z2;
    }

    /* renamed from: a */
    public void mo10516a() {
        for (Request dVar : C1122j.m2843a(this.f1591a)) {
            m2478a(dVar, false);
        }
        this.f1592b.clear();
    }

    /* renamed from: b */
    public void mo10518b() {
        this.f1593c = true;
        for (Request dVar : C1122j.m2843a(this.f1591a)) {
            if (dVar.isRunning()) {
                dVar.clear();
                this.f1592b.add(dVar);
            }
        }
    }
}
