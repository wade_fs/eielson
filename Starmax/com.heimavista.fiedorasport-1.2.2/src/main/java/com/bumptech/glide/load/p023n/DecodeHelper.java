package com.bumptech.glide.load.p023n;

import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.DecodeJob;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p030p.UnitTransformation;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.g */
final class DecodeHelper<Transcode> {

    /* renamed from: a */
    private final List<ModelLoader.C1036a<?>> f1089a = new ArrayList();

    /* renamed from: b */
    private final List<Key> f1090b = new ArrayList();

    /* renamed from: c */
    private GlideContext f1091c;

    /* renamed from: d */
    private Object f1092d;

    /* renamed from: e */
    private int f1093e;

    /* renamed from: f */
    private int f1094f;

    /* renamed from: g */
    private Class<?> f1095g;

    /* renamed from: h */
    private DecodeJob.C0982e f1096h;

    /* renamed from: i */
    private Options f1097i;

    /* renamed from: j */
    private Map<Class<?>, Transformation<?>> f1098j;

    /* renamed from: k */
    private Class<Transcode> f1099k;

    /* renamed from: l */
    private boolean f1100l;

    /* renamed from: m */
    private boolean f1101m;

    /* renamed from: n */
    private Key f1102n;

    /* renamed from: o */
    private Priority f1103o;

    /* renamed from: p */
    private DiskCacheStrategy f1104p;

    /* renamed from: q */
    private boolean f1105q;

    /* renamed from: r */
    private boolean f1106r;

    DecodeHelper() {
    }

    /* JADX WARN: Type inference failed for: r8v0, types: [java.lang.Class<Transcode>, java.lang.Class<R>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <R> void mo10145a(com.bumptech.glide.GlideContext r1, java.lang.Object r2, com.bumptech.glide.load.Key r3, int r4, int r5, com.bumptech.glide.load.p023n.DiskCacheStrategy r6, java.lang.Class<?> r7, java.lang.Class<R> r8, com.bumptech.glide.Priority r9, com.bumptech.glide.load.Options r10, java.util.Map<java.lang.Class<?>, com.bumptech.glide.load.Transformation<?>> r11, boolean r12, boolean r13, com.bumptech.glide.load.p023n.DecodeJob.C0982e r14) {
        /*
            r0 = this;
            r0.f1091c = r1
            r0.f1092d = r2
            r0.f1102n = r3
            r0.f1093e = r4
            r0.f1094f = r5
            r0.f1104p = r6
            r0.f1095g = r7
            r0.f1096h = r14
            r0.f1099k = r8
            r0.f1103o = r9
            r0.f1097i = r10
            r0.f1098j = r11
            r0.f1105q = r12
            r0.f1106r = r13
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.DecodeHelper.mo10145a(com.bumptech.glide.e, java.lang.Object, com.bumptech.glide.load.g, int, int, com.bumptech.glide.load.n.j, java.lang.Class, java.lang.Class, com.bumptech.glide.h, com.bumptech.glide.load.i, java.util.Map, boolean, boolean, com.bumptech.glide.load.n.h$e):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public ArrayPool mo10148b() {
        return this.f1091c.mo9906a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public boolean mo10151c(Class<?> cls) {
        return mo10142a(cls) != null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public DiskCache mo10152d() {
        return this.f1096h.mo10180a();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public DiskCacheStrategy mo10153e() {
        return this.f1104p;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public int mo10154f() {
        return this.f1094f;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public List<ModelLoader.C1036a<?>> mo10155g() {
        if (!this.f1100l) {
            this.f1100l = true;
            this.f1089a.clear();
            List a = this.f1091c.mo9912f().mo9926a(this.f1092d);
            int size = a.size();
            for (int i = 0; i < size; i++) {
                ModelLoader.C1036a a2 = ((ModelLoader) a.get(i)).mo10261a(this.f1092d, this.f1093e, this.f1094f, this.f1097i);
                if (a2 != null) {
                    this.f1089a.add(a2);
                }
            }
        }
        return this.f1089a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public Class<?> mo10156h() {
        return this.f1092d.getClass();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public Options mo10157i() {
        return this.f1097i;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j */
    public Priority mo10158j() {
        return this.f1103o;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k */
    public List<Class<?>> mo10159k() {
        return this.f1091c.mo9912f().mo9928b(this.f1092d.getClass(), this.f1095g, this.f1099k);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l */
    public Key mo10160l() {
        return this.f1102n;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m */
    public Class<?> mo10161m() {
        return this.f1099k;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: n */
    public int mo10162n() {
        return this.f1093e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: o */
    public boolean mo10163o() {
        return this.f1106r;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public <Z> Transformation<Z> mo10147b(Class<Z> cls) {
        Transformation<Z> lVar = this.f1098j.get(cls);
        if (lVar == null) {
            Iterator<Map.Entry<Class<?>, Transformation<?>>> it = this.f1098j.entrySet().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Map.Entry next = it.next();
                if (((Class) next.getKey()).isAssignableFrom(cls)) {
                    lVar = (Transformation) next.getValue();
                    break;
                }
            }
        }
        if (lVar != null) {
            return lVar;
        }
        if (!this.f1098j.isEmpty() || !this.f1105q) {
            return UnitTransformation.m2079a();
        }
        throw new IllegalArgumentException("Missing transformation for " + cls + ". If you wish to ignore unknown resource types, use the optional transformation methods.");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public List<Key> mo10150c() {
        if (!this.f1101m) {
            this.f1101m = true;
            this.f1090b.clear();
            List<ModelLoader.C1036a<?>> g = mo10155g();
            int size = g.size();
            for (int i = 0; i < size; i++) {
                ModelLoader.C1036a aVar = g.get(i);
                if (!this.f1090b.contains(aVar.f1329a)) {
                    this.f1090b.add(aVar.f1329a);
                }
                for (int i2 = 0; i2 < aVar.f1330b.size(); i2++) {
                    if (!this.f1090b.contains(aVar.f1330b.get(i2))) {
                        this.f1090b.add(aVar.f1330b.get(i2));
                    }
                }
            }
        }
        return this.f1090b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo10149b(Resource<?> vVar) {
        return this.f1091c.mo9912f().mo9929b(vVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10144a() {
        this.f1091c = null;
        this.f1092d = null;
        this.f1102n = null;
        this.f1095g = null;
        this.f1099k = null;
        this.f1097i = null;
        this.f1103o = null;
        this.f1098j = null;
        this.f1104p = null;
        this.f1089a.clear();
        this.f1100l = false;
        this.f1090b.clear();
        this.f1101m = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public <Data> LoadPath<Data, ?, Transcode> mo10142a(Class cls) {
        return this.f1091c.mo9912f().mo9924a(cls, this.f1095g, this.f1099k);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public <Z> ResourceEncoder<Z> mo10141a(Resource vVar) {
        return this.f1091c.mo9912f().mo9923a(vVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public List<ModelLoader<File, ?>> mo10143a(File file) {
        return this.f1091c.mo9912f().mo9926a(file);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo10146a(Key gVar) {
        List<ModelLoader.C1036a<?>> g = mo10155g();
        int size = g.size();
        for (int i = 0; i < size; i++) {
            if (g.get(i).f1329a.equals(gVar)) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public <X> Encoder<X> mo10140a(Object obj) {
        return this.f1091c.mo9912f().mo9930c(obj);
    }
}
