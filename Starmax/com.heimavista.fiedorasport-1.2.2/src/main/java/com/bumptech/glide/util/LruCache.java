package com.bumptech.glide.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.bumptech.glide.util.f */
public class LruCache<T, Y> {

    /* renamed from: a */
    private final Map<T, Y> f1781a = new LinkedHashMap(100, 0.75f, true);

    /* renamed from: b */
    private long f1782b;

    /* renamed from: c */
    private long f1783c;

    public LruCache(long j) {
        this.f1782b = j;
    }

    @Nullable
    /* renamed from: a */
    public synchronized Y mo10698a(@NonNull Object obj) {
        return this.f1781a.get(obj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo10102a(@NonNull T t, @Nullable Y y) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public int mo10103b(@Nullable Y y) {
        return 1;
    }

    /* renamed from: b */
    public synchronized long mo10701b() {
        return this.f1782b;
    }

    @Nullable
    /* renamed from: c */
    public synchronized Y mo10703c(@NonNull T t) {
        Y remove;
        remove = this.f1781a.remove(t);
        if (remove != null) {
            this.f1783c -= (long) mo10103b(remove);
        }
        return remove;
    }

    /* renamed from: a */
    public void mo10699a() {
        mo10700a(0);
    }

    @Nullable
    /* renamed from: b */
    public synchronized Y mo10702b(@NonNull Object obj, @Nullable Object obj2) {
        long b = (long) mo10103b(obj2);
        if (b >= this.f1782b) {
            mo10102a(obj, obj2);
            return null;
        }
        if (obj2 != null) {
            this.f1783c += b;
        }
        Y put = this.f1781a.put(obj, obj2);
        if (put != null) {
            this.f1783c -= (long) mo10103b(put);
            if (!put.equals(obj2)) {
                mo10102a(obj, put);
            }
        }
        m2816c();
        return put;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized void mo10700a(long j) {
        while (this.f1783c > j) {
            Iterator<Map.Entry<T, Y>> it = this.f1781a.entrySet().iterator();
            Map.Entry next = it.next();
            Object value = next.getValue();
            this.f1783c -= (long) mo10103b(value);
            Object key = next.getKey();
            it.remove();
            mo10102a(key, value);
        }
    }

    /* renamed from: c */
    private void m2816c() {
        mo10700a(this.f1782b);
    }
}
