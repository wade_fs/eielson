package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import java.io.File;

/* renamed from: com.bumptech.glide.load.p.c.b */
public class BitmapDrawableEncoder implements ResourceEncoder<BitmapDrawable> {

    /* renamed from: a */
    private final BitmapPool f1386a;

    /* renamed from: b */
    private final ResourceEncoder<Bitmap> f1387b;

    public BitmapDrawableEncoder(BitmapPool eVar, ResourceEncoder<Bitmap> kVar) {
        this.f1386a = eVar;
        this.f1387b = kVar;
    }

    /* renamed from: a */
    public /* bridge */ /* synthetic */ boolean mo9965a(@NonNull Object obj, @NonNull File file, @NonNull Options iVar) {
        return mo10340a((Resource<BitmapDrawable>) ((Resource) obj), file, iVar);
    }

    /* renamed from: a */
    public boolean mo10340a(@NonNull Resource<BitmapDrawable> vVar, @NonNull File file, @NonNull Options iVar) {
        return this.f1387b.mo9965a(new BitmapResource(vVar.get().getBitmap(), this.f1386a), file, iVar);
    }

    @NonNull
    /* renamed from: a */
    public EncodeStrategy mo9981a(@NonNull Options iVar) {
        return this.f1387b.mo9981a(iVar);
    }
}
