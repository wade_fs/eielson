package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.LruCache;
import java.util.Queue;

/* renamed from: com.bumptech.glide.load.o.m */
public class ModelCache<A, B> {

    /* renamed from: a */
    private final LruCache<C1035b<A>, B> f1324a;

    /* renamed from: com.bumptech.glide.load.o.m$a */
    /* compiled from: ModelCache */
    class C1034a extends LruCache<C1035b<A>, B> {
        C1034a(ModelCache mVar, long j) {
            super(j);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo10102a(@NonNull C1035b<A> bVar, @Nullable B b) {
            bVar.mo10303a();
        }
    }

    public ModelCache(long j) {
        this.f1324a = new C1034a(this, j);
    }

    @Nullable
    /* renamed from: a */
    public B mo10300a(A a, int i, int i2) {
        C1035b a2 = C1035b.m1971a(a, i, i2);
        B a3 = this.f1324a.mo10698a(a2);
        a2.mo10303a();
        return a3;
    }

    /* renamed from: a */
    public void mo10301a(A a, int i, int i2, B b) {
        this.f1324a.mo10702b(C1035b.m1971a(a, i, i2), b);
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.o.m$b */
    /* compiled from: ModelCache */
    static final class C1035b<A> {

        /* renamed from: d */
        private static final Queue<C1035b<?>> f1325d = C1122j.m2844a(0);

        /* renamed from: a */
        private int f1326a;

        /* renamed from: b */
        private int f1327b;

        /* renamed from: c */
        private A f1328c;

        private C1035b() {
        }

        /* renamed from: a */
        static <A> C1035b<A> m1971a(A a, int i, int i2) {
            C1035b<A> poll;
            synchronized (f1325d) {
                poll = f1325d.poll();
            }
            if (poll == null) {
                poll = new C1035b<>();
            }
            poll.m1972b(a, i, i2);
            return poll;
        }

        /* renamed from: b */
        private void m1972b(A a, int i, int i2) {
            this.f1328c = a;
            this.f1327b = i;
            this.f1326a = i2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1035b)) {
                return false;
            }
            C1035b bVar = (C1035b) obj;
            if (this.f1327b == bVar.f1327b && this.f1326a == bVar.f1326a && this.f1328c.equals(bVar.f1328c)) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            return (((this.f1326a * 31) + this.f1327b) * 31) + this.f1328c.hashCode();
        }

        /* renamed from: a */
        public void mo10303a() {
            synchronized (f1325d) {
                f1325d.offer(this);
            }
        }
    }
}
