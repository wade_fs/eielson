package com.bumptech.glide.p040q.p041l;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import androidx.annotation.CallSuper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.p040q.Request;
import com.bumptech.glide.util.Preconditions;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Deprecated
/* renamed from: com.bumptech.glide.q.l.j */
public abstract class ViewTarget<T extends View, Z> extends BaseTarget<Z> {
    @Nullable

    /* renamed from: V */
    private static Integer f1750V;

    /* renamed from: Q */
    protected final T f1751Q;

    /* renamed from: R */
    private final C1114a f1752R;
    @Nullable

    /* renamed from: S */
    private View.OnAttachStateChangeListener f1753S;

    /* renamed from: T */
    private boolean f1754T;

    /* renamed from: U */
    private boolean f1755U;

    public ViewTarget(@NonNull T t) {
        Preconditions.m2828a((Object) t);
        this.f1751Q = (View) t;
        this.f1752R = new C1114a(t);
    }

    @Nullable
    /* renamed from: d */
    private Object m2773d() {
        Integer num = f1750V;
        if (num == null) {
            return this.f1751Q.getTag();
        }
        return this.f1751Q.getTag(num.intValue());
    }

    /* renamed from: e */
    private void m2774e() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.f1753S;
        if (onAttachStateChangeListener != null && !this.f1755U) {
            this.f1751Q.addOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f1755U = true;
        }
    }

    /* renamed from: f */
    private void m2775f() {
        View.OnAttachStateChangeListener onAttachStateChangeListener = this.f1753S;
        if (onAttachStateChangeListener != null && this.f1755U) {
            this.f1751Q.removeOnAttachStateChangeListener(onAttachStateChangeListener);
            this.f1755U = false;
        }
    }

    @CallSuper
    /* renamed from: a */
    public void mo10641a(@NonNull SizeReadyCallback hVar) {
        this.f1752R.mo10667b(hVar);
    }

    @CallSuper
    /* renamed from: b */
    public void mo10644b(@Nullable Drawable drawable) {
        super.mo10644b(drawable);
        m2774e();
    }

    @CallSuper
    /* renamed from: c */
    public void mo10646c(@Nullable Drawable drawable) {
        super.mo10646c(drawable);
        this.f1752R.mo10666b();
        if (!this.f1754T) {
            m2775f();
        }
    }

    public String toString() {
        return "Target for: " + ((Object) this.f1751Q);
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.q.l.j$a */
    /* compiled from: ViewTarget */
    static final class C1114a {
        @VisibleForTesting
        @Nullable

        /* renamed from: e */
        static Integer f1756e;

        /* renamed from: a */
        private final View f1757a;

        /* renamed from: b */
        private final List<SizeReadyCallback> f1758b = new ArrayList();

        /* renamed from: c */
        boolean f1759c;
        @Nullable

        /* renamed from: d */
        private C1115a f1760d;

        /* renamed from: com.bumptech.glide.q.l.j$a$a */
        /* compiled from: ViewTarget */
        private static final class C1115a implements ViewTreeObserver.OnPreDrawListener {

            /* renamed from: P */
            private final WeakReference<C1114a> f1761P;

            C1115a(@NonNull C1114a aVar) {
                this.f1761P = new WeakReference<>(aVar);
            }

            public boolean onPreDraw() {
                if (Log.isLoggable("ViewTarget", 2)) {
                    Log.v("ViewTarget", "OnGlobalLayoutListener called attachStateListener=" + this);
                }
                C1114a aVar = this.f1761P.get();
                if (aVar == null) {
                    return true;
                }
                aVar.mo10664a();
                return true;
            }
        }

        C1114a(@NonNull View view) {
            this.f1757a = view;
        }

        /* renamed from: a */
        private static int m2783a(@NonNull Context context) {
            if (f1756e == null) {
                WindowManager windowManager = (WindowManager) context.getSystemService("window");
                Preconditions.m2828a(windowManager);
                Display defaultDisplay = windowManager.getDefaultDisplay();
                Point point = new Point();
                defaultDisplay.getSize(point);
                f1756e = Integer.valueOf(Math.max(point.x, point.y));
            }
            return f1756e.intValue();
        }

        /* renamed from: a */
        private boolean m2784a(int i) {
            return i > 0 || i == Integer.MIN_VALUE;
        }

        /* renamed from: b */
        private void m2786b(int i, int i2) {
            Iterator it = new ArrayList(this.f1758b).iterator();
            while (it.hasNext()) {
                ((SizeReadyCallback) it.next()).mo10656a(i, i2);
            }
        }

        /* renamed from: c */
        private int m2787c() {
            int paddingTop = this.f1757a.getPaddingTop() + this.f1757a.getPaddingBottom();
            ViewGroup.LayoutParams layoutParams = this.f1757a.getLayoutParams();
            return m2782a(this.f1757a.getHeight(), layoutParams != null ? layoutParams.height : 0, paddingTop);
        }

        /* renamed from: d */
        private int m2788d() {
            int paddingLeft = this.f1757a.getPaddingLeft() + this.f1757a.getPaddingRight();
            ViewGroup.LayoutParams layoutParams = this.f1757a.getLayoutParams();
            return m2782a(this.f1757a.getWidth(), layoutParams != null ? layoutParams.width : 0, paddingLeft);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo10667b(@NonNull SizeReadyCallback hVar) {
            this.f1758b.remove(hVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo10666b() {
            ViewTreeObserver viewTreeObserver = this.f1757a.getViewTreeObserver();
            if (viewTreeObserver.isAlive()) {
                viewTreeObserver.removeOnPreDrawListener(this.f1760d);
            }
            this.f1760d = null;
            this.f1758b.clear();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10664a() {
            if (!this.f1758b.isEmpty()) {
                int d = m2788d();
                int c = m2787c();
                if (m2785a(d, c)) {
                    m2786b(d, c);
                    mo10666b();
                }
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10665a(@NonNull SizeReadyCallback hVar) {
            int d = m2788d();
            int c = m2787c();
            if (m2785a(d, c)) {
                hVar.mo10656a(d, c);
                return;
            }
            if (!this.f1758b.contains(hVar)) {
                this.f1758b.add(hVar);
            }
            if (this.f1760d == null) {
                ViewTreeObserver viewTreeObserver = this.f1757a.getViewTreeObserver();
                this.f1760d = new C1115a(this);
                viewTreeObserver.addOnPreDrawListener(this.f1760d);
            }
        }

        /* renamed from: a */
        private boolean m2785a(int i, int i2) {
            return m2784a(i) && m2784a(i2);
        }

        /* renamed from: a */
        private int m2782a(int i, int i2, int i3) {
            int i4 = i2 - i3;
            if (i4 > 0) {
                return i4;
            }
            if (this.f1759c && this.f1757a.isLayoutRequested()) {
                return 0;
            }
            int i5 = i - i3;
            if (i5 > 0) {
                return i5;
            }
            if (this.f1757a.isLayoutRequested() || i2 != -2) {
                return 0;
            }
            if (Log.isLoggable("ViewTarget", 4)) {
                Log.i("ViewTarget", "Glide treats LayoutParams.WRAP_CONTENT as a request for an image the size of this device's screen dimensions. If you want to load the original image and are ok with the corresponding memory cost and OOMs (depending on the input size), use .override(Target.SIZE_ORIGINAL). Otherwise, use LayoutParams.MATCH_PARENT, set layout_width and layout_height to fixed dimension, or use .override() with fixed dimensions.");
            }
            return m2783a(this.f1757a.getContext());
        }
    }

    /* renamed from: a */
    public void mo10640a(@Nullable Request dVar) {
        mo10659a((Object) dVar);
    }

    @Nullable
    /* renamed from: a */
    public Request mo10638a() {
        Object d = m2773d();
        if (d == null) {
            return null;
        }
        if (d instanceof Request) {
            return (Request) d;
        }
        throw new IllegalArgumentException("You must not call setTag() on a view Glide is targeting");
    }

    @CallSuper
    /* renamed from: b */
    public void mo10645b(@NonNull SizeReadyCallback hVar) {
        this.f1752R.mo10665a(hVar);
    }

    /* renamed from: a */
    private void mo10659a(@Nullable Object obj) {
        Integer num = f1750V;
        if (num == null) {
            this.f1751Q.setTag(obj);
        } else {
            this.f1751Q.setTag(num.intValue(), obj);
        }
    }
}
