package com.bumptech.glide.load.p023n;

import com.bumptech.glide.load.Key;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.s */
final class Jobs {

    /* renamed from: a */
    private final Map<Key, EngineJob<?>> f1248a = new HashMap();

    /* renamed from: b */
    private final Map<Key, EngineJob<?>> f1249b = new HashMap();

    Jobs() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public EngineJob<?> mo10251a(Key gVar, boolean z) {
        return m1837a(z).get(gVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo10253b(Key gVar, EngineJob<?> lVar) {
        Map<Key, EngineJob<?>> a = m1837a(lVar.mo10210f());
        if (lVar.equals(a.get(gVar))) {
            a.remove(gVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10252a(Key gVar, EngineJob<?> lVar) {
        m1837a(lVar.mo10210f()).put(gVar, lVar);
    }

    /* renamed from: a */
    private Map<Key, EngineJob<?>> m1837a(boolean z) {
        return z ? this.f1249b : this.f1248a;
    }
}
