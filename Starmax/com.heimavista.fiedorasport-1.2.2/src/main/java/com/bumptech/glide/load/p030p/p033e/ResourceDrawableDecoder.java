package com.bumptech.glide.load.p030p.p033e;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import java.util.List;

/* renamed from: com.bumptech.glide.load.p.e.d */
public class ResourceDrawableDecoder implements ResourceDecoder<Uri, Drawable> {

    /* renamed from: a */
    private final Context f1463a;

    public ResourceDrawableDecoder(Context context) {
        this.f1463a = context.getApplicationContext();
    }

    @DrawableRes
    /* renamed from: b */
    private int m2266b(Context context, Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        if (pathSegments.size() == 2) {
            return m2263a(context, uri);
        }
        if (pathSegments.size() == 1) {
            return m2264a(uri);
        }
        throw new IllegalArgumentException("Unrecognized Uri format: " + uri);
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull Uri uri, @NonNull Options iVar) {
        return uri.getScheme().equals("android.resource");
    }

    @Nullable
    /* renamed from: a */
    public Resource<Drawable> mo9979a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        Context a = m2265a(uri, uri.getAuthority());
        return NonOwnedDrawableResource.m2260a(DrawableDecoderCompat.m2255a(this.f1463a, a, m2266b(a, uri)));
    }

    @NonNull
    /* renamed from: a */
    private Context m2265a(Uri uri, String str) {
        if (str.equals(this.f1463a.getPackageName())) {
            return this.f1463a;
        }
        try {
            return this.f1463a.createPackageContext(str, 0);
        } catch (PackageManager.NameNotFoundException e) {
            if (str.contains(this.f1463a.getPackageName())) {
                return this.f1463a;
            }
            throw new IllegalArgumentException("Failed to obtain context or unrecognized Uri format for: " + uri, e);
        }
    }

    @DrawableRes
    /* renamed from: a */
    private int m2263a(Context context, Uri uri) {
        List<String> pathSegments = uri.getPathSegments();
        String authority = uri.getAuthority();
        String str = pathSegments.get(0);
        String str2 = pathSegments.get(1);
        int identifier = context.getResources().getIdentifier(str2, str, authority);
        if (identifier == 0) {
            identifier = Resources.getSystem().getIdentifier(str2, str, "android");
        }
        if (identifier != 0) {
            return identifier;
        }
        throw new IllegalArgumentException("Failed to find resource id for: " + uri);
    }

    @DrawableRes
    /* renamed from: a */
    private int m2264a(Uri uri) {
        try {
            return Integer.parseInt(uri.getPathSegments().get(0));
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Unrecognized Uri format: " + uri, e);
        }
    }
}
