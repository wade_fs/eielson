package com.bumptech.glide.load.p030p.p033e;

import android.graphics.drawable.Drawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.p.e.c */
final class NonOwnedDrawableResource extends DrawableResource<Drawable> {
    private NonOwnedDrawableResource(Drawable drawable) {
        super(drawable);
    }

    @Nullable
    /* renamed from: a */
    static Resource<Drawable> m2260a(@Nullable Drawable drawable) {
        if (drawable != null) {
            return new NonOwnedDrawableResource(drawable);
        }
        return null;
    }

    @NonNull
    /* renamed from: b */
    public Class<Drawable> mo10228b() {
        return super.f1462P.getClass();
    }

    public void recycle() {
    }

    /* renamed from: a */
    public int mo10226a() {
        return Math.max(1, super.f1462P.getIntrinsicWidth() * super.f1462P.getIntrinsicHeight() * 4);
    }
}
