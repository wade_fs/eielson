package com.bumptech.glide.load.p023n.p025b0;

import android.util.Log;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import com.bumptech.glide.p036m.DiskLruCache;
import java.io.File;
import java.io.IOException;

/* renamed from: com.bumptech.glide.load.n.b0.e */
public class DiskLruCacheWrapper implements DiskCache {

    /* renamed from: a */
    private final SafeKeyGenerator f1038a;

    /* renamed from: b */
    private final File f1039b;

    /* renamed from: c */
    private final long f1040c;

    /* renamed from: d */
    private final DiskCacheWriteLocker f1041d = new DiskCacheWriteLocker();

    /* renamed from: e */
    private DiskLruCache f1042e;

    @Deprecated
    protected DiskLruCacheWrapper(File file, long j) {
        this.f1039b = file;
        this.f1040c = j;
        this.f1038a = new SafeKeyGenerator();
    }

    /* renamed from: a */
    public static DiskCache m1615a(File file, long j) {
        return new DiskLruCacheWrapper(file, j);
    }

    /* renamed from: a */
    private synchronized DiskLruCache m1616a() {
        if (this.f1042e == null) {
            this.f1042e = DiskLruCache.m2365a(this.f1039b, 1, 1, this.f1040c);
        }
        return this.f1042e;
    }

    /* renamed from: a */
    public File mo10088a(Key gVar) {
        String a = this.f1038a.mo10113a(gVar);
        if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
            Log.v("DiskLruCacheWrapper", "Get: Obtained: " + a + " for for Key: " + gVar);
        }
        try {
            DiskLruCache.C1099e c = m1616a().mo10463c(a);
            if (c != null) {
                return c.mo10475a(0);
            }
            return null;
        } catch (IOException e) {
            if (!Log.isLoggable("DiskLruCacheWrapper", 5)) {
                return null;
            }
            Log.w("DiskLruCacheWrapper", "Unable to get from disk cache", e);
            return null;
        }
    }

    /* renamed from: a */
    public void mo10089a(Key gVar, DiskCache.C0959b bVar) {
        DiskLruCache.C1097c b;
        String a = this.f1038a.mo10113a(gVar);
        this.f1041d.mo10092a(a);
        try {
            if (Log.isLoggable("DiskLruCacheWrapper", 2)) {
                Log.v("DiskLruCacheWrapper", "Put: Obtained: " + a + " for for Key: " + gVar);
            }
            try {
                DiskLruCache a2 = m1616a();
                if (a2.mo10463c(a) == null) {
                    b = a2.mo10462b(a);
                    if (b != null) {
                        if (bVar.mo10091a(b.mo10468a(0))) {
                            b.mo10471c();
                        }
                        b.mo10470b();
                        this.f1041d.mo10093b(a);
                        return;
                    }
                    throw new IllegalStateException("Had two simultaneous puts for: " + a);
                }
            } catch (IOException e) {
                if (Log.isLoggable("DiskLruCacheWrapper", 5)) {
                    Log.w("DiskLruCacheWrapper", "Unable to put to disk cache", e);
                }
            } catch (Throwable th) {
                b.mo10470b();
                throw th;
            }
        } finally {
            this.f1041d.mo10093b(a);
        }
    }
}
