package com.bumptech.glide.load.p023n.p024a0;

import android.util.Log;
import androidx.annotation.Nullable;
import com.bumptech.glide.util.Preconditions;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

/* renamed from: com.bumptech.glide.load.n.a0.j */
public final class LruArrayPool implements ArrayPool {

    /* renamed from: a */
    private final GroupedLinkedMap<C0951a, Object> f1000a = new GroupedLinkedMap<>();

    /* renamed from: b */
    private final C0952b f1001b = new C0952b();

    /* renamed from: c */
    private final Map<Class<?>, NavigableMap<Integer, Integer>> f1002c = new HashMap();

    /* renamed from: d */
    private final Map<Class<?>, ArrayAdapterInterface<?>> f1003d = new HashMap();

    /* renamed from: e */
    private final int f1004e;

    /* renamed from: f */
    private int f1005f;

    /* renamed from: com.bumptech.glide.load.n.a0.j$b */
    /* compiled from: LruArrayPool */
    private static final class C0952b extends BaseKeyPool<C0951a> {
        C0952b() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C0951a mo10078a(int i, Class<?> cls) {
            C0951a aVar = (C0951a) mo10059b();
            aVar.mo10074a(i, cls);
            return aVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0951a m1560a() {
            return new C0951a(this);
        }
    }

    public LruArrayPool(int i) {
        this.f1004e = i;
    }

    /* renamed from: c */
    private boolean m1551c(int i) {
        return i <= this.f1004e / 2;
    }

    /* renamed from: a */
    public synchronized <T> T mo10039a(int i, Class<T> cls) {
        return m1544a(this.f1001b.mo10078a(i, cls), cls);
    }

    /* renamed from: b */
    public synchronized <T> T mo10042b(int i, Class<T> cls) {
        C0951a aVar;
        Integer ceilingKey = m1546b((Class<?>) cls).ceilingKey(Integer.valueOf(i));
        if (m1545a(i, ceilingKey)) {
            aVar = this.f1001b.mo10078a(ceilingKey.intValue(), cls);
        } else {
            aVar = this.f1001b.mo10078a(i, cls);
        }
        return m1544a(aVar, cls);
    }

    public synchronized <T> void put(T t) {
        Class<?> cls = t.getClass();
        ArrayAdapterInterface a = m1541a((Class) cls);
        int a2 = a.mo10036a(t);
        int a3 = a.mo10035a() * a2;
        if (m1551c(a3)) {
            C0951a a4 = this.f1001b.mo10078a(a2, cls);
            this.f1000a.mo10068a(a4, t);
            NavigableMap<Integer, Integer> b = m1546b(cls);
            Integer num = (Integer) b.get(Integer.valueOf(a4.f1007b));
            Integer valueOf = Integer.valueOf(a4.f1007b);
            int i = 1;
            if (num != null) {
                i = 1 + num.intValue();
            }
            b.put(valueOf, Integer.valueOf(i));
            this.f1005f += a3;
            m1547b();
        }
    }

    /* renamed from: com.bumptech.glide.load.n.a0.j$a */
    /* compiled from: LruArrayPool */
    private static final class C0951a implements Poolable {

        /* renamed from: a */
        private final C0952b f1006a;

        /* renamed from: b */
        int f1007b;

        /* renamed from: c */
        private Class<?> f1008c;

        C0951a(C0952b bVar) {
            this.f1006a = bVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10074a(int i, Class<?> cls) {
            this.f1007b = i;
            this.f1008c = cls;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0951a)) {
                return false;
            }
            C0951a aVar = (C0951a) obj;
            if (this.f1007b == aVar.f1007b && this.f1008c == aVar.f1008c) {
                return true;
            }
            return false;
        }

        public int hashCode() {
            int i = this.f1007b * 31;
            Class<?> cls = this.f1008c;
            return i + (cls != null ? cls.hashCode() : 0);
        }

        public String toString() {
            return "Key{size=" + this.f1007b + "array=" + this.f1008c + '}';
        }

        /* renamed from: a */
        public void mo10051a() {
            this.f1006a.mo10058a(this);
        }
    }

    /* renamed from: c */
    private boolean m1550c() {
        int i = this.f1005f;
        return i == 0 || this.f1004e / i >= 2;
    }

    /* renamed from: a */
    private <T> T m1544a(C0951a aVar, Class<T> cls) {
        ArrayAdapterInterface<T> a = m1541a((Class) cls);
        T a2 = m1543a(aVar);
        if (a2 != null) {
            this.f1005f -= a.mo10036a(a2) * a.mo10035a();
            m1549c(a.mo10036a(a2), cls);
        }
        if (a2 != null) {
            return a2;
        }
        if (Log.isLoggable(a.mo10037b(), 2)) {
            Log.v(a.mo10037b(), "Allocated " + aVar.f1007b + " bytes");
        }
        return a.newArray(aVar.f1007b);
    }

    /* renamed from: c */
    private void m1549c(int i, Class<?> cls) {
        NavigableMap<Integer, Integer> b = m1546b(cls);
        Integer num = (Integer) b.get(Integer.valueOf(i));
        if (num == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + i + ", this: " + this);
        } else if (num.intValue() == 1) {
            b.remove(Integer.valueOf(i));
        } else {
            b.put(Integer.valueOf(i), Integer.valueOf(num.intValue() - 1));
        }
    }

    /* renamed from: b */
    private void m1547b() {
        m1548b(this.f1004e);
    }

    /* renamed from: b */
    private void m1548b(int i) {
        while (this.f1005f > i) {
            Object a = this.f1000a.mo10066a();
            Preconditions.m2828a(a);
            ArrayAdapterInterface a2 = m1542a(a);
            this.f1005f -= a2.mo10036a(a) * a2.mo10035a();
            m1549c(a2.mo10036a(a), a.getClass());
            if (Log.isLoggable(a2.mo10037b(), 2)) {
                Log.v(a2.mo10037b(), "evicted: " + a2.mo10036a(a));
            }
        }
    }

    @Nullable
    /* renamed from: a */
    private <T> T m1543a(C0951a aVar) {
        return this.f1000a.mo10067a(aVar);
    }

    /* renamed from: a */
    private boolean m1545a(int i, Integer num) {
        return num != null && (m1550c() || num.intValue() <= i * 8);
    }

    /* renamed from: a */
    public synchronized void mo10040a() {
        m1548b(0);
    }

    /* renamed from: a */
    public synchronized void mo10041a(int i) {
        if (i >= 40) {
            try {
                mo10040a();
            } catch (Throwable th) {
                throw th;
            }
        } else if (i >= 20 || i == 15) {
            m1548b(this.f1004e / 2);
        }
    }

    /* renamed from: b */
    private NavigableMap<Integer, Integer> m1546b(Class<?> cls) {
        NavigableMap<Integer, Integer> navigableMap = this.f1002c.get(cls);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.f1002c.put(cls, treeMap);
        return treeMap;
    }

    /* renamed from: a */
    private <T> ArrayAdapterInterface<T> m1542a(T t) {
        return m1541a((Class) t.getClass());
    }

    /* renamed from: a */
    private <T> ArrayAdapterInterface<T> m1541a(Class<T> cls) {
        ArrayAdapterInterface<T> aVar = this.f1003d.get(cls);
        if (aVar == null) {
            if (cls.equals(int[].class)) {
                aVar = new IntegerArrayAdapter();
            } else if (cls.equals(byte[].class)) {
                aVar = new ByteArrayAdapter();
            } else {
                throw new IllegalArgumentException("No array pool found for: " + cls.getSimpleName());
            }
            this.f1003d.put(cls, aVar);
        }
        return aVar;
    }
}
