package com.bumptech.glide.p039p;

import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.bumptech.glide.load.p023n.DecodePath;
import com.bumptech.glide.load.p023n.LoadPath;
import com.bumptech.glide.load.p030p.p035g.UnitTranscoder;
import com.bumptech.glide.util.MultiClassKey;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.bumptech.glide.p.c */
public class LoadPathCache {

    /* renamed from: c */
    private static final LoadPath<?, ?, ?> f1649c = new LoadPath(Object.class, Object.class, Object.class, Collections.singletonList(new DecodePath(Object.class, Object.class, Object.class, Collections.emptyList(), new UnitTranscoder(), null)), null);

    /* renamed from: a */
    private final ArrayMap<MultiClassKey, LoadPath<?, ?, ?>> f1650a = new ArrayMap<>();

    /* renamed from: b */
    private final AtomicReference<MultiClassKey> f1651b = new AtomicReference<>();

    /* renamed from: b */
    private MultiClassKey m2558b(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        MultiClassKey andSet = this.f1651b.getAndSet(null);
        if (andSet == null) {
            andSet = new MultiClassKey();
        }
        andSet.mo10710a(cls, cls2, cls3);
        return andSet;
    }

    /* renamed from: a */
    public boolean mo10559a(@Nullable LoadPath<?, ?, ?> tVar) {
        return f1649c.equals(tVar);
    }

    @Nullable
    /* renamed from: a */
    public <Data, TResource, Transcode> LoadPath<Data, TResource, Transcode> mo10557a(Class<Data> cls, Class<TResource> cls2, Class<Transcode> cls3) {
        LoadPath<Data, TResource, Transcode> tVar;
        MultiClassKey b = m2558b(cls, cls2, cls3);
        synchronized (this.f1650a) {
            tVar = this.f1650a.get(b);
        }
        this.f1651b.set(b);
        return tVar;
    }

    /* renamed from: a */
    public void mo10558a(Class<?> cls, Class<?> cls2, Class<?> cls3, @Nullable LoadPath<?, ?, ?> tVar) {
        synchronized (this.f1650a) {
            ArrayMap<MultiClassKey, LoadPath<?, ?, ?>> arrayMap = this.f1650a;
            MultiClassKey hVar = new MultiClassKey(cls, cls2, cls3);
            if (tVar == null) {
                tVar = f1649c;
            }
            arrayMap.put(hVar, tVar);
        }
    }
}
