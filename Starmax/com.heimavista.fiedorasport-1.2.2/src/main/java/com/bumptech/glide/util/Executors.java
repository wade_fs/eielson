package com.bumptech.glide.util;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: com.bumptech.glide.util.d */
public final class Executors {

    /* renamed from: a */
    private static final Executor f1777a = new C1120a();

    /* renamed from: b */
    private static final Executor f1778b = new C1121b();

    /* renamed from: com.bumptech.glide.util.d$a */
    /* compiled from: Executors */
    class C1120a implements Executor {

        /* renamed from: a */
        private final Handler f1779a = new Handler(Looper.getMainLooper());

        C1120a() {
        }

        public void execute(@NonNull Runnable runnable) {
            this.f1779a.post(runnable);
        }
    }

    /* renamed from: com.bumptech.glide.util.d$b */
    /* compiled from: Executors */
    class C1121b implements Executor {
        C1121b() {
        }

        public void execute(@NonNull Runnable runnable) {
            runnable.run();
        }
    }

    /* renamed from: a */
    public static Executor m2812a() {
        return f1778b;
    }

    /* renamed from: b */
    public static Executor m2813b() {
        return f1777a;
    }
}
