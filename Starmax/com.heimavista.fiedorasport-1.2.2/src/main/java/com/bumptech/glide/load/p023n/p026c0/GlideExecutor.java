package com.bumptech.glide.load.p023n.p026c0;

import android.os.Process;
import android.os.StrictMode;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.facebook.share.internal.ShareConstants;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/* renamed from: com.bumptech.glide.load.n.c0.a */
public final class GlideExecutor implements ExecutorService {

    /* renamed from: b */
    private static final long f1073b = TimeUnit.SECONDS.toMillis(10);

    /* renamed from: c */
    private static volatile int f1074c;

    /* renamed from: a */
    private final ExecutorService f1075a;

    /* renamed from: com.bumptech.glide.load.n.c0.a$a */
    /* compiled from: GlideExecutor */
    private static final class C0970a implements ThreadFactory {

        /* renamed from: P */
        private final String f1076P;

        /* renamed from: Q */
        final C0972b f1077Q;

        /* renamed from: R */
        final boolean f1078R;

        /* renamed from: S */
        private int f1079S;

        /* renamed from: com.bumptech.glide.load.n.c0.a$a$a */
        /* compiled from: GlideExecutor */
        class C0971a extends Thread {
            C0971a(Runnable runnable, String str) {
                super(runnable, str);
            }

            public void run() {
                Process.setThreadPriority(9);
                if (C0970a.this.f1078R) {
                    StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectNetwork().penaltyDeath().build());
                }
                try {
                    super.run();
                } catch (Throwable th) {
                    C0970a.this.f1077Q.mo10134a(th);
                }
            }
        }

        C0970a(String str, C0972b bVar, boolean z) {
            this.f1076P = str;
            this.f1077Q = bVar;
            this.f1078R = z;
        }

        public synchronized Thread newThread(@NonNull Runnable runnable) {
            C0971a aVar;
            aVar = new C0971a(runnable, "glide-" + this.f1076P + "-thread-" + this.f1079S);
            this.f1079S = this.f1079S + 1;
            return aVar;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.c0.a$b */
    /* compiled from: GlideExecutor */
    public interface C0972b {

        /* renamed from: a */
        public static final C0972b f1081a = new C0974b();

        /* renamed from: b */
        public static final C0972b f1082b = f1081a;

        /* renamed from: com.bumptech.glide.load.n.c0.a$b$a */
        /* compiled from: GlideExecutor */
        class C0973a implements C0972b {
            C0973a() {
            }

            /* renamed from: a */
            public void mo10134a(Throwable th) {
            }
        }

        /* renamed from: com.bumptech.glide.load.n.c0.a$b$b */
        /* compiled from: GlideExecutor */
        class C0974b implements C0972b {
            C0974b() {
            }

            /* renamed from: a */
            public void mo10134a(Throwable th) {
                if (th != null && Log.isLoggable("GlideExecutor", 6)) {
                    Log.e("GlideExecutor", "Request threw uncaught throwable", th);
                }
            }
        }

        /* renamed from: com.bumptech.glide.load.n.c0.a$b$c */
        /* compiled from: GlideExecutor */
        class C0975c implements C0972b {
            C0975c() {
            }

            /* renamed from: a */
            public void mo10134a(Throwable th) {
                if (th != null) {
                    throw new RuntimeException("Request threw uncaught throwable", th);
                }
            }
        }

        static {
            new C0973a();
            new C0975c();
        }

        /* renamed from: a */
        void mo10134a(Throwable th);
    }

    @VisibleForTesting
    GlideExecutor(ExecutorService executorService) {
        this.f1075a = executorService;
    }

    /* renamed from: a */
    public static GlideExecutor m1656a(int i, String str, C0972b bVar) {
        return new GlideExecutor(new ThreadPoolExecutor(i, i, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new C0970a(str, bVar, true)));
    }

    /* renamed from: b */
    public static GlideExecutor m1658b(int i, String str, C0972b bVar) {
        return new GlideExecutor(new ThreadPoolExecutor(i, i, 0, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new C0970a(str, bVar, false)));
    }

    /* renamed from: c */
    public static GlideExecutor m1659c() {
        return m1656a(1, "disk-cache", C0972b.f1082b);
    }

    /* renamed from: d */
    public static GlideExecutor m1660d() {
        return m1658b(m1654a(), ShareConstants.FEED_SOURCE_PARAM, C0972b.f1082b);
    }

    /* renamed from: e */
    public static GlideExecutor m1661e() {
        return new GlideExecutor(new ThreadPoolExecutor(0, Integer.MAX_VALUE, f1073b, TimeUnit.MILLISECONDS, new SynchronousQueue(), new C0970a("source-unlimited", C0972b.f1082b, false)));
    }

    public boolean awaitTermination(long j, @NonNull TimeUnit timeUnit) {
        return this.f1075a.awaitTermination(j, timeUnit);
    }

    public void execute(@NonNull Runnable runnable) {
        this.f1075a.execute(runnable);
    }

    @NonNull
    public <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection) {
        return this.f1075a.invokeAll(collection);
    }

    @NonNull
    public <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection) {
        return this.f1075a.invokeAny(collection);
    }

    public boolean isShutdown() {
        return this.f1075a.isShutdown();
    }

    public boolean isTerminated() {
        return this.f1075a.isTerminated();
    }

    public void shutdown() {
        this.f1075a.shutdown();
    }

    @NonNull
    public List<Runnable> shutdownNow() {
        return this.f1075a.shutdownNow();
    }

    @NonNull
    public Future<?> submit(@NonNull Runnable runnable) {
        return this.f1075a.submit(runnable);
    }

    public String toString() {
        return this.f1075a.toString();
    }

    /* renamed from: a */
    public static GlideExecutor m1655a(int i, C0972b bVar) {
        return new GlideExecutor(new ThreadPoolExecutor(0, i, f1073b, TimeUnit.MILLISECONDS, new PriorityBlockingQueue(), new C0970a("animation", bVar, true)));
    }

    /* renamed from: b */
    public static GlideExecutor m1657b() {
        return m1655a(m1654a() >= 4 ? 2 : 1, C0972b.f1082b);
    }

    @NonNull
    public <T> List<Future<T>> invokeAll(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.f1075a.invokeAll(collection, j, timeUnit);
    }

    public <T> T invokeAny(@NonNull Collection<? extends Callable<T>> collection, long j, @NonNull TimeUnit timeUnit) {
        return this.f1075a.invokeAny(collection, j, timeUnit);
    }

    @NonNull
    public <T> Future<T> submit(@NonNull Runnable runnable, T t) {
        return this.f1075a.submit(runnable, t);
    }

    /* renamed from: a */
    public static int m1654a() {
        if (f1074c == 0) {
            f1074c = Math.min(4, RuntimeCompat.m1666a());
        }
        return f1074c;
    }

    public <T> Future<T> submit(@NonNull Callable<T> callable) {
        return this.f1075a.submit(callable);
    }
}
