package com.bumptech.glide.load.p030p.p035g;

import androidx.annotation.NonNull;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.load.p.g.f */
public class TranscoderRegistry {

    /* renamed from: a */
    private final List<C1087a<?, ?>> f1470a = new ArrayList();

    /* renamed from: com.bumptech.glide.load.p.g.f$a */
    /* compiled from: TranscoderRegistry */
    private static final class C1087a<Z, R> {

        /* renamed from: a */
        private final Class<Z> f1471a;

        /* renamed from: b */
        private final Class<R> f1472b;

        /* renamed from: c */
        final ResourceTranscoder<Z, R> f1473c;

        C1087a(@NonNull Class<Z> cls, @NonNull Class<R> cls2, @NonNull ResourceTranscoder<Z, R> eVar) {
            this.f1471a = cls;
            this.f1472b = cls2;
            this.f1473c = eVar;
        }

        /* renamed from: a */
        public boolean mo10403a(@NonNull Class<?> cls, @NonNull Class<?> cls2) {
            return this.f1471a.isAssignableFrom(cls) && cls2.isAssignableFrom(this.f1472b);
        }
    }

    /* renamed from: a */
    public synchronized <Z, R> void mo10401a(@NonNull Class<Z> cls, @NonNull Class<R> cls2, @NonNull ResourceTranscoder<Z, R> eVar) {
        this.f1470a.add(new C1087a(cls, cls2, eVar));
    }

    @NonNull
    /* renamed from: b */
    public synchronized <Z, R> List<Class<R>> mo10402b(@NonNull Class<Z> cls, @NonNull Class<R> cls2) {
        ArrayList arrayList = new ArrayList();
        if (cls2.isAssignableFrom(cls)) {
            arrayList.add(cls2);
            return arrayList;
        }
        for (C1087a<?, ?> aVar : this.f1470a) {
            if (aVar.mo10403a(cls, cls2)) {
                arrayList.add(cls2);
            }
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public synchronized <Z, R> ResourceTranscoder<Z, R> mo10400a(@NonNull Class<Z> cls, @NonNull Class<R> cls2) {
        if (cls2.isAssignableFrom(cls)) {
            return UnitTranscoder.m2289a();
        }
        for (C1087a aVar : this.f1470a) {
            if (aVar.mo10403a(cls, cls2)) {
                return aVar.f1473c;
            }
        }
        throw new IllegalArgumentException("No transcoder registered to transcode from " + cls + " to " + cls2);
    }
}
