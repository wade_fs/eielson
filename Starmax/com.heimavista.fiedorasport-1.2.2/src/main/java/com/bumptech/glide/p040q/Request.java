package com.bumptech.glide.p040q;

/* renamed from: com.bumptech.glide.q.d */
public interface Request {
    /* renamed from: a */
    boolean mo10624a(Request dVar);

    /* renamed from: c */
    void mo10626c();

    void clear();

    /* renamed from: e */
    boolean mo10631e();

    /* renamed from: f */
    boolean mo10632f();

    /* renamed from: g */
    boolean mo10634g();

    /* renamed from: h */
    boolean mo10635h();

    boolean isRunning();

    void recycle();
}
