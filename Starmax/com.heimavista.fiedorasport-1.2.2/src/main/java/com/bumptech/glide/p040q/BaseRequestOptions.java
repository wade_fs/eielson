package com.bumptech.glide.p040q;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import androidx.annotation.CheckResult;
import androidx.annotation.DrawableRes;
import androidx.annotation.FloatRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.DiskCacheStrategy;
import com.bumptech.glide.load.p030p.p031c.CenterCrop;
import com.bumptech.glide.load.p030p.p031c.CenterInside;
import com.bumptech.glide.load.p030p.p031c.CircleCrop;
import com.bumptech.glide.load.p030p.p031c.DownsampleStrategy;
import com.bumptech.glide.load.p030p.p031c.DrawableTransformation;
import com.bumptech.glide.load.p030p.p031c.FitCenter;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.load.resource.gif.GifDrawableTransformation;
import com.bumptech.glide.p040q.BaseRequestOptions;
import com.bumptech.glide.p043r.EmptySignature;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.CachedHashCodeArrayMap;
import com.bumptech.glide.util.Preconditions;
import java.util.Map;

/* renamed from: com.bumptech.glide.q.a */
public abstract class BaseRequestOptions<T extends BaseRequestOptions<T>> implements Cloneable {

    /* renamed from: P */
    private int f1662P;

    /* renamed from: Q */
    private float f1663Q = 1.0f;
    @NonNull

    /* renamed from: R */
    private DiskCacheStrategy f1664R = DiskCacheStrategy.f1167c;
    @NonNull

    /* renamed from: S */
    private Priority f1665S = Priority.NORMAL;
    @Nullable

    /* renamed from: T */
    private Drawable f1666T;

    /* renamed from: U */
    private int f1667U;
    @Nullable

    /* renamed from: V */
    private Drawable f1668V;

    /* renamed from: W */
    private int f1669W;

    /* renamed from: X */
    private boolean f1670X = true;

    /* renamed from: Y */
    private int f1671Y = -1;

    /* renamed from: Z */
    private int f1672Z = -1;
    @NonNull

    /* renamed from: a0 */
    private Key f1673a0 = EmptySignature.m2798a();

    /* renamed from: b0 */
    private boolean f1674b0;

    /* renamed from: c0 */
    private boolean f1675c0 = true;
    @Nullable

    /* renamed from: d0 */
    private Drawable f1676d0;

    /* renamed from: e0 */
    private int f1677e0;
    @NonNull

    /* renamed from: f0 */
    private Options f1678f0 = new Options();
    @NonNull

    /* renamed from: g0 */
    private Map<Class<?>, Transformation<?>> f1679g0 = new CachedHashCodeArrayMap();
    @NonNull

    /* renamed from: h0 */
    private Class<?> f1680h0 = Object.class;

    /* renamed from: i0 */
    private boolean f1681i0;
    @Nullable

    /* renamed from: j0 */
    private Resources.Theme f1682j0;

    /* renamed from: k0 */
    private boolean f1683k0;

    /* renamed from: l0 */
    private boolean f1684l0;

    /* renamed from: m0 */
    private boolean f1685m0;

    /* renamed from: n0 */
    private boolean f1686n0 = true;

    /* renamed from: o0 */
    private boolean f1687o0;

    /* renamed from: I */
    private T mo9931I() {
        return this;
    }

    @NonNull
    /* renamed from: J */
    private T m2574J() {
        if (!this.f1681i0) {
            mo9931I();
            return this;
        }
        throw new IllegalStateException("You cannot modify locked T, consider clone()");
    }

    /* renamed from: b */
    private static boolean mo9940b(int i, int i2) {
        return (i & i2) != 0;
    }

    /* renamed from: A */
    public final boolean mo10570A() {
        return this.f1675c0;
    }

    /* renamed from: B */
    public final boolean mo10571B() {
        return this.f1674b0;
    }

    /* renamed from: C */
    public final boolean mo10572C() {
        return m2578d(2048);
    }

    /* renamed from: D */
    public final boolean mo10573D() {
        return C1122j.m2849b(this.f1672Z, this.f1671Y);
    }

    @NonNull
    /* renamed from: E */
    public T mo10574E() {
        this.f1681i0 = true;
        mo9931I();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: F */
    public T mo10575F() {
        return mo10589a(DownsampleStrategy.f1403b, new CenterCrop());
    }

    @CheckResult
    @NonNull
    /* renamed from: G */
    public T mo10576G() {
        return m2577c(DownsampleStrategy.f1404c, new CenterInside());
    }

    @CheckResult
    @NonNull
    /* renamed from: H */
    public T mo10577H() {
        return m2577c(DownsampleStrategy.f1402a, new FitCenter());
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10579a(@FloatRange(from = 0.0d, mo446to = 1.0d) float f) {
        if (this.f1683k0) {
            return clone().mo10579a(f);
        }
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("sizeMultiplier must be between 0 and 1");
        }
        this.f1663Q = f;
        this.f1662P |= 2;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: b */
    public T mo10596b(boolean z) {
        if (this.f1683k0) {
            return clone().mo10596b(z);
        }
        this.f1687o0 = z;
        this.f1662P |= 1048576;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: c */
    public T mo10598c(@DrawableRes int i) {
        if (this.f1683k0) {
            return clone().mo10598c(i);
        }
        this.f1669W = i;
        this.f1662P |= 128;
        this.f1668V = null;
        this.f1662P &= -65;
        m2574J();
        return this;
    }

    /* renamed from: d */
    public final int mo10599d() {
        return this.f1667U;
    }

    @Nullable
    /* renamed from: e */
    public final Drawable mo10600e() {
        return this.f1666T;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BaseRequestOptions)) {
            return false;
        }
        BaseRequestOptions aVar = (BaseRequestOptions) obj;
        if (Float.compare(aVar.f1663Q, this.f1663Q) == 0 && this.f1667U == aVar.f1667U && C1122j.m2850b(this.f1666T, aVar.f1666T) && this.f1669W == aVar.f1669W && C1122j.m2850b(this.f1668V, aVar.f1668V) && this.f1677e0 == aVar.f1677e0 && C1122j.m2850b(this.f1676d0, aVar.f1676d0) && this.f1670X == aVar.f1670X && this.f1671Y == aVar.f1671Y && this.f1672Z == aVar.f1672Z && this.f1674b0 == aVar.f1674b0 && this.f1675c0 == aVar.f1675c0 && this.f1684l0 == aVar.f1684l0 && this.f1685m0 == aVar.f1685m0 && this.f1664R.equals(aVar.f1664R) && this.f1665S == aVar.f1665S && this.f1678f0.equals(aVar.f1678f0) && this.f1679g0.equals(aVar.f1679g0) && this.f1680h0.equals(aVar.f1680h0) && C1122j.m2850b(this.f1673a0, aVar.f1673a0) && C1122j.m2850b(this.f1682j0, aVar.f1682j0)) {
            return true;
        }
        return false;
    }

    @Nullable
    /* renamed from: f */
    public final Drawable mo10602f() {
        return this.f1676d0;
    }

    /* renamed from: g */
    public final int mo10603g() {
        return this.f1677e0;
    }

    /* renamed from: h */
    public final boolean mo10604h() {
        return this.f1685m0;
    }

    public int hashCode() {
        return C1122j.m2839a(this.f1682j0, C1122j.m2839a(this.f1673a0, C1122j.m2839a(this.f1680h0, C1122j.m2839a(this.f1679g0, C1122j.m2839a(this.f1678f0, C1122j.m2839a(this.f1665S, C1122j.m2839a(this.f1664R, C1122j.m2840a(this.f1685m0, C1122j.m2840a(this.f1684l0, C1122j.m2840a(this.f1675c0, C1122j.m2840a(this.f1674b0, C1122j.m2835a(this.f1672Z, C1122j.m2835a(this.f1671Y, C1122j.m2840a(this.f1670X, C1122j.m2839a(this.f1676d0, C1122j.m2835a(this.f1677e0, C1122j.m2839a(this.f1668V, C1122j.m2835a(this.f1669W, C1122j.m2839a(this.f1666T, C1122j.m2835a(this.f1667U, C1122j.m2833a(this.f1663Q)))))))))))))))))))));
    }

    @NonNull
    /* renamed from: i */
    public final Options mo10606i() {
        return this.f1678f0;
    }

    /* renamed from: j */
    public final int mo10607j() {
        return this.f1671Y;
    }

    /* renamed from: k */
    public final int mo10608k() {
        return this.f1672Z;
    }

    @Nullable
    /* renamed from: l */
    public final Drawable mo10609l() {
        return this.f1668V;
    }

    /* renamed from: m */
    public final int mo10610m() {
        return this.f1669W;
    }

    @NonNull
    /* renamed from: n */
    public final Priority mo10611n() {
        return this.f1665S;
    }

    @NonNull
    /* renamed from: o */
    public final Class<?> mo10612o() {
        return this.f1680h0;
    }

    @NonNull
    /* renamed from: p */
    public final Key mo10613p() {
        return this.f1673a0;
    }

    /* renamed from: q */
    public final float mo10614q() {
        return this.f1663Q;
    }

    @Nullable
    /* renamed from: t */
    public final Resources.Theme mo10615t() {
        return this.f1682j0;
    }

    @NonNull
    /* renamed from: u */
    public final Map<Class<?>, Transformation<?>> mo10616u() {
        return this.f1679g0;
    }

    /* renamed from: v */
    public final boolean mo10617v() {
        return this.f1687o0;
    }

    /* renamed from: w */
    public final boolean mo10618w() {
        return this.f1684l0;
    }

    /* renamed from: x */
    public final boolean mo10619x() {
        return this.f1670X;
    }

    /* renamed from: y */
    public final boolean mo10620y() {
        return m2578d(8);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: z */
    public boolean mo10621z() {
        return this.f1686n0;
    }

    /* renamed from: d */
    private boolean m2578d(int i) {
        return mo9940b(this.f1662P, i);
    }

    @CheckResult
    public T clone() {
        try {
            T t = (BaseRequestOptions) super.clone();
            t.f1678f0 = new Options();
            t.f1678f0.mo9977a(this.f1678f0);
            t.f1679g0 = new CachedHashCodeArrayMap();
            t.f1679g0.putAll(this.f1679g0);
            t.f1681i0 = false;
            t.f1683k0 = false;
            return t;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    @CheckResult
    @NonNull
    /* renamed from: b */
    public T mo10594b(int i) {
        return mo10581a(i, i);
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10587a(@NonNull DiskCacheStrategy jVar) {
        if (this.f1683k0) {
            return clone().mo10587a(jVar);
        }
        Preconditions.m2828a(jVar);
        this.f1664R = jVar;
        this.f1662P |= 4;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: b */
    public T mo10593b() {
        return mo10595b(DownsampleStrategy.f1404c, new CircleCrop());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.q.a.a(com.bumptech.glide.load.p.c.k, com.bumptech.glide.load.l<android.graphics.Bitmap>, boolean):T
     arg types: [com.bumptech.glide.load.p.c.k, com.bumptech.glide.load.l<android.graphics.Bitmap>, int]
     candidates:
      com.bumptech.glide.q.a.a(java.lang.Class, com.bumptech.glide.load.l, boolean):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.p.c.k, com.bumptech.glide.load.l<android.graphics.Bitmap>, boolean):T */
    @NonNull
    /* renamed from: c */
    private T m2577c(@NonNull DownsampleStrategy kVar, @NonNull Transformation<Bitmap> lVar) {
        return m2575a(kVar, lVar, false);
    }

    /* access modifiers changed from: package-private */
    @CheckResult
    @NonNull
    /* renamed from: b */
    public final T mo10595b(@NonNull DownsampleStrategy kVar, @NonNull Transformation<Bitmap> lVar) {
        if (this.f1683k0) {
            return clone().mo10595b(kVar, lVar);
        }
        mo10588a(kVar);
        return mo10585a(lVar);
    }

    @NonNull
    /* renamed from: c */
    public final DiskCacheStrategy mo10597c() {
        return this.f1664R;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10582a(@NonNull Priority hVar) {
        if (this.f1683k0) {
            return clone().mo10582a(hVar);
        }
        Preconditions.m2828a(hVar);
        this.f1665S = hVar;
        this.f1662P |= 8;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10580a(@DrawableRes int i) {
        if (this.f1683k0) {
            return clone().mo10580a(i);
        }
        this.f1667U = i;
        this.f1662P |= 32;
        this.f1666T = null;
        this.f1662P &= -17;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10592a(boolean z) {
        if (this.f1683k0) {
            return clone().mo10592a(true);
        }
        this.f1670X = !z;
        this.f1662P |= 256;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10581a(int i, int i2) {
        if (this.f1683k0) {
            return clone().mo10581a(i, i2);
        }
        this.f1672Z = i;
        this.f1671Y = i2;
        this.f1662P |= 512;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10583a(@NonNull Key gVar) {
        if (this.f1683k0) {
            return clone().mo10583a(gVar);
        }
        Preconditions.m2828a(gVar);
        this.f1673a0 = gVar;
        this.f1662P |= 1024;
        m2574J();
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [com.bumptech.glide.load.h<Y>, java.lang.Object, com.bumptech.glide.load.h] */
    /* JADX WARNING: Unknown variable types count: 1 */
    @androidx.annotation.CheckResult
    @androidx.annotation.NonNull
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Y> T mo10584a(@androidx.annotation.NonNull com.bumptech.glide.load.Option<Y> r2, @androidx.annotation.NonNull Y r3) {
        /*
            r1 = this;
            boolean r0 = r1.f1683k0
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.q.a r0 = r1.clone()
            com.bumptech.glide.q.a r2 = r0.mo10584a(r2, r3)
            return r2
        L_0x000d:
            com.bumptech.glide.util.Preconditions.m2828a(r2)
            com.bumptech.glide.util.Preconditions.m2828a(r3)
            com.bumptech.glide.load.i r0 = r1.f1678f0
            r0.mo9975a(r2, r3)
            r1.m2574J()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.BaseRequestOptions.mo10584a(com.bumptech.glide.load.h, java.lang.Object):com.bumptech.glide.q.a");
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10590a(@NonNull Class<?> cls) {
        if (this.f1683k0) {
            return clone().mo10590a(cls);
        }
        Preconditions.m2828a(cls);
        this.f1680h0 = cls;
        this.f1662P |= 4096;
        m2574J();
        return this;
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10588a(@NonNull DownsampleStrategy kVar) {
        Option<DownsampleStrategy> hVar = DownsampleStrategy.f1407f;
        Preconditions.m2828a(kVar);
        return mo10584a(hVar, kVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.q.a.a(com.bumptech.glide.load.l<android.graphics.Bitmap>, boolean):T
     arg types: [com.bumptech.glide.load.l<android.graphics.Bitmap>, int]
     candidates:
      com.bumptech.glide.q.a.a(int, int):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.h, java.lang.Object):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.p.c.k, com.bumptech.glide.load.l<android.graphics.Bitmap>):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.l<android.graphics.Bitmap>, boolean):T */
    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public final T mo10589a(@NonNull DownsampleStrategy kVar, @NonNull Transformation<Bitmap> lVar) {
        if (this.f1683k0) {
            return clone().mo10589a(kVar, lVar);
        }
        mo10588a(kVar);
        return mo10586a(lVar, false);
    }

    @NonNull
    /* renamed from: a */
    private T m2575a(@NonNull DownsampleStrategy kVar, @NonNull Transformation<Bitmap> lVar, boolean z) {
        T b = z ? mo10595b(kVar, lVar) : mo10589a(kVar, lVar);
        b.f1686n0 = true;
        return b;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.q.a.a(com.bumptech.glide.load.l<android.graphics.Bitmap>, boolean):T
     arg types: [com.bumptech.glide.load.l<android.graphics.Bitmap>, int]
     candidates:
      com.bumptech.glide.q.a.a(int, int):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.h, java.lang.Object):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.p.c.k, com.bumptech.glide.load.l<android.graphics.Bitmap>):T
      com.bumptech.glide.q.a.a(com.bumptech.glide.load.l<android.graphics.Bitmap>, boolean):T */
    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo10585a(@NonNull Transformation<Bitmap> lVar) {
        return mo10586a(lVar, true);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public T mo10586a(@NonNull Transformation<Bitmap> lVar, boolean z) {
        if (this.f1683k0) {
            return clone().mo10586a(lVar, z);
        }
        DrawableTransformation nVar = new DrawableTransformation(lVar, z);
        mo10591a(Bitmap.class, lVar, z);
        mo10591a(Drawable.class, nVar, z);
        nVar.mo10362a();
        mo10591a(BitmapDrawable.class, nVar, z);
        mo10591a(GifDrawable.class, new GifDrawableTransformation(lVar), z);
        m2574J();
        return this;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [java.lang.Class<Y>, java.lang.Object, java.lang.Class] */
    /* JADX WARN: Type inference failed for: r3v0, types: [com.bumptech.glide.load.l, com.bumptech.glide.load.l<Y>, java.lang.Object] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Unknown variable types count: 2 */
    @androidx.annotation.NonNull
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Y> T mo10591a(@androidx.annotation.NonNull java.lang.Class<Y> r2, @androidx.annotation.NonNull com.bumptech.glide.load.Transformation<Y> r3, boolean r4) {
        /*
            r1 = this;
            boolean r0 = r1.f1683k0
            if (r0 == 0) goto L_0x000d
            com.bumptech.glide.q.a r0 = r1.clone()
            com.bumptech.glide.q.a r2 = r0.mo10591a(r2, r3, r4)
            return r2
        L_0x000d:
            com.bumptech.glide.util.Preconditions.m2828a(r2)
            com.bumptech.glide.util.Preconditions.m2828a(r3)
            java.util.Map<java.lang.Class<?>, com.bumptech.glide.load.l<?>> r0 = r1.f1679g0
            r0.put(r2, r3)
            int r2 = r1.f1662P
            r2 = r2 | 2048(0x800, float:2.87E-42)
            r1.f1662P = r2
            r2 = 1
            r1.f1675c0 = r2
            int r3 = r1.f1662P
            r0 = 65536(0x10000, float:9.18355E-41)
            r3 = r3 | r0
            r1.f1662P = r3
            r3 = 0
            r1.f1686n0 = r3
            if (r4 == 0) goto L_0x0036
            int r3 = r1.f1662P
            r4 = 131072(0x20000, float:1.83671E-40)
            r3 = r3 | r4
            r1.f1662P = r3
            r1.f1674b0 = r2
        L_0x0036:
            r1.m2574J()
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.BaseRequestOptions.mo10591a(java.lang.Class, com.bumptech.glide.load.l, boolean):com.bumptech.glide.q.a");
    }

    @CheckResult
    @NonNull
    /* renamed from: a */
    public T mo9932a(@NonNull BaseRequestOptions<?> aVar) {
        if (this.f1683k0) {
            return clone().mo9932a(aVar);
        }
        if (mo9940b(aVar.f1662P, 2)) {
            this.f1663Q = aVar.f1663Q;
        }
        if (mo9940b(aVar.f1662P, 262144)) {
            this.f1684l0 = aVar.f1684l0;
        }
        if (mo9940b(aVar.f1662P, 1048576)) {
            this.f1687o0 = aVar.f1687o0;
        }
        if (mo9940b(aVar.f1662P, 4)) {
            this.f1664R = aVar.f1664R;
        }
        if (mo9940b(aVar.f1662P, 8)) {
            this.f1665S = aVar.f1665S;
        }
        if (mo9940b(aVar.f1662P, 16)) {
            this.f1666T = aVar.f1666T;
            this.f1667U = 0;
            this.f1662P &= -33;
        }
        if (mo9940b(aVar.f1662P, 32)) {
            this.f1667U = aVar.f1667U;
            this.f1666T = null;
            this.f1662P &= -17;
        }
        if (mo9940b(aVar.f1662P, 64)) {
            this.f1668V = aVar.f1668V;
            this.f1669W = 0;
            this.f1662P &= -129;
        }
        if (mo9940b(aVar.f1662P, 128)) {
            this.f1669W = aVar.f1669W;
            this.f1668V = null;
            this.f1662P &= -65;
        }
        if (mo9940b(aVar.f1662P, 256)) {
            this.f1670X = aVar.f1670X;
        }
        if (mo9940b(aVar.f1662P, 512)) {
            this.f1672Z = aVar.f1672Z;
            this.f1671Y = aVar.f1671Y;
        }
        if (mo9940b(aVar.f1662P, 1024)) {
            this.f1673a0 = aVar.f1673a0;
        }
        if (mo9940b(aVar.f1662P, 4096)) {
            this.f1680h0 = aVar.f1680h0;
        }
        if (mo9940b(aVar.f1662P, 8192)) {
            this.f1676d0 = aVar.f1676d0;
            this.f1677e0 = 0;
            this.f1662P &= -16385;
        }
        if (mo9940b(aVar.f1662P, 16384)) {
            this.f1677e0 = aVar.f1677e0;
            this.f1676d0 = null;
            this.f1662P &= -8193;
        }
        if (mo9940b(aVar.f1662P, 32768)) {
            this.f1682j0 = aVar.f1682j0;
        }
        if (mo9940b(aVar.f1662P, 65536)) {
            this.f1675c0 = aVar.f1675c0;
        }
        if (mo9940b(aVar.f1662P, 131072)) {
            this.f1674b0 = aVar.f1674b0;
        }
        if (mo9940b(aVar.f1662P, 2048)) {
            this.f1679g0.putAll(aVar.f1679g0);
            this.f1686n0 = aVar.f1686n0;
        }
        if (mo9940b(aVar.f1662P, 524288)) {
            this.f1685m0 = aVar.f1685m0;
        }
        if (!this.f1675c0) {
            this.f1679g0.clear();
            this.f1662P &= -2049;
            this.f1674b0 = false;
            this.f1662P &= -131073;
            this.f1686n0 = true;
        }
        this.f1662P |= aVar.f1662P;
        this.f1678f0.mo9977a(aVar.f1678f0);
        m2574J();
        return this;
    }

    @NonNull
    /* renamed from: a */
    public T mo10578a() {
        if (!this.f1681i0 || this.f1683k0) {
            this.f1683k0 = true;
            mo10574E();
            return this;
        }
        throw new IllegalStateException("You cannot auto lock an already locked options object, try clone() first");
    }
}
