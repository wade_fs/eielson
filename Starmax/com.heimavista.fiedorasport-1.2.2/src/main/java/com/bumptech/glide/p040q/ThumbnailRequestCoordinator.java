package com.bumptech.glide.p040q;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;

/* renamed from: com.bumptech.glide.q.k */
public class ThumbnailRequestCoordinator implements RequestCoordinator, Request {
    @Nullable

    /* renamed from: P */
    private final RequestCoordinator f1739P;

    /* renamed from: Q */
    private Request f1740Q;

    /* renamed from: R */
    private Request f1741R;

    /* renamed from: S */
    private boolean f1742S;

    @VisibleForTesting
    ThumbnailRequestCoordinator() {
        this(null);
    }

    /* renamed from: b */
    private boolean m2715b() {
        RequestCoordinator eVar = this.f1739P;
        return eVar == null || eVar.mo10633f(this);
    }

    /* renamed from: i */
    private boolean m2717i() {
        RequestCoordinator eVar = this.f1739P;
        return eVar == null || eVar.mo10629d(this);
    }

    /* renamed from: j */
    private boolean m2718j() {
        RequestCoordinator eVar = this.f1739P;
        return eVar != null && eVar.mo10623a();
    }

    /* renamed from: a */
    public void mo10657a(Request dVar, Request dVar2) {
        this.f1740Q = dVar;
        this.f1741R = dVar2;
    }

    /* renamed from: c */
    public boolean mo10627c(Request dVar) {
        return m2716d() && dVar.equals(this.f1740Q) && !mo10623a();
    }

    public void clear() {
        this.f1742S = false;
        this.f1741R.clear();
        this.f1740Q.clear();
    }

    /* renamed from: d */
    public boolean mo10629d(Request dVar) {
        return m2717i() && (dVar.equals(this.f1740Q) || !this.f1740Q.mo10631e());
    }

    /* renamed from: e */
    public void mo10630e(Request dVar) {
        if (!dVar.equals(this.f1741R)) {
            RequestCoordinator eVar = this.f1739P;
            if (eVar != null) {
                eVar.mo10630e(this);
            }
            if (!this.f1741R.mo10635h()) {
                this.f1741R.clear();
            }
        }
    }

    /* renamed from: f */
    public boolean mo10633f(Request dVar) {
        return m2715b() && dVar.equals(this.f1740Q);
    }

    /* renamed from: g */
    public boolean mo10634g() {
        return this.f1740Q.mo10634g();
    }

    /* renamed from: h */
    public boolean mo10635h() {
        return this.f1740Q.mo10635h() || this.f1741R.mo10635h();
    }

    public boolean isRunning() {
        return this.f1740Q.isRunning();
    }

    public void recycle() {
        this.f1740Q.recycle();
        this.f1741R.recycle();
    }

    public ThumbnailRequestCoordinator(@Nullable RequestCoordinator eVar) {
        this.f1739P = eVar;
    }

    /* renamed from: d */
    private boolean m2716d() {
        RequestCoordinator eVar = this.f1739P;
        return eVar == null || eVar.mo10627c(this);
    }

    /* renamed from: b */
    public void mo10625b(Request dVar) {
        RequestCoordinator eVar;
        if (dVar.equals(this.f1740Q) && (eVar = this.f1739P) != null) {
            eVar.mo10625b(this);
        }
    }

    /* renamed from: c */
    public void mo10626c() {
        this.f1742S = true;
        if (!this.f1740Q.mo10635h() && !this.f1741R.isRunning()) {
            this.f1741R.mo10626c();
        }
        if (this.f1742S && !this.f1740Q.isRunning()) {
            this.f1740Q.mo10626c();
        }
    }

    /* renamed from: f */
    public boolean mo10632f() {
        return this.f1740Q.mo10632f();
    }

    /* renamed from: a */
    public boolean mo10623a() {
        return m2718j() || mo10631e();
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0027 A[ORIG_RETURN, RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo10624a(com.bumptech.glide.p040q.Request r4) {
        /*
            r3 = this;
            boolean r0 = r4 instanceof com.bumptech.glide.p040q.ThumbnailRequestCoordinator
            r1 = 0
            if (r0 == 0) goto L_0x0028
            com.bumptech.glide.q.k r4 = (com.bumptech.glide.p040q.ThumbnailRequestCoordinator) r4
            com.bumptech.glide.q.d r0 = r3.f1740Q
            if (r0 != 0) goto L_0x0010
            com.bumptech.glide.q.d r0 = r4.f1740Q
            if (r0 != 0) goto L_0x0028
            goto L_0x0018
        L_0x0010:
            com.bumptech.glide.q.d r2 = r4.f1740Q
            boolean r0 = r0.mo10624a(r2)
            if (r0 == 0) goto L_0x0028
        L_0x0018:
            com.bumptech.glide.q.d r0 = r3.f1741R
            com.bumptech.glide.q.d r4 = r4.f1741R
            if (r0 != 0) goto L_0x0021
            if (r4 != 0) goto L_0x0028
            goto L_0x0027
        L_0x0021:
            boolean r4 = r0.mo10624a(r4)
            if (r4 == 0) goto L_0x0028
        L_0x0027:
            r1 = 1
        L_0x0028:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.p040q.ThumbnailRequestCoordinator.mo10624a(com.bumptech.glide.q.d):boolean");
    }

    /* renamed from: e */
    public boolean mo10631e() {
        return this.f1740Q.mo10631e() || this.f1741R.mo10631e();
    }
}
