package com.bumptech.glide.p039p;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import com.bumptech.glide.util.MultiClassKey;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/* renamed from: com.bumptech.glide.p.d */
public class ModelToResourceClassCache {

    /* renamed from: a */
    private final AtomicReference<MultiClassKey> f1652a = new AtomicReference<>();

    /* renamed from: b */
    private final ArrayMap<MultiClassKey, List<Class<?>>> f1653b = new ArrayMap<>();

    @Nullable
    /* renamed from: a */
    public List<Class<?>> mo10560a(@NonNull Class<?> cls, @NonNull Class<?> cls2, @NonNull Class<?> cls3) {
        List<Class<?>> list;
        MultiClassKey andSet = this.f1652a.getAndSet(null);
        if (andSet == null) {
            andSet = new MultiClassKey(cls, cls2, cls3);
        } else {
            andSet.mo10710a(cls, cls2, cls3);
        }
        synchronized (this.f1653b) {
            list = this.f1653b.get(andSet);
        }
        this.f1652a.set(andSet);
        return list;
    }

    /* renamed from: a */
    public void mo10561a(@NonNull Class<?> cls, @NonNull Class<?> cls2, @NonNull Class<?> cls3, @NonNull List<Class<?>> list) {
        synchronized (this.f1653b) {
            this.f1653b.put(new MultiClassKey(cls, cls2, cls3), list);
        }
    }
}
