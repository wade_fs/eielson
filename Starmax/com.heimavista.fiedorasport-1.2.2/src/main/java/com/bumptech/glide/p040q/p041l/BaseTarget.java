package com.bumptech.glide.p040q.p041l;

import android.graphics.drawable.Drawable;
import androidx.annotation.Nullable;
import com.bumptech.glide.p040q.Request;

@Deprecated
/* renamed from: com.bumptech.glide.q.l.a */
public abstract class BaseTarget<Z> implements Target<Z> {

    /* renamed from: P */
    private Request f1743P;

    /* renamed from: a */
    public void mo10639a(@Nullable Drawable drawable) {
    }

    /* renamed from: a */
    public void mo10640a(@Nullable Request dVar) {
        this.f1743P = dVar;
    }

    /* renamed from: b */
    public void mo9947b() {
    }

    /* renamed from: b */
    public void mo10644b(@Nullable Drawable drawable) {
    }

    /* renamed from: c */
    public void mo9949c() {
    }

    /* renamed from: c */
    public void mo10646c(@Nullable Drawable drawable) {
    }

    public void onStart() {
    }

    @Nullable
    /* renamed from: a */
    public Request mo10638a() {
        return this.f1743P;
    }
}
