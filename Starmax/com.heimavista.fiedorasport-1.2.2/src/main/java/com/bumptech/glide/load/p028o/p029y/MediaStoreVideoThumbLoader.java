package com.bumptech.glide.load.p028o.p029y;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.p022o.MediaStoreUtil;
import com.bumptech.glide.load.p021m.p022o.ThumbFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p028o.ModelLoaderFactory;
import com.bumptech.glide.load.p028o.MultiModelLoaderFactory;
import com.bumptech.glide.load.p030p.p031c.VideoDecoder;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.y.d */
public class MediaStoreVideoThumbLoader implements ModelLoader<Uri, InputStream> {

    /* renamed from: a */
    private final Context f1379a;

    /* renamed from: com.bumptech.glide.load.o.y.d$a */
    /* compiled from: MediaStoreVideoThumbLoader */
    public static class C1060a implements ModelLoaderFactory<Uri, InputStream> {

        /* renamed from: a */
        private final Context f1380a;

        public C1060a(Context context) {
            this.f1380a = context;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new MediaStoreVideoThumbLoader(this.f1380a);
        }
    }

    public MediaStoreVideoThumbLoader(Context context) {
        this.f1379a = context.getApplicationContext();
    }

    @Nullable
    /* renamed from: a */
    public ModelLoader.C1036a<InputStream> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        if (!MediaStoreUtil.m1462a(i, i2) || !m2066a(iVar)) {
            return null;
        }
        return new ModelLoader.C1036a<>(new ObjectKey(uri), ThumbFetcher.m1469b(this.f1379a, uri));
    }

    /* renamed from: a */
    private boolean m2066a(Options iVar) {
        Long l = (Long) iVar.mo9976a(VideoDecoder.f1451d);
        return l != null && l.longValue() == -1;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        return MediaStoreUtil.m1465c(uri);
    }
}
