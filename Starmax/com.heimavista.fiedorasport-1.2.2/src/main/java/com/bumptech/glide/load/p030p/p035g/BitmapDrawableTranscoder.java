package com.bumptech.glide.load.p030p.p035g;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p030p.p031c.LazyBitmapDrawableResource;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.g.b */
public class BitmapDrawableTranscoder implements ResourceTranscoder<Bitmap, BitmapDrawable> {

    /* renamed from: a */
    private final Resources f1466a;

    public BitmapDrawableTranscoder(@NonNull Resources resources) {
        Preconditions.m2828a(resources);
        this.f1466a = resources;
    }

    @Nullable
    /* renamed from: a */
    public Resource<BitmapDrawable> mo10399a(@NonNull Resource<Bitmap> vVar, @NonNull Options iVar) {
        return LazyBitmapDrawableResource.m2190a(this.f1466a, vVar);
    }
}
