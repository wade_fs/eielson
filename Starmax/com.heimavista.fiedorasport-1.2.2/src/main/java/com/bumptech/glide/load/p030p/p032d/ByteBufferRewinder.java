package com.bumptech.glide.load.p030p.p032d;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p021m.DataRewinder;
import java.nio.ByteBuffer;

/* renamed from: com.bumptech.glide.load.p.d.a */
public class ByteBufferRewinder implements DataRewinder<ByteBuffer> {

    /* renamed from: a */
    private final ByteBuffer f1459a;

    /* renamed from: com.bumptech.glide.load.p.d.a$a */
    /* compiled from: ByteBufferRewinder */
    public static class C1086a implements DataRewinder.C0935a<ByteBuffer> {
        @NonNull
        /* renamed from: a */
        public DataRewinder<ByteBuffer> mo10002a(ByteBuffer byteBuffer) {
            return new ByteBufferRewinder(byteBuffer);
        }

        @NonNull
        /* renamed from: a */
        public Class<ByteBuffer> mo10003a() {
            return ByteBuffer.class;
        }
    }

    public ByteBufferRewinder(ByteBuffer byteBuffer) {
        this.f1459a = byteBuffer;
    }

    /* renamed from: b */
    public void mo10001b() {
    }

    @NonNull
    /* renamed from: a */
    public ByteBuffer mo10000a() {
        this.f1459a.position(0);
        return this.f1459a;
    }
}
