package com.bumptech.glide.load;

/* renamed from: com.bumptech.glide.load.c */
public enum EncodeStrategy {
    SOURCE,
    TRANSFORMED,
    NONE
}
