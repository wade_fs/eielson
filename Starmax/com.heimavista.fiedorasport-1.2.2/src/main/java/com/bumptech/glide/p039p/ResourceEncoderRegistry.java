package com.bumptech.glide.p039p;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.ResourceEncoder;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.p.f */
public class ResourceEncoderRegistry {

    /* renamed from: a */
    private final List<C1110a<?>> f1659a = new ArrayList();

    /* renamed from: com.bumptech.glide.p.f$a */
    /* compiled from: ResourceEncoderRegistry */
    private static final class C1110a<T> {

        /* renamed from: a */
        private final Class<T> f1660a;

        /* renamed from: b */
        final ResourceEncoder<T> f1661b;

        C1110a(@NonNull Class<T> cls, @NonNull ResourceEncoder<T> kVar) {
            this.f1660a = cls;
            this.f1661b = kVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo10569a(@NonNull Class<?> cls) {
            return this.f1660a.isAssignableFrom(cls);
        }
    }

    /* renamed from: a */
    public synchronized <Z> void mo10568a(@NonNull Class<Z> cls, @NonNull ResourceEncoder<Z> kVar) {
        this.f1659a.add(new C1110a(cls, kVar));
    }

    @Nullable
    /* renamed from: a */
    public synchronized <Z> ResourceEncoder<Z> mo10567a(@NonNull Class<Z> cls) {
        int size = this.f1659a.size();
        for (int i = 0; i < size; i++) {
            C1110a aVar = this.f1659a.get(i);
            if (aVar.mo10569a(cls)) {
                return aVar.f1661b;
            }
        }
        return null;
    }
}
