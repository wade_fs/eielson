package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import java.io.File;

/* renamed from: com.bumptech.glide.load.d */
public interface Encoder<T> {
    /* renamed from: a */
    boolean mo9965a(@NonNull Object obj, @NonNull File file, @NonNull Options iVar);
}
