package com.bumptech.glide.p043r;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.r.b */
public final class ObjectKey implements Key {

    /* renamed from: b */
    private final Object f1765b;

    public ObjectKey(@NonNull Object obj) {
        Preconditions.m2828a(obj);
        this.f1765b = obj;
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        messageDigest.update(this.f1765b.toString().getBytes(Key.f928a));
    }

    public boolean equals(Object obj) {
        if (obj instanceof ObjectKey) {
            return this.f1765b.equals(((ObjectKey) obj).f1765b);
        }
        return false;
    }

    public int hashCode() {
        return this.f1765b.hashCode();
    }

    public String toString() {
        return "ObjectKey{object=" + this.f1765b + '}';
    }
}
