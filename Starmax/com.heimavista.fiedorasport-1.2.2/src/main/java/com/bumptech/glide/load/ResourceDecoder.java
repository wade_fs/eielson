package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.j */
public interface ResourceDecoder<T, Z> {
    @Nullable
    /* renamed from: a */
    Resource<Z> mo9979a(@NonNull T t, int i, int i2, @NonNull Options iVar);

    /* renamed from: a */
    boolean mo9980a(@NonNull T t, @NonNull Options iVar);
}
