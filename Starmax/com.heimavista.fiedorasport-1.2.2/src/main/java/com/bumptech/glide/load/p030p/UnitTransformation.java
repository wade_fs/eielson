package com.bumptech.glide.load.p030p;

import android.content.Context;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.Resource;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.p.b */
public final class UnitTransformation<T> implements Transformation<T> {

    /* renamed from: b */
    private static final Transformation<?> f1383b = new UnitTransformation();

    private UnitTransformation() {
    }

    @NonNull
    /* renamed from: a */
    public static <T> UnitTransformation<T> m2079a() {
        return (UnitTransformation) f1383b;
    }

    @NonNull
    /* renamed from: a */
    public Resource<T> mo9982a(@NonNull Context context, @NonNull Resource<T> vVar, int i, int i2) {
        return vVar;
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
    }
}
