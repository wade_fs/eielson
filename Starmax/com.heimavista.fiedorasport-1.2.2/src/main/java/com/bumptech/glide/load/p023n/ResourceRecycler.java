package com.bumptech.glide.load.p023n;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: com.bumptech.glide.load.n.y */
class ResourceRecycler {

    /* renamed from: a */
    private boolean f1277a;

    /* renamed from: b */
    private final Handler f1278b = new Handler(Looper.getMainLooper(), new C1006a());

    /* renamed from: com.bumptech.glide.load.n.y$a */
    /* compiled from: ResourceRecycler */
    private static final class C1006a implements Handler.Callback {
        C1006a() {
        }

        public boolean handleMessage(Message message) {
            if (message.what != 1) {
                return false;
            }
            ((Resource) message.obj).recycle();
            return true;
        }
    }

    ResourceRecycler() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10258a(Resource<?> vVar) {
        if (this.f1277a) {
            this.f1278b.obtainMessage(1, vVar).sendToTarget();
        } else {
            this.f1277a = true;
            vVar.recycle();
            this.f1277a = false;
        }
    }
}
