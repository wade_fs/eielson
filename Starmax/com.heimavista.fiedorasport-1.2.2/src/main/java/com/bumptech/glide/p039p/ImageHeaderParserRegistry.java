package com.bumptech.glide.p039p;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.ImageHeaderParser;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.p.b */
public final class ImageHeaderParserRegistry {

    /* renamed from: a */
    private final List<ImageHeaderParser> f1648a = new ArrayList();

    @NonNull
    /* renamed from: a */
    public synchronized List<ImageHeaderParser> mo10555a() {
        return this.f1648a;
    }

    /* renamed from: a */
    public synchronized void mo10556a(@NonNull ImageHeaderParser imageHeaderParser) {
        this.f1648a.add(imageHeaderParser);
    }
}
