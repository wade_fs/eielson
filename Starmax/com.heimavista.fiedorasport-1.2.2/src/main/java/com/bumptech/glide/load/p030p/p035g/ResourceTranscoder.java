package com.bumptech.glide.load.p030p.p035g;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.p.g.e */
public interface ResourceTranscoder<Z, R> {
    @Nullable
    /* renamed from: a */
    Resource<R> mo10399a(@NonNull Resource<Z> vVar, @NonNull Options iVar);
}
