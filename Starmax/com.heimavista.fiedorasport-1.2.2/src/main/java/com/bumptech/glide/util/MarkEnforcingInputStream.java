package com.bumptech.glide.util;

import androidx.annotation.NonNull;
import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.util.g */
public class MarkEnforcingInputStream extends FilterInputStream {

    /* renamed from: P */
    private int f1784P = Integer.MIN_VALUE;

    public MarkEnforcingInputStream(@NonNull InputStream inputStream) {
        super(inputStream);
    }

    /* renamed from: g */
    private long m2825g(long j) {
        int i = this.f1784P;
        if (i == 0) {
            return -1;
        }
        return (i == Integer.MIN_VALUE || j <= ((long) i)) ? j : (long) i;
    }

    /* renamed from: h */
    private void m2826h(long j) {
        int i = this.f1784P;
        if (i != Integer.MIN_VALUE && j != -1) {
            this.f1784P = (int) (((long) i) - j);
        }
    }

    public int available() {
        int i = this.f1784P;
        if (i == Integer.MIN_VALUE) {
            return super.available();
        }
        return Math.min(i, super.available());
    }

    public synchronized void mark(int i) {
        super.mark(i);
        this.f1784P = i;
    }

    public int read() {
        if (m2825g(1) == -1) {
            return -1;
        }
        int read = super.read();
        m2826h(1);
        return read;
    }

    public synchronized void reset() {
        super.reset();
        this.f1784P = Integer.MIN_VALUE;
    }

    public long skip(long j) {
        long g = m2825g(j);
        if (g == -1) {
            return 0;
        }
        long skip = super.skip(g);
        m2826h(skip);
        return skip;
    }

    public int read(@NonNull byte[] bArr, int i, int i2) {
        int g = (int) m2825g((long) i2);
        if (g == -1) {
            return -1;
        }
        int read = super.read(bArr, i, g);
        m2826h((long) read);
        return read;
    }
}
