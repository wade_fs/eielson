package com.bumptech.glide.manager;

import androidx.annotation.NonNull;
import com.bumptech.glide.util.C1122j;
import java.util.Collections;
import java.util.Set;
import java.util.WeakHashMap;

/* renamed from: com.bumptech.glide.manager.a */
class ActivityFragmentLifecycle implements Lifecycle {

    /* renamed from: a */
    private final Set<LifecycleListener> f1573a = Collections.newSetFromMap(new WeakHashMap());

    /* renamed from: b */
    private boolean f1574b;

    /* renamed from: c */
    private boolean f1575c;

    ActivityFragmentLifecycle() {
    }

    /* renamed from: a */
    public void mo10499a(@NonNull LifecycleListener iVar) {
        this.f1573a.add(iVar);
        if (this.f1575c) {
            iVar.mo9947b();
        } else if (this.f1574b) {
            iVar.onStart();
        } else {
            iVar.mo9949c();
        }
    }

    /* renamed from: b */
    public void mo10501b(@NonNull LifecycleListener iVar) {
        this.f1573a.remove(iVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo10502c() {
        this.f1574b = false;
        for (LifecycleListener iVar : C1122j.m2843a(this.f1573a)) {
            iVar.mo9949c();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo10500b() {
        this.f1574b = true;
        for (LifecycleListener iVar : C1122j.m2843a(this.f1573a)) {
            iVar.onStart();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10498a() {
        this.f1575c = true;
        for (LifecycleListener iVar : C1122j.m2843a(this.f1573a)) {
            iVar.mo9947b();
        }
    }
}
