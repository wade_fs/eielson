package com.bumptech.glide.load.p021m;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import java.io.OutputStream;

/* renamed from: com.bumptech.glide.load.m.c */
public final class BufferedOutputStream extends OutputStream {
    @NonNull

    /* renamed from: P */
    private final OutputStream f938P;

    /* renamed from: Q */
    private byte[] f939Q;

    /* renamed from: R */
    private ArrayPool f940R;

    /* renamed from: S */
    private int f941S;

    public BufferedOutputStream(@NonNull OutputStream outputStream, @NonNull ArrayPool bVar) {
        this(super, bVar, 65536);
    }

    /* renamed from: a */
    private void m1397a() {
        int i = this.f941S;
        if (i > 0) {
            this.f938P.write(this.f939Q, 0, i);
            this.f941S = 0;
        }
    }

    /* renamed from: b */
    private void m1398b() {
        if (this.f941S == this.f939Q.length) {
            m1397a();
        }
    }

    /* renamed from: c */
    private void m1399c() {
        byte[] bArr = this.f939Q;
        if (bArr != null) {
            this.f940R.put(bArr);
            this.f939Q = null;
        }
    }

    /* JADX INFO: finally extract failed */
    public void close() {
        try {
            flush();
            this.f938P.close();
            m1399c();
        } catch (Throwable th) {
            this.f938P.close();
            throw th;
        }
    }

    public void flush() {
        m1397a();
        this.f938P.flush();
    }

    public void write(int i) {
        byte[] bArr = this.f939Q;
        int i2 = this.f941S;
        this.f941S = i2 + 1;
        bArr[i2] = (byte) i;
        m1398b();
    }

    @VisibleForTesting
    BufferedOutputStream(@NonNull OutputStream outputStream, ArrayPool bVar, int i) {
        this.f938P = super;
        this.f940R = bVar;
        this.f939Q = (byte[]) bVar.mo10042b(i, byte[].class);
    }

    public void write(@NonNull byte[] bArr) {
        write(bArr, 0, bArr.length);
    }

    public void write(@NonNull byte[] bArr, int i, int i2) {
        int i3 = 0;
        do {
            int i4 = i2 - i3;
            int i5 = i + i3;
            if (this.f941S != 0 || i4 < this.f939Q.length) {
                int min = Math.min(i4, this.f939Q.length - this.f941S);
                System.arraycopy(bArr, i5, this.f939Q, this.f941S, min);
                this.f941S += min;
                i3 += min;
                m1398b();
            } else {
                this.f938P.write(bArr, i5, i4);
                return;
            }
        } while (i3 < i2);
    }
}
