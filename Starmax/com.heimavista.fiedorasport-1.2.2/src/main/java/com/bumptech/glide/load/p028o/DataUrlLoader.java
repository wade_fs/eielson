package com.bumptech.glide.load.p028o;

import android.util.Base64;
import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.e */
public final class DataUrlLoader<Model, Data> implements ModelLoader<Model, Data> {

    /* renamed from: a */
    private final C1018a<Data> f1295a;

    /* renamed from: com.bumptech.glide.load.o.e$a */
    /* compiled from: DataUrlLoader */
    public interface C1018a<Data> {
        /* renamed from: a */
        Class<Data> mo10273a();

        /* renamed from: a */
        void mo10274a(Data data);

        Data decode(String str);
    }

    /* renamed from: com.bumptech.glide.load.o.e$c */
    /* compiled from: DataUrlLoader */
    public static final class C1020c<Model> implements ModelLoaderFactory<Model, InputStream> {

        /* renamed from: a */
        private final C1018a<InputStream> f1299a = new C1021a(this);

        /* renamed from: com.bumptech.glide.load.o.e$c$a */
        /* compiled from: DataUrlLoader */
        class C1021a implements C1018a<InputStream> {
            C1021a(C1020c cVar) {
            }

            /* renamed from: a */
            public void mo10274a(InputStream inputStream) {
                inputStream.close();
            }

            public InputStream decode(String str) {
                if (str.startsWith("data:image")) {
                    int indexOf = str.indexOf(44);
                    if (indexOf == -1) {
                        throw new IllegalArgumentException("Missing comma in data URL.");
                    } else if (str.substring(0, indexOf).endsWith(";base64")) {
                        return new ByteArrayInputStream(Base64.decode(str.substring(indexOf + 1), 0));
                    } else {
                        throw new IllegalArgumentException("Not a base64 image data URL.");
                    }
                } else {
                    throw new IllegalArgumentException("Not a valid image data URL.");
                }
            }

            /* renamed from: a */
            public Class<InputStream> mo10273a() {
                return InputStream.class;
            }
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Model, InputStream> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new DataUrlLoader(this.f1299a);
        }
    }

    public DataUrlLoader(C1018a<Data> aVar) {
        this.f1295a = aVar;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull Model model, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(model), new C1019b(model.toString(), this.f1295a));
    }

    /* renamed from: com.bumptech.glide.load.o.e$b */
    /* compiled from: DataUrlLoader */
    private static final class C1019b<Data> implements DataFetcher<Data> {

        /* renamed from: P */
        private final String f1296P;

        /* renamed from: Q */
        private final C1018a<Data> f1297Q;

        /* renamed from: R */
        private Data f1298R;

        C1019b(String str, C1018a<Data> aVar) {
            this.f1296P = str;
            this.f1297Q = aVar;
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super Data> aVar) {
            try {
                this.f1298R = this.f1297Q.decode(this.f1296P);
                aVar.mo9999a((Object) this.f1298R);
            } catch (IllegalArgumentException e) {
                aVar.mo9998a((Exception) e);
            }
        }

        /* renamed from: b */
        public void mo9990b() {
            try {
                this.f1297Q.mo10274a(this.f1298R);
            } catch (IOException unused) {
            }
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        @NonNull
        /* renamed from: a */
        public Class<Data> mo9984a() {
            return this.f1297Q.mo10273a();
        }
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Model model) {
        return model.toString().startsWith("data:image");
    }
}
