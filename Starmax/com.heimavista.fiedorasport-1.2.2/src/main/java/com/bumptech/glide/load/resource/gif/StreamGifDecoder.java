package com.bumptech.glide.load.resource.gif;

import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

/* renamed from: com.bumptech.glide.load.resource.gif.i */
public class StreamGifDecoder implements ResourceDecoder<InputStream, GifDrawable> {

    /* renamed from: a */
    private final List<ImageHeaderParser> f1521a;

    /* renamed from: b */
    private final ResourceDecoder<ByteBuffer, GifDrawable> f1522b;

    /* renamed from: c */
    private final ArrayPool f1523c;

    public StreamGifDecoder(List<ImageHeaderParser> list, ResourceDecoder<ByteBuffer, GifDrawable> jVar, ArrayPool bVar) {
        this.f1521a = list;
        this.f1522b = jVar;
        this.f1523c = bVar;
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull InputStream inputStream, @NonNull Options iVar) {
        return !((Boolean) iVar.mo9976a(GifOptions.f1520b)).booleanValue() && ImageHeaderParserUtils.m1367b(this.f1521a, inputStream, this.f1523c) == ImageHeaderParser.ImageType.GIF;
    }

    /* renamed from: a */
    public Resource<GifDrawable> mo9979a(@NonNull InputStream inputStream, int i, int i2, @NonNull Options iVar) {
        byte[] a = m2358a(inputStream);
        if (a == null) {
            return null;
        }
        return this.f1522b.mo9979a(ByteBuffer.wrap(a), i, i2, iVar);
    }

    /* renamed from: a */
    private static byte[] m2358a(InputStream inputStream) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr);
                if (read != -1) {
                    byteArrayOutputStream.write(bArr, 0, read);
                } else {
                    byteArrayOutputStream.flush();
                    return byteArrayOutputStream.toByteArray();
                }
            }
        } catch (IOException e) {
            if (!Log.isLoggable("StreamGifDecoder", 5)) {
                return null;
            }
            Log.w("StreamGifDecoder", "Error reading data from stream", e);
            return null;
        }
    }
}
