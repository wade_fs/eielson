package com.bumptech.glide.load.p023n.p025b0;

import com.bumptech.glide.util.Preconditions;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: com.bumptech.glide.load.n.b0.c */
final class DiskCacheWriteLocker {

    /* renamed from: a */
    private final Map<String, C0960a> f1031a = new HashMap();

    /* renamed from: b */
    private final C0961b f1032b = new C0961b();

    /* renamed from: com.bumptech.glide.load.n.b0.c$a */
    /* compiled from: DiskCacheWriteLocker */
    private static class C0960a {

        /* renamed from: a */
        final Lock f1033a = new ReentrantLock();

        /* renamed from: b */
        int f1034b;

        C0960a() {
        }
    }

    DiskCacheWriteLocker() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10092a(String str) {
        C0960a aVar;
        synchronized (this) {
            aVar = this.f1031a.get(str);
            if (aVar == null) {
                aVar = this.f1032b.mo10094a();
                this.f1031a.put(str, aVar);
            }
            aVar.f1034b++;
        }
        aVar.f1033a.lock();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo10093b(String str) {
        C0960a aVar;
        synchronized (this) {
            C0960a aVar2 = this.f1031a.get(str);
            Preconditions.m2828a(aVar2);
            aVar = aVar2;
            if (aVar.f1034b >= 1) {
                aVar.f1034b--;
                if (aVar.f1034b == 0) {
                    C0960a remove = this.f1031a.remove(str);
                    if (remove.equals(aVar)) {
                        this.f1032b.mo10095a(remove);
                    } else {
                        throw new IllegalStateException("Removed the wrong lock, expected to remove: " + aVar + ", but actually removed: " + remove + ", safeKey: " + str);
                    }
                }
            } else {
                throw new IllegalStateException("Cannot release a lock that is not held, safeKey: " + str + ", interestedThreads: " + aVar.f1034b);
            }
        }
        aVar.f1033a.unlock();
    }

    /* renamed from: com.bumptech.glide.load.n.b0.c$b */
    /* compiled from: DiskCacheWriteLocker */
    private static class C0961b {

        /* renamed from: a */
        private final Queue<C0960a> f1035a = new ArrayDeque();

        C0961b() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C0960a mo10094a() {
            C0960a poll;
            synchronized (this.f1035a) {
                poll = this.f1035a.poll();
            }
            return poll == null ? new C0960a() : poll;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10095a(C0960a aVar) {
            synchronized (this.f1035a) {
                if (this.f1035a.size() < 10) {
                    this.f1035a.offer(aVar);
                }
            }
        }
    }
}
