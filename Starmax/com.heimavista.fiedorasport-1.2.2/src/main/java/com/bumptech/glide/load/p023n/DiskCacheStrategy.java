package com.bumptech.glide.load.p023n;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;

/* renamed from: com.bumptech.glide.load.n.j */
public abstract class DiskCacheStrategy {

    /* renamed from: a */
    public static final DiskCacheStrategy f1165a = new C0988b();

    /* renamed from: b */
    public static final DiskCacheStrategy f1166b = new C0989c();

    /* renamed from: c */
    public static final DiskCacheStrategy f1167c = new C0991e();

    /* renamed from: com.bumptech.glide.load.n.j$a */
    /* compiled from: DiskCacheStrategy */
    class C0987a extends DiskCacheStrategy {
        C0987a() {
        }

        /* renamed from: a */
        public boolean mo10187a() {
            return true;
        }

        /* renamed from: a */
        public boolean mo10188a(DataSource aVar) {
            return aVar == DataSource.REMOTE;
        }

        /* renamed from: b */
        public boolean mo10190b() {
            return true;
        }

        /* renamed from: a */
        public boolean mo10189a(boolean z, DataSource aVar, EncodeStrategy cVar) {
            return (aVar == DataSource.RESOURCE_DISK_CACHE || aVar == DataSource.MEMORY_CACHE) ? false : true;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.j$b */
    /* compiled from: DiskCacheStrategy */
    class C0988b extends DiskCacheStrategy {
        C0988b() {
        }

        /* renamed from: a */
        public boolean mo10187a() {
            return false;
        }

        /* renamed from: a */
        public boolean mo10188a(DataSource aVar) {
            return false;
        }

        /* renamed from: a */
        public boolean mo10189a(boolean z, DataSource aVar, EncodeStrategy cVar) {
            return false;
        }

        /* renamed from: b */
        public boolean mo10190b() {
            return false;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.j$c */
    /* compiled from: DiskCacheStrategy */
    class C0989c extends DiskCacheStrategy {
        C0989c() {
        }

        /* renamed from: a */
        public boolean mo10187a() {
            return true;
        }

        /* renamed from: a */
        public boolean mo10188a(DataSource aVar) {
            return (aVar == DataSource.DATA_DISK_CACHE || aVar == DataSource.MEMORY_CACHE) ? false : true;
        }

        /* renamed from: a */
        public boolean mo10189a(boolean z, DataSource aVar, EncodeStrategy cVar) {
            return false;
        }

        /* renamed from: b */
        public boolean mo10190b() {
            return false;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.j$d */
    /* compiled from: DiskCacheStrategy */
    class C0990d extends DiskCacheStrategy {
        C0990d() {
        }

        /* renamed from: a */
        public boolean mo10187a() {
            return false;
        }

        /* renamed from: a */
        public boolean mo10188a(DataSource aVar) {
            return false;
        }

        /* renamed from: a */
        public boolean mo10189a(boolean z, DataSource aVar, EncodeStrategy cVar) {
            return (aVar == DataSource.RESOURCE_DISK_CACHE || aVar == DataSource.MEMORY_CACHE) ? false : true;
        }

        /* renamed from: b */
        public boolean mo10190b() {
            return true;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.j$e */
    /* compiled from: DiskCacheStrategy */
    class C0991e extends DiskCacheStrategy {
        C0991e() {
        }

        /* renamed from: a */
        public boolean mo10187a() {
            return true;
        }

        /* renamed from: a */
        public boolean mo10188a(DataSource aVar) {
            return aVar == DataSource.REMOTE;
        }

        /* renamed from: b */
        public boolean mo10190b() {
            return true;
        }

        /* renamed from: a */
        public boolean mo10189a(boolean z, DataSource aVar, EncodeStrategy cVar) {
            return ((z && aVar == DataSource.DATA_DISK_CACHE) || aVar == DataSource.LOCAL) && cVar == EncodeStrategy.TRANSFORMED;
        }
    }

    static {
        new C0987a();
        new C0990d();
    }

    /* renamed from: a */
    public abstract boolean mo10187a();

    /* renamed from: a */
    public abstract boolean mo10188a(DataSource aVar);

    /* renamed from: a */
    public abstract boolean mo10189a(boolean z, DataSource aVar, EncodeStrategy cVar);

    /* renamed from: b */
    public abstract boolean mo10190b();
}
