package com.bumptech.glide.util;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.Collection;

/* renamed from: com.bumptech.glide.util.i */
public final class Preconditions {
    /* renamed from: a */
    public static void m2832a(boolean z, @NonNull String str) {
        if (!z) {
            throw new IllegalArgumentException(str);
        }
    }

    @NonNull
    /* renamed from: a */
    public static <T> T m2828a(@Nullable Object obj) {
        m2829a(obj, "Argument must not be null");
        return obj;
    }

    @NonNull
    /* renamed from: a */
    public static <T> T m2829a(@Nullable Object obj, @NonNull String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    @NonNull
    /* renamed from: a */
    public static String m2830a(@Nullable String str) {
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        throw new IllegalArgumentException("Must not be null or empty");
    }

    @NonNull
    /* renamed from: a */
    public static <T extends Collection<Y>, Y> T m2831a(@NonNull Collection collection) {
        if (!collection.isEmpty()) {
            return collection;
        }
        throw new IllegalArgumentException("Must not be empty.");
    }
}
