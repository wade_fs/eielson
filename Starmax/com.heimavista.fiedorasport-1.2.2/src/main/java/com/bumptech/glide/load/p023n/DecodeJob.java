package com.bumptech.glide.load.p023n;

import android.os.Build;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.core.util.Pools;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.EncodeStrategy;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceEncoder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p021m.DataRewinder;
import com.bumptech.glide.load.p023n.DataFetcherGenerator;
import com.bumptech.glide.load.p023n.DecodePath;
import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import com.bumptech.glide.load.p030p.p031c.Downsampler;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.p044k.FactoryPools;
import com.bumptech.glide.util.p044k.GlideTrace;
import com.bumptech.glide.util.p044k.StateVerifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.h */
class DecodeJob<R> implements DataFetcherGenerator.C0977a, Runnable, Comparable<DecodeJob<?>>, FactoryPools.C1129f {

    /* renamed from: P */
    private final DecodeHelper<R> f1107P = new DecodeHelper<>();

    /* renamed from: Q */
    private final List<Throwable> f1108Q = new ArrayList();

    /* renamed from: R */
    private final StateVerifier f1109R = StateVerifier.m2871b();

    /* renamed from: S */
    private final C0982e f1110S;

    /* renamed from: T */
    private final Pools.Pool<DecodeJob<?>> f1111T;

    /* renamed from: U */
    private final C0981d<?> f1112U = new C0981d<>();

    /* renamed from: V */
    private final C0983f f1113V = new C0983f();

    /* renamed from: W */
    private GlideContext f1114W;

    /* renamed from: X */
    private Key f1115X;

    /* renamed from: Y */
    private Priority f1116Y;

    /* renamed from: Z */
    private EngineKey f1117Z;

    /* renamed from: a0 */
    private int f1118a0;

    /* renamed from: b0 */
    private int f1119b0;

    /* renamed from: c0 */
    private DiskCacheStrategy f1120c0;

    /* renamed from: d0 */
    private Options f1121d0;

    /* renamed from: e0 */
    private C0979b<R> f1122e0;

    /* renamed from: f0 */
    private int f1123f0;

    /* renamed from: g0 */
    private C0985h f1124g0;

    /* renamed from: h0 */
    private C0984g f1125h0;

    /* renamed from: i0 */
    private long f1126i0;

    /* renamed from: j0 */
    private boolean f1127j0;

    /* renamed from: k0 */
    private Object f1128k0;

    /* renamed from: l0 */
    private Thread f1129l0;

    /* renamed from: m0 */
    private Key f1130m0;

    /* renamed from: n0 */
    private Key f1131n0;

    /* renamed from: o0 */
    private Object f1132o0;

    /* renamed from: p0 */
    private DataSource f1133p0;

    /* renamed from: q0 */
    private DataFetcher<?> f1134q0;

    /* renamed from: r0 */
    private volatile DataFetcherGenerator f1135r0;

    /* renamed from: s0 */
    private volatile boolean f1136s0;

    /* renamed from: t0 */
    private volatile boolean f1137t0;

    /* renamed from: com.bumptech.glide.load.n.h$a */
    /* compiled from: DecodeJob */
    static /* synthetic */ class C0978a {

        /* renamed from: a */
        static final /* synthetic */ int[] f1138a = new int[C0984g.values().length];

        /* renamed from: b */
        static final /* synthetic */ int[] f1139b = new int[C0985h.values().length];

        /* renamed from: c */
        static final /* synthetic */ int[] f1140c = new int[EncodeStrategy.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|(2:5|6)|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|12|13|15|16|17|18|19|20|21|23|24|25|26|27|28|30) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0047 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0070 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007a */
        static {
            /*
                com.bumptech.glide.load.c[] r0 = com.bumptech.glide.load.EncodeStrategy.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1140c = r0
                r0 = 1
                int[] r1 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1140c     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.bumptech.glide.load.c r2 = com.bumptech.glide.load.EncodeStrategy.SOURCE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1140c     // Catch:{ NoSuchFieldError -> 0x001f }
                com.bumptech.glide.load.c r3 = com.bumptech.glide.load.EncodeStrategy.TRANSFORMED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.bumptech.glide.load.n.h$h[] r2 = com.bumptech.glide.load.p023n.DecodeJob.C0985h.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1139b = r2
                int[] r2 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1139b     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.bumptech.glide.load.n.h$h r3 = com.bumptech.glide.load.p023n.DecodeJob.C0985h.RESOURCE_CACHE     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r2 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1139b     // Catch:{ NoSuchFieldError -> 0x003c }
                com.bumptech.glide.load.n.h$h r3 = com.bumptech.glide.load.p023n.DecodeJob.C0985h.DATA_CACHE     // Catch:{ NoSuchFieldError -> 0x003c }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                r2 = 3
                int[] r3 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1139b     // Catch:{ NoSuchFieldError -> 0x0047 }
                com.bumptech.glide.load.n.h$h r4 = com.bumptech.glide.load.p023n.DecodeJob.C0985h.SOURCE     // Catch:{ NoSuchFieldError -> 0x0047 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0047 }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x0047 }
            L_0x0047:
                int[] r3 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1139b     // Catch:{ NoSuchFieldError -> 0x0052 }
                com.bumptech.glide.load.n.h$h r4 = com.bumptech.glide.load.p023n.DecodeJob.C0985h.FINISHED     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r5 = 4
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r3 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1139b     // Catch:{ NoSuchFieldError -> 0x005d }
                com.bumptech.glide.load.n.h$h r4 = com.bumptech.glide.load.p023n.DecodeJob.C0985h.INITIALIZE     // Catch:{ NoSuchFieldError -> 0x005d }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x005d }
                r5 = 5
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x005d }
            L_0x005d:
                com.bumptech.glide.load.n.h$g[] r3 = com.bumptech.glide.load.p023n.DecodeJob.C0984g.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1138a = r3
                int[] r3 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1138a     // Catch:{ NoSuchFieldError -> 0x0070 }
                com.bumptech.glide.load.n.h$g r4 = com.bumptech.glide.load.p023n.DecodeJob.C0984g.INITIALIZE     // Catch:{ NoSuchFieldError -> 0x0070 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0070 }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0070 }
            L_0x0070:
                int[] r0 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1138a     // Catch:{ NoSuchFieldError -> 0x007a }
                com.bumptech.glide.load.n.h$g r3 = com.bumptech.glide.load.p023n.DecodeJob.C0984g.SWITCH_TO_SOURCE_SERVICE     // Catch:{ NoSuchFieldError -> 0x007a }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                int[] r0 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1138a     // Catch:{ NoSuchFieldError -> 0x0084 }
                com.bumptech.glide.load.n.h$g r1 = com.bumptech.glide.load.p023n.DecodeJob.C0984g.DECODE_DATA     // Catch:{ NoSuchFieldError -> 0x0084 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0084 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0084 }
            L_0x0084:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.DecodeJob.C0978a.<clinit>():void");
        }
    }

    /* renamed from: com.bumptech.glide.load.n.h$b */
    /* compiled from: DecodeJob */
    interface C0979b<R> {
        /* renamed from: a */
        void mo10172a(DecodeJob<?> hVar);

        /* renamed from: a */
        void mo10173a(GlideException qVar);

        /* renamed from: a */
        void mo10174a(Resource<?> vVar, DataSource aVar);
    }

    /* renamed from: com.bumptech.glide.load.n.h$c */
    /* compiled from: DecodeJob */
    private final class C0980c<Z> implements DecodePath.C0986a<Z> {

        /* renamed from: a */
        private final DataSource f1141a;

        C0980c(DataSource aVar) {
            this.f1141a = aVar;
        }

        @NonNull
        /* renamed from: a */
        public Resource<Z> mo10175a(@NonNull Resource<Z> vVar) {
            return DecodeJob.this.mo10166a(this.f1141a, vVar);
        }
    }

    /* renamed from: com.bumptech.glide.load.n.h$e */
    /* compiled from: DecodeJob */
    interface C0982e {
        /* renamed from: a */
        DiskCache mo10180a();
    }

    /* renamed from: com.bumptech.glide.load.n.h$g */
    /* compiled from: DecodeJob */
    private enum C0984g {
        INITIALIZE,
        SWITCH_TO_SOURCE_SERVICE,
        DECODE_DATA
    }

    /* renamed from: com.bumptech.glide.load.n.h$h */
    /* compiled from: DecodeJob */
    private enum C0985h {
        INITIALIZE,
        RESOURCE_CACHE,
        DATA_CACHE,
        SOURCE,
        ENCODE,
        FINISHED
    }

    DecodeJob(C0982e eVar, Pools.Pool<DecodeJob<?>> pool) {
        this.f1110S = eVar;
        this.f1111T = pool;
    }

    /* renamed from: h */
    private void m1707h() {
        if (Log.isLoggable("DecodeJob", 2)) {
            long j = this.f1126i0;
            m1705a("Retrieved data", j, "data: " + this.f1132o0 + ", cache key: " + this.f1130m0 + ", fetcher: " + this.f1134q0);
        }
        Resource vVar = null;
        try {
            vVar = m1700a(this.f1134q0, this.f1132o0, this.f1133p0);
        } catch (GlideException e) {
            e.mo10237a(this.f1131n0, this.f1133p0);
            this.f1108Q.add(e);
        }
        if (vVar != null) {
            m1706b(vVar, this.f1133p0);
        } else {
            m1714o();
        }
    }

    /* renamed from: i */
    private DataFetcherGenerator m1708i() {
        int i = C0978a.f1139b[this.f1124g0.ordinal()];
        if (i == 1) {
            return new ResourceCacheGenerator(this.f1107P, this);
        }
        if (i == 2) {
            return new DataCacheGenerator(this.f1107P, this);
        }
        if (i == 3) {
            return new SourceGenerator(this.f1107P, this);
        }
        if (i == 4) {
            return null;
        }
        throw new IllegalStateException("Unrecognized stage: " + this.f1124g0);
    }

    /* renamed from: j */
    private int m1709j() {
        return this.f1116Y.ordinal();
    }

    /* renamed from: k */
    private void m1710k() {
        m1716q();
        this.f1122e0.mo10173a(new GlideException("Failed to load resource", new ArrayList(this.f1108Q)));
        m1712m();
    }

    /* renamed from: l */
    private void m1711l() {
        if (this.f1113V.mo10181a()) {
            m1713n();
        }
    }

    /* renamed from: m */
    private void m1712m() {
        if (this.f1113V.mo10183b()) {
            m1713n();
        }
    }

    /* renamed from: n */
    private void m1713n() {
        this.f1113V.mo10184c();
        this.f1112U.mo10176a();
        this.f1107P.mo10144a();
        this.f1136s0 = false;
        this.f1114W = null;
        this.f1115X = null;
        this.f1121d0 = null;
        this.f1116Y = null;
        this.f1117Z = null;
        this.f1122e0 = null;
        this.f1124g0 = null;
        this.f1135r0 = null;
        this.f1129l0 = null;
        this.f1130m0 = null;
        this.f1132o0 = null;
        this.f1133p0 = null;
        this.f1134q0 = null;
        this.f1126i0 = 0;
        this.f1137t0 = false;
        this.f1128k0 = null;
        this.f1108Q.clear();
        this.f1111T.release(this);
    }

    /* renamed from: o */
    private void m1714o() {
        this.f1129l0 = Thread.currentThread();
        this.f1126i0 = LogTime.m2815a();
        boolean z = false;
        while (!this.f1137t0 && this.f1135r0 != null && !(z = this.f1135r0.mo10116a())) {
            this.f1124g0 = m1699a(this.f1124g0);
            this.f1135r0 = m1708i();
            if (this.f1124g0 == C0985h.SOURCE) {
                mo10139b();
                return;
            }
        }
        if ((this.f1124g0 == C0985h.FINISHED || this.f1137t0) && !z) {
            m1710k();
        }
    }

    /* renamed from: p */
    private void m1715p() {
        int i = C0978a.f1138a[this.f1125h0.ordinal()];
        if (i == 1) {
            this.f1124g0 = m1699a(C0985h.INITIALIZE);
            this.f1135r0 = m1708i();
            m1714o();
        } else if (i == 2) {
            m1714o();
        } else if (i == 3) {
            m1707h();
        } else {
            throw new IllegalStateException("Unrecognized run reason: " + this.f1125h0);
        }
    }

    /* renamed from: q */
    private void m1716q() {
        Throwable th;
        this.f1109R.mo10716a();
        if (this.f1136s0) {
            if (this.f1108Q.isEmpty()) {
                th = null;
            } else {
                List<Throwable> list = this.f1108Q;
                th = list.get(list.size() - 1);
            }
            throw new IllegalStateException("Already notified", th);
        }
        this.f1136s0 = true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public DecodeJob<R> mo10165a(GlideContext eVar, Object obj, EngineKey nVar, Key gVar, int i, int i2, Class<?> cls, Class<R> cls2, Priority hVar, DiskCacheStrategy jVar, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, boolean z3, Options iVar, C0979b<R> bVar, int i3) {
        this.f1107P.mo10145a(eVar, obj, gVar, i, i2, jVar, cls, cls2, hVar, iVar, map, z, z2, this.f1110S);
        this.f1114W = eVar;
        this.f1115X = gVar;
        this.f1116Y = hVar;
        this.f1117Z = nVar;
        this.f1118a0 = i;
        this.f1119b0 = i2;
        this.f1120c0 = jVar;
        this.f1127j0 = z3;
        this.f1121d0 = iVar;
        this.f1122e0 = bVar;
        this.f1123f0 = i3;
        this.f1125h0 = C0984g.INITIALIZE;
        this.f1128k0 = obj;
        return this;
    }

    /* renamed from: b */
    public void mo10139b() {
        this.f1125h0 = C0984g.SWITCH_TO_SOURCE_SERVICE;
        this.f1122e0.mo10172a(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public boolean mo10169c() {
        C0985h a = m1699a(C0985h.INITIALIZE);
        return a == C0985h.RESOURCE_CACHE || a == C0985h.DATA_CACHE;
    }

    @NonNull
    /* renamed from: d */
    public StateVerifier mo10115d() {
        return this.f1109R;
    }

    public void run() {
        GlideTrace.m2869a("DecodeJob#run(model=%s)", this.f1128k0);
        DataFetcher<?> dVar = this.f1134q0;
        try {
            if (this.f1137t0) {
                m1710k();
                if (dVar != null) {
                    dVar.mo9990b();
                }
                GlideTrace.m2867a();
                return;
            }
            m1715p();
            if (dVar != null) {
                dVar.mo9990b();
            }
            GlideTrace.m2867a();
        } catch (CallbackException e) {
            throw e;
        } catch (Throwable th) {
            if (dVar != null) {
                dVar.mo9990b();
            }
            GlideTrace.m2867a();
            throw th;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.h$f */
    /* compiled from: DecodeJob */
    private static class C0983f {

        /* renamed from: a */
        private boolean f1146a;

        /* renamed from: b */
        private boolean f1147b;

        /* renamed from: c */
        private boolean f1148c;

        C0983f() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public synchronized boolean mo10182a(boolean z) {
            this.f1146a = true;
            return m1736b(z);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public synchronized boolean mo10183b() {
            this.f1148c = true;
            return m1736b(false);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public synchronized void mo10184c() {
            this.f1147b = false;
            this.f1146a = false;
            this.f1148c = false;
        }

        /* renamed from: b */
        private boolean m1736b(boolean z) {
            return (this.f1148c || z || this.f1147b) && this.f1146a;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public synchronized boolean mo10181a() {
            this.f1147b = true;
            return m1736b(false);
        }
    }

    /* renamed from: com.bumptech.glide.load.n.h$d */
    /* compiled from: DecodeJob */
    private static class C0981d<Z> {

        /* renamed from: a */
        private Key f1143a;

        /* renamed from: b */
        private ResourceEncoder<Z> f1144b;

        /* renamed from: c */
        private LockedResource<Z> f1145c;

        C0981d() {
        }

        /* JADX WARN: Type inference failed for: r2v0, types: [com.bumptech.glide.load.k<X>, com.bumptech.glide.load.k<Z>] */
        /* JADX WARN: Type inference failed for: r3v0, types: [com.bumptech.glide.load.n.u<X>, com.bumptech.glide.load.n.u<Z>] */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Unknown variable types count: 2 */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public <X> void mo10177a(com.bumptech.glide.load.Key r1, com.bumptech.glide.load.ResourceEncoder<X> r2, com.bumptech.glide.load.p023n.LockedResource<X> r3) {
            /*
                r0 = this;
                r0.f1143a = r1
                r0.f1144b = r2
                r0.f1145c = r3
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.DecodeJob.C0981d.mo10177a(com.bumptech.glide.load.g, com.bumptech.glide.load.k, com.bumptech.glide.load.n.u):void");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public boolean mo10179b() {
            return this.f1145c != null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10178a(C0982e eVar, Options iVar) {
            GlideTrace.m2868a("DecodeJob.encode");
            try {
                eVar.mo10180a().mo10089a(this.f1143a, new DataCacheWriter(this.f1144b, this.f1145c, iVar));
            } finally {
                this.f1145c.mo10256c();
                GlideTrace.m2867a();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10176a() {
            this.f1143a = null;
            this.f1144b = null;
            this.f1145c = null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.load.n.h.a(com.bumptech.glide.load.n.v, com.bumptech.glide.load.a):void
     arg types: [com.bumptech.glide.load.n.u<R>, com.bumptech.glide.load.a]
     candidates:
      com.bumptech.glide.load.n.h.a(java.lang.Object, com.bumptech.glide.load.a):com.bumptech.glide.load.n.v<R>
      com.bumptech.glide.load.n.h.a(java.lang.String, long):void
      com.bumptech.glide.load.n.h.a(com.bumptech.glide.load.a, com.bumptech.glide.load.n.v):com.bumptech.glide.load.n.v<Z>
      com.bumptech.glide.load.n.h.a(com.bumptech.glide.load.n.v, com.bumptech.glide.load.a):void */
    /* renamed from: b */
    private void m1706b(Resource<R> vVar, DataSource aVar) {
        if (vVar instanceof Initializable) {
            ((Initializable) vVar).mo10250c();
        }
        LockedResource<R> uVar = null;
        LockedResource<R> uVar2 = vVar;
        if (this.f1112U.mo10179b()) {
            LockedResource<R> b = LockedResource.m1844b(vVar);
            uVar = b;
            uVar2 = b;
        }
        m1703a((Resource) uVar2, aVar);
        this.f1124g0 = C0985h.ENCODE;
        try {
            if (this.f1112U.mo10179b()) {
                this.f1112U.mo10178a(this.f1110S, this.f1121d0);
            }
            m1711l();
        } finally {
            if (uVar != null) {
                uVar.mo10256c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10168a(boolean z) {
        if (this.f1113V.mo10182a(z)) {
            m1713n();
        }
    }

    /* renamed from: a */
    public int compareTo(@NonNull DecodeJob<?> hVar) {
        int j = m1709j() - hVar.m1709j();
        return j == 0 ? this.f1123f0 - hVar.f1123f0 : j;
    }

    /* renamed from: a */
    public void mo10167a() {
        this.f1137t0 = true;
        DataFetcherGenerator fVar = this.f1135r0;
        if (fVar != null) {
            fVar.cancel();
        }
    }

    /* renamed from: a */
    private void m1703a(Resource<R> vVar, DataSource aVar) {
        m1716q();
        this.f1122e0.mo10174a(vVar, aVar);
    }

    /* renamed from: a */
    private C0985h m1699a(C0985h hVar) {
        int i = C0978a.f1139b[hVar.ordinal()];
        if (i == 1) {
            return this.f1120c0.mo10187a() ? C0985h.DATA_CACHE : m1699a(C0985h.DATA_CACHE);
        }
        if (i == 2) {
            return this.f1127j0 ? C0985h.FINISHED : C0985h.SOURCE;
        }
        if (i == 3 || i == 4) {
            return C0985h.FINISHED;
        }
        if (i == 5) {
            return this.f1120c0.mo10190b() ? C0985h.RESOURCE_CACHE : m1699a(C0985h.RESOURCE_CACHE);
        }
        throw new IllegalArgumentException("Unrecognized stage: " + hVar);
    }

    /* renamed from: a */
    public void mo10138a(Key gVar, Object obj, DataFetcher<?> dVar, DataSource aVar, Key gVar2) {
        this.f1130m0 = gVar;
        this.f1132o0 = obj;
        this.f1134q0 = dVar;
        this.f1133p0 = aVar;
        this.f1131n0 = gVar2;
        if (Thread.currentThread() != this.f1129l0) {
            this.f1125h0 = C0984g.DECODE_DATA;
            this.f1122e0.mo10172a(this);
            return;
        }
        GlideTrace.m2868a("DecodeJob.decodeFromRetrievedData");
        try {
            m1707h();
        } finally {
            GlideTrace.m2867a();
        }
    }

    /* renamed from: a */
    public void mo10137a(Key gVar, Exception exc, DataFetcher<?> dVar, DataSource aVar) {
        dVar.mo9990b();
        GlideException qVar = new GlideException("Fetching data failed", exc);
        qVar.mo10238a(gVar, aVar, dVar.mo9984a());
        this.f1108Q.add(qVar);
        if (Thread.currentThread() != this.f1129l0) {
            this.f1125h0 = C0984g.SWITCH_TO_SOURCE_SERVICE;
            this.f1122e0.mo10172a(this);
            return;
        }
        m1714o();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.bumptech.glide.load.n.h.a(java.lang.Object, com.bumptech.glide.load.a):com.bumptech.glide.load.n.v<R>
     arg types: [Data, com.bumptech.glide.load.a]
     candidates:
      com.bumptech.glide.load.n.h.a(com.bumptech.glide.load.n.v, com.bumptech.glide.load.a):void
      com.bumptech.glide.load.n.h.a(java.lang.String, long):void
      com.bumptech.glide.load.n.h.a(com.bumptech.glide.load.a, com.bumptech.glide.load.n.v):com.bumptech.glide.load.n.v<Z>
      com.bumptech.glide.load.n.h.a(java.lang.Object, com.bumptech.glide.load.a):com.bumptech.glide.load.n.v<R> */
    /* renamed from: a */
    private <Data> Resource<R> m1700a(DataFetcher<?> dVar, Data data, DataSource aVar) {
        if (data == null) {
            dVar.mo9990b();
            return null;
        }
        try {
            long a = LogTime.m2815a();
            Resource<R> a2 = m1701a((Object) data, aVar);
            if (Log.isLoggable("DecodeJob", 2)) {
                m1704a("Decoded result " + a2, a);
            }
            return a2;
        } finally {
            dVar.mo9990b();
        }
    }

    /* renamed from: a */
    private <Data> Resource<R> m1701a(Data data, DataSource aVar) {
        return m1702a(data, aVar, this.f1107P.mo10142a((Class) data.getClass()));
    }

    @NonNull
    /* renamed from: a */
    private Options m1698a(DataSource aVar) {
        Options iVar = this.f1121d0;
        if (Build.VERSION.SDK_INT < 26) {
            return iVar;
        }
        boolean z = aVar == DataSource.RESOURCE_DISK_CACHE || this.f1107P.mo10163o();
        Boolean bool = (Boolean) iVar.mo9976a(Downsampler.f1413h);
        if (bool != null && (!bool.booleanValue() || z)) {
            return iVar;
        }
        Options iVar2 = new Options();
        iVar2.mo9977a(this.f1121d0);
        iVar2.mo9975a(Downsampler.f1413h, Boolean.valueOf(z));
        return iVar2;
    }

    /* renamed from: a */
    private <Data, ResourceType> Resource<R> m1702a(Data data, DataSource aVar, LoadPath<Data, ResourceType, R> tVar) {
        Options a = m1698a(aVar);
        DataRewinder b = this.f1114W.mo9912f().mo9927b((Object) data);
        try {
            return tVar.mo10254a(b, a, this.f1118a0, this.f1119b0, new C0980c(aVar));
        } finally {
            b.mo10001b();
        }
    }

    /* renamed from: a */
    private void m1704a(String str, long j) {
        m1705a(str, j, (String) null);
    }

    /* renamed from: a */
    private void m1705a(String str, long j, String str2) {
        String str3;
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        sb.append(" in ");
        sb.append(LogTime.m2814a(j));
        sb.append(", load key: ");
        sb.append(this.f1117Z);
        if (str2 != null) {
            str3 = ", " + str2;
        } else {
            str3 = "";
        }
        sb.append(str3);
        sb.append(", thread: ");
        sb.append(Thread.currentThread().getName());
        Log.v("DecodeJob", sb.toString());
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v6, resolved type: com.bumptech.glide.load.n.d} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v8, resolved type: com.bumptech.glide.load.n.x} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v11, resolved type: com.bumptech.glide.load.n.x} */
    /* JADX WARN: Type inference failed for: r12v5, types: [com.bumptech.glide.load.g] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    @androidx.annotation.NonNull
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <Z> com.bumptech.glide.load.p023n.Resource<Z> mo10166a(com.bumptech.glide.load.DataSource r12, @androidx.annotation.NonNull com.bumptech.glide.load.p023n.Resource<Z> r13) {
        /*
            r11 = this;
            java.lang.Object r0 = r13.get()
            java.lang.Class r8 = r0.getClass()
            com.bumptech.glide.load.a r0 = com.bumptech.glide.load.DataSource.RESOURCE_DISK_CACHE
            r1 = 0
            if (r12 == r0) goto L_0x0020
            com.bumptech.glide.load.n.g<R> r0 = r11.f1107P
            com.bumptech.glide.load.l r0 = r0.mo10147b(r8)
            com.bumptech.glide.e r2 = r11.f1114W
            int r3 = r11.f1118a0
            int r4 = r11.f1119b0
            com.bumptech.glide.load.n.v r2 = r0.mo9982a(r2, r13, r3, r4)
            r7 = r0
            r0 = r2
            goto L_0x0022
        L_0x0020:
            r0 = r13
            r7 = r1
        L_0x0022:
            boolean r2 = r13.equals(r0)
            if (r2 != 0) goto L_0x002b
            r13.recycle()
        L_0x002b:
            com.bumptech.glide.load.n.g<R> r13 = r11.f1107P
            boolean r13 = r13.mo10149b(r0)
            if (r13 == 0) goto L_0x0040
            com.bumptech.glide.load.n.g<R> r13 = r11.f1107P
            com.bumptech.glide.load.k r1 = r13.mo10141a(r0)
            com.bumptech.glide.load.i r13 = r11.f1121d0
            com.bumptech.glide.load.c r13 = r1.mo9981a(r13)
            goto L_0x0042
        L_0x0040:
            com.bumptech.glide.load.c r13 = com.bumptech.glide.load.EncodeStrategy.NONE
        L_0x0042:
            r10 = r1
            com.bumptech.glide.load.n.g<R> r1 = r11.f1107P
            com.bumptech.glide.load.g r2 = r11.f1130m0
            boolean r1 = r1.mo10146a(r2)
            r2 = 1
            r1 = r1 ^ r2
            com.bumptech.glide.load.n.j r3 = r11.f1120c0
            boolean r12 = r3.mo10189a(r1, r12, r13)
            if (r12 == 0) goto L_0x00b3
            if (r10 == 0) goto L_0x00a5
            int[] r12 = com.bumptech.glide.load.p023n.DecodeJob.C0978a.f1140c
            int r1 = r13.ordinal()
            r12 = r12[r1]
            if (r12 == r2) goto L_0x0092
            r1 = 2
            if (r12 != r1) goto L_0x007b
            com.bumptech.glide.load.n.x r12 = new com.bumptech.glide.load.n.x
            com.bumptech.glide.load.n.g<R> r13 = r11.f1107P
            com.bumptech.glide.load.n.a0.b r2 = r13.mo10148b()
            com.bumptech.glide.load.g r3 = r11.f1130m0
            com.bumptech.glide.load.g r4 = r11.f1115X
            int r5 = r11.f1118a0
            int r6 = r11.f1119b0
            com.bumptech.glide.load.i r9 = r11.f1121d0
            r1 = r12
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            goto L_0x009b
        L_0x007b:
            java.lang.IllegalArgumentException r12 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Unknown strategy: "
            r0.append(r1)
            r0.append(r13)
            java.lang.String r13 = r0.toString()
            r12.<init>(r13)
            throw r12
        L_0x0092:
            com.bumptech.glide.load.n.d r12 = new com.bumptech.glide.load.n.d
            com.bumptech.glide.load.g r13 = r11.f1130m0
            com.bumptech.glide.load.g r1 = r11.f1115X
            r12.<init>(r13, r1)
        L_0x009b:
            com.bumptech.glide.load.n.u r0 = com.bumptech.glide.load.p023n.LockedResource.m1844b(r0)
            com.bumptech.glide.load.n.h$d<?> r13 = r11.f1112U
            r13.mo10177a(r12, r10, r0)
            goto L_0x00b3
        L_0x00a5:
            com.bumptech.glide.i$d r12 = new com.bumptech.glide.i$d
            java.lang.Object r13 = r0.get()
            java.lang.Class r13 = r13.getClass()
            r12.<init>(r13)
            throw r12
        L_0x00b3:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.DecodeJob.mo10166a(com.bumptech.glide.load.a, com.bumptech.glide.load.n.v):com.bumptech.glide.load.n.v");
    }
}
