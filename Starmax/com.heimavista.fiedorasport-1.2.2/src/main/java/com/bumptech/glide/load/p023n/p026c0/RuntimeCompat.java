package com.bumptech.glide.load.p023n.p026c0;

import android.os.Build;
import android.os.StrictMode;
import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

/* renamed from: com.bumptech.glide.load.n.c0.b */
final class RuntimeCompat {

    /* renamed from: com.bumptech.glide.load.n.c0.b$a */
    /* compiled from: RuntimeCompat */
    class C0976a implements FilenameFilter {

        /* renamed from: a */
        final /* synthetic */ Pattern f1083a;

        C0976a(Pattern pattern) {
            this.f1083a = pattern;
        }

        public boolean accept(File file, String str) {
            return this.f1083a.matcher(str).matches();
        }
    }

    /* renamed from: a */
    static int m1666a() {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        return Build.VERSION.SDK_INT < 17 ? Math.max(m1667b(), availableProcessors) : availableProcessors;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    private static int m1667b() {
        File[] fileArr;
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        try {
            fileArr = new File("/sys/devices/system/cpu/").listFiles(new C0976a(Pattern.compile("cpu[0-9]+")));
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        } catch (Throwable th) {
            StrictMode.setThreadPolicy(allowThreadDiskReads);
            throw th;
        }
        return Math.max(1, fileArr != null ? fileArr.length : 0);
    }
}
