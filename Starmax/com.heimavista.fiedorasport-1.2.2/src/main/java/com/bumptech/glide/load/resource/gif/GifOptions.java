package com.bumptech.glide.load.resource.gif;

import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.Option;

/* renamed from: com.bumptech.glide.load.resource.gif.h */
public final class GifOptions {

    /* renamed from: a */
    public static final Option<DecodeFormat> f1519a = Option.m1370a("com.bumptech.glide.load.resource.gif.GifOptions.DecodeFormat", DecodeFormat.f922R);

    /* renamed from: b */
    public static final Option<Boolean> f1520b = Option.m1370a("com.bumptech.glide.load.resource.gif.GifOptions.DisableAnimation", (Object) false);
}
