package com.bumptech.glide.load.p021m;

import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.HttpException;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p028o.GlideUrl;
import com.bumptech.glide.util.ContentLengthInputStream;
import com.bumptech.glide.util.LogTime;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.m.j */
public class HttpUrlFetcher implements DataFetcher<InputStream> {
    @VisibleForTesting

    /* renamed from: V */
    static final C0939b f950V = new C0938a();

    /* renamed from: P */
    private final GlideUrl f951P;

    /* renamed from: Q */
    private final int f952Q;

    /* renamed from: R */
    private final C0939b f953R;

    /* renamed from: S */
    private HttpURLConnection f954S;

    /* renamed from: T */
    private InputStream f955T;

    /* renamed from: U */
    private volatile boolean f956U;

    /* renamed from: com.bumptech.glide.load.m.j$a */
    /* compiled from: HttpUrlFetcher */
    private static class C0938a implements C0939b {
        C0938a() {
        }

        /* renamed from: a */
        public HttpURLConnection mo10014a(URL url) {
            return (HttpURLConnection) url.openConnection();
        }
    }

    /* renamed from: com.bumptech.glide.load.m.j$b */
    /* compiled from: HttpUrlFetcher */
    interface C0939b {
        /* renamed from: a */
        HttpURLConnection mo10014a(URL url);
    }

    public HttpUrlFetcher(GlideUrl gVar, int i) {
        this(gVar, i, f950V);
    }

    /* renamed from: b */
    private static boolean m1429b(int i) {
        return i / 100 == 3;
    }

    /* renamed from: a */
    public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super InputStream> aVar) {
        StringBuilder sb;
        long a = LogTime.m2815a();
        try {
            aVar.mo9999a(m1427a(this.f951P.mo10286c(), 0, null, this.f951P.mo10285b()));
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
                sb.append("Finished http url fetcher fetch in ");
                sb.append(LogTime.m2814a(a));
                Log.v("HttpUrlFetcher", sb.toString());
            }
        } catch (IOException e) {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Failed to load data for url", e);
            }
            aVar.mo9998a((Exception) e);
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                sb = new StringBuilder();
            }
        } catch (Throwable th) {
            if (Log.isLoggable("HttpUrlFetcher", 2)) {
                Log.v("HttpUrlFetcher", "Finished http url fetcher fetch in " + LogTime.m2814a(a));
            }
            throw th;
        }
    }

    @NonNull
    /* renamed from: c */
    public DataSource mo9991c() {
        return DataSource.REMOTE;
    }

    public void cancel() {
        this.f956U = true;
    }

    @VisibleForTesting
    HttpUrlFetcher(GlideUrl gVar, int i, C0939b bVar) {
        this.f951P = gVar;
        this.f952Q = i;
        this.f953R = bVar;
    }

    /* renamed from: b */
    public void mo9990b() {
        InputStream inputStream = this.f955T;
        if (inputStream != null) {
            try {
                inputStream.close();
            } catch (IOException unused) {
            }
        }
        HttpURLConnection httpURLConnection = this.f954S;
        if (httpURLConnection != null) {
            httpURLConnection.disconnect();
        }
        this.f954S = null;
    }

    /* renamed from: a */
    private InputStream m1427a(URL url, int i, URL url2, Map<String, String> map) {
        if (i < 5) {
            if (url2 != null) {
                try {
                    if (url.toURI().equals(url2.toURI())) {
                        throw new HttpException("In re-direct loop");
                    }
                } catch (URISyntaxException unused) {
                }
            }
            this.f954S = this.f953R.mo10014a(url);
            for (Map.Entry entry : map.entrySet()) {
                this.f954S.addRequestProperty((String) entry.getKey(), (String) entry.getValue());
            }
            this.f954S.setConnectTimeout(this.f952Q);
            this.f954S.setReadTimeout(this.f952Q);
            this.f954S.setUseCaches(false);
            this.f954S.setDoInput(true);
            this.f954S.setInstanceFollowRedirects(false);
            this.f954S.connect();
            this.f955T = this.f954S.getInputStream();
            if (this.f956U) {
                return null;
            }
            int responseCode = this.f954S.getResponseCode();
            if (m1428a(responseCode)) {
                return m1426a(this.f954S);
            }
            if (m1429b(responseCode)) {
                String headerField = this.f954S.getHeaderField("Location");
                if (!TextUtils.isEmpty(headerField)) {
                    URL url3 = new URL(url, headerField);
                    mo9990b();
                    return m1427a(url3, i + 1, url, map);
                }
                throw new HttpException("Received empty or null redirect url");
            } else if (responseCode == -1) {
                throw new HttpException(responseCode);
            } else {
                throw new HttpException(this.f954S.getResponseMessage(), responseCode);
            }
        } else {
            throw new HttpException("Too many (> 5) redirects!");
        }
    }

    /* renamed from: a */
    private static boolean m1428a(int i) {
        return i / 100 == 2;
    }

    /* renamed from: a */
    private InputStream m1426a(HttpURLConnection httpURLConnection) {
        if (TextUtils.isEmpty(httpURLConnection.getContentEncoding())) {
            this.f955T = ContentLengthInputStream.m2807a(httpURLConnection.getInputStream(), (long) httpURLConnection.getContentLength());
        } else {
            if (Log.isLoggable("HttpUrlFetcher", 3)) {
                Log.d("HttpUrlFetcher", "Got non empty content encoding: " + httpURLConnection.getContentEncoding());
            }
            this.f955T = httpURLConnection.getInputStream();
        }
        return this.f955T;
    }

    @NonNull
    /* renamed from: a */
    public Class<InputStream> mo9984a() {
        return InputStream.class;
    }
}
