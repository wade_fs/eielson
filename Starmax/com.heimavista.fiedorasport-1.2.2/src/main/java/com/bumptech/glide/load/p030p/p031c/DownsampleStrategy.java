package com.bumptech.glide.load.p030p.p031c;

import com.bumptech.glide.load.Option;

/* renamed from: com.bumptech.glide.load.p.c.k */
public abstract class DownsampleStrategy {

    /* renamed from: a */
    public static final DownsampleStrategy f1402a = new C1070e();

    /* renamed from: b */
    public static final DownsampleStrategy f1403b = new C1069d();

    /* renamed from: c */
    public static final DownsampleStrategy f1404c = new C1068c();

    /* renamed from: d */
    public static final DownsampleStrategy f1405d = new C1071f();

    /* renamed from: e */
    public static final DownsampleStrategy f1406e = f1403b;

    /* renamed from: f */
    public static final Option<DownsampleStrategy> f1407f = Option.m1370a("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", f1406e);

    /* renamed from: com.bumptech.glide.load.p.c.k$a */
    /* compiled from: DownsampleStrategy */
    private static class C1066a extends DownsampleStrategy {
        C1066a() {
        }

        /* renamed from: a */
        public C1072g mo10354a(int i, int i2, int i3, int i4) {
            return C1072g.QUALITY;
        }

        /* renamed from: b */
        public float mo10355b(int i, int i2, int i3, int i4) {
            int min = Math.min(i2 / i4, i / i3);
            if (min == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(min));
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.k$b */
    /* compiled from: DownsampleStrategy */
    private static class C1067b extends DownsampleStrategy {
        C1067b() {
        }

        /* renamed from: a */
        public C1072g mo10354a(int i, int i2, int i3, int i4) {
            return C1072g.MEMORY;
        }

        /* renamed from: b */
        public float mo10355b(int i, int i2, int i3, int i4) {
            int ceil = (int) Math.ceil((double) Math.max(((float) i2) / ((float) i4), ((float) i) / ((float) i3)));
            int i5 = 1;
            int max = Math.max(1, Integer.highestOneBit(ceil));
            if (max >= ceil) {
                i5 = 0;
            }
            return 1.0f / ((float) (max << i5));
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.k$c */
    /* compiled from: DownsampleStrategy */
    private static class C1068c extends DownsampleStrategy {
        C1068c() {
        }

        /* renamed from: a */
        public C1072g mo10354a(int i, int i2, int i3, int i4) {
            return C1072g.QUALITY;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* renamed from: b */
        public float mo10355b(int i, int i2, int i3, int i4) {
            return Math.min(1.0f, DownsampleStrategy.f1402a.mo10355b(i, i2, i3, i4));
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.k$d */
    /* compiled from: DownsampleStrategy */
    private static class C1069d extends DownsampleStrategy {
        C1069d() {
        }

        /* renamed from: a */
        public C1072g mo10354a(int i, int i2, int i3, int i4) {
            return C1072g.QUALITY;
        }

        /* renamed from: b */
        public float mo10355b(int i, int i2, int i3, int i4) {
            return Math.max(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.k$e */
    /* compiled from: DownsampleStrategy */
    private static class C1070e extends DownsampleStrategy {
        C1070e() {
        }

        /* renamed from: a */
        public C1072g mo10354a(int i, int i2, int i3, int i4) {
            return C1072g.QUALITY;
        }

        /* renamed from: b */
        public float mo10355b(int i, int i2, int i3, int i4) {
            return Math.min(((float) i3) / ((float) i), ((float) i4) / ((float) i2));
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.k$f */
    /* compiled from: DownsampleStrategy */
    private static class C1071f extends DownsampleStrategy {
        C1071f() {
        }

        /* renamed from: a */
        public C1072g mo10354a(int i, int i2, int i3, int i4) {
            return C1072g.QUALITY;
        }

        /* renamed from: b */
        public float mo10355b(int i, int i2, int i3, int i4) {
            return 1.0f;
        }
    }

    /* renamed from: com.bumptech.glide.load.p.c.k$g */
    /* compiled from: DownsampleStrategy */
    public enum C1072g {
        MEMORY,
        QUALITY
    }

    static {
        new C1066a();
        new C1067b();
    }

    /* renamed from: a */
    public abstract C1072g mo10354a(int i, int i2, int i3, int i4);

    /* renamed from: b */
    public abstract float mo10355b(int i, int i2, int i3, int i4);
}
