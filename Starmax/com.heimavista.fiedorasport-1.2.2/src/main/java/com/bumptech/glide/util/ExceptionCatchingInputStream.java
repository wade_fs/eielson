package com.bumptech.glide.util;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

/* renamed from: com.bumptech.glide.util.c */
public class ExceptionCatchingInputStream extends InputStream {

    /* renamed from: R */
    private static final Queue<ExceptionCatchingInputStream> f1774R = C1122j.m2844a(0);

    /* renamed from: P */
    private InputStream f1775P;

    /* renamed from: Q */
    private IOException f1776Q;

    ExceptionCatchingInputStream() {
    }

    @NonNull
    /* renamed from: b */
    public static ExceptionCatchingInputStream m2808b(@NonNull InputStream inputStream) {
        ExceptionCatchingInputStream poll;
        synchronized (f1774R) {
            poll = f1774R.poll();
        }
        if (poll == null) {
            poll = new ExceptionCatchingInputStream();
        }
        poll.mo10685a(super);
        return poll;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10685a(@NonNull InputStream inputStream) {
        this.f1775P = super;
    }

    public int available() {
        return this.f1775P.available();
    }

    public void close() {
        this.f1775P.close();
    }

    public void mark(int i) {
        this.f1775P.mark(i);
    }

    public boolean markSupported() {
        return this.f1775P.markSupported();
    }

    public int read(byte[] bArr) {
        try {
            return this.f1775P.read(bArr);
        } catch (IOException e) {
            this.f1776Q = e;
            return -1;
        }
    }

    public synchronized void reset() {
        this.f1775P.reset();
    }

    public long skip(long j) {
        try {
            return this.f1775P.skip(j);
        } catch (IOException e) {
            this.f1776Q = e;
            return 0;
        }
    }

    @Nullable
    /* renamed from: a */
    public IOException mo10684a() {
        return this.f1776Q;
    }

    public int read(byte[] bArr, int i, int i2) {
        try {
            return this.f1775P.read(bArr, i, i2);
        } catch (IOException e) {
            this.f1776Q = e;
            return -1;
        }
    }

    public int read() {
        try {
            return this.f1775P.read();
        } catch (IOException e) {
            this.f1776Q = e;
            return -1;
        }
    }

    /* renamed from: b */
    public void mo10687b() {
        this.f1776Q = null;
        this.f1775P = null;
        synchronized (f1774R) {
            f1774R.offer(this);
        }
    }
}
