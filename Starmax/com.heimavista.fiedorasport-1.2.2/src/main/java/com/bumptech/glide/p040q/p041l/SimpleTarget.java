package com.bumptech.glide.p040q.p041l;

import androidx.annotation.NonNull;
import com.bumptech.glide.util.C1122j;

@Deprecated
/* renamed from: com.bumptech.glide.q.l.g */
public abstract class SimpleTarget<Z> extends BaseTarget<Z> {

    /* renamed from: Q */
    private final int f1748Q;

    /* renamed from: R */
    private final int f1749R;

    public SimpleTarget() {
        this(Integer.MIN_VALUE, Integer.MIN_VALUE);
    }

    /* renamed from: a */
    public void mo10641a(@NonNull SizeReadyCallback hVar) {
    }

    /* renamed from: b */
    public final void mo10645b(@NonNull SizeReadyCallback hVar) {
        if (C1122j.m2849b(this.f1748Q, this.f1749R)) {
            hVar.mo10656a(this.f1748Q, this.f1749R);
            return;
        }
        throw new IllegalArgumentException("Width and height must both be > 0 or Target#SIZE_ORIGINAL, but given width: " + this.f1748Q + " and height: " + this.f1749R + ", either provide dimensions in the constructor or call override()");
    }

    public SimpleTarget(int i, int i2) {
        this.f1748Q = i;
        this.f1749R = i2;
    }
}
