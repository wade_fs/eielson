package com.bumptech.glide.load.p028o;

import android.net.Uri;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.util.Preconditions;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.o.g */
public class GlideUrl implements Key {

    /* renamed from: b */
    private final Headers f1305b;
    @Nullable

    /* renamed from: c */
    private final URL f1306c;
    @Nullable

    /* renamed from: d */
    private final String f1307d;
    @Nullable

    /* renamed from: e */
    private String f1308e;
    @Nullable

    /* renamed from: f */
    private URL f1309f;
    @Nullable

    /* renamed from: g */
    private volatile byte[] f1310g;

    /* renamed from: h */
    private int f1311h;

    public GlideUrl(URL url) {
        this(url, Headers.f1312a);
    }

    /* renamed from: d */
    private byte[] m1941d() {
        if (this.f1310g == null) {
            this.f1310g = mo10284a().getBytes(Key.f928a);
        }
        return this.f1310g;
    }

    /* renamed from: e */
    private String m1942e() {
        if (TextUtils.isEmpty(this.f1308e)) {
            String str = this.f1307d;
            if (TextUtils.isEmpty(str)) {
                URL url = this.f1306c;
                Preconditions.m2828a(url);
                str = url.toString();
            }
            this.f1308e = Uri.encode(str, "@#&=*+-_.,:!?()/~'%;$");
        }
        return this.f1308e;
    }

    /* renamed from: f */
    private URL m1943f() {
        if (this.f1309f == null) {
            this.f1309f = new URL(m1942e());
        }
        return this.f1309f;
    }

    /* renamed from: a */
    public String mo10284a() {
        String str = this.f1307d;
        if (str != null) {
            return str;
        }
        URL url = this.f1306c;
        Preconditions.m2828a(url);
        return url.toString();
    }

    /* renamed from: b */
    public Map<String, String> mo10285b() {
        return this.f1305b.mo10288a();
    }

    /* renamed from: c */
    public URL mo10286c() {
        return m1943f();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof GlideUrl)) {
            return false;
        }
        GlideUrl gVar = (GlideUrl) obj;
        if (!mo10284a().equals(gVar.mo10284a()) || !this.f1305b.equals(gVar.f1305b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        if (this.f1311h == 0) {
            this.f1311h = mo10284a().hashCode();
            this.f1311h = (this.f1311h * 31) + this.f1305b.hashCode();
        }
        return this.f1311h;
    }

    public String toString() {
        return mo10284a();
    }

    public GlideUrl(String str) {
        this(str, Headers.f1312a);
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        messageDigest.update(m1941d());
    }

    public GlideUrl(URL url, Headers hVar) {
        Preconditions.m2828a(url);
        this.f1306c = url;
        this.f1307d = null;
        Preconditions.m2828a(hVar);
        this.f1305b = hVar;
    }

    public GlideUrl(String str, Headers hVar) {
        this.f1306c = null;
        Preconditions.m2830a(str);
        this.f1307d = str;
        Preconditions.m2828a(hVar);
        this.f1305b = hVar;
    }
}
