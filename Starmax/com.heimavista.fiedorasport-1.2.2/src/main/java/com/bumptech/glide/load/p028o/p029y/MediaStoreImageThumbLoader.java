package com.bumptech.glide.load.p028o.p029y;

import android.content.Context;
import android.net.Uri;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.p022o.MediaStoreUtil;
import com.bumptech.glide.load.p021m.p022o.ThumbFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p028o.ModelLoaderFactory;
import com.bumptech.glide.load.p028o.MultiModelLoaderFactory;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.y.c */
public class MediaStoreImageThumbLoader implements ModelLoader<Uri, InputStream> {

    /* renamed from: a */
    private final Context f1377a;

    /* renamed from: com.bumptech.glide.load.o.y.c$a */
    /* compiled from: MediaStoreImageThumbLoader */
    public static class C1059a implements ModelLoaderFactory<Uri, InputStream> {

        /* renamed from: a */
        private final Context f1378a;

        public C1059a(Context context) {
            this.f1378a = context;
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<Uri, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new MediaStoreImageThumbLoader(this.f1378a);
        }
    }

    public MediaStoreImageThumbLoader(Context context) {
        this.f1377a = context.getApplicationContext();
    }

    /* renamed from: a */
    public ModelLoader.C1036a<InputStream> mo10261a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        if (MediaStoreUtil.m1462a(i, i2)) {
            return new ModelLoader.C1036a<>(new ObjectKey(uri), ThumbFetcher.m1467a(this.f1377a, uri));
        }
        return null;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Uri uri) {
        return MediaStoreUtil.m1463a(uri);
    }
}
