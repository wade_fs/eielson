package com.bumptech.glide.load.p021m;

import android.content.ContentResolver;
import android.content.UriMatcher;
import android.net.Uri;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import java.io.FileNotFoundException;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.m.n */
public class StreamLocalUriFetcher extends LocalUriFetcher<InputStream> {

    /* renamed from: S */
    private static final UriMatcher f962S = new UriMatcher(-1);

    static {
        f962S.addURI("com.android.contacts", "contacts/lookup/*/#", 1);
        f962S.addURI("com.android.contacts", "contacts/lookup/*", 1);
        f962S.addURI("com.android.contacts", "contacts/#/photo", 2);
        f962S.addURI("com.android.contacts", "contacts/#", 3);
        f962S.addURI("com.android.contacts", "contacts/#/display_photo", 4);
        f962S.addURI("com.android.contacts", "phone_lookup/*", 5);
    }

    public StreamLocalUriFetcher(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    /* renamed from: b */
    private InputStream m1453b(Uri uri, ContentResolver contentResolver) {
        int match = f962S.match(uri);
        if (match != 1) {
            if (match == 3) {
                return m1452a(contentResolver, uri);
            }
            if (match != 5) {
                return contentResolver.openInputStream(uri);
            }
        }
        Uri lookupContact = ContactsContract.Contacts.lookupContact(contentResolver, uri);
        if (lookupContact != null) {
            return m1452a(contentResolver, lookupContact);
        }
        throw new FileNotFoundException("Contact cannot be found");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public InputStream m1456a(Uri uri, ContentResolver contentResolver) {
        InputStream b = m1453b(uri, contentResolver);
        if (b != null) {
            return b;
        }
        throw new FileNotFoundException("InputStream is null for " + uri);
    }

    /* renamed from: a */
    private InputStream m1452a(ContentResolver contentResolver, Uri uri) {
        return ContactsContract.Contacts.openContactPhotoInputStream(contentResolver, uri, true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo9986a(InputStream inputStream) {
        inputStream.close();
    }

    @NonNull
    /* renamed from: a */
    public Class<InputStream> mo9984a() {
        return InputStream.class;
    }
}
