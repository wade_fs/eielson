package com.bumptech.glide.load.p028o;

import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.o.j */
public final class LazyHeaders implements Headers {

    /* renamed from: b */
    private final Map<String, List<LazyHeaderFactory>> f1313b;

    /* renamed from: c */
    private volatile Map<String, String> f1314c;

    /* renamed from: com.bumptech.glide.load.o.j$a */
    /* compiled from: LazyHeaders */
    public static final class C1030a {

        /* renamed from: b */
        private static final String f1315b = m1954b();

        /* renamed from: c */
        private static final Map<String, List<LazyHeaderFactory>> f1316c;

        /* renamed from: a */
        private Map<String, List<LazyHeaderFactory>> f1317a = f1316c;

        static {
            HashMap hashMap = new HashMap(2);
            if (!TextUtils.isEmpty(f1315b)) {
                hashMap.put("User-Agent", Collections.singletonList(new C1031b(f1315b)));
            }
            f1316c = Collections.unmodifiableMap(hashMap);
        }

        @VisibleForTesting
        /* renamed from: b */
        static String m1954b() {
            String property = System.getProperty("http.agent");
            if (TextUtils.isEmpty(property)) {
                return property;
            }
            int length = property.length();
            StringBuilder sb = new StringBuilder(property.length());
            for (int i = 0; i < length; i++) {
                char charAt = property.charAt(i);
                if ((charAt > 31 || charAt == 9) && charAt < 127) {
                    sb.append(charAt);
                } else {
                    sb.append('?');
                }
            }
            return sb.toString();
        }

        /* renamed from: a */
        public LazyHeaders mo10293a() {
            return new LazyHeaders(this.f1317a);
        }
    }

    /* renamed from: com.bumptech.glide.load.o.j$b */
    /* compiled from: LazyHeaders */
    static final class C1031b implements LazyHeaderFactory {
        @NonNull

        /* renamed from: a */
        private final String f1318a;

        C1031b(@NonNull String str) {
            this.f1318a = str;
        }

        /* renamed from: a */
        public String mo10289a() {
            return this.f1318a;
        }

        public boolean equals(Object obj) {
            if (obj instanceof C1031b) {
                return this.f1318a.equals(((C1031b) obj).f1318a);
            }
            return false;
        }

        public int hashCode() {
            return this.f1318a.hashCode();
        }

        public String toString() {
            return "StringHeaderFactory{value='" + this.f1318a + '\'' + '}';
        }
    }

    LazyHeaders(Map<String, List<LazyHeaderFactory>> map) {
        this.f1313b = Collections.unmodifiableMap(map);
    }

    /* renamed from: b */
    private Map<String, String> m1952b() {
        HashMap hashMap = new HashMap();
        for (Map.Entry entry : this.f1313b.entrySet()) {
            String a = m1951a((List) entry.getValue());
            if (!TextUtils.isEmpty(a)) {
                hashMap.put(entry.getKey(), a);
            }
        }
        return hashMap;
    }

    /* renamed from: a */
    public Map<String, String> mo10288a() {
        if (this.f1314c == null) {
            synchronized (this) {
                if (this.f1314c == null) {
                    this.f1314c = Collections.unmodifiableMap(m1952b());
                }
            }
        }
        return this.f1314c;
    }

    public boolean equals(Object obj) {
        if (obj instanceof LazyHeaders) {
            return this.f1313b.equals(((LazyHeaders) obj).f1313b);
        }
        return false;
    }

    public int hashCode() {
        return this.f1313b.hashCode();
    }

    public String toString() {
        return "LazyHeaders{headers=" + this.f1313b + '}';
    }

    @NonNull
    /* renamed from: a */
    private String m1951a(@NonNull List<LazyHeaderFactory> list) {
        StringBuilder sb = new StringBuilder();
        int size = list.size();
        for (int i = 0; i < size; i++) {
            String a = list.get(i).mo10289a();
            if (!TextUtils.isEmpty(a)) {
                sb.append(a);
                if (i != list.size() - 1) {
                    sb.append(',');
                }
            }
        }
        return sb.toString();
    }
}
