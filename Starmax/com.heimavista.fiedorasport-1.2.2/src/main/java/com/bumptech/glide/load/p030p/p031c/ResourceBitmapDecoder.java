package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.p033e.ResourceDrawableDecoder;

/* renamed from: com.bumptech.glide.load.p.c.t */
public class ResourceBitmapDecoder implements ResourceDecoder<Uri, Bitmap> {

    /* renamed from: a */
    private final ResourceDrawableDecoder f1439a;

    /* renamed from: b */
    private final BitmapPool f1440b;

    public ResourceBitmapDecoder(ResourceDrawableDecoder dVar, BitmapPool eVar) {
        this.f1439a = dVar;
        this.f1440b = eVar;
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull Uri uri, @NonNull Options iVar) {
        return "android.resource".equals(uri.getScheme());
    }

    @Nullable
    /* renamed from: a */
    public Resource<Bitmap> mo9979a(@NonNull Uri uri, int i, int i2, @NonNull Options iVar) {
        Resource<Drawable> a = this.f1439a.mo9979a(uri, i, i2, iVar);
        if (a == null) {
            return null;
        }
        return DrawableToBitmapConverter.m2175a(this.f1440b, a.get(), i, i2);
    }
}
