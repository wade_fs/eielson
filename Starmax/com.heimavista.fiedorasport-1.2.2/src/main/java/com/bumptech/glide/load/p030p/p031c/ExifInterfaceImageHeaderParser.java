package com.bumptech.glide.load.p030p.p031c;

import android.media.ExifInterface;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import java.io.InputStream;
import java.nio.ByteBuffer;

@RequiresApi(27)
/* renamed from: com.bumptech.glide.load.p.c.o */
public final class ExifInterfaceImageHeaderParser implements ImageHeaderParser {
    @NonNull
    /* renamed from: a */
    public ImageHeaderParser.ImageType mo9962a(@NonNull InputStream inputStream) {
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    @NonNull
    /* renamed from: a */
    public ImageHeaderParser.ImageType mo9963a(@NonNull ByteBuffer byteBuffer) {
        return ImageHeaderParser.ImageType.UNKNOWN;
    }

    /* renamed from: a */
    public int mo9961a(@NonNull InputStream inputStream, @NonNull ArrayPool bVar) {
        int attributeInt = new ExifInterface(inputStream).getAttributeInt(androidx.exifinterface.media.ExifInterface.TAG_ORIENTATION, 1);
        if (attributeInt == 0) {
            return -1;
        }
        return attributeInt;
    }
}
