package com.bumptech.glide.manager;

import androidx.annotation.NonNull;

/* renamed from: com.bumptech.glide.manager.b */
class ApplicationLifecycle implements Lifecycle {
    ApplicationLifecycle() {
    }

    /* renamed from: a */
    public void mo10499a(@NonNull LifecycleListener iVar) {
        iVar.onStart();
    }

    /* renamed from: b */
    public void mo10501b(@NonNull LifecycleListener iVar) {
    }
}
