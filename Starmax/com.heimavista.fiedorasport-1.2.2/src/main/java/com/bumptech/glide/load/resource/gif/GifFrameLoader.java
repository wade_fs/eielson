package com.bumptech.glide.load.resource.gif;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.DiskCacheStrategy;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.p037n.GifDecoder;
import com.bumptech.glide.p040q.BaseRequestOptions;
import com.bumptech.glide.p040q.RequestOptions;
import com.bumptech.glide.p040q.p041l.SimpleTarget;
import com.bumptech.glide.p040q.p041l.Target;
import com.bumptech.glide.p040q.p042m.Transition;
import com.bumptech.glide.p043r.ObjectKey;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.Preconditions;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.bumptech.glide.load.resource.gif.f */
class GifFrameLoader {

    /* renamed from: a */
    private final GifDecoder f1498a;

    /* renamed from: b */
    private final Handler f1499b;

    /* renamed from: c */
    private final List<C1092b> f1500c;

    /* renamed from: d */
    final RequestManager f1501d;

    /* renamed from: e */
    private final BitmapPool f1502e;

    /* renamed from: f */
    private boolean f1503f;

    /* renamed from: g */
    private boolean f1504g;

    /* renamed from: h */
    private boolean f1505h;

    /* renamed from: i */
    private RequestBuilder<Bitmap> f1506i;

    /* renamed from: j */
    private C1091a f1507j;

    /* renamed from: k */
    private boolean f1508k;

    /* renamed from: l */
    private C1091a f1509l;

    /* renamed from: m */
    private Bitmap f1510m;

    /* renamed from: n */
    private C1091a f1511n;
    @Nullable

    /* renamed from: o */
    private C1094d f1512o;

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.resource.gif.f$a */
    /* compiled from: GifFrameLoader */
    static class C1091a extends SimpleTarget<Bitmap> {

        /* renamed from: S */
        private final Handler f1513S;

        /* renamed from: T */
        final int f1514T;

        /* renamed from: U */
        private final long f1515U;

        /* renamed from: V */
        private Bitmap f1516V;

        C1091a(Handler handler, int i, long j) {
            this.f1513S = handler;
            this.f1514T = i;
            this.f1515U = j;
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo10453a(@NonNull Object obj, @Nullable Transition bVar) {
            mo10452a((Bitmap) obj, (Transition<? super Bitmap>) bVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: d */
        public Bitmap mo10454d() {
            return this.f1516V;
        }

        /* renamed from: a */
        public void mo10452a(@NonNull Bitmap bitmap, @Nullable Transition<? super Bitmap> bVar) {
            this.f1516V = bitmap;
            this.f1513S.sendMessageAtTime(this.f1513S.obtainMessage(1, this), this.f1515U);
        }
    }

    /* renamed from: com.bumptech.glide.load.resource.gif.f$b */
    /* compiled from: GifFrameLoader */
    public interface C1092b {
        /* renamed from: a */
        void mo10404a();
    }

    /* renamed from: com.bumptech.glide.load.resource.gif.f$c */
    /* compiled from: GifFrameLoader */
    private class C1093c implements Handler.Callback {
        C1093c() {
        }

        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                GifFrameLoader.this.mo10441a((C1091a) message.obj);
                return true;
            } else if (i != 2) {
                return false;
            } else {
                GifFrameLoader.this.f1501d.mo9944a((C1091a) message.obj);
                return false;
            }
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.resource.gif.f$d */
    /* compiled from: GifFrameLoader */
    interface C1094d {
        /* renamed from: a */
        void mo10456a();
    }

    GifFrameLoader(Glide cVar, GifDecoder aVar, int i, int i2, Transformation<Bitmap> lVar, Bitmap bitmap) {
        this(cVar.mo9894c(), Glide.m1282e(cVar.mo9896e()), aVar, null, m2329a(Glide.m1282e(cVar.mo9896e()), i, i2), lVar, bitmap);
    }

    /* renamed from: j */
    private static Key m2330j() {
        return new ObjectKey(Double.valueOf(Math.random()));
    }

    /* renamed from: k */
    private int m2331k() {
        return C1122j.m2836a(mo10445c().getWidth(), mo10445c().getHeight(), mo10445c().getConfig());
    }

    /* renamed from: l */
    private void m2332l() {
        if (this.f1503f && !this.f1504g) {
            if (this.f1505h) {
                Preconditions.m2832a(this.f1511n == null, "Pending target must be null when starting from the first frame");
                this.f1498a.mo10533e();
                this.f1505h = false;
            }
            C1091a aVar = this.f1511n;
            if (aVar != null) {
                this.f1511n = null;
                mo10441a(aVar);
                return;
            }
            this.f1504g = true;
            long uptimeMillis = SystemClock.uptimeMillis() + ((long) this.f1498a.mo10532d());
            this.f1498a.mo10529b();
            this.f1509l = new C1091a(this.f1499b, this.f1498a.mo10534f(), uptimeMillis);
            RequestBuilder<Bitmap> a = this.f1506i.mo9932a((BaseRequestOptions<?>) RequestOptions.m2677b(m2330j()));
            a.mo9934a(this.f1498a);
            a.mo9936a((Target) this.f1509l);
        }
    }

    /* renamed from: m */
    private void m2333m() {
        Bitmap bitmap = this.f1510m;
        if (bitmap != null) {
            this.f1502e.mo10063a(bitmap);
            this.f1510m = null;
        }
    }

    /* renamed from: n */
    private void m2334n() {
        if (!this.f1503f) {
            this.f1503f = true;
            this.f1508k = false;
            m2332l();
        }
    }

    /* renamed from: o */
    private void m2335o() {
        this.f1503f = false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10440a(Transformation<Bitmap> lVar, Bitmap bitmap) {
        Preconditions.m2828a(lVar);
        Preconditions.m2828a(bitmap);
        this.f1510m = bitmap;
        this.f1506i = this.f1506i.mo9932a((BaseRequestOptions<?>) new RequestOptions().mo10585a(lVar));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo10444b(C1092b bVar) {
        this.f1500c.remove(bVar);
        if (this.f1500c.isEmpty()) {
            m2335o();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public Bitmap mo10445c() {
        C1091a aVar = this.f1507j;
        return aVar != null ? aVar.mo10454d() : this.f1510m;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public int mo10446d() {
        C1091a aVar = this.f1507j;
        if (aVar != null) {
            return aVar.f1514T;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public Bitmap mo10447e() {
        return this.f1510m;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public int mo10448f() {
        return this.f1498a.mo10530c();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public int mo10449g() {
        return mo10445c().getHeight();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h */
    public int mo10450h() {
        return this.f1498a.mo10535g() + m2331k();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: i */
    public int mo10451i() {
        return mo10445c().getWidth();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10442a(C1092b bVar) {
        if (this.f1508k) {
            throw new IllegalStateException("Cannot subscribe to a cleared frame loader");
        } else if (!this.f1500c.contains(bVar)) {
            boolean isEmpty = this.f1500c.isEmpty();
            this.f1500c.add(bVar);
            if (isEmpty) {
                m2334n();
            }
        } else {
            throw new IllegalStateException("Cannot subscribe twice in a row");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public ByteBuffer mo10443b() {
        return this.f1498a.getData().asReadOnlyBuffer();
    }

    GifFrameLoader(BitmapPool eVar, RequestManager kVar, GifDecoder aVar, Handler handler, RequestBuilder<Bitmap> jVar, Transformation<Bitmap> lVar, Bitmap bitmap) {
        this.f1500c = new ArrayList();
        this.f1501d = kVar;
        handler = handler == null ? new Handler(Looper.getMainLooper(), new C1093c()) : handler;
        this.f1502e = eVar;
        this.f1499b = handler;
        this.f1506i = jVar;
        this.f1498a = aVar;
        mo10440a(lVar, bitmap);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10439a() {
        this.f1500c.clear();
        m2333m();
        m2335o();
        C1091a aVar = this.f1507j;
        if (aVar != null) {
            this.f1501d.mo9944a(aVar);
            this.f1507j = null;
        }
        C1091a aVar2 = this.f1509l;
        if (aVar2 != null) {
            this.f1501d.mo9944a(aVar2);
            this.f1509l = null;
        }
        C1091a aVar3 = this.f1511n;
        if (aVar3 != null) {
            this.f1501d.mo9944a(aVar3);
            this.f1511n = null;
        }
        this.f1498a.clear();
        this.f1508k = true;
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: a */
    public void mo10441a(C1091a aVar) {
        C1094d dVar = this.f1512o;
        if (dVar != null) {
            dVar.mo10456a();
        }
        this.f1504g = false;
        if (this.f1508k) {
            this.f1499b.obtainMessage(2, aVar).sendToTarget();
        } else if (!this.f1503f) {
            this.f1511n = aVar;
        } else {
            if (aVar.mo10454d() != null) {
                m2333m();
                C1091a aVar2 = this.f1507j;
                this.f1507j = aVar;
                for (int size = this.f1500c.size() - 1; size >= 0; size--) {
                    this.f1500c.get(size).mo10404a();
                }
                if (aVar2 != null) {
                    this.f1499b.obtainMessage(2, aVar2).sendToTarget();
                }
            }
            m2332l();
        }
    }

    /* renamed from: a */
    private static RequestBuilder<Bitmap> m2329a(RequestManager kVar, int i, int i2) {
        return kVar.mo9950d().mo9932a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) RequestOptions.m2678b(DiskCacheStrategy.f1165a).mo10596b(true)).mo10592a(true)).mo10581a(i, i2));
    }
}
