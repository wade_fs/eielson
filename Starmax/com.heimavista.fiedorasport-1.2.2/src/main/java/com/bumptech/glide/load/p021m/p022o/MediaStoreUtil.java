package com.bumptech.glide.load.p021m.p022o;

import android.net.Uri;

/* renamed from: com.bumptech.glide.load.m.o.b */
public final class MediaStoreUtil {
    /* renamed from: a */
    public static boolean m1462a(int i, int i2) {
        return i != Integer.MIN_VALUE && i2 != Integer.MIN_VALUE && i <= 512 && i2 <= 384;
    }

    /* renamed from: a */
    public static boolean m1463a(Uri uri) {
        return m1464b(uri) && !m1466d(uri);
    }

    /* renamed from: b */
    public static boolean m1464b(Uri uri) {
        return uri != null && "content".equals(uri.getScheme()) && "media".equals(uri.getAuthority());
    }

    /* renamed from: c */
    public static boolean m1465c(Uri uri) {
        return m1464b(uri) && m1466d(uri);
    }

    /* renamed from: d */
    private static boolean m1466d(Uri uri) {
        return uri.getPathSegments().contains("video");
    }
}
