package com.bumptech.glide.load.p023n;

import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p023n.DataFetcherGenerator;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.util.LogTime;
import java.util.Collections;
import java.util.List;

/* renamed from: com.bumptech.glide.load.n.z */
class SourceGenerator implements DataFetcherGenerator, DataFetcher.C0934a<Object>, DataFetcherGenerator.C0977a {

    /* renamed from: P */
    private final DecodeHelper<?> f1279P;

    /* renamed from: Q */
    private final DataFetcherGenerator.C0977a f1280Q;

    /* renamed from: R */
    private int f1281R;

    /* renamed from: S */
    private DataCacheGenerator f1282S;

    /* renamed from: T */
    private Object f1283T;

    /* renamed from: U */
    private volatile ModelLoader.C1036a<?> f1284U;

    /* renamed from: V */
    private DataCacheKey f1285V;

    SourceGenerator(DecodeHelper<?> gVar, DataFetcherGenerator.C0977a aVar) {
        this.f1279P = gVar;
        this.f1280Q = aVar;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    private void m1861b(Object obj) {
        long a = LogTime.m2815a();
        try {
            Encoder<X> a2 = this.f1279P.mo10140a(obj);
            DataCacheWriter eVar = new DataCacheWriter(a2, obj, this.f1279P.mo10157i());
            this.f1285V = new DataCacheKey(this.f1284U.f1329a, this.f1279P.mo10160l());
            this.f1279P.mo10152d().mo10089a(this.f1285V, eVar);
            if (Log.isLoggable("SourceGenerator", 2)) {
                Log.v("SourceGenerator", "Finished encoding source to cache, key: " + this.f1285V + ", data: " + obj + ", encoder: " + a2 + ", duration: " + LogTime.m2814a(a));
            }
            this.f1284U.f1331c.mo9990b();
            this.f1282S = new DataCacheGenerator(Collections.singletonList(this.f1284U.f1329a), this.f1279P, this);
        } catch (Throwable th) {
            this.f1284U.f1331c.mo9990b();
            throw th;
        }
    }

    /* renamed from: c */
    private boolean m1862c() {
        return this.f1281R < this.f1279P.mo10155g().size();
    }

    /* renamed from: a */
    public boolean mo10116a() {
        Object obj = this.f1283T;
        if (obj != null) {
            this.f1283T = null;
            m1861b(obj);
        }
        DataCacheGenerator cVar = this.f1282S;
        if (cVar != null && cVar.mo10116a()) {
            return true;
        }
        this.f1282S = null;
        this.f1284U = null;
        boolean z = false;
        while (!z && m1862c()) {
            List<ModelLoader.C1036a<?>> g = this.f1279P.mo10155g();
            int i = this.f1281R;
            this.f1281R = i + 1;
            this.f1284U = g.get(i);
            if (this.f1284U != null && (this.f1279P.mo10153e().mo10188a(this.f1284U.f1331c.mo9991c()) || this.f1279P.mo10151c(this.f1284U.f1331c.mo9984a()))) {
                this.f1284U.f1331c.mo9988a(this.f1279P.mo10158j(), this);
                z = true;
            }
        }
        return z;
    }

    public void cancel() {
        ModelLoader.C1036a<?> aVar = this.f1284U;
        if (aVar != null) {
            aVar.f1331c.cancel();
        }
    }

    /* renamed from: a */
    public void mo9999a(Object obj) {
        DiskCacheStrategy e = this.f1279P.mo10153e();
        if (obj == null || !e.mo10188a(this.f1284U.f1331c.mo9991c())) {
            this.f1280Q.mo10138a(this.f1284U.f1329a, obj, this.f1284U.f1331c, this.f1284U.f1331c.mo9991c(), this.f1285V);
            return;
        }
        this.f1283T = obj;
        this.f1280Q.mo10139b();
    }

    /* renamed from: b */
    public void mo10139b() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void mo9998a(@NonNull Exception exc) {
        this.f1280Q.mo10137a(this.f1285V, exc, this.f1284U.f1331c, this.f1284U.f1331c.mo9991c());
    }

    /* renamed from: a */
    public void mo10138a(Key gVar, Object obj, DataFetcher<?> dVar, DataSource aVar, Key gVar2) {
        this.f1280Q.mo10138a(gVar, obj, dVar, this.f1284U.f1331c.mo9991c(), gVar);
    }

    /* renamed from: a */
    public void mo10137a(Key gVar, Exception exc, DataFetcher<?> dVar, DataSource aVar) {
        this.f1280Q.mo10137a(gVar, exc, dVar, this.f1284U.f1331c.mo9991c());
    }
}
