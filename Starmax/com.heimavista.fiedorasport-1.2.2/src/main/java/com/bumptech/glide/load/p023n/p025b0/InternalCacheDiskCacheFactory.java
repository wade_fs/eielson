package com.bumptech.glide.load.p023n.p025b0;

import android.content.Context;
import com.bumptech.glide.load.p023n.p025b0.DiskLruCacheFactory;
import java.io.File;

/* renamed from: com.bumptech.glide.load.n.b0.f */
public final class InternalCacheDiskCacheFactory extends DiskLruCacheFactory {

    /* renamed from: com.bumptech.glide.load.n.b0.f$a */
    /* compiled from: InternalCacheDiskCacheFactory */
    class C0963a implements DiskLruCacheFactory.C0962a {

        /* renamed from: a */
        final /* synthetic */ Context f1043a;

        /* renamed from: b */
        final /* synthetic */ String f1044b;

        C0963a(Context context, String str) {
            this.f1043a = context;
            this.f1044b = str;
        }

        /* renamed from: a */
        public File mo10096a() {
            File cacheDir = this.f1043a.getCacheDir();
            if (cacheDir == null) {
                return null;
            }
            String str = this.f1044b;
            return str != null ? new File(cacheDir, str) : cacheDir;
        }
    }

    public InternalCacheDiskCacheFactory(Context context) {
        this(context, "image_manager_disk_cache", 262144000);
    }

    public InternalCacheDiskCacheFactory(Context context, String str, long j) {
        super(new C0963a(context, str), j);
    }
}
