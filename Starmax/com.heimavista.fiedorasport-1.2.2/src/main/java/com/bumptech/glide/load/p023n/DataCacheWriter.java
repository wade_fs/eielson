package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import java.io.File;

/* renamed from: com.bumptech.glide.load.n.e */
class DataCacheWriter<DataType> implements DiskCache.C0959b {

    /* renamed from: a */
    private final Encoder<DataType> f1086a;

    /* renamed from: b */
    private final DataType f1087b;

    /* renamed from: c */
    private final Options f1088c;

    DataCacheWriter(Encoder<DataType> dVar, DataType datatype, Options iVar) {
        this.f1086a = dVar;
        this.f1087b = datatype;
        this.f1088c = iVar;
    }

    /* renamed from: a */
    public boolean mo10091a(@NonNull File file) {
        return this.f1086a.mo9965a(this.f1087b, file, this.f1088c);
    }
}
