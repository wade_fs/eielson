package com.bumptech.glide.load.p021m;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;

/* renamed from: com.bumptech.glide.load.m.d */
public interface DataFetcher<T> {

    /* renamed from: com.bumptech.glide.load.m.d$a */
    /* compiled from: DataFetcher */
    public interface C0934a<T> {
        /* renamed from: a */
        void mo9998a(@NonNull Exception exc);

        /* renamed from: a */
        void mo9999a(@Nullable Object obj);
    }

    @NonNull
    /* renamed from: a */
    Class<T> mo9984a();

    /* renamed from: a */
    void mo9988a(@NonNull Priority hVar, @NonNull C0934a aVar);

    /* renamed from: b */
    void mo9990b();

    @NonNull
    /* renamed from: c */
    DataSource mo9991c();

    void cancel();
}
