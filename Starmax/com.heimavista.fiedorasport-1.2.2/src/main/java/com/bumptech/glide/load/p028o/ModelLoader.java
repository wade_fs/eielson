package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.util.Preconditions;
import java.util.Collections;
import java.util.List;

/* renamed from: com.bumptech.glide.load.o.n */
public interface ModelLoader<Model, Data> {

    /* renamed from: com.bumptech.glide.load.o.n$a */
    /* compiled from: ModelLoader */
    public static class C1036a<Data> {

        /* renamed from: a */
        public final Key f1329a;

        /* renamed from: b */
        public final List<Key> f1330b;

        /* renamed from: c */
        public final DataFetcher<Data> f1331c;

        public C1036a(@NonNull Key gVar, @NonNull DataFetcher<Data> dVar) {
            this(gVar, Collections.emptyList(), dVar);
        }

        public C1036a(@NonNull Key gVar, @NonNull List<Key> list, @NonNull DataFetcher<Data> dVar) {
            Preconditions.m2828a(gVar);
            this.f1329a = gVar;
            Preconditions.m2828a((Object) list);
            this.f1330b = list;
            Preconditions.m2828a(dVar);
            this.f1331c = dVar;
        }
    }

    @Nullable
    /* renamed from: a */
    C1036a<Data> mo10261a(@NonNull Object obj, int i, int i2, @NonNull Options iVar);

    /* renamed from: a */
    boolean mo10263a(@NonNull Object obj);
}
