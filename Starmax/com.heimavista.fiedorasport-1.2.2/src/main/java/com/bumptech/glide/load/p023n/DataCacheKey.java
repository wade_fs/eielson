package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.n.d */
final class DataCacheKey implements Key {

    /* renamed from: b */
    private final Key f1084b;

    /* renamed from: c */
    private final Key f1085c;

    DataCacheKey(Key gVar, Key gVar2) {
        this.f1084b = gVar;
        this.f1085c = gVar2;
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        this.f1084b.mo9966a(messageDigest);
        this.f1085c.mo9966a(messageDigest);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof DataCacheKey)) {
            return false;
        }
        DataCacheKey dVar = (DataCacheKey) obj;
        if (!this.f1084b.equals(dVar.f1084b) || !this.f1085c.equals(dVar.f1085c)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f1084b.hashCode() * 31) + this.f1085c.hashCode();
    }

    public String toString() {
        return "DataCacheKey{sourceKey=" + this.f1084b + ", signature=" + this.f1085c + '}';
    }
}
