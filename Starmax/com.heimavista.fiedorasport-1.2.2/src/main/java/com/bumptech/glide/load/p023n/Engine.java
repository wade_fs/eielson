package com.bumptech.glide.load.p023n;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.util.Pools;
import com.bumptech.glide.GlideContext;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.DecodeJob;
import com.bumptech.glide.load.p023n.EngineResource;
import com.bumptech.glide.load.p023n.p025b0.DiskCache;
import com.bumptech.glide.load.p023n.p025b0.DiskCacheAdapter;
import com.bumptech.glide.load.p023n.p025b0.MemoryCache;
import com.bumptech.glide.load.p023n.p026c0.GlideExecutor;
import com.bumptech.glide.p040q.ResourceCallback;
import com.bumptech.glide.util.LogTime;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.p044k.FactoryPools;
import java.util.Map;

/* renamed from: com.bumptech.glide.load.n.k */
public class Engine implements EngineJobListener, MemoryCache.C0964a, EngineResource.C1003a {

    /* renamed from: i */
    private static final boolean f1168i = Log.isLoggable("Engine", 2);

    /* renamed from: a */
    private final Jobs f1169a;

    /* renamed from: b */
    private final EngineKeyFactory f1170b;

    /* renamed from: c */
    private final MemoryCache f1171c;

    /* renamed from: d */
    private final C0994b f1172d;

    /* renamed from: e */
    private final ResourceRecycler f1173e;

    /* renamed from: f */
    private final C0996c f1174f;

    /* renamed from: g */
    private final C0992a f1175g;

    /* renamed from: h */
    private final ActiveResources f1176h;

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.k$a */
    /* compiled from: Engine */
    static class C0992a {

        /* renamed from: a */
        final DecodeJob.C0982e f1177a;

        /* renamed from: b */
        final Pools.Pool<DecodeJob<?>> f1178b = FactoryPools.m2854a(150, new C0993a());

        /* renamed from: c */
        private int f1179c;

        /* renamed from: com.bumptech.glide.load.n.k$a$a */
        /* compiled from: Engine */
        class C0993a implements FactoryPools.C1127d<DecodeJob<?>> {
            C0993a() {
            }

            /* renamed from: a */
            public DecodeJob<?> m1781a() {
                C0992a aVar = C0992a.this;
                return new DecodeJob<>(aVar.f1177a, aVar.f1178b);
            }
        }

        C0992a(DecodeJob.C0982e eVar) {
            this.f1177a = eVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public <R> DecodeJob<R> mo10196a(GlideContext eVar, Object obj, EngineKey nVar, Key gVar, int i, int i2, Class<?> cls, Class<R> cls2, Priority hVar, DiskCacheStrategy jVar, Map<Class<?>, Transformation<?>> map, boolean z, boolean z2, boolean z3, Options iVar, DecodeJob.C0979b<R> bVar) {
            DecodeJob<R> acquire = this.f1178b.acquire();
            Preconditions.m2828a(acquire);
            DecodeJob<R> hVar2 = acquire;
            int i3 = this.f1179c;
            int i4 = i3;
            this.f1179c = i3 + 1;
            hVar2.mo10165a(eVar, obj, nVar, gVar, i, i2, cls, cls2, hVar, jVar, map, z, z2, z3, iVar, bVar, i4);
            return hVar2;
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.k$b */
    /* compiled from: Engine */
    static class C0994b {

        /* renamed from: a */
        final GlideExecutor f1181a;

        /* renamed from: b */
        final GlideExecutor f1182b;

        /* renamed from: c */
        final GlideExecutor f1183c;

        /* renamed from: d */
        final GlideExecutor f1184d;

        /* renamed from: e */
        final EngineJobListener f1185e;

        /* renamed from: f */
        final Pools.Pool<EngineJob<?>> f1186f = FactoryPools.m2854a(150, new C0995a());

        /* renamed from: com.bumptech.glide.load.n.k$b$a */
        /* compiled from: Engine */
        class C0995a implements FactoryPools.C1127d<EngineJob<?>> {
            C0995a() {
            }

            /* renamed from: a */
            public EngineJob<?> m1784a() {
                C0994b bVar = C0994b.this;
                return new EngineJob(bVar.f1181a, bVar.f1182b, bVar.f1183c, bVar.f1184d, bVar.f1185e, bVar.f1186f);
            }
        }

        C0994b(GlideExecutor aVar, GlideExecutor aVar2, GlideExecutor aVar3, GlideExecutor aVar4, EngineJobListener mVar) {
            this.f1181a = aVar;
            this.f1182b = aVar2;
            this.f1183c = aVar3;
            this.f1184d = aVar4;
            this.f1185e = mVar;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public <R> EngineJob<R> mo10197a(Key gVar, boolean z, boolean z2, boolean z3, boolean z4) {
            EngineJob<R> acquire = this.f1186f.acquire();
            Preconditions.m2828a(acquire);
            EngineJob<R> lVar = acquire;
            lVar.mo10199a(gVar, z, z2, z3, z4);
            return lVar;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.k$c */
    /* compiled from: Engine */
    private static class C0996c implements DecodeJob.C0982e {

        /* renamed from: a */
        private final DiskCache.C0958a f1188a;

        /* renamed from: b */
        private volatile DiskCache f1189b;

        C0996c(DiskCache.C0958a aVar) {
            this.f1188a = aVar;
        }

        /* renamed from: a */
        public DiskCache mo10180a() {
            if (this.f1189b == null) {
                synchronized (this) {
                    if (this.f1189b == null) {
                        this.f1189b = this.f1188a.build();
                    }
                    if (this.f1189b == null) {
                        this.f1189b = new DiskCacheAdapter();
                    }
                }
            }
            return this.f1189b;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.k$d */
    /* compiled from: Engine */
    public class C0997d {

        /* renamed from: a */
        private final EngineJob<?> f1190a;

        /* renamed from: b */
        private final ResourceCallback f1191b;

        C0997d(ResourceCallback iVar, EngineJob<?> lVar) {
            this.f1191b = iVar;
            this.f1190a = lVar;
        }

        /* renamed from: a */
        public void mo10198a() {
            synchronized (Engine.this) {
                this.f1190a.mo10208c(this.f1191b);
            }
        }
    }

    public Engine(MemoryCache hVar, DiskCache.C0958a aVar, GlideExecutor aVar2, GlideExecutor aVar3, GlideExecutor aVar4, GlideExecutor aVar5, boolean z) {
        this(hVar, aVar, aVar2, aVar3, aVar4, aVar5, null, null, null, null, null, null, z);
    }

    /* renamed from: b */
    private EngineResource<?> m1772b(Key gVar, boolean z) {
        if (!z) {
            return null;
        }
        EngineResource<?> a = m1769a(gVar);
        if (a != null) {
            a.mo10229c();
            this.f1176h.mo10026a(gVar, a);
        }
        return a;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0041, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0057, code lost:
        return null;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <R> com.bumptech.glide.load.p023n.Engine.C0997d mo10191a(com.bumptech.glide.GlideContext r31, java.lang.Object r32, com.bumptech.glide.load.Key r33, int r34, int r35, java.lang.Class<?> r36, java.lang.Class<R> r37, com.bumptech.glide.Priority r38, com.bumptech.glide.load.p023n.DiskCacheStrategy r39, java.util.Map<java.lang.Class<?>, com.bumptech.glide.load.Transformation<?>> r40, boolean r41, boolean r42, com.bumptech.glide.load.Options r43, boolean r44, boolean r45, boolean r46, boolean r47, com.bumptech.glide.p040q.ResourceCallback r48, java.util.concurrent.Executor r49) {
        /*
            r30 = this;
            r1 = r30
            r0 = r44
            r8 = r48
            r9 = r49
            monitor-enter(r30)
            boolean r2 = com.bumptech.glide.load.p023n.Engine.f1168i     // Catch:{ all -> 0x00c5 }
            if (r2 == 0) goto L_0x0012
            long r2 = com.bumptech.glide.util.LogTime.m2815a()     // Catch:{ all -> 0x00c5 }
            goto L_0x0014
        L_0x0012:
            r2 = 0
        L_0x0014:
            r10 = r2
            com.bumptech.glide.load.n.o r12 = r1.f1170b     // Catch:{ all -> 0x00c5 }
            r13 = r32
            r14 = r33
            r15 = r34
            r16 = r35
            r17 = r40
            r18 = r36
            r19 = r37
            r20 = r43
            com.bumptech.glide.load.n.n r12 = r12.mo10225a(r13, r14, r15, r16, r17, r18, r19, r20)     // Catch:{ all -> 0x00c5 }
            com.bumptech.glide.load.n.p r2 = r1.m1770a(r12, r0)     // Catch:{ all -> 0x00c5 }
            r3 = 0
            if (r2 == 0) goto L_0x0042
            com.bumptech.glide.load.a r0 = com.bumptech.glide.load.DataSource.MEMORY_CACHE     // Catch:{ all -> 0x00c5 }
            r8.mo10655a(r2, r0)     // Catch:{ all -> 0x00c5 }
            boolean r0 = com.bumptech.glide.load.p023n.Engine.f1168i     // Catch:{ all -> 0x00c5 }
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = "Loaded resource from active resources"
            m1771a(r0, r10, r12)     // Catch:{ all -> 0x00c5 }
        L_0x0040:
            monitor-exit(r30)
            return r3
        L_0x0042:
            com.bumptech.glide.load.n.p r2 = r1.m1772b(r12, r0)     // Catch:{ all -> 0x00c5 }
            if (r2 == 0) goto L_0x0058
            com.bumptech.glide.load.a r0 = com.bumptech.glide.load.DataSource.MEMORY_CACHE     // Catch:{ all -> 0x00c5 }
            r8.mo10655a(r2, r0)     // Catch:{ all -> 0x00c5 }
            boolean r0 = com.bumptech.glide.load.p023n.Engine.f1168i     // Catch:{ all -> 0x00c5 }
            if (r0 == 0) goto L_0x0056
            java.lang.String r0 = "Loaded resource from cache"
            m1771a(r0, r10, r12)     // Catch:{ all -> 0x00c5 }
        L_0x0056:
            monitor-exit(r30)
            return r3
        L_0x0058:
            com.bumptech.glide.load.n.s r2 = r1.f1169a     // Catch:{ all -> 0x00c5 }
            r15 = r47
            com.bumptech.glide.load.n.l r2 = r2.mo10251a(r12, r15)     // Catch:{ all -> 0x00c5 }
            if (r2 == 0) goto L_0x0075
            r2.mo10203a(r8, r9)     // Catch:{ all -> 0x00c5 }
            boolean r0 = com.bumptech.glide.load.p023n.Engine.f1168i     // Catch:{ all -> 0x00c5 }
            if (r0 == 0) goto L_0x006e
            java.lang.String r0 = "Added to existing load"
            m1771a(r0, r10, r12)     // Catch:{ all -> 0x00c5 }
        L_0x006e:
            com.bumptech.glide.load.n.k$d r0 = new com.bumptech.glide.load.n.k$d     // Catch:{ all -> 0x00c5 }
            r0.<init>(r8, r2)     // Catch:{ all -> 0x00c5 }
            monitor-exit(r30)
            return r0
        L_0x0075:
            com.bumptech.glide.load.n.k$b r2 = r1.f1172d     // Catch:{ all -> 0x00c5 }
            r3 = r12
            r4 = r44
            r5 = r45
            r6 = r46
            r7 = r47
            com.bumptech.glide.load.n.l r0 = r2.mo10197a(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00c5 }
            com.bumptech.glide.load.n.k$a r13 = r1.f1175g     // Catch:{ all -> 0x00c5 }
            r14 = r31
            r15 = r32
            r16 = r12
            r17 = r33
            r18 = r34
            r19 = r35
            r20 = r36
            r21 = r37
            r22 = r38
            r23 = r39
            r24 = r40
            r25 = r41
            r26 = r42
            r27 = r47
            r28 = r43
            r29 = r0
            com.bumptech.glide.load.n.h r2 = r13.mo10196a(r14, r15, r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27, r28, r29)     // Catch:{ all -> 0x00c5 }
            com.bumptech.glide.load.n.s r3 = r1.f1169a     // Catch:{ all -> 0x00c5 }
            r3.mo10252a(r12, r0)     // Catch:{ all -> 0x00c5 }
            r0.mo10203a(r8, r9)     // Catch:{ all -> 0x00c5 }
            r0.mo10205b(r2)     // Catch:{ all -> 0x00c5 }
            boolean r2 = com.bumptech.glide.load.p023n.Engine.f1168i     // Catch:{ all -> 0x00c5 }
            if (r2 == 0) goto L_0x00be
            java.lang.String r2 = "Started new load"
            m1771a(r2, r10, r12)     // Catch:{ all -> 0x00c5 }
        L_0x00be:
            com.bumptech.glide.load.n.k$d r2 = new com.bumptech.glide.load.n.k$d     // Catch:{ all -> 0x00c5 }
            r2.<init>(r8, r0)     // Catch:{ all -> 0x00c5 }
            monitor-exit(r30)
            return r2
        L_0x00c5:
            r0 = move-exception
            monitor-exit(r30)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.Engine.mo10191a(com.bumptech.glide.e, java.lang.Object, com.bumptech.glide.load.g, int, int, java.lang.Class, java.lang.Class, com.bumptech.glide.h, com.bumptech.glide.load.n.j, java.util.Map, boolean, boolean, com.bumptech.glide.load.i, boolean, boolean, boolean, boolean, com.bumptech.glide.q.i, java.util.concurrent.Executor):com.bumptech.glide.load.n.k$d");
    }

    @VisibleForTesting
    Engine(MemoryCache hVar, DiskCache.C0958a aVar, GlideExecutor aVar2, GlideExecutor aVar3, GlideExecutor aVar4, GlideExecutor aVar5, Jobs sVar, EngineKeyFactory oVar, ActiveResources aVar6, C0994b bVar, C0992a aVar7, ResourceRecycler yVar, boolean z) {
        this.f1171c = hVar;
        this.f1174f = new C0996c(aVar);
        ActiveResources aVar8 = aVar6 == null ? new ActiveResources(z) : aVar6;
        this.f1176h = aVar8;
        aVar8.mo10028a(this);
        this.f1170b = oVar == null ? new EngineKeyFactory() : oVar;
        this.f1169a = sVar == null ? new Jobs() : sVar;
        this.f1172d = bVar == null ? new C0994b(aVar2, aVar3, aVar4, aVar5, this) : bVar;
        this.f1175g = aVar7 == null ? new C0992a(this.f1174f) : aVar7;
        this.f1173e = yVar == null ? new ResourceRecycler() : yVar;
        hVar.mo10101a(this);
    }

    /* renamed from: b */
    public void mo10195b(Resource<?> vVar) {
        if (vVar instanceof EngineResource) {
            ((EngineResource) vVar).mo10232f();
            return;
        }
        throw new IllegalArgumentException("Cannot release anything but an EngineResource");
    }

    /* renamed from: a */
    private static void m1771a(String str, long j, Key gVar) {
        Log.v("Engine", str + " in " + LogTime.m2814a(j) + "ms, key: " + gVar);
    }

    @Nullable
    /* renamed from: a */
    private EngineResource<?> m1770a(Key gVar, boolean z) {
        if (!z) {
            return null;
        }
        EngineResource<?> b = this.f1176h.mo10029b(gVar);
        if (b != null) {
            b.mo10229c();
        }
        return b;
    }

    /* renamed from: a */
    private EngineResource<?> m1769a(Key gVar) {
        Resource<?> a = this.f1171c.mo10098a(gVar);
        if (a == null) {
            return null;
        }
        if (a instanceof EngineResource) {
            return (EngineResource) a;
        }
        return new EngineResource<>(a, true, true);
    }

    /* renamed from: a */
    public synchronized void mo10194a(EngineJob<?> lVar, Key gVar, EngineResource<?> pVar) {
        if (pVar != null) {
            pVar.mo10227a(gVar, this);
            if (pVar.mo10231e()) {
                this.f1176h.mo10026a(gVar, pVar);
            }
        }
        this.f1169a.mo10253b(gVar, lVar);
    }

    /* renamed from: a */
    public synchronized void mo10193a(EngineJob<?> lVar, Key gVar) {
        this.f1169a.mo10253b(gVar, lVar);
    }

    /* renamed from: a */
    public void mo10106a(@NonNull Resource<?> vVar) {
        this.f1173e.mo10258a(vVar);
    }

    /* renamed from: a */
    public synchronized void mo10192a(Key gVar, EngineResource<?> pVar) {
        this.f1176h.mo10025a(gVar);
        if (pVar.mo10231e()) {
            this.f1171c.mo10099a(gVar, pVar);
        } else {
            this.f1173e.mo10258a(pVar);
        }
    }
}
