package com.bumptech.glide.p040q;

import com.bumptech.glide.p040q.p041l.Target;
import java.util.concurrent.Future;

/* renamed from: com.bumptech.glide.q.c */
public interface FutureTarget<R> extends Future<R>, Target<R> {
}
