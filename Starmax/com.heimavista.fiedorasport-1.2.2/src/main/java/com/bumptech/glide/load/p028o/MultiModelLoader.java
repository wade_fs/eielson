package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.util.Pools;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p023n.GlideException;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/* renamed from: com.bumptech.glide.load.o.q */
class MultiModelLoader<Model, Data> implements ModelLoader<Model, Data> {

    /* renamed from: a */
    private final List<ModelLoader<Model, Data>> f1336a;

    /* renamed from: b */
    private final Pools.Pool<List<Throwable>> f1337b;

    MultiModelLoader(@NonNull List<ModelLoader<Model, Data>> list, @NonNull Pools.Pool<List<Throwable>> pool) {
        this.f1336a = list;
        this.f1337b = pool;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull Model model, int i, int i2, @NonNull Options iVar) {
        ModelLoader.C1036a a;
        int size = this.f1336a.size();
        ArrayList arrayList = new ArrayList(size);
        Key gVar = null;
        for (int i3 = 0; i3 < size; i3++) {
            ModelLoader nVar = this.f1336a.get(i3);
            if (nVar.mo10263a(model) && (a = nVar.mo10261a(model, i, i2, iVar)) != null) {
                gVar = a.f1329a;
                arrayList.add(a.f1331c);
            }
        }
        if (arrayList.isEmpty() || gVar == null) {
            return null;
        }
        return new ModelLoader.C1036a<>(gVar, new C1039a(arrayList, this.f1337b));
    }

    public String toString() {
        return "MultiModelLoader{modelLoaders=" + Arrays.toString(this.f1336a.toArray()) + '}';
    }

    /* renamed from: com.bumptech.glide.load.o.q$a */
    /* compiled from: MultiModelLoader */
    static class C1039a<Data> implements DataFetcher<Data>, DataFetcher.C0934a<Data> {

        /* renamed from: P */
        private final List<DataFetcher<Data>> f1338P;

        /* renamed from: Q */
        private final Pools.Pool<List<Throwable>> f1339Q;

        /* renamed from: R */
        private int f1340R = 0;

        /* renamed from: S */
        private Priority f1341S;

        /* renamed from: T */
        private DataFetcher.C0934a<? super Data> f1342T;
        @Nullable

        /* renamed from: U */
        private List<Throwable> f1343U;

        /* renamed from: V */
        private boolean f1344V;

        C1039a(@NonNull List<DataFetcher<Data>> list, @NonNull Pools.Pool<List<Throwable>> pool) {
            this.f1339Q = pool;
            Preconditions.m2831a((Collection) list);
            this.f1338P = list;
        }

        /* renamed from: d */
        private void m1987d() {
            if (!this.f1344V) {
                if (this.f1340R < this.f1338P.size() - 1) {
                    this.f1340R++;
                    mo9988a(this.f1341S, this.f1342T);
                    return;
                }
                Preconditions.m2828a((Object) this.f1343U);
                this.f1342T.mo9998a((Exception) new GlideException("Fetch failed", new ArrayList(this.f1343U)));
            }
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super Data> aVar) {
            this.f1341S = hVar;
            this.f1342T = aVar;
            this.f1343U = this.f1339Q.acquire();
            this.f1338P.get(this.f1340R).mo9988a(hVar, this);
            if (this.f1344V) {
                cancel();
            }
        }

        /* renamed from: b */
        public void mo9990b() {
            List<Throwable> list = this.f1343U;
            if (list != null) {
                this.f1339Q.release(list);
            }
            this.f1343U = null;
            for (DataFetcher<Data> dVar : this.f1338P) {
                dVar.mo9990b();
            }
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return this.f1338P.get(0).mo9991c();
        }

        public void cancel() {
            this.f1344V = true;
            for (DataFetcher<Data> dVar : this.f1338P) {
                dVar.cancel();
            }
        }

        @NonNull
        /* renamed from: a */
        public Class<Data> mo9984a() {
            return this.f1338P.get(0).mo9984a();
        }

        /* renamed from: a */
        public void mo9999a(@Nullable Data data) {
            if (data != null) {
                this.f1342T.mo9999a((Object) data);
            } else {
                m1987d();
            }
        }

        /* renamed from: a */
        public void mo9998a(@NonNull Exception exc) {
            List<Throwable> list = this.f1343U;
            Preconditions.m2828a((Object) list);
            list.add(exc);
            m1987d();
        }
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull Model model) {
        for (ModelLoader<Model, Data> nVar : this.f1336a) {
            if (nVar.mo10263a(model)) {
                return true;
            }
        }
        return false;
    }
}
