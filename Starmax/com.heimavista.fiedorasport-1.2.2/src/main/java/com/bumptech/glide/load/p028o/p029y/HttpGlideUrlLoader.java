package com.bumptech.glide.load.p028o.p029y;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.Option;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.HttpUrlFetcher;
import com.bumptech.glide.load.p028o.GlideUrl;
import com.bumptech.glide.load.p028o.ModelCache;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.load.p028o.ModelLoaderFactory;
import com.bumptech.glide.load.p028o.MultiModelLoaderFactory;
import com.google.android.exoplayer2.DefaultLoadControl;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.o.y.a */
public class HttpGlideUrlLoader implements ModelLoader<GlideUrl, InputStream> {

    /* renamed from: b */
    public static final Option<Integer> f1372b = Option.m1370a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", Integer.valueOf((int) DefaultLoadControl.DEFAULT_BUFFER_FOR_PLAYBACK_MS));
    @Nullable

    /* renamed from: a */
    private final ModelCache<GlideUrl, GlideUrl> f1373a;

    /* renamed from: com.bumptech.glide.load.o.y.a$a */
    /* compiled from: HttpGlideUrlLoader */
    public static class C1057a implements ModelLoaderFactory<GlideUrl, InputStream> {

        /* renamed from: a */
        private final ModelCache<GlideUrl, GlideUrl> f1374a = new ModelCache<>(500);

        @NonNull
        /* renamed from: a */
        public ModelLoader<GlideUrl, InputStream> mo10265a(MultiModelLoaderFactory rVar) {
            return new HttpGlideUrlLoader(this.f1374a);
        }
    }

    public HttpGlideUrlLoader(@Nullable ModelCache<GlideUrl, GlideUrl> mVar) {
        this.f1373a = mVar;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull GlideUrl gVar) {
        return true;
    }

    /* renamed from: a */
    public ModelLoader.C1036a<InputStream> mo10261a(@NonNull GlideUrl gVar, int i, int i2, @NonNull Options iVar) {
        ModelCache<GlideUrl, GlideUrl> mVar = this.f1373a;
        if (mVar != null) {
            GlideUrl a = mVar.mo10300a(gVar, 0, 0);
            if (a == null) {
                this.f1373a.mo10301a(gVar, 0, 0, gVar);
            } else {
                gVar = a;
            }
        }
        return new ModelLoader.C1036a<>(gVar, new HttpUrlFetcher(gVar, ((Integer) iVar.mo9976a(f1372b)).intValue()));
    }
}
