package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.p.c.h */
public class CenterInside extends BitmapTransformation {

    /* renamed from: b */
    private static final byte[] f1395b = "com.bumptech.glide.load.resource.bitmap.CenterInside".getBytes(Key.f928a);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Bitmap mo10342a(@NonNull BitmapPool eVar, @NonNull Bitmap bitmap, int i, int i2) {
        return TransformationUtils.m2219b(eVar, bitmap, i, i2);
    }

    public boolean equals(Object obj) {
        return obj instanceof CenterInside;
    }

    public int hashCode() {
        return "com.bumptech.glide.load.resource.bitmap.CenterInside".hashCode();
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        messageDigest.update(f1395b);
    }
}
