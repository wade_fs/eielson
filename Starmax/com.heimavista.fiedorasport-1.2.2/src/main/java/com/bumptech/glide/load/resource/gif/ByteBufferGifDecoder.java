package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.DecodeFormat;
import com.bumptech.glide.load.ImageHeaderParser;
import com.bumptech.glide.load.ImageHeaderParserUtils;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p030p.UnitTransformation;
import com.bumptech.glide.p037n.GifDecoder;
import com.bumptech.glide.p037n.GifHeader;
import com.bumptech.glide.p037n.GifHeaderParser;
import com.bumptech.glide.p037n.StandardGifDecoder;
import com.bumptech.glide.util.C1122j;
import com.bumptech.glide.util.LogTime;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Queue;

/* renamed from: com.bumptech.glide.load.resource.gif.a */
public class ByteBufferGifDecoder implements ResourceDecoder<ByteBuffer, GifDrawable> {

    /* renamed from: f */
    private static final C1089a f1487f = new C1089a();

    /* renamed from: g */
    private static final C1090b f1488g = new C1090b();

    /* renamed from: a */
    private final Context f1489a;

    /* renamed from: b */
    private final List<ImageHeaderParser> f1490b;

    /* renamed from: c */
    private final C1090b f1491c;

    /* renamed from: d */
    private final C1089a f1492d;

    /* renamed from: e */
    private final GifBitmapProvider f1493e;

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.resource.gif.a$a */
    /* compiled from: ByteBufferGifDecoder */
    static class C1089a {
        C1089a() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public GifDecoder mo10429a(GifDecoder.C1107a aVar, GifHeader cVar, ByteBuffer byteBuffer, int i) {
            return new StandardGifDecoder(aVar, cVar, byteBuffer, i);
        }
    }

    public ByteBufferGifDecoder(Context context, List<ImageHeaderParser> list, BitmapPool eVar, ArrayPool bVar) {
        this(context, list, eVar, bVar, f1488g, f1487f);
    }

    @VisibleForTesting
    ByteBufferGifDecoder(Context context, List<ImageHeaderParser> list, BitmapPool eVar, ArrayPool bVar, C1090b bVar2, C1089a aVar) {
        this.f1489a = context.getApplicationContext();
        this.f1490b = list;
        this.f1492d = aVar;
        this.f1493e = new GifBitmapProvider(eVar, bVar);
        this.f1491c = bVar2;
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.resource.gif.a$b */
    /* compiled from: ByteBufferGifDecoder */
    static class C1090b {

        /* renamed from: a */
        private final Queue<GifHeaderParser> f1494a = C1122j.m2844a(0);

        C1090b() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public synchronized GifHeaderParser mo10430a(ByteBuffer byteBuffer) {
            GifHeaderParser poll;
            poll = this.f1494a.poll();
            if (poll == null) {
                poll = new GifHeaderParser();
            }
            poll.mo10541a(byteBuffer);
            return poll;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public synchronized void mo10431a(GifHeaderParser dVar) {
            dVar.mo10542a();
            this.f1494a.offer(dVar);
        }
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull ByteBuffer byteBuffer, @NonNull Options iVar) {
        return !((Boolean) iVar.mo9976a(GifOptions.f1520b)).booleanValue() && ImageHeaderParserUtils.m1366a(this.f1490b, byteBuffer) == ImageHeaderParser.ImageType.GIF;
    }

    /* renamed from: a */
    public GifDrawableResource mo9979a(@NonNull ByteBuffer byteBuffer, int i, int i2, @NonNull Options iVar) {
        GifHeaderParser a = this.f1491c.mo10430a(byteBuffer);
        try {
            return m2307a(byteBuffer, i, i2, a, iVar);
        } finally {
            this.f1491c.mo10431a(a);
        }
    }

    @Nullable
    /* renamed from: a */
    private GifDrawableResource m2307a(ByteBuffer byteBuffer, int i, int i2, GifHeaderParser dVar, Options iVar) {
        long a = LogTime.m2815a();
        try {
            GifHeader b = dVar.mo10543b();
            if (b.mo10538b() > 0) {
                if (b.mo10539c() == 0) {
                    Bitmap.Config config = iVar.mo9976a(GifOptions.f1519a) == DecodeFormat.f921Q ? Bitmap.Config.RGB_565 : Bitmap.Config.ARGB_8888;
                    GifDecoder a2 = this.f1492d.mo10429a(this.f1493e, b, byteBuffer, m2306a(b, i, i2));
                    a2.mo10528a(config);
                    a2.mo10529b();
                    Bitmap a3 = a2.mo10527a();
                    if (a3 == null) {
                        if (Log.isLoggable("BufferGifDecoder", 2)) {
                            Log.v("BufferGifDecoder", "Decoded GIF from stream in " + LogTime.m2814a(a));
                        }
                        return null;
                    }
                    GifDrawableResource dVar2 = new GifDrawableResource(new GifDrawable(this.f1489a, a2, UnitTransformation.m2079a(), i, i2, a3));
                    if (Log.isLoggable("BufferGifDecoder", 2)) {
                        Log.v("BufferGifDecoder", "Decoded GIF from stream in " + LogTime.m2814a(a));
                    }
                    return dVar2;
                }
            }
            return null;
        } finally {
            if (Log.isLoggable("BufferGifDecoder", 2)) {
                Log.v("BufferGifDecoder", "Decoded GIF from stream in " + LogTime.m2814a(a));
            }
        }
    }

    /* renamed from: a */
    private static int m2306a(GifHeader cVar, int i, int i2) {
        int i3;
        int min = Math.min(cVar.mo10537a() / i2, cVar.mo10540d() / i);
        if (min == 0) {
            i3 = 0;
        } else {
            i3 = Integer.highestOneBit(min);
        }
        int max = Math.max(1, i3);
        if (Log.isLoggable("BufferGifDecoder", 2) && max > 1) {
            Log.v("BufferGifDecoder", "Downsampling GIF, sampleSize: " + max + ", target dimens: [" + i + "x" + i2 + "], actual dimens: [" + cVar.mo10540d() + "x" + cVar.mo10537a() + "]");
        }
        return max;
    }
}
