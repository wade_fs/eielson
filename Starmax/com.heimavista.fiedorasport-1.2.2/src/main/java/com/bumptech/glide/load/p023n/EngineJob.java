package com.bumptech.glide.load.p023n;

import androidx.annotation.NonNull;
import androidx.annotation.VisibleForTesting;
import androidx.core.util.Pools;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import com.bumptech.glide.load.p023n.DecodeJob;
import com.bumptech.glide.load.p023n.p026c0.GlideExecutor;
import com.bumptech.glide.p040q.ResourceCallback;
import com.bumptech.glide.util.Executors;
import com.bumptech.glide.util.Preconditions;
import com.bumptech.glide.util.p044k.FactoryPools;
import com.bumptech.glide.util.p044k.StateVerifier;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.bumptech.glide.load.n.l */
class EngineJob<R> implements DecodeJob.C0979b<R>, FactoryPools.C1129f {

    /* renamed from: m0 */
    private static final C1000c f1193m0 = new C1000c();

    /* renamed from: P */
    final C1002e f1194P;

    /* renamed from: Q */
    private final StateVerifier f1195Q;

    /* renamed from: R */
    private final Pools.Pool<EngineJob<?>> f1196R;

    /* renamed from: S */
    private final C1000c f1197S;

    /* renamed from: T */
    private final EngineJobListener f1198T;

    /* renamed from: U */
    private final GlideExecutor f1199U;

    /* renamed from: V */
    private final GlideExecutor f1200V;

    /* renamed from: W */
    private final GlideExecutor f1201W;

    /* renamed from: X */
    private final GlideExecutor f1202X;

    /* renamed from: Y */
    private final AtomicInteger f1203Y;

    /* renamed from: Z */
    private Key f1204Z;

    /* renamed from: a0 */
    private boolean f1205a0;

    /* renamed from: b0 */
    private boolean f1206b0;

    /* renamed from: c0 */
    private boolean f1207c0;

    /* renamed from: d0 */
    private boolean f1208d0;

    /* renamed from: e0 */
    private Resource<?> f1209e0;

    /* renamed from: f0 */
    DataSource f1210f0;

    /* renamed from: g0 */
    private boolean f1211g0;

    /* renamed from: h0 */
    GlideException f1212h0;

    /* renamed from: i0 */
    private boolean f1213i0;

    /* renamed from: j0 */
    EngineResource<?> f1214j0;

    /* renamed from: k0 */
    private DecodeJob<R> f1215k0;

    /* renamed from: l0 */
    private volatile boolean f1216l0;

    /* renamed from: com.bumptech.glide.load.n.l$a */
    /* compiled from: EngineJob */
    private class C0998a implements Runnable {

        /* renamed from: P */
        private final ResourceCallback f1217P;

        C0998a(ResourceCallback iVar) {
            this.f1217P = iVar;
        }

        public void run() {
            synchronized (EngineJob.this) {
                if (EngineJob.this.f1194P.mo10217a(this.f1217P)) {
                    EngineJob.this.mo10202a(this.f1217P);
                }
                EngineJob.this.mo10204b();
            }
        }
    }

    /* renamed from: com.bumptech.glide.load.n.l$b */
    /* compiled from: EngineJob */
    private class C0999b implements Runnable {

        /* renamed from: P */
        private final ResourceCallback f1219P;

        C0999b(ResourceCallback iVar) {
            this.f1219P = iVar;
        }

        public void run() {
            synchronized (EngineJob.this) {
                if (EngineJob.this.f1194P.mo10217a(this.f1219P)) {
                    EngineJob.this.f1214j0.mo10229c();
                    EngineJob.this.mo10206b(this.f1219P);
                    EngineJob.this.mo10208c(this.f1219P);
                }
                EngineJob.this.mo10204b();
            }
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.l$c */
    /* compiled from: EngineJob */
    static class C1000c {
        C1000c() {
        }

        /* renamed from: a */
        public <R> EngineResource<R> mo10213a(Resource<R> vVar, boolean z) {
            return new EngineResource<>(vVar, z, true);
        }
    }

    /* renamed from: com.bumptech.glide.load.n.l$d */
    /* compiled from: EngineJob */
    static final class C1001d {

        /* renamed from: a */
        final ResourceCallback f1221a;

        /* renamed from: b */
        final Executor f1222b;

        C1001d(ResourceCallback iVar, Executor executor) {
            this.f1221a = iVar;
            this.f1222b = executor;
        }

        public boolean equals(Object obj) {
            if (obj instanceof C1001d) {
                return this.f1221a.equals(((C1001d) obj).f1221a);
            }
            return false;
        }

        public int hashCode() {
            return this.f1221a.hashCode();
        }
    }

    /* renamed from: com.bumptech.glide.load.n.l$e */
    /* compiled from: EngineJob */
    static final class C1002e implements Iterable<C1001d> {

        /* renamed from: P */
        private final List<C1001d> f1223P;

        C1002e() {
            this(new ArrayList(2));
        }

        /* renamed from: c */
        private static C1001d m1807c(ResourceCallback iVar) {
            return new C1001d(iVar, Executors.m2812a());
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo10216a(ResourceCallback iVar, Executor executor) {
            this.f1223P.add(new C1001d(iVar, executor));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo10219b(ResourceCallback iVar) {
            this.f1223P.remove(m1807c(iVar));
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            this.f1223P.clear();
        }

        /* access modifiers changed from: package-private */
        public boolean isEmpty() {
            return this.f1223P.isEmpty();
        }

        @NonNull
        public Iterator<C1001d> iterator() {
            return this.f1223P.iterator();
        }

        /* access modifiers changed from: package-private */
        public int size() {
            return this.f1223P.size();
        }

        C1002e(List<C1001d> list) {
            this.f1223P = list;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo10217a(ResourceCallback iVar) {
            return this.f1223P.contains(m1807c(iVar));
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public C1002e mo10218b() {
            return new C1002e(new ArrayList(this.f1223P));
        }
    }

    EngineJob(GlideExecutor aVar, GlideExecutor aVar2, GlideExecutor aVar3, GlideExecutor aVar4, EngineJobListener mVar, Pools.Pool<EngineJob<?>> pool) {
        this(aVar, aVar2, aVar3, aVar4, mVar, pool, f1193m0);
    }

    /* renamed from: g */
    private GlideExecutor m1787g() {
        if (this.f1206b0) {
            return this.f1201W;
        }
        return this.f1207c0 ? this.f1202X : this.f1200V;
    }

    /* renamed from: h */
    private boolean m1788h() {
        return this.f1213i0 || this.f1211g0 || this.f1216l0;
    }

    /* renamed from: i */
    private synchronized void m1789i() {
        if (this.f1204Z != null) {
            this.f1194P.clear();
            this.f1204Z = null;
            this.f1214j0 = null;
            this.f1209e0 = null;
            this.f1213i0 = false;
            this.f1216l0 = false;
            this.f1211g0 = false;
            this.f1215k0.mo10168a(false);
            this.f1215k0 = null;
            this.f1212h0 = null;
            this.f1210f0 = null;
            this.f1196R.release(this);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /* access modifiers changed from: package-private */
    @VisibleForTesting
    /* renamed from: a */
    public synchronized EngineJob<R> mo10199a(Key gVar, boolean z, boolean z2, boolean z3, boolean z4) {
        this.f1204Z = gVar;
        this.f1205a0 = z;
        this.f1206b0 = z2;
        this.f1207c0 = z3;
        this.f1208d0 = z4;
        return this;
    }

    /* renamed from: b */
    public synchronized void mo10205b(DecodeJob hVar) {
        GlideExecutor aVar;
        this.f1215k0 = hVar;
        if (hVar.mo10169c()) {
            aVar = this.f1199U;
        } else {
            aVar = m1787g();
        }
        aVar.execute(hVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public synchronized void mo10208c(ResourceCallback iVar) {
        boolean z;
        this.f1195Q.mo10716a();
        this.f1194P.mo10219b(iVar);
        if (this.f1194P.isEmpty()) {
            mo10200a();
            if (!this.f1211g0) {
                if (!this.f1213i0) {
                    z = false;
                    if (z && this.f1203Y.get() == 0) {
                        m1789i();
                    }
                }
            }
            z = true;
            m1789i();
        }
    }

    @NonNull
    /* renamed from: d */
    public StateVerifier mo10115d() {
        return this.f1195Q;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0042, code lost:
        r4.f1198T.mo10194a(r4, r0, r2);
        r0 = r1.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x004f, code lost:
        if (r0.hasNext() == false) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0051, code lost:
        r1 = r0.next();
        r1.f1222b.execute(new com.bumptech.glide.load.p023n.EngineJob.C0999b(r4, r1.f1221a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0064, code lost:
        mo10204b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0067, code lost:
        return;
     */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10209e() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.bumptech.glide.util.k.c r0 = r4.f1195Q     // Catch:{ all -> 0x0078 }
            r0.mo10716a()     // Catch:{ all -> 0x0078 }
            boolean r0 = r4.f1216l0     // Catch:{ all -> 0x0078 }
            if (r0 == 0) goto L_0x0014
            com.bumptech.glide.load.n.v<?> r0 = r4.f1209e0     // Catch:{ all -> 0x0078 }
            r0.recycle()     // Catch:{ all -> 0x0078 }
            r4.m1789i()     // Catch:{ all -> 0x0078 }
            monitor-exit(r4)     // Catch:{ all -> 0x0078 }
            return
        L_0x0014:
            com.bumptech.glide.load.n.l$e r0 = r4.f1194P     // Catch:{ all -> 0x0078 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0078 }
            if (r0 != 0) goto L_0x0070
            boolean r0 = r4.f1211g0     // Catch:{ all -> 0x0078 }
            if (r0 != 0) goto L_0x0068
            com.bumptech.glide.load.n.l$c r0 = r4.f1197S     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.n.v<?> r1 = r4.f1209e0     // Catch:{ all -> 0x0078 }
            boolean r2 = r4.f1205a0     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.n.p r0 = r0.mo10213a(r1, r2)     // Catch:{ all -> 0x0078 }
            r4.f1214j0 = r0     // Catch:{ all -> 0x0078 }
            r0 = 1
            r4.f1211g0 = r0     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.n.l$e r1 = r4.f1194P     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.n.l$e r1 = r1.mo10218b()     // Catch:{ all -> 0x0078 }
            int r2 = r1.size()     // Catch:{ all -> 0x0078 }
            int r2 = r2 + r0
            r4.mo10201a(r2)     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.g r0 = r4.f1204Z     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.n.p<?> r2 = r4.f1214j0     // Catch:{ all -> 0x0078 }
            monitor-exit(r4)     // Catch:{ all -> 0x0078 }
            com.bumptech.glide.load.n.m r3 = r4.f1198T
            r3.mo10194a(r4, r0, r2)
            java.util.Iterator r0 = r1.iterator()
        L_0x004b:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0064
            java.lang.Object r1 = r0.next()
            com.bumptech.glide.load.n.l$d r1 = (com.bumptech.glide.load.p023n.EngineJob.C1001d) r1
            java.util.concurrent.Executor r2 = r1.f1222b
            com.bumptech.glide.load.n.l$b r3 = new com.bumptech.glide.load.n.l$b
            com.bumptech.glide.q.i r1 = r1.f1221a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x004b
        L_0x0064:
            r4.mo10204b()
            return
        L_0x0068:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0078 }
            java.lang.String r1 = "Already have resource"
            r0.<init>(r1)     // Catch:{ all -> 0x0078 }
            throw r0     // Catch:{ all -> 0x0078 }
        L_0x0070:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0078 }
            java.lang.String r1 = "Received a resource without any callbacks to notify"
            r0.<init>(r1)     // Catch:{ all -> 0x0078 }
            throw r0     // Catch:{ all -> 0x0078 }
        L_0x0078:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0078 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.EngineJob.mo10209e():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: f */
    public boolean mo10210f() {
        return this.f1208d0;
    }

    @VisibleForTesting
    EngineJob(GlideExecutor aVar, GlideExecutor aVar2, GlideExecutor aVar3, GlideExecutor aVar4, EngineJobListener mVar, Pools.Pool<EngineJob<?>> pool, C1000c cVar) {
        this.f1194P = new C1002e();
        this.f1195Q = StateVerifier.m2871b();
        this.f1203Y = new AtomicInteger();
        this.f1199U = aVar;
        this.f1200V = aVar2;
        this.f1201W = aVar3;
        this.f1202X = aVar4;
        this.f1198T = mVar;
        this.f1196R = pool;
        this.f1197S = cVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10203a(ResourceCallback iVar, Executor executor) {
        this.f1195Q.mo10716a();
        this.f1194P.mo10216a(iVar, executor);
        boolean z = true;
        if (this.f1211g0) {
            mo10201a(1);
            executor.execute(new C0999b(iVar));
        } else if (this.f1213i0) {
            mo10201a(1);
            executor.execute(new C0998a(iVar));
        } else {
            if (this.f1216l0) {
                z = false;
            }
            Preconditions.m2832a(z, "Cannot add callbacks to a cancelled EngineJob");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public synchronized void mo10206b(ResourceCallback iVar) {
        try {
            iVar.mo10655a(this.f1214j0, this.f1210f0);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x002f, code lost:
        r4.f1198T.mo10194a(r4, r1, null);
        r0 = r2.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003d, code lost:
        if (r0.hasNext() == false) goto L_0x0052;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003f, code lost:
        r1 = r0.next();
        r1.f1222b.execute(new com.bumptech.glide.load.p023n.EngineJob.C0998a(r4, r1.f1221a));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0052, code lost:
        mo10204b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0055, code lost:
        return;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo10207c() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.bumptech.glide.util.k.c r0 = r4.f1195Q     // Catch:{ all -> 0x0066 }
            r0.mo10716a()     // Catch:{ all -> 0x0066 }
            boolean r0 = r4.f1216l0     // Catch:{ all -> 0x0066 }
            if (r0 == 0) goto L_0x000f
            r4.m1789i()     // Catch:{ all -> 0x0066 }
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            return
        L_0x000f:
            com.bumptech.glide.load.n.l$e r0 = r4.f1194P     // Catch:{ all -> 0x0066 }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x005e
            boolean r0 = r4.f1213i0     // Catch:{ all -> 0x0066 }
            if (r0 != 0) goto L_0x0056
            r0 = 1
            r4.f1213i0 = r0     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.g r1 = r4.f1204Z     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.n.l$e r2 = r4.f1194P     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.n.l$e r2 = r2.mo10218b()     // Catch:{ all -> 0x0066 }
            int r3 = r2.size()     // Catch:{ all -> 0x0066 }
            int r3 = r3 + r0
            r4.mo10201a(r3)     // Catch:{ all -> 0x0066 }
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            com.bumptech.glide.load.n.m r0 = r4.f1198T
            r3 = 0
            r0.mo10194a(r4, r1, r3)
            java.util.Iterator r0 = r2.iterator()
        L_0x0039:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0052
            java.lang.Object r1 = r0.next()
            com.bumptech.glide.load.n.l$d r1 = (com.bumptech.glide.load.p023n.EngineJob.C1001d) r1
            java.util.concurrent.Executor r2 = r1.f1222b
            com.bumptech.glide.load.n.l$a r3 = new com.bumptech.glide.load.n.l$a
            com.bumptech.glide.q.i r1 = r1.f1221a
            r3.<init>(r1)
            r2.execute(r3)
            goto L_0x0039
        L_0x0052:
            r4.mo10204b()
            return
        L_0x0056:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "Already failed once"
            r0.<init>(r1)     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x005e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "Received an exception without any callbacks to notify"
            r0.<init>(r1)     // Catch:{ all -> 0x0066 }
            throw r0     // Catch:{ all -> 0x0066 }
        L_0x0066:
            r0 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0066 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.EngineJob.mo10207c():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public synchronized void mo10204b() {
        this.f1195Q.mo10716a();
        Preconditions.m2832a(m1788h(), "Not yet complete!");
        int decrementAndGet = this.f1203Y.decrementAndGet();
        Preconditions.m2832a(decrementAndGet >= 0, "Can't decrement below 0");
        if (decrementAndGet == 0) {
            if (this.f1214j0 != null) {
                this.f1214j0.mo10232f();
            }
            m1789i();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10202a(ResourceCallback iVar) {
        try {
            iVar.mo10654a(this.f1212h0);
        } catch (Throwable th) {
            throw new CallbackException(th);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10200a() {
        if (!m1788h()) {
            this.f1216l0 = true;
            this.f1215k0.mo10167a();
            this.f1198T.mo10193a(this, this.f1204Z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo10201a(int i) {
        Preconditions.m2832a(m1788h(), "Not yet complete!");
        if (this.f1203Y.getAndAdd(i) == 0 && this.f1214j0 != null) {
            this.f1214j0.mo10229c();
        }
    }

    /* renamed from: a */
    public void mo10174a(Resource<?> vVar, DataSource aVar) {
        synchronized (this) {
            this.f1209e0 = vVar;
            this.f1210f0 = aVar;
        }
        mo10209e();
    }

    /* renamed from: a */
    public void mo10173a(GlideException qVar) {
        synchronized (this) {
            this.f1212h0 = qVar;
        }
        mo10207c();
    }

    /* renamed from: a */
    public void mo10172a(DecodeJob<?> hVar) {
        m1787g().execute(hVar);
    }
}
