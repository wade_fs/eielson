package com.bumptech.glide.load.p021m;

import androidx.annotation.NonNull;
import java.io.FilterInputStream;
import java.io.InputStream;

/* renamed from: com.bumptech.glide.load.m.g */
public final class ExifOrientationStream extends FilterInputStream {

    /* renamed from: R */
    private static final byte[] f945R = {-1, -31, 0, 28, 69, 120, 105, 102, 0, 0, 77, 77, 0, 0, 0, 0, 0, 8, 0, 1, 1, 18, 0, 2, 0, 0, 0, 1, 0};

    /* renamed from: S */
    private static final int f946S = f945R.length;

    /* renamed from: T */
    private static final int f947T = (f946S + 2);

    /* renamed from: P */
    private final byte f948P;

    /* renamed from: Q */
    private int f949Q;

    public ExifOrientationStream(InputStream inputStream, int i) {
        super(inputStream);
        if (i < -1 || i > 8) {
            throw new IllegalArgumentException("Cannot add invalid orientation: " + i);
        }
        this.f948P = (byte) i;
    }

    public void mark(int i) {
        throw new UnsupportedOperationException();
    }

    public boolean markSupported() {
        return false;
    }

    public int read() {
        int i;
        int i2;
        int i3 = this.f949Q;
        if (i3 < 2 || i3 > (i2 = f947T)) {
            i = super.read();
        } else if (i3 == i2) {
            i = this.f948P;
        } else {
            i = f945R[i3 - 2] & 255;
        }
        if (i != -1) {
            this.f949Q++;
        }
        return i;
    }

    public void reset() {
        throw new UnsupportedOperationException();
    }

    public long skip(long j) {
        long skip = super.skip(j);
        if (skip > 0) {
            this.f949Q = (int) (((long) this.f949Q) + skip);
        }
        return skip;
    }

    public int read(@NonNull byte[] bArr, int i, int i2) {
        int i3;
        int i4 = this.f949Q;
        int i5 = f947T;
        if (i4 > i5) {
            i3 = super.read(bArr, i, i2);
        } else if (i4 == i5) {
            bArr[i] = this.f948P;
            i3 = 1;
        } else if (i4 < 2) {
            i3 = super.read(bArr, i, 2 - i4);
        } else {
            int min = Math.min(i5 - i4, i2);
            System.arraycopy(f945R, this.f949Q - 2, bArr, i, min);
            i3 = min;
        }
        if (i3 > 0) {
            this.f949Q += i3;
        }
        return i3;
    }
}
