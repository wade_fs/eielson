package com.bumptech.glide.load.p021m;

import android.content.res.AssetManager;
import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p021m.DataFetcher;
import java.io.IOException;

/* renamed from: com.bumptech.glide.load.m.b */
public abstract class AssetPathFetcher<T> implements DataFetcher<T> {

    /* renamed from: P */
    private final String f935P;

    /* renamed from: Q */
    private final AssetManager f936Q;

    /* renamed from: R */
    private T f937R;

    public AssetPathFetcher(AssetManager assetManager, String str) {
        this.f936Q = assetManager;
        this.f935P = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract T mo9987a(AssetManager assetManager, String str);

    /* renamed from: a */
    public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super T> aVar) {
        try {
            this.f937R = mo9987a(this.f936Q, this.f935P);
            aVar.mo9999a((Object) this.f937R);
        } catch (IOException e) {
            if (Log.isLoggable("AssetPathFetcher", 3)) {
                Log.d("AssetPathFetcher", "Failed to load data from asset manager", e);
            }
            aVar.mo9998a((Exception) e);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo9989a(T t);

    /* renamed from: b */
    public void mo9990b() {
        T t = this.f937R;
        if (t != null) {
            try {
                mo9989a(t);
            } catch (IOException unused) {
            }
        }
    }

    @NonNull
    /* renamed from: c */
    public DataSource mo9991c() {
        return DataSource.LOCAL;
    }

    public void cancel() {
    }
}
