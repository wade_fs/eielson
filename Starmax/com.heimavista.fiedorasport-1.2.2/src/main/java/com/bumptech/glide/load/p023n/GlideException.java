package com.bumptech.glide.load.p023n;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Key;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.bumptech.glide.load.n.q */
public final class GlideException extends Exception {

    /* renamed from: U */
    private static final StackTraceElement[] f1240U = new StackTraceElement[0];
    private static final long serialVersionUID = 1;

    /* renamed from: P */
    private final List<Throwable> f1241P;

    /* renamed from: Q */
    private Key f1242Q;

    /* renamed from: R */
    private DataSource f1243R;

    /* renamed from: S */
    private Class<?> f1244S;

    /* renamed from: T */
    private String f1245T;

    public GlideException(String str) {
        this(str, Collections.emptyList());
    }

    /* renamed from: b */
    private static void m1828b(List<Throwable> list, Appendable appendable) {
        int size = list.size();
        int i = 0;
        while (i < size) {
            int i2 = i + 1;
            appendable.append("Cause (").append(String.valueOf(i2)).append(" of ").append(String.valueOf(size)).append("): ");
            Throwable th = list.get(i);
            if (th instanceof GlideException) {
                ((GlideException) th).m1824a(appendable);
            } else {
                m1825a(th, appendable);
            }
            i = i2;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10237a(Key gVar, DataSource aVar) {
        mo10238a(gVar, aVar, null);
    }

    /* renamed from: a */
    public void mo10239a(@Nullable Exception exc) {
    }

    /* renamed from: e */
    public List<Throwable> mo10241e() {
        ArrayList arrayList = new ArrayList();
        m1826a(this, arrayList);
        return arrayList;
    }

    public Throwable fillInStackTrace() {
        return this;
    }

    public String getMessage() {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder(71);
        sb.append(this.f1245T);
        String str3 = "";
        if (this.f1244S != null) {
            str = ", " + this.f1244S;
        } else {
            str = str3;
        }
        sb.append(str);
        if (this.f1243R != null) {
            str2 = ", " + this.f1243R;
        } else {
            str2 = str3;
        }
        sb.append(str2);
        if (this.f1242Q != null) {
            str3 = ", " + this.f1242Q;
        }
        sb.append(str3);
        List<Throwable> e = mo10241e();
        if (e.isEmpty()) {
            return sb.toString();
        }
        if (e.size() == 1) {
            sb.append("\nThere was 1 cause:");
        } else {
            sb.append("\nThere were ");
            sb.append(e.size());
            sb.append(" causes:");
        }
        for (Throwable th : e) {
            sb.append(10);
            sb.append(th.getClass().getName());
            sb.append('(');
            sb.append(th.getMessage());
            sb.append(')');
        }
        sb.append("\n call GlideException#logRootCauses(String) for more detail");
        return sb.toString();
    }

    public void printStackTrace() {
        printStackTrace(System.err);
    }

    public GlideException(String str, Throwable th) {
        this(str, Collections.singletonList(th));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10238a(Key gVar, DataSource aVar, Class<?> cls) {
        this.f1242Q = gVar;
        this.f1243R = aVar;
        this.f1244S = cls;
    }

    public void printStackTrace(PrintStream printStream) {
        m1824a(printStream);
    }

    public GlideException(String str, List<Throwable> list) {
        this.f1245T = str;
        setStackTrace(f1240U);
        this.f1241P = list;
    }

    public void printStackTrace(PrintWriter printWriter) {
        m1824a(printWriter);
    }

    /* renamed from: com.bumptech.glide.load.n.q$a */
    /* compiled from: GlideException */
    private static final class C1004a implements Appendable {

        /* renamed from: P */
        private final Appendable f1246P;

        /* renamed from: Q */
        private boolean f1247Q = true;

        C1004a(Appendable appendable) {
            this.f1246P = appendable;
        }

        @NonNull
        /* renamed from: a */
        private CharSequence m1835a(@Nullable CharSequence charSequence) {
            return charSequence == null ? "" : charSequence;
        }

        public Appendable append(char c) {
            boolean z = false;
            if (this.f1247Q) {
                this.f1247Q = false;
                this.f1246P.append("  ");
            }
            if (c == 10) {
                z = true;
            }
            this.f1247Q = z;
            this.f1246P.append(c);
            return this;
        }

        public Appendable append(@Nullable CharSequence charSequence) {
            CharSequence a = m1835a(charSequence);
            append(a, 0, a.length());
            return this;
        }

        public Appendable append(@Nullable CharSequence charSequence, int i, int i2) {
            CharSequence a = m1835a(charSequence);
            boolean z = false;
            if (this.f1247Q) {
                this.f1247Q = false;
                this.f1246P.append("  ");
            }
            if (a.length() > 0 && a.charAt(i2 - 1) == 10) {
                z = true;
            }
            this.f1247Q = z;
            this.f1246P.append(a, i, i2);
            return this;
        }
    }

    /* renamed from: a */
    public List<Throwable> mo10236a() {
        return this.f1241P;
    }

    /* renamed from: a */
    public void mo10240a(String str) {
        List<Throwable> e = mo10241e();
        int size = e.size();
        int i = 0;
        while (i < size) {
            StringBuilder sb = new StringBuilder();
            sb.append("Root cause (");
            int i2 = i + 1;
            sb.append(i2);
            sb.append(" of ");
            sb.append(size);
            sb.append(")");
            Log.i(str, sb.toString(), e.get(i));
            i = i2;
        }
    }

    /* renamed from: a */
    private void m1826a(Throwable th, List<Throwable> list) {
        if (th instanceof GlideException) {
            for (Throwable th2 : ((GlideException) th).mo10236a()) {
                m1826a(th2, list);
            }
            return;
        }
        list.add(th);
    }

    /* renamed from: a */
    private void m1824a(Appendable appendable) {
        m1825a(this, appendable);
        m1827a(mo10236a(), new C1004a(appendable));
    }

    /* renamed from: a */
    private static void m1825a(Throwable th, Appendable appendable) {
        try {
            appendable.append(th.getClass().toString()).append(": ").append(th.getMessage()).append(10);
        } catch (IOException unused) {
            throw new RuntimeException(th);
        }
    }

    /* renamed from: a */
    private static void m1827a(List<Throwable> list, Appendable appendable) {
        try {
            m1828b(list, appendable);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
