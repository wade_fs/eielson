package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.load.k */
public interface ResourceEncoder<T> extends Encoder<Resource<T>> {
    @NonNull
    /* renamed from: a */
    EncodeStrategy mo9981a(@NonNull Options iVar);
}
