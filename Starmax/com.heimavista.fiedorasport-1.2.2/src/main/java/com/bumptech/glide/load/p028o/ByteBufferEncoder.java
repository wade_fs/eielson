package com.bumptech.glide.load.p028o;

import android.util.Log;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Encoder;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.util.ByteBufferUtil;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

/* renamed from: com.bumptech.glide.load.o.c */
public class ByteBufferEncoder implements Encoder<ByteBuffer> {
    /* renamed from: a */
    public boolean mo9965a(@NonNull ByteBuffer byteBuffer, @NonNull File file, @NonNull Options iVar) {
        try {
            ByteBufferUtil.m2803a(byteBuffer, file);
            return true;
        } catch (IOException e) {
            if (Log.isLoggable("ByteBufferEncoder", 3)) {
                Log.d("ByteBufferEncoder", "Failed to write data", e);
            }
            return false;
        }
    }
}
