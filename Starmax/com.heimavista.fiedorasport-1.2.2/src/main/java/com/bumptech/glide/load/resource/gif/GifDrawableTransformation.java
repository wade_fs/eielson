package com.bumptech.glide.load.resource.gif;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p030p.p031c.BitmapResource;
import com.bumptech.glide.util.Preconditions;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.resource.gif.e */
public class GifDrawableTransformation implements Transformation<GifDrawable> {

    /* renamed from: b */
    private final Transformation<Bitmap> f1497b;

    public GifDrawableTransformation(Transformation<Bitmap> lVar) {
        Preconditions.m2828a(lVar);
        this.f1497b = lVar;
    }

    @NonNull
    /* renamed from: a */
    public Resource<GifDrawable> mo9982a(@NonNull Context context, @NonNull Resource<GifDrawable> vVar, int i, int i2) {
        GifDrawable gifDrawable = vVar.get();
        BitmapResource dVar = new BitmapResource(gifDrawable.mo10407c(), Glide.m1279b(context).mo9894c());
        Resource<Bitmap> a = this.f1497b.mo9982a(context, dVar, i, i2);
        if (!dVar.equals(a)) {
            dVar.recycle();
        }
        gifDrawable.mo10405a(this.f1497b, a.get());
        return vVar;
    }

    public boolean equals(Object obj) {
        if (obj instanceof GifDrawableTransformation) {
            return this.f1497b.equals(((GifDrawableTransformation) obj).f1497b);
        }
        return false;
    }

    public int hashCode() {
        return this.f1497b.hashCode();
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        this.f1497b.mo9966a(messageDigest);
    }
}
