package com.bumptech.glide.p036m;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

/* renamed from: com.bumptech.glide.m.b */
class StrictLineReader implements Closeable {

    /* renamed from: P */
    private final InputStream f1552P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public final Charset f1553Q;

    /* renamed from: R */
    private byte[] f1554R;

    /* renamed from: S */
    private int f1555S;

    /* renamed from: T */
    private int f1556T;

    /* renamed from: com.bumptech.glide.m.b$a */
    /* compiled from: StrictLineReader */
    class C1100a extends ByteArrayOutputStream {
        C1100a(int i) {
            super(i);
        }

        public String toString() {
            int i = super.count;
            try {
                return new String(super.buf, 0, (i <= 0 || super.buf[i + -1] != 13) ? super.count : i - 1, StrictLineReader.this.f1553Q.name());
            } catch (UnsupportedEncodingException e) {
                throw new AssertionError(e);
            }
        }
    }

    public StrictLineReader(InputStream inputStream, Charset charset) {
        this(inputStream, 8192, charset);
    }

    /* renamed from: c */
    private void m2409c() {
        InputStream inputStream = this.f1552P;
        byte[] bArr = this.f1554R;
        int read = inputStream.read(bArr, 0, bArr.length);
        if (read != -1) {
            this.f1555S = 0;
            this.f1556T = read;
            return;
        }
        throw new EOFException();
    }

    /* renamed from: b */
    public String mo10477b() {
        int i;
        int i2;
        synchronized (this.f1552P) {
            if (this.f1554R != null) {
                if (this.f1555S >= this.f1556T) {
                    m2409c();
                }
                for (int i3 = this.f1555S; i3 != this.f1556T; i3++) {
                    if (this.f1554R[i3] == 10) {
                        if (i3 != this.f1555S) {
                            i2 = i3 - 1;
                            if (this.f1554R[i2] == 13) {
                                String str = new String(this.f1554R, this.f1555S, i2 - this.f1555S, this.f1553Q.name());
                                this.f1555S = i3 + 1;
                                return str;
                            }
                        }
                        i2 = i3;
                        String str2 = new String(this.f1554R, this.f1555S, i2 - this.f1555S, this.f1553Q.name());
                        this.f1555S = i3 + 1;
                        return str2;
                    }
                }
                C1100a aVar = new C1100a((this.f1556T - this.f1555S) + 80);
                loop1:
                while (true) {
                    aVar.write(this.f1554R, this.f1555S, this.f1556T - this.f1555S);
                    this.f1556T = -1;
                    m2409c();
                    i = this.f1555S;
                    while (true) {
                        if (i != this.f1556T) {
                            if (this.f1554R[i] == 10) {
                                break loop1;
                            }
                            i++;
                        }
                    }
                }
                if (i != this.f1555S) {
                    aVar.write(this.f1554R, this.f1555S, i - this.f1555S);
                }
                this.f1555S = i + 1;
                String aVar2 = aVar.toString();
                return aVar2;
            }
            throw new IOException("LineReader is closed");
        }
    }

    public void close() {
        synchronized (this.f1552P) {
            if (this.f1554R != null) {
                this.f1554R = null;
                this.f1552P.close();
            }
        }
    }

    public StrictLineReader(InputStream inputStream, int i, Charset charset) {
        if (inputStream == null || charset == null) {
            throw new NullPointerException();
        } else if (i < 0) {
            throw new IllegalArgumentException("capacity <= 0");
        } else if (charset.equals(Util.f1558a)) {
            this.f1552P = inputStream;
            this.f1553Q = charset;
            this.f1554R = new byte[i];
        } else {
            throw new IllegalArgumentException("Unsupported encoding");
        }
    }

    /* renamed from: a */
    public boolean mo10476a() {
        return this.f1556T == -1;
    }
}
