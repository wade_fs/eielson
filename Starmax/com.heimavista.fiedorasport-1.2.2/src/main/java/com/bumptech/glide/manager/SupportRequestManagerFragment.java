package com.bumptech.glide.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import java.util.HashSet;
import java.util.Set;

public class SupportRequestManagerFragment extends Fragment {

    /* renamed from: P */
    private final ActivityFragmentLifecycle f1566P;

    /* renamed from: Q */
    private final RequestManagerTreeNode f1567Q;

    /* renamed from: R */
    private final Set<SupportRequestManagerFragment> f1568R;
    @Nullable

    /* renamed from: S */
    private SupportRequestManagerFragment f1569S;
    @Nullable

    /* renamed from: T */
    private RequestManager f1570T;
    @Nullable

    /* renamed from: U */
    private Fragment f1571U;

    /* renamed from: com.bumptech.glide.manager.SupportRequestManagerFragment$a */
    private class C1102a implements RequestManagerTreeNode {
        C1102a() {
        }

        public String toString() {
            return super.toString() + "{fragment=" + SupportRequestManagerFragment.this + "}";
        }
    }

    public SupportRequestManagerFragment() {
        this(new ActivityFragmentLifecycle());
    }

    @Nullable
    /* renamed from: e */
    private Fragment m2427e() {
        Fragment parentFragment = getParentFragment();
        return parentFragment != null ? super : this.f1571U;
    }

    /* renamed from: f */
    private void m2428f() {
        SupportRequestManagerFragment supportRequestManagerFragment = this.f1569S;
        if (supportRequestManagerFragment != null) {
            supportRequestManagerFragment.m2426b(this);
            this.f1569S = null;
        }
    }

    /* renamed from: a */
    public void mo10493a(@Nullable RequestManager kVar) {
        this.f1570T = kVar;
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: b */
    public ActivityFragmentLifecycle mo10494b() {
        return this.f1566P;
    }

    @Nullable
    /* renamed from: c */
    public RequestManager mo10495c() {
        return this.f1570T;
    }

    @NonNull
    /* renamed from: d */
    public RequestManagerTreeNode mo10496d() {
        return this.f1567Q;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            m2424a(getActivity());
        } catch (IllegalStateException e) {
            if (Log.isLoggable("SupportRMFragment", 5)) {
                Log.w("SupportRMFragment", "Unable to register fragment with root", e);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        this.f1566P.mo10498a();
        m2428f();
    }

    public void onDetach() {
        super.onDetach();
        this.f1571U = null;
        m2428f();
    }

    public void onStart() {
        super.onStart();
        this.f1566P.mo10500b();
    }

    public void onStop() {
        super.onStop();
        this.f1566P.mo10502c();
    }

    public String toString() {
        return super.toString() + "{parent=" + m2427e() + "}";
    }

    @VisibleForTesting
    @SuppressLint({"ValidFragment"})
    public SupportRequestManagerFragment(@NonNull ActivityFragmentLifecycle aVar) {
        this.f1567Q = new C1102a();
        this.f1568R = new HashSet();
        this.f1566P = aVar;
    }

    /* renamed from: a */
    private void m2425a(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.f1568R.add(supportRequestManagerFragment);
    }

    /* renamed from: b */
    private void m2426b(SupportRequestManagerFragment supportRequestManagerFragment) {
        this.f1568R.remove(supportRequestManagerFragment);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10492a(@Nullable Fragment fragment) {
        this.f1571U = super;
        if (fragment != null && super.getActivity() != null) {
            m2424a(super.getActivity());
        }
    }

    /* renamed from: a */
    private void m2424a(@NonNull FragmentActivity fragmentActivity) {
        m2428f();
        this.f1569S = Glide.m1279b(fragmentActivity).mo9899h().mo10513b(fragmentActivity);
        if (!equals(this.f1569S)) {
            this.f1569S.m2425a(this);
        }
    }
}
