package com.bumptech.glide.load.p030p.p031c;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Animatable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.load.p023n.p024a0.BitmapPool;
import com.bumptech.glide.load.p023n.p024a0.BitmapPoolAdapter;
import java.util.concurrent.locks.Lock;

/* renamed from: com.bumptech.glide.load.p.c.m */
final class DrawableToBitmapConverter {

    /* renamed from: a */
    private static final BitmapPool f1423a = new C1075a();

    /* renamed from: com.bumptech.glide.load.p.c.m$a */
    /* compiled from: DrawableToBitmapConverter */
    class C1075a extends BitmapPoolAdapter {
        C1075a() {
        }

        /* renamed from: a */
        public void mo10063a(Bitmap bitmap) {
        }
    }

    @Nullable
    /* renamed from: a */
    static Resource<Bitmap> m2175a(BitmapPool eVar, Drawable drawable, int i, int i2) {
        Bitmap bitmap;
        Drawable current = drawable.getCurrent();
        boolean z = false;
        if (current instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) current).getBitmap();
        } else if (!(current instanceof Animatable)) {
            bitmap = m2176b(eVar, current, i, i2);
            z = true;
        } else {
            bitmap = null;
        }
        if (!z) {
            eVar = f1423a;
        }
        return BitmapResource.m2091a(bitmap, eVar);
    }

    @Nullable
    /* renamed from: b */
    private static Bitmap m2176b(BitmapPool eVar, Drawable drawable, int i, int i2) {
        if (i == Integer.MIN_VALUE && drawable.getIntrinsicWidth() <= 0) {
            if (Log.isLoggable("DrawableToBitmap", 5)) {
                Log.w("DrawableToBitmap", "Unable to draw " + drawable + " to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic width");
            }
            return null;
        } else if (i2 != Integer.MIN_VALUE || drawable.getIntrinsicHeight() > 0) {
            if (drawable.getIntrinsicWidth() > 0) {
                i = drawable.getIntrinsicWidth();
            }
            if (drawable.getIntrinsicHeight() > 0) {
                i2 = drawable.getIntrinsicHeight();
            }
            Lock a = TransformationUtils.m2213a();
            a.lock();
            Bitmap a2 = eVar.mo10060a(i, i2, Bitmap.Config.ARGB_8888);
            try {
                Canvas canvas = new Canvas(a2);
                drawable.setBounds(0, 0, i, i2);
                drawable.draw(canvas);
                canvas.setBitmap(null);
                return a2;
            } finally {
                a.unlock();
            }
        } else {
            if (Log.isLoggable("DrawableToBitmap", 5)) {
                Log.w("DrawableToBitmap", "Unable to draw " + drawable + " to Bitmap with Target.SIZE_ORIGINAL because the Drawable has no intrinsic height");
            }
            return null;
        }
    }
}
