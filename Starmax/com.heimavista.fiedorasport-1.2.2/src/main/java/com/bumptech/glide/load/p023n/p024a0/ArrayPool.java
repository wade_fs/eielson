package com.bumptech.glide.load.p023n.p024a0;

/* renamed from: com.bumptech.glide.load.n.a0.b */
public interface ArrayPool {
    /* renamed from: a */
    <T> T mo10039a(int i, Class<T> cls);

    /* renamed from: a */
    void mo10040a();

    /* renamed from: a */
    void mo10041a(int i);

    /* renamed from: b */
    <T> T mo10042b(int i, Class<T> cls);

    <T> void put(T t);
}
