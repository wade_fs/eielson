package com.bumptech.glide.p040q;

/* renamed from: com.bumptech.glide.q.e */
public interface RequestCoordinator {
    /* renamed from: a */
    boolean mo10623a();

    /* renamed from: b */
    void mo10625b(Request dVar);

    /* renamed from: c */
    boolean mo10627c(Request dVar);

    /* renamed from: d */
    boolean mo10629d(Request dVar);

    /* renamed from: e */
    void mo10630e(Request dVar);

    /* renamed from: f */
    boolean mo10633f(Request dVar);
}
