package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p021m.DataFetcher;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.p043r.ObjectKey;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;

/* renamed from: com.bumptech.glide.load.o.b */
public class ByteArrayLoader<Data> implements ModelLoader<byte[], Data> {

    /* renamed from: a */
    private final C1012b<Data> f1291a;

    /* renamed from: com.bumptech.glide.load.o.b$a */
    /* compiled from: ByteArrayLoader */
    public static class C1010a implements ModelLoaderFactory<byte[], ByteBuffer> {

        /* renamed from: com.bumptech.glide.load.o.b$a$a */
        /* compiled from: ByteArrayLoader */
        class C1011a implements C1012b<ByteBuffer> {
            C1011a(C1010a aVar) {
            }

            /* renamed from: a */
            public ByteBuffer mo10269a(byte[] bArr) {
                return ByteBuffer.wrap(bArr);
            }

            /* renamed from: a */
            public Class<ByteBuffer> mo10268a() {
                return ByteBuffer.class;
            }
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<byte[], ByteBuffer> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new ByteArrayLoader(new C1011a(this));
        }
    }

    /* renamed from: com.bumptech.glide.load.o.b$b */
    /* compiled from: ByteArrayLoader */
    public interface C1012b<Data> {
        /* renamed from: a */
        Class<Data> mo10268a();

        /* renamed from: a */
        Data mo10269a(byte[] bArr);
    }

    /* renamed from: com.bumptech.glide.load.o.b$d */
    /* compiled from: ByteArrayLoader */
    public static class C1014d implements ModelLoaderFactory<byte[], InputStream> {

        /* renamed from: com.bumptech.glide.load.o.b$d$a */
        /* compiled from: ByteArrayLoader */
        class C1015a implements C1012b<InputStream> {
            C1015a(C1014d dVar) {
            }

            /* renamed from: a */
            public InputStream m1895a(byte[] bArr) {
                return new ByteArrayInputStream(bArr);
            }

            /* renamed from: a */
            public Class<InputStream> mo10268a() {
                return InputStream.class;
            }
        }

        @NonNull
        /* renamed from: a */
        public ModelLoader<byte[], InputStream> mo10265a(@NonNull MultiModelLoaderFactory rVar) {
            return new ByteArrayLoader(new C1015a(this));
        }
    }

    public ByteArrayLoader(C1012b<Data> bVar) {
        this.f1291a = bVar;
    }

    /* renamed from: a */
    public boolean mo10263a(@NonNull byte[] bArr) {
        return true;
    }

    /* renamed from: com.bumptech.glide.load.o.b$c */
    /* compiled from: ByteArrayLoader */
    private static class C1013c<Data> implements DataFetcher<Data> {

        /* renamed from: P */
        private final byte[] f1292P;

        /* renamed from: Q */
        private final C1012b<Data> f1293Q;

        C1013c(byte[] bArr, C1012b<Data> bVar) {
            this.f1292P = bArr;
            this.f1293Q = bVar;
        }

        /* renamed from: a */
        public void mo9988a(@NonNull Priority hVar, @NonNull DataFetcher.C0934a<? super Data> aVar) {
            aVar.mo9999a((Object) this.f1293Q.mo10269a(this.f1292P));
        }

        /* renamed from: b */
        public void mo9990b() {
        }

        @NonNull
        /* renamed from: c */
        public DataSource mo9991c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        @NonNull
        /* renamed from: a */
        public Class<Data> mo9984a() {
            return this.f1293Q.mo10268a();
        }
    }

    /* renamed from: a */
    public ModelLoader.C1036a<Data> mo10261a(@NonNull byte[] bArr, int i, int i2, @NonNull Options iVar) {
        return new ModelLoader.C1036a<>(new ObjectKey(bArr), new C1013c(bArr, this.f1291a));
    }
}
