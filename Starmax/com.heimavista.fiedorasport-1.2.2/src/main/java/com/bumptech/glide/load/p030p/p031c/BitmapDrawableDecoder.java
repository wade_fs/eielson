package com.bumptech.glide.load.p030p.p031c;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import androidx.annotation.NonNull;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.ResourceDecoder;
import com.bumptech.glide.load.p023n.Resource;
import com.bumptech.glide.util.Preconditions;

/* renamed from: com.bumptech.glide.load.p.c.a */
public class BitmapDrawableDecoder<DataType> implements ResourceDecoder<DataType, BitmapDrawable> {

    /* renamed from: a */
    private final ResourceDecoder<DataType, Bitmap> f1384a;

    /* renamed from: b */
    private final Resources f1385b;

    public BitmapDrawableDecoder(@NonNull Resources resources, @NonNull ResourceDecoder<DataType, Bitmap> jVar) {
        Preconditions.m2828a(resources);
        this.f1385b = resources;
        Preconditions.m2828a(jVar);
        this.f1384a = jVar;
    }

    /* renamed from: a */
    public boolean mo9980a(@NonNull DataType datatype, @NonNull Options iVar) {
        return this.f1384a.mo9980a(datatype, iVar);
    }

    /* renamed from: a */
    public Resource<BitmapDrawable> mo9979a(@NonNull DataType datatype, int i, int i2, @NonNull Options iVar) {
        return LazyBitmapDrawableResource.m2190a(this.f1385b, this.f1384a.mo9979a(datatype, i, i2, iVar));
    }
}
