package com.bumptech.glide.util;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Looper;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.bumptech.glide.load.p028o.Model;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Queue;

/* renamed from: com.bumptech.glide.util.j */
/* compiled from: Util */
public final class C1122j {

    /* renamed from: a */
    private static final char[] f1788a = "0123456789abcdef".toCharArray();

    /* renamed from: b */
    private static final char[] f1789b = new char[64];

    /* renamed from: com.bumptech.glide.util.j$a */
    /* compiled from: Util */
    static /* synthetic */ class C1123a {

        /* renamed from: a */
        static final /* synthetic */ int[] f1790a = new int[Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                android.graphics.Bitmap$Config[] r0 = android.graphics.Bitmap.Config.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.bumptech.glide.util.C1122j.C1123a.f1790a = r0
                int[] r0 = com.bumptech.glide.util.C1122j.C1123a.f1790a     // Catch:{ NoSuchFieldError -> 0x0014 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ALPHA_8     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.bumptech.glide.util.C1122j.C1123a.f1790a     // Catch:{ NoSuchFieldError -> 0x001f }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.bumptech.glide.util.C1122j.C1123a.f1790a     // Catch:{ NoSuchFieldError -> 0x002a }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_4444     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.bumptech.glide.util.C1122j.C1123a.f1790a     // Catch:{ NoSuchFieldError -> 0x0035 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.RGBA_F16     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.bumptech.glide.util.C1122j.C1123a.f1790a     // Catch:{ NoSuchFieldError -> 0x0040 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.util.C1122j.C1123a.<clinit>():void");
        }
    }

    /* renamed from: a */
    public static int m2835a(int i, int i2) {
        return (i2 * 31) + i;
    }

    @NonNull
    /* renamed from: a */
    public static String m2841a(@NonNull byte[] bArr) {
        String a;
        synchronized (f1789b) {
            a = m2842a(bArr, f1789b);
        }
        return a;
    }

    /* renamed from: b */
    private static boolean m2848b(int i) {
        return i > 0 || i == Integer.MIN_VALUE;
    }

    /* renamed from: b */
    public static boolean m2849b(int i, int i2) {
        return m2848b(i) && m2848b(i2);
    }

    /* renamed from: c */
    public static boolean m2851c() {
        return !m2852d();
    }

    /* renamed from: d */
    public static boolean m2852d() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    /* renamed from: b */
    public static void m2847b() {
        if (!m2852d()) {
            throw new IllegalArgumentException("You must call this method on the main thread");
        }
    }

    @NonNull
    /* renamed from: a */
    private static String m2842a(@NonNull byte[] bArr, @NonNull char[] cArr) {
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & 255;
            int i2 = i * 2;
            char[] cArr2 = f1788a;
            cArr[i2] = cArr2[b >>> 4];
            cArr[i2 + 1] = cArr2[b & 15];
        }
        return new String(cArr);
    }

    /* renamed from: b */
    public static boolean m2850b(@Nullable Object obj, @Nullable Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        return obj.equals(obj2);
    }

    @TargetApi(19)
    /* renamed from: a */
    public static int m2838a(@NonNull Bitmap bitmap) {
        if (!bitmap.isRecycled()) {
            if (Build.VERSION.SDK_INT >= 19) {
                try {
                    return bitmap.getAllocationByteCount();
                } catch (NullPointerException unused) {
                }
            }
            return bitmap.getHeight() * bitmap.getRowBytes();
        }
        throw new IllegalStateException("Cannot obtain size for recycled Bitmap: " + bitmap + "[" + bitmap.getWidth() + "x" + bitmap.getHeight() + "] " + bitmap.getConfig());
    }

    /* renamed from: a */
    public static int m2836a(int i, int i2, @Nullable Bitmap.Config config) {
        return i * i2 * m2837a(config);
    }

    /* renamed from: a */
    private static int m2837a(@Nullable Bitmap.Config config) {
        if (config == null) {
            config = Bitmap.Config.ARGB_8888;
        }
        int i = C1123a.f1790a[config.ordinal()];
        if (i == 1) {
            return 1;
        }
        if (i == 2 || i == 3) {
            return 2;
        }
        return i != 4 ? 4 : 8;
    }

    /* renamed from: a */
    public static void m2845a() {
        if (!m2851c()) {
            throw new IllegalArgumentException("You must call this method on a background thread");
        }
    }

    @NonNull
    /* renamed from: a */
    public static <T> Queue<T> m2844a(int i) {
        return new ArrayDeque(i);
    }

    @NonNull
    /* renamed from: a */
    public static <T> List<T> m2843a(@NonNull Collection collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (Object obj : collection) {
            if (obj != null) {
                arrayList.add(obj);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    public static boolean m2846a(@Nullable Object obj, @Nullable Object obj2) {
        if (obj == null) {
            return obj2 == null;
        }
        if (obj instanceof Model) {
            return ((Model) obj).mo10299a(obj2);
        }
        return obj.equals(obj2);
    }

    /* renamed from: a */
    public static int m2833a(float f) {
        return m2834a(f, 17);
    }

    /* renamed from: a */
    public static int m2834a(float f, int i) {
        return m2835a(Float.floatToIntBits(f), i);
    }

    /* renamed from: a */
    public static int m2839a(@Nullable Object obj, int i) {
        return m2835a(obj == null ? 0 : obj.hashCode(), i);
    }

    /* renamed from: a */
    public static int m2840a(boolean z, int i) {
        return m2835a(z ? 1 : 0, i);
    }
}
