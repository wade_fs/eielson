package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.collection.ArrayMap;
import androidx.collection.SimpleArrayMap;
import com.bumptech.glide.util.CachedHashCodeArrayMap;
import java.security.MessageDigest;

/* renamed from: com.bumptech.glide.load.i */
public final class Options implements Key {

    /* renamed from: b */
    private final ArrayMap<Option<?>, Object> f934b = new CachedHashCodeArrayMap();

    /* renamed from: a */
    public void mo9977a(@NonNull Options iVar) {
        this.f934b.putAll((SimpleArrayMap<? extends Option<?>, ? extends Object>) iVar.f934b);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Options) {
            return this.f934b.equals(((Options) obj).f934b);
        }
        return false;
    }

    public int hashCode() {
        return this.f934b.hashCode();
    }

    public String toString() {
        return "Options{values=" + this.f934b + '}';
    }

    @NonNull
    /* renamed from: a */
    public <T> Options mo9975a(@NonNull Option<T> hVar, @NonNull T t) {
        this.f934b.put(hVar, t);
        return this;
    }

    @Nullable
    /* renamed from: a */
    public <T> T mo9976a(@NonNull Option hVar) {
        return this.f934b.containsKey(hVar) ? this.f934b.get(hVar) : hVar.mo9969a();
    }

    /* renamed from: a */
    public void mo9966a(@NonNull MessageDigest messageDigest) {
        for (int i = 0; i < this.f934b.size(); i++) {
            m1378a(this.f934b.keyAt(i), this.f934b.valueAt(i), messageDigest);
        }
    }

    /* renamed from: a */
    private static <T> void m1378a(@NonNull Option<T> hVar, @NonNull Object obj, @NonNull MessageDigest messageDigest) {
        hVar.mo9970a(obj, messageDigest);
    }
}
