package com.bumptech.glide;

/* renamed from: com.bumptech.glide.h */
public enum Priority {
    IMMEDIATE,
    HIGH,
    NORMAL,
    LOW
}
