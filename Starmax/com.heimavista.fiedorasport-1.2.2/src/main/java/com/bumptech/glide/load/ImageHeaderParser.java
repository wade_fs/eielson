package com.bumptech.glide.load;

import androidx.annotation.NonNull;
import com.bumptech.glide.load.p023n.p024a0.ArrayPool;
import java.io.InputStream;
import java.nio.ByteBuffer;

public interface ImageHeaderParser {

    public enum ImageType {
        GIF(true),
        JPEG(false),
        RAW(false),
        PNG_A(true),
        PNG(false),
        WEBP_A(true),
        WEBP(false),
        UNKNOWN(false);
        

        /* renamed from: P */
        private final boolean f913P;

        private ImageType(boolean z) {
            this.f913P = z;
        }

        public boolean hasAlpha() {
            return this.f913P;
        }
    }

    /* renamed from: a */
    int mo9961a(@NonNull InputStream inputStream, @NonNull ArrayPool bVar);

    @NonNull
    /* renamed from: a */
    ImageType mo9962a(@NonNull InputStream inputStream);

    @NonNull
    /* renamed from: a */
    ImageType mo9963a(@NonNull ByteBuffer byteBuffer);
}
