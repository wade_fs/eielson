package com.bumptech.glide.load.p023n.p024a0;

import android.graphics.Bitmap;
import android.os.Build;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.annotation.VisibleForTesting;
import com.bumptech.glide.load.p023n.p024a0.LruArrayPool;
import com.bumptech.glide.util.C1122j;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;

@RequiresApi(19)
/* renamed from: com.bumptech.glide.load.n.a0.n */
public class SizeConfigStrategy implements LruPoolStrategy {

    /* renamed from: d */
    private static final Bitmap.Config[] f1019d;

    /* renamed from: e */
    private static final Bitmap.Config[] f1020e = f1019d;

    /* renamed from: f */
    private static final Bitmap.Config[] f1021f = {Bitmap.Config.RGB_565};

    /* renamed from: g */
    private static final Bitmap.Config[] f1022g = {Bitmap.Config.ARGB_4444};

    /* renamed from: h */
    private static final Bitmap.Config[] f1023h = {Bitmap.Config.ALPHA_8};

    /* renamed from: a */
    private final C0957c f1024a = new C0957c();

    /* renamed from: b */
    private final GroupedLinkedMap<C0956b, Bitmap> f1025b = new GroupedLinkedMap<>();

    /* renamed from: c */
    private final Map<Bitmap.Config, NavigableMap<Integer, Integer>> f1026c = new HashMap();

    /* renamed from: com.bumptech.glide.load.n.a0.n$a */
    /* compiled from: SizeConfigStrategy */
    static /* synthetic */ class C0955a {

        /* renamed from: a */
        static final /* synthetic */ int[] f1027a = new int[Bitmap.Config.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                android.graphics.Bitmap$Config[] r0 = android.graphics.Bitmap.Config.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.bumptech.glide.load.p023n.p024a0.SizeConfigStrategy.C0955a.f1027a = r0
                int[] r0 = com.bumptech.glide.load.p023n.p024a0.SizeConfigStrategy.C0955a.f1027a     // Catch:{ NoSuchFieldError -> 0x0014 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.bumptech.glide.load.p023n.p024a0.SizeConfigStrategy.C0955a.f1027a     // Catch:{ NoSuchFieldError -> 0x001f }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.RGB_565     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.bumptech.glide.load.p023n.p024a0.SizeConfigStrategy.C0955a.f1027a     // Catch:{ NoSuchFieldError -> 0x002a }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_4444     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.bumptech.glide.load.p023n.p024a0.SizeConfigStrategy.C0955a.f1027a     // Catch:{ NoSuchFieldError -> 0x0035 }
                android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ALPHA_8     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.bumptech.glide.load.p023n.p024a0.SizeConfigStrategy.C0955a.<clinit>():void");
        }
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.a0.n$c */
    /* compiled from: SizeConfigStrategy */
    static class C0957c extends BaseKeyPool<C0956b> {
        C0957c() {
        }

        /* renamed from: a */
        public C0956b mo10087a(int i, Bitmap.Config config) {
            C0956b bVar = (C0956b) mo10059b();
            bVar.mo10083a(i, config);
            return bVar;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C0956b mo10056a() {
            return new C0956b(this);
        }
    }

    static {
        Bitmap.Config[] configArr = {Bitmap.Config.ARGB_8888, null};
        if (Build.VERSION.SDK_INT >= 26) {
            configArr = (Bitmap.Config[]) Arrays.copyOf(configArr, configArr.length + 1);
            configArr[configArr.length - 1] = Bitmap.Config.RGBA_F16;
        }
        f1019d = configArr;
    }

    /* renamed from: b */
    private NavigableMap<Integer, Integer> m1593b(Bitmap.Config config) {
        NavigableMap<Integer, Integer> navigableMap = this.f1026c.get(config);
        if (navigableMap != null) {
            return navigableMap;
        }
        TreeMap treeMap = new TreeMap();
        this.f1026c.put(config, treeMap);
        return treeMap;
    }

    /* renamed from: a */
    public void mo10046a(Bitmap bitmap) {
        C0956b a = this.f1024a.mo10087a(C1122j.m2838a(bitmap), bitmap.getConfig());
        this.f1025b.mo10068a(a, bitmap);
        NavigableMap<Integer, Integer> b = m1593b(bitmap.getConfig());
        Integer num = (Integer) b.get(Integer.valueOf(a.f1029b));
        Integer valueOf = Integer.valueOf(a.f1029b);
        int i = 1;
        if (num != null) {
            i = 1 + num.intValue();
        }
        b.put(valueOf, Integer.valueOf(i));
    }

    /* renamed from: c */
    public String mo10049c(Bitmap bitmap) {
        return m1592b(C1122j.m2838a(bitmap), bitmap.getConfig());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("SizeConfigStrategy{groupedMap=");
        sb.append(this.f1025b);
        sb.append(", sortedSizes=(");
        for (Map.Entry entry : this.f1026c.entrySet()) {
            sb.append(entry.getKey());
            sb.append('[');
            sb.append(entry.getValue());
            sb.append("], ");
        }
        if (!this.f1026c.isEmpty()) {
            sb.replace(sb.length() - 2, sb.length(), "");
        }
        sb.append(")}");
        return sb.toString();
    }

    @VisibleForTesting
    /* renamed from: com.bumptech.glide.load.n.a0.n$b */
    /* compiled from: SizeConfigStrategy */
    static final class C0956b implements Poolable {

        /* renamed from: a */
        private final C0957c f1028a;

        /* renamed from: b */
        int f1029b;

        /* renamed from: c */
        private Bitmap.Config f1030c;

        public C0956b(C0957c cVar) {
            this.f1028a = cVar;
        }

        /* renamed from: a */
        public void mo10083a(int i, Bitmap.Config config) {
            this.f1029b = i;
            this.f1030c = config;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C0956b)) {
                return false;
            }
            C0956b bVar = (C0956b) obj;
            if (this.f1029b != bVar.f1029b || !C1122j.m2850b(this.f1030c, bVar.f1030c)) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i = this.f1029b * 31;
            Bitmap.Config config = this.f1030c;
            return i + (config != null ? config.hashCode() : 0);
        }

        public String toString() {
            return SizeConfigStrategy.m1592b(this.f1029b, this.f1030c);
        }

        /* renamed from: a */
        public void mo10051a() {
            this.f1028a.mo10058a(this);
        }
    }

    /* renamed from: b */
    public String mo10048b(int i, int i2, Bitmap.Config config) {
        return m1592b(C1122j.m2836a(i, i2, config), config);
    }

    /* renamed from: b */
    public int mo10047b(Bitmap bitmap) {
        return C1122j.m2838a(bitmap);
    }

    /* renamed from: b */
    static String m1592b(int i, Bitmap.Config config) {
        return "[" + i + "](" + config + ")";
    }

    @Nullable
    /* renamed from: a */
    public Bitmap mo10045a(int i, int i2, Bitmap.Config config) {
        C0956b a = m1589a(C1122j.m2836a(i, i2, config), config);
        Bitmap a2 = this.f1025b.mo10067a((LruArrayPool.C0951a) a);
        if (a2 != null) {
            m1590a(Integer.valueOf(a.f1029b), a2);
            a2.reconfigure(i, i2, config);
        }
        return a2;
    }

    /* renamed from: a */
    private C0956b m1589a(int i, Bitmap.Config config) {
        C0956b a = this.f1024a.mo10087a(i, config);
        Bitmap.Config[] a2 = m1591a(config);
        int length = a2.length;
        int i2 = 0;
        while (i2 < length) {
            Bitmap.Config config2 = a2[i2];
            Integer ceilingKey = m1593b(config2).ceilingKey(Integer.valueOf(i));
            if (ceilingKey == null || ceilingKey.intValue() > i * 8) {
                i2++;
            } else {
                if (ceilingKey.intValue() == i) {
                    if (config2 == null) {
                        if (config == null) {
                            return a;
                        }
                    } else if (config2.equals(config)) {
                        return a;
                    }
                }
                this.f1024a.mo10058a(a);
                return this.f1024a.mo10087a(ceilingKey.intValue(), config2);
            }
        }
        return a;
    }

    @Nullable
    /* renamed from: a */
    public Bitmap mo10044a() {
        Bitmap a = this.f1025b.mo10066a();
        if (a != null) {
            m1590a(Integer.valueOf(C1122j.m2838a(a)), a);
        }
        return a;
    }

    /* renamed from: a */
    private void m1590a(Integer num, Bitmap bitmap) {
        NavigableMap<Integer, Integer> b = m1593b(bitmap.getConfig());
        Integer num2 = (Integer) b.get(num);
        if (num2 == null) {
            throw new NullPointerException("Tried to decrement empty size, size: " + num + ", removed: " + mo10049c(bitmap) + ", this: " + this);
        } else if (num2.intValue() == 1) {
            b.remove(num);
        } else {
            b.put(num, Integer.valueOf(num2.intValue() - 1));
        }
    }

    /* renamed from: a */
    private static Bitmap.Config[] m1591a(Bitmap.Config config) {
        if (Build.VERSION.SDK_INT >= 26 && Bitmap.Config.RGBA_F16.equals(config)) {
            return f1020e;
        }
        int i = C0955a.f1027a[config.ordinal()];
        if (i == 1) {
            return f1019d;
        }
        if (i == 2) {
            return f1021f;
        }
        if (i == 3) {
            return f1022g;
        }
        if (i == 4) {
            return f1023h;
        }
        return new Bitmap.Config[]{config};
    }
}
