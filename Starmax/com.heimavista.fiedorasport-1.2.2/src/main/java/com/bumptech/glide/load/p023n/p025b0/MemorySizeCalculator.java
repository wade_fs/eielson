package com.bumptech.glide.load.p023n.p025b0;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;

/* renamed from: com.bumptech.glide.load.n.b0.i */
public final class MemorySizeCalculator {

    /* renamed from: a */
    private final int f1046a;

    /* renamed from: b */
    private final int f1047b;

    /* renamed from: c */
    private final Context f1048c;

    /* renamed from: d */
    private final int f1049d;

    /* renamed from: com.bumptech.glide.load.n.b0.i$a */
    /* compiled from: MemorySizeCalculator */
    public static final class C0965a {

        /* renamed from: i */
        static final int f1050i = (Build.VERSION.SDK_INT < 26 ? 4 : 1);

        /* renamed from: a */
        final Context f1051a;

        /* renamed from: b */
        ActivityManager f1052b;

        /* renamed from: c */
        C0967c f1053c;

        /* renamed from: d */
        float f1054d = 2.0f;

        /* renamed from: e */
        float f1055e = ((float) f1050i);

        /* renamed from: f */
        float f1056f = 0.4f;

        /* renamed from: g */
        float f1057g = 0.33f;

        /* renamed from: h */
        int f1058h = 4194304;

        public C0965a(Context context) {
            this.f1051a = context;
            this.f1052b = (ActivityManager) context.getSystemService("activity");
            this.f1053c = new C0966b(context.getResources().getDisplayMetrics());
            if (Build.VERSION.SDK_INT >= 26 && MemorySizeCalculator.m1636a(this.f1052b)) {
                this.f1055e = 0.0f;
            }
        }

        /* renamed from: a */
        public MemorySizeCalculator mo10110a() {
            return new MemorySizeCalculator(this);
        }
    }

    /* renamed from: com.bumptech.glide.load.n.b0.i$b */
    /* compiled from: MemorySizeCalculator */
    private static final class C0966b implements C0967c {

        /* renamed from: a */
        private final DisplayMetrics f1059a;

        C0966b(DisplayMetrics displayMetrics) {
            this.f1059a = displayMetrics;
        }

        /* renamed from: a */
        public int mo10111a() {
            return this.f1059a.heightPixels;
        }

        /* renamed from: b */
        public int mo10112b() {
            return this.f1059a.widthPixels;
        }
    }

    /* renamed from: com.bumptech.glide.load.n.b0.i$c */
    /* compiled from: MemorySizeCalculator */
    interface C0967c {
        /* renamed from: a */
        int mo10111a();

        /* renamed from: b */
        int mo10112b();
    }

    MemorySizeCalculator(C0965a aVar) {
        int i;
        this.f1048c = aVar.f1051a;
        if (m1636a(aVar.f1052b)) {
            i = aVar.f1058h / 2;
        } else {
            i = aVar.f1058h;
        }
        this.f1049d = i;
        int a = m1634a(aVar.f1052b, aVar.f1056f, aVar.f1057g);
        float b = (float) (aVar.f1053c.mo10112b() * aVar.f1053c.mo10111a() * 4);
        int round = Math.round(aVar.f1055e * b);
        int round2 = Math.round(b * aVar.f1054d);
        int i2 = a - this.f1049d;
        int i3 = round2 + round;
        if (i3 <= i2) {
            this.f1047b = round2;
            this.f1046a = round;
        } else {
            float f = (float) i2;
            float f2 = aVar.f1055e;
            float f3 = aVar.f1054d;
            float f4 = f / (f2 + f3);
            this.f1047b = Math.round(f3 * f4);
            this.f1046a = Math.round(f4 * aVar.f1055e);
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            StringBuilder sb = new StringBuilder();
            sb.append("Calculation complete, Calculated memory cache size: ");
            sb.append(m1635a(this.f1047b));
            sb.append(", pool size: ");
            sb.append(m1635a(this.f1046a));
            sb.append(", byte array size: ");
            sb.append(m1635a(this.f1049d));
            sb.append(", memory class limited? ");
            sb.append(i3 > a);
            sb.append(", max size: ");
            sb.append(m1635a(a));
            sb.append(", memoryClass: ");
            sb.append(aVar.f1052b.getMemoryClass());
            sb.append(", isLowMemoryDevice: ");
            sb.append(m1636a(aVar.f1052b));
            Log.d("MemorySizeCalculator", sb.toString());
        }
    }

    /* renamed from: a */
    public int mo10107a() {
        return this.f1049d;
    }

    /* renamed from: b */
    public int mo10108b() {
        return this.f1046a;
    }

    /* renamed from: c */
    public int mo10109c() {
        return this.f1047b;
    }

    /* renamed from: a */
    private static int m1634a(ActivityManager activityManager, float f, float f2) {
        boolean a = m1636a(activityManager);
        float memoryClass = (float) (activityManager.getMemoryClass() * 1024 * 1024);
        if (a) {
            f = f2;
        }
        return Math.round(memoryClass * f);
    }

    /* renamed from: a */
    private String m1635a(int i) {
        return Formatter.formatFileSize(this.f1048c, (long) i);
    }

    @TargetApi(19)
    /* renamed from: a */
    static boolean m1636a(ActivityManager activityManager) {
        if (Build.VERSION.SDK_INT >= 19) {
            return activityManager.isLowRamDevice();
        }
        return true;
    }
}
