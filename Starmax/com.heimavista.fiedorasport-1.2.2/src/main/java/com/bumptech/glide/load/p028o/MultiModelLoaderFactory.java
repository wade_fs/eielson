package com.bumptech.glide.load.p028o;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.core.util.Pools;
import com.bumptech.glide.Registry;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.p028o.ModelLoader;
import com.bumptech.glide.util.Preconditions;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/* renamed from: com.bumptech.glide.load.o.r */
public class MultiModelLoaderFactory {

    /* renamed from: e */
    private static final C1042c f1345e = new C1042c();

    /* renamed from: f */
    private static final ModelLoader<Object, Object> f1346f = new C1040a();

    /* renamed from: a */
    private final List<C1041b<?, ?>> f1347a;

    /* renamed from: b */
    private final C1042c f1348b;

    /* renamed from: c */
    private final Set<C1041b<?, ?>> f1349c;

    /* renamed from: d */
    private final Pools.Pool<List<Throwable>> f1350d;

    /* renamed from: com.bumptech.glide.load.o.r$a */
    /* compiled from: MultiModelLoaderFactory */
    private static class C1040a implements ModelLoader<Object, Object> {
        C1040a() {
        }

        @Nullable
        /* renamed from: a */
        public ModelLoader.C1036a<Object> mo10261a(@NonNull Object obj, int i, int i2, @NonNull Options iVar) {
            return null;
        }

        /* renamed from: a */
        public boolean mo10263a(@NonNull Object obj) {
            return false;
        }
    }

    /* renamed from: com.bumptech.glide.load.o.r$b */
    /* compiled from: MultiModelLoaderFactory */
    private static class C1041b<Model, Data> {

        /* renamed from: a */
        private final Class<Model> f1351a;

        /* renamed from: b */
        final Class<Data> f1352b;

        /* renamed from: c */
        final ModelLoaderFactory<? extends Model, ? extends Data> f1353c;

        public C1041b(@NonNull Class<Model> cls, @NonNull Class<Data> cls2, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> oVar) {
            this.f1351a = cls;
            this.f1352b = cls2;
            this.f1353c = oVar;
        }

        /* renamed from: a */
        public boolean mo10318a(@NonNull Class<?> cls, @NonNull Class<?> cls2) {
            return mo10317a(cls) && this.f1352b.isAssignableFrom(cls2);
        }

        /* renamed from: a */
        public boolean mo10317a(@NonNull Class<?> cls) {
            return this.f1351a.isAssignableFrom(cls);
        }
    }

    /* renamed from: com.bumptech.glide.load.o.r$c */
    /* compiled from: MultiModelLoaderFactory */
    static class C1042c {
        C1042c() {
        }

        @NonNull
        /* renamed from: a */
        public <Model, Data> MultiModelLoader<Model, Data> mo10319a(@NonNull List<ModelLoader<Model, Data>> list, @NonNull Pools.Pool<List<Throwable>> pool) {
            return new MultiModelLoader<>(list, pool);
        }
    }

    public MultiModelLoaderFactory(@NonNull Pools.Pool<List<Throwable>> pool) {
        this(pool, f1345e);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized <Model, Data> void mo10315a(@NonNull Class<Model> cls, @NonNull Class<Data> cls2, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> oVar) {
        m1996a(cls, cls2, oVar, true);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: b */
    public synchronized List<Class<?>> mo10316b(@NonNull Class<?> cls) {
        ArrayList arrayList;
        arrayList = new ArrayList();
        for (C1041b bVar : this.f1347a) {
            if (!arrayList.contains(bVar.f1352b) && bVar.mo10317a(cls)) {
                arrayList.add(bVar.f1352b);
            }
        }
        return arrayList;
    }

    @VisibleForTesting
    MultiModelLoaderFactory(@NonNull Pools.Pool<List<Throwable>> pool, @NonNull C1042c cVar) {
        this.f1347a = new ArrayList();
        this.f1349c = new HashSet();
        this.f1350d = pool;
        this.f1348b = cVar;
    }

    /* renamed from: a */
    private <Model, Data> void m1996a(@NonNull Class<Model> cls, @NonNull Class<Data> cls2, @NonNull ModelLoaderFactory<? extends Model, ? extends Data> oVar, boolean z) {
        C1041b bVar = new C1041b(cls, cls2, oVar);
        List<C1041b<?, ?>> list = this.f1347a;
        list.add(z ? list.size() : 0, bVar);
    }

    /* access modifiers changed from: package-private */
    @NonNull
    /* renamed from: a */
    public synchronized <Model> List<ModelLoader<Model, ?>> mo10314a(@NonNull Class cls) {
        ArrayList arrayList;
        try {
            arrayList = new ArrayList();
            for (C1041b bVar : this.f1347a) {
                if (!this.f1349c.contains(bVar)) {
                    if (bVar.mo10317a(cls)) {
                        this.f1349c.add(bVar);
                        arrayList.add(m1995a(bVar));
                        this.f1349c.remove(bVar);
                    }
                }
            }
        } catch (Throwable th) {
            this.f1349c.clear();
            throw th;
        }
        return arrayList;
    }

    @NonNull
    /* renamed from: a */
    public synchronized <Model, Data> ModelLoader<Model, Data> mo10313a(@NonNull Class<Model> cls, @NonNull Class<Data> cls2) {
        try {
            ArrayList arrayList = new ArrayList();
            boolean z = false;
            for (C1041b bVar : this.f1347a) {
                if (this.f1349c.contains(bVar)) {
                    z = true;
                } else if (bVar.mo10318a(cls, cls2)) {
                    this.f1349c.add(bVar);
                    arrayList.add(m1995a(bVar));
                    this.f1349c.remove(bVar);
                }
            }
            if (arrayList.size() > 1) {
                return this.f1348b.mo10319a(arrayList, this.f1350d);
            } else if (arrayList.size() == 1) {
                return (ModelLoader) arrayList.get(0);
            } else if (z) {
                return m1994a();
            } else {
                throw new Registry.C0926c(cls, cls2);
            }
        } catch (Throwable th) {
            this.f1349c.clear();
            throw th;
        }
    }

    @NonNull
    /* renamed from: a */
    private <Model, Data> ModelLoader<Model, Data> m1995a(@NonNull C1041b<?, ?> bVar) {
        ModelLoader<Model, Data> a = bVar.f1353c.mo10265a(this);
        Preconditions.m2828a(a);
        return a;
    }

    @NonNull
    /* renamed from: a */
    private static <Model, Data> ModelLoader<Model, Data> m1994a() {
        return f1346f;
    }
}
