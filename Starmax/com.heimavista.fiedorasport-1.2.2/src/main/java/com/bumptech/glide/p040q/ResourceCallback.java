package com.bumptech.glide.p040q;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.p023n.GlideException;
import com.bumptech.glide.load.p023n.Resource;

/* renamed from: com.bumptech.glide.q.i */
public interface ResourceCallback {
    /* renamed from: a */
    void mo10654a(GlideException qVar);

    /* renamed from: a */
    void mo10655a(Resource<?> vVar, DataSource aVar);
}
