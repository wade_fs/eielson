package com.bumptech.glide.p040q.p042m;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.p040q.p042m.Transition;

/* renamed from: com.bumptech.glide.q.m.a */
public class NoTransition<R> implements Transition<R> {

    /* renamed from: a */
    static final NoTransition<?> f1762a = new NoTransition<>();

    /* renamed from: b */
    private static final TransitionFactory<?> f1763b = new C1116a();

    /* renamed from: com.bumptech.glide.q.m.a$a */
    /* compiled from: NoTransition */
    public static class C1116a<R> implements TransitionFactory<R> {
        /* renamed from: a */
        public Transition<R> mo10670a(DataSource aVar, boolean z) {
            return NoTransition.f1762a;
        }
    }

    /* renamed from: a */
    public static <R> TransitionFactory<R> m2793a() {
        return f1763b;
    }

    /* renamed from: a */
    public boolean mo10669a(Object obj, Transition.C1117a aVar) {
        return false;
    }
}
