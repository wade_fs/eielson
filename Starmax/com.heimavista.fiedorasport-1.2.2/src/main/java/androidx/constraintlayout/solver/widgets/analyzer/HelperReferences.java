package androidx.constraintlayout.solver.widgets.analyzer;

import androidx.constraintlayout.solver.widgets.Barrier;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import androidx.constraintlayout.solver.widgets.analyzer.DependencyNode;

class HelperReferences extends WidgetRun {
    public HelperReferences(ConstraintWidget constraintWidget) {
        super(constraintWidget);
    }

    private void addDependency(DependencyNode dependencyNode) {
        super.start.dependencies.add(dependencyNode);
        dependencyNode.targets.add(super.start);
    }

    /* access modifiers changed from: package-private */
    public void apply() {
        ConstraintWidget constraintWidget = super.widget;
        if (constraintWidget instanceof Barrier) {
            super.start.delegateToWidgetRun = true;
            Barrier barrier = (Barrier) constraintWidget;
            int barrierType = barrier.getBarrierType();
            boolean allowsGoneWidget = barrier.allowsGoneWidget();
            int i = 0;
            if (barrierType == 0) {
                super.start.type = DependencyNode.Type.LEFT;
                while (i < barrier.mWidgetsCount) {
                    ConstraintWidget constraintWidget2 = barrier.mWidgets[i];
                    if (allowsGoneWidget || constraintWidget2.getVisibility() != 8) {
                        DependencyNode dependencyNode = constraintWidget2.horizontalRun.start;
                        dependencyNode.dependencies.add(super.start);
                        super.start.targets.add(dependencyNode);
                    }
                    i++;
                }
                addDependency(super.widget.horizontalRun.start);
                addDependency(super.widget.horizontalRun.end);
            } else if (barrierType == 1) {
                super.start.type = DependencyNode.Type.RIGHT;
                while (i < barrier.mWidgetsCount) {
                    ConstraintWidget constraintWidget3 = barrier.mWidgets[i];
                    if (allowsGoneWidget || constraintWidget3.getVisibility() != 8) {
                        DependencyNode dependencyNode2 = constraintWidget3.horizontalRun.end;
                        dependencyNode2.dependencies.add(super.start);
                        super.start.targets.add(dependencyNode2);
                    }
                    i++;
                }
                addDependency(super.widget.horizontalRun.start);
                addDependency(super.widget.horizontalRun.end);
            } else if (barrierType == 2) {
                super.start.type = DependencyNode.Type.TOP;
                while (i < barrier.mWidgetsCount) {
                    ConstraintWidget constraintWidget4 = barrier.mWidgets[i];
                    if (allowsGoneWidget || constraintWidget4.getVisibility() != 8) {
                        DependencyNode dependencyNode3 = constraintWidget4.verticalRun.start;
                        dependencyNode3.dependencies.add(super.start);
                        super.start.targets.add(dependencyNode3);
                    }
                    i++;
                }
                addDependency(super.widget.verticalRun.start);
                addDependency(super.widget.verticalRun.end);
            } else if (barrierType == 3) {
                super.start.type = DependencyNode.Type.BOTTOM;
                while (i < barrier.mWidgetsCount) {
                    ConstraintWidget constraintWidget5 = barrier.mWidgets[i];
                    if (allowsGoneWidget || constraintWidget5.getVisibility() != 8) {
                        DependencyNode dependencyNode4 = constraintWidget5.verticalRun.end;
                        dependencyNode4.dependencies.add(super.start);
                        super.start.targets.add(dependencyNode4);
                    }
                    i++;
                }
                addDependency(super.widget.verticalRun.start);
                addDependency(super.widget.verticalRun.end);
            }
        }
    }

    public void applyToWidget() {
        ConstraintWidget constraintWidget = super.widget;
        if (constraintWidget instanceof Barrier) {
            int barrierType = ((Barrier) constraintWidget).getBarrierType();
            if (barrierType == 0 || barrierType == 1) {
                super.widget.setX(super.start.value);
            } else {
                super.widget.setY(super.start.value);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        super.runGroup = null;
        super.start.clear();
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        super.start.resolved = false;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsWrapComputation() {
        return false;
    }

    public void update(Dependency dependency) {
        Barrier barrier = (Barrier) super.widget;
        int barrierType = barrier.getBarrierType();
        int i = -1;
        int i2 = 0;
        for (DependencyNode dependencyNode : super.start.targets) {
            int i3 = dependencyNode.value;
            if (i == -1 || i3 < i) {
                i = i3;
            }
            if (i2 < i3) {
                i2 = i3;
            }
        }
        if (barrierType == 0 || barrierType == 2) {
            super.start.resolve(i + barrier.getMargin());
        } else {
            super.start.resolve(i2 + barrier.getMargin());
        }
    }
}
