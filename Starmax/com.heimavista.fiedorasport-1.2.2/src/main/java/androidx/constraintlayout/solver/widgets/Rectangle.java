package androidx.constraintlayout.solver.widgets;

public class Rectangle {
    public int height;
    public int width;

    /* renamed from: x */
    public int f418x;

    /* renamed from: y */
    public int f419y;

    public boolean contains(int i, int i2) {
        int i3;
        int i4 = this.f418x;
        return i >= i4 && i < i4 + this.width && i2 >= (i3 = this.f419y) && i2 < i3 + this.height;
    }

    public int getCenterX() {
        return (this.f418x + this.width) / 2;
    }

    public int getCenterY() {
        return (this.f419y + this.height) / 2;
    }

    /* access modifiers changed from: package-private */
    public void grow(int i, int i2) {
        this.f418x -= i;
        this.f419y -= i2;
        this.width += i * 2;
        this.height += i2 * 2;
    }

    /* access modifiers changed from: package-private */
    public boolean intersects(Rectangle rectangle) {
        int i;
        int i2;
        int i3 = this.f418x;
        int i4 = rectangle.f418x;
        return i3 >= i4 && i3 < i4 + rectangle.width && (i = this.f419y) >= (i2 = rectangle.f419y) && i < i2 + rectangle.height;
    }

    public void setBounds(int i, int i2, int i3, int i4) {
        this.f418x = i;
        this.f419y = i2;
        this.width = i3;
        this.height = i4;
    }
}
