package androidx.constraintlayout.solver.widgets.analyzer;

import androidx.constraintlayout.solver.widgets.ConstraintAnchor;
import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import androidx.constraintlayout.solver.widgets.Helper;
import androidx.constraintlayout.solver.widgets.analyzer.DependencyNode;
import androidx.constraintlayout.solver.widgets.analyzer.WidgetRun;

public class VerticalWidgetRun extends WidgetRun {
    public DependencyNode baseline = new DependencyNode(super);
    DimensionDependency baselineDimension = null;

    /* renamed from: androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun$1 */
    static /* synthetic */ class C03861 {

        /* renamed from: $SwitchMap$androidx$constraintlayout$solver$widgets$analyzer$WidgetRun$RunType */
        static final /* synthetic */ int[] f421xbf6f0c8e = new int[WidgetRun.RunType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                androidx.constraintlayout.solver.widgets.analyzer.WidgetRun$RunType[] r0 = androidx.constraintlayout.solver.widgets.analyzer.WidgetRun.RunType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun.C03861.f421xbf6f0c8e = r0
                int[] r0 = androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun.C03861.f421xbf6f0c8e     // Catch:{ NoSuchFieldError -> 0x0014 }
                androidx.constraintlayout.solver.widgets.analyzer.WidgetRun$RunType r1 = androidx.constraintlayout.solver.widgets.analyzer.WidgetRun.RunType.START     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun.C03861.f421xbf6f0c8e     // Catch:{ NoSuchFieldError -> 0x001f }
                androidx.constraintlayout.solver.widgets.analyzer.WidgetRun$RunType r1 = androidx.constraintlayout.solver.widgets.analyzer.WidgetRun.RunType.END     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun.C03861.f421xbf6f0c8e     // Catch:{ NoSuchFieldError -> 0x002a }
                androidx.constraintlayout.solver.widgets.analyzer.WidgetRun$RunType r1 = androidx.constraintlayout.solver.widgets.analyzer.WidgetRun.RunType.CENTER     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.analyzer.VerticalWidgetRun.C03861.<clinit>():void");
        }
    }

    public VerticalWidgetRun(ConstraintWidget constraintWidget) {
        super(constraintWidget);
        super.start.type = DependencyNode.Type.TOP;
        super.end.type = DependencyNode.Type.BOTTOM;
        this.baseline.type = DependencyNode.Type.BASELINE;
        super.orientation = 1;
    }

    /* access modifiers changed from: package-private */
    public void apply() {
        ConstraintWidget parent;
        ConstraintWidget parent2;
        ConstraintWidget constraintWidget = super.widget;
        if (constraintWidget.measured) {
            super.dimension.resolve(constraintWidget.getHeight());
        }
        if (!super.dimension.resolved) {
            super.dimensionBehavior = super.widget.getVerticalDimensionBehaviour();
            if (super.widget.hasBaseline()) {
                this.baselineDimension = new BaselineDimensionDependency(super);
            }
            ConstraintWidget.DimensionBehaviour dimensionBehaviour = super.dimensionBehavior;
            if (dimensionBehaviour != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                if (dimensionBehaviour == ConstraintWidget.DimensionBehaviour.MATCH_PARENT && (parent2 = super.widget.getParent()) != null && parent2.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED) {
                    int height = (parent2.getHeight() - super.widget.mTop.getMargin()) - super.widget.mBottom.getMargin();
                    addTarget(super.start, parent2.verticalRun.start, super.widget.mTop.getMargin());
                    addTarget(super.end, parent2.verticalRun.end, -super.widget.mBottom.getMargin());
                    super.dimension.resolve(height);
                    return;
                } else if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.FIXED) {
                    super.dimension.resolve(super.widget.getHeight());
                }
            }
        } else if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_PARENT && (parent = super.widget.getParent()) != null && parent.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.FIXED) {
            addTarget(super.start, parent.verticalRun.start, super.widget.mTop.getMargin());
            addTarget(super.end, parent.verticalRun.end, -super.widget.mBottom.getMargin());
            return;
        }
        if (super.dimension.resolved) {
            ConstraintWidget constraintWidget2 = super.widget;
            if (constraintWidget2.measured) {
                ConstraintAnchor[] constraintAnchorArr = constraintWidget2.mListAnchors;
                if (constraintAnchorArr[2].mTarget == null || constraintAnchorArr[3].mTarget == null) {
                    ConstraintWidget constraintWidget3 = super.widget;
                    ConstraintAnchor[] constraintAnchorArr2 = constraintWidget3.mListAnchors;
                    if (constraintAnchorArr2[2].mTarget != null) {
                        DependencyNode target = getTarget(constraintAnchorArr2[2]);
                        if (target != null) {
                            addTarget(super.start, target, super.widget.mListAnchors[2].getMargin());
                            addTarget(super.end, super.start, super.dimension.value);
                            if (super.widget.hasBaseline()) {
                                addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                                return;
                            }
                            return;
                        }
                        return;
                    } else if (constraintAnchorArr2[3].mTarget != null) {
                        DependencyNode target2 = getTarget(constraintAnchorArr2[3]);
                        if (target2 != null) {
                            addTarget(super.end, target2, -super.widget.mListAnchors[3].getMargin());
                            addTarget(super.start, super.end, -super.dimension.value);
                        }
                        if (super.widget.hasBaseline()) {
                            addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                            return;
                        }
                        return;
                    } else if (constraintAnchorArr2[4].mTarget != null) {
                        DependencyNode target3 = getTarget(constraintAnchorArr2[4]);
                        if (target3 != null) {
                            addTarget(this.baseline, target3, 0);
                            addTarget(super.start, this.baseline, -super.widget.getBaselineDistance());
                            addTarget(super.end, super.start, super.dimension.value);
                            return;
                        }
                        return;
                    } else if (!(constraintWidget3 instanceof Helper) && constraintWidget3.getParent() != null && super.widget.getAnchor(ConstraintAnchor.Type.CENTER).mTarget == null) {
                        addTarget(super.start, super.widget.getParent().verticalRun.start, super.widget.getY());
                        addTarget(super.end, super.start, super.dimension.value);
                        if (super.widget.hasBaseline()) {
                            addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                            return;
                        }
                        return;
                    } else {
                        return;
                    }
                } else {
                    if (constraintWidget2.isInVerticalChain()) {
                        super.start.margin = super.widget.mListAnchors[2].getMargin();
                        super.end.margin = -super.widget.mListAnchors[3].getMargin();
                    } else {
                        DependencyNode target4 = getTarget(super.widget.mListAnchors[2]);
                        if (target4 != null) {
                            addTarget(super.start, target4, super.widget.mListAnchors[2].getMargin());
                        }
                        DependencyNode target5 = getTarget(super.widget.mListAnchors[3]);
                        if (target5 != null) {
                            addTarget(super.end, target5, -super.widget.mListAnchors[3].getMargin());
                        }
                        super.start.delegateToWidgetRun = true;
                        super.end.delegateToWidgetRun = true;
                    }
                    if (super.widget.hasBaseline()) {
                        addTarget(this.baseline, super.start, super.widget.getBaselineDistance());
                        return;
                    }
                    return;
                }
            }
        }
        if (super.dimension.resolved || super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            super.dimension.addDependency(this);
        } else {
            ConstraintWidget constraintWidget4 = super.widget;
            int i = constraintWidget4.mMatchConstraintDefaultHeight;
            if (i == 2) {
                ConstraintWidget parent3 = constraintWidget4.getParent();
                if (parent3 != null) {
                    DimensionDependency dimensionDependency = parent3.verticalRun.dimension;
                    super.dimension.targets.add(dimensionDependency);
                    dimensionDependency.dependencies.add(super.dimension);
                    DimensionDependency dimensionDependency2 = super.dimension;
                    dimensionDependency2.delegateToWidgetRun = true;
                    dimensionDependency2.dependencies.add(super.start);
                    super.dimension.dependencies.add(super.end);
                }
            } else if (i == 3 && !constraintWidget4.isInVerticalChain()) {
                ConstraintWidget constraintWidget5 = super.widget;
                if (constraintWidget5.mMatchConstraintDefaultWidth != 3) {
                    DimensionDependency dimensionDependency3 = constraintWidget5.horizontalRun.dimension;
                    super.dimension.targets.add(dimensionDependency3);
                    dimensionDependency3.dependencies.add(super.dimension);
                    DimensionDependency dimensionDependency4 = super.dimension;
                    dimensionDependency4.delegateToWidgetRun = true;
                    dimensionDependency4.dependencies.add(super.start);
                    super.dimension.dependencies.add(super.end);
                }
            }
        }
        ConstraintWidget constraintWidget6 = super.widget;
        ConstraintAnchor[] constraintAnchorArr3 = constraintWidget6.mListAnchors;
        if (constraintAnchorArr3[2].mTarget == null || constraintAnchorArr3[3].mTarget == null) {
            ConstraintWidget constraintWidget7 = super.widget;
            ConstraintAnchor[] constraintAnchorArr4 = constraintWidget7.mListAnchors;
            if (constraintAnchorArr4[2].mTarget != null) {
                DependencyNode target6 = getTarget(constraintAnchorArr4[2]);
                if (target6 != null) {
                    addTarget(super.start, target6, super.widget.mListAnchors[2].getMargin());
                    addTarget(super.end, super.start, 1, super.dimension);
                    if (super.widget.hasBaseline()) {
                        addTarget(this.baseline, super.start, 1, this.baselineDimension);
                    }
                    if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.widget.getDimensionRatio() > 0.0f) {
                        HorizontalWidgetRun horizontalWidgetRun = super.widget.horizontalRun;
                        if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                            super.dimension.dependencies.add(super.dimension);
                            super.dimension.targets.add(super.widget.horizontalRun.dimension);
                            super.dimension.updateDelegate = this;
                        }
                    }
                }
            } else if (constraintAnchorArr4[3].mTarget != null) {
                DependencyNode target7 = getTarget(constraintAnchorArr4[3]);
                if (target7 != null) {
                    addTarget(super.end, target7, -super.widget.mListAnchors[3].getMargin());
                    addTarget(super.start, super.end, -1, super.dimension);
                    if (super.widget.hasBaseline()) {
                        addTarget(this.baseline, super.start, 1, this.baselineDimension);
                    }
                }
            } else if (constraintAnchorArr4[4].mTarget != null) {
                DependencyNode target8 = getTarget(constraintAnchorArr4[4]);
                if (target8 != null) {
                    addTarget(this.baseline, target8, 0);
                    addTarget(super.start, this.baseline, -1, this.baselineDimension);
                    addTarget(super.end, super.start, 1, super.dimension);
                }
            } else if (!(constraintWidget7 instanceof Helper) && constraintWidget7.getParent() != null) {
                addTarget(super.start, super.widget.getParent().verticalRun.start, super.widget.getY());
                addTarget(super.end, super.start, 1, super.dimension);
                if (super.widget.hasBaseline()) {
                    addTarget(this.baseline, super.start, 1, this.baselineDimension);
                }
                if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.widget.getDimensionRatio() > 0.0f) {
                    HorizontalWidgetRun horizontalWidgetRun2 = super.widget.horizontalRun;
                    if (super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        super.dimension.dependencies.add(super.dimension);
                        super.dimension.targets.add(super.widget.horizontalRun.dimension);
                        super.dimension.updateDelegate = this;
                    }
                }
            }
        } else {
            if (constraintWidget6.isInVerticalChain()) {
                super.start.margin = super.widget.mListAnchors[2].getMargin();
                super.end.margin = -super.widget.mListAnchors[3].getMargin();
            } else {
                DependencyNode target9 = getTarget(super.widget.mListAnchors[2]);
                DependencyNode target10 = getTarget(super.widget.mListAnchors[3]);
                target9.addDependency(this);
                target10.addDependency(this);
                super.mRunType = WidgetRun.RunType.CENTER;
            }
            if (super.widget.hasBaseline()) {
                addTarget(this.baseline, super.start, 1, this.baselineDimension);
            }
        }
        if (super.dimension.targets.size() == 0) {
            super.dimension.readyToSolve = true;
        }
    }

    public void applyToWidget() {
        DependencyNode dependencyNode = super.start;
        if (dependencyNode.resolved) {
            super.widget.setY(dependencyNode.value);
        }
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        super.runGroup = null;
        super.start.clear();
        super.end.clear();
        this.baseline.clear();
        super.dimension.clear();
        super.resolved = false;
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        super.resolved = false;
        super.start.resolved = false;
        super.end.resolved = false;
        this.baseline.resolved = false;
        super.dimension.resolved = false;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsWrapComputation() {
        if (super.dimensionBehavior != ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT || super.widget.mMatchConstraintDefaultHeight == 0) {
            return true;
        }
        return false;
    }

    public String toString() {
        return "VerticalRun " + super.widget.getDebugName();
    }

    public void update(Dependency dependency) {
        int i;
        float f;
        float f2;
        float f3;
        int i2 = C03861.f421xbf6f0c8e[super.mRunType.ordinal()];
        if (i2 == 1) {
            updateRunStart(dependency);
        } else if (i2 == 2) {
            updateRunEnd(dependency);
        } else if (i2 == 3) {
            ConstraintWidget constraintWidget = super.widget;
            updateRunCenter(dependency, constraintWidget.mTop, constraintWidget.mBottom, 1);
            return;
        }
        DimensionDependency dimensionDependency = super.dimension;
        if (dimensionDependency.readyToSolve && !dimensionDependency.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
            ConstraintWidget constraintWidget2 = super.widget;
            int i3 = constraintWidget2.mMatchConstraintDefaultHeight;
            if (i3 == 2) {
                ConstraintWidget parent = constraintWidget2.getParent();
                if (parent != null) {
                    DimensionDependency dimensionDependency2 = parent.verticalRun.dimension;
                    if (dimensionDependency2.resolved) {
                        float f4 = super.widget.mMatchConstraintPercentHeight;
                        super.dimension.resolve((int) ((((float) dimensionDependency2.value) * f4) + 0.5f));
                    }
                }
            } else if (i3 == 3 && constraintWidget2.horizontalRun.dimension.resolved) {
                int dimensionRatioSide = constraintWidget2.getDimensionRatioSide();
                if (dimensionRatioSide == -1) {
                    ConstraintWidget constraintWidget3 = super.widget;
                    f3 = (float) constraintWidget3.horizontalRun.dimension.value;
                    f2 = constraintWidget3.getDimensionRatio();
                } else if (dimensionRatioSide == 0) {
                    ConstraintWidget constraintWidget4 = super.widget;
                    f = ((float) constraintWidget4.horizontalRun.dimension.value) * constraintWidget4.getDimensionRatio();
                    i = (int) (f + 0.5f);
                    super.dimension.resolve(i);
                } else if (dimensionRatioSide != 1) {
                    i = 0;
                    super.dimension.resolve(i);
                } else {
                    ConstraintWidget constraintWidget5 = super.widget;
                    f3 = (float) constraintWidget5.horizontalRun.dimension.value;
                    f2 = constraintWidget5.getDimensionRatio();
                }
                f = f3 / f2;
                i = (int) (f + 0.5f);
                super.dimension.resolve(i);
            }
        }
        DependencyNode dependencyNode = super.start;
        if (dependencyNode.readyToSolve) {
            DependencyNode dependencyNode2 = super.end;
            if (dependencyNode2.readyToSolve) {
                if (!dependencyNode.resolved || !dependencyNode2.resolved || !super.dimension.resolved) {
                    if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT) {
                        ConstraintWidget constraintWidget6 = super.widget;
                        if (constraintWidget6.mMatchConstraintDefaultWidth == 0 && !constraintWidget6.isInVerticalChain()) {
                            int i4 = super.start.targets.get(0).value;
                            DependencyNode dependencyNode3 = super.start;
                            int i5 = i4 + dependencyNode3.margin;
                            int i6 = super.end.targets.get(0).value + super.end.margin;
                            dependencyNode3.resolve(i5);
                            super.end.resolve(i6);
                            super.dimension.resolve(i6 - i5);
                            return;
                        }
                    }
                    if (!super.dimension.resolved && super.dimensionBehavior == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT && super.matchConstraintsType == 1 && super.start.targets.size() > 0 && super.end.targets.size() > 0) {
                        int i7 = (super.end.targets.get(0).value + super.end.margin) - (super.start.targets.get(0).value + super.start.margin);
                        DimensionDependency dimensionDependency3 = super.dimension;
                        int i8 = dimensionDependency3.wrapValue;
                        if (i7 < i8) {
                            dimensionDependency3.resolve(i7);
                        } else {
                            dimensionDependency3.resolve(i8);
                        }
                    }
                    if (super.dimension.resolved && super.start.targets.size() > 0 && super.end.targets.size() > 0) {
                        DependencyNode dependencyNode4 = super.start.targets.get(0);
                        DependencyNode dependencyNode5 = super.end.targets.get(0);
                        int i9 = dependencyNode4.value + super.start.margin;
                        int i10 = dependencyNode5.value + super.end.margin;
                        float verticalBiasPercent = super.widget.getVerticalBiasPercent();
                        if (dependencyNode4 == dependencyNode5) {
                            i9 = dependencyNode4.value;
                            i10 = dependencyNode5.value;
                            verticalBiasPercent = 0.5f;
                        }
                        super.start.resolve((int) (((float) i9) + 0.5f + (((float) ((i10 - i9) - super.dimension.value)) * verticalBiasPercent)));
                        super.end.resolve(super.start.value + super.dimension.value);
                    }
                }
            }
        }
    }
}
