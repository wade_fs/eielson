package androidx.constraintlayout.solver.widgets.analyzer;

import androidx.constraintlayout.solver.widgets.analyzer.DependencyNode;

class DimensionDependency extends DependencyNode {
    public int wrapValue;

    public DimensionDependency(WidgetRun widgetRun) {
        super(widgetRun);
        if (widgetRun instanceof HorizontalWidgetRun) {
            super.type = DependencyNode.Type.HORIZONTAL_DIMENSION;
        } else {
            super.type = DependencyNode.Type.VERTICAL_DIMENSION;
        }
    }

    public void resolve(int i) {
        if (!super.resolved) {
            super.resolved = true;
            super.value = i;
            for (Dependency dependency : super.dependencies) {
                dependency.update(dependency);
            }
        }
    }
}
