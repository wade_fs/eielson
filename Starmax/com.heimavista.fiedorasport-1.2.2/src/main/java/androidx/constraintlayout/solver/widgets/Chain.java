package androidx.constraintlayout.solver.widgets;

import androidx.constraintlayout.solver.LinearSystem;

class Chain {
    private static final boolean DEBUG = false;

    Chain() {
    }

    static void applyChainConstraints(ConstraintWidgetContainer constraintWidgetContainer, LinearSystem linearSystem, int i) {
        int i2;
        ChainHead[] chainHeadArr;
        int i3;
        if (i == 0) {
            int i4 = constraintWidgetContainer.mHorizontalChainsSize;
            chainHeadArr = constraintWidgetContainer.mHorizontalChainsArray;
            i2 = i4;
            i3 = 0;
        } else {
            i3 = 2;
            int i5 = constraintWidgetContainer.mVerticalChainsSize;
            i2 = i5;
            chainHeadArr = constraintWidgetContainer.mVerticalChainsArray;
        }
        for (int i6 = 0; i6 < i2; i6++) {
            ChainHead chainHead = chainHeadArr[i6];
            chainHead.define();
            applyChainConstraints(constraintWidgetContainer, linearSystem, i, i3, chainHead);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0035, code lost:
        if (r2.mHorizontalChainStyle == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0048, code lost:
        if (r2.mVerticalChainStyle == 2) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x004c, code lost:
        r5 = false;
     */
    /* JADX WARNING: Removed duplicated region for block: B:216:0x03c3  */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x03c4 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void applyChainConstraints(androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer r37, androidx.constraintlayout.solver.LinearSystem r38, int r39, int r40, androidx.constraintlayout.solver.widgets.ChainHead r41) {
        /*
            r0 = r37
            r9 = r38
            r1 = r41
            androidx.constraintlayout.solver.widgets.ConstraintWidget r10 = r1.mFirst
            androidx.constraintlayout.solver.widgets.ConstraintWidget r11 = r1.mLast
            androidx.constraintlayout.solver.widgets.ConstraintWidget r12 = r1.mFirstVisibleWidget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r13 = r1.mLastVisibleWidget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r1.mHead
            float r3 = r1.mTotalWeight
            androidx.constraintlayout.solver.widgets.ConstraintWidget r4 = r1.mFirstMatchConstraintWidget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r4 = r1.mLastMatchConstraintWidget
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r4 = r0.mListDimensionBehaviors
            r4 = r4[r39]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r5 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.WRAP_CONTENT
            r7 = 1
            if (r4 != r5) goto L_0x0021
            r4 = 1
            goto L_0x0022
        L_0x0021:
            r4 = 0
        L_0x0022:
            r5 = 2
            if (r39 != 0) goto L_0x0038
            int r8 = r2.mHorizontalChainStyle
            if (r8 != 0) goto L_0x002b
            r8 = 1
            goto L_0x002c
        L_0x002b:
            r8 = 0
        L_0x002c:
            int r14 = r2.mHorizontalChainStyle
            if (r14 != r7) goto L_0x0032
            r14 = 1
            goto L_0x0033
        L_0x0032:
            r14 = 0
        L_0x0033:
            int r15 = r2.mHorizontalChainStyle
            if (r15 != r5) goto L_0x004c
            goto L_0x004a
        L_0x0038:
            int r8 = r2.mVerticalChainStyle
            if (r8 != 0) goto L_0x003e
            r8 = 1
            goto L_0x003f
        L_0x003e:
            r8 = 0
        L_0x003f:
            int r14 = r2.mVerticalChainStyle
            if (r14 != r7) goto L_0x0045
            r14 = 1
            goto L_0x0046
        L_0x0045:
            r14 = 0
        L_0x0046:
            int r15 = r2.mVerticalChainStyle
            if (r15 != r5) goto L_0x004c
        L_0x004a:
            r5 = 1
            goto L_0x004d
        L_0x004c:
            r5 = 0
        L_0x004d:
            r15 = r8
            r8 = r10
            r16 = r14
            r14 = r5
            r5 = 0
        L_0x0053:
            r21 = 0
            if (r5 != 0) goto L_0x0141
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r7 = r8.mListAnchors
            r7 = r7[r40]
            if (r4 != 0) goto L_0x0063
            if (r14 == 0) goto L_0x0060
            goto L_0x0063
        L_0x0060:
            r23 = 4
            goto L_0x0065
        L_0x0063:
            r23 = 1
        L_0x0065:
            int r24 = r7.getMargin()
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r6 = r8.mListDimensionBehaviors
            r6 = r6[r39]
            r26 = r3
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r6 != r3) goto L_0x007b
            int[] r3 = r8.mResolvedMatchConstraintDefault
            r3 = r3[r39]
            if (r3 != 0) goto L_0x007b
            r3 = 1
            goto L_0x007c
        L_0x007b:
            r3 = 0
        L_0x007c:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r6 = r7.mTarget
            if (r6 == 0) goto L_0x0088
            if (r8 == r10) goto L_0x0088
            int r6 = r6.getMargin()
            int r24 = r24 + r6
        L_0x0088:
            r6 = r24
            if (r14 == 0) goto L_0x0095
            if (r8 == r10) goto L_0x0095
            if (r8 == r12) goto L_0x0095
            r23 = r5
            r22 = 6
            goto L_0x00a2
        L_0x0095:
            if (r15 == 0) goto L_0x009e
            if (r4 == 0) goto L_0x009e
            r23 = r5
            r22 = 4
            goto L_0x00a2
        L_0x009e:
            r22 = r23
            r23 = r5
        L_0x00a2:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r7.mTarget
            if (r5 == 0) goto L_0x00d9
            if (r8 != r12) goto L_0x00b5
            r24 = r15
            androidx.constraintlayout.solver.SolverVariable r15 = r7.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r5 = r5.mSolverVariable
            r27 = r2
            r2 = 5
            r9.addGreaterThan(r15, r5, r6, r2)
            goto L_0x00c1
        L_0x00b5:
            r27 = r2
            r24 = r15
            androidx.constraintlayout.solver.SolverVariable r2 = r7.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r5 = r5.mSolverVariable
            r15 = 6
            r9.addGreaterThan(r2, r5, r6, r15)
        L_0x00c1:
            if (r3 == 0) goto L_0x00cd
            if (r14 != 0) goto L_0x00cd
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r7.mTarget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r2.mOwner
            if (r2 != r0) goto L_0x00cd
            r2 = 5
            goto L_0x00cf
        L_0x00cd:
            r2 = r22
        L_0x00cf:
            androidx.constraintlayout.solver.SolverVariable r3 = r7.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r7.mTarget
            androidx.constraintlayout.solver.SolverVariable r5 = r5.mSolverVariable
            r9.addEquality(r3, r5, r6, r2)
            goto L_0x00dd
        L_0x00d9:
            r27 = r2
            r24 = r15
        L_0x00dd:
            if (r4 == 0) goto L_0x0112
            int r2 = r8.getVisibility()
            r3 = 8
            if (r2 == r3) goto L_0x0101
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r2 = r8.mListDimensionBehaviors
            r2 = r2[r39]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r3 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r2 != r3) goto L_0x0101
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r8.mListAnchors
            int r3 = r40 + 1
            r3 = r2[r3]
            androidx.constraintlayout.solver.SolverVariable r3 = r3.mSolverVariable
            r2 = r2[r40]
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
            r5 = 5
            r6 = 0
            r9.addGreaterThan(r3, r2, r6, r5)
            goto L_0x0102
        L_0x0101:
            r6 = 0
        L_0x0102:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r8.mListAnchors
            r2 = r2[r40]
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r3 = r0.mListAnchors
            r3 = r3[r40]
            androidx.constraintlayout.solver.SolverVariable r3 = r3.mSolverVariable
            r5 = 6
            r9.addGreaterThan(r2, r3, r6, r5)
        L_0x0112:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r8.mListAnchors
            int r3 = r40 + 1
            r2 = r2[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            if (r2 == 0) goto L_0x0131
            androidx.constraintlayout.solver.widgets.ConstraintWidget r2 = r2.mOwner
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r3 = r2.mListAnchors
            r5 = r3[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r5.mTarget
            if (r5 == 0) goto L_0x0131
            r3 = r3[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r3 = r3.mOwner
            if (r3 == r8) goto L_0x012f
            goto L_0x0131
        L_0x012f:
            r21 = r2
        L_0x0131:
            if (r21 == 0) goto L_0x0138
            r8 = r21
            r5 = r23
            goto L_0x0139
        L_0x0138:
            r5 = 1
        L_0x0139:
            r15 = r24
            r3 = r26
            r2 = r27
            goto L_0x0053
        L_0x0141:
            r27 = r2
            r26 = r3
            r24 = r15
            if (r13 == 0) goto L_0x01a8
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r11.mListAnchors
            int r3 = r40 + 1
            r2 = r2[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            if (r2 == 0) goto L_0x01a8
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r13.mListAnchors
            r2 = r2[r3]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour[] r5 = r13.mListDimensionBehaviors
            r5 = r5[r39]
            androidx.constraintlayout.solver.widgets.ConstraintWidget$DimensionBehaviour r6 = androidx.constraintlayout.solver.widgets.ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT
            if (r5 != r6) goto L_0x0167
            int[] r5 = r13.mResolvedMatchConstraintDefault
            r5 = r5[r39]
            if (r5 != 0) goto L_0x0167
            r5 = 1
            goto L_0x0168
        L_0x0167:
            r5 = 0
        L_0x0168:
            if (r5 == 0) goto L_0x0180
            if (r14 != 0) goto L_0x0180
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r2.mTarget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r6 = r5.mOwner
            if (r6 != r0) goto L_0x0180
            androidx.constraintlayout.solver.SolverVariable r6 = r2.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r5 = r5.mSolverVariable
            int r7 = r2.getMargin()
            int r7 = -r7
            r8 = 5
            r9.addEquality(r6, r5, r7, r8)
            goto L_0x0195
        L_0x0180:
            if (r14 == 0) goto L_0x0195
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r2.mTarget
            androidx.constraintlayout.solver.widgets.ConstraintWidget r6 = r5.mOwner
            if (r6 != r0) goto L_0x0195
            androidx.constraintlayout.solver.SolverVariable r6 = r2.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r5 = r5.mSolverVariable
            int r7 = r2.getMargin()
            int r7 = -r7
            r8 = 4
            r9.addEquality(r6, r5, r7, r8)
        L_0x0195:
            androidx.constraintlayout.solver.SolverVariable r5 = r2.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r6 = r11.mListAnchors
            r3 = r6[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            androidx.constraintlayout.solver.SolverVariable r3 = r3.mSolverVariable
            int r2 = r2.getMargin()
            int r2 = -r2
            r6 = 5
            r9.addLowerThan(r5, r3, r2, r6)
        L_0x01a8:
            if (r4 == 0) goto L_0x01c2
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r0.mListAnchors
            int r2 = r40 + 1
            r0 = r0[r2]
            androidx.constraintlayout.solver.SolverVariable r0 = r0.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r3 = r11.mListAnchors
            r4 = r3[r2]
            androidx.constraintlayout.solver.SolverVariable r4 = r4.mSolverVariable
            r2 = r3[r2]
            int r2 = r2.getMargin()
            r3 = 6
            r9.addGreaterThan(r0, r4, r2, r3)
        L_0x01c2:
            java.util.ArrayList<androidx.constraintlayout.solver.widgets.ConstraintWidget> r0 = r1.mWeightedMatchConstraintsWidgets
            if (r0 == 0) goto L_0x0273
            int r2 = r0.size()
            r3 = 1
            if (r2 <= r3) goto L_0x0273
            boolean r4 = r1.mHasUndefinedWeights
            if (r4 == 0) goto L_0x01d9
            boolean r4 = r1.mHasComplexMatchWeights
            if (r4 != 0) goto L_0x01d9
            int r4 = r1.mWidgetsMatchCount
            float r4 = (float) r4
            goto L_0x01db
        L_0x01d9:
            r4 = r26
        L_0x01db:
            r5 = 0
            r7 = r21
            r6 = 0
            r29 = 0
        L_0x01e1:
            if (r6 >= r2) goto L_0x0273
            java.lang.Object r8 = r0.get(r6)
            androidx.constraintlayout.solver.widgets.ConstraintWidget r8 = (androidx.constraintlayout.solver.widgets.ConstraintWidget) r8
            float[] r15 = r8.mWeight
            r15 = r15[r39]
            int r17 = (r15 > r5 ? 1 : (r15 == r5 ? 0 : -1))
            if (r17 >= 0) goto L_0x020f
            boolean r15 = r1.mHasComplexMatchWeights
            if (r15 == 0) goto L_0x0208
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r8.mListAnchors
            int r15 = r40 + 1
            r15 = r8[r15]
            androidx.constraintlayout.solver.SolverVariable r15 = r15.mSolverVariable
            r8 = r8[r40]
            androidx.constraintlayout.solver.SolverVariable r8 = r8.mSolverVariable
            r3 = 0
            r5 = 4
            r9.addEquality(r15, r8, r3, r5)
            r5 = 6
            goto L_0x0226
        L_0x0208:
            r5 = 4
            r3 = 1065353216(0x3f800000, float:1.0)
            r3 = 0
            r15 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0211
        L_0x020f:
            r5 = 4
            r3 = 0
        L_0x0211:
            int r22 = (r15 > r3 ? 1 : (r15 == r3 ? 0 : -1))
            if (r22 != 0) goto L_0x022b
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r8.mListAnchors
            int r15 = r40 + 1
            r15 = r8[r15]
            androidx.constraintlayout.solver.SolverVariable r15 = r15.mSolverVariable
            r8 = r8[r40]
            androidx.constraintlayout.solver.SolverVariable r8 = r8.mSolverVariable
            r3 = 0
            r5 = 6
            r9.addEquality(r15, r8, r3, r5)
        L_0x0226:
            r25 = r0
            r20 = r2
            goto L_0x0269
        L_0x022b:
            r3 = 0
            r5 = 6
            if (r7 == 0) goto L_0x0262
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r7 = r7.mListAnchors
            r3 = r7[r40]
            androidx.constraintlayout.solver.SolverVariable r3 = r3.mSolverVariable
            int r20 = r40 + 1
            r7 = r7[r20]
            androidx.constraintlayout.solver.SolverVariable r7 = r7.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r8.mListAnchors
            r25 = r0
            r0 = r5[r40]
            androidx.constraintlayout.solver.SolverVariable r0 = r0.mSolverVariable
            r5 = r5[r20]
            androidx.constraintlayout.solver.SolverVariable r5 = r5.mSolverVariable
            r20 = r2
            androidx.constraintlayout.solver.ArrayRow r2 = r38.createRow()
            r28 = r2
            r30 = r4
            r31 = r15
            r32 = r3
            r33 = r7
            r34 = r0
            r35 = r5
            r28.createRowEqualMatchDimensions(r29, r30, r31, r32, r33, r34, r35)
            r9.addConstraint(r2)
            goto L_0x0266
        L_0x0262:
            r25 = r0
            r20 = r2
        L_0x0266:
            r7 = r8
            r29 = r15
        L_0x0269:
            int r6 = r6 + 1
            r2 = r20
            r0 = r25
            r3 = 1
            r5 = 0
            goto L_0x01e1
        L_0x0273:
            if (r12 == 0) goto L_0x02d5
            if (r12 == r13) goto L_0x0279
            if (r14 == 0) goto L_0x02d5
        L_0x0279:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r10.mListAnchors
            r1 = r0[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r11.mListAnchors
            int r3 = r40 + 1
            r2 = r2[r3]
            r4 = r0[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r4 = r4.mTarget
            if (r4 == 0) goto L_0x0291
            r0 = r0[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r0.mTarget
            androidx.constraintlayout.solver.SolverVariable r0 = r0.mSolverVariable
            r4 = r0
            goto L_0x0293
        L_0x0291:
            r4 = r21
        L_0x0293:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r11.mListAnchors
            r5 = r0[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r5.mTarget
            if (r5 == 0) goto L_0x02a3
            r0 = r0[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r0 = r0.mTarget
            androidx.constraintlayout.solver.SolverVariable r0 = r0.mSolverVariable
            r5 = r0
            goto L_0x02a5
        L_0x02a3:
            r5 = r21
        L_0x02a5:
            if (r12 != r13) goto L_0x02ad
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r1 = r0[r40]
            r2 = r0[r3]
        L_0x02ad:
            if (r4 == 0) goto L_0x04f3
            if (r5 == 0) goto L_0x04f3
            if (r39 != 0) goto L_0x02b8
            r0 = r27
            float r0 = r0.mHorizontalBiasPercent
            goto L_0x02bc
        L_0x02b8:
            r0 = r27
            float r0 = r0.mVerticalBiasPercent
        L_0x02bc:
            r6 = r0
            int r3 = r1.getMargin()
            int r7 = r2.getMargin()
            androidx.constraintlayout.solver.SolverVariable r1 = r1.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r8 = r2.mSolverVariable
            r10 = 5
            r0 = r38
            r2 = r4
            r4 = r6
            r6 = r8
            r8 = r10
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x04f3
        L_0x02d5:
            if (r24 == 0) goto L_0x03c8
            if (r12 == 0) goto L_0x03c8
            int r0 = r1.mWidgetsMatchCount
            if (r0 <= 0) goto L_0x02e4
            int r1 = r1.mWidgetsCount
            if (r1 != r0) goto L_0x02e4
            r17 = 1
            goto L_0x02e6
        L_0x02e4:
            r17 = 0
        L_0x02e6:
            r14 = r12
            r15 = r14
        L_0x02e8:
            if (r14 == 0) goto L_0x04f3
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r0 = r14.mNextChainWidget
            r0 = r0[r39]
            r8 = r0
        L_0x02ef:
            if (r8 == 0) goto L_0x02fe
            int r0 = r8.getVisibility()
            r7 = 8
            if (r0 != r7) goto L_0x0300
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r0 = r8.mNextChainWidget
            r8 = r0[r39]
            goto L_0x02ef
        L_0x02fe:
            r7 = 8
        L_0x0300:
            if (r8 != 0) goto L_0x030b
            if (r14 != r13) goto L_0x0305
            goto L_0x030b
        L_0x0305:
            r18 = r8
            r19 = 6
            goto L_0x03bb
        L_0x030b:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r14.mListAnchors
            r0 = r0[r40]
            androidx.constraintlayout.solver.SolverVariable r1 = r0.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r0.mTarget
            if (r2 == 0) goto L_0x0318
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
            goto L_0x031a
        L_0x0318:
            r2 = r21
        L_0x031a:
            if (r15 == r14) goto L_0x0325
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r15.mListAnchors
            int r3 = r40 + 1
            r2 = r2[r3]
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
            goto L_0x033a
        L_0x0325:
            if (r14 != r12) goto L_0x033a
            if (r15 != r14) goto L_0x033a
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r10.mListAnchors
            r3 = r2[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r3.mTarget
            if (r3 == 0) goto L_0x0338
            r2 = r2[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r2.mTarget
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
            goto L_0x033a
        L_0x0338:
            r2 = r21
        L_0x033a:
            int r0 = r0.getMargin()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r3 = r14.mListAnchors
            int r4 = r40 + 1
            r3 = r3[r4]
            int r3 = r3.getMargin()
            if (r8 == 0) goto L_0x0357
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r8.mListAnchors
            r5 = r5[r40]
            androidx.constraintlayout.solver.SolverVariable r6 = r5.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r7 = r14.mListAnchors
            r7 = r7[r4]
            androidx.constraintlayout.solver.SolverVariable r7 = r7.mSolverVariable
            goto L_0x036a
        L_0x0357:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r11.mListAnchors
            r5 = r5[r4]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r5 = r5.mTarget
            if (r5 == 0) goto L_0x0362
            androidx.constraintlayout.solver.SolverVariable r6 = r5.mSolverVariable
            goto L_0x0364
        L_0x0362:
            r6 = r21
        L_0x0364:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r7 = r14.mListAnchors
            r7 = r7[r4]
            androidx.constraintlayout.solver.SolverVariable r7 = r7.mSolverVariable
        L_0x036a:
            if (r5 == 0) goto L_0x0371
            int r5 = r5.getMargin()
            int r3 = r3 + r5
        L_0x0371:
            if (r15 == 0) goto L_0x037c
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r15.mListAnchors
            r5 = r5[r4]
            int r5 = r5.getMargin()
            int r0 = r0 + r5
        L_0x037c:
            if (r1 == 0) goto L_0x0305
            if (r2 == 0) goto L_0x0305
            if (r6 == 0) goto L_0x0305
            if (r7 == 0) goto L_0x0305
            if (r14 != r12) goto L_0x038e
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r40]
            int r0 = r0.getMargin()
        L_0x038e:
            r5 = r0
            if (r14 != r13) goto L_0x039c
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r13.mListAnchors
            r0 = r0[r4]
            int r0 = r0.getMargin()
            r18 = r0
            goto L_0x039e
        L_0x039c:
            r18 = r3
        L_0x039e:
            if (r17 == 0) goto L_0x03a3
            r20 = 6
            goto L_0x03a5
        L_0x03a3:
            r20 = 4
        L_0x03a5:
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r38
            r3 = r5
            r22 = 6
            r23 = 4
            r5 = r6
            r6 = r7
            r19 = 6
            r7 = r18
            r18 = r8
            r8 = r20
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x03bb:
            int r0 = r14.getVisibility()
            r8 = 8
            if (r0 == r8) goto L_0x03c4
            r15 = r14
        L_0x03c4:
            r14 = r18
            goto L_0x02e8
        L_0x03c8:
            r8 = 8
            r19 = 6
            if (r16 == 0) goto L_0x04f3
            if (r12 == 0) goto L_0x04f3
            int r0 = r1.mWidgetsMatchCount
            if (r0 <= 0) goto L_0x03db
            int r1 = r1.mWidgetsCount
            if (r1 != r0) goto L_0x03db
            r17 = 1
            goto L_0x03dd
        L_0x03db:
            r17 = 0
        L_0x03dd:
            r14 = r12
            r15 = r14
        L_0x03df:
            if (r14 == 0) goto L_0x0495
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r0 = r14.mNextChainWidget
            r0 = r0[r39]
        L_0x03e5:
            if (r0 == 0) goto L_0x03f2
            int r1 = r0.getVisibility()
            if (r1 != r8) goto L_0x03f2
            androidx.constraintlayout.solver.widgets.ConstraintWidget[] r0 = r0.mNextChainWidget
            r0 = r0[r39]
            goto L_0x03e5
        L_0x03f2:
            if (r14 == r12) goto L_0x0482
            if (r14 == r13) goto L_0x0482
            if (r0 == 0) goto L_0x0482
            if (r0 != r13) goto L_0x03fd
            r7 = r21
            goto L_0x03fe
        L_0x03fd:
            r7 = r0
        L_0x03fe:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r14.mListAnchors
            r0 = r0[r40]
            androidx.constraintlayout.solver.SolverVariable r1 = r0.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r2 = r0.mTarget
            if (r2 == 0) goto L_0x040a
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
        L_0x040a:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r15.mListAnchors
            int r3 = r40 + 1
            r2 = r2[r3]
            androidx.constraintlayout.solver.SolverVariable r2 = r2.mSolverVariable
            int r0 = r0.getMargin()
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r4 = r14.mListAnchors
            r4 = r4[r3]
            int r4 = r4.getMargin()
            if (r7 == 0) goto L_0x0430
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r7.mListAnchors
            r5 = r5[r40]
            androidx.constraintlayout.solver.SolverVariable r6 = r5.mSolverVariable
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r8 = r5.mTarget
            if (r8 == 0) goto L_0x042d
            androidx.constraintlayout.solver.SolverVariable r8 = r8.mSolverVariable
            goto L_0x0441
        L_0x042d:
            r8 = r21
            goto L_0x0441
        L_0x0430:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r5 = r13.mListAnchors
            r5 = r5[r40]
            if (r5 == 0) goto L_0x0439
            androidx.constraintlayout.solver.SolverVariable r6 = r5.mSolverVariable
            goto L_0x043b
        L_0x0439:
            r6 = r21
        L_0x043b:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r8 = r14.mListAnchors
            r8 = r8[r3]
            androidx.constraintlayout.solver.SolverVariable r8 = r8.mSolverVariable
        L_0x0441:
            if (r5 == 0) goto L_0x0448
            int r5 = r5.getMargin()
            int r4 = r4 + r5
        L_0x0448:
            r18 = r4
            if (r15 == 0) goto L_0x0455
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r4 = r15.mListAnchors
            r3 = r4[r3]
            int r3 = r3.getMargin()
            int r0 = r0 + r3
        L_0x0455:
            r3 = r0
            if (r17 == 0) goto L_0x045b
            r20 = 6
            goto L_0x045d
        L_0x045b:
            r20 = 4
        L_0x045d:
            if (r1 == 0) goto L_0x0479
            if (r2 == 0) goto L_0x0479
            if (r6 == 0) goto L_0x0479
            if (r8 == 0) goto L_0x0479
            r4 = 1056964608(0x3f000000, float:0.5)
            r0 = r38
            r5 = r6
            r6 = r8
            r22 = r7
            r7 = r18
            r18 = r15
            r15 = 8
            r8 = r20
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x047f
        L_0x0479:
            r22 = r7
            r18 = r15
            r15 = 8
        L_0x047f:
            r0 = r22
            goto L_0x0486
        L_0x0482:
            r18 = r15
            r15 = 8
        L_0x0486:
            int r1 = r14.getVisibility()
            if (r1 == r15) goto L_0x048d
            goto L_0x048f
        L_0x048d:
            r14 = r18
        L_0x048f:
            r15 = r14
            r8 = 8
            r14 = r0
            goto L_0x03df
        L_0x0495:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r1 = r10.mListAnchors
            r1 = r1[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r1 = r1.mTarget
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r13.mListAnchors
            int r3 = r40 + 1
            r10 = r2[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r2 = r11.mListAnchors
            r2 = r2[r3]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r14 = r2.mTarget
            if (r1 == 0) goto L_0x04e2
            if (r12 == r13) goto L_0x04bc
            androidx.constraintlayout.solver.SolverVariable r2 = r0.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r1 = r1.mSolverVariable
            int r0 = r0.getMargin()
            r15 = 4
            r9.addEquality(r2, r1, r0, r15)
            goto L_0x04e3
        L_0x04bc:
            r15 = 4
            if (r14 == 0) goto L_0x04e3
            androidx.constraintlayout.solver.SolverVariable r2 = r0.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r3 = r1.mSolverVariable
            int r4 = r0.getMargin()
            r5 = 1056964608(0x3f000000, float:0.5)
            androidx.constraintlayout.solver.SolverVariable r6 = r10.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r7 = r14.mSolverVariable
            int r8 = r10.getMargin()
            r17 = 4
            r0 = r38
            r1 = r2
            r2 = r3
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r17
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
            goto L_0x04e3
        L_0x04e2:
            r15 = 4
        L_0x04e3:
            if (r14 == 0) goto L_0x04f3
            if (r12 == r13) goto L_0x04f3
            androidx.constraintlayout.solver.SolverVariable r0 = r10.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r1 = r14.mSolverVariable
            int r2 = r10.getMargin()
            int r2 = -r2
            r9.addEquality(r0, r1, r2, r15)
        L_0x04f3:
            if (r24 != 0) goto L_0x04f7
            if (r16 == 0) goto L_0x0557
        L_0x04f7:
            if (r12 == 0) goto L_0x0557
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r0 = r0[r40]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r1 = r13.mListAnchors
            int r2 = r40 + 1
            r1 = r1[r2]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r3 = r0.mTarget
            if (r3 == 0) goto L_0x050a
            androidx.constraintlayout.solver.SolverVariable r3 = r3.mSolverVariable
            goto L_0x050c
        L_0x050a:
            r3 = r21
        L_0x050c:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r4 = r1.mTarget
            if (r4 == 0) goto L_0x0513
            androidx.constraintlayout.solver.SolverVariable r4 = r4.mSolverVariable
            goto L_0x0515
        L_0x0513:
            r4 = r21
        L_0x0515:
            if (r11 == r13) goto L_0x0524
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r4 = r11.mListAnchors
            r4 = r4[r2]
            androidx.constraintlayout.solver.widgets.ConstraintAnchor r4 = r4.mTarget
            if (r4 == 0) goto L_0x0522
            androidx.constraintlayout.solver.SolverVariable r4 = r4.mSolverVariable
            goto L_0x0524
        L_0x0522:
            r4 = r21
        L_0x0524:
            r5 = r4
            if (r12 != r13) goto L_0x0532
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r0 = r12.mListAnchors
            r1 = r0[r40]
            r0 = r0[r2]
            r36 = r1
            r1 = r0
            r0 = r36
        L_0x0532:
            if (r3 == 0) goto L_0x0557
            if (r5 == 0) goto L_0x0557
            r4 = 1056964608(0x3f000000, float:0.5)
            int r6 = r0.getMargin()
            if (r13 != 0) goto L_0x053f
            goto L_0x0540
        L_0x053f:
            r11 = r13
        L_0x0540:
            androidx.constraintlayout.solver.widgets.ConstraintAnchor[] r7 = r11.mListAnchors
            r2 = r7[r2]
            int r7 = r2.getMargin()
            androidx.constraintlayout.solver.SolverVariable r2 = r0.mSolverVariable
            androidx.constraintlayout.solver.SolverVariable r8 = r1.mSolverVariable
            r10 = 5
            r0 = r38
            r1 = r2
            r2 = r3
            r3 = r6
            r6 = r8
            r8 = r10
            r0.addCentering(r1, r2, r3, r4, r5, r6, r7, r8)
        L_0x0557:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.constraintlayout.solver.widgets.Chain.applyChainConstraints(androidx.constraintlayout.solver.widgets.ConstraintWidgetContainer, androidx.constraintlayout.solver.LinearSystem, int, int, androidx.constraintlayout.solver.widgets.ChainHead):void");
    }
}
