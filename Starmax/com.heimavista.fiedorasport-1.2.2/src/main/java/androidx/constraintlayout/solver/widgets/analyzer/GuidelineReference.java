package androidx.constraintlayout.solver.widgets.analyzer;

import androidx.constraintlayout.solver.widgets.ConstraintWidget;
import androidx.constraintlayout.solver.widgets.Guideline;

class GuidelineReference extends WidgetRun {
    public GuidelineReference(ConstraintWidget constraintWidget) {
        super(constraintWidget);
        constraintWidget.horizontalRun.clear();
        constraintWidget.verticalRun.clear();
        super.orientation = ((Guideline) constraintWidget).getOrientation();
    }

    private void addDependency(DependencyNode dependencyNode) {
        super.start.dependencies.add(dependencyNode);
        dependencyNode.targets.add(super.start);
    }

    /* access modifiers changed from: package-private */
    public void apply() {
        Guideline guideline = (Guideline) super.widget;
        int relativeBegin = guideline.getRelativeBegin();
        int relativeEnd = guideline.getRelativeEnd();
        guideline.getRelativePercent();
        if (guideline.getOrientation() == 1) {
            if (relativeBegin != -1) {
                super.start.targets.add(super.widget.mParent.horizontalRun.start);
                super.widget.mParent.horizontalRun.start.dependencies.add(super.start);
                super.start.margin = relativeBegin;
            } else if (relativeEnd != -1) {
                super.start.targets.add(super.widget.mParent.horizontalRun.end);
                super.widget.mParent.horizontalRun.end.dependencies.add(super.start);
                super.start.margin = -relativeEnd;
            } else {
                DependencyNode dependencyNode = super.start;
                dependencyNode.delegateToWidgetRun = true;
                dependencyNode.targets.add(super.widget.mParent.horizontalRun.end);
                super.widget.mParent.horizontalRun.end.dependencies.add(super.start);
            }
            addDependency(super.widget.horizontalRun.start);
            addDependency(super.widget.horizontalRun.end);
            return;
        }
        if (relativeBegin != -1) {
            super.start.targets.add(super.widget.mParent.verticalRun.start);
            super.widget.mParent.verticalRun.start.dependencies.add(super.start);
            super.start.margin = relativeBegin;
        } else if (relativeEnd != -1) {
            super.start.targets.add(super.widget.mParent.verticalRun.end);
            super.widget.mParent.verticalRun.end.dependencies.add(super.start);
            super.start.margin = -relativeEnd;
        } else {
            DependencyNode dependencyNode2 = super.start;
            dependencyNode2.delegateToWidgetRun = true;
            dependencyNode2.targets.add(super.widget.mParent.verticalRun.end);
            super.widget.mParent.verticalRun.end.dependencies.add(super.start);
        }
        addDependency(super.widget.verticalRun.start);
        addDependency(super.widget.verticalRun.end);
    }

    public void applyToWidget() {
        if (((Guideline) super.widget).getOrientation() == 1) {
            super.widget.setX(super.start.value);
        } else {
            super.widget.setY(super.start.value);
        }
    }

    /* access modifiers changed from: package-private */
    public void clear() {
        super.start.clear();
    }

    /* access modifiers changed from: package-private */
    public void reset() {
        super.start.resolved = false;
        super.end.resolved = false;
    }

    /* access modifiers changed from: package-private */
    public boolean supportsWrapComputation() {
        return false;
    }

    public void update(Dependency dependency) {
        DependencyNode dependencyNode = super.start;
        if (dependencyNode.readyToSolve && !dependencyNode.resolved) {
            super.start.resolve((int) ((((float) dependencyNode.targets.get(0).value) * ((Guideline) super.widget).getRelativePercent()) + 0.5f));
        }
    }
}
