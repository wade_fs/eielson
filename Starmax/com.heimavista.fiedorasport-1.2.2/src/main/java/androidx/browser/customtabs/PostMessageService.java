package androidx.browser.customtabs;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import p014b.p015a.p016a.ICustomTabsCallback;
import p014b.p015a.p016a.IPostMessageService;

public class PostMessageService extends Service {
    private IPostMessageService.C0793a mBinder = new IPostMessageService.C0793a() {
        /* class androidx.browser.customtabs.PostMessageService.C03631 */

        public void onMessageChannelReady(ICustomTabsCallback aVar, Bundle bundle) {
            aVar.onMessageChannelReady(bundle);
        }

        public void onPostMessage(ICustomTabsCallback aVar, String str, Bundle bundle) {
            aVar.onPostMessage(str, bundle);
        }
    };

    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }
}
