package androidx.annotation;

import com.tencent.p214mm.opensdk.constants.ConstantsAPI;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD, ElementType.PACKAGE})
@Retention(RetentionPolicy.CLASS)
public @interface RequiresApi {
    @IntRange(from = ConstantsAPI.AppSupportContentFlag.MMAPP_SUPPORT_TEXT)
    int api() default 1;

    @IntRange(from = ConstantsAPI.AppSupportContentFlag.MMAPP_SUPPORT_TEXT)
    int value() default 1;
}
