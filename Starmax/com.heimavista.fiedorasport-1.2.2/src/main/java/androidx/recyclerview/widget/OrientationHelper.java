package androidx.recyclerview.widget;

import android.graphics.Rect;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class OrientationHelper {
    public static final int HORIZONTAL = 0;
    private static final int INVALID_SIZE = Integer.MIN_VALUE;
    public static final int VERTICAL = 1;
    private int mLastTotalSpace;
    protected final RecyclerView.LayoutManager mLayoutManager;
    final Rect mTmpRect;

    public static OrientationHelper createHorizontalHelper(RecyclerView.LayoutManager layoutManager) {
        return new OrientationHelper(layoutManager) {
            /* class androidx.recyclerview.widget.OrientationHelper.C06461 */

            public int getDecoratedEnd(View view) {
                return super.mLayoutManager.getDecoratedRight(view) + ((RecyclerView.LayoutParams) view.getLayoutParams()).rightMargin;
            }

            public int getDecoratedMeasurement(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return super.mLayoutManager.getDecoratedMeasuredWidth(view) + layoutParams.leftMargin + layoutParams.rightMargin;
            }

            public int getDecoratedMeasurementInOther(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return super.mLayoutManager.getDecoratedMeasuredHeight(view) + layoutParams.topMargin + layoutParams.bottomMargin;
            }

            public int getDecoratedStart(View view) {
                return super.mLayoutManager.getDecoratedLeft(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).leftMargin;
            }

            public int getEnd() {
                return super.mLayoutManager.getWidth();
            }

            public int getEndAfterPadding() {
                return super.mLayoutManager.getWidth() - super.mLayoutManager.getPaddingRight();
            }

            public int getEndPadding() {
                return super.mLayoutManager.getPaddingRight();
            }

            public int getMode() {
                return super.mLayoutManager.getWidthMode();
            }

            public int getModeInOther() {
                return super.mLayoutManager.getHeightMode();
            }

            public int getStartAfterPadding() {
                return super.mLayoutManager.getPaddingLeft();
            }

            public int getTotalSpace() {
                return (super.mLayoutManager.getWidth() - super.mLayoutManager.getPaddingLeft()) - super.mLayoutManager.getPaddingRight();
            }

            public int getTransformedEndWithDecoration(View view) {
                super.mLayoutManager.getTransformedBoundingBox(view, true, super.mTmpRect);
                return super.mTmpRect.right;
            }

            public int getTransformedStartWithDecoration(View view) {
                super.mLayoutManager.getTransformedBoundingBox(view, true, super.mTmpRect);
                return super.mTmpRect.left;
            }

            public void offsetChild(View view, int i) {
                view.offsetLeftAndRight(i);
            }

            public void offsetChildren(int i) {
                super.mLayoutManager.offsetChildrenHorizontal(i);
            }
        };
    }

    public static OrientationHelper createOrientationHelper(RecyclerView.LayoutManager layoutManager, int i) {
        if (i == 0) {
            return createHorizontalHelper(layoutManager);
        }
        if (i == 1) {
            return createVerticalHelper(layoutManager);
        }
        throw new IllegalArgumentException("invalid orientation");
    }

    public static OrientationHelper createVerticalHelper(RecyclerView.LayoutManager layoutManager) {
        return new OrientationHelper(layoutManager) {
            /* class androidx.recyclerview.widget.OrientationHelper.C06472 */

            public int getDecoratedEnd(View view) {
                return super.mLayoutManager.getDecoratedBottom(view) + ((RecyclerView.LayoutParams) view.getLayoutParams()).bottomMargin;
            }

            public int getDecoratedMeasurement(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return super.mLayoutManager.getDecoratedMeasuredHeight(view) + layoutParams.topMargin + layoutParams.bottomMargin;
            }

            public int getDecoratedMeasurementInOther(View view) {
                RecyclerView.LayoutParams layoutParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                return super.mLayoutManager.getDecoratedMeasuredWidth(view) + layoutParams.leftMargin + layoutParams.rightMargin;
            }

            public int getDecoratedStart(View view) {
                return super.mLayoutManager.getDecoratedTop(view) - ((RecyclerView.LayoutParams) view.getLayoutParams()).topMargin;
            }

            public int getEnd() {
                return super.mLayoutManager.getHeight();
            }

            public int getEndAfterPadding() {
                return super.mLayoutManager.getHeight() - super.mLayoutManager.getPaddingBottom();
            }

            public int getEndPadding() {
                return super.mLayoutManager.getPaddingBottom();
            }

            public int getMode() {
                return super.mLayoutManager.getHeightMode();
            }

            public int getModeInOther() {
                return super.mLayoutManager.getWidthMode();
            }

            public int getStartAfterPadding() {
                return super.mLayoutManager.getPaddingTop();
            }

            public int getTotalSpace() {
                return (super.mLayoutManager.getHeight() - super.mLayoutManager.getPaddingTop()) - super.mLayoutManager.getPaddingBottom();
            }

            public int getTransformedEndWithDecoration(View view) {
                super.mLayoutManager.getTransformedBoundingBox(view, true, super.mTmpRect);
                return super.mTmpRect.bottom;
            }

            public int getTransformedStartWithDecoration(View view) {
                super.mLayoutManager.getTransformedBoundingBox(view, true, super.mTmpRect);
                return super.mTmpRect.top;
            }

            public void offsetChild(View view, int i) {
                view.offsetTopAndBottom(i);
            }

            public void offsetChildren(int i) {
                super.mLayoutManager.offsetChildrenVertical(i);
            }
        };
    }

    public abstract int getDecoratedEnd(View view);

    public abstract int getDecoratedMeasurement(View view);

    public abstract int getDecoratedMeasurementInOther(View view);

    public abstract int getDecoratedStart(View view);

    public abstract int getEnd();

    public abstract int getEndAfterPadding();

    public abstract int getEndPadding();

    public RecyclerView.LayoutManager getLayoutManager() {
        return this.mLayoutManager;
    }

    public abstract int getMode();

    public abstract int getModeInOther();

    public abstract int getStartAfterPadding();

    public abstract int getTotalSpace();

    public int getTotalSpaceChange() {
        if (Integer.MIN_VALUE == this.mLastTotalSpace) {
            return 0;
        }
        return getTotalSpace() - this.mLastTotalSpace;
    }

    public abstract int getTransformedEndWithDecoration(View view);

    public abstract int getTransformedStartWithDecoration(View view);

    public abstract void offsetChild(View view, int i);

    public abstract void offsetChildren(int i);

    public void onLayoutComplete() {
        this.mLastTotalSpace = getTotalSpace();
    }

    private OrientationHelper(RecyclerView.LayoutManager layoutManager) {
        this.mLastTotalSpace = Integer.MIN_VALUE;
        this.mTmpRect = new Rect();
        this.mLayoutManager = layoutManager;
    }
}
