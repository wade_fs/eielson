package p245k.p246a.p249g;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import p241io.jsonwebtoken.JwtParser;
import p245k.p246a.ILoggerFactory;
import p245k.p246a.Logger;

/* renamed from: k.a.g.b */
class AndroidLoggerFactory implements ILoggerFactory {

    /* renamed from: a */
    private final ConcurrentMap<String, Logger> f11951a = new ConcurrentHashMap();

    AndroidLoggerFactory() {
    }

    /* renamed from: b */
    private static String m18411b(String str) {
        int length = str.length();
        int lastIndexOf = str.lastIndexOf(46);
        if (lastIndexOf != -1) {
            int i = lastIndexOf + 1;
            if (length - i <= 23) {
                return str.substring(i);
            }
        }
        return '*' + str.substring((length - 23) + 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder}
     arg types: [java.lang.String, int, int]
     candidates:
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(char[], int, int):java.lang.StringBuilder}
      ClspMth{java.lang.Appendable.append(java.lang.CharSequence, int, int):java.lang.Appendable throws java.io.IOException}
      ClspMth{java.lang.StringBuilder.append(java.lang.CharSequence, int, int):java.lang.StringBuilder} */
    /* renamed from: c */
    static String m18412c(String str) {
        if (str == null) {
            return "null";
        }
        int length = str.length();
        if (length <= 23) {
            return str;
        }
        StringBuilder sb = new StringBuilder(26);
        int i = 0;
        int i2 = 0;
        do {
            int indexOf = str.indexOf(46, i);
            if (indexOf != -1) {
                sb.append(str.charAt(i));
                if (indexOf - i > 1) {
                    sb.append('*');
                }
                sb.append((char) JwtParser.SEPARATOR_CHAR);
                i = indexOf + 1;
                i2 = sb.length();
            } else {
                int i3 = length - i;
                if (i2 == 0 || i2 + i3 > 23) {
                    return m18411b(str);
                }
                sb.append((CharSequence) str, i, length);
                return sb.toString();
            }
        } while (i2 <= 23);
        return m18411b(str);
    }

    /* renamed from: a */
    public Logger mo28145a(String str) {
        String c = m18412c(str);
        Logger bVar = this.f11951a.get(c);
        if (bVar != null) {
            return bVar;
        }
        AndroidLoggerAdapter aVar = new AndroidLoggerAdapter(c);
        Logger putIfAbsent = this.f11951a.putIfAbsent(c, aVar);
        return putIfAbsent == null ? aVar : putIfAbsent;
    }
}
