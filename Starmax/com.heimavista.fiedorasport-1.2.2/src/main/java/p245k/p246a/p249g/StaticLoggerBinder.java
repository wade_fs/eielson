package p245k.p246a.p249g;

import p245k.p246a.ILoggerFactory;
import p245k.p246a.p250h.LoggerFactoryBinder;

/* renamed from: k.a.g.c */
public class StaticLoggerBinder implements LoggerFactoryBinder {

    /* renamed from: b */
    private static final StaticLoggerBinder f11952b = new StaticLoggerBinder();

    /* renamed from: c */
    public static String f11953c = "1.6.99";

    /* renamed from: d */
    private static final String f11954d = AndroidLoggerFactory.class.getName();

    /* renamed from: a */
    private final ILoggerFactory f11955a = new AndroidLoggerFactory();

    private StaticLoggerBinder() {
    }

    /* renamed from: c */
    public static StaticLoggerBinder m18414c() {
        return f11952b;
    }

    /* renamed from: a */
    public ILoggerFactory mo28181a() {
        return this.f11955a;
    }

    /* renamed from: b */
    public String mo28182b() {
        return f11954d;
    }
}
