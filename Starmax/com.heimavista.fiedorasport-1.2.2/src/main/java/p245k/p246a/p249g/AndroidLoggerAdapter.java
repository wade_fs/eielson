package p245k.p246a.p249g;

import android.util.Log;
import p245k.p246a.p248f.FormattingTuple;
import p245k.p246a.p248f.MarkerIgnoringBase;
import p245k.p246a.p248f.MessageFormatter;

/* renamed from: k.a.g.a */
class AndroidLoggerAdapter extends MarkerIgnoringBase {
    private static final long serialVersionUID = -1227274521521287937L;

    AndroidLoggerAdapter(String str) {
        this.f11940P = str;
    }

    /* renamed from: a */
    public void mo28147a(String str, Object obj) {
        m18401a(5, str, obj);
    }

    /* renamed from: b */
    public void mo28150b(String str) {
        m18400a(3, str, (Throwable) null);
    }

    /* renamed from: c */
    public void mo28151c(String str) {
        m18400a(4, str, (Throwable) null);
    }

    /* renamed from: d */
    public void mo28152d(String str) {
        m18400a(5, str, (Throwable) null);
    }

    /* renamed from: b */
    private void m18403b(int i, String str, Throwable th) {
        if (th != null) {
            str = str + 10 + Log.getStackTraceString(th);
        }
        Log.println(i, this.f11940P, str);
    }

    /* renamed from: a */
    public void mo28148a(String str, Object obj, Object obj2) {
        m18401a(5, str, obj, obj2);
    }

    /* renamed from: a */
    public void mo28146a(String str) {
        m18400a(6, str, (Throwable) null);
    }

    /* renamed from: a */
    public void mo28149a(String str, Throwable th) {
        m18400a(6, str, th);
    }

    /* renamed from: a */
    private void m18401a(int i, String str, Object... objArr) {
        if (m18402a(i)) {
            FormattingTuple a = MessageFormatter.m18352a(str, objArr);
            m18403b(i, a.mo28163a(), a.mo28164b());
        }
    }

    /* renamed from: a */
    private void m18400a(int i, String str, Throwable th) {
        if (m18402a(i)) {
            m18403b(i, str, th);
        }
    }

    /* renamed from: a */
    private boolean m18402a(int i) {
        return Log.isLoggable(this.f11940P, i);
    }
}
