package p245k.p246a;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.concurrent.LinkedBlockingQueue;
import p245k.p246a.p247e.SubstituteLoggingEvent;
import p245k.p246a.p248f.C5052i;
import p245k.p246a.p248f.NOPLoggerFactory;
import p245k.p246a.p248f.SubstituteLogger;
import p245k.p246a.p248f.SubstituteLoggerFactory;
import p245k.p246a.p249g.StaticLoggerBinder;

/* renamed from: k.a.c */
public final class LoggerFactory {

    /* renamed from: a */
    static volatile int f11921a;

    /* renamed from: b */
    static SubstituteLoggerFactory f11922b = new SubstituteLoggerFactory();

    /* renamed from: c */
    static NOPLoggerFactory f11923c = new NOPLoggerFactory();

    /* renamed from: d */
    private static final String[] f11924d = {"1.6", "1.7"};

    /* renamed from: e */
    private static String f11925e = "org/slf4j/impl/StaticLoggerBinder.class";

    static {
        C5052i.m18398b("slf4j.detectLoggerNameMismatch");
    }

    private LoggerFactory() {
    }

    /* renamed from: a */
    private static final void m18314a() {
        Set<URL> set = null;
        try {
            if (!m18327f()) {
                set = m18323c();
                m18324c(set);
            }
            StaticLoggerBinder.m18414c();
            f11921a = 3;
            m18321b(set);
            m18325d();
            m18329h();
            f11922b.mo28177a();
        } catch (NoClassDefFoundError e) {
            if (m18322b(e.getMessage())) {
                f11921a = 4;
                C5052i.m18396a("Failed to load class \"org.slf4j.impl.StaticLoggerBinder\".");
                C5052i.m18396a("Defaulting to no-operation (NOP) logger implementation");
                C5052i.m18396a("See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.");
                return;
            }
            m18316a(e);
            throw e;
        } catch (NoSuchMethodError e2) {
            String message = e2.getMessage();
            if (message != null && message.contains("org.slf4j.impl.StaticLoggerBinder.getSingleton()")) {
                f11921a = 2;
                C5052i.m18396a("slf4j-api 1.6.x (or later) is incompatible with this binding.");
                C5052i.m18396a("Your binding is version 1.5.5 or earlier.");
                C5052i.m18396a("Upgrade your binding to version 1.6.x.");
            }
            throw e2;
        } catch (Exception e3) {
            m18316a(e3);
            throw new IllegalStateException("Unexpected initialization failure", e3);
        }
    }

    /* renamed from: b */
    private static boolean m18322b(String str) {
        if (str == null) {
            return false;
        }
        return str.contains("org/slf4j/impl/StaticLoggerBinder") || str.contains("org.slf4j.impl.StaticLoggerBinder");
    }

    /* renamed from: c */
    static Set<URL> m18323c() {
        Enumeration<URL> enumeration;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        try {
            ClassLoader classLoader = LoggerFactory.class.getClassLoader();
            if (classLoader == null) {
                enumeration = ClassLoader.getSystemResources(f11925e);
            } else {
                enumeration = classLoader.getResources(f11925e);
            }
            while (enumeration.hasMoreElements()) {
                linkedHashSet.add((URL) enumeration.nextElement());
            }
        } catch (IOException e) {
            C5052i.m18397a("Error getting resources from path", e);
        }
        return linkedHashSet;
    }

    /* renamed from: d */
    private static void m18325d() {
        synchronized (f11922b) {
            f11922b.mo28180d();
            for (SubstituteLogger gVar : f11922b.mo28179c()) {
                gVar.mo28169a(m18313a(gVar.mo28171b()));
            }
        }
    }

    /* renamed from: e */
    public static ILoggerFactory m18326e() {
        if (f11921a == 0) {
            synchronized (LoggerFactory.class) {
                if (f11921a == 0) {
                    f11921a = 1;
                    m18328g();
                }
            }
        }
        int i = f11921a;
        if (i == 1) {
            return f11922b;
        }
        if (i == 2) {
            throw new IllegalStateException("org.slf4j.LoggerFactory could not be successfully initialized. See also http://www.slf4j.org/codes.html#unsuccessfulInit");
        } else if (i == 3) {
            return StaticLoggerBinder.m18414c().mo28181a();
        } else {
            if (i == 4) {
                return f11923c;
            }
            throw new IllegalStateException("Unreachable code");
        }
    }

    /* renamed from: f */
    private static boolean m18327f() {
        String c = C5052i.m18399c("java.vendor.url");
        if (c == null) {
            return false;
        }
        return c.toLowerCase().contains("android");
    }

    /* renamed from: g */
    private static final void m18328g() {
        m18314a();
        if (f11921a == 3) {
            m18330i();
        }
    }

    /* renamed from: h */
    private static void m18329h() {
        LinkedBlockingQueue<SubstituteLoggingEvent> b = f11922b.mo28178b();
        int size = b.size();
        ArrayList<SubstituteLoggingEvent> arrayList = new ArrayList<>(128);
        int i = 0;
        while (b.drainTo(arrayList, 128) != 0) {
            for (SubstituteLoggingEvent dVar : arrayList) {
                m18317a(dVar);
                int i2 = i + 1;
                if (i == 0) {
                    m18318a(dVar, size);
                }
                i = i2;
            }
            arrayList.clear();
        }
    }

    /* renamed from: i */
    private static final void m18330i() {
        try {
            String str = StaticLoggerBinder.f11953c;
            boolean z = false;
            for (String str2 : f11924d) {
                if (str.startsWith(str2)) {
                    z = true;
                }
            }
            if (!z) {
                C5052i.m18396a("The requested version " + str + " by your slf4j binding is not compatible with " + Arrays.asList(f11924d).toString());
                C5052i.m18396a("See http://www.slf4j.org/codes.html#version_mismatch for further details.");
            }
        } catch (NoSuchFieldError unused) {
        } catch (Throwable th) {
            C5052i.m18397a("Unexpected problem occured during version sanity check", th);
        }
    }

    /* renamed from: b */
    private static void m18320b() {
        C5052i.m18396a("The following set of substitute loggers may have been accessed");
        C5052i.m18396a("during the initialization phase. Logging calls during this");
        C5052i.m18396a("phase were not honored. However, subsequent logging calls to these");
        C5052i.m18396a("loggers will work as normally expected.");
        C5052i.m18396a("See also http://www.slf4j.org/codes.html#substituteLogger");
    }

    /* renamed from: b */
    private static void m18321b(Set<URL> set) {
        if (set != null && m18319a(set)) {
            C5052i.m18396a("Actual binding is of type [" + StaticLoggerBinder.m18414c().mo28182b() + "]");
        }
    }

    /* renamed from: c */
    private static void m18324c(Set<URL> set) {
        if (m18319a(set)) {
            C5052i.m18396a("Class path contains multiple SLF4J bindings.");
            for (URL url : set) {
                C5052i.m18396a("Found binding in [" + url + "]");
            }
            C5052i.m18396a("See http://www.slf4j.org/codes.html#multiple_bindings for an explanation.");
        }
    }

    /* renamed from: a */
    static void m18316a(Throwable th) {
        f11921a = 2;
        C5052i.m18397a("Failed to instantiate SLF4J LoggerFactory", th);
    }

    /* renamed from: a */
    private static void m18318a(SubstituteLoggingEvent dVar, int i) {
        if (dVar.mo28154a().mo28172c()) {
            m18315a(i);
        } else if (!dVar.mo28154a().mo28173d()) {
            m18320b();
        }
    }

    /* renamed from: a */
    private static void m18317a(SubstituteLoggingEvent dVar) {
        if (dVar != null) {
            SubstituteLogger a = dVar.mo28154a();
            String b = a.mo28171b();
            if (a.mo28174e()) {
                throw new IllegalStateException("Delegate logger cannot be null at this state.");
            } else if (!a.mo28173d()) {
                if (a.mo28172c()) {
                    a.mo28170a(dVar);
                } else {
                    C5052i.m18396a(b);
                }
            }
        }
    }

    /* renamed from: a */
    private static void m18315a(int i) {
        C5052i.m18396a("A number (" + i + ") of logging calls during the initialization phase have been intercepted and are");
        C5052i.m18396a("now being replayed. These are subject to the filtering rules of the underlying logging system.");
        C5052i.m18396a("See also http://www.slf4j.org/codes.html#replay");
    }

    /* renamed from: a */
    private static boolean m18319a(Set<URL> set) {
        return set.size() > 1;
    }

    /* renamed from: a */
    public static Logger m18313a(String str) {
        return m18326e().mo28145a(str);
    }
}
