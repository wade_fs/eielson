package p245k.p246a.p247e;

import p245k.p246a.p248f.SubstituteLogger;

/* renamed from: k.a.e.d */
public class SubstituteLoggingEvent implements LoggingEvent {

    /* renamed from: a */
    SubstituteLogger f11936a;

    /* renamed from: a */
    public SubstituteLogger mo28154a() {
        return this.f11936a;
    }

    /* renamed from: a */
    public void mo28155a(long j) {
    }

    /* renamed from: a */
    public void mo28156a(String str) {
    }

    /* renamed from: a */
    public void mo28157a(Throwable th) {
    }

    /* renamed from: a */
    public void mo28158a(Level bVar) {
    }

    /* renamed from: a */
    public void mo28160a(Object[] objArr) {
    }

    /* renamed from: b */
    public void mo28161b(String str) {
    }

    /* renamed from: c */
    public void mo28162c(String str) {
    }

    /* renamed from: a */
    public void mo28159a(SubstituteLogger gVar) {
        this.f11936a = gVar;
    }
}
