package p245k.p246a.p247e;

/* renamed from: k.a.e.b */
public enum Level {
    ERROR(40, "ERROR"),
    WARN(30, "WARN"),
    INFO(20, "INFO"),
    DEBUG(10, "DEBUG"),
    TRACE(0, "TRACE");
    

    /* renamed from: P */
    private String f11935P;

    private Level(int i, String str) {
        this.f11935P = str;
    }

    public String toString() {
        return this.f11935P;
    }
}
