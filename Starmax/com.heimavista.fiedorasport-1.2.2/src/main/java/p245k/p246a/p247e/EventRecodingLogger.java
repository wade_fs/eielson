package p245k.p246a.p247e;

import java.util.Queue;
import p245k.p246a.Logger;
import p245k.p246a.Marker;
import p245k.p246a.p248f.SubstituteLogger;

/* renamed from: k.a.e.a */
public class EventRecodingLogger implements Logger {

    /* renamed from: P */
    String f11926P;

    /* renamed from: Q */
    SubstituteLogger f11927Q;

    /* renamed from: R */
    Queue<SubstituteLoggingEvent> f11928R;

    public EventRecodingLogger(SubstituteLogger gVar, Queue<SubstituteLoggingEvent> queue) {
        this.f11927Q = gVar;
        this.f11926P = gVar.mo28171b();
        this.f11928R = queue;
    }

    /* renamed from: a */
    private void m18331a(Level bVar, String str, Object[] objArr, Throwable th) {
        m18332a(bVar, null, str, objArr, th);
    }

    /* renamed from: b */
    public void mo28150b(String str) {
        m18331a(Level.TRACE, str, null, null);
    }

    /* renamed from: c */
    public void mo28151c(String str) {
        m18331a(Level.INFO, str, null, null);
    }

    /* renamed from: d */
    public void mo28152d(String str) {
        m18331a(Level.WARN, str, null, null);
    }

    /* renamed from: a */
    private void m18332a(Level bVar, Marker dVar, String str, Object[] objArr, Throwable th) {
        SubstituteLoggingEvent dVar2 = new SubstituteLoggingEvent();
        dVar2.mo28155a(System.currentTimeMillis());
        dVar2.mo28158a(bVar);
        dVar2.mo28159a(this.f11927Q);
        dVar2.mo28156a(this.f11926P);
        dVar2.mo28161b(str);
        dVar2.mo28160a(objArr);
        dVar2.mo28157a(th);
        dVar2.mo28162c(Thread.currentThread().getName());
        this.f11928R.add(dVar2);
    }

    /* renamed from: a */
    public void mo28147a(String str, Object obj) {
        m18331a(Level.WARN, str, new Object[]{obj}, null);
    }

    /* renamed from: a */
    public void mo28148a(String str, Object obj, Object obj2) {
        m18331a(Level.WARN, str, new Object[]{obj, obj2}, null);
    }

    /* renamed from: a */
    public void mo28146a(String str) {
        m18331a(Level.ERROR, str, null, null);
    }

    /* renamed from: a */
    public void mo28149a(String str, Throwable th) {
        m18331a(Level.ERROR, str, null, th);
    }
}
