package p245k.p246a;

/* renamed from: k.a.b */
public interface Logger {
    /* renamed from: a */
    void mo28146a(String str);

    /* renamed from: a */
    void mo28147a(String str, Object obj);

    /* renamed from: a */
    void mo28148a(String str, Object obj, Object obj2);

    /* renamed from: a */
    void mo28149a(String str, Throwable th);

    /* renamed from: b */
    void mo28150b(String str);

    /* renamed from: c */
    void mo28151c(String str);

    /* renamed from: d */
    void mo28152d(String str);
}
