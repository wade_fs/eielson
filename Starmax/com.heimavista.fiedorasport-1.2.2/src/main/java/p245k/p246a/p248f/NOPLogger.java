package p245k.p246a.p248f;

/* renamed from: k.a.f.d */
public class NOPLogger extends MarkerIgnoringBase {

    /* renamed from: Q */
    public static final NOPLogger f11939Q = new NOPLogger();
    private static final long serialVersionUID = -517220405410904473L;

    protected NOPLogger() {
    }

    /* renamed from: a */
    public final void mo28146a(String str) {
    }

    /* renamed from: a */
    public final void mo28147a(String str, Object obj) {
    }

    /* renamed from: a */
    public final void mo28148a(String str, Object obj, Object obj2) {
    }

    /* renamed from: a */
    public final void mo28149a(String str, Throwable th) {
    }

    /* renamed from: b */
    public final void mo28150b(String str) {
    }

    /* renamed from: c */
    public final void mo28151c(String str) {
    }

    /* renamed from: d */
    public final void mo28152d(String str) {
    }

    public String getName() {
        return "NOP";
    }
}
