package p245k.p246a.p248f;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Queue;
import p245k.p246a.Logger;
import p245k.p246a.p247e.EventRecodingLogger;
import p245k.p246a.p247e.LoggingEvent;
import p245k.p246a.p247e.SubstituteLoggingEvent;

/* renamed from: k.a.f.g */
public class SubstituteLogger implements Logger {

    /* renamed from: P */
    private final String f11941P;

    /* renamed from: Q */
    private volatile Logger f11942Q;

    /* renamed from: R */
    private Boolean f11943R;

    /* renamed from: S */
    private Method f11944S;

    /* renamed from: T */
    private EventRecodingLogger f11945T;

    /* renamed from: U */
    private Queue<SubstituteLoggingEvent> f11946U;

    /* renamed from: V */
    private final boolean f11947V;

    public SubstituteLogger(String str, Queue<SubstituteLoggingEvent> queue, boolean z) {
        this.f11941P = str;
        this.f11946U = queue;
        this.f11947V = z;
    }

    /* renamed from: f */
    private Logger m18376f() {
        if (this.f11945T == null) {
            this.f11945T = new EventRecodingLogger(this, this.f11946U);
        }
        return this.f11945T;
    }

    /* renamed from: a */
    public void mo28147a(String str, Object obj) {
        mo28168a().mo28147a(str, obj);
    }

    /* renamed from: b */
    public String mo28171b() {
        return this.f11941P;
    }

    /* renamed from: c */
    public void mo28151c(String str) {
        mo28168a().mo28151c(str);
    }

    /* renamed from: d */
    public void mo28152d(String str) {
        mo28168a().mo28152d(str);
    }

    /* renamed from: e */
    public boolean mo28174e() {
        return this.f11942Q == null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && SubstituteLogger.class == obj.getClass() && this.f11941P.equals(((SubstituteLogger) obj).f11941P);
    }

    public int hashCode() {
        return this.f11941P.hashCode();
    }

    /* renamed from: a */
    public void mo28148a(String str, Object obj, Object obj2) {
        mo28168a().mo28148a(str, obj, obj2);
    }

    /* renamed from: b */
    public void mo28150b(String str) {
        mo28168a().mo28150b(str);
    }

    /* renamed from: c */
    public boolean mo28172c() {
        Boolean bool = this.f11943R;
        if (bool != null) {
            return bool.booleanValue();
        }
        try {
            this.f11944S = this.f11942Q.getClass().getMethod("log", LoggingEvent.class);
            this.f11943R = Boolean.TRUE;
        } catch (NoSuchMethodException unused) {
            this.f11943R = Boolean.FALSE;
        }
        return this.f11943R.booleanValue();
    }

    /* renamed from: d */
    public boolean mo28173d() {
        return this.f11942Q instanceof NOPLogger;
    }

    /* renamed from: a */
    public void mo28146a(String str) {
        mo28168a().mo28146a(str);
    }

    /* renamed from: a */
    public void mo28149a(String str, Throwable th) {
        mo28168a().mo28149a(str, th);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Logger mo28168a() {
        if (this.f11942Q != null) {
            return this.f11942Q;
        }
        if (this.f11947V) {
            return NOPLogger.f11939Q;
        }
        return m18376f();
    }

    /* renamed from: a */
    public void mo28169a(Logger bVar) {
        this.f11942Q = bVar;
    }

    /* renamed from: a */
    public void mo28170a(LoggingEvent cVar) {
        if (mo28172c()) {
            try {
                this.f11944S.invoke(this.f11942Q, cVar);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException unused) {
            }
        }
    }
}
