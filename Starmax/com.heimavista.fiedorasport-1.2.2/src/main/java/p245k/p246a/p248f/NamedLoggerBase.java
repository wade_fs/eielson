package p245k.p246a.p248f;

import java.io.Serializable;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: k.a.f.f */
abstract class NamedLoggerBase implements Logger, Serializable {
    private static final long serialVersionUID = 7535258609338176893L;

    /* renamed from: P */
    protected String f11940P;

    NamedLoggerBase() {
    }

    public String getName() {
        return this.f11940P;
    }

    /* access modifiers changed from: protected */
    public Object readResolve() {
        return LoggerFactory.m18313a(getName());
    }
}
