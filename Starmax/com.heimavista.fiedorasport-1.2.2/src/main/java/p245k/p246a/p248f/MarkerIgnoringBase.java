package p245k.p246a.p248f;

import p245k.p246a.Logger;

/* renamed from: k.a.f.b */
public abstract class MarkerIgnoringBase extends NamedLoggerBase implements Logger {
    private static final long serialVersionUID = 9044267456635152283L;

    public String toString() {
        return getClass().getName() + "(" + getName() + ")";
    }
}
