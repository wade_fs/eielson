package p245k.p246a.p248f;

import com.facebook.internal.ServerProtocol;
import java.io.PrintStream;

/* renamed from: k.a.f.i */
/* compiled from: Util */
public final class C5052i {
    /* renamed from: a */
    public static final void m18397a(String str, Throwable th) {
        System.err.println(str);
        System.err.println("Reported exception:");
        th.printStackTrace();
    }

    /* renamed from: b */
    public static boolean m18398b(String str) {
        String c = m18399c(str);
        if (c == null) {
            return false;
        }
        return c.equalsIgnoreCase(ServerProtocol.DIALOG_RETURN_SCOPES_TRUE);
    }

    /* renamed from: c */
    public static String m18399c(String str) {
        if (str != null) {
            try {
                return System.getProperty(str);
            } catch (SecurityException unused) {
                return null;
            }
        } else {
            throw new IllegalArgumentException("null input");
        }
    }

    /* renamed from: a */
    public static final void m18396a(String str) {
        PrintStream printStream = System.err;
        printStream.println("SLF4J: " + str);
    }
}
