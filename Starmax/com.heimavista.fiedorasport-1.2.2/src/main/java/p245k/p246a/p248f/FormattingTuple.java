package p245k.p246a.p248f;

/* renamed from: k.a.f.a */
public class FormattingTuple {

    /* renamed from: a */
    private String f11937a;

    /* renamed from: b */
    private Throwable f11938b;

    static {
        new FormattingTuple(null);
    }

    public FormattingTuple(String str) {
        this(str, null, null);
    }

    /* renamed from: a */
    public String mo28163a() {
        return this.f11937a;
    }

    /* renamed from: b */
    public Throwable mo28164b() {
        return this.f11938b;
    }

    public FormattingTuple(String str, Object[] objArr, Throwable th) {
        this.f11937a = str;
        this.f11938b = th;
    }
}
