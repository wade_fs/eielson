package p245k.p246a.p248f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import p245k.p246a.ILoggerFactory;
import p245k.p246a.Logger;
import p245k.p246a.p247e.SubstituteLoggingEvent;

/* renamed from: k.a.f.h */
public class SubstituteLoggerFactory implements ILoggerFactory {

    /* renamed from: a */
    boolean f11948a = false;

    /* renamed from: b */
    final Map<String, SubstituteLogger> f11949b = new HashMap();

    /* renamed from: c */
    final LinkedBlockingQueue<SubstituteLoggingEvent> f11950c = new LinkedBlockingQueue<>();

    /* renamed from: a */
    public synchronized Logger mo28145a(String str) {
        SubstituteLogger gVar;
        gVar = this.f11949b.get(str);
        if (gVar == null) {
            gVar = new SubstituteLogger(str, this.f11950c, this.f11948a);
            this.f11949b.put(str, gVar);
        }
        return gVar;
    }

    /* renamed from: b */
    public LinkedBlockingQueue<SubstituteLoggingEvent> mo28178b() {
        return this.f11950c;
    }

    /* renamed from: c */
    public List<SubstituteLogger> mo28179c() {
        return new ArrayList(this.f11949b.values());
    }

    /* renamed from: d */
    public void mo28180d() {
        this.f11948a = true;
    }

    /* renamed from: a */
    public void mo28177a() {
        this.f11949b.clear();
        this.f11950c.clear();
    }
}
