package p245k.p251b.p252a;

import java.util.Vector;

/* renamed from: k.b.a.c */
public class ASN1EncodableVector {

    /* renamed from: a */
    private final Vector f11959a = new Vector();

    /* renamed from: a */
    public void mo28193a(ASN1Encodable bVar) {
        this.f11959a.addElement(bVar);
    }

    /* renamed from: a */
    public ASN1Encodable mo28192a(int i) {
        return (ASN1Encodable) this.f11959a.elementAt(i);
    }

    /* renamed from: a */
    public int mo28191a() {
        return this.f11959a.size();
    }
}
