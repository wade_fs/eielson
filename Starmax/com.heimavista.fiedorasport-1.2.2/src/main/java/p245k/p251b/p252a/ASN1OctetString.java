package p245k.p251b.p252a;

import p245k.p251b.p272e.Arrays;
import p245k.p251b.p272e.Strings;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.a.g */
public abstract class ASN1OctetString extends ASN1Primitive implements ASN1OctetStringParser {

    /* renamed from: P */
    byte[] f11963P;

    public ASN1OctetString(byte[] bArr) {
        if (bArr != null) {
            this.f11963P = bArr;
            return;
        }
        throw new NullPointerException("string cannot be null");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo28183a(ASN1Primitive kVar) {
        if (!(kVar instanceof ASN1OctetString)) {
            return false;
        }
        return Arrays.m20058a(this.f11963P, ((ASN1OctetString) kVar).f11963P);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public ASN1Primitive mo28184c() {
        return new DEROctetString(this.f11963P);
    }

    /* renamed from: d */
    public byte[] mo28202d() {
        return this.f11963P;
    }

    public int hashCode() {
        return Arrays.m20064b(mo28202d());
    }

    public String toString() {
        return "#" + Strings.m20075b(Hex.m20079a(this.f11963P));
    }
}
