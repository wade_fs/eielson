package p245k.p251b.p252a;

import java.math.BigInteger;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.a.d */
public class ASN1Integer extends ASN1Primitive {

    /* renamed from: P */
    private final byte[] f11960P;

    public ASN1Integer(long j) {
        this.f11960P = BigInteger.valueOf(j).toByteArray();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28194a(ASN1OutputStream iVar) {
        iVar.mo28206a(2, this.f11960P);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo28195b() {
        return StreamUtil.m18475a(this.f11960P.length) + 1 + this.f11960P.length;
    }

    /* renamed from: d */
    public BigInteger mo28196d() {
        return new BigInteger(this.f11960P);
    }

    public int hashCode() {
        int i = 0;
        int i2 = 0;
        while (true) {
            byte[] bArr = this.f11960P;
            if (i == bArr.length) {
                return i2;
            }
            i2 ^= (bArr[i] & 255) << (i % 4);
            i++;
        }
    }

    public String toString() {
        return mo28196d().toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo28183a(ASN1Primitive kVar) {
        if (!(kVar instanceof ASN1Integer)) {
            return false;
        }
        return Arrays.m20058a(this.f11960P, ((ASN1Integer) kVar).f11960P);
    }

    public ASN1Integer(BigInteger bigInteger) {
        this.f11960P = bigInteger.toByteArray();
    }
}
