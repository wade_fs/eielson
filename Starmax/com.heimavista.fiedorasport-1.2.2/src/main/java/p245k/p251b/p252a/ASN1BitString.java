package p245k.p251b.p252a;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.a.a */
public abstract class ASN1BitString extends ASN1Primitive implements ASN1String {

    /* renamed from: R */
    private static final char[] f11956R = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: P */
    protected final byte[] f11957P;

    /* renamed from: Q */
    protected final int f11958Q;

    public ASN1BitString(byte[] bArr, int i) {
        if (bArr == null) {
            throw new NullPointerException("data cannot be null");
        } else if (bArr.length == 0 && i != 0) {
            throw new IllegalArgumentException("zero length data with non-zero pad bits");
        } else if (i > 7 || i < 0) {
            throw new IllegalArgumentException("pad bits cannot be greater than 7 or less than 0");
        } else {
            this.f11957P = Arrays.m20060a(bArr);
            this.f11958Q = i;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo28183a(ASN1Primitive kVar) {
        if (!(kVar instanceof ASN1BitString)) {
            return false;
        }
        ASN1BitString aVar = (ASN1BitString) kVar;
        if (this.f11958Q != aVar.f11958Q || !Arrays.m20058a(mo28185d(), aVar.mo28185d())) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public ASN1Primitive mo28184c() {
        return new DERBitString(this.f11957P, this.f11958Q);
    }

    /* renamed from: d */
    public byte[] mo28185d() {
        return m18417a(this.f11957P, this.f11958Q);
    }

    /* renamed from: e */
    public int mo28186e() {
        return this.f11958Q;
    }

    /* renamed from: f */
    public String mo28187f() {
        StringBuffer stringBuffer = new StringBuffer("#");
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            new ASN1OutputStream(byteArrayOutputStream).mo28207a(this);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            for (int i = 0; i != byteArray.length; i++) {
                stringBuffer.append(f11956R[(byteArray[i] >>> 4) & 15]);
                stringBuffer.append(f11956R[byteArray[i] & 15]);
            }
            return stringBuffer.toString();
        } catch (IOException e) {
            throw new ASN1ParsingException("Internal error encoding BitString: " + e.getMessage(), e);
        }
    }

    public int hashCode() {
        return this.f11958Q ^ Arrays.m20064b(mo28185d());
    }

    public String toString() {
        return mo28187f();
    }

    /* renamed from: a */
    protected static byte[] m18417a(byte[] bArr, int i) {
        byte[] a = Arrays.m20060a(bArr);
        if (i > 0) {
            int length = bArr.length - 1;
            a[length] = (byte) ((255 << i) & a[length]);
        }
        return a;
    }
}
