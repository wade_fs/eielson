package p245k.p251b.p252a;

import p245k.p251b.p272e.Encodable;

/* renamed from: k.b.a.e */
public abstract class ASN1Object implements ASN1Encodable, Encodable {
    /* renamed from: a */
    public abstract ASN1Primitive mo28190a();

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ASN1Encodable)) {
            return false;
        }
        return mo28190a().equals(((ASN1Encodable) obj).mo28190a());
    }

    public int hashCode() {
        return mo28190a().hashCode();
    }
}
