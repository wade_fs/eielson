package p245k.p251b.p252a.p255w;

import java.util.Hashtable;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p256x.SECNamedCurves;
import p245k.p251b.p252a.p256x.SECObjectIdentifiers;
import p245k.p251b.p252a.p258z.X9ECParameters;
import p245k.p251b.p272e.Strings;

/* renamed from: k.b.a.w.a */
public class NISTNamedCurves {

    /* renamed from: a */
    static final Hashtable f11985a = new Hashtable();

    /* renamed from: b */
    static final Hashtable f11986b = new Hashtable();

    static {
        m18497a("B-571", SECObjectIdentifiers.f12028F);
        m18497a("B-409", SECObjectIdentifiers.f12026D);
        m18497a("B-283", SECObjectIdentifiers.f12045n);
        m18497a("B-233", SECObjectIdentifiers.f12051t);
        m18497a("B-163", SECObjectIdentifiers.f12043l);
        m18497a("K-571", SECObjectIdentifiers.f12027E);
        m18497a("K-409", SECObjectIdentifiers.f12025C);
        m18497a("K-283", SECObjectIdentifiers.f12044m);
        m18497a("K-233", SECObjectIdentifiers.f12050s);
        m18497a("K-163", SECObjectIdentifiers.f12033b);
        m18497a("P-521", SECObjectIdentifiers.f12024B);
        m18497a("P-384", SECObjectIdentifiers.f12023A);
        m18497a("P-256", SECObjectIdentifiers.f12030H);
        m18497a("P-224", SECObjectIdentifiers.f12057z);
        m18497a("P-192", SECObjectIdentifiers.f12029G);
    }

    /* renamed from: a */
    static void m18497a(String str, ASN1ObjectIdentifier fVar) {
        f11985a.put(str, fVar);
        f11986b.put(fVar, str);
    }

    /* renamed from: a */
    public static X9ECParameters m18495a(String str) {
        ASN1ObjectIdentifier fVar = (ASN1ObjectIdentifier) f11985a.get(Strings.m20074b(str));
        if (fVar != null) {
            return m18496a(fVar);
        }
        return null;
    }

    /* renamed from: a */
    public static X9ECParameters m18496a(ASN1ObjectIdentifier fVar) {
        return SECNamedCurves.m18499a(fVar);
    }
}
