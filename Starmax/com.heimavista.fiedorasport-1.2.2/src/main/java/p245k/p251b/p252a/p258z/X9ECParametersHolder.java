package p245k.p251b.p252a.p258z;

/* renamed from: k.b.a.z.e */
public abstract class X9ECParametersHolder {

    /* renamed from: a */
    private X9ECParameters f12131a;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract X9ECParameters mo28219a();

    /* renamed from: b */
    public synchronized X9ECParameters mo28225b() {
        if (this.f12131a == null) {
            this.f12131a = mo28219a();
        }
        return this.f12131a;
    }
}
