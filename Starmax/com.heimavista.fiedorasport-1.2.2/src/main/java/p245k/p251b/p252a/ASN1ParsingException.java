package p245k.p251b.p252a;

/* renamed from: k.b.a.j */
public class ASN1ParsingException extends IllegalStateException {

    /* renamed from: P */
    private Throwable f11965P;

    public ASN1ParsingException(String str, Throwable th) {
        super(str);
        this.f11965P = th;
    }

    public Throwable getCause() {
        return this.f11965P;
    }
}
