package p245k.p251b.p252a.p258z;

import java.math.BigInteger;
import p245k.p251b.p252a.ASN1EncodableVector;
import p245k.p251b.p252a.ASN1Integer;
import p245k.p251b.p252a.ASN1Object;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.ASN1Primitive;
import p245k.p251b.p252a.DERSequence;

/* renamed from: k.b.a.z.h */
public class X9FieldID extends ASN1Object implements X9ObjectIdentifiers {

    /* renamed from: P */
    private ASN1ObjectIdentifier f12137P;

    /* renamed from: Q */
    private ASN1Primitive f12138Q;

    public X9FieldID(BigInteger bigInteger) {
        this.f12137P = X9ObjectIdentifiers.f12156d;
        this.f12138Q = new ASN1Integer(bigInteger);
    }

    /* renamed from: a */
    public ASN1Primitive mo28190a() {
        ASN1EncodableVector cVar = new ASN1EncodableVector();
        cVar.mo28193a(this.f12137P);
        cVar.mo28193a(this.f12138Q);
        return new DERSequence(cVar);
    }

    public X9FieldID(int i, int i2) {
        this(i, i2, 0, 0);
    }

    public X9FieldID(int i, int i2, int i3, int i4) {
        this.f12137P = X9ObjectIdentifiers.f12157e;
        ASN1EncodableVector cVar = new ASN1EncodableVector();
        cVar.mo28193a(new ASN1Integer((long) i));
        if (i3 == 0) {
            if (i4 == 0) {
                cVar.mo28193a(X9ObjectIdentifiers.f12158f);
                cVar.mo28193a(new ASN1Integer((long) i2));
            } else {
                throw new IllegalArgumentException("inconsistent k values");
            }
        } else if (i3 <= i2 || i4 <= i3) {
            throw new IllegalArgumentException("inconsistent k values");
        } else {
            cVar.mo28193a(X9ObjectIdentifiers.f12159g);
            ASN1EncodableVector cVar2 = new ASN1EncodableVector();
            cVar2.mo28193a(new ASN1Integer((long) i2));
            cVar2.mo28193a(new ASN1Integer((long) i3));
            cVar2.mo28193a(new ASN1Integer((long) i4));
            cVar.mo28193a(new DERSequence(cVar2));
        }
        this.f12138Q = new DERSequence(cVar);
    }
}
