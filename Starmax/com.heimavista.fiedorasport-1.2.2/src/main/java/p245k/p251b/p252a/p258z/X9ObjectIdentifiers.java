package p245k.p251b.p252a.p258z;

import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;
import p245k.p251b.p252a.ASN1ObjectIdentifier;

/* renamed from: k.b.a.z.j */
public interface X9ObjectIdentifiers {

    /* renamed from: A */
    public static final ASN1ObjectIdentifier f12139A = f12164l.mo28199a("19");

    /* renamed from: B */
    public static final ASN1ObjectIdentifier f12140B = f12164l.mo28199a("20");

    /* renamed from: C */
    public static final ASN1ObjectIdentifier f12141C = f12163k.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: D */
    public static final ASN1ObjectIdentifier f12142D = f12141C.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: E */
    public static final ASN1ObjectIdentifier f12143E = f12141C.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);

    /* renamed from: F */
    public static final ASN1ObjectIdentifier f12144F = f12141C.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: G */
    public static final ASN1ObjectIdentifier f12145G = f12141C.mo28199a("4");

    /* renamed from: H */
    public static final ASN1ObjectIdentifier f12146H = f12141C.mo28199a("5");

    /* renamed from: I */
    public static final ASN1ObjectIdentifier f12147I = f12141C.mo28199a("6");

    /* renamed from: J */
    public static final ASN1ObjectIdentifier f12148J = f12141C.mo28199a("7");

    /* renamed from: K */
    public static final ASN1ObjectIdentifier f12149K = new ASN1ObjectIdentifier("1.3.133.16.840.63.0");

    /* renamed from: L */
    public static final ASN1ObjectIdentifier f12150L = new ASN1ObjectIdentifier("1.2.840.10046");

    /* renamed from: M */
    public static final ASN1ObjectIdentifier f12151M = f12150L.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: N */
    public static final ASN1ObjectIdentifier f12152N = new ASN1ObjectIdentifier("1.3.133.16.840.9.44");

    /* renamed from: O */
    public static final ASN1ObjectIdentifier f12153O = f12152N.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: b */
    public static final ASN1ObjectIdentifier f12154b = new ASN1ObjectIdentifier("1.2.840.10045");

    /* renamed from: c */
    public static final ASN1ObjectIdentifier f12155c = f12154b.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: d */
    public static final ASN1ObjectIdentifier f12156d = f12155c.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: e */
    public static final ASN1ObjectIdentifier f12157e = f12155c.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);

    /* renamed from: f */
    public static final ASN1ObjectIdentifier f12158f = f12157e.mo28199a("3.2");

    /* renamed from: g */
    public static final ASN1ObjectIdentifier f12159g = f12157e.mo28199a("3.3");

    /* renamed from: h */
    public static final ASN1ObjectIdentifier f12160h = f12154b.mo28199a("4");

    /* renamed from: i */
    public static final ASN1ObjectIdentifier f12161i = f12154b.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);

    /* renamed from: j */
    public static final ASN1ObjectIdentifier f12162j = f12160h.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: k */
    public static final ASN1ObjectIdentifier f12163k = f12154b.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: l */
    public static final ASN1ObjectIdentifier f12164l = f12163k.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_NO);

    /* renamed from: m */
    public static final ASN1ObjectIdentifier f12165m = f12164l.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: n */
    public static final ASN1ObjectIdentifier f12166n = f12164l.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);

    /* renamed from: o */
    public static final ASN1ObjectIdentifier f12167o = f12164l.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: p */
    public static final ASN1ObjectIdentifier f12168p = f12164l.mo28199a("4");

    /* renamed from: q */
    public static final ASN1ObjectIdentifier f12169q = f12164l.mo28199a("5");

    /* renamed from: r */
    public static final ASN1ObjectIdentifier f12170r = f12164l.mo28199a("6");

    /* renamed from: s */
    public static final ASN1ObjectIdentifier f12171s = f12164l.mo28199a("7");

    /* renamed from: t */
    public static final ASN1ObjectIdentifier f12172t = f12164l.mo28199a("10");

    /* renamed from: u */
    public static final ASN1ObjectIdentifier f12173u = f12164l.mo28199a("11");

    /* renamed from: v */
    public static final ASN1ObjectIdentifier f12174v = f12164l.mo28199a("12");

    /* renamed from: w */
    public static final ASN1ObjectIdentifier f12175w = f12164l.mo28199a("13");

    /* renamed from: x */
    public static final ASN1ObjectIdentifier f12176x = f12164l.mo28199a("16");

    /* renamed from: y */
    public static final ASN1ObjectIdentifier f12177y = f12164l.mo28199a("17");

    /* renamed from: z */
    public static final ASN1ObjectIdentifier f12178z = f12164l.mo28199a("18");

    static {
        f12157e.mo28199a("3.1");
        f12160h.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f12161i.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f12162j.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f12162j.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
        f12162j.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);
        f12162j.mo28199a("4");
        f12164l.mo28199a("8");
        f12164l.mo28199a("9");
        f12164l.mo28199a("14");
        f12164l.mo28199a("15");
        new ASN1ObjectIdentifier("1.2.840.10040.4.1");
        new ASN1ObjectIdentifier("1.2.840.10040.4.3");
        f12149K.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
        f12149K.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);
        f12149K.mo28199a("16");
        f12150L.mo28199a("2.1");
        f12151M.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f12151M.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
        f12151M.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);
        f12151M.mo28199a("4");
        f12151M.mo28199a("5");
        f12151M.mo28199a("6");
        f12151M.mo28199a("7");
        f12151M.mo28199a("8");
        f12153O.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f12153O.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
    }
}
