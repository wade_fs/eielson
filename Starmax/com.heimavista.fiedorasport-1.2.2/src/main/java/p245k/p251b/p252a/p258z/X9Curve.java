package p245k.p251b.p252a.p258z;

import p245k.p251b.p252a.ASN1EncodableVector;
import p245k.p251b.p252a.ASN1Object;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.ASN1Primitive;
import p245k.p251b.p252a.DERBitString;
import p245k.p251b.p252a.DERSequence;
import p245k.p251b.p263d.p264a.ECAlgorithms;
import p245k.p251b.p263d.p264a.ECCurve;

/* renamed from: k.b.a.z.c */
public class X9Curve extends ASN1Object implements X9ObjectIdentifiers {

    /* renamed from: P */
    private ECCurve f12121P;

    /* renamed from: Q */
    private byte[] f12122Q;

    /* renamed from: R */
    private ASN1ObjectIdentifier f12123R = null;

    public X9Curve(ECCurve cVar, byte[] bArr) {
        this.f12121P = cVar;
        this.f12122Q = bArr;
        m18588b();
    }

    /* renamed from: b */
    private void m18588b() {
        if (ECAlgorithms.m18656b(this.f12121P)) {
            this.f12123R = X9ObjectIdentifiers.f12156d;
        } else if (ECAlgorithms.m18654a(this.f12121P)) {
            this.f12123R = X9ObjectIdentifiers.f12157e;
        } else {
            throw new IllegalArgumentException("This type of ECCurve is not implemented");
        }
    }

    /* renamed from: a */
    public ASN1Primitive mo28190a() {
        ASN1EncodableVector cVar = new ASN1EncodableVector();
        if (this.f12123R.equals(X9ObjectIdentifiers.f12156d)) {
            cVar.mo28193a(new X9FieldElement(this.f12121P.mo28250c()).mo28190a());
            cVar.mo28193a(new X9FieldElement(this.f12121P.mo28251d()).mo28190a());
        } else if (this.f12123R.equals(X9ObjectIdentifiers.f12157e)) {
            cVar.mo28193a(new X9FieldElement(this.f12121P.mo28250c()).mo28190a());
            cVar.mo28193a(new X9FieldElement(this.f12121P.mo28251d()).mo28190a());
        }
        byte[] bArr = this.f12122Q;
        if (bArr != null) {
            cVar.mo28193a(new DERBitString(bArr));
        }
        return new DERSequence(cVar);
    }
}
