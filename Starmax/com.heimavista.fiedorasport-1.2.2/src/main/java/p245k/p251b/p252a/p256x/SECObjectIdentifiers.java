package p245k.p251b.p252a.p256x;

import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p258z.X9ObjectIdentifiers;

/* renamed from: k.b.a.x.b */
public interface SECObjectIdentifiers {

    /* renamed from: A */
    public static final ASN1ObjectIdentifier f12023A = f12032a.mo28199a("34");

    /* renamed from: B */
    public static final ASN1ObjectIdentifier f12024B = f12032a.mo28199a("35");

    /* renamed from: C */
    public static final ASN1ObjectIdentifier f12025C = f12032a.mo28199a("36");

    /* renamed from: D */
    public static final ASN1ObjectIdentifier f12026D = f12032a.mo28199a("37");

    /* renamed from: E */
    public static final ASN1ObjectIdentifier f12027E = f12032a.mo28199a("38");

    /* renamed from: F */
    public static final ASN1ObjectIdentifier f12028F = f12032a.mo28199a("39");

    /* renamed from: G */
    public static final ASN1ObjectIdentifier f12029G = X9ObjectIdentifiers.f12142D;

    /* renamed from: H */
    public static final ASN1ObjectIdentifier f12030H = X9ObjectIdentifiers.f12148J;

    /* renamed from: I */
    public static final ASN1ObjectIdentifier f12031I = new ASN1ObjectIdentifier("1.3.132.1");

    /* renamed from: a */
    public static final ASN1ObjectIdentifier f12032a = new ASN1ObjectIdentifier("1.3.132.0");

    /* renamed from: b */
    public static final ASN1ObjectIdentifier f12033b = f12032a.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: c */
    public static final ASN1ObjectIdentifier f12034c = f12032a.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);

    /* renamed from: d */
    public static final ASN1ObjectIdentifier f12035d = f12032a.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: e */
    public static final ASN1ObjectIdentifier f12036e = f12032a.mo28199a("4");

    /* renamed from: f */
    public static final ASN1ObjectIdentifier f12037f = f12032a.mo28199a("5");

    /* renamed from: g */
    public static final ASN1ObjectIdentifier f12038g = f12032a.mo28199a("6");

    /* renamed from: h */
    public static final ASN1ObjectIdentifier f12039h = f12032a.mo28199a("7");

    /* renamed from: i */
    public static final ASN1ObjectIdentifier f12040i = f12032a.mo28199a("8");

    /* renamed from: j */
    public static final ASN1ObjectIdentifier f12041j = f12032a.mo28199a("9");

    /* renamed from: k */
    public static final ASN1ObjectIdentifier f12042k = f12032a.mo28199a("10");

    /* renamed from: l */
    public static final ASN1ObjectIdentifier f12043l = f12032a.mo28199a("15");

    /* renamed from: m */
    public static final ASN1ObjectIdentifier f12044m = f12032a.mo28199a("16");

    /* renamed from: n */
    public static final ASN1ObjectIdentifier f12045n = f12032a.mo28199a("17");

    /* renamed from: o */
    public static final ASN1ObjectIdentifier f12046o = f12032a.mo28199a("22");

    /* renamed from: p */
    public static final ASN1ObjectIdentifier f12047p = f12032a.mo28199a("23");

    /* renamed from: q */
    public static final ASN1ObjectIdentifier f12048q = f12032a.mo28199a("24");

    /* renamed from: r */
    public static final ASN1ObjectIdentifier f12049r = f12032a.mo28199a("25");

    /* renamed from: s */
    public static final ASN1ObjectIdentifier f12050s = f12032a.mo28199a("26");

    /* renamed from: t */
    public static final ASN1ObjectIdentifier f12051t = f12032a.mo28199a("27");

    /* renamed from: u */
    public static final ASN1ObjectIdentifier f12052u = f12032a.mo28199a("28");

    /* renamed from: v */
    public static final ASN1ObjectIdentifier f12053v = f12032a.mo28199a("29");

    /* renamed from: w */
    public static final ASN1ObjectIdentifier f12054w = f12032a.mo28199a("30");

    /* renamed from: x */
    public static final ASN1ObjectIdentifier f12055x = f12032a.mo28199a("31");

    /* renamed from: y */
    public static final ASN1ObjectIdentifier f12056y = f12032a.mo28199a("32");

    /* renamed from: z */
    public static final ASN1ObjectIdentifier f12057z = f12032a.mo28199a("33");

    static {
        f12031I.mo28199a("11.0");
        f12031I.mo28199a("11.1");
        f12031I.mo28199a("11.2");
        f12031I.mo28199a("11.3");
        f12031I.mo28199a("14.0");
        f12031I.mo28199a("14.1");
        f12031I.mo28199a("14.2");
        f12031I.mo28199a("14.3");
        f12031I.mo28199a("15.0");
        f12031I.mo28199a("15.1");
        f12031I.mo28199a("15.2");
        f12031I.mo28199a("15.3");
        f12031I.mo28199a("16.0");
        f12031I.mo28199a("16.1");
        f12031I.mo28199a("16.2");
        f12031I.mo28199a("16.3");
    }
}
