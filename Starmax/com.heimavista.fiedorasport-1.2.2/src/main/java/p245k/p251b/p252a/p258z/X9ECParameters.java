package p245k.p251b.p252a.p258z;

import java.math.BigInteger;
import p245k.p251b.p252a.ASN1EncodableVector;
import p245k.p251b.p252a.ASN1Integer;
import p245k.p251b.p252a.ASN1Object;
import p245k.p251b.p252a.ASN1Primitive;
import p245k.p251b.p252a.DERSequence;
import p245k.p251b.p263d.p264a.ECAlgorithms;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p270b.PolynomialExtensionField;

/* renamed from: k.b.a.z.d */
public class X9ECParameters extends ASN1Object implements X9ObjectIdentifiers {

    /* renamed from: V */
    private static final BigInteger f12124V = BigInteger.valueOf(1);

    /* renamed from: P */
    private X9FieldID f12125P;

    /* renamed from: Q */
    private ECCurve f12126Q;

    /* renamed from: R */
    private X9ECPoint f12127R;

    /* renamed from: S */
    private BigInteger f12128S;

    /* renamed from: T */
    private BigInteger f12129T;

    /* renamed from: U */
    private byte[] f12130U;

    public X9ECParameters(ECCurve cVar, X9ECPoint fVar, BigInteger bigInteger, BigInteger bigInteger2) {
        this(cVar, fVar, bigInteger, bigInteger2, null);
    }

    /* renamed from: a */
    public ASN1Primitive mo28190a() {
        ASN1EncodableVector cVar = new ASN1EncodableVector();
        cVar.mo28193a(new ASN1Integer(f12124V));
        cVar.mo28193a(this.f12125P);
        cVar.mo28193a(new X9Curve(this.f12126Q, this.f12130U));
        cVar.mo28193a(this.f12127R);
        cVar.mo28193a(new ASN1Integer(this.f12128S));
        BigInteger bigInteger = this.f12129T;
        if (bigInteger != null) {
            cVar.mo28193a(new ASN1Integer(bigInteger));
        }
        return new DERSequence(cVar);
    }

    /* renamed from: b */
    public ECCurve mo28220b() {
        return this.f12126Q;
    }

    /* renamed from: c */
    public ECPoint mo28221c() {
        return this.f12127R.mo28226b();
    }

    /* renamed from: d */
    public BigInteger mo28222d() {
        return this.f12129T;
    }

    /* renamed from: e */
    public BigInteger mo28223e() {
        return this.f12128S;
    }

    /* renamed from: f */
    public byte[] mo28224f() {
        return this.f12130U;
    }

    public X9ECParameters(ECCurve cVar, X9ECPoint fVar, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        this.f12126Q = cVar;
        this.f12127R = fVar;
        this.f12128S = bigInteger;
        this.f12129T = bigInteger2;
        this.f12130U = bArr;
        if (ECAlgorithms.m18656b(cVar)) {
            this.f12125P = new X9FieldID(cVar.mo28255g().mo28394c());
        } else if (ECAlgorithms.m18654a(cVar)) {
            int[] a = ((PolynomialExtensionField) cVar.mo28255g()).mo28399a().mo28395a();
            if (a.length == 3) {
                this.f12125P = new X9FieldID(a[2], a[1]);
            } else if (a.length == 5) {
                this.f12125P = new X9FieldID(a[4], a[1], a[2], a[3]);
            } else {
                throw new IllegalArgumentException("Only trinomial and pentomial curves are supported");
            }
        } else {
            throw new IllegalArgumentException("'curve' is of an unsupported type");
        }
    }
}
