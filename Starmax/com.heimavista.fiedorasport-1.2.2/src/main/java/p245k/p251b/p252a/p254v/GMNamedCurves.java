package p245k.p251b.p252a.p254v;

import java.math.BigInteger;
import java.util.Hashtable;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p258z.X9ECParameters;
import p245k.p251b.p252a.p258z.X9ECParametersHolder;
import p245k.p251b.p252a.p258z.X9ECPoint;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p272e.Strings;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.a.v.a */
public class GMNamedCurves {

    /* renamed from: a */
    static X9ECParametersHolder f11975a = new C5054a();

    /* renamed from: b */
    static X9ECParametersHolder f11976b = new C5055b();

    /* renamed from: c */
    static final Hashtable f11977c = new Hashtable();

    /* renamed from: d */
    static final Hashtable f11978d = new Hashtable();

    /* renamed from: e */
    static final Hashtable f11979e = new Hashtable();

    /* renamed from: k.b.a.v.a$a */
    /* compiled from: GMNamedCurves */
    static class C5054a extends X9ECParametersHolder {
        C5054a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            BigInteger a = GMNamedCurves.m18489b("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF");
            BigInteger a2 = GMNamedCurves.m18489b("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC");
            BigInteger a3 = GMNamedCurves.m18489b("28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93");
            BigInteger a4 = GMNamedCurves.m18489b("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123");
            BigInteger valueOf = BigInteger.valueOf(1);
            ECCurve.C5163e eVar = new ECCurve.C5163e(a, a2, a3, a4, valueOf);
            ECCurve unused = GMNamedCurves.m18490b(eVar);
            return new X9ECParameters(eVar, new X9ECPoint(eVar, Hex.m20078a("0432C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0")), a4, valueOf, null);
        }
    }

    /* renamed from: k.b.a.v.a$b */
    /* compiled from: GMNamedCurves */
    static class C5055b extends X9ECParametersHolder {
        C5055b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            BigInteger a = GMNamedCurves.m18489b("BDB6F4FE3E8B1D9E0DA8C0D46F4C318CEFE4AFE3B6B8551F");
            BigInteger a2 = GMNamedCurves.m18489b("BB8E5E8FBC115E139FE6A814FE48AAA6F0ADA1AA5DF91985");
            BigInteger a3 = GMNamedCurves.m18489b("1854BEBDC31B21B7AEFC80AB0ECD10D5B1B3308E6DBF11C1");
            BigInteger a4 = GMNamedCurves.m18489b("BDB6F4FE3E8B1D9E0DA8C0D40FC962195DFAE76F56564677");
            BigInteger valueOf = BigInteger.valueOf(1);
            ECCurve.C5163e eVar = new ECCurve.C5163e(a, a2, a3, a4, valueOf);
            ECCurve unused = GMNamedCurves.m18490b(eVar);
            return new X9ECParameters(eVar, new X9ECPoint(eVar, Hex.m20078a("044AD5F7048DE709AD51236DE65E4D4B482C836DC6E410664002BB3A02D4AAADACAE24817A4CA3A1B014B5270432DB27D2")), a4, valueOf, null);
        }
    }

    static {
        m18488a("wapip192v1", GMObjectIdentifiers.f11983d, f11976b);
        m18488a("sm2p256v1", GMObjectIdentifiers.f11981b, f11975a);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static BigInteger m18489b(String str) {
        return new BigInteger(1, Hex.m20078a(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static ECCurve m18490b(ECCurve cVar) {
        return cVar;
    }

    /* renamed from: c */
    public static X9ECParameters m18491c(String str) {
        ASN1ObjectIdentifier d = m18492d(str);
        if (d == null) {
            return null;
        }
        return m18486a(d);
    }

    /* renamed from: d */
    public static ASN1ObjectIdentifier m18492d(String str) {
        return (ASN1ObjectIdentifier) f11977c.get(Strings.m20072a(str));
    }

    /* renamed from: a */
    static void m18488a(String str, ASN1ObjectIdentifier fVar, X9ECParametersHolder eVar) {
        f11977c.put(Strings.m20072a(str), fVar);
        f11979e.put(fVar, str);
        f11978d.put(fVar, eVar);
    }

    /* renamed from: a */
    public static X9ECParameters m18486a(ASN1ObjectIdentifier fVar) {
        X9ECParametersHolder eVar = (X9ECParametersHolder) f11978d.get(fVar);
        if (eVar == null) {
            return null;
        }
        return eVar.mo28225b();
    }
}
