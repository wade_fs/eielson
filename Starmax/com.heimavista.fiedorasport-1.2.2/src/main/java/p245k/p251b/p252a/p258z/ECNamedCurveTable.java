package p245k.p251b.p252a.p258z;

import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p253u.ANSSINamedCurves;
import p245k.p251b.p252a.p254v.GMNamedCurves;
import p245k.p251b.p252a.p255w.NISTNamedCurves;
import p245k.p251b.p252a.p256x.SECNamedCurves;
import p245k.p251b.p252a.p257y.TeleTrusTNamedCurves;

/* renamed from: k.b.a.z.a */
public class ECNamedCurveTable {
    /* renamed from: a */
    public static X9ECParameters m18560a(String str) {
        X9ECParameters a = X962NamedCurves.m18562a(str);
        if (a == null) {
            a = SECNamedCurves.m18506c(str);
        }
        if (a == null) {
            a = NISTNamedCurves.m18495a(str);
        }
        if (a == null) {
            a = TeleTrusTNamedCurves.m18541a(str);
        }
        if (a == null) {
            a = ANSSINamedCurves.m18482c(str);
        }
        return a == null ? GMNamedCurves.m18491c(str) : a;
    }

    /* renamed from: a */
    public static X9ECParameters m18561a(ASN1ObjectIdentifier fVar) {
        X9ECParameters a = X962NamedCurves.m18563a(fVar);
        if (a == null) {
            a = SECNamedCurves.m18499a(fVar);
        }
        if (a == null) {
            a = TeleTrusTNamedCurves.m18542a(fVar);
        }
        if (a == null) {
            a = ANSSINamedCurves.m18477a(fVar);
        }
        return a == null ? GMNamedCurves.m18486a(fVar) : a;
    }
}
