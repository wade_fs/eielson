package p245k.p251b.p252a.p258z;

import p245k.p251b.p252a.ASN1Object;
import p245k.p251b.p252a.ASN1Primitive;
import p245k.p251b.p252a.DEROctetString;
import p245k.p251b.p263d.p264a.ECFieldElement;

/* renamed from: k.b.a.z.g */
public class X9FieldElement extends ASN1Object {

    /* renamed from: Q */
    private static X9IntegerConverter f12135Q = new X9IntegerConverter();

    /* renamed from: P */
    protected ECFieldElement f12136P;

    public X9FieldElement(ECFieldElement dVar) {
        this.f12136P = dVar;
    }

    /* renamed from: a */
    public ASN1Primitive mo28190a() {
        return new DEROctetString(f12135Q.mo28228a(this.f12136P.mo28279k(), f12135Q.mo28227a(this.f12136P)));
    }
}
