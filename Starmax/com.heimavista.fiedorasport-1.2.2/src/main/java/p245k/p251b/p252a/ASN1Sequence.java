package p245k.p251b.p252a;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Vector;
import p245k.p251b.p272e.Arrays;
import p245k.p251b.p272e.Iterable;

/* renamed from: k.b.a.l */
public abstract class ASN1Sequence extends ASN1Primitive implements Iterable<ASN1Encodable> {

    /* renamed from: P */
    protected Vector f11966P = new Vector();

    protected ASN1Sequence() {
    }

    /* renamed from: a */
    public ASN1Encodable mo28211a(int i) {
        return (ASN1Encodable) this.f11966P.elementAt(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public ASN1Primitive mo28184c() {
        DERSequence qVar = new DERSequence();
        qVar.f11966P = this.f11966P;
        return super;
    }

    /* renamed from: d */
    public Enumeration mo28212d() {
        return this.f11966P.elements();
    }

    /* renamed from: e */
    public int mo28213e() {
        return this.f11966P.size();
    }

    /* renamed from: f */
    public ASN1Encodable[] mo28214f() {
        ASN1Encodable[] bVarArr = new ASN1Encodable[mo28213e()];
        for (int i = 0; i != mo28213e(); i++) {
            bVarArr[i] = mo28211a(i);
        }
        return bVarArr;
    }

    public int hashCode() {
        Enumeration d = mo28212d();
        int e = mo28213e();
        while (d.hasMoreElements()) {
            e = (e * 17) ^ m18457a(d).hashCode();
        }
        return e;
    }

    public Iterator<ASN1Encodable> iterator() {
        return new Arrays.C5170a(mo28214f());
    }

    public String toString() {
        return this.f11966P.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo28183a(ASN1Primitive kVar) {
        if (!(kVar instanceof ASN1Sequence)) {
            return false;
        }
        ASN1Sequence lVar = (ASN1Sequence) kVar;
        if (mo28213e() != lVar.mo28213e()) {
            return false;
        }
        Enumeration d = mo28212d();
        Enumeration d2 = lVar.mo28212d();
        while (d.hasMoreElements()) {
            ASN1Encodable a = m18457a(d);
            ASN1Encodable a2 = m18457a(d2);
            ASN1Primitive a3 = a.mo28190a();
            ASN1Primitive a4 = a2.mo28190a();
            if (a3 != a4 && !super.equals(a4)) {
                return false;
            }
        }
        return true;
    }

    protected ASN1Sequence(ASN1EncodableVector cVar) {
        for (int i = 0; i != cVar.mo28191a(); i++) {
            this.f11966P.addElement(cVar.mo28192a(i));
        }
    }

    /* renamed from: a */
    private ASN1Encodable m18457a(Enumeration enumeration) {
        return (ASN1Encodable) enumeration.nextElement();
    }
}
