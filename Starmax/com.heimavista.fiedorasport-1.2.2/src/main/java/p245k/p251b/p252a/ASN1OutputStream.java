package p245k.p251b.p252a;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: k.b.a.i */
public class ASN1OutputStream {

    /* renamed from: a */
    private OutputStream f11964a;

    public ASN1OutputStream(OutputStream outputStream) {
        this.f11964a = outputStream;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28205a(int i) {
        this.f11964a.write(i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo28209b(int i) {
        if (i > 127) {
            int i2 = i;
            int i3 = 1;
            while (true) {
                i2 >>>= 8;
                if (i2 == 0) {
                    break;
                }
                i3++;
            }
            mo28205a((byte) (i3 | 128));
            for (int i4 = (i3 - 1) * 8; i4 >= 0; i4 -= 8) {
                mo28205a((byte) (i >> i4));
            }
            return;
        }
        mo28205a((byte) i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28208a(byte[] bArr) {
        this.f11964a.write(bArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28206a(int i, byte[] bArr) {
        mo28205a(i);
        mo28209b(bArr.length);
        mo28208a(bArr);
    }

    /* renamed from: a */
    public void mo28207a(ASN1Encodable bVar) {
        if (bVar != null) {
            bVar.mo28190a().mo28194a(this);
            return;
        }
        throw new IOException("null object detected");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ASN1OutputStream mo28204a() {
        return new DEROutputStream(this.f11964a);
    }
}
