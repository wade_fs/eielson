package p245k.p251b.p252a;

/* renamed from: k.b.a.k */
public abstract class ASN1Primitive extends ASN1Object {
    ASN1Primitive() {
    }

    /* renamed from: a */
    public ASN1Primitive mo28190a() {
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo28194a(ASN1OutputStream iVar);

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract boolean mo28183a(ASN1Primitive kVar);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract int mo28195b();

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public ASN1Primitive mo28184c() {
        return this;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return (obj instanceof ASN1Encodable) && mo28183a(((ASN1Encodable) obj).mo28190a());
    }
}
