package p245k.p251b.p252a.p258z;

import p245k.p251b.p252a.ASN1Object;
import p245k.p251b.p252a.ASN1OctetString;
import p245k.p251b.p252a.ASN1Primitive;
import p245k.p251b.p252a.DEROctetString;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.a.z.f */
public class X9ECPoint extends ASN1Object {

    /* renamed from: P */
    private final ASN1OctetString f12132P;

    /* renamed from: Q */
    private ECCurve f12133Q;

    /* renamed from: R */
    private ECPoint f12134R;

    public X9ECPoint(ECCurve cVar, byte[] bArr) {
        this.f12133Q = cVar;
        this.f12132P = new DEROctetString(Arrays.m20060a(bArr));
    }

    /* renamed from: a */
    public ASN1Primitive mo28190a() {
        return this.f12132P;
    }

    /* renamed from: b */
    public synchronized ECPoint mo28226b() {
        if (this.f12134R == null) {
            this.f12134R = this.f12133Q.mo28241a(this.f12132P.mo28202d()).mo28313n();
        }
        return this.f12134R;
    }
}
