package p245k.p251b.p252a;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: k.b.a.f */
public class ASN1ObjectIdentifier extends ASN1Primitive {

    /* renamed from: P */
    private final String f11961P;

    /* renamed from: Q */
    private byte[] f11962Q;

    static {
        new ConcurrentHashMap();
    }

    public ASN1ObjectIdentifier(String str) {
        if (str == null) {
            throw new IllegalArgumentException("'identifier' cannot be null");
        } else if (m18436b(str)) {
            this.f11961P = str;
        } else {
            throw new IllegalArgumentException("string " + str + " not an OID");
        }
    }

    /* renamed from: e */
    private synchronized byte[] m18437e() {
        if (this.f11962Q == null) {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            m18432a(byteArrayOutputStream);
            this.f11962Q = byteArrayOutputStream.toByteArray();
        }
        return this.f11962Q;
    }

    /* renamed from: a */
    public ASN1ObjectIdentifier mo28199a(String str) {
        return new ASN1ObjectIdentifier(this, str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo28195b() {
        int length = m18437e().length;
        return StreamUtil.m18475a(length) + 1 + length;
    }

    /* renamed from: d */
    public String mo28200d() {
        return this.f11961P;
    }

    public int hashCode() {
        return this.f11961P.hashCode();
    }

    public String toString() {
        return mo28200d();
    }

    /* renamed from: a */
    private void m18433a(ByteArrayOutputStream byteArrayOutputStream, long j) {
        byte[] bArr = new byte[9];
        int i = 8;
        bArr[8] = (byte) (((int) j) & 127);
        while (j >= 128) {
            j >>= 7;
            i--;
            bArr[i] = (byte) ((((int) j) & 127) | 128);
        }
        byteArrayOutputStream.write(bArr, i, 9 - i);
    }

    /* renamed from: b */
    private static boolean m18436b(String str) {
        char charAt;
        if (str.length() < 3 || str.charAt(1) != '.' || (charAt = str.charAt(0)) < '0' || charAt > '2') {
            return false;
        }
        return m18435a(str, 2);
    }

    /* renamed from: a */
    private void m18434a(ByteArrayOutputStream byteArrayOutputStream, BigInteger bigInteger) {
        int bitLength = (bigInteger.bitLength() + 6) / 7;
        if (bitLength == 0) {
            byteArrayOutputStream.write(0);
            return;
        }
        byte[] bArr = new byte[bitLength];
        int i = bitLength - 1;
        BigInteger bigInteger2 = bigInteger;
        for (int i2 = i; i2 >= 0; i2--) {
            bArr[i2] = (byte) ((bigInteger2.intValue() & 127) | 128);
            bigInteger2 = bigInteger2.shiftRight(7);
        }
        bArr[i] = (byte) (bArr[i] & Byte.MAX_VALUE);
        byteArrayOutputStream.write(bArr, 0, bArr.length);
    }

    ASN1ObjectIdentifier(ASN1ObjectIdentifier fVar, String str) {
        if (m18435a(str, 0)) {
            this.f11961P = fVar.mo28200d() + "." + str;
            return;
        }
        throw new IllegalArgumentException("string " + str + " not a valid OID branch");
    }

    /* renamed from: a */
    private void m18432a(ByteArrayOutputStream byteArrayOutputStream) {
        OIDTokenizer sVar = new OIDTokenizer(this.f11961P);
        int parseInt = Integer.parseInt(sVar.mo28218b()) * 40;
        String b = sVar.mo28218b();
        if (b.length() <= 18) {
            m18433a(byteArrayOutputStream, ((long) parseInt) + Long.parseLong(b));
        } else {
            m18434a(byteArrayOutputStream, new BigInteger(b).add(BigInteger.valueOf((long) parseInt)));
        }
        while (sVar.mo28217a()) {
            String b2 = sVar.mo28218b();
            if (b2.length() <= 18) {
                m18433a(byteArrayOutputStream, Long.parseLong(b2));
            } else {
                m18434a(byteArrayOutputStream, new BigInteger(b2));
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28194a(ASN1OutputStream iVar) {
        byte[] e = m18437e();
        iVar.mo28205a(6);
        iVar.mo28209b(e.length);
        iVar.mo28208a(e);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo28183a(ASN1Primitive kVar) {
        if (kVar == this) {
            return true;
        }
        if (!(kVar instanceof ASN1ObjectIdentifier)) {
            return false;
        }
        return this.f11961P.equals(((ASN1ObjectIdentifier) kVar).f11961P);
    }

    /* renamed from: a */
    private static boolean m18435a(String str, int i) {
        boolean z;
        char charAt;
        int length = str.length();
        do {
            z = false;
            while (true) {
                length--;
                if (length < i) {
                    return z;
                }
                charAt = str.charAt(length);
                if ('0' <= charAt && charAt <= '9') {
                    z = true;
                }
            }
            if (charAt != '.') {
                break;
            }
        } while (z);
        return false;
    }
}
