package p245k.p251b.p252a;

/* renamed from: k.b.a.o */
public class DEROctetString extends ASN1OctetString {
    public DEROctetString(byte[] bArr) {
        super(bArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28194a(ASN1OutputStream iVar) {
        iVar.mo28206a(4, super.f11963P);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo28195b() {
        return StreamUtil.m18475a(super.f11963P.length) + 1 + super.f11963P.length;
    }
}
