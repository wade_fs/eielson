package p245k.p251b.p252a.p254v;

import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;
import p245k.p251b.p252a.ASN1ObjectIdentifier;

/* renamed from: k.b.a.v.b */
public interface GMObjectIdentifiers {

    /* renamed from: a */
    public static final ASN1ObjectIdentifier f11980a = new ASN1ObjectIdentifier("1.2.156.10197.1");

    /* renamed from: b */
    public static final ASN1ObjectIdentifier f11981b = f11980a.mo28199a("301");

    /* renamed from: c */
    public static final ASN1ObjectIdentifier f11982c = f11980a.mo28199a("301.3");

    /* renamed from: d */
    public static final ASN1ObjectIdentifier f11983d = f11980a.mo28199a("301.101");

    /* renamed from: e */
    public static final ASN1ObjectIdentifier f11984e = f11980a.mo28199a("401");

    static {
        f11980a.mo28199a("101.1");
        f11980a.mo28199a("101.2");
        f11980a.mo28199a("101.3");
        f11980a.mo28199a("101.4");
        f11980a.mo28199a("102.1");
        f11980a.mo28199a("102.2");
        f11980a.mo28199a("102.3");
        f11980a.mo28199a("102.4");
        f11980a.mo28199a("102.5");
        f11980a.mo28199a("102.6");
        f11980a.mo28199a("103.1");
        f11980a.mo28199a("103.2");
        f11980a.mo28199a("103.3");
        f11980a.mo28199a("103.4");
        f11980a.mo28199a("103.5");
        f11980a.mo28199a("103.6");
        f11980a.mo28199a("104.1");
        f11980a.mo28199a("104.2");
        f11980a.mo28199a("104.3");
        f11980a.mo28199a("104.4");
        f11980a.mo28199a("104.5");
        f11980a.mo28199a("104.6");
        f11980a.mo28199a("104.7");
        f11980a.mo28199a("104.8");
        f11980a.mo28199a("104.9");
        f11980a.mo28199a("104.10");
        f11980a.mo28199a("104.11");
        f11980a.mo28199a("104.12");
        f11980a.mo28199a("104.100");
        f11980a.mo28199a("201");
        f11980a.mo28199a("301.1");
        f11980a.mo28199a("301.2");
        f11982c.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f11982c.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
        f11982c.mo28199a("2.1");
        f11982c.mo28199a("2.2");
        f11982c.mo28199a("2.3");
        f11982c.mo28199a("2.4");
        f11982c.mo28199a("2.5");
        f11982c.mo28199a("2.6");
        f11982c.mo28199a("2.7");
        f11982c.mo28199a("2.8");
        f11982c.mo28199a("2.9");
        f11982c.mo28199a("2.10");
        f11982c.mo28199a("2.11");
        f11980a.mo28199a("302");
        f11980a.mo28199a("302.1");
        f11980a.mo28199a("302.2");
        f11980a.mo28199a("302.3");
        f11984e.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
        f11980a.mo28199a("501");
        f11980a.mo28199a("502");
        f11980a.mo28199a("503");
        f11980a.mo28199a("504");
        f11980a.mo28199a("505");
        f11980a.mo28199a("506");
        f11980a.mo28199a("507");
        f11980a.mo28199a("520");
        f11980a.mo28199a("521");
        f11980a.mo28199a("522");
    }
}
