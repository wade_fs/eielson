package p245k.p251b.p252a;

/* renamed from: k.b.a.t */
class StreamUtil {
    static {
        Runtime.getRuntime().maxMemory();
    }

    /* renamed from: a */
    static int m18475a(int i) {
        int i2 = 1;
        if (i > 127) {
            int i3 = 1;
            while (true) {
                i >>>= 8;
                if (i == 0) {
                    break;
                }
                i3++;
            }
            for (int i4 = (i3 - 1) * 8; i4 >= 0; i4 -= 8) {
                i2++;
            }
        }
        return i2;
    }
}
