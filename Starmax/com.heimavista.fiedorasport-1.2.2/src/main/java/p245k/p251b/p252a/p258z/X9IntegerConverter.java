package p245k.p251b.p252a.p258z;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;

/* renamed from: k.b.a.z.i */
public class X9IntegerConverter {
    /* renamed from: a */
    public int mo28227a(ECFieldElement dVar) {
        return (dVar.mo28269c() + 7) / 8;
    }

    /* renamed from: a */
    public byte[] mo28228a(BigInteger bigInteger, int i) {
        byte[] byteArray = bigInteger.toByteArray();
        if (i < byteArray.length) {
            byte[] bArr = new byte[i];
            System.arraycopy(byteArray, byteArray.length - bArr.length, bArr, 0, bArr.length);
            return bArr;
        } else if (i <= byteArray.length) {
            return byteArray;
        } else {
            byte[] bArr2 = new byte[i];
            System.arraycopy(byteArray, 0, bArr2, bArr2.length - byteArray.length, byteArray.length);
            return bArr2;
        }
    }
}
