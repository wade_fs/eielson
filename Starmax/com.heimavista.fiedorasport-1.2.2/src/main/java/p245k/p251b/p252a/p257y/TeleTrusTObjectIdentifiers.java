package p245k.p251b.p252a.p257y;

import androidx.exifinterface.media.ExifInterface;
import com.facebook.appevents.AppEventsConstants;
import p245k.p251b.p252a.ASN1ObjectIdentifier;

/* renamed from: k.b.a.y.b */
public interface TeleTrusTObjectIdentifiers {

    /* renamed from: a */
    public static final ASN1ObjectIdentifier f12075a = new ASN1ObjectIdentifier("1.3.36.3");

    /* renamed from: b */
    public static final ASN1ObjectIdentifier f12076b = f12075a.mo28199a("3.1");

    /* renamed from: c */
    public static final ASN1ObjectIdentifier f12077c = f12075a.mo28199a("3.2");

    /* renamed from: d */
    public static final ASN1ObjectIdentifier f12078d = f12075a.mo28199a("3.2.8");

    /* renamed from: e */
    public static final ASN1ObjectIdentifier f12079e = f12078d.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: f */
    public static final ASN1ObjectIdentifier f12080f = f12079e.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: g */
    public static final ASN1ObjectIdentifier f12081g = f12080f.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);

    /* renamed from: h */
    public static final ASN1ObjectIdentifier f12082h = f12080f.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);

    /* renamed from: i */
    public static final ASN1ObjectIdentifier f12083i = f12080f.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);

    /* renamed from: j */
    public static final ASN1ObjectIdentifier f12084j = f12080f.mo28199a("4");

    /* renamed from: k */
    public static final ASN1ObjectIdentifier f12085k = f12080f.mo28199a("5");

    /* renamed from: l */
    public static final ASN1ObjectIdentifier f12086l = f12080f.mo28199a("6");

    /* renamed from: m */
    public static final ASN1ObjectIdentifier f12087m = f12080f.mo28199a("7");

    /* renamed from: n */
    public static final ASN1ObjectIdentifier f12088n = f12080f.mo28199a("8");

    /* renamed from: o */
    public static final ASN1ObjectIdentifier f12089o = f12080f.mo28199a("9");

    /* renamed from: p */
    public static final ASN1ObjectIdentifier f12090p = f12080f.mo28199a("10");

    /* renamed from: q */
    public static final ASN1ObjectIdentifier f12091q = f12080f.mo28199a("11");

    /* renamed from: r */
    public static final ASN1ObjectIdentifier f12092r = f12080f.mo28199a("12");

    /* renamed from: s */
    public static final ASN1ObjectIdentifier f12093s = f12080f.mo28199a("13");

    /* renamed from: t */
    public static final ASN1ObjectIdentifier f12094t = f12080f.mo28199a("14");

    static {
        f12075a.mo28199a("2.1");
        f12075a.mo28199a("2.2");
        f12075a.mo28199a("2.3");
        f12076b.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
        f12076b.mo28199a(ExifInterface.GPS_MEASUREMENT_3D);
        f12076b.mo28199a("4");
        f12077c.mo28199a(AppEventsConstants.EVENT_PARAM_VALUE_YES);
        f12077c.mo28199a(ExifInterface.GPS_MEASUREMENT_2D);
    }
}
