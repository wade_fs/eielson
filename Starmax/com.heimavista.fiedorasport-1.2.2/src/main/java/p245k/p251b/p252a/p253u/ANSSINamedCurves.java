package p245k.p251b.p252a.p253u;

import java.math.BigInteger;
import java.util.Hashtable;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p258z.X9ECParameters;
import p245k.p251b.p252a.p258z.X9ECParametersHolder;
import p245k.p251b.p252a.p258z.X9ECPoint;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p272e.Strings;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.a.u.a */
public class ANSSINamedCurves {

    /* renamed from: a */
    static X9ECParametersHolder f11970a = new C5053a();

    /* renamed from: b */
    static final Hashtable f11971b = new Hashtable();

    /* renamed from: c */
    static final Hashtable f11972c = new Hashtable();

    /* renamed from: d */
    static final Hashtable f11973d = new Hashtable();

    /* renamed from: k.b.a.u.a$a */
    /* compiled from: ANSSINamedCurves */
    static class C5053a extends X9ECParametersHolder {
        C5053a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            BigInteger a = ANSSINamedCurves.m18480b("F1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C03");
            BigInteger a2 = ANSSINamedCurves.m18480b("F1FD178C0B3AD58F10126DE8CE42435B3961ADBCABC8CA6DE8FCF353D86E9C00");
            BigInteger a3 = ANSSINamedCurves.m18480b("EE353FCA5428A9300D4ABA754A44C00FDFEC0C9AE4B1A1803075ED967B7BB73F");
            BigInteger a4 = ANSSINamedCurves.m18480b("F1FD178C0B3AD58F10126DE8CE42435B53DC67E140D2BF941FFDD459C6D655E1");
            BigInteger valueOf = BigInteger.valueOf(1);
            ECCurve.C5163e eVar = new ECCurve.C5163e(a, a2, a3, a4, valueOf);
            ECCurve unused = ANSSINamedCurves.m18481b(eVar);
            return new X9ECParameters(eVar, new X9ECPoint(eVar, Hex.m20078a("04B6B3D4C356C139EB31183D4749D423958C27D2DCAF98B70164C97A2DD98F5CFF6142E0F7C8B204911F9271F0F3ECEF8C2701C307E8E4C9E183115A1554062CFB")), a4, valueOf, null);
        }
    }

    static {
        m18479a("FRP256v1", ANSSIObjectIdentifiers.f11974a, f11970a);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static BigInteger m18480b(String str) {
        return new BigInteger(1, Hex.m20078a(str));
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static ECCurve m18481b(ECCurve cVar) {
        return cVar;
    }

    /* renamed from: c */
    public static X9ECParameters m18482c(String str) {
        ASN1ObjectIdentifier d = m18483d(str);
        if (d == null) {
            return null;
        }
        return m18477a(d);
    }

    /* renamed from: d */
    public static ASN1ObjectIdentifier m18483d(String str) {
        return (ASN1ObjectIdentifier) f11971b.get(Strings.m20072a(str));
    }

    /* renamed from: a */
    static void m18479a(String str, ASN1ObjectIdentifier fVar, X9ECParametersHolder eVar) {
        f11971b.put(Strings.m20072a(str), fVar);
        f11973d.put(fVar, str);
        f11972c.put(fVar, eVar);
    }

    /* renamed from: a */
    public static X9ECParameters m18477a(ASN1ObjectIdentifier fVar) {
        X9ECParametersHolder eVar = (X9ECParametersHolder) f11972c.get(fVar);
        if (eVar == null) {
            return null;
        }
        return eVar.mo28225b();
    }
}
