package p245k.p251b.p252a;

import java.util.Enumeration;

/* renamed from: k.b.a.q */
public class DERSequence extends ASN1Sequence {

    /* renamed from: Q */
    private int f11967Q = -1;

    public DERSequence() {
    }

    /* renamed from: g */
    private int m18470g() {
        if (this.f11967Q < 0) {
            int i = 0;
            Enumeration d = mo28212d();
            while (d.hasMoreElements()) {
                i += ((ASN1Encodable) d.nextElement()).mo28190a().mo28184c().mo28195b();
            }
            this.f11967Q = i;
        }
        return this.f11967Q;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28194a(ASN1OutputStream iVar) {
        ASN1OutputStream a = iVar.mo28204a();
        int g = m18470g();
        iVar.mo28205a(48);
        iVar.mo28209b(g);
        Enumeration d = mo28212d();
        while (d.hasMoreElements()) {
            a.mo28207a((ASN1Encodable) d.nextElement());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo28195b() {
        int g = m18470g();
        return StreamUtil.m18475a(g) + 1 + g;
    }

    public DERSequence(ASN1EncodableVector cVar) {
        super(cVar);
    }
}
