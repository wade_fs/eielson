package p245k.p251b.p252a;

/* renamed from: k.b.a.s */
public class OIDTokenizer {

    /* renamed from: a */
    private String f11968a;

    /* renamed from: b */
    private int f11969b = 0;

    public OIDTokenizer(String str) {
        this.f11968a = str;
    }

    /* renamed from: a */
    public boolean mo28217a() {
        return this.f11969b != -1;
    }

    /* renamed from: b */
    public String mo28218b() {
        int i = this.f11969b;
        if (i == -1) {
            return null;
        }
        int indexOf = this.f11968a.indexOf(46, i);
        if (indexOf == -1) {
            String substring = this.f11968a.substring(this.f11969b);
            this.f11969b = -1;
            return substring;
        }
        String substring2 = this.f11968a.substring(this.f11969b, indexOf);
        this.f11969b = indexOf + 1;
        return substring2;
    }
}
