package p245k.p251b.p252a;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: k.b.a.p */
public class DEROutputStream extends ASN1OutputStream {
    public DEROutputStream(OutputStream outputStream) {
        super(outputStream);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ASN1OutputStream mo28204a() {
        return super;
    }

    /* renamed from: a */
    public void mo28207a(ASN1Encodable bVar) {
        if (bVar != null) {
            bVar.mo28190a().mo28184c().mo28194a(super);
            return;
        }
        throw new IOException("null object detected");
    }
}
