package p245k.p251b.p252a;

/* renamed from: k.b.a.n */
public class DERBitString extends ASN1BitString {
    public DERBitString(byte[] bArr, int i) {
        super(bArr, i);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo28194a(ASN1OutputStream iVar) {
        byte[] a = ASN1BitString.m18417a(super.f11957P, super.f11958Q);
        byte[] bArr = new byte[(a.length + 1)];
        bArr[0] = (byte) mo28186e();
        System.arraycopy(a, 0, bArr, 1, bArr.length - 1);
        iVar.mo28206a(3, bArr);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo28195b() {
        return StreamUtil.m18475a(super.f11957P.length + 1) + 1 + super.f11957P.length + 1;
    }

    public DERBitString(byte[] bArr) {
        this(bArr, 0);
    }
}
