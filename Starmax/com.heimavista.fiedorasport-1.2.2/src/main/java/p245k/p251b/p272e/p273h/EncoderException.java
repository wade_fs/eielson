package p245k.p251b.p272e.p273h;

/* renamed from: k.b.e.h.c */
public class EncoderException extends IllegalStateException {

    /* renamed from: P */
    private Throwable f12391P;

    EncoderException(String str, Throwable th) {
        super(str);
        this.f12391P = th;
    }

    public Throwable getCause() {
        return this.f12391P;
    }
}
