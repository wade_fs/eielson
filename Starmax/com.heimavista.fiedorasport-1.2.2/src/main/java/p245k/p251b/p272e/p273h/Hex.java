package p245k.p251b.p272e.p273h;

import java.io.ByteArrayOutputStream;

/* renamed from: k.b.e.h.d */
public class Hex {

    /* renamed from: a */
    private static final C5172b f12392a = new HexEncoder();

    /* renamed from: a */
    public static byte[] m20079a(byte[] bArr) {
        return m20080a(bArr, 0, bArr.length);
    }

    /* renamed from: a */
    public static byte[] m20080a(byte[] bArr, int i, int i2) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            f12392a.mo28410a(bArr, i, i2, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            throw new EncoderException("exception encoding Hex string: " + e.getMessage(), e);
        }
    }

    /* renamed from: a */
    public static byte[] m20078a(String str) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            f12392a.mo28409a(str, byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
        } catch (Exception e) {
            throw new DecoderException("exception decoding Hex string: " + e.getMessage(), e);
        }
    }
}
