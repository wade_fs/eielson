package p245k.p251b.p272e;

/* renamed from: k.b.e.f */
public abstract class Pack {
    /* renamed from: a */
    public static void m20070a(int i, byte[] bArr, int i2) {
        bArr[i2] = (byte) (i >>> 24);
        int i3 = i2 + 1;
        bArr[i3] = (byte) (i >>> 16);
        int i4 = i3 + 1;
        bArr[i4] = (byte) (i >>> 8);
        bArr[i4 + 1] = (byte) i;
    }

    /* renamed from: a */
    public static void m20071a(long j, byte[] bArr, int i) {
        m20070a((int) (j >>> 32), bArr, i);
        m20070a((int) (j & 4294967295L), bArr, i + 4);
    }
}
