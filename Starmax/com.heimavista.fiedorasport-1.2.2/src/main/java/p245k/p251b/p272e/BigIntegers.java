package p245k.p251b.p272e;

import java.math.BigInteger;

/* renamed from: k.b.e.b */
public final class BigIntegers {
    static {
        BigInteger.valueOf(0);
    }

    /* renamed from: a */
    public static BigInteger m20068a(byte[] bArr, int i, int i2) {
        if (!(i == 0 && i2 == bArr.length)) {
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, i, bArr2, 0, i2);
            bArr = bArr2;
        }
        return new BigInteger(1, bArr);
    }
}
