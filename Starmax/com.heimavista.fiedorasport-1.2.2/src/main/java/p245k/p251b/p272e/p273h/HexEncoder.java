package p245k.p251b.p272e.p273h;

import java.io.IOException;
import java.io.OutputStream;

/* renamed from: k.b.e.h.e */
public class HexEncoder implements C5172b {

    /* renamed from: a */
    protected final byte[] f12393a = {48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102};

    /* renamed from: b */
    protected final byte[] f12394b = new byte[128];

    public HexEncoder() {
        mo28412a();
    }

    /* renamed from: a */
    private static boolean m20081a(char c) {
        return c == 10 || c == 13 || c == 9 || c == ' ';
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo28412a() {
        int i = 0;
        int i2 = 0;
        while (true) {
            byte[] bArr = this.f12394b;
            if (i2 >= bArr.length) {
                break;
            }
            bArr[i2] = -1;
            i2++;
        }
        while (true) {
            byte[] bArr2 = this.f12393a;
            if (i < bArr2.length) {
                this.f12394b[bArr2[i]] = (byte) i;
                i++;
            } else {
                byte[] bArr3 = this.f12394b;
                bArr3[65] = bArr3[97];
                bArr3[66] = bArr3[98];
                bArr3[67] = bArr3[99];
                bArr3[68] = bArr3[100];
                bArr3[69] = bArr3[101];
                bArr3[70] = bArr3[102];
                return;
            }
        }
    }

    /* renamed from: a */
    public int mo28410a(byte[] bArr, int i, int i2, OutputStream outputStream) {
        for (int i3 = i; i3 < i + i2; i3++) {
            byte b = bArr[i3] & 255;
            outputStream.write(this.f12393a[b >>> 4]);
            outputStream.write(this.f12393a[b & 15]);
        }
        return i2 * 2;
    }

    /* renamed from: a */
    public int mo28409a(String str, OutputStream outputStream) {
        int length = str.length();
        while (length > 0 && m20081a(str.charAt(length - 1))) {
            length--;
        }
        int i = 0;
        int i2 = 0;
        while (i < length) {
            while (i < length && m20081a(str.charAt(i))) {
                i++;
            }
            int i3 = i + 1;
            byte b = this.f12394b[str.charAt(i)];
            while (i3 < length && m20081a(str.charAt(i3))) {
                i3++;
            }
            int i4 = i3 + 1;
            byte b2 = this.f12394b[str.charAt(i3)];
            if ((b | b2) >= 0) {
                outputStream.write((b << 4) | b2);
                i2++;
                i = i4;
            } else {
                throw new IOException("invalid characters encountered in Hex string");
            }
        }
        return i2;
    }
}
