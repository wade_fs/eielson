package p245k.p251b.p272e;

import java.security.AccessController;
import java.security.PrivilegedAction;

/* renamed from: k.b.e.g */
public final class Strings {

    /* renamed from: k.b.e.g$a */
    /* compiled from: Strings */
    static class C5171a implements PrivilegedAction<String> {
        C5171a() {
        }

        public String run() {
            return System.getProperty("line.separator");
        }
    }

    static {
        try {
            String str = (String) AccessController.doPrivileged(new C5171a());
        } catch (Exception unused) {
            try {
                String.format("%n", new Object[0]);
            } catch (Exception unused2) {
            }
        }
    }

    /* renamed from: a */
    public static String m20072a(String str) {
        char[] charArray = str.toCharArray();
        boolean z = false;
        for (int i = 0; i != charArray.length; i++) {
            char c = charArray[i];
            if ('A' <= c && 'Z' >= c) {
                charArray[i] = (char) ((c - 'A') + 97);
                z = true;
            }
        }
        return z ? new String(charArray) : str;
    }

    /* renamed from: b */
    public static String m20074b(String str) {
        char[] charArray = str.toCharArray();
        boolean z = false;
        for (int i = 0; i != charArray.length; i++) {
            char c = charArray[i];
            if ('a' <= c && 'z' >= c) {
                charArray[i] = (char) ((c - 'a') + 65);
                z = true;
            }
        }
        return z ? new String(charArray) : str;
    }

    /* renamed from: a */
    public static char[] m20073a(byte[] bArr) {
        char[] cArr = new char[bArr.length];
        for (int i = 0; i != cArr.length; i++) {
            cArr[i] = (char) (bArr[i] & 255);
        }
        return cArr;
    }

    /* renamed from: b */
    public static String m20075b(byte[] bArr) {
        return new String(m20073a(bArr));
    }
}
