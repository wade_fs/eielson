package p245k.p251b.p272e.p273h;

/* renamed from: k.b.e.h.a */
public class DecoderException extends IllegalStateException {

    /* renamed from: P */
    private Throwable f12390P;

    DecoderException(String str, Throwable th) {
        super(str);
        this.f12390P = th;
    }

    public Throwable getCause() {
        return this.f12390P;
    }
}
