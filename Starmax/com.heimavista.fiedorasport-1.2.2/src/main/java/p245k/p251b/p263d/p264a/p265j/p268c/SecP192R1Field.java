package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat192;

/* renamed from: k.b.d.a.j.c.t */
public class SecP192R1Field {

    /* renamed from: a */
    static final int[] f12359a = {-1, -1, -2, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12360b = {1, 0, 2, 0, 1, 0, -2, -1, -3, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12361c = {-1, -1, -3, -1, -2, -1, 1, 0, 2};

    /* renamed from: a */
    public static void m19636a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat192.m19903a(iArr, iArr2, iArr3) != 0 || (iArr3[5] == -1 && Nat192.m19916b(iArr3, f12359a))) {
            m19633a(iArr3);
        }
    }

    /* renamed from: b */
    public static void m19640b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] c = Nat192.m19926c();
        Nat192.m19925c(iArr, iArr2, c);
        m19641c(c, iArr3);
    }

    /* renamed from: c */
    public static void m19642c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat192.m19928d(iArr, iArr2, iArr3) != 0 || (iArr3[11] == -1 && Nat.m20047c(12, iArr3, f12360b))) {
            int[] iArr4 = f12361c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(12, iArr3, f12361c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m19643d(int[] iArr, int[] iArr2) {
        int[] c = Nat192.m19926c();
        Nat192.m19924c(iArr, c);
        m19641c(c, iArr2);
    }

    /* renamed from: e */
    public static void m19645e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(6, iArr, 0, iArr2) != 0 || (iArr2[5] == -1 && Nat192.m19916b(iArr2, f12359a))) {
            m19633a(iArr2);
        }
    }

    /* renamed from: a */
    public static void m19635a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(6, iArr, iArr2) != 0 || (iArr2[5] == -1 && Nat192.m19916b(iArr2, f12359a))) {
            m19633a(iArr2);
        }
    }

    /* renamed from: b */
    public static void m19639b(int[] iArr, int[] iArr2) {
        if (Nat192.m19914b(iArr)) {
            Nat192.m19929d(iArr2);
        } else {
            Nat192.m19931e(f12359a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m19644d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat192.m19931e(iArr, iArr2, iArr3) != 0) {
            m19638b(iArr3);
        }
    }

    /* renamed from: c */
    public static void m19641c(int[] iArr, int[] iArr2) {
        int[] iArr3 = iArr2;
        long j = ((long) iArr[6]) & 4294967295L;
        long j2 = ((long) iArr[7]) & 4294967295L;
        long j3 = (((long) iArr[10]) & 4294967295L) + j;
        long j4 = (((long) iArr[11]) & 4294967295L) + j2;
        long j5 = j2;
        long j6 = (((long) iArr[0]) & 4294967295L) + j3 + 0;
        int i = (int) j6;
        long j7 = (j6 >> 32) + (((long) iArr[1]) & 4294967295L) + j4;
        iArr3[1] = (int) j7;
        long j8 = j3 + (((long) iArr[8]) & 4294967295L);
        long j9 = j4 + (((long) iArr[9]) & 4294967295L);
        long j10 = (j7 >> 32) + (((long) iArr[2]) & 4294967295L) + j8;
        long j11 = j10 & 4294967295L;
        long j12 = (j10 >> 32) + (((long) iArr[3]) & 4294967295L) + j9;
        iArr3[3] = (int) j12;
        long j13 = (j12 >> 32) + (((long) iArr[4]) & 4294967295L) + (j8 - j);
        iArr3[4] = (int) j13;
        long j14 = (j13 >> 32) + (((long) iArr[5]) & 4294967295L) + (j9 - j5);
        iArr3[5] = (int) j14;
        long j15 = j14 >> 32;
        long j16 = j11 + j15;
        long j17 = j15 + (((long) i) & 4294967295L);
        iArr3[0] = (int) j17;
        long j18 = j17 >> 32;
        if (j18 != 0) {
            long j19 = j18 + (4294967295L & ((long) iArr3[1]));
            iArr3[1] = (int) j19;
            j16 += j19 >> 32;
        }
        iArr3[2] = (int) j16;
        if (((j16 >> 32) != 0 && Nat.m20036b(6, iArr3, 3) != 0) || (iArr3[5] == -1 && Nat192.m19916b(iArr3, f12359a))) {
            m19633a(iArr2);
        }
    }

    /* renamed from: a */
    public static int[] m19637a(BigInteger bigInteger) {
        int[] a = Nat192.m19911a(bigInteger);
        if (a[5] == -1 && Nat192.m19916b(a, f12359a)) {
            Nat192.m19927d(f12359a, a);
        }
        return a;
    }

    /* renamed from: b */
    private static void m19638b(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) - 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            j2 = j3 >> 32;
        }
        long j4 = j2 + ((4294967295L & ((long) iArr[2])) - 1);
        iArr[2] = (int) j4;
        if ((j4 >> 32) != 0) {
            Nat.m20018a(6, iArr, 3);
        }
    }

    /* renamed from: a */
    public static void m19632a(int i, int[] iArr) {
        long j;
        if (i != 0) {
            long j2 = ((long) i) & 4294967295L;
            long j3 = (((long) iArr[0]) & 4294967295L) + j2 + 0;
            iArr[0] = (int) j3;
            long j4 = j3 >> 32;
            if (j4 != 0) {
                long j5 = j4 + (((long) iArr[1]) & 4294967295L);
                iArr[1] = (int) j5;
                j4 = j5 >> 32;
            }
            long j6 = j4 + (4294967295L & ((long) iArr[2])) + j2;
            iArr[2] = (int) j6;
            j = j6 >> 32;
        } else {
            j = 0;
        }
        if ((j != 0 && Nat.m20036b(6, iArr, 3) != 0) || (iArr[5] == -1 && Nat192.m19916b(iArr, f12359a))) {
            m19633a(iArr);
        }
    }

    /* renamed from: a */
    public static void m19634a(int[] iArr, int i, int[] iArr2) {
        int[] c = Nat192.m19926c();
        Nat192.m19924c(iArr, c);
        m19641c(c, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat192.m19924c(iArr2, c);
                m19641c(c, iArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private static void m19633a(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) + 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            j2 = j3 >> 32;
        }
        long j4 = j2 + (4294967295L & ((long) iArr[2])) + 1;
        iArr[2] = (int) j4;
        if ((j4 >> 32) != 0) {
            Nat.m20036b(6, iArr, 3);
        }
    }
}
