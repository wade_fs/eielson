package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat224;

/* renamed from: k.b.d.a.j.c.b0 */
public class SecP224R1Field {

    /* renamed from: a */
    static final int[] f12279a = {1, 0, 0, -1, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12280b = {1, 0, 0, -2, -1, -1, 0, 2, 0, 0, -2, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12281c = {-1, -1, -1, 1, 0, 0, -1, -3, -1, -1, 1};

    /* renamed from: a */
    public static void m18988a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat224.m19935a(iArr, iArr2, iArr3) != 0 || (iArr3[6] == -1 && Nat224.m19947c(iArr3, f12279a))) {
            m18985a(iArr3);
        }
    }

    /* renamed from: b */
    public static void m18992b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] b = Nat224.m19944b();
        Nat224.m19946c(iArr, iArr2, b);
        m18993c(b, iArr3);
    }

    /* renamed from: c */
    public static void m18994c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat224.m19948d(iArr, iArr2, iArr3) != 0 || (iArr3[13] == -1 && Nat.m20047c(14, iArr3, f12280b))) {
            int[] iArr4 = f12281c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(14, iArr3, f12281c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m18995d(int[] iArr, int[] iArr2) {
        int[] b = Nat224.m19944b();
        Nat224.m19950d(iArr, b);
        m18993c(b, iArr2);
    }

    /* renamed from: e */
    public static void m18997e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(7, iArr, 0, iArr2) != 0 || (iArr2[6] == -1 && Nat224.m19947c(iArr2, f12279a))) {
            m18985a(iArr2);
        }
    }

    /* renamed from: a */
    public static void m18987a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(7, iArr, iArr2) != 0 || (iArr2[6] == -1 && Nat224.m19947c(iArr2, f12279a))) {
            m18985a(iArr2);
        }
    }

    /* renamed from: b */
    public static void m18991b(int[] iArr, int[] iArr2) {
        if (Nat224.m19942b(iArr)) {
            Nat224.m19949d(iArr2);
        } else {
            Nat224.m19952e(f12279a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m18996d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat224.m19952e(iArr, iArr2, iArr3) != 0) {
            m18990b(iArr3);
        }
    }

    /* renamed from: c */
    public static void m18993c(int[] iArr, int[] iArr2) {
        int[] iArr3 = iArr2;
        long j = ((long) iArr[10]) & 4294967295L;
        long j2 = ((long) iArr[11]) & 4294967295L;
        long j3 = ((long) iArr[12]) & 4294967295L;
        long j4 = ((long) iArr[13]) & 4294967295L;
        long j5 = ((((long) iArr[7]) & 4294967295L) + j2) - 1;
        long j6 = (((long) iArr[8]) & 4294967295L) + j3;
        long j7 = j3;
        long j8 = (((long) iArr[9]) & 4294967295L) + j4;
        long j9 = j4;
        long j10 = ((((long) iArr[0]) & 4294967295L) - j5) + 0;
        long j11 = j10 & 4294967295L;
        long j12 = j2;
        long j13 = (j10 >> 32) + ((((long) iArr[1]) & 4294967295L) - j6);
        iArr3[1] = (int) j13;
        long j14 = (j13 >> 32) + ((((long) iArr[2]) & 4294967295L) - j8);
        iArr3[2] = (int) j14;
        long j15 = (j14 >> 32) + (((((long) iArr[3]) & 4294967295L) + j5) - j);
        long j16 = (j15 >> 32) + (((((long) iArr[4]) & 4294967295L) + j6) - j12);
        iArr3[4] = (int) j16;
        long j17 = (j16 >> 32) + (((((long) iArr[5]) & 4294967295L) + j8) - j7);
        iArr3[5] = (int) j17;
        long j18 = (j17 >> 32) + (((((long) iArr[6]) & 4294967295L) + j) - j9);
        iArr3[6] = (int) j18;
        long j19 = (j18 >> 32) + 1;
        long j20 = (j15 & 4294967295L) + j19;
        long j21 = j11 - j19;
        iArr3[0] = (int) j21;
        long j22 = j21 >> 32;
        if (j22 != 0) {
            long j23 = j22 + (((long) iArr3[1]) & 4294967295L);
            iArr3[1] = (int) j23;
            long j24 = (j23 >> 32) + (4294967295L & ((long) iArr3[2]));
            iArr3[2] = (int) j24;
            j20 += j24 >> 32;
        }
        iArr3[3] = (int) j20;
        if (((j20 >> 32) != 0 && Nat.m20036b(7, iArr3, 4) != 0) || (iArr3[6] == -1 && Nat224.m19947c(iArr3, f12279a))) {
            m18985a(iArr2);
        }
    }

    /* renamed from: a */
    public static int[] m18989a(BigInteger bigInteger) {
        int[] a = Nat224.m19940a(bigInteger);
        if (a[6] == -1 && Nat224.m19947c(a, f12279a)) {
            Nat224.m19951e(f12279a, a);
        }
        return a;
    }

    /* renamed from: b */
    private static void m18990b(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) + 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            long j4 = (j3 >> 32) + (((long) iArr[2]) & 4294967295L);
            iArr[2] = (int) j4;
            j2 = j4 >> 32;
        }
        long j5 = j2 + ((4294967295L & ((long) iArr[3])) - 1);
        iArr[3] = (int) j5;
        if ((j5 >> 32) != 0) {
            Nat.m20018a(7, iArr, 4);
        }
    }

    /* renamed from: a */
    public static void m18984a(int i, int[] iArr) {
        long j;
        if (i != 0) {
            long j2 = ((long) i) & 4294967295L;
            long j3 = ((((long) iArr[0]) & 4294967295L) - j2) + 0;
            iArr[0] = (int) j3;
            long j4 = j3 >> 32;
            if (j4 != 0) {
                long j5 = j4 + (((long) iArr[1]) & 4294967295L);
                iArr[1] = (int) j5;
                long j6 = (j5 >> 32) + (((long) iArr[2]) & 4294967295L);
                iArr[2] = (int) j6;
                j4 = j6 >> 32;
            }
            long j7 = j4 + (4294967295L & ((long) iArr[3])) + j2;
            iArr[3] = (int) j7;
            j = j7 >> 32;
        } else {
            j = 0;
        }
        if ((j != 0 && Nat.m20036b(7, iArr, 4) != 0) || (iArr[6] == -1 && Nat224.m19947c(iArr, f12279a))) {
            m18985a(iArr);
        }
    }

    /* renamed from: a */
    public static void m18986a(int[] iArr, int i, int[] iArr2) {
        int[] b = Nat224.m19944b();
        Nat224.m19950d(iArr, b);
        m18993c(b, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat224.m19950d(iArr2, b);
                m18993c(b, iArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private static void m18985a(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) - 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            long j4 = (j3 >> 32) + (((long) iArr[2]) & 4294967295L);
            iArr[2] = (int) j4;
            j2 = j4 >> 32;
        }
        long j5 = j2 + (4294967295L & ((long) iArr[3])) + 1;
        iArr[3] = (int) j5;
        if ((j5 >> 32) != 0) {
            Nat.m20036b(7, iArr, 4);
        }
    }
}
