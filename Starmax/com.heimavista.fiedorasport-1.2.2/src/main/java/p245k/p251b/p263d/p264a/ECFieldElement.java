package p245k.p251b.p263d.p264a;

import java.math.BigInteger;
import java.util.Random;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.d */
public abstract class ECFieldElement implements ECConstants {

    /* renamed from: k.b.d.a.d$a */
    /* compiled from: ECFieldElement */
    public static class C5164a extends ECFieldElement {

        /* renamed from: d */
        private int f12242d;

        /* renamed from: e */
        private int f12243e;

        /* renamed from: f */
        private int[] f12244f;

        /* renamed from: g */
        private LongArray f12245g;

        public C5164a(int i, int i2, int i3, int i4, BigInteger bigInteger) {
            if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > i) {
                throw new IllegalArgumentException("x value invalid in F2m field element");
            }
            if (i3 == 0 && i4 == 0) {
                this.f12242d = 2;
                this.f12244f = new int[]{i2};
            } else if (i3 >= i4) {
                throw new IllegalArgumentException("k2 must be smaller than k3");
            } else if (i3 > 0) {
                this.f12242d = 3;
                this.f12244f = new int[]{i2, i3, i4};
            } else {
                throw new IllegalArgumentException("k2 must be larger than 0");
            }
            this.f12243e = i;
            this.f12245g = new LongArray(bigInteger);
        }

        /* renamed from: a */
        public ECFieldElement mo28263a(ECFieldElement dVar) {
            LongArray hVar = (LongArray) this.f12245g.clone();
            hVar.mo28330a(((C5164a) dVar).f12245g, 0);
            return new C5164a(this.f12243e, this.f12244f, hVar);
        }

        /* renamed from: b */
        public int mo28266b() {
            return this.f12245g.mo28331b();
        }

        /* renamed from: c */
        public int mo28269c() {
            return this.f12243e;
        }

        /* renamed from: d */
        public ECFieldElement mo28272d(ECFieldElement dVar) {
            return mo28263a(super);
        }

        /* renamed from: e */
        public boolean mo28273e() {
            return this.f12245g.mo28338d();
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C5164a)) {
                return false;
            }
            C5164a aVar = (C5164a) obj;
            if (this.f12243e != aVar.f12243e || this.f12242d != aVar.f12242d || !Arrays.m20059a(this.f12244f, aVar.f12244f) || !this.f12245g.equals(aVar.f12245g)) {
                return false;
            }
            return true;
        }

        /* renamed from: f */
        public boolean mo28274f() {
            return this.f12245g.mo28339e();
        }

        /* renamed from: g */
        public ECFieldElement mo28275g() {
            return super;
        }

        /* renamed from: h */
        public ECFieldElement mo28276h() {
            return (this.f12245g.mo28339e() || this.f12245g.mo28338d()) ? super : mo28281a(this.f12243e - 1);
        }

        public int hashCode() {
            return (this.f12245g.hashCode() ^ this.f12243e) ^ Arrays.m20065b(this.f12244f);
        }

        /* renamed from: i */
        public ECFieldElement mo28277i() {
            int i = this.f12243e;
            int[] iArr = this.f12244f;
            return new C5164a(i, iArr, this.f12245g.mo28332b(i, iArr));
        }

        /* renamed from: j */
        public boolean mo28278j() {
            return this.f12245g.mo28341f();
        }

        /* renamed from: k */
        public BigInteger mo28279k() {
            return this.f12245g.mo28342g();
        }

        /* renamed from: b */
        public static void m18720b(ECFieldElement dVar, ECFieldElement dVar2) {
            if (!(dVar instanceof C5164a) || !(dVar2 instanceof C5164a)) {
                throw new IllegalArgumentException("Field elements are not both instances of ECFieldElement.F2m");
            }
            C5164a aVar = (C5164a) dVar;
            C5164a aVar2 = (C5164a) dVar2;
            if (aVar.f12242d != aVar2.f12242d) {
                throw new IllegalArgumentException("One of the F2m field elements has incorrect representation");
            } else if (aVar.f12243e != aVar2.f12243e || !Arrays.m20059a(aVar.f12244f, aVar2.f12244f)) {
                throw new IllegalArgumentException("Field elements are not elements of the same field F2m");
            }
        }

        /* renamed from: c */
        public ECFieldElement mo28270c(ECFieldElement dVar) {
            int i = this.f12243e;
            int[] iArr = this.f12244f;
            return new C5164a(i, iArr, this.f12245g.mo28329a(((C5164a) dVar).f12245g, i, iArr));
        }

        /* renamed from: d */
        public ECFieldElement mo28271d() {
            int i = this.f12243e;
            int[] iArr = this.f12244f;
            return new C5164a(i, iArr, this.f12245g.mo28328a(i, iArr));
        }

        /* renamed from: a */
        public ECFieldElement mo28262a() {
            return new C5164a(this.f12243e, this.f12244f, this.f12245g.mo28326a());
        }

        /* renamed from: a */
        public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
            return mo28268b(super, super, super);
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: k.b.d.a.h} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public p245k.p251b.p263d.p264a.ECFieldElement mo28264a(p245k.p251b.p263d.p264a.ECFieldElement r5, p245k.p251b.p263d.p264a.ECFieldElement r6) {
            /*
                r4 = this;
                k.b.d.a.h r0 = r4.f12245g
                k.b.d.a.d$a r5 = (p245k.p251b.p263d.p264a.ECFieldElement.C5164a) r5
                k.b.d.a.h r5 = r5.f12245g
                k.b.d.a.d$a r6 = (p245k.p251b.p263d.p264a.ECFieldElement.C5164a) r6
                k.b.d.a.h r6 = r6.f12245g
                int r1 = r4.f12243e
                int[] r2 = r4.f12244f
                k.b.d.a.h r1 = r0.mo28337d(r1, r2)
                int r2 = r4.f12243e
                int[] r3 = r4.f12244f
                k.b.d.a.h r5 = r5.mo28333b(r6, r2, r3)
                if (r1 != r0) goto L_0x0023
                java.lang.Object r6 = r1.clone()
                r1 = r6
                k.b.d.a.h r1 = (p245k.p251b.p263d.p264a.LongArray) r1
            L_0x0023:
                r6 = 0
                r1.mo28330a(r5, r6)
                int r5 = r4.f12243e
                int[] r6 = r4.f12244f
                r1.mo28335c(r5, r6)
                k.b.d.a.d$a r5 = new k.b.d.a.d$a
                int r6 = r4.f12243e
                int[] r0 = r4.f12244f
                r5.<init>(r6, r0, r1)
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: p245k.p251b.p263d.p264a.ECFieldElement.C5164a.mo28264a(k.b.d.a.d, k.b.d.a.d):k.b.d.a.d");
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: java.lang.Object} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v3, resolved type: k.b.d.a.h} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public p245k.p251b.p263d.p264a.ECFieldElement mo28268b(p245k.p251b.p263d.p264a.ECFieldElement r5, p245k.p251b.p263d.p264a.ECFieldElement r6, p245k.p251b.p263d.p264a.ECFieldElement r7) {
            /*
                r4 = this;
                k.b.d.a.h r0 = r4.f12245g
                k.b.d.a.d$a r5 = (p245k.p251b.p263d.p264a.ECFieldElement.C5164a) r5
                k.b.d.a.h r5 = r5.f12245g
                k.b.d.a.d$a r6 = (p245k.p251b.p263d.p264a.ECFieldElement.C5164a) r6
                k.b.d.a.h r6 = r6.f12245g
                k.b.d.a.d$a r7 = (p245k.p251b.p263d.p264a.ECFieldElement.C5164a) r7
                k.b.d.a.h r7 = r7.f12245g
                int r1 = r4.f12243e
                int[] r2 = r4.f12244f
                k.b.d.a.h r1 = r0.mo28333b(r5, r1, r2)
                int r2 = r4.f12243e
                int[] r3 = r4.f12244f
                k.b.d.a.h r6 = r6.mo28333b(r7, r2, r3)
                if (r1 == r0) goto L_0x0022
                if (r1 != r5) goto L_0x0029
            L_0x0022:
                java.lang.Object r5 = r1.clone()
                r1 = r5
                k.b.d.a.h r1 = (p245k.p251b.p263d.p264a.LongArray) r1
            L_0x0029:
                r5 = 0
                r1.mo28330a(r6, r5)
                int r5 = r4.f12243e
                int[] r6 = r4.f12244f
                r1.mo28335c(r5, r6)
                k.b.d.a.d$a r5 = new k.b.d.a.d$a
                int r6 = r4.f12243e
                int[] r7 = r4.f12244f
                r5.<init>(r6, r7, r1)
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: p245k.p251b.p263d.p264a.ECFieldElement.C5164a.mo28268b(k.b.d.a.d, k.b.d.a.d, k.b.d.a.d):k.b.d.a.d");
        }

        private C5164a(int i, int[] iArr, LongArray hVar) {
            this.f12243e = i;
            this.f12242d = iArr.length == 1 ? 2 : 3;
            this.f12244f = iArr;
            this.f12245g = hVar;
        }

        /* renamed from: a */
        public ECFieldElement mo28281a(int i) {
            if (i < 1) {
                return super;
            }
            int i2 = this.f12243e;
            int[] iArr = this.f12244f;
            return new C5164a(i2, iArr, this.f12245g.mo28327a(i, i2, iArr));
        }

        /* renamed from: b */
        public ECFieldElement mo28267b(ECFieldElement dVar) {
            return mo28270c(super.mo28271d());
        }
    }

    /* renamed from: k.b.d.a.d$b */
    /* compiled from: ECFieldElement */
    public static class C5165b extends ECFieldElement {

        /* renamed from: d */
        BigInteger f12246d;

        /* renamed from: e */
        BigInteger f12247e;

        /* renamed from: f */
        BigInteger f12248f;

        C5165b(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
            if (bigInteger3 == null || bigInteger3.signum() < 0 || bigInteger3.compareTo(bigInteger) >= 0) {
                throw new IllegalArgumentException("x value invalid in Fp field element");
            }
            this.f12246d = bigInteger;
            this.f12247e = bigInteger2;
            this.f12248f = bigInteger3;
        }

        /* renamed from: e */
        static BigInteger m18741e(BigInteger bigInteger) {
            int bitLength = bigInteger.bitLength();
            if (bitLength < 96 || bigInteger.shiftRight(bitLength - 64).longValue() != -1) {
                return null;
            }
            return ECConstants.f12220b.shiftLeft(bitLength).subtract(bigInteger);
        }

        /* renamed from: a */
        public ECFieldElement mo28263a(ECFieldElement dVar) {
            return new C5165b(this.f12246d, this.f12247e, mo28285a(this.f12248f, super.mo28279k()));
        }

        /* renamed from: b */
        public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
            BigInteger bigInteger = this.f12248f;
            BigInteger k = super.mo28279k();
            BigInteger k2 = super.mo28279k();
            BigInteger k3 = super.mo28279k();
            return new C5165b(this.f12246d, this.f12247e, mo28290d(bigInteger.multiply(k).add(k2.multiply(k3))));
        }

        /* renamed from: c */
        public int mo28269c() {
            return this.f12246d.bitLength();
        }

        /* renamed from: d */
        public ECFieldElement mo28272d(ECFieldElement dVar) {
            return new C5165b(this.f12246d, this.f12247e, mo28289c(this.f12248f, super.mo28279k()));
        }

        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }
            if (!(obj instanceof C5165b)) {
                return false;
            }
            C5165b bVar = (C5165b) obj;
            if (!this.f12246d.equals(bVar.f12246d) || !this.f12248f.equals(bVar.f12248f)) {
                return false;
            }
            return true;
        }

        /* renamed from: g */
        public ECFieldElement mo28275g() {
            if (this.f12248f.signum() == 0) {
                return super;
            }
            BigInteger bigInteger = this.f12246d;
            return new C5165b(bigInteger, this.f12247e, bigInteger.subtract(this.f12248f));
        }

        /* renamed from: h */
        public ECFieldElement mo28276h() {
            if (mo28274f() || mo28273e()) {
                return super;
            }
            if (!this.f12246d.testBit(0)) {
                throw new RuntimeException("not done yet");
            } else if (this.f12246d.testBit(1)) {
                BigInteger add = this.f12246d.shiftRight(2).add(ECConstants.f12220b);
                BigInteger bigInteger = this.f12246d;
                return m18742e(new C5165b(bigInteger, this.f12247e, this.f12248f.modPow(add, bigInteger)));
            } else if (this.f12246d.testBit(2)) {
                BigInteger modPow = this.f12248f.modPow(this.f12246d.shiftRight(3), this.f12246d);
                BigInteger b = mo28287b(modPow, this.f12248f);
                if (mo28287b(b, modPow).equals(ECConstants.f12220b)) {
                    return m18742e(new C5165b(this.f12246d, this.f12247e, b));
                }
                return m18742e(new C5165b(this.f12246d, this.f12247e, mo28287b(b, ECConstants.f12221c.modPow(this.f12246d.shiftRight(2), this.f12246d))));
            } else {
                BigInteger shiftRight = this.f12246d.shiftRight(1);
                if (!this.f12248f.modPow(shiftRight, this.f12246d).equals(ECConstants.f12220b)) {
                    return null;
                }
                BigInteger bigInteger2 = this.f12248f;
                BigInteger a = mo28284a(mo28284a(bigInteger2));
                BigInteger add2 = shiftRight.add(ECConstants.f12220b);
                BigInteger subtract = this.f12246d.subtract(ECConstants.f12220b);
                Random random = new Random();
                while (true) {
                    BigInteger bigInteger3 = new BigInteger(this.f12246d.bitLength(), random);
                    if (bigInteger3.compareTo(this.f12246d) < 0 && mo28290d(bigInteger3.multiply(bigInteger3).subtract(a)).modPow(shiftRight, this.f12246d).equals(subtract)) {
                        BigInteger[] a2 = m18740a(bigInteger3, bigInteger2, add2);
                        BigInteger bigInteger4 = a2[0];
                        BigInteger bigInteger5 = a2[1];
                        if (mo28287b(bigInteger5, bigInteger5).equals(a)) {
                            return new C5165b(this.f12246d, this.f12247e, mo28286b(bigInteger5));
                        }
                        if (!bigInteger4.equals(ECConstants.f12220b) && !bigInteger4.equals(subtract)) {
                            return null;
                        }
                    }
                }
            }
        }

        public int hashCode() {
            return this.f12246d.hashCode() ^ this.f12248f.hashCode();
        }

        /* renamed from: i */
        public ECFieldElement mo28277i() {
            BigInteger bigInteger = this.f12246d;
            BigInteger bigInteger2 = this.f12247e;
            BigInteger bigInteger3 = this.f12248f;
            return new C5165b(bigInteger, bigInteger2, mo28287b(bigInteger3, bigInteger3));
        }

        /* renamed from: k */
        public BigInteger mo28279k() {
            return this.f12248f;
        }

        /* renamed from: a */
        public ECFieldElement mo28262a() {
            BigInteger add = this.f12248f.add(ECConstants.f12220b);
            if (add.compareTo(this.f12246d) == 0) {
                add = ECConstants.f12219a;
            }
            return new C5165b(this.f12246d, this.f12247e, add);
        }

        /* renamed from: c */
        public ECFieldElement mo28270c(ECFieldElement dVar) {
            return new C5165b(this.f12246d, this.f12247e, mo28287b(this.f12248f, super.mo28279k()));
        }

        /* renamed from: d */
        public ECFieldElement mo28271d() {
            return new C5165b(this.f12246d, this.f12247e, mo28288c(this.f12248f));
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public BigInteger mo28288c(BigInteger bigInteger) {
            int c = mo28269c();
            int i = (c + 31) >> 5;
            int[] a = Nat.m20032a(c, this.f12246d);
            int[] a2 = Nat.m20032a(c, bigInteger);
            int[] a3 = Nat.m20031a(i);
            Mod.m19851a(a, a2, a3);
            return Nat.m20054f(i, a3);
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public BigInteger mo28290d(BigInteger bigInteger) {
            if (this.f12247e == null) {
                return bigInteger.mod(this.f12246d);
            }
            boolean z = bigInteger.signum() < 0;
            if (z) {
                bigInteger = bigInteger.abs();
            }
            int bitLength = this.f12246d.bitLength();
            boolean equals = this.f12247e.equals(ECConstants.f12220b);
            while (bigInteger.bitLength() > bitLength + 1) {
                BigInteger shiftRight = bigInteger.shiftRight(bitLength);
                BigInteger subtract = bigInteger.subtract(shiftRight.shiftLeft(bitLength));
                if (!equals) {
                    shiftRight = shiftRight.multiply(this.f12247e);
                }
                bigInteger = shiftRight.add(subtract);
            }
            while (bigInteger.compareTo(this.f12246d) >= 0) {
                bigInteger = bigInteger.subtract(this.f12246d);
            }
            if (!z || bigInteger.signum() == 0) {
                return bigInteger;
            }
            return this.f12246d.subtract(bigInteger);
        }

        /* renamed from: e */
        private ECFieldElement m18742e(ECFieldElement dVar) {
            if (super.mo28277i().equals(this)) {
                return super;
            }
            return null;
        }

        /* renamed from: b */
        public ECFieldElement mo28267b(ECFieldElement dVar) {
            return new C5165b(this.f12246d, this.f12247e, mo28287b(this.f12248f, mo28288c(super.mo28279k())));
        }

        /* renamed from: a */
        public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
            BigInteger bigInteger = this.f12248f;
            BigInteger k = super.mo28279k();
            BigInteger k2 = super.mo28279k();
            BigInteger k3 = super.mo28279k();
            return new C5165b(this.f12246d, this.f12247e, mo28290d(bigInteger.multiply(k).subtract(k2.multiply(k3))));
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public BigInteger mo28286b(BigInteger bigInteger) {
            if (bigInteger.testBit(0)) {
                bigInteger = this.f12246d.subtract(bigInteger);
            }
            return bigInteger.shiftRight(1);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public BigInteger mo28287b(BigInteger bigInteger, BigInteger bigInteger2) {
            return mo28290d(bigInteger.multiply(bigInteger2));
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public BigInteger mo28289c(BigInteger bigInteger, BigInteger bigInteger2) {
            BigInteger subtract = bigInteger.subtract(bigInteger2);
            return subtract.signum() < 0 ? subtract.add(this.f12246d) : subtract;
        }

        /* renamed from: a */
        public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
            BigInteger bigInteger = this.f12248f;
            BigInteger k = super.mo28279k();
            BigInteger k2 = super.mo28279k();
            return new C5165b(this.f12246d, this.f12247e, mo28290d(bigInteger.multiply(bigInteger).add(k.multiply(k2))));
        }

        /* renamed from: a */
        private BigInteger[] m18740a(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3) {
            int bitLength = bigInteger3.bitLength();
            int lowestSetBit = bigInteger3.getLowestSetBit();
            BigInteger bigInteger4 = ECConstants.f12220b;
            BigInteger bigInteger5 = ECConstants.f12221c;
            BigInteger bigInteger6 = ECConstants.f12220b;
            BigInteger bigInteger7 = bigInteger;
            BigInteger bigInteger8 = bigInteger5;
            BigInteger bigInteger9 = bigInteger4;
            BigInteger bigInteger10 = bigInteger6;
            for (int i = bitLength - 1; i >= lowestSetBit + 1; i--) {
                bigInteger6 = mo28287b(bigInteger6, bigInteger10);
                if (bigInteger3.testBit(i)) {
                    bigInteger10 = mo28287b(bigInteger6, bigInteger2);
                    bigInteger9 = mo28287b(bigInteger9, bigInteger7);
                    bigInteger8 = mo28290d(bigInteger7.multiply(bigInteger8).subtract(bigInteger.multiply(bigInteger6)));
                    bigInteger7 = mo28290d(bigInteger7.multiply(bigInteger7).subtract(bigInteger10.shiftLeft(1)));
                } else {
                    BigInteger d = mo28290d(bigInteger9.multiply(bigInteger8).subtract(bigInteger6));
                    BigInteger d2 = mo28290d(bigInteger7.multiply(bigInteger8).subtract(bigInteger.multiply(bigInteger6)));
                    bigInteger8 = mo28290d(bigInteger8.multiply(bigInteger8).subtract(bigInteger6.shiftLeft(1)));
                    bigInteger7 = d2;
                    bigInteger9 = d;
                    bigInteger10 = bigInteger6;
                }
            }
            BigInteger b = mo28287b(bigInteger6, bigInteger10);
            BigInteger b2 = mo28287b(b, bigInteger2);
            BigInteger d3 = mo28290d(bigInteger9.multiply(bigInteger8).subtract(b));
            BigInteger d4 = mo28290d(bigInteger7.multiply(bigInteger8).subtract(bigInteger.multiply(b)));
            BigInteger b3 = mo28287b(b, b2);
            BigInteger bigInteger11 = d4;
            for (int i2 = 1; i2 <= lowestSetBit; i2++) {
                d3 = mo28287b(d3, bigInteger11);
                bigInteger11 = mo28290d(bigInteger11.multiply(bigInteger11).subtract(b3.shiftLeft(1)));
                b3 = mo28287b(b3, b3);
            }
            return new BigInteger[]{d3, bigInteger11};
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public BigInteger mo28285a(BigInteger bigInteger, BigInteger bigInteger2) {
            BigInteger add = bigInteger.add(bigInteger2);
            return add.compareTo(this.f12246d) >= 0 ? add.subtract(this.f12246d) : add;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public BigInteger mo28284a(BigInteger bigInteger) {
            BigInteger shiftLeft = bigInteger.shiftLeft(1);
            return shiftLeft.compareTo(this.f12246d) >= 0 ? shiftLeft.subtract(this.f12246d) : shiftLeft;
        }
    }

    /* renamed from: a */
    public abstract ECFieldElement mo28262a();

    /* renamed from: a */
    public abstract ECFieldElement mo28263a(ECFieldElement dVar);

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28270c(dVar).mo28272d(dVar2.mo28270c(dVar3));
    }

    /* renamed from: b */
    public int mo28266b() {
        return mo28279k().bitLength();
    }

    /* renamed from: b */
    public abstract ECFieldElement mo28267b(ECFieldElement dVar);

    /* renamed from: c */
    public abstract int mo28269c();

    /* renamed from: c */
    public abstract ECFieldElement mo28270c(ECFieldElement dVar);

    /* renamed from: d */
    public abstract ECFieldElement mo28271d();

    /* renamed from: d */
    public abstract ECFieldElement mo28272d(ECFieldElement dVar);

    /* renamed from: e */
    public boolean mo28273e() {
        return mo28266b() == 1;
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return mo28279k().signum() == 0;
    }

    /* renamed from: g */
    public abstract ECFieldElement mo28275g();

    /* renamed from: h */
    public abstract ECFieldElement mo28276h();

    /* renamed from: i */
    public abstract ECFieldElement mo28277i();

    /* renamed from: j */
    public boolean mo28278j() {
        return mo28279k().testBit(0);
    }

    /* renamed from: k */
    public abstract BigInteger mo28279k();

    public String toString() {
        return mo28279k().toString(16);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        return mo28277i().mo28263a(dVar.mo28270c(dVar2));
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28270c(dVar).mo28263a(dVar2.mo28270c(dVar3));
    }
}
