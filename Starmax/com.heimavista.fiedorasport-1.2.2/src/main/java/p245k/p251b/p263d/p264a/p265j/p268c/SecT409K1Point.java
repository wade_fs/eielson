package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECConstants;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;

/* renamed from: k.b.d.a.j.c.n2 */
public class SecT409K1Point extends ECPoint.C5166a {
    public SecT409K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        ECFieldElement dVar;
        ECFieldElement dVar2;
        ECFieldElement dVar3;
        ECFieldElement dVar4;
        ECFieldElement dVar5;
        ECFieldElement dVar6;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        ECFieldElement dVar7 = this.f12251b;
        ECFieldElement f = fVar.mo28304f();
        if (!dVar7.mo28274f()) {
            ECFieldElement dVar8 = this.f12252c;
            ECFieldElement dVar9 = this.f12253d[0];
            ECFieldElement g = fVar.mo28305g();
            ECFieldElement a = fVar.mo28293a(0);
            boolean e = dVar9.mo28273e();
            if (!e) {
                dVar2 = f.mo28270c(dVar9);
                dVar = g.mo28270c(dVar9);
            } else {
                dVar2 = f;
                dVar = g;
            }
            boolean e2 = a.mo28273e();
            if (!e2) {
                dVar7 = dVar7.mo28270c(a);
                dVar3 = dVar8.mo28270c(a);
            } else {
                dVar3 = dVar8;
            }
            ECFieldElement a2 = dVar3.mo28263a(dVar);
            ECFieldElement a3 = dVar7.mo28263a(dVar2);
            if (!a3.mo28274f()) {
                if (f.mo28274f()) {
                    ECPoint n = mo28313n();
                    ECFieldElement h = n.mo28306h();
                    ECFieldElement i = n.mo28308i();
                    ECFieldElement b = i.mo28263a(g).mo28267b(h);
                    dVar6 = b.mo28277i().mo28263a(b).mo28263a(h);
                    if (dVar6.mo28274f()) {
                        return new SecT409K1Point(d, dVar6, d.mo28251d(), this.f12254e);
                    }
                    dVar5 = b.mo28270c(h.mo28263a(dVar6)).mo28263a(dVar6).mo28263a(i).mo28267b(dVar6).mo28263a(dVar6);
                    dVar4 = d.mo28235a(ECConstants.f12220b);
                } else {
                    ECFieldElement i2 = a3.mo28277i();
                    ECFieldElement c = a2.mo28270c(dVar7);
                    ECFieldElement c2 = a2.mo28270c(dVar2);
                    ECFieldElement c3 = c.mo28270c(c2);
                    if (c3.mo28274f()) {
                        return new SecT409K1Point(d, c3, d.mo28251d(), this.f12254e);
                    }
                    ECFieldElement c4 = a2.mo28270c(i2);
                    dVar4 = !e2 ? c4.mo28270c(a) : c4;
                    ECFieldElement a4 = c2.mo28263a(i2).mo28264a(dVar4, dVar8.mo28263a(dVar9));
                    if (!e) {
                        dVar4 = dVar4.mo28270c(dVar9);
                    }
                    dVar6 = c3;
                    dVar5 = a4;
                }
                return new SecT409K1Point(d, dVar6, dVar5, new ECFieldElement[]{dVar4}, this.f12254e);
            } else if (a2.mo28274f()) {
                return mo28316q();
            } else {
                return d.mo28258i();
            }
        } else if (f.mo28274f()) {
            return d.mo28258i();
        } else {
            return fVar.mo28296a(this);
        }
    }

    /* renamed from: i */
    public ECFieldElement mo28308i() {
        ECFieldElement dVar = this.f12251b;
        ECFieldElement dVar2 = this.f12252c;
        if (mo28309j() || dVar.mo28274f()) {
            return dVar2;
        }
        ECFieldElement c = dVar2.mo28263a(dVar).mo28270c(dVar);
        ECFieldElement dVar3 = this.f12253d[0];
        return !dVar3.mo28273e() ? c.mo28267b(dVar3) : c;
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        ECFieldElement dVar = this.f12251b;
        if (dVar.mo28274f()) {
            return this;
        }
        ECFieldElement dVar2 = this.f12252c;
        ECFieldElement dVar3 = this.f12253d[0];
        ECCurve cVar = this.f12250a;
        ECFieldElement[] dVarArr = {dVar3};
        return new SecT409K1Point(cVar, dVar, dVar2.mo28263a(dVar3), dVarArr, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        ECFieldElement dVar;
        ECFieldElement dVar2;
        ECFieldElement dVar3;
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        ECFieldElement dVar4 = this.f12251b;
        if (dVar4.mo28274f()) {
            return d.mo28258i();
        }
        ECFieldElement dVar5 = this.f12252c;
        ECFieldElement dVar6 = this.f12253d[0];
        boolean e = dVar6.mo28273e();
        if (e) {
            dVar = dVar6;
        } else {
            dVar = dVar6.mo28277i();
        }
        if (e) {
            dVar2 = dVar5.mo28277i().mo28263a(dVar5);
        } else {
            dVar2 = dVar5.mo28263a(dVar6).mo28270c(dVar5);
        }
        if (dVar2.mo28274f()) {
            return new SecT409K1Point(d, dVar2, d.mo28251d(), this.f12254e);
        }
        ECFieldElement i = dVar2.mo28277i();
        if (e) {
            dVar3 = dVar2;
        } else {
            dVar3 = dVar2.mo28270c(dVar);
        }
        ECFieldElement i2 = dVar5.mo28263a(dVar4).mo28277i();
        if (!e) {
            dVar6 = dVar.mo28277i();
        }
        ECFieldElement a = i2.mo28263a(dVar2).mo28263a(dVar).mo28270c(i2).mo28263a(dVar6).mo28263a(i).mo28263a(dVar3);
        return new SecT409K1Point(d, i, a, new ECFieldElement[]{dVar3}, this.f12254e);
    }

    public SecT409K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecT409K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
