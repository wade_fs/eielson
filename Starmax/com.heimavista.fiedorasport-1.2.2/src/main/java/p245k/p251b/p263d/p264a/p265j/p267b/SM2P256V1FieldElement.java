package p245k.p251b.p263d.p264a.p265j.p267b;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.b.c */
public class SM2P256V1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12269e = SM2P256V1Curve.f12265j;

    /* renamed from: d */
    protected int[] f12270d;

    public SM2P256V1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12269e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SM2P256V1FieldElement");
        }
        this.f12270d = SM2P256V1Field.m18897a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18896a(this.f12270d, ((SM2P256V1FieldElement) dVar).f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Mod.m19851a(SM2P256V1Field.f12267a, ((SM2P256V1FieldElement) dVar).f12270d, a);
        SM2P256V1Field.m18900b(a, this.f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12269e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18904d(this.f12270d, ((SM2P256V1FieldElement) dVar).f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19962a(this.f12270d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SM2P256V1FieldElement)) {
            return false;
        }
        return Nat256.m19972b(this.f12270d, ((SM2P256V1FieldElement) obj).f12270d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19970b(this.f12270d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18899b(this.f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12270d;
        if (Nat256.m19970b(iArr) || Nat256.m19962a(iArr)) {
            return super;
        }
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18903d(iArr, a);
        SM2P256V1Field.m18900b(a, iArr, a);
        int[] a2 = Nat256.m19966a();
        SM2P256V1Field.m18894a(a, 2, a2);
        SM2P256V1Field.m18900b(a2, a, a2);
        int[] a3 = Nat256.m19966a();
        SM2P256V1Field.m18894a(a2, 2, a3);
        SM2P256V1Field.m18900b(a3, a, a3);
        SM2P256V1Field.m18894a(a3, 6, a);
        SM2P256V1Field.m18900b(a, a3, a);
        int[] a4 = Nat256.m19966a();
        SM2P256V1Field.m18894a(a, 12, a4);
        SM2P256V1Field.m18900b(a4, a, a4);
        SM2P256V1Field.m18894a(a4, 6, a);
        SM2P256V1Field.m18900b(a, a3, a);
        SM2P256V1Field.m18903d(a, a3);
        SM2P256V1Field.m18900b(a3, iArr, a3);
        SM2P256V1Field.m18894a(a3, 31, a4);
        SM2P256V1Field.m18900b(a4, a3, a);
        SM2P256V1Field.m18894a(a4, 32, a4);
        SM2P256V1Field.m18900b(a4, a, a4);
        SM2P256V1Field.m18894a(a4, 62, a4);
        SM2P256V1Field.m18900b(a4, a, a4);
        SM2P256V1Field.m18894a(a4, 4, a4);
        SM2P256V1Field.m18900b(a4, a2, a4);
        SM2P256V1Field.m18894a(a4, 32, a4);
        SM2P256V1Field.m18900b(a4, iArr, a4);
        SM2P256V1Field.m18894a(a4, 62, a4);
        SM2P256V1Field.m18903d(a4, a2);
        if (Nat256.m19972b(iArr, a2)) {
            return new SM2P256V1FieldElement(a4);
        }
        return null;
    }

    public int hashCode() {
        return f12269e.hashCode() ^ Arrays.m20066b(this.f12270d, 0, 8);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18903d(this.f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat256.m19956a(this.f12270d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19977c(this.f12270d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18900b(this.f12270d, ((SM2P256V1FieldElement) dVar).f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat256.m19966a();
        SM2P256V1Field.m18895a(this.f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat256.m19966a();
        Mod.m19851a(SM2P256V1Field.f12267a, this.f12270d, a);
        return new SM2P256V1FieldElement(a);
    }

    public SM2P256V1FieldElement() {
        this.f12270d = Nat256.m19966a();
    }

    protected SM2P256V1FieldElement(int[] iArr) {
        this.f12270d = iArr;
    }
}
