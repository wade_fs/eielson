package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.v1 */
public class SecT233FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12368d;

    public SecT233FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 233) {
            throw new IllegalArgumentException("x value invalid for SecT233FieldElement");
        }
        this.f12368d = SecT233Field.m19693a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] b = Nat256.m19974b();
        SecT233Field.m19692a(this.f12368d, ((SecT233FieldElement) dVar).f12368d, b);
        return new SecT233FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12368d;
        long[] jArr2 = ((SecT233FieldElement) dVar).f12368d;
        long[] jArr3 = ((SecT233FieldElement) dVar2).f12368d;
        long[] jArr4 = ((SecT233FieldElement) dVar3).f12368d;
        long[] d = Nat256.m19986d();
        SecT233Field.m19701e(jArr, jArr2, d);
        SecT233Field.m19701e(jArr3, jArr4, d);
        long[] b = Nat256.m19974b();
        SecT233Field.m19700e(d, b);
        return new SecT233FieldElement(b);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 233;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] b = Nat256.m19974b();
        SecT233Field.m19699d(this.f12368d, ((SecT233FieldElement) dVar).f12368d, b);
        return new SecT233FieldElement(b);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19964a(this.f12368d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT233FieldElement)) {
            return false;
        }
        return Nat256.m19965a(this.f12368d, ((SecT233FieldElement) obj).f12368d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19973b(this.f12368d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] b = Nat256.m19974b();
        SecT233Field.m19702f(this.f12368d, b);
        return new SecT233FieldElement(b);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12368d, 0, 4) ^ 2330074;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] b = Nat256.m19974b();
        SecT233Field.m19703g(this.f12368d, b);
        return new SecT233FieldElement(b);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12368d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19978c(this.f12368d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] b = Nat256.m19974b();
        SecT233Field.m19698d(this.f12368d, b);
        return new SecT233FieldElement(b);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] b = Nat256.m19974b();
        SecT233Field.m19691a(this.f12368d, b);
        return new SecT233FieldElement(b);
    }

    public SecT233FieldElement() {
        this.f12368d = Nat256.m19974b();
    }

    protected SecT233FieldElement(long[] jArr) {
        this.f12368d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12368d;
        long[] jArr2 = ((SecT233FieldElement) dVar).f12368d;
        long[] jArr3 = ((SecT233FieldElement) dVar2).f12368d;
        long[] d = Nat256.m19986d();
        SecT233Field.m19704h(jArr, d);
        SecT233Field.m19701e(jArr2, jArr3, d);
        long[] b = Nat256.m19974b();
        SecT233Field.m19700e(d, b);
        return new SecT233FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
