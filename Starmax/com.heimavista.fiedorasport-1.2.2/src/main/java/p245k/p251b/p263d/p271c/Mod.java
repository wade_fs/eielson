package p245k.p251b.p263d.p271c;

import java.util.Random;

/* renamed from: k.b.d.c.b */
public abstract class Mod {
    /* renamed from: a */
    private static int m19848a(int i) {
        int i2 = 0;
        while ((i & 1) == 0) {
            i >>>= 1;
            i2++;
        }
        return i2;
    }

    /* renamed from: a */
    public static void m19851a(int[] iArr, int[] iArr2, int[] iArr3) {
        int length = iArr.length;
        if (Nat.m20053e(length, iArr2)) {
            throw new IllegalArgumentException("'x' cannot be 0");
        } else if (Nat.m20051d(length, iArr2)) {
            System.arraycopy(iArr2, 0, iArr3, 0, length);
        } else {
            int[] a = Nat.m20033a(length, iArr2);
            int[] a2 = Nat.m20031a(length);
            a2[0] = 1;
            int a3 = (1 & a[0]) == 0 ? m19849a(iArr, a, length, a2, 0) : 0;
            if (Nat.m20051d(length, a)) {
                m19850a(iArr, a3, a2, iArr3);
                return;
            }
            int[] a4 = Nat.m20033a(length, iArr);
            int[] a5 = Nat.m20031a(length);
            int i = length;
            int i2 = 0;
            while (true) {
                int i3 = i - 1;
                if (a[i3] == 0 && a4[i3] == 0) {
                    i--;
                } else if (Nat.m20047c(i, a, a4)) {
                    Nat.m20052e(i, a4, a);
                    a3 = m19849a(iArr, a, i, a2, a3 + (Nat.m20052e(length, a5, a2) - i2));
                    if (Nat.m20051d(i, a)) {
                        m19850a(iArr, a3, a2, iArr3);
                        return;
                    }
                } else {
                    Nat.m20052e(i, a, a4);
                    i2 = m19849a(iArr, a4, i, a5, i2 + (Nat.m20052e(length, a2, a5) - a3));
                    if (Nat.m20051d(i, a4)) {
                        m19850a(iArr, i2, a5, iArr3);
                        return;
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public static int[] m19852a(int[] iArr) {
        int length = iArr.length;
        Random random = new Random();
        int[] a = Nat.m20031a(length);
        int i = length - 1;
        int i2 = iArr[i];
        int i3 = i2 | (i2 >>> 1);
        int i4 = i3 | (i3 >>> 2);
        int i5 = i4 | (i4 >>> 4);
        int i6 = i5 | (i5 >>> 8);
        int i7 = i6 | (i6 >>> 16);
        do {
            for (int i8 = 0; i8 != length; i8++) {
                a[i8] = random.nextInt();
            }
            a[i] = a[i] & i7;
        } while (Nat.m20047c(length, a, iArr));
        return a;
    }

    /* renamed from: a */
    private static void m19850a(int[] iArr, int i, int[] iArr2, int[] iArr3) {
        if (i < 0) {
            Nat.m20026a(iArr.length, iArr2, iArr, iArr3);
        } else {
            System.arraycopy(iArr2, 0, iArr3, 0, iArr.length);
        }
    }

    /* renamed from: a */
    private static int m19849a(int[] iArr, int[] iArr2, int i, int[] iArr3, int i2) {
        int i3;
        int length = iArr.length;
        int i4 = 0;
        while (iArr2[0] == 0) {
            Nat.m20049d(i, iArr2, 0);
            i4 += 32;
        }
        int a = m19848a(iArr2[0]);
        if (a > 0) {
            Nat.m20037b(i, iArr2, a, 0);
            i4 += a;
        }
        for (int i5 = 0; i5 < i4; i5++) {
            if ((iArr3[0] & 1) != 0) {
                if (i2 < 0) {
                    i3 = Nat.m20025a(length, iArr, iArr3);
                } else {
                    i3 = Nat.m20052e(length, iArr, iArr3);
                }
                i2 += i3;
            }
            Nat.m20044c(length, iArr3, i2);
        }
        return i2;
    }
}
