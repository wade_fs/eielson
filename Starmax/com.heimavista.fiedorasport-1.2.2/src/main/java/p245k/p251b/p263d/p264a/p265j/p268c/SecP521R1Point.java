package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;

/* renamed from: k.b.d.a.j.c.t0 */
public class SecP521R1Point extends ECPoint.C5167b {
    public SecP521R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* JADX WARN: Type inference failed for: r17v0, types: [k.b.d.a.f] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p245k.p251b.p263d.p264a.ECPoint mo28296a(p245k.p251b.p263d.p264a.ECPoint r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            boolean r2 = r16.mo28309j()
            if (r2 == 0) goto L_0x000b
            return r1
        L_0x000b:
            boolean r2 = r17.mo28309j()
            if (r2 == 0) goto L_0x0012
            return r0
        L_0x0012:
            if (r0 != r1) goto L_0x0019
            k.b.d.a.f r1 = r16.mo28316q()
            return r1
        L_0x0019:
            k.b.d.a.c r3 = r16.mo28301d()
            k.b.d.a.d r2 = r0.f12251b
            k.b.d.a.j.c.s0 r2 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1FieldElement) r2
            k.b.d.a.d r4 = r0.f12252c
            k.b.d.a.j.c.s0 r4 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1FieldElement) r4
            k.b.d.a.d r5 = r17.mo28306h()
            k.b.d.a.j.c.s0 r5 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1FieldElement) r5
            k.b.d.a.d r6 = r17.mo28308i()
            k.b.d.a.j.c.s0 r6 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1FieldElement) r6
            k.b.d.a.d[] r7 = r0.f12253d
            r8 = 0
            r7 = r7[r8]
            k.b.d.a.j.c.s0 r7 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1FieldElement) r7
            k.b.d.a.d r1 = r1.mo28293a(r8)
            k.b.d.a.j.c.s0 r1 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1FieldElement) r1
            r9 = 17
            int[] r10 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            int[] r11 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            int[] r12 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            int[] r13 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            boolean r14 = r7.mo28273e()
            if (r14 == 0) goto L_0x005b
            int[] r5 = r5.f12356d
            int[] r6 = r6.f12356d
            goto L_0x0071
        L_0x005b:
            int[] r15 = r7.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19577e(r15, r12)
            int[] r5 = r5.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r12, r5, r11)
            int[] r5 = r7.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r12, r5, r12)
            int[] r5 = r6.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r12, r5, r12)
            r5 = r11
            r6 = r12
        L_0x0071:
            boolean r15 = r1.mo28273e()
            if (r15 == 0) goto L_0x007c
            int[] r2 = r2.f12356d
            int[] r4 = r4.f12356d
            goto L_0x0092
        L_0x007c:
            int[] r8 = r1.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19577e(r8, r13)
            int[] r2 = r2.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r13, r2, r10)
            int[] r2 = r1.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r13, r2, r13)
            int[] r2 = r4.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r13, r2, r13)
            r2 = r10
            r4 = r13
        L_0x0092:
            int[] r8 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19576d(r2, r5, r8)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19576d(r4, r6, r11)
            boolean r5 = p245k.p251b.p263d.p271c.Nat.m20053e(r9, r8)
            if (r5 == 0) goto L_0x00b2
            boolean r1 = p245k.p251b.p263d.p271c.Nat.m20053e(r9, r11)
            if (r1 == 0) goto L_0x00ad
            k.b.d.a.f r1 = r16.mo28316q()
            return r1
        L_0x00ad:
            k.b.d.a.f r1 = r3.mo28258i()
            return r1
        L_0x00b2:
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19577e(r8, r12)
            int[] r5 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r12, r8, r5)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r12, r2, r12)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r4, r5, r10)
            k.b.d.a.j.c.s0 r4 = new k.b.d.a.j.c.s0
            r4.<init>(r13)
            int[] r2 = r4.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19577e(r11, r2)
            int[] r2 = r4.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19569a(r2, r5, r2)
            int[] r2 = r4.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19576d(r2, r12, r2)
            int[] r2 = r4.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19576d(r2, r12, r2)
            k.b.d.a.j.c.s0 r6 = new k.b.d.a.j.c.s0
            r6.<init>(r5)
            int[] r2 = r4.f12356d
            int[] r5 = r6.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19576d(r12, r2, r5)
            int[] r2 = r6.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r2, r11, r11)
            int[] r2 = r6.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19576d(r11, r10, r2)
            k.b.d.a.j.c.s0 r2 = new k.b.d.a.j.c.s0
            r2.<init>(r8)
            if (r14 != 0) goto L_0x00ff
            int[] r5 = r2.f12356d
            int[] r7 = r7.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r5, r7, r5)
        L_0x00ff:
            if (r15 != 0) goto L_0x0108
            int[] r5 = r2.f12356d
            int[] r1 = r1.f12356d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Field.m19574c(r5, r1, r5)
        L_0x0108:
            r1 = 1
            k.b.d.a.d[] r1 = new p245k.p251b.p263d.p264a.ECFieldElement[r1]
            r5 = 0
            r1[r5] = r2
            k.b.d.a.j.c.t0 r8 = new k.b.d.a.j.c.t0
            boolean r7 = r0.f12254e
            r2 = r8
            r5 = r6
            r6 = r1
            r2.<init>(r3, r4, r5, r6, r7)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Point.mo28296a(k.b.d.a.f):k.b.d.a.f");
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP521R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP521R1FieldElement s0Var = (SecP521R1FieldElement) this.f12252c;
        if (s0Var.mo28274f()) {
            return d.mo28258i();
        }
        SecP521R1FieldElement s0Var2 = (SecP521R1FieldElement) this.f12251b;
        SecP521R1FieldElement s0Var3 = (SecP521R1FieldElement) this.f12253d[0];
        int[] a = Nat.m20031a(17);
        int[] a2 = Nat.m20031a(17);
        int[] a3 = Nat.m20031a(17);
        SecP521R1Field.m19577e(s0Var.f12356d, a3);
        int[] a4 = Nat.m20031a(17);
        SecP521R1Field.m19577e(a3, a4);
        boolean e = s0Var3.mo28273e();
        int[] iArr = s0Var3.f12356d;
        if (!e) {
            SecP521R1Field.m19577e(iArr, a2);
            iArr = a2;
        }
        SecP521R1Field.m19576d(s0Var2.f12356d, iArr, a);
        SecP521R1Field.m19569a(s0Var2.f12356d, iArr, a2);
        SecP521R1Field.m19574c(a2, a, a2);
        Nat.m20039b(17, a2, a2, a2);
        SecP521R1Field.m19566a(a2);
        SecP521R1Field.m19574c(a3, s0Var2.f12356d, a3);
        Nat.m20045c(17, a3, 2, 0);
        SecP521R1Field.m19566a(a3);
        Nat.m20021a(17, a4, 3, 0, a);
        SecP521R1Field.m19566a(a);
        SecP521R1FieldElement s0Var4 = new SecP521R1FieldElement(a4);
        SecP521R1Field.m19577e(a2, s0Var4.f12356d);
        int[] iArr2 = s0Var4.f12356d;
        SecP521R1Field.m19576d(iArr2, a3, iArr2);
        int[] iArr3 = s0Var4.f12356d;
        SecP521R1Field.m19576d(iArr3, a3, iArr3);
        SecP521R1FieldElement s0Var5 = new SecP521R1FieldElement(a3);
        SecP521R1Field.m19576d(a3, s0Var4.f12356d, s0Var5.f12356d);
        int[] iArr4 = s0Var5.f12356d;
        SecP521R1Field.m19574c(iArr4, a2, iArr4);
        int[] iArr5 = s0Var5.f12356d;
        SecP521R1Field.m19576d(iArr5, a, iArr5);
        SecP521R1FieldElement s0Var6 = new SecP521R1FieldElement(a2);
        SecP521R1Field.m19578f(s0Var.f12356d, s0Var6.f12356d);
        if (!e) {
            int[] iArr6 = s0Var6.f12356d;
            SecP521R1Field.m19574c(iArr6, s0Var3.f12356d, iArr6);
        }
        return new SecP521R1Point(d, s0Var4, s0Var5, new ECFieldElement[]{s0Var6}, this.f12254e);
    }

    public SecP521R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP521R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
