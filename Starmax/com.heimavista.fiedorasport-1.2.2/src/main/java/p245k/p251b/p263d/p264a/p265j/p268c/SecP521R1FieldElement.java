package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.s0 */
public class SecP521R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12355e = SecP521R1Curve.f12347j;

    /* renamed from: d */
    protected int[] f12356d;

    public SecP521R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12355e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP521R1FieldElement");
        }
        this.f12356d = SecP521R1Field.m19570a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat.m20031a(17);
        SecP521R1Field.m19569a(this.f12356d, ((SecP521R1FieldElement) dVar).f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat.m20031a(17);
        Mod.m19851a(SecP521R1Field.f12351a, ((SecP521R1FieldElement) dVar).f12356d, a);
        SecP521R1Field.m19574c(a, this.f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12355e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat.m20031a(17);
        SecP521R1Field.m19576d(this.f12356d, ((SecP521R1FieldElement) dVar).f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat.m20051d(17, this.f12356d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP521R1FieldElement)) {
            return false;
        }
        return Nat.m20040b(17, this.f12356d, ((SecP521R1FieldElement) obj).f12356d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat.m20053e(17, this.f12356d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat.m20031a(17);
        SecP521R1Field.m19573c(this.f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12356d;
        if (Nat.m20053e(17, iArr) || Nat.m20051d(17, iArr)) {
            return super;
        }
        int[] a = Nat.m20031a(17);
        int[] a2 = Nat.m20031a(17);
        SecP521R1Field.m19567a(iArr, 519, a);
        SecP521R1Field.m19577e(a, a2);
        if (Nat.m20040b(17, iArr, a2)) {
            return new SecP521R1FieldElement(a);
        }
        return null;
    }

    public int hashCode() {
        return f12355e.hashCode() ^ Arrays.m20066b(this.f12356d, 0, 17);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat.m20031a(17);
        SecP521R1Field.m19577e(this.f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat.m20027a(this.f12356d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat.m20054f(17, this.f12356d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat.m20031a(17);
        SecP521R1Field.m19574c(this.f12356d, ((SecP521R1FieldElement) dVar).f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat.m20031a(17);
        SecP521R1Field.m19568a(this.f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat.m20031a(17);
        Mod.m19851a(SecP521R1Field.f12351a, this.f12356d, a);
        return new SecP521R1FieldElement(a);
    }

    public SecP521R1FieldElement() {
        this.f12356d = Nat.m20031a(17);
    }

    protected SecP521R1FieldElement(int[] iArr) {
        this.f12356d = iArr;
    }
}
