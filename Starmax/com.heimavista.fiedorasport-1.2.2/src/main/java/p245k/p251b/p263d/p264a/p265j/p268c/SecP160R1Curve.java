package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.g */
public class SecP160R1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12300j = new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFF"));

    /* renamed from: i */
    protected SecP160R1Point f12301i = new SecP160R1Point(this, null, null);

    public SecP160R1Curve() {
        super(f12300j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FFFFFFC")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("1C97BEFC54BD7A8B65ACF89F81D4D4ADC565FA45")));
        this.f12225d = new BigInteger(1, Hex.m20078a("0100000000000000000001F4C8F927AED3CA752257"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP160R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12300j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12301i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP160R1FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP160R1Point(this, dVar, dVar2, z);
    }
}
