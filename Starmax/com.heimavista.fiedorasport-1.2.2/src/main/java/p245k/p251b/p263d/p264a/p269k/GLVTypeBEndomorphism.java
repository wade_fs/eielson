package p245k.p251b.p263d.p264a.p269k;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ScaleXPointMap;

/* renamed from: k.b.d.a.k.c */
public class GLVTypeBEndomorphism implements GLVEndomorphism {
    public GLVTypeBEndomorphism(ECCurve cVar, GLVTypeBParameters dVar) {
        new ScaleXPointMap(cVar.mo28235a(dVar.mo28392a()));
    }
}
