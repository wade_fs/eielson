package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.i2 */
public class SecT283R1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT283R1Point f12315i = new SecT283R1Point(this, null, null);

    public SecT283R1Curve() {
        super(283, 5, 7, 12);
        this.f12223b = mo28235a(BigInteger.valueOf(1));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("027B680AC8B8596DA5A4AF8A19A0303FCA97FD7645309FA2A581485AF6263E313B79A2F5")));
        this.f12225d = new BigInteger(1, Hex.m20078a("03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEF90399660FC938A90165B042A7CEFADB307"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT283R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 283;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12315i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT283FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT283R1Point(this, dVar, dVar2, z);
    }
}
