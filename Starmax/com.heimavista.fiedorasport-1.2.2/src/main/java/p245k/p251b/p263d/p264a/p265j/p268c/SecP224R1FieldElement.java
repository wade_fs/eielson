package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat224;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.c0 */
public class SecP224R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12286e = SecP224R1Curve.f12273j;

    /* renamed from: d */
    protected int[] f12287d;

    public SecP224R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12286e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP224R1FieldElement");
        }
        this.f12287d = SecP224R1Field.m18989a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18988a(this.f12287d, ((SecP224R1FieldElement) dVar).f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        Mod.m19851a(SecP224R1Field.f12279a, ((SecP224R1FieldElement) dVar).f12287d, a);
        SecP224R1Field.m18992b(a, this.f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12286e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18996d(this.f12287d, ((SecP224R1FieldElement) dVar).f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat224.m19938a(this.f12287d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP224R1FieldElement)) {
            return false;
        }
        return Nat224.m19943b(this.f12287d, ((SecP224R1FieldElement) obj).f12287d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat224.m19942b(this.f12287d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18991b(this.f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12287d;
        if (Nat224.m19942b(iArr) || Nat224.m19938a(iArr)) {
            return super;
        }
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18991b(iArr, a);
        int[] a2 = Mod.m19852a(SecP224R1Field.f12279a);
        int[] a3 = Nat224.m19939a();
        if (!m19049a(iArr)) {
            return null;
        }
        while (!m19050a(a, a2, a3)) {
            SecP224R1Field.m18987a(a2, a2);
        }
        SecP224R1Field.m18995d(a3, a2);
        if (Nat224.m19943b(iArr, a2)) {
            return new SecP224R1FieldElement(a3);
        }
        return null;
    }

    public int hashCode() {
        return f12286e.hashCode() ^ Arrays.m20066b(this.f12287d, 0, 7);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18995d(this.f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat224.m19934a(this.f12287d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat224.m19945c(this.f12287d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18992b(this.f12287d, ((SecP224R1FieldElement) dVar).f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat224.m19939a();
        SecP224R1Field.m18987a(this.f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat224.m19939a();
        Mod.m19851a(SecP224R1Field.f12279a, this.f12287d, a);
        return new SecP224R1FieldElement(a);
    }

    public SecP224R1FieldElement() {
        this.f12287d = Nat224.m19939a();
    }

    protected SecP224R1FieldElement(int[] iArr) {
        this.f12287d = iArr;
    }

    /* renamed from: a */
    private static boolean m19049a(int[] iArr) {
        int[] a = Nat224.m19939a();
        int[] a2 = Nat224.m19939a();
        Nat224.m19937a(iArr, a);
        for (int i = 0; i < 7; i++) {
            Nat224.m19937a(a, a2);
            SecP224R1Field.m18986a(a, 1 << i, a);
            SecP224R1Field.m18992b(a, a2, a);
        }
        SecP224R1Field.m18986a(a, 95, a);
        return Nat224.m19938a(a);
    }

    /* renamed from: a */
    private static void m19048a(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4, int[] iArr5, int[] iArr6, int[] iArr7) {
        SecP224R1Field.m18992b(iArr5, iArr3, iArr7);
        SecP224R1Field.m18992b(iArr7, iArr, iArr7);
        SecP224R1Field.m18992b(iArr4, iArr2, iArr6);
        SecP224R1Field.m18988a(iArr6, iArr7, iArr6);
        SecP224R1Field.m18992b(iArr4, iArr3, iArr7);
        Nat224.m19937a(iArr6, iArr4);
        SecP224R1Field.m18992b(iArr5, iArr2, iArr5);
        SecP224R1Field.m18988a(iArr5, iArr7, iArr5);
        SecP224R1Field.m18995d(iArr5, iArr6);
        SecP224R1Field.m18992b(iArr6, iArr, iArr6);
    }

    /* renamed from: a */
    private static void m19047a(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4, int[] iArr5) {
        Nat224.m19937a(iArr, iArr4);
        int[] a = Nat224.m19939a();
        int[] a2 = Nat224.m19939a();
        for (int i = 0; i < 7; i++) {
            Nat224.m19937a(iArr2, a);
            Nat224.m19937a(iArr3, a2);
            int i2 = 1 << i;
            while (true) {
                i2--;
                if (i2 < 0) {
                    break;
                }
                m19046a(iArr2, iArr3, iArr4, iArr5);
            }
            m19048a(iArr, a, a2, iArr2, iArr3, iArr4, iArr5);
        }
    }

    /* renamed from: a */
    private static void m19046a(int[] iArr, int[] iArr2, int[] iArr3, int[] iArr4) {
        SecP224R1Field.m18992b(iArr2, iArr, iArr2);
        SecP224R1Field.m18997e(iArr2, iArr2);
        SecP224R1Field.m18995d(iArr, iArr4);
        SecP224R1Field.m18988a(iArr3, iArr4, iArr);
        SecP224R1Field.m18992b(iArr3, iArr4, iArr3);
        SecP224R1Field.m18984a(Nat.m20045c(7, iArr3, 2, 0), iArr3);
    }

    /* renamed from: a */
    private static boolean m19050a(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] a = Nat224.m19939a();
        Nat224.m19937a(iArr2, a);
        int[] a2 = Nat224.m19939a();
        a2[0] = 1;
        int[] a3 = Nat224.m19939a();
        m19047a(iArr, a, a2, a3, iArr3);
        int[] a4 = Nat224.m19939a();
        int[] a5 = Nat224.m19939a();
        for (int i = 1; i < 96; i++) {
            Nat224.m19937a(a, a4);
            Nat224.m19937a(a2, a5);
            m19046a(a, a2, a3, iArr3);
            if (Nat224.m19942b(a)) {
                Mod.m19851a(SecP224R1Field.f12279a, a5, iArr3);
                SecP224R1Field.m18992b(iArr3, a4, iArr3);
                return true;
            }
        }
        return false;
    }
}
