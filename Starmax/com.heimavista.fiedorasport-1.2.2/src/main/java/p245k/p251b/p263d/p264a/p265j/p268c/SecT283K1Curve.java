package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.g2 */
public class SecT283K1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT283K1Point f12305i = new SecT283K1Point(this, null, null);

    public SecT283K1Curve() {
        super(283, 5, 7, 12);
        this.f12223b = mo28235a(BigInteger.valueOf(0));
        this.f12224c = mo28235a(BigInteger.valueOf(1));
        this.f12225d = new BigInteger(1, Hex.m20078a("01FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9AE2ED07577265DFF7F94451E061E163C61"));
        this.f12226e = BigInteger.valueOf(4);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT283K1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 283;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12305i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT283FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT283K1Point(this, dVar, dVar2, z);
    }
}
