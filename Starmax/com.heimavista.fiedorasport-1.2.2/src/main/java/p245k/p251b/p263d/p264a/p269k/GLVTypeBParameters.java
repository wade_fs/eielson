package p245k.p251b.p263d.p264a.p269k;

import java.math.BigInteger;

/* renamed from: k.b.d.a.k.d */
public class GLVTypeBParameters {

    /* renamed from: a */
    protected final BigInteger f12381a;

    public GLVTypeBParameters(BigInteger bigInteger, BigInteger bigInteger2, BigInteger[] bigIntegerArr, BigInteger[] bigIntegerArr2, BigInteger bigInteger3, BigInteger bigInteger4, int i) {
        m19827a(bigIntegerArr, "v1");
        m19827a(bigIntegerArr2, "v2");
        this.f12381a = bigInteger;
        BigInteger bigInteger5 = bigIntegerArr[0];
        BigInteger bigInteger6 = bigIntegerArr[1];
        BigInteger bigInteger7 = bigIntegerArr2[0];
        BigInteger bigInteger8 = bigIntegerArr2[1];
    }

    /* renamed from: a */
    private static void m19827a(BigInteger[] bigIntegerArr, String str) {
        if (bigIntegerArr == null || bigIntegerArr.length != 2 || bigIntegerArr[0] == null || bigIntegerArr[1] == null) {
            throw new IllegalArgumentException("'" + str + "' must consist of exactly 2 (non-null) values");
        }
    }

    /* renamed from: a */
    public BigInteger mo28392a() {
        return this.f12381a;
    }
}
