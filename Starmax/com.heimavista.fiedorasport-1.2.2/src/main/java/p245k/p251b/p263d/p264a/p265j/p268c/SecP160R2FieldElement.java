package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat160;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.m */
public class SecP160R2FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12327e = SecP160R2Curve.f12318j;

    /* renamed from: d */
    protected int[] f12328d;

    public SecP160R2FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12327e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP160R2FieldElement");
        }
        this.f12328d = SecP160R2Field.m19347a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19346a(this.f12328d, ((SecP160R2FieldElement) dVar).f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        Mod.m19851a(SecP160R2Field.f12323a, ((SecP160R2FieldElement) dVar).f12328d, a);
        SecP160R2Field.m19349b(a, this.f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12327e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19353d(this.f12328d, ((SecP160R2FieldElement) dVar).f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat160.m19882a(this.f12328d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP160R2FieldElement)) {
            return false;
        }
        return Nat160.m19883a(this.f12328d, ((SecP160R2FieldElement) obj).f12328d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat160.m19888b(this.f12328d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19348b(this.f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12328d;
        if (Nat160.m19888b(iArr) || Nat160.m19882a(iArr)) {
            return super;
        }
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19352d(iArr, a);
        SecP160R2Field.m19349b(a, iArr, a);
        int[] a2 = Nat160.m19884a();
        SecP160R2Field.m19352d(a, a2);
        SecP160R2Field.m19349b(a2, iArr, a2);
        int[] a3 = Nat160.m19884a();
        SecP160R2Field.m19352d(a2, a3);
        SecP160R2Field.m19349b(a3, iArr, a3);
        int[] a4 = Nat160.m19884a();
        SecP160R2Field.m19344a(a3, 3, a4);
        SecP160R2Field.m19349b(a4, a2, a4);
        SecP160R2Field.m19344a(a4, 7, a3);
        SecP160R2Field.m19349b(a3, a4, a3);
        SecP160R2Field.m19344a(a3, 3, a4);
        SecP160R2Field.m19349b(a4, a2, a4);
        int[] a5 = Nat160.m19884a();
        SecP160R2Field.m19344a(a4, 14, a5);
        SecP160R2Field.m19349b(a5, a3, a5);
        SecP160R2Field.m19344a(a5, 31, a3);
        SecP160R2Field.m19349b(a3, a5, a3);
        SecP160R2Field.m19344a(a3, 62, a5);
        SecP160R2Field.m19349b(a5, a3, a5);
        SecP160R2Field.m19344a(a5, 3, a3);
        SecP160R2Field.m19349b(a3, a2, a3);
        SecP160R2Field.m19344a(a3, 18, a3);
        SecP160R2Field.m19349b(a3, a4, a3);
        SecP160R2Field.m19344a(a3, 2, a3);
        SecP160R2Field.m19349b(a3, iArr, a3);
        SecP160R2Field.m19344a(a3, 3, a3);
        SecP160R2Field.m19349b(a3, a, a3);
        SecP160R2Field.m19344a(a3, 6, a3);
        SecP160R2Field.m19349b(a3, a2, a3);
        SecP160R2Field.m19344a(a3, 2, a3);
        SecP160R2Field.m19349b(a3, iArr, a3);
        SecP160R2Field.m19352d(a3, a);
        if (Nat160.m19883a(iArr, a)) {
            return new SecP160R2FieldElement(a3);
        }
        return null;
    }

    public int hashCode() {
        return f12327e.hashCode() ^ Arrays.m20066b(this.f12328d, 0, 5);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19352d(this.f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat160.m19879a(this.f12328d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat160.m19891c(this.f12328d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19349b(this.f12328d, ((SecP160R2FieldElement) dVar).f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19345a(this.f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat160.m19884a();
        Mod.m19851a(SecP160R2Field.f12323a, this.f12328d, a);
        return new SecP160R2FieldElement(a);
    }

    public SecP160R2FieldElement() {
        this.f12328d = Nat160.m19884a();
    }

    protected SecP160R2FieldElement(int[] iArr) {
        this.f12328d = iArr;
    }
}
