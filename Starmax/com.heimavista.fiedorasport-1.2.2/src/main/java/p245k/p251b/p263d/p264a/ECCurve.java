package p245k.p251b.p263d.p264a;

import java.math.BigInteger;
import java.util.Random;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p264a.p269k.ECEndomorphism;
import p245k.p251b.p263d.p270b.FiniteField;
import p245k.p251b.p263d.p270b.FiniteFields;
import p245k.p251b.p272e.BigIntegers;
import p245k.p251b.p272e.Integers;

/* renamed from: k.b.d.a.c */
public abstract class ECCurve {

    /* renamed from: a */
    protected FiniteField f12222a;

    /* renamed from: b */
    protected ECFieldElement f12223b;

    /* renamed from: c */
    protected ECFieldElement f12224c;

    /* renamed from: d */
    protected BigInteger f12225d;

    /* renamed from: e */
    protected BigInteger f12226e;

    /* renamed from: f */
    protected int f12227f = 0;

    /* renamed from: g */
    protected ECEndomorphism f12228g = null;

    /* renamed from: h */
    protected ECMultiplier f12229h = null;

    /* renamed from: k.b.d.a.c$b */
    /* compiled from: ECCurve */
    public static abstract class C5160b extends ECCurve {
        protected C5160b(BigInteger bigInteger) {
            super(FiniteFields.m19831a(bigInteger));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: k.b.d.a.c.a(k.b.d.a.d, k.b.d.a.d, boolean):k.b.d.a.f
         arg types: [k.b.d.a.d, k.b.d.a.d, int]
         candidates:
          k.b.d.a.c.a(java.math.BigInteger, java.math.BigInteger, boolean):k.b.d.a.f
          k.b.d.a.c.a(k.b.d.a.f[], int, int):void
          k.b.d.a.c.a(k.b.d.a.d, k.b.d.a.d, boolean):k.b.d.a.f */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ECPoint mo28236a(int i, BigInteger bigInteger) {
            ECFieldElement a = mo28235a(bigInteger);
            ECFieldElement h = a.mo28277i().mo28263a(super.f12223b).mo28270c(a).mo28263a(super.f12224c).mo28276h();
            if (h != null) {
                if (h.mo28278j() != (i == 1)) {
                    h = h.mo28275g();
                }
                return mo28239a(a, h, true);
            }
            throw new IllegalArgumentException("Invalid point compression");
        }
    }

    /* renamed from: k.b.d.a.c$c */
    /* compiled from: ECCurve */
    public class C5161c {

        /* renamed from: a */
        protected int f12230a;

        /* renamed from: b */
        protected ECEndomorphism f12231b;

        /* renamed from: c */
        protected ECMultiplier f12232c;

        C5161c(int i, ECEndomorphism aVar, ECMultiplier eVar) {
            this.f12230a = i;
            this.f12231b = aVar;
            this.f12232c = eVar;
        }

        /* renamed from: a */
        public C5161c mo28260a(ECEndomorphism aVar) {
            this.f12231b = aVar;
            return this;
        }

        /* renamed from: a */
        public ECCurve mo28261a() {
            if (ECCurve.this.mo28245a(this.f12230a)) {
                ECCurve a = ECCurve.this.mo28234a();
                if (a != ECCurve.this) {
                    synchronized (a) {
                        a.f12227f = this.f12230a;
                        a.f12228g = this.f12231b;
                        a.f12229h = this.f12232c;
                    }
                    return a;
                }
                throw new IllegalStateException("implementation returned current curve");
            }
            throw new IllegalStateException("unsupported coordinate system");
        }
    }

    /* renamed from: k.b.d.a.c$d */
    /* compiled from: ECCurve */
    public static class C5162d extends C5159a {

        /* renamed from: i */
        private int f12234i;

        /* renamed from: j */
        private int f12235j;

        /* renamed from: k */
        private int f12236k;

        /* renamed from: l */
        private int f12237l;

        /* renamed from: m */
        private ECPoint.C5168c f12238m;

        public C5162d(int i, int i2, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) {
            this(i, i2, 0, 0, bigInteger, bigInteger2, bigInteger3, bigInteger4);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ECCurve mo28234a() {
            return new C5162d(this.f12234i, this.f12235j, this.f12236k, this.f12237l, this.f12223b, this.f12224c, this.f12225d, this.f12226e);
        }

        /* renamed from: a */
        public boolean mo28245a(int i) {
            return i == 0 || i == 1 || i == 6;
        }

        /* renamed from: h */
        public int mo28256h() {
            return this.f12234i;
        }

        /* renamed from: i */
        public ECPoint mo28258i() {
            return this.f12238m;
        }

        public C5162d(int i, int i2, int i3, int i4, BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4) {
            super(i, i2, i3, i4);
            this.f12234i = i;
            this.f12235j = i2;
            this.f12236k = i3;
            this.f12237l = i4;
            this.f12225d = bigInteger3;
            this.f12226e = bigInteger4;
            this.f12238m = new ECPoint.C5168c(this, null, null);
            this.f12223b = mo28235a(bigInteger);
            this.f12224c = mo28235a(bigInteger2);
            this.f12227f = 6;
        }

        /* renamed from: a */
        public ECFieldElement mo28235a(BigInteger bigInteger) {
            return new ECFieldElement.C5164a(this.f12234i, this.f12235j, this.f12236k, this.f12237l, bigInteger);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
            return new ECPoint.C5168c(this, dVar, dVar2, z);
        }

        protected C5162d(int i, int i2, int i3, int i4, ECFieldElement dVar, ECFieldElement dVar2, BigInteger bigInteger, BigInteger bigInteger2) {
            super(i, i2, i3, i4);
            this.f12234i = i;
            this.f12235j = i2;
            this.f12236k = i3;
            this.f12237l = i4;
            this.f12225d = bigInteger;
            this.f12226e = bigInteger2;
            this.f12238m = new ECPoint.C5168c(this, null, null);
            this.f12223b = dVar;
            this.f12224c = dVar2;
            this.f12227f = 6;
        }
    }

    /* renamed from: k.b.d.a.c$e */
    /* compiled from: ECCurve */
    public static class C5163e extends C5160b {

        /* renamed from: i */
        BigInteger f12239i;

        /* renamed from: j */
        BigInteger f12240j;

        /* renamed from: k */
        ECPoint.C5169d f12241k = new ECPoint.C5169d(this, null, null);

        public C5163e(BigInteger bigInteger, BigInteger bigInteger2, BigInteger bigInteger3, BigInteger bigInteger4, BigInteger bigInteger5) {
            super(bigInteger);
            this.f12239i = bigInteger;
            this.f12240j = ECFieldElement.C5165b.m18741e(bigInteger);
            this.f12223b = mo28235a(bigInteger2);
            this.f12224c = mo28235a(bigInteger3);
            this.f12225d = bigInteger4;
            this.f12226e = bigInteger5;
            this.f12227f = 4;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ECCurve mo28234a() {
            return new C5163e(this.f12239i, this.f12240j, this.f12223b, this.f12224c, this.f12225d, this.f12226e);
        }

        /* renamed from: a */
        public boolean mo28245a(int i) {
            return i == 0 || i == 1 || i == 2 || i == 4;
        }

        /* renamed from: h */
        public int mo28256h() {
            return this.f12239i.bitLength();
        }

        /* renamed from: i */
        public ECPoint mo28258i() {
            return this.f12241k;
        }

        /* renamed from: a */
        public ECFieldElement mo28235a(BigInteger bigInteger) {
            return new ECFieldElement.C5165b(this.f12239i, this.f12240j, bigInteger);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
            return new ECPoint.C5169d(this, dVar, dVar2, z);
        }

        /* renamed from: a */
        public ECPoint mo28240a(ECPoint fVar) {
            int f;
            if (this == fVar.mo28301d() || mo28254f() != 2 || fVar.mo28309j() || ((f = fVar.mo28301d().mo28254f()) != 2 && f != 3 && f != 4)) {
                return ECCurve.super.mo28240a(fVar);
            }
            return new ECPoint.C5169d(this, mo28235a(fVar.f12251b.mo28279k()), mo28235a(fVar.f12252c.mo28279k()), new ECFieldElement[]{mo28235a(fVar.f12253d[0].mo28279k())}, fVar.f12254e);
        }

        protected C5163e(BigInteger bigInteger, BigInteger bigInteger2, ECFieldElement dVar, ECFieldElement dVar2, BigInteger bigInteger3, BigInteger bigInteger4) {
            super(bigInteger);
            this.f12239i = bigInteger;
            this.f12240j = bigInteger2;
            this.f12223b = dVar;
            this.f12224c = dVar2;
            this.f12225d = bigInteger3;
            this.f12226e = bigInteger4;
            this.f12227f = 4;
        }
    }

    protected ECCurve(FiniteField bVar) {
        this.f12222a = bVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract ECCurve mo28234a();

    /* renamed from: a */
    public abstract ECFieldElement mo28235a(BigInteger bigInteger);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract ECPoint mo28236a(int i, BigInteger bigInteger);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.b.d.a.c.a(java.math.BigInteger, java.math.BigInteger, boolean):k.b.d.a.f
     arg types: [java.math.BigInteger, java.math.BigInteger, int]
     candidates:
      k.b.d.a.c.a(k.b.d.a.d, k.b.d.a.d, boolean):k.b.d.a.f
      k.b.d.a.c.a(k.b.d.a.f[], int, int):void
      k.b.d.a.c.a(java.math.BigInteger, java.math.BigInteger, boolean):k.b.d.a.f */
    /* renamed from: a */
    public ECPoint mo28237a(BigInteger bigInteger, BigInteger bigInteger2) {
        return mo28238a(bigInteger, bigInteger2, false);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z);

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 0;
    }

    /* renamed from: b */
    public synchronized C5161c mo28247b() {
        return new C5161c(this.f12227f, this.f12228g, this.f12229h);
    }

    /* renamed from: c */
    public ECFieldElement mo28250c() {
        return this.f12223b;
    }

    /* renamed from: d */
    public ECFieldElement mo28251d() {
        return this.f12224c;
    }

    /* renamed from: e */
    public BigInteger mo28252e() {
        return this.f12226e;
    }

    public boolean equals(Object obj) {
        return this == obj || ((obj instanceof ECCurve) && mo28246a((ECCurve) obj));
    }

    /* renamed from: f */
    public int mo28254f() {
        return this.f12227f;
    }

    /* renamed from: g */
    public FiniteField mo28255g() {
        return this.f12222a;
    }

    /* renamed from: h */
    public abstract int mo28256h();

    public int hashCode() {
        return (mo28255g().hashCode() ^ Integers.m20069a(mo28250c().mo28279k().hashCode(), 8)) ^ Integers.m20069a(mo28251d().mo28279k().hashCode(), 16);
    }

    /* renamed from: i */
    public abstract ECPoint mo28258i();

    /* renamed from: j */
    public BigInteger mo28259j() {
        return this.f12225d;
    }

    /* renamed from: a */
    public ECPoint mo28238a(BigInteger bigInteger, BigInteger bigInteger2, boolean z) {
        return mo28239a(mo28235a(bigInteger), mo28235a(bigInteger2), z);
    }

    /* renamed from: b */
    public ECPoint mo28248b(BigInteger bigInteger, BigInteger bigInteger2) {
        ECPoint a = mo28237a(bigInteger, bigInteger2);
        if (a.mo28311l()) {
            return a;
        }
        throw new IllegalArgumentException("Invalid point coordinates");
    }

    /* renamed from: a */
    public ECPoint mo28240a(ECPoint fVar) {
        if (this == fVar.mo28301d()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return mo28258i();
        }
        ECPoint n = fVar.mo28313n();
        return mo28249b(n.mo28306h().mo28279k(), n.mo28308i().mo28279k(), n.f12254e);
    }

    /* renamed from: b */
    public ECPoint mo28249b(BigInteger bigInteger, BigInteger bigInteger2, boolean z) {
        ECPoint a = mo28238a(bigInteger, bigInteger2, z);
        if (a.mo28311l()) {
            return a;
        }
        throw new IllegalArgumentException("Invalid point coordinates");
    }

    /* renamed from: k.b.d.a.c$a */
    /* compiled from: ECCurve */
    public static abstract class C5159a extends ECCurve {
        protected C5159a(int i, int i2, int i3, int i4) {
            super(m18683a(i, i2, i3, i4));
        }

        /* renamed from: a */
        private static FiniteField m18683a(int i, int i2, int i3, int i4) {
            if (i2 == 0) {
                throw new IllegalArgumentException("k1 must be > 0");
            } else if (i3 == 0) {
                if (i4 == 0) {
                    return FiniteFields.m19832a(new int[]{0, i2, i});
                }
                throw new IllegalArgumentException("k3 must be 0 if k2 == 0");
            } else if (i3 <= i2) {
                throw new IllegalArgumentException("k2 must be > k1");
            } else if (i4 > i3) {
                return FiniteFields.m19832a(new int[]{0, i2, i3, i4, i});
            } else {
                throw new IllegalArgumentException("k3 must be > k2");
            }
        }

        /* renamed from: a */
        public ECPoint mo28238a(BigInteger bigInteger, BigInteger bigInteger2, boolean z) {
            ECFieldElement a = mo28235a(bigInteger);
            ECFieldElement a2 = mo28235a(bigInteger2);
            int f = mo28254f();
            if (f == 5 || f == 6) {
                if (!a.mo28274f()) {
                    a2 = a2.mo28267b(a).mo28263a(a);
                } else if (!a2.mo28277i().equals(mo28251d())) {
                    throw new IllegalArgumentException();
                }
            }
            return mo28239a(a, a2, z);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: k.b.d.a.c.a(k.b.d.a.d, k.b.d.a.d, boolean):k.b.d.a.f
         arg types: [k.b.d.a.d, k.b.d.a.d, int]
         candidates:
          k.b.d.a.c.a.a(java.math.BigInteger, java.math.BigInteger, boolean):k.b.d.a.f
          k.b.d.a.c.a(java.math.BigInteger, java.math.BigInteger, boolean):k.b.d.a.f
          k.b.d.a.c.a(k.b.d.a.f[], int, int):void
          k.b.d.a.c.a(k.b.d.a.d, k.b.d.a.d, boolean):k.b.d.a.f */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ECPoint mo28236a(int i, BigInteger bigInteger) {
            ECFieldElement dVar;
            ECFieldElement a = mo28235a(bigInteger);
            if (a.mo28274f()) {
                dVar = mo28251d().mo28276h();
            } else {
                ECFieldElement a2 = m18682a(a.mo28277i().mo28271d().mo28270c(mo28251d()).mo28263a(mo28250c()).mo28263a(a));
                if (a2 != null) {
                    if (a2.mo28278j() != (i == 1)) {
                        a2 = a2.mo28262a();
                    }
                    int f = mo28254f();
                    dVar = (f == 5 || f == 6) ? a2.mo28263a(a) : a2.mo28270c(a);
                } else {
                    dVar = null;
                }
            }
            if (dVar != null) {
                return mo28239a(a, dVar, true);
            }
            throw new IllegalArgumentException("Invalid point compression");
        }

        /* renamed from: a */
        private ECFieldElement m18682a(ECFieldElement dVar) {
            ECFieldElement dVar2;
            if (dVar.mo28274f()) {
                return dVar;
            }
            ECFieldElement a = mo28235a(ECConstants.f12219a);
            int h = mo28256h();
            Random random = new Random();
            do {
                ECFieldElement a2 = mo28235a(new BigInteger(h, random));
                ECFieldElement dVar3 = dVar;
                dVar2 = a;
                for (int i = 1; i < h; i++) {
                    ECFieldElement i2 = dVar3.mo28277i();
                    dVar2 = dVar2.mo28277i().mo28263a(i2.mo28270c(a2));
                    dVar3 = i2.mo28263a(dVar);
                }
                if (!dVar3.mo28274f()) {
                    return null;
                }
            } while (dVar2.mo28277i().mo28263a(dVar2).mo28274f());
            return dVar2;
        }
    }

    /* renamed from: a */
    public void mo28242a(ECPoint[] fVarArr) {
        mo28244a(fVarArr, 0, fVarArr.length, null);
    }

    /* renamed from: a */
    public void mo28244a(ECPoint[] fVarArr, int i, int i2, ECFieldElement dVar) {
        mo28243a(fVarArr, i, i2);
        int f = mo28254f();
        if (f != 0 && f != 5) {
            ECFieldElement[] dVarArr = new ECFieldElement[i2];
            int[] iArr = new int[i2];
            int i3 = 0;
            for (int i4 = 0; i4 < i2; i4++) {
                int i5 = i + i4;
                ECPoint fVar = fVarArr[i5];
                if (fVar != null && (dVar != null || !fVar.mo28310k())) {
                    dVarArr[i3] = fVar.mo28293a(0);
                    iArr[i3] = i5;
                    i3++;
                }
            }
            if (i3 != 0) {
                ECAlgorithms.m18653a(dVarArr, 0, i3, dVar);
                for (int i6 = 0; i6 < i3; i6++) {
                    int i7 = iArr[i6];
                    fVarArr[i7] = fVarArr[i7].mo28294a(dVarArr[i6]);
                }
            }
        } else if (dVar != null) {
            throw new IllegalArgumentException("'iso' not valid for affine coordinates");
        }
    }

    /* renamed from: a */
    public ECPoint mo28241a(byte[] bArr) {
        ECPoint fVar;
        int h = (mo28256h() + 7) / 8;
        boolean z = false;
        byte b = bArr[0];
        if (b != 0) {
            if (b == 2 || b == 3) {
                if (bArr.length == h + 1) {
                    fVar = mo28236a(b & 1, BigIntegers.m20068a(bArr, 1, h));
                    if (!fVar.mo28314o()) {
                        throw new IllegalArgumentException("Invalid point");
                    }
                } else {
                    throw new IllegalArgumentException("Incorrect length for compressed encoding");
                }
            } else if (b != 4) {
                if (b != 6 && b != 7) {
                    throw new IllegalArgumentException("Invalid point encoding 0x" + Integer.toString(b, 16));
                } else if (bArr.length == (h * 2) + 1) {
                    BigInteger a = BigIntegers.m20068a(bArr, 1, h);
                    BigInteger a2 = BigIntegers.m20068a(bArr, h + 1, h);
                    boolean testBit = a2.testBit(0);
                    if (b == 7) {
                        z = true;
                    }
                    if (testBit == z) {
                        fVar = mo28248b(a, a2);
                    } else {
                        throw new IllegalArgumentException("Inconsistent Y coordinate in hybrid encoding");
                    }
                } else {
                    throw new IllegalArgumentException("Incorrect length for hybrid encoding");
                }
            } else if (bArr.length == (h * 2) + 1) {
                fVar = mo28248b(BigIntegers.m20068a(bArr, 1, h), BigIntegers.m20068a(bArr, h + 1, h));
            } else {
                throw new IllegalArgumentException("Incorrect length for uncompressed encoding");
            }
        } else if (bArr.length == 1) {
            fVar = mo28258i();
        } else {
            throw new IllegalArgumentException("Incorrect length for infinity encoding");
        }
        if (b == 0 || !fVar.mo28309j()) {
            return fVar;
        }
        throw new IllegalArgumentException("Invalid infinity encoding");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo28243a(ECPoint[] fVarArr, int i, int i2) {
        if (fVarArr == null) {
            throw new IllegalArgumentException("'points' cannot be null");
        } else if (i < 0 || i2 < 0 || i > fVarArr.length - i2) {
            throw new IllegalArgumentException("invalid range specified for 'points'");
        } else {
            int i3 = 0;
            while (i3 < i2) {
                ECPoint fVar = fVarArr[i + i3];
                if (fVar == null || this == fVar.mo28301d()) {
                    i3++;
                } else {
                    throw new IllegalArgumentException("'points' entries must be null or on this curve");
                }
            }
        }
    }

    /* renamed from: a */
    public boolean mo28246a(ECCurve cVar) {
        return this == cVar || (cVar != null && mo28255g().equals(cVar.mo28255g()) && mo28250c().mo28279k().equals(cVar.mo28250c().mo28279k()) && mo28251d().mo28279k().equals(cVar.mo28251d().mo28279k()));
    }
}
