package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.k */
public class SecP160R2Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12318j = new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFAC73"));

    /* renamed from: i */
    protected SecP160R2Point f12319i = new SecP160R2Point(this, null, null);

    public SecP160R2Curve() {
        super(f12318j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFAC70")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("B4E134D3FB59EB8BAB57274904664D5AF50388BA")));
        this.f12225d = new BigInteger(1, Hex.m20078a("0100000000000000000000351EE786A818F3A1A16B"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP160R2Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12318j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12319i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP160R2FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP160R2Point(this, dVar, dVar2, z);
    }
}
