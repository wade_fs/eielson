package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.o0 */
public class SecP384R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12338e = SecP384R1Curve.f12329j;

    /* renamed from: d */
    protected int[] f12339d;

    public SecP384R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12338e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP384R1FieldElement");
        }
        this.f12339d = SecP384R1Field.m19419a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat.m20031a(12);
        SecP384R1Field.m19418a(this.f12339d, ((SecP384R1FieldElement) dVar).f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat.m20031a(12);
        Mod.m19851a(SecP384R1Field.f12333a, ((SecP384R1FieldElement) dVar).f12339d, a);
        SecP384R1Field.m19424c(a, this.f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12338e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat.m20031a(12);
        SecP384R1Field.m19426d(this.f12339d, ((SecP384R1FieldElement) dVar).f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat.m20051d(12, this.f12339d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP384R1FieldElement)) {
            return false;
        }
        return Nat.m20040b(12, this.f12339d, ((SecP384R1FieldElement) obj).f12339d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat.m20053e(12, this.f12339d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat.m20031a(12);
        SecP384R1Field.m19421b(this.f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12339d;
        if (Nat.m20053e(12, iArr) || Nat.m20051d(12, iArr)) {
            return super;
        }
        int[] a = Nat.m20031a(12);
        int[] a2 = Nat.m20031a(12);
        int[] a3 = Nat.m20031a(12);
        int[] a4 = Nat.m20031a(12);
        SecP384R1Field.m19425d(iArr, a);
        SecP384R1Field.m19424c(a, iArr, a);
        SecP384R1Field.m19416a(a, 2, a2);
        SecP384R1Field.m19424c(a2, a, a2);
        SecP384R1Field.m19425d(a2, a2);
        SecP384R1Field.m19424c(a2, iArr, a2);
        SecP384R1Field.m19416a(a2, 5, a3);
        SecP384R1Field.m19424c(a3, a2, a3);
        SecP384R1Field.m19416a(a3, 5, a4);
        SecP384R1Field.m19424c(a4, a2, a4);
        SecP384R1Field.m19416a(a4, 15, a2);
        SecP384R1Field.m19424c(a2, a4, a2);
        SecP384R1Field.m19416a(a2, 2, a3);
        SecP384R1Field.m19424c(a, a3, a);
        SecP384R1Field.m19416a(a3, 28, a3);
        SecP384R1Field.m19424c(a2, a3, a2);
        SecP384R1Field.m19416a(a2, 60, a3);
        SecP384R1Field.m19424c(a3, a2, a3);
        SecP384R1Field.m19416a(a3, 120, a2);
        SecP384R1Field.m19424c(a2, a3, a2);
        SecP384R1Field.m19416a(a2, 15, a2);
        SecP384R1Field.m19424c(a2, a4, a2);
        SecP384R1Field.m19416a(a2, 33, a2);
        SecP384R1Field.m19424c(a2, a, a2);
        SecP384R1Field.m19416a(a2, 64, a2);
        SecP384R1Field.m19424c(a2, iArr, a2);
        SecP384R1Field.m19416a(a2, 30, a);
        SecP384R1Field.m19425d(a, a2);
        if (Nat.m20040b(12, iArr, a2)) {
            return new SecP384R1FieldElement(a);
        }
        return null;
    }

    public int hashCode() {
        return f12338e.hashCode() ^ Arrays.m20066b(this.f12339d, 0, 12);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat.m20031a(12);
        SecP384R1Field.m19425d(this.f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat.m20027a(this.f12339d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat.m20054f(12, this.f12339d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat.m20031a(12);
        SecP384R1Field.m19424c(this.f12339d, ((SecP384R1FieldElement) dVar).f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat.m20031a(12);
        SecP384R1Field.m19417a(this.f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat.m20031a(12);
        Mod.m19851a(SecP384R1Field.f12333a, this.f12339d, a);
        return new SecP384R1FieldElement(a);
    }

    public SecP384R1FieldElement() {
        this.f12339d = Nat.m20031a(12);
    }

    protected SecP384R1FieldElement(int[] iArr) {
        this.f12339d = iArr;
    }
}
