package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.i0 */
public class SecP256R1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12312j = new BigInteger(1, Hex.m20078a("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF"));

    /* renamed from: i */
    protected SecP256R1Point f12313i = new SecP256R1Point(this, null, null);

    public SecP256R1Curve() {
        super(f12312j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("FFFFFFFF00000001000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFC")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("5AC635D8AA3A93E7B3EBBD55769886BC651D06B0CC53B0F63BCE3C3E27D2604B")));
        this.f12225d = new BigInteger(1, Hex.m20078a("FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP256R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12312j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12313i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP256R1FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP256R1Point(this, dVar, dVar2, z);
    }
}
