package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat448;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.l2 */
public class SecT409FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12326d;

    public SecT409FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 409) {
            throw new IllegalArgumentException("x value invalid for SecT409FieldElement");
        }
        this.f12326d = SecT409Field.m19331a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] a = Nat448.m20000a();
        SecT409Field.m19330a(this.f12326d, ((SecT409FieldElement) dVar).f12326d, a);
        return new SecT409FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12326d;
        long[] jArr2 = ((SecT409FieldElement) dVar).f12326d;
        long[] jArr3 = ((SecT409FieldElement) dVar2).f12326d;
        long[] jArr4 = ((SecT409FieldElement) dVar3).f12326d;
        long[] b = Nat.m20041b(13);
        SecT409Field.m19339e(jArr, jArr2, b);
        SecT409Field.m19339e(jArr3, jArr4, b);
        long[] a = Nat448.m20000a();
        SecT409Field.m19338e(b, a);
        return new SecT409FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 409;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] a = Nat448.m20000a();
        SecT409Field.m19337d(this.f12326d, ((SecT409FieldElement) dVar).f12326d, a);
        return new SecT409FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat448.m19998a(this.f12326d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT409FieldElement)) {
            return false;
        }
        return Nat448.m19999a(this.f12326d, ((SecT409FieldElement) obj).f12326d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat448.m20002b(this.f12326d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] a = Nat448.m20000a();
        SecT409Field.m19340f(this.f12326d, a);
        return new SecT409FieldElement(a);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12326d, 0, 7) ^ 4090087;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] a = Nat448.m20000a();
        SecT409Field.m19341g(this.f12326d, a);
        return new SecT409FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12326d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat448.m20004c(this.f12326d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] a = Nat448.m20000a();
        SecT409Field.m19336d(this.f12326d, a);
        return new SecT409FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] a = Nat448.m20000a();
        SecT409Field.m19329a(this.f12326d, a);
        return new SecT409FieldElement(a);
    }

    public SecT409FieldElement() {
        this.f12326d = Nat448.m20000a();
    }

    protected SecT409FieldElement(long[] jArr) {
        this.f12326d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12326d;
        long[] jArr2 = ((SecT409FieldElement) dVar).f12326d;
        long[] jArr3 = ((SecT409FieldElement) dVar2).f12326d;
        long[] b = Nat.m20041b(13);
        SecT409Field.m19342h(jArr, b);
        SecT409Field.m19339e(jArr2, jArr3, b);
        long[] a = Nat448.m20000a();
        SecT409Field.m19338e(b, a);
        return new SecT409FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
