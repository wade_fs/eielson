package p245k.p251b.p263d.p271c;

import java.math.BigInteger;
import p245k.p251b.p272e.Pack;

/* renamed from: k.b.d.c.h */
public abstract class Nat320 {
    /* renamed from: a */
    public static boolean m19990a(long[] jArr, long[] jArr2) {
        for (int i = 4; i >= 0; i--) {
            if (jArr[i] != jArr2[i]) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static long[] m19991a() {
        return new long[5];
    }

    /* renamed from: b */
    public static boolean m19993b(long[] jArr) {
        for (int i = 0; i < 5; i++) {
            if (jArr[i] != 0) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    public static long[] m19994b() {
        return new long[10];
    }

    /* renamed from: c */
    public static BigInteger m19995c(long[] jArr) {
        byte[] bArr = new byte[40];
        for (int i = 0; i < 5; i++) {
            long j = jArr[i];
            if (j != 0) {
                Pack.m20071a(j, bArr, (4 - i) << 3);
            }
        }
        return new BigInteger(1, bArr);
    }

    /* renamed from: a */
    public static long[] m19992a(BigInteger bigInteger) {
        if (bigInteger.signum() < 0 || bigInteger.bitLength() > 320) {
            throw new IllegalArgumentException();
        }
        long[] a = m19991a();
        int i = 0;
        while (bigInteger.signum() != 0) {
            a[i] = bigInteger.longValue();
            bigInteger = bigInteger.shiftRight(64);
            i++;
        }
        return a;
    }

    /* renamed from: a */
    public static boolean m19989a(long[] jArr) {
        if (jArr[0] != 1) {
            return false;
        }
        for (int i = 1; i < 5; i++) {
            if (jArr[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
