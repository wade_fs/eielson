package p245k.p251b.p263d.p264a;

import java.math.BigInteger;

/* renamed from: k.b.d.a.b */
public interface ECConstants {

    /* renamed from: a */
    public static final BigInteger f12219a = BigInteger.valueOf(0);

    /* renamed from: b */
    public static final BigInteger f12220b = BigInteger.valueOf(1);

    /* renamed from: c */
    public static final BigInteger f12221c = BigInteger.valueOf(2);

    static {
        BigInteger.valueOf(3);
        BigInteger.valueOf(4);
        BigInteger.valueOf(8);
    }
}
