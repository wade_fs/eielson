package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Interleave;
import p245k.p251b.p263d.p271c.Nat128;

/* renamed from: k.b.d.a.j.c.u0 */
public class SecT113Field {
    /* renamed from: a */
    public static void m19675a(long[] jArr, long[] jArr2, long[] jArr3) {
        jArr3[0] = jArr[0] ^ jArr2[0];
        jArr3[1] = jArr2[1] ^ jArr[1];
    }

    /* renamed from: b */
    public static void m19678b(long[] jArr, long[] jArr2, long[] jArr3) {
        jArr3[0] = jArr[0] ^ jArr2[0];
        jArr3[1] = jArr[1] ^ jArr2[1];
        jArr3[2] = jArr[2] ^ jArr2[2];
        jArr3[3] = jArr2[3] ^ jArr[3];
    }

    /* renamed from: c */
    public static void m19679c(long[] jArr, long[] jArr2) {
        if (!Nat128.m19864b(jArr)) {
            long[] b = Nat128.m19865b();
            long[] b2 = Nat128.m19865b();
            m19685f(jArr, b);
            m19682d(b, jArr, b);
            m19685f(b, b);
            m19682d(b, jArr, b);
            m19673a(b, 3, b2);
            m19682d(b2, b, b2);
            m19685f(b2, b2);
            m19682d(b2, jArr, b2);
            m19673a(b2, 7, b);
            m19682d(b, b2, b);
            m19673a(b, 14, b2);
            m19682d(b2, b, b2);
            m19673a(b2, 28, b);
            m19682d(b, b2, b);
            m19673a(b, 56, b2);
            m19682d(b2, b, b2);
            m19685f(b2, jArr2);
            return;
        }
        throw new IllegalStateException();
    }

    /* renamed from: d */
    public static void m19682d(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] d = Nat128.m19875d();
        m19680c(jArr, jArr2, d);
        m19681d(d, jArr3);
    }

    /* renamed from: e */
    public static void m19684e(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] d = Nat128.m19875d();
        m19680c(jArr, jArr2, d);
        m19678b(jArr3, d, jArr3);
    }

    /* renamed from: f */
    public static void m19685f(long[] jArr, long[] jArr2) {
        long[] d = Nat128.m19875d();
        m19677b(jArr, d);
        m19681d(d, jArr2);
    }

    /* renamed from: g */
    public static void m19686g(long[] jArr, long[] jArr2) {
        long[] d = Nat128.m19875d();
        m19677b(jArr, d);
        m19678b(jArr2, d, jArr2);
    }

    /* renamed from: a */
    public static void m19674a(long[] jArr, long[] jArr2) {
        jArr2[0] = jArr[0] ^ 1;
        jArr2[1] = jArr[1];
    }

    /* renamed from: d */
    public static void m19681d(long[] jArr, long[] jArr2) {
        long j = jArr[0];
        long j2 = jArr[1];
        long j3 = jArr[2];
        long j4 = jArr[3];
        long j5 = j3 ^ ((j4 >>> 40) ^ (j4 >>> 49));
        long j6 = j ^ ((j5 << 15) ^ (j5 << 24));
        long j7 = (j2 ^ ((j4 << 15) ^ (j4 << 24))) ^ ((j5 >>> 40) ^ (j5 >>> 49));
        long j8 = j7 >>> 49;
        jArr2[0] = (j6 ^ j8) ^ (j8 << 9);
        jArr2[1] = 562949953421311L & j7;
    }

    /* renamed from: e */
    public static void m19683e(long[] jArr, long[] jArr2) {
        long a = Interleave.m19844a(jArr[0]);
        long a2 = Interleave.m19844a(jArr[1]);
        long j = (a >>> 32) | (a2 & -4294967296L);
        jArr2[0] = ((j << 57) ^ ((4294967295L & a) | (a2 << 32))) ^ (j << 5);
        jArr2[1] = (j >>> 59) ^ (j >>> 7);
    }

    /* renamed from: a */
    public static long[] m19676a(BigInteger bigInteger) {
        long[] b = Nat128.m19866b(bigInteger);
        m19672a(b, 0);
        return b;
    }

    /* renamed from: b */
    protected static void m19677b(long[] jArr, long[] jArr2) {
        Interleave.m19845a(jArr[0], jArr2, 0);
        Interleave.m19845a(jArr[1], jArr2, 2);
    }

    /* renamed from: a */
    public static void m19672a(long[] jArr, int i) {
        int i2 = i + 1;
        long j = jArr[i2];
        long j2 = j >>> 49;
        jArr[i] = (j2 ^ (j2 << 9)) ^ jArr[i];
        jArr[i2] = j & 562949953421311L;
    }

    /* renamed from: a */
    public static void m19673a(long[] jArr, int i, long[] jArr2) {
        long[] d = Nat128.m19875d();
        m19677b(jArr, d);
        m19681d(d, jArr2);
        while (true) {
            i--;
            if (i > 0) {
                m19677b(jArr2, d);
                m19681d(d, jArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    protected static void m19671a(long j, long j2, long[] jArr, int i) {
        long j3 = j;
        long[] jArr2 = new long[8];
        jArr2[1] = j2;
        jArr2[2] = jArr2[1] << 1;
        jArr2[3] = jArr2[2] ^ j2;
        jArr2[4] = jArr2[2] << 1;
        jArr2[5] = jArr2[4] ^ j2;
        jArr2[6] = jArr2[3] << 1;
        jArr2[7] = jArr2[6] ^ j2;
        long j4 = jArr2[((int) j3) & 7];
        long j5 = 0;
        int i2 = 48;
        do {
            int i3 = (int) (j3 >>> i2);
            long j6 = (jArr2[(i3 >>> 6) & 7] << 6) ^ (jArr2[i3 & 7] ^ (jArr2[(i3 >>> 3) & 7] << 3));
            j4 ^= j6 << i2;
            j5 ^= j6 >>> (-i2);
            i2 -= 9;
        } while (i2 > 0);
        jArr[i] = 144115188075855871L & j4;
        jArr[i + 1] = (((((j3 & 72198606942111744L) & ((j2 << 7) >> 63)) >>> 8) ^ j5) << 7) ^ (j4 >>> 57);
    }

    /* renamed from: c */
    protected static void m19680c(long[] jArr, long[] jArr2, long[] jArr3) {
        long j = jArr[0];
        long j2 = ((jArr[1] << 7) ^ (j >>> 57)) & 144115188075855871L;
        long j3 = j & 144115188075855871L;
        long j4 = jArr2[0];
        long j5 = ((jArr2[1] << 7) ^ (j4 >>> 57)) & 144115188075855871L;
        long j6 = 144115188075855871L & j4;
        long[] jArr4 = new long[6];
        long[] jArr5 = jArr4;
        m19671a(j3, j6, jArr4, 0);
        m19671a(j2, j5, jArr5, 2);
        m19671a(j3 ^ j2, j6 ^ j5, jArr5, 4);
        long j7 = jArr5[1] ^ jArr5[2];
        long j8 = jArr5[0];
        long j9 = jArr5[3];
        long j10 = (jArr5[4] ^ j8) ^ j7;
        long j11 = j7 ^ (jArr5[5] ^ j9);
        jArr3[0] = j8 ^ (j10 << 57);
        jArr3[1] = (j10 >>> 7) ^ (j11 << 50);
        jArr3[2] = (j11 >>> 14) ^ (j9 << 43);
        jArr3[3] = j9 >>> 21;
    }
}
