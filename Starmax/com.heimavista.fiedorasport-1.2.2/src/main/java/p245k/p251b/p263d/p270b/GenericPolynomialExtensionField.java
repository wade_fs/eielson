package p245k.p251b.p263d.p270b;

import java.math.BigInteger;
import p245k.p251b.p272e.Integers;

/* renamed from: k.b.d.b.e */
class GenericPolynomialExtensionField implements PolynomialExtensionField {

    /* renamed from: a */
    protected final FiniteField f12385a;

    /* renamed from: b */
    protected final Polynomial f12386b;

    GenericPolynomialExtensionField(FiniteField bVar, Polynomial fVar) {
        this.f12385a = bVar;
        this.f12386b = fVar;
    }

    /* renamed from: a */
    public Polynomial mo28399a() {
        return this.f12386b;
    }

    /* renamed from: b */
    public int mo28393b() {
        return this.f12385a.mo28393b() * this.f12386b.mo28396b();
    }

    /* renamed from: c */
    public BigInteger mo28394c() {
        return this.f12385a.mo28394c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GenericPolynomialExtensionField)) {
            return false;
        }
        GenericPolynomialExtensionField eVar = (GenericPolynomialExtensionField) obj;
        if (!this.f12385a.equals(eVar.f12385a) || !this.f12386b.equals(eVar.f12386b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.f12385a.hashCode() ^ Integers.m20069a(this.f12386b.hashCode(), 16);
    }
}
