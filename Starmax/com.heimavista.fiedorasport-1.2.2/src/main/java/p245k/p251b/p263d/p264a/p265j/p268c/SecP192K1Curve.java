package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECConstants;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.o */
public class SecP192K1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12336j = new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFEE37"));

    /* renamed from: i */
    protected SecP192K1Point f12337i = new SecP192K1Point(this, null, null);

    public SecP192K1Curve() {
        super(f12336j);
        this.f12223b = mo28235a(ECConstants.f12219a);
        this.f12224c = mo28235a(BigInteger.valueOf(3));
        this.f12225d = new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFE26F2FC170F69466A74DEFD8D"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP192K1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12336j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12337i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP192K1FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP192K1Point(this, dVar, dVar2, z);
    }
}
