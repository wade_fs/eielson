package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.s1 */
public class SecT193R2Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT193R2Point f12357i = new SecT193R2Point(this, null, null);

    public SecT193R2Curve() {
        super(193, 15, 0, 0);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("0163F35A5137C2CE3EA6ED8667190B0BC43ECD69977702709B")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("00C9BB9E8927D4D64C377E2AB2856A5B16E3EFB7F61D4316AE")));
        this.f12225d = new BigInteger(1, Hex.m20078a("010000000000000000000000015AAB561B005413CCD4EE99D5"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT193R2Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 193;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12357i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT193FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT193R2Point(this, dVar, dVar2, z);
    }
}
