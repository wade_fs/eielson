package p245k.p251b.p263d.p271c;

/* renamed from: k.b.d.c.i */
public abstract class Nat384 {
    /* renamed from: a */
    public static void m19997a(int[] iArr, int[] iArr2, int[] iArr3) {
        Nat192.m19925c(iArr, iArr2, iArr3);
        Nat192.m19913b(iArr, 6, iArr2, 6, iArr3, 12);
        int a = Nat192.m19901a(iArr3, 6, iArr3, 12);
        int a2 = a + Nat192.m19902a(iArr3, 18, iArr3, 12, Nat192.m19902a(iArr3, 0, iArr3, 6, 0) + a);
        int[] a3 = Nat192.m19910a();
        int[] a4 = Nat192.m19910a();
        boolean z = Nat192.m19906a(iArr, 6, iArr, 0, a3, 0) != Nat192.m19906a(iArr2, 6, iArr2, 0, a4, 0);
        int[] c = Nat192.m19926c();
        Nat192.m19925c(a3, a4, c);
        Nat.m20015a(24, a2 + (z ? Nat.m20024a(12, c, 0, iArr3, 6) : Nat.m20038b(12, c, 0, iArr3, 6)), iArr3, 18);
    }

    /* renamed from: a */
    public static void m19996a(int[] iArr, int[] iArr2) {
        Nat192.m19924c(iArr, iArr2);
        Nat192.m19923c(iArr, 6, iArr2, 12);
        int a = Nat192.m19901a(iArr2, 6, iArr2, 12);
        int a2 = a + Nat192.m19902a(iArr2, 18, iArr2, 12, Nat192.m19902a(iArr2, 0, iArr2, 6, 0) + a);
        int[] a3 = Nat192.m19910a();
        Nat192.m19906a(iArr, 6, iArr, 0, a3, 0);
        int[] c = Nat192.m19926c();
        Nat192.m19924c(a3, c);
        Nat.m20015a(24, a2 + Nat.m20038b(12, c, 0, iArr2, 6), iArr2, 18);
    }
}
