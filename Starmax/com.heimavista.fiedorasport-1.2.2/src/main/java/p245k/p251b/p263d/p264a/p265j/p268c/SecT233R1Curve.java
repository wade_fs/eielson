package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.y1 */
public class SecT233R1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT233R1Point f12380i = new SecT233R1Point(this, null, null);

    public SecT233R1Curve() {
        super(233, 74, 0, 0);
        this.f12223b = mo28235a(BigInteger.valueOf(1));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("0066647EDE6C332C7F8C0923BB58213B333B20E9CE4281FE115F7D8F90AD")));
        this.f12225d = new BigInteger(1, Hex.m20078a("01000000000000000000000000000013E974E72F8A6922031D2603CFE0D7"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT233R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 233;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12380i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT233FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT233R1Point(this, dVar, dVar2, z);
    }
}
