package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.c2 */
public class SecT239K1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT239K1Point f12289i = new SecT239K1Point(this, null, null);

    public SecT239K1Curve() {
        super(239, 158, 0, 0);
        this.f12223b = mo28235a(BigInteger.valueOf(0));
        this.f12224c = mo28235a(BigInteger.valueOf(1));
        this.f12225d = new BigInteger(1, Hex.m20078a("2000000000000000000000000000005A79FEC67CB6E91F1C1DA800E478A5"));
        this.f12226e = BigInteger.valueOf(4);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT239K1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 239;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12289i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT239FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT239K1Point(this, dVar, dVar2, z);
    }
}
