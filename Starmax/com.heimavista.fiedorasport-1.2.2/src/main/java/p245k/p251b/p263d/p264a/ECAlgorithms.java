package p245k.p251b.p263d.p264a;

import java.math.BigInteger;
import p245k.p251b.p263d.p270b.FiniteField;
import p245k.p251b.p263d.p270b.PolynomialExtensionField;

/* renamed from: k.b.d.a.a */
public class ECAlgorithms {
    /* renamed from: a */
    public static boolean m18654a(ECCurve cVar) {
        return m18655a(cVar.mo28255g());
    }

    /* renamed from: b */
    public static boolean m18656b(ECCurve cVar) {
        return m18657b(cVar.mo28255g());
    }

    /* renamed from: a */
    public static boolean m18655a(FiniteField bVar) {
        return bVar.mo28393b() > 1 && bVar.mo28394c().equals(ECConstants.f12221c) && (bVar instanceof PolynomialExtensionField);
    }

    /* renamed from: b */
    public static boolean m18657b(FiniteField bVar) {
        return bVar.mo28393b() == 1;
    }

    /* renamed from: a */
    public static void m18653a(ECFieldElement[] dVarArr, int i, int i2, ECFieldElement dVar) {
        ECFieldElement[] dVarArr2 = new ECFieldElement[i2];
        int i3 = 0;
        dVarArr2[0] = dVarArr[i];
        while (true) {
            i3++;
            if (i3 >= i2) {
                break;
            }
            dVarArr2[i3] = dVarArr2[i3 - 1].mo28270c(dVarArr[i + i3]);
        }
        int i4 = i3 - 1;
        if (dVar != null) {
            dVarArr2[i4] = dVarArr2[i4].mo28270c(dVar);
        }
        ECFieldElement d = dVarArr2[i4].mo28271d();
        while (i4 > 0) {
            int i5 = i4 - 1;
            int i6 = i4 + i;
            ECFieldElement dVar2 = dVarArr[i6];
            dVarArr[i6] = dVarArr2[i5].mo28270c(d);
            d = d.mo28270c(dVar2);
            i4 = i5;
        }
        dVarArr[i] = d;
    }

    /* renamed from: a */
    public static ECPoint m18652a(ECPoint fVar, BigInteger bigInteger) {
        BigInteger abs = bigInteger.abs();
        ECPoint i = fVar.mo28301d().mo28258i();
        int bitLength = abs.bitLength();
        if (bitLength > 0) {
            if (abs.testBit(0)) {
                i = fVar;
            }
            for (int i2 = 1; i2 < bitLength; i2++) {
                fVar = fVar.mo28316q();
                if (abs.testBit(i2)) {
                    i = i.mo28296a(fVar);
                }
            }
        }
        return bigInteger.signum() < 0 ? i.mo28312m() : i;
    }
}
