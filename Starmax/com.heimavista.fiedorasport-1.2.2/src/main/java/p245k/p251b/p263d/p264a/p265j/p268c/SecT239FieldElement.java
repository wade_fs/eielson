package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.b2 */
public class SecT239FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12283d;

    public SecT239FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 239) {
            throw new IllegalArgumentException("x value invalid for SecT239FieldElement");
        }
        this.f12283d = SecT239Field.m18958a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] b = Nat256.m19974b();
        SecT239Field.m18957a(this.f12283d, ((SecT239FieldElement) dVar).f12283d, b);
        return new SecT239FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12283d;
        long[] jArr2 = ((SecT239FieldElement) dVar).f12283d;
        long[] jArr3 = ((SecT239FieldElement) dVar2).f12283d;
        long[] jArr4 = ((SecT239FieldElement) dVar3).f12283d;
        long[] d = Nat256.m19986d();
        SecT239Field.m18966e(jArr, jArr2, d);
        SecT239Field.m18966e(jArr3, jArr4, d);
        long[] b = Nat256.m19974b();
        SecT239Field.m18965e(d, b);
        return new SecT239FieldElement(b);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 239;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] b = Nat256.m19974b();
        SecT239Field.m18964d(this.f12283d, ((SecT239FieldElement) dVar).f12283d, b);
        return new SecT239FieldElement(b);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19964a(this.f12283d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT239FieldElement)) {
            return false;
        }
        return Nat256.m19965a(this.f12283d, ((SecT239FieldElement) obj).f12283d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19973b(this.f12283d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] b = Nat256.m19974b();
        SecT239Field.m18967f(this.f12283d, b);
        return new SecT239FieldElement(b);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12283d, 0, 4) ^ 23900158;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] b = Nat256.m19974b();
        SecT239Field.m18968g(this.f12283d, b);
        return new SecT239FieldElement(b);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12283d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19978c(this.f12283d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] b = Nat256.m19974b();
        SecT239Field.m18963d(this.f12283d, b);
        return new SecT239FieldElement(b);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] b = Nat256.m19974b();
        SecT239Field.m18956a(this.f12283d, b);
        return new SecT239FieldElement(b);
    }

    public SecT239FieldElement() {
        this.f12283d = Nat256.m19974b();
    }

    protected SecT239FieldElement(long[] jArr) {
        this.f12283d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12283d;
        long[] jArr2 = ((SecT239FieldElement) dVar).f12283d;
        long[] jArr3 = ((SecT239FieldElement) dVar2).f12283d;
        long[] d = Nat256.m19986d();
        SecT239Field.m18969h(jArr, d);
        SecT239Field.m18966e(jArr2, jArr3, d);
        long[] b = Nat256.m19974b();
        SecT239Field.m18965e(d, b);
        return new SecT239FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
