package p245k.p251b.p263d.p264a;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;

/* renamed from: k.b.d.a.f */
public abstract class ECPoint {

    /* renamed from: f */
    protected static ECFieldElement[] f12249f = new ECFieldElement[0];

    /* renamed from: a */
    protected ECCurve f12250a;

    /* renamed from: b */
    protected ECFieldElement f12251b;

    /* renamed from: c */
    protected ECFieldElement f12252c;

    /* renamed from: d */
    protected ECFieldElement[] f12253d;

    /* renamed from: e */
    protected boolean f12254e;

    /* renamed from: k.b.d.a.f$a */
    /* compiled from: ECPoint */
    public static abstract class C5166a extends ECPoint {
        protected C5166a(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
            super(cVar, dVar, dVar2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: p */
        public boolean mo28315p() {
            ECFieldElement dVar;
            ECFieldElement dVar2;
            ECCurve d = mo28301d();
            ECFieldElement dVar3 = super.f12251b;
            ECFieldElement c = d.mo28250c();
            ECFieldElement d2 = d.mo28251d();
            int f = d.mo28254f();
            if (f == 6) {
                ECFieldElement dVar4 = super.f12253d[0];
                boolean e = dVar4.mo28273e();
                if (dVar3.mo28274f()) {
                    ECFieldElement i = super.f12252c.mo28277i();
                    if (!e) {
                        d2 = d2.mo28270c(dVar4.mo28277i());
                    }
                    return i.equals(d2);
                }
                ECFieldElement dVar5 = super.f12252c;
                ECFieldElement i2 = dVar3.mo28277i();
                if (e) {
                    dVar2 = dVar5.mo28277i().mo28263a(dVar5).mo28263a(c);
                    dVar = i2.mo28277i().mo28263a(d2);
                } else {
                    ECFieldElement i3 = dVar4.mo28277i();
                    ECFieldElement i4 = i3.mo28277i();
                    dVar2 = dVar5.mo28263a(dVar4).mo28268b(dVar5, c, i3);
                    dVar = i2.mo28264a(d2, i4);
                }
                return dVar2.mo28270c(i2).equals(dVar);
            }
            ECFieldElement dVar6 = super.f12252c;
            ECFieldElement c2 = dVar6.mo28263a(dVar3).mo28270c(dVar6);
            if (f != 0) {
                if (f == 1) {
                    ECFieldElement dVar7 = super.f12253d[0];
                    if (!dVar7.mo28273e()) {
                        ECFieldElement c3 = dVar7.mo28270c(dVar7.mo28277i());
                        c2 = c2.mo28270c(dVar7);
                        c = c.mo28270c(dVar7);
                        d2 = d2.mo28270c(c3);
                    }
                } else {
                    throw new IllegalStateException("unsupported coordinate system");
                }
            }
            return c2.equals(dVar3.mo28263a(c).mo28270c(dVar3.mo28277i()).mo28263a(d2));
        }

        protected C5166a(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr) {
            super(cVar, dVar, dVar2, dVarArr);
        }
    }

    /* renamed from: k.b.d.a.f$b */
    /* compiled from: ECPoint */
    public static abstract class C5167b extends ECPoint {
        protected C5167b(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
            super(cVar, dVar, dVar2);
        }

        /* access modifiers changed from: protected */
        /* renamed from: p */
        public boolean mo28315p() {
            ECFieldElement dVar = super.f12251b;
            ECFieldElement dVar2 = super.f12252c;
            ECFieldElement c = super.f12250a.mo28250c();
            ECFieldElement d = super.f12250a.mo28251d();
            ECFieldElement i = dVar2.mo28277i();
            int e = mo28302e();
            if (e != 0) {
                if (e == 1) {
                    ECFieldElement dVar3 = super.f12253d[0];
                    if (!dVar3.mo28273e()) {
                        ECFieldElement i2 = dVar3.mo28277i();
                        ECFieldElement c2 = dVar3.mo28270c(i2);
                        i = i.mo28270c(dVar3);
                        c = c.mo28270c(i2);
                        d = d.mo28270c(c2);
                    }
                } else if (e == 2 || e == 3 || e == 4) {
                    ECFieldElement dVar4 = super.f12253d[0];
                    if (!dVar4.mo28273e()) {
                        ECFieldElement i3 = dVar4.mo28277i();
                        ECFieldElement i4 = i3.mo28277i();
                        ECFieldElement c3 = i3.mo28270c(i4);
                        c = c.mo28270c(i4);
                        d = d.mo28270c(c3);
                    }
                } else {
                    throw new IllegalStateException("unsupported coordinate system");
                }
            }
            return i.equals(dVar.mo28277i().mo28263a(c).mo28270c(dVar).mo28263a(d));
        }

        protected C5167b(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr) {
            super(cVar, dVar, dVar2, dVarArr);
        }
    }

    /* renamed from: k.b.d.a.f$c */
    /* compiled from: ECPoint */
    public static class C5168c extends C5166a {
        public C5168c(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
            this(cVar, dVar, dVar2, false);
        }

        /* renamed from: a */
        public ECPoint mo28296a(ECPoint fVar) {
            ECFieldElement dVar;
            ECFieldElement dVar2;
            ECFieldElement dVar3;
            ECFieldElement dVar4;
            ECFieldElement dVar5;
            ECFieldElement dVar6;
            ECFieldElement dVar7;
            ECFieldElement dVar8;
            ECPoint fVar2 = fVar;
            if (mo28309j()) {
                return fVar2;
            }
            if (fVar.mo28309j()) {
                return this;
            }
            ECCurve d = mo28301d();
            int f = d.mo28254f();
            ECFieldElement dVar9 = this.f12251b;
            ECFieldElement dVar10 = fVar2.f12251b;
            if (f == 0) {
                ECFieldElement dVar11 = this.f12252c;
                ECFieldElement dVar12 = fVar2.f12252c;
                ECFieldElement a = dVar9.mo28263a(dVar10);
                ECFieldElement a2 = dVar11.mo28263a(dVar12);
                if (!a.mo28274f()) {
                    ECFieldElement b = a2.mo28267b(a);
                    ECFieldElement a3 = b.mo28277i().mo28263a(b).mo28263a(a).mo28263a(d.mo28250c());
                    return new C5168c(d, a3, b.mo28270c(dVar9.mo28263a(a3)).mo28263a(a3).mo28263a(dVar11), this.f12254e);
                } else if (a2.mo28274f()) {
                    return mo28316q();
                } else {
                    return d.mo28258i();
                }
            } else if (f == 1) {
                ECFieldElement dVar13 = this.f12252c;
                ECFieldElement dVar14 = this.f12253d[0];
                ECFieldElement dVar15 = fVar2.f12252c;
                ECFieldElement dVar16 = fVar2.f12253d[0];
                boolean e = dVar16.mo28273e();
                ECFieldElement c = dVar14.mo28270c(dVar15);
                if (e) {
                    dVar = dVar13;
                } else {
                    dVar = dVar13.mo28270c(dVar16);
                }
                ECFieldElement a4 = c.mo28263a(dVar);
                ECFieldElement c2 = dVar14.mo28270c(dVar10);
                if (e) {
                    dVar2 = dVar9;
                } else {
                    dVar2 = dVar9.mo28270c(dVar16);
                }
                ECFieldElement a5 = c2.mo28263a(dVar2);
                if (!a5.mo28274f()) {
                    ECFieldElement i = a5.mo28277i();
                    ECFieldElement c3 = i.mo28270c(a5);
                    if (!e) {
                        dVar14 = dVar14.mo28270c(dVar16);
                    }
                    ECFieldElement a6 = a4.mo28263a(a5);
                    ECFieldElement a7 = a6.mo28268b(a4, i, d.mo28250c()).mo28270c(dVar14).mo28263a(c3);
                    ECFieldElement c4 = a5.mo28270c(a7);
                    if (!e) {
                        i = i.mo28270c(dVar16);
                    }
                    return new C5168c(d, c4, a4.mo28268b(dVar9, a5, dVar13).mo28268b(i, a6, a7), new ECFieldElement[]{c3.mo28270c(dVar14)}, this.f12254e);
                } else if (a4.mo28274f()) {
                    return mo28316q();
                } else {
                    return d.mo28258i();
                }
            } else if (f != 6) {
                throw new IllegalStateException("unsupported coordinate system");
            } else if (!dVar9.mo28274f()) {
                ECFieldElement dVar17 = this.f12252c;
                ECFieldElement dVar18 = this.f12253d[0];
                ECFieldElement dVar19 = fVar2.f12252c;
                ECFieldElement dVar20 = fVar2.f12253d[0];
                boolean e2 = dVar18.mo28273e();
                if (!e2) {
                    dVar4 = dVar10.mo28270c(dVar18);
                    dVar3 = dVar19.mo28270c(dVar18);
                } else {
                    dVar4 = dVar10;
                    dVar3 = dVar19;
                }
                boolean e3 = dVar20.mo28273e();
                if (!e3) {
                    dVar9 = dVar9.mo28270c(dVar20);
                    dVar5 = dVar17.mo28270c(dVar20);
                } else {
                    dVar5 = dVar17;
                }
                ECFieldElement a8 = dVar5.mo28263a(dVar3);
                ECFieldElement a9 = dVar9.mo28263a(dVar4);
                if (!a9.mo28274f()) {
                    if (dVar10.mo28274f()) {
                        ECPoint n = mo28313n();
                        ECFieldElement h = n.mo28306h();
                        ECFieldElement i2 = n.mo28308i();
                        ECFieldElement b2 = i2.mo28263a(dVar19).mo28267b(h);
                        dVar6 = b2.mo28277i().mo28263a(b2).mo28263a(h).mo28263a(d.mo28250c());
                        if (dVar6.mo28274f()) {
                            return new C5168c(d, dVar6, d.mo28251d().mo28276h(), this.f12254e);
                        }
                        dVar7 = b2.mo28270c(h.mo28263a(dVar6)).mo28263a(dVar6).mo28263a(i2).mo28267b(dVar6).mo28263a(dVar6);
                        dVar8 = d.mo28235a(ECConstants.f12220b);
                    } else {
                        ECFieldElement i3 = a9.mo28277i();
                        ECFieldElement c5 = a8.mo28270c(dVar9);
                        ECFieldElement c6 = a8.mo28270c(dVar4);
                        ECFieldElement c7 = c5.mo28270c(c6);
                        if (c7.mo28274f()) {
                            return new C5168c(d, c7, d.mo28251d().mo28276h(), this.f12254e);
                        }
                        ECFieldElement c8 = a8.mo28270c(i3);
                        dVar8 = !e3 ? c8.mo28270c(dVar20) : c8;
                        dVar7 = c6.mo28263a(i3).mo28264a(dVar8, dVar17.mo28263a(dVar18));
                        if (!e2) {
                            dVar8 = dVar8.mo28270c(dVar18);
                        }
                        dVar6 = c7;
                    }
                    return new C5168c(d, dVar6, dVar7, new ECFieldElement[]{dVar8}, this.f12254e);
                } else if (a8.mo28274f()) {
                    return mo28316q();
                } else {
                    return d.mo28258i();
                }
            } else if (dVar10.mo28274f()) {
                return d.mo28258i();
            } else {
                return fVar2.mo28296a(this);
            }
        }

        /* renamed from: i */
        public ECFieldElement mo28308i() {
            int e = mo28302e();
            if (e != 5 && e != 6) {
                return this.f12252c;
            }
            ECFieldElement dVar = this.f12251b;
            ECFieldElement dVar2 = this.f12252c;
            if (mo28309j() || dVar.mo28274f()) {
                return dVar2;
            }
            ECFieldElement c = dVar2.mo28263a(dVar).mo28270c(dVar);
            if (6 != e) {
                return c;
            }
            ECFieldElement dVar3 = this.f12253d[0];
            return !dVar3.mo28273e() ? c.mo28267b(dVar3) : c;
        }

        /* renamed from: m */
        public ECPoint mo28312m() {
            if (mo28309j()) {
                return this;
            }
            ECFieldElement dVar = this.f12251b;
            if (dVar.mo28274f()) {
                return this;
            }
            int e = mo28302e();
            if (e == 0) {
                return new C5168c(this.f12250a, dVar, this.f12252c.mo28263a(dVar), this.f12254e);
            } else if (e == 1) {
                ECFieldElement dVar2 = this.f12252c;
                ECFieldElement dVar3 = this.f12253d[0];
                return new C5168c(this.f12250a, dVar, dVar2.mo28263a(dVar), new ECFieldElement[]{dVar3}, this.f12254e);
            } else if (e == 5) {
                return new C5168c(this.f12250a, dVar, this.f12252c.mo28262a(), this.f12254e);
            } else if (e == 6) {
                ECFieldElement dVar4 = this.f12252c;
                ECFieldElement dVar5 = this.f12253d[0];
                return new C5168c(this.f12250a, dVar, dVar4.mo28263a(dVar5), new ECFieldElement[]{dVar5}, this.f12254e);
            } else {
                throw new IllegalStateException("unsupported coordinate system");
            }
        }

        /* renamed from: q */
        public ECPoint mo28316q() {
            ECFieldElement dVar;
            ECFieldElement dVar2;
            ECFieldElement dVar3;
            ECFieldElement dVar4;
            ECFieldElement dVar5;
            ECFieldElement dVar6;
            ECFieldElement dVar7;
            if (mo28309j()) {
                return this;
            }
            ECCurve d = mo28301d();
            ECFieldElement dVar8 = this.f12251b;
            if (dVar8.mo28274f()) {
                return d.mo28258i();
            }
            int f = d.mo28254f();
            if (f == 0) {
                ECCurve cVar = d;
                ECFieldElement a = this.f12252c.mo28267b(dVar8).mo28263a(dVar8);
                ECFieldElement a2 = a.mo28277i().mo28263a(a).mo28263a(cVar.mo28250c());
                return new C5168c(cVar, a2, dVar8.mo28264a(a2, a.mo28262a()), this.f12254e);
            } else if (f == 1) {
                ECCurve cVar2 = d;
                ECFieldElement dVar9 = this.f12252c;
                ECFieldElement dVar10 = this.f12253d[0];
                boolean e = dVar10.mo28273e();
                if (e) {
                    dVar = dVar8;
                } else {
                    dVar = dVar8.mo28270c(dVar10);
                }
                if (!e) {
                    dVar9 = dVar9.mo28270c(dVar10);
                }
                ECFieldElement i = dVar8.mo28277i();
                ECFieldElement a3 = i.mo28263a(dVar9);
                ECFieldElement i2 = dVar.mo28277i();
                ECFieldElement a4 = a3.mo28263a(dVar);
                ECFieldElement b = a4.mo28268b(a3, i2, cVar2.mo28250c());
                return new C5168c(cVar2, dVar.mo28270c(b), i.mo28277i().mo28268b(dVar, b, a4), new ECFieldElement[]{dVar.mo28270c(i2)}, this.f12254e);
            } else if (f == 6) {
                ECFieldElement dVar11 = this.f12252c;
                ECFieldElement dVar12 = this.f12253d[0];
                boolean e2 = dVar12.mo28273e();
                if (e2) {
                    dVar2 = dVar11;
                } else {
                    dVar2 = dVar11.mo28270c(dVar12);
                }
                if (e2) {
                    dVar3 = dVar12;
                } else {
                    dVar3 = dVar12.mo28277i();
                }
                ECFieldElement c = d.mo28250c();
                if (e2) {
                    dVar4 = c;
                } else {
                    dVar4 = c.mo28270c(dVar3);
                }
                ECFieldElement a5 = dVar11.mo28277i().mo28263a(dVar2).mo28263a(dVar4);
                if (a5.mo28274f()) {
                    return new C5168c(d, a5, d.mo28251d().mo28276h(), this.f12254e);
                }
                ECFieldElement i3 = a5.mo28277i();
                if (e2) {
                    dVar5 = a5;
                } else {
                    dVar5 = a5.mo28270c(dVar3);
                }
                ECFieldElement d2 = d.mo28251d();
                ECCurve cVar3 = d;
                if (d2.mo28266b() < (d.mo28256h() >> 1)) {
                    ECFieldElement i4 = dVar11.mo28263a(dVar8).mo28277i();
                    if (d2.mo28273e()) {
                        dVar7 = dVar4.mo28263a(dVar3).mo28277i();
                    } else {
                        dVar7 = dVar4.mo28264a(d2, dVar3.mo28277i());
                    }
                    dVar6 = i4.mo28263a(a5).mo28263a(dVar3).mo28270c(i4).mo28263a(dVar7).mo28263a(i3);
                    if (c.mo28274f()) {
                        dVar6 = dVar6.mo28263a(dVar5);
                    } else if (!c.mo28273e()) {
                        dVar6 = dVar6.mo28263a(c.mo28262a().mo28270c(dVar5));
                    }
                } else {
                    if (!e2) {
                        dVar8 = dVar8.mo28270c(dVar12);
                    }
                    dVar6 = dVar8.mo28264a(a5, dVar2).mo28263a(i3).mo28263a(dVar5);
                }
                return new C5168c(cVar3, i3, dVar6, new ECFieldElement[]{dVar5}, this.f12254e);
            } else {
                throw new IllegalStateException("unsupported coordinate system");
            }
        }

        public C5168c(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
            super(cVar, dVar, dVar2);
            if ((dVar == null) == (dVar2 != null ? false : true)) {
                if (dVar != null) {
                    ECFieldElement.C5164a.m18720b(this.f12251b, this.f12252c);
                    if (cVar != null) {
                        ECFieldElement.C5164a.m18720b(this.f12251b, this.f12250a.mo28250c());
                    }
                }
                this.f12254e = z;
                return;
            }
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }

        C5168c(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
            super(cVar, dVar, dVar2, dVarArr);
            this.f12254e = z;
        }
    }

    /* renamed from: k.b.d.a.f$d */
    /* compiled from: ECPoint */
    public static class C5169d extends C5167b {
        public C5169d(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
            this(cVar, dVar, dVar2, false);
        }

        /* renamed from: a */
        public ECFieldElement mo28293a(int i) {
            if (i == 1 && 4 == mo28302e()) {
                return mo28324r();
            }
            return ECPoint.super.mo28293a(i);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public ECFieldElement mo28319b(ECFieldElement dVar) {
            return mo28321c(mo28323e(dVar));
        }

        /* access modifiers changed from: protected */
        /* renamed from: c */
        public ECFieldElement mo28321c(ECFieldElement dVar) {
            return mo28323e(mo28323e(dVar));
        }

        /* access modifiers changed from: protected */
        /* renamed from: d */
        public ECFieldElement mo28322d(ECFieldElement dVar) {
            return mo28323e(dVar).mo28263a(dVar);
        }

        /* access modifiers changed from: protected */
        /* renamed from: e */
        public ECFieldElement mo28323e(ECFieldElement dVar) {
            return dVar.mo28263a(dVar);
        }

        /* renamed from: m */
        public ECPoint mo28312m() {
            if (mo28309j()) {
                return this;
            }
            ECCurve d = mo28301d();
            if (d.mo28254f() != 0) {
                return new C5169d(d, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
            }
            return new C5169d(d, this.f12251b, this.f12252c.mo28275g(), this.f12254e);
        }

        /* renamed from: q */
        public ECPoint mo28316q() {
            ECFieldElement dVar;
            ECFieldElement dVar2;
            ECFieldElement dVar3;
            ECFieldElement dVar4;
            if (mo28309j()) {
                return this;
            }
            ECCurve d = mo28301d();
            ECFieldElement dVar5 = this.f12252c;
            if (dVar5.mo28274f()) {
                return d.mo28258i();
            }
            int f = d.mo28254f();
            ECFieldElement dVar6 = this.f12251b;
            if (f == 0) {
                ECFieldElement b = mo28322d(dVar6.mo28277i()).mo28263a(mo28301d().mo28250c()).mo28267b(mo28323e(dVar5));
                ECFieldElement d2 = b.mo28277i().mo28272d(mo28323e(dVar6));
                return new C5169d(d, d2, b.mo28270c(dVar6.mo28272d(d2)).mo28272d(dVar5), this.f12254e);
            } else if (f == 1) {
                ECFieldElement dVar7 = this.f12253d[0];
                boolean e = dVar7.mo28273e();
                ECFieldElement c = d.mo28250c();
                if (!c.mo28274f() && !e) {
                    c = c.mo28270c(dVar7.mo28277i());
                }
                ECFieldElement a = c.mo28263a(mo28322d(dVar6.mo28277i()));
                if (e) {
                    dVar = dVar5;
                } else {
                    dVar = dVar5.mo28270c(dVar7);
                }
                ECFieldElement i = e ? dVar5.mo28277i() : dVar.mo28270c(dVar5);
                ECFieldElement c2 = mo28321c(dVar6.mo28270c(i));
                ECFieldElement d3 = a.mo28277i().mo28272d(mo28323e(c2));
                ECFieldElement e2 = mo28323e(dVar);
                ECFieldElement c3 = d3.mo28270c(e2);
                ECFieldElement e3 = mo28323e(i);
                return new C5169d(d, c3, c2.mo28272d(d3).mo28270c(a).mo28272d(mo28323e(e3.mo28277i())), new ECFieldElement[]{mo28323e(e ? mo28323e(e3) : e2.mo28277i()).mo28270c(dVar)}, this.f12254e);
            } else if (f == 2) {
                ECFieldElement dVar8 = this.f12253d[0];
                boolean e4 = dVar8.mo28273e();
                ECFieldElement i2 = dVar5.mo28277i();
                ECFieldElement i3 = i2.mo28277i();
                ECFieldElement c4 = d.mo28250c();
                ECFieldElement g = c4.mo28275g();
                if (g.mo28279k().equals(BigInteger.valueOf(3))) {
                    if (e4) {
                        dVar4 = dVar8;
                    } else {
                        dVar4 = dVar8.mo28277i();
                    }
                    dVar2 = mo28322d(dVar6.mo28263a(dVar4).mo28270c(dVar6.mo28272d(dVar4)));
                    dVar3 = mo28321c(i2.mo28270c(dVar6));
                } else {
                    ECFieldElement d4 = mo28322d(dVar6.mo28277i());
                    if (e4) {
                        dVar2 = d4.mo28263a(c4);
                    } else if (!c4.mo28274f()) {
                        ECFieldElement i4 = dVar8.mo28277i().mo28277i();
                        dVar2 = g.mo28266b() < c4.mo28266b() ? d4.mo28272d(i4.mo28270c(g)) : d4.mo28263a(i4.mo28270c(c4));
                    } else {
                        dVar2 = d4;
                    }
                    dVar3 = mo28321c(dVar6.mo28270c(i2));
                }
                ECFieldElement d5 = dVar2.mo28277i().mo28272d(mo28323e(dVar3));
                ECFieldElement d6 = dVar3.mo28272d(d5).mo28270c(dVar2).mo28272d(mo28319b(i3));
                ECFieldElement e5 = mo28323e(dVar5);
                if (!e4) {
                    e5 = e5.mo28270c(dVar8);
                }
                return new C5169d(d, d5, d6, new ECFieldElement[]{e5}, this.f12254e);
            } else if (f == 4) {
                return mo28318a(true);
            } else {
                throw new IllegalStateException("unsupported coordinate system");
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: r */
        public ECFieldElement mo28324r() {
            ECFieldElement[] dVarArr = this.f12253d;
            ECFieldElement dVar = dVarArr[1];
            if (dVar != null) {
                return dVar;
            }
            ECFieldElement b = mo28320b(dVarArr[0], null);
            dVarArr[1] = b;
            return b;
        }

        public C5169d(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
            super(cVar, dVar, dVar2);
            if ((dVar == null) == (dVar2 != null ? false : true)) {
                this.f12254e = z;
                return;
            }
            throw new IllegalArgumentException("Exactly one of the field elements is null");
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public ECFieldElement mo28320b(ECFieldElement dVar, ECFieldElement dVar2) {
            ECFieldElement c = mo28301d().mo28250c();
            if (c.mo28274f() || dVar.mo28273e()) {
                return c;
            }
            if (dVar2 == null) {
                dVar2 = dVar.mo28277i();
            }
            ECFieldElement i = dVar2.mo28277i();
            ECFieldElement g = c.mo28275g();
            if (g.mo28266b() < c.mo28266b()) {
                return i.mo28270c(g).mo28275g();
            }
            return i.mo28270c(c);
        }

        /* JADX WARN: Type inference failed for: r17v0, types: [k.b.d.a.f] */
        /* JADX WARNING: Code restructure failed: missing block: B:53:0x0123, code lost:
            if (r1 == r6) goto L_0x0125;
         */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public p245k.p251b.p263d.p264a.ECPoint mo28296a(p245k.p251b.p263d.p264a.ECPoint r17) {
            /*
                r16 = this;
                r0 = r16
                r1 = r17
                boolean r2 = r16.mo28309j()
                if (r2 == 0) goto L_0x000b
                return r1
            L_0x000b:
                boolean r2 = r17.mo28309j()
                if (r2 == 0) goto L_0x0012
                return r0
            L_0x0012:
                if (r0 != r1) goto L_0x0019
                k.b.d.a.f r1 = r16.mo28316q()
                return r1
            L_0x0019:
                k.b.d.a.c r3 = r16.mo28301d()
                int r2 = r3.mo28254f()
                k.b.d.a.d r4 = r0.f12251b
                k.b.d.a.d r5 = r0.f12252c
                k.b.d.a.d r6 = r1.f12251b
                k.b.d.a.d r7 = r1.f12252c
                if (r2 == 0) goto L_0x01dd
                r8 = 1
                r9 = 0
                if (r2 == r8) goto L_0x0145
                r10 = 4
                r11 = 2
                if (r2 == r11) goto L_0x003e
                if (r2 != r10) goto L_0x0036
                goto L_0x003e
            L_0x0036:
                java.lang.IllegalStateException r1 = new java.lang.IllegalStateException
                java.lang.String r2 = "unsupported coordinate system"
                r1.<init>(r2)
                throw r1
            L_0x003e:
                k.b.d.a.d[] r12 = r0.f12253d
                r12 = r12[r9]
                k.b.d.a.d[] r1 = r1.f12253d
                r1 = r1[r9]
                boolean r13 = r12.mo28273e()
                if (r13 != 0) goto L_0x00a5
                boolean r15 = r12.equals(r1)
                if (r15 == 0) goto L_0x00a5
                k.b.d.a.d r1 = r4.mo28272d(r6)
                k.b.d.a.d r7 = r5.mo28272d(r7)
                boolean r13 = r1.mo28274f()
                if (r13 == 0) goto L_0x0070
                boolean r1 = r7.mo28274f()
                if (r1 == 0) goto L_0x006b
                k.b.d.a.f r1 = r16.mo28316q()
                return r1
            L_0x006b:
                k.b.d.a.f r1 = r3.mo28258i()
                return r1
            L_0x0070:
                k.b.d.a.d r13 = r1.mo28277i()
                k.b.d.a.d r4 = r4.mo28270c(r13)
                k.b.d.a.d r6 = r6.mo28270c(r13)
                k.b.d.a.d r13 = r4.mo28272d(r6)
                k.b.d.a.d r5 = r13.mo28270c(r5)
                k.b.d.a.d r13 = r7.mo28277i()
                k.b.d.a.d r13 = r13.mo28272d(r4)
                k.b.d.a.d r6 = r13.mo28272d(r6)
                k.b.d.a.d r4 = r4.mo28272d(r6)
                k.b.d.a.d r4 = r4.mo28270c(r7)
                k.b.d.a.d r4 = r4.mo28272d(r5)
                k.b.d.a.d r1 = r1.mo28270c(r12)
                r5 = r4
                r4 = r6
            L_0x00a2:
                r14 = 0
                goto L_0x0125
            L_0x00a5:
                if (r13 == 0) goto L_0x00a8
                goto L_0x00b8
            L_0x00a8:
                k.b.d.a.d r15 = r12.mo28277i()
                k.b.d.a.d r6 = r15.mo28270c(r6)
                k.b.d.a.d r15 = r15.mo28270c(r12)
                k.b.d.a.d r7 = r15.mo28270c(r7)
            L_0x00b8:
                boolean r15 = r1.mo28273e()
                if (r15 == 0) goto L_0x00bf
                goto L_0x00cf
            L_0x00bf:
                k.b.d.a.d r14 = r1.mo28277i()
                k.b.d.a.d r4 = r14.mo28270c(r4)
                k.b.d.a.d r14 = r14.mo28270c(r1)
                k.b.d.a.d r5 = r14.mo28270c(r5)
            L_0x00cf:
                k.b.d.a.d r6 = r4.mo28272d(r6)
                k.b.d.a.d r7 = r5.mo28272d(r7)
                boolean r14 = r6.mo28274f()
                if (r14 == 0) goto L_0x00ed
                boolean r1 = r7.mo28274f()
                if (r1 == 0) goto L_0x00e8
                k.b.d.a.f r1 = r16.mo28316q()
                return r1
            L_0x00e8:
                k.b.d.a.f r1 = r3.mo28258i()
                return r1
            L_0x00ed:
                k.b.d.a.d r14 = r6.mo28277i()
                k.b.d.a.d r8 = r14.mo28270c(r6)
                k.b.d.a.d r4 = r14.mo28270c(r4)
                k.b.d.a.d r9 = r7.mo28277i()
                k.b.d.a.d r9 = r9.mo28263a(r8)
                k.b.d.a.d r11 = r0.mo28323e(r4)
                k.b.d.a.d r9 = r9.mo28272d(r11)
                k.b.d.a.d r4 = r4.mo28272d(r9)
                k.b.d.a.d r4 = r4.mo28265a(r7, r8, r5)
                if (r13 != 0) goto L_0x0118
                k.b.d.a.d r5 = r6.mo28270c(r12)
                goto L_0x0119
            L_0x0118:
                r5 = r6
            L_0x0119:
                if (r15 != 0) goto L_0x0120
                k.b.d.a.d r1 = r5.mo28270c(r1)
                goto L_0x0121
            L_0x0120:
                r1 = r5
            L_0x0121:
                r5 = r4
                r4 = r9
                if (r1 != r6) goto L_0x00a2
            L_0x0125:
                if (r2 != r10) goto L_0x0135
                k.b.d.a.d r2 = r0.mo28320b(r1, r14)
                r6 = 2
                k.b.d.a.d[] r6 = new p245k.p251b.p263d.p264a.ECFieldElement[r6]
                r7 = 0
                r6[r7] = r1
                r8 = 1
                r6[r8] = r2
                goto L_0x013c
            L_0x0135:
                r7 = 0
                r8 = 1
                k.b.d.a.d[] r2 = new p245k.p251b.p263d.p264a.ECFieldElement[r8]
                r2[r7] = r1
                r6 = r2
            L_0x013c:
                k.b.d.a.f$d r1 = new k.b.d.a.f$d
                boolean r7 = r0.f12254e
                r2 = r1
                r2.<init>(r3, r4, r5, r6, r7)
                return r1
            L_0x0145:
                k.b.d.a.d[] r2 = r0.f12253d
                r8 = 0
                r2 = r2[r8]
                k.b.d.a.d[] r1 = r1.f12253d
                r1 = r1[r8]
                boolean r8 = r2.mo28273e()
                boolean r9 = r1.mo28273e()
                if (r8 == 0) goto L_0x0159
                goto L_0x015d
            L_0x0159:
                k.b.d.a.d r7 = r7.mo28270c(r2)
            L_0x015d:
                if (r9 == 0) goto L_0x0160
                goto L_0x0164
            L_0x0160:
                k.b.d.a.d r5 = r5.mo28270c(r1)
            L_0x0164:
                k.b.d.a.d r7 = r7.mo28272d(r5)
                if (r8 == 0) goto L_0x016b
                goto L_0x016f
            L_0x016b:
                k.b.d.a.d r6 = r6.mo28270c(r2)
            L_0x016f:
                if (r9 == 0) goto L_0x0172
                goto L_0x0176
            L_0x0172:
                k.b.d.a.d r4 = r4.mo28270c(r1)
            L_0x0176:
                k.b.d.a.d r6 = r6.mo28272d(r4)
                boolean r10 = r6.mo28274f()
                if (r10 == 0) goto L_0x0190
                boolean r1 = r7.mo28274f()
                if (r1 == 0) goto L_0x018b
                k.b.d.a.f r1 = r16.mo28316q()
                return r1
            L_0x018b:
                k.b.d.a.f r1 = r3.mo28258i()
                return r1
            L_0x0190:
                if (r8 == 0) goto L_0x0194
                r2 = r1
                goto L_0x019b
            L_0x0194:
                if (r9 == 0) goto L_0x0197
                goto L_0x019b
            L_0x0197:
                k.b.d.a.d r2 = r2.mo28270c(r1)
            L_0x019b:
                k.b.d.a.d r1 = r6.mo28277i()
                k.b.d.a.d r8 = r1.mo28270c(r6)
                k.b.d.a.d r1 = r1.mo28270c(r4)
                k.b.d.a.d r4 = r7.mo28277i()
                k.b.d.a.d r4 = r4.mo28270c(r2)
                k.b.d.a.d r4 = r4.mo28272d(r8)
                k.b.d.a.d r9 = r0.mo28323e(r1)
                k.b.d.a.d r4 = r4.mo28272d(r9)
                k.b.d.a.d r6 = r6.mo28270c(r4)
                k.b.d.a.d r1 = r1.mo28272d(r4)
                k.b.d.a.d r5 = r1.mo28265a(r7, r5, r8)
                k.b.d.a.d r1 = r8.mo28270c(r2)
                k.b.d.a.f$d r8 = new k.b.d.a.f$d
                r2 = 1
                k.b.d.a.d[] r7 = new p245k.p251b.p263d.p264a.ECFieldElement[r2]
                r2 = 0
                r7[r2] = r1
                boolean r1 = r0.f12254e
                r2 = r8
                r4 = r6
                r6 = r7
                r7 = r1
                r2.<init>(r3, r4, r5, r6, r7)
                return r8
            L_0x01dd:
                k.b.d.a.d r1 = r6.mo28272d(r4)
                k.b.d.a.d r2 = r7.mo28272d(r5)
                boolean r7 = r1.mo28274f()
                if (r7 == 0) goto L_0x01fb
                boolean r1 = r2.mo28274f()
                if (r1 == 0) goto L_0x01f6
                k.b.d.a.f r1 = r16.mo28316q()
                return r1
            L_0x01f6:
                k.b.d.a.f r1 = r3.mo28258i()
                return r1
            L_0x01fb:
                k.b.d.a.d r1 = r2.mo28267b(r1)
                k.b.d.a.d r2 = r1.mo28277i()
                k.b.d.a.d r2 = r2.mo28272d(r4)
                k.b.d.a.d r2 = r2.mo28272d(r6)
                k.b.d.a.d r4 = r4.mo28272d(r2)
                k.b.d.a.d r1 = r1.mo28270c(r4)
                k.b.d.a.d r1 = r1.mo28272d(r5)
                k.b.d.a.f$d r4 = new k.b.d.a.f$d
                boolean r5 = r0.f12254e
                r4.<init>(r3, r2, r1, r5)
                return r4
            */
            throw new UnsupportedOperationException("Method not decompiled: p245k.p251b.p263d.p264a.ECPoint.C5169d.mo28296a(k.b.d.a.f):k.b.d.a.f");
        }

        C5169d(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
            super(cVar, dVar, dVar2, dVarArr);
            this.f12254e = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C5169d mo28318a(boolean z) {
            ECFieldElement dVar = this.f12251b;
            ECFieldElement dVar2 = this.f12252c;
            ECFieldElement dVar3 = this.f12253d[0];
            ECFieldElement r = mo28324r();
            ECFieldElement a = mo28322d(dVar.mo28277i()).mo28263a(r);
            ECFieldElement e = mo28323e(dVar2);
            ECFieldElement c = e.mo28270c(dVar2);
            ECFieldElement e2 = mo28323e(dVar.mo28270c(c));
            ECFieldElement d = a.mo28277i().mo28272d(mo28323e(e2));
            ECFieldElement e3 = mo28323e(c.mo28277i());
            ECFieldElement d2 = a.mo28270c(e2.mo28272d(d)).mo28272d(e3);
            ECFieldElement e4 = z ? mo28323e(e3.mo28270c(r)) : null;
            if (!dVar3.mo28273e()) {
                e = e.mo28270c(dVar3);
            }
            return new C5169d(mo28301d(), d, d2, new ECFieldElement[]{e, e4}, this.f12254e);
        }
    }

    protected ECPoint(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, m18764a(cVar));
    }

    /* renamed from: a */
    protected static ECFieldElement[] m18764a(ECCurve cVar) {
        int f = cVar == null ? 0 : cVar.mo28254f();
        if (f == 0 || f == 5) {
            return f12249f;
        }
        ECFieldElement a = cVar.mo28235a(ECConstants.f12220b);
        if (!(f == 1 || f == 2)) {
            if (f == 3) {
                return new ECFieldElement[]{a, a, a};
            } else if (f == 4) {
                return new ECFieldElement[]{a, cVar.mo28250c()};
            } else if (f != 6) {
                throw new IllegalArgumentException("unknown coordinate system");
            }
        }
        return new ECFieldElement[]{a};
    }

    /* renamed from: a */
    public abstract ECPoint mo28296a(ECPoint fVar);

    /* renamed from: b */
    public ECFieldElement mo28298b() {
        mo28297a();
        return mo28306h();
    }

    /* renamed from: c */
    public ECFieldElement mo28300c() {
        mo28297a();
        return mo28308i();
    }

    /* renamed from: d */
    public ECCurve mo28301d() {
        return this.f12250a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public int mo28302e() {
        ECCurve cVar = this.f12250a;
        if (cVar == null) {
            return 0;
        }
        return cVar.mo28254f();
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ECPoint)) {
            return false;
        }
        return mo28299b((ECPoint) obj);
    }

    /* renamed from: f */
    public final ECFieldElement mo28304f() {
        return this.f12251b;
    }

    /* renamed from: g */
    public final ECFieldElement mo28305g() {
        return this.f12252c;
    }

    /* renamed from: h */
    public ECFieldElement mo28306h() {
        return this.f12251b;
    }

    public int hashCode() {
        int i;
        ECCurve d = mo28301d();
        if (d == null) {
            i = 0;
        } else {
            i = ~d.hashCode();
        }
        if (mo28309j()) {
            return i;
        }
        ECPoint n = mo28313n();
        return (i ^ (n.mo28306h().hashCode() * 17)) ^ (n.mo28308i().hashCode() * 257);
    }

    /* renamed from: i */
    public ECFieldElement mo28308i() {
        return this.f12252c;
    }

    /* renamed from: j */
    public boolean mo28309j() {
        if (!(this.f12251b == null || this.f12252c == null)) {
            ECFieldElement[] dVarArr = this.f12253d;
            if (dVarArr.length <= 0 || !dVarArr[0].mo28274f()) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: k */
    public boolean mo28310k() {
        int e = mo28302e();
        if (e == 0 || e == 5 || mo28309j() || this.f12253d[0].mo28273e()) {
            return true;
        }
        return false;
    }

    /* renamed from: l */
    public boolean mo28311l() {
        if (!mo28309j() && mo28301d() != null && (!mo28315p() || !mo28314o())) {
            return false;
        }
        return true;
    }

    /* renamed from: m */
    public abstract ECPoint mo28312m();

    /* renamed from: n */
    public ECPoint mo28313n() {
        int e;
        if (mo28309j() || (e = mo28302e()) == 0 || e == 5) {
            return this;
        }
        ECFieldElement a = mo28293a(0);
        if (a.mo28273e()) {
            return this;
        }
        return mo28294a(a.mo28271d());
    }

    /* access modifiers changed from: protected */
    /* renamed from: o */
    public boolean mo28314o() {
        BigInteger e = this.f12250a.mo28252e();
        return e == null || e.equals(ECConstants.f12220b) || !ECAlgorithms.m18652a(this, e).mo28309j();
    }

    /* access modifiers changed from: protected */
    /* renamed from: p */
    public abstract boolean mo28315p();

    /* renamed from: q */
    public abstract ECPoint mo28316q();

    public String toString() {
        if (mo28309j()) {
            return "INF";
        }
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append('(');
        stringBuffer.append(mo28304f());
        stringBuffer.append(',');
        stringBuffer.append(mo28305g());
        for (int i = 0; i < this.f12253d.length; i++) {
            stringBuffer.append(',');
            stringBuffer.append(this.f12253d[i]);
        }
        stringBuffer.append(')');
        return stringBuffer.toString();
    }

    protected ECPoint(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr) {
        this.f12250a = cVar;
        this.f12251b = dVar;
        this.f12252c = dVar2;
        this.f12253d = dVarArr;
    }

    /* renamed from: b */
    public boolean mo28299b(ECPoint fVar) {
        ECPoint fVar2;
        ECPoint fVar3;
        if (fVar == null) {
            return false;
        }
        ECCurve d = mo28301d();
        ECCurve d2 = fVar.mo28301d();
        boolean z = d == null;
        boolean z2 = d2 == null;
        boolean j = mo28309j();
        boolean j2 = fVar.mo28309j();
        if (!j && !j2) {
            if (!z || !z2) {
                if (z) {
                    fVar = fVar.mo28313n();
                } else {
                    if (z2) {
                        fVar3 = fVar;
                        fVar2 = mo28313n();
                    } else if (!d.mo28246a(d2)) {
                        return false;
                    } else {
                        ECPoint[] fVarArr = {this, d.mo28240a(fVar)};
                        d.mo28242a(fVarArr);
                        fVar2 = fVarArr[0];
                        fVar3 = fVarArr[1];
                    }
                    if (!fVar2.mo28306h().equals(fVar3.mo28306h()) && fVar2.mo28308i().equals(fVar3.mo28308i())) {
                        return true;
                    }
                }
            }
            fVar3 = fVar;
            fVar2 = this;
            return !fVar2.mo28306h().equals(fVar3.mo28306h()) ? false : false;
        } else if (!j || !j2) {
            return false;
        } else {
            if (z || z2 || d.mo28246a(d2)) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: a */
    public ECFieldElement mo28293a(int i) {
        if (i >= 0) {
            ECFieldElement[] dVarArr = this.f12253d;
            if (i < dVarArr.length) {
                return dVarArr[i];
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo28297a() {
        if (!mo28310k()) {
            throw new IllegalStateException("point not in normal form");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public ECPoint mo28294a(ECFieldElement dVar) {
        int e = mo28302e();
        if (e != 1) {
            if (e == 2 || e == 3 || e == 4) {
                ECFieldElement i = dVar.mo28277i();
                return mo28295a(i, i.mo28270c(dVar));
            } else if (e != 6) {
                throw new IllegalStateException("not a projective coordinate system");
            }
        }
        return mo28295a(dVar, dVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28295a(ECFieldElement dVar, ECFieldElement dVar2) {
        return mo28301d().mo28239a(mo28304f().mo28270c(dVar), mo28305g().mo28270c(dVar2), this.f12254e);
    }
}
