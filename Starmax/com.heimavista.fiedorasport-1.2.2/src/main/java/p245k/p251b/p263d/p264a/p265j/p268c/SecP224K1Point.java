package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat224;

/* renamed from: k.b.d.a.j.c.z */
public class SecP224K1Point extends ECPoint.C5167b {
    public SecP224K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP224K1FieldElement yVar = (SecP224K1FieldElement) this.f12251b;
        SecP224K1FieldElement yVar2 = (SecP224K1FieldElement) this.f12252c;
        SecP224K1FieldElement yVar3 = (SecP224K1FieldElement) fVar.mo28306h();
        SecP224K1FieldElement yVar4 = (SecP224K1FieldElement) fVar.mo28308i();
        SecP224K1FieldElement yVar5 = (SecP224K1FieldElement) this.f12253d[0];
        SecP224K1FieldElement yVar6 = (SecP224K1FieldElement) fVar.mo28293a(0);
        int[] b = Nat224.m19944b();
        int[] a = Nat224.m19939a();
        int[] a2 = Nat224.m19939a();
        int[] a3 = Nat224.m19939a();
        boolean e = yVar5.mo28273e();
        if (e) {
            iArr2 = yVar3.f12378d;
            iArr = yVar4.f12378d;
        } else {
            SecP224K1Field.m19779d(yVar5.f12378d, a2);
            SecP224K1Field.m19776b(a2, yVar3.f12378d, a);
            SecP224K1Field.m19776b(a2, yVar5.f12378d, a2);
            SecP224K1Field.m19776b(a2, yVar4.f12378d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = yVar6.mo28273e();
        if (e2) {
            iArr4 = yVar.f12378d;
            iArr3 = yVar2.f12378d;
        } else {
            SecP224K1Field.m19779d(yVar6.f12378d, a3);
            SecP224K1Field.m19776b(a3, yVar.f12378d, b);
            SecP224K1Field.m19776b(a3, yVar6.f12378d, a3);
            SecP224K1Field.m19776b(a3, yVar2.f12378d, a3);
            iArr4 = b;
            iArr3 = a3;
        }
        int[] a4 = Nat224.m19939a();
        SecP224K1Field.m19780d(iArr4, iArr2, a4);
        SecP224K1Field.m19780d(iArr3, iArr, a);
        if (!Nat224.m19942b(a4)) {
            SecP224K1Field.m19779d(a4, a2);
            int[] a5 = Nat224.m19939a();
            SecP224K1Field.m19776b(a2, a4, a5);
            SecP224K1Field.m19776b(a2, iArr4, a2);
            SecP224K1Field.m19775b(a5, a5);
            Nat224.m19946c(iArr3, a5, b);
            SecP224K1Field.m19770a(Nat224.m19941b(a2, a2, a5), a5);
            SecP224K1FieldElement yVar7 = new SecP224K1FieldElement(a3);
            SecP224K1Field.m19779d(a, yVar7.f12378d);
            int[] iArr5 = yVar7.f12378d;
            SecP224K1Field.m19780d(iArr5, a5, iArr5);
            SecP224K1FieldElement yVar8 = new SecP224K1FieldElement(a5);
            SecP224K1Field.m19780d(a2, yVar7.f12378d, yVar8.f12378d);
            SecP224K1Field.m19778c(yVar8.f12378d, a, b);
            SecP224K1Field.m19777c(b, yVar8.f12378d);
            SecP224K1FieldElement yVar9 = new SecP224K1FieldElement(a4);
            if (!e) {
                int[] iArr6 = yVar9.f12378d;
                SecP224K1Field.m19776b(iArr6, yVar5.f12378d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = yVar9.f12378d;
                SecP224K1Field.m19776b(iArr7, yVar6.f12378d, iArr7);
            }
            return new SecP224K1Point(d, yVar7, yVar8, new ECFieldElement[]{yVar9}, this.f12254e);
        } else if (Nat224.m19942b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP224K1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP224K1FieldElement yVar = (SecP224K1FieldElement) this.f12252c;
        if (yVar.mo28274f()) {
            return d.mo28258i();
        }
        SecP224K1FieldElement yVar2 = (SecP224K1FieldElement) this.f12251b;
        SecP224K1FieldElement yVar3 = (SecP224K1FieldElement) this.f12253d[0];
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19779d(yVar.f12378d, a);
        int[] a2 = Nat224.m19939a();
        SecP224K1Field.m19779d(a, a2);
        int[] a3 = Nat224.m19939a();
        SecP224K1Field.m19779d(yVar2.f12378d, a3);
        SecP224K1Field.m19770a(Nat224.m19941b(a3, a3, a3), a3);
        SecP224K1Field.m19776b(a, yVar2.f12378d, a);
        SecP224K1Field.m19770a(Nat.m20045c(7, a, 2, 0), a);
        int[] a4 = Nat224.m19939a();
        SecP224K1Field.m19770a(Nat.m20021a(7, a2, 3, 0, a4), a4);
        SecP224K1FieldElement yVar4 = new SecP224K1FieldElement(a2);
        SecP224K1Field.m19779d(a3, yVar4.f12378d);
        int[] iArr = yVar4.f12378d;
        SecP224K1Field.m19780d(iArr, a, iArr);
        int[] iArr2 = yVar4.f12378d;
        SecP224K1Field.m19780d(iArr2, a, iArr2);
        SecP224K1FieldElement yVar5 = new SecP224K1FieldElement(a);
        SecP224K1Field.m19780d(a, yVar4.f12378d, yVar5.f12378d);
        int[] iArr3 = yVar5.f12378d;
        SecP224K1Field.m19776b(iArr3, a3, iArr3);
        int[] iArr4 = yVar5.f12378d;
        SecP224K1Field.m19780d(iArr4, a4, iArr4);
        SecP224K1FieldElement yVar6 = new SecP224K1FieldElement(a3);
        SecP224K1Field.m19781e(yVar.f12378d, yVar6.f12378d);
        if (!yVar3.mo28273e()) {
            int[] iArr5 = yVar6.f12378d;
            SecP224K1Field.m19776b(iArr5, yVar3.f12378d, iArr5);
        }
        return new SecP224K1Point(d, yVar4, yVar5, new ECFieldElement[]{yVar6}, this.f12254e);
    }

    public SecP224K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP224K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
