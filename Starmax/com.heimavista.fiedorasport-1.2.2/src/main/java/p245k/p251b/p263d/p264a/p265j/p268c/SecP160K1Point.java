package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat160;

/* renamed from: k.b.d.a.j.c.f */
public class SecP160K1Point extends ECPoint.C5167b {
    public SecP160K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP160R2FieldElement mVar = (SecP160R2FieldElement) this.f12251b;
        SecP160R2FieldElement mVar2 = (SecP160R2FieldElement) this.f12252c;
        SecP160R2FieldElement mVar3 = (SecP160R2FieldElement) fVar.mo28306h();
        SecP160R2FieldElement mVar4 = (SecP160R2FieldElement) fVar.mo28308i();
        SecP160R2FieldElement mVar5 = (SecP160R2FieldElement) this.f12253d[0];
        SecP160R2FieldElement mVar6 = (SecP160R2FieldElement) fVar.mo28293a(0);
        int[] b = Nat160.m19890b();
        int[] a = Nat160.m19884a();
        int[] a2 = Nat160.m19884a();
        int[] a3 = Nat160.m19884a();
        boolean e = mVar5.mo28273e();
        if (e) {
            iArr2 = mVar3.f12328d;
            iArr = mVar4.f12328d;
        } else {
            SecP160R2Field.m19352d(mVar5.f12328d, a2);
            SecP160R2Field.m19349b(a2, mVar3.f12328d, a);
            SecP160R2Field.m19349b(a2, mVar5.f12328d, a2);
            SecP160R2Field.m19349b(a2, mVar4.f12328d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = mVar6.mo28273e();
        if (e2) {
            iArr4 = mVar.f12328d;
            iArr3 = mVar2.f12328d;
        } else {
            SecP160R2Field.m19352d(mVar6.f12328d, a3);
            SecP160R2Field.m19349b(a3, mVar.f12328d, b);
            SecP160R2Field.m19349b(a3, mVar6.f12328d, a3);
            SecP160R2Field.m19349b(a3, mVar2.f12328d, a3);
            iArr4 = b;
            iArr3 = a3;
        }
        int[] a4 = Nat160.m19884a();
        SecP160R2Field.m19353d(iArr4, iArr2, a4);
        SecP160R2Field.m19353d(iArr3, iArr, a);
        if (!Nat160.m19888b(a4)) {
            SecP160R2Field.m19352d(a4, a2);
            int[] a5 = Nat160.m19884a();
            SecP160R2Field.m19349b(a2, a4, a5);
            SecP160R2Field.m19349b(a2, iArr4, a2);
            SecP160R2Field.m19348b(a5, a5);
            Nat160.m19893c(iArr3, a5, b);
            SecP160R2Field.m19343a(Nat160.m19887b(a2, a2, a5), a5);
            SecP160R2FieldElement mVar7 = new SecP160R2FieldElement(a3);
            SecP160R2Field.m19352d(a, mVar7.f12328d);
            int[] iArr5 = mVar7.f12328d;
            SecP160R2Field.m19353d(iArr5, a5, iArr5);
            SecP160R2FieldElement mVar8 = new SecP160R2FieldElement(a5);
            SecP160R2Field.m19353d(a2, mVar7.f12328d, mVar8.f12328d);
            SecP160R2Field.m19351c(mVar8.f12328d, a, b);
            SecP160R2Field.m19350c(b, mVar8.f12328d);
            SecP160R2FieldElement mVar9 = new SecP160R2FieldElement(a4);
            if (!e) {
                int[] iArr6 = mVar9.f12328d;
                SecP160R2Field.m19349b(iArr6, mVar5.f12328d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = mVar9.f12328d;
                SecP160R2Field.m19349b(iArr7, mVar6.f12328d, iArr7);
            }
            return new SecP160K1Point(d, mVar7, mVar8, new ECFieldElement[]{mVar9}, this.f12254e);
        } else if (Nat160.m19888b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP160K1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP160R2FieldElement mVar = (SecP160R2FieldElement) this.f12252c;
        if (mVar.mo28274f()) {
            return d.mo28258i();
        }
        SecP160R2FieldElement mVar2 = (SecP160R2FieldElement) this.f12251b;
        SecP160R2FieldElement mVar3 = (SecP160R2FieldElement) this.f12253d[0];
        int[] a = Nat160.m19884a();
        SecP160R2Field.m19352d(mVar.f12328d, a);
        int[] a2 = Nat160.m19884a();
        SecP160R2Field.m19352d(a, a2);
        int[] a3 = Nat160.m19884a();
        SecP160R2Field.m19352d(mVar2.f12328d, a3);
        SecP160R2Field.m19343a(Nat160.m19887b(a3, a3, a3), a3);
        SecP160R2Field.m19349b(a, mVar2.f12328d, a);
        SecP160R2Field.m19343a(Nat.m20045c(5, a, 2, 0), a);
        int[] a4 = Nat160.m19884a();
        SecP160R2Field.m19343a(Nat.m20021a(5, a2, 3, 0, a4), a4);
        SecP160R2FieldElement mVar4 = new SecP160R2FieldElement(a2);
        SecP160R2Field.m19352d(a3, mVar4.f12328d);
        int[] iArr = mVar4.f12328d;
        SecP160R2Field.m19353d(iArr, a, iArr);
        int[] iArr2 = mVar4.f12328d;
        SecP160R2Field.m19353d(iArr2, a, iArr2);
        SecP160R2FieldElement mVar5 = new SecP160R2FieldElement(a);
        SecP160R2Field.m19353d(a, mVar4.f12328d, mVar5.f12328d);
        int[] iArr3 = mVar5.f12328d;
        SecP160R2Field.m19349b(iArr3, a3, iArr3);
        int[] iArr4 = mVar5.f12328d;
        SecP160R2Field.m19353d(iArr4, a4, iArr4);
        SecP160R2FieldElement mVar6 = new SecP160R2FieldElement(a3);
        SecP160R2Field.m19354e(mVar.f12328d, mVar6.f12328d);
        if (!mVar3.mo28273e()) {
            int[] iArr5 = mVar6.f12328d;
            SecP160R2Field.m19349b(iArr5, mVar3.f12328d, iArr5);
        }
        return new SecP160K1Point(d, mVar4, mVar5, new ECFieldElement[]{mVar6}, this.f12254e);
    }

    public SecP160K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP160K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
