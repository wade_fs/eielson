package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat160;

/* renamed from: k.b.d.a.j.c.j */
public class SecP160R1Point extends ECPoint.C5167b {
    public SecP160R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP160R1FieldElement iVar = (SecP160R1FieldElement) this.f12251b;
        SecP160R1FieldElement iVar2 = (SecP160R1FieldElement) this.f12252c;
        SecP160R1FieldElement iVar3 = (SecP160R1FieldElement) fVar.mo28306h();
        SecP160R1FieldElement iVar4 = (SecP160R1FieldElement) fVar.mo28308i();
        SecP160R1FieldElement iVar5 = (SecP160R1FieldElement) this.f12253d[0];
        SecP160R1FieldElement iVar6 = (SecP160R1FieldElement) fVar.mo28293a(0);
        int[] b = Nat160.m19890b();
        int[] a = Nat160.m19884a();
        int[] a2 = Nat160.m19884a();
        int[] a3 = Nat160.m19884a();
        boolean e = iVar5.mo28273e();
        if (e) {
            iArr2 = iVar3.f12311d;
            iArr = iVar4.f12311d;
        } else {
            SecP160R1Field.m19215d(iVar5.f12311d, a2);
            SecP160R1Field.m19212b(a2, iVar3.f12311d, a);
            SecP160R1Field.m19212b(a2, iVar5.f12311d, a2);
            SecP160R1Field.m19212b(a2, iVar4.f12311d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = iVar6.mo28273e();
        if (e2) {
            iArr4 = iVar.f12311d;
            iArr3 = iVar2.f12311d;
        } else {
            SecP160R1Field.m19215d(iVar6.f12311d, a3);
            SecP160R1Field.m19212b(a3, iVar.f12311d, b);
            SecP160R1Field.m19212b(a3, iVar6.f12311d, a3);
            SecP160R1Field.m19212b(a3, iVar2.f12311d, a3);
            iArr4 = b;
            iArr3 = a3;
        }
        int[] a4 = Nat160.m19884a();
        SecP160R1Field.m19216d(iArr4, iArr2, a4);
        SecP160R1Field.m19216d(iArr3, iArr, a);
        if (!Nat160.m19888b(a4)) {
            SecP160R1Field.m19215d(a4, a2);
            int[] a5 = Nat160.m19884a();
            SecP160R1Field.m19212b(a2, a4, a5);
            SecP160R1Field.m19212b(a2, iArr4, a2);
            SecP160R1Field.m19211b(a5, a5);
            Nat160.m19893c(iArr3, a5, b);
            SecP160R1Field.m19206a(Nat160.m19887b(a2, a2, a5), a5);
            SecP160R1FieldElement iVar7 = new SecP160R1FieldElement(a3);
            SecP160R1Field.m19215d(a, iVar7.f12311d);
            int[] iArr5 = iVar7.f12311d;
            SecP160R1Field.m19216d(iArr5, a5, iArr5);
            SecP160R1FieldElement iVar8 = new SecP160R1FieldElement(a5);
            SecP160R1Field.m19216d(a2, iVar7.f12311d, iVar8.f12311d);
            SecP160R1Field.m19214c(iVar8.f12311d, a, b);
            SecP160R1Field.m19213c(b, iVar8.f12311d);
            SecP160R1FieldElement iVar9 = new SecP160R1FieldElement(a4);
            if (!e) {
                int[] iArr6 = iVar9.f12311d;
                SecP160R1Field.m19212b(iArr6, iVar5.f12311d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = iVar9.f12311d;
                SecP160R1Field.m19212b(iArr7, iVar6.f12311d, iArr7);
            }
            return new SecP160R1Point(d, iVar7, iVar8, new ECFieldElement[]{iVar9}, this.f12254e);
        } else if (Nat160.m19888b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP160R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP160R1FieldElement iVar = (SecP160R1FieldElement) this.f12252c;
        if (iVar.mo28274f()) {
            return d.mo28258i();
        }
        SecP160R1FieldElement iVar2 = (SecP160R1FieldElement) this.f12251b;
        SecP160R1FieldElement iVar3 = (SecP160R1FieldElement) this.f12253d[0];
        int[] a = Nat160.m19884a();
        int[] a2 = Nat160.m19884a();
        int[] a3 = Nat160.m19884a();
        SecP160R1Field.m19215d(iVar.f12311d, a3);
        int[] a4 = Nat160.m19884a();
        SecP160R1Field.m19215d(a3, a4);
        boolean e = iVar3.mo28273e();
        int[] iArr = iVar3.f12311d;
        if (!e) {
            SecP160R1Field.m19215d(iArr, a2);
            iArr = a2;
        }
        SecP160R1Field.m19216d(iVar2.f12311d, iArr, a);
        SecP160R1Field.m19209a(iVar2.f12311d, iArr, a2);
        SecP160R1Field.m19212b(a2, a, a2);
        SecP160R1Field.m19206a(Nat160.m19887b(a2, a2, a2), a2);
        SecP160R1Field.m19212b(a3, iVar2.f12311d, a3);
        SecP160R1Field.m19206a(Nat.m20045c(5, a3, 2, 0), a3);
        SecP160R1Field.m19206a(Nat.m20021a(5, a4, 3, 0, a), a);
        SecP160R1FieldElement iVar4 = new SecP160R1FieldElement(a4);
        SecP160R1Field.m19215d(a2, iVar4.f12311d);
        int[] iArr2 = iVar4.f12311d;
        SecP160R1Field.m19216d(iArr2, a3, iArr2);
        int[] iArr3 = iVar4.f12311d;
        SecP160R1Field.m19216d(iArr3, a3, iArr3);
        SecP160R1FieldElement iVar5 = new SecP160R1FieldElement(a3);
        SecP160R1Field.m19216d(a3, iVar4.f12311d, iVar5.f12311d);
        int[] iArr4 = iVar5.f12311d;
        SecP160R1Field.m19212b(iArr4, a2, iArr4);
        int[] iArr5 = iVar5.f12311d;
        SecP160R1Field.m19216d(iArr5, a, iArr5);
        SecP160R1FieldElement iVar6 = new SecP160R1FieldElement(a2);
        SecP160R1Field.m19217e(iVar.f12311d, iVar6.f12311d);
        if (!e) {
            int[] iArr6 = iVar6.f12311d;
            SecP160R1Field.m19212b(iArr6, iVar3.f12311d, iArr6);
        }
        return new SecP160R1Point(d, iVar4, iVar5, new ECFieldElement[]{iVar6}, this.f12254e);
    }

    public SecP160R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP160R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
