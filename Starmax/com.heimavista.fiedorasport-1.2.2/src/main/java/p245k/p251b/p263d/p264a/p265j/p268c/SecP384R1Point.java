package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;

/* renamed from: k.b.d.a.j.c.p0 */
public class SecP384R1Point extends ECPoint.C5167b {
    public SecP384R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* JADX WARN: Type inference failed for: r17v0, types: [k.b.d.a.f] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p245k.p251b.p263d.p264a.ECPoint mo28296a(p245k.p251b.p263d.p264a.ECPoint r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            boolean r2 = r16.mo28309j()
            if (r2 == 0) goto L_0x000b
            return r1
        L_0x000b:
            boolean r2 = r17.mo28309j()
            if (r2 == 0) goto L_0x0012
            return r0
        L_0x0012:
            if (r0 != r1) goto L_0x0019
            k.b.d.a.f r1 = r16.mo28316q()
            return r1
        L_0x0019:
            k.b.d.a.c r3 = r16.mo28301d()
            k.b.d.a.d r2 = r0.f12251b
            k.b.d.a.j.c.o0 r2 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1FieldElement) r2
            k.b.d.a.d r4 = r0.f12252c
            k.b.d.a.j.c.o0 r4 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1FieldElement) r4
            k.b.d.a.d r5 = r17.mo28306h()
            k.b.d.a.j.c.o0 r5 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1FieldElement) r5
            k.b.d.a.d r6 = r17.mo28308i()
            k.b.d.a.j.c.o0 r6 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1FieldElement) r6
            k.b.d.a.d[] r7 = r0.f12253d
            r8 = 0
            r7 = r7[r8]
            k.b.d.a.j.c.o0 r7 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1FieldElement) r7
            k.b.d.a.d r1 = r1.mo28293a(r8)
            k.b.d.a.j.c.o0 r1 = (p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1FieldElement) r1
            r9 = 24
            int[] r10 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            int[] r9 = p245k.p251b.p263d.p271c.Nat.m20031a(r9)
            r11 = 12
            int[] r12 = p245k.p251b.p263d.p271c.Nat.m20031a(r11)
            int[] r13 = p245k.p251b.p263d.p271c.Nat.m20031a(r11)
            boolean r14 = r7.mo28273e()
            if (r14 == 0) goto L_0x005d
            int[] r5 = r5.f12339d
            int[] r6 = r6.f12339d
            goto L_0x0073
        L_0x005d:
            int[] r15 = r7.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19425d(r15, r12)
            int[] r5 = r5.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r12, r5, r9)
            int[] r5 = r7.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r12, r5, r12)
            int[] r5 = r6.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r12, r5, r12)
            r5 = r9
            r6 = r12
        L_0x0073:
            boolean r15 = r1.mo28273e()
            if (r15 == 0) goto L_0x007e
            int[] r2 = r2.f12339d
            int[] r4 = r4.f12339d
            goto L_0x0094
        L_0x007e:
            int[] r8 = r1.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19425d(r8, r13)
            int[] r2 = r2.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r13, r2, r10)
            int[] r2 = r1.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r13, r2, r13)
            int[] r2 = r4.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r13, r2, r13)
            r2 = r10
            r4 = r13
        L_0x0094:
            int[] r8 = p245k.p251b.p263d.p271c.Nat.m20031a(r11)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19426d(r2, r5, r8)
            int[] r5 = p245k.p251b.p263d.p271c.Nat.m20031a(r11)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19426d(r4, r6, r5)
            boolean r6 = p245k.p251b.p263d.p271c.Nat.m20053e(r11, r8)
            if (r6 == 0) goto L_0x00b8
            boolean r1 = p245k.p251b.p263d.p271c.Nat.m20053e(r11, r5)
            if (r1 == 0) goto L_0x00b3
            k.b.d.a.f r1 = r16.mo28316q()
            return r1
        L_0x00b3:
            k.b.d.a.f r1 = r3.mo28258i()
            return r1
        L_0x00b8:
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19425d(r8, r12)
            int[] r6 = p245k.p251b.p263d.p271c.Nat.m20031a(r11)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r12, r8, r6)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r12, r2, r12)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19421b(r6, r6)
            p245k.p251b.p263d.p271c.Nat384.m19997a(r4, r6, r10)
            int r2 = p245k.p251b.p263d.p271c.Nat.m20039b(r11, r12, r12, r6)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19414a(r2, r6)
            k.b.d.a.j.c.o0 r4 = new k.b.d.a.j.c.o0
            r4.<init>(r13)
            int[] r2 = r4.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19425d(r5, r2)
            int[] r2 = r4.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19426d(r2, r6, r2)
            k.b.d.a.j.c.o0 r11 = new k.b.d.a.j.c.o0
            r11.<init>(r6)
            int[] r2 = r4.f12339d
            int[] r6 = r11.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19426d(r12, r2, r6)
            int[] r2 = r11.f12339d
            p245k.p251b.p263d.p271c.Nat384.m19997a(r2, r5, r9)
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19422b(r10, r9, r10)
            int[] r2 = r11.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19423c(r10, r2)
            k.b.d.a.j.c.o0 r2 = new k.b.d.a.j.c.o0
            r2.<init>(r8)
            if (r14 != 0) goto L_0x0108
            int[] r5 = r2.f12339d
            int[] r6 = r7.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r5, r6, r5)
        L_0x0108:
            if (r15 != 0) goto L_0x0111
            int[] r5 = r2.f12339d
            int[] r1 = r1.f12339d
            p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Field.m19424c(r5, r1, r5)
        L_0x0111:
            r1 = 1
            k.b.d.a.d[] r6 = new p245k.p251b.p263d.p264a.ECFieldElement[r1]
            r1 = 0
            r6[r1] = r2
            k.b.d.a.j.c.p0 r1 = new k.b.d.a.j.c.p0
            boolean r7 = r0.f12254e
            r2 = r1
            r5 = r11
            r2.<init>(r3, r4, r5, r6, r7)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Point.mo28296a(k.b.d.a.f):k.b.d.a.f");
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP384R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP384R1FieldElement o0Var = (SecP384R1FieldElement) this.f12252c;
        if (o0Var.mo28274f()) {
            return d.mo28258i();
        }
        SecP384R1FieldElement o0Var2 = (SecP384R1FieldElement) this.f12251b;
        SecP384R1FieldElement o0Var3 = (SecP384R1FieldElement) this.f12253d[0];
        int[] a = Nat.m20031a(12);
        int[] a2 = Nat.m20031a(12);
        int[] a3 = Nat.m20031a(12);
        SecP384R1Field.m19425d(o0Var.f12339d, a3);
        int[] a4 = Nat.m20031a(12);
        SecP384R1Field.m19425d(a3, a4);
        boolean e = o0Var3.mo28273e();
        int[] iArr = o0Var3.f12339d;
        if (!e) {
            SecP384R1Field.m19425d(iArr, a2);
            iArr = a2;
        }
        SecP384R1Field.m19426d(o0Var2.f12339d, iArr, a);
        SecP384R1Field.m19418a(o0Var2.f12339d, iArr, a2);
        SecP384R1Field.m19424c(a2, a, a2);
        SecP384R1Field.m19414a(Nat.m20039b(12, a2, a2, a2), a2);
        SecP384R1Field.m19424c(a3, o0Var2.f12339d, a3);
        SecP384R1Field.m19414a(Nat.m20045c(12, a3, 2, 0), a3);
        SecP384R1Field.m19414a(Nat.m20021a(12, a4, 3, 0, a), a);
        SecP384R1FieldElement o0Var4 = new SecP384R1FieldElement(a4);
        SecP384R1Field.m19425d(a2, o0Var4.f12339d);
        int[] iArr2 = o0Var4.f12339d;
        SecP384R1Field.m19426d(iArr2, a3, iArr2);
        int[] iArr3 = o0Var4.f12339d;
        SecP384R1Field.m19426d(iArr3, a3, iArr3);
        SecP384R1FieldElement o0Var5 = new SecP384R1FieldElement(a3);
        SecP384R1Field.m19426d(a3, o0Var4.f12339d, o0Var5.f12339d);
        int[] iArr4 = o0Var5.f12339d;
        SecP384R1Field.m19424c(iArr4, a2, iArr4);
        int[] iArr5 = o0Var5.f12339d;
        SecP384R1Field.m19426d(iArr5, a, iArr5);
        SecP384R1FieldElement o0Var6 = new SecP384R1FieldElement(a2);
        SecP384R1Field.m19427e(o0Var.f12339d, o0Var6.f12339d);
        if (!e) {
            int[] iArr6 = o0Var6.f12339d;
            SecP384R1Field.m19424c(iArr6, o0Var3.f12339d, iArr6);
        }
        return new SecP384R1Point(d, o0Var4, o0Var5, new ECFieldElement[]{o0Var6}, this.f12254e);
    }

    public SecP384R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP384R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
