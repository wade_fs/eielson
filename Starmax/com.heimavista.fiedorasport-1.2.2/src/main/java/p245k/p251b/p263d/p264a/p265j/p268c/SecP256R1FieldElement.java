package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.k0 */
public class SecP256R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12320e = SecP256R1Curve.f12312j;

    /* renamed from: d */
    protected int[] f12321d;

    public SecP256R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12320e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP256R1FieldElement");
        }
        this.f12321d = SecP256R1Field.m19282a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SecP256R1Field.m19281a(this.f12321d, ((SecP256R1FieldElement) dVar).f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Mod.m19851a(SecP256R1Field.f12316a, ((SecP256R1FieldElement) dVar).f12321d, a);
        SecP256R1Field.m19285b(a, this.f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12320e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SecP256R1Field.m19289d(this.f12321d, ((SecP256R1FieldElement) dVar).f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19962a(this.f12321d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP256R1FieldElement)) {
            return false;
        }
        return Nat256.m19972b(this.f12321d, ((SecP256R1FieldElement) obj).f12321d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19970b(this.f12321d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat256.m19966a();
        SecP256R1Field.m19284b(this.f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12321d;
        if (Nat256.m19970b(iArr) || Nat256.m19962a(iArr)) {
            return super;
        }
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        SecP256R1Field.m19288d(iArr, a);
        SecP256R1Field.m19285b(a, iArr, a);
        SecP256R1Field.m19279a(a, 2, a2);
        SecP256R1Field.m19285b(a2, a, a2);
        SecP256R1Field.m19279a(a2, 4, a);
        SecP256R1Field.m19285b(a, a2, a);
        SecP256R1Field.m19279a(a, 8, a2);
        SecP256R1Field.m19285b(a2, a, a2);
        SecP256R1Field.m19279a(a2, 16, a);
        SecP256R1Field.m19285b(a, a2, a);
        SecP256R1Field.m19279a(a, 32, a);
        SecP256R1Field.m19285b(a, iArr, a);
        SecP256R1Field.m19279a(a, 96, a);
        SecP256R1Field.m19285b(a, iArr, a);
        SecP256R1Field.m19279a(a, 94, a);
        SecP256R1Field.m19288d(a, a2);
        if (Nat256.m19972b(iArr, a2)) {
            return new SecP256R1FieldElement(a);
        }
        return null;
    }

    public int hashCode() {
        return f12320e.hashCode() ^ Arrays.m20066b(this.f12321d, 0, 8);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat256.m19966a();
        SecP256R1Field.m19288d(this.f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat256.m19956a(this.f12321d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19977c(this.f12321d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SecP256R1Field.m19285b(this.f12321d, ((SecP256R1FieldElement) dVar).f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat256.m19966a();
        SecP256R1Field.m19280a(this.f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat256.m19966a();
        Mod.m19851a(SecP256R1Field.f12316a, this.f12321d, a);
        return new SecP256R1FieldElement(a);
    }

    public SecP256R1FieldElement() {
        this.f12321d = Nat256.m19966a();
    }

    protected SecP256R1FieldElement(int[] iArr) {
        this.f12321d = iArr;
    }
}
