package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECConstants;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.e */
public class SecP160K1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12290j = SecP160R2Curve.f12318j;

    /* renamed from: i */
    protected SecP160K1Point f12291i = new SecP160K1Point(this, null, null);

    public SecP160K1Curve() {
        super(f12290j);
        this.f12223b = mo28235a(ECConstants.f12219a);
        this.f12224c = mo28235a(BigInteger.valueOf(7));
        this.f12225d = new BigInteger(1, Hex.m20078a("0100000000000000000001B8FA16DFAB9ACA16B6B3"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP160K1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12290j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12291i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP160R2FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP160K1Point(this, dVar, dVar2, z);
    }
}
