package p245k.p251b.p263d.p264a.p265j.p266a;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.a.a */
public class Curve25519 extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12258j = Nat256.m19977c(Curve25519Field.f12260a);

    /* renamed from: i */
    protected Curve25519Point f12259i = new Curve25519Point(this, null, null);

    public Curve25519() {
        super(f12258j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA984914A144")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("7B425ED097B425ED097B425ED097B425ED097B425ED097B4260B5E9C7710C864")));
        this.f12225d = new BigInteger(1, Hex.m20078a("1000000000000000000000000000000014DEF9DEA2F79CD65812631A5CF5D3ED"));
        this.f12226e = BigInteger.valueOf(8);
        this.f12227f = 4;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new Curve25519();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 4;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12258j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12259i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new Curve25519FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new Curve25519Point(this, dVar, dVar2, z);
    }
}
