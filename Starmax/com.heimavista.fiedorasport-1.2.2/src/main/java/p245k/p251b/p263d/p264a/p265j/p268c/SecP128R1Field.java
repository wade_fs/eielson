package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat128;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.c.b */
public class SecP128R1Field {

    /* renamed from: a */
    static final int[] f12276a = {-1, -1, -1, -3};

    /* renamed from: b */
    static final int[] f12277b = {1, 0, 0, 4, -2, -1, 3, -4};

    /* renamed from: c */
    private static final int[] f12278c = {-1, -1, -1, -5, 1, 0, -4, 3};

    /* renamed from: a */
    public static void m18974a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat128.m19854a(iArr, iArr2, iArr3) != 0 || ((iArr3[3] >>> 1) >= 2147483646 && Nat128.m19863b(iArr3, f12276a))) {
            m18971a(iArr3);
        }
    }

    /* renamed from: b */
    public static void m18978b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] c = Nat128.m19871c();
        Nat128.m19870c(iArr, iArr2, c);
        m18979c(c, iArr3);
    }

    /* renamed from: c */
    public static void m18980c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat128.m19873d(iArr, iArr2, iArr3) != 0 || ((iArr3[7] >>> 1) >= 2147483646 && Nat256.m19981c(iArr3, f12277b))) {
            int[] iArr4 = f12278c;
            Nat.m20025a(iArr4.length, iArr4, iArr3);
        }
    }

    /* renamed from: d */
    public static void m18981d(int[] iArr, int[] iArr2) {
        int[] c = Nat128.m19871c();
        Nat128.m19869c(iArr, c);
        m18979c(c, iArr2);
    }

    /* renamed from: e */
    public static void m18983e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(4, iArr, 0, iArr2) != 0 || ((iArr2[3] >>> 1) >= 2147483646 && Nat128.m19863b(iArr2, f12276a))) {
            m18971a(iArr2);
        }
    }

    /* renamed from: a */
    public static void m18973a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(4, iArr, iArr2) != 0 || ((iArr2[3] >>> 1) >= 2147483646 && Nat128.m19863b(iArr2, f12276a))) {
            m18971a(iArr2);
        }
    }

    /* renamed from: b */
    public static void m18977b(int[] iArr, int[] iArr2) {
        if (Nat128.m19862b(iArr)) {
            Nat128.m19874d(iArr2);
        } else {
            Nat128.m19876e(f12276a, iArr, iArr2);
        }
    }

    /* renamed from: c */
    public static void m18979c(int[] iArr, int[] iArr2) {
        int[] iArr3 = iArr2;
        long j = ((long) iArr[7]) & 4294967295L;
        long j2 = (((long) iArr[3]) & 4294967295L) + j;
        long j3 = (((long) iArr[6]) & 4294967295L) + (j << 1);
        long j4 = (((long) iArr[2]) & 4294967295L) + j3;
        long j5 = (((long) iArr[5]) & 4294967295L) + (j3 << 1);
        long j6 = (((long) iArr[1]) & 4294967295L) + j5;
        long j7 = (((long) iArr[4]) & 4294967295L) + (j5 << 1);
        long j8 = (((long) iArr[0]) & 4294967295L) + j7;
        iArr3[0] = (int) j8;
        long j9 = j6 + (j8 >>> 32);
        iArr3[1] = (int) j9;
        long j10 = j4 + (j9 >>> 32);
        iArr3[2] = (int) j10;
        long j11 = j2 + (j7 << 1) + (j10 >>> 32);
        iArr3[3] = (int) j11;
        m18970a((int) (j11 >>> 32), iArr3);
    }

    /* renamed from: d */
    public static void m18982d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat128.m19876e(iArr, iArr2, iArr3) != 0) {
            m18976b(iArr3);
        }
    }

    /* renamed from: a */
    public static int[] m18975a(BigInteger bigInteger) {
        int[] a = Nat128.m19860a(bigInteger);
        if ((a[3] >>> 1) >= 2147483646 && Nat128.m19863b(a, f12276a)) {
            Nat128.m19872d(f12276a, a);
        }
        return a;
    }

    /* renamed from: b */
    private static void m18976b(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) - 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            long j4 = (j3 >> 32) + (((long) iArr[2]) & 4294967295L);
            iArr[2] = (int) j4;
            j2 = j4 >> 32;
        }
        iArr[3] = (int) (j2 + ((4294967295L & ((long) iArr[3])) - 2));
    }

    /* renamed from: a */
    public static void m18970a(int i, int[] iArr) {
        while (i != 0) {
            long j = ((long) i) & 4294967295L;
            long j2 = (((long) iArr[0]) & 4294967295L) + j;
            iArr[0] = (int) j2;
            long j3 = j2 >> 32;
            if (j3 != 0) {
                long j4 = j3 + (((long) iArr[1]) & 4294967295L);
                iArr[1] = (int) j4;
                long j5 = (j4 >> 32) + (((long) iArr[2]) & 4294967295L);
                iArr[2] = (int) j5;
                j3 = j5 >> 32;
            }
            long j6 = j3 + (4294967295L & ((long) iArr[3])) + (j << 1);
            iArr[3] = (int) j6;
            i = (int) (j6 >> 32);
        }
    }

    /* renamed from: a */
    public static void m18972a(int[] iArr, int i, int[] iArr2) {
        int[] c = Nat128.m19871c();
        Nat128.m19869c(iArr, c);
        m18979c(c, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat128.m19869c(iArr2, c);
                m18979c(c, iArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private static void m18971a(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) + 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            long j4 = (j3 >> 32) + (((long) iArr[2]) & 4294967295L);
            iArr[2] = (int) j4;
            j2 = j4 >> 32;
        }
        iArr[3] = (int) (j2 + (4294967295L & ((long) iArr[3])) + 2);
    }
}
