package p245k.p251b.p263d.p270b;

import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.b.d */
class GF2Polynomial implements Polynomial {

    /* renamed from: a */
    protected final int[] f12384a;

    GF2Polynomial(int[] iArr) {
        this.f12384a = Arrays.m20061a(iArr);
    }

    /* renamed from: a */
    public int[] mo28395a() {
        return Arrays.m20061a(this.f12384a);
    }

    /* renamed from: b */
    public int mo28396b() {
        int[] iArr = this.f12384a;
        return iArr[iArr.length - 1];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof GF2Polynomial)) {
            return false;
        }
        return Arrays.m20059a(this.f12384a, ((GF2Polynomial) obj).f12384a);
    }

    public int hashCode() {
        return Arrays.m20065b(this.f12384a);
    }
}
