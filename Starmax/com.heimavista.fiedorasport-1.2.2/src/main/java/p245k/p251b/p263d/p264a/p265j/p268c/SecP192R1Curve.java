package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.s */
public class SecP192R1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12353j = new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFF"));

    /* renamed from: i */
    protected SecP192R1Point f12354i = new SecP192R1Point(this, null, null);

    public SecP192R1Curve() {
        super(f12353j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFFFFFFFFFFFC")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("64210519E59C80E70FA7E9AB72243049FEB8DEECC146B9B1")));
        this.f12225d = new BigInteger(1, Hex.m20078a("FFFFFFFFFFFFFFFFFFFFFFFF99DEF836146BC9B1B4D22831"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP192R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12353j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12354i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP192R1FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP192R1Point(this, dVar, dVar2, z);
    }
}
