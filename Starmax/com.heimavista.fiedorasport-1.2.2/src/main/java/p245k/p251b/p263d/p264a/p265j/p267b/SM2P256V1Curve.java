package p245k.p251b.p263d.p264a.p265j.p267b;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.b.a */
public class SM2P256V1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12265j = new BigInteger(1, Hex.m20078a("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFF"));

    /* renamed from: i */
    protected SM2P256V1Point f12266i = new SM2P256V1Point(this, null, null);

    public SM2P256V1Curve() {
        super(f12265j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000FFFFFFFFFFFFFFFC")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("28E9FA9E9D9F5E344D5A9E4BCF6509A7F39789F515AB8F92DDBCBD414D940E93")));
        this.f12225d = new BigInteger(1, Hex.m20078a("FFFFFFFEFFFFFFFFFFFFFFFFFFFFFFFF7203DF6B21C6052B53BBF40939D54123"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SM2P256V1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12265j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12266i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SM2P256V1FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SM2P256V1Point(this, dVar, dVar2, z);
    }
}
