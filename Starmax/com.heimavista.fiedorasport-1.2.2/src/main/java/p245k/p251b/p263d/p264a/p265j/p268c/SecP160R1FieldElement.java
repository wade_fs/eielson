package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat160;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.i */
public class SecP160R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12310e = SecP160R1Curve.f12300j;

    /* renamed from: d */
    protected int[] f12311d;

    public SecP160R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12310e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP160R1FieldElement");
        }
        this.f12311d = SecP160R1Field.m19210a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19209a(this.f12311d, ((SecP160R1FieldElement) dVar).f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        Mod.m19851a(SecP160R1Field.f12306a, ((SecP160R1FieldElement) dVar).f12311d, a);
        SecP160R1Field.m19212b(a, this.f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12310e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19216d(this.f12311d, ((SecP160R1FieldElement) dVar).f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat160.m19882a(this.f12311d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP160R1FieldElement)) {
            return false;
        }
        return Nat160.m19883a(this.f12311d, ((SecP160R1FieldElement) obj).f12311d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat160.m19888b(this.f12311d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19211b(this.f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12311d;
        if (Nat160.m19888b(iArr) || Nat160.m19882a(iArr)) {
            return super;
        }
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19215d(iArr, a);
        SecP160R1Field.m19212b(a, iArr, a);
        int[] a2 = Nat160.m19884a();
        SecP160R1Field.m19207a(a, 2, a2);
        SecP160R1Field.m19212b(a2, a, a2);
        SecP160R1Field.m19207a(a2, 4, a);
        SecP160R1Field.m19212b(a, a2, a);
        SecP160R1Field.m19207a(a, 8, a2);
        SecP160R1Field.m19212b(a2, a, a2);
        SecP160R1Field.m19207a(a2, 16, a);
        SecP160R1Field.m19212b(a, a2, a);
        SecP160R1Field.m19207a(a, 32, a2);
        SecP160R1Field.m19212b(a2, a, a2);
        SecP160R1Field.m19207a(a2, 64, a);
        SecP160R1Field.m19212b(a, a2, a);
        SecP160R1Field.m19215d(a, a2);
        SecP160R1Field.m19212b(a2, iArr, a2);
        SecP160R1Field.m19207a(a2, 29, a2);
        SecP160R1Field.m19215d(a2, a);
        if (Nat160.m19883a(iArr, a)) {
            return new SecP160R1FieldElement(a2);
        }
        return null;
    }

    public int hashCode() {
        return f12310e.hashCode() ^ Arrays.m20066b(this.f12311d, 0, 5);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19215d(this.f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat160.m19879a(this.f12311d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat160.m19891c(this.f12311d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19212b(this.f12311d, ((SecP160R1FieldElement) dVar).f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat160.m19884a();
        SecP160R1Field.m19208a(this.f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat160.m19884a();
        Mod.m19851a(SecP160R1Field.f12306a, this.f12311d, a);
        return new SecP160R1FieldElement(a);
    }

    public SecP160R1FieldElement() {
        this.f12311d = Nat160.m19884a();
    }

    protected SecP160R1FieldElement(int[] iArr) {
        this.f12311d = iArr;
    }
}
