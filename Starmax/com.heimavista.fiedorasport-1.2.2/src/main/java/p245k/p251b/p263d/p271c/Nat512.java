package p245k.p251b.p263d.p271c;

/* renamed from: k.b.d.c.k */
public abstract class Nat512 {
    /* renamed from: a */
    public static void m20006a(int[] iArr, int[] iArr2, int[] iArr3) {
        Nat256.m19980c(iArr, iArr2, iArr3);
        Nat256.m19969b(iArr, 8, iArr2, 8, iArr3, 16);
        int a = Nat256.m19957a(iArr3, 8, iArr3, 16);
        int a2 = a + Nat256.m19958a(iArr3, 24, iArr3, 16, Nat256.m19958a(iArr3, 0, iArr3, 8, 0) + a);
        int[] a3 = Nat256.m19966a();
        int[] a4 = Nat256.m19966a();
        boolean z = Nat256.m19963a(iArr, 8, iArr, 0, a3, 0) != Nat256.m19963a(iArr2, 8, iArr2, 0, a4, 0);
        int[] c = Nat256.m19982c();
        Nat256.m19980c(a3, a4, c);
        Nat.m20015a(32, a2 + (z ? Nat.m20024a(16, c, 0, iArr3, 8) : Nat.m20038b(16, c, 0, iArr3, 8)), iArr3, 24);
    }

    /* renamed from: a */
    public static void m20005a(int[] iArr, int[] iArr2) {
        Nat256.m19985d(iArr, iArr2);
        Nat256.m19979c(iArr, 8, iArr2, 16);
        int a = Nat256.m19957a(iArr2, 8, iArr2, 16);
        int a2 = a + Nat256.m19958a(iArr2, 24, iArr2, 16, Nat256.m19958a(iArr2, 0, iArr2, 8, 0) + a);
        int[] a3 = Nat256.m19966a();
        Nat256.m19963a(iArr, 8, iArr, 0, a3, 0);
        int[] c = Nat256.m19982c();
        Nat256.m19985d(a3, c);
        Nat.m20015a(32, a2 + Nat.m20038b(16, c, 0, iArr2, 8), iArr2, 24);
    }
}
