package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat160;

/* renamed from: k.b.d.a.j.c.l */
public class SecP160R2Field {

    /* renamed from: a */
    static final int[] f12323a = {-21389, -2, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12324b = {457489321, 42778, 1, 0, 0, -42778, -3, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12325c = {-457489321, -42779, -2, -1, -1, 42777, 2};

    /* renamed from: a */
    public static void m19346a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat160.m19880a(iArr, iArr2, iArr3) != 0 || (iArr3[4] == -1 && Nat160.m19889b(iArr3, f12323a))) {
            Nat.m20014a(5, 21389, iArr3);
        }
    }

    /* renamed from: b */
    public static void m19349b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] b = Nat160.m19890b();
        Nat160.m19893c(iArr, iArr2, b);
        m19350c(b, iArr3);
    }

    /* renamed from: c */
    public static void m19351c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat160.m19895d(iArr, iArr2, iArr3) != 0 || (iArr3[9] == -1 && Nat.m20047c(10, iArr3, f12324b))) {
            int[] iArr4 = f12325c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(10, iArr3, f12325c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m19352d(int[] iArr, int[] iArr2) {
        int[] b = Nat160.m19890b();
        Nat160.m19892c(iArr, b);
        m19350c(b, iArr2);
    }

    /* renamed from: e */
    public static void m19354e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(5, iArr, 0, iArr2) != 0 || (iArr2[4] == -1 && Nat160.m19889b(iArr2, f12323a))) {
            Nat.m20014a(5, 21389, iArr2);
        }
    }

    /* renamed from: a */
    public static void m19345a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(5, iArr, iArr2) != 0 || (iArr2[4] == -1 && Nat160.m19889b(iArr2, f12323a))) {
            Nat.m20014a(5, 21389, iArr2);
        }
    }

    /* renamed from: b */
    public static void m19348b(int[] iArr, int[] iArr2) {
        if (Nat160.m19888b(iArr)) {
            Nat160.m19896d(iArr2);
        } else {
            Nat160.m19897e(f12323a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m19353d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat160.m19897e(iArr, iArr2, iArr3) != 0) {
            Nat.m20042c(5, 21389, iArr3);
        }
    }

    /* renamed from: c */
    public static void m19350c(int[] iArr, int[] iArr2) {
        if (Nat160.m19878a(21389, Nat160.m19881a(21389, iArr, 5, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[4] == -1 && Nat160.m19889b(iArr2, f12323a))) {
            Nat.m20014a(5, 21389, iArr2);
        }
    }

    /* renamed from: a */
    public static int[] m19347a(BigInteger bigInteger) {
        int[] a = Nat160.m19885a(bigInteger);
        if (a[4] == -1 && Nat160.m19889b(a, f12323a)) {
            Nat160.m19894d(f12323a, a);
        }
        return a;
    }

    /* renamed from: a */
    public static void m19343a(int i, int[] iArr) {
        if ((i != 0 && Nat160.m19877a(21389, i, iArr, 0) != 0) || (iArr[4] == -1 && Nat160.m19889b(iArr, f12323a))) {
            Nat.m20014a(5, 21389, iArr);
        }
    }

    /* renamed from: a */
    public static void m19344a(int[] iArr, int i, int[] iArr2) {
        int[] b = Nat160.m19890b();
        Nat160.m19892c(iArr, b);
        m19350c(b, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat160.m19892c(iArr2, b);
                m19350c(b, iArr2);
            } else {
                return;
            }
        }
    }
}
