package p245k.p251b.p263d.p270b;

import java.math.BigInteger;

/* renamed from: k.b.d.b.c */
public abstract class FiniteFields {

    /* renamed from: a */
    static final FiniteField f12382a = new PrimeField(BigInteger.valueOf(2));

    /* renamed from: b */
    static final FiniteField f12383b = new PrimeField(BigInteger.valueOf(3));

    /* renamed from: a */
    public static PolynomialExtensionField m19832a(int[] iArr) {
        if (iArr[0] == 0) {
            int i = 1;
            while (i < iArr.length) {
                if (iArr[i] > iArr[i - 1]) {
                    i++;
                } else {
                    throw new IllegalArgumentException("Polynomial exponents must be montonically increasing");
                }
            }
            return new GenericPolynomialExtensionField(f12382a, new GF2Polynomial(iArr));
        }
        throw new IllegalArgumentException("Irreducible polynomials in GF(2) must have constant term");
    }

    /* renamed from: a */
    public static FiniteField m19831a(BigInteger bigInteger) {
        int bitLength = bigInteger.bitLength();
        if (bigInteger.signum() <= 0 || bitLength < 2) {
            throw new IllegalArgumentException("'characteristic' must be >= 2");
        }
        if (bitLength < 3) {
            int intValue = bigInteger.intValue();
            if (intValue == 2) {
                return f12382a;
            }
            if (intValue == 3) {
                return f12383b;
            }
        }
        return new PrimeField(bigInteger);
    }
}
