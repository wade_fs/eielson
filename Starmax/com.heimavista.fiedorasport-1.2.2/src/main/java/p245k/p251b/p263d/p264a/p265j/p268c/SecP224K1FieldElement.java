package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat224;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.y */
public class SecP224K1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12376e = SecP224K1Curve.f12369j;

    /* renamed from: f */
    private static final int[] f12377f = {868209154, -587542221, 579297866, -1014948952, -1470801668, 514782679, -1897982644};

    /* renamed from: d */
    protected int[] f12378d;

    public SecP224K1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12376e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP224K1FieldElement");
        }
        this.f12378d = SecP224K1Field.m19774a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19773a(this.f12378d, ((SecP224K1FieldElement) dVar).f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        Mod.m19851a(SecP224K1Field.f12373a, ((SecP224K1FieldElement) dVar).f12378d, a);
        SecP224K1Field.m19776b(a, this.f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12376e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19780d(this.f12378d, ((SecP224K1FieldElement) dVar).f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat224.m19938a(this.f12378d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP224K1FieldElement)) {
            return false;
        }
        return Nat224.m19943b(this.f12378d, ((SecP224K1FieldElement) obj).f12378d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat224.m19942b(this.f12378d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19775b(this.f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12378d;
        if (Nat224.m19942b(iArr) || Nat224.m19938a(iArr)) {
            return super;
        }
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19779d(iArr, a);
        SecP224K1Field.m19776b(a, iArr, a);
        SecP224K1Field.m19779d(a, a);
        SecP224K1Field.m19776b(a, iArr, a);
        int[] a2 = Nat224.m19939a();
        SecP224K1Field.m19779d(a, a2);
        SecP224K1Field.m19776b(a2, iArr, a2);
        int[] a3 = Nat224.m19939a();
        SecP224K1Field.m19771a(a2, 4, a3);
        SecP224K1Field.m19776b(a3, a2, a3);
        int[] a4 = Nat224.m19939a();
        SecP224K1Field.m19771a(a3, 3, a4);
        SecP224K1Field.m19776b(a4, a, a4);
        SecP224K1Field.m19771a(a4, 8, a4);
        SecP224K1Field.m19776b(a4, a3, a4);
        SecP224K1Field.m19771a(a4, 4, a3);
        SecP224K1Field.m19776b(a3, a2, a3);
        SecP224K1Field.m19771a(a3, 19, a2);
        SecP224K1Field.m19776b(a2, a4, a2);
        int[] a5 = Nat224.m19939a();
        SecP224K1Field.m19771a(a2, 42, a5);
        SecP224K1Field.m19776b(a5, a2, a5);
        SecP224K1Field.m19771a(a5, 23, a2);
        SecP224K1Field.m19776b(a2, a3, a2);
        SecP224K1Field.m19771a(a2, 84, a3);
        SecP224K1Field.m19776b(a3, a5, a3);
        SecP224K1Field.m19771a(a3, 20, a3);
        SecP224K1Field.m19776b(a3, a4, a3);
        SecP224K1Field.m19771a(a3, 3, a3);
        SecP224K1Field.m19776b(a3, iArr, a3);
        SecP224K1Field.m19771a(a3, 2, a3);
        SecP224K1Field.m19776b(a3, iArr, a3);
        SecP224K1Field.m19771a(a3, 4, a3);
        SecP224K1Field.m19776b(a3, a, a3);
        SecP224K1Field.m19779d(a3, a3);
        SecP224K1Field.m19779d(a3, a5);
        if (Nat224.m19943b(iArr, a5)) {
            return new SecP224K1FieldElement(a3);
        }
        SecP224K1Field.m19776b(a3, f12377f, a3);
        SecP224K1Field.m19779d(a3, a5);
        if (Nat224.m19943b(iArr, a5)) {
            return new SecP224K1FieldElement(a3);
        }
        return null;
    }

    public int hashCode() {
        return f12376e.hashCode() ^ Arrays.m20066b(this.f12378d, 0, 7);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19779d(this.f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat224.m19934a(this.f12378d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat224.m19945c(this.f12378d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19776b(this.f12378d, ((SecP224K1FieldElement) dVar).f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat224.m19939a();
        SecP224K1Field.m19772a(this.f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat224.m19939a();
        Mod.m19851a(SecP224K1Field.f12373a, this.f12378d, a);
        return new SecP224K1FieldElement(a);
    }

    public SecP224K1FieldElement() {
        this.f12378d = Nat224.m19939a();
    }

    protected SecP224K1FieldElement(int[] iArr) {
        this.f12378d = iArr;
    }
}
