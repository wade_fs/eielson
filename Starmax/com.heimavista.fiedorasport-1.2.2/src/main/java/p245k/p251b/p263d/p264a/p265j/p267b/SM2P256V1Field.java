package p245k.p251b.p263d.p264a.p265j.p267b;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.b.b */
public class SM2P256V1Field {

    /* renamed from: a */
    static final int[] f12267a = {-1, -1, 0, -1, -1, -1, -1, -2};

    /* renamed from: b */
    static final int[] f12268b = {1, 0, -2, 1, 1, -2, 0, 2, -2, -3, 3, -2, -1, -1, 0, -2};

    /* renamed from: a */
    public static void m18896a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19960a(iArr, iArr2, iArr3) != 0 || ((iArr3[7] >>> 1) >= Integer.MAX_VALUE && Nat256.m19981c(iArr3, f12267a))) {
            m18893a(iArr3);
        }
    }

    /* renamed from: b */
    public static void m18900b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] c = Nat256.m19982c();
        Nat256.m19980c(iArr, iArr2, c);
        m18901c(c, iArr3);
    }

    /* renamed from: c */
    public static void m18902c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19983d(iArr, iArr2, iArr3) != 0 || ((iArr3[15] >>> 1) >= Integer.MAX_VALUE && Nat.m20047c(16, iArr3, f12268b))) {
            Nat.m20052e(16, f12268b, iArr3);
        }
    }

    /* renamed from: d */
    public static void m18903d(int[] iArr, int[] iArr2) {
        int[] c = Nat256.m19982c();
        Nat256.m19985d(iArr, c);
        m18901c(c, iArr2);
    }

    /* renamed from: e */
    public static void m18905e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(8, iArr, 0, iArr2) != 0 || ((iArr2[7] >>> 1) >= Integer.MAX_VALUE && Nat256.m19981c(iArr2, f12267a))) {
            m18893a(iArr2);
        }
    }

    /* renamed from: a */
    public static void m18895a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(8, iArr, iArr2) != 0 || ((iArr2[7] >>> 1) >= Integer.MAX_VALUE && Nat256.m19981c(iArr2, f12267a))) {
            m18893a(iArr2);
        }
    }

    /* renamed from: b */
    public static void m18899b(int[] iArr, int[] iArr2) {
        if (Nat256.m19970b(iArr)) {
            Nat256.m19984d(iArr2);
        } else {
            Nat256.m19988e(f12267a, iArr, iArr2);
        }
    }

    /* renamed from: c */
    public static void m18901c(int[] iArr, int[] iArr2) {
        int[] iArr3 = iArr2;
        long j = ((long) iArr[8]) & 4294967295L;
        long j2 = ((long) iArr[9]) & 4294967295L;
        long j3 = ((long) iArr[10]) & 4294967295L;
        long j4 = ((long) iArr[11]) & 4294967295L;
        long j5 = ((long) iArr[12]) & 4294967295L;
        long j6 = ((long) iArr[13]) & 4294967295L;
        long j7 = ((long) iArr[14]) & 4294967295L;
        long j8 = ((long) iArr[15]) & 4294967295L;
        long j9 = j3 + j4;
        long j10 = j6 + j7;
        long j11 = j10 + (j8 << 1);
        long j12 = j + j2 + j10;
        long j13 = j9 + j5 + j8 + j12;
        long j14 = j5;
        long j15 = (((long) iArr[0]) & 4294967295L) + j13 + j6 + j7 + j8 + 0;
        iArr3[0] = (int) j15;
        long j16 = j4;
        long j17 = (j15 >> 32) + (((((long) iArr[1]) & 4294967295L) + j13) - j) + j7 + j8;
        iArr3[1] = (int) j17;
        long j18 = (j17 >> 32) + ((((long) iArr[2]) & 4294967295L) - j12);
        iArr3[2] = (int) j18;
        long j19 = (j18 >> 32) + ((((((long) iArr[3]) & 4294967295L) + j13) - j2) - j3) + j6;
        iArr3[3] = (int) j19;
        long j20 = (j19 >> 32) + ((((((long) iArr[4]) & 4294967295L) + j13) - j9) - j) + j7;
        iArr3[4] = (int) j20;
        long j21 = (j20 >> 32) + (((long) iArr[5]) & 4294967295L) + j11 + j3;
        iArr3[5] = (int) j21;
        long j22 = (j21 >> 32) + (((long) iArr[6]) & 4294967295L) + j16 + j7 + j8;
        iArr3[6] = (int) j22;
        long j23 = (j22 >> 32) + (4294967295L & ((long) iArr[7])) + j13 + j11 + j14;
        iArr3[7] = (int) j23;
        m18892a((int) (j23 >> 32), iArr3);
    }

    /* renamed from: d */
    public static void m18904d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19988e(iArr, iArr2, iArr3) != 0) {
            m18898b(iArr3);
        }
    }

    /* renamed from: a */
    public static int[] m18897a(BigInteger bigInteger) {
        int[] a = Nat256.m19967a(bigInteger);
        if ((a[7] >>> 1) >= Integer.MAX_VALUE && Nat256.m19981c(a, f12267a)) {
            Nat256.m19987e(f12267a, a);
        }
        return a;
    }

    /* renamed from: b */
    private static void m18898b(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) - 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            j2 = j3 >> 32;
        }
        long j4 = j2 + (((long) iArr[2]) & 4294967295L) + 1;
        iArr[2] = (int) j4;
        long j5 = (j4 >> 32) + ((((long) iArr[3]) & 4294967295L) - 1);
        iArr[3] = (int) j5;
        long j6 = j5 >> 32;
        if (j6 != 0) {
            long j7 = j6 + (((long) iArr[4]) & 4294967295L);
            iArr[4] = (int) j7;
            long j8 = (j7 >> 32) + (((long) iArr[5]) & 4294967295L);
            iArr[5] = (int) j8;
            long j9 = (j8 >> 32) + (((long) iArr[6]) & 4294967295L);
            iArr[6] = (int) j9;
            j6 = j9 >> 32;
        }
        iArr[7] = (int) (j6 + ((4294967295L & ((long) iArr[7])) - 1));
    }

    /* renamed from: a */
    public static void m18892a(int i, int[] iArr) {
        long j;
        if (i != 0) {
            long j2 = ((long) i) & 4294967295L;
            long j3 = (((long) iArr[0]) & 4294967295L) + j2 + 0;
            iArr[0] = (int) j3;
            long j4 = j3 >> 32;
            if (j4 != 0) {
                long j5 = j4 + (((long) iArr[1]) & 4294967295L);
                iArr[1] = (int) j5;
                j4 = j5 >> 32;
            }
            long j6 = j4 + ((((long) iArr[2]) & 4294967295L) - j2);
            iArr[2] = (int) j6;
            long j7 = (j6 >> 32) + (((long) iArr[3]) & 4294967295L) + j2;
            iArr[3] = (int) j7;
            long j8 = j7 >> 32;
            if (j8 != 0) {
                long j9 = j8 + (((long) iArr[4]) & 4294967295L);
                iArr[4] = (int) j9;
                long j10 = (j9 >> 32) + (((long) iArr[5]) & 4294967295L);
                iArr[5] = (int) j10;
                long j11 = (j10 >> 32) + (((long) iArr[6]) & 4294967295L);
                iArr[6] = (int) j11;
                j8 = j11 >> 32;
            }
            long j12 = j8 + (4294967295L & ((long) iArr[7])) + j2;
            iArr[7] = (int) j12;
            j = j12 >> 32;
        } else {
            j = 0;
        }
        if (j != 0 || ((iArr[7] >>> 1) >= Integer.MAX_VALUE && Nat256.m19981c(iArr, f12267a))) {
            m18893a(iArr);
        }
    }

    /* renamed from: a */
    public static void m18894a(int[] iArr, int i, int[] iArr2) {
        int[] c = Nat256.m19982c();
        Nat256.m19985d(iArr, c);
        m18901c(c, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat256.m19985d(iArr2, c);
                m18901c(c, iArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private static void m18893a(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) + 1;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            long j3 = j2 + (((long) iArr[1]) & 4294967295L);
            iArr[1] = (int) j3;
            j2 = j3 >> 32;
        }
        long j4 = j2 + ((((long) iArr[2]) & 4294967295L) - 1);
        iArr[2] = (int) j4;
        long j5 = (j4 >> 32) + (((long) iArr[3]) & 4294967295L) + 1;
        iArr[3] = (int) j5;
        long j6 = j5 >> 32;
        if (j6 != 0) {
            long j7 = j6 + (((long) iArr[4]) & 4294967295L);
            iArr[4] = (int) j7;
            long j8 = (j7 >> 32) + (((long) iArr[5]) & 4294967295L);
            iArr[5] = (int) j8;
            long j9 = (j8 >> 32) + (((long) iArr[6]) & 4294967295L);
            iArr[6] = (int) j9;
            j6 = j9 >> 32;
        }
        iArr[7] = (int) (j6 + (4294967295L & ((long) iArr[7])) + 1);
    }
}
