package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.g0 */
public class SecP256K1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12302e = SecP256K1Curve.f12292j;

    /* renamed from: d */
    protected int[] f12303d;

    public SecP256K1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12302e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP256K1FieldElement");
        }
        this.f12303d = SecP256K1Field.m19134a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19133a(this.f12303d, ((SecP256K1FieldElement) dVar).f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Mod.m19851a(SecP256K1Field.f12296a, ((SecP256K1FieldElement) dVar).f12303d, a);
        SecP256K1Field.m19136b(a, this.f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12302e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19140d(this.f12303d, ((SecP256K1FieldElement) dVar).f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19962a(this.f12303d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP256K1FieldElement)) {
            return false;
        }
        return Nat256.m19972b(this.f12303d, ((SecP256K1FieldElement) obj).f12303d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19970b(this.f12303d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19135b(this.f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12303d;
        if (Nat256.m19970b(iArr) || Nat256.m19962a(iArr)) {
            return super;
        }
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19139d(iArr, a);
        SecP256K1Field.m19136b(a, iArr, a);
        int[] a2 = Nat256.m19966a();
        SecP256K1Field.m19139d(a, a2);
        SecP256K1Field.m19136b(a2, iArr, a2);
        int[] a3 = Nat256.m19966a();
        SecP256K1Field.m19131a(a2, 3, a3);
        SecP256K1Field.m19136b(a3, a2, a3);
        SecP256K1Field.m19131a(a3, 3, a3);
        SecP256K1Field.m19136b(a3, a2, a3);
        SecP256K1Field.m19131a(a3, 2, a3);
        SecP256K1Field.m19136b(a3, a, a3);
        int[] a4 = Nat256.m19966a();
        SecP256K1Field.m19131a(a3, 11, a4);
        SecP256K1Field.m19136b(a4, a3, a4);
        SecP256K1Field.m19131a(a4, 22, a3);
        SecP256K1Field.m19136b(a3, a4, a3);
        int[] a5 = Nat256.m19966a();
        SecP256K1Field.m19131a(a3, 44, a5);
        SecP256K1Field.m19136b(a5, a3, a5);
        int[] a6 = Nat256.m19966a();
        SecP256K1Field.m19131a(a5, 88, a6);
        SecP256K1Field.m19136b(a6, a5, a6);
        SecP256K1Field.m19131a(a6, 44, a5);
        SecP256K1Field.m19136b(a5, a3, a5);
        SecP256K1Field.m19131a(a5, 3, a3);
        SecP256K1Field.m19136b(a3, a2, a3);
        SecP256K1Field.m19131a(a3, 23, a3);
        SecP256K1Field.m19136b(a3, a4, a3);
        SecP256K1Field.m19131a(a3, 6, a3);
        SecP256K1Field.m19136b(a3, a, a3);
        SecP256K1Field.m19131a(a3, 2, a3);
        SecP256K1Field.m19139d(a3, a);
        if (Nat256.m19972b(iArr, a)) {
            return new SecP256K1FieldElement(a3);
        }
        return null;
    }

    public int hashCode() {
        return f12302e.hashCode() ^ Arrays.m20066b(this.f12303d, 0, 8);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19139d(this.f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat256.m19956a(this.f12303d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19977c(this.f12303d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19136b(this.f12303d, ((SecP256K1FieldElement) dVar).f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19132a(this.f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat256.m19966a();
        Mod.m19851a(SecP256K1Field.f12296a, this.f12303d, a);
        return new SecP256K1FieldElement(a);
    }

    public SecP256K1FieldElement() {
        this.f12303d = Nat256.m19966a();
    }

    protected SecP256K1FieldElement(int[] iArr) {
        this.f12303d = iArr;
    }
}
