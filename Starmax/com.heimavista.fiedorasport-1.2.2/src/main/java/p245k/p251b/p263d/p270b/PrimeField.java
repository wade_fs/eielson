package p245k.p251b.p263d.p270b;

import java.math.BigInteger;

/* renamed from: k.b.d.b.h */
class PrimeField implements FiniteField {

    /* renamed from: a */
    protected final BigInteger f12387a;

    PrimeField(BigInteger bigInteger) {
        this.f12387a = bigInteger;
    }

    /* renamed from: b */
    public int mo28393b() {
        return 1;
    }

    /* renamed from: c */
    public BigInteger mo28394c() {
        return this.f12387a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PrimeField)) {
            return false;
        }
        return this.f12387a.equals(((PrimeField) obj).f12387a);
    }

    public int hashCode() {
        return this.f12387a.hashCode();
    }
}
