package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.u2 */
public class SecT571R1Curve extends ECCurve.C5159a {

    /* renamed from: j */
    static final SecT571FieldElement f12364j = new SecT571FieldElement(new BigInteger(1, Hex.m20078a("02F40E7E2221F295DE297117B7F3D62F5C6A97FFCB8CEFF1CD6BA8CE4A9A18AD84FFABBD8EFA59332BE7AD6756A66E294AFD185A78FF12AA520E4DE739BACA0C7FFEFF7F2955727A")));

    /* renamed from: k */
    static final SecT571FieldElement f12365k = ((SecT571FieldElement) f12364j.mo28276h());

    /* renamed from: i */
    protected SecT571R1Point f12366i = new SecT571R1Point(this, null, null);

    public SecT571R1Curve() {
        super(571, 2, 5, 10);
        this.f12223b = mo28235a(BigInteger.valueOf(1));
        this.f12224c = f12364j;
        this.f12225d = new BigInteger(1, Hex.m20078a("03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE661CE18FF55987308059B186823851EC7DD9CA1161DE93D5174D66E8382E9BB2FE84E47"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT571R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 571;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12366i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT571FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT571R1Point(this, dVar, dVar2, z);
    }
}
