package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.p1 */
public class SecT193FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12344d;

    public SecT193FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 193) {
            throw new IllegalArgumentException("x value invalid for SecT193FieldElement");
        }
        this.f12344d = SecT193Field.m19462a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] b = Nat256.m19974b();
        SecT193Field.m19461a(this.f12344d, ((SecT193FieldElement) dVar).f12344d, b);
        return new SecT193FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12344d;
        long[] jArr2 = ((SecT193FieldElement) dVar).f12344d;
        long[] jArr3 = ((SecT193FieldElement) dVar2).f12344d;
        long[] jArr4 = ((SecT193FieldElement) dVar3).f12344d;
        long[] d = Nat256.m19986d();
        SecT193Field.m19470e(jArr, jArr2, d);
        SecT193Field.m19470e(jArr3, jArr4, d);
        long[] b = Nat256.m19974b();
        SecT193Field.m19469e(d, b);
        return new SecT193FieldElement(b);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 193;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] b = Nat256.m19974b();
        SecT193Field.m19468d(this.f12344d, ((SecT193FieldElement) dVar).f12344d, b);
        return new SecT193FieldElement(b);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19964a(this.f12344d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT193FieldElement)) {
            return false;
        }
        return Nat256.m19965a(this.f12344d, ((SecT193FieldElement) obj).f12344d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19973b(this.f12344d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] b = Nat256.m19974b();
        SecT193Field.m19471f(this.f12344d, b);
        return new SecT193FieldElement(b);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12344d, 0, 4) ^ 1930015;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] b = Nat256.m19974b();
        SecT193Field.m19472g(this.f12344d, b);
        return new SecT193FieldElement(b);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12344d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19978c(this.f12344d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] b = Nat256.m19974b();
        SecT193Field.m19467d(this.f12344d, b);
        return new SecT193FieldElement(b);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] b = Nat256.m19974b();
        SecT193Field.m19460a(this.f12344d, b);
        return new SecT193FieldElement(b);
    }

    public SecT193FieldElement() {
        this.f12344d = Nat256.m19974b();
    }

    protected SecT193FieldElement(long[] jArr) {
        this.f12344d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12344d;
        long[] jArr2 = ((SecT193FieldElement) dVar).f12344d;
        long[] jArr3 = ((SecT193FieldElement) dVar2).f12344d;
        long[] d = Nat256.m19986d();
        SecT193Field.m19473h(jArr, d);
        SecT193Field.m19470e(jArr2, jArr3, d);
        long[] b = Nat256.m19974b();
        SecT193Field.m19469e(d, b);
        return new SecT193FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
