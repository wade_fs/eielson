package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat576;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.r2 */
public class SecT571FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12352d;

    public SecT571FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 571) {
            throw new IllegalArgumentException("x value invalid for SecT571FieldElement");
        }
        this.f12352d = SecT571Field.m19547a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] a = Nat576.m20009a();
        SecT571Field.m19546a(this.f12352d, ((SecT571FieldElement) dVar).f12352d, a);
        return new SecT571FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12352d;
        long[] jArr2 = ((SecT571FieldElement) dVar).f12352d;
        long[] jArr3 = ((SecT571FieldElement) dVar2).f12352d;
        long[] jArr4 = ((SecT571FieldElement) dVar3).f12352d;
        long[] b = Nat576.m20012b();
        SecT571Field.m19561g(jArr, jArr2, b);
        SecT571Field.m19561g(jArr3, jArr4, b);
        long[] a = Nat576.m20009a();
        SecT571Field.m19554d(b, a);
        return new SecT571FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 571;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] a = Nat576.m20009a();
        SecT571Field.m19559f(this.f12352d, ((SecT571FieldElement) dVar).f12352d, a);
        return new SecT571FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat576.m20007a(this.f12352d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT571FieldElement)) {
            return false;
        }
        return Nat576.m20008a(this.f12352d, ((SecT571FieldElement) obj).f12352d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat576.m20011b(this.f12352d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] a = Nat576.m20009a();
        SecT571Field.m19556e(this.f12352d, a);
        return new SecT571FieldElement(a);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12352d, 0, 9) ^ 5711052;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] a = Nat576.m20009a();
        SecT571Field.m19558f(this.f12352d, a);
        return new SecT571FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12352d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat576.m20013c(this.f12352d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] a = Nat576.m20009a();
        SecT571Field.m19552c(this.f12352d, a);
        return new SecT571FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] a = Nat576.m20009a();
        SecT571Field.m19545a(this.f12352d, a);
        return new SecT571FieldElement(a);
    }

    public SecT571FieldElement() {
        this.f12352d = Nat576.m20009a();
    }

    protected SecT571FieldElement(long[] jArr) {
        this.f12352d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12352d;
        long[] jArr2 = ((SecT571FieldElement) dVar).f12352d;
        long[] jArr3 = ((SecT571FieldElement) dVar2).f12352d;
        long[] b = Nat576.m20012b();
        SecT571Field.m19560g(jArr, b);
        SecT571Field.m19561g(jArr2, jArr3, b);
        long[] a = Nat576.m20009a();
        SecT571Field.m19554d(b, a);
        return new SecT571FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
