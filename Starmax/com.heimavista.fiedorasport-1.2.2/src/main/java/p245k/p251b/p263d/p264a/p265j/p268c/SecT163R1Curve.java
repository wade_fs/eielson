package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.k1 */
public class SecT163R1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT163R1Point f12322i = new SecT163R1Point(this, null, null);

    public SecT163R1Curve() {
        super(163, 3, 6, 7);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("07B6882CAAEFA84F9554FF8428BD88E246D2782AE2")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("0713612DCDDCB40AAB946BDA29CA91F73AF958AFD9")));
        this.f12225d = new BigInteger(1, Hex.m20078a("03FFFFFFFFFFFFFFFFFFFF48AAB689C29CA710279B"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT163R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 163;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12322i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT163FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT163R1Point(this, dVar, dVar2, z);
    }
}
