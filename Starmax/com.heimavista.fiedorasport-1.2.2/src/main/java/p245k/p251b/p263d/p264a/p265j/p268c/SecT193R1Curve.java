package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.q1 */
public class SecT193R1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT193R1Point f12349i = new SecT193R1Point(this, null, null);

    public SecT193R1Curve() {
        super(193, 15, 0, 0);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("0017858FEB7A98975169E171F77B4087DE098AC8A911DF7B01")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("00FDFB49BFE6C3A89FACADAA7A1E5BBC7CC1C2E5D831478814")));
        this.f12225d = new BigInteger(1, Hex.m20078a("01000000000000000000000000C7F34A778F443ACC920EBA49"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT193R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 193;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12349i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT193FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT193R1Point(this, dVar, dVar2, z);
    }
}
