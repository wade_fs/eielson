package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.w0 */
public class SecT113R1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT113R1Point f12371i = new SecT113R1Point(this, null, null);

    public SecT113R1Curve() {
        super(113, 9, 0, 0);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("003088250CA6E7C7FE649CE85820F7")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("00E8BEE4D3E2260744188BE0E9C723")));
        this.f12225d = new BigInteger(1, Hex.m20078a("0100000000000000D9CCEC8A39E56F"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT113R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 113;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12371i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT113FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT113R1Point(this, dVar, dVar2, z);
    }
}
