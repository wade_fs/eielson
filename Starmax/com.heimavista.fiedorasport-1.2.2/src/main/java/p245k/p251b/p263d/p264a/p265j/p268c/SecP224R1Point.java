package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat224;

/* renamed from: k.b.d.a.j.c.d0 */
public class SecP224R1Point extends ECPoint.C5167b {
    public SecP224R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP224R1FieldElement c0Var = (SecP224R1FieldElement) this.f12251b;
        SecP224R1FieldElement c0Var2 = (SecP224R1FieldElement) this.f12252c;
        SecP224R1FieldElement c0Var3 = (SecP224R1FieldElement) fVar.mo28306h();
        SecP224R1FieldElement c0Var4 = (SecP224R1FieldElement) fVar.mo28308i();
        SecP224R1FieldElement c0Var5 = (SecP224R1FieldElement) this.f12253d[0];
        SecP224R1FieldElement c0Var6 = (SecP224R1FieldElement) fVar.mo28293a(0);
        int[] b = Nat224.m19944b();
        int[] a = Nat224.m19939a();
        int[] a2 = Nat224.m19939a();
        int[] a3 = Nat224.m19939a();
        boolean e = c0Var5.mo28273e();
        if (e) {
            iArr2 = c0Var3.f12287d;
            iArr = c0Var4.f12287d;
        } else {
            SecP224R1Field.m18995d(c0Var5.f12287d, a2);
            SecP224R1Field.m18992b(a2, c0Var3.f12287d, a);
            SecP224R1Field.m18992b(a2, c0Var5.f12287d, a2);
            SecP224R1Field.m18992b(a2, c0Var4.f12287d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = c0Var6.mo28273e();
        if (e2) {
            iArr4 = c0Var.f12287d;
            iArr3 = c0Var2.f12287d;
        } else {
            SecP224R1Field.m18995d(c0Var6.f12287d, a3);
            SecP224R1Field.m18992b(a3, c0Var.f12287d, b);
            SecP224R1Field.m18992b(a3, c0Var6.f12287d, a3);
            SecP224R1Field.m18992b(a3, c0Var2.f12287d, a3);
            iArr4 = b;
            iArr3 = a3;
        }
        int[] a4 = Nat224.m19939a();
        SecP224R1Field.m18996d(iArr4, iArr2, a4);
        SecP224R1Field.m18996d(iArr3, iArr, a);
        if (!Nat224.m19942b(a4)) {
            SecP224R1Field.m18995d(a4, a2);
            int[] a5 = Nat224.m19939a();
            SecP224R1Field.m18992b(a2, a4, a5);
            SecP224R1Field.m18992b(a2, iArr4, a2);
            SecP224R1Field.m18991b(a5, a5);
            Nat224.m19946c(iArr3, a5, b);
            SecP224R1Field.m18984a(Nat224.m19941b(a2, a2, a5), a5);
            SecP224R1FieldElement c0Var7 = new SecP224R1FieldElement(a3);
            SecP224R1Field.m18995d(a, c0Var7.f12287d);
            int[] iArr5 = c0Var7.f12287d;
            SecP224R1Field.m18996d(iArr5, a5, iArr5);
            SecP224R1FieldElement c0Var8 = new SecP224R1FieldElement(a5);
            SecP224R1Field.m18996d(a2, c0Var7.f12287d, c0Var8.f12287d);
            SecP224R1Field.m18994c(c0Var8.f12287d, a, b);
            SecP224R1Field.m18993c(b, c0Var8.f12287d);
            SecP224R1FieldElement c0Var9 = new SecP224R1FieldElement(a4);
            if (!e) {
                int[] iArr6 = c0Var9.f12287d;
                SecP224R1Field.m18992b(iArr6, c0Var5.f12287d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = c0Var9.f12287d;
                SecP224R1Field.m18992b(iArr7, c0Var6.f12287d, iArr7);
            }
            return new SecP224R1Point(d, c0Var7, c0Var8, new ECFieldElement[]{c0Var9}, this.f12254e);
        } else if (Nat224.m19942b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP224R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP224R1FieldElement c0Var = (SecP224R1FieldElement) this.f12252c;
        if (c0Var.mo28274f()) {
            return d.mo28258i();
        }
        SecP224R1FieldElement c0Var2 = (SecP224R1FieldElement) this.f12251b;
        SecP224R1FieldElement c0Var3 = (SecP224R1FieldElement) this.f12253d[0];
        int[] a = Nat224.m19939a();
        int[] a2 = Nat224.m19939a();
        int[] a3 = Nat224.m19939a();
        SecP224R1Field.m18995d(c0Var.f12287d, a3);
        int[] a4 = Nat224.m19939a();
        SecP224R1Field.m18995d(a3, a4);
        boolean e = c0Var3.mo28273e();
        int[] iArr = c0Var3.f12287d;
        if (!e) {
            SecP224R1Field.m18995d(iArr, a2);
            iArr = a2;
        }
        SecP224R1Field.m18996d(c0Var2.f12287d, iArr, a);
        SecP224R1Field.m18988a(c0Var2.f12287d, iArr, a2);
        SecP224R1Field.m18992b(a2, a, a2);
        SecP224R1Field.m18984a(Nat224.m19941b(a2, a2, a2), a2);
        SecP224R1Field.m18992b(a3, c0Var2.f12287d, a3);
        SecP224R1Field.m18984a(Nat.m20045c(7, a3, 2, 0), a3);
        SecP224R1Field.m18984a(Nat.m20021a(7, a4, 3, 0, a), a);
        SecP224R1FieldElement c0Var4 = new SecP224R1FieldElement(a4);
        SecP224R1Field.m18995d(a2, c0Var4.f12287d);
        int[] iArr2 = c0Var4.f12287d;
        SecP224R1Field.m18996d(iArr2, a3, iArr2);
        int[] iArr3 = c0Var4.f12287d;
        SecP224R1Field.m18996d(iArr3, a3, iArr3);
        SecP224R1FieldElement c0Var5 = new SecP224R1FieldElement(a3);
        SecP224R1Field.m18996d(a3, c0Var4.f12287d, c0Var5.f12287d);
        int[] iArr4 = c0Var5.f12287d;
        SecP224R1Field.m18992b(iArr4, a2, iArr4);
        int[] iArr5 = c0Var5.f12287d;
        SecP224R1Field.m18996d(iArr5, a, iArr5);
        SecP224R1FieldElement c0Var6 = new SecP224R1FieldElement(a2);
        SecP224R1Field.m18997e(c0Var.f12287d, c0Var6.f12287d);
        if (!e) {
            int[] iArr6 = c0Var6.f12287d;
            SecP224R1Field.m18992b(iArr6, c0Var3.f12287d, iArr6);
        }
        return new SecP224R1Point(d, c0Var4, c0Var5, new ECFieldElement[]{c0Var6}, this.f12254e);
    }

    public SecP224R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP224R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
