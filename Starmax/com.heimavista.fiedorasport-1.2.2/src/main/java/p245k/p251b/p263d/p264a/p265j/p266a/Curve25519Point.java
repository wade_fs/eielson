package p245k.p251b.p263d.p264a.p265j.p266a;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.a.d */
public class Curve25519Point extends ECPoint.C5167b {
    public Curve25519Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECFieldElement mo28293a(int i) {
        if (i == 1) {
            return mo28349r();
        }
        return super.mo28293a(i);
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new Curve25519Point(mo28301d(), this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        if (this.f12252c.mo28274f()) {
            return d.mo28258i();
        }
        return mo28348a(true);
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public Curve25519FieldElement mo28349r() {
        ECFieldElement[] dVarArr = this.f12253d;
        Curve25519FieldElement cVar = (Curve25519FieldElement) dVarArr[1];
        if (cVar != null) {
            return cVar;
        }
        Curve25519FieldElement a = mo28347a((Curve25519FieldElement) dVarArr[0], (int[]) null);
        dVarArr[1] = a;
        return a;
    }

    public Curve25519Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        Curve25519FieldElement cVar = (Curve25519FieldElement) this.f12251b;
        Curve25519FieldElement cVar2 = (Curve25519FieldElement) this.f12252c;
        Curve25519FieldElement cVar3 = (Curve25519FieldElement) this.f12253d[0];
        Curve25519FieldElement cVar4 = (Curve25519FieldElement) fVar.mo28306h();
        Curve25519FieldElement cVar5 = (Curve25519FieldElement) fVar.mo28308i();
        Curve25519FieldElement cVar6 = (Curve25519FieldElement) fVar.mo28293a(0);
        int[] c = Nat256.m19982c();
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        int[] a3 = Nat256.m19966a();
        boolean e = cVar3.mo28273e();
        if (e) {
            iArr2 = cVar4.f12264d;
            iArr = cVar5.f12264d;
        } else {
            Curve25519Field.m18862d(cVar3.f12264d, a2);
            Curve25519Field.m18858b(a2, cVar4.f12264d, a);
            Curve25519Field.m18858b(a2, cVar3.f12264d, a2);
            Curve25519Field.m18858b(a2, cVar5.f12264d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = cVar6.mo28273e();
        if (e2) {
            iArr4 = cVar.f12264d;
            iArr3 = cVar2.f12264d;
        } else {
            Curve25519Field.m18862d(cVar6.f12264d, a3);
            Curve25519Field.m18858b(a3, cVar.f12264d, c);
            Curve25519Field.m18858b(a3, cVar6.f12264d, a3);
            Curve25519Field.m18858b(a3, cVar2.f12264d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat256.m19966a();
        Curve25519Field.m18863d(iArr4, iArr2, a4);
        Curve25519Field.m18863d(iArr3, iArr, a);
        if (!Nat256.m19970b(a4)) {
            int[] a5 = Nat256.m19966a();
            Curve25519Field.m18862d(a4, a5);
            int[] a6 = Nat256.m19966a();
            Curve25519Field.m18858b(a5, a4, a6);
            Curve25519Field.m18858b(a5, iArr4, a2);
            Curve25519Field.m18857b(a6, a6);
            Nat256.m19980c(iArr3, a6, c);
            Curve25519Field.m18851a(Nat256.m19968b(a2, a2, a6), a6);
            Curve25519FieldElement cVar7 = new Curve25519FieldElement(a3);
            Curve25519Field.m18862d(a, cVar7.f12264d);
            int[] iArr5 = cVar7.f12264d;
            Curve25519Field.m18863d(iArr5, a6, iArr5);
            Curve25519FieldElement cVar8 = new Curve25519FieldElement(a6);
            Curve25519Field.m18863d(a2, cVar7.f12264d, cVar8.f12264d);
            Curve25519Field.m18861c(cVar8.f12264d, a, c);
            Curve25519Field.m18860c(c, cVar8.f12264d);
            Curve25519FieldElement cVar9 = new Curve25519FieldElement(a4);
            if (!e) {
                int[] iArr6 = cVar9.f12264d;
                Curve25519Field.m18858b(iArr6, cVar3.f12264d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = cVar9.f12264d;
                Curve25519Field.m18858b(iArr7, cVar6.f12264d, iArr7);
            }
            if (!e || !e2) {
                a5 = null;
            }
            return new Curve25519Point(d, cVar7, cVar8, new ECFieldElement[]{cVar9, mo28347a(cVar9, a5)}, this.f12254e);
        } else if (Nat256.m19970b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    Curve25519Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Curve25519FieldElement mo28347a(Curve25519FieldElement cVar, int[] iArr) {
        Curve25519FieldElement cVar2 = (Curve25519FieldElement) mo28301d().mo28250c();
        if (cVar.mo28273e()) {
            return cVar2;
        }
        Curve25519FieldElement cVar3 = new Curve25519FieldElement();
        if (iArr == null) {
            iArr = cVar3.f12264d;
            Curve25519Field.m18862d(cVar.f12264d, iArr);
        }
        Curve25519Field.m18862d(iArr, cVar3.f12264d);
        int[] iArr2 = cVar3.f12264d;
        Curve25519Field.m18858b(iArr2, cVar2.f12264d, iArr2);
        return cVar3;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Curve25519Point mo28348a(boolean z) {
        Curve25519FieldElement cVar = (Curve25519FieldElement) this.f12251b;
        Curve25519FieldElement cVar2 = (Curve25519FieldElement) this.f12252c;
        Curve25519FieldElement cVar3 = (Curve25519FieldElement) this.f12253d[0];
        Curve25519FieldElement r = mo28349r();
        int[] a = Nat256.m19966a();
        Curve25519Field.m18862d(cVar.f12264d, a);
        Curve25519Field.m18851a(Nat256.m19968b(a, a, a) + Nat256.m19959a(r.f12264d, a), a);
        int[] a2 = Nat256.m19966a();
        Curve25519Field.m18864e(cVar2.f12264d, a2);
        int[] a3 = Nat256.m19966a();
        Curve25519Field.m18858b(a2, cVar2.f12264d, a3);
        int[] a4 = Nat256.m19966a();
        Curve25519Field.m18858b(a3, cVar.f12264d, a4);
        Curve25519Field.m18864e(a4, a4);
        int[] a5 = Nat256.m19966a();
        Curve25519Field.m18862d(a3, a5);
        Curve25519Field.m18864e(a5, a5);
        Curve25519FieldElement cVar4 = new Curve25519FieldElement(a3);
        Curve25519Field.m18862d(a, cVar4.f12264d);
        int[] iArr = cVar4.f12264d;
        Curve25519Field.m18863d(iArr, a4, iArr);
        int[] iArr2 = cVar4.f12264d;
        Curve25519Field.m18863d(iArr2, a4, iArr2);
        Curve25519FieldElement cVar5 = new Curve25519FieldElement(a4);
        Curve25519Field.m18863d(a4, cVar4.f12264d, cVar5.f12264d);
        int[] iArr3 = cVar5.f12264d;
        Curve25519Field.m18858b(iArr3, a, iArr3);
        int[] iArr4 = cVar5.f12264d;
        Curve25519Field.m18863d(iArr4, a5, iArr4);
        Curve25519FieldElement cVar6 = new Curve25519FieldElement(a2);
        if (!Nat256.m19962a(cVar3.f12264d)) {
            int[] iArr5 = cVar6.f12264d;
            Curve25519Field.m18858b(iArr5, cVar3.f12264d, iArr5);
        }
        Curve25519FieldElement cVar7 = null;
        if (z) {
            cVar7 = new Curve25519FieldElement(a5);
            int[] iArr6 = cVar7.f12264d;
            Curve25519Field.m18858b(iArr6, r.f12264d, iArr6);
            int[] iArr7 = cVar7.f12264d;
            Curve25519Field.m18864e(iArr7, iArr7);
        }
        return new Curve25519Point(mo28301d(), cVar4, cVar5, new ECFieldElement[]{cVar6, cVar7}, this.f12254e);
    }
}
