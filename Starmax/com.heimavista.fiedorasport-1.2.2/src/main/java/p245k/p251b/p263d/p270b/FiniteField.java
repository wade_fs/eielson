package p245k.p251b.p263d.p270b;

import java.math.BigInteger;

/* renamed from: k.b.d.b.b */
public interface FiniteField {
    /* renamed from: b */
    int mo28393b();

    /* renamed from: c */
    BigInteger mo28394c();
}
