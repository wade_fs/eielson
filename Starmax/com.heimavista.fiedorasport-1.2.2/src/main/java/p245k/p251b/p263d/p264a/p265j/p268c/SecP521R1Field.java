package p245k.p251b.p263d.p264a.p265j.p268c;

import androidx.core.app.FrameMetricsAggregator;
import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat512;

/* renamed from: k.b.d.a.j.c.r0 */
public class SecP521R1Field {

    /* renamed from: a */
    static final int[] f12351a = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, FrameMetricsAggregator.EVERY_DURATION};

    /* renamed from: a */
    public static void m19569a(int[] iArr, int[] iArr2, int[] iArr3) {
        int a = Nat.m20026a(16, iArr, iArr2, iArr3) + iArr[16] + iArr2[16];
        if (a > 511 || (a == 511 && Nat.m20040b(16, iArr3, f12351a))) {
            a = (a + Nat.m20043c(16, iArr3)) & FrameMetricsAggregator.EVERY_DURATION;
        }
        iArr3[16] = a;
    }

    /* renamed from: b */
    protected static void m19572b(int[] iArr, int[] iArr2, int[] iArr3) {
        Nat512.m20006a(iArr, iArr2, iArr3);
        int i = iArr[16];
        int i2 = iArr2[16];
        iArr3[32] = Nat.m20017a(16, i, iArr2, i2, iArr, iArr3, 16) + (i * i2);
    }

    /* renamed from: c */
    public static void m19574c(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] a = Nat.m20031a(33);
        m19572b(iArr, iArr2, a);
        m19575d(a, iArr3);
    }

    /* renamed from: d */
    public static void m19575d(int[] iArr, int[] iArr2) {
        int i = iArr[32];
        int a = (Nat.m20020a(16, iArr, 16, 9, i, iArr2, 0) >>> 23) + (i >>> 9) + Nat.m20025a(16, iArr, iArr2);
        if (a > 511 || (a == 511 && Nat.m20040b(16, iArr2, f12351a))) {
            a = (a + Nat.m20043c(16, iArr2)) & FrameMetricsAggregator.EVERY_DURATION;
        }
        iArr2[16] = a;
    }

    /* renamed from: e */
    public static void m19577e(int[] iArr, int[] iArr2) {
        int[] a = Nat.m20031a(33);
        m19571b(iArr, a);
        m19575d(a, iArr2);
    }

    /* renamed from: f */
    public static void m19578f(int[] iArr, int[] iArr2) {
        int i = iArr[16];
        iArr2[16] = (Nat.m20023a(16, iArr, i << 23, iArr2) | (i << 1)) & FrameMetricsAggregator.EVERY_DURATION;
    }

    /* renamed from: b */
    protected static void m19571b(int[] iArr, int[] iArr2) {
        Nat512.m20005a(iArr, iArr2);
        int i = iArr[16];
        iArr2[32] = Nat.m20016a(16, i << 1, iArr, 0, iArr2, 16) + (i * i);
    }

    /* renamed from: c */
    public static void m19573c(int[] iArr, int[] iArr2) {
        if (Nat.m20053e(17, iArr)) {
            Nat.m20055g(17, iArr2);
        } else {
            Nat.m20046c(17, f12351a, iArr, iArr2);
        }
    }

    /* renamed from: a */
    public static void m19568a(int[] iArr, int[] iArr2) {
        int d = Nat.m20050d(16, iArr, iArr2) + iArr[16];
        if (d > 511 || (d == 511 && Nat.m20040b(16, iArr2, f12351a))) {
            d = (d + Nat.m20043c(16, iArr2)) & FrameMetricsAggregator.EVERY_DURATION;
        }
        iArr2[16] = d;
    }

    /* renamed from: d */
    public static void m19576d(int[] iArr, int[] iArr2, int[] iArr3) {
        int c = (Nat.m20046c(16, iArr, iArr2, iArr3) + iArr[16]) - iArr2[16];
        if (c < 0) {
            c = (c + Nat.m20035b(16, iArr3)) & FrameMetricsAggregator.EVERY_DURATION;
        }
        iArr3[16] = c;
    }

    /* renamed from: a */
    public static int[] m19570a(BigInteger bigInteger) {
        int[] a = Nat.m20032a(521, bigInteger);
        if (Nat.m20040b(17, a, f12351a)) {
            Nat.m20055g(17, a);
        }
        return a;
    }

    /* renamed from: a */
    public static void m19566a(int[] iArr) {
        int i = iArr[16];
        int b = Nat.m20034b(16, i >>> 9, iArr) + (i & FrameMetricsAggregator.EVERY_DURATION);
        if (b > 511 || (b == 511 && Nat.m20040b(16, iArr, f12351a))) {
            b = (b + Nat.m20043c(16, iArr)) & FrameMetricsAggregator.EVERY_DURATION;
        }
        iArr[16] = b;
    }

    /* renamed from: a */
    public static void m19567a(int[] iArr, int i, int[] iArr2) {
        int[] a = Nat.m20031a(33);
        m19571b(iArr, a);
        m19575d(a, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                m19571b(iArr2, a);
                m19575d(a, iArr2);
            } else {
                return;
            }
        }
    }
}
