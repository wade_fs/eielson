package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.e1 */
public class SecT131R2Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT131R2Point f12294i = new SecT131R2Point(this, null, null);

    public SecT131R2Curve() {
        super(131, 2, 3, 8);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("03E5A88919D7CAFCBF415F07C2176573B2")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("04B8266A46C55657AC734CE38F018F2192")));
        this.f12225d = new BigInteger(1, Hex.m20078a("0400000000000000016954A233049BA98F"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT131R2Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 131;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12294i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT131FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT131R2Point(this, dVar, dVar2, z);
    }
}
