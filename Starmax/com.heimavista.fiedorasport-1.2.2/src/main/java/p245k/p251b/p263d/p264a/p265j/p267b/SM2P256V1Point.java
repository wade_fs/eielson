package p245k.p251b.p263d.p264a.p265j.p267b;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.b.d */
public class SM2P256V1Point extends ECPoint.C5167b {
    public SM2P256V1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SM2P256V1FieldElement cVar = (SM2P256V1FieldElement) this.f12251b;
        SM2P256V1FieldElement cVar2 = (SM2P256V1FieldElement) this.f12252c;
        SM2P256V1FieldElement cVar3 = (SM2P256V1FieldElement) fVar.mo28306h();
        SM2P256V1FieldElement cVar4 = (SM2P256V1FieldElement) fVar.mo28308i();
        SM2P256V1FieldElement cVar5 = (SM2P256V1FieldElement) this.f12253d[0];
        SM2P256V1FieldElement cVar6 = (SM2P256V1FieldElement) fVar.mo28293a(0);
        int[] c = Nat256.m19982c();
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        int[] a3 = Nat256.m19966a();
        boolean e = cVar5.mo28273e();
        if (e) {
            iArr2 = cVar3.f12270d;
            iArr = cVar4.f12270d;
        } else {
            SM2P256V1Field.m18903d(cVar5.f12270d, a2);
            SM2P256V1Field.m18900b(a2, cVar3.f12270d, a);
            SM2P256V1Field.m18900b(a2, cVar5.f12270d, a2);
            SM2P256V1Field.m18900b(a2, cVar4.f12270d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = cVar6.mo28273e();
        if (e2) {
            iArr4 = cVar.f12270d;
            iArr3 = cVar2.f12270d;
        } else {
            SM2P256V1Field.m18903d(cVar6.f12270d, a3);
            SM2P256V1Field.m18900b(a3, cVar.f12270d, c);
            SM2P256V1Field.m18900b(a3, cVar6.f12270d, a3);
            SM2P256V1Field.m18900b(a3, cVar2.f12270d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat256.m19966a();
        SM2P256V1Field.m18904d(iArr4, iArr2, a4);
        SM2P256V1Field.m18904d(iArr3, iArr, a);
        if (!Nat256.m19970b(a4)) {
            SM2P256V1Field.m18903d(a4, a2);
            int[] a5 = Nat256.m19966a();
            SM2P256V1Field.m18900b(a2, a4, a5);
            SM2P256V1Field.m18900b(a2, iArr4, a2);
            SM2P256V1Field.m18899b(a5, a5);
            Nat256.m19980c(iArr3, a5, c);
            SM2P256V1Field.m18892a(Nat256.m19968b(a2, a2, a5), a5);
            SM2P256V1FieldElement cVar7 = new SM2P256V1FieldElement(a3);
            SM2P256V1Field.m18903d(a, cVar7.f12270d);
            int[] iArr5 = cVar7.f12270d;
            SM2P256V1Field.m18904d(iArr5, a5, iArr5);
            SM2P256V1FieldElement cVar8 = new SM2P256V1FieldElement(a5);
            SM2P256V1Field.m18904d(a2, cVar7.f12270d, cVar8.f12270d);
            SM2P256V1Field.m18902c(cVar8.f12270d, a, c);
            SM2P256V1Field.m18901c(c, cVar8.f12270d);
            SM2P256V1FieldElement cVar9 = new SM2P256V1FieldElement(a4);
            if (!e) {
                int[] iArr6 = cVar9.f12270d;
                SM2P256V1Field.m18900b(iArr6, cVar5.f12270d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = cVar9.f12270d;
                SM2P256V1Field.m18900b(iArr7, cVar6.f12270d, iArr7);
            }
            return new SM2P256V1Point(d, cVar7, cVar8, new ECFieldElement[]{cVar9}, this.f12254e);
        } else if (Nat256.m19970b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SM2P256V1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SM2P256V1FieldElement cVar = (SM2P256V1FieldElement) this.f12252c;
        if (cVar.mo28274f()) {
            return d.mo28258i();
        }
        SM2P256V1FieldElement cVar2 = (SM2P256V1FieldElement) this.f12251b;
        SM2P256V1FieldElement cVar3 = (SM2P256V1FieldElement) this.f12253d[0];
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        int[] a3 = Nat256.m19966a();
        SM2P256V1Field.m18903d(cVar.f12270d, a3);
        int[] a4 = Nat256.m19966a();
        SM2P256V1Field.m18903d(a3, a4);
        boolean e = cVar3.mo28273e();
        int[] iArr = cVar3.f12270d;
        if (!e) {
            SM2P256V1Field.m18903d(iArr, a2);
            iArr = a2;
        }
        SM2P256V1Field.m18904d(cVar2.f12270d, iArr, a);
        SM2P256V1Field.m18896a(cVar2.f12270d, iArr, a2);
        SM2P256V1Field.m18900b(a2, a, a2);
        SM2P256V1Field.m18892a(Nat256.m19968b(a2, a2, a2), a2);
        SM2P256V1Field.m18900b(a3, cVar2.f12270d, a3);
        SM2P256V1Field.m18892a(Nat.m20045c(8, a3, 2, 0), a3);
        SM2P256V1Field.m18892a(Nat.m20021a(8, a4, 3, 0, a), a);
        SM2P256V1FieldElement cVar4 = new SM2P256V1FieldElement(a4);
        SM2P256V1Field.m18903d(a2, cVar4.f12270d);
        int[] iArr2 = cVar4.f12270d;
        SM2P256V1Field.m18904d(iArr2, a3, iArr2);
        int[] iArr3 = cVar4.f12270d;
        SM2P256V1Field.m18904d(iArr3, a3, iArr3);
        SM2P256V1FieldElement cVar5 = new SM2P256V1FieldElement(a3);
        SM2P256V1Field.m18904d(a3, cVar4.f12270d, cVar5.f12270d);
        int[] iArr4 = cVar5.f12270d;
        SM2P256V1Field.m18900b(iArr4, a2, iArr4);
        int[] iArr5 = cVar5.f12270d;
        SM2P256V1Field.m18904d(iArr5, a, iArr5);
        SM2P256V1FieldElement cVar6 = new SM2P256V1FieldElement(a2);
        SM2P256V1Field.m18905e(cVar.f12270d, cVar6.f12270d);
        if (!e) {
            int[] iArr6 = cVar6.f12270d;
            SM2P256V1Field.m18900b(iArr6, cVar3.f12270d, iArr6);
        }
        return new SM2P256V1Point(d, cVar4, cVar5, new ECFieldElement[]{cVar6}, this.f12254e);
    }

    public SM2P256V1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SM2P256V1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
