package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat192;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.h1 */
public class SecT163FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12309d;

    public SecT163FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 163) {
            throw new IllegalArgumentException("x value invalid for SecT163FieldElement");
        }
        this.f12309d = SecT163Field.m19189a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] b = Nat192.m19918b();
        SecT163Field.m19188a(this.f12309d, ((SecT163FieldElement) dVar).f12309d, b);
        return new SecT163FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12309d;
        long[] jArr2 = ((SecT163FieldElement) dVar).f12309d;
        long[] jArr3 = ((SecT163FieldElement) dVar2).f12309d;
        long[] jArr4 = ((SecT163FieldElement) dVar3).f12309d;
        long[] d = Nat192.m19930d();
        SecT163Field.m19197e(jArr, jArr2, d);
        SecT163Field.m19197e(jArr3, jArr4, d);
        long[] b = Nat192.m19918b();
        SecT163Field.m19194d(d, b);
        return new SecT163FieldElement(b);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 163;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] b = Nat192.m19918b();
        SecT163Field.m19195d(this.f12309d, ((SecT163FieldElement) dVar).f12309d, b);
        return new SecT163FieldElement(b);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat192.m19908a(this.f12309d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT163FieldElement)) {
            return false;
        }
        return Nat192.m19909a(this.f12309d, ((SecT163FieldElement) obj).f12309d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat192.m19917b(this.f12309d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] b = Nat192.m19918b();
        SecT163Field.m19196e(this.f12309d, b);
        return new SecT163FieldElement(b);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12309d, 0, 3) ^ 163763;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] b = Nat192.m19918b();
        SecT163Field.m19198f(this.f12309d, b);
        return new SecT163FieldElement(b);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12309d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat192.m19922c(this.f12309d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] b = Nat192.m19918b();
        SecT163Field.m19192c(this.f12309d, b);
        return new SecT163FieldElement(b);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] b = Nat192.m19918b();
        SecT163Field.m19187a(this.f12309d, b);
        return new SecT163FieldElement(b);
    }

    public SecT163FieldElement() {
        this.f12309d = Nat192.m19918b();
    }

    protected SecT163FieldElement(long[] jArr) {
        this.f12309d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12309d;
        long[] jArr2 = ((SecT163FieldElement) dVar).f12309d;
        long[] jArr3 = ((SecT163FieldElement) dVar2).f12309d;
        long[] d = Nat192.m19930d();
        SecT163Field.m19199g(jArr, d);
        SecT163Field.m19197e(jArr2, jArr3, d);
        long[] b = Nat192.m19918b();
        SecT163Field.m19194d(d, b);
        return new SecT163FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
