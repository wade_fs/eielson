package p245k.p251b.p263d.p264a.p265j.p266a;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat256;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.a.c */
public class Curve25519FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12262e = Curve25519.f12258j;

    /* renamed from: f */
    private static final int[] f12263f = {1242472624, -991028441, -1389370248, 792926214, 1039914919, 726466713, 1338105611, 730014848};

    /* renamed from: d */
    protected int[] f12264d;

    public Curve25519FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12262e) >= 0) {
            throw new IllegalArgumentException("x value invalid for Curve25519FieldElement");
        }
        this.f12264d = Curve25519Field.m18855a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Curve25519Field.m18854a(this.f12264d, ((Curve25519FieldElement) dVar).f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Mod.m19851a(Curve25519Field.f12260a, ((Curve25519FieldElement) dVar).f12264d, a);
        Curve25519Field.m18858b(a, this.f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12262e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Curve25519Field.m18863d(this.f12264d, ((Curve25519FieldElement) dVar).f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat256.m19962a(this.f12264d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Curve25519FieldElement)) {
            return false;
        }
        return Nat256.m19972b(this.f12264d, ((Curve25519FieldElement) obj).f12264d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat256.m19970b(this.f12264d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat256.m19966a();
        Curve25519Field.m18857b(this.f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12264d;
        if (Nat256.m19970b(iArr) || Nat256.m19962a(iArr)) {
            return super;
        }
        int[] a = Nat256.m19966a();
        Curve25519Field.m18862d(iArr, a);
        Curve25519Field.m18858b(a, iArr, a);
        Curve25519Field.m18862d(a, a);
        Curve25519Field.m18858b(a, iArr, a);
        int[] a2 = Nat256.m19966a();
        Curve25519Field.m18862d(a, a2);
        Curve25519Field.m18858b(a2, iArr, a2);
        int[] a3 = Nat256.m19966a();
        Curve25519Field.m18852a(a2, 3, a3);
        Curve25519Field.m18858b(a3, a, a3);
        Curve25519Field.m18852a(a3, 4, a);
        Curve25519Field.m18858b(a, a2, a);
        Curve25519Field.m18852a(a, 4, a3);
        Curve25519Field.m18858b(a3, a2, a3);
        Curve25519Field.m18852a(a3, 15, a2);
        Curve25519Field.m18858b(a2, a3, a2);
        Curve25519Field.m18852a(a2, 30, a3);
        Curve25519Field.m18858b(a3, a2, a3);
        Curve25519Field.m18852a(a3, 60, a2);
        Curve25519Field.m18858b(a2, a3, a2);
        Curve25519Field.m18852a(a2, 11, a3);
        Curve25519Field.m18858b(a3, a, a3);
        Curve25519Field.m18852a(a3, 120, a);
        Curve25519Field.m18858b(a, a2, a);
        Curve25519Field.m18862d(a, a);
        Curve25519Field.m18862d(a, a2);
        if (Nat256.m19972b(iArr, a2)) {
            return new Curve25519FieldElement(a);
        }
        Curve25519Field.m18858b(a, f12263f, a);
        Curve25519Field.m18862d(a, a2);
        if (Nat256.m19972b(iArr, a2)) {
            return new Curve25519FieldElement(a);
        }
        return null;
    }

    public int hashCode() {
        return f12262e.hashCode() ^ Arrays.m20066b(this.f12264d, 0, 8);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat256.m19966a();
        Curve25519Field.m18862d(this.f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat256.m19956a(this.f12264d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat256.m19977c(this.f12264d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat256.m19966a();
        Curve25519Field.m18858b(this.f12264d, ((Curve25519FieldElement) dVar).f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat256.m19966a();
        Curve25519Field.m18853a(this.f12264d, a);
        return new Curve25519FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat256.m19966a();
        Mod.m19851a(Curve25519Field.f12260a, this.f12264d, a);
        return new Curve25519FieldElement(a);
    }

    public Curve25519FieldElement() {
        this.f12264d = Nat256.m19966a();
    }

    protected Curve25519FieldElement(int[] iArr) {
        this.f12264d = iArr;
    }
}
