package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat192;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.q */
public class SecP192K1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12345e = SecP192K1Curve.f12336j;

    /* renamed from: d */
    protected int[] f12346d;

    public SecP192K1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12345e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP192K1FieldElement");
        }
        this.f12346d = SecP192K1Field.m19484a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19483a(this.f12346d, ((SecP192K1FieldElement) dVar).f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        Mod.m19851a(SecP192K1Field.f12341a, ((SecP192K1FieldElement) dVar).f12346d, a);
        SecP192K1Field.m19486b(a, this.f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12345e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19490d(this.f12346d, ((SecP192K1FieldElement) dVar).f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat192.m19905a(this.f12346d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP192K1FieldElement)) {
            return false;
        }
        return Nat192.m19907a(this.f12346d, ((SecP192K1FieldElement) obj).f12346d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat192.m19914b(this.f12346d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19485b(this.f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12346d;
        if (Nat192.m19914b(iArr) || Nat192.m19905a(iArr)) {
            return super;
        }
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19489d(iArr, a);
        SecP192K1Field.m19486b(a, iArr, a);
        int[] a2 = Nat192.m19910a();
        SecP192K1Field.m19489d(a, a2);
        SecP192K1Field.m19486b(a2, iArr, a2);
        int[] a3 = Nat192.m19910a();
        SecP192K1Field.m19481a(a2, 3, a3);
        SecP192K1Field.m19486b(a3, a2, a3);
        SecP192K1Field.m19481a(a3, 2, a3);
        SecP192K1Field.m19486b(a3, a, a3);
        SecP192K1Field.m19481a(a3, 8, a);
        SecP192K1Field.m19486b(a, a3, a);
        SecP192K1Field.m19481a(a, 3, a3);
        SecP192K1Field.m19486b(a3, a2, a3);
        int[] a4 = Nat192.m19910a();
        SecP192K1Field.m19481a(a3, 16, a4);
        SecP192K1Field.m19486b(a4, a, a4);
        SecP192K1Field.m19481a(a4, 35, a);
        SecP192K1Field.m19486b(a, a4, a);
        SecP192K1Field.m19481a(a, 70, a4);
        SecP192K1Field.m19486b(a4, a, a4);
        SecP192K1Field.m19481a(a4, 19, a);
        SecP192K1Field.m19486b(a, a3, a);
        SecP192K1Field.m19481a(a, 20, a);
        SecP192K1Field.m19486b(a, a3, a);
        SecP192K1Field.m19481a(a, 4, a);
        SecP192K1Field.m19486b(a, a2, a);
        SecP192K1Field.m19481a(a, 6, a);
        SecP192K1Field.m19486b(a, a2, a);
        SecP192K1Field.m19489d(a, a);
        SecP192K1Field.m19489d(a, a2);
        if (Nat192.m19907a(iArr, a2)) {
            return new SecP192K1FieldElement(a);
        }
        return null;
    }

    public int hashCode() {
        return f12345e.hashCode() ^ Arrays.m20066b(this.f12346d, 0, 6);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19489d(this.f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat192.m19900a(this.f12346d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat192.m19921c(this.f12346d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19486b(this.f12346d, ((SecP192K1FieldElement) dVar).f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19482a(this.f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat192.m19910a();
        Mod.m19851a(SecP192K1Field.f12341a, this.f12346d, a);
        return new SecP192K1FieldElement(a);
    }

    public SecP192K1FieldElement() {
        this.f12346d = Nat192.m19910a();
    }

    protected SecP192K1FieldElement(int[] iArr) {
        this.f12346d = iArr;
    }
}
