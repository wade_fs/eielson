package p245k.p251b.p263d.p271c;

import java.math.BigInteger;
import p245k.p251b.p272e.Pack;

/* renamed from: k.b.d.c.j */
public abstract class Nat448 {
    /* renamed from: a */
    public static boolean m19999a(long[] jArr, long[] jArr2) {
        for (int i = 6; i >= 0; i--) {
            if (jArr[i] != jArr2[i]) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public static long[] m20000a() {
        return new long[7];
    }

    /* renamed from: b */
    public static boolean m20002b(long[] jArr) {
        for (int i = 0; i < 7; i++) {
            if (jArr[i] != 0) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    public static long[] m20003b() {
        return new long[14];
    }

    /* renamed from: c */
    public static BigInteger m20004c(long[] jArr) {
        byte[] bArr = new byte[56];
        for (int i = 0; i < 7; i++) {
            long j = jArr[i];
            if (j != 0) {
                Pack.m20071a(j, bArr, (6 - i) << 3);
            }
        }
        return new BigInteger(1, bArr);
    }

    /* renamed from: a */
    public static long[] m20001a(BigInteger bigInteger) {
        if (bigInteger.signum() < 0 || bigInteger.bitLength() > 448) {
            throw new IllegalArgumentException();
        }
        long[] a = m20000a();
        int i = 0;
        while (bigInteger.signum() != 0) {
            a[i] = bigInteger.longValue();
            bigInteger = bigInteger.shiftRight(64);
            i++;
        }
        return a;
    }

    /* renamed from: a */
    public static boolean m19998a(long[] jArr) {
        if (jArr[0] != 1) {
            return false;
        }
        for (int i = 1; i < 7; i++) {
            if (jArr[i] != 0) {
                return false;
            }
        }
        return true;
    }
}
