package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.c1 */
public class SecT131R1Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT131R1Point f12288i = new SecT131R1Point(this, null, null);

    public SecT131R1Curve() {
        super(131, 2, 3, 8);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("07A11B09A76B562144418FF3FF8C2570B8")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("0217C05610884B63B9C6C7291678F9D341")));
        this.f12225d = new BigInteger(1, Hex.m20078a("0400000000000000023123953A9464B54D"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT131R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 131;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12288i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT131FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT131R1Point(this, dVar, dVar2, z);
    }
}
