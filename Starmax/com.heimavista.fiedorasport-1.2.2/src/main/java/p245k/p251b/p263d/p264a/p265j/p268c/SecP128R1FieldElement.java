package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat128;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.c */
public class SecP128R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12284e = SecP128R1Curve.f12271j;

    /* renamed from: d */
    protected int[] f12285d;

    public SecP128R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12284e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP128R1FieldElement");
        }
        this.f12285d = SecP128R1Field.m18975a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18974a(this.f12285d, ((SecP128R1FieldElement) dVar).f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat128.m19859a();
        Mod.m19851a(SecP128R1Field.f12276a, ((SecP128R1FieldElement) dVar).f12285d, a);
        SecP128R1Field.m18978b(a, this.f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12284e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18982d(this.f12285d, ((SecP128R1FieldElement) dVar).f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat128.m19855a(this.f12285d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP128R1FieldElement)) {
            return false;
        }
        return Nat128.m19856a(this.f12285d, ((SecP128R1FieldElement) obj).f12285d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat128.m19862b(this.f12285d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18977b(this.f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12285d;
        if (Nat128.m19862b(iArr) || Nat128.m19855a(iArr)) {
            return super;
        }
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18981d(iArr, a);
        SecP128R1Field.m18978b(a, iArr, a);
        int[] a2 = Nat128.m19859a();
        SecP128R1Field.m18972a(a, 2, a2);
        SecP128R1Field.m18978b(a2, a, a2);
        int[] a3 = Nat128.m19859a();
        SecP128R1Field.m18972a(a2, 4, a3);
        SecP128R1Field.m18978b(a3, a2, a3);
        SecP128R1Field.m18972a(a3, 2, a2);
        SecP128R1Field.m18978b(a2, a, a2);
        SecP128R1Field.m18972a(a2, 10, a);
        SecP128R1Field.m18978b(a, a2, a);
        SecP128R1Field.m18972a(a, 10, a3);
        SecP128R1Field.m18978b(a3, a2, a3);
        SecP128R1Field.m18981d(a3, a2);
        SecP128R1Field.m18978b(a2, iArr, a2);
        SecP128R1Field.m18972a(a2, 95, a2);
        SecP128R1Field.m18981d(a2, a3);
        if (Nat128.m19856a(iArr, a3)) {
            return new SecP128R1FieldElement(a2);
        }
        return null;
    }

    public int hashCode() {
        return f12284e.hashCode() ^ Arrays.m20066b(this.f12285d, 0, 4);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18981d(this.f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat128.m19853a(this.f12285d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat128.m19867c(this.f12285d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18978b(this.f12285d, ((SecP128R1FieldElement) dVar).f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat128.m19859a();
        SecP128R1Field.m18973a(this.f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat128.m19859a();
        Mod.m19851a(SecP128R1Field.f12276a, this.f12285d, a);
        return new SecP128R1FieldElement(a);
    }

    public SecP128R1FieldElement() {
        this.f12285d = Nat128.m19859a();
    }

    protected SecP128R1FieldElement(int[] iArr) {
        this.f12285d = iArr;
    }
}
