package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat192;

/* renamed from: k.b.d.a.j.c.r */
public class SecP192K1Point extends ECPoint.C5167b {
    public SecP192K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP192K1FieldElement qVar = (SecP192K1FieldElement) this.f12251b;
        SecP192K1FieldElement qVar2 = (SecP192K1FieldElement) this.f12252c;
        SecP192K1FieldElement qVar3 = (SecP192K1FieldElement) fVar.mo28306h();
        SecP192K1FieldElement qVar4 = (SecP192K1FieldElement) fVar.mo28308i();
        SecP192K1FieldElement qVar5 = (SecP192K1FieldElement) this.f12253d[0];
        SecP192K1FieldElement qVar6 = (SecP192K1FieldElement) fVar.mo28293a(0);
        int[] c = Nat192.m19926c();
        int[] a = Nat192.m19910a();
        int[] a2 = Nat192.m19910a();
        int[] a3 = Nat192.m19910a();
        boolean e = qVar5.mo28273e();
        if (e) {
            iArr2 = qVar3.f12346d;
            iArr = qVar4.f12346d;
        } else {
            SecP192K1Field.m19489d(qVar5.f12346d, a2);
            SecP192K1Field.m19486b(a2, qVar3.f12346d, a);
            SecP192K1Field.m19486b(a2, qVar5.f12346d, a2);
            SecP192K1Field.m19486b(a2, qVar4.f12346d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = qVar6.mo28273e();
        if (e2) {
            iArr4 = qVar.f12346d;
            iArr3 = qVar2.f12346d;
        } else {
            SecP192K1Field.m19489d(qVar6.f12346d, a3);
            SecP192K1Field.m19486b(a3, qVar.f12346d, c);
            SecP192K1Field.m19486b(a3, qVar6.f12346d, a3);
            SecP192K1Field.m19486b(a3, qVar2.f12346d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat192.m19910a();
        SecP192K1Field.m19490d(iArr4, iArr2, a4);
        SecP192K1Field.m19490d(iArr3, iArr, a);
        if (!Nat192.m19914b(a4)) {
            SecP192K1Field.m19489d(a4, a2);
            int[] a5 = Nat192.m19910a();
            SecP192K1Field.m19486b(a2, a4, a5);
            SecP192K1Field.m19486b(a2, iArr4, a2);
            SecP192K1Field.m19485b(a5, a5);
            Nat192.m19925c(iArr3, a5, c);
            SecP192K1Field.m19480a(Nat192.m19912b(a2, a2, a5), a5);
            SecP192K1FieldElement qVar7 = new SecP192K1FieldElement(a3);
            SecP192K1Field.m19489d(a, qVar7.f12346d);
            int[] iArr5 = qVar7.f12346d;
            SecP192K1Field.m19490d(iArr5, a5, iArr5);
            SecP192K1FieldElement qVar8 = new SecP192K1FieldElement(a5);
            SecP192K1Field.m19490d(a2, qVar7.f12346d, qVar8.f12346d);
            SecP192K1Field.m19488c(qVar8.f12346d, a, c);
            SecP192K1Field.m19487c(c, qVar8.f12346d);
            SecP192K1FieldElement qVar9 = new SecP192K1FieldElement(a4);
            if (!e) {
                int[] iArr6 = qVar9.f12346d;
                SecP192K1Field.m19486b(iArr6, qVar5.f12346d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = qVar9.f12346d;
                SecP192K1Field.m19486b(iArr7, qVar6.f12346d, iArr7);
            }
            return new SecP192K1Point(d, qVar7, qVar8, new ECFieldElement[]{qVar9}, this.f12254e);
        } else if (Nat192.m19914b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP192K1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP192K1FieldElement qVar = (SecP192K1FieldElement) this.f12252c;
        if (qVar.mo28274f()) {
            return d.mo28258i();
        }
        SecP192K1FieldElement qVar2 = (SecP192K1FieldElement) this.f12251b;
        SecP192K1FieldElement qVar3 = (SecP192K1FieldElement) this.f12253d[0];
        int[] a = Nat192.m19910a();
        SecP192K1Field.m19489d(qVar.f12346d, a);
        int[] a2 = Nat192.m19910a();
        SecP192K1Field.m19489d(a, a2);
        int[] a3 = Nat192.m19910a();
        SecP192K1Field.m19489d(qVar2.f12346d, a3);
        SecP192K1Field.m19480a(Nat192.m19912b(a3, a3, a3), a3);
        SecP192K1Field.m19486b(a, qVar2.f12346d, a);
        SecP192K1Field.m19480a(Nat.m20045c(6, a, 2, 0), a);
        int[] a4 = Nat192.m19910a();
        SecP192K1Field.m19480a(Nat.m20021a(6, a2, 3, 0, a4), a4);
        SecP192K1FieldElement qVar4 = new SecP192K1FieldElement(a2);
        SecP192K1Field.m19489d(a3, qVar4.f12346d);
        int[] iArr = qVar4.f12346d;
        SecP192K1Field.m19490d(iArr, a, iArr);
        int[] iArr2 = qVar4.f12346d;
        SecP192K1Field.m19490d(iArr2, a, iArr2);
        SecP192K1FieldElement qVar5 = new SecP192K1FieldElement(a);
        SecP192K1Field.m19490d(a, qVar4.f12346d, qVar5.f12346d);
        int[] iArr3 = qVar5.f12346d;
        SecP192K1Field.m19486b(iArr3, a3, iArr3);
        int[] iArr4 = qVar5.f12346d;
        SecP192K1Field.m19490d(iArr4, a4, iArr4);
        SecP192K1FieldElement qVar6 = new SecP192K1FieldElement(a3);
        SecP192K1Field.m19491e(qVar.f12346d, qVar6.f12346d);
        if (!qVar3.mo28273e()) {
            int[] iArr5 = qVar6.f12346d;
            SecP192K1Field.m19486b(iArr5, qVar3.f12346d, iArr5);
        }
        return new SecP192K1Point(d, qVar4, qVar5, new ECFieldElement[]{qVar6}, this.f12254e);
    }

    public SecP192K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP192K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
