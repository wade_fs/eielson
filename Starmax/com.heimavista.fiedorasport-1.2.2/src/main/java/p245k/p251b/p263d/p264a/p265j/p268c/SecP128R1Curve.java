package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.a */
public class SecP128R1Curve extends ECCurve.C5160b {

    /* renamed from: j */
    public static final BigInteger f12271j = new BigInteger(1, Hex.m20078a("FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFF"));

    /* renamed from: i */
    protected SecP128R1Point f12272i = new SecP128R1Point(this, null, null);

    public SecP128R1Curve() {
        super(f12271j);
        this.f12223b = mo28235a(new BigInteger(1, Hex.m20078a("FFFFFFFDFFFFFFFFFFFFFFFFFFFFFFFC")));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("E87579C11079F43DD824993C2CEE5ED3")));
        this.f12225d = new BigInteger(1, Hex.m20078a("FFFFFFFE0000000075A30D1B9038A115"));
        this.f12226e = BigInteger.valueOf(1);
        this.f12227f = 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecP128R1Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 2;
    }

    /* renamed from: h */
    public int mo28256h() {
        return f12271j.bitLength();
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12272i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecP128R1FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecP128R1Point(this, dVar, dVar2, z);
    }
}
