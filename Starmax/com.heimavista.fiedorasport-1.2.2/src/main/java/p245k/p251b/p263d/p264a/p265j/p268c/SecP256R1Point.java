package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.c.l0 */
public class SecP256R1Point extends ECPoint.C5167b {
    public SecP256R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP256R1FieldElement k0Var = (SecP256R1FieldElement) this.f12251b;
        SecP256R1FieldElement k0Var2 = (SecP256R1FieldElement) this.f12252c;
        SecP256R1FieldElement k0Var3 = (SecP256R1FieldElement) fVar.mo28306h();
        SecP256R1FieldElement k0Var4 = (SecP256R1FieldElement) fVar.mo28308i();
        SecP256R1FieldElement k0Var5 = (SecP256R1FieldElement) this.f12253d[0];
        SecP256R1FieldElement k0Var6 = (SecP256R1FieldElement) fVar.mo28293a(0);
        int[] c = Nat256.m19982c();
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        int[] a3 = Nat256.m19966a();
        boolean e = k0Var5.mo28273e();
        if (e) {
            iArr2 = k0Var3.f12321d;
            iArr = k0Var4.f12321d;
        } else {
            SecP256R1Field.m19288d(k0Var5.f12321d, a2);
            SecP256R1Field.m19285b(a2, k0Var3.f12321d, a);
            SecP256R1Field.m19285b(a2, k0Var5.f12321d, a2);
            SecP256R1Field.m19285b(a2, k0Var4.f12321d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = k0Var6.mo28273e();
        if (e2) {
            iArr4 = k0Var.f12321d;
            iArr3 = k0Var2.f12321d;
        } else {
            SecP256R1Field.m19288d(k0Var6.f12321d, a3);
            SecP256R1Field.m19285b(a3, k0Var.f12321d, c);
            SecP256R1Field.m19285b(a3, k0Var6.f12321d, a3);
            SecP256R1Field.m19285b(a3, k0Var2.f12321d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat256.m19966a();
        SecP256R1Field.m19289d(iArr4, iArr2, a4);
        SecP256R1Field.m19289d(iArr3, iArr, a);
        if (!Nat256.m19970b(a4)) {
            SecP256R1Field.m19288d(a4, a2);
            int[] a5 = Nat256.m19966a();
            SecP256R1Field.m19285b(a2, a4, a5);
            SecP256R1Field.m19285b(a2, iArr4, a2);
            SecP256R1Field.m19284b(a5, a5);
            Nat256.m19980c(iArr3, a5, c);
            SecP256R1Field.m19277a(Nat256.m19968b(a2, a2, a5), a5);
            SecP256R1FieldElement k0Var7 = new SecP256R1FieldElement(a3);
            SecP256R1Field.m19288d(a, k0Var7.f12321d);
            int[] iArr5 = k0Var7.f12321d;
            SecP256R1Field.m19289d(iArr5, a5, iArr5);
            SecP256R1FieldElement k0Var8 = new SecP256R1FieldElement(a5);
            SecP256R1Field.m19289d(a2, k0Var7.f12321d, k0Var8.f12321d);
            SecP256R1Field.m19287c(k0Var8.f12321d, a, c);
            SecP256R1Field.m19286c(c, k0Var8.f12321d);
            SecP256R1FieldElement k0Var9 = new SecP256R1FieldElement(a4);
            if (!e) {
                int[] iArr6 = k0Var9.f12321d;
                SecP256R1Field.m19285b(iArr6, k0Var5.f12321d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = k0Var9.f12321d;
                SecP256R1Field.m19285b(iArr7, k0Var6.f12321d, iArr7);
            }
            return new SecP256R1Point(d, k0Var7, k0Var8, new ECFieldElement[]{k0Var9}, this.f12254e);
        } else if (Nat256.m19970b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP256R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP256R1FieldElement k0Var = (SecP256R1FieldElement) this.f12252c;
        if (k0Var.mo28274f()) {
            return d.mo28258i();
        }
        SecP256R1FieldElement k0Var2 = (SecP256R1FieldElement) this.f12251b;
        SecP256R1FieldElement k0Var3 = (SecP256R1FieldElement) this.f12253d[0];
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        int[] a3 = Nat256.m19966a();
        SecP256R1Field.m19288d(k0Var.f12321d, a3);
        int[] a4 = Nat256.m19966a();
        SecP256R1Field.m19288d(a3, a4);
        boolean e = k0Var3.mo28273e();
        int[] iArr = k0Var3.f12321d;
        if (!e) {
            SecP256R1Field.m19288d(iArr, a2);
            iArr = a2;
        }
        SecP256R1Field.m19289d(k0Var2.f12321d, iArr, a);
        SecP256R1Field.m19281a(k0Var2.f12321d, iArr, a2);
        SecP256R1Field.m19285b(a2, a, a2);
        SecP256R1Field.m19277a(Nat256.m19968b(a2, a2, a2), a2);
        SecP256R1Field.m19285b(a3, k0Var2.f12321d, a3);
        SecP256R1Field.m19277a(Nat.m20045c(8, a3, 2, 0), a3);
        SecP256R1Field.m19277a(Nat.m20021a(8, a4, 3, 0, a), a);
        SecP256R1FieldElement k0Var4 = new SecP256R1FieldElement(a4);
        SecP256R1Field.m19288d(a2, k0Var4.f12321d);
        int[] iArr2 = k0Var4.f12321d;
        SecP256R1Field.m19289d(iArr2, a3, iArr2);
        int[] iArr3 = k0Var4.f12321d;
        SecP256R1Field.m19289d(iArr3, a3, iArr3);
        SecP256R1FieldElement k0Var5 = new SecP256R1FieldElement(a3);
        SecP256R1Field.m19289d(a3, k0Var4.f12321d, k0Var5.f12321d);
        int[] iArr4 = k0Var5.f12321d;
        SecP256R1Field.m19285b(iArr4, a2, iArr4);
        int[] iArr5 = k0Var5.f12321d;
        SecP256R1Field.m19289d(iArr5, a, iArr5);
        SecP256R1FieldElement k0Var6 = new SecP256R1FieldElement(a2);
        SecP256R1Field.m19290e(k0Var.f12321d, k0Var6.f12321d);
        if (!e) {
            int[] iArr6 = k0Var6.f12321d;
            SecP256R1Field.m19285b(iArr6, k0Var3.f12321d, iArr6);
        }
        return new SecP256R1Point(d, k0Var4, k0Var5, new ECFieldElement[]{k0Var6}, this.f12254e);
    }

    public SecP256R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP256R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
