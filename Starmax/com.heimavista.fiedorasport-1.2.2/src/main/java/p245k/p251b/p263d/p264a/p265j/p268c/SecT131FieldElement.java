package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat192;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.b1 */
public class SecT131FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12282d;

    public SecT131FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 131) {
            throw new IllegalArgumentException("x value invalid for SecT131FieldElement");
        }
        this.f12282d = SecT131Field.m18941a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] b = Nat192.m19918b();
        SecT131Field.m18940a(this.f12282d, ((SecT131FieldElement) dVar).f12282d, b);
        return new SecT131FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12282d;
        long[] jArr2 = ((SecT131FieldElement) dVar).f12282d;
        long[] jArr3 = ((SecT131FieldElement) dVar2).f12282d;
        long[] jArr4 = ((SecT131FieldElement) dVar3).f12282d;
        long[] b = Nat.m20041b(5);
        SecT131Field.m18949e(jArr, jArr2, b);
        SecT131Field.m18949e(jArr3, jArr4, b);
        long[] b2 = Nat192.m19918b();
        SecT131Field.m18946d(b, b2);
        return new SecT131FieldElement(b2);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 131;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] b = Nat192.m19918b();
        SecT131Field.m18947d(this.f12282d, ((SecT131FieldElement) dVar).f12282d, b);
        return new SecT131FieldElement(b);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat192.m19908a(this.f12282d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT131FieldElement)) {
            return false;
        }
        return Nat192.m19909a(this.f12282d, ((SecT131FieldElement) obj).f12282d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat192.m19917b(this.f12282d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] b = Nat192.m19918b();
        SecT131Field.m18948e(this.f12282d, b);
        return new SecT131FieldElement(b);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12282d, 0, 3) ^ 131832;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] b = Nat192.m19918b();
        SecT131Field.m18950f(this.f12282d, b);
        return new SecT131FieldElement(b);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12282d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat192.m19922c(this.f12282d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] b = Nat192.m19918b();
        SecT131Field.m18944c(this.f12282d, b);
        return new SecT131FieldElement(b);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] b = Nat192.m19918b();
        SecT131Field.m18939a(this.f12282d, b);
        return new SecT131FieldElement(b);
    }

    public SecT131FieldElement() {
        this.f12282d = Nat192.m19918b();
    }

    protected SecT131FieldElement(long[] jArr) {
        this.f12282d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12282d;
        long[] jArr2 = ((SecT131FieldElement) dVar).f12282d;
        long[] jArr3 = ((SecT131FieldElement) dVar2).f12282d;
        long[] b = Nat.m20041b(5);
        SecT131Field.m18951g(jArr, b);
        SecT131Field.m18949e(jArr2, jArr3, b);
        long[] b2 = Nat192.m19918b();
        SecT131Field.m18946d(b, b2);
        return new SecT131FieldElement(b2);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
