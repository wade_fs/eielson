package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECConstants;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat576;

/* renamed from: k.b.d.a.j.c.v2 */
public class SecT571R1Point extends ECPoint.C5166a {
    public SecT571R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        long[] jArr;
        long[] jArr2;
        long[] jArr3;
        long[] jArr4;
        SecT571FieldElement r2Var;
        SecT571FieldElement r2Var2;
        SecT571FieldElement r2Var3;
        ECPoint fVar2 = fVar;
        if (mo28309j()) {
            return fVar2;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecT571FieldElement r2Var4 = (SecT571FieldElement) this.f12251b;
        SecT571FieldElement r2Var5 = (SecT571FieldElement) fVar.mo28304f();
        if (!r2Var4.mo28274f()) {
            SecT571FieldElement r2Var6 = (SecT571FieldElement) this.f12252c;
            SecT571FieldElement r2Var7 = (SecT571FieldElement) this.f12253d[0];
            SecT571FieldElement r2Var8 = (SecT571FieldElement) fVar.mo28305g();
            SecT571FieldElement r2Var9 = (SecT571FieldElement) fVar2.mo28293a(0);
            long[] a = Nat576.m20009a();
            long[] a2 = Nat576.m20009a();
            long[] a3 = Nat576.m20009a();
            long[] a4 = Nat576.m20009a();
            long[] a5 = r2Var7.mo28273e() ? null : SecT571Field.m19548a(r2Var7.f12352d);
            if (a5 == null) {
                jArr = r2Var5.f12352d;
                jArr2 = r2Var8.f12352d;
            } else {
                SecT571Field.m19562h(r2Var5.f12352d, a5, a2);
                SecT571Field.m19562h(r2Var8.f12352d, a5, a4);
                jArr = a2;
                jArr2 = a4;
            }
            long[] a6 = r2Var9.mo28273e() ? null : SecT571Field.m19548a(r2Var9.f12352d);
            if (a6 == null) {
                jArr4 = r2Var4.f12352d;
                jArr3 = r2Var6.f12352d;
            } else {
                SecT571Field.m19562h(r2Var4.f12352d, a6, a);
                SecT571Field.m19562h(r2Var6.f12352d, a6, a3);
                jArr4 = a;
                jArr3 = a3;
            }
            SecT571Field.m19546a(jArr3, jArr2, a3);
            SecT571Field.m19546a(jArr4, jArr, a4);
            if (!Nat576.m20011b(a4)) {
                if (r2Var5.mo28274f()) {
                    ECPoint n = mo28313n();
                    SecT571FieldElement r2Var10 = (SecT571FieldElement) n.mo28306h();
                    ECFieldElement i = n.mo28308i();
                    ECFieldElement b = i.mo28263a(r2Var8).mo28267b(r2Var10);
                    r2Var2 = (SecT571FieldElement) b.mo28277i().mo28263a(b).mo28263a(r2Var10).mo28262a();
                    if (r2Var2.mo28274f()) {
                        return new SecT571R1Point(d, r2Var2, SecT571R1Curve.f12365k, this.f12254e);
                    }
                    r2Var3 = (SecT571FieldElement) d.mo28235a(ECConstants.f12220b);
                    r2Var = (SecT571FieldElement) b.mo28270c(r2Var10.mo28263a(r2Var2)).mo28263a(r2Var2).mo28263a(i).mo28267b(r2Var2).mo28263a(r2Var2);
                } else {
                    SecT571Field.m19558f(a4, a4);
                    long[] a7 = SecT571Field.m19548a(a3);
                    SecT571Field.m19562h(jArr4, a7, a);
                    SecT571Field.m19562h(jArr, a7, a2);
                    SecT571FieldElement r2Var11 = new SecT571FieldElement(a);
                    SecT571Field.m19559f(a, a2, r2Var11.f12352d);
                    if (r2Var11.mo28274f()) {
                        return new SecT571R1Point(d, r2Var11, SecT571R1Curve.f12365k, this.f12254e);
                    }
                    SecT571FieldElement r2Var12 = new SecT571FieldElement(a3);
                    SecT571Field.m19562h(a4, a7, r2Var12.f12352d);
                    if (a6 != null) {
                        long[] jArr5 = r2Var12.f12352d;
                        SecT571Field.m19562h(jArr5, a6, jArr5);
                    }
                    long[] b2 = Nat576.m20012b();
                    SecT571Field.m19546a(a2, a4, a4);
                    SecT571Field.m19560g(a4, b2);
                    SecT571Field.m19546a(r2Var6.f12352d, r2Var7.f12352d, a4);
                    SecT571Field.m19561g(a4, r2Var12.f12352d, b2);
                    SecT571FieldElement r2Var13 = new SecT571FieldElement(a4);
                    SecT571Field.m19554d(b2, r2Var13.f12352d);
                    if (a5 != null) {
                        long[] jArr6 = r2Var12.f12352d;
                        SecT571Field.m19562h(jArr6, a5, jArr6);
                    }
                    r2Var2 = r2Var11;
                    r2Var = r2Var13;
                    r2Var3 = r2Var12;
                }
                return new SecT571R1Point(d, r2Var2, r2Var, new ECFieldElement[]{r2Var3}, this.f12254e);
            } else if (Nat576.m20011b(a3)) {
                return mo28316q();
            } else {
                return d.mo28258i();
            }
        } else if (r2Var5.mo28274f()) {
            return d.mo28258i();
        } else {
            return fVar2.mo28296a(this);
        }
    }

    /* renamed from: i */
    public ECFieldElement mo28308i() {
        ECFieldElement dVar = this.f12251b;
        ECFieldElement dVar2 = this.f12252c;
        if (mo28309j() || dVar.mo28274f()) {
            return dVar2;
        }
        ECFieldElement c = dVar2.mo28263a(dVar).mo28270c(dVar);
        ECFieldElement dVar3 = this.f12253d[0];
        return !dVar3.mo28273e() ? c.mo28267b(dVar3) : c;
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        ECFieldElement dVar = this.f12251b;
        if (dVar.mo28274f()) {
            return this;
        }
        ECFieldElement dVar2 = this.f12252c;
        ECFieldElement dVar3 = this.f12253d[0];
        ECCurve cVar = this.f12250a;
        ECFieldElement[] dVarArr = {dVar3};
        return new SecT571R1Point(cVar, dVar, dVar2.mo28263a(dVar3), dVarArr, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        long[] jArr;
        long[] jArr2;
        long[] jArr3;
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecT571FieldElement r2Var = (SecT571FieldElement) this.f12251b;
        if (r2Var.mo28274f()) {
            return d.mo28258i();
        }
        SecT571FieldElement r2Var2 = (SecT571FieldElement) this.f12252c;
        SecT571FieldElement r2Var3 = (SecT571FieldElement) this.f12253d[0];
        long[] a = Nat576.m20009a();
        long[] a2 = Nat576.m20009a();
        long[] a3 = r2Var3.mo28273e() ? null : SecT571Field.m19548a(r2Var3.f12352d);
        if (a3 == null) {
            jArr = r2Var2.f12352d;
            jArr2 = r2Var3.f12352d;
        } else {
            SecT571Field.m19562h(r2Var2.f12352d, a3, a);
            SecT571Field.m19558f(r2Var3.f12352d, a2);
            jArr = a;
            jArr2 = a2;
        }
        long[] a4 = Nat576.m20009a();
        SecT571Field.m19558f(r2Var2.f12352d, a4);
        SecT571Field.m19551b(jArr, jArr2, a4);
        if (Nat576.m20011b(a4)) {
            return new SecT571R1Point(d, new SecT571FieldElement(a4), SecT571R1Curve.f12365k, this.f12254e);
        }
        long[] b = Nat576.m20012b();
        SecT571Field.m19561g(a4, jArr, b);
        SecT571FieldElement r2Var4 = new SecT571FieldElement(a);
        SecT571Field.m19558f(a4, r2Var4.f12352d);
        SecT571FieldElement r2Var5 = new SecT571FieldElement(a4);
        if (a3 != null) {
            long[] jArr4 = r2Var5.f12352d;
            SecT571Field.m19559f(jArr4, jArr2, jArr4);
        }
        if (a3 == null) {
            jArr3 = r2Var.f12352d;
        } else {
            SecT571Field.m19562h(r2Var.f12352d, a3, a2);
            jArr3 = a2;
        }
        SecT571Field.m19560g(jArr3, b);
        SecT571Field.m19554d(b, a2);
        SecT571Field.m19551b(r2Var4.f12352d, r2Var5.f12352d, a2);
        return new SecT571R1Point(d, r2Var4, new SecT571FieldElement(a2), new ECFieldElement[]{r2Var5}, this.f12254e);
    }

    public SecT571R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecT571R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
