package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Mod;
import p245k.p251b.p263d.p271c.Nat192;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.u */
public class SecP192R1FieldElement extends ECFieldElement {

    /* renamed from: e */
    public static final BigInteger f12362e = SecP192R1Curve.f12353j;

    /* renamed from: d */
    protected int[] f12363d;

    public SecP192R1FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.compareTo(f12362e) >= 0) {
            throw new IllegalArgumentException("x value invalid for SecP192R1FieldElement");
        }
        this.f12363d = SecP192R1Field.m19637a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        SecP192R1Field.m19636a(this.f12363d, ((SecP192R1FieldElement) dVar).f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        Mod.m19851a(SecP192R1Field.f12359a, ((SecP192R1FieldElement) dVar).f12363d, a);
        SecP192R1Field.m19640b(a, this.f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return f12362e.bitLength();
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        SecP192R1Field.m19644d(this.f12363d, ((SecP192R1FieldElement) dVar).f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat192.m19905a(this.f12363d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecP192R1FieldElement)) {
            return false;
        }
        return Nat192.m19907a(this.f12363d, ((SecP192R1FieldElement) obj).f12363d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat192.m19914b(this.f12363d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        int[] a = Nat192.m19910a();
        SecP192R1Field.m19639b(this.f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        int[] iArr = this.f12363d;
        if (Nat192.m19914b(iArr) || Nat192.m19905a(iArr)) {
            return super;
        }
        int[] a = Nat192.m19910a();
        int[] a2 = Nat192.m19910a();
        SecP192R1Field.m19643d(iArr, a);
        SecP192R1Field.m19640b(a, iArr, a);
        SecP192R1Field.m19634a(a, 2, a2);
        SecP192R1Field.m19640b(a2, a, a2);
        SecP192R1Field.m19634a(a2, 4, a);
        SecP192R1Field.m19640b(a, a2, a);
        SecP192R1Field.m19634a(a, 8, a2);
        SecP192R1Field.m19640b(a2, a, a2);
        SecP192R1Field.m19634a(a2, 16, a);
        SecP192R1Field.m19640b(a, a2, a);
        SecP192R1Field.m19634a(a, 32, a2);
        SecP192R1Field.m19640b(a2, a, a2);
        SecP192R1Field.m19634a(a2, 64, a);
        SecP192R1Field.m19640b(a, a2, a);
        SecP192R1Field.m19634a(a, 62, a);
        SecP192R1Field.m19643d(a, a2);
        if (Nat192.m19907a(iArr, a2)) {
            return new SecP192R1FieldElement(a);
        }
        return null;
    }

    public int hashCode() {
        return f12362e.hashCode() ^ Arrays.m20066b(this.f12363d, 0, 6);
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        int[] a = Nat192.m19910a();
        SecP192R1Field.m19643d(this.f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return Nat192.m19900a(this.f12363d, 0) == 1;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat192.m19921c(this.f12363d);
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        int[] a = Nat192.m19910a();
        SecP192R1Field.m19640b(this.f12363d, ((SecP192R1FieldElement) dVar).f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        int[] a = Nat192.m19910a();
        SecP192R1Field.m19635a(this.f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        int[] a = Nat192.m19910a();
        Mod.m19851a(SecP192R1Field.f12359a, this.f12363d, a);
        return new SecP192R1FieldElement(a);
    }

    public SecP192R1FieldElement() {
        this.f12363d = Nat192.m19910a();
    }

    protected SecP192R1FieldElement(int[] iArr) {
        this.f12363d = iArr;
    }
}
