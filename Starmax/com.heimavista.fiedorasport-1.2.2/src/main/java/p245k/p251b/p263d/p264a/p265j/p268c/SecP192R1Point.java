package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat192;

/* renamed from: k.b.d.a.j.c.v */
public class SecP192R1Point extends ECPoint.C5167b {
    public SecP192R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP192R1FieldElement uVar = (SecP192R1FieldElement) this.f12251b;
        SecP192R1FieldElement uVar2 = (SecP192R1FieldElement) this.f12252c;
        SecP192R1FieldElement uVar3 = (SecP192R1FieldElement) fVar.mo28306h();
        SecP192R1FieldElement uVar4 = (SecP192R1FieldElement) fVar.mo28308i();
        SecP192R1FieldElement uVar5 = (SecP192R1FieldElement) this.f12253d[0];
        SecP192R1FieldElement uVar6 = (SecP192R1FieldElement) fVar.mo28293a(0);
        int[] c = Nat192.m19926c();
        int[] a = Nat192.m19910a();
        int[] a2 = Nat192.m19910a();
        int[] a3 = Nat192.m19910a();
        boolean e = uVar5.mo28273e();
        if (e) {
            iArr2 = uVar3.f12363d;
            iArr = uVar4.f12363d;
        } else {
            SecP192R1Field.m19643d(uVar5.f12363d, a2);
            SecP192R1Field.m19640b(a2, uVar3.f12363d, a);
            SecP192R1Field.m19640b(a2, uVar5.f12363d, a2);
            SecP192R1Field.m19640b(a2, uVar4.f12363d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = uVar6.mo28273e();
        if (e2) {
            iArr4 = uVar.f12363d;
            iArr3 = uVar2.f12363d;
        } else {
            SecP192R1Field.m19643d(uVar6.f12363d, a3);
            SecP192R1Field.m19640b(a3, uVar.f12363d, c);
            SecP192R1Field.m19640b(a3, uVar6.f12363d, a3);
            SecP192R1Field.m19640b(a3, uVar2.f12363d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat192.m19910a();
        SecP192R1Field.m19644d(iArr4, iArr2, a4);
        SecP192R1Field.m19644d(iArr3, iArr, a);
        if (!Nat192.m19914b(a4)) {
            SecP192R1Field.m19643d(a4, a2);
            int[] a5 = Nat192.m19910a();
            SecP192R1Field.m19640b(a2, a4, a5);
            SecP192R1Field.m19640b(a2, iArr4, a2);
            SecP192R1Field.m19639b(a5, a5);
            Nat192.m19925c(iArr3, a5, c);
            SecP192R1Field.m19632a(Nat192.m19912b(a2, a2, a5), a5);
            SecP192R1FieldElement uVar7 = new SecP192R1FieldElement(a3);
            SecP192R1Field.m19643d(a, uVar7.f12363d);
            int[] iArr5 = uVar7.f12363d;
            SecP192R1Field.m19644d(iArr5, a5, iArr5);
            SecP192R1FieldElement uVar8 = new SecP192R1FieldElement(a5);
            SecP192R1Field.m19644d(a2, uVar7.f12363d, uVar8.f12363d);
            SecP192R1Field.m19642c(uVar8.f12363d, a, c);
            SecP192R1Field.m19641c(c, uVar8.f12363d);
            SecP192R1FieldElement uVar9 = new SecP192R1FieldElement(a4);
            if (!e) {
                int[] iArr6 = uVar9.f12363d;
                SecP192R1Field.m19640b(iArr6, uVar5.f12363d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = uVar9.f12363d;
                SecP192R1Field.m19640b(iArr7, uVar6.f12363d, iArr7);
            }
            return new SecP192R1Point(d, uVar7, uVar8, new ECFieldElement[]{uVar9}, this.f12254e);
        } else if (Nat192.m19914b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP192R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP192R1FieldElement uVar = (SecP192R1FieldElement) this.f12252c;
        if (uVar.mo28274f()) {
            return d.mo28258i();
        }
        SecP192R1FieldElement uVar2 = (SecP192R1FieldElement) this.f12251b;
        SecP192R1FieldElement uVar3 = (SecP192R1FieldElement) this.f12253d[0];
        int[] a = Nat192.m19910a();
        int[] a2 = Nat192.m19910a();
        int[] a3 = Nat192.m19910a();
        SecP192R1Field.m19643d(uVar.f12363d, a3);
        int[] a4 = Nat192.m19910a();
        SecP192R1Field.m19643d(a3, a4);
        boolean e = uVar3.mo28273e();
        int[] iArr = uVar3.f12363d;
        if (!e) {
            SecP192R1Field.m19643d(iArr, a2);
            iArr = a2;
        }
        SecP192R1Field.m19644d(uVar2.f12363d, iArr, a);
        SecP192R1Field.m19636a(uVar2.f12363d, iArr, a2);
        SecP192R1Field.m19640b(a2, a, a2);
        SecP192R1Field.m19632a(Nat192.m19912b(a2, a2, a2), a2);
        SecP192R1Field.m19640b(a3, uVar2.f12363d, a3);
        SecP192R1Field.m19632a(Nat.m20045c(6, a3, 2, 0), a3);
        SecP192R1Field.m19632a(Nat.m20021a(6, a4, 3, 0, a), a);
        SecP192R1FieldElement uVar4 = new SecP192R1FieldElement(a4);
        SecP192R1Field.m19643d(a2, uVar4.f12363d);
        int[] iArr2 = uVar4.f12363d;
        SecP192R1Field.m19644d(iArr2, a3, iArr2);
        int[] iArr3 = uVar4.f12363d;
        SecP192R1Field.m19644d(iArr3, a3, iArr3);
        SecP192R1FieldElement uVar5 = new SecP192R1FieldElement(a3);
        SecP192R1Field.m19644d(a3, uVar4.f12363d, uVar5.f12363d);
        int[] iArr4 = uVar5.f12363d;
        SecP192R1Field.m19640b(iArr4, a2, iArr4);
        int[] iArr5 = uVar5.f12363d;
        SecP192R1Field.m19644d(iArr5, a, iArr5);
        SecP192R1FieldElement uVar6 = new SecP192R1FieldElement(a2);
        SecP192R1Field.m19645e(uVar.f12363d, uVar6.f12363d);
        if (!e) {
            int[] iArr6 = uVar6.f12363d;
            SecP192R1Field.m19640b(iArr6, uVar3.f12363d, iArr6);
        }
        return new SecP192R1Point(d, uVar4, uVar5, new ECFieldElement[]{uVar6}, this.f12254e);
    }

    public SecP192R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP192R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
