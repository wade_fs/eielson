package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.c.f0 */
public class SecP256K1Field {

    /* renamed from: a */
    static final int[] f12296a = {-977, -2, -1, -1, -1, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12297b = {954529, 1954, 1, 0, 0, 0, 0, 0, -1954, -3, -1, -1, -1, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12298c = {-954529, -1955, -2, -1, -1, -1, -1, -1, 1953, 2};

    /* renamed from: a */
    public static void m19133a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19960a(iArr, iArr2, iArr3) != 0 || (iArr3[7] == -1 && Nat256.m19981c(iArr3, f12296a))) {
            Nat.m20014a(8, 977, iArr3);
        }
    }

    /* renamed from: b */
    public static void m19136b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] c = Nat256.m19982c();
        Nat256.m19980c(iArr, iArr2, c);
        m19137c(c, iArr3);
    }

    /* renamed from: c */
    public static void m19138c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19983d(iArr, iArr2, iArr3) != 0 || (iArr3[15] == -1 && Nat.m20047c(16, iArr3, f12297b))) {
            int[] iArr4 = f12298c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(16, iArr3, f12298c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m19139d(int[] iArr, int[] iArr2) {
        int[] c = Nat256.m19982c();
        Nat256.m19985d(iArr, c);
        m19137c(c, iArr2);
    }

    /* renamed from: e */
    public static void m19141e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(8, iArr, 0, iArr2) != 0 || (iArr2[7] == -1 && Nat256.m19981c(iArr2, f12296a))) {
            Nat.m20014a(8, 977, iArr2);
        }
    }

    /* renamed from: a */
    public static void m19132a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(8, iArr, iArr2) != 0 || (iArr2[7] == -1 && Nat256.m19981c(iArr2, f12296a))) {
            Nat.m20014a(8, 977, iArr2);
        }
    }

    /* renamed from: b */
    public static void m19135b(int[] iArr, int[] iArr2) {
        if (Nat256.m19970b(iArr)) {
            Nat256.m19984d(iArr2);
        } else {
            Nat256.m19988e(f12296a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m19140d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19988e(iArr, iArr2, iArr3) != 0) {
            Nat.m20042c(8, 977, iArr3);
        }
    }

    /* renamed from: c */
    public static void m19137c(int[] iArr, int[] iArr2) {
        if (Nat256.m19954a(977, Nat256.m19961a(977, iArr, 8, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[7] == -1 && Nat256.m19981c(iArr2, f12296a))) {
            Nat.m20014a(8, 977, iArr2);
        }
    }

    /* renamed from: a */
    public static int[] m19134a(BigInteger bigInteger) {
        int[] a = Nat256.m19967a(bigInteger);
        if (a[7] == -1 && Nat256.m19981c(a, f12296a)) {
            Nat256.m19987e(f12296a, a);
        }
        return a;
    }

    /* renamed from: a */
    public static void m19130a(int i, int[] iArr) {
        if ((i != 0 && Nat256.m19953a(977, i, iArr, 0) != 0) || (iArr[7] == -1 && Nat256.m19981c(iArr, f12296a))) {
            Nat.m20014a(8, 977, iArr);
        }
    }

    /* renamed from: a */
    public static void m19131a(int[] iArr, int i, int[] iArr2) {
        int[] c = Nat256.m19982c();
        Nat256.m19985d(iArr, c);
        m19137c(c, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat256.m19985d(iArr2, c);
                m19137c(c, iArr2);
            } else {
                return;
            }
        }
    }
}
