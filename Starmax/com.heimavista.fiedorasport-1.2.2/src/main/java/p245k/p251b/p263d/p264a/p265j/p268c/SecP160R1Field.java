package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat160;

/* renamed from: k.b.d.a.j.c.h */
public class SecP160R1Field {

    /* renamed from: a */
    static final int[] f12306a = {Integer.MAX_VALUE, -1, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12307b = {1, 1073741825, 0, 0, 0, -2, -2, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12308c = {-1, -1073741826, -1, -1, -1, 1, 1};

    /* renamed from: a */
    public static void m19209a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat160.m19880a(iArr, iArr2, iArr3) != 0 || (iArr3[4] == -1 && Nat160.m19889b(iArr3, f12306a))) {
            Nat.m20034b(5, -2147483647, iArr3);
        }
    }

    /* renamed from: b */
    public static void m19212b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] b = Nat160.m19890b();
        Nat160.m19893c(iArr, iArr2, b);
        m19213c(b, iArr3);
    }

    /* renamed from: c */
    public static void m19214c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat160.m19895d(iArr, iArr2, iArr3) != 0 || (iArr3[9] == -1 && Nat.m20047c(10, iArr3, f12307b))) {
            int[] iArr4 = f12308c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(10, iArr3, f12308c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m19215d(int[] iArr, int[] iArr2) {
        int[] b = Nat160.m19890b();
        Nat160.m19892c(iArr, b);
        m19213c(b, iArr2);
    }

    /* renamed from: e */
    public static void m19217e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(5, iArr, 0, iArr2) != 0 || (iArr2[4] == -1 && Nat160.m19889b(iArr2, f12306a))) {
            Nat.m20034b(5, -2147483647, iArr2);
        }
    }

    /* renamed from: a */
    public static void m19208a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(5, iArr, iArr2) != 0 || (iArr2[4] == -1 && Nat160.m19889b(iArr2, f12306a))) {
            Nat.m20034b(5, -2147483647, iArr2);
        }
    }

    /* renamed from: b */
    public static void m19211b(int[] iArr, int[] iArr2) {
        if (Nat160.m19888b(iArr)) {
            Nat160.m19896d(iArr2);
        } else {
            Nat160.m19897e(f12306a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m19216d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat160.m19897e(iArr, iArr2, iArr3) != 0) {
            Nat.m20048d(5, -2147483647, iArr3);
        }
    }

    /* renamed from: c */
    public static void m19213c(int[] iArr, int[] iArr2) {
        int[] iArr3 = iArr2;
        long j = ((long) iArr[5]) & 4294967295L;
        long j2 = ((long) iArr[6]) & 4294967295L;
        long j3 = ((long) iArr[7]) & 4294967295L;
        long j4 = ((long) iArr[8]) & 4294967295L;
        long j5 = ((long) iArr[9]) & 4294967295L;
        long j6 = (((long) iArr[0]) & 4294967295L) + j + (j << 31) + 0;
        iArr3[0] = (int) j6;
        long j7 = (j6 >>> 32) + (((long) iArr[1]) & 4294967295L) + j2 + (j2 << 31);
        iArr3[1] = (int) j7;
        long j8 = (j7 >>> 32) + (((long) iArr[2]) & 4294967295L) + j3 + (j3 << 31);
        iArr3[2] = (int) j8;
        long j9 = (j8 >>> 32) + (((long) iArr[3]) & 4294967295L) + j4 + (j4 << 31);
        iArr3[3] = (int) j9;
        long j10 = (j9 >>> 32) + (4294967295L & ((long) iArr[4])) + j5 + (j5 << 31);
        iArr3[4] = (int) j10;
        m19206a((int) (j10 >>> 32), iArr3);
    }

    /* renamed from: a */
    public static int[] m19210a(BigInteger bigInteger) {
        int[] a = Nat160.m19885a(bigInteger);
        if (a[4] == -1 && Nat160.m19889b(a, f12306a)) {
            Nat160.m19894d(f12306a, a);
        }
        return a;
    }

    /* renamed from: a */
    public static void m19206a(int i, int[] iArr) {
        if ((i != 0 && Nat160.m19886b(-2147483647, i, iArr, 0) != 0) || (iArr[4] == -1 && Nat160.m19889b(iArr, f12306a))) {
            Nat.m20034b(5, -2147483647, iArr);
        }
    }

    /* renamed from: a */
    public static void m19207a(int[] iArr, int i, int[] iArr2) {
        int[] b = Nat160.m19890b();
        Nat160.m19892c(iArr, b);
        m19213c(b, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat160.m19892c(iArr2, b);
                m19213c(b, iArr2);
            } else {
                return;
            }
        }
    }
}
