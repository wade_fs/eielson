package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.c.h0 */
public class SecP256K1Point extends ECPoint.C5167b {
    public SecP256K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP256K1FieldElement g0Var = (SecP256K1FieldElement) this.f12251b;
        SecP256K1FieldElement g0Var2 = (SecP256K1FieldElement) this.f12252c;
        SecP256K1FieldElement g0Var3 = (SecP256K1FieldElement) fVar.mo28306h();
        SecP256K1FieldElement g0Var4 = (SecP256K1FieldElement) fVar.mo28308i();
        SecP256K1FieldElement g0Var5 = (SecP256K1FieldElement) this.f12253d[0];
        SecP256K1FieldElement g0Var6 = (SecP256K1FieldElement) fVar.mo28293a(0);
        int[] c = Nat256.m19982c();
        int[] a = Nat256.m19966a();
        int[] a2 = Nat256.m19966a();
        int[] a3 = Nat256.m19966a();
        boolean e = g0Var5.mo28273e();
        if (e) {
            iArr2 = g0Var3.f12303d;
            iArr = g0Var4.f12303d;
        } else {
            SecP256K1Field.m19139d(g0Var5.f12303d, a2);
            SecP256K1Field.m19136b(a2, g0Var3.f12303d, a);
            SecP256K1Field.m19136b(a2, g0Var5.f12303d, a2);
            SecP256K1Field.m19136b(a2, g0Var4.f12303d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = g0Var6.mo28273e();
        if (e2) {
            iArr4 = g0Var.f12303d;
            iArr3 = g0Var2.f12303d;
        } else {
            SecP256K1Field.m19139d(g0Var6.f12303d, a3);
            SecP256K1Field.m19136b(a3, g0Var.f12303d, c);
            SecP256K1Field.m19136b(a3, g0Var6.f12303d, a3);
            SecP256K1Field.m19136b(a3, g0Var2.f12303d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat256.m19966a();
        SecP256K1Field.m19140d(iArr4, iArr2, a4);
        SecP256K1Field.m19140d(iArr3, iArr, a);
        if (!Nat256.m19970b(a4)) {
            SecP256K1Field.m19139d(a4, a2);
            int[] a5 = Nat256.m19966a();
            SecP256K1Field.m19136b(a2, a4, a5);
            SecP256K1Field.m19136b(a2, iArr4, a2);
            SecP256K1Field.m19135b(a5, a5);
            Nat256.m19980c(iArr3, a5, c);
            SecP256K1Field.m19130a(Nat256.m19968b(a2, a2, a5), a5);
            SecP256K1FieldElement g0Var7 = new SecP256K1FieldElement(a3);
            SecP256K1Field.m19139d(a, g0Var7.f12303d);
            int[] iArr5 = g0Var7.f12303d;
            SecP256K1Field.m19140d(iArr5, a5, iArr5);
            SecP256K1FieldElement g0Var8 = new SecP256K1FieldElement(a5);
            SecP256K1Field.m19140d(a2, g0Var7.f12303d, g0Var8.f12303d);
            SecP256K1Field.m19138c(g0Var8.f12303d, a, c);
            SecP256K1Field.m19137c(c, g0Var8.f12303d);
            SecP256K1FieldElement g0Var9 = new SecP256K1FieldElement(a4);
            if (!e) {
                int[] iArr6 = g0Var9.f12303d;
                SecP256K1Field.m19136b(iArr6, g0Var5.f12303d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = g0Var9.f12303d;
                SecP256K1Field.m19136b(iArr7, g0Var6.f12303d, iArr7);
            }
            return new SecP256K1Point(d, g0Var7, g0Var8, new ECFieldElement[]{g0Var9}, this.f12254e);
        } else if (Nat256.m19970b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP256K1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP256K1FieldElement g0Var = (SecP256K1FieldElement) this.f12252c;
        if (g0Var.mo28274f()) {
            return d.mo28258i();
        }
        SecP256K1FieldElement g0Var2 = (SecP256K1FieldElement) this.f12251b;
        SecP256K1FieldElement g0Var3 = (SecP256K1FieldElement) this.f12253d[0];
        int[] a = Nat256.m19966a();
        SecP256K1Field.m19139d(g0Var.f12303d, a);
        int[] a2 = Nat256.m19966a();
        SecP256K1Field.m19139d(a, a2);
        int[] a3 = Nat256.m19966a();
        SecP256K1Field.m19139d(g0Var2.f12303d, a3);
        SecP256K1Field.m19130a(Nat256.m19968b(a3, a3, a3), a3);
        SecP256K1Field.m19136b(a, g0Var2.f12303d, a);
        SecP256K1Field.m19130a(Nat.m20045c(8, a, 2, 0), a);
        int[] a4 = Nat256.m19966a();
        SecP256K1Field.m19130a(Nat.m20021a(8, a2, 3, 0, a4), a4);
        SecP256K1FieldElement g0Var4 = new SecP256K1FieldElement(a2);
        SecP256K1Field.m19139d(a3, g0Var4.f12303d);
        int[] iArr = g0Var4.f12303d;
        SecP256K1Field.m19140d(iArr, a, iArr);
        int[] iArr2 = g0Var4.f12303d;
        SecP256K1Field.m19140d(iArr2, a, iArr2);
        SecP256K1FieldElement g0Var5 = new SecP256K1FieldElement(a);
        SecP256K1Field.m19140d(a, g0Var4.f12303d, g0Var5.f12303d);
        int[] iArr3 = g0Var5.f12303d;
        SecP256K1Field.m19136b(iArr3, a3, iArr3);
        int[] iArr4 = g0Var5.f12303d;
        SecP256K1Field.m19140d(iArr4, a4, iArr4);
        SecP256K1FieldElement g0Var6 = new SecP256K1FieldElement(a3);
        SecP256K1Field.m19141e(g0Var.f12303d, g0Var6.f12303d);
        if (!g0Var3.mo28273e()) {
            int[] iArr5 = g0Var6.f12303d;
            SecP256K1Field.m19136b(iArr5, g0Var3.f12303d, iArr5);
        }
        return new SecP256K1Point(d, g0Var4, g0Var5, new ECFieldElement[]{g0Var6}, this.f12254e);
    }

    public SecP256K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP256K1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
