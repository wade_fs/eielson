package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Interleave;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.c.o1 */
public class SecT193Field {
    /* renamed from: a */
    public static void m19461a(long[] jArr, long[] jArr2, long[] jArr3) {
        jArr3[0] = jArr[0] ^ jArr2[0];
        jArr3[1] = jArr[1] ^ jArr2[1];
        jArr3[2] = jArr[2] ^ jArr2[2];
        jArr3[3] = jArr2[3] ^ jArr[3];
    }

    /* renamed from: b */
    public static void m19464b(long[] jArr, long[] jArr2, long[] jArr3) {
        jArr3[0] = jArr[0] ^ jArr2[0];
        jArr3[1] = jArr[1] ^ jArr2[1];
        jArr3[2] = jArr[2] ^ jArr2[2];
        jArr3[3] = jArr[3] ^ jArr2[3];
        jArr3[4] = jArr[4] ^ jArr2[4];
        jArr3[5] = jArr[5] ^ jArr2[5];
        jArr3[6] = jArr2[6] ^ jArr[6];
    }

    /* renamed from: c */
    protected static void m19466c(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] jArr4 = new long[4];
        long[] jArr5 = new long[4];
        m19463b(jArr, jArr4);
        m19463b(jArr2, jArr5);
        long[] jArr6 = jArr3;
        m19456a(jArr4[0], jArr5[0], jArr6, 0);
        m19456a(jArr4[1], jArr5[1], jArr6, 1);
        m19456a(jArr4[2], jArr5[2], jArr6, 2);
        m19456a(jArr4[3], jArr5[3], jArr6, 3);
        for (int i = 5; i > 0; i--) {
            jArr3[i] = jArr3[i] ^ jArr3[i - 1];
        }
        m19456a(jArr4[0] ^ jArr4[1], jArr5[0] ^ jArr5[1], jArr3, 1);
        m19456a(jArr4[2] ^ jArr4[3], jArr5[2] ^ jArr5[3], jArr3, 3);
        for (int i2 = 7; i2 > 1; i2--) {
            jArr3[i2] = jArr3[i2] ^ jArr3[i2 - 2];
        }
        long j = jArr4[0] ^ jArr4[2];
        long j2 = jArr4[1] ^ jArr4[3];
        long j3 = jArr5[0] ^ jArr5[2];
        long j4 = jArr5[1] ^ jArr5[3];
        m19456a(j ^ j2, j3 ^ j4, jArr3, 3);
        long[] jArr7 = new long[3];
        m19456a(j, j3, jArr7, 0);
        m19456a(j2, j4, jArr7, 1);
        long j5 = jArr7[0];
        long j6 = jArr7[1];
        long j7 = jArr7[2];
        jArr3[2] = jArr3[2] ^ j5;
        jArr3[3] = (j5 ^ j6) ^ jArr3[3];
        jArr3[4] = jArr3[4] ^ (j7 ^ j6);
        jArr3[5] = jArr3[5] ^ j7;
        m19457a(jArr3);
    }

    /* renamed from: d */
    public static void m19467d(long[] jArr, long[] jArr2) {
        if (!Nat256.m19973b(jArr)) {
            long[] b = Nat256.m19974b();
            long[] b2 = Nat256.m19974b();
            m19472g(jArr, b);
            m19459a(b, 1, b2);
            m19468d(b, b2, b);
            m19459a(b2, 1, b2);
            m19468d(b, b2, b);
            m19459a(b, 3, b2);
            m19468d(b, b2, b);
            m19459a(b, 6, b2);
            m19468d(b, b2, b);
            m19459a(b, 12, b2);
            m19468d(b, b2, b);
            m19459a(b, 24, b2);
            m19468d(b, b2, b);
            m19459a(b, 48, b2);
            m19468d(b, b2, b);
            m19459a(b, 96, b2);
            m19468d(b, b2, jArr2);
            return;
        }
        throw new IllegalStateException();
    }

    /* renamed from: e */
    public static void m19470e(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] d = Nat256.m19986d();
        m19466c(jArr, jArr2, d);
        m19464b(jArr3, d, jArr3);
    }

    /* renamed from: f */
    public static void m19471f(long[] jArr, long[] jArr2) {
        long a = Interleave.m19844a(jArr[0]);
        long a2 = Interleave.m19844a(jArr[1]);
        long j = (a & 4294967295L) | (a2 << 32);
        long j2 = (a >>> 32) | (a2 & -4294967296L);
        long a3 = Interleave.m19844a(jArr[2]);
        long j3 = (a3 & 4294967295L) ^ (jArr[3] << 32);
        long j4 = a3 >>> 32;
        jArr2[0] = j ^ (j2 << 8);
        jArr2[1] = ((j3 ^ (j4 << 8)) ^ (j2 >>> 56)) ^ (j2 << 33);
        jArr2[2] = (j2 >>> 31) ^ ((j4 >>> 56) ^ (j4 << 33));
        jArr2[3] = j4 >>> 31;
    }

    /* renamed from: g */
    public static void m19472g(long[] jArr, long[] jArr2) {
        long[] d = Nat256.m19986d();
        m19465c(jArr, d);
        m19469e(d, jArr2);
    }

    /* renamed from: h */
    public static void m19473h(long[] jArr, long[] jArr2) {
        long[] d = Nat256.m19986d();
        m19465c(jArr, d);
        m19464b(jArr2, d, jArr2);
    }

    /* renamed from: e */
    public static void m19469e(long[] jArr, long[] jArr2) {
        long j = jArr[0];
        long j2 = jArr[1];
        long j3 = jArr[2];
        long j4 = jArr[3];
        long j5 = jArr[4];
        long j6 = jArr[5];
        long j7 = jArr[6];
        long j8 = j5 ^ (j7 >>> 50);
        long j9 = (j4 ^ ((j7 >>> 1) ^ (j7 << 14))) ^ (j6 >>> 50);
        long j10 = j ^ (j8 << 63);
        long j11 = (j2 ^ (j6 << 63)) ^ ((j8 >>> 1) ^ (j8 << 14));
        long j12 = ((j3 ^ (j7 << 63)) ^ ((j6 >>> 1) ^ (j6 << 14))) ^ (j8 >>> 50);
        long j13 = j9 >>> 1;
        jArr2[0] = (j10 ^ j13) ^ (j13 << 15);
        jArr2[1] = (j13 >>> 49) ^ j11;
        jArr2[2] = j12;
        jArr2[3] = 1 & j9;
    }

    /* renamed from: a */
    public static void m19460a(long[] jArr, long[] jArr2) {
        jArr2[0] = jArr[0] ^ 1;
        jArr2[1] = jArr[1];
        jArr2[2] = jArr[2];
        jArr2[3] = jArr[3];
    }

    /* renamed from: b */
    protected static void m19463b(long[] jArr, long[] jArr2) {
        long j = jArr[0];
        long j2 = jArr[1];
        long j3 = jArr[2];
        long j4 = jArr[3];
        jArr2[0] = j & 562949953421311L;
        jArr2[1] = ((j >>> 49) ^ (j2 << 15)) & 562949953421311L;
        jArr2[2] = ((j2 >>> 34) ^ (j3 << 30)) & 562949953421311L;
        jArr2[3] = (j3 >>> 19) ^ (j4 << 45);
    }

    /* renamed from: a */
    public static long[] m19462a(BigInteger bigInteger) {
        long[] b = Nat256.m19975b(bigInteger);
        m19458a(b, 0);
        return b;
    }

    /* renamed from: a */
    public static void m19458a(long[] jArr, int i) {
        int i2 = i + 3;
        long j = jArr[i2];
        long j2 = j >>> 1;
        jArr[i] = jArr[i] ^ ((j2 << 15) ^ j2);
        int i3 = i + 1;
        jArr[i3] = (j2 >>> 49) ^ jArr[i3];
        jArr[i2] = j & 1;
    }

    /* renamed from: a */
    public static void m19459a(long[] jArr, int i, long[] jArr2) {
        long[] d = Nat256.m19986d();
        m19465c(jArr, d);
        m19469e(d, jArr2);
        while (true) {
            i--;
            if (i > 0) {
                m19465c(jArr2, d);
                m19469e(d, jArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    protected static void m19457a(long[] jArr) {
        long j = jArr[0];
        long j2 = jArr[1];
        long j3 = jArr[2];
        long j4 = jArr[3];
        long j5 = jArr[4];
        long j6 = jArr[5];
        long j7 = jArr[6];
        long j8 = jArr[7];
        jArr[0] = j ^ (j2 << 49);
        jArr[1] = (j2 >>> 15) ^ (j3 << 34);
        jArr[2] = (j3 >>> 30) ^ (j4 << 19);
        jArr[3] = ((j4 >>> 45) ^ (j5 << 4)) ^ (j6 << 53);
        jArr[4] = ((j5 >>> 60) ^ (j7 << 38)) ^ (j6 >>> 11);
        jArr[5] = (j7 >>> 26) ^ (j8 << 23);
        jArr[6] = j8 >>> 41;
        jArr[7] = 0;
    }

    /* renamed from: c */
    protected static void m19465c(long[] jArr, long[] jArr2) {
        Interleave.m19845a(jArr[0], jArr2, 0);
        Interleave.m19845a(jArr[1], jArr2, 2);
        Interleave.m19845a(jArr[2], jArr2, 4);
        jArr2[6] = jArr[3] & 1;
    }

    /* renamed from: d */
    public static void m19468d(long[] jArr, long[] jArr2, long[] jArr3) {
        long[] d = Nat256.m19986d();
        m19466c(jArr, jArr2, d);
        m19469e(d, jArr3);
    }

    /* renamed from: a */
    protected static void m19456a(long j, long j2, long[] jArr, int i) {
        long j3 = j;
        long[] jArr2 = new long[8];
        jArr2[1] = j2;
        jArr2[2] = jArr2[1] << 1;
        jArr2[3] = jArr2[2] ^ j2;
        jArr2[4] = jArr2[2] << 1;
        jArr2[5] = jArr2[4] ^ j2;
        jArr2[6] = jArr2[3] << 1;
        jArr2[7] = jArr2[6] ^ j2;
        int i2 = (int) j3;
        long j4 = (jArr2[(i2 >>> 3) & 7] << 3) ^ jArr2[i2 & 7];
        long j5 = 0;
        int i3 = 36;
        do {
            int i4 = (int) (j3 >>> i3);
            long j6 = (jArr2[(i4 >>> 12) & 7] << 12) ^ (((jArr2[i4 & 7] ^ (jArr2[(i4 >>> 3) & 7] << 3)) ^ (jArr2[(i4 >>> 6) & 7] << 6)) ^ (jArr2[(i4 >>> 9) & 7] << 9));
            j4 ^= j6 << i3;
            j5 ^= j6 >>> (-i3);
            i3 -= 15;
        } while (i3 > 0);
        jArr[i] = jArr[i] ^ (562949953421311L & j4);
        int i5 = i + 1;
        jArr[i5] = jArr[i5] ^ ((j4 >>> 49) ^ (j5 << 15));
    }
}
