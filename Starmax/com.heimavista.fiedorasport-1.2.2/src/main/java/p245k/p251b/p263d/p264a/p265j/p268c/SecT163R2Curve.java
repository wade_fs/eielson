package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.d.a.j.c.m1 */
public class SecT163R2Curve extends ECCurve.C5159a {

    /* renamed from: i */
    protected SecT163R2Point f12331i = new SecT163R2Point(this, null, null);

    public SecT163R2Curve() {
        super(163, 3, 6, 7);
        this.f12223b = mo28235a(BigInteger.valueOf(1));
        this.f12224c = mo28235a(new BigInteger(1, Hex.m20078a("020A601907B8C953CA1481EB10512F78744A3205FD")));
        this.f12225d = new BigInteger(1, Hex.m20078a("040000000000000000000292FE77E70C12A4234C33"));
        this.f12226e = BigInteger.valueOf(2);
        this.f12227f = 6;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECCurve mo28234a() {
        return new SecT163R2Curve();
    }

    /* renamed from: a */
    public boolean mo28245a(int i) {
        return i == 6;
    }

    /* renamed from: h */
    public int mo28256h() {
        return 163;
    }

    /* renamed from: i */
    public ECPoint mo28258i() {
        return this.f12331i;
    }

    /* renamed from: a */
    public ECFieldElement mo28235a(BigInteger bigInteger) {
        return new SecT163FieldElement(bigInteger);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ECPoint mo28239a(ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        return new SecT163R2Point(this, dVar, dVar2, z);
    }
}
