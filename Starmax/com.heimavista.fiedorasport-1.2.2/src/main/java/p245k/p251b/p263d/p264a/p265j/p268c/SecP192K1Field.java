package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat192;

/* renamed from: k.b.d.a.j.c.p */
public class SecP192K1Field {

    /* renamed from: a */
    static final int[] f12341a = {-4553, -2, -1, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12342b = {20729809, 9106, 1, 0, 0, 0, -9106, -3, -1, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12343c = {-20729809, -9107, -2, -1, -1, -1, 9105, 2};

    /* renamed from: a */
    public static void m19483a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat192.m19903a(iArr, iArr2, iArr3) != 0 || (iArr3[5] == -1 && Nat192.m19916b(iArr3, f12341a))) {
            Nat.m20014a(6, 4553, iArr3);
        }
    }

    /* renamed from: b */
    public static void m19486b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] c = Nat192.m19926c();
        Nat192.m19925c(iArr, iArr2, c);
        m19487c(c, iArr3);
    }

    /* renamed from: c */
    public static void m19488c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat192.m19928d(iArr, iArr2, iArr3) != 0 || (iArr3[11] == -1 && Nat.m20047c(12, iArr3, f12342b))) {
            int[] iArr4 = f12343c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(12, iArr3, f12343c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m19489d(int[] iArr, int[] iArr2) {
        int[] c = Nat192.m19926c();
        Nat192.m19924c(iArr, c);
        m19487c(c, iArr2);
    }

    /* renamed from: e */
    public static void m19491e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(6, iArr, 0, iArr2) != 0 || (iArr2[5] == -1 && Nat192.m19916b(iArr2, f12341a))) {
            Nat.m20014a(6, 4553, iArr2);
        }
    }

    /* renamed from: a */
    public static void m19482a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(6, iArr, iArr2) != 0 || (iArr2[5] == -1 && Nat192.m19916b(iArr2, f12341a))) {
            Nat.m20014a(6, 4553, iArr2);
        }
    }

    /* renamed from: b */
    public static void m19485b(int[] iArr, int[] iArr2) {
        if (Nat192.m19914b(iArr)) {
            Nat192.m19929d(iArr2);
        } else {
            Nat192.m19931e(f12341a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m19490d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat192.m19931e(iArr, iArr2, iArr3) != 0) {
            Nat.m20042c(6, 4553, iArr3);
        }
    }

    /* renamed from: c */
    public static void m19487c(int[] iArr, int[] iArr2) {
        if (Nat192.m19899a(4553, Nat192.m19904a(4553, iArr, 6, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[5] == -1 && Nat192.m19916b(iArr2, f12341a))) {
            Nat.m20014a(6, 4553, iArr2);
        }
    }

    /* renamed from: a */
    public static int[] m19484a(BigInteger bigInteger) {
        int[] a = Nat192.m19911a(bigInteger);
        if (a[5] == -1 && Nat192.m19916b(a, f12341a)) {
            Nat192.m19927d(f12341a, a);
        }
        return a;
    }

    /* renamed from: a */
    public static void m19480a(int i, int[] iArr) {
        if ((i != 0 && Nat192.m19898a(4553, i, iArr, 0) != 0) || (iArr[5] == -1 && Nat192.m19916b(iArr, f12341a))) {
            Nat.m20014a(6, 4553, iArr);
        }
    }

    /* renamed from: a */
    public static void m19481a(int[] iArr, int i, int[] iArr2) {
        int[] c = Nat192.m19926c();
        Nat192.m19924c(iArr, c);
        m19487c(c, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat192.m19924c(iArr2, c);
                m19487c(c, iArr2);
            } else {
                return;
            }
        }
    }
}
