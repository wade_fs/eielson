package p245k.p251b.p263d.p264a.p265j.p268c;

import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat128;

/* renamed from: k.b.d.a.j.c.d */
public class SecP128R1Point extends ECPoint.C5167b {
    public SecP128R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2) {
        this(cVar, dVar, dVar2, false);
    }

    /* renamed from: a */
    public ECPoint mo28296a(ECPoint fVar) {
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        int[] iArr4;
        if (mo28309j()) {
            return fVar;
        }
        if (fVar.mo28309j()) {
            return this;
        }
        if (this == fVar) {
            return mo28316q();
        }
        ECCurve d = mo28301d();
        SecP128R1FieldElement cVar = (SecP128R1FieldElement) this.f12251b;
        SecP128R1FieldElement cVar2 = (SecP128R1FieldElement) this.f12252c;
        SecP128R1FieldElement cVar3 = (SecP128R1FieldElement) fVar.mo28306h();
        SecP128R1FieldElement cVar4 = (SecP128R1FieldElement) fVar.mo28308i();
        SecP128R1FieldElement cVar5 = (SecP128R1FieldElement) this.f12253d[0];
        SecP128R1FieldElement cVar6 = (SecP128R1FieldElement) fVar.mo28293a(0);
        int[] c = Nat128.m19871c();
        int[] a = Nat128.m19859a();
        int[] a2 = Nat128.m19859a();
        int[] a3 = Nat128.m19859a();
        boolean e = cVar5.mo28273e();
        if (e) {
            iArr2 = cVar3.f12285d;
            iArr = cVar4.f12285d;
        } else {
            SecP128R1Field.m18981d(cVar5.f12285d, a2);
            SecP128R1Field.m18978b(a2, cVar3.f12285d, a);
            SecP128R1Field.m18978b(a2, cVar5.f12285d, a2);
            SecP128R1Field.m18978b(a2, cVar4.f12285d, a2);
            iArr2 = a;
            iArr = a2;
        }
        boolean e2 = cVar6.mo28273e();
        if (e2) {
            iArr4 = cVar.f12285d;
            iArr3 = cVar2.f12285d;
        } else {
            SecP128R1Field.m18981d(cVar6.f12285d, a3);
            SecP128R1Field.m18978b(a3, cVar.f12285d, c);
            SecP128R1Field.m18978b(a3, cVar6.f12285d, a3);
            SecP128R1Field.m18978b(a3, cVar2.f12285d, a3);
            iArr4 = c;
            iArr3 = a3;
        }
        int[] a4 = Nat128.m19859a();
        SecP128R1Field.m18982d(iArr4, iArr2, a4);
        SecP128R1Field.m18982d(iArr3, iArr, a);
        if (!Nat128.m19862b(a4)) {
            SecP128R1Field.m18981d(a4, a2);
            int[] a5 = Nat128.m19859a();
            SecP128R1Field.m18978b(a2, a4, a5);
            SecP128R1Field.m18978b(a2, iArr4, a2);
            SecP128R1Field.m18977b(a5, a5);
            Nat128.m19870c(iArr3, a5, c);
            SecP128R1Field.m18970a(Nat128.m19861b(a2, a2, a5), a5);
            SecP128R1FieldElement cVar7 = new SecP128R1FieldElement(a3);
            SecP128R1Field.m18981d(a, cVar7.f12285d);
            int[] iArr5 = cVar7.f12285d;
            SecP128R1Field.m18982d(iArr5, a5, iArr5);
            SecP128R1FieldElement cVar8 = new SecP128R1FieldElement(a5);
            SecP128R1Field.m18982d(a2, cVar7.f12285d, cVar8.f12285d);
            SecP128R1Field.m18980c(cVar8.f12285d, a, c);
            SecP128R1Field.m18979c(c, cVar8.f12285d);
            SecP128R1FieldElement cVar9 = new SecP128R1FieldElement(a4);
            if (!e) {
                int[] iArr6 = cVar9.f12285d;
                SecP128R1Field.m18978b(iArr6, cVar5.f12285d, iArr6);
            }
            if (!e2) {
                int[] iArr7 = cVar9.f12285d;
                SecP128R1Field.m18978b(iArr7, cVar6.f12285d, iArr7);
            }
            return new SecP128R1Point(d, cVar7, cVar8, new ECFieldElement[]{cVar9}, this.f12254e);
        } else if (Nat128.m19862b(a)) {
            return mo28316q();
        } else {
            return d.mo28258i();
        }
    }

    /* renamed from: m */
    public ECPoint mo28312m() {
        if (mo28309j()) {
            return this;
        }
        return new SecP128R1Point(this.f12250a, this.f12251b, this.f12252c.mo28275g(), this.f12253d, this.f12254e);
    }

    /* renamed from: q */
    public ECPoint mo28316q() {
        if (mo28309j()) {
            return this;
        }
        ECCurve d = mo28301d();
        SecP128R1FieldElement cVar = (SecP128R1FieldElement) this.f12252c;
        if (cVar.mo28274f()) {
            return d.mo28258i();
        }
        SecP128R1FieldElement cVar2 = (SecP128R1FieldElement) this.f12251b;
        SecP128R1FieldElement cVar3 = (SecP128R1FieldElement) this.f12253d[0];
        int[] a = Nat128.m19859a();
        int[] a2 = Nat128.m19859a();
        int[] a3 = Nat128.m19859a();
        SecP128R1Field.m18981d(cVar.f12285d, a3);
        int[] a4 = Nat128.m19859a();
        SecP128R1Field.m18981d(a3, a4);
        boolean e = cVar3.mo28273e();
        int[] iArr = cVar3.f12285d;
        if (!e) {
            SecP128R1Field.m18981d(iArr, a2);
            iArr = a2;
        }
        SecP128R1Field.m18982d(cVar2.f12285d, iArr, a);
        SecP128R1Field.m18974a(cVar2.f12285d, iArr, a2);
        SecP128R1Field.m18978b(a2, a, a2);
        SecP128R1Field.m18970a(Nat128.m19861b(a2, a2, a2), a2);
        SecP128R1Field.m18978b(a3, cVar2.f12285d, a3);
        SecP128R1Field.m18970a(Nat.m20045c(4, a3, 2, 0), a3);
        SecP128R1Field.m18970a(Nat.m20021a(4, a4, 3, 0, a), a);
        SecP128R1FieldElement cVar4 = new SecP128R1FieldElement(a4);
        SecP128R1Field.m18981d(a2, cVar4.f12285d);
        int[] iArr2 = cVar4.f12285d;
        SecP128R1Field.m18982d(iArr2, a3, iArr2);
        int[] iArr3 = cVar4.f12285d;
        SecP128R1Field.m18982d(iArr3, a3, iArr3);
        SecP128R1FieldElement cVar5 = new SecP128R1FieldElement(a3);
        SecP128R1Field.m18982d(a3, cVar4.f12285d, cVar5.f12285d);
        int[] iArr4 = cVar5.f12285d;
        SecP128R1Field.m18978b(iArr4, a2, iArr4);
        int[] iArr5 = cVar5.f12285d;
        SecP128R1Field.m18982d(iArr5, a, iArr5);
        SecP128R1FieldElement cVar6 = new SecP128R1FieldElement(a2);
        SecP128R1Field.m18983e(cVar.f12285d, cVar6.f12285d);
        if (!e) {
            int[] iArr6 = cVar6.f12285d;
            SecP128R1Field.m18978b(iArr6, cVar3.f12285d, iArr6);
        }
        return new SecP128R1Point(d, cVar4, cVar5, new ECFieldElement[]{cVar6}, this.f12254e);
    }

    public SecP128R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, boolean z) {
        super(cVar, dVar, dVar2);
        if ((dVar == null) == (dVar2 != null ? false : true)) {
            this.f12254e = z;
            return;
        }
        throw new IllegalArgumentException("Exactly one of the field elements is null");
    }

    SecP128R1Point(ECCurve cVar, ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement[] dVarArr, boolean z) {
        super(cVar, dVar, dVar2, dVarArr);
        this.f12254e = z;
    }
}
