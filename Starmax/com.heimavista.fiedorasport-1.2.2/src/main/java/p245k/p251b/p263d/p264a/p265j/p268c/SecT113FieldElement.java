package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat128;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.v0 */
public class SecT113FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12367d;

    public SecT113FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 113) {
            throw new IllegalArgumentException("x value invalid for SecT113FieldElement");
        }
        this.f12367d = SecT113Field.m19676a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] b = Nat128.m19865b();
        SecT113Field.m19675a(this.f12367d, ((SecT113FieldElement) dVar).f12367d, b);
        return new SecT113FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12367d;
        long[] jArr2 = ((SecT113FieldElement) dVar).f12367d;
        long[] jArr3 = ((SecT113FieldElement) dVar2).f12367d;
        long[] jArr4 = ((SecT113FieldElement) dVar3).f12367d;
        long[] d = Nat128.m19875d();
        SecT113Field.m19684e(jArr, jArr2, d);
        SecT113Field.m19684e(jArr3, jArr4, d);
        long[] b = Nat128.m19865b();
        SecT113Field.m19681d(d, b);
        return new SecT113FieldElement(b);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 113;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] b = Nat128.m19865b();
        SecT113Field.m19682d(this.f12367d, ((SecT113FieldElement) dVar).f12367d, b);
        return new SecT113FieldElement(b);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat128.m19857a(this.f12367d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT113FieldElement)) {
            return false;
        }
        return Nat128.m19858a(this.f12367d, ((SecT113FieldElement) obj).f12367d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat128.m19864b(this.f12367d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] b = Nat128.m19865b();
        SecT113Field.m19683e(this.f12367d, b);
        return new SecT113FieldElement(b);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12367d, 0, 2) ^ 113009;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] b = Nat128.m19865b();
        SecT113Field.m19685f(this.f12367d, b);
        return new SecT113FieldElement(b);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12367d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat128.m19868c(this.f12367d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] b = Nat128.m19865b();
        SecT113Field.m19679c(this.f12367d, b);
        return new SecT113FieldElement(b);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] b = Nat128.m19865b();
        SecT113Field.m19674a(this.f12367d, b);
        return new SecT113FieldElement(b);
    }

    public SecT113FieldElement() {
        this.f12367d = Nat128.m19865b();
    }

    protected SecT113FieldElement(long[] jArr) {
        this.f12367d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12367d;
        long[] jArr2 = ((SecT113FieldElement) dVar).f12367d;
        long[] jArr3 = ((SecT113FieldElement) dVar2).f12367d;
        long[] d = Nat128.m19875d();
        SecT113Field.m19686g(jArr, d);
        SecT113Field.m19684e(jArr2, jArr3, d);
        long[] b = Nat128.m19865b();
        SecT113Field.m19681d(d, b);
        return new SecT113FieldElement(b);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
