package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECFieldElement;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat320;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.d.a.j.c.f2 */
public class SecT283FieldElement extends ECFieldElement {

    /* renamed from: d */
    protected long[] f12299d;

    public SecT283FieldElement(BigInteger bigInteger) {
        if (bigInteger == null || bigInteger.signum() < 0 || bigInteger.bitLength() > 283) {
            throw new IllegalArgumentException("x value invalid for SecT283FieldElement");
        }
        this.f12299d = SecT283Field.m19115a(bigInteger);
    }

    /* renamed from: a */
    public ECFieldElement mo28263a(ECFieldElement dVar) {
        long[] a = Nat320.m19991a();
        SecT283Field.m19114a(this.f12299d, ((SecT283FieldElement) dVar).f12299d, a);
        return new SecT283FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28268b(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        long[] jArr = this.f12299d;
        long[] jArr2 = ((SecT283FieldElement) dVar).f12299d;
        long[] jArr3 = ((SecT283FieldElement) dVar2).f12299d;
        long[] jArr4 = ((SecT283FieldElement) dVar3).f12299d;
        long[] b = Nat.m20041b(9);
        SecT283Field.m19123e(jArr, jArr2, b);
        SecT283Field.m19123e(jArr3, jArr4, b);
        long[] a = Nat320.m19991a();
        SecT283Field.m19122e(b, a);
        return new SecT283FieldElement(a);
    }

    /* renamed from: c */
    public int mo28269c() {
        return 283;
    }

    /* renamed from: c */
    public ECFieldElement mo28270c(ECFieldElement dVar) {
        long[] a = Nat320.m19991a();
        SecT283Field.m19121d(this.f12299d, ((SecT283FieldElement) dVar).f12299d, a);
        return new SecT283FieldElement(a);
    }

    /* renamed from: d */
    public ECFieldElement mo28272d(ECFieldElement dVar) {
        return mo28263a(super);
    }

    /* renamed from: e */
    public boolean mo28273e() {
        return Nat320.m19989a(this.f12299d);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof SecT283FieldElement)) {
            return false;
        }
        return Nat320.m19990a(this.f12299d, ((SecT283FieldElement) obj).f12299d);
    }

    /* renamed from: f */
    public boolean mo28274f() {
        return Nat320.m19993b(this.f12299d);
    }

    /* renamed from: g */
    public ECFieldElement mo28275g() {
        return super;
    }

    /* renamed from: h */
    public ECFieldElement mo28276h() {
        long[] a = Nat320.m19991a();
        SecT283Field.m19124f(this.f12299d, a);
        return new SecT283FieldElement(a);
    }

    public int hashCode() {
        return Arrays.m20057a(this.f12299d, 0, 5) ^ 2831275;
    }

    /* renamed from: i */
    public ECFieldElement mo28277i() {
        long[] a = Nat320.m19991a();
        SecT283Field.m19125g(this.f12299d, a);
        return new SecT283FieldElement(a);
    }

    /* renamed from: j */
    public boolean mo28278j() {
        return (this.f12299d[0] & 1) != 0;
    }

    /* renamed from: k */
    public BigInteger mo28279k() {
        return Nat320.m19995c(this.f12299d);
    }

    /* renamed from: d */
    public ECFieldElement mo28271d() {
        long[] a = Nat320.m19991a();
        SecT283Field.m19120d(this.f12299d, a);
        return new SecT283FieldElement(a);
    }

    /* renamed from: a */
    public ECFieldElement mo28262a() {
        long[] a = Nat320.m19991a();
        SecT283Field.m19113a(this.f12299d, a);
        return new SecT283FieldElement(a);
    }

    public SecT283FieldElement() {
        this.f12299d = Nat320.m19991a();
    }

    protected SecT283FieldElement(long[] jArr) {
        this.f12299d = jArr;
    }

    /* renamed from: a */
    public ECFieldElement mo28265a(ECFieldElement dVar, ECFieldElement dVar2, ECFieldElement dVar3) {
        return mo28268b(super, super, super);
    }

    /* renamed from: a */
    public ECFieldElement mo28264a(ECFieldElement dVar, ECFieldElement dVar2) {
        long[] jArr = this.f12299d;
        long[] jArr2 = ((SecT283FieldElement) dVar).f12299d;
        long[] jArr3 = ((SecT283FieldElement) dVar2).f12299d;
        long[] b = Nat.m20041b(9);
        SecT283Field.m19126h(jArr, b);
        SecT283Field.m19123e(jArr2, jArr3, b);
        long[] a = Nat320.m19991a();
        SecT283Field.m19122e(b, a);
        return new SecT283FieldElement(a);
    }

    /* renamed from: b */
    public ECFieldElement mo28267b(ECFieldElement dVar) {
        return mo28270c(super.mo28271d());
    }
}
