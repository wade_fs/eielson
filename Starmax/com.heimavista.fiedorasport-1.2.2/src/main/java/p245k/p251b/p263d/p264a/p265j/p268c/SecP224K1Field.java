package p245k.p251b.p263d.p264a.p265j.p268c;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat224;

/* renamed from: k.b.d.a.j.c.x */
public class SecP224K1Field {

    /* renamed from: a */
    static final int[] f12373a = {-6803, -2, -1, -1, -1, -1, -1};

    /* renamed from: b */
    static final int[] f12374b = {46280809, 13606, 1, 0, 0, 0, 0, -13606, -3, -1, -1, -1, -1, -1};

    /* renamed from: c */
    private static final int[] f12375c = {-46280809, -13607, -2, -1, -1, -1, -1, 13605, 2};

    /* renamed from: a */
    public static void m19773a(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat224.m19935a(iArr, iArr2, iArr3) != 0 || (iArr3[6] == -1 && Nat224.m19947c(iArr3, f12373a))) {
            Nat.m20014a(7, 6803, iArr3);
        }
    }

    /* renamed from: b */
    public static void m19776b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] b = Nat224.m19944b();
        Nat224.m19946c(iArr, iArr2, b);
        m19777c(b, iArr3);
    }

    /* renamed from: c */
    public static void m19778c(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat224.m19948d(iArr, iArr2, iArr3) != 0 || (iArr3[13] == -1 && Nat.m20047c(14, iArr3, f12374b))) {
            int[] iArr4 = f12375c;
            if (Nat.m20025a(iArr4.length, iArr4, iArr3) != 0) {
                Nat.m20036b(14, iArr3, f12375c.length);
            }
        }
    }

    /* renamed from: d */
    public static void m19779d(int[] iArr, int[] iArr2) {
        int[] b = Nat224.m19944b();
        Nat224.m19950d(iArr, b);
        m19777c(b, iArr2);
    }

    /* renamed from: e */
    public static void m19781e(int[] iArr, int[] iArr2) {
        if (Nat.m20023a(7, iArr, 0, iArr2) != 0 || (iArr2[6] == -1 && Nat224.m19947c(iArr2, f12373a))) {
            Nat.m20014a(7, 6803, iArr2);
        }
    }

    /* renamed from: a */
    public static void m19772a(int[] iArr, int[] iArr2) {
        if (Nat.m20050d(7, iArr, iArr2) != 0 || (iArr2[6] == -1 && Nat224.m19947c(iArr2, f12373a))) {
            Nat.m20014a(7, 6803, iArr2);
        }
    }

    /* renamed from: b */
    public static void m19775b(int[] iArr, int[] iArr2) {
        if (Nat224.m19942b(iArr)) {
            Nat224.m19949d(iArr2);
        } else {
            Nat224.m19952e(f12373a, iArr, iArr2);
        }
    }

    /* renamed from: d */
    public static void m19780d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat224.m19952e(iArr, iArr2, iArr3) != 0) {
            Nat.m20042c(7, 6803, iArr3);
        }
    }

    /* renamed from: c */
    public static void m19777c(int[] iArr, int[] iArr2) {
        if (Nat224.m19933a(6803, Nat224.m19936a(6803, iArr, 7, iArr, 0, iArr2, 0), iArr2, 0) != 0 || (iArr2[6] == -1 && Nat224.m19947c(iArr2, f12373a))) {
            Nat.m20014a(7, 6803, iArr2);
        }
    }

    /* renamed from: a */
    public static int[] m19774a(BigInteger bigInteger) {
        int[] a = Nat224.m19940a(bigInteger);
        if (a[6] == -1 && Nat224.m19947c(a, f12373a)) {
            Nat.m20014a(7, 6803, a);
        }
        return a;
    }

    /* renamed from: a */
    public static void m19770a(int i, int[] iArr) {
        if ((i != 0 && Nat224.m19932a(6803, i, iArr, 0) != 0) || (iArr[6] == -1 && Nat224.m19947c(iArr, f12373a))) {
            Nat.m20014a(7, 6803, iArr);
        }
    }

    /* renamed from: a */
    public static void m19771a(int[] iArr, int i, int[] iArr2) {
        int[] b = Nat224.m19944b();
        Nat224.m19950d(iArr, b);
        m19777c(b, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat224.m19950d(iArr2, b);
                m19777c(b, iArr2);
            } else {
                return;
            }
        }
    }
}
