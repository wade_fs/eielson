package p245k.p251b.p263d.p264a.p265j.p266a;

import java.math.BigInteger;
import p245k.p251b.p263d.p271c.Nat;
import p245k.p251b.p263d.p271c.Nat256;

/* renamed from: k.b.d.a.j.a.b */
public class Curve25519Field {

    /* renamed from: a */
    static final int[] f12260a = {-19, -1, -1, -1, -1, -1, -1, Integer.MAX_VALUE};

    /* renamed from: b */
    private static final int[] f12261b = {361, 0, 0, 0, 0, 0, 0, 0, -19, -1, -1, -1, -1, -1, -1, 1073741823};

    /* renamed from: a */
    public static void m18854a(int[] iArr, int[] iArr2, int[] iArr3) {
        Nat256.m19960a(iArr, iArr2, iArr3);
        if (Nat256.m19981c(iArr3, f12260a)) {
            m18859c(iArr3);
        }
    }

    /* renamed from: b */
    public static void m18858b(int[] iArr, int[] iArr2, int[] iArr3) {
        int[] c = Nat256.m19982c();
        Nat256.m19980c(iArr, iArr2, c);
        m18860c(c, iArr3);
    }

    /* renamed from: c */
    public static void m18861c(int[] iArr, int[] iArr2, int[] iArr3) {
        Nat256.m19983d(iArr, iArr2, iArr3);
        if (Nat.m20047c(16, iArr3, f12261b)) {
            m18856b(iArr3);
        }
    }

    /* renamed from: d */
    public static void m18862d(int[] iArr, int[] iArr2) {
        int[] c = Nat256.m19982c();
        Nat256.m19985d(iArr, c);
        m18860c(c, iArr2);
    }

    /* renamed from: e */
    public static void m18864e(int[] iArr, int[] iArr2) {
        Nat.m20023a(8, iArr, 0, iArr2);
        if (Nat256.m19981c(iArr2, f12260a)) {
            m18859c(iArr2);
        }
    }

    /* renamed from: a */
    public static void m18853a(int[] iArr, int[] iArr2) {
        Nat.m20050d(8, iArr, iArr2);
        if (Nat256.m19981c(iArr2, f12260a)) {
            m18859c(iArr2);
        }
    }

    /* renamed from: b */
    public static void m18857b(int[] iArr, int[] iArr2) {
        if (Nat256.m19970b(iArr)) {
            Nat256.m19984d(iArr2);
        } else {
            Nat256.m19988e(f12260a, iArr, iArr2);
        }
    }

    /* renamed from: c */
    public static void m18860c(int[] iArr, int[] iArr2) {
        int i = iArr[7];
        Nat.m20022a(8, iArr, 8, i, iArr2, 0);
        int i2 = iArr2[7];
        iArr2[7] = (i2 & Integer.MAX_VALUE) + Nat.m20034b(7, ((Nat256.m19955a(19, iArr, iArr2) << 1) + ((i2 >>> 31) - (i >>> 31))) * 19, iArr2);
        if (Nat256.m19981c(iArr2, f12260a)) {
            m18859c(iArr2);
        }
    }

    /* renamed from: d */
    public static void m18863d(int[] iArr, int[] iArr2, int[] iArr3) {
        if (Nat256.m19988e(iArr, iArr2, iArr3) != 0) {
            m18850a(iArr3);
        }
    }

    /* renamed from: a */
    public static int[] m18855a(BigInteger bigInteger) {
        int[] a = Nat256.m19967a(bigInteger);
        while (Nat256.m19981c(a, f12260a)) {
            Nat256.m19987e(f12260a, a);
        }
        return a;
    }

    /* renamed from: b */
    private static int m18856b(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) - (((long) f12261b[0]) & 4294967295L);
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            j2 = (long) Nat.m20018a(8, iArr, 1);
        }
        long j3 = j2 + (((long) iArr[8]) & 4294967295L) + 19;
        iArr[8] = (int) j3;
        long j4 = j3 >> 32;
        if (j4 != 0) {
            j4 = (long) Nat.m20036b(15, iArr, 9);
        }
        long j5 = j4 + ((((long) iArr[15]) & 4294967295L) - (4294967295L & ((long) (f12261b[15] + 1))));
        iArr[15] = (int) j5;
        return (int) (j5 >> 32);
    }

    /* renamed from: a */
    public static void m18851a(int i, int[] iArr) {
        int i2 = iArr[7];
        iArr[7] = (i2 & Integer.MAX_VALUE) + Nat.m20034b(7, ((i << 1) | (i2 >>> 31)) * 19, iArr);
        if (Nat256.m19981c(iArr, f12260a)) {
            m18859c(iArr);
        }
    }

    /* renamed from: c */
    private static int m18859c(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) + 19;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            j2 = (long) Nat.m20036b(7, iArr, 1);
        }
        long j3 = j2 + ((4294967295L & ((long) iArr[7])) - 2147483648L);
        iArr[7] = (int) j3;
        return (int) (j3 >> 32);
    }

    /* renamed from: a */
    public static void m18852a(int[] iArr, int i, int[] iArr2) {
        int[] c = Nat256.m19982c();
        Nat256.m19985d(iArr, c);
        m18860c(c, iArr2);
        while (true) {
            i--;
            if (i > 0) {
                Nat256.m19985d(iArr2, c);
                m18860c(c, iArr2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    private static int m18850a(int[] iArr) {
        long j = (((long) iArr[0]) & 4294967295L) - 19;
        iArr[0] = (int) j;
        long j2 = j >> 32;
        if (j2 != 0) {
            j2 = (long) Nat.m20018a(7, iArr, 1);
        }
        long j3 = j2 + (4294967295L & ((long) iArr[7])) + 2147483648L;
        iArr[7] = (int) j3;
        return (int) (j3 >> 32);
    }
}
