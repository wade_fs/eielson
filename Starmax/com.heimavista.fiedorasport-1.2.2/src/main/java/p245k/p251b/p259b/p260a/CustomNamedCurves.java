package p245k.p251b.p259b.p260a;

import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import java.math.BigInteger;
import java.util.Hashtable;
import java.util.Vector;
import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p254v.GMObjectIdentifiers;
import p245k.p251b.p252a.p256x.SECObjectIdentifiers;
import p245k.p251b.p252a.p258z.X9ECParameters;
import p245k.p251b.p252a.p258z.X9ECParametersHolder;
import p245k.p251b.p252a.p258z.X9ECPoint;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.p265j.p266a.Curve25519;
import p245k.p251b.p263d.p264a.p265j.p267b.SM2P256V1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP128R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP160K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP160R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP160R2Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP192K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP192R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP224K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP224R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP256K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP256R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP384R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecP521R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT113R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT113R2Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT131R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT131R2Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT163K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT163R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT163R2Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT193R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT193R2Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT233K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT233R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT239K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT283K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT283R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT409K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT409R1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT571K1Curve;
import p245k.p251b.p263d.p264a.p265j.p268c.SecT571R1Curve;
import p245k.p251b.p263d.p264a.p269k.GLVTypeBEndomorphism;
import p245k.p251b.p263d.p264a.p269k.GLVTypeBParameters;
import p245k.p251b.p272e.Strings;
import p245k.p251b.p272e.p273h.Hex;

/* renamed from: k.b.b.a.a */
public class CustomNamedCurves {

    /* renamed from: A */
    static X9ECParametersHolder f12179A = new C5150s();

    /* renamed from: B */
    static X9ECParametersHolder f12180B = new C5151t();

    /* renamed from: C */
    static X9ECParametersHolder f12181C = new C5152u();

    /* renamed from: D */
    static X9ECParametersHolder f12182D = new C5154w();

    /* renamed from: E */
    static X9ECParametersHolder f12183E = new C5155x();

    /* renamed from: F */
    static X9ECParametersHolder f12184F = new C5156y();

    /* renamed from: G */
    static final Hashtable f12185G = new Hashtable();

    /* renamed from: H */
    static final Hashtable f12186H = new Hashtable();

    /* renamed from: I */
    static final Hashtable f12187I = new Hashtable();

    /* renamed from: J */
    static final Hashtable f12188J = new Hashtable();

    /* renamed from: K */
    static final Vector f12189K = new Vector();

    /* renamed from: a */
    static X9ECParametersHolder f12190a = new C5142k();

    /* renamed from: b */
    static X9ECParametersHolder f12191b = new C5153v();

    /* renamed from: c */
    static X9ECParametersHolder f12192c = new C5157z();

    /* renamed from: d */
    static X9ECParametersHolder f12193d = new C5127a0();

    /* renamed from: e */
    static X9ECParametersHolder f12194e = new C5129b0();

    /* renamed from: f */
    static X9ECParametersHolder f12195f = new C5131c0();

    /* renamed from: g */
    static X9ECParametersHolder f12196g = new C5133d0();

    /* renamed from: h */
    static X9ECParametersHolder f12197h = new C5135e0();

    /* renamed from: i */
    static X9ECParametersHolder f12198i = new C5137f0();

    /* renamed from: j */
    static X9ECParametersHolder f12199j = new C5126a();

    /* renamed from: k */
    static X9ECParametersHolder f12200k = new C5128b();

    /* renamed from: l */
    static X9ECParametersHolder f12201l = new C5130c();

    /* renamed from: m */
    static X9ECParametersHolder f12202m = new C5132d();

    /* renamed from: n */
    static X9ECParametersHolder f12203n = new C5134e();

    /* renamed from: o */
    static X9ECParametersHolder f12204o = new C5136f();

    /* renamed from: p */
    static X9ECParametersHolder f12205p = new C5138g();

    /* renamed from: q */
    static X9ECParametersHolder f12206q = new C5139h();

    /* renamed from: r */
    static X9ECParametersHolder f12207r = new C5140i();

    /* renamed from: s */
    static X9ECParametersHolder f12208s = new C5141j();

    /* renamed from: t */
    static X9ECParametersHolder f12209t = new C5143l();

    /* renamed from: u */
    static X9ECParametersHolder f12210u = new C5144m();

    /* renamed from: v */
    static X9ECParametersHolder f12211v = new C5145n();

    /* renamed from: w */
    static X9ECParametersHolder f12212w = new C5146o();

    /* renamed from: x */
    static X9ECParametersHolder f12213x = new C5147p();

    /* renamed from: y */
    static X9ECParametersHolder f12214y = new C5148q();

    /* renamed from: z */
    static X9ECParametersHolder f12215z = new C5149r();

    /* renamed from: k.b.b.a.a$a */
    /* compiled from: CustomNamedCurves */
    static class C5126a extends X9ECParametersHolder {
        C5126a() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            ECCurve a = CustomNamedCurves.m18612b(new SecP256K1Curve(), new GLVTypeBParameters(new BigInteger("7ae96a2b657c07106e64479eac3434e99cf0497512f58995c1396c28719501ee", 16), new BigInteger("5363ad4cc05c30e0a5261c028812645a122e22ea20816678df02967c1b23bd72", 16), new BigInteger[]{new BigInteger("3086d221a7d46bcde86c90e49284eb15", 16), new BigInteger("-e4437ed6010e88286f547fa90abfe4c3", 16)}, new BigInteger[]{new BigInteger("114ca50f7a8e2f3f657c1108d9d44cfd8", 16), new BigInteger("3086d221a7d46bcde86c90e49284eb15", 16)}, new BigInteger("3086d221a7d46bcde86c90e49284eb153dab", 16), new BigInteger("e4437ed6010e88286f547fa90abfe4c42212", 16), 272));
            return new X9ECParameters(a, new X9ECPoint(a, Hex.m20078a("0479BE667EF9DCBBAC55A06295CE870B07029BFCDB2DCE28D959F2815B16F81798483ADA7726A3C4655DA4FBFC0E1108A8FD17B448A68554199C47D08FFB10D4B8")), a.mo28259j(), a.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$a0 */
    /* compiled from: CustomNamedCurves */
    static class C5127a0 extends X9ECParametersHolder {
        C5127a0() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("1053CDE42C14D696E67687561517533BF3F83345");
            SecP160R1Curve gVar = new SecP160R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(gVar);
            return new X9ECParameters(gVar, new X9ECPoint(gVar, Hex.m20078a("044A96B5688EF573284664698968C38BB913CBFC8223A628553168947D59DCC912042351377AC5FB32")), gVar.mo28259j(), gVar.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$b */
    /* compiled from: CustomNamedCurves */
    static class C5128b extends X9ECParametersHolder {
        C5128b() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("C49D360886E704936A6678E1139D26B7819F7E90");
            SecP256R1Curve i0Var = new SecP256R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(i0Var);
            return new X9ECParameters(i0Var, new X9ECPoint(i0Var, Hex.m20078a("046B17D1F2E12C4247F8BCE6E563A440F277037D812DEB33A0F4A13945D898C2964FE342E2FE1A7F9B8EE7EB4A7C0F9E162BCE33576B315ECECBB6406837BF51F5")), i0Var.mo28259j(), i0Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$b0 */
    /* compiled from: CustomNamedCurves */
    static class C5129b0 extends X9ECParametersHolder {
        C5129b0() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("B99B99B099B323E02709A4D696E6768756151751");
            SecP160R2Curve kVar = new SecP160R2Curve();
            ECCurve unused = CustomNamedCurves.m18611b(kVar);
            return new X9ECParameters(kVar, new X9ECPoint(kVar, Hex.m20078a("0452DCB034293A117E1F4FF11B30F7199D3144CE6DFEAFFEF2E331F296E071FA0DF9982CFEA7D43F2E")), kVar.mo28259j(), kVar.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$c */
    /* compiled from: CustomNamedCurves */
    static class C5130c extends X9ECParametersHolder {
        C5130c() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("A335926AA319A27A1D00896A6773A4827ACDAC73");
            SecP384R1Curve m0Var = new SecP384R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(m0Var);
            return new X9ECParameters(m0Var, new X9ECPoint(m0Var, Hex.m20078a("04AA87CA22BE8B05378EB1C71EF320AD746E1D3B628BA79B9859F741E082542A385502F25DBF55296C3A545E3872760AB73617DE4A96262C6F5D9E98BF9292DC29F8F41DBD289A147CE9DA3113B5F0B8C00A60B1CE1D7E819D7A431D7C90EA0E5F")), m0Var.mo28259j(), m0Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$c0 */
    /* compiled from: CustomNamedCurves */
    static class C5131c0 extends X9ECParametersHolder {
        C5131c0() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            ECCurve a = CustomNamedCurves.m18612b(new SecP192K1Curve(), new GLVTypeBParameters(new BigInteger("bb85691939b869c1d087f601554b96b80cb4f55b35f433c2", 16), new BigInteger("3d84f26c12238d7b4f3d516613c1759033b1a5800175d0b1", 16), new BigInteger[]{new BigInteger("71169be7330b3038edb025f1", 16), new BigInteger("-b3fb3400dec5c4adceb8655c", 16)}, new BigInteger[]{new BigInteger("12511cfe811d0f4e6bc688b4d", 16), new BigInteger("71169be7330b3038edb025f1", 16)}, new BigInteger("71169be7330b3038edb025f1d0f9", 16), new BigInteger("b3fb3400dec5c4adceb8655d4c94", 16), 208));
            return new X9ECParameters(a, new X9ECPoint(a, Hex.m20078a("04DB4FF10EC057E9AE26B07D0280B7F4341DA5D1B1EAE06C7D9B2F2F6D9C5628A7844163D015BE86344082AA88D95E2F9D")), a.mo28259j(), a.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$d */
    /* compiled from: CustomNamedCurves */
    static class C5132d extends X9ECParametersHolder {
        C5132d() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("D09E8800291CB85396CC6717393284AAA0DA64BA");
            SecP521R1Curve q0Var = new SecP521R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(q0Var);
            return new X9ECParameters(q0Var, new X9ECPoint(q0Var, Hex.m20078a("0400C6858E06B70404E9CD9E3ECB662395B4429C648139053FB521F828AF606B4D3DBAA14B5E77EFE75928FE1DC127A2FFA8DE3348B3C1856A429BF97E7E31C2E5BD66011839296A789A3BC0045C8A5FB42C7D1BD998F54449579B446817AFBD17273E662C97EE72995EF42640C550B9013FAD0761353C7086A272C24088BE94769FD16650")), q0Var.mo28259j(), q0Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$d0 */
    /* compiled from: CustomNamedCurves */
    static class C5133d0 extends X9ECParametersHolder {
        C5133d0() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("3045AE6FC8422F64ED579528D38120EAE12196D5");
            SecP192R1Curve sVar = new SecP192R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(sVar);
            return new X9ECParameters(sVar, new X9ECPoint(sVar, Hex.m20078a("04188DA80EB03090F67CBF20EB43A18800F4FF0AFD82FF101207192B95FFC8DA78631011ED6B24CDD573F977A11E794811")), sVar.mo28259j(), sVar.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$e */
    /* compiled from: CustomNamedCurves */
    static class C5134e extends X9ECParametersHolder {
        C5134e() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("10E723AB14D696E6768756151756FEBF8FCB49A9");
            SecT113R1Curve w0Var = new SecT113R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(w0Var);
            return new X9ECParameters(w0Var, new X9ECPoint(w0Var, Hex.m20078a("04009D73616F35F4AB1407D73562C10F00A52830277958EE84D1315ED31886")), w0Var.mo28259j(), w0Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$e0 */
    /* compiled from: CustomNamedCurves */
    static class C5135e0 extends X9ECParametersHolder {
        C5135e0() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            ECCurve a = CustomNamedCurves.m18612b(new SecP224K1Curve(), new GLVTypeBParameters(new BigInteger("fe0e87005b4e83761908c5131d552a850b3f58b749c37cf5b84d6768", 16), new BigInteger("60dcd2104c4cbc0be6eeefc2bdd610739ec34e317f9b33046c9e4788", 16), new BigInteger[]{new BigInteger("6b8cf07d4ca75c88957d9d670591", 16), new BigInteger("-b8adf1378a6eb73409fa6c9c637d", 16)}, new BigInteger[]{new BigInteger("1243ae1b4d71613bc9f780a03690e", 16), new BigInteger("6b8cf07d4ca75c88957d9d670591", 16)}, new BigInteger("6b8cf07d4ca75c88957d9d67059037a4", 16), new BigInteger("b8adf1378a6eb73409fa6c9c637ba7f5", 16), PsExtractor.VIDEO_STREAM_MASK));
            return new X9ECParameters(a, new X9ECPoint(a, Hex.m20078a("04A1455B334DF099DF30FC28A169A467E9E47075A90F7E650EB6B7A45C7E089FED7FBA344282CAFBD6F7E319F7C0B0BD59E2CA4BDB556D61A5")), a.mo28259j(), a.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$f */
    /* compiled from: CustomNamedCurves */
    static class C5136f extends X9ECParametersHolder {
        C5136f() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("10C0FB15760860DEF1EEF4D696E676875615175D");
            SecT113R2Curve y0Var = new SecT113R2Curve();
            ECCurve unused = CustomNamedCurves.m18611b(y0Var);
            return new X9ECParameters(y0Var, new X9ECPoint(y0Var, Hex.m20078a("0401A57A6A7B26CA5EF52FCDB816479700B3ADC94ED1FE674C06E695BABA1D")), y0Var.mo28259j(), y0Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$f0 */
    /* compiled from: CustomNamedCurves */
    static class C5137f0 extends X9ECParametersHolder {
        C5137f0() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("BD71344799D5C7FCDC45B59FA3B9AB8F6A948BC5");
            SecP224R1Curve a0Var = new SecP224R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(a0Var);
            return new X9ECParameters(a0Var, new X9ECPoint(a0Var, Hex.m20078a("04B70E0CBD6BB4BF7F321390B94A03C1D356C21122343280D6115C1D21BD376388B5F723FB4C22DFE6CD4375A05A07476444D5819985007E34")), a0Var.mo28259j(), a0Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$g */
    /* compiled from: CustomNamedCurves */
    static class C5138g extends X9ECParametersHolder {
        C5138g() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("4D696E676875615175985BD3ADBADA21B43A97E2");
            SecT131R1Curve c1Var = new SecT131R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(c1Var);
            return new X9ECParameters(c1Var, new X9ECPoint(c1Var, Hex.m20078a("040081BAF91FDF9833C40F9C181343638399078C6E7EA38C001F73C8134B1B4EF9E150")), c1Var.mo28259j(), c1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$h */
    /* compiled from: CustomNamedCurves */
    static class C5139h extends X9ECParametersHolder {
        C5139h() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("985BD3ADBAD4D696E676875615175A21B43A97E3");
            SecT131R2Curve e1Var = new SecT131R2Curve();
            ECCurve unused = CustomNamedCurves.m18611b(e1Var);
            return new X9ECParameters(e1Var, new X9ECPoint(e1Var, Hex.m20078a("040356DCD8F2F95031AD652D23951BB366A80648F06D867940A5366D9E265DE9EB240F")), e1Var.mo28259j(), e1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$i */
    /* compiled from: CustomNamedCurves */
    static class C5140i extends X9ECParametersHolder {
        C5140i() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SecT163K1Curve i1Var = new SecT163K1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(i1Var);
            return new X9ECParameters(i1Var, new X9ECPoint(i1Var, Hex.m20078a("0402FE13C0537BBC11ACAA07D793DE4E6D5E5C94EEE80289070FB05D38FF58321F2E800536D538CCDAA3D9")), i1Var.mo28259j(), i1Var.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$j */
    /* compiled from: CustomNamedCurves */
    static class C5141j extends X9ECParametersHolder {
        C5141j() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("24B7B137C8A14D696E6768756151756FD0DA2E5C");
            SecT163R1Curve k1Var = new SecT163R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(k1Var);
            return new X9ECParameters(k1Var, new X9ECPoint(k1Var, Hex.m20078a("040369979697AB43897789566789567F787A7876A65400435EDB42EFAFB2989D51FEFCE3C80988F41FF883")), k1Var.mo28259j(), k1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$k */
    /* compiled from: CustomNamedCurves */
    static class C5142k extends X9ECParametersHolder {
        C5142k() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            Curve25519 aVar = new Curve25519();
            ECCurve unused = CustomNamedCurves.m18611b(aVar);
            return new X9ECParameters(aVar, new X9ECPoint(aVar, Hex.m20078a("042AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD245A20AE19A1B8A086B4E01EDD2C7748D14C923D4D7E6D7C61B229E9C5A27ECED3D9")), aVar.mo28259j(), aVar.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$l */
    /* compiled from: CustomNamedCurves */
    static class C5143l extends X9ECParametersHolder {
        C5143l() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("85E25BFE5C86226CDB12016F7553F9D0E693A268");
            SecT163R2Curve m1Var = new SecT163R2Curve();
            ECCurve unused = CustomNamedCurves.m18611b(m1Var);
            return new X9ECParameters(m1Var, new X9ECPoint(m1Var, Hex.m20078a("0403F0EBA16286A2D57EA0991168D4994637E8343E3600D51FBC6C71A0094FA2CDD545B11C5C0C797324F1")), m1Var.mo28259j(), m1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$m */
    /* compiled from: CustomNamedCurves */
    static class C5144m extends X9ECParametersHolder {
        C5144m() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("103FAEC74D696E676875615175777FC5B191EF30");
            SecT193R1Curve q1Var = new SecT193R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(q1Var);
            return new X9ECParameters(q1Var, new X9ECPoint(q1Var, Hex.m20078a("0401F481BC5F0FF84A74AD6CDF6FDEF4BF6179625372D8C0C5E10025E399F2903712CCF3EA9E3A1AD17FB0B3201B6AF7CE1B05")), q1Var.mo28259j(), q1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$n */
    /* compiled from: CustomNamedCurves */
    static class C5145n extends X9ECParametersHolder {
        C5145n() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("10B7B4D696E676875615175137C8A16FD0DA2211");
            SecT193R2Curve s1Var = new SecT193R2Curve();
            ECCurve unused = CustomNamedCurves.m18611b(s1Var);
            return new X9ECParameters(s1Var, new X9ECPoint(s1Var, Hex.m20078a("0400D9B67D192E0367C803F39E1A7E82CA14A651350AAE617E8F01CE94335607C304AC29E7DEFBD9CA01F596F927224CDECF6C")), s1Var.mo28259j(), s1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$o */
    /* compiled from: CustomNamedCurves */
    static class C5146o extends X9ECParametersHolder {
        C5146o() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SecT233K1Curve w1Var = new SecT233K1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(w1Var);
            return new X9ECParameters(w1Var, new X9ECPoint(w1Var, Hex.m20078a("04017232BA853A7E731AF129F22FF4149563A419C26BF50A4C9D6EEFAD612601DB537DECE819B7F70F555A67C427A8CD9BF18AEB9B56E0C11056FAE6A3")), w1Var.mo28259j(), w1Var.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$p */
    /* compiled from: CustomNamedCurves */
    static class C5147p extends X9ECParametersHolder {
        C5147p() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("74D59FF07F6B413D0EA14B344B20A2DB049B50C3");
            SecT233R1Curve y1Var = new SecT233R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(y1Var);
            return new X9ECParameters(y1Var, new X9ECPoint(y1Var, Hex.m20078a("0400FAC9DFCBAC8313BB2139F1BB755FEF65BC391F8B36F8F8EB7371FD558B01006A08A41903350678E58528BEBF8A0BEFF867A7CA36716F7E01F81052")), y1Var.mo28259j(), y1Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$q */
    /* compiled from: CustomNamedCurves */
    static class C5148q extends X9ECParametersHolder {
        C5148q() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SecT239K1Curve c2Var = new SecT239K1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(c2Var);
            return new X9ECParameters(c2Var, new X9ECPoint(c2Var, Hex.m20078a("0429A0B6A887A983E9730988A68727A8B2D126C44CC2CC7B2A6555193035DC76310804F12E549BDB011C103089E73510ACB275FC312A5DC6B76553F0CA")), c2Var.mo28259j(), c2Var.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$r */
    /* compiled from: CustomNamedCurves */
    static class C5149r extends X9ECParametersHolder {
        C5149r() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SecT283K1Curve g2Var = new SecT283K1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(g2Var);
            return new X9ECParameters(g2Var, new X9ECPoint(g2Var, Hex.m20078a("040503213F78CA44883F1A3B8162F188E553CD265F23C1567A16876913B0C2AC245849283601CCDA380F1C9E318D90F95D07E5426FE87E45C0E8184698E45962364E34116177DD2259")), g2Var.mo28259j(), g2Var.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$s */
    /* compiled from: CustomNamedCurves */
    static class C5150s extends X9ECParametersHolder {
        C5150s() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("77E2B07370EB0F832A6DD5B62DFC88CD06BB84BE");
            SecT283R1Curve i2Var = new SecT283R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(i2Var);
            return new X9ECParameters(i2Var, new X9ECPoint(i2Var, Hex.m20078a("0405F939258DB7DD90E1934F8C70B0DFEC2EED25B8557EAC9C80E2E198F8CDBECD86B1205303676854FE24141CB98FE6D4B20D02B4516FF702350EDDB0826779C813F0DF45BE8112F4")), i2Var.mo28259j(), i2Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$t */
    /* compiled from: CustomNamedCurves */
    static class C5151t extends X9ECParametersHolder {
        C5151t() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SecT409K1Curve m2Var = new SecT409K1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(m2Var);
            return new X9ECParameters(m2Var, new X9ECPoint(m2Var, Hex.m20078a("040060F05F658F49C1AD3AB1890F7184210EFD0987E307C84C27ACCFB8F9F67CC2C460189EB5AAAA62EE222EB1B35540CFE902374601E369050B7C4E42ACBA1DACBF04299C3460782F918EA427E6325165E9EA10E3DA5F6C42E9C55215AA9CA27A5863EC48D8E0286B")), m2Var.mo28259j(), m2Var.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$u */
    /* compiled from: CustomNamedCurves */
    static class C5152u extends X9ECParametersHolder {
        C5152u() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("4099B5A457F9D69F79213D094C4BCD4D4262210B");
            SecT409R1Curve o2Var = new SecT409R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(o2Var);
            return new X9ECParameters(o2Var, new X9ECPoint(o2Var, Hex.m20078a("04015D4860D088DDB3496B0C6064756260441CDE4AF1771D4DB01FFE5B34E59703DC255A868A1180515603AEAB60794E54BB7996A70061B1CFAB6BE5F32BBFA78324ED106A7636B9C5A7BD198D0158AA4F5488D08F38514F1FDF4B4F40D2181B3681C364BA0273C706")), o2Var.mo28259j(), o2Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$v */
    /* compiled from: CustomNamedCurves */
    static class C5153v extends X9ECParametersHolder {
        C5153v() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("000E0D4D696E6768756151750CC03A4473D03679");
            SecP128R1Curve aVar = new SecP128R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(aVar);
            return new X9ECParameters(aVar, new X9ECPoint(aVar, Hex.m20078a("04161FF7528B899B2D0C28607CA52C5B86CF5AC8395BAFEB13C02DA292DDED7A83")), aVar.mo28259j(), aVar.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$w */
    /* compiled from: CustomNamedCurves */
    static class C5154w extends X9ECParametersHolder {
        C5154w() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SecT571K1Curve s2Var = new SecT571K1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(s2Var);
            return new X9ECParameters(s2Var, new X9ECPoint(s2Var, Hex.m20078a("04026EB7A859923FBC82189631F8103FE4AC9CA2970012D5D46024804801841CA44370958493B205E647DA304DB4CEB08CBBD1BA39494776FB988B47174DCA88C7E2945283A01C89720349DC807F4FBF374F4AEADE3BCA95314DD58CEC9F307A54FFC61EFC006D8A2C9D4979C0AC44AEA74FBEBBB9F772AEDCB620B01A7BA7AF1B320430C8591984F601CD4C143EF1C7A3")), s2Var.mo28259j(), s2Var.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$x */
    /* compiled from: CustomNamedCurves */
    static class C5155x extends X9ECParametersHolder {
        C5155x() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            byte[] a = Hex.m20078a("2AA058F73A0E33AB486B0F610410C53A7F132310");
            SecT571R1Curve u2Var = new SecT571R1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(u2Var);
            return new X9ECParameters(u2Var, new X9ECPoint(u2Var, Hex.m20078a("040303001D34B856296C16C0D40D3CD7750A93D1D2955FA80AA5F40FC8DB7B2ABDBDE53950F4C0D293CDD711A35B67FB1499AE60038614F1394ABFA3B4C850D927E1E7769C8EEC2D19037BF27342DA639B6DCCFFFEB73D69D78C6C27A6009CBBCA1980F8533921E8A684423E43BAB08A576291AF8F461BB2A8B3531D2F0485C19B16E2F1516E23DD3C1A4827AF1B8AC15B")), u2Var.mo28259j(), u2Var.mo28252e(), a);
        }
    }

    /* renamed from: k.b.b.a.a$y */
    /* compiled from: CustomNamedCurves */
    static class C5156y extends X9ECParametersHolder {
        C5156y() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            SM2P256V1Curve aVar = new SM2P256V1Curve();
            ECCurve unused = CustomNamedCurves.m18611b(aVar);
            return new X9ECParameters(aVar, new X9ECPoint(aVar, Hex.m20078a("0432C4AE2C1F1981195F9904466A39C9948FE30BBFF2660BE1715A4589334C74C7BC3736A2F4F6779C59BDCEE36B692153D0A9877CC62A474002DF32E52139F0A0")), aVar.mo28259j(), aVar.mo28252e(), null);
        }
    }

    /* renamed from: k.b.b.a.a$z */
    /* compiled from: CustomNamedCurves */
    static class C5157z extends X9ECParametersHolder {
        C5157z() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public X9ECParameters mo28219a() {
            ECCurve a = CustomNamedCurves.m18612b(new SecP160K1Curve(), new GLVTypeBParameters(new BigInteger("9ba48cba5ebcb9b6bd33b92830b2a2e0e192f10a", 16), new BigInteger("c39c6c3b3a36d7701b9c71a1f5804ae5d0003f4", 16), new BigInteger[]{new BigInteger("9162fbe73984472a0a9e", 16), new BigInteger("-96341f1138933bc2f505", 16)}, new BigInteger[]{new BigInteger("127971af8721782ecffa3", 16), new BigInteger("9162fbe73984472a0a9e", 16)}, new BigInteger("9162fbe73984472a0a9d0590", 16), new BigInteger("96341f1138933bc2f503fd44", 16), 176));
            return new X9ECParameters(a, new X9ECPoint(a, Hex.m20078a("043B4C382CE37AA192A4019E763036F4F5DD4D7EBB938CF935318FDCED6BC28286531733C3F03C4FEE")), a.mo28259j(), a.mo28252e(), null);
        }
    }

    static {
        m18610a("curve25519", f12190a);
        m18609a("secp128r1", SECObjectIdentifiers.f12052u, f12191b);
        m18609a("secp160k1", SECObjectIdentifiers.f12041j, f12192c);
        m18609a("secp160r1", SECObjectIdentifiers.f12040i, f12193d);
        m18609a("secp160r2", SECObjectIdentifiers.f12054w, f12194e);
        m18609a("secp192k1", SECObjectIdentifiers.f12055x, f12195f);
        m18609a("secp192r1", SECObjectIdentifiers.f12029G, f12196g);
        m18609a("secp224k1", SECObjectIdentifiers.f12056y, f12197h);
        m18609a("secp224r1", SECObjectIdentifiers.f12057z, f12198i);
        m18609a("secp256k1", SECObjectIdentifiers.f12042k, f12199j);
        m18609a("secp256r1", SECObjectIdentifiers.f12030H, f12200k);
        m18609a("secp384r1", SECObjectIdentifiers.f12023A, f12201l);
        m18609a("secp521r1", SECObjectIdentifiers.f12024B, f12202m);
        m18609a("sect113r1", SECObjectIdentifiers.f12036e, f12203n);
        m18609a("sect113r2", SECObjectIdentifiers.f12037f, f12204o);
        m18609a("sect131r1", SECObjectIdentifiers.f12046o, f12205p);
        m18609a("sect131r2", SECObjectIdentifiers.f12047p, f12206q);
        m18609a("sect163k1", SECObjectIdentifiers.f12033b, f12207r);
        m18609a("sect163r1", SECObjectIdentifiers.f12034c, f12208s);
        m18609a("sect163r2", SECObjectIdentifiers.f12043l, f12209t);
        m18609a("sect193r1", SECObjectIdentifiers.f12048q, f12210u);
        m18609a("sect193r2", SECObjectIdentifiers.f12049r, f12211v);
        m18609a("sect233k1", SECObjectIdentifiers.f12050s, f12212w);
        m18609a("sect233r1", SECObjectIdentifiers.f12051t, f12213x);
        m18609a("sect239k1", SECObjectIdentifiers.f12035d, f12214y);
        m18609a("sect283k1", SECObjectIdentifiers.f12044m, f12215z);
        m18609a("sect283r1", SECObjectIdentifiers.f12045n, f12179A);
        m18609a("sect409k1", SECObjectIdentifiers.f12025C, f12180B);
        m18609a("sect409r1", SECObjectIdentifiers.f12026D, f12181C);
        m18609a("sect571k1", SECObjectIdentifiers.f12027E, f12182D);
        m18609a("sect571r1", SECObjectIdentifiers.f12028F, f12183E);
        m18609a("sm2p256v1", GMObjectIdentifiers.f11981b, f12184F);
        m18608a("B-163", SECObjectIdentifiers.f12043l);
        m18608a("B-233", SECObjectIdentifiers.f12051t);
        m18608a("B-283", SECObjectIdentifiers.f12045n);
        m18608a("B-409", SECObjectIdentifiers.f12026D);
        m18608a("B-571", SECObjectIdentifiers.f12028F);
        m18608a("K-163", SECObjectIdentifiers.f12033b);
        m18608a("K-233", SECObjectIdentifiers.f12050s);
        m18608a("K-283", SECObjectIdentifiers.f12044m);
        m18608a("K-409", SECObjectIdentifiers.f12025C);
        m18608a("K-571", SECObjectIdentifiers.f12027E);
        m18608a("P-192", SECObjectIdentifiers.f12029G);
        m18608a("P-224", SECObjectIdentifiers.f12057z);
        m18608a("P-256", SECObjectIdentifiers.f12030H);
        m18608a("P-384", SECObjectIdentifiers.f12023A);
        m18608a("P-521", SECObjectIdentifiers.f12024B);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static ECCurve m18611b(ECCurve cVar) {
        return cVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static ECCurve m18612b(ECCurve cVar, GLVTypeBParameters dVar) {
        ECCurve.C5161c b = cVar.mo28247b();
        b.mo28260a(new GLVTypeBEndomorphism(cVar, dVar));
        return b.mo28261a();
    }

    /* renamed from: a */
    static void m18610a(String str, X9ECParametersHolder eVar) {
        f12189K.addElement(str);
        f12185G.put(Strings.m20072a(str), eVar);
    }

    /* renamed from: a */
    static void m18609a(String str, ASN1ObjectIdentifier fVar, X9ECParametersHolder eVar) {
        f12189K.addElement(str);
        f12188J.put(fVar, str);
        f12187I.put(fVar, eVar);
        String a = Strings.m20072a(str);
        f12186H.put(a, fVar);
        f12185G.put(a, eVar);
    }

    /* renamed from: a */
    static void m18608a(String str, ASN1ObjectIdentifier fVar) {
        Object obj = f12187I.get(fVar);
        if (obj != null) {
            String a = Strings.m20072a(str);
            f12186H.put(a, fVar);
            f12185G.put(a, obj);
            return;
        }
        throw new IllegalStateException();
    }

    /* renamed from: a */
    public static X9ECParameters m18604a(String str) {
        X9ECParametersHolder eVar = (X9ECParametersHolder) f12185G.get(Strings.m20072a(str));
        if (eVar == null) {
            return null;
        }
        return eVar.mo28225b();
    }

    /* renamed from: a */
    public static X9ECParameters m18605a(ASN1ObjectIdentifier fVar) {
        X9ECParametersHolder eVar = (X9ECParametersHolder) f12187I.get(fVar);
        if (eVar == null) {
            return null;
        }
        return eVar.mo28225b();
    }
}
