package p245k.p251b.p261c;

import p245k.p251b.p252a.ASN1ObjectIdentifier;
import p245k.p251b.p252a.p258z.ECNamedCurveTable;
import p245k.p251b.p252a.p258z.X9ECParameters;
import p245k.p251b.p259b.p260a.CustomNamedCurves;
import p245k.p251b.p261c.p262b.ECNamedCurveParameterSpec;

/* renamed from: k.b.c.a */
/* compiled from: ECNamedCurveTable */
public class C5158a {
    /* renamed from: a */
    public static ECNamedCurveParameterSpec m18645a(String str) {
        X9ECParameters a = CustomNamedCurves.m18604a(str);
        if (a == null) {
            try {
                a = CustomNamedCurves.m18605a(new ASN1ObjectIdentifier(str));
            } catch (IllegalArgumentException unused) {
            }
            if (a == null && (a = ECNamedCurveTable.m18560a(str)) == null) {
                try {
                    a = ECNamedCurveTable.m18561a(new ASN1ObjectIdentifier(str));
                } catch (IllegalArgumentException unused2) {
                }
            }
        }
        if (a == null) {
            return null;
        }
        return new ECNamedCurveParameterSpec(str, a.mo28220b(), a.mo28221c(), a.mo28223e(), a.mo28222d(), a.mo28224f());
    }
}
