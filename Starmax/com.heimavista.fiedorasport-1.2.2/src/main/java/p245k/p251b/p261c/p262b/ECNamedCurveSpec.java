package p245k.p251b.p261c.p262b;

import java.math.BigInteger;
import java.security.spec.ECField;
import java.security.spec.ECFieldF2m;
import java.security.spec.ECFieldFp;
import java.security.spec.ECParameterSpec;
import java.security.spec.EllipticCurve;
import p245k.p251b.p263d.p264a.ECAlgorithms;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECPoint;
import p245k.p251b.p263d.p270b.FiniteField;
import p245k.p251b.p263d.p270b.Polynomial;
import p245k.p251b.p263d.p270b.PolynomialExtensionField;
import p245k.p251b.p272e.Arrays;

/* renamed from: k.b.c.b.b */
public class ECNamedCurveSpec extends ECParameterSpec {
    public ECNamedCurveSpec(String str, ECCurve cVar, ECPoint fVar, BigInteger bigInteger) {
        super(m18648a(cVar, null), m18647a(fVar), bigInteger, 1);
    }

    /* renamed from: a */
    private static EllipticCurve m18648a(ECCurve cVar, byte[] bArr) {
        return new EllipticCurve(m18646a(cVar.mo28255g()), cVar.mo28250c().mo28279k(), cVar.mo28251d().mo28279k(), bArr);
    }

    /* renamed from: a */
    private static ECField m18646a(FiniteField bVar) {
        if (ECAlgorithms.m18657b(bVar)) {
            return new ECFieldFp(bVar.mo28394c());
        }
        Polynomial a = ((PolynomialExtensionField) bVar).mo28399a();
        int[] a2 = a.mo28395a();
        return new ECFieldF2m(a.mo28396b(), Arrays.m20067c(Arrays.m20062a(a2, 1, a2.length - 1)));
    }

    /* renamed from: a */
    private static java.security.spec.ECPoint m18647a(ECPoint fVar) {
        ECPoint n = fVar.mo28313n();
        return new java.security.spec.ECPoint(n.mo28298b().mo28279k(), n.mo28300c().mo28279k());
    }
}
