package p245k.p251b.p261c.p262b;

import java.math.BigInteger;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECPoint;

/* renamed from: k.b.c.b.a */
public class ECNamedCurveParameterSpec extends ECParameterSpec {
    public ECNamedCurveParameterSpec(String str, ECCurve cVar, ECPoint fVar, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        super(cVar, fVar, bigInteger, bigInteger2, bArr);
    }
}
