package p245k.p251b.p261c.p262b;

import java.math.BigInteger;
import java.security.spec.AlgorithmParameterSpec;
import p245k.p251b.p263d.p264a.ECCurve;
import p245k.p251b.p263d.p264a.ECPoint;

/* renamed from: k.b.c.b.c */
public class ECParameterSpec implements AlgorithmParameterSpec {

    /* renamed from: a */
    private ECCurve f12216a;

    /* renamed from: b */
    private ECPoint f12217b;

    /* renamed from: c */
    private BigInteger f12218c;

    public ECParameterSpec(ECCurve cVar, ECPoint fVar, BigInteger bigInteger, BigInteger bigInteger2, byte[] bArr) {
        this.f12216a = cVar;
        this.f12217b = fVar.mo28313n();
        this.f12218c = bigInteger;
    }

    /* renamed from: a */
    public ECCurve mo28229a() {
        return this.f12216a;
    }

    /* renamed from: b */
    public ECPoint mo28230b() {
        return this.f12217b;
    }

    /* renamed from: c */
    public BigInteger mo28231c() {
        return this.f12218c;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ECParameterSpec)) {
            return false;
        }
        ECParameterSpec cVar = (ECParameterSpec) obj;
        if (!mo28229a().mo28246a(cVar.mo28229a()) || !mo28230b().mo28299b(cVar.mo28230b())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return mo28229a().hashCode() ^ mo28230b().hashCode();
    }
}
