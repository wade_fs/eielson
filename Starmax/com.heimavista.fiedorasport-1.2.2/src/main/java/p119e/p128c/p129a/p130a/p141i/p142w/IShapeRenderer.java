package p119e.p128c.p129a.p130a.p141i.p142w;

import android.graphics.Canvas;
import android.graphics.Paint;
import p119e.p128c.p129a.p130a.p135e.p137b.IScatterDataSet;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.w.a */
public interface IShapeRenderer {
    /* renamed from: a */
    void mo23416a(Canvas canvas, IScatterDataSet kVar, ViewPortHandler jVar, float f, float f2, Paint paint);
}
