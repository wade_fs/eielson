package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.PieRadarChartBase;
import java.util.ArrayList;
import java.util.List;

/* renamed from: e.c.a.a.d.h */
public abstract class PieRadarHighlighter<T extends PieRadarChartBase> implements IHighlighter {

    /* renamed from: a */
    protected T f6954a;

    /* renamed from: b */
    protected List<Highlight> f6955b = new ArrayList();

    public PieRadarHighlighter(T t) {
        this.f6954a = t;
    }

    /* renamed from: a */
    public Highlight mo23231a(float f, float f2) {
        if (this.f6954a.mo13161c(f, f2) > this.f6954a.getRadius()) {
            return null;
        }
        float d = this.f6954a.mo13163d(f, f2);
        T t = this.f6954a;
        if (t instanceof PieChart) {
            d /= t.getAnimator().mo23204b();
        }
        int a = this.f6954a.mo13113a(d);
        if (a < 0 || a >= this.f6954a.getData().mo13388e().mo13417t()) {
            return null;
        }
        return mo23254a(a, f, f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Highlight mo23254a(int i, float f, float f2);
}
