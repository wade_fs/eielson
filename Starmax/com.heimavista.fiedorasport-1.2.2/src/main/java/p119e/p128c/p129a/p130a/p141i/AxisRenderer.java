package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Paint;
import androidx.core.view.ViewCompat;
import com.github.mikephil.charting.components.AxisBase;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.a */
public abstract class AxisRenderer extends Renderer {

    /* renamed from: b */
    protected AxisBase f7004b;

    /* renamed from: c */
    protected Transformer f7005c;

    /* renamed from: d */
    protected Paint f7006d;

    /* renamed from: e */
    protected Paint f7007e;

    /* renamed from: f */
    protected Paint f7008f;

    /* renamed from: g */
    protected Paint f7009g;

    public AxisRenderer(ViewPortHandler jVar, Transformer gVar, AxisBase aVar) {
        super(jVar);
        this.f7005c = gVar;
        this.f7004b = aVar;
        if (super.f7088a != null) {
            this.f7007e = new Paint(1);
            this.f7006d = new Paint();
            this.f7006d.setColor(-7829368);
            this.f7006d.setStrokeWidth(1.0f);
            this.f7006d.setStyle(Paint.Style.STROKE);
            this.f7006d.setAlpha(90);
            this.f7008f = new Paint();
            this.f7008f.setColor((int) ViewCompat.MEASURED_STATE_MASK);
            this.f7008f.setStrokeWidth(1.0f);
            this.f7008f.setStyle(Paint.Style.STROKE);
            this.f7009g = new Paint(1);
            this.f7009g.setStyle(Paint.Style.STROKE);
        }
    }

    /* renamed from: a */
    public Paint mo23334a() {
        return this.f7007e;
    }

    /* renamed from: a */
    public void mo23336a(float f, float f2, boolean z) {
        float f3;
        double d;
        ViewPortHandler jVar = super.f7088a;
        if (jVar != null && jVar.mo23469j() > 10.0f && !super.f7088a.mo23485w()) {
            MPPointD b = this.f7005c.mo23440b(super.f7088a.mo23463g(), super.f7088a.mo23467i());
            MPPointD b2 = this.f7005c.mo23440b(super.f7088a.mo23463g(), super.f7088a.mo23459e());
            if (!z) {
                f3 = (float) b2.f7121S;
                d = b.f7121S;
            } else {
                f3 = (float) b.f7121S;
                d = b2.f7121S;
            }
            MPPointD.m11565a(b);
            MPPointD.m11565a(b2);
            f = f3;
            f2 = (float) d;
        }
        mo23335a(f, f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23335a(float f, float f2) {
        double d;
        double d2;
        float f3 = f;
        float f4 = f2;
        int n = this.f7004b.mo13217n();
        double abs = (double) Math.abs(f4 - f3);
        if (n == 0 || abs <= 0.0d || Double.isInfinite(abs)) {
            AxisBase aVar = this.f7004b;
            aVar.f2357l = new float[0];
            aVar.f2358m = new float[0];
            aVar.f2359n = 0;
            return;
        }
        double b = (double) Utils.m11612b(abs / ((double) n));
        if (this.f7004b.mo13228y() && b < ((double) this.f7004b.mo13213j())) {
            b = (double) this.f7004b.mo13213j();
        }
        double b2 = (double) Utils.m11612b(Math.pow(10.0d, (double) ((int) Math.log10(b))));
        if (((int) (b / b2)) > 5) {
            b = Math.floor(b2 * 10.0d);
        }
        int r = this.f7004b.mo13221r();
        if (this.f7004b.mo13227x()) {
            b = (double) (((float) abs) / ((float) (n - 1)));
            AxisBase aVar2 = this.f7004b;
            aVar2.f2359n = n;
            if (aVar2.f2357l.length < n) {
                aVar2.f2357l = new float[n];
            }
            float f5 = f3;
            for (int i = 0; i < n; i++) {
                this.f7004b.f2357l[i] = f5;
                f5 = (float) (((double) f5) + b);
            }
        } else {
            int i2 = (b > 0.0d ? 1 : (b == 0.0d ? 0 : -1));
            if (i2 == 0) {
                d = 0.0d;
            } else {
                d = Math.ceil(((double) f3) / b) * b;
            }
            if (this.f7004b.mo13221r()) {
                d -= b;
            }
            if (i2 == 0) {
                d2 = 0.0d;
            } else {
                d2 = Utils.m11598a(Math.floor(((double) f4) / b) * b);
            }
            if (i2 != 0) {
                for (double d3 = d; d3 <= d2; d3 += b) {
                    r++;
                }
            }
            n = r;
            AxisBase aVar3 = this.f7004b;
            aVar3.f2359n = n;
            if (aVar3.f2357l.length < n) {
                aVar3.f2357l = new float[n];
            }
            for (int i3 = 0; i3 < n; i3++) {
                if (d == 0.0d) {
                    d = 0.0d;
                }
                this.f7004b.f2357l[i3] = (float) d;
                d += b;
            }
        }
        if (b < 1.0d) {
            this.f7004b.f2360o = (int) Math.ceil(-Math.log10(b));
        } else {
            this.f7004b.f2360o = 0;
        }
        if (this.f7004b.mo13221r()) {
            AxisBase aVar4 = this.f7004b;
            if (aVar4.f2358m.length < n) {
                aVar4.f2358m = new float[n];
            }
            float f6 = ((float) b) / 2.0f;
            for (int i4 = 0; i4 < n; i4++) {
                AxisBase aVar5 = this.f7004b;
                aVar5.f2358m[i4] = aVar5.f2357l[i4] + f6;
            }
        }
    }
}
