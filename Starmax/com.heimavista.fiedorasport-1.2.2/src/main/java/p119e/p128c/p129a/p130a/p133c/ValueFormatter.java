package p119e.p128c.p129a.p130a.p133c;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.BubbleEntry;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.data.RadarEntry;

/* renamed from: e.c.a.a.c.g */
public abstract class ValueFormatter implements IAxisValueFormatter, IValueFormatter {
    /* renamed from: a */
    public String mo23217a(float f) {
        return String.valueOf(f);
    }

    /* renamed from: a */
    public String mo23221a(float f, AxisBase aVar) {
        return mo23217a(f);
    }

    /* renamed from: a */
    public String mo23223a(BarEntry barEntry) {
        return mo23217a(barEntry.mo13303c());
    }

    /* renamed from: a */
    public String mo23222a(float f, BarEntry barEntry) {
        return mo23217a(f);
    }

    /* renamed from: a */
    public String mo23226a(Entry entry) {
        return mo23217a(entry.mo13303c());
    }

    /* renamed from: a */
    public String mo23220a(float f, PieEntry pieEntry) {
        return mo23217a(f);
    }

    /* renamed from: a */
    public String mo23227a(RadarEntry radarEntry) {
        return mo23217a(radarEntry.mo13303c());
    }

    /* renamed from: a */
    public String mo23224a(BubbleEntry bubbleEntry) {
        return mo23217a(bubbleEntry.mo13310e());
    }

    /* renamed from: a */
    public String mo23225a(CandleEntry candleEntry) {
        return mo23217a(candleEntry.mo13312f());
    }
}
