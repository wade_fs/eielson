package p119e.p128c.p129a.p130a.p135e.p137b;

import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p141i.p142w.IShapeRenderer;

/* renamed from: e.c.a.a.e.b.k */
public interface IScatterDataSet extends ILineScatterCandleRadarDataSet<Entry> {
    /* renamed from: e0 */
    float mo23298e0();

    /* renamed from: g0 */
    IShapeRenderer mo23299g0();
}
