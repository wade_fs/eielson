package p119e.p128c.p129a.p130a.p135e.p137b;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineDataSet;
import p119e.p128c.p129a.p130a.p133c.IFillFormatter;

/* renamed from: e.c.a.a.e.b.f */
public interface ILineDataSet extends ILineRadarDataSet<Entry> {
    /* renamed from: I */
    int mo23270I();

    /* renamed from: M */
    IFillFormatter mo23271M();

    /* renamed from: P */
    boolean mo23272P();

    /* renamed from: Q */
    int mo23273Q();

    /* renamed from: S */
    float mo23274S();

    /* renamed from: T */
    DashPathEffect mo23275T();

    /* renamed from: U */
    float mo23276U();

    /* renamed from: V */
    LineDataSet mo23277V();

    /* renamed from: W */
    boolean mo23278W();

    /* renamed from: X */
    float mo23279X();

    /* renamed from: Y */
    boolean mo23280Y();

    /* renamed from: f */
    int mo23281f(int i);
}
