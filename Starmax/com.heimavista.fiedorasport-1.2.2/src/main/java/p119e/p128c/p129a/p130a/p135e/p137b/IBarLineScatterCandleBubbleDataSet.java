package p119e.p128c.p129a.p130a.p135e.p137b;

import com.github.mikephil.charting.data.Entry;

/* renamed from: e.c.a.a.e.b.b */
public interface IBarLineScatterCandleBubbleDataSet<T extends Entry> extends IDataSet<T> {
    /* renamed from: x */
    int mo13335x();
}
