package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import p119e.p128c.p129a.p130a.p143j.Transformer;

/* renamed from: e.c.a.a.e.a.b */
public interface BarLineScatterCandleBubbleDataProvider extends ChartInterface {
    /* renamed from: a */
    Transformer mo12952a(YAxis.C1655a aVar);

    /* renamed from: b */
    boolean mo12956b(YAxis.C1655a aVar);

    BarLineScatterCandleBubbleData getData();

    float getHighestVisibleX();

    float getLowestVisibleX();
}
