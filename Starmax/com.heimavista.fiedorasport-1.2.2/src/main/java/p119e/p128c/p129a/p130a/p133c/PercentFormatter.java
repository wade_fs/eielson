package p119e.p128c.p129a.p130a.p133c;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieEntry;
import java.text.DecimalFormat;

/* renamed from: e.c.a.a.c.f */
public class PercentFormatter extends ValueFormatter {

    /* renamed from: a */
    public DecimalFormat f6939a = new DecimalFormat("###,###,##0.0");

    /* renamed from: b */
    private PieChart f6940b;

    /* renamed from: a */
    public String mo23217a(float f) {
        return this.f6939a.format((double) f) + " %";
    }

    /* renamed from: a */
    public String mo23220a(float f, PieEntry pieEntry) {
        PieChart pieChart = this.f6940b;
        if (pieChart == null || !pieChart.mo13158v()) {
            return this.f6939a.format((double) f);
        }
        return mo23217a(f);
    }
}
