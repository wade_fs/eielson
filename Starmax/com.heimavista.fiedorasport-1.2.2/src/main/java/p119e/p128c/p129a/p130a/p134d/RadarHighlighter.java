package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.RadarData;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: e.c.a.a.d.i */
public class RadarHighlighter extends PieRadarHighlighter<RadarChart> {
    public RadarHighlighter(RadarChart radarChart) {
        super(radarChart);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Highlight mo23254a(int i, float f, float f2) {
        List<Highlight> a = mo23255a(i);
        float c = ((RadarChart) super.f6954a).mo13161c(f, f2) / ((RadarChart) super.f6954a).getFactor();
        Highlight dVar = null;
        float f3 = Float.MAX_VALUE;
        for (int i2 = 0; i2 < a.size(); i2++) {
            Highlight dVar2 = a.get(i2);
            float abs = Math.abs(dVar2.mo23251i() - c);
            if (abs < f3) {
                dVar = dVar2;
                f3 = abs;
            }
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<Highlight> mo23255a(int i) {
        int i2 = i;
        super.f6955b.clear();
        float a = ((RadarChart) super.f6954a).getAnimator().mo23201a();
        float b = ((RadarChart) super.f6954a).getAnimator().mo23204b();
        float sliceAngle = ((RadarChart) super.f6954a).getSliceAngle();
        float factor = ((RadarChart) super.f6954a).getFactor();
        MPPointF a2 = MPPointF.m11567a(0.0f, 0.0f);
        int i3 = 0;
        while (i3 < ((RadarData) ((RadarChart) super.f6954a).getData()).mo13383b()) {
            IDataSet a3 = ((RadarData) ((RadarChart) super.f6954a).getData()).mo13374a(i3);
            Entry b2 = a3.mo13410b(i2);
            float f = (float) i2;
            Utils.m11611a(((RadarChart) super.f6954a).getCenterOffsets(), (b2.mo13303c() - ((RadarChart) super.f6954a).getYChartMin()) * factor * b, (sliceAngle * f * a) + ((RadarChart) super.f6954a).getRotationAngle(), a2);
            List<Highlight> list = super.f6955b;
            Highlight dVar = r8;
            Highlight dVar2 = new Highlight(f, b2.mo13303c(), a2.f7123R, a2.f7124S, i3, a3.mo13364s());
            list.add(dVar);
            i3++;
            i2 = i;
        }
        return super.f6955b;
    }
}
