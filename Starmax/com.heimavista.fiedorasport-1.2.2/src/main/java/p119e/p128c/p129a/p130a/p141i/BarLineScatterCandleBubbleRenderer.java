package p119e.p128c.p129a.p130a.p141i;

import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p135e.p136a.BarLineScatterCandleBubbleDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.c */
public abstract class BarLineScatterCandleBubbleRenderer extends DataRenderer {

    /* renamed from: f */
    protected C3869a f7016f = new C3869a();

    /* renamed from: e.c.a.a.i.c$a */
    /* compiled from: BarLineScatterCandleBubbleRenderer */
    protected class C3869a {

        /* renamed from: a */
        public int f7017a;

        /* renamed from: b */
        public int f7018b;

        /* renamed from: c */
        public int f7019c;

        protected C3869a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.max(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.max(double, double):double}
          ClspMth{java.lang.Math.max(int, int):int}
          ClspMth{java.lang.Math.max(long, long):long}
          ClspMth{java.lang.Math.max(float, float):float} */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        /* renamed from: a */
        public void mo23348a(BarLineScatterCandleBubbleDataProvider bVar, IBarLineScatterCandleBubbleDataSet bVar2) {
            int i;
            float max = Math.max(0.0f, Math.min(1.0f, BarLineScatterCandleBubbleRenderer.this.f7035b.mo23201a()));
            float lowestVisibleX = bVar.getLowestVisibleX();
            float highestVisibleX = bVar.getHighestVisibleX();
            Entry a = bVar2.mo13407a(lowestVisibleX, Float.NaN, DataSet.C1658a.DOWN);
            Entry a2 = bVar2.mo13407a(highestVisibleX, Float.NaN, DataSet.C1658a.UP);
            int i2 = 0;
            if (a == null) {
                i = 0;
            } else {
                i = bVar2.mo13405a(a);
            }
            this.f7017a = i;
            if (a2 != null) {
                i2 = bVar2.mo13405a(a2);
            }
            this.f7018b = i2;
            this.f7019c = (int) (((float) (this.f7018b - this.f7017a)) * max);
        }
    }

    public BarLineScatterCandleBubbleRenderer(ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo23346a(Entry entry, IBarLineScatterCandleBubbleDataSet bVar) {
        if (entry == null) {
            return false;
        }
        float a = (float) bVar.mo13405a(entry);
        if (entry == null || a >= ((float) bVar.mo13417t()) * super.f7035b.mo23201a()) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo23347b(IDataSet eVar) {
        return eVar.isVisible() && (eVar.mo13363r() || eVar.mo13349d());
    }
}
