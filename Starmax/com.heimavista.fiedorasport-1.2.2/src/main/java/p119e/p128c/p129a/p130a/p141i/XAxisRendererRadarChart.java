package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.RadarData;
import p119e.p128c.p129a.p130a.p135e.p137b.IRadarDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.s */
public class XAxisRendererRadarChart extends XAxisRenderer {

    /* renamed from: p */
    private RadarChart f7100p;

    public XAxisRendererRadarChart(ViewPortHandler jVar, XAxis hVar, RadarChart radarChart) {
        super(jVar, hVar, null);
        this.f7100p = radarChart;
    }

    /* renamed from: a */
    public void mo23395a(Canvas canvas) {
        if (super.f7091h.mo13240f() && super.f7091h.mo13225v()) {
            float z = super.f7091h.mo13285z();
            MPPointF a = MPPointF.m11567a(0.5f, 0.25f);
            this.f7007e.setTypeface(super.f7091h.mo13236c());
            this.f7007e.setTextSize(super.f7091h.mo13234b());
            this.f7007e.setColor(super.f7091h.mo13229a());
            float sliceAngle = this.f7100p.getSliceAngle();
            float factor = this.f7100p.getFactor();
            MPPointF centerOffsets = this.f7100p.getCenterOffsets();
            MPPointF a2 = MPPointF.m11567a(0.0f, 0.0f);
            for (int i = 0; i < ((IRadarDataSet) ((RadarData) this.f7100p.getData()).mo13388e()).mo13417t(); i++) {
                float f = (float) i;
                String a3 = super.f7091h.mo13220q().mo23221a(f, super.f7091h);
                Utils.m11611a(centerOffsets, (this.f7100p.getYRange() * factor) + (((float) super.f7091h.f2447L) / 2.0f), ((f * sliceAngle) + this.f7100p.getRotationAngle()) % 360.0f, a2);
                mo23400a(canvas, a3, a2.f7123R, a2.f7124S - (((float) super.f7091h.f2448M) / 2.0f), a, z);
            }
            MPPointF.m11570b(centerOffsets);
            MPPointF.m11570b(a2);
            MPPointF.m11570b(a);
        }
    }

    /* renamed from: d */
    public void mo23406d(Canvas canvas) {
    }
}
