package p119e.p128c.p129a.p130a.p143j;

import android.graphics.Matrix;
import android.graphics.RectF;
import android.view.View;

/* renamed from: e.c.a.a.j.j */
public class ViewPortHandler {

    /* renamed from: a */
    protected final Matrix f7155a = new Matrix();

    /* renamed from: b */
    protected RectF f7156b = new RectF();

    /* renamed from: c */
    protected float f7157c = 0.0f;

    /* renamed from: d */
    protected float f7158d = 0.0f;

    /* renamed from: e */
    private float f7159e = 1.0f;

    /* renamed from: f */
    private float f7160f = Float.MAX_VALUE;

    /* renamed from: g */
    private float f7161g = 1.0f;

    /* renamed from: h */
    private float f7162h = Float.MAX_VALUE;

    /* renamed from: i */
    private float f7163i = 1.0f;

    /* renamed from: j */
    private float f7164j = 1.0f;

    /* renamed from: k */
    private float f7165k = 0.0f;

    /* renamed from: l */
    private float f7166l = 0.0f;

    /* renamed from: m */
    private float f7167m = 0.0f;

    /* renamed from: n */
    private float f7168n = 0.0f;

    /* renamed from: o */
    protected Matrix f7169o = new Matrix();

    /* renamed from: p */
    protected final float[] f7170p = new float[9];

    /* renamed from: A */
    public float mo23443A() {
        return this.f7156b.top;
    }

    /* renamed from: a */
    public void mo23445a(float f, float f2, float f3, float f4) {
        this.f7156b.set(f, f2, this.f7157c - f3, this.f7158d - f4);
    }

    /* renamed from: b */
    public void mo23452b(float f, float f2) {
        float y = mo23487y();
        float A = mo23443A();
        float z = mo23488z();
        float x = mo23486x();
        this.f7158d = f2;
        this.f7157c = f;
        mo23445a(y, A, z, x);
    }

    /* renamed from: c */
    public boolean mo23456c(float f) {
        return this.f7156b.right >= (((float) ((int) (f * 100.0f))) / 100.0f) - 1.0f;
    }

    /* renamed from: d */
    public boolean mo23458d(float f) {
        return this.f7156b.top <= f;
    }

    /* renamed from: e */
    public float mo23459e() {
        return this.f7156b.bottom;
    }

    /* renamed from: f */
    public float mo23461f() {
        return this.f7156b.height();
    }

    /* renamed from: g */
    public float mo23463g() {
        return this.f7156b.left;
    }

    /* renamed from: h */
    public float mo23465h() {
        return this.f7156b.right;
    }

    /* renamed from: i */
    public float mo23467i() {
        return this.f7156b.top;
    }

    /* renamed from: j */
    public float mo23469j() {
        return this.f7156b.width();
    }

    /* renamed from: k */
    public float mo23471k() {
        return this.f7158d;
    }

    /* renamed from: l */
    public float mo23473l() {
        return this.f7157c;
    }

    /* renamed from: m */
    public MPPointF mo23475m() {
        return MPPointF.m11567a(this.f7156b.centerX(), this.f7156b.centerY());
    }

    /* renamed from: n */
    public RectF mo23476n() {
        return this.f7156b;
    }

    /* renamed from: o */
    public Matrix mo23477o() {
        return this.f7155a;
    }

    /* renamed from: p */
    public float mo23478p() {
        return this.f7163i;
    }

    /* renamed from: q */
    public float mo23479q() {
        return this.f7164j;
    }

    /* renamed from: r */
    public float mo23480r() {
        return Math.min(this.f7156b.width(), this.f7156b.height());
    }

    /* renamed from: s */
    public boolean mo23481s() {
        return this.f7158d > 0.0f && this.f7157c > 0.0f;
    }

    /* renamed from: t */
    public boolean mo23482t() {
        return this.f7167m <= 0.0f && this.f7168n <= 0.0f;
    }

    /* renamed from: u */
    public boolean mo23483u() {
        return mo23484v() && mo23485w();
    }

    /* renamed from: v */
    public boolean mo23484v() {
        float f = this.f7163i;
        float f2 = this.f7161g;
        return f <= f2 && f2 <= 1.0f;
    }

    /* renamed from: w */
    public boolean mo23485w() {
        float f = this.f7164j;
        float f2 = this.f7159e;
        return f <= f2 && f2 <= 1.0f;
    }

    /* renamed from: x */
    public float mo23486x() {
        return this.f7158d - this.f7156b.bottom;
    }

    /* renamed from: y */
    public float mo23487y() {
        return this.f7156b.left;
    }

    /* renamed from: z */
    public float mo23488z() {
        return this.f7157c - this.f7156b.right;
    }

    /* renamed from: a */
    public void mo23446a(float f, float f2, float f3, float f4, Matrix matrix) {
        matrix.reset();
        matrix.set(this.f7155a);
        matrix.postScale(f, f2, f3, f4);
    }

    /* renamed from: c */
    public boolean mo23455c() {
        return this.f7163i > this.f7161g;
    }

    /* renamed from: d */
    public boolean mo23457d() {
        return this.f7164j > this.f7159e;
    }

    /* renamed from: e */
    public boolean mo23460e(float f) {
        return mo23454b(f) && mo23456c(f);
    }

    /* renamed from: f */
    public boolean mo23462f(float f) {
        return mo23458d(f) && mo23450a(f);
    }

    /* renamed from: g */
    public void mo23464g(float f) {
        this.f7167m = Utils.m11599a(f);
    }

    /* renamed from: h */
    public void mo23466h(float f) {
        this.f7168n = Utils.m11599a(f);
    }

    /* renamed from: i */
    public void mo23468i(float f) {
        if (f == 0.0f) {
            f = Float.MAX_VALUE;
        }
        this.f7162h = f;
        mo23447a(this.f7155a, this.f7156b);
    }

    /* renamed from: j */
    public void mo23470j(float f) {
        if (f == 0.0f) {
            f = Float.MAX_VALUE;
        }
        this.f7160f = f;
        mo23447a(this.f7155a, this.f7156b);
    }

    /* renamed from: k */
    public void mo23472k(float f) {
        if (f < 1.0f) {
            f = 1.0f;
        }
        this.f7161g = f;
        mo23447a(this.f7155a, this.f7156b);
    }

    /* renamed from: l */
    public void mo23474l(float f) {
        if (f < 1.0f) {
            f = 1.0f;
        }
        this.f7159e = f;
        mo23447a(this.f7155a, this.f7156b);
    }

    /* renamed from: a */
    public void mo23448a(float[] fArr, View view) {
        Matrix matrix = this.f7169o;
        matrix.reset();
        matrix.set(this.f7155a);
        matrix.postTranslate(-(fArr[0] - mo23487y()), -(fArr[1] - mo23443A()));
        mo23444a(matrix, view, true);
    }

    /* renamed from: b */
    public boolean mo23454b(float f) {
        return this.f7156b.left <= f + 1.0f;
    }

    /* renamed from: b */
    public boolean mo23453b() {
        return this.f7164j < this.f7160f;
    }

    /* renamed from: a */
    public Matrix mo23444a(Matrix matrix, View view, boolean z) {
        this.f7155a.set(matrix);
        mo23447a(this.f7155a, this.f7156b);
        if (z) {
            view.invalidate();
        }
        matrix.set(this.f7155a);
        return matrix;
    }

    /* renamed from: a */
    public void mo23447a(Matrix matrix, RectF rectF) {
        float f;
        matrix.getValues(this.f7170p);
        float[] fArr = this.f7170p;
        float f2 = fArr[2];
        float f3 = fArr[0];
        float f4 = fArr[5];
        float f5 = fArr[4];
        this.f7163i = Math.min(Math.max(this.f7161g, f3), this.f7162h);
        this.f7164j = Math.min(Math.max(this.f7159e, f5), this.f7160f);
        float f6 = 0.0f;
        if (rectF != null) {
            f6 = rectF.width();
            f = rectF.height();
        } else {
            f = 0.0f;
        }
        this.f7165k = Math.min(Math.max(f2, ((-f6) * (this.f7163i - 1.0f)) - this.f7167m), this.f7167m);
        this.f7166l = Math.max(Math.min(f4, (f * (this.f7164j - 1.0f)) + this.f7168n), -this.f7168n);
        float[] fArr2 = this.f7170p;
        fArr2[2] = this.f7165k;
        fArr2[0] = this.f7163i;
        fArr2[5] = this.f7166l;
        fArr2[4] = this.f7164j;
        matrix.setValues(fArr2);
    }

    /* renamed from: a */
    public boolean mo23451a(float f, float f2) {
        return mo23460e(f) && mo23462f(f2);
    }

    /* renamed from: a */
    public boolean mo23450a(float f) {
        return this.f7156b.bottom >= ((float) ((int) (f * 100.0f))) / 100.0f;
    }

    /* renamed from: a */
    public boolean mo23449a() {
        return this.f7163i < this.f7162h;
    }
}
