package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.LineDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.ILineDataSet;
import p119e.p128c.p129a.p130a.p141i.BarLineScatterCandleBubbleRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.j */
public class LineChartRenderer extends LineRadarRenderer {

    /* renamed from: h */
    protected LineDataProvider f7050h;

    /* renamed from: i */
    protected Paint f7051i;

    /* renamed from: j */
    protected WeakReference<Bitmap> f7052j;

    /* renamed from: k */
    protected Canvas f7053k;

    /* renamed from: l */
    protected Bitmap.Config f7054l = Bitmap.Config.ARGB_8888;

    /* renamed from: m */
    protected Path f7055m = new Path();

    /* renamed from: n */
    protected Path f7056n = new Path();

    /* renamed from: o */
    private float[] f7057o = new float[4];

    /* renamed from: p */
    protected Path f7058p = new Path();

    /* renamed from: q */
    private HashMap<IDataSet, C3873b> f7059q = new HashMap<>();

    /* renamed from: r */
    private float[] f7060r = new float[2];

    /* renamed from: e.c.a.a.i.j$a */
    /* compiled from: LineChartRenderer */
    static /* synthetic */ class C3872a {

        /* renamed from: a */
        static final /* synthetic */ int[] f7061a = new int[LineDataSet.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.github.mikephil.charting.data.m[] r0 = com.github.mikephil.charting.data.LineDataSet.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p128c.p129a.p130a.p141i.LineChartRenderer.C3872a.f7061a = r0
                int[] r0 = p119e.p128c.p129a.p130a.p141i.LineChartRenderer.C3872a.f7061a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.github.mikephil.charting.data.m r1 = com.github.mikephil.charting.data.LineDataSet.LINEAR     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.LineChartRenderer.C3872a.f7061a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.github.mikephil.charting.data.m r1 = com.github.mikephil.charting.data.LineDataSet.STEPPED     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.LineChartRenderer.C3872a.f7061a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.github.mikephil.charting.data.m r1 = com.github.mikephil.charting.data.LineDataSet.CUBIC_BEZIER     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.LineChartRenderer.C3872a.f7061a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.github.mikephil.charting.data.m r1 = com.github.mikephil.charting.data.LineDataSet.HORIZONTAL_BEZIER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p128c.p129a.p130a.p141i.LineChartRenderer.C3872a.<clinit>():void");
        }
    }

    public LineChartRenderer(LineDataProvider gVar, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7050h = gVar;
        this.f7051i = new Paint(1);
        this.f7051i.setStyle(Paint.Style.FILL);
        this.f7051i.setColor(-1);
    }

    /* renamed from: a */
    public void mo23337a() {
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        int l = (int) this.f7088a.mo23473l();
        int k = (int) this.f7088a.mo23471k();
        WeakReference<Bitmap> weakReference = this.f7052j;
        Bitmap bitmap = weakReference == null ? null : weakReference.get();
        if (!(bitmap != null && bitmap.getWidth() == l && bitmap.getHeight() == k)) {
            if (l > 0 && k > 0) {
                bitmap = Bitmap.createBitmap(l, k, this.f7054l);
                this.f7052j = new WeakReference<>(bitmap);
                this.f7053k = new Canvas(bitmap);
            } else {
                return;
            }
        }
        bitmap.eraseColor(0);
        for (ILineDataSet fVar : this.f7050h.getLineData().mo13386c()) {
            if (fVar.isVisible()) {
                mo23362a(canvas, fVar);
            }
        }
        canvas.drawBitmap(bitmap, 0.0f, 0.0f, this.f7036c);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo23369b(ILineDataSet fVar) {
        float b = this.f7035b.mo23204b();
        Transformer a = this.f7050h.mo12952a(fVar.mo13364s());
        this.f7016f.mo23348a(this.f7050h, fVar);
        this.f7055m.reset();
        BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
        if (aVar.f7019c >= 1) {
            Entry b2 = fVar.mo13410b(aVar.f7017a);
            this.f7055m.moveTo(b2.mo13315d(), b2.mo13303c() * b);
            int i = this.f7016f.f7017a + 1;
            while (true) {
                BarLineScatterCandleBubbleRenderer.C3869a aVar2 = this.f7016f;
                if (i > aVar2.f7019c + aVar2.f7017a) {
                    break;
                }
                Entry b3 = fVar.mo13410b(i);
                float d = b2.mo13315d() + ((b3.mo13315d() - b2.mo13315d()) / 2.0f);
                this.f7055m.cubicTo(d, b2.mo13303c() * b, d, b3.mo13303c() * b, b3.mo13315d(), b3.mo13303c() * b);
                i++;
                b2 = b3;
            }
        }
        if (fVar.mo23286G()) {
            this.f7056n.reset();
            this.f7056n.addPath(this.f7055m);
            mo23363a(this.f7053k, fVar, this.f7056n, a, this.f7016f);
        }
        this.f7036c.setColor(fVar.mo13366v());
        this.f7036c.setStyle(Paint.Style.STROKE);
        a.mo23431a(this.f7055m);
        this.f7053k.drawPath(this.f7055m, this.f7036c);
        this.f7036c.setPathEffect(null);
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        int i;
        ILineDataSet fVar;
        Entry entry;
        if (mo23356a(this.f7050h)) {
            List c = this.f7050h.getLineData().mo13386c();
            for (int i2 = 0; i2 < c.size(); i2++) {
                ILineDataSet fVar2 = (ILineDataSet) c.get(i2);
                if (mo23347b((IDataSet) fVar2) && fVar2.mo13417t() >= 1) {
                    mo23355a((IDataSet) fVar2);
                    Transformer a = this.f7050h.mo12952a(fVar2.mo13364s());
                    int U = (int) (fVar2.mo23276U() * 1.75f);
                    if (!fVar2.mo23278W()) {
                        U /= 2;
                    }
                    int i3 = U;
                    this.f7016f.mo23348a(this.f7050h, fVar2);
                    float a2 = this.f7035b.mo23201a();
                    float b = this.f7035b.mo23204b();
                    BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
                    float[] a3 = a.mo23438a(fVar2, a2, b, aVar.f7017a, aVar.f7018b);
                    ValueFormatter j = fVar2.mo13356j();
                    MPPointF a4 = MPPointF.m11568a(fVar2.mo13365u());
                    a4.f7123R = Utils.m11599a(a4.f7123R);
                    a4.f7124S = Utils.m11599a(a4.f7124S);
                    int i4 = 0;
                    while (i4 < a3.length) {
                        float f = a3[i4];
                        float f2 = a3[i4 + 1];
                        if (!this.f7088a.mo23456c(f)) {
                            break;
                        }
                        if (!this.f7088a.mo23454b(f) || !this.f7088a.mo23462f(f2)) {
                            i = i3;
                            fVar = fVar2;
                        } else {
                            int i5 = i4 / 2;
                            Entry b2 = fVar2.mo13410b(this.f7016f.f7017a + i5);
                            if (fVar2.mo13363r()) {
                                entry = b2;
                                i = i3;
                                float f3 = f2 - ((float) i3);
                                fVar = fVar2;
                                mo23365a(canvas, j.mo23226a(b2), f, f3, fVar2.mo13348d(i5));
                            } else {
                                entry = b2;
                                i = i3;
                                fVar = fVar2;
                            }
                            if (entry.mo13371b() != null && fVar.mo13349d()) {
                                Drawable b3 = entry.mo13371b();
                                Utils.m11606a(canvas, b3, (int) (f + a4.f7123R), (int) (f2 + a4.f7124S), b3.getIntrinsicWidth(), b3.getIntrinsicHeight());
                            }
                        }
                        i4 += 2;
                        fVar2 = fVar;
                        i3 = i;
                    }
                    MPPointF.m11570b(a4);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo23370d(Canvas canvas) {
        C3873b bVar;
        Bitmap a;
        this.f7036c.setStyle(Paint.Style.FILL);
        float b = this.f7035b.mo23204b();
        float[] fArr = this.f7060r;
        float f = 0.0f;
        char c = 0;
        fArr[0] = 0.0f;
        fArr[1] = 0.0f;
        List c2 = this.f7050h.getLineData().mo13386c();
        int i = 0;
        while (i < c2.size()) {
            ILineDataSet fVar = (ILineDataSet) c2.get(i);
            if (fVar.isVisible() && fVar.mo23278W() && fVar.mo13417t() != 0) {
                this.f7051i.setColor(fVar.mo23273Q());
                Transformer a2 = this.f7050h.mo12952a(fVar.mo13364s());
                this.f7016f.mo23348a(this.f7050h, fVar);
                float U = fVar.mo23276U();
                float X = fVar.mo23279X();
                boolean z = fVar.mo23280Y() && X < U && X > f;
                boolean z2 = z && fVar.mo23273Q() == 1122867;
                if (this.f7059q.containsKey(fVar)) {
                    bVar = this.f7059q.get(fVar);
                } else {
                    bVar = new C3873b(this, null);
                    this.f7059q.put(fVar, bVar);
                }
                if (bVar.mo23373a(fVar)) {
                    bVar.mo23372a(fVar, z, z2);
                }
                BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
                int i2 = aVar.f7019c;
                int i3 = aVar.f7017a;
                int i4 = i2 + i3;
                while (i3 <= i4) {
                    Entry b2 = fVar.mo13410b(i3);
                    if (b2 == null) {
                        break;
                    }
                    this.f7060r[c] = b2.mo13315d();
                    this.f7060r[1] = b2.mo13303c() * b;
                    a2.mo23442b(this.f7060r);
                    if (!this.f7088a.mo23456c(this.f7060r[c])) {
                        break;
                    }
                    if (this.f7088a.mo23454b(this.f7060r[c]) && this.f7088a.mo23462f(this.f7060r[1]) && (a = bVar.mo23371a(i3)) != null) {
                        float[] fArr2 = this.f7060r;
                        canvas.drawBitmap(a, fArr2[c] - U, fArr2[1] - U, (Paint) null);
                    }
                    i3++;
                    c = 0;
                }
            }
            i++;
            f = 0.0f;
            c = 0;
        }
    }

    /* renamed from: e.c.a.a.i.j$b */
    /* compiled from: LineChartRenderer */
    private class C3873b {

        /* renamed from: a */
        private Path f7062a;

        /* renamed from: b */
        private Bitmap[] f7063b;

        private C3873b() {
            this.f7062a = new Path();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public boolean mo23373a(ILineDataSet fVar) {
            int I = fVar.mo23270I();
            Bitmap[] bitmapArr = this.f7063b;
            if (bitmapArr == null) {
                this.f7063b = new Bitmap[I];
                return true;
            } else if (bitmapArr.length == I) {
                return false;
            } else {
                this.f7063b = new Bitmap[I];
                return true;
            }
        }

        /* synthetic */ C3873b(LineChartRenderer jVar, C3872a aVar) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo23372a(ILineDataSet fVar, boolean z, boolean z2) {
            int I = fVar.mo23270I();
            float U = fVar.mo23276U();
            float X = fVar.mo23279X();
            for (int i = 0; i < I; i++) {
                int i2 = (int) (((double) U) * 2.1d);
                Bitmap createBitmap = Bitmap.createBitmap(i2, i2, Bitmap.Config.ARGB_4444);
                Canvas canvas = new Canvas(createBitmap);
                this.f7063b[i] = createBitmap;
                LineChartRenderer.this.f7036c.setColor(fVar.mo23281f(i));
                if (z2) {
                    this.f7062a.reset();
                    this.f7062a.addCircle(U, U, U, Path.Direction.CW);
                    this.f7062a.addCircle(U, U, X, Path.Direction.CCW);
                    canvas.drawPath(this.f7062a, LineChartRenderer.this.f7036c);
                } else {
                    canvas.drawCircle(U, U, U, LineChartRenderer.this.f7036c);
                    if (z) {
                        canvas.drawCircle(U, U, X, LineChartRenderer.this.f7051i);
                    }
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Bitmap mo23371a(int i) {
            Bitmap[] bitmapArr = this.f7063b;
            return bitmapArr[i % bitmapArr.length];
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23362a(Canvas canvas, ILineDataSet fVar) {
        if (fVar.mo13417t() >= 1) {
            this.f7036c.setStrokeWidth(fVar.mo23284E());
            this.f7036c.setPathEffect(fVar.mo23275T());
            int i = C3872a.f7061a[fVar.mo23277V().ordinal()];
            if (i == 3) {
                mo23366a(fVar);
            } else if (i != 4) {
                mo23368b(canvas, fVar);
            } else {
                mo23369b(fVar);
            }
            this.f7036c.setPathEffect(null);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23366a(ILineDataSet fVar) {
        ILineDataSet fVar2 = fVar;
        float b = this.f7035b.mo23204b();
        Transformer a = this.f7050h.mo12952a(fVar.mo13364s());
        this.f7016f.mo23348a(this.f7050h, fVar2);
        float S = fVar.mo23274S();
        this.f7055m.reset();
        BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
        if (aVar.f7019c >= 1) {
            int i = aVar.f7017a + 1;
            Entry b2 = fVar2.mo13410b(Math.max(i - 2, 0));
            Entry b3 = fVar2.mo13410b(Math.max(i - 1, 0));
            int i2 = -1;
            if (b3 != null) {
                this.f7055m.moveTo(b3.mo13315d(), b3.mo13303c() * b);
                int i3 = this.f7016f.f7017a + 1;
                Entry entry = b3;
                while (true) {
                    BarLineScatterCandleBubbleRenderer.C3869a aVar2 = this.f7016f;
                    if (i3 > aVar2.f7019c + aVar2.f7017a) {
                        break;
                    }
                    if (i2 != i3) {
                        entry = fVar2.mo13410b(i3);
                    }
                    int i4 = i3 + 1;
                    if (i4 < fVar.mo13417t()) {
                        i3 = i4;
                    }
                    Entry b4 = fVar2.mo13410b(i3);
                    this.f7055m.cubicTo(b3.mo13315d() + ((entry.mo13315d() - b2.mo13315d()) * S), (b3.mo13303c() + ((entry.mo13303c() - b2.mo13303c()) * S)) * b, entry.mo13315d() - ((b4.mo13315d() - b3.mo13315d()) * S), (entry.mo13303c() - ((b4.mo13303c() - b3.mo13303c()) * S)) * b, entry.mo13315d(), entry.mo13303c() * b);
                    b2 = b3;
                    b3 = entry;
                    entry = b4;
                    int i5 = i3;
                    i3 = i4;
                    i2 = i5;
                }
            } else {
                return;
            }
        }
        if (fVar.mo23286G()) {
            this.f7056n.reset();
            this.f7056n.addPath(this.f7055m);
            mo23363a(this.f7053k, fVar, this.f7056n, a, this.f7016f);
        }
        this.f7036c.setColor(fVar.mo13366v());
        this.f7036c.setStyle(Paint.Style.STROKE);
        a.mo23431a(this.f7055m);
        this.f7053k.drawPath(this.f7055m, this.f7036c);
        this.f7036c.setPathEffect(null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo23368b(Canvas canvas, ILineDataSet fVar) {
        ILineDataSet fVar2 = fVar;
        int t = fVar.mo13417t();
        boolean z = fVar.mo23277V() == LineDataSet.STEPPED;
        int i = z ? 4 : 2;
        Transformer a = this.f7050h.mo12952a(fVar.mo13364s());
        float b = this.f7035b.mo23204b();
        this.f7036c.setStyle(Paint.Style.STROKE);
        Canvas canvas2 = fVar.mo23272P() ? this.f7053k : canvas;
        this.f7016f.mo23348a(this.f7050h, fVar2);
        if (fVar.mo23286G() && t > 0) {
            mo23364a(canvas, fVar2, a, this.f7016f);
        }
        if (fVar.mo13361o().size() > 1) {
            int i2 = i * 2;
            if (this.f7057o.length <= i2) {
                this.f7057o = new float[(i * 4)];
            }
            int i3 = this.f7016f.f7017a;
            while (true) {
                BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
                if (i3 > aVar.f7019c + aVar.f7017a) {
                    break;
                }
                Entry b2 = fVar2.mo13410b(i3);
                if (b2 != null) {
                    this.f7057o[0] = b2.mo13315d();
                    this.f7057o[1] = b2.mo13303c() * b;
                    if (i3 < this.f7016f.f7018b) {
                        Entry b3 = fVar2.mo13410b(i3 + 1);
                        if (b3 == null) {
                            break;
                        } else if (z) {
                            this.f7057o[2] = b3.mo13315d();
                            float[] fArr = this.f7057o;
                            fArr[3] = fArr[1];
                            fArr[4] = fArr[2];
                            fArr[5] = fArr[3];
                            fArr[6] = b3.mo13315d();
                            this.f7057o[7] = b3.mo13303c() * b;
                        } else {
                            this.f7057o[2] = b3.mo13315d();
                            this.f7057o[3] = b3.mo13303c() * b;
                        }
                    } else {
                        float[] fArr2 = this.f7057o;
                        fArr2[2] = fArr2[0];
                        fArr2[3] = fArr2[1];
                    }
                    a.mo23442b(this.f7057o);
                    if (!this.f7088a.mo23456c(this.f7057o[0])) {
                        break;
                    } else if (this.f7088a.mo23454b(this.f7057o[2]) && (this.f7088a.mo23458d(this.f7057o[1]) || this.f7088a.mo23450a(this.f7057o[3]))) {
                        this.f7036c.setColor(fVar2.mo13345c(i3));
                        canvas2.drawLines(this.f7057o, 0, i2, this.f7036c);
                    }
                }
                i3++;
            }
        } else {
            int i4 = t * i;
            if (this.f7057o.length < Math.max(i4, i) * 2) {
                this.f7057o = new float[(Math.max(i4, i) * 4)];
            }
            if (fVar2.mo13410b(this.f7016f.f7017a) != null) {
                int i5 = this.f7016f.f7017a;
                int i6 = 0;
                while (true) {
                    BarLineScatterCandleBubbleRenderer.C3869a aVar2 = this.f7016f;
                    if (i5 > aVar2.f7019c + aVar2.f7017a) {
                        break;
                    }
                    Entry b4 = fVar2.mo13410b(i5 == 0 ? 0 : i5 - 1);
                    Entry b5 = fVar2.mo13410b(i5);
                    if (!(b4 == null || b5 == null)) {
                        int i7 = i6 + 1;
                        this.f7057o[i6] = b4.mo13315d();
                        int i8 = i7 + 1;
                        this.f7057o[i7] = b4.mo13303c() * b;
                        if (z) {
                            int i9 = i8 + 1;
                            this.f7057o[i8] = b5.mo13315d();
                            int i10 = i9 + 1;
                            this.f7057o[i9] = b4.mo13303c() * b;
                            int i11 = i10 + 1;
                            this.f7057o[i10] = b5.mo13315d();
                            i8 = i11 + 1;
                            this.f7057o[i11] = b4.mo13303c() * b;
                        }
                        int i12 = i8 + 1;
                        this.f7057o[i8] = b5.mo13315d();
                        this.f7057o[i12] = b5.mo13303c() * b;
                        i6 = i12 + 1;
                    }
                    i5++;
                }
                if (i6 > 0) {
                    a.mo23442b(this.f7057o);
                    this.f7036c.setColor(fVar.mo13366v());
                    canvas2.drawLines(this.f7057o, 0, Math.max((this.f7016f.f7019c + 1) * i, i) * 2, this.f7036c);
                }
            }
        }
        this.f7036c.setPathEffect(null);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23363a(Canvas canvas, ILineDataSet fVar, Path path, Transformer gVar, BarLineScatterCandleBubbleRenderer.C3869a aVar) {
        float a = fVar.mo23271M().mo23219a(fVar, this.f7050h);
        path.lineTo(fVar.mo13410b(aVar.f7017a + aVar.f7019c).mo13315d(), a);
        path.lineTo(fVar.mo13410b(aVar.f7017a).mo13315d(), a);
        path.close();
        gVar.mo23431a(path);
        Drawable F = fVar.mo23285F();
        if (F != null) {
            mo23375a(canvas, path, F);
        } else {
            mo23374a(canvas, path, fVar.mo23282C(), fVar.mo23283D());
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23364a(Canvas canvas, ILineDataSet fVar, Transformer gVar, BarLineScatterCandleBubbleRenderer.C3869a aVar) {
        int i;
        int i2;
        Path path = this.f7058p;
        int i3 = aVar.f7017a;
        int i4 = aVar.f7019c + i3;
        int i5 = 0;
        do {
            i = (i5 * 128) + i3;
            i2 = i + 128;
            if (i2 > i4) {
                i2 = i4;
            }
            if (i <= i2) {
                m11457a(fVar, i, i2, path);
                gVar.mo23431a(path);
                Drawable F = fVar.mo23285F();
                if (F != null) {
                    mo23375a(canvas, path, F);
                } else {
                    mo23374a(canvas, path, fVar.mo23282C(), fVar.mo23283D());
                }
            }
            i5++;
        } while (i <= i2);
    }

    /* renamed from: a */
    private void m11457a(ILineDataSet fVar, int i, int i2, Path path) {
        float a = fVar.mo23271M().mo23219a(fVar, this.f7050h);
        float b = this.f7035b.mo23204b();
        boolean z = fVar.mo23277V() == LineDataSet.STEPPED;
        path.reset();
        Entry b2 = fVar.mo13410b(i);
        path.moveTo(b2.mo13315d(), a);
        path.lineTo(b2.mo13315d(), b2.mo13303c() * b);
        Entry entry = null;
        int i3 = i + 1;
        while (i3 <= i2) {
            Entry b3 = fVar.mo13410b(i3);
            if (z) {
                path.lineTo(b3.mo13315d(), b2.mo13303c() * b);
            }
            path.lineTo(b3.mo13315d(), b3.mo13303c() * b);
            i3++;
            b2 = b3;
            entry = b2;
        }
        if (entry != null) {
            path.lineTo(entry.mo13315d(), a);
        }
        path.close();
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
        mo23370d(canvas);
    }

    /* renamed from: b */
    public void mo23367b() {
        Canvas canvas = this.f7053k;
        if (canvas != null) {
            canvas.setBitmap(null);
            this.f7053k = null;
        }
        WeakReference<Bitmap> weakReference = this.f7052j;
        if (weakReference != null) {
            Bitmap bitmap = weakReference.get();
            if (bitmap != null) {
                bitmap.recycle();
            }
            this.f7052j.clear();
            this.f7052j = null;
        }
    }

    /* renamed from: a */
    public void mo23365a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        LineData lineData = this.f7050h.getLineData();
        for (Highlight dVar : dVarArr) {
            ILineDataSet fVar = (ILineDataSet) lineData.mo13374a(dVar.mo23245c());
            if (fVar != null && fVar.mo13367w()) {
                Entry a = fVar.mo13406a(dVar.mo23249g(), dVar.mo23251i());
                if (mo23346a(a, fVar)) {
                    MPPointD a2 = this.f7050h.mo12952a(fVar.mo13364s()).mo23428a(a.mo13315d(), a.mo13303c() * this.f7035b.mo23204b());
                    dVar.mo23241a((float) a2.f7120R, (float) a2.f7121S);
                    mo23376a(canvas, (float) a2.f7120R, (float) a2.f7121S, fVar);
                }
            }
        }
    }
}
