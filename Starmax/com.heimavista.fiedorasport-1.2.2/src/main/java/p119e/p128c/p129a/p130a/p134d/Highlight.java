package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.components.YAxis;

/* renamed from: e.c.a.a.d.d */
public class Highlight {

    /* renamed from: a */
    private float f6944a;

    /* renamed from: b */
    private float f6945b;

    /* renamed from: c */
    private float f6946c;

    /* renamed from: d */
    private float f6947d;

    /* renamed from: e */
    private int f6948e;

    /* renamed from: f */
    private int f6949f;

    /* renamed from: g */
    private int f6950g;

    /* renamed from: h */
    private YAxis.C1655a f6951h;

    /* renamed from: i */
    private float f6952i;

    /* renamed from: j */
    private float f6953j;

    public Highlight(float f, float f2, float f3, float f4, int i, YAxis.C1655a aVar) {
        this.f6944a = Float.NaN;
        this.f6945b = Float.NaN;
        this.f6948e = -1;
        this.f6950g = -1;
        this.f6944a = f;
        this.f6945b = f2;
        this.f6946c = f3;
        this.f6947d = f4;
        this.f6949f = i;
        this.f6951h = aVar;
    }

    /* renamed from: a */
    public void mo23242a(int i) {
        this.f6948e = i;
    }

    /* renamed from: b */
    public int mo23244b() {
        return this.f6948e;
    }

    /* renamed from: c */
    public int mo23245c() {
        return this.f6949f;
    }

    /* renamed from: d */
    public float mo23246d() {
        return this.f6952i;
    }

    /* renamed from: e */
    public float mo23247e() {
        return this.f6953j;
    }

    /* renamed from: f */
    public int mo23248f() {
        return this.f6950g;
    }

    /* renamed from: g */
    public float mo23249g() {
        return this.f6944a;
    }

    /* renamed from: h */
    public float mo23250h() {
        return this.f6946c;
    }

    /* renamed from: i */
    public float mo23251i() {
        return this.f6945b;
    }

    /* renamed from: j */
    public float mo23252j() {
        return this.f6947d;
    }

    public String toString() {
        return "Highlight, x: " + this.f6944a + ", y: " + this.f6945b + ", dataSetIndex: " + this.f6949f + ", stackIndex (only stacked barentry): " + this.f6950g;
    }

    /* renamed from: a */
    public YAxis.C1655a mo23240a() {
        return this.f6951h;
    }

    /* renamed from: a */
    public void mo23241a(float f, float f2) {
        this.f6952i = f;
        this.f6953j = f2;
    }

    /* renamed from: a */
    public boolean mo23243a(Highlight dVar) {
        return dVar != null && this.f6949f == dVar.f6949f && this.f6944a == dVar.f6944a && this.f6950g == dVar.f6950g && this.f6948e == dVar.f6948e;
    }

    public Highlight(float f, float f2, float f3, float f4, int i, int i2, YAxis.C1655a aVar) {
        this(f, f2, f3, f4, i, aVar);
        this.f6950g = i2;
    }
}
