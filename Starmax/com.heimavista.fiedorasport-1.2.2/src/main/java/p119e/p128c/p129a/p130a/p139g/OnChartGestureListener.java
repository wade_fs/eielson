package p119e.p128c.p129a.p130a.p139g;

import android.view.MotionEvent;
import p119e.p128c.p129a.p130a.p139g.ChartTouchListener;

/* renamed from: e.c.a.a.g.c */
public interface OnChartGestureListener {
    /* renamed from: a */
    void mo23314a(MotionEvent motionEvent);

    /* renamed from: a */
    void mo23315a(MotionEvent motionEvent, float f, float f2);

    /* renamed from: a */
    void mo23316a(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2);

    /* renamed from: a */
    void mo23317a(MotionEvent motionEvent, ChartTouchListener.C3867a aVar);

    /* renamed from: b */
    void mo23318b(MotionEvent motionEvent);

    /* renamed from: b */
    void mo23319b(MotionEvent motionEvent, float f, float f2);

    /* renamed from: b */
    void mo23320b(MotionEvent motionEvent, ChartTouchListener.C3867a aVar);

    /* renamed from: c */
    void mo23321c(MotionEvent motionEvent);
}
