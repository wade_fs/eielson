package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.data.CandleData;
import com.github.mikephil.charting.data.CandleEntry;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.CandleDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.ICandleDataSet;
import p119e.p128c.p129a.p130a.p141i.BarLineScatterCandleBubbleRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.e */
public class CandleStickChartRenderer extends LineScatterCandleRadarRenderer {

    /* renamed from: h */
    protected CandleDataProvider f7025h;

    /* renamed from: i */
    private float[] f7026i = new float[8];

    /* renamed from: j */
    private float[] f7027j = new float[4];

    /* renamed from: k */
    private float[] f7028k = new float[4];

    /* renamed from: l */
    private float[] f7029l = new float[4];

    /* renamed from: m */
    private float[] f7030m = new float[4];

    public CandleStickChartRenderer(CandleDataProvider dVar, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7025h = dVar;
    }

    /* renamed from: a */
    public void mo23337a() {
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        for (ICandleDataSet dVar : this.f7025h.getCandleData().mo13386c()) {
            if (dVar.isVisible()) {
                mo23352a(canvas, dVar);
            }
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        ICandleDataSet dVar;
        float f;
        CandleEntry candleEntry;
        if (mo23356a(this.f7025h)) {
            List c = this.f7025h.getCandleData().mo13386c();
            for (int i = 0; i < c.size(); i++) {
                ICandleDataSet dVar2 = (ICandleDataSet) c.get(i);
                if (mo23347b(dVar2) && dVar2.mo13417t() >= 1) {
                    mo23355a(dVar2);
                    Transformer a = this.f7025h.mo12952a(dVar2.mo13364s());
                    this.f7016f.mo23348a(this.f7025h, dVar2);
                    float a2 = this.f7035b.mo23201a();
                    float b = this.f7035b.mo23204b();
                    BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
                    float[] a3 = a.mo23437a(dVar2, a2, b, aVar.f7017a, aVar.f7018b);
                    float a4 = Utils.m11599a(5.0f);
                    ValueFormatter j = dVar2.mo13356j();
                    MPPointF a5 = MPPointF.m11568a(dVar2.mo13365u());
                    a5.f7123R = Utils.m11599a(a5.f7123R);
                    a5.f7124S = Utils.m11599a(a5.f7124S);
                    int i2 = 0;
                    while (i2 < a3.length) {
                        float f2 = a3[i2];
                        float f3 = a3[i2 + 1];
                        if (!this.f7088a.mo23456c(f2)) {
                            break;
                        }
                        if (!this.f7088a.mo23454b(f2) || !this.f7088a.mo23462f(f3)) {
                            dVar = dVar2;
                        } else {
                            int i3 = i2 / 2;
                            CandleEntry candleEntry2 = (CandleEntry) dVar2.mo13410b(this.f7016f.f7017a + i3);
                            if (dVar2.mo13363r()) {
                                candleEntry = candleEntry2;
                                f = f3;
                                dVar = dVar2;
                                mo23353a(canvas, j.mo23225a(candleEntry2), f2, f3 - a4, dVar2.mo13348d(i3));
                            } else {
                                candleEntry = candleEntry2;
                                f = f3;
                                dVar = dVar2;
                            }
                            if (candleEntry.mo13371b() != null && dVar.mo13349d()) {
                                Drawable b2 = candleEntry.mo13371b();
                                Utils.m11606a(canvas, b2, (int) (f2 + a5.f7123R), (int) (f + a5.f7124S), b2.getIntrinsicWidth(), b2.getIntrinsicHeight());
                            }
                        }
                        i2 += 2;
                        dVar2 = dVar;
                    }
                    MPPointF.m11570b(a5);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23352a(Canvas canvas, ICandleDataSet dVar) {
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        ICandleDataSet dVar2 = dVar;
        Transformer a = this.f7025h.mo12952a(dVar.mo13364s());
        float b = this.f7035b.mo23204b();
        float c0 = dVar.mo23263c0();
        boolean j0 = dVar.mo23268j0();
        this.f7016f.mo23348a(this.f7025h, dVar2);
        this.f7036c.setStrokeWidth(dVar.mo23261a0());
        int i6 = this.f7016f.f7017a;
        while (true) {
            BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
            if (i6 <= aVar.f7019c + aVar.f7017a) {
                CandleEntry candleEntry = (CandleEntry) dVar2.mo13410b(i6);
                if (candleEntry != null) {
                    float d = candleEntry.mo13315d();
                    float h = candleEntry.mo13314h();
                    float e = candleEntry.mo13311e();
                    float f = candleEntry.mo13312f();
                    float g = candleEntry.mo13313g();
                    if (j0) {
                        float[] fArr = this.f7026i;
                        fArr[0] = d;
                        fArr[2] = d;
                        fArr[4] = d;
                        fArr[6] = d;
                        int i7 = (h > e ? 1 : (h == e ? 0 : -1));
                        if (i7 > 0) {
                            fArr[1] = f * b;
                            fArr[3] = h * b;
                            fArr[5] = g * b;
                            fArr[7] = e * b;
                        } else if (h < e) {
                            fArr[1] = f * b;
                            fArr[3] = e * b;
                            fArr[5] = g * b;
                            fArr[7] = h * b;
                        } else {
                            fArr[1] = f * b;
                            fArr[3] = h * b;
                            fArr[5] = g * b;
                            fArr[7] = fArr[3];
                        }
                        a.mo23442b(this.f7026i);
                        if (!dVar.mo23265f0()) {
                            Paint paint = this.f7036c;
                            if (dVar.mo23266h0() == 1122867) {
                                i2 = dVar2.mo13345c(i6);
                            } else {
                                i2 = dVar.mo23266h0();
                            }
                            paint.setColor(i2);
                        } else if (i7 > 0) {
                            Paint paint2 = this.f7036c;
                            if (dVar.mo23269k0() == 1122867) {
                                i5 = dVar2.mo13345c(i6);
                            } else {
                                i5 = dVar.mo23269k0();
                            }
                            paint2.setColor(i5);
                        } else if (h < e) {
                            Paint paint3 = this.f7036c;
                            if (dVar.mo23267i0() == 1122867) {
                                i4 = dVar2.mo13345c(i6);
                            } else {
                                i4 = dVar.mo23267i0();
                            }
                            paint3.setColor(i4);
                        } else {
                            Paint paint4 = this.f7036c;
                            if (dVar.mo23260Z() == 1122867) {
                                i3 = dVar2.mo13345c(i6);
                            } else {
                                i3 = dVar.mo23260Z();
                            }
                            paint4.setColor(i3);
                        }
                        this.f7036c.setStyle(Paint.Style.STROKE);
                        canvas.drawLines(this.f7026i, this.f7036c);
                        float[] fArr2 = this.f7027j;
                        fArr2[0] = (d - 0.5f) + c0;
                        fArr2[1] = e * b;
                        fArr2[2] = (d + 0.5f) - c0;
                        fArr2[3] = h * b;
                        a.mo23442b(fArr2);
                        if (i7 > 0) {
                            if (dVar.mo23269k0() == 1122867) {
                                this.f7036c.setColor(dVar2.mo13345c(i6));
                            } else {
                                this.f7036c.setColor(dVar.mo23269k0());
                            }
                            this.f7036c.setStyle(dVar.mo23262b0());
                            float[] fArr3 = this.f7027j;
                            canvas.drawRect(fArr3[0], fArr3[3], fArr3[2], fArr3[1], this.f7036c);
                        } else if (h < e) {
                            if (dVar.mo23267i0() == 1122867) {
                                this.f7036c.setColor(dVar2.mo13345c(i6));
                            } else {
                                this.f7036c.setColor(dVar.mo23267i0());
                            }
                            this.f7036c.setStyle(dVar.mo23264d0());
                            float[] fArr4 = this.f7027j;
                            canvas.drawRect(fArr4[0], fArr4[1], fArr4[2], fArr4[3], this.f7036c);
                        } else {
                            if (dVar.mo23260Z() == 1122867) {
                                this.f7036c.setColor(dVar2.mo13345c(i6));
                            } else {
                                this.f7036c.setColor(dVar.mo23260Z());
                            }
                            float[] fArr5 = this.f7027j;
                            canvas.drawLine(fArr5[0], fArr5[1], fArr5[2], fArr5[3], this.f7036c);
                        }
                    } else {
                        float[] fArr6 = this.f7028k;
                        fArr6[0] = d;
                        fArr6[1] = f * b;
                        fArr6[2] = d;
                        fArr6[3] = g * b;
                        float[] fArr7 = this.f7029l;
                        fArr7[0] = (d - 0.5f) + c0;
                        float f2 = h * b;
                        fArr7[1] = f2;
                        fArr7[2] = d;
                        fArr7[3] = f2;
                        float[] fArr8 = this.f7030m;
                        fArr8[0] = (0.5f + d) - c0;
                        float f3 = e * b;
                        fArr8[1] = f3;
                        fArr8[2] = d;
                        fArr8[3] = f3;
                        a.mo23442b(fArr6);
                        a.mo23442b(this.f7029l);
                        a.mo23442b(this.f7030m);
                        if (h > e) {
                            if (dVar.mo23269k0() == 1122867) {
                                i = dVar2.mo13345c(i6);
                            } else {
                                i = dVar.mo23269k0();
                            }
                        } else if (h < e) {
                            if (dVar.mo23267i0() == 1122867) {
                                i = dVar2.mo13345c(i6);
                            } else {
                                i = dVar.mo23267i0();
                            }
                        } else if (dVar.mo23260Z() == 1122867) {
                            i = dVar2.mo13345c(i6);
                        } else {
                            i = dVar.mo23260Z();
                        }
                        this.f7036c.setColor(i);
                        float[] fArr9 = this.f7028k;
                        Canvas canvas2 = canvas;
                        canvas2.drawLine(fArr9[0], fArr9[1], fArr9[2], fArr9[3], this.f7036c);
                        float[] fArr10 = this.f7029l;
                        canvas2.drawLine(fArr10[0], fArr10[1], fArr10[2], fArr10[3], this.f7036c);
                        float[] fArr11 = this.f7030m;
                        canvas2.drawLine(fArr11[0], fArr11[1], fArr11[2], fArr11[3], this.f7036c);
                    }
                }
                i6++;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo23353a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        CandleData candleData = this.f7025h.getCandleData();
        for (Highlight dVar : dVarArr) {
            ICandleDataSet dVar2 = (ICandleDataSet) candleData.mo13374a(dVar.mo23245c());
            if (dVar2 != null && dVar2.mo13367w()) {
                CandleEntry candleEntry = (CandleEntry) dVar2.mo13406a(dVar.mo23249g(), dVar.mo23251i());
                if (mo23346a(candleEntry, dVar2)) {
                    MPPointD a = this.f7025h.mo12952a(dVar2.mo13364s()).mo23428a(candleEntry.mo13315d(), ((candleEntry.mo13313g() * this.f7035b.mo23204b()) + (candleEntry.mo13312f() * this.f7035b.mo23204b())) / 2.0f);
                    dVar.mo23241a((float) a.f7120R, (float) a.f7121S);
                    mo23376a(canvas, (float) a.f7120R, (float) a.f7121S, dVar2);
                }
            }
        }
    }
}
