package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Path;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.RadarData;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IRadarDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.v */
public class YAxisRendererRadarChart extends YAxisRenderer {

    /* renamed from: r */
    private RadarChart f7114r;

    /* renamed from: s */
    private Path f7115s = new Path();

    public YAxisRendererRadarChart(ViewPortHandler jVar, YAxis iVar, RadarChart radarChart) {
        super(jVar, iVar, null);
        this.f7114r = radarChart;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23335a(float f, float f2) {
        int i;
        double d;
        double d2;
        boolean z;
        float f3 = f;
        float f4 = f2;
        int n = this.f7004b.mo13217n();
        double abs = (double) Math.abs(f4 - f3);
        if (n == 0 || abs <= 0.0d || Double.isInfinite(abs)) {
            AxisBase aVar = this.f7004b;
            aVar.f2357l = new float[0];
            aVar.f2358m = new float[0];
            aVar.f2359n = 0;
            return;
        }
        double b = (double) Utils.m11612b(abs / ((double) n));
        if (this.f7004b.mo13228y() && b < ((double) this.f7004b.mo13213j())) {
            b = (double) this.f7004b.mo13213j();
        }
        double b2 = (double) Utils.m11612b(Math.pow(10.0d, (double) ((int) Math.log10(b))));
        if (((int) (b / b2)) > 5) {
            b = Math.floor(b2 * 10.0d);
        }
        boolean r = this.f7004b.mo13221r();
        if (this.f7004b.mo13227x()) {
            float f5 = ((float) abs) / ((float) (n - 1));
            AxisBase aVar2 = this.f7004b;
            aVar2.f2359n = n;
            if (aVar2.f2357l.length < n) {
                aVar2.f2357l = new float[n];
            }
            float f6 = f3;
            for (int i2 = 0; i2 < n; i2++) {
                this.f7004b.f2357l[i2] = f6;
                f6 += f5;
            }
            i = n;
        } else {
            int i3 = (b > 0.0d ? 1 : (b == 0.0d ? 0 : -1));
            if (i3 == 0) {
                d = 0.0d;
            } else {
                d = Math.ceil(((double) f3) / b) * b;
            }
            if (r) {
                d -= b;
            }
            if (i3 == 0) {
                d2 = 0.0d;
            } else {
                d2 = Utils.m11598a(Math.floor(((double) f4) / b) * b);
            }
            if (i3 != 0) {
                z = r;
                for (double d3 = d; d3 <= d2; d3 += b) {
                    z++;
                }
            } else {
                z = r;
            }
            i = ((int) z) + 1;
            AxisBase aVar3 = this.f7004b;
            aVar3.f2359n = i;
            if (aVar3.f2357l.length < i) {
                aVar3.f2357l = new float[i];
            }
            for (int i4 = 0; i4 < i; i4++) {
                if (d == 0.0d) {
                    d = 0.0d;
                }
                this.f7004b.f2357l[i4] = (float) d;
                d += b;
            }
        }
        if (b < 1.0d) {
            this.f7004b.f2360o = (int) Math.ceil(-Math.log10(b));
        } else {
            this.f7004b.f2360o = 0;
        }
        if (r) {
            AxisBase aVar4 = this.f7004b;
            if (aVar4.f2358m.length < i) {
                aVar4.f2358m = new float[i];
            }
            float[] fArr = this.f7004b.f2357l;
            float f7 = (fArr[1] - fArr[0]) / 2.0f;
            for (int i5 = 0; i5 < i; i5++) {
                AxisBase aVar5 = this.f7004b;
                aVar5.f2358m[i5] = aVar5.f2357l[i5] + f7;
            }
        }
        AxisBase aVar6 = this.f7004b;
        float[] fArr2 = aVar6.f2357l;
        aVar6.f2350H = fArr2[0];
        aVar6.f2349G = fArr2[i - 1];
        aVar6.f2351I = Math.abs(aVar6.f2349G - aVar6.f2350H);
    }

    /* renamed from: b */
    public void mo23411b(Canvas canvas) {
        if (super.f7101h.mo13240f() && super.f7101h.mo13225v()) {
            this.f7007e.setTypeface(super.f7101h.mo13236c());
            this.f7007e.setTextSize(super.f7101h.mo13234b());
            this.f7007e.setColor(super.f7101h.mo13229a());
            MPPointF centerOffsets = this.f7114r.getCenterOffsets();
            MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
            float factor = this.f7114r.getFactor();
            int i = super.f7101h.mo13294I() ? super.f7101h.f2359n : super.f7101h.f2359n - 1;
            for (int i2 = !super.f7101h.mo13293H(); i2 < i; i2++) {
                YAxis iVar = super.f7101h;
                Utils.m11611a(centerOffsets, (iVar.f2357l[i2] - iVar.f2350H) * factor, this.f7114r.getRotationAngle(), a);
                canvas.drawText(super.f7101h.mo13201b(i2), a.f7123R + 10.0f, a.f7124S, this.f7007e);
            }
            MPPointF.m11570b(centerOffsets);
            MPPointF.m11570b(a);
        }
    }

    /* renamed from: e */
    public void mo23415e(Canvas canvas) {
        List<LimitLine> o = super.f7101h.mo13218o();
        if (o != null) {
            float sliceAngle = this.f7114r.getSliceAngle();
            float factor = this.f7114r.getFactor();
            MPPointF centerOffsets = this.f7114r.getCenterOffsets();
            MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
            for (int i = 0; i < o.size(); i++) {
                LimitLine gVar = o.get(i);
                if (gVar.mo13240f()) {
                    this.f7009g.setColor(gVar.mo13279k());
                    this.f7009g.setPathEffect(gVar.mo13275g());
                    this.f7009g.setStrokeWidth(gVar.mo13280l());
                    float j = (gVar.mo13278j() - this.f7114r.getYChartMin()) * factor;
                    Path path = this.f7115s;
                    path.reset();
                    for (int i2 = 0; i2 < ((IRadarDataSet) ((RadarData) this.f7114r.getData()).mo13388e()).mo13417t(); i2++) {
                        Utils.m11611a(centerOffsets, j, (((float) i2) * sliceAngle) + this.f7114r.getRotationAngle(), a);
                        if (i2 == 0) {
                            path.moveTo(a.f7123R, a.f7124S);
                        } else {
                            path.lineTo(a.f7123R, a.f7124S);
                        }
                    }
                    path.close();
                    canvas.drawPath(path, this.f7009g);
                }
            }
            MPPointF.m11570b(centerOffsets);
            MPPointF.m11570b(a);
        }
    }
}
