package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.CandleData;

/* renamed from: e.c.a.a.e.a.d */
public interface CandleDataProvider extends BarLineScatterCandleBubbleDataProvider {
    CandleData getCandleData();
}
