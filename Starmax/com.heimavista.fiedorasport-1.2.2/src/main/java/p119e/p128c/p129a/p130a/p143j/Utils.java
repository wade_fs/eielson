package p119e.p128c.p129a.p130a.p143j;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import p119e.p128c.p129a.p130a.p133c.DefaultValueFormatter;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;

/* renamed from: e.c.a.a.j.i */
public abstract class Utils {

    /* renamed from: a */
    private static DisplayMetrics f7144a = null;

    /* renamed from: b */
    private static int f7145b = 50;

    /* renamed from: c */
    private static int f7146c = 8000;

    /* renamed from: d */
    public static final float f7147d = Float.intBitsToFloat(1);

    /* renamed from: e */
    private static Rect f7148e = new Rect();

    /* renamed from: f */
    private static Paint.FontMetrics f7149f = new Paint.FontMetrics();

    /* renamed from: g */
    private static Rect f7150g = new Rect();

    /* renamed from: h */
    private static ValueFormatter f7151h = m11603a();

    /* renamed from: i */
    private static Rect f7152i = new Rect();

    /* renamed from: j */
    private static Rect f7153j = new Rect();

    /* renamed from: k */
    private static Paint.FontMetrics f7154k = new Paint.FontMetrics();

    static {
        Double.longBitsToDouble(1);
    }

    /* renamed from: a */
    public static void m11605a(Context context) {
        if (context == null) {
            f7145b = ViewConfiguration.getMinimumFlingVelocity();
            f7146c = ViewConfiguration.getMaximumFlingVelocity();
            Log.e("MPChartLib-Utils", "Utils.init(...) PROVIDED CONTEXT OBJECT IS NULL");
            return;
        }
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        f7145b = viewConfiguration.getScaledMinimumFlingVelocity();
        f7146c = viewConfiguration.getScaledMaximumFlingVelocity();
        f7144a = context.getResources().getDisplayMetrics();
    }

    /* renamed from: b */
    public static float m11613b(Paint paint) {
        return m11614b(paint, f7149f);
    }

    /* renamed from: c */
    public static float m11619c(float f) {
        while (f < 0.0f) {
            f += 360.0f;
        }
        return f % 360.0f;
    }

    /* renamed from: c */
    public static int m11621c(Paint paint, String str) {
        return (int) paint.measureText(str);
    }

    /* renamed from: d */
    public static int m11622d() {
        return f7145b;
    }

    /* renamed from: e */
    public static int m11623e() {
        return Build.VERSION.SDK_INT;
    }

    /* renamed from: b */
    public static float m11614b(Paint paint, Paint.FontMetrics fontMetrics) {
        paint.getFontMetrics(fontMetrics);
        return (fontMetrics.ascent - fontMetrics.top) + fontMetrics.bottom;
    }

    /* renamed from: c */
    public static int m11620c() {
        return f7146c;
    }

    /* renamed from: b */
    public static FSize m11618b(Paint paint, String str) {
        FSize a = FSize.m11561a(0.0f, 0.0f);
        m11608a(paint, str, a);
        return a;
    }

    /* renamed from: b */
    public static ValueFormatter m11616b() {
        return f7151h;
    }

    /* renamed from: b */
    public static float m11612b(double d) {
        if (Double.isInfinite(d) || Double.isNaN(d) || d == 0.0d) {
            return 0.0f;
        }
        float pow = (float) Math.pow(10.0d, (double) (1 - ((int) ((float) Math.ceil((double) ((float) Math.log10(d < 0.0d ? -d : d)))))));
        return ((float) Math.round(d * ((double) pow))) / pow;
    }

    /* renamed from: a */
    public static float m11599a(float f) {
        DisplayMetrics displayMetrics = f7144a;
        if (displayMetrics != null) {
            return f * displayMetrics.density;
        }
        Log.e("MPChartLib-Utils", "Utils NOT INITIALIZED. You need to call Utils.init(...) at least once before calling Utils.convertDpToPixel(...). Otherwise conversion does not take place.");
        return f;
    }

    /* renamed from: a */
    public static int m11602a(Paint paint, String str) {
        Rect rect = f7148e;
        rect.set(0, 0, 0, 0);
        paint.getTextBounds(str, 0, str.length(), rect);
        return rect.height();
    }

    /* renamed from: b */
    public static int m11615b(float f) {
        float b = m11612b((double) f);
        if (Float.isInfinite(b)) {
            return 0;
        }
        return ((int) Math.ceil(-Math.log10((double) b))) + 2;
    }

    /* renamed from: b */
    public static FSize m11617b(float f, float f2, float f3) {
        double d = (double) f3;
        return FSize.m11561a(Math.abs(((float) Math.cos(d)) * f) + Math.abs(((float) Math.sin(d)) * f2), Math.abs(f * ((float) Math.sin(d))) + Math.abs(f2 * ((float) Math.cos(d))));
    }

    /* renamed from: a */
    public static float m11600a(Paint paint) {
        return m11601a(paint, f7149f);
    }

    /* renamed from: a */
    public static float m11601a(Paint paint, Paint.FontMetrics fontMetrics) {
        paint.getFontMetrics(fontMetrics);
        return fontMetrics.descent - fontMetrics.ascent;
    }

    /* renamed from: a */
    public static void m11608a(Paint paint, String str, FSize bVar) {
        Rect rect = f7150g;
        rect.set(0, 0, 0, 0);
        paint.getTextBounds(str, 0, str.length(), rect);
        bVar.f7117R = (float) rect.width();
        bVar.f7118S = (float) rect.height();
    }

    /* renamed from: a */
    private static ValueFormatter m11603a() {
        return new DefaultValueFormatter(1);
    }

    /* renamed from: a */
    public static double m11598a(double d) {
        if (d == Double.POSITIVE_INFINITY) {
            return d;
        }
        double d2 = d + 0.0d;
        return Double.longBitsToDouble(Double.doubleToRawLongBits(d2) + (d2 >= 0.0d ? 1 : -1));
    }

    /* renamed from: a */
    public static void m11611a(MPPointF eVar, float f, float f2, MPPointF eVar2) {
        double d = (double) f;
        double d2 = (double) f2;
        eVar2.f7123R = (float) (((double) eVar.f7123R) + (Math.cos(Math.toRadians(d2)) * d));
        eVar2.f7124S = (float) (((double) eVar.f7124S) + (d * Math.sin(Math.toRadians(d2))));
    }

    /* renamed from: a */
    public static void m11609a(MotionEvent motionEvent, VelocityTracker velocityTracker) {
        velocityTracker.computeCurrentVelocity(1000, (float) f7146c);
        int actionIndex = motionEvent.getActionIndex();
        int pointerId = motionEvent.getPointerId(actionIndex);
        float xVelocity = velocityTracker.getXVelocity(pointerId);
        float yVelocity = velocityTracker.getYVelocity(pointerId);
        int pointerCount = motionEvent.getPointerCount();
        for (int i = 0; i < pointerCount; i++) {
            if (i != actionIndex) {
                int pointerId2 = motionEvent.getPointerId(i);
                if ((velocityTracker.getXVelocity(pointerId2) * xVelocity) + (velocityTracker.getYVelocity(pointerId2) * yVelocity) < 0.0f) {
                    velocityTracker.clear();
                    return;
                }
            }
        }
    }

    @SuppressLint({"NewApi"})
    /* renamed from: a */
    public static void m11610a(View view) {
        if (Build.VERSION.SDK_INT >= 16) {
            view.postInvalidateOnAnimation();
        } else {
            view.postInvalidateDelayed(10);
        }
    }

    /* renamed from: a */
    public static void m11606a(Canvas canvas, Drawable drawable, int i, int i2, int i3, int i4) {
        MPPointF b = MPPointF.m11569b();
        b.f7123R = (float) (i - (i3 / 2));
        b.f7124S = (float) (i2 - (i4 / 2));
        drawable.copyBounds(f7152i);
        Rect rect = f7152i;
        int i5 = rect.left;
        int i6 = rect.top;
        drawable.setBounds(i5, i6, i5 + i3, i3 + i6);
        int save = canvas.save();
        canvas.translate(b.f7123R, b.f7124S);
        drawable.draw(canvas);
        canvas.restoreToCount(save);
    }

    /* renamed from: a */
    public static void m11607a(Canvas canvas, String str, float f, float f2, Paint paint, MPPointF eVar, float f3) {
        float fontMetrics = paint.getFontMetrics(f7154k);
        paint.getTextBounds(str, 0, str.length(), f7153j);
        float f4 = 0.0f - ((float) f7153j.left);
        float f5 = (-f7154k.ascent) + 0.0f;
        Paint.Align textAlign = paint.getTextAlign();
        paint.setTextAlign(Paint.Align.LEFT);
        if (f3 != 0.0f) {
            float width = f4 - (((float) f7153j.width()) * 0.5f);
            float f6 = f5 - (fontMetrics * 0.5f);
            if (!(eVar.f7123R == 0.5f && eVar.f7124S == 0.5f)) {
                FSize a = m11604a((float) f7153j.width(), fontMetrics, f3);
                f -= a.f7117R * (eVar.f7123R - 0.5f);
                f2 -= a.f7118S * (eVar.f7124S - 0.5f);
                FSize.m11562a(a);
            }
            canvas.save();
            canvas.translate(f, f2);
            canvas.rotate(f3);
            canvas.drawText(str, width, f6, paint);
            canvas.restore();
        } else {
            if (!(eVar.f7123R == 0.0f && eVar.f7124S == 0.0f)) {
                f4 -= ((float) f7153j.width()) * eVar.f7123R;
                f5 -= fontMetrics * eVar.f7124S;
            }
            canvas.drawText(str, f4 + f, f5 + f2, paint);
        }
        paint.setTextAlign(textAlign);
    }

    /* renamed from: a */
    public static FSize m11604a(float f, float f2, float f3) {
        return m11617b(f, f2, f3 * 0.017453292f);
    }
}
