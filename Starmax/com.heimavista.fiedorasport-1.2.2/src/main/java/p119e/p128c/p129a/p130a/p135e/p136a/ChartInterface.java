package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.ChartData;

/* renamed from: e.c.a.a.e.a.e */
public interface ChartInterface {
    ChartData getData();

    float getMaxHighlightDistance();

    int getMaxVisibleCount();
}
