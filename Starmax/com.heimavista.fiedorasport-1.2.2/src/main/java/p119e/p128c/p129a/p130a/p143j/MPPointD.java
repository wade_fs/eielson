package p119e.p128c.p129a.p130a.p143j;

import p119e.p128c.p129a.p130a.p138f.MoveViewJob;
import p119e.p128c.p129a.p130a.p143j.ObjectPool;

/* renamed from: e.c.a.a.j.d */
public class MPPointD extends ObjectPool.C3875a {

    /* renamed from: T */
    private static ObjectPool<MPPointD> f7119T = ObjectPool.m11573a(64, new MPPointD(0.0d, 0.0d));

    /* renamed from: R */
    public double f7120R;

    /* renamed from: S */
    public double f7121S;

    static {
        f7119T.mo23425a(0.5f);
    }

    private MPPointD(double d, double d2) {
        this.f7120R = d;
        this.f7121S = d2;
    }

    /* renamed from: a */
    public static MPPointD m11564a(double d, double d2) {
        MPPointD a = f7119T.mo23424a();
        a.f7120R = d;
        a.f7121S = d2;
        return a;
    }

    public String toString() {
        return "MPPointD, x: " + this.f7120R + ", y: " + this.f7121S;
    }

    /* renamed from: a */
    public static void m11565a(MPPointD dVar) {
        f7119T.mo23426a((MoveViewJob) super);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ObjectPool.C3875a mo23300a() {
        return new MPPointD(0.0d, 0.0d);
    }
}
