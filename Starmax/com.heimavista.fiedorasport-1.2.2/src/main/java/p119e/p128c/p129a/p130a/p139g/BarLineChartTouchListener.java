package p119e.p128c.p129a.p130a.p139g;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.animation.AnimationUtils;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarLineScatterCandleBubbleDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p139g.ChartTouchListener;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.g.a */
public class BarLineChartTouchListener extends ChartTouchListener<BarLineChartBase<? extends BarLineScatterCandleBubbleData<? extends IBarLineScatterCandleBubbleDataSet<? extends Entry>>>> {

    /* renamed from: U */
    private Matrix f6965U = new Matrix();

    /* renamed from: V */
    private Matrix f6966V = new Matrix();

    /* renamed from: W */
    private MPPointF f6967W = MPPointF.m11567a(0.0f, 0.0f);

    /* renamed from: X */
    private MPPointF f6968X = MPPointF.m11567a(0.0f, 0.0f);

    /* renamed from: Y */
    private float f6969Y = 1.0f;

    /* renamed from: Z */
    private float f6970Z = 1.0f;

    /* renamed from: a0 */
    private float f6971a0 = 1.0f;

    /* renamed from: b0 */
    private IDataSet f6972b0;

    /* renamed from: c0 */
    private VelocityTracker f6973c0;

    /* renamed from: d0 */
    private long f6974d0 = 0;

    /* renamed from: e0 */
    private MPPointF f6975e0 = MPPointF.m11567a(0.0f, 0.0f);

    /* renamed from: f0 */
    private MPPointF f6976f0 = MPPointF.m11567a(0.0f, 0.0f);

    /* renamed from: g0 */
    private float f6977g0;

    /* renamed from: h0 */
    private float f6978h0;

    public BarLineChartTouchListener(BarLineChartBase<? extends BarLineScatterCandleBubbleData<? extends IBarLineScatterCandleBubbleDataSet<? extends Entry>>> barLineChartBase, Matrix matrix, float f) {
        super(barLineChartBase);
        this.f6965U = matrix;
        this.f6977g0 = Utils.m11599a(f);
        this.f6978h0 = Utils.m11599a(3.5f);
    }

    /* renamed from: a */
    private void m11366a(MotionEvent motionEvent, float f, float f2) {
        super.f6979P = ChartTouchListener.C3867a.DRAG;
        this.f6965U.set(this.f6966V);
        OnChartGestureListener onChartGestureListener = ((BarLineChartBase) super.f6983T).getOnChartGestureListener();
        if (m11369c()) {
            if (super.f6983T instanceof HorizontalBarChart) {
                f = -f;
            } else {
                f2 = -f2;
            }
        }
        this.f6965U.postTranslate(f, f2);
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23319b(motionEvent, f, f2);
        }
    }

    /* renamed from: c */
    private static float m11368c(MotionEvent motionEvent) {
        return Math.abs(motionEvent.getX(0) - motionEvent.getX(1));
    }

    /* renamed from: d */
    private static float m11370d(MotionEvent motionEvent) {
        return Math.abs(motionEvent.getY(0) - motionEvent.getY(1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.github.mikephil.charting.charts.Chart.a(e.c.a.a.d.d, boolean):void
     arg types: [e.c.a.a.d.d, int]
     candidates:
      com.github.mikephil.charting.charts.Chart.a(float, float):e.c.a.a.d.d
      com.github.mikephil.charting.charts.Chart.a(int, e.c.a.a.a.b$c0):void
      com.github.mikephil.charting.charts.Chart.a(e.c.a.a.d.d, boolean):void */
    /* renamed from: e */
    private void m11371e(MotionEvent motionEvent) {
        Highlight a = ((BarLineChartBase) super.f6983T).mo12937a(motionEvent.getX(), motionEvent.getY());
        if (a != null && !a.mo23243a(super.f6981R)) {
            super.f6981R = a;
            ((BarLineChartBase) super.f6983T).mo13026a(a, true);
        }
    }

    /* renamed from: f */
    private void m11372f(MotionEvent motionEvent) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        if (motionEvent.getPointerCount() >= 2) {
            OnChartGestureListener onChartGestureListener = ((BarLineChartBase) super.f6983T).getOnChartGestureListener();
            float h = m11374h(motionEvent);
            if (h > this.f6978h0) {
                MPPointF eVar = this.f6968X;
                MPPointF a = mo23302a(eVar.f7123R, eVar.f7124S);
                ViewPortHandler viewPortHandler = ((BarLineChartBase) super.f6983T).getViewPortHandler();
                int i = super.f6980Q;
                boolean z5 = true;
                if (i == 4) {
                    super.f6979P = ChartTouchListener.C3867a.PINCH_ZOOM;
                    float f = h / this.f6971a0;
                    if (f >= 1.0f) {
                        z5 = false;
                    }
                    if (z5) {
                        z3 = viewPortHandler.mo23455c();
                    } else {
                        z3 = viewPortHandler.mo23449a();
                    }
                    if (z5) {
                        z4 = viewPortHandler.mo23457d();
                    } else {
                        z4 = viewPortHandler.mo23453b();
                    }
                    float f2 = ((BarLineChartBase) super.f6983T).mo12948A() ? f : 1.0f;
                    if (!((BarLineChartBase) super.f6983T).mo12949B()) {
                        f = 1.0f;
                    }
                    if (z4 || z3) {
                        this.f6965U.set(this.f6966V);
                        this.f6965U.postScale(f2, f, a.f7123R, a.f7124S);
                        if (onChartGestureListener != null) {
                            onChartGestureListener.mo23315a(motionEvent, f2, f);
                        }
                    }
                } else if (i == 2 && ((BarLineChartBase) super.f6983T).mo12948A()) {
                    super.f6979P = ChartTouchListener.C3867a.X_ZOOM;
                    float c = m11368c(motionEvent) / this.f6969Y;
                    if (c >= 1.0f) {
                        z5 = false;
                    }
                    if (z5) {
                        z2 = viewPortHandler.mo23455c();
                    } else {
                        z2 = viewPortHandler.mo23449a();
                    }
                    if (z2) {
                        this.f6965U.set(this.f6966V);
                        this.f6965U.postScale(c, 1.0f, a.f7123R, a.f7124S);
                        if (onChartGestureListener != null) {
                            onChartGestureListener.mo23315a(motionEvent, c, 1.0f);
                        }
                    }
                } else if (super.f6980Q == 3 && ((BarLineChartBase) super.f6983T).mo12949B()) {
                    super.f6979P = ChartTouchListener.C3867a.Y_ZOOM;
                    float d = m11370d(motionEvent) / this.f6970Z;
                    if (d >= 1.0f) {
                        z5 = false;
                    }
                    if (z5) {
                        z = viewPortHandler.mo23457d();
                    } else {
                        z = viewPortHandler.mo23453b();
                    }
                    if (z) {
                        this.f6965U.set(this.f6966V);
                        this.f6965U.postScale(1.0f, d, a.f7123R, a.f7124S);
                        if (onChartGestureListener != null) {
                            onChartGestureListener.mo23315a(motionEvent, 1.0f, d);
                        }
                    }
                }
                MPPointF.m11570b(a);
            }
        }
    }

    /* renamed from: g */
    private void m11373g(MotionEvent motionEvent) {
        this.f6966V.set(this.f6965U);
        this.f6967W.f7123R = motionEvent.getX();
        this.f6967W.f7124S = motionEvent.getY();
        this.f6972b0 = ((BarLineChartBase) super.f6983T).mo12958c(motionEvent.getX(), motionEvent.getY());
    }

    /* renamed from: h */
    private static float m11374h(MotionEvent motionEvent) {
        float x = motionEvent.getX(0) - motionEvent.getX(1);
        float y = motionEvent.getY(0) - motionEvent.getY(1);
        return (float) Math.sqrt((double) ((x * x) + (y * y)));
    }

    /* renamed from: b */
    public void mo23304b() {
        MPPointF eVar = this.f6976f0;
        eVar.f7123R = 0.0f;
        eVar.f7124S = 0.0f;
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        super.f6979P = ChartTouchListener.C3867a.DOUBLE_TAP;
        OnChartGestureListener onChartGestureListener = ((BarLineChartBase) super.f6983T).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23321c(motionEvent);
        }
        if (((BarLineChartBase) super.f6983T).mo13013t() && ((BarLineScatterCandleBubbleData) ((BarLineChartBase) super.f6983T).getData()).mo13387d() > 0) {
            MPPointF a = mo23302a(motionEvent.getX(), motionEvent.getY());
            T t = super.f6983T;
            BarLineChartBase barLineChartBase = (BarLineChartBase) t;
            float f = 1.4f;
            float f2 = ((BarLineChartBase) t).mo12948A() ? 1.4f : 1.0f;
            if (!((BarLineChartBase) super.f6983T).mo12949B()) {
                f = 1.0f;
            }
            barLineChartBase.mo12955b(f2, f, a.f7123R, a.f7124S);
            if (((BarLineChartBase) super.f6983T).mo13071l()) {
                Log.i("BarlineChartTouch", "Double-Tap, Zooming In, x: " + a.f7123R + ", y: " + a.f7124S);
            }
            MPPointF.m11570b(a);
        }
        return super.onDoubleTap(motionEvent);
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        super.f6979P = ChartTouchListener.C3867a.FLING;
        OnChartGestureListener onChartGestureListener = ((BarLineChartBase) super.f6983T).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23316a(motionEvent, motionEvent2, f, f2);
        }
        return super.onFling(motionEvent, motionEvent2, f, f2);
    }

    public void onLongPress(MotionEvent motionEvent) {
        super.f6979P = ChartTouchListener.C3867a.LONG_PRESS;
        OnChartGestureListener onChartGestureListener = ((BarLineChartBase) super.f6983T).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23318b(motionEvent);
        }
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        super.f6979P = ChartTouchListener.C3867a.SINGLE_TAP;
        OnChartGestureListener onChartGestureListener = ((BarLineChartBase) super.f6983T).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23314a(motionEvent);
        }
        if (!((BarLineChartBase) super.f6983T).mo13070k()) {
            return false;
        }
        mo23312a(((BarLineChartBase) super.f6983T).mo12937a(motionEvent.getX(), motionEvent.getY()), motionEvent);
        return super.onSingleTapUp(motionEvent);
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        VelocityTracker velocityTracker;
        if (this.f6973c0 == null) {
            this.f6973c0 = VelocityTracker.obtain();
        }
        this.f6973c0.addMovement(motionEvent);
        int i = 3;
        if (motionEvent.getActionMasked() == 3 && (velocityTracker = this.f6973c0) != null) {
            velocityTracker.recycle();
            this.f6973c0 = null;
        }
        if (super.f6980Q == 0) {
            super.f6982S.onTouchEvent(motionEvent);
        }
        if (!((BarLineChartBase) super.f6983T).mo13014u() && !((BarLineChartBase) super.f6983T).mo12948A() && !((BarLineChartBase) super.f6983T).mo12949B()) {
            return true;
        }
        int action = motionEvent.getAction() & 255;
        if (action != 0) {
            boolean z = false;
            if (action == 1) {
                VelocityTracker velocityTracker2 = this.f6973c0;
                int pointerId = motionEvent.getPointerId(0);
                velocityTracker2.computeCurrentVelocity(1000, (float) Utils.m11620c());
                float yVelocity = velocityTracker2.getYVelocity(pointerId);
                float xVelocity = velocityTracker2.getXVelocity(pointerId);
                if ((Math.abs(xVelocity) > ((float) Utils.m11622d()) || Math.abs(yVelocity) > ((float) Utils.m11622d())) && super.f6980Q == 1 && ((BarLineChartBase) super.f6983T).mo13068i()) {
                    mo23304b();
                    this.f6974d0 = AnimationUtils.currentAnimationTimeMillis();
                    this.f6975e0.f7123R = motionEvent.getX();
                    this.f6975e0.f7124S = motionEvent.getY();
                    MPPointF eVar = this.f6976f0;
                    eVar.f7123R = xVelocity;
                    eVar.f7124S = yVelocity;
                    Utils.m11610a((View) super.f6983T);
                }
                int i2 = super.f6980Q;
                if (i2 == 2 || i2 == 3 || i2 == 4 || i2 == 5) {
                    ((BarLineChartBase) super.f6983T).mo12961d();
                    ((BarLineChartBase) super.f6983T).postInvalidate();
                }
                super.f6980Q = 0;
                ((BarLineChartBase) super.f6983T).mo13036g();
                VelocityTracker velocityTracker3 = this.f6973c0;
                if (velocityTracker3 != null) {
                    velocityTracker3.recycle();
                    this.f6973c0 = null;
                }
                mo23310a(motionEvent);
            } else if (action == 2) {
                int i3 = super.f6980Q;
                if (i3 == 1) {
                    ((BarLineChartBase) super.f6983T).mo13035f();
                    float f = 0.0f;
                    float x = ((BarLineChartBase) super.f6983T).mo13015v() ? motionEvent.getX() - this.f6967W.f7123R : 0.0f;
                    if (((BarLineChartBase) super.f6983T).mo13016w()) {
                        f = motionEvent.getY() - this.f6967W.f7124S;
                    }
                    m11366a(motionEvent, x, f);
                } else if (i3 == 2 || i3 == 3 || i3 == 4) {
                    ((BarLineChartBase) super.f6983T).mo13035f();
                    if (((BarLineChartBase) super.f6983T).mo12948A() || ((BarLineChartBase) super.f6983T).mo12949B()) {
                        m11372f(motionEvent);
                    }
                } else if (i3 == 0 && Math.abs(ChartTouchListener.m11378a(motionEvent.getX(), this.f6967W.f7123R, motionEvent.getY(), this.f6967W.f7124S)) > this.f6977g0 && ((BarLineChartBase) super.f6983T).mo13014u()) {
                    if (!((BarLineChartBase) super.f6983T).mo13017x() || !((BarLineChartBase) super.f6983T).mo12983q()) {
                        z = true;
                    }
                    if (z) {
                        float abs = Math.abs(motionEvent.getX() - this.f6967W.f7123R);
                        float abs2 = Math.abs(motionEvent.getY() - this.f6967W.f7124S);
                        if ((((BarLineChartBase) super.f6983T).mo13015v() || abs2 >= abs) && (((BarLineChartBase) super.f6983T).mo13016w() || abs2 <= abs)) {
                            super.f6979P = ChartTouchListener.C3867a.DRAG;
                            super.f6980Q = 1;
                        }
                    } else if (((BarLineChartBase) super.f6983T).mo13018y()) {
                        super.f6979P = ChartTouchListener.C3867a.DRAG;
                        if (((BarLineChartBase) super.f6983T).mo13018y()) {
                            m11371e(motionEvent);
                        }
                    }
                }
            } else if (action == 3) {
                super.f6980Q = 0;
                mo23310a(motionEvent);
            } else if (action != 5) {
                if (action == 6) {
                    Utils.m11609a(motionEvent, this.f6973c0);
                    super.f6980Q = 5;
                }
            } else if (motionEvent.getPointerCount() >= 2) {
                ((BarLineChartBase) super.f6983T).mo13035f();
                m11373g(motionEvent);
                this.f6969Y = m11368c(motionEvent);
                this.f6970Z = m11370d(motionEvent);
                this.f6971a0 = m11374h(motionEvent);
                if (this.f6971a0 > 10.0f) {
                    if (((BarLineChartBase) super.f6983T).mo13019z()) {
                        super.f6980Q = 4;
                    } else if (((BarLineChartBase) super.f6983T).mo12948A() != ((BarLineChartBase) super.f6983T).mo12949B()) {
                        if (((BarLineChartBase) super.f6983T).mo12948A()) {
                            i = 2;
                        }
                        super.f6980Q = i;
                    } else {
                        if (this.f6969Y > this.f6970Z) {
                            i = 2;
                        }
                        super.f6980Q = i;
                    }
                }
                m11367a(this.f6968X, motionEvent);
            }
        } else {
            mo23313b(motionEvent);
            mo23304b();
            m11373g(motionEvent);
        }
        ViewPortHandler viewPortHandler = ((BarLineChartBase) super.f6983T).getViewPortHandler();
        Matrix matrix = this.f6965U;
        viewPortHandler.mo23444a(matrix, super.f6983T, true);
        this.f6965U = matrix;
        return true;
    }

    /* renamed from: c */
    private boolean m11369c() {
        IDataSet eVar;
        return (this.f6972b0 == null && ((BarLineChartBase) super.f6983T).mo12984r()) || ((eVar = this.f6972b0) != null && ((BarLineChartBase) super.f6983T).mo12956b(eVar.mo13364s()));
    }

    /* renamed from: a */
    private static void m11367a(MPPointF eVar, MotionEvent motionEvent) {
        eVar.f7123R = (motionEvent.getX(0) + motionEvent.getX(1)) / 2.0f;
        eVar.f7124S = (motionEvent.getY(0) + motionEvent.getY(1)) / 2.0f;
    }

    /* renamed from: a */
    public MPPointF mo23302a(float f, float f2) {
        float f3;
        ViewPortHandler viewPortHandler = ((BarLineChartBase) super.f6983T).getViewPortHandler();
        float y = f - viewPortHandler.mo23487y();
        if (m11369c()) {
            f3 = -(f2 - viewPortHandler.mo23443A());
        } else {
            f3 = -((((float) ((BarLineChartBase) super.f6983T).getMeasuredHeight()) - f2) - viewPortHandler.mo23486x());
        }
        return MPPointF.m11567a(y, f3);
    }

    /* renamed from: a */
    public void mo23303a() {
        MPPointF eVar = this.f6976f0;
        float f = 0.0f;
        if (eVar.f7123R != 0.0f || eVar.f7124S != 0.0f) {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.f6976f0.f7123R *= ((BarLineChartBase) super.f6983T).getDragDecelerationFrictionCoef();
            this.f6976f0.f7124S *= ((BarLineChartBase) super.f6983T).getDragDecelerationFrictionCoef();
            float f2 = ((float) (currentAnimationTimeMillis - this.f6974d0)) / 1000.0f;
            MPPointF eVar2 = this.f6976f0;
            float f3 = eVar2.f7123R * f2;
            float f4 = eVar2.f7124S * f2;
            MPPointF eVar3 = this.f6975e0;
            eVar3.f7123R += f3;
            eVar3.f7124S += f4;
            MotionEvent obtain = MotionEvent.obtain(currentAnimationTimeMillis, currentAnimationTimeMillis, 2, eVar3.f7123R, eVar3.f7124S, 0);
            float f5 = ((BarLineChartBase) super.f6983T).mo13015v() ? this.f6975e0.f7123R - this.f6967W.f7123R : 0.0f;
            if (((BarLineChartBase) super.f6983T).mo13016w()) {
                f = this.f6975e0.f7124S - this.f6967W.f7124S;
            }
            m11366a(obtain, f5, f);
            obtain.recycle();
            ViewPortHandler viewPortHandler = ((BarLineChartBase) super.f6983T).getViewPortHandler();
            Matrix matrix = this.f6965U;
            viewPortHandler.mo23444a(matrix, super.f6983T, false);
            this.f6965U = matrix;
            this.f6974d0 = currentAnimationTimeMillis;
            if (((double) Math.abs(this.f6976f0.f7123R)) >= 0.01d || ((double) Math.abs(this.f6976f0.f7124S)) >= 0.01d) {
                Utils.m11610a((View) super.f6983T);
                return;
            }
            ((BarLineChartBase) super.f6983T).mo12961d();
            ((BarLineChartBase) super.f6983T).postInvalidate();
            mo23304b();
        }
    }
}
