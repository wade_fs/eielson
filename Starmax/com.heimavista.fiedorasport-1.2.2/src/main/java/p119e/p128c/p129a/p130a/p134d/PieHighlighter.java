package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import p119e.p128c.p129a.p130a.p135e.p137b.IPieDataSet;

/* renamed from: e.c.a.a.d.g */
public class PieHighlighter extends PieRadarHighlighter<PieChart> {
    public PieHighlighter(PieChart pieChart) {
        super(pieChart);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Highlight mo23254a(int i, float f, float f2) {
        IPieDataSet k = ((PieData) ((PieChart) super.f6954a).getData()).mo13419k();
        return new Highlight((float) i, k.mo13410b(i).mo13303c(), f, f2, 0, k.mo13364s());
    }
}
