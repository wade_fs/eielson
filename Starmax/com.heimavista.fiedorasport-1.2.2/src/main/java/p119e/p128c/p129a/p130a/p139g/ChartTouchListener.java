package p119e.p128c.p129a.p130a.p139g;

import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import com.github.mikephil.charting.charts.Chart;
import p119e.p128c.p129a.p130a.p134d.Highlight;

/* renamed from: e.c.a.a.g.b */
public abstract class ChartTouchListener<T extends Chart<?>> extends GestureDetector.SimpleOnGestureListener implements View.OnTouchListener {

    /* renamed from: P */
    protected C3867a f6979P = C3867a.NONE;

    /* renamed from: Q */
    protected int f6980Q = 0;

    /* renamed from: R */
    protected Highlight f6981R;

    /* renamed from: S */
    protected GestureDetector f6982S;

    /* renamed from: T */
    protected T f6983T;

    /* renamed from: e.c.a.a.g.b$a */
    /* compiled from: ChartTouchListener */
    public enum C3867a {
        NONE,
        DRAG,
        X_ZOOM,
        Y_ZOOM,
        PINCH_ZOOM,
        ROTATE,
        SINGLE_TAP,
        DOUBLE_TAP,
        LONG_PRESS,
        FLING
    }

    public ChartTouchListener(T t) {
        this.f6983T = t;
        this.f6982S = new GestureDetector(t.getContext(), this);
    }

    /* renamed from: a */
    public void mo23310a(MotionEvent motionEvent) {
        OnChartGestureListener onChartGestureListener = this.f6983T.getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23320b(motionEvent, this.f6979P);
        }
    }

    /* renamed from: b */
    public void mo23313b(MotionEvent motionEvent) {
        OnChartGestureListener onChartGestureListener = this.f6983T.getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23317a(motionEvent, this.f6979P);
        }
    }

    /* renamed from: a */
    public void mo23311a(Highlight dVar) {
        this.f6981R = dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23312a(Highlight dVar, MotionEvent motionEvent) {
        if (dVar == null || dVar.mo23243a(this.f6981R)) {
            this.f6983T.mo13026a(null, true);
            this.f6981R = null;
            return;
        }
        this.f6983T.mo13026a(dVar, true);
        this.f6981R = dVar;
    }

    /* renamed from: a */
    protected static float m11378a(float f, float f2, float f3, float f4) {
        float f5 = f - f2;
        float f6 = f3 - f4;
        return (float) Math.sqrt((double) ((f5 * f5) + (f6 * f6)));
    }
}
