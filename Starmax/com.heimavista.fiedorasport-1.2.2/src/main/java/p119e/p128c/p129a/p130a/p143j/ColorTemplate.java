package p119e.p128c.p129a.p130a.p143j;

import android.graphics.Color;
import androidx.core.view.ViewCompat;
import com.flyco.tablayout.BuildConfig;
import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;
import java.util.ArrayList;
import java.util.List;

/* renamed from: e.c.a.a.j.a */
public class ColorTemplate {
    static {
        int[] iArr = {Color.rgb(207, 248, 246), Color.rgb(148, (int) BuildConfig.VERSION_CODE, (int) BuildConfig.VERSION_CODE), Color.rgb(136, 180, 187), Color.rgb(118, 174, 175), Color.rgb(42, 109, (int) TsExtractor.TS_STREAM_TYPE_HDMV_DTS)};
        int[] iArr2 = {Color.rgb(217, 80, (int) TsExtractor.TS_STREAM_TYPE_DTS), Color.rgb(254, 149, 7), Color.rgb(254, 247, 120), Color.rgb(106, 167, (int) TsExtractor.TS_STREAM_TYPE_SPLICE_INFO), Color.rgb(53, 194, 209)};
        int[] iArr3 = {Color.rgb(64, 89, 128), Color.rgb(149, 165, 124), Color.rgb(217, 184, 162), Color.rgb(191, (int) TsExtractor.TS_STREAM_TYPE_SPLICE_INFO, (int) TsExtractor.TS_STREAM_TYPE_SPLICE_INFO), Color.rgb(179, 48, 80)};
        int[] iArr4 = {Color.rgb(193, 37, 82), Color.rgb(255, 102, 0), Color.rgb(245, 199, 0), Color.rgb(106, 150, 31), Color.rgb(179, 100, 53)};
        int[] iArr5 = {Color.rgb((int) PsExtractor.AUDIO_STREAM, 255, 140), Color.rgb(255, 247, 140), Color.rgb(255, 208, 140), Color.rgb(140, 234, 255), Color.rgb(255, 140, 157)};
        int[] iArr6 = {m11559a("#2ecc71"), m11559a("#f1c40f"), m11559a("#e74c3c"), m11559a("#3498db")};
    }

    /* renamed from: a */
    public static int m11558a(int i, int i2) {
        return (i & ViewCompat.MEASURED_SIZE_MASK) | ((i2 & 255) << 24);
    }

    /* renamed from: a */
    public static int m11559a(String str) {
        int parseLong = (int) Long.parseLong(str.replace("#", ""), 16);
        return Color.rgb((parseLong >> 16) & 255, (parseLong >> 8) & 255, (parseLong >> 0) & 255);
    }

    /* renamed from: a */
    public static List<Integer> m11560a(int[] iArr) {
        ArrayList arrayList = new ArrayList();
        for (int i : iArr) {
            arrayList.add(Integer.valueOf(i));
        }
        return arrayList;
    }
}
