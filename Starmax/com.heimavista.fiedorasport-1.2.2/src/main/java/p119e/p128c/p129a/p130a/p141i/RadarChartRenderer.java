package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarEntry;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IRadarDataSet;
import p119e.p128c.p129a.p130a.p143j.ColorTemplate;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.n */
public class RadarChartRenderer extends LineRadarRenderer {

    /* renamed from: h */
    protected RadarChart f7083h;

    /* renamed from: i */
    protected Paint f7084i;

    /* renamed from: j */
    protected Paint f7085j;

    /* renamed from: k */
    protected Path f7086k = new Path();

    /* renamed from: l */
    protected Path f7087l = new Path();

    public RadarChartRenderer(RadarChart radarChart, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7083h = radarChart;
        this.f7037d = new Paint(1);
        this.f7037d.setStyle(Paint.Style.STROKE);
        this.f7037d.setStrokeWidth(2.0f);
        this.f7037d.setColor(Color.rgb(255, 187, 115));
        this.f7084i = new Paint(1);
        this.f7084i.setStyle(Paint.Style.STROKE);
        this.f7085j = new Paint(1);
    }

    /* renamed from: a */
    public void mo23337a() {
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        RadarData pVar = (RadarData) this.f7083h.getData();
        int t = ((IRadarDataSet) pVar.mo13388e()).mo13417t();
        for (IRadarDataSet jVar : pVar.mo13386c()) {
            if (jVar.isVisible()) {
                mo23389a(canvas, jVar, t);
            }
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
        mo23392d(canvas);
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        float f;
        int i;
        float f2;
        int i2;
        int i3;
        RadarEntry radarEntry;
        ValueFormatter gVar;
        IRadarDataSet jVar;
        MPPointF eVar;
        float a = this.f7035b.mo23201a();
        float b = this.f7035b.mo23204b();
        float sliceAngle = this.f7083h.getSliceAngle();
        float factor = this.f7083h.getFactor();
        MPPointF centerOffsets = this.f7083h.getCenterOffsets();
        MPPointF a2 = MPPointF.m11567a(0.0f, 0.0f);
        MPPointF a3 = MPPointF.m11567a(0.0f, 0.0f);
        float a4 = Utils.m11599a(5.0f);
        int i4 = 0;
        while (i4 < ((RadarData) this.f7083h.getData()).mo13383b()) {
            IRadarDataSet jVar2 = (IRadarDataSet) ((RadarData) this.f7083h.getData()).mo13374a(i4);
            if (!mo23347b(jVar2)) {
                i = i4;
                f = a;
            } else {
                mo23355a(jVar2);
                ValueFormatter j = jVar2.mo13356j();
                MPPointF a5 = MPPointF.m11568a(jVar2.mo13365u());
                a5.f7123R = Utils.m11599a(a5.f7123R);
                a5.f7124S = Utils.m11599a(a5.f7124S);
                int i5 = 0;
                while (i5 < jVar2.mo13417t()) {
                    RadarEntry radarEntry2 = (RadarEntry) jVar2.mo13410b(i5);
                    MPPointF eVar2 = a5;
                    float f3 = ((float) i5) * sliceAngle * a;
                    Utils.m11611a(centerOffsets, (radarEntry2.mo13303c() - this.f7083h.getYChartMin()) * factor * b, f3 + this.f7083h.getRotationAngle(), a2);
                    if (jVar2.mo13363r()) {
                        String a6 = j.mo23227a(radarEntry2);
                        float f4 = a2.f7123R;
                        radarEntry = radarEntry2;
                        i2 = i5;
                        f2 = a;
                        eVar = eVar2;
                        gVar = j;
                        float f5 = f4;
                        jVar = jVar2;
                        i3 = i4;
                        mo23391a(canvas, a6, f5, a2.f7124S - a4, jVar2.mo13348d(i5));
                    } else {
                        radarEntry = radarEntry2;
                        i2 = i5;
                        jVar = jVar2;
                        i3 = i4;
                        f2 = a;
                        eVar = eVar2;
                        gVar = j;
                    }
                    if (radarEntry.mo13371b() != null && jVar.mo13349d()) {
                        Drawable b2 = radarEntry.mo13371b();
                        Utils.m11611a(centerOffsets, (radarEntry.mo13303c() * factor * b) + eVar.f7124S, f3 + this.f7083h.getRotationAngle(), a3);
                        a3.f7124S += eVar.f7123R;
                        Utils.m11606a(canvas, b2, (int) a3.f7123R, (int) a3.f7124S, b2.getIntrinsicWidth(), b2.getIntrinsicHeight());
                    }
                    i5 = i2 + 1;
                    a5 = eVar;
                    jVar2 = jVar;
                    j = gVar;
                    i4 = i3;
                    a = f2;
                }
                i = i4;
                f = a;
                MPPointF.m11570b(a5);
            }
            i4 = i + 1;
            a = f;
        }
        MPPointF.m11570b(centerOffsets);
        MPPointF.m11570b(a2);
        MPPointF.m11570b(a3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo23392d(Canvas canvas) {
        float sliceAngle = this.f7083h.getSliceAngle();
        float factor = this.f7083h.getFactor();
        float rotationAngle = this.f7083h.getRotationAngle();
        MPPointF centerOffsets = this.f7083h.getCenterOffsets();
        this.f7084i.setStrokeWidth(this.f7083h.getWebLineWidth());
        this.f7084i.setColor(this.f7083h.getWebColor());
        this.f7084i.setAlpha(this.f7083h.getWebAlpha());
        int skipWebLineCount = this.f7083h.getSkipWebLineCount() + 1;
        int t = ((IRadarDataSet) ((RadarData) this.f7083h.getData()).mo13388e()).mo13417t();
        MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
        for (int i = 0; i < t; i += skipWebLineCount) {
            Utils.m11611a(centerOffsets, this.f7083h.getYRange() * factor, (((float) i) * sliceAngle) + rotationAngle, a);
            canvas.drawLine(centerOffsets.f7123R, centerOffsets.f7124S, a.f7123R, a.f7124S, this.f7084i);
        }
        MPPointF.m11570b(a);
        this.f7084i.setStrokeWidth(this.f7083h.getWebLineWidthInner());
        this.f7084i.setColor(this.f7083h.getWebColorInner());
        this.f7084i.setAlpha(this.f7083h.getWebAlpha());
        int i2 = this.f7083h.getYAxis().f2359n;
        MPPointF a2 = MPPointF.m11567a(0.0f, 0.0f);
        MPPointF a3 = MPPointF.m11567a(0.0f, 0.0f);
        for (int i3 = 0; i3 < i2; i3++) {
            int i4 = 0;
            while (i4 < ((RadarData) this.f7083h.getData()).mo13387d()) {
                float yChartMin = (this.f7083h.getYAxis().f2357l[i3] - this.f7083h.getYChartMin()) * factor;
                Utils.m11611a(centerOffsets, yChartMin, (((float) i4) * sliceAngle) + rotationAngle, a2);
                i4++;
                Utils.m11611a(centerOffsets, yChartMin, (((float) i4) * sliceAngle) + rotationAngle, a3);
                canvas.drawLine(a2.f7123R, a2.f7124S, a3.f7123R, a3.f7124S, this.f7084i);
            }
        }
        MPPointF.m11570b(a2);
        MPPointF.m11570b(a3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23389a(Canvas canvas, IRadarDataSet jVar, int i) {
        float a = this.f7035b.mo23201a();
        float b = this.f7035b.mo23204b();
        float sliceAngle = this.f7083h.getSliceAngle();
        float factor = this.f7083h.getFactor();
        MPPointF centerOffsets = this.f7083h.getCenterOffsets();
        MPPointF a2 = MPPointF.m11567a(0.0f, 0.0f);
        Path path = this.f7086k;
        path.reset();
        boolean z = false;
        for (int i2 = 0; i2 < jVar.mo13417t(); i2++) {
            this.f7036c.setColor(jVar.mo13345c(i2));
            Utils.m11611a(centerOffsets, (((RadarEntry) jVar.mo13410b(i2)).mo13303c() - this.f7083h.getYChartMin()) * factor * b, (((float) i2) * sliceAngle * a) + this.f7083h.getRotationAngle(), a2);
            if (!Float.isNaN(a2.f7123R)) {
                if (!z) {
                    path.moveTo(a2.f7123R, a2.f7124S);
                    z = true;
                } else {
                    path.lineTo(a2.f7123R, a2.f7124S);
                }
            }
        }
        if (jVar.mo13417t() > i) {
            path.lineTo(centerOffsets.f7123R, centerOffsets.f7124S);
        }
        path.close();
        if (jVar.mo23286G()) {
            Drawable F = jVar.mo23285F();
            if (F != null) {
                mo23375a(canvas, path, F);
            } else {
                mo23374a(canvas, path, jVar.mo23282C(), jVar.mo23283D());
            }
        }
        this.f7036c.setStrokeWidth(jVar.mo23284E());
        this.f7036c.setStyle(Paint.Style.STROKE);
        if (!jVar.mo23286G() || jVar.mo23283D() < 255) {
            canvas.drawPath(path, this.f7036c);
        }
        MPPointF.m11570b(centerOffsets);
        MPPointF.m11570b(a2);
    }

    /* renamed from: a */
    public void mo23391a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        int i;
        Highlight[] dVarArr2 = dVarArr;
        float sliceAngle = this.f7083h.getSliceAngle();
        float factor = this.f7083h.getFactor();
        MPPointF centerOffsets = this.f7083h.getCenterOffsets();
        MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
        RadarData pVar = (RadarData) this.f7083h.getData();
        int length = dVarArr2.length;
        int i2 = 0;
        int i3 = 0;
        while (i3 < length) {
            Highlight dVar = dVarArr2[i3];
            IRadarDataSet jVar = (IRadarDataSet) pVar.mo13374a(dVar.mo23245c());
            if (jVar != null && jVar.mo13367w()) {
                RadarEntry radarEntry = (RadarEntry) jVar.mo13410b((int) dVar.mo23249g());
                if (mo23346a(radarEntry, jVar)) {
                    Utils.m11611a(centerOffsets, (radarEntry.mo13303c() - this.f7083h.getYChartMin()) * factor * this.f7035b.mo23204b(), (dVar.mo23249g() * sliceAngle * this.f7035b.mo23201a()) + this.f7083h.getRotationAngle(), a);
                    dVar.mo23241a(a.f7123R, a.f7124S);
                    mo23376a(canvas, a.f7123R, a.f7124S, jVar);
                    if (jVar.mo23296O() && !Float.isNaN(a.f7123R) && !Float.isNaN(a.f7124S)) {
                        int N = jVar.mo23295N();
                        if (N == 1122867) {
                            N = jVar.mo13345c(i2);
                        }
                        if (jVar.mo23294L() < 255) {
                            N = ColorTemplate.m11558a(N, jVar.mo23294L());
                        }
                        float K = jVar.mo23293K();
                        float R = jVar.mo23297R();
                        int J = jVar.mo23292J();
                        int i4 = J;
                        i = i3;
                        mo23390a(canvas, a, K, R, i4, N, jVar.mo23291H());
                        i3 = i + 1;
                        i2 = 0;
                    }
                }
            }
            i = i3;
            i3 = i + 1;
            i2 = 0;
        }
        MPPointF.m11570b(centerOffsets);
        MPPointF.m11570b(a);
    }

    /* renamed from: a */
    public void mo23390a(Canvas canvas, MPPointF eVar, float f, float f2, int i, int i2, float f3) {
        canvas.save();
        float a = Utils.m11599a(f2);
        float a2 = Utils.m11599a(f);
        if (i != 1122867) {
            Path path = this.f7087l;
            path.reset();
            path.addCircle(eVar.f7123R, eVar.f7124S, a, Path.Direction.CW);
            if (a2 > 0.0f) {
                path.addCircle(eVar.f7123R, eVar.f7124S, a2, Path.Direction.CCW);
            }
            this.f7085j.setColor(i);
            this.f7085j.setStyle(Paint.Style.FILL);
            canvas.drawPath(path, this.f7085j);
        }
        if (i2 != 1122867) {
            this.f7085j.setColor(i2);
            this.f7085j.setStyle(Paint.Style.STROKE);
            this.f7085j.setStrokeWidth(Utils.m11599a(f3));
            canvas.drawCircle(eVar.f7123R, eVar.f7124S, a, this.f7085j);
        }
        canvas.restore();
    }
}
