package p119e.p128c.p129a.p130a.p132b;

import com.github.mikephil.charting.data.BarEntry;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;

/* renamed from: e.c.a.a.b.b */
public class BarBuffer extends AbstractBuffer<IBarDataSet> {

    /* renamed from: e */
    protected boolean f6933e = false;

    /* renamed from: f */
    protected boolean f6934f = false;

    /* renamed from: g */
    protected float f6935g = 1.0f;

    public BarBuffer(int i, int i2, boolean z) {
        super(i);
        this.f6933e = z;
    }

    /* renamed from: a */
    public void mo23211a(float f) {
        this.f6935g = f;
    }

    /* renamed from: a */
    public void mo23213a(int i) {
    }

    /* renamed from: a */
    public void mo23215a(boolean z) {
        this.f6934f = z;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23212a(float f, float f2, float f3, float f4) {
        float[] fArr = super.f6930b;
        int i = super.f6929a;
        super.f6929a = i + 1;
        fArr[i] = f;
        int i2 = super.f6929a;
        super.f6929a = i2 + 1;
        fArr[i2] = f2;
        int i3 = super.f6929a;
        super.f6929a = i3 + 1;
        fArr[i3] = f3;
        int i4 = super.f6929a;
        super.f6929a = i4 + 1;
        fArr[i4] = f4;
    }

    /* renamed from: a */
    public void mo23214a(IBarDataSet aVar) {
        float f;
        float f2;
        float f3;
        float f4;
        float t = ((float) aVar.mo13417t()) * super.f6931c;
        float f5 = this.f6935g / 2.0f;
        for (int i = 0; ((float) i) < t; i++) {
            BarEntry barEntry = (BarEntry) aVar.mo13410b(i);
            if (barEntry != null) {
                float d = barEntry.mo13315d();
                float c = barEntry.mo13303c();
                float[] i2 = barEntry.mo13308i();
                if (!this.f6933e || i2 == null) {
                    float f6 = d - f5;
                    float f7 = d + f5;
                    if (this.f6934f) {
                        float f8 = c >= 0.0f ? c : 0.0f;
                        if (c > 0.0f) {
                            c = 0.0f;
                        }
                        float f9 = c;
                        c = f8;
                        f = f9;
                    } else {
                        f = c >= 0.0f ? c : 0.0f;
                        if (c > 0.0f) {
                            c = 0.0f;
                        }
                    }
                    if (f > 0.0f) {
                        f *= super.f6932d;
                    } else {
                        c *= super.f6932d;
                    }
                    mo23212a(f6, f, f7, c);
                } else {
                    float f10 = -barEntry.mo13305f();
                    int i3 = 0;
                    float f11 = 0.0f;
                    while (i3 < i2.length) {
                        float f12 = i2[i3];
                        int i4 = (f12 > 0.0f ? 1 : (f12 == 0.0f ? 0 : -1));
                        if (i4 == 0 && (f11 == 0.0f || f10 == 0.0f)) {
                            f2 = f10;
                            f3 = f11;
                            f11 = f12;
                        } else if (i4 >= 0) {
                            f12 += f11;
                            f2 = f10;
                            f3 = f12;
                        } else {
                            float abs = Math.abs(f12) + f10;
                            float abs2 = Math.abs(f12) + f10;
                            float f13 = f10;
                            f3 = f11;
                            f11 = f13;
                            float f14 = abs;
                            f2 = abs2;
                            f12 = f14;
                        }
                        float f15 = d - f5;
                        float f16 = d + f5;
                        if (this.f6934f) {
                            float f17 = f11 >= f12 ? f11 : f12;
                            if (f11 > f12) {
                                f11 = f12;
                            }
                            float f18 = f11;
                            f11 = f17;
                            f4 = f18;
                        } else {
                            f4 = f11 >= f12 ? f11 : f12;
                            if (f11 > f12) {
                                f11 = f12;
                            }
                        }
                        float f19 = super.f6932d;
                        mo23212a(f15, f4 * f19, f16, f11 * f19);
                        i3++;
                        f11 = f3;
                        f10 = f2;
                    }
                }
            }
        }
        mo23208a();
    }
}
