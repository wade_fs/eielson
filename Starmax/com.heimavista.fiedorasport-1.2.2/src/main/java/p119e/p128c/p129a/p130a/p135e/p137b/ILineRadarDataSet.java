package p119e.p128c.p129a.p130a.p135e.p137b;

import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.data.Entry;

/* renamed from: e.c.a.a.e.b.g */
public interface ILineRadarDataSet<T extends Entry> extends ILineScatterCandleRadarDataSet<T> {
    /* renamed from: C */
    int mo23282C();

    /* renamed from: D */
    int mo23283D();

    /* renamed from: E */
    float mo23284E();

    /* renamed from: F */
    Drawable mo23285F();

    /* renamed from: G */
    boolean mo23286G();
}
