package p119e.p128c.p129a.p130a.p134d;

/* renamed from: e.c.a.a.d.j */
public final class Range {

    /* renamed from: a */
    public float f6956a;

    /* renamed from: b */
    public float f6957b;

    public Range(float f, float f2) {
        this.f6956a = f;
        this.f6957b = f2;
    }

    /* renamed from: a */
    public boolean mo23256a(float f) {
        return f > this.f6956a && f <= this.f6957b;
    }
}
