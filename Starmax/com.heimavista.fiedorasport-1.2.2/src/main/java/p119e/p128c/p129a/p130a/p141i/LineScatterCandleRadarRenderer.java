package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Path;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p135e.p137b.ILineScatterCandleRadarDataSet;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.l */
public abstract class LineScatterCandleRadarRenderer extends BarLineScatterCandleBubbleRenderer {

    /* renamed from: g */
    private Path f7065g = new Path();

    public LineScatterCandleRadarRenderer(ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23376a(Canvas canvas, float f, float f2, ILineScatterCandleRadarDataSet hVar) {
        this.f7037d.setColor(hVar.mo13335x());
        this.f7037d.setStrokeWidth(hVar.mo23289y());
        this.f7037d.setPathEffect(hVar.mo23290z());
        if (hVar.mo23287A()) {
            this.f7065g.reset();
            this.f7065g.moveTo(f, this.f7088a.mo23467i());
            this.f7065g.lineTo(f, this.f7088a.mo23459e());
            canvas.drawPath(this.f7065g, this.f7037d);
        }
        if (hVar.mo23288B()) {
            this.f7065g.reset();
            this.f7065g.moveTo(this.f7088a.mo23463g(), f2);
            this.f7065g.lineTo(this.f7088a.mo23465h(), f2);
            canvas.drawPath(this.f7065g, this.f7037d);
        }
    }
}
