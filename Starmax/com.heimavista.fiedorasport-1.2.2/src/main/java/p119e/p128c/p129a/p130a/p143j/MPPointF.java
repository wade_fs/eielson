package p119e.p128c.p129a.p130a.p143j;

import android.os.Parcel;
import android.os.Parcelable;
import p119e.p128c.p129a.p130a.p138f.MoveViewJob;
import p119e.p128c.p129a.p130a.p143j.ObjectPool;

/* renamed from: e.c.a.a.j.e */
public class MPPointF extends ObjectPool.C3875a {

    /* renamed from: T */
    private static ObjectPool<MPPointF> f7122T = ObjectPool.m11573a(32, new MPPointF(0.0f, 0.0f));

    /* renamed from: R */
    public float f7123R;

    /* renamed from: S */
    public float f7124S;

    /* renamed from: e.c.a.a.j.e$a */
    /* compiled from: MPPointF */
    static class C3874a implements Parcelable.Creator<MPPointF> {
        C3874a() {
        }

        public MPPointF createFromParcel(Parcel parcel) {
            MPPointF eVar = new MPPointF(0.0f, 0.0f);
            eVar.mo23421a(parcel);
            return eVar;
        }

        public MPPointF[] newArray(int i) {
            return new MPPointF[i];
        }
    }

    static {
        f7122T.mo23425a(0.5f);
        new C3874a();
    }

    public MPPointF() {
    }

    /* renamed from: a */
    public static MPPointF m11567a(float f, float f2) {
        MPPointF a = f7122T.mo23424a();
        a.f7123R = f;
        a.f7124S = f2;
        return a;
    }

    /* renamed from: b */
    public static MPPointF m11569b() {
        return f7122T.mo23424a();
    }

    public MPPointF(float f, float f2) {
        this.f7123R = f;
        this.f7124S = f2;
    }

    /* renamed from: b */
    public static void m11570b(MPPointF eVar) {
        f7122T.mo23426a((MoveViewJob) super);
    }

    /* renamed from: a */
    public static MPPointF m11568a(MPPointF eVar) {
        MPPointF a = f7122T.mo23424a();
        a.f7123R = eVar.f7123R;
        a.f7124S = eVar.f7124S;
        return a;
    }

    /* renamed from: a */
    public void mo23421a(Parcel parcel) {
        this.f7123R = parcel.readFloat();
        this.f7124S = parcel.readFloat();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ObjectPool.C3875a mo23300a() {
        return new MPPointF(0.0f, 0.0f);
    }
}
