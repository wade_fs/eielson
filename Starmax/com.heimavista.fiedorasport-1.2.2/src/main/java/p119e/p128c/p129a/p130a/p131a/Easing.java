package p119e.p128c.p129a.p130a.p131a;

import android.animation.TimeInterpolator;
import androidx.annotation.RequiresApi;

@RequiresApi(11)
/* renamed from: e.c.a.a.a.b */
public class Easing {

    /* renamed from: a */
    public static final C3843c0 f6925a = new C3851k();

    /* renamed from: b */
    public static final C3843c0 f6926b = new C3863w();

    /* renamed from: c */
    public static final C3843c0 f6927c = new C3858r();

    /* renamed from: d */
    public static final C3843c0 f6928d = new C3859s();

    /* renamed from: e.c.a.a.a.b$a */
    /* compiled from: Easing */
    static class C3838a implements C3843c0 {
        C3838a() {
        }

        public float getInterpolation(float f) {
            float f2 = f * 2.0f;
            if (f2 < 1.0f) {
                return ((float) Math.pow((double) f2, 4.0d)) * 0.5f;
            }
            return (((float) Math.pow((double) (f2 - 2.0f), 4.0d)) - 2.0f) * -0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$a0 */
    /* compiled from: Easing */
    static class C3839a0 implements C3843c0 {
        C3839a0() {
        }

        public float getInterpolation(float f) {
            return (float) Math.pow((double) f, 4.0d);
        }
    }

    /* renamed from: e.c.a.a.a.b$b */
    /* compiled from: Easing */
    static class C3840b implements C3843c0 {
        C3840b() {
        }

        public float getInterpolation(float f) {
            return (-((float) Math.cos(((double) f) * 1.5707963267948966d))) + 1.0f;
        }
    }

    /* renamed from: e.c.a.a.a.b$b0 */
    /* compiled from: Easing */
    static class C3841b0 implements C3843c0 {
        C3841b0() {
        }

        public float getInterpolation(float f) {
            return -(((float) Math.pow((double) (f - 1.0f), 4.0d)) - 1.0f);
        }
    }

    /* renamed from: e.c.a.a.a.b$c */
    /* compiled from: Easing */
    static class C3842c implements C3843c0 {
        C3842c() {
        }

        public float getInterpolation(float f) {
            return (float) Math.sin(((double) f) * 1.5707963267948966d);
        }
    }

    /* renamed from: e.c.a.a.a.b$c0 */
    /* compiled from: Easing */
    public interface C3843c0 extends TimeInterpolator {
        float getInterpolation(float f);
    }

    /* renamed from: e.c.a.a.a.b$d */
    /* compiled from: Easing */
    static class C3844d implements C3843c0 {
        C3844d() {
        }

        public float getInterpolation(float f) {
            return (((float) Math.cos(((double) f) * 3.141592653589793d)) - 1.0f) * -0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$e */
    /* compiled from: Easing */
    static class C3845e implements C3843c0 {
        C3845e() {
        }

        public float getInterpolation(float f) {
            if (f == 0.0f) {
                return 0.0f;
            }
            return (float) Math.pow(2.0d, (double) ((f - 1.0f) * 10.0f));
        }
    }

    /* renamed from: e.c.a.a.a.b$f */
    /* compiled from: Easing */
    static class C3846f implements C3843c0 {
        C3846f() {
        }

        public float getInterpolation(float f) {
            if (f == 1.0f) {
                return 1.0f;
            }
            return -((float) Math.pow(2.0d, (double) ((f + 1.0f) * -10.0f)));
        }
    }

    /* renamed from: e.c.a.a.a.b$g */
    /* compiled from: Easing */
    static class C3847g implements C3843c0 {
        C3847g() {
        }

        public float getInterpolation(float f) {
            float f2;
            if (f == 0.0f) {
                return 0.0f;
            }
            if (f == 1.0f) {
                return 1.0f;
            }
            float f3 = f * 2.0f;
            if (f3 < 1.0f) {
                f2 = (float) Math.pow(2.0d, (double) ((f3 - 1.0f) * 10.0f));
            } else {
                f2 = (-((float) Math.pow(2.0d, (double) ((f3 - 1.0f) * -10.0f)))) + 2.0f;
            }
            return f2 * 0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$h */
    /* compiled from: Easing */
    static class C3848h implements C3843c0 {
        C3848h() {
        }

        public float getInterpolation(float f) {
            return -(((float) Math.sqrt((double) (1.0f - (f * f)))) - 1.0f);
        }
    }

    /* renamed from: e.c.a.a.a.b$i */
    /* compiled from: Easing */
    static class C3849i implements C3843c0 {
        C3849i() {
        }

        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (float) Math.sqrt((double) (1.0f - (f2 * f2)));
        }
    }

    /* renamed from: e.c.a.a.a.b$j */
    /* compiled from: Easing */
    static class C3850j implements C3843c0 {
        C3850j() {
        }

        public float getInterpolation(float f) {
            float f2 = f * 2.0f;
            if (f2 < 1.0f) {
                return (((float) Math.sqrt((double) (1.0f - (f2 * f2)))) - 1.0f) * -0.5f;
            }
            float f3 = f2 - 2.0f;
            return (((float) Math.sqrt((double) (1.0f - (f3 * f3)))) + 1.0f) * 0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$k */
    /* compiled from: Easing */
    static class C3851k implements C3843c0 {
        C3851k() {
        }

        public float getInterpolation(float f) {
            return f;
        }
    }

    /* renamed from: e.c.a.a.a.b$l */
    /* compiled from: Easing */
    static class C3852l implements C3843c0 {
        C3852l() {
        }

        public float getInterpolation(float f) {
            if (f == 0.0f) {
                return 0.0f;
            }
            if (f == 1.0f) {
                return 1.0f;
            }
            float f2 = f - 1.0f;
            return -(((float) Math.pow(2.0d, (double) (10.0f * f2))) * ((float) Math.sin((double) (((f2 - (0.047746483f * ((float) Math.asin(1.0d)))) * 6.2831855f) / 0.3f))));
        }
    }

    /* renamed from: e.c.a.a.a.b$m */
    /* compiled from: Easing */
    static class C3853m implements C3843c0 {
        C3853m() {
        }

        public float getInterpolation(float f) {
            if (f == 0.0f) {
                return 0.0f;
            }
            if (f == 1.0f) {
                return 1.0f;
            }
            return (((float) Math.pow(2.0d, (double) (-10.0f * f))) * ((float) Math.sin((double) (((f - (0.047746483f * ((float) Math.asin(1.0d)))) * 6.2831855f) / 0.3f)))) + 1.0f;
        }
    }

    /* renamed from: e.c.a.a.a.b$n */
    /* compiled from: Easing */
    static class C3854n implements C3843c0 {
        C3854n() {
        }

        public float getInterpolation(float f) {
            if (f == 0.0f) {
                return 0.0f;
            }
            float f2 = f * 2.0f;
            if (f2 == 2.0f) {
                return 1.0f;
            }
            float asin = ((float) Math.asin(1.0d)) * 0.07161972f;
            if (f2 < 1.0f) {
                float f3 = f2 - 1.0f;
                return ((float) Math.pow(2.0d, (double) (10.0f * f3))) * ((float) Math.sin((double) (((f3 * 1.0f) - asin) * 6.2831855f * 2.2222223f))) * -0.5f;
            }
            float f4 = f2 - 1.0f;
            return (((float) Math.pow(2.0d, (double) (-10.0f * f4))) * 0.5f * ((float) Math.sin((double) (((f4 * 1.0f) - asin) * 6.2831855f * 2.2222223f)))) + 1.0f;
        }
    }

    /* renamed from: e.c.a.a.a.b$o */
    /* compiled from: Easing */
    static class C3855o implements C3843c0 {
        C3855o() {
        }

        public float getInterpolation(float f) {
            return f * f * ((f * 2.70158f) - 1.70158f);
        }
    }

    /* renamed from: e.c.a.a.a.b$p */
    /* compiled from: Easing */
    static class C3856p implements C3843c0 {
        C3856p() {
        }

        public float getInterpolation(float f) {
            float f2 = f - 1.0f;
            return (f2 * f2 * ((f2 * 2.70158f) + 1.70158f)) + 1.0f;
        }
    }

    /* renamed from: e.c.a.a.a.b$q */
    /* compiled from: Easing */
    static class C3857q implements C3843c0 {
        C3857q() {
        }

        public float getInterpolation(float f) {
            float f2 = f * 2.0f;
            if (f2 < 1.0f) {
                return f2 * f2 * ((3.5949094f * f2) - 2.5949094f) * 0.5f;
            }
            float f3 = f2 - 2.0f;
            return ((f3 * f3 * ((3.5949094f * f3) + 2.5949094f)) + 2.0f) * 0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$r */
    /* compiled from: Easing */
    static class C3858r implements C3843c0 {
        C3858r() {
        }

        public float getInterpolation(float f) {
            return 1.0f - Easing.f6928d.getInterpolation(1.0f - f);
        }
    }

    /* renamed from: e.c.a.a.a.b$s */
    /* compiled from: Easing */
    static class C3859s implements C3843c0 {
        C3859s() {
        }

        public float getInterpolation(float f) {
            if (f < 0.36363637f) {
                return 7.5625f * f * f;
            }
            if (f < 0.72727275f) {
                float f2 = f - 0.54545456f;
                return (7.5625f * f2 * f2) + 0.75f;
            } else if (f < 0.90909094f) {
                float f3 = f - 0.8181818f;
                return (7.5625f * f3 * f3) + 0.9375f;
            } else {
                float f4 = f - 0.95454544f;
                return (7.5625f * f4 * f4) + 0.984375f;
            }
        }
    }

    /* renamed from: e.c.a.a.a.b$t */
    /* compiled from: Easing */
    static class C3860t implements C3843c0 {
        C3860t() {
        }

        public float getInterpolation(float f) {
            if (f < 0.5f) {
                return Easing.f6927c.getInterpolation(f * 2.0f) * 0.5f;
            }
            return (Easing.f6928d.getInterpolation((f * 2.0f) - 1.0f) * 0.5f) + 0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$u */
    /* compiled from: Easing */
    static class C3861u implements C3843c0 {
        C3861u() {
        }

        public float getInterpolation(float f) {
            return f * f;
        }
    }

    /* renamed from: e.c.a.a.a.b$v */
    /* compiled from: Easing */
    static class C3862v implements C3843c0 {
        C3862v() {
        }

        public float getInterpolation(float f) {
            return (-f) * (f - 2.0f);
        }
    }

    /* renamed from: e.c.a.a.a.b$w */
    /* compiled from: Easing */
    static class C3863w implements C3843c0 {
        C3863w() {
        }

        public float getInterpolation(float f) {
            float f2 = f * 2.0f;
            if (f2 < 1.0f) {
                return 0.5f * f2 * f2;
            }
            float f3 = f2 - 1.0f;
            return ((f3 * (f3 - 2.0f)) - 1.0f) * -0.5f;
        }
    }

    /* renamed from: e.c.a.a.a.b$x */
    /* compiled from: Easing */
    static class C3864x implements C3843c0 {
        C3864x() {
        }

        public float getInterpolation(float f) {
            return (float) Math.pow((double) f, 3.0d);
        }
    }

    /* renamed from: e.c.a.a.a.b$y */
    /* compiled from: Easing */
    static class C3865y implements C3843c0 {
        C3865y() {
        }

        public float getInterpolation(float f) {
            return ((float) Math.pow((double) (f - 1.0f), 3.0d)) + 1.0f;
        }
    }

    /* renamed from: e.c.a.a.a.b$z */
    /* compiled from: Easing */
    static class C3866z implements C3843c0 {
        C3866z() {
        }

        public float getInterpolation(float f) {
            float pow;
            float f2 = f * 2.0f;
            if (f2 < 1.0f) {
                pow = (float) Math.pow((double) f2, 3.0d);
            } else {
                pow = ((float) Math.pow((double) (f2 - 2.0f), 3.0d)) + 2.0f;
            }
            return pow * 0.5f;
        }
    }

    static {
        new C3861u();
        new C3862v();
        new C3864x();
        new C3865y();
        new C3866z();
        new C3839a0();
        new C3841b0();
        new C3838a();
        new C3840b();
        new C3842c();
        new C3844d();
        new C3845e();
        new C3846f();
        new C3847g();
        new C3848h();
        new C3849i();
        new C3850j();
        new C3852l();
        new C3853m();
        new C3854n();
        new C3855o();
        new C3856p();
        new C3857q();
        new C3860t();
    }
}
