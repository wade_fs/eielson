package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.ScatterData;

/* renamed from: e.c.a.a.e.a.h */
public interface ScatterDataProvider extends BarLineScatterCandleBubbleDataProvider {
    ScatterData getScatterData();
}
