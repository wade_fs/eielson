package p119e.p128c.p129a.p130a.p138f;

import android.view.View;
import p119e.p128c.p129a.p130a.p143j.ObjectPool;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.f.b */
public abstract class ViewPortJob extends ObjectPool.C3875a implements Runnable {

    /* renamed from: R */
    protected float[] f6959R = new float[2];

    /* renamed from: S */
    protected ViewPortHandler f6960S;

    /* renamed from: T */
    protected float f6961T = 0.0f;

    /* renamed from: U */
    protected float f6962U = 0.0f;

    /* renamed from: V */
    protected Transformer f6963V;

    /* renamed from: W */
    protected View f6964W;

    public ViewPortJob(ViewPortHandler jVar, float f, float f2, Transformer gVar, View view) {
        this.f6960S = jVar;
        this.f6961T = f;
        this.f6962U = f2;
        this.f6963V = gVar;
        this.f6964W = view;
    }
}
