package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p136a.BarDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointD;

/* renamed from: e.c.a.a.d.e */
public class HorizontalBarHighlighter extends BarHighlighter {
    public HorizontalBarHighlighter(BarDataProvider aVar) {
        super(aVar);
    }

    /* renamed from: a */
    public Highlight mo23231a(float f, float f2) {
        BarData barData = ((BarDataProvider) this.f6941a).getBarData();
        MPPointD b = mo23238b(f2, f);
        Highlight a = mo23235a((float) b.f7121S, f2, f);
        if (a == null) {
            return null;
        }
        IBarDataSet aVar = (IBarDataSet) barData.mo13374a(a.mo23245c());
        if (aVar.mo13332t0()) {
            return mo23232a(a, aVar, (float) b.f7121S, (float) b.f7120R);
        }
        MPPointD.m11565a(b);
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<Highlight> mo23237a(IDataSet eVar, int i, float f, DataSet.C1658a aVar) {
        Entry a;
        ArrayList arrayList = new ArrayList();
        List<Entry> b = eVar.mo13411b(f);
        if (b.size() == 0 && (a = eVar.mo13407a(f, Float.NaN, aVar)) != null) {
            b = eVar.mo13411b(a.mo13315d());
        }
        if (b.size() == 0) {
            return arrayList;
        }
        for (Entry entry : b) {
            MPPointD a2 = ((BarDataProvider) this.f6941a).mo12952a(eVar.mo13364s()).mo23428a(entry.mo13303c(), entry.mo13315d());
            arrayList.add(new Highlight(entry.mo13315d(), entry.mo13303c(), (float) a2.f7120R, (float) a2.f7121S, i, eVar.mo13364s()));
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23228a(float f, float f2, float f3, float f4) {
        return Math.abs(f2 - f4);
    }
}
