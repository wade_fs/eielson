package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.DataSet;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p136a.BarDataProvider;
import p119e.p128c.p129a.p130a.p135e.p136a.CombinedDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;

/* renamed from: e.c.a.a.d.c */
public class CombinedHighlighter extends ChartHighlighter<CombinedDataProvider> implements IHighlighter {

    /* renamed from: c */
    protected BarHighlighter f6943c;

    public CombinedHighlighter(CombinedDataProvider fVar, BarDataProvider aVar) {
        super(fVar);
        this.f6943c = aVar.getBarData() == null ? null : new BarHighlighter(aVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public List<Highlight> mo23239b(float f, float f2, float f3) {
        super.f6942b.clear();
        List<BarLineScatterCandleBubbleData> k = ((CombinedDataProvider) super.f6941a).getCombinedData().mo13396k();
        for (int i = 0; i < k.size(); i++) {
            ChartData iVar = k.get(i);
            BarHighlighter aVar = this.f6943c;
            if (aVar == null || !(iVar instanceof BarData)) {
                int b = iVar.mo13383b();
                for (int i2 = 0; i2 < b; i2++) {
                    IDataSet a = k.get(i).mo13374a(i2);
                    if (a.mo13367w()) {
                        for (Highlight dVar : mo23237a(a, i2, f, DataSet.C1658a.CLOSEST)) {
                            dVar.mo23242a(i);
                            super.f6942b.add(dVar);
                        }
                    }
                }
            } else {
                Highlight a2 = aVar.mo23231a(f2, f3);
                if (a2 != null) {
                    a2.mo23242a(i);
                    super.f6942b.add(a2);
                }
            }
        }
        return super.f6942b;
    }
}
