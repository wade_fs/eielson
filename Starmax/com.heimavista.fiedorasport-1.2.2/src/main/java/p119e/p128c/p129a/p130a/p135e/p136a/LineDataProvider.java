package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.LineData;

/* renamed from: e.c.a.a.e.a.g */
public interface LineDataProvider extends BarLineScatterCandleBubbleDataProvider {
    LineData getLineData();
}
