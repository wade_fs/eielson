package p119e.p128c.p129a.p130a.p135e.p137b;

import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import java.util.List;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p140h.GradientColor;
import p119e.p128c.p129a.p130a.p143j.MPPointF;

/* renamed from: e.c.a.a.e.b.e */
public interface IDataSet<T extends Entry> {
    /* renamed from: a */
    float mo13404a();

    /* renamed from: a */
    int mo13405a(Entry entry);

    /* renamed from: a */
    T mo13406a(float f, float f2);

    /* renamed from: a */
    T mo13407a(float f, float f2, DataSet.C1658a aVar);

    /* renamed from: a */
    void mo13336a(float f);

    /* renamed from: a */
    void mo13337a(int i);

    /* renamed from: a */
    void mo13338a(Typeface typeface);

    /* renamed from: a */
    void mo13339a(ValueFormatter gVar);

    /* renamed from: b */
    float mo13408b();

    /* renamed from: b */
    T mo13410b(int i);

    /* renamed from: b */
    List<T> mo13411b(float f);

    /* renamed from: b */
    void mo13412b(float f, float f2);

    /* renamed from: c */
    int mo13345c(int i);

    /* renamed from: c */
    DashPathEffect mo13346c();

    /* renamed from: d */
    int mo13348d(int i);

    /* renamed from: d */
    boolean mo13349d();

    /* renamed from: e */
    Legend.C1649c mo13350e();

    /* renamed from: e */
    GradientColor mo13351e(int i);

    /* renamed from: f */
    String mo13352f();

    /* renamed from: g */
    float mo13415g();

    /* renamed from: h */
    GradientColor mo13353h();

    /* renamed from: i */
    float mo13354i();

    boolean isVisible();

    /* renamed from: j */
    ValueFormatter mo13356j();

    /* renamed from: k */
    float mo13357k();

    /* renamed from: l */
    float mo13358l();

    /* renamed from: m */
    Typeface mo13359m();

    /* renamed from: n */
    boolean mo13360n();

    /* renamed from: o */
    List<Integer> mo13361o();

    /* renamed from: p */
    List<GradientColor> mo13362p();

    /* renamed from: q */
    float mo13416q();

    /* renamed from: r */
    boolean mo13363r();

    /* renamed from: s */
    YAxis.C1655a mo13364s();

    /* renamed from: t */
    int mo13417t();

    /* renamed from: u */
    MPPointF mo13365u();

    /* renamed from: v */
    int mo13366v();

    /* renamed from: w */
    boolean mo13367w();
}
