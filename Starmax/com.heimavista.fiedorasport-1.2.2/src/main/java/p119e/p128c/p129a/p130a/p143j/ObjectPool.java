package p119e.p128c.p129a.p130a.p143j;

import p119e.p128c.p129a.p130a.p138f.MoveViewJob;
import p119e.p128c.p129a.p130a.p143j.ObjectPool.C3875a;

/* renamed from: e.c.a.a.j.f */
public class ObjectPool<T extends C3875a> {

    /* renamed from: g */
    private static int f7125g;

    /* renamed from: a */
    private int f7126a;

    /* renamed from: b */
    private int f7127b;

    /* renamed from: c */
    private Object[] f7128c;

    /* renamed from: d */
    private int f7129d;

    /* renamed from: e */
    private T f7130e;

    /* renamed from: f */
    private float f7131f;

    /* renamed from: e.c.a.a.j.f$a */
    /* compiled from: ObjectPool */
    public static abstract class C3875a {

        /* renamed from: Q */
        public static int f7132Q = -1;

        /* renamed from: P */
        int f7133P = f7132Q;

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract C3875a mo23300a();
    }

    private ObjectPool(int i, T t) {
        if (i > 0) {
            this.f7127b = i;
            this.f7128c = new Object[this.f7127b];
            this.f7129d = 0;
            this.f7130e = t;
            this.f7131f = 1.0f;
            m11574b();
            return;
        }
        throw new IllegalArgumentException("Object Pool must be instantiated with a capacity greater than 0!");
    }

    /* renamed from: a */
    public static synchronized ObjectPool m11573a(int i, C3875a aVar) {
        ObjectPool fVar;
        synchronized (ObjectPool.class) {
            fVar = new ObjectPool(i, aVar);
            fVar.f7126a = f7125g;
            f7125g++;
        }
        return fVar;
    }

    /* renamed from: b */
    private void m11574b() {
        m11575b(this.f7131f);
    }

    /* renamed from: c */
    private void m11576c() {
        int i = this.f7127b;
        this.f7127b = i * 2;
        Object[] objArr = new Object[this.f7127b];
        for (int i2 = 0; i2 < i; i2++) {
            objArr[i2] = this.f7128c[i2];
        }
        this.f7128c = objArr;
    }

    /* renamed from: b */
    private void m11575b(float f) {
        int i = this.f7127b;
        int i2 = (int) (((float) i) * f);
        if (i2 < 1) {
            i2 = 1;
        } else if (i2 > i) {
            i2 = i;
        }
        for (int i3 = 0; i3 < i2; i3++) {
            this.f7128c[i3] = this.f7130e.mo23300a();
        }
        this.f7129d = i2 - 1;
    }

    /* renamed from: a */
    public void mo23425a(float f) {
        if (f > 1.0f) {
            f = 1.0f;
        } else if (f < 0.0f) {
            f = 0.0f;
        }
        this.f7131f = f;
    }

    /* renamed from: a */
    public synchronized T mo23424a() {
        T t;
        if (this.f7129d == -1 && this.f7131f > 0.0f) {
            m11574b();
        }
        t = (C3875a) this.f7128c[this.f7129d];
        t.f7133P = C3875a.f7132Q;
        this.f7129d--;
        return t;
    }

    /* renamed from: a */
    public synchronized void mo23426a(MoveViewJob aVar) {
        if (aVar.f7133P == C3875a.f7132Q) {
            this.f7129d++;
            if (this.f7129d >= this.f7128c.length) {
                m11576c();
            }
            aVar.f7133P = this.f7126a;
            this.f7128c[this.f7129d] = aVar;
        } else if (aVar.f7133P == this.f7126a) {
            throw new IllegalArgumentException("The object passed is already stored in this pool!");
        } else {
            throw new IllegalArgumentException("The object to recycle already belongs to poolId " + aVar.f7133P + ".  Object cannot belong to two different pool instances simultaneously!");
        }
    }
}
