package p119e.p128c.p129a.p130a.p132b;

import com.github.mikephil.charting.data.BarEntry;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;

/* renamed from: e.c.a.a.b.c */
public class HorizontalBarBuffer extends BarBuffer {
    public HorizontalBarBuffer(int i, int i2, boolean z) {
        super(i, i2, z);
    }

    /* renamed from: a */
    public void mo23214a(IBarDataSet aVar) {
        float f;
        float f2;
        float f3;
        float f4;
        float t = ((float) aVar.mo13417t()) * this.f6931c;
        float f5 = super.f6935g / 2.0f;
        for (int i = 0; ((float) i) < t; i++) {
            BarEntry barEntry = (BarEntry) aVar.mo13410b(i);
            if (barEntry != null) {
                float d = barEntry.mo13315d();
                float c = barEntry.mo13303c();
                float[] i2 = barEntry.mo13308i();
                if (!super.f6933e || i2 == null) {
                    float f6 = d - f5;
                    float f7 = d + f5;
                    if (super.f6934f) {
                        float f8 = c >= 0.0f ? c : 0.0f;
                        if (c > 0.0f) {
                            c = 0.0f;
                        }
                        float f9 = c;
                        c = f8;
                        f = f9;
                    } else {
                        f = c >= 0.0f ? c : 0.0f;
                        if (c > 0.0f) {
                            c = 0.0f;
                        }
                    }
                    if (f > 0.0f) {
                        f *= this.f6932d;
                    } else {
                        c *= this.f6932d;
                    }
                    mo23212a(c, f7, f, f6);
                } else {
                    float f10 = -barEntry.mo13305f();
                    int i3 = 0;
                    float f11 = 0.0f;
                    while (i3 < i2.length) {
                        float f12 = i2[i3];
                        if (f12 >= 0.0f) {
                            f3 = f12 + f11;
                            f2 = f10;
                            f10 = f11;
                            f11 = f3;
                        } else {
                            float abs = Math.abs(f12) + f10;
                            f2 = Math.abs(f12) + f10;
                            f3 = abs;
                        }
                        float f13 = d - f5;
                        float f14 = d + f5;
                        if (super.f6934f) {
                            float f15 = f10 >= f3 ? f10 : f3;
                            if (f10 > f3) {
                                f10 = f3;
                            }
                            float f16 = f10;
                            f10 = f15;
                            f4 = f16;
                        } else {
                            f4 = f10 >= f3 ? f10 : f3;
                            if (f10 > f3) {
                                f10 = f3;
                            }
                        }
                        float f17 = this.f6932d;
                        mo23212a(f10 * f17, f14, f4 * f17, f13);
                        i3++;
                        f10 = f2;
                    }
                }
            }
        }
        mo23208a();
    }
}
