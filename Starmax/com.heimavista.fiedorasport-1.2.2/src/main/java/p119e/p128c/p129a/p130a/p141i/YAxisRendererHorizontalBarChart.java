package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import java.util.List;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.u */
public class YAxisRendererHorizontalBarChart extends YAxisRenderer {

    /* renamed from: r */
    protected Path f7111r = new Path();

    /* renamed from: s */
    protected Path f7112s = new Path();

    /* renamed from: t */
    protected float[] f7113t = new float[4];

    public YAxisRendererHorizontalBarChart(ViewPortHandler jVar, YAxis iVar, Transformer gVar) {
        super(jVar, iVar, gVar);
        this.f7009g.setTextAlign(Paint.Align.LEFT);
    }

    /* renamed from: a */
    public void mo23336a(float f, float f2, boolean z) {
        float f3;
        double d;
        if (this.f7088a.mo23461f() > 10.0f && !this.f7088a.mo23484v()) {
            MPPointD b = this.f7005c.mo23440b(this.f7088a.mo23463g(), this.f7088a.mo23467i());
            MPPointD b2 = this.f7005c.mo23440b(this.f7088a.mo23465h(), this.f7088a.mo23467i());
            if (!z) {
                f3 = (float) b.f7120R;
                d = b2.f7120R;
            } else {
                f3 = (float) b2.f7120R;
                d = b.f7120R;
            }
            MPPointD.m11565a(b);
            MPPointD.m11565a(b2);
            f = f3;
            f2 = (float) d;
        }
        mo23335a(f, f2);
    }

    /* renamed from: b */
    public void mo23411b(Canvas canvas) {
        float f;
        float f2;
        float f3;
        if (super.f7101h.mo13240f() && super.f7101h.mo13225v()) {
            float[] c = mo23413c();
            this.f7007e.setTypeface(super.f7101h.mo13236c());
            this.f7007e.setTextSize(super.f7101h.mo13234b());
            this.f7007e.setColor(super.f7101h.mo13229a());
            this.f7007e.setTextAlign(Paint.Align.CENTER);
            float a = Utils.m11599a(2.5f);
            float a2 = (float) Utils.m11602a(this.f7007e, "Q");
            YAxis.C1655a z = super.f7101h.mo13302z();
            YAxis.C1656b A = super.f7101h.mo13286A();
            if (z == YAxis.C1655a.LEFT) {
                if (A == YAxis.C1656b.OUTSIDE_CHART) {
                    f3 = this.f7088a.mo23467i();
                } else {
                    f3 = this.f7088a.mo23467i();
                }
                f = f3 - a;
            } else {
                if (A == YAxis.C1656b.OUTSIDE_CHART) {
                    f2 = this.f7088a.mo23459e();
                } else {
                    f2 = this.f7088a.mo23459e();
                }
                f = f2 + a2 + a;
            }
            mo23409a(canvas, f, c, super.f7101h.mo13239e());
        }
    }

    /* renamed from: c */
    public void mo23412c(Canvas canvas) {
        if (super.f7101h.mo13240f() && super.f7101h.mo13222s()) {
            this.f7008f.setColor(super.f7101h.mo13209g());
            this.f7008f.setStrokeWidth(super.f7101h.mo13212i());
            if (super.f7101h.mo13302z() == YAxis.C1655a.LEFT) {
                canvas.drawLine(this.f7088a.mo23463g(), this.f7088a.mo23467i(), this.f7088a.mo23465h(), this.f7088a.mo23467i(), this.f7008f);
                return;
            }
            canvas.drawLine(this.f7088a.mo23463g(), this.f7088a.mo23459e(), this.f7088a.mo23465h(), this.f7088a.mo23459e(), this.f7008f);
        }
    }

    /* renamed from: e */
    public void mo23415e(Canvas canvas) {
        Canvas canvas2 = canvas;
        List<LimitLine> o = super.f7101h.mo13218o();
        if (o != null && o.size() > 0) {
            float[] fArr = this.f7113t;
            float f = 0.0f;
            fArr[0] = 0.0f;
            char c = 1;
            fArr[1] = 0.0f;
            fArr[2] = 0.0f;
            fArr[3] = 0.0f;
            Path path = this.f7112s;
            path.reset();
            int i = 0;
            while (i < o.size()) {
                LimitLine gVar = o.get(i);
                if (gVar.mo13240f()) {
                    int save = canvas.save();
                    super.f7110q.set(this.f7088a.mo23476n());
                    super.f7110q.inset(-gVar.mo13280l(), f);
                    canvas2.clipRect(super.f7110q);
                    fArr[0] = gVar.mo13278j();
                    fArr[2] = gVar.mo13278j();
                    this.f7005c.mo23442b(fArr);
                    fArr[c] = this.f7088a.mo23467i();
                    fArr[3] = this.f7088a.mo23459e();
                    path.moveTo(fArr[0], fArr[c]);
                    path.lineTo(fArr[2], fArr[3]);
                    this.f7009g.setStyle(Paint.Style.STROKE);
                    this.f7009g.setColor(gVar.mo13279k());
                    this.f7009g.setPathEffect(gVar.mo13275g());
                    this.f7009g.setStrokeWidth(gVar.mo13280l());
                    canvas2.drawPath(path, this.f7009g);
                    path.reset();
                    String h = gVar.mo13276h();
                    if (h != null && !h.equals("")) {
                        this.f7009g.setStyle(gVar.mo13281m());
                        this.f7009g.setPathEffect(null);
                        this.f7009g.setColor(gVar.mo13229a());
                        this.f7009g.setTypeface(gVar.mo13236c());
                        this.f7009g.setStrokeWidth(0.5f);
                        this.f7009g.setTextSize(gVar.mo13234b());
                        float l = gVar.mo13280l() + gVar.mo13238d();
                        float a = Utils.m11599a(2.0f) + gVar.mo13239e();
                        LimitLine.C1653a i2 = gVar.mo13277i();
                        if (i2 == LimitLine.C1653a.RIGHT_TOP) {
                            this.f7009g.setTextAlign(Paint.Align.LEFT);
                            canvas2.drawText(h, fArr[0] + l, this.f7088a.mo23467i() + a + ((float) Utils.m11602a(this.f7009g, h)), this.f7009g);
                        } else if (i2 == LimitLine.C1653a.RIGHT_BOTTOM) {
                            this.f7009g.setTextAlign(Paint.Align.LEFT);
                            canvas2.drawText(h, fArr[0] + l, this.f7088a.mo23459e() - a, this.f7009g);
                        } else if (i2 == LimitLine.C1653a.LEFT_TOP) {
                            this.f7009g.setTextAlign(Paint.Align.RIGHT);
                            canvas2.drawText(h, fArr[0] - l, this.f7088a.mo23467i() + a + ((float) Utils.m11602a(this.f7009g, h)), this.f7009g);
                        } else {
                            this.f7009g.setTextAlign(Paint.Align.RIGHT);
                            canvas2.drawText(h, fArr[0] - l, this.f7088a.mo23459e() - a, this.f7009g);
                        }
                    }
                    canvas2.restoreToCount(save);
                }
                i++;
                f = 0.0f;
                c = 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public float[] mo23413c() {
        int length = super.f7105l.length;
        int i = super.f7101h.f2359n;
        if (length != i * 2) {
            super.f7105l = new float[(i * 2)];
        }
        float[] fArr = super.f7105l;
        for (int i2 = 0; i2 < fArr.length; i2 += 2) {
            fArr[i2] = super.f7101h.f2357l[i2 / 2];
        }
        this.f7005c.mo23442b(fArr);
        return fArr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23409a(Canvas canvas, float f, float[] fArr, float f2) {
        this.f7007e.setTypeface(super.f7101h.mo13236c());
        this.f7007e.setTextSize(super.f7101h.mo13234b());
        this.f7007e.setColor(super.f7101h.mo13229a());
        int i = super.f7101h.mo13294I() ? super.f7101h.f2359n : super.f7101h.f2359n - 1;
        for (int i2 = !super.f7101h.mo13293H(); i2 < i; i2++) {
            canvas.drawText(super.f7101h.mo13201b(i2), fArr[i2 * 2], f - f2, this.f7007e);
        }
    }

    /* renamed from: b */
    public RectF mo23410b() {
        super.f7104k.set(this.f7088a.mo23476n());
        super.f7104k.inset(-this.f7004b.mo13216m(), 0.0f);
        return super.f7104k;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Path mo23407a(Path path, int i, float[] fArr) {
        path.moveTo(fArr[i], this.f7088a.mo23467i());
        path.lineTo(fArr[i], this.f7088a.mo23459e());
        return path;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23408a(Canvas canvas) {
        int save = canvas.save();
        super.f7107n.set(this.f7088a.mo23476n());
        super.f7107n.inset(-super.f7101h.mo13292G(), 0.0f);
        canvas.clipRect(super.f7110q);
        MPPointD a = this.f7005c.mo23428a(0.0f, 0.0f);
        super.f7102i.setColor(super.f7101h.mo13291F());
        super.f7102i.setStrokeWidth(super.f7101h.mo13292G());
        Path path = this.f7111r;
        path.reset();
        path.moveTo(((float) a.f7120R) - 1.0f, this.f7088a.mo23467i());
        path.lineTo(((float) a.f7120R) - 1.0f, this.f7088a.mo23459e());
        canvas.drawPath(path, super.f7102i);
        canvas.restoreToCount(save);
    }
}
