package p119e.p128c.p129a.p130a.p139g;

import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p134d.Highlight;

/* renamed from: e.c.a.a.g.d */
public interface OnChartValueSelectedListener {
    /* renamed from: a */
    void mo23322a();

    /* renamed from: a */
    void mo23323a(Entry entry, Highlight dVar);
}
