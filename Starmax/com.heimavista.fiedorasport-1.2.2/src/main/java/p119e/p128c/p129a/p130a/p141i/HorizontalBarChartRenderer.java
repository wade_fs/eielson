package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p132b.BarBuffer;
import p119e.p128c.p129a.p130a.p132b.HorizontalBarBuffer;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.BarDataProvider;
import p119e.p128c.p129a.p130a.p135e.p136a.ChartInterface;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.h */
public class HorizontalBarChartRenderer extends BarChartRenderer {

    /* renamed from: m */
    private RectF f7039m = new RectF();

    public HorizontalBarChartRenderer(BarDataProvider aVar, ChartAnimator aVar2, ViewPortHandler jVar) {
        super(aVar, aVar2, jVar);
        this.f7038e.setTextAlign(Paint.Align.LEFT);
    }

    /* renamed from: a */
    public void mo23337a() {
        BarData barData = super.f7010g.getBarData();
        super.f7012i = new HorizontalBarBuffer[barData.mo13383b()];
        for (int i = 0; i < super.f7012i.length; i++) {
            IBarDataSet aVar = (IBarDataSet) barData.mo13374a(i);
            super.f7012i[i] = new HorizontalBarBuffer(aVar.mo13417t() * 4 * (aVar.mo13332t0() ? aVar.mo13330q0() : 1), barData.mo13383b(), aVar.mo13332t0());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:142:0x03ae  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x03b1  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo23345c(android.graphics.Canvas r40) {
        /*
            r39 = this;
            r6 = r39
            e.c.a.a.e.a.a r0 = r6.f7010g
            boolean r0 = r6.mo23356a(r0)
            if (r0 == 0) goto L_0x03c9
            e.c.a.a.e.a.a r0 = r6.f7010g
            com.github.mikephil.charting.data.a r0 = r0.getBarData()
            java.util.List r7 = r0.mo13386c()
            r0 = 1084227584(0x40a00000, float:5.0)
            float r8 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r0)
            e.c.a.a.e.a.a r0 = r6.f7010g
            boolean r9 = r0.mo12939b()
            r11 = 0
        L_0x0021:
            e.c.a.a.e.a.a r0 = r6.f7010g
            com.github.mikephil.charting.data.a r0 = r0.getBarData()
            int r0 = r0.mo13383b()
            if (r11 >= r0) goto L_0x03c9
            java.lang.Object r0 = r7.get(r11)
            r12 = r0
            e.c.a.a.e.b.a r12 = (p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet) r12
            boolean r0 = r6.mo23347b(r12)
            if (r0 != 0) goto L_0x0042
            r19 = r7
            r26 = r9
            r20 = r11
            goto L_0x03c1
        L_0x0042:
            e.c.a.a.e.a.a r0 = r6.f7010g
            com.github.mikephil.charting.components.i$a r1 = r12.mo13364s()
            boolean r13 = r0.mo12956b(r1)
            r6.mo23355a(r12)
            android.graphics.Paint r0 = r6.f7038e
            java.lang.String r1 = "10"
            int r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11602a(r0, r1)
            float r0 = (float) r0
            r14 = 1073741824(0x40000000, float:2.0)
            float r15 = r0 / r14
            e.c.a.a.c.g r5 = r12.mo13356j()
            e.c.a.a.b.b[] r0 = r6.f7012i
            r4 = r0[r11]
            e.c.a.a.a.a r0 = r6.f7035b
            float r16 = r0.mo23204b()
            e.c.a.a.j.e r0 = r12.mo13365u()
            e.c.a.a.j.e r3 = p119e.p128c.p129a.p130a.p143j.MPPointF.m11568a(r0)
            float r0 = r3.f7123R
            float r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r0)
            r3.f7123R = r0
            float r0 = r3.f7124S
            float r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r0)
            r3.f7124S = r0
            boolean r0 = r12.mo13332t0()
            r17 = 0
            if (r0 != 0) goto L_0x01a1
            r2 = 0
        L_0x008b:
            float r0 = (float) r2
            float[] r1 = r4.f6930b
            int r1 = r1.length
            float r1 = (float) r1
            e.c.a.a.a.a r10 = r6.f7035b
            float r10 = r10.mo23201a()
            float r1 = r1 * r10
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x019a
            float[] r0 = r4.f6930b
            int r1 = r2 + 1
            r10 = r0[r1]
            int r16 = r2 + 3
            r16 = r0[r16]
            float r10 = r10 + r16
            float r10 = r10 / r14
            e.c.a.a.j.j r14 = r6.f7088a
            r0 = r0[r1]
            boolean r0 = r14.mo23458d(r0)
            if (r0 != 0) goto L_0x00b5
            goto L_0x019a
        L_0x00b5:
            e.c.a.a.j.j r0 = r6.f7088a
            float[] r14 = r4.f6930b
            r14 = r14[r2]
            boolean r0 = r0.mo23460e(r14)
            if (r0 != 0) goto L_0x00ce
        L_0x00c1:
            r25 = r2
            r19 = r7
            r20 = r11
            r23 = r15
            r7 = r3
            r11 = r4
            r15 = r5
            goto L_0x018b
        L_0x00ce:
            e.c.a.a.j.j r0 = r6.f7088a
            float[] r14 = r4.f6930b
            r1 = r14[r1]
            boolean r0 = r0.mo23450a(r1)
            if (r0 != 0) goto L_0x00db
            goto L_0x00c1
        L_0x00db:
            int r0 = r2 / 4
            com.github.mikephil.charting.data.Entry r0 = r12.mo13410b(r0)
            r14 = r0
            com.github.mikephil.charting.data.BarEntry r14 = (com.github.mikephil.charting.data.BarEntry) r14
            float r16 = r14.mo13303c()
            java.lang.String r1 = r5.mo23223a(r14)
            android.graphics.Paint r0 = r6.f7038e
            int r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11621c(r0, r1)
            float r0 = (float) r0
            r19 = r1
            if (r9 == 0) goto L_0x00f9
            r1 = r8
            goto L_0x00fc
        L_0x00f9:
            float r1 = r0 + r8
            float r1 = -r1
        L_0x00fc:
            r20 = r3
            if (r9 == 0) goto L_0x0104
            float r3 = r0 + r8
            float r3 = -r3
            goto L_0x0105
        L_0x0104:
            r3 = r8
        L_0x0105:
            if (r13 == 0) goto L_0x010b
            float r1 = -r1
            float r1 = r1 - r0
            float r3 = -r3
            float r3 = r3 - r0
        L_0x010b:
            r22 = r1
            r21 = r3
            boolean r0 = r12.mo13363r()
            if (r0 == 0) goto L_0x0148
            float[] r0 = r4.f6930b
            int r1 = r2 + 2
            r0 = r0[r1]
            int r1 = (r16 > r17 ? 1 : (r16 == r17 ? 0 : -1))
            if (r1 < 0) goto L_0x0122
            r1 = r22
            goto L_0x0124
        L_0x0122:
            r1 = r21
        L_0x0124:
            float r3 = r0 + r1
            float r23 = r10 + r15
            int r0 = r2 / 2
            int r24 = r12.mo13348d(r0)
            r0 = r39
            r1 = r40
            r25 = r2
            r2 = r19
            r19 = r7
            r7 = r20
            r20 = r11
            r11 = r4
            r4 = r23
            r23 = r15
            r15 = r5
            r5 = r24
            r0.mo23341a(r1, r2, r3, r4, r5)
            goto L_0x0154
        L_0x0148:
            r25 = r2
            r19 = r7
            r23 = r15
            r7 = r20
            r15 = r5
            r20 = r11
            r11 = r4
        L_0x0154:
            android.graphics.drawable.Drawable r0 = r14.mo13371b()
            if (r0 == 0) goto L_0x018b
            boolean r0 = r12.mo13349d()
            if (r0 == 0) goto L_0x018b
            android.graphics.drawable.Drawable r27 = r14.mo13371b()
            float[] r0 = r11.f6930b
            int r2 = r25 + 2
            r0 = r0[r2]
            int r1 = (r16 > r17 ? 1 : (r16 == r17 ? 0 : -1))
            if (r1 < 0) goto L_0x0170
            r21 = r22
        L_0x0170:
            float r0 = r0 + r21
            float r1 = r7.f7123R
            float r0 = r0 + r1
            float r1 = r7.f7124S
            float r10 = r10 + r1
            int r0 = (int) r0
            int r1 = (int) r10
            int r30 = r27.getIntrinsicWidth()
            int r31 = r27.getIntrinsicHeight()
            r26 = r40
            r28 = r0
            r29 = r1
            p119e.p128c.p129a.p130a.p143j.Utils.m11606a(r26, r27, r28, r29, r30, r31)
        L_0x018b:
            int r2 = r25 + 4
            r3 = r7
            r4 = r11
            r5 = r15
            r7 = r19
            r11 = r20
            r15 = r23
            r14 = 1073741824(0x40000000, float:2.0)
            goto L_0x008b
        L_0x019a:
            r19 = r7
            r20 = r11
            r7 = r3
            goto L_0x03bc
        L_0x01a1:
            r19 = r7
            r20 = r11
            r23 = r15
            r7 = r3
            r11 = r4
            r15 = r5
            e.c.a.a.e.a.a r0 = r6.f7010g
            com.github.mikephil.charting.components.i$a r1 = r12.mo13364s()
            e.c.a.a.j.g r10 = r0.mo12952a(r1)
            r14 = 0
            r21 = 0
        L_0x01b7:
            float r0 = (float) r14
            int r1 = r12.mo13417t()
            float r1 = (float) r1
            e.c.a.a.a.a r2 = r6.f7035b
            float r2 = r2.mo23201a()
            float r1 = r1 * r2
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 >= 0) goto L_0x03bc
            com.github.mikephil.charting.data.Entry r0 = r12.mo13410b(r14)
            r5 = r0
            com.github.mikephil.charting.data.BarEntry r5 = (com.github.mikephil.charting.data.BarEntry) r5
            int r22 = r12.mo13348d(r14)
            float[] r4 = r5.mo13308i()
            if (r4 != 0) goto L_0x029f
            e.c.a.a.j.j r0 = r6.f7088a
            float[] r1 = r11.f6930b
            int r24 = r21 + 1
            r1 = r1[r24]
            boolean r0 = r0.mo23458d(r1)
            if (r0 != 0) goto L_0x01ea
            goto L_0x03bc
        L_0x01ea:
            e.c.a.a.j.j r0 = r6.f7088a
            float[] r1 = r11.f6930b
            r1 = r1[r21]
            boolean r0 = r0.mo23460e(r1)
            if (r0 != 0) goto L_0x01f7
            goto L_0x01b7
        L_0x01f7:
            e.c.a.a.j.j r0 = r6.f7088a
            float[] r1 = r11.f6930b
            r1 = r1[r24]
            boolean r0 = r0.mo23450a(r1)
            if (r0 != 0) goto L_0x0204
            goto L_0x01b7
        L_0x0204:
            java.lang.String r2 = r15.mo23223a(r5)
            android.graphics.Paint r0 = r6.f7038e
            int r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11621c(r0, r2)
            float r0 = (float) r0
            if (r9 == 0) goto L_0x0213
            r1 = r8
            goto L_0x0216
        L_0x0213:
            float r1 = r0 + r8
            float r1 = -r1
        L_0x0216:
            if (r9 == 0) goto L_0x021c
            float r3 = r0 + r8
            float r3 = -r3
            goto L_0x021d
        L_0x021c:
            r3 = r8
        L_0x021d:
            if (r13 == 0) goto L_0x0223
            float r1 = -r1
            float r1 = r1 - r0
            float r3 = -r3
            float r3 = r3 - r0
        L_0x0223:
            r26 = r1
            r25 = r3
            boolean r0 = r12.mo13363r()
            if (r0 == 0) goto L_0x0259
            float[] r0 = r11.f6930b
            int r1 = r21 + 2
            r0 = r0[r1]
            float r1 = r5.mo13303c()
            int r1 = (r1 > r17 ? 1 : (r1 == r17 ? 0 : -1))
            if (r1 < 0) goto L_0x023e
            r1 = r26
            goto L_0x0240
        L_0x023e:
            r1 = r25
        L_0x0240:
            float r3 = r0 + r1
            float[] r0 = r11.f6930b
            r0 = r0[r24]
            float r27 = r0 + r23
            r0 = r39
            r1 = r40
            r28 = r14
            r14 = r4
            r4 = r27
            r27 = r5
            r5 = r22
            r0.mo23341a(r1, r2, r3, r4, r5)
            goto L_0x025e
        L_0x0259:
            r27 = r5
            r28 = r14
            r14 = r4
        L_0x025e:
            android.graphics.drawable.Drawable r0 = r27.mo13371b()
            if (r0 == 0) goto L_0x03a8
            boolean r0 = r12.mo13349d()
            if (r0 == 0) goto L_0x03a8
            android.graphics.drawable.Drawable r30 = r27.mo13371b()
            float[] r0 = r11.f6930b
            int r1 = r21 + 2
            r0 = r0[r1]
            float r1 = r27.mo13303c()
            int r1 = (r1 > r17 ? 1 : (r1 == r17 ? 0 : -1))
            if (r1 < 0) goto L_0x027e
            r25 = r26
        L_0x027e:
            float r0 = r0 + r25
            float[] r1 = r11.f6930b
            r1 = r1[r24]
            float r2 = r7.f7123R
            float r0 = r0 + r2
            float r2 = r7.f7124S
            float r1 = r1 + r2
            int r0 = (int) r0
            int r1 = (int) r1
            int r33 = r30.getIntrinsicWidth()
            int r34 = r30.getIntrinsicHeight()
            r29 = r40
            r31 = r0
            r32 = r1
            p119e.p128c.p129a.p130a.p143j.Utils.m11606a(r29, r30, r31, r32, r33, r34)
            goto L_0x03a8
        L_0x029f:
            r27 = r5
            r28 = r14
            r14 = r4
            int r0 = r14.length
            int r0 = r0 * 2
            float[] r5 = new float[r0]
            float r0 = r27.mo13305f()
            float r0 = -r0
            r25 = r0
            r0 = 0
            r1 = 0
            r24 = 0
        L_0x02b4:
            int r2 = r5.length
            if (r0 >= r2) goto L_0x02de
            r2 = r14[r1]
            int r3 = (r2 > r17 ? 1 : (r2 == r17 ? 0 : -1))
            if (r3 != 0) goto L_0x02c6
            int r4 = (r24 > r17 ? 1 : (r24 == r17 ? 0 : -1))
            if (r4 == 0) goto L_0x02d5
            int r4 = (r25 > r17 ? 1 : (r25 == r17 ? 0 : -1))
            if (r4 != 0) goto L_0x02c6
            goto L_0x02d5
        L_0x02c6:
            if (r3 < 0) goto L_0x02cd
            float r2 = r24 + r2
            r24 = r2
            goto L_0x02d5
        L_0x02cd:
            float r2 = r25 - r2
            r38 = r25
            r25 = r2
            r2 = r38
        L_0x02d5:
            float r2 = r2 * r16
            r5[r0] = r2
            int r0 = r0 + 2
            int r1 = r1 + 1
            goto L_0x02b4
        L_0x02de:
            r10.mo23442b(r5)
            r4 = 0
        L_0x02e2:
            int r0 = r5.length
            if (r4 >= r0) goto L_0x03a8
            int r0 = r4 / 2
            r0 = r14[r0]
            r3 = r27
            java.lang.String r2 = r15.mo23222a(r0, r3)
            android.graphics.Paint r1 = r6.f7038e
            int r1 = p119e.p128c.p129a.p130a.p143j.Utils.m11621c(r1, r2)
            float r1 = (float) r1
            if (r9 == 0) goto L_0x02fa
            r3 = r8
            goto L_0x02fd
        L_0x02fa:
            float r3 = r1 + r8
            float r3 = -r3
        L_0x02fd:
            r26 = r9
            if (r9 == 0) goto L_0x0305
            float r9 = r1 + r8
            float r9 = -r9
            goto L_0x0306
        L_0x0305:
            r9 = r8
        L_0x0306:
            if (r13 == 0) goto L_0x030c
            float r3 = -r3
            float r3 = r3 - r1
            float r9 = -r9
            float r9 = r9 - r1
        L_0x030c:
            int r1 = (r0 > r17 ? 1 : (r0 == r17 ? 0 : -1))
            if (r1 != 0) goto L_0x0318
            int r1 = (r25 > r17 ? 1 : (r25 == r17 ? 0 : -1))
            if (r1 != 0) goto L_0x0318
            int r1 = (r24 > r17 ? 1 : (r24 == r17 ? 0 : -1))
            if (r1 > 0) goto L_0x031c
        L_0x0318:
            int r0 = (r0 > r17 ? 1 : (r0 == r17 ? 0 : -1))
            if (r0 >= 0) goto L_0x031e
        L_0x031c:
            r0 = 1
            goto L_0x031f
        L_0x031e:
            r0 = 0
        L_0x031f:
            r1 = r5[r4]
            if (r0 == 0) goto L_0x0324
            r3 = r9
        L_0x0324:
            float r9 = r1 + r3
            float[] r0 = r11.f6930b
            int r1 = r21 + 1
            r1 = r0[r1]
            int r3 = r21 + 3
            r0 = r0[r3]
            float r1 = r1 + r0
            r18 = 1073741824(0x40000000, float:2.0)
            float r3 = r1 / r18
            e.c.a.a.j.j r0 = r6.f7088a
            boolean r0 = r0.mo23458d(r3)
            if (r0 != 0) goto L_0x033f
            goto L_0x03ac
        L_0x033f:
            e.c.a.a.j.j r0 = r6.f7088a
            boolean r0 = r0.mo23460e(r9)
            if (r0 != 0) goto L_0x034c
        L_0x0347:
            r31 = r4
            r29 = r5
            goto L_0x03a0
        L_0x034c:
            e.c.a.a.j.j r0 = r6.f7088a
            boolean r0 = r0.mo23450a(r3)
            if (r0 != 0) goto L_0x0355
            goto L_0x0347
        L_0x0355:
            boolean r0 = r12.mo13363r()
            if (r0 == 0) goto L_0x0370
            float r29 = r3 + r23
            r0 = r39
            r1 = r40
            r30 = r3
            r3 = r9
            r31 = r4
            r4 = r29
            r29 = r5
            r5 = r22
            r0.mo23341a(r1, r2, r3, r4, r5)
            goto L_0x0376
        L_0x0370:
            r30 = r3
            r31 = r4
            r29 = r5
        L_0x0376:
            android.graphics.drawable.Drawable r0 = r27.mo13371b()
            if (r0 == 0) goto L_0x03a0
            boolean r0 = r12.mo13349d()
            if (r0 == 0) goto L_0x03a0
            android.graphics.drawable.Drawable r33 = r27.mo13371b()
            float r0 = r7.f7123R
            float r9 = r9 + r0
            int r0 = (int) r9
            float r1 = r7.f7124S
            float r3 = r30 + r1
            int r1 = (int) r3
            int r36 = r33.getIntrinsicWidth()
            int r37 = r33.getIntrinsicHeight()
            r32 = r40
            r34 = r0
            r35 = r1
            p119e.p128c.p129a.p130a.p143j.Utils.m11606a(r32, r33, r34, r35, r36, r37)
        L_0x03a0:
            int r4 = r31 + 2
            r9 = r26
            r5 = r29
            goto L_0x02e2
        L_0x03a8:
            r26 = r9
            r18 = 1073741824(0x40000000, float:2.0)
        L_0x03ac:
            if (r14 != 0) goto L_0x03b1
            int r21 = r21 + 4
            goto L_0x03b6
        L_0x03b1:
            int r0 = r14.length
            int r0 = r0 * 4
            int r21 = r21 + r0
        L_0x03b6:
            int r14 = r28 + 1
            r9 = r26
            goto L_0x01b7
        L_0x03bc:
            r26 = r9
            p119e.p128c.p129a.p130a.p143j.MPPointF.m11570b(r7)
        L_0x03c1:
            int r11 = r20 + 1
            r7 = r19
            r9 = r26
            goto L_0x0021
        L_0x03c9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p128c.p129a.p130a.p141i.HorizontalBarChartRenderer.mo23345c(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23340a(Canvas canvas, IBarDataSet aVar, int i) {
        IBarDataSet aVar2 = aVar;
        int i2 = i;
        Transformer a = super.f7010g.mo12952a(aVar.mo13364s());
        super.f7014k.setColor(aVar.mo13327m0());
        super.f7014k.setStrokeWidth(Utils.m11599a(aVar.mo13328n0()));
        int i3 = 0;
        boolean z = true;
        boolean z2 = aVar.mo13328n0() > 0.0f;
        float a2 = this.f7035b.mo23201a();
        float b = this.f7035b.mo23204b();
        if (super.f7010g.mo12938a()) {
            super.f7013j.setColor(aVar.mo13329p0());
            float k = super.f7010g.getBarData().mo13323k() / 2.0f;
            int min = Math.min((int) Math.ceil((double) (((float) aVar.mo13417t()) * a2)), aVar.mo13417t());
            for (int i4 = 0; i4 < min; i4++) {
                float d = ((BarEntry) aVar2.mo13410b(i4)).mo13315d();
                RectF rectF = this.f7039m;
                rectF.top = d - k;
                rectF.bottom = d + k;
                a.mo23432a(rectF);
                if (this.f7088a.mo23458d(this.f7039m.bottom)) {
                    if (!this.f7088a.mo23450a(this.f7039m.top)) {
                        break;
                    }
                    this.f7039m.left = this.f7088a.mo23463g();
                    this.f7039m.right = this.f7088a.mo23465h();
                    canvas.drawRect(this.f7039m, super.f7013j);
                }
            }
        }
        BarBuffer bVar = super.f7012i[i2];
        bVar.mo23209a(a2, b);
        bVar.mo23213a(i2);
        bVar.mo23215a(super.f7010g.mo12956b(aVar.mo13364s()));
        bVar.mo23211a(super.f7010g.getBarData().mo13323k());
        bVar.mo23214a(aVar2);
        a.mo23442b(bVar.f6930b);
        if (aVar.mo13361o().size() != 1) {
            z = false;
        }
        if (z) {
            this.f7036c.setColor(aVar.mo13366v());
        }
        while (i3 < bVar.mo23210b()) {
            int i5 = i3 + 3;
            if (this.f7088a.mo23458d(bVar.f6930b[i5])) {
                int i6 = i3 + 1;
                if (this.f7088a.mo23450a(bVar.f6930b[i6])) {
                    if (!z) {
                        this.f7036c.setColor(aVar2.mo13345c(i3 / 4));
                    }
                    float[] fArr = bVar.f6930b;
                    int i7 = i3 + 2;
                    canvas.drawRect(fArr[i3], fArr[i6], fArr[i7], fArr[i5], this.f7036c);
                    if (z2) {
                        float[] fArr2 = bVar.f6930b;
                        canvas.drawRect(fArr2[i3], fArr2[i6], fArr2[i7], fArr2[i5], super.f7014k);
                    }
                }
                i3 += 4;
            } else {
                return;
            }
        }
    }

    /* renamed from: a */
    public void mo23341a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23338a(float f, float f2, float f3, float f4, Transformer gVar) {
        super.f7011h.set(f2, f - f4, f3, f + f4);
        gVar.mo23441b(super.f7011h, this.f7035b.mo23204b());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23343a(Highlight dVar, RectF rectF) {
        dVar.mo23241a(rectF.centerY(), rectF.right);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo23356a(ChartInterface eVar) {
        return ((float) eVar.getData().mo13387d()) < ((float) eVar.getMaxVisibleCount()) * this.f7088a.mo23479q();
    }
}
