package p119e.p128c.p129a.p130a.p132b;

/* renamed from: e.c.a.a.b.a */
public abstract class AbstractBuffer<T> {

    /* renamed from: a */
    protected int f6929a;

    /* renamed from: b */
    public final float[] f6930b;

    /* renamed from: c */
    protected float f6931c;

    /* renamed from: d */
    protected float f6932d;

    public AbstractBuffer(int i) {
        this.f6929a = 0;
        this.f6931c = 1.0f;
        this.f6932d = 1.0f;
        this.f6929a = 0;
        this.f6930b = new float[i];
    }

    /* renamed from: a */
    public void mo23208a() {
        this.f6929a = 0;
    }

    /* renamed from: b */
    public int mo23210b() {
        return this.f6930b.length;
    }

    /* renamed from: a */
    public void mo23209a(float f, float f2) {
        this.f6931c = f;
        this.f6932d = f2;
    }
}
