package p119e.p128c.p129a.p130a.p135e.p137b;

import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;

/* renamed from: e.c.a.a.e.b.i */
public interface IPieDataSet extends IDataSet<PieEntry> {
    /* renamed from: A0 */
    PieDataSet.C1659a mo13421A0();

    /* renamed from: B0 */
    PieDataSet.C1659a mo13422B0();

    /* renamed from: C0 */
    boolean mo13423C0();

    /* renamed from: D0 */
    float mo13424D0();

    /* renamed from: E0 */
    boolean mo13425E0();

    /* renamed from: F0 */
    float mo13426F0();

    /* renamed from: G0 */
    float mo13427G0();

    /* renamed from: v0 */
    float mo13432v0();

    /* renamed from: w0 */
    boolean mo13433w0();

    /* renamed from: x0 */
    int mo13434x0();

    /* renamed from: y0 */
    float mo13435y0();

    /* renamed from: z0 */
    float mo13436z0();
}
