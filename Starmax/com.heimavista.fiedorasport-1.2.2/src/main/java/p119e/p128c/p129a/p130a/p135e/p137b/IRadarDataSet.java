package p119e.p128c.p129a.p130a.p135e.p137b;

import com.github.mikephil.charting.data.RadarEntry;

/* renamed from: e.c.a.a.e.b.j */
public interface IRadarDataSet extends ILineRadarDataSet<RadarEntry> {
    /* renamed from: H */
    float mo23291H();

    /* renamed from: J */
    int mo23292J();

    /* renamed from: K */
    float mo23293K();

    /* renamed from: L */
    int mo23294L();

    /* renamed from: N */
    int mo23295N();

    /* renamed from: O */
    boolean mo23296O();

    /* renamed from: R */
    float mo23297R();
}
