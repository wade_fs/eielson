package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.ChartInterface;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.g */
public abstract class DataRenderer extends Renderer {

    /* renamed from: b */
    protected ChartAnimator f7035b;

    /* renamed from: c */
    protected Paint f7036c = new Paint(1);

    /* renamed from: d */
    protected Paint f7037d;

    /* renamed from: e */
    protected Paint f7038e;

    public DataRenderer(ChartAnimator aVar, ViewPortHandler jVar) {
        super(jVar);
        this.f7035b = aVar;
        this.f7036c.setStyle(Paint.Style.FILL);
        new Paint(4);
        this.f7038e = new Paint(1);
        this.f7038e.setColor(Color.rgb(63, 63, 63));
        this.f7038e.setTextAlign(Paint.Align.CENTER);
        this.f7038e.setTextSize(Utils.m11599a(9.0f));
        this.f7037d = new Paint(1);
        this.f7037d.setStyle(Paint.Style.STROKE);
        this.f7037d.setStrokeWidth(2.0f);
        this.f7037d.setColor(Color.rgb(255, 187, 115));
    }

    /* renamed from: a */
    public abstract void mo23337a();

    /* renamed from: a */
    public abstract void mo23339a(Canvas canvas);

    /* renamed from: a */
    public abstract void mo23342a(Canvas canvas, Highlight[] dVarArr);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo23356a(ChartInterface eVar) {
        return ((float) eVar.getData().mo13387d()) < ((float) eVar.getMaxVisibleCount()) * super.f7088a.mo23478p();
    }

    /* renamed from: b */
    public abstract void mo23344b(Canvas canvas);

    /* renamed from: c */
    public abstract void mo23345c(Canvas canvas);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23355a(IDataSet eVar) {
        this.f7038e.setTypeface(eVar.mo13359m());
        this.f7038e.setTextSize(eVar.mo13354i());
    }
}
