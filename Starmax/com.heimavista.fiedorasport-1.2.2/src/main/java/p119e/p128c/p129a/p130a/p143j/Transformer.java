package p119e.p128c.p129a.p130a.p143j;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import p119e.p128c.p129a.p130a.p135e.p137b.IBubbleDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.ICandleDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.ILineDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.IScatterDataSet;

/* renamed from: e.c.a.a.j.g */
public class Transformer {

    /* renamed from: a */
    protected Matrix f7134a = new Matrix();

    /* renamed from: b */
    protected Matrix f7135b = new Matrix();

    /* renamed from: c */
    protected ViewPortHandler f7136c;

    /* renamed from: d */
    protected float[] f7137d = new float[1];

    /* renamed from: e */
    protected float[] f7138e = new float[1];

    /* renamed from: f */
    protected float[] f7139f = new float[1];

    /* renamed from: g */
    protected float[] f7140g = new float[1];

    /* renamed from: h */
    protected Matrix f7141h = new Matrix();

    /* renamed from: i */
    float[] f7142i = new float[2];

    /* renamed from: j */
    private Matrix f7143j = new Matrix();

    public Transformer(ViewPortHandler jVar) {
        new Matrix();
        this.f7136c = jVar;
    }

    /* renamed from: a */
    public void mo23429a(float f, float f2, float f3, float f4) {
        float j = this.f7136c.mo23469j() / f2;
        float f5 = this.f7136c.mo23461f() / f3;
        if (Float.isInfinite(j)) {
            j = 0.0f;
        }
        if (Float.isInfinite(f5)) {
            f5 = 0.0f;
        }
        this.f7134a.reset();
        this.f7134a.postTranslate(-f, -f4);
        this.f7134a.postScale(j, -f5);
    }

    /* renamed from: b */
    public void mo23442b(float[] fArr) {
        this.f7134a.mapPoints(fArr);
        this.f7136c.mo23477o().mapPoints(fArr);
        this.f7135b.mapPoints(fArr);
    }

    /* renamed from: b */
    public void mo23441b(RectF rectF, float f) {
        rectF.left *= f;
        rectF.right *= f;
        this.f7134a.mapRect(rectF);
        this.f7136c.mo23477o().mapRect(rectF);
        this.f7135b.mapRect(rectF);
    }

    /* renamed from: a */
    public void mo23434a(boolean z) {
        this.f7135b.reset();
        if (!z) {
            this.f7135b.postTranslate(this.f7136c.mo23487y(), this.f7136c.mo23471k() - this.f7136c.mo23486x());
            return;
        }
        this.f7135b.setTranslate(this.f7136c.mo23487y(), -this.f7136c.mo23443A());
        this.f7135b.postScale(1.0f, -1.0f);
    }

    /* renamed from: b */
    public MPPointD mo23440b(float f, float f2) {
        MPPointD a = MPPointD.m11564a(0.0d, 0.0d);
        mo23430a(f, f2, a);
        return a;
    }

    /* renamed from: a */
    public float[] mo23439a(IScatterDataSet kVar, float f, float f2, int i, int i2) {
        int i3 = ((int) ((((float) (i2 - i)) * f) + 1.0f)) * 2;
        if (this.f7137d.length != i3) {
            this.f7137d = new float[i3];
        }
        float[] fArr = this.f7137d;
        for (int i4 = 0; i4 < i3; i4 += 2) {
            Entry b = kVar.mo13410b((i4 / 2) + i);
            if (b != null) {
                fArr[i4] = b.mo13315d();
                fArr[i4 + 1] = b.mo13303c() * f2;
            } else {
                fArr[i4] = 0.0f;
                fArr[i4 + 1] = 0.0f;
            }
        }
        mo23427a().mapPoints(fArr);
        return fArr;
    }

    /* renamed from: a */
    public float[] mo23436a(IBubbleDataSet cVar, float f, int i, int i2) {
        int i3 = ((i2 - i) + 1) * 2;
        if (this.f7138e.length != i3) {
            this.f7138e = new float[i3];
        }
        float[] fArr = this.f7138e;
        for (int i4 = 0; i4 < i3; i4 += 2) {
            Entry b = cVar.mo13410b((i4 / 2) + i);
            if (b != null) {
                fArr[i4] = b.mo13315d();
                fArr[i4 + 1] = b.mo13303c() * f;
            } else {
                fArr[i4] = 0.0f;
                fArr[i4 + 1] = 0.0f;
            }
        }
        mo23427a().mapPoints(fArr);
        return fArr;
    }

    /* renamed from: a */
    public float[] mo23438a(ILineDataSet fVar, float f, float f2, int i, int i2) {
        int i3 = (((int) (((float) (i2 - i)) * f)) + 1) * 2;
        if (this.f7139f.length != i3) {
            this.f7139f = new float[i3];
        }
        float[] fArr = this.f7139f;
        for (int i4 = 0; i4 < i3; i4 += 2) {
            Entry b = fVar.mo13410b((i4 / 2) + i);
            if (b != null) {
                fArr[i4] = b.mo13315d();
                fArr[i4 + 1] = b.mo13303c() * f2;
            } else {
                fArr[i4] = 0.0f;
                fArr[i4 + 1] = 0.0f;
            }
        }
        mo23427a().mapPoints(fArr);
        return fArr;
    }

    /* renamed from: a */
    public float[] mo23437a(ICandleDataSet dVar, float f, float f2, int i, int i2) {
        int i3 = ((int) ((((float) (i2 - i)) * f) + 1.0f)) * 2;
        if (this.f7140g.length != i3) {
            this.f7140g = new float[i3];
        }
        float[] fArr = this.f7140g;
        for (int i4 = 0; i4 < i3; i4 += 2) {
            CandleEntry candleEntry = (CandleEntry) dVar.mo13410b((i4 / 2) + i);
            if (candleEntry != null) {
                fArr[i4] = candleEntry.mo13315d();
                fArr[i4 + 1] = candleEntry.mo13312f() * f2;
            } else {
                fArr[i4] = 0.0f;
                fArr[i4 + 1] = 0.0f;
            }
        }
        mo23427a().mapPoints(fArr);
        return fArr;
    }

    /* renamed from: a */
    public void mo23431a(Path path) {
        path.transform(this.f7134a);
        path.transform(this.f7136c.mo23477o());
        path.transform(this.f7135b);
    }

    /* renamed from: a */
    public void mo23432a(RectF rectF) {
        this.f7134a.mapRect(rectF);
        this.f7136c.mo23477o().mapRect(rectF);
        this.f7135b.mapRect(rectF);
    }

    /* renamed from: a */
    public void mo23433a(RectF rectF, float f) {
        rectF.top *= f;
        rectF.bottom *= f;
        this.f7134a.mapRect(rectF);
        this.f7136c.mo23477o().mapRect(rectF);
        this.f7135b.mapRect(rectF);
    }

    /* renamed from: a */
    public void mo23435a(float[] fArr) {
        Matrix matrix = this.f7141h;
        matrix.reset();
        this.f7135b.invert(matrix);
        matrix.mapPoints(fArr);
        this.f7136c.mo23477o().invert(matrix);
        matrix.mapPoints(fArr);
        this.f7134a.invert(matrix);
        matrix.mapPoints(fArr);
    }

    /* renamed from: a */
    public void mo23430a(float f, float f2, MPPointD dVar) {
        float[] fArr = this.f7142i;
        fArr[0] = f;
        fArr[1] = f2;
        mo23435a(fArr);
        float[] fArr2 = this.f7142i;
        dVar.f7120R = (double) fArr2[0];
        dVar.f7121S = (double) fArr2[1];
    }

    /* renamed from: a */
    public MPPointD mo23428a(float f, float f2) {
        float[] fArr = this.f7142i;
        fArr[0] = f;
        fArr[1] = f2;
        mo23442b(fArr);
        float[] fArr2 = this.f7142i;
        return MPPointD.m11564a((double) fArr2[0], (double) fArr2[1]);
    }

    /* renamed from: a */
    public Matrix mo23427a() {
        this.f7143j.set(this.f7134a);
        this.f7143j.postConcat(this.f7136c.f7155a);
        this.f7143j.postConcat(this.f7135b);
        return this.f7143j;
    }
}
