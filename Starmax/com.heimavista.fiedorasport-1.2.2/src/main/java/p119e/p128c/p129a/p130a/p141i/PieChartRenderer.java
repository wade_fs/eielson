package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import androidx.core.view.ViewCompat;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import java.lang.ref.WeakReference;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.IPieDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.m */
public class PieChartRenderer extends DataRenderer {

    /* renamed from: f */
    protected PieChart f7066f;

    /* renamed from: g */
    protected Paint f7067g;

    /* renamed from: h */
    protected Paint f7068h;

    /* renamed from: i */
    protected Paint f7069i;

    /* renamed from: j */
    private TextPaint f7070j;

    /* renamed from: k */
    private Paint f7071k;

    /* renamed from: l */
    private StaticLayout f7072l;

    /* renamed from: m */
    private CharSequence f7073m;

    /* renamed from: n */
    private RectF f7074n = new RectF();

    /* renamed from: o */
    private RectF[] f7075o = {new RectF(), new RectF(), new RectF()};

    /* renamed from: p */
    protected WeakReference<Bitmap> f7076p;

    /* renamed from: q */
    protected Canvas f7077q;

    /* renamed from: r */
    private Path f7078r = new Path();

    /* renamed from: s */
    private RectF f7079s = new RectF();

    /* renamed from: t */
    private Path f7080t = new Path();

    /* renamed from: u */
    protected Path f7081u = new Path();

    /* renamed from: v */
    protected RectF f7082v = new RectF();

    public PieChartRenderer(PieChart pieChart, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7066f = pieChart;
        this.f7067g = new Paint(1);
        this.f7067g.setColor(-1);
        this.f7067g.setStyle(Paint.Style.FILL);
        this.f7068h = new Paint(1);
        this.f7068h.setColor(-1);
        this.f7068h.setStyle(Paint.Style.FILL);
        this.f7068h.setAlpha(105);
        this.f7070j = new TextPaint(1);
        this.f7070j.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        this.f7070j.setTextSize(Utils.m11599a(12.0f));
        super.f7038e.setTextSize(Utils.m11599a(13.0f));
        super.f7038e.setColor(-1);
        super.f7038e.setTextAlign(Paint.Align.CENTER);
        this.f7071k = new Paint(1);
        this.f7071k.setColor(-1);
        this.f7071k.setTextAlign(Paint.Align.CENTER);
        this.f7071k.setTextSize(Utils.m11599a(13.0f));
        this.f7069i = new Paint(1);
        this.f7069i.setStyle(Paint.Style.STROKE);
    }

    /* renamed from: a */
    public void mo23337a() {
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        int l = (int) this.f7088a.mo23473l();
        int k = (int) this.f7088a.mo23471k();
        WeakReference<Bitmap> weakReference = this.f7076p;
        Bitmap bitmap = weakReference == null ? null : weakReference.get();
        if (!(bitmap != null && bitmap.getWidth() == l && bitmap.getHeight() == k)) {
            if (l > 0 && k > 0) {
                bitmap = Bitmap.createBitmap(l, k, Bitmap.Config.ARGB_4444);
                this.f7076p = new WeakReference<>(bitmap);
                this.f7077q = new Canvas(bitmap);
            } else {
                return;
            }
        }
        bitmap.eraseColor(0);
        for (IPieDataSet iVar : ((PieData) this.f7066f.getData()).mo13386c()) {
            if (iVar.isVisible() && iVar.mo13417t() > 0) {
                mo23379a(canvas, iVar);
            }
        }
    }

    /* renamed from: b */
    public TextPaint mo23382b() {
        return this.f7070j;
    }

    /* renamed from: c */
    public Paint mo23383c() {
        return this.f7071k;
    }

    /* renamed from: d */
    public Paint mo23384d() {
        return this.f7067g;
    }

    /* renamed from: e */
    public Paint mo23386e() {
        return this.f7068h;
    }

    /* renamed from: f */
    public void mo23388f() {
        Canvas canvas = this.f7077q;
        if (canvas != null) {
            canvas.setBitmap(null);
            this.f7077q = null;
        }
        WeakReference<Bitmap> weakReference = this.f7076p;
        if (weakReference != null) {
            Bitmap bitmap = weakReference.get();
            if (bitmap != null) {
                bitmap.recycle();
            }
            this.f7076p.clear();
            this.f7076p = null;
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
        mo23387e(canvas);
        canvas.drawBitmap(this.f7076p.get(), 0.0f, 0.0f, (Paint) null);
        mo23385d(canvas);
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        float f;
        float f2;
        float f3;
        float[] fArr;
        float[] fArr2;
        List list;
        float f4;
        int i;
        MPPointF eVar;
        Canvas canvas2;
        float f5;
        float f6;
        MPPointF eVar2;
        MPPointF eVar3;
        ValueFormatter gVar;
        PieDataSet.C1659a aVar;
        float f7;
        List list2;
        float f8;
        PieEntry pieEntry;
        String str;
        IPieDataSet iVar;
        Canvas canvas3;
        MPPointF eVar4;
        MPPointF eVar5;
        float f9;
        float f10;
        float f11;
        float f12;
        Canvas canvas4;
        String str2;
        Canvas canvas5 = canvas;
        MPPointF centerCircleBox = this.f7066f.getCenterCircleBox();
        float radius = this.f7066f.getRadius();
        float rotationAngle = this.f7066f.getRotationAngle();
        float[] drawAngles = this.f7066f.getDrawAngles();
        float[] absoluteAngles = this.f7066f.getAbsoluteAngles();
        float a = super.f7035b.mo23201a();
        float b = super.f7035b.mo23204b();
        float holeRadius = (radius - ((this.f7066f.getHoleRadius() * radius) / 100.0f)) / 2.0f;
        float holeRadius2 = this.f7066f.getHoleRadius() / 100.0f;
        float f13 = (radius / 10.0f) * 3.6f;
        if (this.f7066f.mo13132s()) {
            f13 = (radius - (radius * holeRadius2)) / 2.0f;
            if (!this.f7066f.mo13157u() && this.f7066f.mo13156t()) {
                rotationAngle = (float) (((double) rotationAngle) + (((double) (holeRadius * 360.0f)) / (((double) radius) * 6.283185307179586d)));
            }
        }
        float f14 = rotationAngle;
        float f15 = radius - f13;
        PieData nVar = (PieData) this.f7066f.getData();
        List c = nVar.mo13386c();
        float l = nVar.mo13420l();
        boolean r = this.f7066f.mo13131r();
        canvas.save();
        float a2 = Utils.m11599a(5.0f);
        int i2 = 0;
        int i3 = 0;
        while (i3 < c.size()) {
            IPieDataSet iVar2 = (IPieDataSet) c.get(i3);
            boolean r2 = iVar2.mo13363r();
            if (r2 || r) {
                PieDataSet.C1659a A0 = iVar2.mo13421A0();
                PieDataSet.C1659a B0 = iVar2.mo13422B0();
                mo23355a((IDataSet) iVar2);
                int i4 = i2;
                i = i3;
                float a3 = ((float) Utils.m11602a(super.f7038e, "Q")) + Utils.m11599a(4.0f);
                ValueFormatter j = iVar2.mo13356j();
                int t = iVar2.mo13417t();
                List list3 = c;
                this.f7069i.setColor(iVar2.mo13434x0());
                this.f7069i.setStrokeWidth(Utils.m11599a(iVar2.mo13435y0()));
                float a4 = mo23377a(iVar2);
                MPPointF a5 = MPPointF.m11568a(iVar2.mo13365u());
                MPPointF eVar6 = centerCircleBox;
                a5.f7123R = Utils.m11599a(a5.f7123R);
                a5.f7124S = Utils.m11599a(a5.f7124S);
                int i5 = 0;
                while (i5 < t) {
                    MPPointF eVar7 = a5;
                    PieEntry pieEntry2 = (PieEntry) iVar2.mo13410b(i5);
                    if (i4 == 0) {
                        f5 = 0.0f;
                    } else {
                        f5 = absoluteAngles[i4 - 1] * a;
                    }
                    int i6 = t;
                    float f16 = f14 + ((f5 + ((drawAngles[i4] - ((a4 / (f15 * 0.017453292f)) / 2.0f)) / 2.0f)) * b);
                    float f17 = a4;
                    if (this.f7066f.mo13158v()) {
                        f6 = (pieEntry2.mo13303c() / l) * 100.0f;
                    } else {
                        f6 = pieEntry2.mo13303c();
                    }
                    String a6 = j.mo23220a(f6, pieEntry2);
                    float[] fArr3 = drawAngles;
                    String e = pieEntry2.mo13321e();
                    ValueFormatter gVar2 = j;
                    PieEntry pieEntry3 = pieEntry2;
                    double d = (double) (f16 * 0.017453292f);
                    float[] fArr4 = absoluteAngles;
                    float f18 = a;
                    float cos = (float) Math.cos(d);
                    float f19 = b;
                    float sin = (float) Math.sin(d);
                    boolean z = r && A0 == PieDataSet.C1659a.OUTSIDE_SLICE;
                    float f20 = f14;
                    boolean z2 = r2 && B0 == PieDataSet.C1659a.OUTSIDE_SLICE;
                    String str3 = e;
                    boolean z3 = r && A0 == PieDataSet.C1659a.INSIDE_SLICE;
                    PieDataSet.C1659a aVar2 = A0;
                    boolean z4 = r2 && B0 == PieDataSet.C1659a.INSIDE_SLICE;
                    if (z || z2) {
                        float z0 = iVar2.mo13436z0();
                        float D0 = iVar2.mo13424D0();
                        float G0 = iVar2.mo13427G0() / 100.0f;
                        aVar = B0;
                        if (this.f7066f.mo13132s()) {
                            float f21 = radius * holeRadius2;
                            f9 = ((radius - f21) * G0) + f21;
                        } else {
                            f9 = radius * G0;
                        }
                        float abs = iVar2.mo13423C0() ? D0 * f15 * ((float) Math.abs(Math.sin(d))) : D0 * f15;
                        MPPointF eVar8 = eVar6;
                        float f22 = eVar8.f7123R;
                        float f23 = (f9 * cos) + f22;
                        f8 = radius;
                        float f24 = eVar8.f7124S;
                        float f25 = (f9 * sin) + f24;
                        float f26 = (z0 + 1.0f) * f15;
                        float f27 = (f26 * cos) + f22;
                        float f28 = f24 + (f26 * sin);
                        MPPointF eVar9 = eVar8;
                        double d2 = ((double) f16) % 360.0d;
                        if (d2 < 90.0d || d2 > 270.0d) {
                            f10 = f27 + abs;
                            super.f7038e.setTextAlign(Paint.Align.LEFT);
                            if (z) {
                                this.f7071k.setTextAlign(Paint.Align.LEFT);
                            }
                            f11 = f10 + a2;
                        } else {
                            float f29 = f27 - abs;
                            super.f7038e.setTextAlign(Paint.Align.RIGHT);
                            if (z) {
                                this.f7071k.setTextAlign(Paint.Align.RIGHT);
                            }
                            f10 = f29;
                            f11 = f29 - a2;
                        }
                        if (iVar2.mo13434x0() != 1122867) {
                            if (iVar2.mo13425E0()) {
                                this.f7069i.setColor(iVar2.mo13345c(i5));
                            }
                            Canvas canvas6 = canvas;
                            float f30 = f25;
                            f7 = sin;
                            iVar = iVar2;
                            gVar = gVar2;
                            float f31 = f28;
                            eVar2 = eVar7;
                            eVar3 = eVar9;
                            f12 = f11;
                            PieEntry pieEntry4 = pieEntry3;
                            list2 = list3;
                            pieEntry = pieEntry4;
                            canvas6.drawLine(f23, f30, f27, f31, this.f7069i);
                            canvas6.drawLine(f27, f28, f10, f31, this.f7069i);
                        } else {
                            f7 = sin;
                            eVar2 = eVar7;
                            gVar = gVar2;
                            eVar3 = eVar9;
                            iVar = iVar2;
                            f12 = f11;
                            PieEntry pieEntry5 = pieEntry3;
                            list2 = list3;
                            pieEntry = pieEntry5;
                        }
                        if (!z || !z2) {
                            canvas4 = canvas;
                            float f32 = f12;
                            str2 = str3;
                            if (z) {
                                if (i5 < nVar.mo13387d() && str2 != null) {
                                    mo23380a(canvas4, str2, f32, f28 + (a3 / 2.0f));
                                }
                            } else if (z2) {
                                str = str2;
                                float f33 = f28 + (a3 / 2.0f);
                                canvas3 = canvas4;
                                mo23381a(canvas, a6, f32, f33, iVar.mo13348d(i5));
                            }
                        } else {
                            mo23381a(canvas, a6, f12, f28, iVar.mo13348d(i5));
                            if (i5 >= nVar.mo13387d() || str3 == null) {
                                canvas3 = canvas;
                                str = str3;
                            } else {
                                canvas4 = canvas;
                                str2 = str3;
                                mo23380a(canvas4, str2, f12, f28 + a3);
                            }
                        }
                        str = str2;
                        canvas3 = canvas4;
                    } else {
                        aVar = B0;
                        f7 = sin;
                        eVar3 = eVar6;
                        eVar2 = eVar7;
                        gVar = gVar2;
                        str = str3;
                        iVar = iVar2;
                        f8 = radius;
                        canvas3 = canvas;
                        PieEntry pieEntry6 = pieEntry3;
                        list2 = list3;
                        pieEntry = pieEntry6;
                    }
                    if (z3 || z4) {
                        eVar4 = eVar3;
                        float f34 = (f15 * cos) + eVar4.f7123R;
                        float f35 = (f15 * f7) + eVar4.f7124S;
                        super.f7038e.setTextAlign(Paint.Align.CENTER);
                        if (!z3 || !z4) {
                            float f36 = f34;
                            if (z3) {
                                if (i5 < nVar.mo13387d() && str != null) {
                                    mo23380a(canvas3, str, f36, f35 + (a3 / 2.0f));
                                }
                            } else if (z4) {
                                mo23381a(canvas, a6, f36, f35 + (a3 / 2.0f), iVar.mo13348d(i5));
                            }
                            if (pieEntry.mo13371b() != null || !iVar.mo13349d()) {
                                eVar5 = eVar2;
                            } else {
                                Drawable b2 = pieEntry.mo13371b();
                                eVar5 = eVar2;
                                float f37 = eVar5.f7124S;
                                Utils.m11606a(canvas, b2, (int) (((f15 + f37) * cos) + eVar4.f7123R), (int) (((f37 + f15) * f7) + eVar4.f7124S + eVar5.f7123R), b2.getIntrinsicWidth(), b2.getIntrinsicHeight());
                            }
                            i4++;
                            i5++;
                            a5 = eVar5;
                            iVar2 = iVar;
                            radius = f8;
                            a4 = f17;
                            t = i6;
                            list3 = list2;
                            drawAngles = fArr3;
                            absoluteAngles = fArr4;
                            a = f18;
                            f14 = f20;
                            A0 = aVar2;
                            B0 = aVar;
                            j = gVar;
                            eVar6 = eVar4;
                            b = f19;
                        } else {
                            float f38 = f34;
                            mo23381a(canvas, a6, f34, f35, iVar.mo13348d(i5));
                            if (i5 < nVar.mo13387d() && str != null) {
                                mo23380a(canvas3, str, f38, f35 + a3);
                            }
                        }
                    } else {
                        eVar4 = eVar3;
                    }
                    if (pieEntry.mo13371b() != null) {
                    }
                    eVar5 = eVar2;
                    i4++;
                    i5++;
                    a5 = eVar5;
                    iVar2 = iVar;
                    radius = f8;
                    a4 = f17;
                    t = i6;
                    list3 = list2;
                    drawAngles = fArr3;
                    absoluteAngles = fArr4;
                    a = f18;
                    f14 = f20;
                    A0 = aVar2;
                    B0 = aVar;
                    j = gVar;
                    eVar6 = eVar4;
                    b = f19;
                }
                fArr2 = drawAngles;
                fArr = absoluteAngles;
                f3 = a;
                f2 = b;
                f = f14;
                list = list3;
                eVar = eVar6;
                f4 = radius;
                canvas2 = canvas;
                MPPointF.m11570b(a5);
                i2 = i4;
            } else {
                i = i3;
                list = c;
                f4 = radius;
                fArr2 = drawAngles;
                fArr = absoluteAngles;
                f3 = a;
                f2 = b;
                f = f14;
                canvas2 = canvas5;
                eVar = centerCircleBox;
            }
            i3 = i + 1;
            canvas5 = canvas2;
            centerCircleBox = eVar;
            radius = f4;
            c = list;
            drawAngles = fArr2;
            absoluteAngles = fArr;
            a = f3;
            b = f2;
            f14 = f;
        }
        MPPointF.m11570b(centerCircleBox);
        canvas.restore();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo23385d(Canvas canvas) {
        float f;
        MPPointF eVar;
        Canvas canvas2 = canvas;
        CharSequence centerText = this.f7066f.getCenterText();
        if (this.f7066f.mo13130q() && centerText != null) {
            MPPointF centerCircleBox = this.f7066f.getCenterCircleBox();
            MPPointF centerTextOffset = this.f7066f.getCenterTextOffset();
            float f2 = centerCircleBox.f7123R + centerTextOffset.f7123R;
            float f3 = centerCircleBox.f7124S + centerTextOffset.f7124S;
            if (!this.f7066f.mo13132s() || this.f7066f.mo13157u()) {
                f = this.f7066f.getRadius();
            } else {
                f = this.f7066f.getRadius() * (this.f7066f.getHoleRadius() / 100.0f);
            }
            RectF[] rectFArr = this.f7075o;
            RectF rectF = rectFArr[0];
            rectF.left = f2 - f;
            rectF.top = f3 - f;
            rectF.right = f2 + f;
            rectF.bottom = f3 + f;
            RectF rectF2 = rectFArr[1];
            rectF2.set(rectF);
            float centerTextRadiusPercent = this.f7066f.getCenterTextRadiusPercent() / 100.0f;
            if (((double) centerTextRadiusPercent) > 0.0d) {
                rectF2.inset((rectF2.width() - (rectF2.width() * centerTextRadiusPercent)) / 2.0f, (rectF2.height() - (rectF2.height() * centerTextRadiusPercent)) / 2.0f);
            }
            if (!centerText.equals(this.f7073m) || !rectF2.equals(this.f7074n)) {
                this.f7074n.set(rectF2);
                this.f7073m = centerText;
                eVar = centerTextOffset;
                StaticLayout staticLayout = r3;
                StaticLayout staticLayout2 = new StaticLayout(centerText, 0, centerText.length(), this.f7070j, (int) Math.max(Math.ceil((double) this.f7074n.width()), 1.0d), Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);
                this.f7072l = staticLayout;
            } else {
                eVar = centerTextOffset;
            }
            float height = (float) this.f7072l.getHeight();
            canvas.save();
            if (Build.VERSION.SDK_INT >= 18) {
                Path path = this.f7081u;
                path.reset();
                path.addOval(rectF, Path.Direction.CW);
                canvas2.clipPath(path);
            }
            canvas2.translate(rectF2.left, rectF2.top + ((rectF2.height() - height) / 2.0f));
            this.f7072l.draw(canvas2);
            canvas.restore();
            MPPointF.m11570b(centerCircleBox);
            MPPointF.m11570b(eVar);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo23387e(Canvas canvas) {
        if (this.f7066f.mo13132s() && this.f7077q != null) {
            float radius = this.f7066f.getRadius();
            float holeRadius = (this.f7066f.getHoleRadius() / 100.0f) * radius;
            MPPointF centerCircleBox = this.f7066f.getCenterCircleBox();
            if (Color.alpha(this.f7067g.getColor()) > 0) {
                this.f7077q.drawCircle(centerCircleBox.f7123R, centerCircleBox.f7124S, holeRadius, this.f7067g);
            }
            if (Color.alpha(this.f7068h.getColor()) > 0 && this.f7066f.getTransparentCircleRadius() > this.f7066f.getHoleRadius()) {
                int alpha = this.f7068h.getAlpha();
                float transparentCircleRadius = radius * (this.f7066f.getTransparentCircleRadius() / 100.0f);
                this.f7068h.setAlpha((int) (((float) alpha) * super.f7035b.mo23201a() * super.f7035b.mo23204b()));
                this.f7080t.reset();
                this.f7080t.addCircle(centerCircleBox.f7123R, centerCircleBox.f7124S, transparentCircleRadius, Path.Direction.CW);
                this.f7080t.addCircle(centerCircleBox.f7123R, centerCircleBox.f7124S, holeRadius, Path.Direction.CCW);
                this.f7077q.drawPath(this.f7080t, this.f7068h);
                this.f7068h.setAlpha(alpha);
            }
            MPPointF.m11570b(centerCircleBox);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23378a(MPPointF eVar, float f, float f2, float f3, float f4, float f5, float f6) {
        MPPointF eVar2 = eVar;
        double d = (double) ((f5 + f6) * 0.017453292f);
        float cos = eVar2.f7123R + (((float) Math.cos(d)) * f);
        float sin = eVar2.f7124S + (((float) Math.sin(d)) * f);
        double d2 = (double) ((f5 + (f6 / 2.0f)) * 0.017453292f);
        return (float) (((double) (f - ((float) ((Math.sqrt(Math.pow((double) (cos - f3), 2.0d) + Math.pow((double) (sin - f4), 2.0d)) / 2.0d) * Math.tan(((180.0d - ((double) f2)) / 2.0d) * 0.017453292519943295d))))) - Math.sqrt(Math.pow((double) ((eVar2.f7123R + (((float) Math.cos(d2)) * f)) - ((cos + f3) / 2.0f)), 2.0d) + Math.pow((double) ((eVar2.f7124S + (((float) Math.sin(d2)) * f)) - ((sin + f4) / 2.0f)), 2.0d)));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23377a(IPieDataSet iVar) {
        if (!iVar.mo13433w0()) {
            return iVar.mo13432v0();
        }
        if (iVar.mo13432v0() / this.f7088a.mo23480r() > (iVar.mo13415g() / ((PieData) this.f7066f.getData()).mo13420l()) * 2.0f) {
            return 0.0f;
        }
        return iVar.mo13432v0();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23379a(Canvas canvas, IPieDataSet iVar) {
        float f;
        RectF rectF;
        float f2;
        float[] fArr;
        int i;
        int i2;
        float f3;
        float f4;
        MPPointF eVar;
        int i3;
        float f5;
        RectF rectF2;
        int i4;
        RectF rectF3;
        float f6;
        float f7;
        float f8;
        float f9;
        RectF rectF4;
        int i5;
        RectF rectF5;
        RectF rectF6;
        float f10;
        MPPointF eVar2;
        int i6;
        PieChartRenderer mVar = this;
        IPieDataSet iVar2 = iVar;
        float rotationAngle = mVar.f7066f.getRotationAngle();
        float a = super.f7035b.mo23201a();
        float b = super.f7035b.mo23204b();
        RectF circleBox = mVar.f7066f.getCircleBox();
        int t = iVar.mo13417t();
        float[] drawAngles = mVar.f7066f.getDrawAngles();
        MPPointF centerCircleBox = mVar.f7066f.getCenterCircleBox();
        float radius = mVar.f7066f.getRadius();
        boolean z = mVar.f7066f.mo13132s() && !mVar.f7066f.mo13157u();
        float holeRadius = z ? (mVar.f7066f.getHoleRadius() / 100.0f) * radius : 0.0f;
        float holeRadius2 = (radius - ((mVar.f7066f.getHoleRadius() * radius) / 100.0f)) / 2.0f;
        RectF rectF7 = new RectF();
        boolean z2 = z && mVar.f7066f.mo13156t();
        int i7 = 0;
        for (int i8 = 0; i8 < t; i8++) {
            if (Math.abs(((PieEntry) iVar2.mo13410b(i8)).mo13303c()) > Utils.f7147d) {
                i7++;
            }
        }
        if (i7 <= 1) {
            f = 0.0f;
        } else {
            f = mVar.mo23377a(iVar2);
        }
        int i9 = 0;
        float f11 = 0.0f;
        while (i9 < t) {
            float f12 = drawAngles[i9];
            if (Math.abs(iVar2.mo13410b(i9).mo13303c()) > Utils.f7147d && (!mVar.f7066f.mo13114d(i9) || z2)) {
                boolean z3 = f > 0.0f && f12 <= 180.0f;
                super.f7036c.setColor(iVar2.mo13345c(i9));
                float f13 = i7 == 1 ? 0.0f : f / (radius * 0.017453292f);
                float f14 = rotationAngle + ((f11 + (f13 / 2.0f)) * b);
                float f15 = (f12 - f13) * b;
                if (f15 < 0.0f) {
                    f15 = 0.0f;
                }
                mVar.f7078r.reset();
                if (z2) {
                    float f16 = radius - holeRadius2;
                    i2 = i9;
                    i4 = i7;
                    double d = (double) (f14 * 0.017453292f);
                    i = t;
                    fArr = drawAngles;
                    float cos = centerCircleBox.f7123R + (((float) Math.cos(d)) * f16);
                    float sin = centerCircleBox.f7124S + (f16 * ((float) Math.sin(d)));
                    rectF7.set(cos - holeRadius2, sin - holeRadius2, cos + holeRadius2, sin + holeRadius2);
                } else {
                    i2 = i9;
                    i4 = i7;
                    i = t;
                    fArr = drawAngles;
                }
                double d2 = (double) (f14 * 0.017453292f);
                f3 = rotationAngle;
                f2 = a;
                float cos2 = centerCircleBox.f7123R + (((float) Math.cos(d2)) * radius);
                float sin2 = centerCircleBox.f7124S + (((float) Math.sin(d2)) * radius);
                int i10 = (f15 > 360.0f ? 1 : (f15 == 360.0f ? 0 : -1));
                if (i10 < 0 || f15 % 360.0f > Utils.f7147d) {
                    if (z2) {
                        mVar.f7078r.arcTo(rectF7, f14 + 180.0f, -180.0f);
                    }
                    mVar.f7078r.arcTo(circleBox, f14, f15);
                } else {
                    mVar.f7078r.addCircle(centerCircleBox.f7123R, centerCircleBox.f7124S, radius, Path.Direction.CW);
                }
                RectF rectF8 = mVar.f7079s;
                float f17 = centerCircleBox.f7123R;
                float f18 = centerCircleBox.f7124S;
                float f19 = f15;
                rectF8.set(f17 - holeRadius, f18 - holeRadius, f17 + holeRadius, f18 + holeRadius);
                if (!z) {
                    f8 = holeRadius;
                    f9 = radius;
                    eVar = centerCircleBox;
                    rectF4 = circleBox;
                    i5 = i4;
                    f6 = f19;
                    rectF5 = rectF7;
                    f7 = 360.0f;
                } else if (holeRadius > 0.0f || z3) {
                    if (z3) {
                        f10 = f19;
                        rectF = circleBox;
                        i3 = i4;
                        rectF6 = rectF7;
                        f5 = holeRadius;
                        i6 = 1;
                        f4 = radius;
                        float f20 = f14;
                        eVar2 = centerCircleBox;
                        float a2 = mo23378a(centerCircleBox, radius, f12 * b, cos2, sin2, f20, f10);
                        if (a2 < 0.0f) {
                            a2 = -a2;
                        }
                        holeRadius = Math.max(f5, a2);
                    } else {
                        rectF6 = rectF7;
                        f5 = holeRadius;
                        f4 = radius;
                        eVar2 = centerCircleBox;
                        rectF = circleBox;
                        i3 = i4;
                        f10 = f19;
                        i6 = 1;
                    }
                    float f21 = (i3 == i6 || holeRadius == 0.0f) ? 0.0f : f / (holeRadius * 0.017453292f);
                    float f22 = f3 + ((f11 + (f21 / 2.0f)) * b);
                    float f23 = (f12 - f21) * b;
                    if (f23 < 0.0f) {
                        f23 = 0.0f;
                    }
                    float f24 = f22 + f23;
                    if (i10 < 0 || f10 % 360.0f > Utils.f7147d) {
                        mVar = this;
                        if (z2) {
                            float f25 = f4 - holeRadius2;
                            double d3 = (double) (f24 * 0.017453292f);
                            float cos3 = eVar2.f7123R + (((float) Math.cos(d3)) * f25);
                            float sin3 = eVar2.f7124S + (f25 * ((float) Math.sin(d3)));
                            rectF3 = rectF6;
                            rectF3.set(cos3 - holeRadius2, sin3 - holeRadius2, cos3 + holeRadius2, sin3 + holeRadius2);
                            mVar.f7078r.arcTo(rectF3, f24, 180.0f);
                        } else {
                            rectF3 = rectF6;
                            double d4 = (double) (f24 * 0.017453292f);
                            mVar.f7078r.lineTo(eVar2.f7123R + (((float) Math.cos(d4)) * holeRadius), eVar2.f7124S + (holeRadius * ((float) Math.sin(d4))));
                        }
                        mVar.f7078r.arcTo(mVar.f7079s, f24, -f23);
                    } else {
                        mVar = this;
                        mVar.f7078r.addCircle(eVar2.f7123R, eVar2.f7124S, holeRadius, Path.Direction.CCW);
                        rectF3 = rectF6;
                    }
                    eVar = eVar2;
                    rectF2 = rectF3;
                    mVar.f7078r.close();
                    mVar.f7077q.drawPath(mVar.f7078r, super.f7036c);
                    f11 += f12 * f2;
                } else {
                    f8 = holeRadius;
                    f9 = radius;
                    eVar = centerCircleBox;
                    rectF4 = circleBox;
                    i5 = i4;
                    f6 = f19;
                    f7 = 360.0f;
                    rectF5 = rectF7;
                }
                if (f6 % f7 > Utils.f7147d) {
                    if (z3) {
                        float f26 = f14 + (f6 / 2.0f);
                        float f27 = f14;
                        rectF2 = rectF3;
                        float a3 = mo23378a(eVar, f4, f12 * b, cos2, sin2, f27, f6);
                        double d5 = (double) (f26 * 0.017453292f);
                        mVar.f7078r.lineTo(eVar.f7123R + (((float) Math.cos(d5)) * a3), eVar.f7124S + (a3 * ((float) Math.sin(d5))));
                    } else {
                        rectF2 = rectF3;
                        mVar.f7078r.lineTo(eVar.f7123R, eVar.f7124S);
                    }
                    mVar.f7078r.close();
                    mVar.f7077q.drawPath(mVar.f7078r, super.f7036c);
                    f11 += f12 * f2;
                }
                rectF2 = rectF3;
                mVar.f7078r.close();
                mVar.f7077q.drawPath(mVar.f7078r, super.f7036c);
                f11 += f12 * f2;
            } else {
                f11 += f12 * a;
                i2 = i9;
                rectF2 = rectF7;
                f4 = radius;
                f3 = rotationAngle;
                f2 = a;
                rectF = circleBox;
                i = t;
                fArr = drawAngles;
                i3 = i7;
                f5 = holeRadius;
                eVar = centerCircleBox;
            }
            i9 = i2 + 1;
            rectF7 = rectF2;
            holeRadius = f5;
            i7 = i3;
            centerCircleBox = eVar;
            radius = f4;
            rotationAngle = f3;
            t = i;
            drawAngles = fArr;
            a = f2;
            circleBox = rectF;
            iVar2 = iVar;
        }
        MPPointF.m11570b(centerCircleBox);
    }

    /* renamed from: a */
    public void mo23381a(Canvas canvas, String str, float f, float f2, int i) {
        super.f7038e.setColor(i);
        canvas.drawText(str, f, f2, super.f7038e);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23380a(Canvas canvas, String str, float f, float f2) {
        canvas.drawText(str, f, f2, this.f7071k);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        boolean z;
        float[] fArr;
        float f;
        MPPointF eVar;
        float f2;
        int i;
        RectF rectF;
        float f3;
        IPieDataSet a;
        float f4;
        int i2;
        float f5;
        int i3;
        float f6;
        float[] fArr2;
        float f7;
        float f8;
        Highlight[] dVarArr2 = dVarArr;
        boolean z2 = this.f7066f.mo13132s() && !this.f7066f.mo13157u();
        if (!z2 || !this.f7066f.mo13156t()) {
            float a2 = super.f7035b.mo23201a();
            float b = super.f7035b.mo23204b();
            float rotationAngle = this.f7066f.getRotationAngle();
            float[] drawAngles = this.f7066f.getDrawAngles();
            float[] absoluteAngles = this.f7066f.getAbsoluteAngles();
            MPPointF centerCircleBox = this.f7066f.getCenterCircleBox();
            float radius = this.f7066f.getRadius();
            float holeRadius = z2 ? (this.f7066f.getHoleRadius() / 100.0f) * radius : 0.0f;
            RectF rectF2 = this.f7082v;
            rectF2.set(0.0f, 0.0f, 0.0f, 0.0f);
            int i4 = 0;
            while (i4 < dVarArr2.length) {
                int g = (int) dVarArr2[i4].mo23249g();
                if (g < drawAngles.length && (a = ((PieData) this.f7066f.getData()).mo13374a(dVarArr2[i4].mo23245c())) != null && a.mo13367w()) {
                    int t = a.mo13417t();
                    int i5 = 0;
                    for (int i6 = 0; i6 < t; i6++) {
                        if (Math.abs(((PieEntry) a.mo13410b(i6)).mo13303c()) > Utils.f7147d) {
                            i5++;
                        }
                    }
                    if (g == 0) {
                        i2 = 1;
                        f4 = 0.0f;
                    } else {
                        f4 = absoluteAngles[g - 1] * a2;
                        i2 = 1;
                    }
                    if (i5 <= i2) {
                        f5 = 0.0f;
                    } else {
                        f5 = a.mo13432v0();
                    }
                    float f9 = drawAngles[g];
                    float F0 = a.mo13426F0();
                    int i7 = i4;
                    float f10 = radius + F0;
                    float f11 = holeRadius;
                    rectF2.set(this.f7066f.getCircleBox());
                    float f12 = -F0;
                    rectF2.inset(f12, f12);
                    boolean z3 = f5 > 0.0f && f9 <= 180.0f;
                    super.f7036c.setColor(a.mo13345c(g));
                    float f13 = i5 == 1 ? 0.0f : f5 / (radius * 0.017453292f);
                    float f14 = i5 == 1 ? 0.0f : f5 / (f10 * 0.017453292f);
                    float f15 = rotationAngle + (((f13 / 2.0f) + f4) * b);
                    float f16 = (f9 - f13) * b;
                    float f17 = f16 < 0.0f ? 0.0f : f16;
                    float f18 = (((f14 / 2.0f) + f4) * b) + rotationAngle;
                    float f19 = (f9 - f14) * b;
                    if (f19 < 0.0f) {
                        f19 = 0.0f;
                    }
                    this.f7078r.reset();
                    int i8 = (f17 > 360.0f ? 1 : (f17 == 360.0f ? 0 : -1));
                    if (i8 < 0 || f17 % 360.0f > Utils.f7147d) {
                        fArr2 = drawAngles;
                        f6 = f4;
                        double d = (double) (f18 * 0.017453292f);
                        i3 = i5;
                        z = z2;
                        this.f7078r.moveTo(centerCircleBox.f7123R + (((float) Math.cos(d)) * f10), centerCircleBox.f7124S + (f10 * ((float) Math.sin(d))));
                        this.f7078r.arcTo(rectF2, f18, f19);
                    } else {
                        this.f7078r.addCircle(centerCircleBox.f7123R, centerCircleBox.f7124S, f10, Path.Direction.CW);
                        fArr2 = drawAngles;
                        f6 = f4;
                        i3 = i5;
                        z = z2;
                    }
                    if (z3) {
                        double d2 = (double) (f15 * 0.017453292f);
                        i = i7;
                        rectF = rectF2;
                        f2 = f11;
                        eVar = centerCircleBox;
                        fArr = fArr2;
                        f7 = mo23378a(centerCircleBox, radius, f9 * b, (((float) Math.cos(d2)) * radius) + centerCircleBox.f7123R, centerCircleBox.f7124S + (((float) Math.sin(d2)) * radius), f15, f17);
                    } else {
                        rectF = rectF2;
                        eVar = centerCircleBox;
                        i = i7;
                        f2 = f11;
                        fArr = fArr2;
                        f7 = 0.0f;
                    }
                    RectF rectF3 = this.f7079s;
                    float f20 = eVar.f7123R;
                    float f21 = eVar.f7124S;
                    rectF3.set(f20 - f2, f21 - f2, f20 + f2, f21 + f2);
                    if (!z || (f2 <= 0.0f && !z3)) {
                        f3 = a2;
                        f = b;
                        if (f17 % 360.0f > Utils.f7147d) {
                            if (z3) {
                                double d3 = (double) ((f15 + (f17 / 2.0f)) * 0.017453292f);
                                this.f7078r.lineTo(eVar.f7123R + (((float) Math.cos(d3)) * f7), eVar.f7124S + (f7 * ((float) Math.sin(d3))));
                            } else {
                                this.f7078r.lineTo(eVar.f7123R, eVar.f7124S);
                            }
                        }
                    } else {
                        if (z3) {
                            if (f7 < 0.0f) {
                                f7 = -f7;
                            }
                            f8 = Math.max(f2, f7);
                        } else {
                            f8 = f2;
                        }
                        float f22 = (i3 == 1 || f8 == 0.0f) ? 0.0f : f5 / (f8 * 0.017453292f);
                        float f23 = ((f6 + (f22 / 2.0f)) * b) + rotationAngle;
                        float f24 = (f9 - f22) * b;
                        if (f24 < 0.0f) {
                            f24 = 0.0f;
                        }
                        float f25 = f23 + f24;
                        if (i8 < 0 || f17 % 360.0f > Utils.f7147d) {
                            double d4 = (double) (f25 * 0.017453292f);
                            f3 = a2;
                            f = b;
                            this.f7078r.lineTo(eVar.f7123R + (((float) Math.cos(d4)) * f8), eVar.f7124S + (f8 * ((float) Math.sin(d4))));
                            this.f7078r.arcTo(this.f7079s, f25, -f24);
                        } else {
                            this.f7078r.addCircle(eVar.f7123R, eVar.f7124S, f8, Path.Direction.CCW);
                            f3 = a2;
                            f = b;
                        }
                    }
                    this.f7078r.close();
                    this.f7077q.drawPath(this.f7078r, super.f7036c);
                } else {
                    i = i4;
                    rectF = rectF2;
                    f2 = holeRadius;
                    fArr = drawAngles;
                    z = z2;
                    f3 = a2;
                    f = b;
                    eVar = centerCircleBox;
                }
                i4 = i + 1;
                a2 = f3;
                rectF2 = rectF;
                holeRadius = f2;
                centerCircleBox = eVar;
                b = f;
                drawAngles = fArr;
                z2 = z;
                dVarArr2 = dVarArr;
            }
            MPPointF.m11570b(centerCircleBox);
        }
    }
}
