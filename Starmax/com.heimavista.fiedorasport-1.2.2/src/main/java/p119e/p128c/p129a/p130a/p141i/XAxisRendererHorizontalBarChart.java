package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import java.util.List;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p143j.FSize;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.r */
public class XAxisRendererHorizontalBarChart extends XAxisRenderer {

    /* renamed from: p */
    protected Path f7099p = new Path();

    public XAxisRendererHorizontalBarChart(ViewPortHandler jVar, XAxis hVar, Transformer gVar, BarChart barChart) {
        super(jVar, hVar, gVar);
    }

    /* renamed from: a */
    public void mo23336a(float f, float f2, boolean z) {
        float f3;
        double d;
        if (this.f7088a.mo23469j() > 10.0f && !this.f7088a.mo23485w()) {
            MPPointD b = this.f7005c.mo23440b(this.f7088a.mo23463g(), this.f7088a.mo23459e());
            MPPointD b2 = this.f7005c.mo23440b(this.f7088a.mo23463g(), this.f7088a.mo23467i());
            if (z) {
                f3 = (float) b2.f7121S;
                d = b.f7121S;
            } else {
                f3 = (float) b.f7121S;
                d = b2.f7121S;
            }
            MPPointD.m11565a(b);
            MPPointD.m11565a(b2);
            f = f3;
            f2 = (float) d;
        }
        mo23335a(f, f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo23401b() {
        this.f7007e.setTypeface(super.f7091h.mo13236c());
        this.f7007e.setTextSize(super.f7091h.mo13234b());
        FSize b = Utils.m11618b(this.f7007e, super.f7091h.mo13219p());
        float f = b.f7118S;
        FSize a = Utils.m11604a(b.f7117R, f, super.f7091h.mo13285z());
        super.f7091h.f2445J = Math.round((float) ((int) (b.f7117R + (super.f7091h.mo13238d() * 3.5f))));
        super.f7091h.f2446K = Math.round(f);
        XAxis hVar = super.f7091h;
        hVar.f2447L = (int) (a.f7117R + (hVar.mo13238d() * 3.5f));
        super.f7091h.f2448M = Math.round(a.f7118S);
        FSize.m11562a(a);
    }

    /* renamed from: c */
    public RectF mo23403c() {
        super.f7094k.set(this.f7088a.mo23476n());
        super.f7094k.inset(0.0f, -this.f7004b.mo13216m());
        return super.f7094k;
    }

    /* renamed from: d */
    public void mo23406d(Canvas canvas) {
        List<LimitLine> o = super.f7091h.mo13218o();
        if (o != null && o.size() > 0) {
            float[] fArr = super.f7095l;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            Path path = this.f7099p;
            path.reset();
            for (int i = 0; i < o.size(); i++) {
                LimitLine gVar = o.get(i);
                if (gVar.mo13240f()) {
                    int save = canvas.save();
                    super.f7096m.set(this.f7088a.mo23476n());
                    super.f7096m.inset(0.0f, -gVar.mo13280l());
                    canvas.clipRect(super.f7096m);
                    this.f7009g.setStyle(Paint.Style.STROKE);
                    this.f7009g.setColor(gVar.mo13279k());
                    this.f7009g.setStrokeWidth(gVar.mo13280l());
                    this.f7009g.setPathEffect(gVar.mo13275g());
                    fArr[1] = gVar.mo13278j();
                    this.f7005c.mo23442b(fArr);
                    path.moveTo(this.f7088a.mo23463g(), fArr[1]);
                    path.lineTo(this.f7088a.mo23465h(), fArr[1]);
                    canvas.drawPath(path, this.f7009g);
                    path.reset();
                    String h = gVar.mo13276h();
                    if (h != null && !h.equals("")) {
                        this.f7009g.setStyle(gVar.mo13281m());
                        this.f7009g.setPathEffect(null);
                        this.f7009g.setColor(gVar.mo13229a());
                        this.f7009g.setStrokeWidth(0.5f);
                        this.f7009g.setTextSize(gVar.mo13234b());
                        float a = (float) Utils.m11602a(this.f7009g, h);
                        float a2 = Utils.m11599a(4.0f) + gVar.mo13238d();
                        float l = gVar.mo13280l() + a + gVar.mo13239e();
                        LimitLine.C1653a i2 = gVar.mo13277i();
                        if (i2 == LimitLine.C1653a.RIGHT_TOP) {
                            this.f7009g.setTextAlign(Paint.Align.RIGHT);
                            canvas.drawText(h, this.f7088a.mo23465h() - a2, (fArr[1] - l) + a, this.f7009g);
                        } else if (i2 == LimitLine.C1653a.RIGHT_BOTTOM) {
                            this.f7009g.setTextAlign(Paint.Align.RIGHT);
                            canvas.drawText(h, this.f7088a.mo23465h() - a2, fArr[1] + l, this.f7009g);
                        } else if (i2 == LimitLine.C1653a.LEFT_TOP) {
                            this.f7009g.setTextAlign(Paint.Align.LEFT);
                            canvas.drawText(h, this.f7088a.mo23463g() + a2, (fArr[1] - l) + a, this.f7009g);
                        } else {
                            this.f7009g.setTextAlign(Paint.Align.LEFT);
                            canvas.drawText(h, this.f7088a.mo23487y() + a2, fArr[1] + l, this.f7009g);
                        }
                    }
                    canvas.restoreToCount(save);
                }
            }
        }
    }

    /* renamed from: a */
    public void mo23395a(Canvas canvas) {
        if (super.f7091h.mo13240f() && super.f7091h.mo13225v()) {
            float d = super.f7091h.mo13238d();
            this.f7007e.setTypeface(super.f7091h.mo13236c());
            this.f7007e.setTextSize(super.f7091h.mo13234b());
            this.f7007e.setColor(super.f7091h.mo13229a());
            MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
            if (super.f7091h.mo13282A() == XAxis.C1654a.TOP) {
                a.f7123R = 0.0f;
                a.f7124S = 0.5f;
                mo23397a(canvas, this.f7088a.mo23465h() + d, a);
            } else if (super.f7091h.mo13282A() == XAxis.C1654a.TOP_INSIDE) {
                a.f7123R = 1.0f;
                a.f7124S = 0.5f;
                mo23397a(canvas, this.f7088a.mo23465h() - d, a);
            } else if (super.f7091h.mo13282A() == XAxis.C1654a.BOTTOM) {
                a.f7123R = 1.0f;
                a.f7124S = 0.5f;
                mo23397a(canvas, this.f7088a.mo23463g() - d, a);
            } else if (super.f7091h.mo13282A() == XAxis.C1654a.BOTTOM_INSIDE) {
                a.f7123R = 1.0f;
                a.f7124S = 0.5f;
                mo23397a(canvas, this.f7088a.mo23463g() + d, a);
            } else {
                a.f7123R = 0.0f;
                a.f7124S = 0.5f;
                mo23397a(canvas, this.f7088a.mo23465h() + d, a);
                a.f7123R = 1.0f;
                a.f7124S = 0.5f;
                mo23397a(canvas, this.f7088a.mo23463g() - d, a);
            }
            MPPointF.m11570b(a);
        }
    }

    /* renamed from: b */
    public void mo23402b(Canvas canvas) {
        if (super.f7091h.mo13222s() && super.f7091h.mo13240f()) {
            this.f7008f.setColor(super.f7091h.mo13209g());
            this.f7008f.setStrokeWidth(super.f7091h.mo13212i());
            if (super.f7091h.mo13282A() == XAxis.C1654a.TOP || super.f7091h.mo13282A() == XAxis.C1654a.TOP_INSIDE || super.f7091h.mo13282A() == XAxis.C1654a.BOTH_SIDED) {
                canvas.drawLine(this.f7088a.mo23465h(), this.f7088a.mo23467i(), this.f7088a.mo23465h(), this.f7088a.mo23459e(), this.f7008f);
            }
            if (super.f7091h.mo13282A() == XAxis.C1654a.BOTTOM || super.f7091h.mo13282A() == XAxis.C1654a.BOTTOM_INSIDE || super.f7091h.mo13282A() == XAxis.C1654a.BOTH_SIDED) {
                canvas.drawLine(this.f7088a.mo23463g(), this.f7088a.mo23467i(), this.f7088a.mo23463g(), this.f7088a.mo23459e(), this.f7008f);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23397a(Canvas canvas, float f, MPPointF eVar) {
        float z = super.f7091h.mo13285z();
        boolean r = super.f7091h.mo13221r();
        float[] fArr = new float[(super.f7091h.f2359n * 2)];
        for (int i = 0; i < fArr.length; i += 2) {
            if (r) {
                fArr[i + 1] = super.f7091h.f2358m[i / 2];
            } else {
                fArr[i + 1] = super.f7091h.f2357l[i / 2];
            }
        }
        this.f7005c.mo23442b(fArr);
        for (int i2 = 0; i2 < fArr.length; i2 += 2) {
            float f2 = fArr[i2 + 1];
            if (this.f7088a.mo23462f(f2)) {
                ValueFormatter q = super.f7091h.mo13220q();
                XAxis hVar = super.f7091h;
                mo23400a(canvas, q.mo23221a(hVar.f2357l[i2 / 2], hVar), f, f2, eVar, z);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23396a(Canvas canvas, float f, float f2, Path path) {
        path.moveTo(this.f7088a.mo23465h(), f2);
        path.lineTo(this.f7088a.mo23463g(), f2);
        canvas.drawPath(path, this.f7006d);
        path.reset();
    }
}
