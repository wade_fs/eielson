package p119e.p128c.p129a.p130a.p135e.p137b;

import android.graphics.DashPathEffect;
import com.github.mikephil.charting.data.Entry;

/* renamed from: e.c.a.a.e.b.h */
public interface ILineScatterCandleRadarDataSet<T extends Entry> extends IBarLineScatterCandleBubbleDataSet<T> {
    /* renamed from: A */
    boolean mo23287A();

    /* renamed from: B */
    boolean mo23288B();

    /* renamed from: y */
    float mo23289y();

    /* renamed from: z */
    DashPathEffect mo23290z();
}
