package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import androidx.core.view.ViewCompat;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.k */
public abstract class LineRadarRenderer extends LineScatterCandleRadarRenderer {
    public LineRadarRenderer(ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
    }

    /* renamed from: b */
    private boolean mo23367b() {
        return Utils.m11623e() >= 18;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23375a(Canvas canvas, Path path, Drawable drawable) {
        if (mo23367b()) {
            int save = canvas.save();
            canvas.clipPath(path);
            drawable.setBounds((int) this.f7088a.mo23463g(), (int) this.f7088a.mo23467i(), (int) this.f7088a.mo23465h(), (int) this.f7088a.mo23459e());
            drawable.draw(canvas);
            canvas.restoreToCount(save);
            return;
        }
        throw new RuntimeException("Fill-drawables not (yet) supported below API level 18, this code was run on API level " + Utils.m11623e() + ".");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23374a(Canvas canvas, Path path, int i, int i2) {
        int i3 = (i & ViewCompat.MEASURED_SIZE_MASK) | (i2 << 24);
        if (mo23367b()) {
            int save = canvas.save();
            canvas.clipPath(path);
            canvas.drawColor(i3);
            canvas.restoreToCount(save);
            return;
        }
        Paint.Style style = this.f7036c.getStyle();
        int color = this.f7036c.getColor();
        this.f7036c.setStyle(Paint.Style.FILL);
        this.f7036c.setColor(i3);
        canvas.drawPath(path, this.f7036c);
        this.f7036c.setColor(color);
        this.f7036c.setStyle(style);
    }
}
