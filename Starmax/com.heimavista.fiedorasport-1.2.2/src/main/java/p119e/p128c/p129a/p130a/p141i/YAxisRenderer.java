package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import androidx.core.view.ViewCompat;
import androidx.exifinterface.media.ExifInterface;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import java.util.List;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.t */
public class YAxisRenderer extends AxisRenderer {

    /* renamed from: h */
    protected YAxis f7101h;

    /* renamed from: i */
    protected Paint f7102i;

    /* renamed from: j */
    protected Path f7103j = new Path();

    /* renamed from: k */
    protected RectF f7104k = new RectF();

    /* renamed from: l */
    protected float[] f7105l = new float[2];

    /* renamed from: m */
    protected Path f7106m = new Path();

    /* renamed from: n */
    protected RectF f7107n = new RectF();

    /* renamed from: o */
    protected Path f7108o = new Path();

    /* renamed from: p */
    protected float[] f7109p = new float[2];

    /* renamed from: q */
    protected RectF f7110q = new RectF();

    public YAxisRenderer(ViewPortHandler jVar, YAxis iVar, Transformer gVar) {
        super(jVar, gVar, iVar);
        this.f7101h = iVar;
        if (this.f7088a != null) {
            super.f7007e.setColor((int) ViewCompat.MEASURED_STATE_MASK);
            super.f7007e.setTextSize(Utils.m11599a(10.0f));
            this.f7102i = new Paint(1);
            this.f7102i.setColor(-7829368);
            this.f7102i.setStrokeWidth(1.0f);
            this.f7102i.setStyle(Paint.Style.STROKE);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23409a(Canvas canvas, float f, float[] fArr, float f2) {
        int i = this.f7101h.mo13294I() ? this.f7101h.f2359n : this.f7101h.f2359n - 1;
        for (int i2 = !this.f7101h.mo13293H(); i2 < i; i2++) {
            canvas.drawText(this.f7101h.mo13201b(i2), f, fArr[(i2 * 2) + 1] + f2, super.f7007e);
        }
    }

    /* renamed from: b */
    public void mo23411b(Canvas canvas) {
        float f;
        float f2;
        float f3;
        if (this.f7101h.mo13240f() && this.f7101h.mo13225v()) {
            float[] c = mo23413c();
            super.f7007e.setTypeface(this.f7101h.mo13236c());
            super.f7007e.setTextSize(this.f7101h.mo13234b());
            super.f7007e.setColor(this.f7101h.mo13229a());
            float d = this.f7101h.mo13238d();
            float a = (((float) Utils.m11602a(super.f7007e, ExifInterface.GPS_MEASUREMENT_IN_PROGRESS)) / 2.5f) + this.f7101h.mo13239e();
            YAxis.C1655a z = this.f7101h.mo13302z();
            YAxis.C1656b A = this.f7101h.mo13286A();
            if (z == YAxis.C1655a.LEFT) {
                if (A == YAxis.C1656b.OUTSIDE_CHART) {
                    super.f7007e.setTextAlign(Paint.Align.RIGHT);
                    f2 = this.f7088a.mo23487y();
                    f = f2 - d;
                    mo23409a(canvas, f, c, a);
                }
                super.f7007e.setTextAlign(Paint.Align.LEFT);
                f3 = this.f7088a.mo23487y();
            } else if (A == YAxis.C1656b.OUTSIDE_CHART) {
                super.f7007e.setTextAlign(Paint.Align.LEFT);
                f3 = this.f7088a.mo23465h();
            } else {
                super.f7007e.setTextAlign(Paint.Align.RIGHT);
                f2 = this.f7088a.mo23465h();
                f = f2 - d;
                mo23409a(canvas, f, c, a);
            }
            f = f3 + d;
            mo23409a(canvas, f, c, a);
        }
    }

    /* renamed from: c */
    public void mo23412c(Canvas canvas) {
        if (this.f7101h.mo13240f() && this.f7101h.mo13222s()) {
            super.f7008f.setColor(this.f7101h.mo13209g());
            super.f7008f.setStrokeWidth(this.f7101h.mo13212i());
            if (this.f7101h.mo13302z() == YAxis.C1655a.LEFT) {
                canvas.drawLine(this.f7088a.mo23463g(), this.f7088a.mo23467i(), this.f7088a.mo23463g(), this.f7088a.mo23459e(), super.f7008f);
                return;
            }
            canvas.drawLine(this.f7088a.mo23465h(), this.f7088a.mo23467i(), this.f7088a.mo23465h(), this.f7088a.mo23459e(), super.f7008f);
        }
    }

    /* renamed from: d */
    public void mo23414d(Canvas canvas) {
        if (this.f7101h.mo13240f()) {
            if (this.f7101h.mo13224u()) {
                int save = canvas.save();
                canvas.clipRect(mo23410b());
                float[] c = mo23413c();
                super.f7006d.setColor(this.f7101h.mo13214k());
                super.f7006d.setStrokeWidth(this.f7101h.mo13216m());
                super.f7006d.setPathEffect(this.f7101h.mo13215l());
                Path path = this.f7103j;
                path.reset();
                for (int i = 0; i < c.length; i += 2) {
                    canvas.drawPath(mo23407a(path, i, c), super.f7006d);
                    path.reset();
                }
                canvas.restoreToCount(save);
            }
            if (this.f7101h.mo13295J()) {
                mo23408a(canvas);
            }
        }
    }

    /* renamed from: e */
    public void mo23415e(Canvas canvas) {
        List<LimitLine> o = this.f7101h.mo13218o();
        if (o != null && o.size() > 0) {
            float[] fArr = this.f7109p;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            Path path = this.f7108o;
            path.reset();
            for (int i = 0; i < o.size(); i++) {
                LimitLine gVar = o.get(i);
                if (gVar.mo13240f()) {
                    int save = canvas.save();
                    this.f7110q.set(this.f7088a.mo23476n());
                    this.f7110q.inset(0.0f, -gVar.mo13280l());
                    canvas.clipRect(this.f7110q);
                    super.f7009g.setStyle(Paint.Style.STROKE);
                    super.f7009g.setColor(gVar.mo13279k());
                    super.f7009g.setStrokeWidth(gVar.mo13280l());
                    super.f7009g.setPathEffect(gVar.mo13275g());
                    fArr[1] = gVar.mo13278j();
                    super.f7005c.mo23442b(fArr);
                    path.moveTo(this.f7088a.mo23463g(), fArr[1]);
                    path.lineTo(this.f7088a.mo23465h(), fArr[1]);
                    canvas.drawPath(path, super.f7009g);
                    path.reset();
                    String h = gVar.mo13276h();
                    if (h != null && !h.equals("")) {
                        super.f7009g.setStyle(gVar.mo13281m());
                        super.f7009g.setPathEffect(null);
                        super.f7009g.setColor(gVar.mo13229a());
                        super.f7009g.setTypeface(gVar.mo13236c());
                        super.f7009g.setStrokeWidth(0.5f);
                        super.f7009g.setTextSize(gVar.mo13234b());
                        float a = (float) Utils.m11602a(super.f7009g, h);
                        float a2 = Utils.m11599a(4.0f) + gVar.mo13238d();
                        float l = gVar.mo13280l() + a + gVar.mo13239e();
                        LimitLine.C1653a i2 = gVar.mo13277i();
                        if (i2 == LimitLine.C1653a.RIGHT_TOP) {
                            super.f7009g.setTextAlign(Paint.Align.RIGHT);
                            canvas.drawText(h, this.f7088a.mo23465h() - a2, (fArr[1] - l) + a, super.f7009g);
                        } else if (i2 == LimitLine.C1653a.RIGHT_BOTTOM) {
                            super.f7009g.setTextAlign(Paint.Align.RIGHT);
                            canvas.drawText(h, this.f7088a.mo23465h() - a2, fArr[1] + l, super.f7009g);
                        } else if (i2 == LimitLine.C1653a.LEFT_TOP) {
                            super.f7009g.setTextAlign(Paint.Align.LEFT);
                            canvas.drawText(h, this.f7088a.mo23463g() + a2, (fArr[1] - l) + a, super.f7009g);
                        } else {
                            super.f7009g.setTextAlign(Paint.Align.LEFT);
                            canvas.drawText(h, this.f7088a.mo23487y() + a2, fArr[1] + l, super.f7009g);
                        }
                    }
                    canvas.restoreToCount(save);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Path mo23407a(Path path, int i, float[] fArr) {
        int i2 = i + 1;
        path.moveTo(this.f7088a.mo23487y(), fArr[i2]);
        path.lineTo(this.f7088a.mo23465h(), fArr[i2]);
        return path;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23408a(Canvas canvas) {
        int save = canvas.save();
        this.f7107n.set(this.f7088a.mo23476n());
        this.f7107n.inset(0.0f, -this.f7101h.mo13292G());
        canvas.clipRect(this.f7107n);
        MPPointD a = super.f7005c.mo23428a(0.0f, 0.0f);
        this.f7102i.setColor(this.f7101h.mo13291F());
        this.f7102i.setStrokeWidth(this.f7101h.mo13292G());
        Path path = this.f7106m;
        path.reset();
        path.moveTo(this.f7088a.mo23463g(), (float) a.f7121S);
        path.lineTo(this.f7088a.mo23465h(), (float) a.f7121S);
        canvas.drawPath(path, this.f7102i);
        canvas.restoreToCount(save);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public float[] mo23413c() {
        int length = this.f7105l.length;
        int i = this.f7101h.f2359n;
        if (length != i * 2) {
            this.f7105l = new float[(i * 2)];
        }
        float[] fArr = this.f7105l;
        for (int i2 = 0; i2 < fArr.length; i2 += 2) {
            fArr[i2 + 1] = this.f7101h.f2357l[i2 / 2];
        }
        super.f7005c.mo23442b(fArr);
        return fArr;
    }

    /* renamed from: b */
    public RectF mo23410b() {
        this.f7104k.set(this.f7088a.mo23476n());
        this.f7104k.inset(0.0f, -super.f7004b.mo13216m());
        return this.f7104k;
    }
}
