package p119e.p128c.p129a.p130a.p135e.p137b;

import com.github.mikephil.charting.data.BarEntry;

/* renamed from: e.c.a.a.e.b.a */
public interface IBarDataSet extends IBarLineScatterCandleBubbleDataSet<BarEntry> {
    /* renamed from: m0 */
    int mo13327m0();

    /* renamed from: n0 */
    float mo13328n0();

    /* renamed from: p0 */
    int mo13329p0();

    /* renamed from: q0 */
    int mo13330q0();

    /* renamed from: r0 */
    int mo13331r0();

    /* renamed from: t0 */
    boolean mo13332t0();

    /* renamed from: u0 */
    String[] mo13333u0();
}
