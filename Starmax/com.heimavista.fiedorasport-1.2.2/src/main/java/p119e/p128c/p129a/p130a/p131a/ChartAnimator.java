package p119e.p128c.p129a.p130a.p131a;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import androidx.annotation.RequiresApi;
import p119e.p128c.p129a.p130a.p131a.Easing;

/* renamed from: e.c.a.a.a.a */
public class ChartAnimator {

    /* renamed from: a */
    private ValueAnimator.AnimatorUpdateListener f6922a;

    /* renamed from: b */
    protected float f6923b = 1.0f;

    /* renamed from: c */
    protected float f6924c = 1.0f;

    @RequiresApi(11)
    public ChartAnimator(ValueAnimator.AnimatorUpdateListener animatorUpdateListener) {
        this.f6922a = animatorUpdateListener;
    }

    @RequiresApi(11)
    /* renamed from: c */
    private ObjectAnimator m11187c(int i, Easing.C3843c0 c0Var) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "phaseX", 0.0f, 1.0f);
        ofFloat.setInterpolator(c0Var);
        ofFloat.setDuration((long) i);
        return ofFloat;
    }

    @RequiresApi(11)
    /* renamed from: d */
    private ObjectAnimator m11188d(int i, Easing.C3843c0 c0Var) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "phaseY", 0.0f, 1.0f);
        ofFloat.setInterpolator(c0Var);
        ofFloat.setDuration((long) i);
        return ofFloat;
    }

    @RequiresApi(11)
    /* renamed from: a */
    public void mo23202a(int i) {
        mo23203a(i, Easing.f6925a);
    }

    @RequiresApi(11)
    /* renamed from: b */
    public void mo23205b(int i) {
        mo23206b(i, Easing.f6925a);
    }

    @RequiresApi(11)
    /* renamed from: a */
    public void mo23203a(int i, Easing.C3843c0 c0Var) {
        ObjectAnimator c = m11187c(i, c0Var);
        c.addUpdateListener(this.f6922a);
        c.start();
    }

    @RequiresApi(11)
    /* renamed from: b */
    public void mo23206b(int i, Easing.C3843c0 c0Var) {
        ObjectAnimator d = m11188d(i, c0Var);
        d.addUpdateListener(this.f6922a);
        d.start();
    }

    /* renamed from: a */
    public float mo23201a() {
        return this.f6924c;
    }

    /* renamed from: b */
    public float mo23204b() {
        return this.f6923b;
    }
}
