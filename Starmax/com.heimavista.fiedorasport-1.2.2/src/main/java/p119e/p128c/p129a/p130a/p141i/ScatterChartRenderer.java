package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.ScatterData;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.ScatterDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IScatterDataSet;
import p119e.p128c.p129a.p130a.p141i.BarLineScatterCandleBubbleRenderer;
import p119e.p128c.p129a.p130a.p141i.p142w.IShapeRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.p */
public class ScatterChartRenderer extends LineScatterCandleRadarRenderer {

    /* renamed from: h */
    protected ScatterDataProvider f7089h;

    /* renamed from: i */
    float[] f7090i = new float[2];

    public ScatterChartRenderer(ScatterDataProvider hVar, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7089h = hVar;
    }

    /* renamed from: a */
    public void mo23337a() {
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        for (IScatterDataSet kVar : this.f7089h.getScatterData().mo13386c()) {
            if (kVar.isVisible()) {
                mo23393a(canvas, kVar);
            }
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        IScatterDataSet kVar;
        Entry entry;
        if (mo23356a(this.f7089h)) {
            List c = this.f7089h.getScatterData().mo13386c();
            for (int i = 0; i < this.f7089h.getScatterData().mo13383b(); i++) {
                IScatterDataSet kVar2 = (IScatterDataSet) c.get(i);
                if (mo23347b(kVar2) && kVar2.mo13417t() >= 1) {
                    mo23355a(kVar2);
                    this.f7016f.mo23348a(this.f7089h, kVar2);
                    Transformer a = this.f7089h.mo12952a(kVar2.mo13364s());
                    float a2 = this.f7035b.mo23201a();
                    float b = this.f7035b.mo23204b();
                    BarLineScatterCandleBubbleRenderer.C3869a aVar = this.f7016f;
                    float[] a3 = a.mo23439a(kVar2, a2, b, aVar.f7017a, aVar.f7018b);
                    float a4 = Utils.m11599a(kVar2.mo23298e0());
                    ValueFormatter j = kVar2.mo13356j();
                    MPPointF a5 = MPPointF.m11568a(kVar2.mo13365u());
                    a5.f7123R = Utils.m11599a(a5.f7123R);
                    a5.f7124S = Utils.m11599a(a5.f7124S);
                    int i2 = 0;
                    while (i2 < a3.length && this.f7088a.mo23456c(a3[i2])) {
                        if (this.f7088a.mo23454b(a3[i2])) {
                            int i3 = i2 + 1;
                            if (this.f7088a.mo23462f(a3[i3])) {
                                int i4 = i2 / 2;
                                Entry b2 = kVar2.mo13410b(this.f7016f.f7017a + i4);
                                if (kVar2.mo13363r()) {
                                    String a6 = j.mo23226a(b2);
                                    float f = a3[i2];
                                    entry = b2;
                                    float f2 = a3[i3] - a4;
                                    kVar = kVar2;
                                    mo23394a(canvas, a6, f, f2, kVar2.mo13348d(i4 + this.f7016f.f7017a));
                                } else {
                                    entry = b2;
                                    kVar = kVar2;
                                }
                                if (entry.mo13371b() != null && kVar.mo13349d()) {
                                    Drawable b3 = entry.mo13371b();
                                    Utils.m11606a(canvas, b3, (int) (a3[i2] + a5.f7123R), (int) (a3[i3] + a5.f7124S), b3.getIntrinsicWidth(), b3.getIntrinsicHeight());
                                }
                                i2 += 2;
                                kVar2 = kVar;
                            }
                        }
                        kVar = kVar2;
                        i2 += 2;
                        kVar2 = kVar;
                    }
                    MPPointF.m11570b(a5);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23393a(Canvas canvas, IScatterDataSet kVar) {
        int i;
        IScatterDataSet kVar2 = kVar;
        if (kVar.mo13417t() >= 1) {
            ViewPortHandler jVar = this.f7088a;
            Transformer a = this.f7089h.mo12952a(kVar.mo13364s());
            float b = this.f7035b.mo23204b();
            IShapeRenderer g0 = kVar.mo23299g0();
            if (g0 == null) {
                Log.i("MISSING", "There's no IShapeRenderer specified for ScatterDataSet");
                return;
            }
            int min = (int) Math.min(Math.ceil((double) (((float) kVar.mo13417t()) * this.f7035b.mo23201a())), (double) ((float) kVar.mo13417t()));
            int i2 = 0;
            while (i2 < min) {
                Entry b2 = kVar2.mo13410b(i2);
                this.f7090i[0] = b2.mo13315d();
                this.f7090i[1] = b2.mo13303c() * b;
                a.mo23442b(this.f7090i);
                if (jVar.mo23456c(this.f7090i[0])) {
                    if (!jVar.mo23454b(this.f7090i[0]) || !jVar.mo23462f(this.f7090i[1])) {
                        i = i2;
                    } else {
                        this.f7036c.setColor(kVar2.mo13345c(i2 / 2));
                        ViewPortHandler jVar2 = this.f7088a;
                        float[] fArr = this.f7090i;
                        i = i2;
                        g0.mo23416a(canvas, kVar, jVar2, fArr[0], fArr[1], this.f7036c);
                    }
                    i2 = i + 1;
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: a */
    public void mo23394a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        ScatterData scatterData = this.f7089h.getScatterData();
        for (Highlight dVar : dVarArr) {
            IScatterDataSet kVar = (IScatterDataSet) scatterData.mo13374a(dVar.mo23245c());
            if (kVar != null && kVar.mo13367w()) {
                Entry a = kVar.mo13406a(dVar.mo23249g(), dVar.mo23251i());
                if (mo23346a(a, kVar)) {
                    MPPointD a2 = this.f7089h.mo12952a(kVar.mo13364s()).mo23428a(a.mo13315d(), a.mo13303c() * this.f7035b.mo23204b());
                    dVar.mo23241a((float) a2.f7120R, (float) a2.f7121S);
                    mo23376a(canvas, (float) a2.f7120R, (float) a2.f7121S, kVar);
                }
            }
        }
    }
}
