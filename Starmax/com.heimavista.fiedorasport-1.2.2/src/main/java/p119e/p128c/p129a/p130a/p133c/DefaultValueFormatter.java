package p119e.p128c.p129a.p130a.p133c;

import com.facebook.appevents.AppEventsConstants;
import java.text.DecimalFormat;

/* renamed from: e.c.a.a.c.b */
public class DefaultValueFormatter extends ValueFormatter {

    /* renamed from: a */
    protected DecimalFormat f6938a;

    public DefaultValueFormatter(int i) {
        mo23218a(i);
    }

    /* renamed from: a */
    public void mo23218a(int i) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 == 0) {
                stringBuffer.append(".");
            }
            stringBuffer.append(AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
        this.f6938a = new DecimalFormat("###,###,###,##0" + stringBuffer.toString());
    }

    /* renamed from: a */
    public String mo23217a(float f) {
        return this.f6938a.format((double) f);
    }
}
