package p119e.p128c.p129a.p130a.p143j;

/* renamed from: e.c.a.a.j.h */
public class TransformerHorizontalBarChart extends Transformer {
    public TransformerHorizontalBarChart(ViewPortHandler jVar) {
        super(jVar);
    }

    /* renamed from: a */
    public void mo23434a(boolean z) {
        super.f7135b.reset();
        if (!z) {
            super.f7135b.postTranslate(super.f7136c.mo23487y(), super.f7136c.mo23471k() - super.f7136c.mo23486x());
            return;
        }
        super.f7135b.setTranslate(-(super.f7136c.mo23473l() - super.f7136c.mo23488z()), super.f7136c.mo23471k() - super.f7136c.mo23486x());
        super.f7135b.postScale(-1.0f, 1.0f);
    }
}
