package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarEntry;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p132b.BarBuffer;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p134d.Range;
import p119e.p128c.p129a.p130a.p135e.p136a.BarDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;
import p119e.p128c.p129a.p130a.p140h.GradientColor;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.b */
public class BarChartRenderer extends BarLineScatterCandleBubbleRenderer {

    /* renamed from: g */
    protected BarDataProvider f7010g;

    /* renamed from: h */
    protected RectF f7011h = new RectF();

    /* renamed from: i */
    protected BarBuffer[] f7012i;

    /* renamed from: j */
    protected Paint f7013j;

    /* renamed from: k */
    protected Paint f7014k;

    /* renamed from: l */
    private RectF f7015l = new RectF();

    public BarChartRenderer(BarDataProvider aVar, ChartAnimator aVar2, ViewPortHandler jVar) {
        super(aVar2, jVar);
        this.f7010g = aVar;
        this.f7037d = new Paint(1);
        this.f7037d.setStyle(Paint.Style.FILL);
        this.f7037d.setColor(Color.rgb(0, 0, 0));
        this.f7037d.setAlpha(120);
        this.f7013j = new Paint(1);
        this.f7013j.setStyle(Paint.Style.FILL);
        this.f7014k = new Paint(1);
        this.f7014k.setStyle(Paint.Style.STROKE);
    }

    /* renamed from: a */
    public void mo23337a() {
        BarData barData = this.f7010g.getBarData();
        this.f7012i = new BarBuffer[barData.mo13383b()];
        for (int i = 0; i < this.f7012i.length; i++) {
            IBarDataSet aVar = (IBarDataSet) barData.mo13374a(i);
            this.f7012i[i] = new BarBuffer(aVar.mo13417t() * 4 * (aVar.mo13332t0() ? aVar.mo13330q0() : 1), barData.mo13383b(), aVar.mo13332t0());
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        boolean z;
        float f;
        float f2;
        List list;
        boolean z2;
        MPPointF eVar;
        int i;
        float f3;
        boolean z3;
        Transformer gVar;
        float[] fArr;
        float f4;
        float[] fArr2;
        BarEntry barEntry;
        int i2;
        float f5;
        float f6;
        BarEntry barEntry2;
        List list2;
        int i3;
        ValueFormatter gVar2;
        MPPointF eVar2;
        float f7;
        BarEntry barEntry3;
        if (mo23356a(this.f7010g)) {
            List c = this.f7010g.getBarData().mo13386c();
            float a = Utils.m11599a(4.5f);
            boolean b = this.f7010g.mo12939b();
            int i4 = 0;
            while (i4 < this.f7010g.getBarData().mo13383b()) {
                IBarDataSet aVar = (IBarDataSet) c.get(i4);
                if (!mo23347b(aVar)) {
                    list = c;
                    f2 = f;
                    z2 = z;
                } else {
                    mo23355a(aVar);
                    boolean b2 = this.f7010g.mo12956b(aVar.mo13364s());
                    float a2 = (float) Utils.m11602a(this.f7038e, "8");
                    float f8 = z ? -f : a2 + f;
                    float f9 = z ? a2 + f : -f;
                    if (b2) {
                        f8 = (-f8) - a2;
                        f9 = (-f9) - a2;
                    }
                    float f10 = f8;
                    float f11 = f9;
                    BarBuffer bVar = this.f7012i[i4];
                    float b3 = this.f7035b.mo23204b();
                    ValueFormatter j = aVar.mo13356j();
                    MPPointF a3 = MPPointF.m11568a(aVar.mo13365u());
                    a3.f7123R = Utils.m11599a(a3.f7123R);
                    a3.f7124S = Utils.m11599a(a3.f7124S);
                    if (!aVar.mo13332t0()) {
                        int i5 = 0;
                        while (((float) i5) < ((float) bVar.f6930b.length) * this.f7035b.mo23201a()) {
                            float[] fArr3 = bVar.f6930b;
                            float f12 = (fArr3[i5] + fArr3[i5 + 2]) / 2.0f;
                            if (!this.f7088a.mo23456c(f12)) {
                                break;
                            }
                            int i6 = i5 + 1;
                            if (!this.f7088a.mo23462f(bVar.f6930b[i6]) || !this.f7088a.mo23454b(f12)) {
                                i3 = i5;
                                gVar2 = j;
                                list2 = c;
                                eVar2 = a3;
                            } else {
                                int i7 = i5 / 4;
                                BarEntry barEntry4 = (BarEntry) aVar.mo13410b(i7);
                                float c2 = barEntry4.mo13303c();
                                if (aVar.mo13363r()) {
                                    String a4 = j.mo23223a(barEntry4);
                                    int i8 = (c2 > 0.0f ? 1 : (c2 == 0.0f ? 0 : -1));
                                    float[] fArr4 = bVar.f6930b;
                                    barEntry3 = barEntry4;
                                    f7 = f12;
                                    String str = a4;
                                    i3 = i5;
                                    list2 = c;
                                    eVar2 = a3;
                                    float f13 = i8 >= 0 ? fArr4[i6] + f10 : fArr4[i5 + 3] + f11;
                                    gVar2 = j;
                                    mo23341a(canvas, str, f7, f13, aVar.mo13348d(i7));
                                } else {
                                    barEntry3 = barEntry4;
                                    f7 = f12;
                                    i3 = i5;
                                    gVar2 = j;
                                    list2 = c;
                                    eVar2 = a3;
                                }
                                if (barEntry3.mo13371b() != null && aVar.mo13349d()) {
                                    Drawable b4 = barEntry3.mo13371b();
                                    Utils.m11606a(canvas, b4, (int) (f7 + eVar2.f7123R), (int) ((c2 >= 0.0f ? bVar.f6930b[i6] + f10 : bVar.f6930b[i3 + 3] + f11) + eVar2.f7124S), b4.getIntrinsicWidth(), b4.getIntrinsicHeight());
                                }
                            }
                            i5 = i3 + 4;
                            a3 = eVar2;
                            j = gVar2;
                            c = list2;
                        }
                        list = c;
                        eVar = a3;
                    } else {
                        ValueFormatter gVar3 = j;
                        list = c;
                        eVar = a3;
                        Transformer a5 = this.f7010g.mo12952a(aVar.mo13364s());
                        int i9 = 0;
                        int i10 = 0;
                        while (((float) i9) < ((float) aVar.mo13417t()) * this.f7035b.mo23201a()) {
                            BarEntry barEntry5 = (BarEntry) aVar.mo13410b(i9);
                            float[] i11 = barEntry5.mo13308i();
                            float[] fArr5 = bVar.f6930b;
                            float f14 = (fArr5[i10] + fArr5[i10 + 2]) / 2.0f;
                            int d = aVar.mo13348d(i9);
                            if (i11 != null) {
                                BarEntry barEntry6 = barEntry5;
                                i = i9;
                                f3 = f;
                                z3 = z;
                                fArr = i11;
                                gVar = a5;
                                float f15 = f14;
                                float[] fArr6 = new float[(fArr.length * 2)];
                                float f16 = -barEntry6.mo13305f();
                                int i12 = 0;
                                int i13 = 0;
                                float f17 = 0.0f;
                                while (i12 < fArr6.length) {
                                    float f18 = fArr[i13];
                                    int i14 = (f18 > 0.0f ? 1 : (f18 == 0.0f ? 0 : -1));
                                    if (!(i14 == 0 && (f17 == 0.0f || f16 == 0.0f))) {
                                        if (i14 >= 0) {
                                            f18 = f17 + f18;
                                            f17 = f18;
                                        } else {
                                            float f19 = f16;
                                            f16 -= f18;
                                            f18 = f19;
                                        }
                                    }
                                    fArr6[i12 + 1] = f18 * b3;
                                    i12 += 2;
                                    i13++;
                                }
                                gVar.mo23442b(fArr6);
                                int i15 = 0;
                                while (i15 < fArr6.length) {
                                    float f20 = fArr[i15 / 2];
                                    float f21 = fArr6[i15 + 1] + (((f20 > 0.0f ? 1 : (f20 == 0.0f ? 0 : -1)) == 0 && (f16 > 0.0f ? 1 : (f16 == 0.0f ? 0 : -1)) == 0 && (f17 > 0.0f ? 1 : (f17 == 0.0f ? 0 : -1)) > 0) || (f20 > 0.0f ? 1 : (f20 == 0.0f ? 0 : -1)) < 0 ? f11 : f10);
                                    if (!this.f7088a.mo23456c(f15)) {
                                        break;
                                    }
                                    if (!this.f7088a.mo23462f(f21) || !this.f7088a.mo23454b(f15)) {
                                        fArr2 = fArr6;
                                        f4 = f15;
                                        barEntry = barEntry6;
                                        i2 = i15;
                                    } else {
                                        if (aVar.mo13363r()) {
                                            BarEntry barEntry7 = barEntry6;
                                            barEntry = barEntry7;
                                            f5 = f21;
                                            i2 = i15;
                                            fArr2 = fArr6;
                                            f4 = f15;
                                            mo23341a(canvas, gVar3.mo23222a(f20, barEntry7), f15, f5, d);
                                        } else {
                                            f5 = f21;
                                            fArr2 = fArr6;
                                            f4 = f15;
                                            barEntry = barEntry6;
                                            i2 = i15;
                                        }
                                        if (barEntry.mo13371b() != null && aVar.mo13349d()) {
                                            Drawable b5 = barEntry.mo13371b();
                                            Utils.m11606a(canvas, b5, (int) (f4 + eVar.f7123R), (int) (f5 + eVar.f7124S), b5.getIntrinsicWidth(), b5.getIntrinsicHeight());
                                        }
                                    }
                                    i15 = i2 + 2;
                                    barEntry6 = barEntry;
                                    fArr6 = fArr2;
                                    f15 = f4;
                                }
                            } else if (!this.f7088a.mo23456c(f14)) {
                                break;
                            } else {
                                float[] fArr7 = i11;
                                int i16 = i10 + 1;
                                if (!this.f7088a.mo23462f(bVar.f6930b[i16]) || !this.f7088a.mo23454b(f14)) {
                                    a5 = a5;
                                    z = z;
                                    f = f;
                                    i9 = i9;
                                } else {
                                    if (aVar.mo13363r()) {
                                        f6 = f14;
                                        f3 = f;
                                        fArr = fArr7;
                                        barEntry2 = barEntry5;
                                        i = i9;
                                        z3 = z;
                                        gVar = a5;
                                        mo23341a(canvas, gVar3.mo23223a(barEntry5), f6, bVar.f6930b[i16] + (barEntry5.mo13303c() >= 0.0f ? f10 : f11), d);
                                    } else {
                                        f6 = f14;
                                        i = i9;
                                        f3 = f;
                                        z3 = z;
                                        fArr = fArr7;
                                        barEntry2 = barEntry5;
                                        gVar = a5;
                                    }
                                    if (barEntry2.mo13371b() != null && aVar.mo13349d()) {
                                        Drawable b6 = barEntry2.mo13371b();
                                        Utils.m11606a(canvas, b6, (int) (eVar.f7123R + f6), (int) (bVar.f6930b[i16] + (barEntry2.mo13303c() >= 0.0f ? f10 : f11) + eVar.f7124S), b6.getIntrinsicWidth(), b6.getIntrinsicHeight());
                                    }
                                }
                            }
                            if (fArr == null) {
                                i10 += 4;
                            } else {
                                i10 += fArr.length * 4;
                            }
                            i9 = i + 1;
                            a5 = gVar;
                            z = z3;
                            f = f3;
                        }
                    }
                    f2 = f;
                    z2 = z;
                    MPPointF.m11570b(eVar);
                }
                i4++;
                b = z2;
                c = list;
                a = f2;
            }
        }
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        BarData barData = this.f7010g.getBarData();
        for (int i = 0; i < barData.mo13383b(); i++) {
            IBarDataSet aVar = (IBarDataSet) barData.mo13374a(i);
            if (aVar.isVisible()) {
                mo23340a(canvas, aVar, i);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23340a(Canvas canvas, IBarDataSet aVar, int i) {
        IBarDataSet aVar2 = aVar;
        int i2 = i;
        Transformer a = this.f7010g.mo12952a(aVar.mo13364s());
        this.f7014k.setColor(aVar.mo13327m0());
        this.f7014k.setStrokeWidth(Utils.m11599a(aVar.mo13328n0()));
        boolean z = true;
        boolean z2 = aVar.mo13328n0() > 0.0f;
        float a2 = this.f7035b.mo23201a();
        float b = this.f7035b.mo23204b();
        if (this.f7010g.mo12938a()) {
            this.f7013j.setColor(aVar.mo13329p0());
            float k = this.f7010g.getBarData().mo13323k() / 2.0f;
            int min = Math.min((int) Math.ceil((double) (((float) aVar.mo13417t()) * a2)), aVar.mo13417t());
            for (int i3 = 0; i3 < min; i3++) {
                float d = ((BarEntry) aVar2.mo13410b(i3)).mo13315d();
                RectF rectF = this.f7015l;
                rectF.left = d - k;
                rectF.right = d + k;
                a.mo23432a(rectF);
                if (this.f7088a.mo23454b(this.f7015l.right)) {
                    if (!this.f7088a.mo23456c(this.f7015l.left)) {
                        break;
                    }
                    this.f7015l.top = this.f7088a.mo23467i();
                    this.f7015l.bottom = this.f7088a.mo23459e();
                    canvas.drawRect(this.f7015l, this.f7013j);
                }
            }
        }
        BarBuffer bVar = this.f7012i[i2];
        bVar.mo23209a(a2, b);
        bVar.mo23213a(i2);
        bVar.mo23215a(this.f7010g.mo12956b(aVar.mo13364s()));
        bVar.mo23211a(this.f7010g.getBarData().mo13323k());
        bVar.mo23214a(aVar2);
        a.mo23442b(bVar.f6930b);
        if (aVar.mo13361o().size() != 1) {
            z = false;
        }
        if (z) {
            this.f7036c.setColor(aVar.mo13366v());
        }
        for (int i4 = 0; i4 < bVar.mo23210b(); i4 += 4) {
            int i5 = i4 + 2;
            if (this.f7088a.mo23454b(bVar.f6930b[i5])) {
                if (this.f7088a.mo23456c(bVar.f6930b[i4])) {
                    if (!z) {
                        this.f7036c.setColor(aVar2.mo13345c(i4 / 4));
                    }
                    if (aVar.mo13353h() != null) {
                        GradientColor h = aVar.mo13353h();
                        Paint paint = this.f7036c;
                        float[] fArr = bVar.f6930b;
                        paint.setShader(new LinearGradient(fArr[i4], fArr[i4 + 3], fArr[i4], fArr[i4 + 1], h.mo23333b(), h.mo23332a(), Shader.TileMode.MIRROR));
                    }
                    if (aVar.mo13362p() != null) {
                        Paint paint2 = this.f7036c;
                        float[] fArr2 = bVar.f6930b;
                        float f = fArr2[i4];
                        float f2 = fArr2[i4 + 3];
                        float f3 = fArr2[i4];
                        float f4 = fArr2[i4 + 1];
                        int i6 = i4 / 4;
                        paint2.setShader(new LinearGradient(f, f2, f3, f4, aVar2.mo13351e(i6).mo23333b(), aVar2.mo13351e(i6).mo23332a(), Shader.TileMode.MIRROR));
                    }
                    float[] fArr3 = bVar.f6930b;
                    int i7 = i4 + 1;
                    int i8 = i4 + 3;
                    canvas.drawRect(fArr3[i4], fArr3[i7], fArr3[i5], fArr3[i8], this.f7036c);
                    if (z2) {
                        float[] fArr4 = bVar.f6930b;
                        canvas.drawRect(fArr4[i4], fArr4[i7], fArr4[i5], fArr4[i8], this.f7014k);
                    }
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23338a(float f, float f2, float f3, float f4, Transformer gVar) {
        this.f7011h.set(f - f4, f2, f + f4, f3);
        gVar.mo23433a(this.f7011h, this.f7035b.mo23204b());
    }

    /* renamed from: a */
    public void mo23341a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        float f;
        float f2;
        BarData barData = this.f7010g.getBarData();
        for (Highlight dVar : dVarArr) {
            IBarDataSet aVar = (IBarDataSet) barData.mo13374a(dVar.mo23245c());
            if (aVar != null && aVar.mo13367w()) {
                BarEntry barEntry = (BarEntry) aVar.mo13406a(dVar.mo23249g(), dVar.mo23251i());
                if (mo23346a(barEntry, aVar)) {
                    Transformer a = this.f7010g.mo12952a(aVar.mo13364s());
                    this.f7037d.setColor(aVar.mo13335x());
                    this.f7037d.setAlpha(aVar.mo13331r0());
                    if (!(dVar.mo23248f() >= 0 && barEntry.mo13309j())) {
                        f2 = barEntry.mo13303c();
                        f = 0.0f;
                    } else if (this.f7010g.mo12940c()) {
                        float g = barEntry.mo13306g();
                        f = -barEntry.mo13305f();
                        f2 = g;
                    } else {
                        Range jVar = barEntry.mo13307h()[dVar.mo23248f()];
                        f2 = jVar.f6956a;
                        f = jVar.f6957b;
                    }
                    mo23338a(barEntry.mo13315d(), f2, f, barData.mo13323k() / 2.0f, a);
                    mo23343a(dVar, this.f7011h);
                    canvas.drawRect(this.f7011h, this.f7037d);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23343a(Highlight dVar, RectF rectF) {
        dVar.mo23241a(rectF.centerX(), rectF.top);
    }
}
