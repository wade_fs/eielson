package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.BarData;

/* renamed from: e.c.a.a.e.a.a */
public interface BarDataProvider extends BarLineScatterCandleBubbleDataProvider {
    /* renamed from: a */
    boolean mo12938a();

    /* renamed from: b */
    boolean mo12939b();

    /* renamed from: c */
    boolean mo12940c();

    BarData getBarData();
}
