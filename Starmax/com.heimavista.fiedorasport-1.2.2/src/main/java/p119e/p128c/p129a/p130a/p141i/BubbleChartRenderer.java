package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import com.facebook.appevents.AppEventsConstants;
import com.github.mikephil.charting.data.BubbleData;
import com.github.mikephil.charting.data.BubbleEntry;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p135e.p136a.BubbleDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBubbleDataSet;
import p119e.p128c.p129a.p130a.p141i.BarLineScatterCandleBubbleRenderer;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.d */
public class BubbleChartRenderer extends BarLineScatterCandleBubbleRenderer {

    /* renamed from: g */
    protected BubbleDataProvider f7021g;

    /* renamed from: h */
    private float[] f7022h = new float[4];

    /* renamed from: i */
    private float[] f7023i = new float[2];

    /* renamed from: j */
    private float[] f7024j = new float[3];

    public BubbleChartRenderer(BubbleDataProvider cVar, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7021g = cVar;
        this.f7036c.setStyle(Paint.Style.FILL);
        this.f7037d.setStyle(Paint.Style.STROKE);
        this.f7037d.setStrokeWidth(Utils.m11599a(1.5f));
    }

    /* renamed from: a */
    public void mo23337a() {
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        for (IBubbleDataSet cVar : this.f7021g.getBubbleData().mo13386c()) {
            if (cVar.isVisible()) {
                mo23350a(canvas, cVar);
            }
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        int i;
        float f;
        BubbleEntry bubbleEntry;
        float f2;
        BubbleData bubbleData = this.f7021g.getBubbleData();
        if (bubbleData != null && mo23356a(this.f7021g)) {
            List c = bubbleData.mo13386c();
            float a = (float) Utils.m11602a(this.f7038e, AppEventsConstants.EVENT_PARAM_VALUE_YES);
            for (int i2 = 0; i2 < c.size(); i2++) {
                IBubbleDataSet cVar = (IBubbleDataSet) c.get(i2);
                if (mo23347b(cVar) && cVar.mo13417t() >= 1) {
                    mo23355a(cVar);
                    float max = Math.max(0.0f, Math.min(1.0f, this.f7035b.mo23201a()));
                    float b = this.f7035b.mo23204b();
                    super.f7016f.mo23348a(this.f7021g, cVar);
                    Transformer a2 = this.f7021g.mo12952a(cVar.mo13364s());
                    BarLineScatterCandleBubbleRenderer.C3869a aVar = super.f7016f;
                    float[] a3 = a2.mo23436a(cVar, b, aVar.f7017a, aVar.f7018b);
                    float f3 = max == 1.0f ? b : max;
                    ValueFormatter j = cVar.mo13356j();
                    MPPointF a4 = MPPointF.m11568a(cVar.mo13365u());
                    a4.f7123R = Utils.m11599a(a4.f7123R);
                    a4.f7124S = Utils.m11599a(a4.f7124S);
                    for (int i3 = 0; i3 < a3.length; i3 = i + 2) {
                        int i4 = i3 / 2;
                        int d = cVar.mo13348d(super.f7016f.f7017a + i4);
                        int argb = Color.argb(Math.round(255.0f * f3), Color.red(d), Color.green(d), Color.blue(d));
                        float f4 = a3[i3];
                        float f5 = a3[i3 + 1];
                        if (!this.f7088a.mo23456c(f4)) {
                            break;
                        }
                        if (!this.f7088a.mo23454b(f4) || !this.f7088a.mo23462f(f5)) {
                            i = i3;
                        } else {
                            BubbleEntry bubbleEntry2 = (BubbleEntry) cVar.mo13410b(i4 + super.f7016f.f7017a);
                            if (cVar.mo13363r()) {
                                float f6 = f5 + (0.5f * a);
                                bubbleEntry = bubbleEntry2;
                                f2 = f5;
                                float f7 = f4;
                                f = f4;
                                float f8 = f6;
                                i = i3;
                                mo23351a(canvas, j.mo23224a(bubbleEntry2), f7, f8, argb);
                            } else {
                                bubbleEntry = bubbleEntry2;
                                f2 = f5;
                                f = f4;
                                i = i3;
                            }
                            if (bubbleEntry.mo13371b() != null && cVar.mo13349d()) {
                                Drawable b2 = bubbleEntry.mo13371b();
                                Utils.m11606a(canvas, b2, (int) (f + a4.f7123R), (int) (f2 + a4.f7124S), b2.getIntrinsicWidth(), b2.getIntrinsicHeight());
                            }
                        }
                    }
                    MPPointF.m11570b(a4);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23349a(float f, float f2, float f3, boolean z) {
        if (z) {
            f = f2 == 0.0f ? 1.0f : (float) Math.sqrt((double) (f / f2));
        }
        return f3 * f;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23350a(Canvas canvas, IBubbleDataSet cVar) {
        if (cVar.mo13417t() >= 1) {
            Transformer a = this.f7021g.mo12952a(cVar.mo13364s());
            float b = this.f7035b.mo23204b();
            super.f7016f.mo23348a(this.f7021g, cVar);
            float[] fArr = this.f7022h;
            fArr[0] = 0.0f;
            fArr[2] = 1.0f;
            a.mo23442b(fArr);
            boolean l0 = cVar.mo23257l0();
            float[] fArr2 = this.f7022h;
            float min = Math.min(Math.abs(this.f7088a.mo23459e() - this.f7088a.mo23467i()), Math.abs(fArr2[2] - fArr2[0]));
            int i = super.f7016f.f7017a;
            while (true) {
                BarLineScatterCandleBubbleRenderer.C3869a aVar = super.f7016f;
                if (i <= aVar.f7019c + aVar.f7017a) {
                    BubbleEntry bubbleEntry = (BubbleEntry) cVar.mo13410b(i);
                    this.f7023i[0] = bubbleEntry.mo13315d();
                    this.f7023i[1] = bubbleEntry.mo13303c() * b;
                    a.mo23442b(this.f7023i);
                    float a2 = mo23349a(bubbleEntry.mo13310e(), cVar.mo23258o0(), min, l0) / 2.0f;
                    if (this.f7088a.mo23458d(this.f7023i[1] + a2) && this.f7088a.mo23450a(this.f7023i[1] - a2) && this.f7088a.mo23454b(this.f7023i[0] + a2)) {
                        if (this.f7088a.mo23456c(this.f7023i[0] - a2)) {
                            this.f7036c.setColor(cVar.mo13345c((int) bubbleEntry.mo13315d()));
                            float[] fArr3 = this.f7023i;
                            canvas.drawCircle(fArr3[0], fArr3[1], a2, this.f7036c);
                        } else {
                            return;
                        }
                    }
                    i++;
                } else {
                    return;
                }
            }
        }
    }

    /* renamed from: a */
    public void mo23351a(Canvas canvas, String str, float f, float f2, int i) {
        this.f7038e.setColor(i);
        canvas.drawText(str, f, f2, this.f7038e);
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        Highlight[] dVarArr2 = dVarArr;
        BubbleData bubbleData = this.f7021g.getBubbleData();
        float b = this.f7035b.mo23204b();
        for (Highlight dVar : dVarArr2) {
            IBubbleDataSet cVar = (IBubbleDataSet) bubbleData.mo13374a(dVar.mo23245c());
            if (cVar != null && cVar.mo13367w()) {
                BubbleEntry bubbleEntry = (BubbleEntry) cVar.mo13406a(dVar.mo23249g(), dVar.mo23251i());
                if (bubbleEntry.mo13303c() == dVar.mo23251i() && mo23346a(bubbleEntry, cVar)) {
                    Transformer a = this.f7021g.mo12952a(cVar.mo13364s());
                    float[] fArr = this.f7022h;
                    fArr[0] = 0.0f;
                    fArr[2] = 1.0f;
                    a.mo23442b(fArr);
                    boolean l0 = cVar.mo23257l0();
                    float[] fArr2 = this.f7022h;
                    float min = Math.min(Math.abs(this.f7088a.mo23459e() - this.f7088a.mo23467i()), Math.abs(fArr2[2] - fArr2[0]));
                    this.f7023i[0] = bubbleEntry.mo13315d();
                    this.f7023i[1] = bubbleEntry.mo13303c() * b;
                    a.mo23442b(this.f7023i);
                    float[] fArr3 = this.f7023i;
                    dVar.mo23241a(fArr3[0], fArr3[1]);
                    float a2 = mo23349a(bubbleEntry.mo13310e(), cVar.mo23258o0(), min, l0) / 2.0f;
                    if (this.f7088a.mo23458d(this.f7023i[1] + a2) && this.f7088a.mo23450a(this.f7023i[1] - a2) && this.f7088a.mo23454b(this.f7023i[0] + a2)) {
                        if (this.f7088a.mo23456c(this.f7023i[0] - a2)) {
                            int c = cVar.mo13345c((int) bubbleEntry.mo13315d());
                            Color.RGBToHSV(Color.red(c), Color.green(c), Color.blue(c), this.f7024j);
                            float[] fArr4 = this.f7024j;
                            fArr4[2] = fArr4[2] * 0.5f;
                            this.f7037d.setColor(Color.HSVToColor(Color.alpha(c), this.f7024j));
                            this.f7037d.setStrokeWidth(cVar.mo23259s0());
                            float[] fArr5 = this.f7023i;
                            canvas.drawCircle(fArr5[0], fArr5[1], a2, this.f7037d);
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }
}
