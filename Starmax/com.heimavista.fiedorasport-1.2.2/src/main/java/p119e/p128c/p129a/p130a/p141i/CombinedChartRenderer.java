package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.data.CombinedData;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p131a.ChartAnimator;
import p119e.p128c.p129a.p130a.p134d.Highlight;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.f */
public class CombinedChartRenderer extends DataRenderer {

    /* renamed from: f */
    protected List<DataRenderer> f7031f = new ArrayList(5);

    /* renamed from: g */
    protected WeakReference<Chart> f7032g;

    /* renamed from: h */
    protected List<Highlight> f7033h = new ArrayList();

    /* renamed from: e.c.a.a.i.f$a */
    /* compiled from: CombinedChartRenderer */
    static /* synthetic */ class C3870a {

        /* renamed from: a */
        static final /* synthetic */ int[] f7034a = new int[CombinedChart.C1645a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.github.mikephil.charting.charts.CombinedChart$a[] r0 = com.github.mikephil.charting.charts.CombinedChart.C1645a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.f7034a = r0
                int[] r0 = p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.f7034a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.github.mikephil.charting.charts.CombinedChart$a r1 = com.github.mikephil.charting.charts.CombinedChart.C1645a.BAR     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.f7034a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.github.mikephil.charting.charts.CombinedChart$a r1 = com.github.mikephil.charting.charts.CombinedChart.C1645a.BUBBLE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.f7034a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.github.mikephil.charting.charts.CombinedChart$a r1 = com.github.mikephil.charting.charts.CombinedChart.C1645a.LINE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.f7034a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.github.mikephil.charting.charts.CombinedChart$a r1 = com.github.mikephil.charting.charts.CombinedChart.C1645a.CANDLE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.f7034a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.github.mikephil.charting.charts.CombinedChart$a r1 = com.github.mikephil.charting.charts.CombinedChart.C1645a.SCATTER     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p128c.p129a.p130a.p141i.CombinedChartRenderer.C3870a.<clinit>():void");
        }
    }

    public CombinedChartRenderer(CombinedChart combinedChart, ChartAnimator aVar, ViewPortHandler jVar) {
        super(aVar, jVar);
        this.f7032g = new WeakReference<>(combinedChart);
        mo23354b();
    }

    /* renamed from: a */
    public void mo23337a() {
        for (DataRenderer superR : this.f7031f) {
            superR.mo23337a();
        }
    }

    /* renamed from: b */
    public void mo23354b() {
        this.f7031f.clear();
        CombinedChart combinedChart = (CombinedChart) this.f7032g.get();
        if (combinedChart != null) {
            for (CombinedChart.C1645a aVar : combinedChart.getDrawOrder()) {
                int i = C3870a.f7034a[aVar.ordinal()];
                if (i != 1) {
                    if (i != 2) {
                        if (i != 3) {
                            if (i != 4) {
                                if (i == 5 && combinedChart.getScatterData() != null) {
                                    this.f7031f.add(new ScatterChartRenderer(combinedChart, super.f7035b, this.f7088a));
                                }
                            } else if (combinedChart.getCandleData() != null) {
                                this.f7031f.add(new CandleStickChartRenderer(combinedChart, super.f7035b, this.f7088a));
                            }
                        } else if (combinedChart.getLineData() != null) {
                            this.f7031f.add(new LineChartRenderer(combinedChart, super.f7035b, this.f7088a));
                        }
                    } else if (combinedChart.getBubbleData() != null) {
                        this.f7031f.add(new BubbleChartRenderer(combinedChart, super.f7035b, this.f7088a));
                    }
                } else if (combinedChart.getBarData() != null) {
                    this.f7031f.add(new BarChartRenderer(combinedChart, super.f7035b, this.f7088a));
                }
            }
        }
    }

    /* renamed from: c */
    public void mo23345c(Canvas canvas) {
        for (DataRenderer superR : this.f7031f) {
            superR.mo23345c(canvas);
        }
    }

    /* renamed from: a */
    public void mo23339a(Canvas canvas) {
        for (DataRenderer superR : this.f7031f) {
            superR.mo23339a(canvas);
        }
    }

    /* renamed from: a */
    public void mo23342a(Canvas canvas, Highlight[] dVarArr) {
        int i;
        Chart chart = this.f7032g.get();
        if (chart != null) {
            for (DataRenderer gVar : this.f7031f) {
                Object obj = null;
                if (gVar instanceof BarChartRenderer) {
                    obj = ((BarChartRenderer) gVar).f7010g.getBarData();
                } else if (gVar instanceof LineChartRenderer) {
                    obj = ((LineChartRenderer) gVar).f7050h.getLineData();
                } else if (gVar instanceof CandleStickChartRenderer) {
                    obj = ((CandleStickChartRenderer) gVar).f7025h.getCandleData();
                } else if (gVar instanceof ScatterChartRenderer) {
                    obj = ((ScatterChartRenderer) gVar).f7089h.getScatterData();
                } else if (gVar instanceof BubbleChartRenderer) {
                    obj = ((BubbleChartRenderer) gVar).f7021g.getBubbleData();
                }
                if (obj == null) {
                    i = -1;
                } else {
                    i = ((CombinedData) chart.getData()).mo13396k().indexOf(obj);
                }
                this.f7033h.clear();
                for (Highlight dVar : dVarArr) {
                    if (dVar.mo23244b() == i || dVar.mo23244b() == -1) {
                        this.f7033h.add(dVar);
                    }
                }
                List<Highlight> list = this.f7033h;
                super.mo23342a(canvas, (Highlight[]) list.toArray(new Highlight[list.size()]));
            }
        }
    }

    /* renamed from: b */
    public void mo23344b(Canvas canvas) {
        for (DataRenderer superR : this.f7031f) {
            superR.mo23344b(canvas);
        }
    }
}
