package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.CombinedData;

/* renamed from: e.c.a.a.e.a.f */
public interface CombinedDataProvider extends LineDataProvider, BarDataProvider, BubbleDataProvider, CandleDataProvider, ScatterDataProvider {
    CombinedData getCombinedData();
}
