package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Typeface;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.PieEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.ICandleDataSet;
import p119e.p128c.p129a.p130a.p135e.p137b.IPieDataSet;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.i */
public class LegendRenderer extends Renderer {

    /* renamed from: b */
    protected Paint f7040b;

    /* renamed from: c */
    protected Paint f7041c;

    /* renamed from: d */
    protected Legend f7042d;

    /* renamed from: e */
    protected List<LegendEntry> f7043e = new ArrayList(16);

    /* renamed from: f */
    protected Paint.FontMetrics f7044f = new Paint.FontMetrics();

    /* renamed from: g */
    private Path f7045g = new Path();

    /* renamed from: e.c.a.a.i.i$a */
    /* compiled from: LegendRenderer */
    static /* synthetic */ class C3871a {

        /* renamed from: a */
        static final /* synthetic */ int[] f7046a = new int[Legend.C1650d.values().length];

        /* renamed from: b */
        static final /* synthetic */ int[] f7047b = new int[Legend.C1652f.values().length];

        /* renamed from: c */
        static final /* synthetic */ int[] f7048c = new int[Legend.C1651e.values().length];

        /* renamed from: d */
        static final /* synthetic */ int[] f7049d = new int[Legend.C1649c.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(33:0|(2:1|2)|3|(2:5|6)|7|9|10|11|12|13|14|15|16|17|19|20|21|22|23|25|26|27|28|29|30|31|33|34|35|36|37|38|40) */
        /* JADX WARNING: Can't wrap try/catch for region: R(34:0|1|2|3|(2:5|6)|7|9|10|11|12|13|14|15|16|17|19|20|21|22|23|25|26|27|28|29|30|31|33|34|35|36|37|38|40) */
        /* JADX WARNING: Can't wrap try/catch for region: R(35:0|1|2|3|5|6|7|9|10|11|12|13|14|15|16|17|19|20|21|22|23|25|26|27|28|29|30|31|33|34|35|36|37|38|40) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x007b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x0085 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00a2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00ac */
        static {
            /*
                com.github.mikephil.charting.components.e$c[] r0 = com.github.mikephil.charting.components.Legend.C1649c.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d = r0
                r0 = 1
                int[] r1 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.github.mikephil.charting.components.e$c r2 = com.github.mikephil.charting.components.Legend.C1649c.NONE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d     // Catch:{ NoSuchFieldError -> 0x001f }
                com.github.mikephil.charting.components.e$c r3 = com.github.mikephil.charting.components.Legend.C1649c.EMPTY     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d     // Catch:{ NoSuchFieldError -> 0x002a }
                com.github.mikephil.charting.components.e$c r4 = com.github.mikephil.charting.components.Legend.C1649c.f2411R     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.github.mikephil.charting.components.e$c r4 = com.github.mikephil.charting.components.Legend.C1649c.CIRCLE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r5 = 4
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.github.mikephil.charting.components.e$c r4 = com.github.mikephil.charting.components.Legend.C1649c.SQUARE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r5 = 5
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7049d     // Catch:{ NoSuchFieldError -> 0x004b }
                com.github.mikephil.charting.components.e$c r4 = com.github.mikephil.charting.components.Legend.C1649c.LINE     // Catch:{ NoSuchFieldError -> 0x004b }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r5 = 6
                r3[r4] = r5     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                com.github.mikephil.charting.components.e$e[] r3 = com.github.mikephil.charting.components.Legend.C1651e.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7048c = r3
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7048c     // Catch:{ NoSuchFieldError -> 0x005e }
                com.github.mikephil.charting.components.e$e r4 = com.github.mikephil.charting.components.Legend.C1651e.HORIZONTAL     // Catch:{ NoSuchFieldError -> 0x005e }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x005e }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x005e }
            L_0x005e:
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7048c     // Catch:{ NoSuchFieldError -> 0x0068 }
                com.github.mikephil.charting.components.e$e r4 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL     // Catch:{ NoSuchFieldError -> 0x0068 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0068 }
                r3[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0068 }
            L_0x0068:
                com.github.mikephil.charting.components.e$f[] r3 = com.github.mikephil.charting.components.Legend.C1652f.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7047b = r3
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7047b     // Catch:{ NoSuchFieldError -> 0x007b }
                com.github.mikephil.charting.components.e$f r4 = com.github.mikephil.charting.components.Legend.C1652f.TOP     // Catch:{ NoSuchFieldError -> 0x007b }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x007b }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x007b }
            L_0x007b:
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7047b     // Catch:{ NoSuchFieldError -> 0x0085 }
                com.github.mikephil.charting.components.e$f r4 = com.github.mikephil.charting.components.Legend.C1652f.BOTTOM     // Catch:{ NoSuchFieldError -> 0x0085 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0085 }
                r3[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0085 }
            L_0x0085:
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7047b     // Catch:{ NoSuchFieldError -> 0x008f }
                com.github.mikephil.charting.components.e$f r4 = com.github.mikephil.charting.components.Legend.C1652f.CENTER     // Catch:{ NoSuchFieldError -> 0x008f }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x008f }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x008f }
            L_0x008f:
                com.github.mikephil.charting.components.e$d[] r3 = com.github.mikephil.charting.components.Legend.C1650d.values()
                int r3 = r3.length
                int[] r3 = new int[r3]
                p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7046a = r3
                int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7046a     // Catch:{ NoSuchFieldError -> 0x00a2 }
                com.github.mikephil.charting.components.e$d r4 = com.github.mikephil.charting.components.Legend.C1650d.LEFT     // Catch:{ NoSuchFieldError -> 0x00a2 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x00a2 }
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x00a2 }
            L_0x00a2:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7046a     // Catch:{ NoSuchFieldError -> 0x00ac }
                com.github.mikephil.charting.components.e$d r3 = com.github.mikephil.charting.components.Legend.C1650d.RIGHT     // Catch:{ NoSuchFieldError -> 0x00ac }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ac }
                r0[r3] = r1     // Catch:{ NoSuchFieldError -> 0x00ac }
            L_0x00ac:
                int[] r0 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7046a     // Catch:{ NoSuchFieldError -> 0x00b6 }
                com.github.mikephil.charting.components.e$d r1 = com.github.mikephil.charting.components.Legend.C1650d.CENTER     // Catch:{ NoSuchFieldError -> 0x00b6 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b6 }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b6 }
            L_0x00b6:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.<clinit>():void");
        }
    }

    public LegendRenderer(ViewPortHandler jVar, Legend eVar) {
        super(jVar);
        this.f7042d = eVar;
        this.f7040b = new Paint(1);
        this.f7040b.setTextSize(Utils.m11599a(9.0f));
        this.f7040b.setTextAlign(Paint.Align.LEFT);
        this.f7041c = new Paint(1);
        this.f7041c.setStyle(Paint.Style.FILL);
    }

    /* renamed from: a */
    public Paint mo23357a() {
        return this.f7040b;
    }

    /* JADX WARN: Type inference failed for: r4v1, types: [e.c.a.a.e.b.e] */
    /* JADX WARN: Type inference failed for: r7v2, types: [e.c.a.a.e.b.e] */
    /* renamed from: a */
    public void mo23361a(ChartData<?> iVar) {
        ChartData<?> iVar2;
        String str;
        ChartData<?> iVar3 = iVar;
        if (!this.f7042d.mo13274z()) {
            this.f7043e.clear();
            int i = 0;
            while (i < iVar.mo13383b()) {
                ? a = iVar3.mo13374a(i);
                List<Integer> o = a.mo13361o();
                int t = a.mo13417t();
                if (a instanceof IBarDataSet) {
                    IBarDataSet aVar = a;
                    if (aVar.mo13332t0()) {
                        String[] u0 = aVar.mo13333u0();
                        int i2 = 0;
                        while (i2 < o.size() && i2 < aVar.mo13330q0()) {
                            List<LegendEntry> list = this.f7043e;
                            LegendEntry fVar = r10;
                            LegendEntry fVar2 = new LegendEntry(u0[i2 % u0.length], a.mo13350e(), a.mo13358l(), a.mo13357k(), a.mo13346c(), o.get(i2).intValue());
                            list.add(fVar);
                            i2++;
                        }
                        if (aVar.mo13352f() != null) {
                            this.f7043e.add(new LegendEntry(a.mo13352f(), Legend.C1649c.NONE, Float.NaN, Float.NaN, null, 1122867));
                        }
                        iVar2 = iVar3;
                        i++;
                        iVar3 = iVar2;
                    }
                }
                if (a instanceof IPieDataSet) {
                    IPieDataSet iVar4 = a;
                    int i3 = 0;
                    while (i3 < o.size() && i3 < t) {
                        List<LegendEntry> list2 = this.f7043e;
                        LegendEntry fVar3 = r9;
                        LegendEntry fVar4 = new LegendEntry(((PieEntry) iVar4.mo13410b(i3)).mo13321e(), a.mo13350e(), a.mo13358l(), a.mo13357k(), a.mo13346c(), o.get(i3).intValue());
                        list2.add(fVar3);
                        i3++;
                    }
                    if (iVar4.mo13352f() != null) {
                        this.f7043e.add(new LegendEntry(a.mo13352f(), Legend.C1649c.NONE, Float.NaN, Float.NaN, null, 1122867));
                    }
                } else {
                    if (a instanceof ICandleDataSet) {
                        ICandleDataSet dVar = a;
                        if (dVar.mo23269k0() != 1122867) {
                            int k0 = dVar.mo23269k0();
                            int i0 = dVar.mo23267i0();
                            this.f7043e.add(new LegendEntry(null, a.mo13350e(), a.mo13358l(), a.mo13357k(), a.mo13346c(), k0));
                            this.f7043e.add(new LegendEntry(a.mo13352f(), a.mo13350e(), a.mo13358l(), a.mo13357k(), a.mo13346c(), i0));
                        }
                    }
                    int i4 = 0;
                    while (i4 < o.size() && i4 < t) {
                        if (i4 >= o.size() - 1 || i4 >= t - 1) {
                            str = iVar.mo13374a(i).mo13352f();
                        } else {
                            str = null;
                        }
                        this.f7043e.add(new LegendEntry(str, a.mo13350e(), a.mo13358l(), a.mo13357k(), a.mo13346c(), o.get(i4).intValue()));
                        i4++;
                    }
                }
                iVar2 = iVar;
                i++;
                iVar3 = iVar2;
            }
            if (this.f7042d.mo13260l() != null) {
                Collections.addAll(this.f7043e, this.f7042d.mo13260l());
            }
            this.f7042d.mo13249a(this.f7043e);
        }
        Typeface c = this.f7042d.mo13236c();
        if (c != null) {
            this.f7040b.setTypeface(c);
        }
        this.f7040b.setTextSize(this.f7042d.mo13234b());
        this.f7040b.setColor(this.f7042d.mo13229a());
        this.f7042d.mo13245a(this.f7040b, super.f7088a);
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x0266  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0160  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo23358a(android.graphics.Canvas r35) {
        /*
            r34 = this;
            r6 = r34
            r7 = r35
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            boolean r0 = r0.mo13240f()
            if (r0 != 0) goto L_0x000d
            return
        L_0x000d:
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            android.graphics.Typeface r0 = r0.mo13236c()
            if (r0 == 0) goto L_0x001a
            android.graphics.Paint r1 = r6.f7040b
            r1.setTypeface(r0)
        L_0x001a:
            android.graphics.Paint r0 = r6.f7040b
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            float r1 = r1.mo13234b()
            r0.setTextSize(r1)
            android.graphics.Paint r0 = r6.f7040b
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            int r1 = r1.mo13229a()
            r0.setColor(r1)
            android.graphics.Paint r0 = r6.f7040b
            android.graphics.Paint$FontMetrics r1 = r6.f7044f
            float r8 = p119e.p128c.p129a.p130a.p143j.Utils.m11601a(r0, r1)
            android.graphics.Paint r0 = r6.f7040b
            android.graphics.Paint$FontMetrics r1 = r6.f7044f
            float r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11614b(r0, r1)
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            float r1 = r1.mo13272x()
            float r1 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r1)
            float r9 = r0 + r1
            android.graphics.Paint r0 = r6.f7040b
            java.lang.String r1 = "ABC"
            int r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11602a(r0, r1)
            float r0 = (float) r0
            r10 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 / r10
            float r11 = r8 - r0
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            com.github.mikephil.charting.components.f[] r12 = r0.mo13259k()
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            float r0 = r0.mo13265q()
            float r13 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r0)
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            float r0 = r0.mo13271w()
            float r14 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r0)
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            com.github.mikephil.charting.components.e$e r0 = r0.mo13268t()
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            com.github.mikephil.charting.components.e$d r15 = r1.mo13266r()
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            com.github.mikephil.charting.components.e$f r1 = r1.mo13270v()
            com.github.mikephil.charting.components.e r2 = r6.f7042d
            com.github.mikephil.charting.components.e$b r5 = r2.mo13258j()
            com.github.mikephil.charting.components.e r2 = r6.f7042d
            float r2 = r2.mo13264p()
            float r16 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r2)
            com.github.mikephil.charting.components.e r2 = r6.f7042d
            float r2 = r2.mo13269u()
            float r4 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r2)
            com.github.mikephil.charting.components.e r2 = r6.f7042d
            float r2 = r2.mo13239e()
            com.github.mikephil.charting.components.e r3 = r6.f7042d
            float r3 = r3.mo13238d()
            int[] r17 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7046a
            int r18 = r15.ordinal()
            r10 = r17[r18]
            r17 = r4
            r4 = 2
            r20 = 0
            r21 = r14
            r14 = 1
            if (r10 == r14) goto L_0x013c
            if (r10 == r4) goto L_0x011a
            r4 = 3
            if (r10 == r4) goto L_0x00c9
            r26 = r8
            r14 = r9
            r7 = 0
            goto L_0x0155
        L_0x00c9:
            com.github.mikephil.charting.components.e$e r4 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL
            if (r0 != r4) goto L_0x00d7
            e.c.a.a.j.j r4 = r6.f7088a
            float r4 = r4.mo23473l()
            r10 = 1073741824(0x40000000, float:2.0)
            float r4 = r4 / r10
            goto L_0x00e7
        L_0x00d7:
            r10 = 1073741824(0x40000000, float:2.0)
            e.c.a.a.j.j r4 = r6.f7088a
            float r4 = r4.mo23463g()
            e.c.a.a.j.j r14 = r6.f7088a
            float r14 = r14.mo23469j()
            float r14 = r14 / r10
            float r4 = r4 + r14
        L_0x00e7:
            com.github.mikephil.charting.components.e$b r10 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r5 != r10) goto L_0x00ed
            r10 = r3
            goto L_0x00ee
        L_0x00ed:
            float r10 = -r3
        L_0x00ee:
            float r4 = r4 + r10
            com.github.mikephil.charting.components.e$e r10 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL
            if (r0 != r10) goto L_0x0116
            r14 = r9
            double r9 = (double) r4
            com.github.mikephil.charting.components.e$b r4 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            r24 = 4611686018427387904(0x4000000000000000, double:2.0)
            if (r5 != r4) goto L_0x0108
            com.github.mikephil.charting.components.e r4 = r6.f7042d
            float r4 = r4.f2402x
            float r4 = -r4
            r26 = r8
            double r7 = (double) r4
            double r7 = r7 / r24
            double r3 = (double) r3
            double r7 = r7 + r3
            goto L_0x0113
        L_0x0108:
            r26 = r8
            com.github.mikephil.charting.components.e r4 = r6.f7042d
            float r4 = r4.f2402x
            double r7 = (double) r4
            double r7 = r7 / r24
            double r3 = (double) r3
            double r7 = r7 - r3
        L_0x0113:
            double r9 = r9 + r7
            float r3 = (float) r9
            goto L_0x0154
        L_0x0116:
            r26 = r8
            r14 = r9
            goto L_0x013a
        L_0x011a:
            r26 = r8
            r14 = r9
            com.github.mikephil.charting.components.e$e r4 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL
            if (r0 != r4) goto L_0x0128
            e.c.a.a.j.j r4 = r6.f7088a
            float r4 = r4.mo23473l()
            goto L_0x012e
        L_0x0128:
            e.c.a.a.j.j r4 = r6.f7088a
            float r4 = r4.mo23465h()
        L_0x012e:
            float r4 = r4 - r3
            com.github.mikephil.charting.components.e$b r3 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r5 != r3) goto L_0x013a
            com.github.mikephil.charting.components.e r3 = r6.f7042d
            float r3 = r3.f2402x
            float r3 = r4 - r3
            goto L_0x0154
        L_0x013a:
            r7 = r4
            goto L_0x0155
        L_0x013c:
            r26 = r8
            r14 = r9
            com.github.mikephil.charting.components.e$e r4 = com.github.mikephil.charting.components.Legend.C1651e.VERTICAL
            if (r0 != r4) goto L_0x0144
            goto L_0x014b
        L_0x0144:
            e.c.a.a.j.j r4 = r6.f7088a
            float r4 = r4.mo23463g()
            float r3 = r3 + r4
        L_0x014b:
            com.github.mikephil.charting.components.e$b r4 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r5 != r4) goto L_0x0154
            com.github.mikephil.charting.components.e r4 = r6.f7042d
            float r4 = r4.f2402x
            float r3 = r3 + r4
        L_0x0154:
            r7 = r3
        L_0x0155:
            int[] r3 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7048c
            int r0 = r0.ordinal()
            r0 = r3[r0]
            r3 = 1
            if (r0 == r3) goto L_0x0266
            r4 = 2
            if (r0 == r4) goto L_0x0165
            goto L_0x03c9
        L_0x0165:
            int[] r0 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7047b
            int r1 = r1.ordinal()
            r0 = r0[r1]
            if (r0 == r3) goto L_0x01a3
            if (r0 == r4) goto L_0x018b
            r1 = 3
            if (r0 == r1) goto L_0x0176
            r0 = 0
            goto L_0x01b0
        L_0x0176:
            e.c.a.a.j.j r0 = r6.f7088a
            float r0 = r0.mo23471k()
            r1 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 / r1
            com.github.mikephil.charting.components.e r2 = r6.f7042d
            float r3 = r2.f2403y
            float r3 = r3 / r1
            float r0 = r0 - r3
            float r1 = r2.mo13239e()
            float r0 = r0 + r1
            goto L_0x01b0
        L_0x018b:
            com.github.mikephil.charting.components.e$d r0 = com.github.mikephil.charting.components.Legend.C1650d.CENTER
            if (r15 != r0) goto L_0x0196
            e.c.a.a.j.j r0 = r6.f7088a
            float r0 = r0.mo23471k()
            goto L_0x019c
        L_0x0196:
            e.c.a.a.j.j r0 = r6.f7088a
            float r0 = r0.mo23459e()
        L_0x019c:
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            float r1 = r1.f2403y
            float r1 = r1 + r2
            float r0 = r0 - r1
            goto L_0x01b0
        L_0x01a3:
            com.github.mikephil.charting.components.e$d r0 = com.github.mikephil.charting.components.Legend.C1650d.CENTER
            if (r15 != r0) goto L_0x01a9
            r0 = 0
            goto L_0x01af
        L_0x01a9:
            e.c.a.a.j.j r0 = r6.f7088a
            float r0 = r0.mo23467i()
        L_0x01af:
            float r0 = r0 + r2
        L_0x01b0:
            r15 = r0
            r9 = 0
            r10 = 0
            r18 = 0
        L_0x01b5:
            int r0 = r12.length
            if (r9 >= r0) goto L_0x03c9
            r4 = r12[r9]
            com.github.mikephil.charting.components.e$c r0 = r4.f2428b
            com.github.mikephil.charting.components.e$c r1 = com.github.mikephil.charting.components.Legend.C1649c.NONE
            if (r0 == r1) goto L_0x01c3
            r19 = 1
            goto L_0x01c5
        L_0x01c3:
            r19 = 0
        L_0x01c5:
            float r0 = r4.f2429c
            boolean r0 = java.lang.Float.isNaN(r0)
            if (r0 == 0) goto L_0x01d0
            r21 = r16
            goto L_0x01d8
        L_0x01d0:
            float r0 = r4.f2429c
            float r0 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r0)
            r21 = r0
        L_0x01d8:
            if (r19 == 0) goto L_0x020a
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r5 != r0) goto L_0x01e1
            float r0 = r7 + r10
            goto L_0x01e5
        L_0x01e1:
            float r0 = r21 - r10
            float r0 = r7 - r0
        L_0x01e5:
            r22 = r0
            float r3 = r15 + r11
            com.github.mikephil.charting.components.e r2 = r6.f7042d
            r0 = r34
            r1 = r35
            r24 = r2
            r2 = r22
            r8 = r17
            r17 = r4
            r27 = r11
            r11 = r5
            r5 = r24
            r0.mo23359a(r1, r2, r3, r4, r5)
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r11 != r0) goto L_0x0207
            float r0 = r22 + r21
            r22 = r0
        L_0x0207:
            r0 = r17
            goto L_0x0212
        L_0x020a:
            r27 = r11
            r8 = r17
            r11 = r5
            r0 = r4
            r22 = r7
        L_0x0212:
            java.lang.String r1 = r0.f2427a
            if (r1 == 0) goto L_0x0255
            if (r19 == 0) goto L_0x0224
            if (r18 != 0) goto L_0x0224
            com.github.mikephil.charting.components.e$b r1 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r11 != r1) goto L_0x0220
            r1 = r13
            goto L_0x0221
        L_0x0220:
            float r1 = -r13
        L_0x0221:
            float r1 = r22 + r1
            goto L_0x022a
        L_0x0224:
            if (r18 == 0) goto L_0x0228
            r1 = r7
            goto L_0x022a
        L_0x0228:
            r1 = r22
        L_0x022a:
            com.github.mikephil.charting.components.e$b r2 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r2) goto L_0x0238
            android.graphics.Paint r2 = r6.f7040b
            java.lang.String r3 = r0.f2427a
            int r2 = p119e.p128c.p129a.p130a.p143j.Utils.m11621c(r2, r3)
            float r2 = (float) r2
            float r1 = r1 - r2
        L_0x0238:
            if (r18 != 0) goto L_0x0244
            float r2 = r15 + r26
            java.lang.String r0 = r0.f2427a
            r5 = r35
            r6.mo23360a(r5, r1, r2, r0)
            goto L_0x0250
        L_0x0244:
            r5 = r35
            float r2 = r26 + r14
            float r15 = r15 + r2
            float r2 = r15 + r26
            java.lang.String r0 = r0.f2427a
            r6.mo23360a(r5, r1, r2, r0)
        L_0x0250:
            float r0 = r26 + r14
            float r15 = r15 + r0
            r10 = 0
            goto L_0x025d
        L_0x0255:
            r5 = r35
            float r21 = r21 + r8
            float r10 = r10 + r21
            r18 = 1
        L_0x025d:
            int r9 = r9 + 1
            r17 = r8
            r5 = r11
            r11 = r27
            goto L_0x01b5
        L_0x0266:
            r27 = r11
            r8 = r17
            r11 = r5
            r5 = r35
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            java.util.List r9 = r0.mo13257i()
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            java.util.List r10 = r0.mo13256h()
            com.github.mikephil.charting.components.e r0 = r6.f7042d
            java.util.List r4 = r0.mo13255g()
            int[] r0 = p119e.p128c.p129a.p130a.p141i.LegendRenderer.C3871a.f7047b
            int r1 = r1.ordinal()
            r0 = r0[r1]
            r3 = 1
            if (r0 == r3) goto L_0x02af
            r1 = 2
            if (r0 == r1) goto L_0x02a2
            r1 = 3
            if (r0 == r1) goto L_0x0292
            r2 = 0
            goto L_0x02af
        L_0x0292:
            e.c.a.a.j.j r0 = r6.f7088a
            float r0 = r0.mo23471k()
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            float r1 = r1.f2403y
            float r0 = r0 - r1
            r1 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 / r1
            float r2 = r2 + r0
            goto L_0x02af
        L_0x02a2:
            e.c.a.a.j.j r0 = r6.f7088a
            float r0 = r0.mo23471k()
            float r0 = r0 - r2
            com.github.mikephil.charting.components.e r1 = r6.f7042d
            float r1 = r1.f2403y
            float r2 = r0 - r1
        L_0x02af:
            int r1 = r12.length
            r0 = r2
            r17 = r7
            r2 = 0
            r3 = 0
        L_0x02b5:
            if (r2 >= r1) goto L_0x03c9
            r18 = r8
            r8 = r12[r2]
            r20 = r1
            com.github.mikephil.charting.components.e$c r1 = r8.f2428b
            com.github.mikephil.charting.components.e$c r5 = com.github.mikephil.charting.components.Legend.C1649c.NONE
            if (r1 == r5) goto L_0x02c6
            r22 = 1
            goto L_0x02c8
        L_0x02c6:
            r22 = 0
        L_0x02c8:
            float r1 = r8.f2429c
            boolean r1 = java.lang.Float.isNaN(r1)
            if (r1 == 0) goto L_0x02d3
            r24 = r16
            goto L_0x02db
        L_0x02d3:
            float r1 = r8.f2429c
            float r1 = p119e.p128c.p129a.p130a.p143j.Utils.m11599a(r1)
            r24 = r1
        L_0x02db:
            int r1 = r4.size()
            if (r2 >= r1) goto L_0x02f5
            java.lang.Object r1 = r4.get(r2)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x02f5
            float r1 = r26 + r14
            float r0 = r0 + r1
            r28 = r0
            r17 = r7
            goto L_0x02f7
        L_0x02f5:
            r28 = r0
        L_0x02f7:
            int r0 = (r17 > r7 ? 1 : (r17 == r7 ? 0 : -1))
            if (r0 != 0) goto L_0x0324
            com.github.mikephil.charting.components.e$d r0 = com.github.mikephil.charting.components.Legend.C1650d.CENTER
            if (r15 != r0) goto L_0x0324
            int r0 = r9.size()
            if (r3 >= r0) goto L_0x0324
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r0) goto L_0x0312
            java.lang.Object r0 = r9.get(r3)
            e.c.a.a.j.b r0 = (p119e.p128c.p129a.p130a.p143j.FSize) r0
            float r0 = r0.f7117R
            goto L_0x031b
        L_0x0312:
            java.lang.Object r0 = r9.get(r3)
            e.c.a.a.j.b r0 = (p119e.p128c.p129a.p130a.p143j.FSize) r0
            float r0 = r0.f7117R
            float r0 = -r0
        L_0x031b:
            r19 = 1073741824(0x40000000, float:2.0)
            float r0 = r0 / r19
            float r17 = r17 + r0
            int r3 = r3 + 1
            goto L_0x0326
        L_0x0324:
            r19 = 1073741824(0x40000000, float:2.0)
        L_0x0326:
            r29 = r3
            java.lang.String r0 = r8.f2427a
            if (r0 != 0) goto L_0x032f
            r30 = 1
            goto L_0x0331
        L_0x032f:
            r30 = 0
        L_0x0331:
            if (r22 == 0) goto L_0x0359
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r0) goto L_0x0339
            float r17 = r17 - r24
        L_0x0339:
            float r3 = r28 + r27
            com.github.mikephil.charting.components.e r5 = r6.f7042d
            r0 = r34
            r1 = r35
            r31 = r7
            r7 = r2
            r2 = r17
            r23 = 1
            r32 = r4
            r4 = r8
            r33 = r9
            r9 = r35
            r0.mo23359a(r1, r2, r3, r4, r5)
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r11 != r0) goto L_0x0364
            float r17 = r17 + r24
            goto L_0x0364
        L_0x0359:
            r32 = r4
            r31 = r7
            r33 = r9
            r23 = 1
            r9 = r35
            r7 = r2
        L_0x0364:
            if (r30 != 0) goto L_0x03a6
            if (r22 == 0) goto L_0x0371
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r0) goto L_0x036e
            float r0 = -r13
            goto L_0x036f
        L_0x036e:
            r0 = r13
        L_0x036f:
            float r17 = r17 + r0
        L_0x0371:
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r0) goto L_0x037f
            java.lang.Object r0 = r10.get(r7)
            e.c.a.a.j.b r0 = (p119e.p128c.p129a.p130a.p143j.FSize) r0
            float r0 = r0.f7117R
            float r17 = r17 - r0
        L_0x037f:
            r0 = r17
            float r1 = r28 + r26
            java.lang.String r2 = r8.f2427a
            r6.mo23360a(r9, r0, r1, r2)
            com.github.mikephil.charting.components.e$b r1 = com.github.mikephil.charting.components.Legend.C1648b.LEFT_TO_RIGHT
            if (r11 != r1) goto L_0x0395
            java.lang.Object r1 = r10.get(r7)
            e.c.a.a.j.b r1 = (p119e.p128c.p129a.p130a.p143j.FSize) r1
            float r1 = r1.f7117R
            float r0 = r0 + r1
        L_0x0395:
            com.github.mikephil.charting.components.e$b r1 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r1) goto L_0x039d
            r1 = r21
            float r2 = -r1
            goto L_0x03a0
        L_0x039d:
            r1 = r21
            r2 = r1
        L_0x03a0:
            float r0 = r0 + r2
            r17 = r0
            r0 = r18
            goto L_0x03b5
        L_0x03a6:
            r1 = r21
            com.github.mikephil.charting.components.e$b r0 = com.github.mikephil.charting.components.Legend.C1648b.RIGHT_TO_LEFT
            if (r11 != r0) goto L_0x03b0
            r0 = r18
            float r4 = -r0
            goto L_0x03b3
        L_0x03b0:
            r0 = r18
            r4 = r0
        L_0x03b3:
            float r17 = r17 + r4
        L_0x03b5:
            int r2 = r7 + 1
            r8 = r0
            r21 = r1
            r5 = r9
            r1 = r20
            r0 = r28
            r3 = r29
            r7 = r31
            r4 = r32
            r9 = r33
            goto L_0x02b5
        L_0x03c9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p128c.p129a.p130a.p141i.LegendRenderer.mo23358a(android.graphics.Canvas):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23359a(Canvas canvas, float f, float f2, LegendEntry fVar, Legend eVar) {
        int i = fVar.f2432f;
        if (i != 1122868 && i != 1122867 && i != 0) {
            int save = canvas.save();
            Legend.C1649c cVar = fVar.f2428b;
            if (cVar == Legend.C1649c.f2411R) {
                cVar = eVar.mo13261m();
            }
            this.f7041c.setColor(fVar.f2432f);
            float a = Utils.m11599a(Float.isNaN(fVar.f2429c) ? eVar.mo13264p() : fVar.f2429c);
            float f3 = a / 2.0f;
            switch (C3871a.f7049d[cVar.ordinal()]) {
                case 3:
                case 4:
                    this.f7041c.setStyle(Paint.Style.FILL);
                    canvas.drawCircle(f + f3, f2, f3, this.f7041c);
                    break;
                case 5:
                    this.f7041c.setStyle(Paint.Style.FILL);
                    canvas.drawRect(f, f2 - f3, f + a, f2 + f3, this.f7041c);
                    break;
                case 6:
                    float a2 = Utils.m11599a(Float.isNaN(fVar.f2430d) ? eVar.mo13263o() : fVar.f2430d);
                    DashPathEffect dashPathEffect = fVar.f2431e;
                    if (dashPathEffect == null) {
                        dashPathEffect = eVar.mo13262n();
                    }
                    this.f7041c.setStyle(Paint.Style.STROKE);
                    this.f7041c.setStrokeWidth(a2);
                    this.f7041c.setPathEffect(dashPathEffect);
                    this.f7045g.reset();
                    this.f7045g.moveTo(f, f2);
                    this.f7045g.lineTo(f + a, f2);
                    canvas.drawPath(this.f7045g, this.f7041c);
                    break;
            }
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23360a(Canvas canvas, float f, float f2, String str) {
        canvas.drawText(str, f, f2, this.f7040b);
    }
}
