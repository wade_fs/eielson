package p119e.p128c.p129a.p130a.p135e.p137b;

import android.graphics.Paint;
import com.github.mikephil.charting.data.CandleEntry;

/* renamed from: e.c.a.a.e.b.d */
public interface ICandleDataSet extends ILineScatterCandleRadarDataSet<CandleEntry> {
    /* renamed from: Z */
    int mo23260Z();

    /* renamed from: a0 */
    float mo23261a0();

    /* renamed from: b0 */
    Paint.Style mo23262b0();

    /* renamed from: c0 */
    float mo23263c0();

    /* renamed from: d0 */
    Paint.Style mo23264d0();

    /* renamed from: f0 */
    boolean mo23265f0();

    /* renamed from: h0 */
    int mo23266h0();

    /* renamed from: i0 */
    int mo23267i0();

    /* renamed from: j0 */
    boolean mo23268j0();

    /* renamed from: k0 */
    int mo23269k0();
}
