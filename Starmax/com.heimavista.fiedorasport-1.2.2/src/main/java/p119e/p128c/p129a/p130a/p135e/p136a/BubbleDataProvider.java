package p119e.p128c.p129a.p130a.p135e.p136a;

import com.github.mikephil.charting.data.BubbleData;

/* renamed from: e.c.a.a.e.a.c */
public interface BubbleDataProvider extends BarLineScatterCandleBubbleDataProvider {
    BubbleData getBubbleData();
}
