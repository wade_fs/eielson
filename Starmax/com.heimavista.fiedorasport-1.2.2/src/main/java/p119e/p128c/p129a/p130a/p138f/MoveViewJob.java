package p119e.p128c.p129a.p130a.p138f;

import android.view.View;
import p119e.p128c.p129a.p130a.p143j.ObjectPool;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.f.a */
public class MoveViewJob extends ViewPortJob {

    /* renamed from: X */
    private static ObjectPool<MoveViewJob> f6958X = ObjectPool.m11573a(2, new MoveViewJob(null, 0.0f, 0.0f, null, null));

    static {
        f6958X.mo23425a(0.5f);
    }

    public MoveViewJob(ViewPortHandler jVar, float f, float f2, Transformer gVar, View view) {
        super(jVar, f, f2, gVar, view);
    }

    /* renamed from: a */
    public static MoveViewJob m11363a(ViewPortHandler jVar, float f, float f2, Transformer gVar, View view) {
        MoveViewJob a = f6958X.mo23424a();
        super.f6960S = jVar;
        super.f6961T = f;
        super.f6962U = f2;
        super.f6963V = gVar;
        super.f6964W = view;
        return a;
    }

    public void run() {
        float[] fArr = super.f6959R;
        fArr[0] = super.f6961T;
        fArr[1] = super.f6962U;
        super.f6963V.mo23442b(fArr);
        super.f6960S.mo23448a(super.f6959R, super.f6964W);
        m11364a(this);
    }

    /* renamed from: a */
    public static void m11364a(MoveViewJob aVar) {
        f6958X.mo23426a(aVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ObjectPool.C3875a mo23300a() {
        return new MoveViewJob(super.f6960S, super.f6961T, super.f6962U, super.f6963V, super.f6964W);
    }
}
