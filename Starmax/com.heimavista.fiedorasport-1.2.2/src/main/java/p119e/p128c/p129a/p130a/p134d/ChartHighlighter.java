package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import com.github.mikephil.charting.data.DataSet;
import com.github.mikephil.charting.data.Entry;
import java.util.ArrayList;
import java.util.List;
import p119e.p128c.p129a.p130a.p135e.p136a.BarLineScatterCandleBubbleDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointD;

/* renamed from: e.c.a.a.d.b */
public class ChartHighlighter<T extends BarLineScatterCandleBubbleDataProvider> implements IHighlighter {

    /* renamed from: a */
    protected T f6941a;

    /* renamed from: b */
    protected List<Highlight> f6942b = new ArrayList();

    public ChartHighlighter(T t) {
        this.f6941a = t;
    }

    /* renamed from: a */
    public Highlight mo23231a(float f, float f2) {
        MPPointD b = mo23238b(f, f2);
        MPPointD.m11565a(b);
        return mo23235a((float) b.f7120R, f, f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public MPPointD mo23238b(float f, float f2) {
        return this.f6941a.mo12952a(YAxis.C1655a.LEFT).mo23440b(f, f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public List<Highlight> mo23239b(float f, float f2, float f3) {
        this.f6942b.clear();
        BarLineScatterCandleBubbleData a = mo23230a();
        if (a == null) {
            return this.f6942b;
        }
        int b = a.mo13383b();
        for (int i = 0; i < b; i++) {
            IDataSet a2 = a.mo13374a(i);
            if (a2.mo13367w()) {
                this.f6942b.addAll(mo23237a(a2, i, f, DataSet.C1658a.CLOSEST));
            }
        }
        return this.f6942b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Highlight mo23235a(float f, float f2, float f3) {
        List<Highlight> b = mo23239b(f, f2, f3);
        if (b.isEmpty()) {
            return null;
        }
        return mo23236a(b, f2, f3, mo23234a(b, f3, YAxis.C1655a.LEFT) < mo23234a(b, f3, YAxis.C1655a.RIGHT) ? YAxis.C1655a.LEFT : YAxis.C1655a.RIGHT, this.f6941a.getMaxHighlightDistance());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23234a(List<Highlight> list, float f, YAxis.C1655a aVar) {
        float f2 = Float.MAX_VALUE;
        for (int i = 0; i < list.size(); i++) {
            Highlight dVar = list.get(i);
            if (dVar.mo23240a() == aVar) {
                float abs = Math.abs(mo23233a(dVar) - f);
                if (abs < f2) {
                    f2 = abs;
                }
            }
        }
        return f2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23233a(Highlight dVar) {
        return dVar.mo23252j();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<Highlight> mo23237a(IDataSet eVar, int i, float f, DataSet.C1658a aVar) {
        Entry a;
        ArrayList arrayList = new ArrayList();
        List<Entry> b = eVar.mo13411b(f);
        if (b.size() == 0 && (a = eVar.mo13407a(f, Float.NaN, aVar)) != null) {
            b = eVar.mo13411b(a.mo13315d());
        }
        if (b.size() == 0) {
            return arrayList;
        }
        for (Entry entry : b) {
            MPPointD a2 = this.f6941a.mo12952a(eVar.mo13364s()).mo23428a(entry.mo13315d(), entry.mo13303c());
            arrayList.add(new Highlight(entry.mo13315d(), entry.mo13303c(), (float) a2.f7120R, (float) a2.f7121S, i, eVar.mo13364s()));
        }
        return arrayList;
    }

    /* renamed from: a */
    public Highlight mo23236a(List<Highlight> list, float f, float f2, YAxis.C1655a aVar, float f3) {
        Highlight dVar = null;
        for (int i = 0; i < list.size(); i++) {
            Highlight dVar2 = list.get(i);
            if (aVar == null || dVar2.mo23240a() == aVar) {
                float a = mo23228a(f, f2, dVar2.mo23250h(), dVar2.mo23252j());
                if (a < f3) {
                    dVar = dVar2;
                    f3 = a;
                }
            }
        }
        return dVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23228a(float f, float f2, float f3, float f4) {
        return (float) Math.hypot((double) (f - f3), (double) (f2 - f4));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public BarLineScatterCandleBubbleData mo23230a() {
        return this.f6941a.getData();
    }
}
