package p119e.p128c.p129a.p130a.p133c;

import p119e.p128c.p129a.p130a.p135e.p136a.LineDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.ILineDataSet;

/* renamed from: e.c.a.a.c.d */
public interface IFillFormatter {
    /* renamed from: a */
    float mo23219a(ILineDataSet fVar, LineDataProvider gVar);
}
