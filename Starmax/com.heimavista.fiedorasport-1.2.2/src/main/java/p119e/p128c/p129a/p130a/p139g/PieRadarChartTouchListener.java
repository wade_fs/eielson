package p119e.p128c.p129a.p130a.p139g;

import android.annotation.SuppressLint;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import com.github.mikephil.charting.charts.PieRadarChartBase;
import java.util.ArrayList;
import p119e.p128c.p129a.p130a.p139g.ChartTouchListener;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Utils;

/* renamed from: e.c.a.a.g.f */
public class PieRadarChartTouchListener extends ChartTouchListener<PieRadarChartBase<?>> {

    /* renamed from: U */
    private MPPointF f6995U = MPPointF.m11567a(0.0f, 0.0f);

    /* renamed from: V */
    private float f6996V = 0.0f;

    /* renamed from: W */
    private ArrayList<C3868a> f6997W = new ArrayList<>();

    /* renamed from: X */
    private long f6998X = 0;

    /* renamed from: Y */
    private float f6999Y = 0.0f;

    /* renamed from: e.c.a.a.g.f$a */
    /* compiled from: PieRadarChartTouchListener */
    private class C3868a {

        /* renamed from: a */
        public long f7000a;

        /* renamed from: b */
        public float f7001b;

        public C3868a(PieRadarChartTouchListener fVar, long j, float f) {
            this.f7000a = j;
            this.f7001b = f;
        }
    }

    public PieRadarChartTouchListener(PieRadarChartBase<?> pieRadarChartBase) {
        super(pieRadarChartBase);
    }

    /* renamed from: c */
    private void m11394c(float f, float f2) {
        long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
        this.f6997W.add(new C3868a(this, currentAnimationTimeMillis, ((PieRadarChartBase) super.f6983T).mo13163d(f, f2)));
        for (int size = this.f6997W.size(); size - 2 > 0 && currentAnimationTimeMillis - this.f6997W.get(0).f7000a > 1000; size--) {
            this.f6997W.remove(0);
        }
    }

    /* renamed from: d */
    private void m11395d() {
        this.f6997W.clear();
    }

    /* renamed from: a */
    public void mo23325a(float f, float f2) {
        this.f6996V = ((PieRadarChartBase) super.f6983T).mo13163d(f, f2) - ((PieRadarChartBase) super.f6983T).getRawRotationAngle();
    }

    /* renamed from: b */
    public void mo23327b(float f, float f2) {
        T t = super.f6983T;
        ((PieRadarChartBase) t).setRotationAngle(((PieRadarChartBase) t).mo13163d(f, f2) - this.f6996V);
    }

    public void onLongPress(MotionEvent motionEvent) {
        super.f6979P = ChartTouchListener.C3867a.LONG_PRESS;
        OnChartGestureListener onChartGestureListener = ((PieRadarChartBase) super.f6983T).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23318b(motionEvent);
        }
    }

    public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
        return true;
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        super.f6979P = ChartTouchListener.C3867a.SINGLE_TAP;
        OnChartGestureListener onChartGestureListener = ((PieRadarChartBase) super.f6983T).getOnChartGestureListener();
        if (onChartGestureListener != null) {
            onChartGestureListener.mo23314a(motionEvent);
        }
        if (!((PieRadarChartBase) super.f6983T).mo13070k()) {
            return false;
        }
        mo23312a(((PieRadarChartBase) super.f6983T).mo12937a(motionEvent.getX(), motionEvent.getY()), motionEvent);
        return true;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (!super.f6982S.onTouchEvent(motionEvent) && ((PieRadarChartBase) super.f6983T).mo13171p()) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int action = motionEvent.getAction();
            if (action == 0) {
                mo23313b(motionEvent);
                mo23326b();
                m11395d();
                if (((PieRadarChartBase) super.f6983T).mo13068i()) {
                    m11394c(x, y);
                }
                mo23325a(x, y);
                MPPointF eVar = this.f6995U;
                eVar.f7123R = x;
                eVar.f7124S = y;
            } else if (action == 1) {
                if (((PieRadarChartBase) super.f6983T).mo13068i()) {
                    mo23326b();
                    m11394c(x, y);
                    this.f6999Y = m11393c();
                    if (this.f6999Y != 0.0f) {
                        this.f6998X = AnimationUtils.currentAnimationTimeMillis();
                        Utils.m11610a((View) super.f6983T);
                    }
                }
                ((PieRadarChartBase) super.f6983T).mo13036g();
                super.f6980Q = 0;
                mo23310a(motionEvent);
            } else if (action == 2) {
                if (((PieRadarChartBase) super.f6983T).mo13068i()) {
                    m11394c(x, y);
                }
                if (super.f6980Q == 0) {
                    MPPointF eVar2 = this.f6995U;
                    if (ChartTouchListener.m11378a(x, eVar2.f7123R, y, eVar2.f7124S) > Utils.m11599a(8.0f)) {
                        super.f6979P = ChartTouchListener.C3867a.ROTATE;
                        super.f6980Q = 6;
                        ((PieRadarChartBase) super.f6983T).mo13035f();
                        mo23310a(motionEvent);
                    }
                }
                if (super.f6980Q == 6) {
                    mo23327b(x, y);
                    ((PieRadarChartBase) super.f6983T).invalidate();
                }
                mo23310a(motionEvent);
            }
        }
        return true;
    }

    /* renamed from: a */
    public void mo23324a() {
        if (this.f6999Y != 0.0f) {
            long currentAnimationTimeMillis = AnimationUtils.currentAnimationTimeMillis();
            this.f6999Y *= ((PieRadarChartBase) super.f6983T).getDragDecelerationFrictionCoef();
            T t = super.f6983T;
            ((PieRadarChartBase) t).setRotationAngle(((PieRadarChartBase) t).getRotationAngle() + (this.f6999Y * (((float) (currentAnimationTimeMillis - this.f6998X)) / 1000.0f)));
            this.f6998X = currentAnimationTimeMillis;
            if (((double) Math.abs(this.f6999Y)) >= 0.001d) {
                Utils.m11610a((View) super.f6983T);
            } else {
                mo23326b();
            }
        }
    }

    /* renamed from: b */
    public void mo23326b() {
        this.f6999Y = 0.0f;
    }

    /* renamed from: c */
    private float m11393c() {
        if (this.f6997W.isEmpty()) {
            return 0.0f;
        }
        boolean z = false;
        C3868a aVar = this.f6997W.get(0);
        ArrayList<C3868a> arrayList = this.f6997W;
        C3868a aVar2 = arrayList.get(arrayList.size() - 1);
        C3868a aVar3 = aVar;
        for (int size = this.f6997W.size() - 1; size >= 0; size--) {
            aVar3 = this.f6997W.get(size);
            if (aVar3.f7001b != aVar2.f7001b) {
                break;
            }
        }
        float f = ((float) (aVar2.f7000a - aVar.f7000a)) / 1000.0f;
        if (f == 0.0f) {
            f = 0.1f;
        }
        if (aVar2.f7001b >= aVar3.f7001b) {
            z = true;
        }
        if (((double) Math.abs(aVar2.f7001b - aVar3.f7001b)) > 270.0d) {
            z = !z;
        }
        float f2 = aVar2.f7001b;
        float f3 = aVar.f7001b;
        if (((double) (f2 - f3)) > 180.0d) {
            aVar.f7001b = (float) (((double) f3) + 360.0d);
        } else if (((double) (f3 - f2)) > 180.0d) {
            aVar2.f7001b = (float) (((double) f2) + 360.0d);
        }
        float abs = Math.abs((aVar2.f7001b - aVar.f7001b) / f);
        return !z ? -abs : abs;
    }
}
