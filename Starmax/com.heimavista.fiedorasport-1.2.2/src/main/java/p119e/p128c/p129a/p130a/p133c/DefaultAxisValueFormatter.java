package p119e.p128c.p129a.p130a.p133c;

import com.facebook.appevents.AppEventsConstants;
import java.text.DecimalFormat;

/* renamed from: e.c.a.a.c.a */
public class DefaultAxisValueFormatter extends ValueFormatter {

    /* renamed from: a */
    protected DecimalFormat f6936a;

    /* renamed from: b */
    protected int f6937b;

    public DefaultAxisValueFormatter(int i) {
        this.f6937b = i;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i2 = 0; i2 < i; i2++) {
            if (i2 == 0) {
                stringBuffer.append(".");
            }
            stringBuffer.append(AppEventsConstants.EVENT_PARAM_VALUE_NO);
        }
        this.f6936a = new DecimalFormat("###,###,###,##0" + stringBuffer.toString());
    }

    /* renamed from: a */
    public String mo23217a(float f) {
        return this.f6936a.format((double) f);
    }

    /* renamed from: a */
    public int mo23216a() {
        return this.f6937b;
    }
}
