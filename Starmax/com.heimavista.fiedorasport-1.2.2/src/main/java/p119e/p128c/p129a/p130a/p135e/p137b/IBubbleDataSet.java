package p119e.p128c.p129a.p130a.p135e.p137b;

import com.github.mikephil.charting.data.BubbleEntry;

/* renamed from: e.c.a.a.e.b.c */
public interface IBubbleDataSet extends IBarLineScatterCandleBubbleDataSet<BubbleEntry> {
    /* renamed from: l0 */
    boolean mo23257l0();

    /* renamed from: o0 */
    float mo23258o0();

    /* renamed from: s0 */
    float mo23259s0();
}
