package p119e.p128c.p129a.p130a.p143j;

import p119e.p128c.p129a.p130a.p138f.MoveViewJob;
import p119e.p128c.p129a.p130a.p143j.ObjectPool;

/* renamed from: e.c.a.a.j.b */
public final class FSize extends ObjectPool.C3875a {

    /* renamed from: T */
    private static ObjectPool<FSize> f7116T = ObjectPool.m11573a(256, new FSize(0.0f, 0.0f));

    /* renamed from: R */
    public float f7117R;

    /* renamed from: S */
    public float f7118S;

    static {
        f7116T.mo23425a(0.5f);
    }

    public FSize() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ObjectPool.C3875a mo23300a() {
        return new FSize(0.0f, 0.0f);
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof FSize)) {
            return false;
        }
        FSize bVar = (FSize) obj;
        if (this.f7117R == bVar.f7117R && this.f7118S == bVar.f7118S) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Float.floatToIntBits(this.f7117R) ^ Float.floatToIntBits(this.f7118S);
    }

    public String toString() {
        return this.f7117R + "x" + this.f7118S;
    }

    public FSize(float f, float f2) {
        this.f7117R = f;
        this.f7118S = f2;
    }

    /* renamed from: a */
    public static FSize m11561a(float f, float f2) {
        FSize a = f7116T.mo23424a();
        a.f7117R = f;
        a.f7118S = f2;
        return a;
    }

    /* renamed from: a */
    public static void m11562a(FSize bVar) {
        f7116T.mo23426a((MoveViewJob) super);
    }
}
