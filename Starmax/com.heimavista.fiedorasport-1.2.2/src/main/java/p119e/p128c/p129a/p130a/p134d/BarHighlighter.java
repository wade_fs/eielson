package p119e.p128c.p129a.p130a.p134d;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.BarLineScatterCandleBubbleData;
import p119e.p128c.p129a.p130a.p135e.p136a.BarDataProvider;
import p119e.p128c.p129a.p130a.p135e.p137b.IBarDataSet;
import p119e.p128c.p129a.p130a.p143j.MPPointD;

/* renamed from: e.c.a.a.d.a */
public class BarHighlighter extends ChartHighlighter<BarDataProvider> {
    public BarHighlighter(BarDataProvider aVar) {
        super(aVar);
    }

    /* renamed from: a */
    public Highlight mo23231a(float f, float f2) {
        Highlight a = super.mo23231a(f, f2);
        if (a == null) {
            return null;
        }
        MPPointD b = mo23238b(f, f2);
        IBarDataSet aVar = (IBarDataSet) ((BarDataProvider) super.f6941a).getBarData().mo13374a(a.mo23245c());
        if (aVar.mo13332t0()) {
            return mo23232a(a, aVar, (float) b.f7120R, (float) b.f7121S);
        }
        MPPointD.m11565a(b);
        return a;
    }

    /* renamed from: a */
    public Highlight mo23232a(Highlight dVar, IBarDataSet aVar, float f, float f2) {
        BarEntry barEntry = (BarEntry) aVar.mo13406a(f, f2);
        if (barEntry == null) {
            return null;
        }
        if (barEntry.mo13308i() == null) {
            return dVar;
        }
        Range[] h = barEntry.mo13307h();
        if (h.length <= 0) {
            return null;
        }
        int a = mo23229a(h, f2);
        MPPointD a2 = ((BarDataProvider) super.f6941a).mo12952a(aVar.mo13364s()).mo23428a(dVar.mo23249g(), h[a].f6957b);
        Highlight dVar2 = new Highlight(barEntry.mo13315d(), barEntry.mo13303c(), (float) a2.f7120R, (float) a2.f7121S, dVar.mo23245c(), a, dVar.mo23240a());
        MPPointD.m11565a(a2);
        return dVar2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo23229a(Range[] jVarArr, float f) {
        if (jVarArr == null || jVarArr.length == 0) {
            return 0;
        }
        int i = 0;
        for (Range jVar : jVarArr) {
            if (jVar.mo23256a(f)) {
                return i;
            }
            i++;
        }
        int max = Math.max(jVarArr.length - 1, 0);
        if (f > jVarArr[max].f6957b) {
            return max;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public float mo23228a(float f, float f2, float f3, float f4) {
        return Math.abs(f - f3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public BarLineScatterCandleBubbleData mo23230a() {
        return ((BarDataProvider) super.f6941a).getBarData();
    }
}
