package p119e.p128c.p129a.p130a.p141i;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import androidx.core.view.ViewCompat;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import java.util.List;
import p119e.p128c.p129a.p130a.p133c.ValueFormatter;
import p119e.p128c.p129a.p130a.p143j.FSize;
import p119e.p128c.p129a.p130a.p143j.MPPointD;
import p119e.p128c.p129a.p130a.p143j.MPPointF;
import p119e.p128c.p129a.p130a.p143j.Transformer;
import p119e.p128c.p129a.p130a.p143j.Utils;
import p119e.p128c.p129a.p130a.p143j.ViewPortHandler;

/* renamed from: e.c.a.a.i.q */
public class XAxisRenderer extends AxisRenderer {

    /* renamed from: h */
    protected XAxis f7091h;

    /* renamed from: i */
    protected Path f7092i = new Path();

    /* renamed from: j */
    protected float[] f7093j = new float[2];

    /* renamed from: k */
    protected RectF f7094k = new RectF();

    /* renamed from: l */
    protected float[] f7095l = new float[2];

    /* renamed from: m */
    protected RectF f7096m = new RectF();

    /* renamed from: n */
    float[] f7097n = new float[4];

    /* renamed from: o */
    private Path f7098o = new Path();

    public XAxisRenderer(ViewPortHandler jVar, XAxis hVar, Transformer gVar) {
        super(jVar, gVar, hVar);
        this.f7091h = hVar;
        super.f7007e.setColor((int) ViewCompat.MEASURED_STATE_MASK);
        super.f7007e.setTextAlign(Paint.Align.CENTER);
        super.f7007e.setTextSize(Utils.m11599a(10.0f));
    }

    /* renamed from: a */
    public void mo23336a(float f, float f2, boolean z) {
        float f3;
        double d;
        if (this.f7088a.mo23469j() > 10.0f && !this.f7088a.mo23484v()) {
            MPPointD b = super.f7005c.mo23440b(this.f7088a.mo23463g(), this.f7088a.mo23467i());
            MPPointD b2 = super.f7005c.mo23440b(this.f7088a.mo23465h(), this.f7088a.mo23467i());
            if (z) {
                f3 = (float) b2.f7120R;
                d = b.f7120R;
            } else {
                f3 = (float) b.f7120R;
                d = b2.f7120R;
            }
            MPPointD.m11565a(b);
            MPPointD.m11565a(b2);
            f = f3;
            f2 = (float) d;
        }
        mo23335a(f, f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo23401b() {
        String p = this.f7091h.mo13219p();
        super.f7007e.setTypeface(this.f7091h.mo13236c());
        super.f7007e.setTextSize(this.f7091h.mo13234b());
        FSize b = Utils.m11618b(super.f7007e, p);
        float f = b.f7117R;
        float a = (float) Utils.m11602a(super.f7007e, "Q");
        FSize a2 = Utils.m11604a(f, a, this.f7091h.mo13285z());
        this.f7091h.f2445J = Math.round(f);
        this.f7091h.f2446K = Math.round(a);
        this.f7091h.f2447L = Math.round(a2.f7117R);
        this.f7091h.f2448M = Math.round(a2.f7118S);
        FSize.m11562a(a2);
        FSize.m11562a(b);
    }

    /* renamed from: c */
    public void mo23404c(Canvas canvas) {
        if (this.f7091h.mo13224u() && this.f7091h.mo13240f()) {
            int save = canvas.save();
            canvas.clipRect(mo23403c());
            if (this.f7093j.length != super.f7004b.f2359n * 2) {
                this.f7093j = new float[(this.f7091h.f2359n * 2)];
            }
            float[] fArr = this.f7093j;
            for (int i = 0; i < fArr.length; i += 2) {
                float[] fArr2 = this.f7091h.f2357l;
                int i2 = i / 2;
                fArr[i] = fArr2[i2];
                fArr[i + 1] = fArr2[i2];
            }
            super.f7005c.mo23442b(fArr);
            mo23405d();
            Path path = this.f7092i;
            path.reset();
            for (int i3 = 0; i3 < fArr.length; i3 += 2) {
                mo23396a(canvas, fArr[i3], fArr[i3 + 1], path);
            }
            canvas.restoreToCount(save);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo23405d() {
        super.f7006d.setColor(this.f7091h.mo13214k());
        super.f7006d.setStrokeWidth(this.f7091h.mo13216m());
        super.f7006d.setPathEffect(this.f7091h.mo13215l());
    }

    /* renamed from: d */
    public void mo23406d(Canvas canvas) {
        List<LimitLine> o = this.f7091h.mo13218o();
        if (o != null && o.size() > 0) {
            float[] fArr = this.f7095l;
            fArr[0] = 0.0f;
            fArr[1] = 0.0f;
            for (int i = 0; i < o.size(); i++) {
                LimitLine gVar = o.get(i);
                if (gVar.mo13240f()) {
                    int save = canvas.save();
                    this.f7096m.set(this.f7088a.mo23476n());
                    this.f7096m.inset(-gVar.mo13280l(), 0.0f);
                    canvas.clipRect(this.f7096m);
                    fArr[0] = gVar.mo13278j();
                    fArr[1] = 0.0f;
                    super.f7005c.mo23442b(fArr);
                    mo23398a(canvas, gVar, fArr);
                    mo23399a(canvas, gVar, fArr, gVar.mo13239e() + 2.0f);
                    canvas.restoreToCount(save);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23335a(float f, float f2) {
        super.mo23335a(f, f2);
        mo23401b();
    }

    /* renamed from: a */
    public void mo23395a(Canvas canvas) {
        if (this.f7091h.mo13240f() && this.f7091h.mo13225v()) {
            float e = this.f7091h.mo13239e();
            super.f7007e.setTypeface(this.f7091h.mo13236c());
            super.f7007e.setTextSize(this.f7091h.mo13234b());
            super.f7007e.setColor(this.f7091h.mo13229a());
            MPPointF a = MPPointF.m11567a(0.0f, 0.0f);
            if (this.f7091h.mo13282A() == XAxis.C1654a.TOP) {
                a.f7123R = 0.5f;
                a.f7124S = 1.0f;
                mo23397a(canvas, this.f7088a.mo23467i() - e, a);
            } else if (this.f7091h.mo13282A() == XAxis.C1654a.TOP_INSIDE) {
                a.f7123R = 0.5f;
                a.f7124S = 1.0f;
                mo23397a(canvas, this.f7088a.mo23467i() + e + ((float) this.f7091h.f2448M), a);
            } else if (this.f7091h.mo13282A() == XAxis.C1654a.BOTTOM) {
                a.f7123R = 0.5f;
                a.f7124S = 0.0f;
                mo23397a(canvas, this.f7088a.mo23459e() + e, a);
            } else if (this.f7091h.mo13282A() == XAxis.C1654a.BOTTOM_INSIDE) {
                a.f7123R = 0.5f;
                a.f7124S = 0.0f;
                mo23397a(canvas, (this.f7088a.mo23459e() - e) - ((float) this.f7091h.f2448M), a);
            } else {
                a.f7123R = 0.5f;
                a.f7124S = 1.0f;
                mo23397a(canvas, this.f7088a.mo23467i() - e, a);
                a.f7123R = 0.5f;
                a.f7124S = 0.0f;
                mo23397a(canvas, this.f7088a.mo23459e() + e, a);
            }
            MPPointF.m11570b(a);
        }
    }

    /* renamed from: b */
    public void mo23402b(Canvas canvas) {
        if (this.f7091h.mo13222s() && this.f7091h.mo13240f()) {
            super.f7008f.setColor(this.f7091h.mo13209g());
            super.f7008f.setStrokeWidth(this.f7091h.mo13212i());
            super.f7008f.setPathEffect(this.f7091h.mo13211h());
            if (this.f7091h.mo13282A() == XAxis.C1654a.TOP || this.f7091h.mo13282A() == XAxis.C1654a.TOP_INSIDE || this.f7091h.mo13282A() == XAxis.C1654a.BOTH_SIDED) {
                canvas.drawLine(this.f7088a.mo23463g(), this.f7088a.mo23467i(), this.f7088a.mo23465h(), this.f7088a.mo23467i(), super.f7008f);
            }
            if (this.f7091h.mo13282A() == XAxis.C1654a.BOTTOM || this.f7091h.mo13282A() == XAxis.C1654a.BOTTOM_INSIDE || this.f7091h.mo13282A() == XAxis.C1654a.BOTH_SIDED) {
                canvas.drawLine(this.f7088a.mo23463g(), this.f7088a.mo23459e(), this.f7088a.mo23465h(), this.f7088a.mo23459e(), super.f7008f);
            }
        }
    }

    /* renamed from: c */
    public RectF mo23403c() {
        this.f7094k.set(this.f7088a.mo23476n());
        this.f7094k.inset(-super.f7004b.mo13216m(), 0.0f);
        return this.f7094k;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23397a(Canvas canvas, float f, MPPointF eVar) {
        float z = this.f7091h.mo13285z();
        boolean r = this.f7091h.mo13221r();
        float[] fArr = new float[(this.f7091h.f2359n * 2)];
        for (int i = 0; i < fArr.length; i += 2) {
            if (r) {
                fArr[i] = this.f7091h.f2358m[i / 2];
            } else {
                fArr[i] = this.f7091h.f2357l[i / 2];
            }
        }
        super.f7005c.mo23442b(fArr);
        for (int i2 = 0; i2 < fArr.length; i2 += 2) {
            float f2 = fArr[i2];
            if (this.f7088a.mo23460e(f2)) {
                ValueFormatter q = this.f7091h.mo13220q();
                XAxis hVar = this.f7091h;
                int i3 = i2 / 2;
                String a = q.mo23221a(hVar.f2357l[i3], hVar);
                if (this.f7091h.mo13283B()) {
                    int i4 = this.f7091h.f2359n;
                    if (i3 == i4 - 1 && i4 > 1) {
                        float c = (float) Utils.m11621c(super.f7007e, a);
                        if (c > this.f7088a.mo23488z() * 2.0f && f2 + c > this.f7088a.mo23473l()) {
                            f2 -= c / 2.0f;
                        }
                    } else if (i2 == 0) {
                        f2 += ((float) Utils.m11621c(super.f7007e, a)) / 2.0f;
                    }
                }
                mo23400a(canvas, a, f2, f, eVar, z);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23400a(Canvas canvas, String str, float f, float f2, MPPointF eVar, float f3) {
        Utils.m11607a(canvas, str, f, f2, super.f7007e, eVar, f3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23396a(Canvas canvas, float f, float f2, Path path) {
        path.moveTo(f, this.f7088a.mo23459e());
        path.lineTo(f, this.f7088a.mo23467i());
        canvas.drawPath(path, super.f7006d);
        path.reset();
    }

    /* renamed from: a */
    public void mo23398a(Canvas canvas, LimitLine gVar, float[] fArr) {
        float[] fArr2 = this.f7097n;
        fArr2[0] = fArr[0];
        fArr2[1] = this.f7088a.mo23467i();
        float[] fArr3 = this.f7097n;
        fArr3[2] = fArr[0];
        fArr3[3] = this.f7088a.mo23459e();
        this.f7098o.reset();
        Path path = this.f7098o;
        float[] fArr4 = this.f7097n;
        path.moveTo(fArr4[0], fArr4[1]);
        Path path2 = this.f7098o;
        float[] fArr5 = this.f7097n;
        path2.lineTo(fArr5[2], fArr5[3]);
        super.f7009g.setStyle(Paint.Style.STROKE);
        super.f7009g.setColor(gVar.mo13279k());
        super.f7009g.setStrokeWidth(gVar.mo13280l());
        super.f7009g.setPathEffect(gVar.mo13275g());
        canvas.drawPath(this.f7098o, super.f7009g);
    }

    /* renamed from: a */
    public void mo23399a(Canvas canvas, LimitLine gVar, float[] fArr, float f) {
        String h = gVar.mo13276h();
        if (h != null && !h.equals("")) {
            super.f7009g.setStyle(gVar.mo13281m());
            super.f7009g.setPathEffect(null);
            super.f7009g.setColor(gVar.mo13229a());
            super.f7009g.setStrokeWidth(0.5f);
            super.f7009g.setTextSize(gVar.mo13234b());
            float l = gVar.mo13280l() + gVar.mo13238d();
            LimitLine.C1653a i = gVar.mo13277i();
            if (i == LimitLine.C1653a.RIGHT_TOP) {
                super.f7009g.setTextAlign(Paint.Align.LEFT);
                canvas.drawText(h, fArr[0] + l, this.f7088a.mo23467i() + f + ((float) Utils.m11602a(super.f7009g, h)), super.f7009g);
            } else if (i == LimitLine.C1653a.RIGHT_BOTTOM) {
                super.f7009g.setTextAlign(Paint.Align.LEFT);
                canvas.drawText(h, fArr[0] + l, this.f7088a.mo23459e() - f, super.f7009g);
            } else if (i == LimitLine.C1653a.LEFT_TOP) {
                super.f7009g.setTextAlign(Paint.Align.RIGHT);
                canvas.drawText(h, fArr[0] - l, this.f7088a.mo23467i() + f + ((float) Utils.m11602a(super.f7009g, h)), super.f7009g);
            } else {
                super.f7009g.setTextAlign(Paint.Align.RIGHT);
                canvas.drawText(h, fArr[0] - l, this.f7088a.mo23459e() - f, super.f7009g);
            }
        }
    }
}
