package p119e.p120a.p121a.p122a;

import android.os.Bundle;

/* renamed from: e.a.a.a.d */
public class ReferrerDetails {

    /* renamed from: a */
    private final Bundle f6845a;

    public ReferrerDetails(Bundle bundle) {
        this.f6845a = bundle;
    }

    /* renamed from: a */
    public String mo23151a() {
        return this.f6845a.getString("install_referrer");
    }
}
