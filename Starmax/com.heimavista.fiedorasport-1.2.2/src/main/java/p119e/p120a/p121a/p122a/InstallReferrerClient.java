package p119e.p120a.p121a.p122a;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.UiThread;

/* renamed from: e.a.a.a.a */
public abstract class InstallReferrerClient {

    /* renamed from: e.a.a.a.a$b */
    /* compiled from: InstallReferrerClient */
    public static final class C3823b {

        /* renamed from: a */
        private final Context f6838a;

        @UiThread
        /* renamed from: a */
        public InstallReferrerClient mo23147a() {
            Context context = this.f6838a;
            if (context != null) {
                return new InstallReferrerClientImpl(context);
            }
            throw new IllegalArgumentException("Please provide a valid Context.");
        }

        private C3823b(Context context) {
            this.f6838a = context;
        }
    }

    @UiThread
    /* renamed from: a */
    public static C3823b m11057a(@NonNull Context context) {
        return new C3823b(context);
    }

    @UiThread
    /* renamed from: a */
    public abstract ReferrerDetails mo23145a();

    @UiThread
    /* renamed from: a */
    public abstract void mo23146a(@NonNull InstallReferrerStateListener cVar);
}
