package p119e.p120a.p121a.p123b;

import android.util.Log;

/* renamed from: e.a.a.b.a */
public final class InstallReferrerCommons {
    /* renamed from: a */
    public static void m11068a(String str, String str2) {
        if (Log.isLoggable(str, 2)) {
            Log.v(str, str2);
        }
    }

    /* renamed from: b */
    public static void m11069b(String str, String str2) {
        if (Log.isLoggable(str, 5)) {
            Log.w(str, str2);
        }
    }
}
