package p119e.p120a.p121a.p122a;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import androidx.annotation.NonNull;
import java.util.List;
import p119e.p120a.p121a.p123b.InstallReferrerCommons;
import p119e.p144d.p145a.p155b.p156a.IGetInstallReferrerService;

/* renamed from: e.a.a.a.b */
class InstallReferrerClientImpl extends InstallReferrerClient {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f6839a = 0;

    /* renamed from: b */
    private final Context f6840b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public IGetInstallReferrerService f6841c;

    /* renamed from: d */
    private ServiceConnection f6842d;

    /* renamed from: e.a.a.a.b$b */
    /* compiled from: InstallReferrerClientImpl */
    private final class C3825b implements ServiceConnection {

        /* renamed from: P */
        private final InstallReferrerStateListener f6843P;

        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            InstallReferrerCommons.m11068a("InstallReferrerClient", "Install Referrer service connected.");
            IGetInstallReferrerService unused = InstallReferrerClientImpl.this.f6841c = IGetInstallReferrerService.C3978a.m11942a(iBinder);
            int unused2 = InstallReferrerClientImpl.this.f6839a = 2;
            this.f6843P.onInstallReferrerSetupFinished(0);
        }

        public void onServiceDisconnected(ComponentName componentName) {
            InstallReferrerCommons.m11069b("InstallReferrerClient", "Install Referrer service disconnected.");
            IGetInstallReferrerService unused = InstallReferrerClientImpl.this.f6841c = (IGetInstallReferrerService) null;
            int unused2 = InstallReferrerClientImpl.this.f6839a = 0;
            this.f6843P.onInstallReferrerServiceDisconnected();
        }

        private C3825b(@NonNull InstallReferrerStateListener cVar) {
            if (cVar != null) {
                this.f6843P = cVar;
                return;
            }
            throw new RuntimeException("Please specify a listener to know when setup is done.");
        }
    }

    public InstallReferrerClientImpl(@NonNull Context context) {
        this.f6840b = context.getApplicationContext();
    }

    /* renamed from: c */
    private boolean m11063c() {
        try {
            if (this.f6840b.getPackageManager().getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }

    /* renamed from: b */
    public boolean mo23148b() {
        return (this.f6839a != 2 || this.f6841c == null || this.f6842d == null) ? false : true;
    }

    /* renamed from: a */
    public void mo23146a(@NonNull InstallReferrerStateListener cVar) {
        ResolveInfo resolveInfo;
        ServiceInfo serviceInfo;
        if (mo23148b()) {
            InstallReferrerCommons.m11068a("InstallReferrerClient", "Service connection is valid. No need to re-initialize.");
            cVar.onInstallReferrerSetupFinished(0);
            return;
        }
        int i = this.f6839a;
        if (i == 1) {
            InstallReferrerCommons.m11069b("InstallReferrerClient", "Client is already in the process of connecting to the service.");
            cVar.onInstallReferrerSetupFinished(3);
        } else if (i == 3) {
            InstallReferrerCommons.m11069b("InstallReferrerClient", "Client was already closed and can't be reused. Please create another instance.");
            cVar.onInstallReferrerSetupFinished(3);
        } else {
            InstallReferrerCommons.m11068a("InstallReferrerClient", "Starting install referrer service setup.");
            this.f6842d = new C3825b(cVar);
            Intent intent = new Intent("com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE");
            intent.setComponent(new ComponentName("com.android.vending", "com.google.android.finsky.externalreferrer.GetInstallReferrerService"));
            List<ResolveInfo> queryIntentServices = this.f6840b.getPackageManager().queryIntentServices(intent, 0);
            if (queryIntentServices == null || queryIntentServices.isEmpty() || (serviceInfo = (resolveInfo = queryIntentServices.get(0)).serviceInfo) == null) {
                this.f6839a = 0;
                InstallReferrerCommons.m11068a("InstallReferrerClient", "Install Referrer service unavailable on device.");
                cVar.onInstallReferrerSetupFinished(2);
                return;
            }
            String str = serviceInfo.packageName;
            String str2 = resolveInfo.serviceInfo.name;
            if (!"com.android.vending".equals(str) || str2 == null || !m11063c()) {
                InstallReferrerCommons.m11069b("InstallReferrerClient", "Play Store missing or incompatible. Version 8.3.73 or later required.");
                this.f6839a = 0;
                cVar.onInstallReferrerSetupFinished(2);
            } else if (this.f6840b.bindService(new Intent(intent), this.f6842d, 1)) {
                InstallReferrerCommons.m11068a("InstallReferrerClient", "Service was bonded successfully.");
            } else {
                InstallReferrerCommons.m11069b("InstallReferrerClient", "Connection to service is blocked.");
                this.f6839a = 0;
                cVar.onInstallReferrerSetupFinished(1);
            }
        }
    }

    /* renamed from: a */
    public ReferrerDetails mo23145a() {
        if (mo23148b()) {
            Bundle bundle = new Bundle();
            bundle.putString("package_name", this.f6840b.getPackageName());
            try {
                return new ReferrerDetails(this.f6841c.mo23605e(bundle));
            } catch (RemoteException e) {
                InstallReferrerCommons.m11069b("InstallReferrerClient", "RemoteException getting install referrer information");
                this.f6839a = 0;
                throw e;
            }
        } else {
            throw new IllegalStateException("Service not connected. Please start a connection before using the service.");
        }
    }
}
