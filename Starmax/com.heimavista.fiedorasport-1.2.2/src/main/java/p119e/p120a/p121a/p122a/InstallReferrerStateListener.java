package p119e.p120a.p121a.p122a;

/* renamed from: e.a.a.a.c */
public interface InstallReferrerStateListener {
    void onInstallReferrerServiceDisconnected();

    void onInstallReferrerSetupFinished(int i);
}
