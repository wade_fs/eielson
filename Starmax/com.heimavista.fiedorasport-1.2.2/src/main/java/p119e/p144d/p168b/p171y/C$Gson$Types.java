package p119e.p144d.p168b.p171y;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.lang.reflect.WildcardType;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Properties;

/* renamed from: e.d.b.y.b  reason: invalid class name */
public final class C$Gson$Types {

    /* renamed from: a */
    static final Type[] f7535a = new Type[0];

    /* renamed from: e.d.b.y.b$a */
    /* compiled from: $Gson$Types */
    private static final class C4103a implements GenericArrayType, Serializable {
        private static final long serialVersionUID = 0;

        /* renamed from: P */
        private final Type f7536P;

        public C4103a(Type type) {
            this.f7536P = C$Gson$Types.m12350b(type);
        }

        public boolean equals(Object obj) {
            return (obj instanceof GenericArrayType) && C$Gson$Types.m12349a(this, (GenericArrayType) obj);
        }

        public Type getGenericComponentType() {
            return this.f7536P;
        }

        public int hashCode() {
            return this.f7536P.hashCode();
        }

        public String toString() {
            return C$Gson$Types.m12358h(this.f7536P) + "[]";
        }
    }

    /* renamed from: e.d.b.y.b$b */
    /* compiled from: $Gson$Types */
    private static final class C4104b implements ParameterizedType, Serializable {
        private static final long serialVersionUID = 0;

        /* renamed from: P */
        private final Type f7537P;

        /* renamed from: Q */
        private final Type f7538Q;

        /* renamed from: R */
        private final Type[] f7539R;

        public C4104b(Type type, Type type2, Type... typeArr) {
            Type type3;
            if (type2 instanceof Class) {
                Class cls = (Class) type2;
                boolean z = true;
                boolean z2 = Modifier.isStatic(cls.getModifiers()) || cls.getEnclosingClass() == null;
                if (type == null && !z2) {
                    z = false;
                }
                C$Gson$Preconditions.m12337a(z);
            }
            if (type == null) {
                type3 = null;
            } else {
                type3 = C$Gson$Types.m12350b(type);
            }
            this.f7537P = type3;
            this.f7538Q = C$Gson$Types.m12350b(type2);
            this.f7539R = (Type[]) typeArr.clone();
            int length = this.f7539R.length;
            for (int i = 0; i < length; i++) {
                C$Gson$Preconditions.m12336a(this.f7539R[i]);
                C$Gson$Types.m12353c(this.f7539R[i]);
                Type[] typeArr2 = this.f7539R;
                typeArr2[i] = C$Gson$Types.m12350b(typeArr2[i]);
            }
        }

        public boolean equals(Object obj) {
            return (obj instanceof ParameterizedType) && C$Gson$Types.m12349a(this, (ParameterizedType) obj);
        }

        public Type[] getActualTypeArguments() {
            return (Type[]) this.f7539R.clone();
        }

        public Type getOwnerType() {
            return this.f7537P;
        }

        public Type getRawType() {
            return this.f7538Q;
        }

        public int hashCode() {
            return (Arrays.hashCode(this.f7539R) ^ this.f7538Q.hashCode()) ^ C$Gson$Types.m12338a((Object) this.f7537P);
        }

        public String toString() {
            int length = this.f7539R.length;
            if (length == 0) {
                return C$Gson$Types.m12358h(this.f7538Q);
            }
            StringBuilder sb = new StringBuilder((length + 1) * 30);
            sb.append(C$Gson$Types.m12358h(this.f7538Q));
            sb.append("<");
            sb.append(C$Gson$Types.m12358h(this.f7539R[0]));
            for (int i = 1; i < length; i++) {
                sb.append(", ");
                sb.append(C$Gson$Types.m12358h(this.f7539R[i]));
            }
            sb.append(">");
            return sb.toString();
        }
    }

    /* renamed from: e.d.b.y.b$c */
    /* compiled from: $Gson$Types */
    private static final class C4105c implements WildcardType, Serializable {
        private static final long serialVersionUID = 0;

        /* renamed from: P */
        private final Type f7540P;

        /* renamed from: Q */
        private final Type f7541Q;

        public C4105c(Type[] typeArr, Type[] typeArr2) {
            Class<Object> cls = Object.class;
            boolean z = true;
            C$Gson$Preconditions.m12337a(typeArr2.length <= 1);
            C$Gson$Preconditions.m12337a(typeArr.length == 1);
            if (typeArr2.length == 1) {
                C$Gson$Preconditions.m12336a(typeArr2[0]);
                C$Gson$Types.m12353c(typeArr2[0]);
                C$Gson$Preconditions.m12337a(typeArr[0] != cls ? false : z);
                this.f7541Q = C$Gson$Types.m12350b(typeArr2[0]);
                this.f7540P = cls;
                return;
            }
            C$Gson$Preconditions.m12336a(typeArr[0]);
            C$Gson$Types.m12353c(typeArr[0]);
            this.f7541Q = null;
            this.f7540P = C$Gson$Types.m12350b(typeArr[0]);
        }

        public boolean equals(Object obj) {
            return (obj instanceof WildcardType) && C$Gson$Types.m12349a(this, (WildcardType) obj);
        }

        public Type[] getLowerBounds() {
            Type type = this.f7541Q;
            if (type == null) {
                return C$Gson$Types.f7535a;
            }
            return new Type[]{type};
        }

        public Type[] getUpperBounds() {
            return new Type[]{this.f7540P};
        }

        public int hashCode() {
            Type type = this.f7541Q;
            return (type != null ? type.hashCode() + 31 : 1) ^ (this.f7540P.hashCode() + 31);
        }

        public String toString() {
            if (this.f7541Q != null) {
                return "? super " + C$Gson$Types.m12358h(this.f7541Q);
            } else if (this.f7540P == Object.class) {
                return "?";
            } else {
                return "? extends " + C$Gson$Types.m12358h(this.f7540P);
            }
        }
    }

    /* renamed from: a */
    public static ParameterizedType m12342a(Type type, Type type2, Type... typeArr) {
        return new C4104b(type, type2, typeArr);
    }

    /* renamed from: b */
    public static Type m12350b(Type type) {
        if (type instanceof Class) {
            Class cls = (Class) type;
            return cls.isArray() ? new C4103a(m12350b(cls.getComponentType())) : cls;
        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            return new C4104b(parameterizedType.getOwnerType(), parameterizedType.getRawType(), parameterizedType.getActualTypeArguments());
        } else if (type instanceof GenericArrayType) {
            return new C4103a(((GenericArrayType) type).getGenericComponentType());
        } else {
            if (!(type instanceof WildcardType)) {
                return type;
            }
            WildcardType wildcardType = (WildcardType) type;
            return new C4105c(wildcardType.getUpperBounds(), wildcardType.getLowerBounds());
        }
    }

    /* renamed from: c */
    static void m12353c(Type type) {
        C$Gson$Preconditions.m12337a(!(type instanceof Class) || !((Class) type).isPrimitive());
    }

    /* renamed from: d */
    public static Type m12354d(Type type) {
        if (type instanceof GenericArrayType) {
            return ((GenericArrayType) type).getGenericComponentType();
        }
        return ((Class) type).getComponentType();
    }

    /* renamed from: e */
    public static Class<?> m12355e(Type type) {
        String str;
        if (type instanceof Class) {
            return (Class) type;
        }
        if (type instanceof ParameterizedType) {
            Type rawType = ((ParameterizedType) type).getRawType();
            C$Gson$Preconditions.m12337a(rawType instanceof Class);
            return (Class) rawType;
        } else if (type instanceof GenericArrayType) {
            return Array.newInstance(m12355e(((GenericArrayType) type).getGenericComponentType()), 0).getClass();
        } else {
            if (type instanceof TypeVariable) {
                return Object.class;
            }
            if (type instanceof WildcardType) {
                return m12355e(((WildcardType) type).getUpperBounds()[0]);
            }
            if (type == null) {
                str = "null";
            } else {
                str = type.getClass().getName();
            }
            throw new IllegalArgumentException("Expected a Class, ParameterizedType, or GenericArrayType, but <" + type + "> is of type " + str);
        }
    }

    /* renamed from: f */
    public static WildcardType m12356f(Type type) {
        Type[] typeArr;
        if (type instanceof WildcardType) {
            typeArr = ((WildcardType) type).getUpperBounds();
        } else {
            typeArr = new Type[]{type};
        }
        return new C4105c(typeArr, f7535a);
    }

    /* renamed from: g */
    public static WildcardType m12357g(Type type) {
        Type[] typeArr;
        if (type instanceof WildcardType) {
            typeArr = ((WildcardType) type).getLowerBounds();
        } else {
            typeArr = new Type[]{type};
        }
        return new C4105c(new Type[]{Object.class}, typeArr);
    }

    /* renamed from: h */
    public static String m12358h(Type type) {
        return type instanceof Class ? ((Class) type).getName() : type.toString();
    }

    /* renamed from: a */
    public static GenericArrayType m12341a(Type type) {
        return new C4103a(type);
    }

    /* renamed from: a */
    static boolean m12348a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.b.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.reflect.Type, java.lang.reflect.Type]
     candidates:
      e.d.b.y.b.a(java.lang.Object[], java.lang.Object):int
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      e.d.b.y.b.a(java.lang.Object, java.lang.Object):boolean */
    /* renamed from: a */
    public static boolean m12349a(Type type, Type type2) {
        if (type == type2) {
            return true;
        }
        if (type instanceof Class) {
            return type.equals(type2);
        }
        if (type instanceof ParameterizedType) {
            if (!(type2 instanceof ParameterizedType)) {
                return false;
            }
            ParameterizedType parameterizedType = (ParameterizedType) type;
            ParameterizedType parameterizedType2 = (ParameterizedType) type2;
            if (!m12348a((Object) parameterizedType.getOwnerType(), (Object) parameterizedType2.getOwnerType()) || !parameterizedType.getRawType().equals(parameterizedType2.getRawType()) || !Arrays.equals(parameterizedType.getActualTypeArguments(), parameterizedType2.getActualTypeArguments())) {
                return false;
            }
            return true;
        } else if (type instanceof GenericArrayType) {
            if (!(type2 instanceof GenericArrayType)) {
                return false;
            }
            return m12349a(((GenericArrayType) type).getGenericComponentType(), ((GenericArrayType) type2).getGenericComponentType());
        } else if (type instanceof WildcardType) {
            if (!(type2 instanceof WildcardType)) {
                return false;
            }
            WildcardType wildcardType = (WildcardType) type;
            WildcardType wildcardType2 = (WildcardType) type2;
            if (!Arrays.equals(wildcardType.getUpperBounds(), wildcardType2.getUpperBounds()) || !Arrays.equals(wildcardType.getLowerBounds(), wildcardType2.getLowerBounds())) {
                return false;
            }
            return true;
        } else if (!(type instanceof TypeVariable) || !(type2 instanceof TypeVariable)) {
            return false;
        } else {
            TypeVariable typeVariable = (TypeVariable) type;
            TypeVariable typeVariable2 = (TypeVariable) type2;
            if (typeVariable.getGenericDeclaration() != typeVariable2.getGenericDeclaration() || !typeVariable.getName().equals(typeVariable2.getName())) {
                return false;
            }
            return true;
        }
    }

    /* renamed from: b */
    static Type m12351b(Type type, Class<?> cls, Class<?> cls2) {
        if (type instanceof WildcardType) {
            type = ((WildcardType) type).getUpperBounds()[0];
        }
        C$Gson$Preconditions.m12337a(cls2.isAssignableFrom(cls));
        return m12345a(type, cls, m12344a(type, cls, cls2));
    }

    /* renamed from: b */
    public static Type[] m12352b(Type type, Class<?> cls) {
        Class<Object> cls2 = Object.class;
        if (type == Properties.class) {
            return new Type[]{String.class, String.class};
        }
        Type b = m12351b(type, cls, Map.class);
        if (b instanceof ParameterizedType) {
            return ((ParameterizedType) b).getActualTypeArguments();
        }
        return new Type[]{cls2, cls2};
    }

    /* renamed from: a */
    static int m12338a(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super ?>, java.lang.Class<?>]
     candidates:
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.reflect.Type, java.lang.reflect.Type[]):java.lang.reflect.ParameterizedType
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.Type):java.lang.reflect.Type
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.reflect.TypeVariable<?>):java.lang.reflect.Type
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>, java.lang.Class<?>):java.lang.reflect.Type */
    /* renamed from: a */
    static Type m12344a(Type type, Class<?> cls, Class<?> cls2) {
        if (cls2 == cls) {
            return type;
        }
        if (cls2.isInterface()) {
            Class<?>[] interfaces = cls.getInterfaces();
            int length = interfaces.length;
            for (int i = 0; i < length; i++) {
                if (interfaces[i] == cls2) {
                    return cls.getGenericInterfaces()[i];
                }
                if (cls2.isAssignableFrom(interfaces[i])) {
                    return m12344a(cls.getGenericInterfaces()[i], interfaces[i], cls2);
                }
            }
        }
        if (!cls.isInterface()) {
            while (cls != Object.class) {
                Class<? super Object> superclass = cls.getSuperclass();
                if (superclass == cls2) {
                    return cls.getGenericSuperclass();
                }
                if (cls2.isAssignableFrom(superclass)) {
                    return m12344a(cls.getGenericSuperclass(), (Class<?>) superclass, cls2);
                }
                cls = superclass;
            }
        }
        return cls2;
    }

    /* renamed from: a */
    public static Type m12343a(Type type, Class<?> cls) {
        Type b = m12351b(type, cls, Collection.class);
        if (b instanceof WildcardType) {
            b = ((WildcardType) b).getUpperBounds()[0];
        }
        if (b instanceof ParameterizedType) {
            return ((ParameterizedType) b).getActualTypeArguments()[0];
        }
        return Object.class;
    }

    /* renamed from: a */
    public static Type m12345a(Type type, Class<?> cls, Type type2) {
        return m12346a(type, cls, type2, new HashSet());
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:59:? */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v13, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.lang.reflect.Type[]} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v11, resolved type: java.lang.reflect.WildcardType} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: java.lang.reflect.WildcardType} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v13, resolved type: java.lang.reflect.WildcardType} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.reflect.Type m12346a(java.lang.reflect.Type r8, java.lang.Class<?> r9, java.lang.reflect.Type r10, java.util.Collection<java.lang.reflect.TypeVariable> r11) {
        /*
        L_0x0000:
            boolean r0 = r10 instanceof java.lang.reflect.TypeVariable
            if (r0 == 0) goto L_0x0018
            r0 = r10
            java.lang.reflect.TypeVariable r0 = (java.lang.reflect.TypeVariable) r0
            boolean r1 = r11.contains(r0)
            if (r1 == 0) goto L_0x000e
            return r10
        L_0x000e:
            r11.add(r0)
            java.lang.reflect.Type r10 = m12347a(r8, r9, r0)
            if (r10 != r0) goto L_0x0000
            return r10
        L_0x0018:
            boolean r0 = r10 instanceof java.lang.Class
            if (r0 == 0) goto L_0x0035
            r0 = r10
            java.lang.Class r0 = (java.lang.Class) r0
            boolean r1 = r0.isArray()
            if (r1 == 0) goto L_0x0035
            java.lang.Class r10 = r0.getComponentType()
            java.lang.reflect.Type r8 = m12346a(r8, r9, r10, r11)
            if (r10 != r8) goto L_0x0030
            goto L_0x0034
        L_0x0030:
            java.lang.reflect.GenericArrayType r0 = m12341a(r8)
        L_0x0034:
            return r0
        L_0x0035:
            boolean r0 = r10 instanceof java.lang.reflect.GenericArrayType
            if (r0 == 0) goto L_0x004b
            java.lang.reflect.GenericArrayType r10 = (java.lang.reflect.GenericArrayType) r10
            java.lang.reflect.Type r0 = r10.getGenericComponentType()
            java.lang.reflect.Type r8 = m12346a(r8, r9, r0, r11)
            if (r0 != r8) goto L_0x0046
            goto L_0x004a
        L_0x0046:
            java.lang.reflect.GenericArrayType r10 = m12341a(r8)
        L_0x004a:
            return r10
        L_0x004b:
            boolean r0 = r10 instanceof java.lang.reflect.ParameterizedType
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x008b
            java.lang.reflect.ParameterizedType r10 = (java.lang.reflect.ParameterizedType) r10
            java.lang.reflect.Type r0 = r10.getOwnerType()
            java.lang.reflect.Type r3 = m12346a(r8, r9, r0, r11)
            if (r3 == r0) goto L_0x005f
            r0 = 1
            goto L_0x0060
        L_0x005f:
            r0 = 0
        L_0x0060:
            java.lang.reflect.Type[] r4 = r10.getActualTypeArguments()
            int r5 = r4.length
        L_0x0065:
            if (r2 >= r5) goto L_0x0080
            r6 = r4[r2]
            java.lang.reflect.Type r6 = m12346a(r8, r9, r6, r11)
            r7 = r4[r2]
            if (r6 == r7) goto L_0x007d
            if (r0 != 0) goto L_0x007b
            java.lang.Object r0 = r4.clone()
            r4 = r0
            java.lang.reflect.Type[] r4 = (java.lang.reflect.Type[]) r4
            r0 = 1
        L_0x007b:
            r4[r2] = r6
        L_0x007d:
            int r2 = r2 + 1
            goto L_0x0065
        L_0x0080:
            if (r0 == 0) goto L_0x008a
            java.lang.reflect.Type r8 = r10.getRawType()
            java.lang.reflect.ParameterizedType r10 = m12342a(r3, r8, r4)
        L_0x008a:
            return r10
        L_0x008b:
            boolean r0 = r10 instanceof java.lang.reflect.WildcardType
            if (r0 == 0) goto L_0x00bd
            java.lang.reflect.WildcardType r10 = (java.lang.reflect.WildcardType) r10
            java.lang.reflect.Type[] r0 = r10.getLowerBounds()
            java.lang.reflect.Type[] r3 = r10.getUpperBounds()
            int r4 = r0.length
            if (r4 != r1) goto L_0x00ab
            r1 = r0[r2]
            java.lang.reflect.Type r8 = m12346a(r8, r9, r1, r11)
            r9 = r0[r2]
            if (r8 == r9) goto L_0x00bd
            java.lang.reflect.WildcardType r8 = m12357g(r8)
            return r8
        L_0x00ab:
            int r0 = r3.length
            if (r0 != r1) goto L_0x00bd
            r0 = r3[r2]
            java.lang.reflect.Type r8 = m12346a(r8, r9, r0, r11)     // Catch:{ all -> 0x00be }
            r9 = r3[r2]
            if (r8 == r9) goto L_0x00bd
            java.lang.reflect.WildcardType r8 = m12356f(r8)
            return r8
        L_0x00bd:
            return r10
        L_0x00be:
            r8 = move-exception
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.C$Gson$Types.m12346a(java.lang.reflect.Type, java.lang.Class, java.lang.reflect.Type, java.util.Collection):java.lang.reflect.Type");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.b.a(java.lang.Object[], java.lang.Object):int
     arg types: [java.lang.reflect.TypeVariable[], java.lang.reflect.TypeVariable<?>]
     candidates:
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
      e.d.b.y.b.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      e.d.b.y.b.a(java.lang.Object[], java.lang.Object):int */
    /* renamed from: a */
    static Type m12347a(Type type, Class<?> cls, TypeVariable<?> typeVariable) {
        Class<?> a = m12340a(typeVariable);
        if (a == null) {
            return typeVariable;
        }
        Type a2 = m12344a(type, cls, a);
        if (!(a2 instanceof ParameterizedType)) {
            return typeVariable;
        }
        return ((ParameterizedType) a2).getActualTypeArguments()[m12339a((Object[]) a.getTypeParameters(), (Object) typeVariable)];
    }

    /* renamed from: a */
    private static int m12339a(Object[] objArr, Object obj) {
        int length = objArr.length;
        for (int i = 0; i < length; i++) {
            if (obj.equals(objArr[i])) {
                return i;
            }
        }
        throw new NoSuchElementException();
    }

    /* renamed from: a */
    private static Class<?> m12340a(TypeVariable<?> typeVariable) {
        Object genericDeclaration = typeVariable.getGenericDeclaration();
        if (genericDeclaration instanceof Class) {
            return (Class) genericDeclaration;
        }
        return null;
    }
}
