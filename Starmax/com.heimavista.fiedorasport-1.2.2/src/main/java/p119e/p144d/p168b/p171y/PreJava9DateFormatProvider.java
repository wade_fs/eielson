package p119e.p144d.p168b.p171y;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

/* renamed from: e.d.b.y.j */
public class PreJava9DateFormatProvider {
    /* renamed from: a */
    public static DateFormat m12414a(int i, int i2) {
        return new SimpleDateFormat(m12413a(i) + " " + m12415b(i2), Locale.US);
    }

    /* renamed from: b */
    private static String m12415b(int i) {
        if (i == 0 || i == 1) {
            return "h:mm:ss a z";
        }
        if (i == 2) {
            return "h:mm:ss a";
        }
        if (i == 3) {
            return "h:mm a";
        }
        throw new IllegalArgumentException("Unknown DateFormat style: " + i);
    }

    /* renamed from: a */
    private static String m12413a(int i) {
        if (i == 0) {
            return "EEEE, MMMM d, yyyy";
        }
        if (i == 1) {
            return "MMMM d, yyyy";
        }
        if (i == 2) {
            return "MMM d, yyyy";
        }
        if (i == 3) {
            return "M/d/yy";
        }
        throw new IllegalArgumentException("Unknown DateFormat style: " + i);
    }
}
