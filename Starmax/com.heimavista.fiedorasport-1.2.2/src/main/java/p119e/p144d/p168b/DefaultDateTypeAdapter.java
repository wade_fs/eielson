package p119e.p144d.p168b;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.JavaVersion;
import p119e.p144d.p168b.p171y.PreJava9DateFormatProvider;
import p119e.p144d.p168b.p171y.p172n.p173o.ISO8601Utils;

/* renamed from: e.d.b.a */
final class DefaultDateTypeAdapter extends TypeAdapter<Date> {

    /* renamed from: a */
    private final Class<? extends Date> f7450a;

    /* renamed from: b */
    private final List<DateFormat> f7451b = new ArrayList();

    DefaultDateTypeAdapter(Class<? extends Date> cls, String str) {
        m12175a(cls);
        this.f7450a = cls;
        this.f7451b.add(new SimpleDateFormat(str, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.f7451b.add(new SimpleDateFormat(str));
        }
    }

    public String toString() {
        DateFormat dateFormat = this.f7451b.get(0);
        if (dateFormat instanceof SimpleDateFormat) {
            return "DefaultDateTypeAdapter(" + ((SimpleDateFormat) dateFormat).toPattern() + ')';
        }
        return "DefaultDateTypeAdapter(" + dateFormat.getClass().getSimpleName() + ')';
    }

    /* renamed from: a */
    private static Class<? extends Date> m12175a(Class<? extends Date> cls) {
        if (cls == Date.class || cls == java.sql.Date.class || cls == Timestamp.class) {
            return cls;
        }
        throw new IllegalArgumentException("Date type must be one of " + Date.class + ", " + Timestamp.class + ", or " + java.sql.Date.class + " but was " + cls);
    }

    /* renamed from: a */
    public void mo23736a(JsonWriter cVar, Date date) {
        if (date == null) {
            cVar.mo23780u();
            return;
        }
        synchronized (this.f7451b) {
            cVar.mo23773d(this.f7451b.get(0).format(date));
        }
    }

    public DefaultDateTypeAdapter(Class<? extends Date> cls, int i, int i2) {
        m12175a(cls);
        this.f7450a = cls;
        this.f7451b.add(DateFormat.getDateTimeInstance(i, i2, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.f7451b.add(DateFormat.getDateTimeInstance(i, i2));
        }
        if (JavaVersion.m12396c()) {
            this.f7451b.add(PreJava9DateFormatProvider.m12414a(i, i2));
        }
    }

    /* renamed from: a */
    public Date mo23735a(JsonReader aVar) {
        if (aVar.mo23742D() == JsonToken.NULL) {
            aVar.mo23740B();
            return null;
        }
        Date a = m12176a(aVar.mo23741C());
        Class<? extends Date> cls = this.f7450a;
        if (cls == Date.class) {
            return a;
        }
        if (cls == Timestamp.class) {
            return new Timestamp(a.getTime());
        }
        if (cls == java.sql.Date.class) {
            return new java.sql.Date(a.getTime());
        }
        throw new AssertionError();
    }

    /* renamed from: a */
    private Date m12176a(String str) {
        synchronized (this.f7451b) {
            for (DateFormat dateFormat : this.f7451b) {
                try {
                    Date parse = dateFormat.parse(str);
                    return parse;
                } catch (ParseException unused) {
                }
            }
            try {
                Date a = ISO8601Utils.m12653a(str, new ParsePosition(0));
                return a;
            } catch (ParseException e) {
                throw new JsonSyntaxException(str, e);
            }
        }
    }
}
