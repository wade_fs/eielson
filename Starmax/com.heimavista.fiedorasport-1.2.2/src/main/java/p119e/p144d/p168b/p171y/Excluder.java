package p119e.p144d.p168b.p171y;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.List;
import p119e.p144d.p168b.ExclusionStrategy;
import p119e.p144d.p168b.FieldAttributes;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p170x.Expose;
import p119e.p144d.p168b.p170x.Since;
import p119e.p144d.p168b.p170x.Until;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.d */
public final class Excluder implements TypeAdapterFactory, Cloneable {

    /* renamed from: V */
    public static final Excluder f7553V = new Excluder();

    /* renamed from: P */
    private double f7554P = -1.0d;

    /* renamed from: Q */
    private int f7555Q = 136;

    /* renamed from: R */
    private boolean f7556R = true;

    /* renamed from: S */
    private boolean f7557S;

    /* renamed from: T */
    private List<ExclusionStrategy> f7558T = Collections.emptyList();

    /* renamed from: U */
    private List<ExclusionStrategy> f7559U = Collections.emptyList();

    /* renamed from: b */
    private boolean m12382b(Class<?> cls, boolean z) {
        for (ExclusionStrategy bVar : z ? this.f7558T : this.f7559U) {
            if (bVar.mo23782a(cls)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: c */
    private boolean m12383c(Class<?> cls) {
        return cls.isMemberClass() && !m12384d(cls);
    }

    /* renamed from: d */
    private boolean m12384d(Class<?> cls) {
        return (cls.getModifiers() & 8) != 0;
    }

    /* renamed from: a */
    public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
        Class<? super T> a = aVar.mo23980a();
        boolean a2 = m12380a(a);
        boolean z = a2 || m12382b(a, true);
        boolean z2 = a2 || m12382b(a, false);
        if (z || z2) {
            return new C4120a(z2, z, fVar, aVar);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public Excluder clone() {
        try {
            return (Excluder) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new AssertionError(e);
        }
    }

    /* renamed from: e.d.b.y.d$a */
    /* compiled from: Excluder */
    class C4120a extends TypeAdapter<T> {

        /* renamed from: a */
        private TypeAdapter<T> f7560a;

        /* renamed from: b */
        final /* synthetic */ boolean f7561b;

        /* renamed from: c */
        final /* synthetic */ boolean f7562c;

        /* renamed from: d */
        final /* synthetic */ Gson f7563d;

        /* renamed from: e */
        final /* synthetic */ TypeToken f7564e;

        C4120a(boolean z, boolean z2, Gson fVar, TypeToken aVar) {
            this.f7561b = z;
            this.f7562c = z2;
            this.f7563d = fVar;
            this.f7564e = aVar;
        }

        /* renamed from: b */
        private TypeAdapter<T> m12388b() {
            TypeAdapter<T> vVar = this.f7560a;
            if (vVar != null) {
                return super;
            }
            TypeAdapter<T> a = this.f7563d.mo23786a(Excluder.this, this.f7564e);
            this.f7560a = super;
            return super;
        }

        /* renamed from: a */
        public T mo23735a(JsonReader aVar) {
            if (!this.f7561b) {
                return m12388b().mo23735a(aVar);
            }
            aVar.mo23743E();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, T t) {
            if (this.f7562c) {
                cVar.mo23780u();
            } else {
                m12388b().mo23736a(cVar, t);
            }
        }
    }

    /* renamed from: b */
    private boolean m12381b(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    /* renamed from: a */
    public boolean mo23872a(Field field, boolean z) {
        Expose aVar;
        if ((this.f7555Q & field.getModifiers()) != 0) {
            return true;
        }
        if ((this.f7554P != -1.0d && !m12378a((Since) field.getAnnotation(Since.class), (Until) field.getAnnotation(Until.class))) || field.isSynthetic()) {
            return true;
        }
        if (this.f7557S && ((aVar = (Expose) field.getAnnotation(Expose.class)) == null || (!z ? !aVar.deserialize() : !aVar.serialize()))) {
            return true;
        }
        if ((!this.f7556R && m12383c(field.getType())) || m12381b(field.getType())) {
            return true;
        }
        List<ExclusionStrategy> list = z ? this.f7558T : this.f7559U;
        if (list.isEmpty()) {
            return false;
        }
        FieldAttributes cVar = new FieldAttributes(field);
        for (ExclusionStrategy bVar : list) {
            if (bVar.mo23781a(cVar)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a */
    private boolean m12380a(Class<?> cls) {
        if (this.f7554P != -1.0d && !m12378a((Since) cls.getAnnotation(Since.class), (Until) cls.getAnnotation(Until.class))) {
            return true;
        }
        if ((this.f7556R || !m12383c(cls)) && !m12381b(cls)) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public boolean mo23871a(Class<?> cls, boolean z) {
        return m12380a(cls) || m12382b(cls, z);
    }

    /* renamed from: a */
    private boolean m12378a(Since dVar, Until eVar) {
        return m12377a(dVar) && m12379a(eVar);
    }

    /* renamed from: a */
    private boolean m12377a(Since dVar) {
        return dVar == null || dVar.value() <= this.f7554P;
    }

    /* renamed from: a */
    private boolean m12379a(Until eVar) {
        return eVar == null || eVar.value() > this.f7554P;
    }
}
