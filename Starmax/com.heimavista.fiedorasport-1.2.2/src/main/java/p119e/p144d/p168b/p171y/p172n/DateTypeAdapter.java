package p119e.p144d.p168b.p171y.p172n;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonSyntaxException;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.JavaVersion;
import p119e.p144d.p168b.p171y.PreJava9DateFormatProvider;
import p119e.p144d.p168b.p171y.p172n.p173o.ISO8601Utils;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.c */
public final class DateTypeAdapter extends TypeAdapter<Date> {

    /* renamed from: b */
    public static final TypeAdapterFactory f7605b = new C4136a();

    /* renamed from: a */
    private final List<DateFormat> f7606a = new ArrayList();

    /* renamed from: e.d.b.y.n.c$a */
    /* compiled from: DateTypeAdapter */
    class C4136a implements TypeAdapterFactory {
        C4136a() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            if (aVar.mo23980a() == Date.class) {
                return new DateTypeAdapter();
            }
            return null;
        }
    }

    public DateTypeAdapter() {
        this.f7606a.add(DateFormat.getDateTimeInstance(2, 2, Locale.US));
        if (!Locale.getDefault().equals(Locale.US)) {
            this.f7606a.add(DateFormat.getDateTimeInstance(2, 2));
        }
        if (JavaVersion.m12396c()) {
            this.f7606a.add(PreJava9DateFormatProvider.m12414a(2, 2));
        }
    }

    /* renamed from: a */
    public Date mo23735a(JsonReader aVar) {
        if (aVar.mo23742D() != JsonToken.NULL) {
            return m12436a(aVar.mo23741C());
        }
        aVar.mo23740B();
        return null;
    }

    /* renamed from: a */
    private synchronized Date m12436a(String str) {
        for (DateFormat dateFormat : this.f7606a) {
            try {
                return dateFormat.parse(str);
            } catch (ParseException unused) {
            }
        }
        try {
            return ISO8601Utils.m12653a(str, new ParsePosition(0));
        } catch (ParseException e) {
            throw new JsonSyntaxException(str, e);
        }
    }

    /* renamed from: a */
    public synchronized void mo23736a(JsonWriter cVar, Date date) {
        if (date == null) {
            cVar.mo23780u();
        } else {
            cVar.mo23773d(this.f7606a.get(0).format(date));
        }
    }
}
