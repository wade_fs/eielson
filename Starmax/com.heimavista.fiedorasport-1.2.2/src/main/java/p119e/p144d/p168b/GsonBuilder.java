package p119e.p144d.p168b;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import p119e.p144d.p168b.p171y.Excluder;
import p119e.p144d.p168b.p171y.p172n.TypeAdapters;

/* renamed from: e.d.b.g */
public final class GsonBuilder {

    /* renamed from: a */
    private Excluder f7511a = Excluder.f7553V;

    /* renamed from: b */
    private LongSerializationPolicy f7512b = LongSerializationPolicy.f7531P;

    /* renamed from: c */
    private FieldNamingStrategy f7513c = FieldNamingPolicy.IDENTITY;

    /* renamed from: d */
    private final Map<Type, InstanceCreator<?>> f7514d = new HashMap();

    /* renamed from: e */
    private final List<TypeAdapterFactory> f7515e = new ArrayList();

    /* renamed from: f */
    private final List<TypeAdapterFactory> f7516f = new ArrayList();

    /* renamed from: g */
    private boolean f7517g = false;

    /* renamed from: h */
    private String f7518h;

    /* renamed from: i */
    private int f7519i = 2;

    /* renamed from: j */
    private int f7520j = 2;

    /* renamed from: k */
    private boolean f7521k = false;

    /* renamed from: l */
    private boolean f7522l = false;

    /* renamed from: m */
    private boolean f7523m = true;

    /* renamed from: n */
    private boolean f7524n = false;

    /* renamed from: o */
    private boolean f7525o = false;

    /* renamed from: p */
    private boolean f7526p = false;

    /* renamed from: a */
    public Gson mo23807a() {
        ArrayList arrayList = r1;
        ArrayList arrayList2 = new ArrayList(this.f7515e.size() + this.f7516f.size() + 3);
        arrayList2.addAll(this.f7515e);
        Collections.reverse(arrayList2);
        ArrayList arrayList3 = new ArrayList(this.f7516f);
        Collections.reverse(arrayList3);
        arrayList2.addAll(arrayList3);
        m12302a(this.f7518h, this.f7519i, this.f7520j, arrayList2);
        return new Gson(this.f7511a, this.f7513c, this.f7514d, this.f7517g, this.f7521k, this.f7525o, this.f7523m, this.f7524n, this.f7526p, this.f7522l, this.f7512b, this.f7518h, this.f7519i, this.f7520j, this.f7515e, this.f7516f, arrayList);
    }

    /* renamed from: b */
    public GsonBuilder mo23808b() {
        this.f7517g = true;
        return this;
    }

    /* renamed from: c */
    public GsonBuilder mo23809c() {
        this.f7524n = true;
        return this;
    }

    /* renamed from: a */
    private void m12302a(String str, int i, int i2, List<TypeAdapterFactory> list) {
        DefaultDateTypeAdapter aVar;
        DefaultDateTypeAdapter aVar2;
        DefaultDateTypeAdapter aVar3;
        if (str != null && !"".equals(str.trim())) {
            DefaultDateTypeAdapter aVar4 = new DefaultDateTypeAdapter(Date.class, str);
            aVar = new DefaultDateTypeAdapter(Timestamp.class, str);
            aVar3 = new DefaultDateTypeAdapter(java.sql.Date.class, str);
            aVar2 = aVar4;
        } else if (i != 2 && i2 != 2) {
            aVar2 = new DefaultDateTypeAdapter(Date.class, i, i2);
            DefaultDateTypeAdapter aVar5 = new DefaultDateTypeAdapter(Timestamp.class, i, i2);
            DefaultDateTypeAdapter aVar6 = new DefaultDateTypeAdapter(java.sql.Date.class, i, i2);
            aVar = aVar5;
            aVar3 = aVar6;
        } else {
            return;
        }
        list.add(TypeAdapters.m12519a(Date.class, aVar2));
        list.add(TypeAdapters.m12519a(Timestamp.class, aVar));
        list.add(TypeAdapters.m12519a(java.sql.Date.class, aVar3));
    }
}
