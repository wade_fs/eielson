package p119e.p144d.p168b.p171y;

import java.io.Writer;
import p119e.p144d.p168b.JsonElement;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.p172n.TypeAdapters;

/* renamed from: e.d.b.y.l */
public final class Streams {
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0016, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001c, code lost:
        throw new p119e.p144d.p168b.JsonIOException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x001d, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0023, code lost:
        throw new p119e.p144d.p168b.JsonSyntaxException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002a, code lost:
        return p119e.p144d.p168b.JsonNull.f7528a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0030, code lost:
        throw new p119e.p144d.p168b.JsonSyntaxException(r2);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000d, code lost:
        r2 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x000f, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0015, code lost:
        throw new p119e.p144d.p168b.JsonSyntaxException(r2);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0016 A[ExcHandler: IOException (r2v5 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x001d A[ExcHandler: d (r2v4 'e' e.d.b.a0.d A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:7:0x000f A[ExcHandler: NumberFormatException (r2v6 'e' java.lang.NumberFormatException A[CUSTOM_DECLARE]), Splitter:B:0:0x0000] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static p119e.p144d.p168b.JsonElement m12418a(p119e.p144d.p168b.p169a0.JsonReader r2) {
        /*
            r2.mo23742D()     // Catch:{ EOFException -> 0x0024, d -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            r0 = 0
            e.d.b.v<e.d.b.l> r1 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.f7680X     // Catch:{ EOFException -> 0x000d, d -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            java.lang.Object r2 = r1.mo23735a(r2)     // Catch:{ EOFException -> 0x000d, d -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            e.d.b.l r2 = (p119e.p144d.p168b.JsonElement) r2     // Catch:{ EOFException -> 0x000d, d -> 0x001d, IOException -> 0x0016, NumberFormatException -> 0x000f }
            return r2
        L_0x000d:
            r2 = move-exception
            goto L_0x0026
        L_0x000f:
            r2 = move-exception
            e.d.b.t r0 = new e.d.b.t
            r0.<init>(r2)
            throw r0
        L_0x0016:
            r2 = move-exception
            e.d.b.m r0 = new e.d.b.m
            r0.<init>(r2)
            throw r0
        L_0x001d:
            r2 = move-exception
            e.d.b.t r0 = new e.d.b.t
            r0.<init>(r2)
            throw r0
        L_0x0024:
            r2 = move-exception
            r0 = 1
        L_0x0026:
            if (r0 == 0) goto L_0x002b
            e.d.b.n r2 = p119e.p144d.p168b.JsonNull.f7528a
            return r2
        L_0x002b:
            e.d.b.t r0 = new e.d.b.t
            r0.<init>(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.Streams.m12418a(e.d.b.a0.a):e.d.b.l");
    }

    /* renamed from: e.d.b.y.l$a */
    /* compiled from: Streams */
    private static final class C4128a extends Writer {

        /* renamed from: P */
        private final Appendable f7591P;

        /* renamed from: Q */
        private final C4129a f7592Q = new C4129a();

        /* renamed from: e.d.b.y.l$a$a */
        /* compiled from: Streams */
        static class C4129a implements CharSequence {

            /* renamed from: P */
            char[] f7593P;

            C4129a() {
            }

            public char charAt(int i) {
                return this.f7593P[i];
            }

            public int length() {
                return this.f7593P.length;
            }

            public CharSequence subSequence(int i, int i2) {
                return new String(this.f7593P, i, i2 - i);
            }
        }

        C4128a(Appendable appendable) {
            this.f7591P = appendable;
        }

        public void close() {
        }

        public void flush() {
        }

        public void write(char[] cArr, int i, int i2) {
            C4129a aVar = this.f7592Q;
            aVar.f7593P = cArr;
            this.f7591P.append(aVar, i, i2 + i);
        }

        public void write(int i) {
            this.f7591P.append((char) i);
        }
    }

    /* renamed from: a */
    public static void m12420a(JsonElement lVar, JsonWriter cVar) {
        TypeAdapters.f7680X.mo23736a(cVar, lVar);
    }

    /* renamed from: a */
    public static Writer m12419a(Appendable appendable) {
        return appendable instanceof Writer ? (Writer) appendable : new C4128a(appendable);
    }
}
