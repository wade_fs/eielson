package p119e.p144d.p168b;

import java.lang.reflect.Type;

/* renamed from: e.d.b.h */
public interface InstanceCreator<T> {
    /* renamed from: a */
    T mo23810a(Type type);
}
