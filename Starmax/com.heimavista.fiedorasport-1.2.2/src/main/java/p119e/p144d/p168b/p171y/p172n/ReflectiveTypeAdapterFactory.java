package p119e.p144d.p168b.p171y.p172n;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import p119e.p144d.p168b.FieldNamingStrategy;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonSyntaxException;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p170x.JsonAdapter;
import p119e.p144d.p168b.p170x.SerializedName;
import p119e.p144d.p168b.p171y.C$Gson$Types;
import p119e.p144d.p168b.p171y.ConstructorConstructor;
import p119e.p144d.p168b.p171y.Excluder;
import p119e.p144d.p168b.p171y.ObjectConstructor;
import p119e.p144d.p168b.p171y.Primitives;
import p119e.p144d.p168b.p171y.p174o.ReflectionAccessor;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.i */
public final class ReflectiveTypeAdapterFactory implements TypeAdapterFactory {

    /* renamed from: P */
    private final ConstructorConstructor f7627P;

    /* renamed from: Q */
    private final FieldNamingStrategy f7628Q;

    /* renamed from: R */
    private final Excluder f7629R;

    /* renamed from: S */
    private final JsonAdapterAnnotationTypeAdapterFactory f7630S;

    /* renamed from: T */
    private final ReflectionAccessor f7631T = ReflectionAccessor.m12656a();

    /* renamed from: e.d.b.y.n.i$c */
    /* compiled from: ReflectiveTypeAdapterFactory */
    static abstract class C4144c {

        /* renamed from: a */
        final String f7640a;

        /* renamed from: b */
        final boolean f7641b;

        /* renamed from: c */
        final boolean f7642c;

        protected C4144c(String str, boolean z, boolean z2) {
            this.f7640a = str;
            this.f7641b = z;
            this.f7642c = z2;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract void mo23939a(JsonReader aVar, Object obj);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract void mo23940a(JsonWriter cVar, Object obj);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract boolean mo23941a(Object obj);
    }

    public ReflectiveTypeAdapterFactory(ConstructorConstructor cVar, FieldNamingStrategy eVar, Excluder dVar, JsonAdapterAnnotationTypeAdapterFactory dVar2) {
        this.f7627P = cVar;
        this.f7628Q = eVar;
        this.f7629R = dVar;
        this.f7630S = dVar2;
    }

    /* renamed from: a */
    public boolean mo23938a(Field field, boolean z) {
        return m12492a(field, z, this.f7629R);
    }

    /* renamed from: a */
    static boolean m12492a(Field field, boolean z, Excluder dVar) {
        return !dVar.mo23871a(field.getType(), z) && !dVar.mo23872a(field, z);
    }

    /* renamed from: a */
    private List<String> m12490a(Field field) {
        SerializedName cVar = (SerializedName) field.getAnnotation(SerializedName.class);
        if (cVar == null) {
            return Collections.singletonList(this.f7628Q.mo23783a(field));
        }
        String value = cVar.value();
        String[] alternate = cVar.alternate();
        if (alternate.length == 0) {
            return Collections.singletonList(value);
        }
        ArrayList arrayList = new ArrayList(alternate.length + 1);
        arrayList.add(value);
        for (String str : alternate) {
            arrayList.add(str);
        }
        return arrayList;
    }

    /* renamed from: e.d.b.y.n.i$a */
    /* compiled from: ReflectiveTypeAdapterFactory */
    class C4142a extends C4144c {

        /* renamed from: d */
        final /* synthetic */ Field f7632d;

        /* renamed from: e */
        final /* synthetic */ boolean f7633e;

        /* renamed from: f */
        final /* synthetic */ TypeAdapter f7634f;

        /* renamed from: g */
        final /* synthetic */ Gson f7635g;

        /* renamed from: h */
        final /* synthetic */ TypeToken f7636h;

        /* renamed from: i */
        final /* synthetic */ boolean f7637i;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        C4142a(ReflectiveTypeAdapterFactory iVar, String str, boolean z, boolean z2, Field field, boolean z3, TypeAdapter vVar, Gson fVar, TypeToken aVar, boolean z4) {
            super(str, z, z2);
            this.f7632d = field;
            this.f7633e = z3;
            this.f7634f = vVar;
            this.f7635g = fVar;
            this.f7636h = aVar;
            this.f7637i = z4;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo23940a(JsonWriter cVar, Object obj) {
            TypeAdapter vVar;
            Object obj2 = this.f7632d.get(obj);
            if (this.f7633e) {
                vVar = this.f7634f;
            } else {
                vVar = new TypeAdapterRuntimeTypeWrapper(this.f7635g, this.f7634f, this.f7636h.mo23981b());
            }
            vVar.mo23736a(cVar, obj2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo23939a(JsonReader aVar, Object obj) {
            Object a = this.f7634f.mo23735a(aVar);
            if (a != null || !this.f7637i) {
                this.f7632d.set(obj, a);
            }
        }

        /* renamed from: a */
        public boolean mo23941a(Object obj) {
            if (super.f7641b && this.f7632d.get(obj) != obj) {
                return true;
            }
            return false;
        }
    }

    /* renamed from: e.d.b.y.n.i$b */
    /* compiled from: ReflectiveTypeAdapterFactory */
    public static final class C4143b<T> extends TypeAdapter<T> {

        /* renamed from: a */
        private final ObjectConstructor<T> f7638a;

        /* renamed from: b */
        private final Map<String, C4144c> f7639b;

        C4143b(ObjectConstructor<T> iVar, Map<String, C4144c> map) {
            this.f7638a = iVar;
            this.f7639b = map;
        }

        /* renamed from: a */
        public T mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            T a = this.f7638a.mo23870a();
            try {
                aVar.mo23746b();
                while (aVar.mo23752t()) {
                    C4144c cVar = this.f7639b.get(aVar.mo23739A());
                    if (cVar != null) {
                        if (cVar.f7642c) {
                            cVar.mo23939a(aVar, a);
                        }
                    }
                    aVar.mo23743E();
                }
                aVar.mo23750e();
                return a;
            } catch (IllegalStateException e) {
                throw new JsonSyntaxException(e);
            } catch (IllegalAccessException e2) {
                throw new AssertionError(e2);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, T t) {
            if (t == null) {
                cVar.mo23780u();
                return;
            }
            cVar.mo23765b();
            try {
                for (C4144c cVar2 : this.f7639b.values()) {
                    if (cVar2.mo23941a(t)) {
                        cVar.mo23766b(cVar2.f7640a);
                        cVar2.mo23940a(cVar, t);
                    }
                }
                cVar.mo23772d();
            } catch (IllegalAccessException e) {
                throw new AssertionError(e);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.n.i.a(e.d.b.f, e.d.b.z.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, e.d.b.y.n.i$c>
     arg types: [e.d.b.f, e.d.b.z.a<T>, java.lang.Class<? super T>]
     candidates:
      e.d.b.y.n.i.a(java.lang.reflect.Field, boolean, e.d.b.y.d):boolean
      e.d.b.y.n.i.a(e.d.b.f, e.d.b.z.a<?>, java.lang.Class<?>):java.util.Map<java.lang.String, e.d.b.y.n.i$c> */
    /* renamed from: a */
    public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
        Class<? super T> a = aVar.mo23980a();
        if (!Object.class.isAssignableFrom(a)) {
            return null;
        }
        return new C4143b(this.f7627P.mo23868a(aVar), m12491a(fVar, (TypeToken<?>) aVar, (Class<?>) a));
    }

    /* renamed from: a */
    private C4144c m12489a(Gson fVar, Field field, String str, TypeToken<?> aVar, boolean z, boolean z2) {
        TypeToken<?> aVar2 = aVar;
        boolean a = Primitives.m12417a((Type) aVar.mo23980a());
        JsonAdapter bVar = (JsonAdapter) field.getAnnotation(JsonAdapter.class);
        TypeAdapter<?> a2 = bVar != null ? this.f7630S.mo23929a(this.f7627P, fVar, aVar2, bVar) : null;
        boolean z3 = a2 != null;
        if (a2 == null) {
            a2 = fVar.mo23787a((TypeToken) aVar2);
        }
        return new C4142a(this, str, z, z2, field, z3, a2, fVar, aVar, a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.n.i.a(java.lang.reflect.Field, boolean):boolean
     arg types: [java.lang.reflect.Field, int]
     candidates:
      e.d.b.y.n.i.a(e.d.b.f, e.d.b.z.a):e.d.b.v<T>
      e.d.b.w.a(e.d.b.f, e.d.b.z.a):e.d.b.v<T>
      e.d.b.y.n.i.a(java.lang.reflect.Field, boolean):boolean */
    /* renamed from: a */
    private Map<String, C4144c> m12491a(Gson fVar, TypeToken<?> aVar, Class<?> cls) {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        if (cls.isInterface()) {
            return linkedHashMap;
        }
        Type b = aVar.mo23981b();
        TypeToken<?> aVar2 = aVar;
        Class<?> cls2 = cls;
        while (cls2 != Object.class) {
            Field[] declaredFields = cls2.getDeclaredFields();
            int length = declaredFields.length;
            boolean z = false;
            int i = 0;
            while (i < length) {
                Field field = declaredFields[i];
                boolean a = mo23938a(field, true);
                boolean a2 = mo23938a(field, z);
                if (a || a2) {
                    this.f7631T.mo23978a(field);
                    Type a3 = C$Gson$Types.m12345a(aVar2.mo23981b(), cls2, field.getGenericType());
                    List<String> a4 = m12490a(field);
                    int size = a4.size();
                    C4144c cVar = null;
                    int i2 = 0;
                    while (i2 < size) {
                        String str = a4.get(i2);
                        boolean z2 = i2 != 0 ? false : a;
                        String str2 = str;
                        C4144c cVar2 = cVar;
                        int i3 = i2;
                        int i4 = size;
                        List<String> list = a4;
                        Field field2 = field;
                        cVar = cVar2 == null ? (C4144c) linkedHashMap.put(str2, m12489a(fVar, field, str2, TypeToken.m12663a(a3), z2, a2)) : cVar2;
                        i2 = i3 + 1;
                        a = z2;
                        a4 = list;
                        size = i4;
                        field = field2;
                    }
                    C4144c cVar3 = cVar;
                    if (cVar3 != null) {
                        throw new IllegalArgumentException(b + " declares multiple JSON fields named " + cVar3.f7640a);
                    }
                }
                i++;
                z = false;
            }
            aVar2 = TypeToken.m12663a(C$Gson$Types.m12345a(aVar2.mo23981b(), cls2, cls2.getGenericSuperclass()));
            cls2 = aVar2.mo23980a();
        }
        return linkedHashMap;
    }
}
