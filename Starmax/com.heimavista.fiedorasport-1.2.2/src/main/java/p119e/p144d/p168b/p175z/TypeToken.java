package p119e.p144d.p168b.p175z;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import p119e.p144d.p168b.p171y.C$Gson$Preconditions;
import p119e.p144d.p168b.p171y.C$Gson$Types;

/* renamed from: e.d.b.z.a */
public class TypeToken<T> {

    /* renamed from: a */
    final Class<? super T> f7730a;

    /* renamed from: b */
    final Type f7731b;

    /* renamed from: c */
    final int f7732c;

    protected TypeToken() {
        this.f7731b = m12664b(TypeToken.class);
        this.f7730a = C$Gson$Types.m12355e(this.f7731b);
        this.f7732c = this.f7731b.hashCode();
    }

    /* renamed from: b */
    static Type m12664b(Class<?> cls) {
        Type genericSuperclass = cls.getGenericSuperclass();
        if (!(genericSuperclass instanceof Class)) {
            return C$Gson$Types.m12350b(((ParameterizedType) genericSuperclass).getActualTypeArguments()[0]);
        }
        throw new RuntimeException("Missing type parameter.");
    }

    /* renamed from: a */
    public final Class<? super T> mo23980a() {
        return this.f7730a;
    }

    public final boolean equals(Object obj) {
        return (obj instanceof TypeToken) && C$Gson$Types.m12349a(this.f7731b, ((TypeToken) obj).f7731b);
    }

    public final int hashCode() {
        return this.f7732c;
    }

    public final String toString() {
        return C$Gson$Types.m12358h(this.f7731b);
    }

    /* renamed from: a */
    public static TypeToken<?> m12663a(Type type) {
        return new TypeToken<>(type);
    }

    /* renamed from: a */
    public static <T> TypeToken<T> m12662a(Class cls) {
        return new TypeToken<>(cls);
    }

    TypeToken(Type type) {
        C$Gson$Preconditions.m12336a(type);
        this.f7731b = C$Gson$Types.m12350b(type);
        this.f7730a = C$Gson$Types.m12355e(this.f7731b);
        this.f7732c = this.f7731b.hashCode();
    }

    /* renamed from: b */
    public final Type mo23981b() {
        return this.f7731b;
    }
}
