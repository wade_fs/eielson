package p119e.p144d.p168b.p171y.p172n;

import java.util.ArrayList;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.LinkedTreeMap;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.h */
public final class ObjectTypeAdapter extends TypeAdapter<Object> {

    /* renamed from: b */
    public static final TypeAdapterFactory f7624b = new C4140a();

    /* renamed from: a */
    private final Gson f7625a;

    /* renamed from: e.d.b.y.n.h$a */
    /* compiled from: ObjectTypeAdapter */
    class C4140a implements TypeAdapterFactory {
        C4140a() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            if (aVar.mo23980a() == Object.class) {
                return new ObjectTypeAdapter(fVar);
            }
            return null;
        }
    }

    /* renamed from: e.d.b.y.n.h$b */
    /* compiled from: ObjectTypeAdapter */
    static /* synthetic */ class C4141b {

        /* renamed from: a */
        static final /* synthetic */ int[] f7626a = new int[JsonToken.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                e.d.b.a0.b[] r0 = p119e.p144d.p168b.p169a0.JsonToken.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a = r0
                int[] r0 = p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a     // Catch:{ NoSuchFieldError -> 0x0014 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.BEGIN_ARRAY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a     // Catch:{ NoSuchFieldError -> 0x001f }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.BEGIN_OBJECT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a     // Catch:{ NoSuchFieldError -> 0x002a }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.STRING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a     // Catch:{ NoSuchFieldError -> 0x0035 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.NUMBER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a     // Catch:{ NoSuchFieldError -> 0x0040 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.BOOLEAN     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.f7626a     // Catch:{ NoSuchFieldError -> 0x004b }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.NULL     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter.C4141b.<clinit>():void");
        }
    }

    ObjectTypeAdapter(Gson fVar) {
        this.f7625a = fVar;
    }

    /* renamed from: a */
    public Object mo23735a(JsonReader aVar) {
        switch (C4141b.f7626a[aVar.mo23742D().ordinal()]) {
            case 1:
                ArrayList arrayList = new ArrayList();
                aVar.mo23744a();
                while (aVar.mo23752t()) {
                    arrayList.add(mo23735a(aVar));
                }
                aVar.mo23749d();
                return arrayList;
            case 2:
                LinkedTreeMap hVar = new LinkedTreeMap();
                aVar.mo23746b();
                while (aVar.mo23752t()) {
                    hVar.put(aVar.mo23739A(), mo23735a(aVar));
                }
                aVar.mo23750e();
                return hVar;
            case 3:
                return aVar.mo23741C();
            case 4:
                return Double.valueOf(aVar.mo23757x());
            case 5:
                return Boolean.valueOf(aVar.mo23756w());
            case 6:
                aVar.mo23740B();
                return null;
            default:
                throw new IllegalStateException();
        }
    }

    /* renamed from: a */
    public void mo23736a(JsonWriter cVar, Object obj) {
        if (obj == null) {
            cVar.mo23780u();
            return;
        }
        TypeAdapter a = this.f7625a.mo23788a((Class) obj.getClass());
        if (a instanceof ObjectTypeAdapter) {
            cVar.mo23765b();
            cVar.mo23772d();
            return;
        }
        super.mo23736a(cVar, obj);
    }
}
