package p119e.p144d.p168b.p171y.p174o;

import java.lang.reflect.AccessibleObject;
import p119e.p144d.p168b.p171y.JavaVersion;

/* renamed from: e.d.b.y.o.b */
public abstract class ReflectionAccessor {

    /* renamed from: a */
    private static final ReflectionAccessor f7726a = (JavaVersion.m12393b() < 9 ? new PreJava9ReflectionAccessor() : new UnsafeReflectionAccessor());

    /* renamed from: a */
    public static ReflectionAccessor m12656a() {
        return f7726a;
    }

    /* renamed from: a */
    public abstract void mo23978a(AccessibleObject accessibleObject);
}
