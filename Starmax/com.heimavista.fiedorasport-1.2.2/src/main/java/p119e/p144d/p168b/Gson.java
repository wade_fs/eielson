package p119e.p144d.p168b;

import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicLongArray;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p169a0.MalformedJsonException;
import p119e.p144d.p168b.p171y.ConstructorConstructor;
import p119e.p144d.p168b.p171y.Excluder;
import p119e.p144d.p168b.p171y.Primitives;
import p119e.p144d.p168b.p171y.Streams;
import p119e.p144d.p168b.p171y.p172n.ArrayTypeAdapter;
import p119e.p144d.p168b.p171y.p172n.CollectionTypeAdapterFactory;
import p119e.p144d.p168b.p171y.p172n.DateTypeAdapter;
import p119e.p144d.p168b.p171y.p172n.JsonAdapterAnnotationTypeAdapterFactory;
import p119e.p144d.p168b.p171y.p172n.MapTypeAdapterFactory;
import p119e.p144d.p168b.p171y.p172n.ObjectTypeAdapter;
import p119e.p144d.p168b.p171y.p172n.ReflectiveTypeAdapterFactory;
import p119e.p144d.p168b.p171y.p172n.SqlDateTypeAdapter;
import p119e.p144d.p168b.p171y.p172n.TimeTypeAdapter;
import p119e.p144d.p168b.p171y.p172n.TypeAdapters;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.f */
public final class Gson {

    /* renamed from: k */
    private static final TypeToken<?> f7497k = TypeToken.m12662a(Object.class);

    /* renamed from: a */
    private final ThreadLocal<Map<TypeToken<?>, C4099f<?>>> f7498a = new ThreadLocal<>();

    /* renamed from: b */
    private final Map<TypeToken<?>, TypeAdapter<?>> f7499b = new ConcurrentHashMap();

    /* renamed from: c */
    private final ConstructorConstructor f7500c;

    /* renamed from: d */
    private final JsonAdapterAnnotationTypeAdapterFactory f7501d;

    /* renamed from: e */
    final List<TypeAdapterFactory> f7502e;

    /* renamed from: f */
    final boolean f7503f;

    /* renamed from: g */
    final boolean f7504g;

    /* renamed from: h */
    final boolean f7505h;

    /* renamed from: i */
    final boolean f7506i;

    /* renamed from: j */
    final boolean f7507j;

    Gson(Excluder dVar, FieldNamingStrategy eVar, Map<Type, InstanceCreator<?>> map, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7, LongSerializationPolicy uVar, String str, int i, int i2, List<TypeAdapterFactory> list, List<TypeAdapterFactory> list2, List<TypeAdapterFactory> list3) {
        boolean z8 = z7;
        this.f7500c = new ConstructorConstructor(map);
        this.f7503f = z;
        this.f7504g = z3;
        this.f7505h = z4;
        this.f7506i = z5;
        this.f7507j = z6;
        ArrayList arrayList = new ArrayList();
        arrayList.add(TypeAdapters.f7681Y);
        arrayList.add(ObjectTypeAdapter.f7624b);
        arrayList.add(dVar);
        arrayList.addAll(list3);
        arrayList.add(TypeAdapters.f7660D);
        arrayList.add(TypeAdapters.f7695m);
        arrayList.add(TypeAdapters.f7689g);
        arrayList.add(TypeAdapters.f7691i);
        arrayList.add(TypeAdapters.f7693k);
        TypeAdapter<Number> a = m12256a(uVar);
        arrayList.add(TypeAdapters.m12520a(Long.TYPE, Long.class, a));
        arrayList.add(TypeAdapters.m12520a(Double.TYPE, Double.class, m12258a(z8)));
        arrayList.add(TypeAdapters.m12520a(Float.TYPE, Float.class, m12262b(z8)));
        arrayList.add(TypeAdapters.f7706x);
        arrayList.add(TypeAdapters.f7697o);
        arrayList.add(TypeAdapters.f7699q);
        arrayList.add(TypeAdapters.m12519a(AtomicLong.class, m12257a(a)));
        arrayList.add(TypeAdapters.m12519a(AtomicLongArray.class, m12261b(a)));
        arrayList.add(TypeAdapters.f7701s);
        arrayList.add(TypeAdapters.f7708z);
        arrayList.add(TypeAdapters.f7662F);
        arrayList.add(TypeAdapters.f7664H);
        arrayList.add(TypeAdapters.m12519a(BigDecimal.class, TypeAdapters.f7658B));
        arrayList.add(TypeAdapters.m12519a(BigInteger.class, TypeAdapters.f7659C));
        arrayList.add(TypeAdapters.f7666J);
        arrayList.add(TypeAdapters.f7668L);
        arrayList.add(TypeAdapters.f7672P);
        arrayList.add(TypeAdapters.f7674R);
        arrayList.add(TypeAdapters.f7679W);
        arrayList.add(TypeAdapters.f7670N);
        arrayList.add(TypeAdapters.f7686d);
        arrayList.add(DateTypeAdapter.f7605b);
        arrayList.add(TypeAdapters.f7677U);
        arrayList.add(TimeTypeAdapter.f7645b);
        arrayList.add(SqlDateTypeAdapter.f7643b);
        arrayList.add(TypeAdapters.f7675S);
        arrayList.add(ArrayTypeAdapter.f7599c);
        arrayList.add(TypeAdapters.f7684b);
        arrayList.add(new CollectionTypeAdapterFactory(this.f7500c));
        arrayList.add(new MapTypeAdapterFactory(this.f7500c, z2));
        this.f7501d = new JsonAdapterAnnotationTypeAdapterFactory(this.f7500c);
        arrayList.add(this.f7501d);
        arrayList.add(TypeAdapters.f7682Z);
        arrayList.add(new ReflectiveTypeAdapterFactory(this.f7500c, eVar, dVar, this.f7501d));
        this.f7502e = Collections.unmodifiableList(arrayList);
    }

    /* renamed from: a */
    private TypeAdapter<Number> m12258a(boolean z) {
        if (z) {
            return TypeAdapters.f7704v;
        }
        return new C4094a(this);
    }

    /* renamed from: b */
    private TypeAdapter<Number> m12262b(boolean z) {
        if (z) {
            return TypeAdapters.f7703u;
        }
        return new C4095b(this);
    }

    public String toString() {
        return "{serializeNulls:" + this.f7503f + ",factories:" + this.f7502e + ",instanceCreators:" + this.f7500c + "}";
    }

    /* renamed from: e.d.b.f$a */
    /* compiled from: Gson */
    class C4094a extends TypeAdapter<Number> {
        C4094a(Gson fVar) {
        }

        /* renamed from: a */
        public Double m12280a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return Double.valueOf(aVar.mo23757x());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            if (number == null) {
                cVar.mo23780u();
                return;
            }
            Gson.m12259a(number.doubleValue());
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.f$b */
    /* compiled from: Gson */
    class C4095b extends TypeAdapter<Number> {
        C4095b(Gson fVar) {
        }

        /* renamed from: a */
        public Float m12284a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return Float.valueOf((float) aVar.mo23757x());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            if (number == null) {
                cVar.mo23780u();
                return;
            }
            Gson.m12259a((double) number.floatValue());
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.f$c */
    /* compiled from: Gson */
    class C4096c extends TypeAdapter<Number> {
        C4096c() {
        }

        /* renamed from: a */
        public Number m12288a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return Long.valueOf(aVar.mo23759z());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            if (number == null) {
                cVar.mo23780u();
            } else {
                cVar.mo23773d(number.toString());
            }
        }
    }

    /* renamed from: e.d.b.f$d */
    /* compiled from: Gson */
    class C4097d extends TypeAdapter<AtomicLong> {

        /* renamed from: a */
        final /* synthetic */ TypeAdapter f7508a;

        C4097d(TypeAdapter vVar) {
            this.f7508a = super;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, AtomicLong atomicLong) {
            this.f7508a.mo23736a(cVar, Long.valueOf(atomicLong.get()));
        }

        /* renamed from: a */
        public AtomicLong mo23735a(JsonReader aVar) {
            return new AtomicLong(((Number) this.f7508a.mo23735a(aVar)).longValue());
        }
    }

    /* renamed from: e.d.b.f$e */
    /* compiled from: Gson */
    class C4098e extends TypeAdapter<AtomicLongArray> {

        /* renamed from: a */
        final /* synthetic */ TypeAdapter f7509a;

        C4098e(TypeAdapter vVar) {
            this.f7509a = super;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, AtomicLongArray atomicLongArray) {
            cVar.mo23761a();
            int length = atomicLongArray.length();
            for (int i = 0; i < length; i++) {
                this.f7509a.mo23736a(cVar, Long.valueOf(atomicLongArray.get(i)));
            }
            cVar.mo23768c();
        }

        /* renamed from: a */
        public AtomicLongArray mo23735a(JsonReader aVar) {
            ArrayList arrayList = new ArrayList();
            aVar.mo23744a();
            while (aVar.mo23752t()) {
                arrayList.add(Long.valueOf(((Number) this.f7509a.mo23735a(aVar)).longValue()));
            }
            aVar.mo23749d();
            int size = arrayList.size();
            AtomicLongArray atomicLongArray = new AtomicLongArray(size);
            for (int i = 0; i < size; i++) {
                atomicLongArray.set(i, ((Long) arrayList.get(i)).longValue());
            }
            return atomicLongArray;
        }
    }

    /* renamed from: e.d.b.f$f */
    /* compiled from: Gson */
    static class C4099f<T> extends TypeAdapter<T> {

        /* renamed from: a */
        private TypeAdapter<T> f7510a;

        C4099f() {
        }

        /* renamed from: a */
        public void mo23806a(TypeAdapter<T> vVar) {
            if (this.f7510a == null) {
                this.f7510a = super;
                return;
            }
            throw new AssertionError();
        }

        /* renamed from: a */
        public T mo23735a(JsonReader aVar) {
            TypeAdapter<T> vVar = this.f7510a;
            if (vVar != null) {
                return super.mo23735a(aVar);
            }
            throw new IllegalStateException();
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, T t) {
            TypeAdapter<T> vVar = this.f7510a;
            if (vVar != null) {
                super.mo23736a(cVar, t);
                return;
            }
            throw new IllegalStateException();
        }
    }

    /* renamed from: a */
    static void m12259a(double d) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            throw new IllegalArgumentException(d + " is not a valid double value as per JSON specification. To override this behavior, use GsonBuilder.serializeSpecialFloatingPointValues() method.");
        }
    }

    /* renamed from: b */
    private static TypeAdapter<AtomicLongArray> m12261b(TypeAdapter<Number> vVar) {
        return new C4098e(vVar).mo23843a();
    }

    /* renamed from: a */
    private static TypeAdapter<Number> m12256a(LongSerializationPolicy uVar) {
        if (uVar == LongSerializationPolicy.f7531P) {
            return TypeAdapters.f7702t;
        }
        return new C4096c();
    }

    /* renamed from: a */
    private static TypeAdapter<AtomicLong> m12257a(TypeAdapter<Number> vVar) {
        return new C4097d(vVar).mo23843a();
    }

    /* renamed from: a */
    public <T> TypeAdapter<T> mo23787a(TypeToken aVar) {
        TypeAdapter<T> vVar = this.f7499b.get(aVar == null ? f7497k : aVar);
        if (vVar != null) {
            return vVar;
        }
        Map map = this.f7498a.get();
        boolean z = false;
        if (map == null) {
            map = new HashMap();
            this.f7498a.set(map);
            z = true;
        }
        C4099f fVar = (C4099f) map.get(aVar);
        if (fVar != null) {
            return fVar;
        }
        try {
            C4099f fVar2 = new C4099f();
            map.put(aVar, fVar2);
            for (TypeAdapterFactory wVar : this.f7502e) {
                TypeAdapter<T> a = wVar.mo23844a(this, aVar);
                if (a != null) {
                    fVar2.mo23806a((TypeAdapter) a);
                    this.f7499b.put(aVar, a);
                    return a;
                }
            }
            throw new IllegalArgumentException("GSON (2.8.6) cannot handle " + aVar);
        } finally {
            map.remove(aVar);
            if (z) {
                this.f7498a.remove();
            }
        }
    }

    /* renamed from: a */
    public <T> TypeAdapter<T> mo23786a(TypeAdapterFactory wVar, TypeToken aVar) {
        if (!this.f7502e.contains(wVar)) {
            wVar = this.f7501d;
        }
        boolean z = false;
        for (TypeAdapterFactory wVar2 : this.f7502e) {
            if (z) {
                TypeAdapter<T> a = wVar2.mo23844a(this, aVar);
                if (a != null) {
                    return a;
                }
            } else if (wVar2 == wVar) {
                z = true;
            }
        }
        throw new IllegalArgumentException("GSON cannot serialize " + aVar);
    }

    /* renamed from: a */
    public <T> TypeAdapter<T> mo23788a(Class cls) {
        return mo23787a(TypeToken.m12662a(cls));
    }

    /* renamed from: a */
    public String mo23794a(Object obj) {
        if (obj == null) {
            return mo23793a((JsonElement) JsonNull.f7528a);
        }
        return mo23795a(obj, obj.getClass());
    }

    /* renamed from: a */
    public String mo23795a(Object obj, Type type) {
        StringWriter stringWriter = new StringWriter();
        mo23799a(obj, type, stringWriter);
        return stringWriter.toString();
    }

    /* renamed from: a */
    public void mo23799a(Object obj, Type type, Appendable appendable) {
        try {
            mo23798a(obj, type, mo23785a(Streams.m12419a(appendable)));
        } catch (IOException e) {
            throw new JsonIOException(e);
        }
    }

    /* renamed from: a */
    public void mo23798a(Object obj, Type type, JsonWriter cVar) {
        TypeAdapter a = mo23787a((TypeToken) TypeToken.m12663a(type));
        boolean t = cVar.mo23779t();
        cVar.mo23767b(true);
        boolean g = cVar.mo23778g();
        cVar.mo23764a(this.f7505h);
        boolean e = cVar.mo23775e();
        cVar.mo23770c(this.f7503f);
        try {
            a.mo23736a(cVar, obj);
            cVar.mo23767b(t);
            cVar.mo23764a(g);
            cVar.mo23770c(e);
        } catch (IOException e2) {
            throw new JsonIOException(e2);
        } catch (AssertionError e3) {
            AssertionError assertionError = new AssertionError("AssertionError (GSON 2.8.6): " + e3.getMessage());
            assertionError.initCause(e3);
            throw assertionError;
        } catch (Throwable th) {
            cVar.mo23767b(t);
            cVar.mo23764a(g);
            cVar.mo23770c(e);
            throw th;
        }
    }

    /* renamed from: a */
    public String mo23793a(JsonElement lVar) {
        StringWriter stringWriter = new StringWriter();
        mo23797a(lVar, stringWriter);
        return stringWriter.toString();
    }

    /* renamed from: a */
    public void mo23797a(JsonElement lVar, Appendable appendable) {
        try {
            mo23796a(lVar, mo23785a(Streams.m12419a(appendable)));
        } catch (IOException e) {
            throw new JsonIOException(e);
        }
    }

    /* renamed from: a */
    public JsonWriter mo23785a(Writer writer) {
        if (this.f7504g) {
            writer.write(")]}'\n");
        }
        JsonWriter cVar = new JsonWriter(writer);
        if (this.f7506i) {
            cVar.mo23769c("  ");
        }
        cVar.mo23770c(this.f7503f);
        return cVar;
    }

    /* renamed from: a */
    public JsonReader mo23784a(Reader reader) {
        JsonReader aVar = new JsonReader(reader);
        aVar.mo23745a(this.f7507j);
        return aVar;
    }

    /* renamed from: a */
    public void mo23796a(JsonElement lVar, JsonWriter cVar) {
        boolean t = cVar.mo23779t();
        cVar.mo23767b(true);
        boolean g = cVar.mo23778g();
        cVar.mo23764a(this.f7505h);
        boolean e = cVar.mo23775e();
        cVar.mo23770c(this.f7503f);
        try {
            Streams.m12420a(lVar, cVar);
            cVar.mo23767b(t);
            cVar.mo23764a(g);
            cVar.mo23770c(e);
        } catch (IOException e2) {
            throw new JsonIOException(e2);
        } catch (AssertionError e3) {
            AssertionError assertionError = new AssertionError("AssertionError (GSON 2.8.6): " + e3.getMessage());
            assertionError.initCause(e3);
            throw assertionError;
        } catch (Throwable th) {
            cVar.mo23767b(t);
            cVar.mo23764a(g);
            cVar.mo23770c(e);
            throw th;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.f.a(java.lang.String, java.lang.reflect.Type):T
     arg types: [java.lang.String, java.lang.Class]
     candidates:
      e.d.b.f.a(java.lang.Object, e.d.b.a0.a):void
      e.d.b.f.a(e.d.b.w, e.d.b.z.a):e.d.b.v<T>
      e.d.b.f.a(e.d.b.a0.a, java.lang.reflect.Type):T
      e.d.b.f.a(java.io.Reader, java.lang.reflect.Type):T
      e.d.b.f.a(java.lang.String, java.lang.Class):T
      e.d.b.f.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      e.d.b.f.a(e.d.b.l, e.d.b.a0.c):void
      e.d.b.f.a(e.d.b.l, java.lang.Appendable):void
      e.d.b.f.a(java.lang.String, java.lang.reflect.Type):T */
    /* renamed from: a */
    public <T> T mo23791a(String str, Class cls) {
        return Primitives.m12416a(cls).cast(mo23792a(str, (Type) cls));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.f.a(java.io.Reader, java.lang.reflect.Type):T
     arg types: [java.io.StringReader, java.lang.reflect.Type]
     candidates:
      e.d.b.f.a(java.lang.Object, e.d.b.a0.a):void
      e.d.b.f.a(e.d.b.w, e.d.b.z.a):e.d.b.v<T>
      e.d.b.f.a(e.d.b.a0.a, java.lang.reflect.Type):T
      e.d.b.f.a(java.lang.String, java.lang.Class):T
      e.d.b.f.a(java.lang.String, java.lang.reflect.Type):T
      e.d.b.f.a(java.lang.Object, java.lang.reflect.Type):java.lang.String
      e.d.b.f.a(e.d.b.l, e.d.b.a0.c):void
      e.d.b.f.a(e.d.b.l, java.lang.Appendable):void
      e.d.b.f.a(java.io.Reader, java.lang.reflect.Type):T */
    /* renamed from: a */
    public <T> T mo23792a(String str, Type type) {
        if (str == null) {
            return null;
        }
        return mo23790a((Reader) new StringReader(str), type);
    }

    /* renamed from: a */
    public <T> T mo23790a(Reader reader, Type type) {
        JsonReader a = mo23784a(reader);
        T a2 = mo23789a(a, type);
        m12260a(a2, a);
        return a2;
    }

    /* renamed from: a */
    private static void m12260a(Object obj, JsonReader aVar) {
        if (obj != null) {
            try {
                if (aVar.mo23742D() != JsonToken.END_DOCUMENT) {
                    throw new JsonIOException("JSON document was not fully consumed.");
                }
            } catch (MalformedJsonException e) {
                throw new JsonSyntaxException(e);
            } catch (IOException e2) {
                throw new JsonIOException(e2);
            }
        }
    }

    /* renamed from: a */
    public <T> T mo23789a(JsonReader aVar, Type type) {
        boolean u = aVar.mo23754u();
        aVar.mo23745a(true);
        try {
            aVar.mo23742D();
            T a = mo23787a((TypeToken) TypeToken.m12663a(type)).mo23735a(aVar);
            aVar.mo23745a(u);
            return a;
        } catch (EOFException e) {
            if (1 != 0) {
                aVar.mo23745a(u);
                return null;
            }
            throw new JsonSyntaxException(e);
        } catch (IllegalStateException e2) {
            throw new JsonSyntaxException(e2);
        } catch (IOException e3) {
            throw new JsonSyntaxException(e3);
        } catch (AssertionError e4) {
            AssertionError assertionError = new AssertionError("AssertionError (GSON 2.8.6): " + e4.getMessage());
            assertionError.initCause(e4);
            throw assertionError;
        } catch (Throwable th) {
            aVar.mo23745a(u);
            throw th;
        }
    }
}
