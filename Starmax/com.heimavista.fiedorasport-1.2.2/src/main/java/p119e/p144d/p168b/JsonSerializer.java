package p119e.p144d.p168b;

import java.lang.reflect.Type;

/* renamed from: e.d.b.s */
public interface JsonSerializer<T> {
    /* renamed from: a */
    JsonElement mo23841a(T t, Type type, JsonSerializationContext rVar);
}
