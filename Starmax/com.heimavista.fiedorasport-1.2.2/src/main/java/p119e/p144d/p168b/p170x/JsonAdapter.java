package p119e.p144d.p168b.p170x;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: e.d.b.x.b */
public @interface JsonAdapter {
    boolean nullSafe() default true;

    Class<?> value();
}
