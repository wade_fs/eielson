package p119e.p144d.p168b.p171y;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.AbstractSet;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

/* renamed from: e.d.b.y.h */
public final class LinkedTreeMap<K, V> extends AbstractMap<K, V> implements Serializable {

    /* renamed from: W */
    private static final Comparator<Comparable> f7569W = new C4121a();

    /* renamed from: P */
    Comparator<? super K> f7570P;

    /* renamed from: Q */
    C4127e<K, V> f7571Q;

    /* renamed from: R */
    int f7572R;

    /* renamed from: S */
    int f7573S;

    /* renamed from: T */
    final C4127e<K, V> f7574T;

    /* renamed from: U */
    private LinkedTreeMap<K, V>.b f7575U;

    /* renamed from: V */
    private LinkedTreeMap<K, V>.c f7576V;

    /* renamed from: e.d.b.y.h$a */
    /* compiled from: LinkedTreeMap */
    class C4121a implements Comparator<Comparable> {
        C4121a() {
        }

        /* renamed from: a */
        public int compare(Comparable comparable, Comparable comparable2) {
            return comparable.compareTo(comparable2);
        }
    }

    /* renamed from: e.d.b.y.h$b */
    /* compiled from: LinkedTreeMap */
    class C4122b extends AbstractSet<Map.Entry<K, V>> {

        /* renamed from: e.d.b.y.h$b$a */
        /* compiled from: LinkedTreeMap */
        class C4123a extends LinkedTreeMap<K, V>.d<Map.Entry<K, V>> {
            C4123a(C4122b bVar) {
                super();
            }

            public Map.Entry<K, V> next() {
                return mo23908b();
            }
        }

        C4122b() {
        }

        public void clear() {
            LinkedTreeMap.this.clear();
        }

        public boolean contains(Object obj) {
            return (obj instanceof Map.Entry) && LinkedTreeMap.this.mo23883a((Map.Entry) obj) != null;
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new C4123a(this);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
         arg types: [e.d.b.y.h$e, int]
         candidates:
          e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
          e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
          e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
          e.d.b.y.h.a(e.d.b.y.h$e, boolean):void */
        public boolean remove(Object obj) {
            C4127e a;
            if (!(obj instanceof Map.Entry) || (a = LinkedTreeMap.this.mo23883a((Map.Entry<?, ?>) ((Map.Entry) obj))) == null) {
                return false;
            }
            LinkedTreeMap.this.mo23884a(a, true);
            return true;
        }

        public int size() {
            return LinkedTreeMap.this.f7572R;
        }
    }

    /* renamed from: e.d.b.y.h$c */
    /* compiled from: LinkedTreeMap */
    final class C4124c extends AbstractSet<K> {

        /* renamed from: e.d.b.y.h$c$a */
        /* compiled from: LinkedTreeMap */
        class C4125a extends LinkedTreeMap<K, V>.d<K> {
            C4125a(C4124c cVar) {
                super();
            }

            public K next() {
                return mo23908b().f7588U;
            }
        }

        C4124c() {
        }

        public void clear() {
            LinkedTreeMap.this.clear();
        }

        public boolean contains(Object obj) {
            return LinkedTreeMap.this.containsKey(obj);
        }

        public Iterator<K> iterator() {
            return new C4125a(this);
        }

        public boolean remove(Object obj) {
            return LinkedTreeMap.this.mo23885b(obj) != null;
        }

        public int size() {
            return LinkedTreeMap.this.f7572R;
        }
    }

    /* renamed from: e.d.b.y.h$d */
    /* compiled from: LinkedTreeMap */
    private abstract class C4126d<T> implements Iterator<T> {

        /* renamed from: P */
        C4127e<K, V> f7579P;

        /* renamed from: Q */
        C4127e<K, V> f7580Q = null;

        /* renamed from: R */
        int f7581R;

        C4126d() {
            LinkedTreeMap hVar = LinkedTreeMap.this;
            this.f7579P = hVar.f7574T.f7586S;
            this.f7581R = hVar.f7573S;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public final C4127e<K, V> mo23908b() {
            C4127e<K, V> eVar = this.f7579P;
            LinkedTreeMap hVar = LinkedTreeMap.this;
            if (eVar == hVar.f7574T) {
                throw new NoSuchElementException();
            } else if (hVar.f7573S == this.f7581R) {
                this.f7579P = eVar.f7586S;
                this.f7580Q = eVar;
                return eVar;
            } else {
                throw new ConcurrentModificationException();
            }
        }

        public final boolean hasNext() {
            return this.f7579P != LinkedTreeMap.this.f7574T;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
         arg types: [e.d.b.y.h$e<K, V>, int]
         candidates:
          e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
          e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
          e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
          e.d.b.y.h.a(e.d.b.y.h$e, boolean):void */
        public final void remove() {
            C4127e<K, V> eVar = this.f7580Q;
            if (eVar != null) {
                LinkedTreeMap.this.mo23884a((C4127e) eVar, true);
                this.f7580Q = null;
                this.f7581R = LinkedTreeMap.this.f7573S;
                return;
            }
            throw new IllegalStateException();
        }
    }

    static {
        Class<LinkedTreeMap> cls = LinkedTreeMap.class;
    }

    public LinkedTreeMap() {
        this(f7569W);
    }

    private Object writeReplace() {
        return new LinkedHashMap(this);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C4127e<K, V> mo23882a(K k, boolean z) {
        int i;
        C4127e<K, V> eVar;
        Comparator<? super K> comparator = this.f7570P;
        C4127e<K, V> eVar2 = this.f7571Q;
        if (eVar2 != null) {
            Comparable comparable = comparator == f7569W ? (Comparable) k : null;
            while (true) {
                if (comparable != null) {
                    i = comparable.compareTo(eVar2.f7588U);
                } else {
                    i = comparator.compare(k, eVar2.f7588U);
                }
                if (i == 0) {
                    return eVar2;
                }
                C4127e<K, V> eVar3 = i < 0 ? eVar2.f7584Q : eVar2.f7585R;
                if (eVar3 == null) {
                    break;
                }
                eVar2 = eVar3;
            }
        } else {
            i = 0;
        }
        if (!z) {
            return null;
        }
        C4127e<K, V> eVar4 = this.f7574T;
        if (eVar2 != null) {
            eVar = new C4127e<>(eVar2, k, eVar4, eVar4.f7587T);
            if (i < 0) {
                eVar2.f7584Q = eVar;
            } else {
                eVar2.f7585R = eVar;
            }
            m12402b(eVar2, true);
        } else if (comparator != f7569W || (k instanceof Comparable)) {
            eVar = new C4127e<>(eVar2, k, eVar4, eVar4.f7587T);
            this.f7571Q = eVar;
        } else {
            throw new ClassCastException(k.getClass().getName() + " is not Comparable");
        }
        this.f7572R++;
        this.f7573S++;
        return eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
     arg types: [e.d.b.y.h$e<K, V>, int]
     candidates:
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void */
    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public C4127e<K, V> mo23885b(Object obj) {
        C4127e<K, V> a = mo23881a(obj);
        if (a != null) {
            mo23884a((C4127e) a, true);
        }
        return a;
    }

    public void clear() {
        this.f7571Q = null;
        this.f7572R = 0;
        this.f7573S++;
        C4127e<K, V> eVar = this.f7574T;
        eVar.f7587T = eVar;
        eVar.f7586S = eVar;
    }

    public boolean containsKey(Object obj) {
        return mo23881a(obj) != null;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        LinkedTreeMap<K, V>.b bVar = this.f7575U;
        if (bVar != null) {
            return bVar;
        }
        LinkedTreeMap<K, V>.b bVar2 = new C4122b();
        this.f7575U = bVar2;
        return bVar2;
    }

    public V get(Object obj) {
        C4127e a = mo23881a(obj);
        if (a != null) {
            return a.f7589V;
        }
        return null;
    }

    public Set<K> keySet() {
        LinkedTreeMap<K, V>.c cVar = this.f7576V;
        if (cVar != null) {
            return cVar;
        }
        LinkedTreeMap<K, V>.c cVar2 = new C4124c();
        this.f7576V = cVar2;
        return cVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
     arg types: [K, int]
     candidates:
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V> */
    public V put(K k, V v) {
        if (k != null) {
            C4127e a = mo23882a((Object) k, true);
            V v2 = a.f7589V;
            a.f7589V = v;
            return v2;
        }
        throw new NullPointerException("key == null");
    }

    public V remove(Object obj) {
        C4127e b = mo23885b(obj);
        if (b != null) {
            return b.f7589V;
        }
        return null;
    }

    public int size() {
        return this.f7572R;
    }

    public LinkedTreeMap(Comparator<? super K> comparator) {
        this.f7572R = 0;
        this.f7573S = 0;
        this.f7574T = new C4127e<>();
        this.f7570P = comparator == null ? f7569W : comparator;
    }

    /* renamed from: e.d.b.y.h$e */
    /* compiled from: LinkedTreeMap */
    static final class C4127e<K, V> implements Map.Entry<K, V> {

        /* renamed from: P */
        C4127e<K, V> f7583P;

        /* renamed from: Q */
        C4127e<K, V> f7584Q;

        /* renamed from: R */
        C4127e<K, V> f7585R;

        /* renamed from: S */
        C4127e<K, V> f7586S;

        /* renamed from: T */
        C4127e<K, V> f7587T;

        /* renamed from: U */
        final K f7588U;

        /* renamed from: V */
        V f7589V;

        /* renamed from: W */
        int f7590W;

        C4127e() {
            this.f7588U = null;
            this.f7587T = this;
            this.f7586S = this;
        }

        /* renamed from: a */
        public C4127e<K, V> mo23911a() {
            C4127e<K, V> eVar = this;
            for (C4127e<K, V> eVar2 = this.f7584Q; eVar2 != null; eVar2 = eVar2.f7584Q) {
                eVar = eVar2;
            }
            return eVar;
        }

        /* renamed from: b */
        public C4127e<K, V> mo23912b() {
            C4127e<K, V> eVar = this;
            for (C4127e<K, V> eVar2 = this.f7585R; eVar2 != null; eVar2 = eVar2.f7585R) {
                eVar = eVar2;
            }
            return eVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:14:0x0031 A[ORIG_RETURN, RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean equals(java.lang.Object r4) {
            /*
                r3 = this;
                boolean r0 = r4 instanceof java.util.Map.Entry
                r1 = 0
                if (r0 == 0) goto L_0x0032
                java.util.Map$Entry r4 = (java.util.Map.Entry) r4
                K r0 = r3.f7588U
                if (r0 != 0) goto L_0x0012
                java.lang.Object r0 = r4.getKey()
                if (r0 != 0) goto L_0x0032
                goto L_0x001c
            L_0x0012:
                java.lang.Object r2 = r4.getKey()
                boolean r0 = r0.equals(r2)
                if (r0 == 0) goto L_0x0032
            L_0x001c:
                V r0 = r3.f7589V
                if (r0 != 0) goto L_0x0027
                java.lang.Object r4 = r4.getValue()
                if (r4 != 0) goto L_0x0032
                goto L_0x0031
            L_0x0027:
                java.lang.Object r4 = r4.getValue()
                boolean r4 = r0.equals(r4)
                if (r4 == 0) goto L_0x0032
            L_0x0031:
                r1 = 1
            L_0x0032:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.LinkedTreeMap.C4127e.equals(java.lang.Object):boolean");
        }

        public K getKey() {
            return this.f7588U;
        }

        public V getValue() {
            return this.f7589V;
        }

        public int hashCode() {
            K k = this.f7588U;
            int i = 0;
            int hashCode = k == null ? 0 : k.hashCode();
            V v = this.f7589V;
            if (v != null) {
                i = v.hashCode();
            }
            return hashCode ^ i;
        }

        public V setValue(V v) {
            V v2 = this.f7589V;
            this.f7589V = v;
            return v2;
        }

        public String toString() {
            return ((Object) this.f7588U) + "=" + ((Object) this.f7589V);
        }

        C4127e(C4127e<K, V> eVar, K k, C4127e<K, V> eVar2, C4127e<K, V> eVar3) {
            this.f7583P = eVar;
            this.f7588U = k;
            this.f7590W = 1;
            this.f7586S = eVar2;
            this.f7587T = eVar3;
            eVar3.f7586S = this;
            eVar2.f7587T = this;
        }
    }

    /* renamed from: b */
    private void m12402b(C4127e<K, V> eVar, boolean z) {
        while (eVar != null) {
            C4127e<K, V> eVar2 = eVar.f7584Q;
            C4127e<K, V> eVar3 = eVar.f7585R;
            int i = 0;
            int i2 = eVar2 != null ? eVar2.f7590W : 0;
            int i3 = eVar3 != null ? eVar3.f7590W : 0;
            int i4 = i2 - i3;
            if (i4 == -2) {
                C4127e<K, V> eVar4 = eVar3.f7584Q;
                C4127e<K, V> eVar5 = eVar3.f7585R;
                int i5 = eVar5 != null ? eVar5.f7590W : 0;
                if (eVar4 != null) {
                    i = eVar4.f7590W;
                }
                int i6 = i - i5;
                if (i6 == -1 || (i6 == 0 && !z)) {
                    m12398a((C4127e) eVar);
                } else {
                    m12401b((C4127e) eVar3);
                    m12398a((C4127e) eVar);
                }
                if (z) {
                    return;
                }
            } else if (i4 == 2) {
                C4127e<K, V> eVar6 = eVar2.f7584Q;
                C4127e<K, V> eVar7 = eVar2.f7585R;
                int i7 = eVar7 != null ? eVar7.f7590W : 0;
                if (eVar6 != null) {
                    i = eVar6.f7590W;
                }
                int i8 = i - i7;
                if (i8 == 1 || (i8 == 0 && !z)) {
                    m12401b((C4127e) eVar);
                } else {
                    m12398a((C4127e) eVar2);
                    m12401b((C4127e) eVar);
                }
                if (z) {
                    return;
                }
            } else if (i4 == 0) {
                eVar.f7590W = i2 + 1;
                if (z) {
                    return;
                }
            } else {
                eVar.f7590W = Math.max(i2, i3) + 1;
                if (!z) {
                    return;
                }
            }
            eVar = eVar.f7583P;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
     arg types: [java.lang.Object, int]
     candidates:
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V> */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C4127e<K, V> mo23881a(Object obj) {
        if (obj == null) {
            return null;
        }
        try {
            return mo23882a(obj, false);
        } catch (ClassCastException unused) {
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C4127e<K, V> mo23883a(Map.Entry<?, ?> entry) {
        C4127e<K, V> a = mo23881a(entry.getKey());
        if (a != null && m12400a(a.f7589V, entry.getValue())) {
            return a;
        }
        return null;
    }

    /* renamed from: a */
    private boolean m12400a(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
     arg types: [e.d.b.y.h$e<K, V>, int]
     candidates:
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
     arg types: [e.d.b.y.h$e<K, V>, e.d.b.y.h$e<K, V>]
     candidates:
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
     arg types: [e.d.b.y.h$e<K, V>, ?[OBJECT, ARRAY]]
     candidates:
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo23884a(C4127e<K, V> eVar, boolean z) {
        int i;
        if (z) {
            C4127e<K, V> eVar2 = eVar.f7587T;
            eVar2.f7586S = eVar.f7586S;
            eVar.f7586S.f7587T = eVar2;
        }
        C4127e<K, V> eVar3 = eVar.f7584Q;
        C4127e<K, V> eVar4 = eVar.f7585R;
        C4127e<K, V> eVar5 = eVar.f7583P;
        int i2 = 0;
        if (eVar3 == null || eVar4 == null) {
            if (eVar3 != null) {
                m12399a((C4127e) eVar, (C4127e) eVar3);
                eVar.f7584Q = null;
            } else if (eVar4 != null) {
                m12399a((C4127e) eVar, (C4127e) eVar4);
                eVar.f7585R = null;
            } else {
                m12399a((C4127e) eVar, (C4127e) null);
            }
            m12402b(eVar5, false);
            this.f7572R--;
            this.f7573S++;
            return;
        }
        C4127e<K, V> b = eVar3.f7590W > eVar4.f7590W ? eVar3.mo23912b() : eVar4.mo23911a();
        mo23884a((C4127e) b, false);
        C4127e<K, V> eVar6 = eVar.f7584Q;
        if (eVar6 != null) {
            i = eVar6.f7590W;
            b.f7584Q = eVar6;
            eVar6.f7583P = b;
            eVar.f7584Q = null;
        } else {
            i = 0;
        }
        C4127e<K, V> eVar7 = eVar.f7585R;
        if (eVar7 != null) {
            i2 = eVar7.f7590W;
            b.f7585R = eVar7;
            eVar7.f7583P = b;
            eVar.f7585R = null;
        }
        b.f7590W = Math.max(i, i2) + 1;
        m12399a((C4127e) eVar, (C4127e) b);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
     arg types: [e.d.b.y.h$e<K, V>, e.d.b.y.h$e<K, V>]
     candidates:
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void */
    /* renamed from: b */
    private void m12401b(C4127e<K, V> eVar) {
        C4127e<K, V> eVar2 = eVar.f7584Q;
        C4127e<K, V> eVar3 = eVar.f7585R;
        C4127e<K, V> eVar4 = eVar2.f7584Q;
        C4127e<K, V> eVar5 = eVar2.f7585R;
        eVar.f7584Q = eVar5;
        if (eVar5 != null) {
            eVar5.f7583P = eVar;
        }
        m12399a((C4127e) eVar, (C4127e) eVar2);
        eVar2.f7585R = eVar;
        eVar.f7583P = eVar2;
        int i = 0;
        eVar.f7590W = Math.max(eVar3 != null ? eVar3.f7590W : 0, eVar5 != null ? eVar5.f7590W : 0) + 1;
        int i2 = eVar.f7590W;
        if (eVar4 != null) {
            i = eVar4.f7590W;
        }
        eVar2.f7590W = Math.max(i2, i) + 1;
    }

    /* renamed from: a */
    private void m12399a(C4127e<K, V> eVar, C4127e<K, V> eVar2) {
        C4127e<K, V> eVar3 = eVar.f7583P;
        eVar.f7583P = null;
        if (eVar2 != null) {
            eVar2.f7583P = eVar3;
        }
        if (eVar3 == null) {
            this.f7571Q = eVar2;
        } else if (eVar3.f7584Q == eVar) {
            eVar3.f7584Q = eVar2;
        } else {
            eVar3.f7585R = eVar2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void
     arg types: [e.d.b.y.h$e<K, V>, e.d.b.y.h$e<K, V>]
     candidates:
      e.d.b.y.h.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.h.a(java.lang.Object, boolean):e.d.b.y.h$e<K, V>
      e.d.b.y.h.a(e.d.b.y.h$e, boolean):void
      e.d.b.y.h.a(e.d.b.y.h$e, e.d.b.y.h$e):void */
    /* renamed from: a */
    private void m12398a(C4127e<K, V> eVar) {
        C4127e<K, V> eVar2 = eVar.f7584Q;
        C4127e<K, V> eVar3 = eVar.f7585R;
        C4127e<K, V> eVar4 = eVar3.f7584Q;
        C4127e<K, V> eVar5 = eVar3.f7585R;
        eVar.f7585R = eVar4;
        if (eVar4 != null) {
            eVar4.f7583P = eVar;
        }
        m12399a((C4127e) eVar, (C4127e) eVar3);
        eVar3.f7584Q = eVar;
        eVar.f7583P = eVar3;
        int i = 0;
        eVar.f7590W = Math.max(eVar2 != null ? eVar2.f7590W : 0, eVar4 != null ? eVar4.f7590W : 0) + 1;
        int i2 = eVar.f7590W;
        if (eVar5 != null) {
            i = eVar5.f7590W;
        }
        eVar3.f7590W = Math.max(i2, i) + 1;
    }
}
