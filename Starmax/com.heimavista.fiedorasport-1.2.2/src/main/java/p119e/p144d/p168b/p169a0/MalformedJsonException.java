package p119e.p144d.p168b.p169a0;

import java.io.IOException;

/* renamed from: e.d.b.a0.d */
public final class MalformedJsonException extends IOException {
    private static final long serialVersionUID = 1;

    public MalformedJsonException(String str) {
        super(str);
    }
}
