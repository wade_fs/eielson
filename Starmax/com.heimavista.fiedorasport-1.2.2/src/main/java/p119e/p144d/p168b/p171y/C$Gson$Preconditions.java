package p119e.p144d.p168b.p171y;

/* renamed from: e.d.b.y.a  reason: invalid class name */
public final class C$Gson$Preconditions {
    /* renamed from: a */
    public static <T> T m12336a(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException();
    }

    /* renamed from: a */
    public static void m12337a(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }
}
