package p119e.p144d.p168b.p171y.p172n;

import java.lang.reflect.Type;
import java.util.Collection;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.C$Gson$Types;
import p119e.p144d.p168b.p171y.ConstructorConstructor;
import p119e.p144d.p168b.p171y.ObjectConstructor;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.b */
public final class CollectionTypeAdapterFactory implements TypeAdapterFactory {

    /* renamed from: P */
    private final ConstructorConstructor f7602P;

    public CollectionTypeAdapterFactory(ConstructorConstructor cVar) {
        this.f7602P = cVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type
     arg types: [java.lang.reflect.Type, java.lang.Class<? super T>]
     candidates:
      e.d.b.y.b.a(java.lang.Object[], java.lang.Object):int
      e.d.b.y.b.a(java.lang.Object, java.lang.Object):boolean
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.reflect.Type):boolean
      e.d.b.y.b.a(java.lang.reflect.Type, java.lang.Class<?>):java.lang.reflect.Type */
    /* renamed from: a */
    public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
        Type b = aVar.mo23981b();
        Class<? super T> a = aVar.mo23980a();
        if (!Collection.class.isAssignableFrom(a)) {
            return null;
        }
        Type a2 = C$Gson$Types.m12343a(b, (Class<?>) a);
        return new C4135a(fVar, a2, fVar.mo23787a((TypeToken) TypeToken.m12663a(a2)), this.f7602P.mo23868a(aVar));
    }

    /* renamed from: e.d.b.y.n.b$a */
    /* compiled from: CollectionTypeAdapterFactory */
    private static final class C4135a<E> extends TypeAdapter<Collection<E>> {

        /* renamed from: a */
        private final TypeAdapter<E> f7603a;

        /* renamed from: b */
        private final ObjectConstructor<? extends Collection<E>> f7604b;

        public C4135a(Gson fVar, Type type, TypeAdapter<E> vVar, ObjectConstructor<? extends Collection<E>> iVar) {
            this.f7603a = new TypeAdapterRuntimeTypeWrapper(fVar, super, type);
            this.f7604b = iVar;
        }

        /* renamed from: a */
        public Collection<E> mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            Collection<E> collection = (Collection) this.f7604b.mo23870a();
            aVar.mo23744a();
            while (aVar.mo23752t()) {
                collection.add(this.f7603a.mo23735a(aVar));
            }
            aVar.mo23749d();
            return collection;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Collection<E> collection) {
            if (collection == null) {
                cVar.mo23780u();
                return;
            }
            cVar.mo23761a();
            for (E e : collection) {
                this.f7603a.mo23736a(cVar, e);
            }
            cVar.mo23768c();
        }
    }
}
