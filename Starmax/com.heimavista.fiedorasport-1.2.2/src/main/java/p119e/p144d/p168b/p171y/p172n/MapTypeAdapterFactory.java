package p119e.p144d.p168b.p171y.p172n;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Map;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonElement;
import p119e.p144d.p168b.JsonPrimitive;
import p119e.p144d.p168b.JsonSyntaxException;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.C$Gson$Types;
import p119e.p144d.p168b.p171y.ConstructorConstructor;
import p119e.p144d.p168b.p171y.JsonReaderInternalAccess;
import p119e.p144d.p168b.p171y.ObjectConstructor;
import p119e.p144d.p168b.p171y.Streams;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.g */
public final class MapTypeAdapterFactory implements TypeAdapterFactory {

    /* renamed from: P */
    private final ConstructorConstructor f7618P;

    /* renamed from: Q */
    final boolean f7619Q;

    public MapTypeAdapterFactory(ConstructorConstructor cVar, boolean z) {
        this.f7618P = cVar;
        this.f7619Q = z;
    }

    /* renamed from: a */
    public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
        Type b = aVar.mo23981b();
        if (!Map.class.isAssignableFrom(aVar.mo23980a())) {
            return null;
        }
        Type[] b2 = C$Gson$Types.m12352b(b, C$Gson$Types.m12355e(b));
        return new C4139a(fVar, b2[0], m12479a(fVar, b2[0]), b2[1], fVar.mo23787a((TypeToken) TypeToken.m12663a(b2[1])), this.f7618P.mo23868a(aVar));
    }

    /* renamed from: e.d.b.y.n.g$a */
    /* compiled from: MapTypeAdapterFactory */
    private final class C4139a<K, V> extends TypeAdapter<Map<K, V>> {

        /* renamed from: a */
        private final TypeAdapter<K> f7620a;

        /* renamed from: b */
        private final TypeAdapter<V> f7621b;

        /* renamed from: c */
        private final ObjectConstructor<? extends Map<K, V>> f7622c;

        public C4139a(Gson fVar, Type type, TypeAdapter<K> vVar, Type type2, TypeAdapter<V> vVar2, ObjectConstructor<? extends Map<K, V>> iVar) {
            this.f7620a = new TypeAdapterRuntimeTypeWrapper(fVar, super, type);
            this.f7621b = new TypeAdapterRuntimeTypeWrapper(fVar, super, type2);
            this.f7622c = iVar;
        }

        /* renamed from: a */
        public Map<K, V> mo23735a(JsonReader aVar) {
            JsonToken D = aVar.mo23742D();
            if (D == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            Map<K, V> map = (Map) this.f7622c.mo23870a();
            if (D == JsonToken.BEGIN_ARRAY) {
                aVar.mo23744a();
                while (aVar.mo23752t()) {
                    aVar.mo23744a();
                    K a = this.f7620a.mo23735a(aVar);
                    if (map.put(a, this.f7621b.mo23735a(aVar)) == null) {
                        aVar.mo23749d();
                    } else {
                        throw new JsonSyntaxException("duplicate key: " + ((Object) a));
                    }
                }
                aVar.mo23749d();
            } else {
                aVar.mo23746b();
                while (aVar.mo23752t()) {
                    JsonReaderInternalAccess.f7567a.mo23760a(aVar);
                    K a2 = this.f7620a.mo23735a(aVar);
                    if (map.put(a2, this.f7621b.mo23735a(aVar)) != null) {
                        throw new JsonSyntaxException("duplicate key: " + ((Object) a2));
                    }
                }
                aVar.mo23750e();
            }
            return map;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Map<K, V> map) {
            if (map == null) {
                cVar.mo23780u();
            } else if (!MapTypeAdapterFactory.this.f7619Q) {
                cVar.mo23765b();
                for (Map.Entry entry : map.entrySet()) {
                    cVar.mo23766b(String.valueOf(entry.getKey()));
                    this.f7621b.mo23736a(cVar, entry.getValue());
                }
                cVar.mo23772d();
            } else {
                ArrayList arrayList = new ArrayList(map.size());
                ArrayList arrayList2 = new ArrayList(map.size());
                int i = 0;
                boolean z = false;
                for (Map.Entry entry2 : map.entrySet()) {
                    JsonElement a = this.f7620a.mo23842a(entry2.getKey());
                    arrayList.add(a);
                    arrayList2.add(entry2.getValue());
                    z |= a.mo23819f() || a.mo23821i();
                }
                if (z) {
                    cVar.mo23761a();
                    int size = arrayList.size();
                    while (i < size) {
                        cVar.mo23761a();
                        Streams.m12420a((JsonElement) arrayList.get(i), cVar);
                        this.f7621b.mo23736a(cVar, arrayList2.get(i));
                        cVar.mo23768c();
                        i++;
                    }
                    cVar.mo23768c();
                    return;
                }
                cVar.mo23765b();
                int size2 = arrayList.size();
                while (i < size2) {
                    cVar.mo23766b(m12481a((JsonElement) arrayList.get(i)));
                    this.f7621b.mo23736a(cVar, arrayList2.get(i));
                    i++;
                }
                cVar.mo23772d();
            }
        }

        /* renamed from: a */
        private String m12481a(JsonElement lVar) {
            if (lVar.mo23822j()) {
                JsonPrimitive d = lVar.mo23818d();
                if (d.mo23839r()) {
                    return String.valueOf(d.mo23836o());
                }
                if (d.mo23838q()) {
                    return Boolean.toString(d.mo23832k());
                }
                if (d.mo23840s()) {
                    return d.mo23837p();
                }
                throw new AssertionError();
            } else if (lVar.mo23820h()) {
                return "null";
            } else {
                throw new AssertionError();
            }
        }
    }

    /* renamed from: a */
    private TypeAdapter<?> m12479a(Gson fVar, Type type) {
        if (type == Boolean.TYPE || type == Boolean.class) {
            return TypeAdapters.f7688f;
        }
        return fVar.mo23787a((TypeToken) TypeToken.m12663a(type));
    }
}
