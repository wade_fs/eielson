package p119e.p144d.p168b;

import java.io.IOException;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.p172n.JsonTreeWriter;

/* renamed from: e.d.b.v */
public abstract class TypeAdapter<T> {
    /* renamed from: a */
    public final TypeAdapter<T> mo23843a() {
        return new C4102a();
    }

    /* renamed from: a */
    public abstract T mo23735a(JsonReader aVar);

    /* renamed from: a */
    public abstract void mo23736a(JsonWriter cVar, Object obj);

    /* renamed from: e.d.b.v$a */
    /* compiled from: TypeAdapter */
    class C4102a extends TypeAdapter<T> {
        C4102a() {
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, T t) {
            if (t == null) {
                cVar.mo23780u();
            } else {
                TypeAdapter.this.mo23736a(cVar, t);
            }
        }

        /* renamed from: a */
        public T mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return TypeAdapter.this.mo23735a(aVar);
            }
            aVar.mo23740B();
            return null;
        }
    }

    /* renamed from: a */
    public final JsonElement mo23842a(Object obj) {
        try {
            JsonTreeWriter fVar = new JsonTreeWriter();
            mo23736a(fVar, obj);
            return fVar.mo23933v();
        } catch (IOException e) {
            throw new JsonIOException(e);
        }
    }
}
