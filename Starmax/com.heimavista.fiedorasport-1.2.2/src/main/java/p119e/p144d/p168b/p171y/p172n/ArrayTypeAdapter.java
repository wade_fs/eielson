package p119e.p144d.p168b.p171y.p172n;

import java.lang.reflect.Array;
import java.lang.reflect.GenericArrayType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.C$Gson$Types;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.a */
public final class ArrayTypeAdapter<E> extends TypeAdapter<Object> {

    /* renamed from: c */
    public static final TypeAdapterFactory f7599c = new C4134a();

    /* renamed from: a */
    private final Class<E> f7600a;

    /* renamed from: b */
    private final TypeAdapter<E> f7601b;

    /* renamed from: e.d.b.y.n.a$a */
    /* compiled from: ArrayTypeAdapter */
    class C4134a implements TypeAdapterFactory {
        C4134a() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            Type b = aVar.mo23981b();
            if (!(b instanceof GenericArrayType) && (!(b instanceof Class) || !((Class) b).isArray())) {
                return null;
            }
            Type d = C$Gson$Types.m12354d(b);
            return new ArrayTypeAdapter(fVar, fVar.mo23787a((TypeToken) TypeToken.m12663a(d)), C$Gson$Types.m12355e(d));
        }
    }

    public ArrayTypeAdapter(Gson fVar, TypeAdapter<E> vVar, Class<E> cls) {
        this.f7601b = new TypeAdapterRuntimeTypeWrapper(fVar, super, cls);
        this.f7600a = cls;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<E>, int]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException} */
    /* renamed from: a */
    public Object mo23735a(JsonReader aVar) {
        if (aVar.mo23742D() == JsonToken.NULL) {
            aVar.mo23740B();
            return null;
        }
        ArrayList arrayList = new ArrayList();
        aVar.mo23744a();
        while (aVar.mo23752t()) {
            arrayList.add(this.f7601b.mo23735a(aVar));
        }
        aVar.mo23749d();
        int size = arrayList.size();
        Object newInstance = Array.newInstance((Class<?>) this.f7600a, size);
        for (int i = 0; i < size; i++) {
            Array.set(newInstance, i, arrayList.get(i));
        }
        return newInstance;
    }

    /* renamed from: a */
    public void mo23736a(JsonWriter cVar, Object obj) {
        if (obj == null) {
            cVar.mo23780u();
            return;
        }
        cVar.mo23761a();
        int length = Array.getLength(obj);
        for (int i = 0; i < length; i++) {
            this.f7601b.mo23736a(cVar, Array.get(obj, i));
        }
        cVar.mo23768c();
    }
}
