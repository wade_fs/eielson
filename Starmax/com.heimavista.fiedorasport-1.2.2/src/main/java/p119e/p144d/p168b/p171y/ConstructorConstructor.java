package p119e.p144d.p168b.p171y;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;
import p119e.p144d.p168b.InstanceCreator;
import p119e.p144d.p168b.JsonIOException;
import p119e.p144d.p168b.p171y.p174o.ReflectionAccessor;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.c */
public final class ConstructorConstructor {

    /* renamed from: a */
    private final Map<Type, InstanceCreator<?>> f7542a;

    /* renamed from: b */
    private final ReflectionAccessor f7543b = ReflectionAccessor.m12656a();

    /* renamed from: e.d.b.y.c$a */
    /* compiled from: ConstructorConstructor */
    class C4106a implements ObjectConstructor<T> {
        C4106a(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new ConcurrentHashMap();
        }
    }

    /* renamed from: e.d.b.y.c$b */
    /* compiled from: ConstructorConstructor */
    class C4107b implements ObjectConstructor<T> {
        C4107b(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new TreeMap();
        }
    }

    /* renamed from: e.d.b.y.c$c */
    /* compiled from: ConstructorConstructor */
    class C4108c implements ObjectConstructor<T> {
        C4108c(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new LinkedHashMap();
        }
    }

    /* renamed from: e.d.b.y.c$d */
    /* compiled from: ConstructorConstructor */
    class C4109d implements ObjectConstructor<T> {
        C4109d(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new LinkedTreeMap();
        }
    }

    /* renamed from: e.d.b.y.c$e */
    /* compiled from: ConstructorConstructor */
    class C4110e implements ObjectConstructor<T> {

        /* renamed from: a */
        private final UnsafeAllocator f7544a = UnsafeAllocator.m12421a();

        /* renamed from: b */
        final /* synthetic */ Class f7545b;

        /* renamed from: c */
        final /* synthetic */ Type f7546c;

        C4110e(ConstructorConstructor cVar, Class cls, Type type) {
            this.f7545b = cls;
            this.f7546c = type;
        }

        /* renamed from: a */
        public T mo23870a() {
            try {
                return this.f7544a.mo23926a(this.f7545b);
            } catch (Exception e) {
                throw new RuntimeException("Unable to invoke no-args constructor for " + this.f7546c + ". Registering an InstanceCreator with Gson for this type may fix this problem.", e);
            }
        }
    }

    /* renamed from: e.d.b.y.c$f */
    /* compiled from: ConstructorConstructor */
    class C4111f implements ObjectConstructor<T> {

        /* renamed from: a */
        final /* synthetic */ InstanceCreator f7547a;

        /* renamed from: b */
        final /* synthetic */ Type f7548b;

        C4111f(ConstructorConstructor cVar, InstanceCreator hVar, Type type) {
            this.f7547a = hVar;
            this.f7548b = type;
        }

        /* renamed from: a */
        public T mo23870a() {
            return this.f7547a.mo23810a(this.f7548b);
        }
    }

    /* renamed from: e.d.b.y.c$g */
    /* compiled from: ConstructorConstructor */
    class C4112g implements ObjectConstructor<T> {

        /* renamed from: a */
        final /* synthetic */ InstanceCreator f7549a;

        /* renamed from: b */
        final /* synthetic */ Type f7550b;

        C4112g(ConstructorConstructor cVar, InstanceCreator hVar, Type type) {
            this.f7549a = hVar;
            this.f7550b = type;
        }

        /* renamed from: a */
        public T mo23870a() {
            return this.f7549a.mo23810a(this.f7550b);
        }
    }

    /* renamed from: e.d.b.y.c$h */
    /* compiled from: ConstructorConstructor */
    class C4113h implements ObjectConstructor<T> {

        /* renamed from: a */
        final /* synthetic */ Constructor f7551a;

        C4113h(ConstructorConstructor cVar, Constructor constructor) {
            this.f7551a = constructor;
        }

        /* renamed from: a */
        public T mo23870a() {
            try {
                return this.f7551a.newInstance(null);
            } catch (InstantiationException e) {
                throw new RuntimeException("Failed to invoke " + this.f7551a + " with no args", e);
            } catch (InvocationTargetException e2) {
                throw new RuntimeException("Failed to invoke " + this.f7551a + " with no args", e2.getTargetException());
            } catch (IllegalAccessException e3) {
                throw new AssertionError(e3);
            }
        }
    }

    /* renamed from: e.d.b.y.c$i */
    /* compiled from: ConstructorConstructor */
    class C4114i implements ObjectConstructor<T> {
        C4114i(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new TreeSet();
        }
    }

    /* renamed from: e.d.b.y.c$j */
    /* compiled from: ConstructorConstructor */
    class C4115j implements ObjectConstructor<T> {

        /* renamed from: a */
        final /* synthetic */ Type f7552a;

        C4115j(ConstructorConstructor cVar, Type type) {
            this.f7552a = type;
        }

        /* renamed from: a */
        public T mo23870a() {
            Type type = this.f7552a;
            if (type instanceof ParameterizedType) {
                Type type2 = ((ParameterizedType) type).getActualTypeArguments()[0];
                if (type2 instanceof Class) {
                    return EnumSet.noneOf((Class) type2);
                }
                throw new JsonIOException("Invalid EnumSet type: " + this.f7552a.toString());
            }
            throw new JsonIOException("Invalid EnumSet type: " + this.f7552a.toString());
        }
    }

    /* renamed from: e.d.b.y.c$k */
    /* compiled from: ConstructorConstructor */
    class C4116k implements ObjectConstructor<T> {
        C4116k(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new LinkedHashSet();
        }
    }

    /* renamed from: e.d.b.y.c$l */
    /* compiled from: ConstructorConstructor */
    class C4117l implements ObjectConstructor<T> {
        C4117l(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new ArrayDeque();
        }
    }

    /* renamed from: e.d.b.y.c$m */
    /* compiled from: ConstructorConstructor */
    class C4118m implements ObjectConstructor<T> {
        C4118m(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new ArrayList();
        }
    }

    /* renamed from: e.d.b.y.c$n */
    /* compiled from: ConstructorConstructor */
    class C4119n implements ObjectConstructor<T> {
        C4119n(ConstructorConstructor cVar) {
        }

        /* renamed from: a */
        public T mo23870a() {
            return new ConcurrentSkipListMap();
        }
    }

    public ConstructorConstructor(Map<Type, InstanceCreator<?>> map) {
        this.f7542a = map;
    }

    /* renamed from: b */
    private <T> ObjectConstructor<T> m12361b(Type type, Class<? super T> cls) {
        return new C4110e(this, cls, type);
    }

    /* renamed from: a */
    public <T> ObjectConstructor<T> mo23868a(TypeToken<T> aVar) {
        Type b = aVar.mo23981b();
        Class<? super T> a = aVar.mo23980a();
        InstanceCreator hVar = this.f7542a.get(b);
        if (hVar != null) {
            return new C4111f(this, hVar, b);
        }
        InstanceCreator hVar2 = this.f7542a.get(a);
        if (hVar2 != null) {
            return new C4112g(this, hVar2, b);
        }
        ObjectConstructor<T> a2 = m12359a(a);
        if (a2 != null) {
            return a2;
        }
        ObjectConstructor<T> a3 = m12360a(b, a);
        if (a3 != null) {
            return a3;
        }
        return m12361b(b, a);
    }

    public String toString() {
        return this.f7542a.toString();
    }

    /* renamed from: a */
    private <T> ObjectConstructor<T> m12359a(Class<? super T> cls) {
        try {
            Constructor<? super T> declaredConstructor = cls.getDeclaredConstructor(new Class[0]);
            if (!declaredConstructor.isAccessible()) {
                this.f7543b.mo23978a(declaredConstructor);
            }
            return new C4113h(this, declaredConstructor);
        } catch (NoSuchMethodException unused) {
            return null;
        }
    }

    /* renamed from: a */
    private <T> ObjectConstructor<T> m12360a(Type type, Class<? super T> cls) {
        if (Collection.class.isAssignableFrom(cls)) {
            if (SortedSet.class.isAssignableFrom(cls)) {
                return new C4114i(this);
            }
            if (EnumSet.class.isAssignableFrom(cls)) {
                return new C4115j(this, type);
            }
            if (Set.class.isAssignableFrom(cls)) {
                return new C4116k(this);
            }
            if (Queue.class.isAssignableFrom(cls)) {
                return new C4117l(this);
            }
            return new C4118m(this);
        } else if (!Map.class.isAssignableFrom(cls)) {
            return null;
        } else {
            if (ConcurrentNavigableMap.class.isAssignableFrom(cls)) {
                return new C4119n(this);
            }
            if (ConcurrentMap.class.isAssignableFrom(cls)) {
                return new C4106a(this);
            }
            if (SortedMap.class.isAssignableFrom(cls)) {
                return new C4107b(this);
            }
            if (!(type instanceof ParameterizedType) || String.class.isAssignableFrom(TypeToken.m12663a(((ParameterizedType) type).getActualTypeArguments()[0]).mo23980a())) {
                return new C4109d(this);
            }
            return new C4108c(this);
        }
    }
}
