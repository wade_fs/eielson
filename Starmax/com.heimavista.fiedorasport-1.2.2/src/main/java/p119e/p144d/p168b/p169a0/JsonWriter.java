package p119e.p144d.p168b.p169a0;

import com.facebook.internal.ServerProtocol;
import com.tencent.bugly.Bugly;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;

/* renamed from: e.d.b.a0.c */
public class JsonWriter implements Closeable, Flushable {

    /* renamed from: Y */
    private static final String[] f7479Y = new String[128];

    /* renamed from: Z */
    private static final String[] f7480Z;

    /* renamed from: P */
    private final Writer f7481P;

    /* renamed from: Q */
    private int[] f7482Q = new int[32];

    /* renamed from: R */
    private int f7483R = 0;

    /* renamed from: S */
    private String f7484S;

    /* renamed from: T */
    private String f7485T;

    /* renamed from: U */
    private boolean f7486U;

    /* renamed from: V */
    private boolean f7487V;

    /* renamed from: W */
    private String f7488W;

    /* renamed from: X */
    private boolean f7489X;

    static {
        for (int i = 0; i <= 31; i++) {
            f7479Y[i] = String.format("\\u%04x", Integer.valueOf(i));
        }
        String[] strArr = f7479Y;
        strArr[34] = "\\\"";
        strArr[92] = "\\\\";
        strArr[9] = "\\t";
        strArr[8] = "\\b";
        strArr[10] = "\\n";
        strArr[13] = "\\r";
        strArr[12] = "\\f";
        f7480Z = (String[]) strArr.clone();
        String[] strArr2 = f7480Z;
        strArr2[60] = "\\u003c";
        strArr2[62] = "\\u003e";
        strArr2[38] = "\\u0026";
        strArr2[61] = "\\u003d";
        strArr2[39] = "\\u0027";
    }

    public JsonWriter(Writer writer) {
        m12219a(6);
        this.f7485T = ":";
        this.f7489X = true;
        if (writer != null) {
            this.f7481P = writer;
            return;
        }
        throw new NullPointerException("out == null");
    }

    /* renamed from: v */
    private void mo23933v() {
        int y = m12225y();
        if (y == 5) {
            this.f7481P.write(44);
        } else if (y != 3) {
            throw new IllegalStateException("Nesting problem.");
        }
        m12224x();
        m12220b(4);
    }

    /* renamed from: w */
    private void m12223w() {
        int y = m12225y();
        if (y == 1) {
            m12220b(2);
            m12224x();
        } else if (y == 2) {
            this.f7481P.append(',');
            m12224x();
        } else if (y != 4) {
            if (y != 6) {
                if (y != 7) {
                    throw new IllegalStateException("Nesting problem.");
                } else if (!this.f7486U) {
                    throw new IllegalStateException("JSON must have only one top-level value.");
                }
            }
            m12220b(7);
        } else {
            this.f7481P.append((CharSequence) this.f7485T);
            m12220b(5);
        }
    }

    /* renamed from: x */
    private void m12224x() {
        if (this.f7484S != null) {
            this.f7481P.write(10);
            int i = this.f7483R;
            for (int i2 = 1; i2 < i; i2++) {
                this.f7481P.write(this.f7484S);
            }
        }
    }

    /* renamed from: y */
    private int m12225y() {
        int i = this.f7483R;
        if (i != 0) {
            return this.f7482Q[i - 1];
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    /* renamed from: z */
    private void m12226z() {
        if (this.f7488W != null) {
            mo23933v();
            m12221e(this.f7488W);
            this.f7488W = null;
        }
    }

    /* renamed from: a */
    public final void mo23764a(boolean z) {
        this.f7487V = z;
    }

    /* renamed from: b */
    public final void mo23767b(boolean z) {
        this.f7486U = z;
    }

    /* renamed from: c */
    public final void mo23769c(String str) {
        if (str.length() == 0) {
            this.f7484S = null;
            this.f7485T = ":";
            return;
        }
        this.f7484S = str;
        this.f7485T = ": ";
    }

    public void close() {
        this.f7481P.close();
        int i = this.f7483R;
        if (i > 1 || (i == 1 && this.f7482Q[i - 1] != 7)) {
            throw new IOException("Incomplete document");
        }
        this.f7483R = 0;
    }

    /* renamed from: d */
    public JsonWriter mo23772d() {
        m12218a(3, 5, '}');
        return this;
    }

    /* renamed from: e */
    public final boolean mo23775e() {
        return this.f7489X;
    }

    public void flush() {
        if (this.f7483R != 0) {
            this.f7481P.flush();
            return;
        }
        throw new IllegalStateException("JsonWriter is closed.");
    }

    /* renamed from: g */
    public final boolean mo23778g() {
        return this.f7487V;
    }

    /* renamed from: t */
    public boolean mo23779t() {
        return this.f7486U;
    }

    /* renamed from: u */
    public JsonWriter mo23780u() {
        if (this.f7488W != null) {
            if (this.f7489X) {
                m12226z();
            } else {
                this.f7488W = null;
                return this;
            }
        }
        m12223w();
        this.f7481P.write("null");
        return this;
    }

    /* renamed from: e */
    private void m12221e(String str) {
        String str2;
        String[] strArr = this.f7487V ? f7480Z : f7479Y;
        this.f7481P.write(34);
        int length = str.length();
        int i = 0;
        for (int i2 = 0; i2 < length; i2++) {
            char charAt = str.charAt(i2);
            if (charAt < 128) {
                str2 = strArr[charAt];
                if (str2 == null) {
                }
            } else if (charAt == 8232) {
                str2 = "\\u2028";
            } else if (charAt == 8233) {
                str2 = "\\u2029";
            }
            if (i < i2) {
                this.f7481P.write(str, i, i2 - i);
            }
            this.f7481P.write(str2);
            i = i2 + 1;
        }
        if (i < length) {
            this.f7481P.write(str, i, length - i);
        }
        this.f7481P.write(34);
    }

    /* renamed from: a */
    public JsonWriter mo23761a() {
        m12226z();
        m12217a(1, '[');
        return this;
    }

    /* renamed from: b */
    public JsonWriter mo23765b() {
        m12226z();
        m12217a(3, '{');
        return this;
    }

    /* renamed from: d */
    public JsonWriter mo23773d(String str) {
        if (str == null) {
            return mo23780u();
        }
        m12226z();
        m12223w();
        m12221e(str);
        return this;
    }

    /* renamed from: g */
    public JsonWriter mo23777g(long j) {
        m12226z();
        m12223w();
        this.f7481P.write(Long.toString(j));
        return this;
    }

    /* renamed from: a */
    private JsonWriter m12217a(int i, char c) {
        m12223w();
        m12219a(i);
        this.f7481P.write(c);
        return this;
    }

    /* renamed from: b */
    private void m12220b(int i) {
        this.f7482Q[this.f7483R - 1] = i;
    }

    /* renamed from: b */
    public JsonWriter mo23766b(String str) {
        if (str == null) {
            throw new NullPointerException("name == null");
        } else if (this.f7488W != null) {
            throw new IllegalStateException();
        } else if (this.f7483R != 0) {
            this.f7488W = str;
            return this;
        } else {
            throw new IllegalStateException("JsonWriter is closed.");
        }
    }

    /* renamed from: c */
    public final void mo23770c(boolean z) {
        this.f7489X = z;
    }

    /* renamed from: d */
    public JsonWriter mo23774d(boolean z) {
        m12226z();
        m12223w();
        this.f7481P.write(z ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : Bugly.SDK_IS_DEV);
        return this;
    }

    /* renamed from: a */
    private JsonWriter m12218a(int i, int i2, char c) {
        int y = m12225y();
        if (y != i2 && y != i) {
            throw new IllegalStateException("Nesting problem.");
        } else if (this.f7488W == null) {
            this.f7483R--;
            if (y == i2) {
                m12224x();
            }
            this.f7481P.write(c);
            return this;
        } else {
            throw new IllegalStateException("Dangling name: " + this.f7488W);
        }
    }

    /* renamed from: c */
    public JsonWriter mo23768c() {
        m12218a(1, 2, ']');
        return this;
    }

    /* renamed from: a */
    private void m12219a(int i) {
        int i2 = this.f7483R;
        int[] iArr = this.f7482Q;
        if (i2 == iArr.length) {
            this.f7482Q = Arrays.copyOf(iArr, i2 * 2);
        }
        int[] iArr2 = this.f7482Q;
        int i3 = this.f7483R;
        this.f7483R = i3 + 1;
        iArr2[i3] = i;
    }

    /* renamed from: a */
    public JsonWriter mo23762a(Boolean bool) {
        if (bool == null) {
            return mo23780u();
        }
        m12226z();
        m12223w();
        this.f7481P.write(bool.booleanValue() ? ServerProtocol.DIALOG_RETURN_SCOPES_TRUE : Bugly.SDK_IS_DEV);
        return this;
    }

    /* renamed from: a */
    public JsonWriter mo23763a(Number number) {
        if (number == null) {
            return mo23780u();
        }
        m12226z();
        String obj = number.toString();
        if (this.f7486U || (!obj.equals("-Infinity") && !obj.equals("Infinity") && !obj.equals("NaN"))) {
            m12223w();
            this.f7481P.append((CharSequence) obj);
            return this;
        }
        throw new IllegalArgumentException("Numeric values must be finite, but was " + number);
    }
}
