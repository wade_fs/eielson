package p119e.p144d.p168b.p171y;

import java.math.BigDecimal;

/* renamed from: e.d.b.y.g */
public final class LazilyParsedNumber extends Number {

    /* renamed from: P */
    private final String f7568P;

    public LazilyParsedNumber(String str) {
        this.f7568P = str;
    }

    private Object writeReplace() {
        return new BigDecimal(this.f7568P);
    }

    public double doubleValue() {
        return Double.parseDouble(this.f7568P);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LazilyParsedNumber)) {
            return false;
        }
        String str = this.f7568P;
        String str2 = ((LazilyParsedNumber) obj).f7568P;
        if (str == str2 || str.equals(str2)) {
            return true;
        }
        return false;
    }

    public float floatValue() {
        return Float.parseFloat(this.f7568P);
    }

    public int hashCode() {
        return this.f7568P.hashCode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x000e, code lost:
        return (int) java.lang.Long.parseLong(r2.f7568P);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        return new java.math.BigDecimal(r2.f7568P).intValue();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0007 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public int intValue() {
        /*
            r2 = this;
            java.lang.String r0 = r2.f7568P     // Catch:{ NumberFormatException -> 0x0007 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0007 }
            return r0
        L_0x0007:
            java.lang.String r0 = r2.f7568P     // Catch:{ NumberFormatException -> 0x000f }
            long r0 = java.lang.Long.parseLong(r0)     // Catch:{ NumberFormatException -> 0x000f }
            int r1 = (int) r0
            return r1
        L_0x000f:
            java.math.BigDecimal r0 = new java.math.BigDecimal
            java.lang.String r1 = r2.f7568P
            r0.<init>(r1)
            int r0 = r0.intValue()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.LazilyParsedNumber.intValue():int");
    }

    public long longValue() {
        try {
            return Long.parseLong(this.f7568P);
        } catch (NumberFormatException unused) {
            return new BigDecimal(this.f7568P).longValue();
        }
    }

    public String toString() {
        return this.f7568P;
    }
}
