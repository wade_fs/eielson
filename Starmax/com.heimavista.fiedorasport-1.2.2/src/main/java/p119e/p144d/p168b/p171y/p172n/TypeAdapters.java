package p119e.p144d.p168b.p171y.p172n;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicIntegerArray;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonArray;
import p119e.p144d.p168b.JsonElement;
import p119e.p144d.p168b.JsonIOException;
import p119e.p144d.p168b.JsonNull;
import p119e.p144d.p168b.JsonObject;
import p119e.p144d.p168b.JsonPrimitive;
import p119e.p144d.p168b.JsonSyntaxException;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p170x.SerializedName;
import p119e.p144d.p168b.p171y.LazilyParsedNumber;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.n */
public final class TypeAdapters {

    /* renamed from: A */
    public static final TypeAdapter<String> f7657A = new C4162g();

    /* renamed from: B */
    public static final TypeAdapter<BigDecimal> f7658B = new C4164h();

    /* renamed from: C */
    public static final TypeAdapter<BigInteger> f7659C = new C4166i();

    /* renamed from: D */
    public static final TypeAdapterFactory f7660D = m12519a(String.class, f7657A);

    /* renamed from: E */
    public static final TypeAdapter<StringBuilder> f7661E = new C4168j();

    /* renamed from: F */
    public static final TypeAdapterFactory f7662F = m12519a(StringBuilder.class, f7661E);

    /* renamed from: G */
    public static final TypeAdapter<StringBuffer> f7663G = new C4171l();

    /* renamed from: H */
    public static final TypeAdapterFactory f7664H = m12519a(StringBuffer.class, f7663G);

    /* renamed from: I */
    public static final TypeAdapter<URL> f7665I = new C4172m();

    /* renamed from: J */
    public static final TypeAdapterFactory f7666J = m12519a(URL.class, f7665I);

    /* renamed from: K */
    public static final TypeAdapter<URI> f7667K = new C4173n();

    /* renamed from: L */
    public static final TypeAdapterFactory f7668L = m12519a(URI.class, f7667K);

    /* renamed from: M */
    public static final TypeAdapter<InetAddress> f7669M = new C4174o();

    /* renamed from: N */
    public static final TypeAdapterFactory f7670N = m12521b(InetAddress.class, f7669M);

    /* renamed from: O */
    public static final TypeAdapter<UUID> f7671O = new C4175p();

    /* renamed from: P */
    public static final TypeAdapterFactory f7672P = m12519a(UUID.class, f7671O);

    /* renamed from: Q */
    public static final TypeAdapter<Currency> f7673Q = new C4176q().mo23843a();

    /* renamed from: R */
    public static final TypeAdapterFactory f7674R = m12519a(Currency.class, f7673Q);

    /* renamed from: S */
    public static final TypeAdapterFactory f7675S = new C4177r();

    /* renamed from: T */
    public static final TypeAdapter<Calendar> f7676T = new C4179s();

    /* renamed from: U */
    public static final TypeAdapterFactory f7677U = m12522b(Calendar.class, GregorianCalendar.class, f7676T);

    /* renamed from: V */
    public static final TypeAdapter<Locale> f7678V = new C4180t();

    /* renamed from: W */
    public static final TypeAdapterFactory f7679W = m12519a(Locale.class, f7678V);

    /* renamed from: X */
    public static final TypeAdapter<JsonElement> f7680X = new C4181u();

    /* renamed from: Y */
    public static final TypeAdapterFactory f7681Y = m12521b(JsonElement.class, f7680X);

    /* renamed from: Z */
    public static final TypeAdapterFactory f7682Z = new C4183w();

    /* renamed from: a */
    public static final TypeAdapter<Class> f7683a = new C4170k().mo23843a();

    /* renamed from: b */
    public static final TypeAdapterFactory f7684b = m12519a(Class.class, f7683a);

    /* renamed from: c */
    public static final TypeAdapter<BitSet> f7685c = new C4182v().mo23843a();

    /* renamed from: d */
    public static final TypeAdapterFactory f7686d = m12519a(BitSet.class, f7685c);

    /* renamed from: e */
    public static final TypeAdapter<Boolean> f7687e = new C4155c0();

    /* renamed from: f */
    public static final TypeAdapter<Boolean> f7688f = new C4157d0();

    /* renamed from: g */
    public static final TypeAdapterFactory f7689g = m12520a(Boolean.TYPE, Boolean.class, f7687e);

    /* renamed from: h */
    public static final TypeAdapter<Number> f7690h = new C4159e0();

    /* renamed from: i */
    public static final TypeAdapterFactory f7691i = m12520a(Byte.TYPE, Byte.class, f7690h);

    /* renamed from: j */
    public static final TypeAdapter<Number> f7692j = new C4161f0();

    /* renamed from: k */
    public static final TypeAdapterFactory f7693k = m12520a(Short.TYPE, Short.class, f7692j);

    /* renamed from: l */
    public static final TypeAdapter<Number> f7694l = new C4163g0();

    /* renamed from: m */
    public static final TypeAdapterFactory f7695m = m12520a(Integer.TYPE, Integer.class, f7694l);

    /* renamed from: n */
    public static final TypeAdapter<AtomicInteger> f7696n = new C4165h0().mo23843a();

    /* renamed from: o */
    public static final TypeAdapterFactory f7697o = m12519a(AtomicInteger.class, f7696n);

    /* renamed from: p */
    public static final TypeAdapter<AtomicBoolean> f7698p = new C4167i0().mo23843a();

    /* renamed from: q */
    public static final TypeAdapterFactory f7699q = m12519a(AtomicBoolean.class, f7698p);

    /* renamed from: r */
    public static final TypeAdapter<AtomicIntegerArray> f7700r = new C4149a().mo23843a();

    /* renamed from: s */
    public static final TypeAdapterFactory f7701s = m12519a(AtomicIntegerArray.class, f7700r);

    /* renamed from: t */
    public static final TypeAdapter<Number> f7702t = new C4152b();

    /* renamed from: u */
    public static final TypeAdapter<Number> f7703u = new C4154c();

    /* renamed from: v */
    public static final TypeAdapter<Number> f7704v = new C4156d();

    /* renamed from: w */
    public static final TypeAdapter<Number> f7705w = new C4158e();

    /* renamed from: x */
    public static final TypeAdapterFactory f7706x = m12519a(Number.class, f7705w);

    /* renamed from: y */
    public static final TypeAdapter<Character> f7707y = new C4160f();

    /* renamed from: z */
    public static final TypeAdapterFactory f7708z = m12520a(Character.TYPE, Character.class, f7707y);

    /* renamed from: e.d.b.y.n.n$a0 */
    /* compiled from: TypeAdapters */
    class C4150a0 implements TypeAdapterFactory {

        /* renamed from: P */
        final /* synthetic */ Class f7709P;

        /* renamed from: Q */
        final /* synthetic */ TypeAdapter f7710Q;

        /* renamed from: e.d.b.y.n.n$a0$a */
        /* compiled from: TypeAdapters */
        class C4151a extends TypeAdapter<T1> {

            /* renamed from: a */
            final /* synthetic */ Class f7711a;

            C4151a(Class cls) {
                this.f7711a = cls;
            }

            /* renamed from: a */
            public void mo23736a(JsonWriter cVar, T1 t1) {
                C4150a0.this.f7710Q.mo23736a(cVar, t1);
            }

            /* renamed from: a */
            public T1 mo23735a(JsonReader aVar) {
                T1 a = C4150a0.this.f7710Q.mo23735a(aVar);
                if (a == null || this.f7711a.isInstance(a)) {
                    return a;
                }
                throw new JsonSyntaxException("Expected a " + this.f7711a.getName() + " but was " + a.getClass().getName());
            }
        }

        C4150a0(Class cls, TypeAdapter vVar) {
            this.f7709P = cls;
            this.f7710Q = vVar;
        }

        /* renamed from: a */
        public <T2> TypeAdapter<T2> mo23844a(Gson fVar, TypeToken<T2> aVar) {
            Class<? super T2> a = aVar.mo23980a();
            if (!this.f7709P.isAssignableFrom(a)) {
                return null;
            }
            return new C4151a(a);
        }

        public String toString() {
            return "Factory[typeHierarchy=" + this.f7709P.getName() + ",adapter=" + this.f7710Q + "]";
        }
    }

    /* renamed from: e.d.b.y.n.n$b0 */
    /* compiled from: TypeAdapters */
    static /* synthetic */ class C4153b0 {

        /* renamed from: a */
        static final /* synthetic */ int[] f7713a = new int[JsonToken.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|(3:19|20|22)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|22) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                e.d.b.a0.b[] r0 = p119e.p144d.p168b.p169a0.JsonToken.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a = r0
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x0014 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.NUMBER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x001f }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.BOOLEAN     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x002a }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.STRING     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x0035 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.NULL     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x0040 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.BEGIN_ARRAY     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x004b }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.BEGIN_OBJECT     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x0056 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.END_DOCUMENT     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x0062 }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.NAME     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x006e }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.END_OBJECT     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a     // Catch:{ NoSuchFieldError -> 0x007a }
                e.d.b.a0.b r1 = p119e.p144d.p168b.p169a0.JsonToken.END_ARRAY     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.<clinit>():void");
        }
    }

    /* renamed from: e.d.b.y.n.n$k */
    /* compiled from: TypeAdapters */
    class C4170k extends TypeAdapter<Class> {
        C4170k() {
        }

        /* renamed from: a */
        public /* bridge */ /* synthetic */ void mo23736a(JsonWriter cVar, Object obj) {
            mo23963a(cVar, (Class) obj);
            throw null;
        }

        /* renamed from: a */
        public void mo23963a(JsonWriter cVar, Class cls) {
            throw new UnsupportedOperationException("Attempted to serialize java.lang.Class: " + cls.getName() + ". Forgot to register a type adapter?");
        }

        /* renamed from: a */
        public Class mo23735a(JsonReader aVar) {
            throw new UnsupportedOperationException("Attempted to deserialize a java.lang.Class. Forgot to register a type adapter?");
        }
    }

    /* renamed from: e.d.b.y.n.n$r */
    /* compiled from: TypeAdapters */
    class C4177r implements TypeAdapterFactory {
        C4177r() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            if (aVar.mo23980a() != Timestamp.class) {
                return null;
            }
            return new C4178a(this, fVar.mo23788a(Date.class));
        }

        /* renamed from: e.d.b.y.n.n$r$a */
        /* compiled from: TypeAdapters */
        class C4178a extends TypeAdapter<Timestamp> {

            /* renamed from: a */
            final /* synthetic */ TypeAdapter f7716a;

            C4178a(C4177r rVar, TypeAdapter vVar) {
                this.f7716a = super;
            }

            /* renamed from: a */
            public Timestamp mo23735a(JsonReader aVar) {
                Date date = (Date) this.f7716a.mo23735a(aVar);
                if (date != null) {
                    return new Timestamp(date.getTime());
                }
                return null;
            }

            /* renamed from: a */
            public void mo23736a(JsonWriter cVar, Timestamp timestamp) {
                this.f7716a.mo23736a(cVar, timestamp);
            }
        }
    }

    /* renamed from: e.d.b.y.n.n$w */
    /* compiled from: TypeAdapters */
    class C4183w implements TypeAdapterFactory {
        C4183w() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            Class<? super T> a = aVar.mo23980a();
            if (!Enum.class.isAssignableFrom(a) || a == Enum.class) {
                return null;
            }
            if (!a.isEnum()) {
                a = a.getSuperclass();
            }
            return new C4169j0(a);
        }
    }

    /* renamed from: e.d.b.y.n.n$x */
    /* compiled from: TypeAdapters */
    class C4184x implements TypeAdapterFactory {

        /* renamed from: P */
        final /* synthetic */ Class f7717P;

        /* renamed from: Q */
        final /* synthetic */ TypeAdapter f7718Q;

        C4184x(Class cls, TypeAdapter vVar) {
            this.f7717P = cls;
            this.f7718Q = vVar;
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            if (aVar.mo23980a() == this.f7717P) {
                return this.f7718Q;
            }
            return null;
        }

        public String toString() {
            return "Factory[type=" + this.f7717P.getName() + ",adapter=" + this.f7718Q + "]";
        }
    }

    /* renamed from: e.d.b.y.n.n$y */
    /* compiled from: TypeAdapters */
    class C4185y implements TypeAdapterFactory {

        /* renamed from: P */
        final /* synthetic */ Class f7719P;

        /* renamed from: Q */
        final /* synthetic */ Class f7720Q;

        /* renamed from: R */
        final /* synthetic */ TypeAdapter f7721R;

        C4185y(Class cls, Class cls2, TypeAdapter vVar) {
            this.f7719P = cls;
            this.f7720Q = cls2;
            this.f7721R = vVar;
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            Class<? super T> a = aVar.mo23980a();
            if (a == this.f7719P || a == this.f7720Q) {
                return this.f7721R;
            }
            return null;
        }

        public String toString() {
            return "Factory[type=" + this.f7720Q.getName() + "+" + this.f7719P.getName() + ",adapter=" + this.f7721R + "]";
        }
    }

    /* renamed from: e.d.b.y.n.n$z */
    /* compiled from: TypeAdapters */
    class C4186z implements TypeAdapterFactory {

        /* renamed from: P */
        final /* synthetic */ Class f7722P;

        /* renamed from: Q */
        final /* synthetic */ Class f7723Q;

        /* renamed from: R */
        final /* synthetic */ TypeAdapter f7724R;

        C4186z(Class cls, Class cls2, TypeAdapter vVar) {
            this.f7722P = cls;
            this.f7723Q = cls2;
            this.f7724R = vVar;
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            Class<? super T> a = aVar.mo23980a();
            if (a == this.f7722P || a == this.f7723Q) {
                return this.f7724R;
            }
            return null;
        }

        public String toString() {
            return "Factory[type=" + this.f7722P.getName() + "+" + this.f7723Q.getName() + ",adapter=" + this.f7724R + "]";
        }
    }

    /* renamed from: a */
    public static <TT> TypeAdapterFactory m12519a(Class<TT> cls, TypeAdapter<TT> vVar) {
        return new C4184x(cls, vVar);
    }

    /* renamed from: b */
    public static <TT> TypeAdapterFactory m12522b(Class<TT> cls, Class<? extends TT> cls2, TypeAdapter<? super TT> vVar) {
        return new C4186z(cls, cls2, vVar);
    }

    /* renamed from: e.d.b.y.n.n$a */
    /* compiled from: TypeAdapters */
    class C4149a extends TypeAdapter<AtomicIntegerArray> {
        C4149a() {
        }

        /* renamed from: a */
        public AtomicIntegerArray mo23735a(JsonReader aVar) {
            ArrayList arrayList = new ArrayList();
            aVar.mo23744a();
            while (aVar.mo23752t()) {
                try {
                    arrayList.add(Integer.valueOf(aVar.mo23758y()));
                } catch (NumberFormatException e) {
                    throw new JsonSyntaxException(e);
                }
            }
            aVar.mo23749d();
            int size = arrayList.size();
            AtomicIntegerArray atomicIntegerArray = new AtomicIntegerArray(size);
            for (int i = 0; i < size; i++) {
                atomicIntegerArray.set(i, ((Integer) arrayList.get(i)).intValue());
            }
            return atomicIntegerArray;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, AtomicIntegerArray atomicIntegerArray) {
            cVar.mo23761a();
            int length = atomicIntegerArray.length();
            for (int i = 0; i < length; i++) {
                cVar.mo23777g((long) atomicIntegerArray.get(i));
            }
            cVar.mo23768c();
        }
    }

    /* renamed from: e.d.b.y.n.n$b */
    /* compiled from: TypeAdapters */
    class C4152b extends TypeAdapter<Number> {
        C4152b() {
        }

        /* renamed from: a */
        public Number m12531a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                return Long.valueOf(aVar.mo23759z());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$c */
    /* compiled from: TypeAdapters */
    class C4154c extends TypeAdapter<Number> {
        C4154c() {
        }

        /* renamed from: a */
        public Number m12535a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return Float.valueOf((float) aVar.mo23757x());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$c0 */
    /* compiled from: TypeAdapters */
    class C4155c0 extends TypeAdapter<Boolean> {
        C4155c0() {
        }

        /* renamed from: a */
        public Boolean m12539a(JsonReader aVar) {
            JsonToken D = aVar.mo23742D();
            if (D == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            } else if (D == JsonToken.STRING) {
                return Boolean.valueOf(Boolean.parseBoolean(aVar.mo23741C()));
            } else {
                return Boolean.valueOf(aVar.mo23756w());
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Boolean bool) {
            cVar.mo23762a(bool);
        }
    }

    /* renamed from: e.d.b.y.n.n$d */
    /* compiled from: TypeAdapters */
    class C4156d extends TypeAdapter<Number> {
        C4156d() {
        }

        /* renamed from: a */
        public Number m12543a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return Double.valueOf(aVar.mo23757x());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$d0 */
    /* compiled from: TypeAdapters */
    class C4157d0 extends TypeAdapter<Boolean> {
        C4157d0() {
        }

        /* renamed from: a */
        public Boolean m12547a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return Boolean.valueOf(aVar.mo23741C());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Boolean bool) {
            cVar.mo23773d(bool == null ? "null" : bool.toString());
        }
    }

    /* renamed from: e.d.b.y.n.n$e */
    /* compiled from: TypeAdapters */
    class C4158e extends TypeAdapter<Number> {
        C4158e() {
        }

        /* renamed from: a */
        public Number m12551a(JsonReader aVar) {
            JsonToken D = aVar.mo23742D();
            int i = C4153b0.f7713a[D.ordinal()];
            if (i == 1 || i == 3) {
                return new LazilyParsedNumber(aVar.mo23741C());
            }
            if (i == 4) {
                aVar.mo23740B();
                return null;
            }
            throw new JsonSyntaxException("Expecting number, got: " + D);
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$e0 */
    /* compiled from: TypeAdapters */
    class C4159e0 extends TypeAdapter<Number> {
        C4159e0() {
        }

        /* renamed from: a */
        public Number m12555a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                return Byte.valueOf((byte) aVar.mo23758y());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$f */
    /* compiled from: TypeAdapters */
    class C4160f extends TypeAdapter<Character> {
        C4160f() {
        }

        /* renamed from: a */
        public Character m12559a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            String C = aVar.mo23741C();
            if (C.length() == 1) {
                return Character.valueOf(C.charAt(0));
            }
            throw new JsonSyntaxException("Expecting character, got: " + C);
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Character ch) {
            cVar.mo23773d(ch == null ? null : String.valueOf(ch));
        }
    }

    /* renamed from: e.d.b.y.n.n$f0 */
    /* compiled from: TypeAdapters */
    class C4161f0 extends TypeAdapter<Number> {
        C4161f0() {
        }

        /* renamed from: a */
        public Number m12563a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                return Short.valueOf((short) aVar.mo23758y());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$g */
    /* compiled from: TypeAdapters */
    class C4162g extends TypeAdapter<String> {
        C4162g() {
        }

        /* renamed from: a */
        public String mo23735a(JsonReader aVar) {
            JsonToken D = aVar.mo23742D();
            if (D == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            } else if (D == JsonToken.BOOLEAN) {
                return Boolean.toString(aVar.mo23756w());
            } else {
                return aVar.mo23741C();
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, String str) {
            cVar.mo23773d(str);
        }
    }

    /* renamed from: e.d.b.y.n.n$g0 */
    /* compiled from: TypeAdapters */
    class C4163g0 extends TypeAdapter<Number> {
        C4163g0() {
        }

        /* renamed from: a */
        public Number m12571a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                return Integer.valueOf(aVar.mo23758y());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Number number) {
            cVar.mo23763a(number);
        }
    }

    /* renamed from: e.d.b.y.n.n$h */
    /* compiled from: TypeAdapters */
    class C4164h extends TypeAdapter<BigDecimal> {
        C4164h() {
        }

        /* renamed from: a */
        public BigDecimal mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                return new BigDecimal(aVar.mo23741C());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, BigDecimal bigDecimal) {
            cVar.mo23763a(bigDecimal);
        }
    }

    /* renamed from: e.d.b.y.n.n$h0 */
    /* compiled from: TypeAdapters */
    class C4165h0 extends TypeAdapter<AtomicInteger> {
        C4165h0() {
        }

        /* renamed from: a */
        public AtomicInteger mo23735a(JsonReader aVar) {
            try {
                return new AtomicInteger(aVar.mo23758y());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, AtomicInteger atomicInteger) {
            cVar.mo23777g((long) atomicInteger.get());
        }
    }

    /* renamed from: e.d.b.y.n.n$i */
    /* compiled from: TypeAdapters */
    class C4166i extends TypeAdapter<BigInteger> {
        C4166i() {
        }

        /* renamed from: a */
        public BigInteger mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                return new BigInteger(aVar.mo23741C());
            } catch (NumberFormatException e) {
                throw new JsonSyntaxException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, BigInteger bigInteger) {
            cVar.mo23763a(bigInteger);
        }
    }

    /* renamed from: e.d.b.y.n.n$i0 */
    /* compiled from: TypeAdapters */
    class C4167i0 extends TypeAdapter<AtomicBoolean> {
        C4167i0() {
        }

        /* renamed from: a */
        public AtomicBoolean mo23735a(JsonReader aVar) {
            return new AtomicBoolean(aVar.mo23756w());
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, AtomicBoolean atomicBoolean) {
            cVar.mo23774d(atomicBoolean.get());
        }
    }

    /* renamed from: e.d.b.y.n.n$j */
    /* compiled from: TypeAdapters */
    class C4168j extends TypeAdapter<StringBuilder> {
        C4168j() {
        }

        /* renamed from: a */
        public StringBuilder mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return new StringBuilder(aVar.mo23741C());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, StringBuilder sb) {
            cVar.mo23773d(sb == null ? null : sb.toString());
        }
    }

    /* renamed from: e.d.b.y.n.n$j0 */
    /* compiled from: TypeAdapters */
    private static final class C4169j0<T extends Enum<T>> extends TypeAdapter<T> {

        /* renamed from: a */
        private final Map<String, T> f7714a = new HashMap();

        /* renamed from: b */
        private final Map<T, String> f7715b = new HashMap();

        public C4169j0(Class<T> cls) {
            try {
                Enum[] enumArr = (Enum[]) cls.getEnumConstants();
                for (Enum enumR : enumArr) {
                    String name = enumR.name();
                    SerializedName cVar = (SerializedName) cls.getField(name).getAnnotation(SerializedName.class);
                    if (cVar != null) {
                        name = cVar.value();
                        for (String str : cVar.alternate()) {
                            this.f7714a.put(str, enumR);
                        }
                    }
                    this.f7714a.put(name, enumR);
                    this.f7715b.put(enumR, name);
                }
            } catch (NoSuchFieldException e) {
                throw new AssertionError(e);
            }
        }

        /* renamed from: a */
        public T m12595a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return (Enum) this.f7714a.get(aVar.mo23741C());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, T t) {
            cVar.mo23773d(t == null ? null : this.f7715b.get(t));
        }
    }

    /* renamed from: e.d.b.y.n.n$l */
    /* compiled from: TypeAdapters */
    class C4171l extends TypeAdapter<StringBuffer> {
        C4171l() {
        }

        /* renamed from: a */
        public StringBuffer mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return new StringBuffer(aVar.mo23741C());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, StringBuffer stringBuffer) {
            cVar.mo23773d(stringBuffer == null ? null : stringBuffer.toString());
        }
    }

    /* renamed from: e.d.b.y.n.n$m */
    /* compiled from: TypeAdapters */
    class C4172m extends TypeAdapter<URL> {
        C4172m() {
        }

        /* renamed from: a */
        public URL mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            String C = aVar.mo23741C();
            if ("null".equals(C)) {
                return null;
            }
            return new URL(C);
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, URL url) {
            cVar.mo23773d(url == null ? null : url.toExternalForm());
        }
    }

    /* renamed from: e.d.b.y.n.n$n */
    /* compiled from: TypeAdapters */
    class C4173n extends TypeAdapter<URI> {
        C4173n() {
        }

        /* renamed from: a */
        public URI mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            try {
                String C = aVar.mo23741C();
                if ("null".equals(C)) {
                    return null;
                }
                return new URI(C);
            } catch (URISyntaxException e) {
                throw new JsonIOException(e);
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, URI uri) {
            cVar.mo23773d(uri == null ? null : uri.toASCIIString());
        }
    }

    /* renamed from: e.d.b.y.n.n$o */
    /* compiled from: TypeAdapters */
    class C4174o extends TypeAdapter<InetAddress> {
        C4174o() {
        }

        /* renamed from: a */
        public InetAddress mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return InetAddress.getByName(aVar.mo23741C());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, InetAddress inetAddress) {
            cVar.mo23773d(inetAddress == null ? null : inetAddress.getHostAddress());
        }
    }

    /* renamed from: e.d.b.y.n.n$p */
    /* compiled from: TypeAdapters */
    class C4175p extends TypeAdapter<UUID> {
        C4175p() {
        }

        /* renamed from: a */
        public UUID mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() != JsonToken.NULL) {
                return UUID.fromString(aVar.mo23741C());
            }
            aVar.mo23740B();
            return null;
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, UUID uuid) {
            cVar.mo23773d(uuid == null ? null : uuid.toString());
        }
    }

    /* renamed from: e.d.b.y.n.n$q */
    /* compiled from: TypeAdapters */
    class C4176q extends TypeAdapter<Currency> {
        C4176q() {
        }

        /* renamed from: a */
        public Currency mo23735a(JsonReader aVar) {
            return Currency.getInstance(aVar.mo23741C());
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Currency currency) {
            cVar.mo23773d(currency.getCurrencyCode());
        }
    }

    /* renamed from: e.d.b.y.n.n$s */
    /* compiled from: TypeAdapters */
    class C4179s extends TypeAdapter<Calendar> {
        C4179s() {
        }

        /* renamed from: a */
        public Calendar mo23735a(JsonReader aVar) {
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            aVar.mo23746b();
            int i = 0;
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            int i5 = 0;
            int i6 = 0;
            while (aVar.mo23742D() != JsonToken.END_OBJECT) {
                String A = aVar.mo23739A();
                int y = aVar.mo23758y();
                if ("year".equals(A)) {
                    i = y;
                } else if ("month".equals(A)) {
                    i2 = y;
                } else if ("dayOfMonth".equals(A)) {
                    i3 = y;
                } else if ("hourOfDay".equals(A)) {
                    i4 = y;
                } else if ("minute".equals(A)) {
                    i5 = y;
                } else if ("second".equals(A)) {
                    i6 = y;
                }
            }
            aVar.mo23750e();
            return new GregorianCalendar(i, i2, i3, i4, i5, i6);
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Calendar calendar) {
            if (calendar == null) {
                cVar.mo23780u();
                return;
            }
            cVar.mo23765b();
            cVar.mo23766b("year");
            cVar.mo23777g((long) calendar.get(1));
            cVar.mo23766b("month");
            cVar.mo23777g((long) calendar.get(2));
            cVar.mo23766b("dayOfMonth");
            cVar.mo23777g((long) calendar.get(5));
            cVar.mo23766b("hourOfDay");
            cVar.mo23777g((long) calendar.get(11));
            cVar.mo23766b("minute");
            cVar.mo23777g((long) calendar.get(12));
            cVar.mo23766b("second");
            cVar.mo23777g((long) calendar.get(13));
            cVar.mo23772d();
        }
    }

    /* renamed from: e.d.b.y.n.n$t */
    /* compiled from: TypeAdapters */
    class C4180t extends TypeAdapter<Locale> {
        C4180t() {
        }

        /* renamed from: a */
        public Locale mo23735a(JsonReader aVar) {
            String str = null;
            if (aVar.mo23742D() == JsonToken.NULL) {
                aVar.mo23740B();
                return null;
            }
            StringTokenizer stringTokenizer = new StringTokenizer(aVar.mo23741C(), "_");
            String nextToken = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            String nextToken2 = stringTokenizer.hasMoreElements() ? stringTokenizer.nextToken() : null;
            if (stringTokenizer.hasMoreElements()) {
                str = stringTokenizer.nextToken();
            }
            if (nextToken2 == null && str == null) {
                return new Locale(nextToken);
            }
            if (str == null) {
                return new Locale(nextToken, nextToken2);
            }
            return new Locale(nextToken, nextToken2, str);
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, Locale locale) {
            cVar.mo23773d(locale == null ? null : locale.toString());
        }
    }

    /* renamed from: e.d.b.y.n.n$u */
    /* compiled from: TypeAdapters */
    class C4181u extends TypeAdapter<JsonElement> {
        C4181u() {
        }

        /* renamed from: a */
        public JsonElement m12640a(JsonReader aVar) {
            switch (C4153b0.f7713a[aVar.mo23742D().ordinal()]) {
                case 1:
                    return new JsonPrimitive(new LazilyParsedNumber(aVar.mo23741C()));
                case 2:
                    return new JsonPrimitive(Boolean.valueOf(aVar.mo23756w()));
                case 3:
                    return new JsonPrimitive(aVar.mo23741C());
                case 4:
                    aVar.mo23740B();
                    return JsonNull.f7528a;
                case 5:
                    JsonArray iVar = new JsonArray();
                    aVar.mo23744a();
                    while (aVar.mo23752t()) {
                        iVar.mo23811a(m12640a(aVar));
                    }
                    aVar.mo23749d();
                    return iVar;
                case 6:
                    JsonObject oVar = new JsonObject();
                    aVar.mo23746b();
                    while (aVar.mo23752t()) {
                        oVar.mo23826a(aVar.mo23739A(), m12640a(aVar));
                    }
                    aVar.mo23750e();
                    return oVar;
                default:
                    throw new IllegalArgumentException();
            }
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, JsonElement lVar) {
            if (lVar == null || lVar.mo23820h()) {
                cVar.mo23780u();
            } else if (lVar.mo23822j()) {
                JsonPrimitive d = lVar.mo23818d();
                if (d.mo23839r()) {
                    cVar.mo23763a(d.mo23836o());
                } else if (d.mo23838q()) {
                    cVar.mo23774d(d.mo23832k());
                } else {
                    cVar.mo23773d(d.mo23837p());
                }
            } else if (lVar.mo23819f()) {
                cVar.mo23761a();
                Iterator<JsonElement> it = lVar.mo23816b().iterator();
                while (it.hasNext()) {
                    mo23736a(cVar, it.next());
                }
                cVar.mo23768c();
            } else if (lVar.mo23821i()) {
                cVar.mo23765b();
                for (Map.Entry entry : lVar.mo23817c().mo23829k()) {
                    cVar.mo23766b((String) entry.getKey());
                    mo23736a(cVar, (JsonElement) entry.getValue());
                }
                cVar.mo23772d();
            } else {
                throw new IllegalArgumentException("Couldn't write " + lVar.getClass());
            }
        }
    }

    /* renamed from: e.d.b.y.n.n$v */
    /* compiled from: TypeAdapters */
    class C4182v extends TypeAdapter<BitSet> {
        C4182v() {
        }

        /* JADX WARNING: Code restructure failed: missing block: B:12:0x002b, code lost:
            if (java.lang.Integer.parseInt(r1) != 0) goto L_0x0069;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:21:0x0067, code lost:
            if (r8.mo23758y() != 0) goto L_0x0069;
         */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x006b  */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x006e A[SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.util.BitSet mo23735a(p119e.p144d.p168b.p169a0.JsonReader r8) {
            /*
                r7 = this;
                java.util.BitSet r0 = new java.util.BitSet
                r0.<init>()
                r8.mo23744a()
                e.d.b.a0.b r1 = r8.mo23742D()
                r2 = 0
                r3 = 0
            L_0x000e:
                e.d.b.a0.b r4 = p119e.p144d.p168b.p169a0.JsonToken.END_ARRAY
                if (r1 == r4) goto L_0x0075
                int[] r4 = p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4153b0.f7713a
                int r5 = r1.ordinal()
                r4 = r4[r5]
                r5 = 1
                if (r4 == r5) goto L_0x0063
                r6 = 2
                if (r4 == r6) goto L_0x005e
                r6 = 3
                if (r4 != r6) goto L_0x0047
                java.lang.String r1 = r8.mo23741C()
                int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x0030 }
                if (r1 == 0) goto L_0x002e
                goto L_0x0069
            L_0x002e:
                r5 = 0
                goto L_0x0069
            L_0x0030:
                e.d.b.t r8 = new e.d.b.t
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Error: Expecting: bitset number value (1, 0), Found: "
                r0.append(r2)
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r8.<init>(r0)
                throw r8
            L_0x0047:
                e.d.b.t r8 = new e.d.b.t
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                java.lang.String r2 = "Invalid bitset value type: "
                r0.append(r2)
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r8.<init>(r0)
                throw r8
            L_0x005e:
                boolean r5 = r8.mo23756w()
                goto L_0x0069
            L_0x0063:
                int r1 = r8.mo23758y()
                if (r1 == 0) goto L_0x002e
            L_0x0069:
                if (r5 == 0) goto L_0x006e
                r0.set(r3)
            L_0x006e:
                int r3 = r3 + 1
                e.d.b.a0.b r1 = r8.mo23742D()
                goto L_0x000e
            L_0x0075:
                r8.mo23749d()
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.p172n.TypeAdapters.C4182v.mo23735a(e.d.b.a0.a):java.util.BitSet");
        }

        /* renamed from: a */
        public void mo23736a(JsonWriter cVar, BitSet bitSet) {
            cVar.mo23761a();
            int length = bitSet.length();
            for (int i = 0; i < length; i++) {
                cVar.mo23777g(bitSet.get(i) ? 1 : 0);
            }
            cVar.mo23768c();
        }
    }

    /* renamed from: a */
    public static <TT> TypeAdapterFactory m12520a(Class<TT> cls, Class<TT> cls2, TypeAdapter<? super TT> vVar) {
        return new C4185y(cls, cls2, vVar);
    }

    /* renamed from: b */
    public static <T1> TypeAdapterFactory m12521b(Class<T1> cls, TypeAdapter<T1> vVar) {
        return new C4150a0(cls, vVar);
    }
}
