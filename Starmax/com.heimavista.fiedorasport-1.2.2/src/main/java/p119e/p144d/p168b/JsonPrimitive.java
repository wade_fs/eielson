package p119e.p144d.p168b;

import java.math.BigInteger;
import p119e.p144d.p168b.p171y.C$Gson$Preconditions;
import p119e.p144d.p168b.p171y.LazilyParsedNumber;

/* renamed from: e.d.b.q */
public final class JsonPrimitive extends JsonElement {

    /* renamed from: a */
    private final Object f7530a;

    public JsonPrimitive(Boolean bool) {
        C$Gson$Preconditions.m12336a(bool);
        this.f7530a = bool;
    }

    /* renamed from: a */
    private static boolean m12318a(JsonPrimitive qVar) {
        Object obj = qVar.f7530a;
        if (!(obj instanceof Number)) {
            return false;
        }
        Number number = (Number) obj;
        if ((number instanceof BigInteger) || (number instanceof Long) || (number instanceof Integer) || (number instanceof Short) || (number instanceof Byte)) {
            return true;
        }
        return false;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || JsonPrimitive.class != obj.getClass()) {
            return false;
        }
        JsonPrimitive qVar = (JsonPrimitive) obj;
        if (this.f7530a == null) {
            if (qVar.f7530a == null) {
                return true;
            }
            return false;
        } else if (!m12318a(this) || !m12318a(qVar)) {
            if (!(this.f7530a instanceof Number) || !(qVar.f7530a instanceof Number)) {
                return this.f7530a.equals(qVar.f7530a);
            }
            double doubleValue = mo23836o().doubleValue();
            double doubleValue2 = qVar.mo23836o().doubleValue();
            if (doubleValue == doubleValue2) {
                return true;
            }
            if (!Double.isNaN(doubleValue) || !Double.isNaN(doubleValue2)) {
                return false;
            }
            return true;
        } else if (mo23836o().longValue() == qVar.mo23836o().longValue()) {
            return true;
        } else {
            return false;
        }
    }

    public int hashCode() {
        long doubleToLongBits;
        if (this.f7530a == null) {
            return 31;
        }
        if (m12318a(this)) {
            doubleToLongBits = mo23836o().longValue();
        } else {
            Object obj = this.f7530a;
            if (!(obj instanceof Number)) {
                return obj.hashCode();
            }
            doubleToLongBits = Double.doubleToLongBits(mo23836o().doubleValue());
        }
        return (int) ((doubleToLongBits >>> 32) ^ doubleToLongBits);
    }

    /* renamed from: k */
    public boolean mo23832k() {
        if (mo23838q()) {
            return ((Boolean) this.f7530a).booleanValue();
        }
        return Boolean.parseBoolean(mo23837p());
    }

    /* renamed from: l */
    public double mo23833l() {
        return mo23839r() ? mo23836o().doubleValue() : Double.parseDouble(mo23837p());
    }

    /* renamed from: m */
    public int mo23834m() {
        return mo23839r() ? mo23836o().intValue() : Integer.parseInt(mo23837p());
    }

    /* renamed from: n */
    public long mo23835n() {
        return mo23839r() ? mo23836o().longValue() : Long.parseLong(mo23837p());
    }

    /* renamed from: o */
    public Number mo23836o() {
        Object obj = this.f7530a;
        return obj instanceof String ? new LazilyParsedNumber((String) obj) : (Number) obj;
    }

    /* renamed from: p */
    public String mo23837p() {
        if (mo23839r()) {
            return mo23836o().toString();
        }
        if (mo23838q()) {
            return ((Boolean) this.f7530a).toString();
        }
        return (String) this.f7530a;
    }

    /* renamed from: q */
    public boolean mo23838q() {
        return this.f7530a instanceof Boolean;
    }

    /* renamed from: r */
    public boolean mo23839r() {
        return this.f7530a instanceof Number;
    }

    /* renamed from: s */
    public boolean mo23840s() {
        return this.f7530a instanceof String;
    }

    public JsonPrimitive(Number number) {
        C$Gson$Preconditions.m12336a(number);
        this.f7530a = number;
    }

    public JsonPrimitive(String str) {
        C$Gson$Preconditions.m12336a(str);
        this.f7530a = str;
    }
}
