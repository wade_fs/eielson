package p119e.p144d.p168b.p171y.p172n;

import java.io.Reader;
import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import p119e.p144d.p168b.JsonArray;
import p119e.p144d.p168b.JsonNull;
import p119e.p144d.p168b.JsonObject;
import p119e.p144d.p168b.JsonPrimitive;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: e.d.b.y.n.e */
public final class JsonTreeReader extends JsonReader {

    /* renamed from: j0 */
    private static final Object f7608j0 = new Object();

    /* renamed from: f0 */
    private Object[] f7609f0;

    /* renamed from: g0 */
    private int f7610g0;

    /* renamed from: h0 */
    private String[] f7611h0;

    /* renamed from: i0 */
    private int[] f7612i0;

    /* renamed from: e.d.b.y.n.e$a */
    /* compiled from: JsonTreeReader */
    class C4137a extends Reader {
        C4137a() {
        }

        public void close() {
            throw new AssertionError();
        }

        public int read(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    static {
        new C4137a();
    }

    /* renamed from: G */
    private Object m12444G() {
        return this.f7609f0[this.f7610g0 - 1];
    }

    /* renamed from: H */
    private Object m12445H() {
        Object[] objArr = this.f7609f0;
        int i = this.f7610g0 - 1;
        this.f7610g0 = i;
        Object obj = objArr[i];
        objArr[this.f7610g0] = null;
        return obj;
    }

    /* renamed from: v */
    private String m12448v() {
        return " at path " + mo23751g();
    }

    /* renamed from: A */
    public String mo23739A() {
        m12446a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) m12444G()).next();
        String str = (String) entry.getKey();
        this.f7611h0[this.f7610g0 - 1] = str;
        m12447a(entry.getValue());
        return str;
    }

    /* renamed from: B */
    public void mo23740B() {
        m12446a(JsonToken.NULL);
        m12445H();
        int i = this.f7610g0;
        if (i > 0) {
            int[] iArr = this.f7612i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    /* renamed from: C */
    public String mo23741C() {
        JsonToken D = mo23742D();
        if (D == JsonToken.STRING || D == JsonToken.NUMBER) {
            String p = ((JsonPrimitive) m12445H()).mo23837p();
            int i = this.f7610g0;
            if (i > 0) {
                int[] iArr = this.f7612i0;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return p;
        }
        throw new IllegalStateException("Expected " + JsonToken.STRING + " but was " + D + m12448v());
    }

    /* renamed from: D */
    public JsonToken mo23742D() {
        if (this.f7610g0 == 0) {
            return JsonToken.END_DOCUMENT;
        }
        Object G = m12444G();
        if (G instanceof Iterator) {
            boolean z = this.f7609f0[this.f7610g0 - 2] instanceof JsonObject;
            Iterator it = (Iterator) G;
            if (!it.hasNext()) {
                return z ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;
            }
            if (z) {
                return JsonToken.NAME;
            }
            m12447a(it.next());
            return mo23742D();
        } else if (G instanceof JsonObject) {
            return JsonToken.BEGIN_OBJECT;
        } else {
            if (G instanceof JsonArray) {
                return JsonToken.BEGIN_ARRAY;
            }
            if (G instanceof JsonPrimitive) {
                JsonPrimitive qVar = (JsonPrimitive) G;
                if (qVar.mo23840s()) {
                    return JsonToken.STRING;
                }
                if (qVar.mo23838q()) {
                    return JsonToken.BOOLEAN;
                }
                if (qVar.mo23839r()) {
                    return JsonToken.NUMBER;
                }
                throw new AssertionError();
            } else if (G instanceof JsonNull) {
                return JsonToken.NULL;
            } else {
                if (G == f7608j0) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    /* renamed from: E */
    public void mo23743E() {
        if (mo23742D() == JsonToken.NAME) {
            mo23739A();
            this.f7611h0[this.f7610g0 - 2] = "null";
        } else {
            m12445H();
            int i = this.f7610g0;
            if (i > 0) {
                this.f7611h0[i - 1] = "null";
            }
        }
        int i2 = this.f7610g0;
        if (i2 > 0) {
            int[] iArr = this.f7612i0;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    /* renamed from: F */
    public void mo23930F() {
        m12446a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) m12444G()).next();
        m12447a(entry.getValue());
        m12447a(new JsonPrimitive((String) entry.getKey()));
    }

    /* renamed from: a */
    public void mo23744a() {
        m12446a(JsonToken.BEGIN_ARRAY);
        m12447a(((JsonArray) m12444G()).iterator());
        this.f7612i0[this.f7610g0 - 1] = 0;
    }

    /* renamed from: b */
    public void mo23746b() {
        m12446a(JsonToken.BEGIN_OBJECT);
        m12447a(((JsonObject) m12444G()).mo23829k().iterator());
    }

    public void close() {
        this.f7609f0 = new Object[]{f7608j0};
        this.f7610g0 = 1;
    }

    /* renamed from: d */
    public void mo23749d() {
        m12446a(JsonToken.END_ARRAY);
        m12445H();
        m12445H();
        int i = this.f7610g0;
        if (i > 0) {
            int[] iArr = this.f7612i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    /* renamed from: e */
    public void mo23750e() {
        m12446a(JsonToken.END_OBJECT);
        m12445H();
        m12445H();
        int i = this.f7610g0;
        if (i > 0) {
            int[] iArr = this.f7612i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
    }

    /* renamed from: g */
    public String mo23751g() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = 0;
        while (i < this.f7610g0) {
            Object[] objArr = this.f7609f0;
            if (objArr[i] instanceof JsonArray) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.f7612i0[i]);
                    sb.append(']');
                }
            } else if (objArr[i] instanceof JsonObject) {
                i++;
                if (objArr[i] instanceof Iterator) {
                    sb.append((char) JwtParser.SEPARATOR_CHAR);
                    String[] strArr = this.f7611h0;
                    if (strArr[i] != null) {
                        sb.append(strArr[i]);
                    }
                }
            }
            i++;
        }
        return sb.toString();
    }

    /* renamed from: t */
    public boolean mo23752t() {
        JsonToken D = mo23742D();
        return (D == JsonToken.END_OBJECT || D == JsonToken.END_ARRAY) ? false : true;
    }

    public String toString() {
        return JsonTreeReader.class.getSimpleName();
    }

    /* renamed from: w */
    public boolean mo23756w() {
        m12446a(JsonToken.BOOLEAN);
        boolean k = ((JsonPrimitive) m12445H()).mo23832k();
        int i = this.f7610g0;
        if (i > 0) {
            int[] iArr = this.f7612i0;
            int i2 = i - 1;
            iArr[i2] = iArr[i2] + 1;
        }
        return k;
    }

    /* renamed from: x */
    public double mo23757x() {
        JsonToken D = mo23742D();
        if (D == JsonToken.NUMBER || D == JsonToken.STRING) {
            double l = ((JsonPrimitive) m12444G()).mo23833l();
            if (mo23754u() || (!Double.isNaN(l) && !Double.isInfinite(l))) {
                m12445H();
                int i = this.f7610g0;
                if (i > 0) {
                    int[] iArr = this.f7612i0;
                    int i2 = i - 1;
                    iArr[i2] = iArr[i2] + 1;
                }
                return l;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + l);
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + D + m12448v());
    }

    /* renamed from: y */
    public int mo23758y() {
        JsonToken D = mo23742D();
        if (D == JsonToken.NUMBER || D == JsonToken.STRING) {
            int m = ((JsonPrimitive) m12444G()).mo23834m();
            m12445H();
            int i = this.f7610g0;
            if (i > 0) {
                int[] iArr = this.f7612i0;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return m;
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + D + m12448v());
    }

    /* renamed from: z */
    public long mo23759z() {
        JsonToken D = mo23742D();
        if (D == JsonToken.NUMBER || D == JsonToken.STRING) {
            long n = ((JsonPrimitive) m12444G()).mo23835n();
            m12445H();
            int i = this.f7610g0;
            if (i > 0) {
                int[] iArr = this.f7612i0;
                int i2 = i - 1;
                iArr[i2] = iArr[i2] + 1;
            }
            return n;
        }
        throw new IllegalStateException("Expected " + JsonToken.NUMBER + " but was " + D + m12448v());
    }

    /* renamed from: a */
    private void m12446a(JsonToken bVar) {
        if (mo23742D() != bVar) {
            throw new IllegalStateException("Expected " + bVar + " but was " + mo23742D() + m12448v());
        }
    }

    /* renamed from: a */
    private void m12447a(Object obj) {
        int i = this.f7610g0;
        Object[] objArr = this.f7609f0;
        if (i == objArr.length) {
            int i2 = i * 2;
            this.f7609f0 = Arrays.copyOf(objArr, i2);
            this.f7612i0 = Arrays.copyOf(this.f7612i0, i2);
            this.f7611h0 = (String[]) Arrays.copyOf(this.f7611h0, i2);
        }
        Object[] objArr2 = this.f7609f0;
        int i3 = this.f7610g0;
        this.f7610g0 = i3 + 1;
        objArr2[i3] = obj;
    }
}
