package p119e.p144d.p168b.p171y.p172n;

import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonSyntaxException;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.j */
public final class SqlDateTypeAdapter extends TypeAdapter<Date> {

    /* renamed from: b */
    public static final TypeAdapterFactory f7643b = new C4145a();

    /* renamed from: a */
    private final DateFormat f7644a = new SimpleDateFormat("MMM d, yyyy");

    /* renamed from: e.d.b.y.n.j$a */
    /* compiled from: SqlDateTypeAdapter */
    class C4145a implements TypeAdapterFactory {
        C4145a() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            if (aVar.mo23980a() == Date.class) {
                return new SqlDateTypeAdapter();
            }
            return null;
        }
    }

    /* renamed from: a */
    public synchronized Date mo23735a(JsonReader aVar) {
        if (aVar.mo23742D() == JsonToken.NULL) {
            aVar.mo23740B();
            return null;
        }
        try {
            return new Date(this.f7644a.parse(aVar.mo23741C()).getTime());
        } catch (ParseException e) {
            throw new JsonSyntaxException(e);
        }
    }

    /* renamed from: a */
    public synchronized void mo23736a(JsonWriter cVar, Date date) {
        cVar.mo23773d(date == null ? null : this.f7644a.format(date));
    }
}
