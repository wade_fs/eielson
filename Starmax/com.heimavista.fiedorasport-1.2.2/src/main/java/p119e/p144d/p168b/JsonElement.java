package p119e.p144d.p168b;

import java.io.IOException;
import java.io.StringWriter;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.Streams;

/* renamed from: e.d.b.l */
public abstract class JsonElement {
    /* renamed from: b */
    public JsonArray mo23816b() {
        if (mo23819f()) {
            return (JsonArray) this;
        }
        throw new IllegalStateException("Not a JSON Array: " + this);
    }

    /* renamed from: c */
    public JsonObject mo23817c() {
        if (mo23821i()) {
            return (JsonObject) this;
        }
        throw new IllegalStateException("Not a JSON Object: " + this);
    }

    /* renamed from: d */
    public JsonPrimitive mo23818d() {
        if (mo23822j()) {
            return (JsonPrimitive) this;
        }
        throw new IllegalStateException("Not a JSON Primitive: " + this);
    }

    /* renamed from: f */
    public boolean mo23819f() {
        return this instanceof JsonArray;
    }

    /* renamed from: h */
    public boolean mo23820h() {
        return this instanceof JsonNull;
    }

    /* renamed from: i */
    public boolean mo23821i() {
        return this instanceof JsonObject;
    }

    /* renamed from: j */
    public boolean mo23822j() {
        return this instanceof JsonPrimitive;
    }

    public String toString() {
        try {
            StringWriter stringWriter = new StringWriter();
            JsonWriter cVar = new JsonWriter(stringWriter);
            cVar.mo23767b(true);
            Streams.m12420a(this, cVar);
            return stringWriter.toString();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
    }
}
