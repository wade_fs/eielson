package p119e.p144d.p168b;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: e.d.b.i */
public final class JsonArray extends JsonElement implements Iterable<JsonElement> {

    /* renamed from: P */
    private final List<JsonElement> f7527P = new ArrayList();

    /* renamed from: a */
    public void mo23811a(JsonElement lVar) {
        if (lVar == null) {
            lVar = JsonNull.f7528a;
        }
        this.f7527P.add(lVar);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonArray) && ((JsonArray) obj).f7527P.equals(this.f7527P));
    }

    public int hashCode() {
        return this.f7527P.hashCode();
    }

    public Iterator<JsonElement> iterator() {
        return this.f7527P.iterator();
    }
}
