package p119e.p144d.p168b.p171y.p172n;

import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p171y.ConstructorConstructor;

/* renamed from: e.d.b.y.n.d */
public final class JsonAdapterAnnotationTypeAdapterFactory implements TypeAdapterFactory {

    /* renamed from: P */
    private final ConstructorConstructor f7607P;

    public JsonAdapterAnnotationTypeAdapterFactory(ConstructorConstructor cVar) {
        this.f7607P = cVar;
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [e.d.b.z.a, e.d.b.z.a<T>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> p119e.p144d.p168b.TypeAdapter<T> mo23844a(p119e.p144d.p168b.Gson r3, p119e.p144d.p168b.p175z.TypeToken<T> r4) {
        /*
            r2 = this;
            java.lang.Class r0 = r4.mo23980a()
            java.lang.Class<e.d.b.x.b> r1 = p119e.p144d.p168b.p170x.JsonAdapter.class
            java.lang.annotation.Annotation r0 = r0.getAnnotation(r1)
            e.d.b.x.b r0 = (p119e.p144d.p168b.p170x.JsonAdapter) r0
            if (r0 != 0) goto L_0x0010
            r3 = 0
            return r3
        L_0x0010:
            e.d.b.y.c r1 = r2.f7607P
            e.d.b.v r3 = r2.mo23929a(r1, r3, r4, r0)
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.p172n.JsonAdapterAnnotationTypeAdapterFactory.mo23844a(e.d.b.f, e.d.b.z.a):e.d.b.v");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r9v13, types: [e.d.b.v] */
    /* JADX WARN: Type inference failed for: r9v14, types: [e.d.b.v] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p119e.p144d.p168b.TypeAdapter<?> mo23929a(p119e.p144d.p168b.p171y.ConstructorConstructor r9, p119e.p144d.p168b.Gson r10, p119e.p144d.p168b.p175z.TypeToken<?> r11, p119e.p144d.p168b.p170x.JsonAdapter r12) {
        /*
            r8 = this;
            java.lang.Class r0 = r12.value()
            e.d.b.z.a r0 = p119e.p144d.p168b.p175z.TypeToken.m12662a(r0)
            e.d.b.y.i r9 = r9.mo23868a(r0)
            java.lang.Object r9 = r9.mo23870a()
            boolean r0 = r9 instanceof p119e.p144d.p168b.TypeAdapter
            if (r0 == 0) goto L_0x0017
            e.d.b.v r9 = (p119e.p144d.p168b.TypeAdapter) r9
            goto L_0x0075
        L_0x0017:
            boolean r0 = r9 instanceof p119e.p144d.p168b.TypeAdapterFactory
            if (r0 == 0) goto L_0x0022
            e.d.b.w r9 = (p119e.p144d.p168b.TypeAdapterFactory) r9
            e.d.b.v r9 = r9.mo23844a(r10, r11)
            goto L_0x0075
        L_0x0022:
            boolean r0 = r9 instanceof p119e.p144d.p168b.JsonSerializer
            if (r0 != 0) goto L_0x005b
            boolean r1 = r9 instanceof p119e.p144d.p168b.JsonDeserializer
            if (r1 == 0) goto L_0x002b
            goto L_0x005b
        L_0x002b:
            java.lang.IllegalArgumentException r10 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r0 = "Invalid attempt to bind an instance of "
            r12.append(r0)
            java.lang.Class r9 = r9.getClass()
            java.lang.String r9 = r9.getName()
            r12.append(r9)
            java.lang.String r9 = " as a @JsonAdapter for "
            r12.append(r9)
            java.lang.String r9 = r11.toString()
            r12.append(r9)
            java.lang.String r9 = ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer."
            r12.append(r9)
            java.lang.String r9 = r12.toString()
            r10.<init>(r9)
            throw r10
        L_0x005b:
            r1 = 0
            if (r0 == 0) goto L_0x0063
            r0 = r9
            e.d.b.s r0 = (p119e.p144d.p168b.JsonSerializer) r0
            r3 = r0
            goto L_0x0064
        L_0x0063:
            r3 = r1
        L_0x0064:
            boolean r0 = r9 instanceof p119e.p144d.p168b.JsonDeserializer
            if (r0 == 0) goto L_0x006b
            r1 = r9
            e.d.b.k r1 = (p119e.p144d.p168b.JsonDeserializer) r1
        L_0x006b:
            r4 = r1
            e.d.b.y.n.l r9 = new e.d.b.y.n.l
            r7 = 0
            r2 = r9
            r5 = r10
            r6 = r11
            r2.<init>(r3, r4, r5, r6, r7)
        L_0x0075:
            if (r9 == 0) goto L_0x0081
            boolean r10 = r12.nullSafe()
            if (r10 == 0) goto L_0x0081
            e.d.b.v r9 = r9.mo23843a()
        L_0x0081:
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p171y.p172n.JsonAdapterAnnotationTypeAdapterFactory.mo23929a(e.d.b.y.c, e.d.b.f, e.d.b.z.a, e.d.b.x.b):e.d.b.v");
    }
}
