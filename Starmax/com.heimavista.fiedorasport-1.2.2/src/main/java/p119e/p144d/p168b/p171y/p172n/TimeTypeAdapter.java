package p119e.p144d.p168b.p171y.p172n;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonSyntaxException;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonToken;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.k */
public final class TimeTypeAdapter extends TypeAdapter<Time> {

    /* renamed from: b */
    public static final TypeAdapterFactory f7645b = new C4146a();

    /* renamed from: a */
    private final DateFormat f7646a = new SimpleDateFormat("hh:mm:ss a");

    /* renamed from: e.d.b.y.n.k$a */
    /* compiled from: TimeTypeAdapter */
    class C4146a implements TypeAdapterFactory {
        C4146a() {
        }

        /* renamed from: a */
        public <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar) {
            if (aVar.mo23980a() == Time.class) {
                return new TimeTypeAdapter();
            }
            return null;
        }
    }

    /* renamed from: a */
    public synchronized Time mo23735a(JsonReader aVar) {
        if (aVar.mo23742D() == JsonToken.NULL) {
            aVar.mo23740B();
            return null;
        }
        try {
            return new Time(this.f7646a.parse(aVar.mo23741C()).getTime());
        } catch (ParseException e) {
            throw new JsonSyntaxException(e);
        }
    }

    /* renamed from: a */
    public synchronized void mo23736a(JsonWriter cVar, Time time) {
        cVar.mo23773d(time == null ? null : this.f7646a.format(time));
    }
}
