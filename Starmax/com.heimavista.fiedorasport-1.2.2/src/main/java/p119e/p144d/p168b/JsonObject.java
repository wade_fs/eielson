package p119e.p144d.p168b;

import java.util.Map;
import java.util.Set;
import p119e.p144d.p168b.p171y.LinkedTreeMap;

/* renamed from: e.d.b.o */
public final class JsonObject extends JsonElement {

    /* renamed from: a */
    private final LinkedTreeMap<String, JsonElement> f7529a = new LinkedTreeMap<>();

    /* renamed from: a */
    public void mo23826a(String str, JsonElement lVar) {
        LinkedTreeMap<String, JsonElement> hVar = this.f7529a;
        if (lVar == null) {
            lVar = JsonNull.f7528a;
        }
        hVar.put(str, lVar);
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonObject) && ((JsonObject) obj).f7529a.equals(this.f7529a));
    }

    public int hashCode() {
        return this.f7529a.hashCode();
    }

    /* renamed from: k */
    public Set<Map.Entry<String, JsonElement>> mo23829k() {
        return this.f7529a.entrySet();
    }
}
