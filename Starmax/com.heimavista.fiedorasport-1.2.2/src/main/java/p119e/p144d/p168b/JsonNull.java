package p119e.p144d.p168b;

/* renamed from: e.d.b.n */
public final class JsonNull extends JsonElement {

    /* renamed from: a */
    public static final JsonNull f7528a = new JsonNull();

    public boolean equals(Object obj) {
        return this == obj || (obj instanceof JsonNull);
    }

    public int hashCode() {
        return JsonNull.class.hashCode();
    }
}
