package p119e.p144d.p168b.p171y.p172n;

import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.JsonDeserializationContext;
import p119e.p144d.p168b.JsonDeserializer;
import p119e.p144d.p168b.JsonElement;
import p119e.p144d.p168b.JsonSerializationContext;
import p119e.p144d.p168b.JsonSerializer;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.TypeAdapterFactory;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.Streams;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.l */
public final class TreeTypeAdapter<T> extends TypeAdapter<T> {

    /* renamed from: a */
    private final JsonSerializer<T> f7647a;

    /* renamed from: b */
    private final JsonDeserializer<T> f7648b;

    /* renamed from: c */
    final Gson f7649c;

    /* renamed from: d */
    private final TypeToken<T> f7650d;

    /* renamed from: e */
    private final TypeAdapterFactory f7651e;

    /* renamed from: f */
    private final TreeTypeAdapter<T>.b f7652f = new C4148b();

    /* renamed from: g */
    private TypeAdapter<T> f7653g;

    /* renamed from: e.d.b.y.n.l$b */
    /* compiled from: TreeTypeAdapter */
    private final class C4148b implements JsonSerializationContext, JsonDeserializationContext {
        private C4148b(TreeTypeAdapter lVar) {
        }
    }

    public TreeTypeAdapter(JsonSerializer<T> sVar, JsonDeserializer<T> kVar, Gson fVar, TypeToken<T> aVar, TypeAdapterFactory wVar) {
        this.f7647a = sVar;
        this.f7648b = kVar;
        this.f7649c = fVar;
        this.f7650d = aVar;
        this.f7651e = wVar;
    }

    /* renamed from: b */
    private TypeAdapter<T> m12513b() {
        TypeAdapter<T> vVar = this.f7653g;
        if (vVar != null) {
            return super;
        }
        TypeAdapter<T> a = this.f7649c.mo23786a(this.f7651e, this.f7650d);
        this.f7653g = super;
        return super;
    }

    /* renamed from: a */
    public T mo23735a(JsonReader aVar) {
        if (this.f7648b == null) {
            return m12513b().mo23735a(aVar);
        }
        JsonElement a = Streams.m12418a(aVar);
        if (a.mo23820h()) {
            return null;
        }
        return this.f7648b.mo23815a(a, this.f7650d.mo23981b(), this.f7652f);
    }

    /* renamed from: a */
    public void mo23736a(JsonWriter cVar, T t) {
        JsonSerializer<T> sVar = this.f7647a;
        if (sVar == null) {
            m12513b().mo23736a(cVar, t);
        } else if (t == null) {
            cVar.mo23780u();
        } else {
            Streams.m12420a(sVar.mo23841a(t, this.f7650d.mo23981b(), this.f7652f), cVar);
        }
    }
}
