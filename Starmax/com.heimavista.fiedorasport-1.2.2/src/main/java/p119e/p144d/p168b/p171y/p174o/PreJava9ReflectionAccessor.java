package p119e.p144d.p168b.p171y.p174o;

import java.lang.reflect.AccessibleObject;

/* renamed from: e.d.b.y.o.a */
final class PreJava9ReflectionAccessor extends ReflectionAccessor {
    PreJava9ReflectionAccessor() {
    }

    /* renamed from: a */
    public void mo23978a(AccessibleObject accessibleObject) {
        accessibleObject.setAccessible(true);
    }
}
