package p119e.p144d.p168b.p171y.p174o;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import p119e.p144d.p168b.JsonIOException;

/* renamed from: e.d.b.y.o.c */
final class UnsafeReflectionAccessor extends ReflectionAccessor {

    /* renamed from: d */
    private static Class f7727d;

    /* renamed from: b */
    private final Object f7728b = m12659c();

    /* renamed from: c */
    private final Field f7729c = m12658b();

    UnsafeReflectionAccessor() {
    }

    /* renamed from: c */
    private static Object m12659c() {
        try {
            f7727d = Class.forName("sun.misc.Unsafe");
            Field declaredField = f7727d.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return declaredField.get(null);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: a */
    public void mo23978a(AccessibleObject accessibleObject) {
        if (!mo23979b(accessibleObject)) {
            try {
                accessibleObject.setAccessible(true);
            } catch (SecurityException e) {
                throw new JsonIOException("Gson couldn't modify fields for " + accessibleObject + "\nand sun.misc.Unsafe not found.\nEither write a custom type adapter, or make fields accessible, or include sun.misc.Unsafe.", e);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo23979b(AccessibleObject accessibleObject) {
        if (!(this.f7728b == null || this.f7729c == null)) {
            try {
                long longValue = ((Long) f7727d.getMethod("objectFieldOffset", Field.class).invoke(this.f7728b, this.f7729c)).longValue();
                f7727d.getMethod("putBoolean", Object.class, Long.TYPE, Boolean.TYPE).invoke(this.f7728b, accessibleObject, Long.valueOf(longValue), true);
                return true;
            } catch (Exception unused) {
            }
        }
        return false;
    }

    /* renamed from: b */
    private static Field m12658b() {
        try {
            return AccessibleObject.class.getDeclaredField("override");
        } catch (NoSuchFieldException unused) {
            return null;
        }
    }
}
