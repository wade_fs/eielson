package p119e.p144d.p168b.p171y.p172n;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import p119e.p144d.p168b.JsonArray;
import p119e.p144d.p168b.JsonElement;
import p119e.p144d.p168b.JsonNull;
import p119e.p144d.p168b.JsonObject;
import p119e.p144d.p168b.JsonPrimitive;
import p119e.p144d.p168b.p169a0.JsonWriter;

/* renamed from: e.d.b.y.n.f */
public final class JsonTreeWriter extends JsonWriter {

    /* renamed from: d0 */
    private static final Writer f7613d0 = new C4138a();

    /* renamed from: e0 */
    private static final JsonPrimitive f7614e0 = new JsonPrimitive("closed");

    /* renamed from: a0 */
    private final List<JsonElement> f7615a0 = new ArrayList();

    /* renamed from: b0 */
    private String f7616b0;

    /* renamed from: c0 */
    private JsonElement f7617c0 = JsonNull.f7528a;

    /* renamed from: e.d.b.y.n.f$a */
    /* compiled from: JsonTreeWriter */
    class C4138a extends Writer {
        C4138a() {
        }

        public void close() {
            throw new AssertionError();
        }

        public void flush() {
            throw new AssertionError();
        }

        public void write(char[] cArr, int i, int i2) {
            throw new AssertionError();
        }
    }

    public JsonTreeWriter() {
        super(f7613d0);
    }

    /* renamed from: a */
    private void m12465a(JsonElement lVar) {
        if (this.f7616b0 != null) {
            if (!lVar.mo23820h() || mo23775e()) {
                ((JsonObject) m12466w()).mo23826a(this.f7616b0, lVar);
            }
            this.f7616b0 = null;
        } else if (this.f7615a0.isEmpty()) {
            this.f7617c0 = lVar;
        } else {
            JsonElement w = m12466w();
            if (w instanceof JsonArray) {
                ((JsonArray) w).mo23811a(lVar);
                return;
            }
            throw new IllegalStateException();
        }
    }

    /* renamed from: w */
    private JsonElement m12466w() {
        List<JsonElement> list = this.f7615a0;
        return list.get(list.size() - 1);
    }

    /* renamed from: b */
    public JsonWriter mo23765b() {
        JsonObject oVar = new JsonObject();
        m12465a(oVar);
        this.f7615a0.add(oVar);
        return super;
    }

    /* renamed from: c */
    public JsonWriter mo23768c() {
        if (this.f7615a0.isEmpty() || this.f7616b0 != null) {
            throw new IllegalStateException();
        } else if (m12466w() instanceof JsonArray) {
            List<JsonElement> list = this.f7615a0;
            list.remove(list.size() - 1);
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    public void close() {
        if (this.f7615a0.isEmpty()) {
            this.f7615a0.add(f7614e0);
            return;
        }
        throw new IOException("Incomplete document");
    }

    /* renamed from: d */
    public JsonWriter mo23772d() {
        if (this.f7615a0.isEmpty() || this.f7616b0 != null) {
            throw new IllegalStateException();
        } else if (m12466w() instanceof JsonObject) {
            List<JsonElement> list = this.f7615a0;
            list.remove(list.size() - 1);
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    public void flush() {
    }

    /* renamed from: g */
    public JsonWriter mo23777g(long j) {
        m12465a(new JsonPrimitive(Long.valueOf(j)));
        return super;
    }

    /* renamed from: u */
    public JsonWriter mo23780u() {
        m12465a(JsonNull.f7528a);
        return super;
    }

    /* renamed from: v */
    public JsonElement mo23933v() {
        if (this.f7615a0.isEmpty()) {
            return this.f7617c0;
        }
        throw new IllegalStateException("Expected one JSON element but was " + this.f7615a0);
    }

    /* renamed from: b */
    public JsonWriter mo23766b(String str) {
        if (this.f7615a0.isEmpty() || this.f7616b0 != null) {
            throw new IllegalStateException();
        } else if (m12466w() instanceof JsonObject) {
            this.f7616b0 = str;
            return super;
        } else {
            throw new IllegalStateException();
        }
    }

    /* renamed from: d */
    public JsonWriter mo23773d(String str) {
        if (str == null) {
            mo23780u();
            return super;
        }
        m12465a(new JsonPrimitive(str));
        return super;
    }

    /* renamed from: d */
    public JsonWriter mo23774d(boolean z) {
        m12465a(new JsonPrimitive(Boolean.valueOf(z)));
        return super;
    }

    /* renamed from: a */
    public JsonWriter mo23761a() {
        JsonArray iVar = new JsonArray();
        m12465a(iVar);
        this.f7615a0.add(iVar);
        return super;
    }

    /* renamed from: a */
    public JsonWriter mo23762a(Boolean bool) {
        if (bool == null) {
            mo23780u();
            return super;
        }
        m12465a(new JsonPrimitive(bool));
        return super;
    }

    /* renamed from: a */
    public JsonWriter mo23763a(Number number) {
        if (number == null) {
            mo23780u();
            return super;
        }
        if (!mo23779t()) {
            double doubleValue = number.doubleValue();
            if (Double.isNaN(doubleValue) || Double.isInfinite(doubleValue)) {
                throw new IllegalArgumentException("JSON forbids NaN and infinities: " + number);
            }
        }
        m12465a(new JsonPrimitive(number));
        return super;
    }
}
