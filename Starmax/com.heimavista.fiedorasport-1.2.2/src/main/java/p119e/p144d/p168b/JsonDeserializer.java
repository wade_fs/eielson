package p119e.p144d.p168b;

import java.lang.reflect.Type;

/* renamed from: e.d.b.k */
public interface JsonDeserializer<T> {
    /* renamed from: a */
    T mo23815a(JsonElement lVar, Type type, JsonDeserializationContext jVar);
}
