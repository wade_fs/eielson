package p119e.p144d.p168b;

import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.w */
public interface TypeAdapterFactory {
    /* renamed from: a */
    <T> TypeAdapter<T> mo23844a(Gson fVar, TypeToken<T> aVar);
}
