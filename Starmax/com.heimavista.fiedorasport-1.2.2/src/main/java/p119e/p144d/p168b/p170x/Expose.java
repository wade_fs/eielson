package p119e.p144d.p168b.p170x;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
/* renamed from: e.d.b.x.a */
public @interface Expose {
    boolean deserialize() default true;

    boolean serialize() default true;
}
