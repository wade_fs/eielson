package p119e.p144d.p168b.p171y;

import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/* renamed from: e.d.b.y.m */
public abstract class UnsafeAllocator {

    /* renamed from: e.d.b.y.m$a */
    /* compiled from: UnsafeAllocator */
    class C4130a extends UnsafeAllocator {

        /* renamed from: a */
        final /* synthetic */ Method f7594a;

        /* renamed from: b */
        final /* synthetic */ Object f7595b;

        C4130a(Method method, Object obj) {
            this.f7594a = method;
            this.f7595b = obj;
        }

        /* renamed from: a */
        public <T> T mo23926a(Class<T> cls) {
            UnsafeAllocator.m12422b(cls);
            return this.f7594a.invoke(this.f7595b, cls);
        }
    }

    /* renamed from: e.d.b.y.m$b */
    /* compiled from: UnsafeAllocator */
    class C4131b extends UnsafeAllocator {

        /* renamed from: a */
        final /* synthetic */ Method f7596a;

        /* renamed from: b */
        final /* synthetic */ int f7597b;

        C4131b(Method method, int i) {
            this.f7596a = method;
            this.f7597b = i;
        }

        /* renamed from: a */
        public <T> T mo23926a(Class<T> cls) {
            UnsafeAllocator.m12422b(cls);
            return this.f7596a.invoke(null, cls, Integer.valueOf(this.f7597b));
        }
    }

    /* renamed from: e.d.b.y.m$c */
    /* compiled from: UnsafeAllocator */
    class C4132c extends UnsafeAllocator {

        /* renamed from: a */
        final /* synthetic */ Method f7598a;

        C4132c(Method method) {
            this.f7598a = method;
        }

        /* renamed from: a */
        public <T> T mo23926a(Class<T> cls) {
            UnsafeAllocator.m12422b(cls);
            return this.f7598a.invoke(null, cls, Object.class);
        }
    }

    /* renamed from: e.d.b.y.m$d */
    /* compiled from: UnsafeAllocator */
    class C4133d extends UnsafeAllocator {
        C4133d() {
        }

        /* renamed from: a */
        public <T> T mo23926a(Class<T> cls) {
            throw new UnsupportedOperationException("Cannot allocate " + cls);
        }
    }

    /* renamed from: a */
    public static UnsafeAllocator m12421a() {
        try {
            Class<?> cls = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return new C4130a(cls.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception unused) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                return new C4131b(declaredMethod2, intValue);
            } catch (Exception unused2) {
                try {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    return new C4132c(declaredMethod3);
                } catch (Exception unused3) {
                    return new C4133d();
                }
            }
        }
    }

    /* renamed from: b */
    static void m12422b(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            throw new UnsupportedOperationException("Interface can't be instantiated! Interface name: " + cls.getName());
        } else if (Modifier.isAbstract(modifiers)) {
            throw new UnsupportedOperationException("Abstract class can't be instantiated! Class name: " + cls.getName());
        }
    }

    /* renamed from: a */
    public abstract <T> T mo23926a(Class<T> cls);
}
