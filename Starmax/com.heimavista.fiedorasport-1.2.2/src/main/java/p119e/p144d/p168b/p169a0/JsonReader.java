package p119e.p144d.p168b.p169a0;

import com.facebook.internal.ServerProtocol;
import com.tencent.bugly.Bugly;
import java.io.Closeable;
import java.io.EOFException;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;
import p119e.p144d.p168b.p171y.JsonReaderInternalAccess;
import p119e.p144d.p168b.p171y.p172n.JsonTreeReader;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: e.d.b.a0.a */
public class JsonReader implements Closeable {

    /* renamed from: e0 */
    private static final char[] f7452e0 = ")]}'\n".toCharArray();

    /* renamed from: P */
    private final Reader f7453P;

    /* renamed from: Q */
    private boolean f7454Q = false;

    /* renamed from: R */
    private final char[] f7455R = new char[1024];

    /* renamed from: S */
    private int f7456S = 0;

    /* renamed from: T */
    private int f7457T = 0;

    /* renamed from: U */
    private int f7458U = 0;

    /* renamed from: V */
    private int f7459V = 0;

    /* renamed from: W */
    int f7460W = 0;

    /* renamed from: X */
    private long f7461X;

    /* renamed from: Y */
    private int f7462Y;

    /* renamed from: Z */
    private String f7463Z;

    /* renamed from: a0 */
    private int[] f7464a0 = new int[32];

    /* renamed from: b0 */
    private int f7465b0 = 0;

    /* renamed from: c0 */
    private String[] f7466c0;

    /* renamed from: d0 */
    private int[] f7467d0;

    /* renamed from: e.d.b.a0.a$a */
    /* compiled from: JsonReader */
    class C4087a extends JsonReaderInternalAccess {
        C4087a() {
        }

        /* renamed from: a */
        public void mo23760a(JsonReader aVar) {
            if (aVar instanceof JsonTreeReader) {
                ((JsonTreeReader) aVar).mo23930F();
                return;
            }
            int i = aVar.f7460W;
            if (i == 0) {
                i = aVar.mo23747c();
            }
            if (i == 13) {
                aVar.f7460W = 9;
            } else if (i == 12) {
                aVar.f7460W = 8;
            } else if (i == 14) {
                aVar.f7460W = 10;
            } else {
                throw new IllegalStateException("Expected a name but was " + aVar.mo23742D() + aVar.mo23755v());
            }
        }
    }

    static {
        JsonReaderInternalAccess.f7567a = new C4087a();
    }

    public JsonReader(Reader reader) {
        int[] iArr = this.f7464a0;
        int i = this.f7465b0;
        this.f7465b0 = i + 1;
        iArr[i] = 6;
        this.f7466c0 = new String[32];
        this.f7467d0 = new int[32];
        if (reader != null) {
            this.f7453P = reader;
            return;
        }
        throw new NullPointerException("in == null");
    }

    /* renamed from: F */
    private void mo23930F() {
        if (!this.f7454Q) {
            m12195c("Use JsonReader.setLenient(true) to accept malformed JSON");
            throw null;
        }
    }

    /* renamed from: G */
    private void m12182G() {
        m12191b(true);
        this.f7456S--;
        int i = this.f7456S;
        char[] cArr = f7452e0;
        if (i + cArr.length <= this.f7457T || m12190a(cArr.length)) {
            int i2 = 0;
            while (true) {
                char[] cArr2 = f7452e0;
                if (i2 >= cArr2.length) {
                    this.f7456S += cArr2.length;
                    return;
                } else if (this.f7455R[this.f7456S + i2] == cArr2[i2]) {
                    i2++;
                } else {
                    return;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x004b, code lost:
        mo23930F();
     */
    /* renamed from: H */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m12183H() {
        /*
            r6 = this;
            r0 = 0
            r1 = 0
            r2 = r1
        L_0x0003:
            r1 = 0
        L_0x0004:
            int r3 = r6.f7456S
            int r4 = r3 + r1
            int r5 = r6.f7457T
            if (r4 >= r5) goto L_0x004f
            char[] r4 = r6.f7455R
            int r3 = r3 + r1
            char r3 = r4[r3]
            r4 = 9
            if (r3 == r4) goto L_0x005d
            r4 = 10
            if (r3 == r4) goto L_0x005d
            r4 = 12
            if (r3 == r4) goto L_0x005d
            r4 = 13
            if (r3 == r4) goto L_0x005d
            r4 = 32
            if (r3 == r4) goto L_0x005d
            r4 = 35
            if (r3 == r4) goto L_0x004b
            r4 = 44
            if (r3 == r4) goto L_0x005d
            r4 = 47
            if (r3 == r4) goto L_0x004b
            r4 = 61
            if (r3 == r4) goto L_0x004b
            r4 = 123(0x7b, float:1.72E-43)
            if (r3 == r4) goto L_0x005d
            r4 = 125(0x7d, float:1.75E-43)
            if (r3 == r4) goto L_0x005d
            r4 = 58
            if (r3 == r4) goto L_0x005d
            r4 = 59
            if (r3 == r4) goto L_0x004b
            switch(r3) {
                case 91: goto L_0x005d;
                case 92: goto L_0x004b;
                case 93: goto L_0x005d;
                default: goto L_0x0048;
            }
        L_0x0048:
            int r1 = r1 + 1
            goto L_0x0004
        L_0x004b:
            r6.mo23930F()
            goto L_0x005d
        L_0x004f:
            char[] r3 = r6.f7455R
            int r3 = r3.length
            if (r1 >= r3) goto L_0x005f
            int r3 = r1 + 1
            boolean r3 = r6.m12190a(r3)
            if (r3 == 0) goto L_0x005d
            goto L_0x0004
        L_0x005d:
            r0 = r1
            goto L_0x007f
        L_0x005f:
            if (r2 != 0) goto L_0x006c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r3 = 16
            int r3 = java.lang.Math.max(r1, r3)
            r2.<init>(r3)
        L_0x006c:
            char[] r3 = r6.f7455R
            int r4 = r6.f7456S
            r2.append(r3, r4, r1)
            int r3 = r6.f7456S
            int r3 = r3 + r1
            r6.f7456S = r3
            r1 = 1
            boolean r1 = r6.m12190a(r1)
            if (r1 != 0) goto L_0x0003
        L_0x007f:
            if (r2 != 0) goto L_0x008b
            java.lang.String r1 = new java.lang.String
            char[] r2 = r6.f7455R
            int r3 = r6.f7456S
            r1.<init>(r2, r3, r0)
            goto L_0x0096
        L_0x008b:
            char[] r1 = r6.f7455R
            int r3 = r6.f7456S
            r2.append(r1, r3, r0)
            java.lang.String r1 = r2.toString()
        L_0x0096:
            int r2 = r6.f7456S
            int r2 = r2 + r0
            r6.f7456S = r2
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p169a0.JsonReader.m12183H():java.lang.String");
    }

    /* renamed from: I */
    private int m12184I() {
        String str;
        String str2;
        int i;
        char c = this.f7455R[this.f7456S];
        if (c == 't' || c == 'T') {
            i = 5;
            str2 = ServerProtocol.DIALOG_RETURN_SCOPES_TRUE;
            str = "TRUE";
        } else if (c == 'f' || c == 'F') {
            i = 6;
            str2 = Bugly.SDK_IS_DEV;
            str = "FALSE";
        } else if (c != 'n' && c != 'N') {
            return 0;
        } else {
            i = 7;
            str2 = "null";
            str = "NULL";
        }
        int length = str2.length();
        for (int i2 = 1; i2 < length; i2++) {
            if (this.f7456S + i2 >= this.f7457T && !m12190a(i2 + 1)) {
                return 0;
            }
            char c2 = this.f7455R[this.f7456S + i2];
            if (c2 != str2.charAt(i2) && c2 != str.charAt(i2)) {
                return 0;
            }
        }
        if ((this.f7456S + length < this.f7457T || m12190a(length + 1)) && m12189a(this.f7455R[this.f7456S + length])) {
            return 0;
        }
        this.f7456S += length;
        this.f7460W = i;
        return i;
    }

    /* renamed from: J */
    private int m12185J() {
        char c;
        char[] cArr = this.f7455R;
        int i = this.f7456S;
        int i2 = 0;
        int i3 = this.f7457T;
        int i4 = 0;
        char c2 = 0;
        boolean z = true;
        long j = 0;
        boolean z2 = false;
        while (true) {
            if (i + i4 == i3) {
                if (i4 == cArr.length) {
                    return i2;
                }
                if (!m12190a(i4 + 1)) {
                    break;
                }
                i = this.f7456S;
                i3 = this.f7457T;
            }
            c = cArr[i + i4];
            if (c == '+') {
                i2 = 0;
                if (c2 != 5) {
                    return 0;
                }
            } else if (c == 'E' || c == 'e') {
                i2 = 0;
                if (c2 != 2 && c2 != 4) {
                    return 0;
                }
                c2 = 5;
                i4++;
            } else {
                if (c == '-') {
                    i2 = 0;
                    if (c2 == 0) {
                        c2 = 1;
                        z2 = true;
                    } else if (c2 != 5) {
                        return 0;
                    }
                } else if (c == '.') {
                    i2 = 0;
                    if (c2 != 2) {
                        return 0;
                    }
                    c2 = 3;
                } else if (c >= '0' && c <= '9') {
                    if (c2 == 1 || c2 == 0) {
                        j = (long) (-(c - '0'));
                        i2 = 0;
                        c2 = 2;
                    } else {
                        if (c2 == 2) {
                            if (j == 0) {
                                return 0;
                            }
                            long j2 = (10 * j) - ((long) (c - '0'));
                            int i5 = (j > -922337203685477580L ? 1 : (j == -922337203685477580L ? 0 : -1));
                            boolean z3 = i5 > 0 || (i5 == 0 && j2 < j);
                            j = j2;
                            z = z3 & z;
                        } else if (c2 == 3) {
                            i2 = 0;
                            c2 = 4;
                        } else if (c2 == 5 || c2 == 6) {
                            i2 = 0;
                            c2 = 7;
                        }
                        i2 = 0;
                    }
                }
                i4++;
            }
            c2 = 6;
            i4++;
        }
        if (m12189a(c)) {
            return 0;
        }
        if (c2 == 2 && z && ((j != Long.MIN_VALUE || z2) && (j != 0 || !z2))) {
            if (!z2) {
                j = -j;
            }
            this.f7461X = j;
            this.f7456S += i4;
            this.f7460W = 15;
            return 15;
        } else if (c2 != 2 && c2 != 4 && c2 != 7) {
            return 0;
        } else {
            this.f7462Y = i4;
            this.f7460W = 16;
            return 16;
        }
    }

    /* renamed from: K */
    private char m12186K() {
        int i;
        int i2;
        if (this.f7456S != this.f7457T || m12190a(1)) {
            char[] cArr = this.f7455R;
            int i3 = this.f7456S;
            this.f7456S = i3 + 1;
            char c = cArr[i3];
            if (c == 10) {
                this.f7458U++;
                this.f7459V = this.f7456S;
            } else if (!(c == '\"' || c == '\'' || c == '/' || c == '\\')) {
                if (c == 'b') {
                    return 8;
                }
                if (c == 'f') {
                    return 12;
                }
                if (c == 'n') {
                    return 10;
                }
                if (c == 'r') {
                    return 13;
                }
                if (c == 't') {
                    return 9;
                }
                if (c != 'u') {
                    m12195c("Invalid escape sequence");
                    throw null;
                } else if (this.f7456S + 4 <= this.f7457T || m12190a(4)) {
                    char c2 = 0;
                    int i4 = this.f7456S;
                    int i5 = i4 + 4;
                    while (i4 < i5) {
                        char c3 = this.f7455R[i4];
                        char c4 = (char) (c2 << 4);
                        if (c3 < '0' || c3 > '9') {
                            if (c3 >= 'a' && c3 <= 'f') {
                                i = c3 - 'a';
                            } else if (c3 < 'A' || c3 > 'F') {
                                throw new NumberFormatException("\\u" + new String(this.f7455R, this.f7456S, 4));
                            } else {
                                i = c3 - 'A';
                            }
                            i2 = i + 10;
                        } else {
                            i2 = c3 - '0';
                        }
                        c2 = (char) (c4 + i2);
                        i4++;
                    }
                    this.f7456S += 4;
                    return c2;
                } else {
                    m12195c("Unterminated escape sequence");
                    throw null;
                }
            }
            return c;
        }
        m12195c("Unterminated escape sequence");
        throw null;
    }

    /* renamed from: L */
    private void m12187L() {
        char c;
        do {
            if (this.f7456S < this.f7457T || m12190a(1)) {
                char[] cArr = this.f7455R;
                int i = this.f7456S;
                this.f7456S = i + 1;
                c = cArr[i];
                if (c == 10) {
                    this.f7458U++;
                    this.f7459V = this.f7456S;
                    return;
                }
            } else {
                return;
            }
        } while (c != 13);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0048, code lost:
        mo23930F();
     */
    /* renamed from: M */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m12188M() {
        /*
            r4 = this;
        L_0x0000:
            r0 = 0
        L_0x0001:
            int r1 = r4.f7456S
            int r2 = r1 + r0
            int r3 = r4.f7457T
            if (r2 >= r3) goto L_0x0051
            char[] r2 = r4.f7455R
            int r1 = r1 + r0
            char r1 = r2[r1]
            r2 = 9
            if (r1 == r2) goto L_0x004b
            r2 = 10
            if (r1 == r2) goto L_0x004b
            r2 = 12
            if (r1 == r2) goto L_0x004b
            r2 = 13
            if (r1 == r2) goto L_0x004b
            r2 = 32
            if (r1 == r2) goto L_0x004b
            r2 = 35
            if (r1 == r2) goto L_0x0048
            r2 = 44
            if (r1 == r2) goto L_0x004b
            r2 = 47
            if (r1 == r2) goto L_0x0048
            r2 = 61
            if (r1 == r2) goto L_0x0048
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 == r2) goto L_0x004b
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 == r2) goto L_0x004b
            r2 = 58
            if (r1 == r2) goto L_0x004b
            r2 = 59
            if (r1 == r2) goto L_0x0048
            switch(r1) {
                case 91: goto L_0x004b;
                case 92: goto L_0x0048;
                case 93: goto L_0x004b;
                default: goto L_0x0045;
            }
        L_0x0045:
            int r0 = r0 + 1
            goto L_0x0001
        L_0x0048:
            r4.mo23930F()
        L_0x004b:
            int r1 = r4.f7456S
            int r1 = r1 + r0
            r4.f7456S = r1
            return
        L_0x0051:
            int r1 = r1 + r0
            r4.f7456S = r1
            r0 = 1
            boolean r0 = r4.m12190a(r0)
            if (r0 != 0) goto L_0x0000
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p169a0.JsonReader.m12188M():void");
    }

    /* renamed from: A */
    public String mo23739A() {
        String str;
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 14) {
            str = m12183H();
        } else if (i == 12) {
            str = m12192b('\'');
        } else if (i == 13) {
            str = m12192b('\"');
        } else {
            throw new IllegalStateException("Expected a name but was " + mo23742D() + mo23755v());
        }
        this.f7460W = 0;
        this.f7466c0[this.f7465b0 - 1] = str;
        return str;
    }

    /* renamed from: B */
    public void mo23740B() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 7) {
            this.f7460W = 0;
            int[] iArr = this.f7467d0;
            int i2 = this.f7465b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return;
        }
        throw new IllegalStateException("Expected null but was " + mo23742D() + mo23755v());
    }

    /* renamed from: C */
    public String mo23741C() {
        String str;
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 10) {
            str = m12183H();
        } else if (i == 8) {
            str = m12192b('\'');
        } else if (i == 9) {
            str = m12192b('\"');
        } else if (i == 11) {
            str = this.f7463Z;
            this.f7463Z = null;
        } else if (i == 15) {
            str = Long.toString(this.f7461X);
        } else if (i == 16) {
            str = new String(this.f7455R, this.f7456S, this.f7462Y);
            this.f7456S += this.f7462Y;
        } else {
            throw new IllegalStateException("Expected a string but was " + mo23742D() + mo23755v());
        }
        this.f7460W = 0;
        int[] iArr = this.f7467d0;
        int i2 = this.f7465b0 - 1;
        iArr[i2] = iArr[i2] + 1;
        return str;
    }

    /* renamed from: D */
    public JsonToken mo23742D() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        switch (i) {
            case 1:
                return JsonToken.BEGIN_OBJECT;
            case 2:
                return JsonToken.END_OBJECT;
            case 3:
                return JsonToken.BEGIN_ARRAY;
            case 4:
                return JsonToken.END_ARRAY;
            case 5:
            case 6:
                return JsonToken.BOOLEAN;
            case 7:
                return JsonToken.NULL;
            case 8:
            case 9:
            case 10:
            case 11:
                return JsonToken.STRING;
            case 12:
            case 13:
            case 14:
                return JsonToken.NAME;
            case 15:
            case 16:
                return JsonToken.NUMBER;
            case 17:
                return JsonToken.END_DOCUMENT;
            default:
                throw new AssertionError();
        }
    }

    /* renamed from: E */
    public void mo23743E() {
        int i = 0;
        do {
            int i2 = this.f7460W;
            if (i2 == 0) {
                i2 = mo23747c();
            }
            if (i2 == 3) {
                m12193b(1);
            } else if (i2 == 1) {
                m12193b(3);
            } else {
                if (i2 == 4) {
                    this.f7465b0--;
                } else if (i2 == 2) {
                    this.f7465b0--;
                } else if (i2 == 14 || i2 == 10) {
                    m12188M();
                    this.f7460W = 0;
                } else if (i2 == 8 || i2 == 12) {
                    m12196c('\'');
                    this.f7460W = 0;
                } else if (i2 == 9 || i2 == 13) {
                    m12196c('\"');
                    this.f7460W = 0;
                } else {
                    if (i2 == 16) {
                        this.f7456S += this.f7462Y;
                    }
                    this.f7460W = 0;
                }
                i--;
                this.f7460W = 0;
            }
            i++;
            this.f7460W = 0;
        } while (i != 0);
        int[] iArr = this.f7467d0;
        int i3 = this.f7465b0;
        int i4 = i3 - 1;
        iArr[i4] = iArr[i4] + 1;
        this.f7466c0[i3 - 1] = "null";
    }

    /* renamed from: a */
    public final void mo23745a(boolean z) {
        this.f7454Q = z;
    }

    /* renamed from: b */
    public void mo23746b() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 1) {
            m12193b(3);
            this.f7460W = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_OBJECT but was " + mo23742D() + mo23755v());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo23747c() {
        int b;
        int[] iArr = this.f7464a0;
        int i = this.f7465b0;
        int i2 = iArr[i - 1];
        if (i2 == 1) {
            iArr[i - 1] = 2;
        } else if (i2 == 2) {
            int b2 = m12191b(true);
            if (b2 != 44) {
                if (b2 == 59) {
                    mo23930F();
                } else if (b2 == 93) {
                    this.f7460W = 4;
                    return 4;
                } else {
                    m12195c("Unterminated array");
                    throw null;
                }
            }
        } else if (i2 == 3 || i2 == 5) {
            this.f7464a0[this.f7465b0 - 1] = 4;
            if (i2 == 5 && (b = m12191b(true)) != 44) {
                if (b == 59) {
                    mo23930F();
                } else if (b == 125) {
                    this.f7460W = 2;
                    return 2;
                } else {
                    m12195c("Unterminated object");
                    throw null;
                }
            }
            int b3 = m12191b(true);
            if (b3 == 34) {
                this.f7460W = 13;
                return 13;
            } else if (b3 == 39) {
                mo23930F();
                this.f7460W = 12;
                return 12;
            } else if (b3 != 125) {
                mo23930F();
                this.f7456S--;
                if (m12189a((char) b3)) {
                    this.f7460W = 14;
                    return 14;
                }
                m12195c("Expected name");
                throw null;
            } else if (i2 != 5) {
                this.f7460W = 2;
                return 2;
            } else {
                m12195c("Expected name");
                throw null;
            }
        } else if (i2 == 4) {
            iArr[i - 1] = 5;
            int b4 = m12191b(true);
            if (b4 != 58) {
                if (b4 == 61) {
                    mo23930F();
                    if (this.f7456S < this.f7457T || m12190a(1)) {
                        char[] cArr = this.f7455R;
                        int i3 = this.f7456S;
                        if (cArr[i3] == '>') {
                            this.f7456S = i3 + 1;
                        }
                    }
                } else {
                    m12195c("Expected ':'");
                    throw null;
                }
            }
        } else if (i2 == 6) {
            if (this.f7454Q) {
                m12182G();
            }
            this.f7464a0[this.f7465b0 - 1] = 7;
        } else if (i2 == 7) {
            if (m12191b(false) == -1) {
                this.f7460W = 17;
                return 17;
            }
            mo23930F();
            this.f7456S--;
        } else if (i2 == 8) {
            throw new IllegalStateException("JsonReader is closed");
        }
        int b5 = m12191b(true);
        if (b5 == 34) {
            this.f7460W = 9;
            return 9;
        } else if (b5 != 39) {
            if (!(b5 == 44 || b5 == 59)) {
                if (b5 == 91) {
                    this.f7460W = 3;
                    return 3;
                } else if (b5 != 93) {
                    if (b5 != 123) {
                        this.f7456S--;
                        int I = m12184I();
                        if (I != 0) {
                            return I;
                        }
                        int J = m12185J();
                        if (J != 0) {
                            return J;
                        }
                        if (m12189a(this.f7455R[this.f7456S])) {
                            mo23930F();
                            this.f7460W = 10;
                            return 10;
                        }
                        m12195c("Expected value");
                        throw null;
                    }
                    this.f7460W = 1;
                    return 1;
                } else if (i2 == 1) {
                    this.f7460W = 4;
                    return 4;
                }
            }
            if (i2 == 1 || i2 == 2) {
                mo23930F();
                this.f7456S--;
                this.f7460W = 7;
                return 7;
            }
            m12195c("Unexpected value");
            throw null;
        } else {
            mo23930F();
            this.f7460W = 8;
            return 8;
        }
    }

    public void close() {
        this.f7460W = 0;
        this.f7464a0[0] = 8;
        this.f7465b0 = 1;
        this.f7453P.close();
    }

    /* renamed from: d */
    public void mo23749d() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 4) {
            this.f7465b0--;
            int[] iArr = this.f7467d0;
            int i2 = this.f7465b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            this.f7460W = 0;
            return;
        }
        throw new IllegalStateException("Expected END_ARRAY but was " + mo23742D() + mo23755v());
    }

    /* renamed from: e */
    public void mo23750e() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 2) {
            this.f7465b0--;
            String[] strArr = this.f7466c0;
            int i2 = this.f7465b0;
            strArr[i2] = null;
            int[] iArr = this.f7467d0;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
            this.f7460W = 0;
            return;
        }
        throw new IllegalStateException("Expected END_OBJECT but was " + mo23742D() + mo23755v());
    }

    /* renamed from: g */
    public String mo23751g() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i = this.f7465b0;
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.f7464a0[i2];
            if (i3 == 1 || i3 == 2) {
                sb.append('[');
                sb.append(this.f7467d0[i2]);
                sb.append(']');
            } else if (i3 == 3 || i3 == 4 || i3 == 5) {
                sb.append((char) JwtParser.SEPARATOR_CHAR);
                String[] strArr = this.f7466c0;
                if (strArr[i2] != null) {
                    sb.append(strArr[i2]);
                }
            }
        }
        return sb.toString();
    }

    /* renamed from: t */
    public boolean mo23752t() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        return (i == 2 || i == 4) ? false : true;
    }

    public String toString() {
        return getClass().getSimpleName() + mo23755v();
    }

    /* renamed from: u */
    public final boolean mo23754u() {
        return this.f7454Q;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: v */
    public String mo23755v() {
        return " at line " + (this.f7458U + 1) + " column " + ((this.f7456S - this.f7459V) + 1) + " path " + mo23751g();
    }

    /* renamed from: w */
    public boolean mo23756w() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 5) {
            this.f7460W = 0;
            int[] iArr = this.f7467d0;
            int i2 = this.f7465b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return true;
        } else if (i == 6) {
            this.f7460W = 0;
            int[] iArr2 = this.f7467d0;
            int i3 = this.f7465b0 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return false;
        } else {
            throw new IllegalStateException("Expected a boolean but was " + mo23742D() + mo23755v());
        }
    }

    /* renamed from: x */
    public double mo23757x() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 15) {
            this.f7460W = 0;
            int[] iArr = this.f7467d0;
            int i2 = this.f7465b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return (double) this.f7461X;
        }
        if (i == 16) {
            this.f7463Z = new String(this.f7455R, this.f7456S, this.f7462Y);
            this.f7456S += this.f7462Y;
        } else if (i == 8 || i == 9) {
            this.f7463Z = m12192b(i == 8 ? '\'' : '\"');
        } else if (i == 10) {
            this.f7463Z = m12183H();
        } else if (i != 11) {
            throw new IllegalStateException("Expected a double but was " + mo23742D() + mo23755v());
        }
        this.f7460W = 11;
        double parseDouble = Double.parseDouble(this.f7463Z);
        if (this.f7454Q || (!Double.isNaN(parseDouble) && !Double.isInfinite(parseDouble))) {
            this.f7463Z = null;
            this.f7460W = 0;
            int[] iArr2 = this.f7467d0;
            int i3 = this.f7465b0 - 1;
            iArr2[i3] = iArr2[i3] + 1;
            return parseDouble;
        }
        throw new MalformedJsonException("JSON forbids NaN and infinities: " + parseDouble + mo23755v());
    }

    /* renamed from: y */
    public int mo23758y() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 15) {
            long j = this.f7461X;
            int i2 = (int) j;
            if (j == ((long) i2)) {
                this.f7460W = 0;
                int[] iArr = this.f7467d0;
                int i3 = this.f7465b0 - 1;
                iArr[i3] = iArr[i3] + 1;
                return i2;
            }
            throw new NumberFormatException("Expected an int but was " + this.f7461X + mo23755v());
        }
        if (i == 16) {
            this.f7463Z = new String(this.f7455R, this.f7456S, this.f7462Y);
            this.f7456S += this.f7462Y;
        } else if (i == 8 || i == 9 || i == 10) {
            if (i == 10) {
                this.f7463Z = m12183H();
            } else {
                this.f7463Z = m12192b(i == 8 ? '\'' : '\"');
            }
            try {
                int parseInt = Integer.parseInt(this.f7463Z);
                this.f7460W = 0;
                int[] iArr2 = this.f7467d0;
                int i4 = this.f7465b0 - 1;
                iArr2[i4] = iArr2[i4] + 1;
                return parseInt;
            } catch (NumberFormatException unused) {
            }
        } else {
            throw new IllegalStateException("Expected an int but was " + mo23742D() + mo23755v());
        }
        this.f7460W = 11;
        double parseDouble = Double.parseDouble(this.f7463Z);
        int i5 = (int) parseDouble;
        if (((double) i5) == parseDouble) {
            this.f7463Z = null;
            this.f7460W = 0;
            int[] iArr3 = this.f7467d0;
            int i6 = this.f7465b0 - 1;
            iArr3[i6] = iArr3[i6] + 1;
            return i5;
        }
        throw new NumberFormatException("Expected an int but was " + this.f7463Z + mo23755v());
    }

    /* renamed from: z */
    public long mo23759z() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 15) {
            this.f7460W = 0;
            int[] iArr = this.f7467d0;
            int i2 = this.f7465b0 - 1;
            iArr[i2] = iArr[i2] + 1;
            return this.f7461X;
        }
        if (i == 16) {
            this.f7463Z = new String(this.f7455R, this.f7456S, this.f7462Y);
            this.f7456S += this.f7462Y;
        } else if (i == 8 || i == 9 || i == 10) {
            if (i == 10) {
                this.f7463Z = m12183H();
            } else {
                this.f7463Z = m12192b(i == 8 ? '\'' : '\"');
            }
            try {
                long parseLong = Long.parseLong(this.f7463Z);
                this.f7460W = 0;
                int[] iArr2 = this.f7467d0;
                int i3 = this.f7465b0 - 1;
                iArr2[i3] = iArr2[i3] + 1;
                return parseLong;
            } catch (NumberFormatException unused) {
            }
        } else {
            throw new IllegalStateException("Expected a long but was " + mo23742D() + mo23755v());
        }
        this.f7460W = 11;
        double parseDouble = Double.parseDouble(this.f7463Z);
        long j = (long) parseDouble;
        if (((double) j) == parseDouble) {
            this.f7463Z = null;
            this.f7460W = 0;
            int[] iArr3 = this.f7467d0;
            int i4 = this.f7465b0 - 1;
            iArr3[i4] = iArr3[i4] + 1;
            return j;
        }
        throw new NumberFormatException("Expected a long but was " + this.f7463Z + mo23755v());
    }

    /* renamed from: a */
    public void mo23744a() {
        int i = this.f7460W;
        if (i == 0) {
            i = mo23747c();
        }
        if (i == 3) {
            m12193b(1);
            this.f7467d0[this.f7465b0 - 1] = 0;
            this.f7460W = 0;
            return;
        }
        throw new IllegalStateException("Expected BEGIN_ARRAY but was " + mo23742D() + mo23755v());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:23:0x005c, code lost:
        if (r2 != null) goto L_0x006c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005e, code lost:
        r2 = new java.lang.StringBuilder(java.lang.Math.max((r3 - r5) * 2, 16));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        r2.append(r0, r5, r3 - r5);
        r10.f7456S = r3;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m12192b(char r11) {
        /*
            r10 = this;
            char[] r0 = r10.f7455R
            r1 = 0
            r2 = r1
        L_0x0004:
            int r3 = r10.f7456S
            int r4 = r10.f7457T
        L_0x0008:
            r5 = r3
        L_0x0009:
            r6 = 16
            r7 = 1
            if (r3 >= r4) goto L_0x005c
            int r8 = r3 + 1
            char r3 = r0[r3]
            if (r3 != r11) goto L_0x0028
            r10.f7456S = r8
            int r8 = r8 - r5
            int r8 = r8 - r7
            if (r2 != 0) goto L_0x0020
            java.lang.String r11 = new java.lang.String
            r11.<init>(r0, r5, r8)
            return r11
        L_0x0020:
            r2.append(r0, r5, r8)
            java.lang.String r11 = r2.toString()
            return r11
        L_0x0028:
            r9 = 92
            if (r3 != r9) goto L_0x004f
            r10.f7456S = r8
            int r8 = r8 - r5
            int r8 = r8 - r7
            if (r2 != 0) goto L_0x0040
            int r2 = r8 + 1
            int r2 = r2 * 2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            int r2 = java.lang.Math.max(r2, r6)
            r3.<init>(r2)
            r2 = r3
        L_0x0040:
            r2.append(r0, r5, r8)
            char r3 = r10.m12186K()
            r2.append(r3)
            int r3 = r10.f7456S
            int r4 = r10.f7457T
            goto L_0x0008
        L_0x004f:
            r6 = 10
            if (r3 != r6) goto L_0x005a
            int r3 = r10.f7458U
            int r3 = r3 + r7
            r10.f7458U = r3
            r10.f7459V = r8
        L_0x005a:
            r3 = r8
            goto L_0x0009
        L_0x005c:
            if (r2 != 0) goto L_0x006c
            int r2 = r3 - r5
            int r2 = r2 * 2
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            int r2 = java.lang.Math.max(r2, r6)
            r4.<init>(r2)
            r2 = r4
        L_0x006c:
            int r4 = r3 - r5
            r2.append(r0, r5, r4)
            r10.f7456S = r3
            boolean r3 = r10.m12190a(r7)
            if (r3 == 0) goto L_0x007a
            goto L_0x0004
        L_0x007a:
            java.lang.String r11 = "Unterminated string"
            r10.m12195c(r11)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p168b.p169a0.JsonReader.m12192b(char):java.lang.String");
    }

    /* renamed from: a */
    private boolean m12189a(char c) {
        if (c == 9 || c == 10 || c == 12 || c == 13 || c == ' ') {
            return false;
        }
        if (c != '#') {
            if (c == ',') {
                return false;
            }
            if (!(c == '/' || c == '=')) {
                if (c == '{' || c == '}' || c == ':') {
                    return false;
                }
                if (c != ';') {
                    switch (c) {
                        case '[':
                        case ']':
                            return false;
                        case '\\':
                            break;
                        default:
                            return true;
                    }
                }
            }
        }
        mo23930F();
        return false;
    }

    /* renamed from: a */
    private boolean m12190a(int i) {
        int i2;
        char[] cArr = this.f7455R;
        int i3 = this.f7459V;
        int i4 = this.f7456S;
        this.f7459V = i3 - i4;
        int i5 = this.f7457T;
        if (i5 != i4) {
            this.f7457T = i5 - i4;
            System.arraycopy(cArr, i4, cArr, 0, this.f7457T);
        } else {
            this.f7457T = 0;
        }
        this.f7456S = 0;
        do {
            Reader reader = this.f7453P;
            int i6 = this.f7457T;
            int read = reader.read(cArr, i6, cArr.length - i6);
            if (read == -1) {
                return false;
            }
            this.f7457T += read;
            if (this.f7458U == 0 && (i2 = this.f7459V) == 0 && this.f7457T > 0 && cArr[0] == 65279) {
                this.f7456S++;
                this.f7459V = i2 + 1;
                i++;
            }
        } while (this.f7457T < i);
        return true;
    }

    /* renamed from: b */
    private void m12193b(int i) {
        int i2 = this.f7465b0;
        int[] iArr = this.f7464a0;
        if (i2 == iArr.length) {
            int i3 = i2 * 2;
            this.f7464a0 = Arrays.copyOf(iArr, i3);
            this.f7467d0 = Arrays.copyOf(this.f7467d0, i3);
            this.f7466c0 = (String[]) Arrays.copyOf(this.f7466c0, i3);
        }
        int[] iArr2 = this.f7464a0;
        int i4 = this.f7465b0;
        this.f7465b0 = i4 + 1;
        iArr2[i4] = i;
    }

    /* renamed from: b */
    private int m12191b(boolean z) {
        char[] cArr = this.f7455R;
        int i = this.f7456S;
        int i2 = this.f7457T;
        while (true) {
            if (i == i2) {
                this.f7456S = i;
                if (m12190a(1)) {
                    i = this.f7456S;
                    i2 = this.f7457T;
                } else if (!z) {
                    return -1;
                } else {
                    throw new EOFException("End of input" + mo23755v());
                }
            }
            int i3 = i + 1;
            char c = cArr[i];
            if (c == 10) {
                this.f7458U++;
                this.f7459V = i3;
            } else if (!(c == ' ' || c == 13 || c == 9)) {
                if (c == '/') {
                    this.f7456S = i3;
                    if (i3 == i2) {
                        this.f7456S--;
                        boolean a = m12190a(2);
                        this.f7456S++;
                        if (!a) {
                            return c;
                        }
                    }
                    mo23930F();
                    int i4 = this.f7456S;
                    char c2 = cArr[i4];
                    if (c2 == '*') {
                        this.f7456S = i4 + 1;
                        if (m12194b("*/")) {
                            i = this.f7456S + 2;
                            i2 = this.f7457T;
                        } else {
                            m12195c("Unterminated comment");
                            throw null;
                        }
                    } else if (c2 != '/') {
                        return c;
                    } else {
                        this.f7456S = i4 + 1;
                        m12187L();
                        i = this.f7456S;
                        i2 = this.f7457T;
                    }
                } else if (c == '#') {
                    this.f7456S = i3;
                    mo23930F();
                    m12187L();
                    i = this.f7456S;
                    i2 = this.f7457T;
                } else {
                    this.f7456S = i3;
                    return c;
                }
            }
            i = i3;
        }
    }

    /* renamed from: c */
    private void m12196c(char c) {
        char[] cArr = this.f7455R;
        do {
            int i = this.f7456S;
            int i2 = this.f7457T;
            while (i < i2) {
                int i3 = i + 1;
                char c2 = cArr[i];
                if (c2 == c) {
                    this.f7456S = i3;
                    return;
                } else if (c2 == '\\') {
                    this.f7456S = i3;
                    m12186K();
                    i = this.f7456S;
                    i2 = this.f7457T;
                } else {
                    if (c2 == 10) {
                        this.f7458U++;
                        this.f7459V = i3;
                    }
                    i = i3;
                }
            }
            this.f7456S = i;
        } while (m12190a(1));
        m12195c("Unterminated string");
        throw null;
    }

    /* renamed from: b */
    private boolean m12194b(String str) {
        int length = str.length();
        while (true) {
            int i = 0;
            if (this.f7456S + length > this.f7457T && !m12190a(length)) {
                return false;
            }
            char[] cArr = this.f7455R;
            int i2 = this.f7456S;
            if (cArr[i2] == 10) {
                this.f7458U++;
                this.f7459V = i2 + 1;
            } else {
                while (i < length) {
                    if (this.f7455R[this.f7456S + i] == str.charAt(i)) {
                        i++;
                    }
                }
                return true;
            }
            this.f7456S++;
        }
    }

    /* renamed from: c */
    private IOException m12195c(String str) {
        throw new MalformedJsonException(str + mo23755v());
    }
}
