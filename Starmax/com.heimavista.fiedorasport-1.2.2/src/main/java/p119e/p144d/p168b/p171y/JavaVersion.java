package p119e.p144d.p168b.p171y;

/* renamed from: e.d.b.y.e */
public final class JavaVersion {

    /* renamed from: a */
    private static final int f7566a = m12391a();

    /* renamed from: a */
    private static int m12391a() {
        return m12394b(System.getProperty("java.version"));
    }

    /* renamed from: b */
    static int m12394b(String str) {
        int c = m12395c(str);
        if (c == -1) {
            c = m12392a(str);
        }
        if (c == -1) {
            return 6;
        }
        return c;
    }

    /* renamed from: c */
    private static int m12395c(String str) {
        try {
            String[] split = str.split("[._]");
            int parseInt = Integer.parseInt(split[0]);
            return (parseInt != 1 || split.length <= 1) ? parseInt : Integer.parseInt(split[1]);
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    /* renamed from: a */
    private static int m12392a(String str) {
        try {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char charAt = str.charAt(i);
                if (!Character.isDigit(charAt)) {
                    break;
                }
                sb.append(charAt);
            }
            return Integer.parseInt(sb.toString());
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    /* renamed from: b */
    public static int m12393b() {
        return f7566a;
    }

    /* renamed from: c */
    public static boolean m12396c() {
        return f7566a >= 9;
    }
}
