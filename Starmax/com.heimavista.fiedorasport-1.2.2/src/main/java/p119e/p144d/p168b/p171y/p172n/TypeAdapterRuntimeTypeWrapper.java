package p119e.p144d.p168b.p171y.p172n;

import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import p119e.p144d.p168b.Gson;
import p119e.p144d.p168b.TypeAdapter;
import p119e.p144d.p168b.p169a0.JsonReader;
import p119e.p144d.p168b.p169a0.JsonWriter;
import p119e.p144d.p168b.p171y.p172n.ReflectiveTypeAdapterFactory;
import p119e.p144d.p168b.p175z.TypeToken;

/* renamed from: e.d.b.y.n.m */
final class TypeAdapterRuntimeTypeWrapper<T> extends TypeAdapter<T> {

    /* renamed from: a */
    private final Gson f7654a;

    /* renamed from: b */
    private final TypeAdapter<T> f7655b;

    /* renamed from: c */
    private final Type f7656c;

    TypeAdapterRuntimeTypeWrapper(Gson fVar, TypeAdapter<T> vVar, Type type) {
        this.f7654a = fVar;
        this.f7655b = super;
        this.f7656c = type;
    }

    /* renamed from: a */
    public T mo23735a(JsonReader aVar) {
        return this.f7655b.mo23735a(aVar);
    }

    /* renamed from: a */
    public void mo23736a(JsonWriter cVar, T t) {
        TypeAdapter<T> vVar = this.f7655b;
        Type a = m12516a(this.f7656c, t);
        if (a != this.f7656c) {
            vVar = this.f7654a.mo23787a((TypeToken) TypeToken.m12663a(a));
            if (vVar instanceof ReflectiveTypeAdapterFactory.C4143b) {
                TypeAdapter<T> vVar2 = this.f7655b;
                if (!(vVar2 instanceof ReflectiveTypeAdapterFactory.C4143b)) {
                    vVar = vVar2;
                }
            }
        }
        super.mo23736a(cVar, t);
    }

    /* renamed from: a */
    private Type m12516a(Type type, Object obj) {
        if (obj != null) {
            return (type == Object.class || (type instanceof TypeVariable) || (type instanceof Class)) ? obj.getClass() : type;
        }
        return type;
    }
}
