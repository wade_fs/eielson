package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.k */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3948k implements C3963z.C3965b {

    /* renamed from: a */
    private static final C3948k f7299a = new C3948k();

    private C3948k() {
    }

    /* renamed from: a */
    public static C3963z.C3965b m11869a() {
        return f7299a;
    }

    public Object apply(Object obj) {
        return C3963z.m11914c((SQLiteDatabase) obj);
    }
}
