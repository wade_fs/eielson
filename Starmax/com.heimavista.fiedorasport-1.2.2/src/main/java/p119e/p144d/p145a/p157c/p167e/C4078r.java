package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.r */
final class C4078r<TResult> implements C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7430a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Object f7431b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C4057c<TResult> f7432c;

    public C4078r(@NonNull Executor executor, @NonNull C4057c<TResult> cVar) {
        this.f7430a = executor;
        this.f7432c = cVar;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h<TResult> hVar) {
        synchronized (this.f7431b) {
            if (this.f7432c != null) {
                this.f7430a.execute(new C4079s(this, hVar));
            }
        }
    }

    public final void cancel() {
        synchronized (this.f7431b) {
            this.f7432c = null;
        }
    }
}
