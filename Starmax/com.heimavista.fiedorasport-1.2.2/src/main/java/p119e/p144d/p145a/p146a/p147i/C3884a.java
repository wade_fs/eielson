package p119e.p144d.p145a.p146a.p147i;

import androidx.annotation.Nullable;
import java.util.Map;
import p119e.p144d.p145a.p146a.p147i.C3899h;

/* renamed from: e.d.a.a.i.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3884a extends C3899h {

    /* renamed from: a */
    private final String f7179a;

    /* renamed from: b */
    private final Integer f7180b;

    /* renamed from: c */
    private final C3898g f7181c;

    /* renamed from: d */
    private final long f7182d;

    /* renamed from: e */
    private final long f7183e;

    /* renamed from: f */
    private final Map<String, String> f7184f;

    /* renamed from: e.d.a.a.i.a$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static final class C3886b extends C3899h.C3900a {

        /* renamed from: a */
        private String f7185a;

        /* renamed from: b */
        private Integer f7186b;

        /* renamed from: c */
        private C3898g f7187c;

        /* renamed from: d */
        private Long f7188d;

        /* renamed from: e */
        private Long f7189e;

        /* renamed from: f */
        private Map<String, String> f7190f;

        C3886b() {
        }

        /* renamed from: a */
        public C3899h.C3900a mo23512a(String str) {
            if (str != null) {
                this.f7185a = str;
                return super;
            }
            throw new NullPointerException("Null transportName");
        }

        /* renamed from: b */
        public C3899h.C3900a mo23515b(long j) {
            this.f7189e = Long.valueOf(j);
            return super;
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public Map<String, String> mo23516b() {
            Map<String, String> map = this.f7190f;
            if (map != null) {
                return map;
            }
            throw new IllegalStateException("Property \"autoMetadata\" has not been set");
        }

        /* renamed from: a */
        public C3899h.C3900a mo23511a(Integer num) {
            this.f7186b = num;
            return super;
        }

        /* renamed from: a */
        public C3899h.C3900a mo23510a(C3898g gVar) {
            if (gVar != null) {
                this.f7187c = gVar;
                return super;
            }
            throw new NullPointerException("Null encodedPayload");
        }

        /* renamed from: a */
        public C3899h.C3900a mo23509a(long j) {
            this.f7188d = Long.valueOf(j);
            return super;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public C3899h.C3900a mo23513a(Map<String, String> map) {
            if (map != null) {
                this.f7190f = map;
                return super;
            }
            throw new NullPointerException("Null autoMetadata");
        }

        /* renamed from: a */
        public C3899h mo23514a() {
            String str = "";
            if (this.f7185a == null) {
                str = str + " transportName";
            }
            if (this.f7187c == null) {
                str = str + " encodedPayload";
            }
            if (this.f7188d == null) {
                str = str + " eventMillis";
            }
            if (this.f7189e == null) {
                str = str + " uptimeMillis";
            }
            if (this.f7190f == null) {
                str = str + " autoMetadata";
            }
            if (str.isEmpty()) {
                return new C3884a(this.f7185a, this.f7186b, this.f7187c, this.f7188d.longValue(), this.f7189e.longValue(), this.f7190f);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Map<String, String> mo23500a() {
        return this.f7184f;
    }

    @Nullable
    /* renamed from: b */
    public Integer mo23501b() {
        return this.f7180b;
    }

    /* renamed from: c */
    public C3898g mo23502c() {
        return this.f7181c;
    }

    /* renamed from: d */
    public long mo23503d() {
        return this.f7182d;
    }

    public boolean equals(Object obj) {
        Integer num;
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3899h)) {
            return false;
        }
        C3899h hVar = (C3899h) obj;
        if (!this.f7179a.equals(super.mo23505f()) || ((num = this.f7180b) != null ? !num.equals(super.mo23501b()) : super.mo23501b() != null) || !this.f7181c.equals(super.mo23502c()) || this.f7182d != super.mo23503d() || this.f7183e != super.mo23506g() || !this.f7184f.equals(super.mo23500a())) {
            return false;
        }
        return true;
    }

    /* renamed from: f */
    public String mo23505f() {
        return this.f7179a;
    }

    /* renamed from: g */
    public long mo23506g() {
        return this.f7183e;
    }

    public int hashCode() {
        int hashCode = (this.f7179a.hashCode() ^ 1000003) * 1000003;
        Integer num = this.f7180b;
        int hashCode2 = num == null ? 0 : num.hashCode();
        long j = this.f7182d;
        long j2 = this.f7183e;
        return ((((((((hashCode ^ hashCode2) * 1000003) ^ this.f7181c.hashCode()) * 1000003) ^ ((int) (j ^ (j >>> 32)))) * 1000003) ^ ((int) (j2 ^ (j2 >>> 32)))) * 1000003) ^ this.f7184f.hashCode();
    }

    public String toString() {
        return "EventInternal{transportName=" + this.f7179a + ", code=" + this.f7180b + ", encodedPayload=" + this.f7181c + ", eventMillis=" + this.f7182d + ", uptimeMillis=" + this.f7183e + ", autoMetadata=" + this.f7184f + "}";
    }

    private C3884a(String str, @Nullable Integer num, C3898g gVar, long j, long j2, Map<String, String> map) {
        this.f7179a = str;
        this.f7180b = num;
        this.f7181c = gVar;
        this.f7182d = j;
        this.f7183e = j2;
        this.f7184f = map;
    }
}
