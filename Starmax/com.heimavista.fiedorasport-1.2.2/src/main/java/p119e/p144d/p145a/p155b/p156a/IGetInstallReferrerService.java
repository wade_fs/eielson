package p119e.p144d.p145a.p155b.p156a;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: e.d.a.b.a.a */
public interface IGetInstallReferrerService extends IInterface {

    /* renamed from: e.d.a.b.a.a$a */
    /* compiled from: IGetInstallReferrerService */
    public static abstract class C3978a extends Binder implements IGetInstallReferrerService {

        /* renamed from: e.d.a.b.a.a$a$a */
        /* compiled from: IGetInstallReferrerService */
        private static class C3979a implements IGetInstallReferrerService {

            /* renamed from: a */
            private IBinder f7329a;

            C3979a(IBinder iBinder) {
                this.f7329a = iBinder;
            }

            public IBinder asBinder() {
                return this.f7329a;
            }

            /* renamed from: e */
            public Bundle mo23605e(Bundle bundle) {
                Parcel obtain = Parcel.obtain();
                Parcel obtain2 = Parcel.obtain();
                try {
                    obtain.writeInterfaceToken("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
                    if (bundle != null) {
                        obtain.writeInt(1);
                        bundle.writeToParcel(obtain, 0);
                    } else {
                        obtain.writeInt(0);
                    }
                    this.f7329a.transact(1, obtain, obtain2, 0);
                    obtain2.readException();
                    return obtain2.readInt() != 0 ? (Bundle) Bundle.CREATOR.createFromParcel(obtain2) : null;
                } finally {
                    obtain2.recycle();
                    obtain.recycle();
                }
            }
        }

        /* renamed from: a */
        public static IGetInstallReferrerService m11942a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
            if (queryLocalInterface == null || !(queryLocalInterface instanceof IGetInstallReferrerService)) {
                return new C3979a(iBinder);
            }
            return (IGetInstallReferrerService) queryLocalInterface;
        }
    }

    /* renamed from: e */
    Bundle mo23605e(Bundle bundle);
}
