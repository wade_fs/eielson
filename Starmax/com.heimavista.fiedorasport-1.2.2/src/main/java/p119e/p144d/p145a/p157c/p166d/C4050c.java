package p119e.p144d.p145a.p157c.p166d;

import android.content.Context;
import android.os.Looper;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.C2036f;
import com.google.android.gms.common.internal.C2211e;
import com.google.android.gms.signin.internal.C3268a;

/* renamed from: e.d.a.c.d.c */
final class C4050c extends C2016a.C2017a<C3268a, C4047a> {
    C4050c() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.gms.signin.internal.a.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.e, e.d.a.c.d.a, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void
     arg types: [android.content.Context, android.os.Looper, int, com.google.android.gms.common.internal.e, e.d.a.c.d.a, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c]
     candidates:
      com.google.android.gms.signin.internal.a.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.e, android.os.Bundle, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void
      com.google.android.gms.signin.internal.a.<init>(android.content.Context, android.os.Looper, boolean, com.google.android.gms.common.internal.e, e.d.a.c.d.a, com.google.android.gms.common.api.f$b, com.google.android.gms.common.api.f$c):void */
    /* renamed from: a */
    public final /* synthetic */ C2016a.C2027f mo16371a(Context context, Looper looper, C2211e eVar, Object obj, C2036f.C2038b bVar, C2036f.C2039c cVar) {
        C4047a aVar = (C4047a) obj;
        if (aVar == null) {
            aVar = C4047a.f7385X;
        }
        return new C3268a(context, looper, true, eVar, aVar, bVar, cVar);
    }
}
