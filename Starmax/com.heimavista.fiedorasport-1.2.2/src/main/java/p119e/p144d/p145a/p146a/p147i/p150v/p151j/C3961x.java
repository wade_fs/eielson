package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.Cursor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.x */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3961x implements C3963z.C3965b {

    /* renamed from: a */
    private static final C3961x f7315a = new C3961x();

    private C3961x() {
    }

    /* renamed from: a */
    public static C3963z.C3965b m11884a() {
        return f7315a;
    }

    public Object apply(Object obj) {
        return C3963z.m11890a((Cursor) obj);
    }
}
