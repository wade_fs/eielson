package p119e.p144d.p145a.p157c.p160b;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;

/* renamed from: e.d.a.c.b.k */
final class C4000k implements View.OnClickListener {

    /* renamed from: P */
    private final /* synthetic */ Context f7365P;

    /* renamed from: Q */
    private final /* synthetic */ Intent f7366Q;

    C4000k(Context context, Intent intent) {
        this.f7365P = context;
        this.f7366Q = intent;
    }

    public final void onClick(View view) {
        try {
            this.f7365P.startActivity(this.f7366Q);
        } catch (ActivityNotFoundException e) {
            Log.e("DeferredLifecycleHelper", "Failed to start resolution intent", e);
        }
    }
}
