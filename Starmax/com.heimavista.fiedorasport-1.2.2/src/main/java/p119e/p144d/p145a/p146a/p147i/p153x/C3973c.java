package p119e.p144d.p145a.p146a.p147i.p153x;

import p224f.p225b.C4936d;
import p224f.p225b.Factory;

/* renamed from: e.d.a.a.i.x.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3973c implements Factory<C3971a> {

    /* renamed from: a */
    private static final C3973c f7325a = new C3973c();

    /* renamed from: a */
    public static C3973c m11933a() {
        return f7325a;
    }

    /* renamed from: b */
    public static C3971a m11934b() {
        C3971a a = C3972b.m11931a();
        C4936d.m17213a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }

    public C3971a get() {
        return m11934b();
    }
}
