package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.Parcel;
import com.google.android.gms.maps.model.LatLng;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: e.d.a.c.c.d.n */
public final class C4045n extends C4032a implements C4043l {
    C4045n(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IMarkerDelegate");
    }

    /* renamed from: a */
    public final void mo23675a(float f) {
        Parcel a = mo23664a();
        a.writeFloat(f);
        mo23667b(27, a);
    }

    /* renamed from: b */
    public final void mo23677b(LatLng latLng) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, latLng);
        mo23667b(3, a);
    }

    public final LatLng getPosition() {
        Parcel a = mo23665a(4, mo23664a());
        LatLng latLng = (LatLng) C4039h.m12042a(a, LatLng.CREATOR);
        a.recycle();
        return latLng;
    }

    /* renamed from: k */
    public final int mo23680k() {
        Parcel a = mo23665a(17, mo23664a());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    /* renamed from: m */
    public final C3988b mo23681m() {
        Parcel a = mo23665a(30, mo23664a());
        C3988b a2 = C3988b.C3989a.m11981a(a.readStrongBinder());
        a.recycle();
        return a2;
    }

    /* renamed from: a */
    public final void mo23676a(C3988b bVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, bVar);
        mo23667b(29, a);
    }

    /* renamed from: b */
    public final boolean mo23678b(C4043l lVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, lVar);
        Parcel a2 = mo23665a(16, a);
        boolean a3 = C4039h.m12046a(a2);
        a2.recycle();
        return a3;
    }
}
