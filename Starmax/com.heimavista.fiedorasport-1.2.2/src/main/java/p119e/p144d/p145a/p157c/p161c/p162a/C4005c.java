package p119e.p144d.p145a.p157c.p161c.p162a;

import android.os.Parcel;

/* renamed from: e.d.a.c.c.a.c */
public class C4005c {
    static {
        C4005c.class.getClassLoader();
    }

    private C4005c() {
    }

    /* renamed from: a */
    public static void m12003a(Parcel parcel, boolean z) {
        parcel.writeInt(1);
    }

    /* renamed from: a */
    public static boolean m12004a(Parcel parcel) {
        return parcel.readInt() != 0;
    }
}
