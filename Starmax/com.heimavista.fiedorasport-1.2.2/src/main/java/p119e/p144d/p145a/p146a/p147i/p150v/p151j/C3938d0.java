package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3940e0;

/* renamed from: e.d.a.a.i.v.j.d0 */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3938d0 implements C3940e0.C3941a {

    /* renamed from: a */
    private static final C3938d0 f7283a = new C3938d0();

    private C3938d0() {
    }

    /* renamed from: a */
    public static C3940e0.C3941a m11848a() {
        return f7283a;
    }

    /* renamed from: a */
    public void mo23585a(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE events ADD COLUMN payload_encoding TEXT");
    }
}
