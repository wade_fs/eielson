package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.i */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3946i implements C3963z.C3965b {

    /* renamed from: a */
    private final long f7295a;

    /* renamed from: b */
    private final C3905l f7296b;

    private C3946i(long j, C3905l lVar) {
        this.f7295a = j;
        this.f7296b = lVar;
    }

    /* renamed from: a */
    public static C3963z.C3965b m11867a(long j, C3905l lVar) {
        return new C3946i(j, lVar);
    }

    public Object apply(Object obj) {
        return C3963z.m11892a(this.f7295a, this.f7296b, (SQLiteDatabase) obj);
    }
}
