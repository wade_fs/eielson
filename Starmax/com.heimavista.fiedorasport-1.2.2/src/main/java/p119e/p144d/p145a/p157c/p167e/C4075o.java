package p119e.p144d.p145a.p157c.p167e;

/* renamed from: e.d.a.c.e.o */
final class C4075o implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4065h f7424P;

    /* renamed from: Q */
    private final /* synthetic */ C4074n f7425Q;

    C4075o(C4074n nVar, C4065h hVar) {
        this.f7425Q = nVar;
        this.f7424P = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult>
     arg types: [java.util.concurrent.Executor, e.d.a.c.e.n]
     candidates:
      e.d.a.c.e.h.a(android.app.Activity, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.a):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.e<? super ?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.g):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult>
     arg types: [java.util.concurrent.Executor, e.d.a.c.e.n]
     candidates:
      e.d.a.c.e.h.a(android.app.Activity, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.a):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.e<? super ?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.g):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult> */
    public final void run() {
        try {
            C4065h hVar = (C4065h) this.f7425Q.f7422b.mo16773a(this.f7424P);
            if (hVar == null) {
                this.f7425Q.mo22541a(new NullPointerException("Continuation returned null"));
                return;
            }
            hVar.mo23702a(C4067j.f7413b, (C4062e<? super Object>) this.f7425Q);
            hVar.mo23701a(C4067j.f7413b, (C4060d) this.f7425Q);
            hVar.mo23699a(C4067j.f7413b, (C4055b) this.f7425Q);
        } catch (C4063f e) {
            if (e.getCause() instanceof Exception) {
                this.f7425Q.f7423c.mo23706a((Exception) e.getCause());
            } else {
                this.f7425Q.f7423c.mo23706a((Exception) e);
            }
        } catch (Exception e2) {
            this.f7425Q.f7423c.mo23706a(e2);
        }
    }
}
