package p119e.p144d.p145a.p157c.p161c.p164c;

import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: e.d.a.c.c.c.n */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C4027n {

    /* renamed from: a */
    private final ConcurrentHashMap<C4030q, List<Throwable>> f7379a = new ConcurrentHashMap<>(16, 0.75f, 10);

    /* renamed from: b */
    private final ReferenceQueue<Throwable> f7380b = new ReferenceQueue<>();

    C4027n() {
    }

    /* renamed from: a */
    public final List<Throwable> mo23661a(Throwable th, boolean z) {
        Reference<? extends Throwable> poll = this.f7380b.poll();
        while (poll != null) {
            this.f7379a.remove(poll);
            poll = this.f7380b.poll();
        }
        List<Throwable> list = this.f7379a.get(new C4030q(th, null));
        if (list != null) {
            return list;
        }
        Vector vector = new Vector(2);
        List<Throwable> putIfAbsent = this.f7379a.putIfAbsent(new C4030q(th, this.f7380b), vector);
        return putIfAbsent == null ? vector : putIfAbsent;
    }
}
