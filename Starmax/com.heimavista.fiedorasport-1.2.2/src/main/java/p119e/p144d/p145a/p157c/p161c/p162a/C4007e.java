package p119e.p144d.p145a.p157c.p161c.p162a;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: e.d.a.c.c.a.e */
public abstract class C4007e extends C4004b implements C4006d {
    /* renamed from: a */
    public static C4006d m12006a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
        return queryLocalInterface instanceof C4006d ? (C4006d) queryLocalInterface : new C4008f(iBinder);
    }
}
