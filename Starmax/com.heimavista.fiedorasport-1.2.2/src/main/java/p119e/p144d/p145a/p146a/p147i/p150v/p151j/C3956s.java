package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.Cursor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.s */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3956s implements C3963z.C3965b {

    /* renamed from: a */
    private static final C3956s f7308a = new C3956s();

    private C3956s() {
    }

    /* renamed from: a */
    public static C3963z.C3965b m11879a() {
        return f7308a;
    }

    public Object apply(Object obj) {
        return Boolean.valueOf(((Cursor) obj).moveToNext());
    }
}
