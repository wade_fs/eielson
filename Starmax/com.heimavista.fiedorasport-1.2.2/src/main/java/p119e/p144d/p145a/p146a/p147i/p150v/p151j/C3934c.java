package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import java.io.Closeable;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;

@WorkerThread
/* renamed from: e.d.a.a.i.v.j.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public interface C3934c extends Closeable {
    @Nullable
    /* renamed from: a */
    C3945h mo23586a(C3905l lVar, C3899h hVar);

    /* renamed from: a */
    Iterable<C3945h> mo23587a(C3905l lVar);

    /* renamed from: a */
    void mo23588a(C3905l lVar, long j);

    /* renamed from: a */
    void mo23589a(Iterable<C3945h> iterable);

    /* renamed from: b */
    long mo23590b(C3905l lVar);

    /* renamed from: b */
    void mo23591b(Iterable<C3945h> iterable);

    /* renamed from: c */
    boolean mo23592c(C3905l lVar);

    /* renamed from: r */
    int mo23593r();

    /* renamed from: s */
    Iterable<C3905l> mo23594s();
}
