package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.j */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3947j implements C3963z.C3965b {

    /* renamed from: a */
    private final C3963z f7297a;

    /* renamed from: b */
    private final C3905l f7298b;

    private C3947j(C3963z zVar, C3905l lVar) {
        this.f7297a = zVar;
        this.f7298b = lVar;
    }

    /* renamed from: a */
    public static C3963z.C3965b m11868a(C3963z zVar, C3905l lVar) {
        return new C3947j(zVar, lVar);
    }

    public Object apply(Object obj) {
        return C3963z.m11910b(this.f7297a, this.f7298b, (SQLiteDatabase) obj);
    }
}
