package p119e.p144d.p145a.p157c.p160b;

import android.os.Bundle;
import java.util.Iterator;
import p119e.p144d.p145a.p157c.p160b.C3986a;

/* renamed from: e.d.a.c.b.g */
final class C3996g implements C3993e<T> {

    /* renamed from: a */
    private final /* synthetic */ C3986a f7353a;

    C3996g(C3986a aVar) {
        this.f7353a = aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.b.a.a(e.d.a.c.b.a, e.d.a.c.b.c):e.d.a.c.b.c
     arg types: [e.d.a.c.b.a, T]
     candidates:
      e.d.a.c.b.a.a(e.d.a.c.b.a, android.os.Bundle):android.os.Bundle
      e.d.a.c.b.a.a(android.os.Bundle, e.d.a.c.b.a$a):void
      e.d.a.c.b.a.a(e.d.a.c.b.a, e.d.a.c.b.c):e.d.a.c.b.c */
    /* renamed from: a */
    public final void mo23633a(T t) {
        C3991c unused = this.f7353a.f7346a = (C3991c) t;
        Iterator it = this.f7353a.f7348c.iterator();
        while (it.hasNext()) {
            ((C3986a.C3987a) it.next()).mo23631a(this.f7353a.f7346a);
        }
        this.f7353a.f7348c.clear();
        Bundle unused2 = this.f7353a.f7347b = null;
    }
}
