package p119e.p144d.p145a.p146a.p147i.p150v;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1733g;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p224f.p225b.C4936d;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.v.g */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3925g implements Factory<C1733g> {

    /* renamed from: a */
    private final Provider<C3971a> f7260a;

    public C3925g(Provider<C3971a> aVar) {
        this.f7260a = aVar;
    }

    /* renamed from: a */
    public static C3925g m11808a(Provider<C3971a> aVar) {
        return new C3925g(aVar);
    }

    /* renamed from: a */
    public static C1733g m11807a(C3971a aVar) {
        C1733g a = C3924f.m11806a(aVar);
        C4936d.m17213a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }

    public C1733g get() {
        return m11807a(this.f7260a.get());
    }
}
