package p119e.p144d.p145a.p146a.p147i.p150v;

import android.content.Context;
import android.os.Build;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1725a;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1731e;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1733g;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1749s;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: e.d.a.a.i.v.h */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C3926h {
    /* renamed from: a */
    static C1749s m11809a(Context context, C3934c cVar, C1733g gVar, C3971a aVar) {
        if (Build.VERSION.SDK_INT >= 21) {
            return new C1731e(context, cVar, gVar);
        }
        return new C1725a(context, cVar, aVar, gVar);
    }
}
