package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.Cursor;
import java.util.List;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.m */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3950m implements C3963z.C3965b {

    /* renamed from: a */
    private final List f7301a;

    /* renamed from: b */
    private final C3905l f7302b;

    private C3950m(List list, C3905l lVar) {
        this.f7301a = list;
        this.f7302b = lVar;
    }

    /* renamed from: a */
    public static C3963z.C3965b m11871a(List list, C3905l lVar) {
        return new C3950m(list, lVar);
    }

    public Object apply(Object obj) {
        return C3963z.m11898a(this.f7301a, this.f7302b, (Cursor) obj);
    }
}
