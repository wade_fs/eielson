package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;

/* renamed from: e.d.a.c.e.a */
public interface C4053a<TResult, TContinuationResult> {
    /* renamed from: a */
    TContinuationResult mo16773a(@NonNull C4065h<TResult> hVar);
}
