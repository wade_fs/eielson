package p119e.p144d.p145a.p157c.p161c.p165d;

import android.graphics.Bitmap;
import android.os.IBinder;
import android.os.Parcel;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: e.d.a.c.c.d.k */
public final class C4042k extends C4032a implements C4040i {
    C4042k(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
    }

    /* renamed from: a */
    public final C3988b mo23673a(Bitmap bitmap) {
        Parcel a = mo23664a();
        C4039h.m12044a(a, bitmap);
        Parcel a2 = mo23665a(6, a);
        C3988b a3 = C3988b.C3989a.m11981a(a2.readStrongBinder());
        a2.recycle();
        return a3;
    }

    /* renamed from: i */
    public final C3988b mo23674i() {
        Parcel a = mo23665a(4, mo23664a());
        C3988b a2 = C3988b.C3989a.m11981a(a.readStrongBinder());
        a.recycle();
        return a2;
    }
}
