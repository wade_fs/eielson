package p119e.p144d.p145a.p157c.p160b;

import android.app.Activity;
import android.os.Bundle;
import p119e.p144d.p145a.p157c.p160b.C3986a;

/* renamed from: e.d.a.c.b.h */
final class C3997h implements C3986a.C3987a {

    /* renamed from: a */
    private final /* synthetic */ Activity f7354a;

    /* renamed from: b */
    private final /* synthetic */ Bundle f7355b;

    /* renamed from: c */
    private final /* synthetic */ Bundle f7356c;

    /* renamed from: d */
    private final /* synthetic */ C3986a f7357d;

    C3997h(C3986a aVar, Activity activity, Bundle bundle, Bundle bundle2) {
        this.f7357d = aVar;
        this.f7354a = activity;
        this.f7355b = bundle;
        this.f7356c = bundle2;
    }

    /* renamed from: a */
    public final void mo23631a(C3991c cVar) {
        this.f7357d.f7346a.mo18350a(this.f7354a, this.f7355b, this.f7356c);
    }

    public final int getState() {
        return 0;
    }
}
