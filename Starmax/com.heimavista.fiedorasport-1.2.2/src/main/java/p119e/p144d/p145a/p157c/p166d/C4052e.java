package p119e.p144d.p145a.p157c.p166d;

import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.internal.C2231m;
import com.google.android.gms.signin.internal.C3271d;

/* renamed from: e.d.a.c.d.e */
public interface C4052e extends C2016a.C2027f {
    /* renamed from: a */
    void mo19473a(C2231m mVar, boolean z);

    /* renamed from: a */
    void mo19474a(C3271d dVar);

    /* renamed from: b */
    void mo19475b();

    /* renamed from: g */
    void mo19476g();
}
