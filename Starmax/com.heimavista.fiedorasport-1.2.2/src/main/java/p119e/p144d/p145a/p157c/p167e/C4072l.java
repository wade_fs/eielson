package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.l */
final class C4072l<TResult, TContinuationResult> implements C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7416a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C4053a<TResult, TContinuationResult> f7417b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final C4058c0<TContinuationResult> f7418c;

    public C4072l(@NonNull Executor executor, @NonNull C4053a<TResult, TContinuationResult> aVar, @NonNull C4058c0<TContinuationResult> c0Var) {
        this.f7416a = executor;
        this.f7417b = aVar;
        this.f7418c = c0Var;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h<TResult> hVar) {
        this.f7416a.execute(new C4073m(this, hVar));
    }

    public final void cancel() {
        throw new UnsupportedOperationException();
    }
}
