package p119e.p144d.p145a.p146a.p147i.p149u;

import androidx.annotation.Nullable;

/* renamed from: e.d.a.a.i.u.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public interface C3918c<TInput, TResult> {
    @Nullable
    /* renamed from: a */
    TInput mo13529a(TInput tinput, TResult tresult);
}
