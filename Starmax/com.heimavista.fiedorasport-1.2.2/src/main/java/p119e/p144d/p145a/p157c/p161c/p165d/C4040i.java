package p119e.p144d.p145a.p157c.p161c.p165d;

import android.graphics.Bitmap;
import android.os.IInterface;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: e.d.a.c.c.d.i */
public interface C4040i extends IInterface {
    /* renamed from: a */
    C3988b mo23673a(Bitmap bitmap);

    /* renamed from: i */
    C3988b mo23674i();
}
