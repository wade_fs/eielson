package p119e.p144d.p145a.p146a.p147i.p150v;

import p119e.p144d.p145a.p146a.C3883h;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;

/* renamed from: e.d.a.a.i.v.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3919a implements Runnable {

    /* renamed from: P */
    private final C3921c f7242P;

    /* renamed from: Q */
    private final C3905l f7243Q;

    /* renamed from: R */
    private final C3883h f7244R;

    /* renamed from: S */
    private final C3899h f7245S;

    private C3919a(C3921c cVar, C3905l lVar, C3883h hVar, C3899h hVar2) {
        this.f7242P = cVar;
        this.f7243Q = lVar;
        this.f7244R = hVar;
        this.f7245S = hVar2;
    }

    /* renamed from: a */
    public static Runnable m11798a(C3921c cVar, C3905l lVar, C3883h hVar, C3899h hVar2) {
        return new C3919a(cVar, lVar, hVar, hVar2);
    }

    public void run() {
        C3921c.m11802a(this.f7242P, this.f7243Q, this.f7244R, this.f7245S);
    }
}
