package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p224f.p225b.Factory;

/* renamed from: e.d.a.a.i.v.j.f */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3942f implements Factory<Integer> {

    /* renamed from: a */
    private static final C3942f f7291a = new C3942f();

    /* renamed from: a */
    public static C3942f m11858a() {
        return f7291a;
    }

    /* renamed from: b */
    public static int m11859b() {
        return C3939e.m11850a();
    }

    public Integer get() {
        return Integer.valueOf(m11859b());
    }
}
