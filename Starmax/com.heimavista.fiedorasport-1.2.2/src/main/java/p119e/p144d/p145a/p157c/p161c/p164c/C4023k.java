package p119e.p144d.p145a.p157c.p161c.p164c;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/* renamed from: e.d.a.c.c.c.k */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final class C4023k extends FilterInputStream {

    /* renamed from: P */
    private long f7376P;

    /* renamed from: Q */
    private long f7377Q = -1;

    C4023k(InputStream inputStream, long j) {
        super(inputStream);
        C4019g.m12021a(inputStream);
        this.f7376P = 1048577;
    }

    public final int available() {
        return (int) Math.min((long) super.in.available(), this.f7376P);
    }

    public final synchronized void mark(int i) {
        super.in.mark(i);
        this.f7377Q = this.f7376P;
    }

    public final int read() {
        if (this.f7376P == 0) {
            return -1;
        }
        int read = super.in.read();
        if (read != -1) {
            this.f7376P--;
        }
        return read;
    }

    public final synchronized void reset() {
        if (!super.in.markSupported()) {
            throw new IOException("Mark not supported");
        } else if (this.f7377Q != -1) {
            super.in.reset();
            this.f7376P = this.f7377Q;
        } else {
            throw new IOException("Mark not set");
        }
    }

    public final long skip(long j) {
        long skip = super.in.skip(Math.min(j, this.f7376P));
        this.f7376P -= skip;
        return skip;
    }

    public final int read(byte[] bArr, int i, int i2) {
        long j = this.f7376P;
        if (j == 0) {
            return -1;
        }
        int read = super.in.read(bArr, i, (int) Math.min((long) i2, j));
        if (read != -1) {
            this.f7376P -= (long) read;
        }
        return read;
    }
}
