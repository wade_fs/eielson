package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: e.d.a.c.c.d.j */
public abstract class C4041j extends C4038g implements C4040i {
    /* renamed from: a */
    public static C4040i m12049a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IBitmapDescriptorFactoryDelegate");
        if (queryLocalInterface instanceof C4040i) {
            return (C4040i) queryLocalInterface;
        }
        return new C4042k(iBinder);
    }
}
