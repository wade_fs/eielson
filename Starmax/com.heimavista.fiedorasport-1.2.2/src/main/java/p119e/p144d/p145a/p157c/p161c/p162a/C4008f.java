package p119e.p144d.p145a.p157c.p161c.p162a;

import android.os.IBinder;
import android.os.Parcel;

/* renamed from: e.d.a.c.c.a.f */
public final class C4008f extends C4003a implements C4006d {
    C4008f(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.ads.identifier.internal.IAdvertisingIdService");
    }

    /* renamed from: d */
    public final boolean mo23639d(boolean z) {
        Parcel H = mo23636H();
        C4005c.m12003a(H, true);
        Parcel a = mo23637a(2, H);
        boolean a2 = C4005c.m12004a(a);
        a.recycle();
        return a2;
    }

    public final String getId() {
        Parcel a = mo23637a(1, mo23636H());
        String readString = a.readString();
        a.recycle();
        return readString;
    }
}
