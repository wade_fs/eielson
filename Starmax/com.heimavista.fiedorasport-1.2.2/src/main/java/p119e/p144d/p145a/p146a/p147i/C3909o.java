package p119e.p144d.p145a.p146a.p147i;

import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3878c;
import p119e.p144d.p145a.p146a.C3880e;
import p119e.p144d.p145a.p146a.C3881f;
import p119e.p144d.p145a.p146a.C3883h;
import p119e.p144d.p145a.p146a.p147i.C3903k;

/* renamed from: e.d.a.a.i.o */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3909o<T> implements C3881f<T> {

    /* renamed from: a */
    private final C3905l f7227a;

    /* renamed from: b */
    private final String f7228b;

    /* renamed from: c */
    private final C3877b f7229c;

    /* renamed from: d */
    private final C3880e<T, byte[]> f7230d;

    /* renamed from: e */
    private final C3910p f7231e;

    C3909o(C3905l lVar, String str, C3877b bVar, C3880e<T, byte[]> eVar, C3910p pVar) {
        this.f7227a = lVar;
        this.f7228b = str;
        this.f7229c = bVar;
        this.f7230d = eVar;
        this.f7231e = pVar;
    }

    /* renamed from: a */
    static /* synthetic */ void m11775a(Exception exc) {
    }

    /* renamed from: a */
    public void mo22309a(C3878c<T> cVar) {
        mo23560a(cVar, C3908n.m11773a());
    }

    /* renamed from: a */
    public void mo23560a(C3878c<T> cVar, C3883h hVar) {
        C3910p pVar = this.f7231e;
        C3903k.C3904a g = C3903k.m11750g();
        g.mo23528a(this.f7227a);
        g.mo23526a((C3878c<?>) cVar);
        g.mo23529a(this.f7228b);
        g.mo23527a((C3880e<?, byte[]>) this.f7230d);
        g.mo23525a(this.f7229c);
        pVar.mo23561a(g.mo23530a(), hVar);
    }
}
