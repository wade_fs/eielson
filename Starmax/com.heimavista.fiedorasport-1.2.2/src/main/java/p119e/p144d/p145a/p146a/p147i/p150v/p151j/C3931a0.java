package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.v.j.a0 */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3931a0 implements Factory<C3963z> {

    /* renamed from: a */
    private final Provider<C3971a> f7273a;

    /* renamed from: b */
    private final Provider<C3971a> f7274b;

    /* renamed from: c */
    private final Provider<C3936d> f7275c;

    /* renamed from: d */
    private final Provider<C3940e0> f7276d;

    public C3931a0(Provider<C3971a> aVar, Provider<C3971a> aVar2, Provider<C3936d> aVar3, Provider<C3940e0> aVar4) {
        this.f7273a = aVar;
        this.f7274b = aVar2;
        this.f7275c = aVar3;
        this.f7276d = aVar4;
    }

    /* renamed from: a */
    public static C3931a0 m11821a(Provider<C3971a> aVar, Provider<C3971a> aVar2, Provider<C3936d> aVar3, Provider<C3940e0> aVar4) {
        return new C3931a0(aVar, aVar2, aVar3, aVar4);
    }

    public C3963z get() {
        return new C3963z(this.f7273a.get(), this.f7274b.get(), this.f7275c.get(), this.f7276d.get());
    }
}
