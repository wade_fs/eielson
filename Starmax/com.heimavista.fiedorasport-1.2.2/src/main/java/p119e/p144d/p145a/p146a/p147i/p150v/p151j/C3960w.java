package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.w */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3960w implements C3963z.C3965b {

    /* renamed from: a */
    private final String f7314a;

    private C3960w(String str) {
        this.f7314a = str;
    }

    /* renamed from: a */
    public static C3963z.C3965b m11883a(String str) {
        return new C3960w(str);
    }

    public Object apply(Object obj) {
        return C3963z.m11896a(this.f7314a, (SQLiteDatabase) obj);
    }
}
