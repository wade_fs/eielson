package p119e.p144d.p145a.p146a.p147i;

import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1743m;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1747q;
import p119e.p144d.p145a.p146a.p147i.p150v.C3923e;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.s */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3914s implements Factory<C3911q> {

    /* renamed from: a */
    private final Provider<C3971a> f7237a;

    /* renamed from: b */
    private final Provider<C3971a> f7238b;

    /* renamed from: c */
    private final Provider<C3923e> f7239c;

    /* renamed from: d */
    private final Provider<C1743m> f7240d;

    /* renamed from: e */
    private final Provider<C1747q> f7241e;

    public C3914s(Provider<C3971a> aVar, Provider<C3971a> aVar2, Provider<C3923e> aVar3, Provider<C1743m> aVar4, Provider<C1747q> aVar5) {
        this.f7237a = aVar;
        this.f7238b = aVar2;
        this.f7239c = aVar3;
        this.f7240d = aVar4;
        this.f7241e = aVar5;
    }

    /* renamed from: a */
    public static C3914s m11789a(Provider<C3971a> aVar, Provider<C3971a> aVar2, Provider<C3923e> aVar3, Provider<C1743m> aVar4, Provider<C1747q> aVar5) {
        return new C3914s(aVar, aVar2, aVar3, aVar4, aVar5);
    }

    public C3911q get() {
        return new C3911q(this.f7237a.get(), this.f7238b.get(), this.f7239c.get(), this.f7240d.get(), this.f7241e.get());
    }
}
