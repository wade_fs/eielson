package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import com.google.android.gms.common.internal.C2258v;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/* renamed from: e.d.a.c.e.k */
public final class C4069k {

    /* renamed from: e.d.a.c.e.k$a */
    private static final class C4070a implements C4071b {

        /* renamed from: a */
        private final CountDownLatch f7415a;

        private C4070a() {
            this.f7415a = new CountDownLatch(1);
        }

        /* renamed from: a */
        public final void mo22541a(@NonNull Exception exc) {
            this.f7415a.countDown();
        }

        /* renamed from: b */
        public final void mo23725b() {
            this.f7415a.await();
        }

        public final void onSuccess(Object obj) {
            this.f7415a.countDown();
        }

        /* renamed from: a */
        public final void mo23692a() {
            this.f7415a.countDown();
        }

        /* synthetic */ C4070a(C4061d0 d0Var) {
            this();
        }

        /* renamed from: a */
        public final boolean mo23724a(long j, TimeUnit timeUnit) {
            return this.f7415a.await(j, timeUnit);
        }
    }

    /* renamed from: e.d.a.c.e.k$b */
    interface C4071b extends C4055b, C4060d, C4062e<Object> {
    }

    /* renamed from: a */
    public static <TResult> C4065h<TResult> m12140a(Object obj) {
        C4058c0 c0Var = new C4058c0();
        c0Var.mo23707a(obj);
        return c0Var;
    }

    /* renamed from: b */
    private static <TResult> TResult m12145b(C4065h<TResult> hVar) {
        if (hVar.mo23714e()) {
            return hVar.mo23709b();
        }
        if (hVar.mo23712c()) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(hVar.mo23704a());
    }

    /* renamed from: a */
    public static <TResult> C4065h<TResult> m12139a(@NonNull Exception exc) {
        C4058c0 c0Var = new C4058c0();
        c0Var.mo23706a(exc);
        return c0Var;
    }

    /* renamed from: a */
    public static <TResult> C4065h<TResult> m12141a(@NonNull Executor executor, @NonNull Callable<TResult> callable) {
        C2258v.m5630a(executor, "Executor must not be null");
        C2258v.m5630a(callable, "Callback must not be null");
        C4058c0 c0Var = new C4058c0();
        executor.execute(new C4061d0(c0Var, callable));
        return c0Var;
    }

    /* renamed from: a */
    public static <TResult> TResult m12142a(@NonNull C4065h hVar) {
        C2258v.m5632a();
        C2258v.m5630a(hVar, "Task must not be null");
        if (hVar.mo23713d()) {
            return m12145b(hVar);
        }
        C4070a aVar = new C4070a(null);
        m12144a(hVar, aVar);
        aVar.mo23725b();
        return m12145b(hVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.k.a(e.d.a.c.e.h<?>, e.d.a.c.e.k$b):void
     arg types: [e.d.a.c.e.h<TResult>, e.d.a.c.e.k$a]
     candidates:
      e.d.a.c.e.k.a(java.util.concurrent.Executor, java.util.concurrent.Callable):e.d.a.c.e.h<TResult>
      e.d.a.c.e.k.a(e.d.a.c.e.h<?>, e.d.a.c.e.k$b):void */
    /* renamed from: a */
    public static <TResult> TResult m12143a(@NonNull C4065h<TResult> hVar, long j, @NonNull TimeUnit timeUnit) {
        C2258v.m5632a();
        C2258v.m5630a(hVar, "Task must not be null");
        C2258v.m5630a(timeUnit, "TimeUnit must not be null");
        if (hVar.mo23713d()) {
            return m12145b(hVar);
        }
        C4070a aVar = new C4070a(null);
        m12144a((C4065h<?>) hVar, (C4071b) aVar);
        if (aVar.mo23724a(j, timeUnit)) {
            return m12145b(hVar);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult>
     arg types: [java.util.concurrent.Executor, e.d.a.c.e.k$b]
     candidates:
      e.d.a.c.e.h.a(android.app.Activity, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.a):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.e<? super ?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.g):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult>
     arg types: [java.util.concurrent.Executor, e.d.a.c.e.k$b]
     candidates:
      e.d.a.c.e.h.a(android.app.Activity, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.a):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.e<? super ?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.g):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult> */
    /* renamed from: a */
    private static void m12144a(C4065h<?> hVar, C4071b bVar) {
        hVar.mo23702a(C4067j.f7413b, (C4062e<? super Object>) bVar);
        hVar.mo23701a(C4067j.f7413b, (C4060d) bVar);
        hVar.mo23699a(C4067j.f7413b, (C4055b) bVar);
    }
}
