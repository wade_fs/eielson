package p119e.p144d.p145a.p157c.p161c.p162a;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: e.d.a.c.c.a.a */
public class C4003a implements IInterface {

    /* renamed from: a */
    private final IBinder f7369a;

    /* renamed from: b */
    private final String f7370b;

    protected C4003a(IBinder iBinder, String str) {
        this.f7369a = iBinder;
        this.f7370b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: H */
    public final Parcel mo23636H() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f7370b);
        return obtain;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo23637a(int i, Parcel parcel) {
        parcel = Parcel.obtain();
        try {
            this.f7369a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }

    public IBinder asBinder() {
        return this.f7369a;
    }
}
