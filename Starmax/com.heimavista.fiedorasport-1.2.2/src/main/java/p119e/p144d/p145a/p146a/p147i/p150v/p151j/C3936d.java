package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import androidx.recyclerview.widget.ItemTouchHelper;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3928a;

/* renamed from: e.d.a.a.i.v.j.d */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
abstract class C3936d {

    /* renamed from: a */
    static final C3936d f7282a;

    /* renamed from: e.d.a.a.i.v.j.d$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static abstract class C3937a {
        C3937a() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract C3937a mo23574a(int i);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract C3937a mo23575a(long j);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract C3936d mo23576a();

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public abstract C3937a mo23577b(int i);

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public abstract C3937a mo23578b(long j);
    }

    static {
        C3937a e = m11838e();
        e.mo23578b(10485760L);
        e.mo23577b((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        e.mo23574a(10000);
        e.mo23575a(604800000L);
        f7282a = e.mo23576a();
    }

    C3936d() {
    }

    /* renamed from: e */
    static C3937a m11838e() {
        return new C3928a.C3930b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract int mo23567a();

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract long mo23568b();

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public abstract int mo23569c();

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public abstract long mo23570d();
}
