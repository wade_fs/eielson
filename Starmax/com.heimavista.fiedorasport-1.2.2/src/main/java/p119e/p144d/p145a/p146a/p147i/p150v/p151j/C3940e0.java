package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import java.util.Arrays;
import java.util.List;

/* renamed from: e.d.a.a.i.v.j.e0 */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3940e0 extends SQLiteOpenHelper {

    /* renamed from: R */
    static int f7284R = 3;

    /* renamed from: S */
    private static final C3941a f7285S = C3933b0.m11825a();

    /* renamed from: T */
    private static final C3941a f7286T = C3935c0.m11836a();

    /* renamed from: U */
    private static final C3941a f7287U = C3938d0.m11848a();

    /* renamed from: V */
    private static final List<C3941a> f7288V = Arrays.asList(f7285S, f7286T, f7287U);

    /* renamed from: P */
    private final int f7289P;

    /* renamed from: Q */
    private boolean f7290Q = false;

    /* renamed from: e.d.a.a.i.v.j.e0$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public interface C3941a {
        /* renamed from: a */
        void mo23585a(SQLiteDatabase sQLiteDatabase);
    }

    C3940e0(Context context, int i) {
        super(context, "com.google.android.datatransport.events", (SQLiteDatabase.CursorFactory) null, i);
        this.f7289P = i;
    }

    /* renamed from: a */
    private void m11852a(SQLiteDatabase sQLiteDatabase) {
        if (!this.f7290Q) {
            onConfigure(sQLiteDatabase);
        }
    }

    /* renamed from: b */
    static /* synthetic */ void m11854b(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE events (_id INTEGER PRIMARY KEY, context_id INTEGER NOT NULL, transport_name TEXT NOT NULL, timestamp_ms INTEGER NOT NULL, uptime_ms INTEGER NOT NULL, payload BLOB NOT NULL, code INTEGER, num_attempts INTEGER NOT NULL,FOREIGN KEY (context_id) REFERENCES transport_contexts(_id) ON DELETE CASCADE)");
        sQLiteDatabase.execSQL("CREATE TABLE event_metadata (_id INTEGER PRIMARY KEY, event_id INTEGER NOT NULL, name TEXT NOT NULL, value TEXT NOT NULL,FOREIGN KEY (event_id) REFERENCES events(_id) ON DELETE CASCADE)");
        sQLiteDatabase.execSQL("CREATE TABLE transport_contexts (_id INTEGER PRIMARY KEY, backend_name TEXT NOT NULL, priority INTEGER NOT NULL, next_request_ms INTEGER NOT NULL)");
        sQLiteDatabase.execSQL("CREATE INDEX events_backend_id on events(context_id)");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX contexts_backend_priority on transport_contexts(backend_name, priority)");
    }

    /* renamed from: c */
    static /* synthetic */ void m11855c(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("ALTER TABLE transport_contexts ADD COLUMN extras BLOB");
        sQLiteDatabase.execSQL("CREATE UNIQUE INDEX contexts_backend_priority_extras on transport_contexts(backend_name, priority, extras)");
        sQLiteDatabase.execSQL("DROP INDEX contexts_backend_priority");
    }

    public void onConfigure(SQLiteDatabase sQLiteDatabase) {
        this.f7290Q = true;
        sQLiteDatabase.rawQuery("PRAGMA busy_timeout=0;", new String[0]).close();
        if (Build.VERSION.SDK_INT >= 16) {
            sQLiteDatabase.setForeignKeyConstraintsEnabled(true);
        }
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        m11852a(sQLiteDatabase);
        m11853a(sQLiteDatabase, 0, this.f7289P);
    }

    public void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE events");
        sQLiteDatabase.execSQL("DROP TABLE event_metadata");
        sQLiteDatabase.execSQL("DROP TABLE transport_contexts");
        onCreate(sQLiteDatabase);
    }

    public void onOpen(SQLiteDatabase sQLiteDatabase) {
        m11852a(sQLiteDatabase);
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        m11852a(sQLiteDatabase);
        m11853a(sQLiteDatabase, i, i2);
    }

    /* renamed from: a */
    private void m11853a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (i2 <= f7288V.size()) {
            while (i < i2) {
                f7288V.get(i).mo23585a(sQLiteDatabase);
                i++;
            }
            return;
        }
        throw new IllegalArgumentException("Migration from " + i + " to " + i2 + " was requested, but cannot be performed. Only " + f7288V.size() + " migrations are provided");
    }
}
