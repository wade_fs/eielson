package p119e.p144d.p145a.p146a.p147i;

import android.content.Context;
import java.io.Closeable;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;

/* renamed from: e.d.a.a.i.r */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
abstract class C3912r implements Closeable {

    /* renamed from: e.d.a.a.i.r$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    interface C3913a {
        /* renamed from: a */
        C3913a mo23542a(Context context);

        C3912r build();
    }

    C3912r() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract C3934c mo23540a();

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract C3911q mo23541b();

    public void close() {
        mo23540a().close();
    }
}
