package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.n */
final class C4074n<TResult, TContinuationResult> implements C4055b, C4060d, C4062e<TContinuationResult>, C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7421a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C4053a<TResult, C4065h<TContinuationResult>> f7422b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public final C4058c0<TContinuationResult> f7423c;

    public C4074n(@NonNull Executor executor, @NonNull C4053a<TResult, C4065h<TContinuationResult>> aVar, @NonNull C4058c0<TContinuationResult> c0Var) {
        this.f7421a = executor;
        this.f7422b = aVar;
        this.f7423c = c0Var;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h<TResult> hVar) {
        this.f7421a.execute(new C4075o(this, hVar));
    }

    public final void cancel() {
        throw new UnsupportedOperationException();
    }

    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.f7423c.mo23707a((Object) tcontinuationresult);
    }

    /* renamed from: a */
    public final void mo22541a(@NonNull Exception exc) {
        this.f7423c.mo23706a(exc);
    }

    /* renamed from: a */
    public final void mo23692a() {
        this.f7423c.mo23715f();
    }
}
