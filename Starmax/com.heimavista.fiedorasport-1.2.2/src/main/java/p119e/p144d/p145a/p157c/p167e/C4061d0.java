package p119e.p144d.p145a.p157c.p167e;

import java.util.concurrent.Callable;

/* renamed from: e.d.a.c.e.d0 */
final class C4061d0 implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4058c0 f7409P;

    /* renamed from: Q */
    private final /* synthetic */ Callable f7410Q;

    C4061d0(C4058c0 c0Var, Callable callable) {
        this.f7409P = c0Var;
        this.f7410Q = callable;
    }

    public final void run() {
        try {
            this.f7409P.mo23707a(this.f7410Q.call());
        } catch (Exception e) {
            this.f7409P.mo23706a(e);
        }
    }
}
