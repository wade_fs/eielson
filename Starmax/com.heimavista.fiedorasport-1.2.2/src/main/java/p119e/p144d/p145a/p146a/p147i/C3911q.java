package p119e.p144d.p145a.p146a.p147i;

import android.content.Context;
import androidx.annotation.RestrictTo;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1743m;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1747q;
import java.util.Collections;
import java.util.Set;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3882g;
import p119e.p144d.p145a.p146a.C3883h;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.C3912r;
import p119e.p144d.p145a.p146a.p147i.p150v.C3923e;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;

/* renamed from: e.d.a.a.i.q */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C3911q implements C3910p {

    /* renamed from: e */
    private static volatile C3912r f7232e;

    /* renamed from: a */
    private final C3971a f7233a;

    /* renamed from: b */
    private final C3971a f7234b;

    /* renamed from: c */
    private final C3923e f7235c;

    /* renamed from: d */
    private final C1743m f7236d;

    C3911q(C3971a aVar, C3971a aVar2, C3923e eVar, C1743m mVar, C1747q qVar) {
        this.f7233a = aVar;
        this.f7234b = aVar2;
        this.f7235c = eVar;
        this.f7236d = mVar;
        qVar.mo13592a();
    }

    /* renamed from: a */
    public static void m11780a(Context context) {
        if (f7232e == null) {
            synchronized (C3911q.class) {
                if (f7232e == null) {
                    C3912r.C3913a c = C3893d.m11715c();
                    c.mo23542a(context);
                    f7232e = c.build();
                }
            }
        }
    }

    /* renamed from: b */
    public static C3911q m11781b() {
        C3912r rVar = f7232e;
        if (rVar != null) {
            return rVar.mo23541b();
        }
        throw new IllegalStateException("Not initialized!");
    }

    /* renamed from: b */
    private static Set<C3877b> m11782b(C3896e eVar) {
        if (eVar instanceof C3897f) {
            return Collections.unmodifiableSet(((C3897f) eVar).mo13438a());
        }
        return Collections.singleton(C3877b.m11673a("proto"));
    }

    /* renamed from: a */
    public C3882g mo23563a(C3896e eVar) {
        Set<C3877b> b = m11782b(eVar);
        C3905l.C3906a d = C3905l.m11763d();
        d.mo23537a(eVar.getName());
        d.mo23538a(eVar.mo13439b());
        return new C3907m(b, d.mo23539a(), this);
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    /* renamed from: a */
    public C1743m mo23562a() {
        return this.f7236d;
    }

    /* renamed from: a */
    public void mo23561a(C3903k kVar, C3883h hVar) {
        this.f7235c.mo23566a(kVar.mo23520e().mo23558a(kVar.mo23518b().mo23491c()), m11779a(kVar), hVar);
    }

    /* renamed from: a */
    private C3899h m11779a(C3903k kVar) {
        C3899h.C3900a i = C3899h.m11724i();
        i.mo23509a(this.f7233a.mo23604a());
        i.mo23515b(this.f7234b.mo23604a());
        i.mo23512a(kVar.mo23522f());
        i.mo23510a(new C3898g(kVar.mo23517a(), kVar.mo23557c()));
        i.mo23511a(kVar.mo23518b().mo23489a());
        return i.mo23514a();
    }
}
