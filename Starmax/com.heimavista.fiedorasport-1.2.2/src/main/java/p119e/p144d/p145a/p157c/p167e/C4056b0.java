package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.b0 */
final class C4056b0 implements Executor {
    C4056b0() {
    }

    public final void execute(@NonNull Runnable runnable) {
        runnable.run();
    }
}
