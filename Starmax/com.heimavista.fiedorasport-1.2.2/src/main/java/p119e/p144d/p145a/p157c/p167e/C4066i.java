package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;

/* renamed from: e.d.a.c.e.i */
public class C4066i<TResult> {

    /* renamed from: a */
    private final C4058c0<TResult> f7411a = new C4058c0<>();

    /* renamed from: a */
    public void mo23720a(Boolean bool) {
        this.f7411a.mo23707a(bool);
    }

    /* renamed from: b */
    public boolean mo23722b(Object obj) {
        return this.f7411a.mo23711b(obj);
    }

    /* renamed from: a */
    public void mo23719a(@NonNull Exception exc) {
        this.f7411a.mo23706a(exc);
    }

    /* renamed from: b */
    public boolean mo23721b(@NonNull Exception exc) {
        return this.f7411a.mo23710b(exc);
    }

    @NonNull
    /* renamed from: a */
    public C4065h<TResult> mo23718a() {
        return this.f7411a;
    }
}
