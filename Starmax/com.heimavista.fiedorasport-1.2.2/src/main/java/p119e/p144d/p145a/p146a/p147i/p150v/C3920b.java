package p119e.p144d.p145a.p146a.p147i.p150v;

import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: e.d.a.a.i.v.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3920b implements C3969b.C3970a {

    /* renamed from: a */
    private final C3921c f7246a;

    /* renamed from: b */
    private final C3905l f7247b;

    /* renamed from: c */
    private final C3899h f7248c;

    private C3920b(C3921c cVar, C3905l lVar, C3899h hVar) {
        this.f7246a = cVar;
        this.f7247b = lVar;
        this.f7248c = hVar;
    }

    /* renamed from: a */
    public static C3969b.C3970a m11799a(C3921c cVar, C3905l lVar, C3899h hVar) {
        return new C3920b(cVar, lVar, hVar);
    }

    /* renamed from: s */
    public Object mo13587s() {
        return C3921c.m11801a(this.f7246a, this.f7247b, this.f7248c);
    }
}
