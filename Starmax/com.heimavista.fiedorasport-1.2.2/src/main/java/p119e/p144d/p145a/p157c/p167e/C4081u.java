package p119e.p144d.p145a.p157c.p167e;

/* renamed from: e.d.a.c.e.u */
final class C4081u implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4065h f7438P;

    /* renamed from: Q */
    private final /* synthetic */ C4080t f7439Q;

    C4081u(C4080t tVar, C4065h hVar) {
        this.f7439Q = tVar;
        this.f7438P = hVar;
    }

    public final void run() {
        synchronized (this.f7439Q.f7436b) {
            if (this.f7439Q.f7437c != null) {
                this.f7439Q.f7437c.mo22541a(this.f7438P.mo23704a());
            }
        }
    }
}
