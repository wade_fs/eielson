package p119e.p144d.p145a.p146a.p147i.p154y;

import android.util.SparseArray;
import androidx.annotation.NonNull;
import java.util.EnumMap;
import p119e.p144d.p145a.p146a.C3879d;

/* renamed from: e.d.a.a.i.y.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3977a {

    /* renamed from: a */
    private static SparseArray<C3879d> f7327a = new SparseArray<>();

    /* renamed from: b */
    private static EnumMap<C3879d, Integer> f7328b = new EnumMap<>(C3879d.class);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V}
     arg types: [e.d.a.a.d, int]
     candidates:
      ClspMth{java.util.EnumMap.put(java.lang.Object, java.lang.Object):java.lang.Object}
      ClspMth{java.util.AbstractMap.put(e.d.a.a.d, java.lang.Integer):V}
      ClspMth{java.util.Map.put(java.lang.Object, java.lang.Object):V}
      ClspMth{java.util.EnumMap.put(java.lang.Enum, java.lang.Object):V} */
    static {
        f7328b.put((Enum) C3879d.f7175P, (Object) 0);
        f7328b.put((Enum) C3879d.VERY_LOW, (Object) 1);
        f7328b.put((Enum) C3879d.HIGHEST, (Object) 2);
        for (C3879d dVar : f7328b.keySet()) {
            f7327a.append(f7328b.get(dVar).intValue(), dVar);
        }
    }

    @NonNull
    /* renamed from: a */
    public static C3879d m11940a(int i) {
        C3879d dVar = f7327a.get(i);
        if (dVar != null) {
            return dVar;
        }
        throw new IllegalArgumentException("Unknown Priority for value " + i);
    }

    /* renamed from: a */
    public static int m11939a(@NonNull C3879d dVar) {
        Integer num = f7328b.get(dVar);
        if (num != null) {
            return num.intValue();
        }
        throw new IllegalStateException("PriorityMapping is missing known Priority value " + dVar);
    }
}
