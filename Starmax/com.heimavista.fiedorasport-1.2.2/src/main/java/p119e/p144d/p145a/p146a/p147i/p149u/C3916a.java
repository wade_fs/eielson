package p119e.p144d.p145a.p146a.p147i.p149u;

import java.lang.Throwable;

/* renamed from: e.d.a.a.i.u.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public interface C3916a<TInput, TResult, TException extends Throwable> {
    TResult apply(TInput tinput);
}
