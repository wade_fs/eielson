package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: e.d.a.c.c.d.a */
public class C4032a implements IInterface {

    /* renamed from: a */
    private final IBinder f7383a;

    /* renamed from: b */
    private final String f7384b;

    protected C4032a(IBinder iBinder, String str) {
        this.f7383a = iBinder;
        this.f7384b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo23664a() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f7384b);
        return obtain;
    }

    public IBinder asBinder() {
        return this.f7383a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public final void mo23667b(int i, Parcel parcel) {
        Parcel obtain = Parcel.obtain();
        try {
            this.f7383a.transact(i, parcel, obtain, 0);
            obtain.readException();
        } finally {
            parcel.recycle();
            obtain.recycle();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo23665a(int i, Parcel parcel) {
        parcel = Parcel.obtain();
        try {
            this.f7383a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }
}
