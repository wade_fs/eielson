package p119e.p144d.p145a.p157c.p161c.p164c;

import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;

/* renamed from: e.d.a.c.c.c.q */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C4030q extends WeakReference<Throwable> {

    /* renamed from: a */
    private final int f7382a;

    public C4030q(Throwable th, ReferenceQueue<Throwable> referenceQueue) {
        super(th, referenceQueue);
        if (th != null) {
            this.f7382a = System.identityHashCode(th);
            return;
        }
        throw new NullPointerException("The referent cannot be null");
    }

    public final boolean equals(Object obj) {
        if (obj != null && obj.getClass() == C4030q.class) {
            if (this == obj) {
                return true;
            }
            C4030q qVar = (C4030q) obj;
            return this.f7382a == qVar.f7382a && get() == super.get();
        }
    }

    public final int hashCode() {
        return this.f7382a;
    }
}
