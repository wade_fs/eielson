package p119e.p144d.p145a.p146a.p147i;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: e.d.a.a.i.e */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public interface C3896e {
    @Nullable
    /* renamed from: b */
    byte[] mo13439b();

    @NonNull
    String getName();
}
