package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p224f.p225b.C4936d;
import p224f.p225b.Factory;

/* renamed from: e.d.a.a.i.v.j.g */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3944g implements Factory<C3936d> {

    /* renamed from: a */
    private static final C3944g f7294a = new C3944g();

    /* renamed from: a */
    public static C3944g m11861a() {
        return f7294a;
    }

    /* renamed from: b */
    public static C3936d m11862b() {
        C3936d b = C3939e.m11851b();
        C4936d.m17213a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    public C3936d get() {
        return m11862b();
    }
}
