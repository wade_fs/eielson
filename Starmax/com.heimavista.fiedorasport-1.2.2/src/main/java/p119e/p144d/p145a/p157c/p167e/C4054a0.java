package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.ArrayDeque;
import java.util.Queue;

/* renamed from: e.d.a.c.e.a0 */
final class C4054a0<TResult> {

    /* renamed from: a */
    private final Object f7399a = new Object();

    /* renamed from: b */
    private Queue<C4086z<TResult>> f7400b;

    /* renamed from: c */
    private boolean f7401c;

    C4054a0() {
    }

    /* renamed from: a */
    public final void mo23691a(@NonNull C4086z<TResult> zVar) {
        synchronized (this.f7399a) {
            if (this.f7400b == null) {
                this.f7400b = new ArrayDeque();
            }
            this.f7400b.add(zVar);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0010, code lost:
        r1 = r2.f7399a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0012, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        r0 = r2.f7400b.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001b, code lost:
        if (r0 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x001d, code lost:
        r2.f7401c = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0020, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0021, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0022, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0023, code lost:
        r0.mo23726a(r3);
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo23690a(@androidx.annotation.NonNull p119e.p144d.p145a.p157c.p167e.C4065h<TResult> r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f7399a
            monitor-enter(r0)
            java.util.Queue<e.d.a.c.e.z<TResult>> r1 = r2.f7400b     // Catch:{ all -> 0x002c }
            if (r1 == 0) goto L_0x002a
            boolean r1 = r2.f7401c     // Catch:{ all -> 0x002c }
            if (r1 == 0) goto L_0x000c
            goto L_0x002a
        L_0x000c:
            r1 = 1
            r2.f7401c = r1     // Catch:{ all -> 0x002c }
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
        L_0x0010:
            java.lang.Object r1 = r2.f7399a
            monitor-enter(r1)
            java.util.Queue<e.d.a.c.e.z<TResult>> r0 = r2.f7400b     // Catch:{ all -> 0x0027 }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x0027 }
            e.d.a.c.e.z r0 = (p119e.p144d.p145a.p157c.p167e.C4086z) r0     // Catch:{ all -> 0x0027 }
            if (r0 != 0) goto L_0x0022
            r3 = 0
            r2.f7401c = r3     // Catch:{ all -> 0x0027 }
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            return
        L_0x0022:
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            r0.mo23726a(r3)
            goto L_0x0010
        L_0x0027:
            r3 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0027 }
            throw r3
        L_0x002a:
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            return
        L_0x002c:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x002c }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p145a.p157c.p167e.C4054a0.mo23690a(e.d.a.c.e.h):void");
    }
}
