package p119e.p144d.p145a.p157c.p161c.p164c;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: e.d.a.c.c.c.e */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public class C4017e extends Handler {
    public C4017e(Looper looper) {
        super(looper);
    }

    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }

    public C4017e(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
