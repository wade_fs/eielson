package p119e.p144d.p145a.p157c.p160b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.C2167b;
import com.google.android.gms.common.internal.C2215f;
import java.util.LinkedList;
import p119e.p144d.p145a.p157c.p160b.C3991c;

/* renamed from: e.d.a.c.b.a */
public abstract class C3986a<T extends C3991c> {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public T f7346a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Bundle f7347b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public LinkedList<C3987a> f7348c;

    /* renamed from: d */
    private final C3993e<T> f7349d = new C3996g(this);

    /* renamed from: e.d.a.c.b.a$a */
    private interface C3987a {
        /* renamed from: a */
        void mo23631a(C3991c cVar);

        int getState();
    }

    /* renamed from: b */
    public static void m11965b(FrameLayout frameLayout) {
        C2167b a = C2167b.m5251a();
        Context context = frameLayout.getContext();
        int c = a.mo16831c(context);
        String b = C2215f.m5467b(context, c);
        String a2 = C2215f.m5463a(context, c);
        LinearLayout linearLayout = new LinearLayout(frameLayout.getContext());
        linearLayout.setOrientation(1);
        linearLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        frameLayout.addView(linearLayout);
        TextView textView = new TextView(frameLayout.getContext());
        textView.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
        textView.setText(b);
        linearLayout.addView(textView);
        Intent a3 = a.mo16825a(context, c, (String) null);
        if (a3 != null) {
            Button button = new Button(context);
            button.setId(16908313);
            button.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
            button.setText(a2);
            linearLayout.addView(button);
            button.setOnClickListener(new C4000k(context, a3));
        }
    }

    /* renamed from: a */
    public T mo23619a() {
        return this.f7346a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo18361a(C3993e eVar);

    /* renamed from: c */
    public void mo23625c() {
        T t = this.f7346a;
        if (t != null) {
            t.mo18357g();
        } else {
            m11962a(2);
        }
    }

    /* renamed from: d */
    public void mo23626d() {
        T t = this.f7346a;
        if (t != null) {
            t.onLowMemory();
        }
    }

    /* renamed from: e */
    public void mo23627e() {
        T t = this.f7346a;
        if (t != null) {
            t.mo18356d();
        } else {
            m11962a(5);
        }
    }

    /* renamed from: f */
    public void mo23628f() {
        m11963a((Bundle) null, new C4002m(this));
    }

    /* renamed from: g */
    public void mo23629g() {
        m11963a((Bundle) null, new C4001l(this));
    }

    /* renamed from: h */
    public void mo23630h() {
        T t = this.f7346a;
        if (t != null) {
            t.mo18355c();
        } else {
            m11962a(4);
        }
    }

    /* renamed from: a */
    private final void m11962a(int i) {
        while (!this.f7348c.isEmpty() && this.f7348c.getLast().getState() >= i) {
            this.f7348c.removeLast();
        }
    }

    /* renamed from: a */
    private final void m11963a(Bundle bundle, C3987a aVar) {
        T t = this.f7346a;
        if (t != null) {
            aVar.mo23631a(t);
            return;
        }
        if (this.f7348c == null) {
            this.f7348c = new LinkedList<>();
        }
        this.f7348c.add(aVar);
        if (bundle != null) {
            Bundle bundle2 = this.f7347b;
            if (bundle2 == null) {
                this.f7347b = (Bundle) bundle.clone();
            } else {
                bundle2.putAll(bundle);
            }
        }
        mo18361a(this.f7349d);
    }

    /* renamed from: a */
    public void mo23620a(Activity activity, Bundle bundle, Bundle bundle2) {
        m11963a(bundle2, new C3997h(this, activity, bundle, bundle2));
    }

    /* renamed from: a */
    public void mo23621a(Bundle bundle) {
        m11963a(bundle, new C3998i(this, bundle));
    }

    /* renamed from: a */
    public View mo23618a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        FrameLayout frameLayout = new FrameLayout(layoutInflater.getContext());
        m11963a(bundle, new C3999j(this, frameLayout, layoutInflater, viewGroup, bundle));
        if (this.f7346a == null) {
            mo23622a(frameLayout);
        }
        return frameLayout;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23622a(FrameLayout frameLayout) {
        m11965b(frameLayout);
    }

    /* renamed from: b */
    public void mo23623b() {
        T t = this.f7346a;
        if (t != null) {
            t.mo18353b();
        } else {
            m11962a(1);
        }
    }

    /* renamed from: b */
    public void mo23624b(Bundle bundle) {
        T t = this.f7346a;
        if (t != null) {
            t.mo18351a(bundle);
            return;
        }
        Bundle bundle2 = this.f7347b;
        if (bundle2 != null) {
            bundle.putAll(bundle2);
        }
    }
}
