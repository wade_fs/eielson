package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: e.d.a.c.e.g */
public interface C4064g<TResult, TContinuationResult> {
    @NonNull
    /* renamed from: a */
    C4065h<TContinuationResult> mo22259a(@Nullable TResult tresult);
}
