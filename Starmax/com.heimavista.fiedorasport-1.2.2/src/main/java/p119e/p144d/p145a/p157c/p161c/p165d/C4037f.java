package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;

/* renamed from: e.d.a.c.c.d.f */
public final class C4037f extends C4032a implements C4035d {
    C4037f(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.ITileProviderDelegate");
    }
}
