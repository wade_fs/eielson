package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.Cursor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.q */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3954q implements C3963z.C3965b {

    /* renamed from: a */
    private static final C3954q f7306a = new C3954q();

    private C3954q() {
    }

    /* renamed from: a */
    public static C3963z.C3965b m11876a() {
        return f7306a;
    }

    public Object apply(Object obj) {
        return C3963z.m11913c((Cursor) obj);
    }
}
