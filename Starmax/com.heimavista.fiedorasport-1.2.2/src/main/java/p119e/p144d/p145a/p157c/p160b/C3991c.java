package p119e.p144d.p145a.p157c.p160b;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/* renamed from: e.d.a.c.b.c */
public interface C3991c {
    /* renamed from: a */
    View mo18349a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle);

    /* renamed from: a */
    void mo18350a(Activity activity, Bundle bundle, Bundle bundle2);

    /* renamed from: a */
    void mo18351a(Bundle bundle);

    /* renamed from: b */
    void mo18353b();

    /* renamed from: b */
    void mo18354b(Bundle bundle);

    /* renamed from: c */
    void mo18355c();

    /* renamed from: d */
    void mo18356d();

    /* renamed from: g */
    void mo18357g();

    void onLowMemory();

    void onResume();

    void onStart();
}
