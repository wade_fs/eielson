package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.u */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3958u implements C3963z.C3965b {

    /* renamed from: a */
    private final C3963z f7310a;

    /* renamed from: b */
    private final C3905l f7311b;

    /* renamed from: c */
    private final C3899h f7312c;

    private C3958u(C3963z zVar, C3905l lVar, C3899h hVar) {
        this.f7310a = zVar;
        this.f7311b = lVar;
        this.f7312c = hVar;
    }

    /* renamed from: a */
    public static C3963z.C3965b m11881a(C3963z zVar, C3905l lVar, C3899h hVar) {
        return new C3958u(zVar, lVar, hVar);
    }

    public Object apply(Object obj) {
        return C3963z.m11891a(this.f7310a, this.f7311b, this.f7312c, (SQLiteDatabase) obj);
    }
}
