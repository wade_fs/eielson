package p119e.p144d.p145a.p157c.p167e;

import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.h */
public abstract class C4065h<TResult> {
    @NonNull
    /* renamed from: a */
    public abstract C4065h<TResult> mo23696a(@NonNull C4060d dVar);

    @NonNull
    /* renamed from: a */
    public abstract C4065h<TResult> mo23697a(@NonNull C4062e<? super TResult> eVar);

    @NonNull
    /* renamed from: a */
    public C4065h<TResult> mo23700a(@NonNull Executor executor, @NonNull C4057c<TResult> cVar) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @NonNull
    /* renamed from: a */
    public abstract C4065h<TResult> mo23701a(@NonNull Executor executor, @NonNull C4060d dVar);

    @NonNull
    /* renamed from: a */
    public abstract C4065h<TResult> mo23702a(@NonNull Executor executor, @NonNull C4062e<? super TResult> eVar);

    @Nullable
    /* renamed from: a */
    public abstract Exception mo23704a();

    @Nullable
    /* renamed from: a */
    public abstract <X extends Throwable> TResult mo23705a(@NonNull Class<X> cls);

    @NonNull
    /* renamed from: b */
    public <TContinuationResult> C4065h<TContinuationResult> mo23708b(@NonNull Executor executor, @NonNull C4053a<TResult, C4065h<TContinuationResult>> aVar) {
        throw new UnsupportedOperationException("continueWithTask is not implemented");
    }

    @Nullable
    /* renamed from: b */
    public abstract TResult mo23709b();

    /* renamed from: c */
    public abstract boolean mo23712c();

    /* renamed from: d */
    public abstract boolean mo23713d();

    /* renamed from: e */
    public abstract boolean mo23714e();

    @NonNull
    /* renamed from: a */
    public C4065h<TResult> mo23694a(@NonNull Activity activity, @NonNull C4057c<TResult> cVar) {
        throw new UnsupportedOperationException("addOnCompleteListener is not implemented");
    }

    @NonNull
    /* renamed from: a */
    public C4065h<TResult> mo23699a(@NonNull Executor executor, @NonNull C4055b bVar) {
        throw new UnsupportedOperationException("addOnCanceledListener is not implemented");
    }

    @NonNull
    /* renamed from: a */
    public <TContinuationResult> C4065h<TContinuationResult> mo23695a(@NonNull C4053a<TResult, TContinuationResult> aVar) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @NonNull
    /* renamed from: a */
    public <TContinuationResult> C4065h<TContinuationResult> mo23698a(@NonNull Executor executor, @NonNull C4053a<TResult, TContinuationResult> aVar) {
        throw new UnsupportedOperationException("continueWith is not implemented");
    }

    @NonNull
    /* renamed from: a */
    public <TContinuationResult> C4065h<TContinuationResult> mo23703a(@NonNull Executor executor, @NonNull C4064g<TResult, TContinuationResult> gVar) {
        throw new UnsupportedOperationException("onSuccessTask is not implemented");
    }
}
