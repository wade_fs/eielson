package p119e.p144d.p145a.p157c.p166d;

import androidx.annotation.Nullable;
import com.google.android.gms.common.api.C2016a;

/* renamed from: e.d.a.c.d.a */
public final class C4047a implements C2016a.C2020d.C2025e {

    /* renamed from: X */
    public static final C4047a f7385X = new C4047a(false, false, null, false, null, false, null, null);

    /* renamed from: P */
    private final boolean f7386P = false;

    /* renamed from: Q */
    private final boolean f7387Q = false;

    /* renamed from: R */
    private final String f7388R = null;

    /* renamed from: S */
    private final boolean f7389S = false;

    /* renamed from: T */
    private final String f7390T = null;

    /* renamed from: U */
    private final boolean f7391U = false;

    /* renamed from: V */
    private final Long f7392V = null;

    /* renamed from: W */
    private final Long f7393W = null;

    /* renamed from: e.d.a.c.d.a$a */
    public static final class C4048a {
    }

    static {
        new C4048a();
    }

    private C4047a(boolean z, boolean z2, String str, boolean z3, String str2, boolean z4, Long l, Long l2) {
    }

    @Nullable
    /* renamed from: a */
    public final Long mo23682a() {
        return this.f7392V;
    }

    @Nullable
    /* renamed from: b */
    public final String mo23683b() {
        return this.f7390T;
    }

    @Nullable
    /* renamed from: c */
    public final Long mo23684c() {
        return this.f7393W;
    }

    /* renamed from: d */
    public final String mo23685d() {
        return this.f7388R;
    }

    /* renamed from: e */
    public final boolean mo23686e() {
        return this.f7389S;
    }

    /* renamed from: f */
    public final boolean mo23687f() {
        return this.f7387Q;
    }

    /* renamed from: g */
    public final boolean mo23688g() {
        return this.f7386P;
    }

    /* renamed from: h */
    public final boolean mo23689h() {
        return this.f7391U;
    }
}
