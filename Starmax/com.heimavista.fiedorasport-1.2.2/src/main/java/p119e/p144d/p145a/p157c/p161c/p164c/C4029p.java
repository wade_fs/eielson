package p119e.p144d.p145a.p157c.p161c.p164c;

/* renamed from: e.d.a.c.c.c.p */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
final class C4029p extends C4028o {

    /* renamed from: a */
    private final C4027n f7381a = new C4027n();

    C4029p() {
    }

    /* renamed from: a */
    public final void mo23660a(Throwable th, Throwable th2) {
        if (th2 == th) {
            throw new IllegalArgumentException("Self suppression is not allowed.", th2);
        } else if (th2 != null) {
            this.f7381a.mo23661a(th, true).add(th2);
        } else {
            throw new NullPointerException("The suppressed exception cannot be null.");
        }
    }
}
