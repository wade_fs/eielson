package p119e.p144d.p145a.p146a.p147i;

import android.util.Base64;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import p119e.p144d.p145a.p146a.C3879d;
import p119e.p144d.p145a.p146a.p147i.C3890c;

/* renamed from: e.d.a.a.i.l */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C3905l {

    /* renamed from: e.d.a.a.i.l$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public static abstract class C3906a {
        @RestrictTo({RestrictTo.Scope.LIBRARY})
        /* renamed from: a */
        public abstract C3906a mo23536a(C3879d dVar);

        /* renamed from: a */
        public abstract C3906a mo23537a(String str);

        /* renamed from: a */
        public abstract C3906a mo23538a(@Nullable byte[] bArr);

        /* renamed from: a */
        public abstract C3905l mo23539a();
    }

    /* renamed from: d */
    public static C3906a m11763d() {
        C3890c.C3892b bVar = new C3890c.C3892b();
        bVar.mo23536a(C3879d.f7175P);
        return bVar;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    /* renamed from: a */
    public C3905l mo23558a(C3879d dVar) {
        C3906a d = m11763d();
        d.mo23537a(mo23531a());
        d.mo23536a(dVar);
        d.mo23538a(mo23532b());
        return d.mo23539a();
    }

    /* renamed from: a */
    public abstract String mo23531a();

    @Nullable
    /* renamed from: b */
    public abstract byte[] mo23532b();

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    /* renamed from: c */
    public abstract C3879d mo23533c();

    public final String toString() {
        Object[] objArr = new Object[3];
        objArr[0] = mo23531a();
        objArr[1] = mo23533c();
        objArr[2] = mo23532b() == null ? "" : Base64.encodeToString(mo23532b(), 2);
        return String.format("TransportContext(%s, %s, %s)", objArr);
    }
}
