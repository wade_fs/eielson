package p119e.p144d.p145a.p157c.p160b;

import android.os.IBinder;
import android.os.IInterface;
import p119e.p144d.p145a.p157c.p161c.p163b.C4009a;
import p119e.p144d.p145a.p157c.p161c.p163b.C4010b;

/* renamed from: e.d.a.c.b.b */
public interface C3988b extends IInterface {

    /* renamed from: e.d.a.c.b.b$a */
    public static abstract class C3989a extends C4010b implements C3988b {

        /* renamed from: e.d.a.c.b.b$a$a */
        public static class C3990a extends C4009a implements C3988b {
            C3990a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        public C3989a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        /* renamed from: a */
        public static C3988b m11981a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            if (queryLocalInterface instanceof C3988b) {
                return (C3988b) queryLocalInterface;
            }
            return new C3990a(iBinder);
        }
    }
}
