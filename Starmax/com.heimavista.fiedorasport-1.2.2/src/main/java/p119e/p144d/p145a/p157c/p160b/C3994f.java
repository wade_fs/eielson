package p119e.p144d.p145a.p157c.p160b;

import android.content.Context;
import android.os.IBinder;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.internal.C2258v;

/* renamed from: e.d.a.c.b.f */
public abstract class C3994f<T> {

    /* renamed from: a */
    private final String f7351a;

    /* renamed from: b */
    private T f7352b;

    /* renamed from: e.d.a.c.b.f$a */
    public static class C3995a extends Exception {
        public C3995a(String str) {
            super(str);
        }

        public C3995a(String str, Throwable th) {
            super(str, th);
        }
    }

    protected C3994f(String str) {
        this.f7351a = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final T mo23634a(Context context) {
        if (this.f7352b == null) {
            C2258v.m5629a(context);
            Context c = C2176f.m5302c(context);
            if (c != null) {
                try {
                    this.f7352b = mo17044a((IBinder) c.getClassLoader().loadClass(this.f7351a).newInstance());
                } catch (ClassNotFoundException e) {
                    throw new C3995a("Could not load creator class.", e);
                } catch (InstantiationException e2) {
                    throw new C3995a("Could not instantiate creator.", e2);
                } catch (IllegalAccessException e3) {
                    throw new C3995a("Could not access creator.", e3);
                }
            } else {
                throw new C3995a("Could not get remote context.");
            }
        }
        return this.f7352b;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract T mo17044a(IBinder iBinder);
}
