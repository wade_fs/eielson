package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;

/* renamed from: e.d.a.a.i.v.j.h */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C3945h {
    /* renamed from: a */
    public static C3945h m11863a(long j, C3905l lVar, C3899h hVar) {
        return new C3932b(j, lVar, hVar);
    }

    /* renamed from: a */
    public abstract C3899h mo23579a();

    /* renamed from: b */
    public abstract long mo23580b();

    /* renamed from: c */
    public abstract C3905l mo23581c();
}
