package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.Parcel;

/* renamed from: e.d.a.c.c.d.c */
public final class C4034c extends C4032a implements C4046o {
    C4034c(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.maps.model.internal.IPolylineDelegate");
    }

    /* renamed from: a */
    public final boolean mo23668a(C4046o oVar) {
        Parcel a = mo23664a();
        C4039h.m12043a(a, oVar);
        Parcel a2 = mo23665a(15, a);
        boolean a3 = C4039h.m12046a(a2);
        a2.recycle();
        return a3;
    }

    /* renamed from: k */
    public final int mo23669k() {
        Parcel a = mo23665a(16, mo23664a());
        int readInt = a.readInt();
        a.recycle();
        return readInt;
    }

    public final void remove() {
        mo23667b(1, mo23664a());
    }
}
