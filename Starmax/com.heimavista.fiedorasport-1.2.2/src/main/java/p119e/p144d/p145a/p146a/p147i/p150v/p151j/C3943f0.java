package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.content.Context;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.v.j.f0 */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3943f0 implements Factory<C3940e0> {

    /* renamed from: a */
    private final Provider<Context> f7292a;

    /* renamed from: b */
    private final Provider<Integer> f7293b;

    public C3943f0(Provider<Context> aVar, Provider<Integer> aVar2) {
        this.f7292a = aVar;
        this.f7293b = aVar2;
    }

    /* renamed from: a */
    public static C3943f0 m11860a(Provider<Context> aVar, Provider<Integer> aVar2) {
        return new C3943f0(aVar, aVar2);
    }

    public C3940e0 get() {
        return new C3940e0(this.f7292a.get(), this.f7293b.get().intValue());
    }
}
