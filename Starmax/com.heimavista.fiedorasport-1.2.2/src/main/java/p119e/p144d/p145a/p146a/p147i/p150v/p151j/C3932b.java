package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;

/* renamed from: e.d.a.a.i.v.j.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3932b extends C3945h {

    /* renamed from: a */
    private final long f7277a;

    /* renamed from: b */
    private final C3905l f7278b;

    /* renamed from: c */
    private final C3899h f7279c;

    C3932b(long j, C3905l lVar, C3899h hVar) {
        this.f7277a = j;
        if (lVar != null) {
            this.f7278b = lVar;
            if (hVar != null) {
                this.f7279c = hVar;
                return;
            }
            throw new NullPointerException("Null event");
        }
        throw new NullPointerException("Null transportContext");
    }

    /* renamed from: a */
    public C3899h mo23579a() {
        return this.f7279c;
    }

    /* renamed from: b */
    public long mo23580b() {
        return this.f7277a;
    }

    /* renamed from: c */
    public C3905l mo23581c() {
        return this.f7278b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3945h)) {
            return false;
        }
        C3945h hVar = (C3945h) obj;
        if (this.f7277a != super.mo23580b() || !this.f7278b.equals(super.mo23581c()) || !this.f7279c.equals(super.mo23579a())) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        long j = this.f7277a;
        return this.f7279c.hashCode() ^ ((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.f7278b.hashCode()) * 1000003);
    }

    public String toString() {
        return "PersistedEvent{id=" + this.f7277a + ", transportContext=" + this.f7278b + ", event=" + this.f7279c + "}";
    }
}
