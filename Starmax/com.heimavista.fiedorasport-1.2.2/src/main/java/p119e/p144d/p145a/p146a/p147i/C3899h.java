package p119e.p144d.p145a.p146a.p147i;

import androidx.annotation.Nullable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import p119e.p144d.p145a.p146a.p147i.C3884a;

/* renamed from: e.d.a.a.i.h */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public abstract class C3899h {

    /* renamed from: e.d.a.a.i.h$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public static abstract class C3900a {
        /* renamed from: a */
        public abstract C3900a mo23509a(long j);

        /* renamed from: a */
        public abstract C3900a mo23510a(C3898g gVar);

        /* renamed from: a */
        public abstract C3900a mo23511a(Integer num);

        /* renamed from: a */
        public abstract C3900a mo23512a(String str);

        /* renamed from: a */
        public final C3900a mo23556a(String str, String str2) {
            mo23516b().put(str, str2);
            return this;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public abstract C3900a mo23513a(Map<String, String> map);

        /* renamed from: a */
        public abstract C3899h mo23514a();

        /* renamed from: b */
        public abstract C3900a mo23515b(long j);

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public abstract Map<String, String> mo23516b();

        /* renamed from: a */
        public final C3900a mo23555a(String str, long j) {
            mo23516b().put(str, String.valueOf(j));
            return this;
        }

        /* renamed from: a */
        public final C3900a mo23554a(String str, int i) {
            mo23516b().put(str, String.valueOf(i));
            return this;
        }
    }

    /* renamed from: i */
    public static C3900a m11724i() {
        C3884a.C3886b bVar = new C3884a.C3886b();
        bVar.mo23513a(new HashMap());
        return bVar;
    }

    /* renamed from: a */
    public final String mo23549a(String str) {
        String str2 = mo23500a().get(str);
        return str2 == null ? "" : str2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract Map<String, String> mo23500a();

    /* renamed from: b */
    public final int mo23550b(String str) {
        String str2 = mo23500a().get(str);
        if (str2 == null) {
            return 0;
        }
        return Integer.valueOf(str2).intValue();
    }

    @Nullable
    /* renamed from: b */
    public abstract Integer mo23501b();

    /* renamed from: c */
    public final long mo23551c(String str) {
        String str2 = mo23500a().get(str);
        if (str2 == null) {
            return 0;
        }
        return Long.valueOf(str2).longValue();
    }

    /* renamed from: c */
    public abstract C3898g mo23502c();

    /* renamed from: d */
    public abstract long mo23503d();

    /* renamed from: e */
    public final Map<String, String> mo23552e() {
        return Collections.unmodifiableMap(mo23500a());
    }

    /* renamed from: f */
    public abstract String mo23505f();

    /* renamed from: g */
    public abstract long mo23506g();

    /* renamed from: h */
    public C3900a mo23553h() {
        C3884a.C3886b bVar = new C3884a.C3886b();
        bVar.mo23512a(mo23505f());
        bVar.mo23511a(mo23501b());
        bVar.mo23510a(mo23502c());
        bVar.mo23509a(mo23503d());
        bVar.mo23515b(mo23506g());
        bVar.mo23513a(new HashMap(mo23500a()));
        return bVar;
    }
}
