package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.p */
final class C4076p<TResult> implements C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7426a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Object f7427b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C4055b f7428c;

    public C4076p(@NonNull Executor executor, @NonNull C4055b bVar) {
        this.f7426a = executor;
        this.f7428c = bVar;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h hVar) {
        if (hVar.mo23712c()) {
            synchronized (this.f7427b) {
                if (this.f7428c != null) {
                    this.f7426a.execute(new C4077q(this));
                }
            }
        }
    }

    public final void cancel() {
        synchronized (this.f7427b) {
            this.f7428c = null;
        }
    }
}
