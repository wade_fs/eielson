package p119e.p144d.p145a.p146a.p147i;

import java.util.Set;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3880e;
import p119e.p144d.p145a.p146a.C3881f;
import p119e.p144d.p145a.p146a.C3882g;

/* renamed from: e.d.a.a.i.m */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3907m implements C3882g {

    /* renamed from: a */
    private final Set<C3877b> f7223a;

    /* renamed from: b */
    private final C3905l f7224b;

    /* renamed from: c */
    private final C3910p f7225c;

    C3907m(Set<C3877b> set, C3905l lVar, C3910p pVar) {
        this.f7223a = set;
        this.f7224b = lVar;
        this.f7225c = pVar;
    }

    /* renamed from: a */
    public <T> C3881f<T> mo22308a(String str, Class<T> cls, C3877b bVar, C3880e<T, byte[]> eVar) {
        if (this.f7223a.contains(bVar)) {
            return new C3909o(this.f7224b, str, bVar, eVar, this.f7225c);
        }
        throw new IllegalArgumentException(String.format("%s is not supported byt this factory. Supported encodings are: %s.", bVar, this.f7223a));
    }
}
