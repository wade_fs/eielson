package p119e.p144d.p145a.p157c.p158a.p159a;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.RemoteException;
import android.os.SystemClock;
import android.util.Log;
import androidx.annotation.Nullable;
import com.facebook.appevents.AppEventsConstants;
import com.facebook.appevents.internal.ViewHierarchyConstants;
import com.google.android.gms.common.C2015a;
import com.google.android.gms.common.C2169c;
import com.google.android.gms.common.C2170d;
import com.google.android.gms.common.C2176f;
import com.google.android.gms.common.internal.C2258v;
import com.google.android.gms.common.stats.C2303a;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import p119e.p144d.p145a.p157c.p161c.p162a.C4006d;
import p119e.p144d.p145a.p157c.p161c.p162a.C4007e;

/* renamed from: e.d.a.c.a.a.a */
public class C3980a {
    @Nullable

    /* renamed from: a */
    private C2015a f7330a;
    @Nullable

    /* renamed from: b */
    private C4006d f7331b;

    /* renamed from: c */
    private boolean f7332c;

    /* renamed from: d */
    private final Object f7333d = new Object();
    @Nullable

    /* renamed from: e */
    private C3982b f7334e;

    /* renamed from: f */
    private final Context f7335f;

    /* renamed from: g */
    private final boolean f7336g;

    /* renamed from: h */
    private final long f7337h;

    /* renamed from: e.d.a.c.a.a.a$a */
    public static final class C3981a {

        /* renamed from: a */
        private final String f7338a;

        /* renamed from: b */
        private final boolean f7339b;

        public C3981a(String str, boolean z) {
            this.f7338a = str;
            this.f7339b = z;
        }

        /* renamed from: a */
        public final String mo23610a() {
            return this.f7338a;
        }

        /* renamed from: b */
        public final boolean mo23611b() {
            return this.f7339b;
        }

        public final String toString() {
            String str = this.f7338a;
            boolean z = this.f7339b;
            StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 7);
            sb.append("{");
            sb.append(str);
            sb.append("}");
            sb.append(z);
            return sb.toString();
        }
    }

    /* renamed from: e.d.a.c.a.a.a$b */
    static class C3982b extends Thread {

        /* renamed from: P */
        private WeakReference<C3980a> f7340P;

        /* renamed from: Q */
        private long f7341Q;

        /* renamed from: R */
        CountDownLatch f7342R = new CountDownLatch(1);

        /* renamed from: S */
        boolean f7343S = false;

        public C3982b(C3980a aVar, long j) {
            this.f7340P = new WeakReference<>(aVar);
            this.f7341Q = j;
            start();
        }

        /* renamed from: a */
        private final void m11955a() {
            C3980a aVar = this.f7340P.get();
            if (aVar != null) {
                aVar.mo23607a();
                this.f7343S = true;
            }
        }

        public final void run() {
            try {
                if (!this.f7342R.await(this.f7341Q, TimeUnit.MILLISECONDS)) {
                    m11955a();
                }
            } catch (InterruptedException unused) {
                m11955a();
            }
        }
    }

    private C3980a(Context context, long j, boolean z, boolean z2) {
        Context applicationContext;
        C2258v.m5629a(context);
        if (z && (applicationContext = context.getApplicationContext()) != null) {
            context = applicationContext;
        }
        this.f7335f = context;
        this.f7332c = false;
        this.f7337h = j;
        this.f7336g = z2;
    }

    /* renamed from: a */
    private static C2015a m11944a(Context context, boolean z) {
        try {
            context.getPackageManager().getPackageInfo("com.android.vending", 0);
            int a = C2169c.m5270a().mo16820a(context, (int) C2176f.f3566a);
            if (a == 0 || a == 2) {
                String str = z ? "com.google.android.gms.ads.identifier.service.PERSISTENT_START" : "com.google.android.gms.ads.identifier.service.START";
                C2015a aVar = new C2015a();
                Intent intent = new Intent(str);
                intent.setPackage("com.google.android.gms");
                try {
                    if (C2303a.m5745a().mo17124a(context, intent, aVar, 1)) {
                        return aVar;
                    }
                    throw new IOException("Connection failure");
                } catch (Throwable th) {
                    throw new IOException(th);
                }
            } else {
                throw new IOException("Google Play services not available");
            }
        } catch (PackageManager.NameNotFoundException unused) {
            throw new C2170d(9);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.a.a.c.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      e.d.a.c.a.a.c.a(java.lang.String, float):float
      e.d.a.c.a.a.c.a(java.lang.String, java.lang.String):java.lang.String
      e.d.a.c.a.a.c.a(java.lang.String, boolean):boolean */
    /* renamed from: a */
    public static C3981a m11945a(Context context) {
        C3984c cVar = new C3984c(context);
        boolean a = cVar.mo23617a("gads:ad_id_app_context:enabled", false);
        float a2 = cVar.mo23615a("gads:ad_id_app_context:ping_ratio", 0.0f);
        String a3 = cVar.mo23616a("gads:ad_id_use_shared_preference:experiment_id", "");
        C3980a aVar = new C3980a(context, -1, a, cVar.mo23617a("gads:ad_id_use_persistent_service:enabled", false));
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime();
            aVar.m11949b(false);
            C3981a b = aVar.mo23608b();
            aVar.m11948a(b, a, a2, SystemClock.elapsedRealtime() - elapsedRealtime, a3, null);
            aVar.mo23607a();
            return b;
        } catch (Throwable th) {
            aVar.mo23607a();
            throw th;
        }
    }

    /* renamed from: a */
    private static C4006d m11946a(Context context, C2015a aVar) {
        try {
            return C4007e.m12006a(aVar.mo16500a(10000, TimeUnit.MILLISECONDS));
        } catch (InterruptedException unused) {
            throw new IOException("Interrupted exception");
        } catch (Throwable th) {
            throw new IOException(th);
        }
    }

    /* renamed from: a */
    public static void m11947a(boolean z) {
    }

    /* renamed from: a */
    private final boolean m11948a(C3981a aVar, boolean z, float f, long j, String str, Throwable th) {
        if (Math.random() > ((double) f)) {
            return false;
        }
        HashMap hashMap = new HashMap();
        String str2 = AppEventsConstants.EVENT_PARAM_VALUE_YES;
        hashMap.put("app_context", z ? str2 : AppEventsConstants.EVENT_PARAM_VALUE_NO);
        if (aVar != null) {
            if (!aVar.mo23611b()) {
                str2 = AppEventsConstants.EVENT_PARAM_VALUE_NO;
            }
            hashMap.put("limit_ad_tracking", str2);
        }
        if (!(aVar == null || aVar.mo23610a() == null)) {
            hashMap.put("ad_id_size", Integer.toString(aVar.mo23610a().length()));
        }
        if (th != null) {
            hashMap.put("error", th.getClass().getName());
        }
        if (str != null && !str.isEmpty()) {
            hashMap.put("experiment_id", str);
        }
        hashMap.put(ViewHierarchyConstants.TAG_KEY, "AdvertisingIdClient");
        hashMap.put("time_spent", Long.toString(j));
        new C3983b(this, hashMap).start();
        return true;
    }

    /* renamed from: b */
    private final void m11949b(boolean z) {
        C2258v.m5643c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (this.f7332c) {
                mo23607a();
            }
            this.f7330a = m11944a(this.f7335f, this.f7336g);
            this.f7331b = m11946a(this.f7335f, this.f7330a);
            this.f7332c = true;
            if (z) {
                m11950c();
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:2|3|(3:5|6|7)|8|9|(1:11)|12) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0013 */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x001b  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m11950c() {
        /*
            r6 = this;
            java.lang.Object r0 = r6.f7333d
            monitor-enter(r0)
            e.d.a.c.a.a.a$b r1 = r6.f7334e     // Catch:{ all -> 0x0026 }
            if (r1 == 0) goto L_0x0013
            e.d.a.c.a.a.a$b r1 = r6.f7334e     // Catch:{ all -> 0x0026 }
            java.util.concurrent.CountDownLatch r1 = r1.f7342R     // Catch:{ all -> 0x0026 }
            r1.countDown()     // Catch:{ all -> 0x0026 }
            e.d.a.c.a.a.a$b r1 = r6.f7334e     // Catch:{ InterruptedException -> 0x0013 }
            r1.join()     // Catch:{ InterruptedException -> 0x0013 }
        L_0x0013:
            long r1 = r6.f7337h     // Catch:{ all -> 0x0026 }
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0024
            e.d.a.c.a.a.a$b r1 = new e.d.a.c.a.a.a$b     // Catch:{ all -> 0x0026 }
            long r2 = r6.f7337h     // Catch:{ all -> 0x0026 }
            r1.<init>(r6, r2)     // Catch:{ all -> 0x0026 }
            r6.f7334e = r1     // Catch:{ all -> 0x0026 }
        L_0x0024:
            monitor-exit(r0)     // Catch:{ all -> 0x0026 }
            return
        L_0x0026:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0026 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p145a.p157c.p158a.p159a.C3980a.m11950c():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0032, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo23607a() {
        /*
            r3 = this;
            java.lang.String r0 = "Calling this from your main thread can lead to deadlock"
            com.google.android.gms.common.internal.C2258v.m5643c(r0)
            monitor-enter(r3)
            android.content.Context r0 = r3.f7335f     // Catch:{ all -> 0x0033 }
            if (r0 == 0) goto L_0x0031
            com.google.android.gms.common.a r0 = r3.f7330a     // Catch:{ all -> 0x0033 }
            if (r0 != 0) goto L_0x000f
            goto L_0x0031
        L_0x000f:
            boolean r0 = r3.f7332c     // Catch:{ all -> 0x001f }
            if (r0 == 0) goto L_0x0027
            com.google.android.gms.common.stats.a r0 = com.google.android.gms.common.stats.C2303a.m5745a()     // Catch:{ all -> 0x001f }
            android.content.Context r1 = r3.f7335f     // Catch:{ all -> 0x001f }
            com.google.android.gms.common.a r2 = r3.f7330a     // Catch:{ all -> 0x001f }
            r0.mo17123a(r1, r2)     // Catch:{ all -> 0x001f }
            goto L_0x0027
        L_0x001f:
            r0 = move-exception
            java.lang.String r1 = "AdvertisingIdClient"
            java.lang.String r2 = "AdvertisingIdClient unbindService failed."
            android.util.Log.i(r1, r2, r0)     // Catch:{ all -> 0x0033 }
        L_0x0027:
            r0 = 0
            r3.f7332c = r0     // Catch:{ all -> 0x0033 }
            r0 = 0
            r3.f7331b = r0     // Catch:{ all -> 0x0033 }
            r3.f7330a = r0     // Catch:{ all -> 0x0033 }
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            return
        L_0x0031:
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            return
        L_0x0033:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0033 }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p145a.p157c.p158a.p159a.C3980a.mo23607a():void");
    }

    /* renamed from: b */
    public C3981a mo23608b() {
        C3981a aVar;
        C2258v.m5643c("Calling this from your main thread can lead to deadlock");
        synchronized (this) {
            if (!this.f7332c) {
                synchronized (this.f7333d) {
                    if (this.f7334e == null || !this.f7334e.f7343S) {
                        throw new IOException("AdvertisingIdClient is not connected.");
                    }
                }
                try {
                    m11949b(false);
                    if (!this.f7332c) {
                        throw new IOException("AdvertisingIdClient cannot reconnect.");
                    }
                } catch (RemoteException e) {
                    Log.i("AdvertisingIdClient", "GMS remote exception ", e);
                    throw new IOException("Remote exception");
                } catch (Exception e2) {
                    throw new IOException("AdvertisingIdClient cannot reconnect.", e2);
                }
            }
            C2258v.m5629a(this.f7330a);
            C2258v.m5629a(this.f7331b);
            aVar = new C3981a(this.f7331b.getId(), this.f7331b.mo23639d(true));
        }
        m11950c();
        return aVar;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        mo23607a();
        super.finalize();
    }
}
