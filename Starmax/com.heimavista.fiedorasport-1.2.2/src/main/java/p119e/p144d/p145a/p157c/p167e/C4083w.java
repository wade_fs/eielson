package p119e.p144d.p145a.p157c.p167e;

/* renamed from: e.d.a.c.e.w */
final class C4083w implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4065h f7443P;

    /* renamed from: Q */
    private final /* synthetic */ C4082v f7444Q;

    C4083w(C4082v vVar, C4065h hVar) {
        this.f7444Q = vVar;
        this.f7443P = hVar;
    }

    public final void run() {
        synchronized (this.f7444Q.f7441b) {
            if (this.f7444Q.f7442c != null) {
                this.f7444Q.f7442c.onSuccess(this.f7443P.mo23709b());
            }
        }
    }
}
