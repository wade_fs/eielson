package p119e.p144d.p145a.p157c.p161c.p164c;

/* renamed from: e.d.a.c.c.c.m */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public final class C4026m {
    /* renamed from: a */
    public static int m12029a(int i, int i2) {
        long j = ((long) i) << 1;
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        if (j < -2147483648L) {
            return Integer.MIN_VALUE;
        }
        return (int) j;
    }
}
