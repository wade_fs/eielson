package p119e.p144d.p145a.p146a;

import androidx.annotation.Nullable;

/* renamed from: e.d.a.a.a */
/* compiled from: com.google.android.datatransport:transport-api@@2.2.0 */
final class C3876a<T> extends C3878c<T> {

    /* renamed from: a */
    private final Integer f7171a;

    /* renamed from: b */
    private final T f7172b;

    /* renamed from: c */
    private final C3879d f7173c;

    C3876a(@Nullable Integer num, T t, C3879d dVar) {
        this.f7171a = num;
        if (t != null) {
            this.f7172b = t;
            if (dVar != null) {
                this.f7173c = dVar;
                return;
            }
            throw new NullPointerException("Null priority");
        }
        throw new NullPointerException("Null payload");
    }

    @Nullable
    /* renamed from: a */
    public Integer mo23489a() {
        return this.f7171a;
    }

    /* renamed from: b */
    public T mo23490b() {
        return this.f7172b;
    }

    /* renamed from: c */
    public C3879d mo23491c() {
        return this.f7173c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3878c)) {
            return false;
        }
        C3878c cVar = (C3878c) obj;
        Integer num = this.f7171a;
        if (num != null ? num.equals(super.mo23489a()) : super.mo23489a() == null) {
            if (!this.f7172b.equals(super.mo23490b()) || !this.f7173c.equals(super.mo23491c())) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        Integer num = this.f7171a;
        return (((((num == null ? 0 : num.hashCode()) ^ 1000003) * 1000003) ^ this.f7172b.hashCode()) * 1000003) ^ this.f7173c.hashCode();
    }

    public String toString() {
        return "Event{code=" + this.f7171a + ", payload=" + ((Object) this.f7172b) + ", priority=" + this.f7173c + "}";
    }
}
