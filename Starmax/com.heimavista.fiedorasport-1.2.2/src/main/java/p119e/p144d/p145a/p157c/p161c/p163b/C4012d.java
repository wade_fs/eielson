package p119e.p144d.p145a.p157c.p161c.p163b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: e.d.a.c.c.b.d */
public class C4012d extends Handler {
    public C4012d(Looper looper) {
        super(looper);
    }

    public final void dispatchMessage(Message message) {
        super.dispatchMessage(message);
    }

    public C4012d(Looper looper, Handler.Callback callback) {
        super(looper, callback);
    }
}
