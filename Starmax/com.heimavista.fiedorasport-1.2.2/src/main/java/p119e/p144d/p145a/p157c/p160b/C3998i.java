package p119e.p144d.p145a.p157c.p160b;

import android.os.Bundle;
import p119e.p144d.p145a.p157c.p160b.C3986a;

/* renamed from: e.d.a.c.b.i */
final class C3998i implements C3986a.C3987a {

    /* renamed from: a */
    private final /* synthetic */ Bundle f7358a;

    /* renamed from: b */
    private final /* synthetic */ C3986a f7359b;

    C3998i(C3986a aVar, Bundle bundle) {
        this.f7359b = aVar;
        this.f7358a = bundle;
    }

    /* renamed from: a */
    public final void mo23631a(C3991c cVar) {
        this.f7359b.f7346a.mo18354b(this.f7358a);
    }

    public final int getState() {
        return 1;
    }
}
