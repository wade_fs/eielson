package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.x */
final class C4084x<TResult, TContinuationResult> implements C4055b, C4060d, C4062e<TContinuationResult>, C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7445a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final C4064g<TResult, TContinuationResult> f7446b;

    /* renamed from: c */
    private final C4058c0<TContinuationResult> f7447c;

    public C4084x(@NonNull Executor executor, @NonNull C4064g<TResult, TContinuationResult> gVar, @NonNull C4058c0<TContinuationResult> c0Var) {
        this.f7445a = executor;
        this.f7446b = gVar;
        this.f7447c = c0Var;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h<TResult> hVar) {
        this.f7445a.execute(new C4085y(this, hVar));
    }

    public final void cancel() {
        throw new UnsupportedOperationException();
    }

    public final void onSuccess(TContinuationResult tcontinuationresult) {
        this.f7447c.mo23707a((Object) tcontinuationresult);
    }

    /* renamed from: a */
    public final void mo22541a(@NonNull Exception exc) {
        this.f7447c.mo23706a(exc);
    }

    /* renamed from: a */
    public final void mo23692a() {
        this.f7447c.mo23715f();
    }
}
