package p119e.p144d.p145a.p146a.p147i;

import androidx.annotation.NonNull;
import java.util.Arrays;
import p119e.p144d.p145a.p146a.C3877b;

/* renamed from: e.d.a.a.i.g */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3898g {

    /* renamed from: a */
    private final C3877b f7220a;

    /* renamed from: b */
    private final byte[] f7221b;

    public C3898g(@NonNull C3877b bVar, @NonNull byte[] bArr) {
        if (bVar == null) {
            throw new NullPointerException("encoding is null");
        } else if (bArr != null) {
            this.f7220a = bVar;
            this.f7221b = bArr;
        } else {
            throw new NullPointerException("bytes is null");
        }
    }

    /* renamed from: a */
    public byte[] mo23544a() {
        return this.f7221b;
    }

    /* renamed from: b */
    public C3877b mo23545b() {
        return this.f7220a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C3898g)) {
            return false;
        }
        C3898g gVar = (C3898g) obj;
        if (!this.f7220a.equals(gVar.f7220a)) {
            return false;
        }
        return Arrays.equals(this.f7221b, gVar.f7221b);
    }

    public int hashCode() {
        return ((this.f7220a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.f7221b);
    }

    public String toString() {
        return "EncodedPayload{encoding=" + this.f7220a + ", bytes=[...]}";
    }
}
