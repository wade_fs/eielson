package p119e.p144d.p145a.p157c.p167e;

/* renamed from: e.d.a.c.e.m */
final class C4073m implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4065h f7419P;

    /* renamed from: Q */
    private final /* synthetic */ C4072l f7420Q;

    C4073m(C4072l lVar, C4065h hVar) {
        this.f7420Q = lVar;
        this.f7419P = hVar;
    }

    public final void run() {
        if (this.f7419P.mo23712c()) {
            this.f7420Q.f7418c.mo23715f();
            return;
        }
        try {
            this.f7420Q.f7418c.mo23707a(this.f7420Q.f7417b.mo16773a(this.f7419P));
        } catch (C4063f e) {
            if (e.getCause() instanceof Exception) {
                this.f7420Q.f7418c.mo23706a((Exception) e.getCause());
            } else {
                this.f7420Q.f7418c.mo23706a((Exception) e);
            }
        } catch (Exception e2) {
            this.f7420Q.f7418c.mo23706a(e2);
        }
    }
}
