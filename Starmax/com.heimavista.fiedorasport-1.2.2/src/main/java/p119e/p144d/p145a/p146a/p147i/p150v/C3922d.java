package p119e.p144d.p145a.p146a.p147i.p150v;

import com.google.android.datatransport.runtime.backends.C1713e;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1749s;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.v.d */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3922d implements Factory<C3921c> {

    /* renamed from: a */
    private final Provider<Executor> f7255a;

    /* renamed from: b */
    private final Provider<C1713e> f7256b;

    /* renamed from: c */
    private final Provider<C1749s> f7257c;

    /* renamed from: d */
    private final Provider<C3934c> f7258d;

    /* renamed from: e */
    private final Provider<C3969b> f7259e;

    public C3922d(Provider<Executor> aVar, Provider<C1713e> aVar2, Provider<C1749s> aVar3, Provider<C3934c> aVar4, Provider<C3969b> aVar5) {
        this.f7255a = aVar;
        this.f7256b = aVar2;
        this.f7257c = aVar3;
        this.f7258d = aVar4;
        this.f7259e = aVar5;
    }

    /* renamed from: a */
    public static C3922d m11804a(Provider<Executor> aVar, Provider<C1713e> aVar2, Provider<C1749s> aVar3, Provider<C3934c> aVar4, Provider<C3969b> aVar5) {
        return new C3922d(aVar, aVar2, aVar3, aVar4, aVar5);
    }

    public C3921c get() {
        return new C3921c(this.f7255a.get(), this.f7256b.get(), this.f7257c.get(), this.f7258d.get(), this.f7259e.get());
    }
}
