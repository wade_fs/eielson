package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabaseLockedException;
import android.os.SystemClock;
import android.util.Base64;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.facebook.share.internal.MessengerShareContentUtility;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;
import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.p147i.C3898g;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.p148t.C3915a;
import p119e.p144d.p145a.p146a.p147i.p152w.C3968a;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p119e.p144d.p145a.p146a.p147i.p154y.C3977a;

@WorkerThread
/* renamed from: e.d.a.a.i.v.j.z */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C3963z implements C3934c, C3969b {

    /* renamed from: T */
    private static final C3877b f7318T = C3877b.m11673a("proto");

    /* renamed from: P */
    private final C3940e0 f7319P;

    /* renamed from: Q */
    private final C3971a f7320Q;

    /* renamed from: R */
    private final C3971a f7321R;

    /* renamed from: S */
    private final C3936d f7322S;

    /* renamed from: e.d.a.a.i.v.j.z$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    interface C3965b<T, U> {
        U apply(T t);
    }

    /* renamed from: e.d.a.a.i.v.j.z$c */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    private static class C3966c {

        /* renamed from: a */
        final String f7323a;

        /* renamed from: b */
        final String f7324b;

        private C3966c(String str, String str2) {
            this.f7323a = str;
            this.f7324b = str2;
        }
    }

    /* renamed from: e.d.a.a.i.v.j.z$d */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    interface C3967d<T> {
        /* renamed from: a */
        T mo23601a();
    }

    C3963z(C3971a aVar, C3971a aVar2, C3936d dVar, C3940e0 e0Var) {
        this.f7319P = e0Var;
        this.f7320Q = aVar;
        this.f7321R = aVar2;
        this.f7322S = dVar;
    }

    /* renamed from: a */
    private SQLiteDatabase m11887a() {
        C3940e0 e0Var = this.f7319P;
        e0Var.getClass();
        return (SQLiteDatabase) m11895a(C3955r.m11877a(e0Var), C3957t.m11880a());
    }

    /* renamed from: b */
    static /* synthetic */ SQLiteDatabase m11905b(Throwable th) {
        throw new C3968a("Timed out while trying to open db.", th);
    }

    /* renamed from: c */
    private static String m11912c(Iterable<C3945h> iterable) {
        StringBuilder sb = new StringBuilder("(");
        Iterator<C3945h> it = iterable.iterator();
        while (it.hasNext()) {
            sb.append(it.next().mo23580b());
            if (it.hasNext()) {
                sb.append(',');
            }
        }
        sb.append(')');
        return sb.toString();
    }

    /* renamed from: d */
    private boolean m11916d() {
        return m11904b() * m11911c() >= this.f7322S.mo23570d();
    }

    public void close() {
        this.f7319P.close();
    }

    /* renamed from: r */
    public int mo23593r() {
        return ((Integer) m11894a(C3949l.m11870a(this.f7320Q.mo23604a() - this.f7322S.mo23568b()))).intValue();
    }

    /* renamed from: s */
    public Iterable<C3905l> mo23594s() {
        return (Iterable) m11894a(C3948k.m11869a());
    }

    @Nullable
    /* renamed from: b */
    private Long m11908b(SQLiteDatabase sQLiteDatabase, C3905l lVar) {
        StringBuilder sb = new StringBuilder("backend_name = ? and priority = ?");
        ArrayList arrayList = new ArrayList(Arrays.asList(lVar.mo23531a(), String.valueOf(C3977a.m11939a(lVar.mo23533c()))));
        if (lVar.mo23532b() != null) {
            sb.append(" and extras = ?");
            arrayList.add(Base64.encodeToString(lVar.mo23532b(), 0));
        }
        return (Long) m11893a(sQLiteDatabase.query("transport_contexts", new String[]{"_id"}, sb.toString(), (String[]) arrayList.toArray(new String[0]), null, null, null), C3959v.m11882a());
    }

    @Nullable
    /* renamed from: a */
    public C3945h mo23586a(C3905l lVar, C3899h hVar) {
        C3915a.m11794a("SQLiteEventStore", "Storing event with priority=%s, name=%s for destination %s", lVar.mo23533c(), hVar.mo23505f(), lVar.mo23531a());
        long longValue = ((Long) m11894a(C3958u.m11881a(this, lVar, hVar))).longValue();
        if (longValue < 1) {
            return null;
        }
        return C3945h.m11863a(longValue, lVar, hVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* renamed from: a */
    static /* synthetic */ Long m11891a(C3963z zVar, C3905l lVar, C3899h hVar, SQLiteDatabase sQLiteDatabase) {
        if (zVar.m11916d()) {
            return -1L;
        }
        long a = zVar.m11886a(sQLiteDatabase, lVar);
        ContentValues contentValues = new ContentValues();
        contentValues.put("context_id", Long.valueOf(a));
        contentValues.put("transport_name", hVar.mo23505f());
        contentValues.put("timestamp_ms", Long.valueOf(hVar.mo23503d()));
        contentValues.put("uptime_ms", Long.valueOf(hVar.mo23506g()));
        contentValues.put("payload_encoding", hVar.mo23502c().mo23545b().mo23495a());
        contentValues.put(MessengerShareContentUtility.ATTACHMENT_PAYLOAD, hVar.mo23502c().mo23544a());
        contentValues.put("code", hVar.mo23501b());
        contentValues.put("num_attempts", (Integer) 0);
        long insert = sQLiteDatabase.insert("events", null, contentValues);
        for (Map.Entry entry : hVar.mo23552e().entrySet()) {
            ContentValues contentValues2 = new ContentValues();
            contentValues2.put("event_id", Long.valueOf(insert));
            contentValues2.put("name", (String) entry.getKey());
            contentValues2.put("value", (String) entry.getValue());
            sQLiteDatabase.insert("event_metadata", null, contentValues2);
        }
        return Long.valueOf(insert);
    }

    /* renamed from: c */
    public boolean mo23592c(C3905l lVar) {
        return ((Boolean) m11894a(C3962y.m11885a(this, lVar))).booleanValue();
    }

    /* renamed from: c */
    static /* synthetic */ List m11914c(SQLiteDatabase sQLiteDatabase) {
        return (List) m11893a(sQLiteDatabase.rawQuery("SELECT distinct t._id, t.backend_name, t.priority, t.extras FROM transport_contexts AS t, events AS e WHERE e.context_id = t._id", new String[0]), C3954q.m11876a());
    }

    /* renamed from: c */
    static /* synthetic */ List m11913c(Cursor cursor) {
        ArrayList arrayList = new ArrayList();
        while (cursor.moveToNext()) {
            C3905l.C3906a d = C3905l.m11763d();
            d.mo23537a(cursor.getString(1));
            d.mo23536a(C3977a.m11940a(cursor.getInt(2)));
            d.mo23538a(m11903a(cursor.getString(3)));
            arrayList.add(d.mo23539a());
        }
        return arrayList;
    }

    /* renamed from: b */
    static /* synthetic */ Long m11907b(Cursor cursor) {
        if (!cursor.moveToNext()) {
            return null;
        }
        return Long.valueOf(cursor.getLong(0));
    }

    /* renamed from: b */
    public void mo23591b(Iterable<C3945h> iterable) {
        if (iterable.iterator().hasNext()) {
            m11894a(C3960w.m11883a("UPDATE events SET num_attempts = num_attempts + 1 WHERE _id in " + m11912c(iterable)));
        }
    }

    /* renamed from: c */
    private List<C3945h> m11915c(SQLiteDatabase sQLiteDatabase, C3905l lVar) {
        ArrayList arrayList = new ArrayList();
        Long b = m11908b(sQLiteDatabase, lVar);
        if (b == null) {
            return arrayList;
        }
        SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
        m11893a(sQLiteDatabase2.query("events", new String[]{"_id", "transport_name", "timestamp_ms", "uptime_ms", "payload_encoding", MessengerShareContentUtility.ATTACHMENT_PAYLOAD, "code"}, "context_id = ?", new String[]{b.toString()}, null, null, null, String.valueOf(this.f7322S.mo23569c())), C3950m.m11871a(arrayList, lVar));
        return arrayList;
    }

    /* renamed from: b */
    public long mo23590b(C3905l lVar) {
        return ((Long) m11893a(m11887a().rawQuery("SELECT next_request_ms FROM transport_contexts WHERE backend_name = ? and priority = ?", new String[]{lVar.mo23531a(), String.valueOf(C3977a.m11939a(lVar.mo23533c()))}), C3961x.m11884a())).longValue();
    }

    /* renamed from: b */
    static /* synthetic */ List m11910b(C3963z zVar, C3905l lVar, SQLiteDatabase sQLiteDatabase) {
        List<C3945h> c = zVar.m11915c(sQLiteDatabase, lVar);
        zVar.m11900a(c, zVar.m11901a(sQLiteDatabase, c));
        return c;
    }

    /* renamed from: c */
    private long m11911c() {
        return m11887a().compileStatement("PRAGMA page_size").simpleQueryForLong();
    }

    /* renamed from: b */
    private static C3877b m11906b(@Nullable String str) {
        if (str == null) {
            return f7318T;
        }
        return C3877b.m11673a(str);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* renamed from: a */
    private long m11886a(SQLiteDatabase sQLiteDatabase, C3905l lVar) {
        Long b = m11908b(sQLiteDatabase, lVar);
        if (b != null) {
            return b.longValue();
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put("backend_name", lVar.mo23531a());
        contentValues.put("priority", Integer.valueOf(C3977a.m11939a(lVar.mo23533c())));
        contentValues.put("next_request_ms", (Integer) 0);
        if (lVar.mo23532b() != null) {
            contentValues.put("extras", Base64.encodeToString(lVar.mo23532b(), 0));
        }
        return sQLiteDatabase.insert("transport_contexts", null, contentValues);
    }

    /* renamed from: b */
    private long m11904b() {
        return m11887a().compileStatement("PRAGMA page_count").simpleQueryForLong();
    }

    /* renamed from: a */
    static /* synthetic */ Object m11896a(String str, SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.compileStatement(str).execute();
        sQLiteDatabase.compileStatement("DELETE FROM events WHERE num_attempts >= 10").execute();
        return null;
    }

    /* renamed from: a */
    public void mo23589a(Iterable<C3945h> iterable) {
        if (iterable.iterator().hasNext()) {
            m11887a().compileStatement("DELETE FROM events WHERE _id in " + m11912c(iterable)).execute();
        }
    }

    /* renamed from: a */
    static /* synthetic */ Long m11890a(Cursor cursor) {
        if (cursor.moveToNext()) {
            return Long.valueOf(cursor.getLong(0));
        }
        return 0L;
    }

    /* renamed from: a */
    static /* synthetic */ Boolean m11888a(C3963z zVar, C3905l lVar, SQLiteDatabase sQLiteDatabase) {
        Long b = zVar.m11908b(sQLiteDatabase, lVar);
        if (b == null) {
            return false;
        }
        return (Boolean) m11893a(zVar.m11887a().rawQuery("SELECT 1 FROM events WHERE context_id = ? LIMIT 1", new String[]{b.toString()}), C3956s.m11879a());
    }

    /* renamed from: a */
    public void mo23588a(C3905l lVar, long j) {
        m11894a(C3946i.m11867a(j, lVar));
    }

    /* renamed from: a */
    static /* synthetic */ Object m11892a(long j, C3905l lVar, SQLiteDatabase sQLiteDatabase) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("next_request_ms", Long.valueOf(j));
        if (sQLiteDatabase.update("transport_contexts", contentValues, "backend_name = ? and priority = ?", new String[]{lVar.mo23531a(), String.valueOf(C3977a.m11939a(lVar.mo23533c()))}) < 1) {
            contentValues.put("backend_name", lVar.mo23531a());
            contentValues.put("priority", Integer.valueOf(C3977a.m11939a(lVar.mo23533c())));
            sQLiteDatabase.insert("transport_contexts", null, contentValues);
        }
        return null;
    }

    /* renamed from: a */
    public Iterable<C3945h> mo23587a(C3905l lVar) {
        return (Iterable) m11894a(C3947j.m11868a(this, lVar));
    }

    /* renamed from: a */
    private static byte[] m11903a(@Nullable String str) {
        if (str == null) {
            return null;
        }
        return Base64.decode(str, 0);
    }

    /* renamed from: a */
    static /* synthetic */ Object m11898a(List list, C3905l lVar, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            C3899h.C3900a i = C3899h.m11724i();
            i.mo23512a(cursor.getString(1));
            i.mo23509a(cursor.getLong(2));
            i.mo23515b(cursor.getLong(3));
            i.mo23510a(new C3898g(m11906b(cursor.getString(4)), cursor.getBlob(5)));
            if (!cursor.isNull(6)) {
                i.mo23511a(Integer.valueOf(cursor.getInt(6)));
            }
            list.add(C3945h.m11863a(j, lVar, i.mo23514a()));
        }
        return null;
    }

    /* renamed from: a */
    private Map<Long, Set<C3966c>> m11901a(SQLiteDatabase sQLiteDatabase, List<C3945h> list) {
        HashMap hashMap = new HashMap();
        StringBuilder sb = new StringBuilder("event_id IN (");
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i).mo23580b());
            if (i < list.size() - 1) {
                sb.append(',');
            }
        }
        sb.append(')');
        m11893a(sQLiteDatabase.query("event_metadata", new String[]{"event_id", "name", "value"}, sb.toString(), null, null, null, null), C3951n.m11872a(hashMap));
        return hashMap;
    }

    /* renamed from: a */
    static /* synthetic */ Object m11899a(Map map, Cursor cursor) {
        while (cursor.moveToNext()) {
            long j = cursor.getLong(0);
            Set set = (Set) map.get(Long.valueOf(j));
            if (set == null) {
                set = new HashSet();
                map.put(Long.valueOf(j), set);
            }
            set.add(new C3966c(cursor.getString(1), cursor.getString(2)));
        }
        return null;
    }

    /* renamed from: a */
    private List<C3945h> m11900a(List<C3945h> list, Map<Long, Set<C3966c>> map) {
        ListIterator<C3945h> listIterator = list.listIterator();
        while (listIterator.hasNext()) {
            C3945h next = listIterator.next();
            if (map.containsKey(Long.valueOf(next.mo23580b()))) {
                C3899h.C3900a h = next.mo23579a().mo23553h();
                for (C3966c cVar : map.get(Long.valueOf(next.mo23580b()))) {
                    h.mo23556a(cVar.f7323a, cVar.f7324b);
                }
                listIterator.set(C3945h.m11863a(next.mo23580b(), next.mo23581c(), h.mo23514a()));
            }
        }
        return list;
    }

    /* renamed from: a */
    private <T> T m11895a(C3967d<T> dVar, C3965b<Throwable, T> bVar) {
        long a = this.f7321R.mo23604a();
        while (true) {
            try {
                return dVar.mo23601a();
            } catch (SQLiteDatabaseLockedException e) {
                if (this.f7321R.mo23604a() >= ((long) this.f7322S.mo23567a()) + a) {
                    return bVar.apply(e);
                }
                SystemClock.sleep(50);
            }
        }
    }

    /* renamed from: a */
    private void m11902a(SQLiteDatabase sQLiteDatabase) {
        m11895a(C3952o.m11873a(sQLiteDatabase), C3953p.m11875a());
    }

    /* renamed from: a */
    static /* synthetic */ Object m11897a(Throwable th) {
        throw new C3968a("Timed out while trying to acquire the lock.", th);
    }

    /* renamed from: a */
    public <T> T mo23602a(C3969b.C3970a<T> aVar) {
        SQLiteDatabase a = m11887a();
        m11902a(a);
        try {
            T s = aVar.mo13587s();
            a.setTransactionSuccessful();
            return s;
        } finally {
            a.endTransaction();
        }
    }

    /* renamed from: a */
    private <T> T m11894a(C3965b<SQLiteDatabase, T> bVar) {
        SQLiteDatabase a = m11887a();
        a.beginTransaction();
        try {
            T apply = bVar.apply(a);
            a.setTransactionSuccessful();
            return apply;
        } finally {
            a.endTransaction();
        }
    }

    /* renamed from: a */
    private static <T> T m11893a(Cursor cursor, C3965b<Cursor, T> bVar) {
        try {
            return bVar.apply(cursor);
        } finally {
            cursor.close();
        }
    }
}
