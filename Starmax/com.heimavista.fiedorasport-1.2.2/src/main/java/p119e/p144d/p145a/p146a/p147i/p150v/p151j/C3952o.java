package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.o */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3952o implements C3963z.C3967d {

    /* renamed from: a */
    private final SQLiteDatabase f7304a;

    private C3952o(SQLiteDatabase sQLiteDatabase) {
        this.f7304a = sQLiteDatabase;
    }

    /* renamed from: a */
    public static C3963z.C3967d m11873a(SQLiteDatabase sQLiteDatabase) {
        return new C3952o(sQLiteDatabase);
    }

    /* renamed from: a */
    public Object mo23601a() {
        return this.f7304a.beginTransaction();
    }
}
