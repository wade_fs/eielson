package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.Cursor;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.v */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3959v implements C3963z.C3965b {

    /* renamed from: a */
    private static final C3959v f7313a = new C3959v();

    private C3959v() {
    }

    /* renamed from: a */
    public static C3963z.C3965b m11882a() {
        return f7313a;
    }

    public Object apply(Object obj) {
        return C3963z.m11907b((Cursor) obj);
    }
}
