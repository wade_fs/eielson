package p119e.p144d.p145a.p146a.p147i;

import android.content.Context;
import com.google.android.datatransport.runtime.backends.C1720j;
import com.google.android.datatransport.runtime.backends.C1723l;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1733g;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1743m;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1744n;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1747q;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1748r;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1749s;
import java.util.concurrent.Executor;
import p119e.p144d.p145a.p146a.p147i.C3912r;
import p119e.p144d.p145a.p146a.p147i.p150v.C3921c;
import p119e.p144d.p145a.p146a.p147i.p150v.C3922d;
import p119e.p144d.p145a.p146a.p147i.p150v.C3925g;
import p119e.p144d.p145a.p146a.p147i.p150v.C3927i;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3931a0;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3942f;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3943f0;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3944g;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;
import p119e.p144d.p145a.p146a.p147i.p153x.C3973c;
import p119e.p144d.p145a.p146a.p147i.p153x.C3974d;
import p224f.p225b.C4936d;
import p224f.p225b.DoubleCheck;
import p224f.p225b.InstanceFactory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.d */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3893d extends C3912r {

    /* renamed from: P */
    private Provider<Executor> f7207P;

    /* renamed from: Q */
    private Provider<Context> f7208Q;

    /* renamed from: R */
    private Provider f7209R;

    /* renamed from: S */
    private Provider f7210S;

    /* renamed from: T */
    private Provider f7211T;

    /* renamed from: U */
    private Provider<C3963z> f7212U;

    /* renamed from: V */
    private Provider<C1733g> f7213V;

    /* renamed from: W */
    private Provider<C1749s> f7214W;

    /* renamed from: X */
    private Provider<C3921c> f7215X;

    /* renamed from: Y */
    private Provider<C1743m> f7216Y;

    /* renamed from: Z */
    private Provider<C1747q> f7217Z;

    /* renamed from: a0 */
    private Provider<C3911q> f7218a0;

    /* renamed from: e.d.a.a.i.d$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    private static final class C3895b implements C3912r.C3913a {

        /* renamed from: a */
        private Context f7219a;

        private C3895b() {
        }

        public C3912r build() {
            C4936d.m17214a(this.f7219a, Context.class);
            return new C3893d(this.f7219a);
        }

        /* renamed from: a */
        public C3895b mo23542a(Context context) {
            C4936d.m17212a(context);
            this.f7219a = context;
            return this;
        }
    }

    /* renamed from: a */
    private void m11714a(Context context) {
        this.f7207P = DoubleCheck.m17209a(C3902j.m11748a());
        this.f7208Q = InstanceFactory.m17211a(context);
        this.f7209R = C1720j.m4298a(this.f7208Q, C3973c.m11933a(), C3974d.m11935a());
        this.f7210S = DoubleCheck.m17209a(C1723l.m4303a(this.f7208Q, this.f7209R));
        this.f7211T = C3943f0.m11860a(this.f7208Q, C3942f.m11858a());
        this.f7212U = DoubleCheck.m17209a(C3931a0.m11821a(C3973c.m11933a(), C3974d.m11935a(), C3944g.m11861a(), this.f7211T));
        this.f7213V = C3925g.m11808a(C3973c.m11933a());
        this.f7214W = C3927i.m11811a(this.f7208Q, this.f7212U, this.f7213V, C3974d.m11935a());
        Provider<Executor> aVar = this.f7207P;
        Provider aVar2 = this.f7210S;
        Provider<C1749s> aVar3 = this.f7214W;
        Provider<C3963z> aVar4 = this.f7212U;
        this.f7215X = C3922d.m11804a(aVar, aVar2, aVar3, aVar4, aVar4);
        Provider<Context> aVar5 = this.f7208Q;
        Provider aVar6 = this.f7210S;
        Provider<C3963z> aVar7 = this.f7212U;
        this.f7216Y = C1744n.m4360a(aVar5, aVar6, aVar7, this.f7214W, this.f7207P, aVar7, C3973c.m11933a());
        Provider<Executor> aVar8 = this.f7207P;
        Provider<C3963z> aVar9 = this.f7212U;
        this.f7217Z = C1748r.m4367a(aVar8, aVar9, this.f7214W, aVar9);
        this.f7218a0 = DoubleCheck.m17209a(C3914s.m11789a(C3973c.m11933a(), C3974d.m11935a(), this.f7215X, this.f7216Y, this.f7217Z));
    }

    /* renamed from: c */
    public static C3912r.C3913a m11715c() {
        return new C3895b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public C3911q mo23541b() {
        return this.f7218a0.get();
    }

    private C3893d(Context context) {
        m11714a(context);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C3934c mo23540a() {
        return this.f7212U.get();
    }
}
