package p119e.p144d.p145a.p157c.p166d;

import androidx.core.app.NotificationCompat;
import com.google.android.gms.common.api.C2016a;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.signin.internal.C3268a;

/* renamed from: e.d.a.c.d.b */
public final class C4049b {

    /* renamed from: a */
    private static final C2016a.C2028g<C3268a> f7394a = new C2016a.C2028g<>();

    /* renamed from: b */
    private static final C2016a.C2028g<C3268a> f7395b = new C2016a.C2028g<>();

    /* renamed from: c */
    public static final C2016a.C2017a<C3268a, C4047a> f7396c = new C4050c();

    /* renamed from: d */
    private static final C2016a.C2017a<C3268a, Object> f7397d = new C4051d();

    /* renamed from: e */
    public static final C2016a<C4047a> f7398e = new C2016a<>("SignIn.API", f7396c, f7394a);

    static {
        new Scope("profile");
        new Scope(NotificationCompat.CATEGORY_EMAIL);
        new C2016a("SignIn.INTERNAL_API", f7397d, f7395b);
    }
}
