package p119e.p144d.p145a.p146a.p147i;

import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3878c;
import p119e.p144d.p145a.p146a.C3880e;
import p119e.p144d.p145a.p146a.p147i.C3887b;

/* renamed from: e.d.a.a.i.k */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
abstract class C3903k {

    /* renamed from: e.d.a.a.i.k$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public static abstract class C3904a {
        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract C3904a mo23525a(C3877b bVar);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract C3904a mo23526a(C3878c<?> cVar);

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public abstract C3904a mo23527a(C3880e<?, byte[]> eVar);

        /* renamed from: a */
        public abstract C3904a mo23528a(C3905l lVar);

        /* renamed from: a */
        public abstract C3904a mo23529a(String str);

        /* renamed from: a */
        public abstract C3903k mo23530a();
    }

    C3903k() {
    }

    /* renamed from: g */
    public static C3904a m11750g() {
        return new C3887b.C3889b();
    }

    /* renamed from: a */
    public abstract C3877b mo23517a();

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public abstract C3878c<?> mo23518b();

    /* renamed from: c */
    public byte[] mo23557c() {
        return mo23519d().apply(mo23518b().mo23490b());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public abstract C3880e<?, byte[]> mo23519d();

    /* renamed from: e */
    public abstract C3905l mo23520e();

    /* renamed from: f */
    public abstract String mo23522f();
}
