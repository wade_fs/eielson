package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;

/* renamed from: e.d.a.c.e.z */
interface C4086z<TResult> {
    /* renamed from: a */
    void mo23726a(@NonNull C4065h<TResult> hVar);

    void cancel();
}
