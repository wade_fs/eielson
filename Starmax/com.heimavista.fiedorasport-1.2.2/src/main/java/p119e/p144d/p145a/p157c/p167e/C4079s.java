package p119e.p144d.p145a.p157c.p167e;

/* renamed from: e.d.a.c.e.s */
final class C4079s implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4065h f7433P;

    /* renamed from: Q */
    private final /* synthetic */ C4078r f7434Q;

    C4079s(C4078r rVar, C4065h hVar) {
        this.f7434Q = rVar;
        this.f7433P = hVar;
    }

    public final void run() {
        synchronized (this.f7434Q.f7431b) {
            if (this.f7434Q.f7432c != null) {
                this.f7434Q.f7432c.mo16766a(this.f7433P);
            }
        }
    }
}
