package p119e.p144d.p145a.p146a.p147i.p148t;

import android.util.Log;

/* renamed from: e.d.a.a.i.t.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3915a {
    /* renamed from: a */
    private static String m11790a(String str) {
        return "TransportRuntime." + str;
    }

    /* renamed from: b */
    public static void m11795b(String str, String str2, Object obj) {
        Log.w(m11790a(str), String.format(str2, obj));
    }

    /* renamed from: a */
    public static void m11792a(String str, String str2, Object obj) {
        Log.d(m11790a(str), String.format(str2, obj));
    }

    /* renamed from: a */
    public static void m11794a(String str, String str2, Object... objArr) {
        Log.d(m11790a(str), String.format(str2, objArr));
    }

    /* renamed from: a */
    public static void m11791a(String str, String str2) {
        Log.i(m11790a(str), str2);
    }

    /* renamed from: a */
    public static void m11793a(String str, String str2, Throwable th) {
        Log.e(m11790a(str), str2, th);
    }
}
