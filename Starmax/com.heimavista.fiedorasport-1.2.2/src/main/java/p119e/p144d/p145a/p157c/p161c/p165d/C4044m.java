package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: e.d.a.c.c.d.m */
public abstract class C4044m extends C4038g implements C4043l {
    /* renamed from: a */
    public static C4043l m12058a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IMarkerDelegate");
        if (queryLocalInterface instanceof C4043l) {
            return (C4043l) queryLocalInterface;
        }
        return new C4045n(iBinder);
    }
}
