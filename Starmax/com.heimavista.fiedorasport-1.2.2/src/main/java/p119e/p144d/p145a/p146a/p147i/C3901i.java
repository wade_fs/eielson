package p119e.p144d.p145a.p146a.p147i;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/* renamed from: e.d.a.a.i.i */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
abstract class C3901i {
    /* renamed from: a */
    static Executor m11747a() {
        return Executors.newSingleThreadExecutor();
    }
}
