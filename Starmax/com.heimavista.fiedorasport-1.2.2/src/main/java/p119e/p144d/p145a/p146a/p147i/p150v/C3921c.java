package p119e.p144d.p145a.p146a.p147i.p150v;

import com.google.android.datatransport.runtime.backends.C1713e;
import com.google.android.datatransport.runtime.backends.C1724m;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1749s;
import java.util.concurrent.Executor;
import java.util.logging.Logger;
import p119e.p144d.p145a.p146a.C3883h;
import p119e.p144d.p145a.p146a.p147i.C3899h;
import p119e.p144d.p145a.p146a.p147i.C3905l;
import p119e.p144d.p145a.p146a.p147i.C3911q;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p152w.C3969b;

/* renamed from: e.d.a.a.i.v.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public class C3921c implements C3923e {

    /* renamed from: f */
    private static final Logger f7249f = Logger.getLogger(C3911q.class.getName());

    /* renamed from: a */
    private final C1749s f7250a;

    /* renamed from: b */
    private final Executor f7251b;

    /* renamed from: c */
    private final C1713e f7252c;

    /* renamed from: d */
    private final C3934c f7253d;

    /* renamed from: e */
    private final C3969b f7254e;

    public C3921c(Executor executor, C1713e eVar, C1749s sVar, C3934c cVar, C3969b bVar) {
        this.f7251b = executor;
        this.f7252c = eVar;
        this.f7250a = sVar;
        this.f7253d = cVar;
        this.f7254e = bVar;
    }

    /* renamed from: a */
    public void mo23566a(C3905l lVar, C3899h hVar, C3883h hVar2) {
        this.f7251b.execute(C3919a.m11798a(this, lVar, hVar2, hVar));
    }

    /* renamed from: a */
    static /* synthetic */ void m11802a(C3921c cVar, C3905l lVar, C3883h hVar, C3899h hVar2) {
        try {
            C1724m mVar = cVar.f7252c.get(lVar.mo23531a());
            if (mVar == null) {
                String format = String.format("Transport backend '%s' is not registered", lVar.mo23531a());
                f7249f.warning(format);
                hVar.mo23499a(new IllegalArgumentException(format));
                return;
            }
            cVar.f7254e.mo23602a(C3920b.m11799a(cVar, lVar, mVar.mo13531a(hVar2)));
            hVar.mo23499a(null);
        } catch (Exception e) {
            Logger logger = f7249f;
            logger.warning("Error scheduling event " + e.getMessage());
            hVar.mo23499a(e);
        }
    }

    /* renamed from: a */
    static /* synthetic */ Object m11801a(C3921c cVar, C3905l lVar, C3899h hVar) {
        cVar.f7253d.mo23586a(lVar, hVar);
        cVar.f7250a.mo13561a(lVar, 1);
        return null;
    }
}
