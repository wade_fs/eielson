package p119e.p144d.p145a.p157c.p167e;

import android.app.Activity;
import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.common.api.internal.C2080h;
import com.google.android.gms.common.api.internal.LifecycleCallback;
import com.google.android.gms.common.internal.C2258v;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.c0 */
final class C4058c0<TResult> extends C4065h<TResult> {

    /* renamed from: a */
    private final Object f7402a = new Object();

    /* renamed from: b */
    private final C4054a0<TResult> f7403b = new C4054a0<>();

    /* renamed from: c */
    private boolean f7404c;

    /* renamed from: d */
    private volatile boolean f7405d;

    /* renamed from: e */
    private TResult f7406e;

    /* renamed from: f */
    private Exception f7407f;

    /* renamed from: e.d.a.c.e.c0$a */
    private static class C4059a extends LifecycleCallback {

        /* renamed from: Q */
        private final List<WeakReference<C4086z<?>>> f7408Q = new ArrayList();

        private C4059a(C2080h hVar) {
            super(hVar);
            super.f3239P.mo16706a("TaskOnStopCallback", super);
        }

        /* renamed from: b */
        public static C4059a m12112b(Activity activity) {
            C2080h a = LifecycleCallback.m4742a(activity);
            C4059a aVar = (C4059a) a.mo16705a("TaskOnStopCallback", C4059a.class);
            return aVar == null ? new C4059a(a) : aVar;
        }

        /* renamed from: a */
        public final <T> void mo23716a(C4086z<T> zVar) {
            synchronized (this.f7408Q) {
                this.f7408Q.add(new WeakReference(zVar));
            }
        }

        @MainThread
        /* renamed from: e */
        public void mo16611e() {
            synchronized (this.f7408Q) {
                for (WeakReference<C4086z<?>> weakReference : this.f7408Q) {
                    C4086z zVar = (C4086z) weakReference.get();
                    if (zVar != null) {
                        zVar.cancel();
                    }
                }
                this.f7408Q.clear();
            }
        }
    }

    C4058c0() {
    }

    /* renamed from: g */
    private final void m12086g() {
        C2258v.m5641b(this.f7404c, "Task is not yet complete");
    }

    /* renamed from: h */
    private final void m12087h() {
        C2258v.m5641b(!this.f7404c, "Task is already complete");
    }

    /* renamed from: i */
    private final void m12088i() {
        if (this.f7405d) {
            throw new CancellationException("Task is already canceled.");
        }
    }

    /* renamed from: j */
    private final void m12089j() {
        synchronized (this.f7402a) {
            if (this.f7404c) {
                this.f7403b.mo23690a(super);
            }
        }
    }

    /* renamed from: a */
    public final <X extends Throwable> TResult mo23705a(@NonNull Class cls) {
        TResult tresult;
        synchronized (this.f7402a) {
            m12086g();
            m12088i();
            if (cls.isInstance(this.f7407f)) {
                throw ((Throwable) cls.cast(this.f7407f));
            } else if (this.f7407f == null) {
                tresult = this.f7406e;
            } else {
                throw new C4063f(this.f7407f);
            }
        }
        return tresult;
    }

    /* renamed from: b */
    public final TResult mo23709b() {
        TResult tresult;
        synchronized (this.f7402a) {
            m12086g();
            m12088i();
            if (this.f7407f == null) {
                tresult = this.f7406e;
            } else {
                throw new C4063f(this.f7407f);
            }
        }
        return tresult;
    }

    /* renamed from: c */
    public final boolean mo23712c() {
        return this.f7405d;
    }

    /* renamed from: d */
    public final boolean mo23713d() {
        boolean z;
        synchronized (this.f7402a) {
            z = this.f7404c;
        }
        return z;
    }

    /* renamed from: e */
    public final boolean mo23714e() {
        boolean z;
        synchronized (this.f7402a) {
            z = this.f7404c && !this.f7405d && this.f7407f == null;
        }
        return z;
    }

    /* renamed from: f */
    public final boolean mo23715f() {
        synchronized (this.f7402a) {
            if (this.f7404c) {
                return false;
            }
            this.f7404c = true;
            this.f7405d = true;
            this.f7403b.mo23690a(super);
            return true;
        }
    }

    @NonNull
    /* renamed from: b */
    public final <TContinuationResult> C4065h<TContinuationResult> mo23708b(@NonNull Executor executor, @NonNull C4053a<TResult, C4065h<TContinuationResult>> aVar) {
        C4058c0 c0Var = new C4058c0();
        this.f7403b.mo23691a(new C4074n(executor, aVar, c0Var));
        m12089j();
        return super;
    }

    @Nullable
    /* renamed from: a */
    public final Exception mo23704a() {
        Exception exc;
        synchronized (this.f7402a) {
            exc = this.f7407f;
        }
        return exc;
    }

    /* renamed from: b */
    public final boolean mo23711b(Object obj) {
        synchronized (this.f7402a) {
            if (this.f7404c) {
                return false;
            }
            this.f7404c = true;
            this.f7406e = obj;
            this.f7403b.mo23690a(super);
            return true;
        }
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23697a(@NonNull C4062e eVar) {
        mo23702a(C4067j.f7412a, eVar);
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23702a(@NonNull Executor executor, @NonNull C4062e<? super TResult> eVar) {
        this.f7403b.mo23691a(new C4082v(executor, eVar));
        m12089j();
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23696a(@NonNull C4060d dVar) {
        mo23701a(C4067j.f7412a, dVar);
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23701a(@NonNull Executor executor, @NonNull C4060d dVar) {
        this.f7403b.mo23691a(new C4080t(executor, dVar));
        m12089j();
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23700a(@NonNull Executor executor, @NonNull C4057c<TResult> cVar) {
        this.f7403b.mo23691a(new C4078r(executor, cVar));
        m12089j();
        return super;
    }

    /* renamed from: b */
    public final boolean mo23710b(@NonNull Exception exc) {
        C2258v.m5630a(exc, "Exception must not be null");
        synchronized (this.f7402a) {
            if (this.f7404c) {
                return false;
            }
            this.f7404c = true;
            this.f7407f = exc;
            this.f7403b.mo23690a(super);
            return true;
        }
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23694a(@NonNull Activity activity, @NonNull C4057c<TResult> cVar) {
        C4078r rVar = new C4078r(C4067j.f7412a, cVar);
        this.f7403b.mo23691a(rVar);
        C4059a.m12112b(activity).mo23716a(rVar);
        m12089j();
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final <TContinuationResult> C4065h<TContinuationResult> mo23695a(@NonNull C4053a aVar) {
        return mo23698a(C4067j.f7412a, aVar);
    }

    @NonNull
    /* renamed from: a */
    public final <TContinuationResult> C4065h<TContinuationResult> mo23698a(@NonNull Executor executor, @NonNull C4053a<TResult, TContinuationResult> aVar) {
        C4058c0 c0Var = new C4058c0();
        this.f7403b.mo23691a(new C4072l(executor, aVar, c0Var));
        m12089j();
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final C4065h<TResult> mo23699a(@NonNull Executor executor, @NonNull C4055b bVar) {
        this.f7403b.mo23691a(new C4076p(executor, bVar));
        m12089j();
        return super;
    }

    @NonNull
    /* renamed from: a */
    public final <TContinuationResult> C4065h<TContinuationResult> mo23703a(Executor executor, C4064g<TResult, TContinuationResult> gVar) {
        C4058c0 c0Var = new C4058c0();
        this.f7403b.mo23691a(new C4084x(executor, gVar, c0Var));
        m12089j();
        return super;
    }

    /* renamed from: a */
    public final void mo23707a(Object obj) {
        synchronized (this.f7402a) {
            m12087h();
            this.f7404c = true;
            this.f7406e = obj;
        }
        this.f7403b.mo23690a(super);
    }

    /* renamed from: a */
    public final void mo23706a(@NonNull Exception exc) {
        C2258v.m5630a(exc, "Exception must not be null");
        synchronized (this.f7402a) {
            m12087h();
            this.f7404c = true;
            this.f7407f = exc;
        }
        this.f7403b.mo23690a(super);
    }
}
