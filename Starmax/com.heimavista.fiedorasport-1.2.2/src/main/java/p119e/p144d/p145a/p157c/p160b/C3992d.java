package p119e.p144d.p145a.p157c.p160b;

import android.os.IBinder;
import java.lang.reflect.Field;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: e.d.a.c.b.d */
public final class C3992d<T> extends C3988b.C3989a {

    /* renamed from: a */
    private final T f7350a;

    private C3992d(T t) {
        this.f7350a = t;
    }

    /* renamed from: a */
    public static <T> C3988b m11990a(Object obj) {
        return new C3992d(obj);
    }

    /* renamed from: e */
    public static <T> T m11991e(C3988b bVar) {
        if (bVar instanceof C3992d) {
            return ((C3992d) bVar).f7350a;
        }
        IBinder asBinder = bVar.asBinder();
        Field[] declaredFields = asBinder.getClass().getDeclaredFields();
        Field field = null;
        int i = 0;
        for (Field field2 : declaredFields) {
            if (!field2.isSynthetic()) {
                i++;
                field = field2;
            }
        }
        if (i != 1) {
            int length = declaredFields.length;
            StringBuilder sb = new StringBuilder(64);
            sb.append("Unexpected number of IObjectWrapper declared fields: ");
            sb.append(length);
            throw new IllegalArgumentException(sb.toString());
        } else if (!field.isAccessible()) {
            field.setAccessible(true);
            try {
                return field.get(asBinder);
            } catch (NullPointerException e) {
                throw new IllegalArgumentException("Binder object is null.", e);
            } catch (IllegalAccessException e2) {
                throw new IllegalArgumentException("Could not access the field in remoteBinder.", e2);
            }
        } else {
            throw new IllegalArgumentException("IObjectWrapper declared field not private!");
        }
    }
}
