package p119e.p144d.p145a.p146a.p147i.p153x;

import p224f.p225b.C4936d;
import p224f.p225b.Factory;

/* renamed from: e.d.a.a.i.x.d */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3974d implements Factory<C3971a> {

    /* renamed from: a */
    private static final C3974d f7326a = new C3974d();

    /* renamed from: a */
    public static C3974d m11935a() {
        return f7326a;
    }

    /* renamed from: b */
    public static C3971a m11936b() {
        C3971a b = C3972b.m11932b();
        C4936d.m17213a(b, "Cannot return null from a non-@Nullable @Provides method");
        return b;
    }

    public C3971a get() {
        return m11936b();
    }
}
