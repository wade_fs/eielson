package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: e.d.a.c.c.d.b */
public abstract class C4033b extends C4038g implements C4046o {
    /* renamed from: a */
    public static C4046o m12037a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.IPolylineDelegate");
        if (queryLocalInterface instanceof C4046o) {
            return (C4046o) queryLocalInterface;
        }
        return new C4034c(iBinder);
    }
}
