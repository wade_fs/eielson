package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.v */
final class C4082v<TResult> implements C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7440a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Object f7441b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C4062e<? super TResult> f7442c;

    public C4082v(@NonNull Executor executor, @NonNull C4062e<? super TResult> eVar) {
        this.f7440a = executor;
        this.f7442c = eVar;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h<TResult> hVar) {
        if (hVar.mo23714e()) {
            synchronized (this.f7441b) {
                if (this.f7442c != null) {
                    this.f7440a.execute(new C4083w(this, hVar));
                }
            }
        }
    }

    public final void cancel() {
        synchronized (this.f7441b) {
            this.f7442c = null;
        }
    }
}
