package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.l */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3949l implements C3963z.C3965b {

    /* renamed from: a */
    private final long f7300a;

    private C3949l(long j) {
        this.f7300a = j;
    }

    /* renamed from: a */
    public static C3963z.C3965b m11870a(long j) {
        return new C3949l(j);
    }

    public Object apply(Object obj) {
        return Integer.valueOf(((SQLiteDatabase) obj).delete("events", "timestamp_ms < ?", new String[]{String.valueOf(this.f7300a)}));
    }
}
