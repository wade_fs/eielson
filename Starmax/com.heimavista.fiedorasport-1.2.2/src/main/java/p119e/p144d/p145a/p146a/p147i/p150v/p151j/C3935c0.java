package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import android.database.sqlite.SQLiteDatabase;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3940e0;

/* renamed from: e.d.a.a.i.v.j.c0 */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3935c0 implements C3940e0.C3941a {

    /* renamed from: a */
    private static final C3935c0 f7281a = new C3935c0();

    private C3935c0() {
    }

    /* renamed from: a */
    public static C3940e0.C3941a m11836a() {
        return f7281a;
    }

    /* renamed from: a */
    public void mo23585a(SQLiteDatabase sQLiteDatabase) {
        C3940e0.m11855c(sQLiteDatabase);
    }
}
