package p119e.p144d.p145a.p146a.p147i;

import java.util.concurrent.Executor;
import p224f.p225b.C4936d;
import p224f.p225b.Factory;

/* renamed from: e.d.a.a.i.j */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3902j implements Factory<Executor> {

    /* renamed from: a */
    private static final C3902j f7222a = new C3902j();

    /* renamed from: a */
    public static C3902j m11748a() {
        return f7222a;
    }

    /* renamed from: b */
    public static Executor m11749b() {
        Executor a = C3901i.m11747a();
        C4936d.m17213a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }

    public Executor get() {
        return m11749b();
    }
}
