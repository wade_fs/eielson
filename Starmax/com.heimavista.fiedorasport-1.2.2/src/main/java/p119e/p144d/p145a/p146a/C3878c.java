package p119e.p144d.p145a.p146a;

import androidx.annotation.Nullable;

/* renamed from: e.d.a.a.c */
/* compiled from: com.google.android.datatransport:transport-api@@2.2.0 */
public abstract class C3878c<T> {
    /* renamed from: a */
    public static <T> C3878c<T> m11675a(T t) {
        return new C3876a(null, t, C3879d.VERY_LOW);
    }

    @Nullable
    /* renamed from: a */
    public abstract Integer mo23489a();

    /* renamed from: b */
    public abstract T mo23490b();

    /* renamed from: c */
    public abstract C3879d mo23491c();
}
