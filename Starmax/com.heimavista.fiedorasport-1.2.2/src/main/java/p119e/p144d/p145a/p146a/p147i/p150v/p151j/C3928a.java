package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3936d;

/* renamed from: e.d.a.a.i.v.j.a */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3928a extends C3936d {

    /* renamed from: b */
    private final long f7265b;

    /* renamed from: c */
    private final int f7266c;

    /* renamed from: d */
    private final int f7267d;

    /* renamed from: e */
    private final long f7268e;

    /* renamed from: e.d.a.a.i.v.j.a$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static final class C3930b extends C3936d.C3937a {

        /* renamed from: a */
        private Long f7269a;

        /* renamed from: b */
        private Integer f7270b;

        /* renamed from: c */
        private Integer f7271c;

        /* renamed from: d */
        private Long f7272d;

        C3930b() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3936d.C3937a mo23574a(int i) {
            this.f7271c = Integer.valueOf(i);
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public C3936d.C3937a mo23578b(long j) {
            this.f7269a = Long.valueOf(j);
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3936d.C3937a mo23575a(long j) {
            this.f7272d = Long.valueOf(j);
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public C3936d.C3937a mo23577b(int i) {
            this.f7270b = Integer.valueOf(i);
            return super;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3936d mo23576a() {
            String str = "";
            if (this.f7269a == null) {
                str = str + " maxStorageSizeInBytes";
            }
            if (this.f7270b == null) {
                str = str + " loadBatchSize";
            }
            if (this.f7271c == null) {
                str = str + " criticalSectionEnterTimeoutMs";
            }
            if (this.f7272d == null) {
                str = str + " eventCleanUpAge";
            }
            if (str.isEmpty()) {
                return new C3928a(this.f7269a.longValue(), this.f7270b.intValue(), this.f7271c.intValue(), this.f7272d.longValue());
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo23567a() {
        return this.f7267d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public long mo23568b() {
        return this.f7268e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo23569c() {
        return this.f7266c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public long mo23570d() {
        return this.f7265b;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3936d)) {
            return false;
        }
        C3936d dVar = (C3936d) obj;
        if (this.f7265b == super.mo23570d() && this.f7266c == super.mo23569c() && this.f7267d == super.mo23567a() && this.f7268e == super.mo23568b()) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long j = this.f7265b;
        long j2 = this.f7268e;
        return ((int) ((j2 >>> 32) ^ j2)) ^ ((((((((int) (j ^ (j >>> 32))) ^ 1000003) * 1000003) ^ this.f7266c) * 1000003) ^ this.f7267d) * 1000003);
    }

    public String toString() {
        return "EventStoreConfig{maxStorageSizeInBytes=" + this.f7265b + ", loadBatchSize=" + this.f7266c + ", criticalSectionEnterTimeoutMs=" + this.f7267d + ", eventCleanUpAge=" + this.f7268e + "}";
    }

    private C3928a(long j, int i, int i2, long j2) {
        this.f7265b = j;
        this.f7266c = i;
        this.f7267d = i2;
        this.f7268e = j2;
    }
}
