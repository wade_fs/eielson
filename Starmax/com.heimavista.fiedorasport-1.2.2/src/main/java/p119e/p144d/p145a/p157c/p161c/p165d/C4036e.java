package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IBinder;
import android.os.IInterface;

/* renamed from: e.d.a.c.c.d.e */
public abstract class C4036e extends C4038g implements C4035d {
    /* renamed from: a */
    public static C4035d m12040a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.maps.model.internal.ITileProviderDelegate");
        if (queryLocalInterface instanceof C4035d) {
            return (C4035d) queryLocalInterface;
        }
        return new C4037f(iBinder);
    }
}
