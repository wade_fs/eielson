package p119e.p144d.p145a.p157c.p161c.p164c;

import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.Deque;

/* renamed from: e.d.a.c.c.c.i */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
public final class C4021i {
    static {
        new C4020h();
    }

    /* renamed from: a */
    private static byte[] m12024a(Deque<byte[]> deque, int i) {
        byte[] bArr = new byte[i];
        int i2 = i;
        while (i2 > 0) {
            byte[] removeFirst = deque.removeFirst();
            int min = Math.min(i2, removeFirst.length);
            System.arraycopy(removeFirst, 0, bArr, i - i2, min);
            i2 -= min;
        }
        return bArr;
    }

    /* renamed from: a */
    public static byte[] m12023a(InputStream inputStream) {
        int i;
        C4019g.m12021a(inputStream);
        ArrayDeque arrayDeque = new ArrayDeque(20);
        int i2 = 8192;
        for (int i3 = 0; i3 < 2147483639; i3 = i) {
            byte[] bArr = new byte[Math.min(i2, 2147483639 - i3)];
            arrayDeque.add(bArr);
            i = i3;
            int i4 = 0;
            while (i4 < bArr.length) {
                int read = inputStream.read(bArr, i4, bArr.length - i4);
                if (read == -1) {
                    return m12024a(arrayDeque, i);
                }
                i4 += read;
                i += read;
            }
            i2 = C4026m.m12029a(i2, 2);
        }
        if (inputStream.read() == -1) {
            return m12024a(arrayDeque, 2147483639);
        }
        throw new OutOfMemoryError("input is too large to fit in a byte array");
    }

    /* renamed from: a */
    public static InputStream m12022a(InputStream inputStream, long j) {
        return new C4023k(inputStream, 1048577);
    }
}
