package p119e.p144d.p145a.p157c.p158a.p159a;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.google.android.gms.common.C2176f;

/* renamed from: e.d.a.c.a.a.c */
public final class C3984c {

    /* renamed from: a */
    private SharedPreferences f7345a;

    public C3984c(Context context) {
        try {
            Context c = C2176f.m5302c(context);
            this.f7345a = c == null ? null : c.getSharedPreferences("google_ads_flags", 0);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while getting SharedPreferences ", th);
            this.f7345a = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final float mo23615a(String str, float f) {
        try {
            if (this.f7345a == null) {
                return 0.0f;
            }
            return this.f7345a.getFloat(str, 0.0f);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return 0.0f;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final String mo23616a(String str, String str2) {
        try {
            return this.f7345a == null ? str2 : this.f7345a.getString(str, str2);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return str2;
        }
    }

    /* renamed from: a */
    public final boolean mo23617a(String str, boolean z) {
        try {
            if (this.f7345a == null) {
                return false;
            }
            return this.f7345a.getBoolean(str, false);
        } catch (Throwable th) {
            Log.w("GmscoreFlag", "Error while reading from SharedPreferences ", th);
            return false;
        }
    }
}
