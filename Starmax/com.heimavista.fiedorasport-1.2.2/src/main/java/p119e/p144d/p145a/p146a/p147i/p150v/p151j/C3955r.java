package p119e.p144d.p145a.p146a.p147i.p150v.p151j;

import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3963z;

/* renamed from: e.d.a.a.i.v.j.r */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final /* synthetic */ class C3955r implements C3963z.C3967d {

    /* renamed from: a */
    private final C3940e0 f7307a;

    private C3955r(C3940e0 e0Var) {
        this.f7307a = e0Var;
    }

    /* renamed from: a */
    public static C3963z.C3967d m11877a(C3940e0 e0Var) {
        return new C3955r(e0Var);
    }

    /* renamed from: a */
    public Object mo23601a() {
        return this.f7307a.getWritableDatabase();
    }
}
