package p119e.p144d.p145a.p157c.p161c.p163b;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;

/* renamed from: e.d.a.c.c.b.a */
public class C4009a implements IInterface {

    /* renamed from: a */
    private final IBinder f7371a;

    /* renamed from: b */
    private final String f7372b;

    protected C4009a(IBinder iBinder, String str) {
        this.f7371a = iBinder;
        this.f7372b = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo23641a() {
        Parcel obtain = Parcel.obtain();
        obtain.writeInterfaceToken(this.f7372b);
        return obtain;
    }

    public IBinder asBinder() {
        return this.f7371a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final Parcel mo23642a(int i, Parcel parcel) {
        parcel = Parcel.obtain();
        try {
            this.f7371a.transact(i, parcel, parcel, 0);
            parcel.readException();
            return parcel;
        } catch (RuntimeException e) {
            throw e;
        } finally {
            parcel.recycle();
        }
    }
}
