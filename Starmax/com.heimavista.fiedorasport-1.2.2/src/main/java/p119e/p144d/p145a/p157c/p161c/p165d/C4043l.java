package p119e.p144d.p145a.p157c.p161c.p165d;

import android.os.IInterface;
import com.google.android.gms.maps.model.LatLng;
import p119e.p144d.p145a.p157c.p160b.C3988b;

/* renamed from: e.d.a.c.c.d.l */
public interface C4043l extends IInterface {
    /* renamed from: a */
    void mo23675a(float f);

    /* renamed from: a */
    void mo23676a(C3988b bVar);

    /* renamed from: b */
    void mo23677b(LatLng latLng);

    /* renamed from: b */
    boolean mo23678b(C4043l lVar);

    LatLng getPosition();

    /* renamed from: k */
    int mo23680k();

    /* renamed from: m */
    C3988b mo23681m();
}
