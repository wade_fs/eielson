package p119e.p144d.p145a.p146a;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/* renamed from: e.d.a.a.b */
/* compiled from: com.google.android.datatransport:transport-api@@2.2.0 */
public final class C3877b {

    /* renamed from: a */
    private final String f7174a;

    private C3877b(@NonNull String str) {
        if (str != null) {
            this.f7174a = str;
            return;
        }
        throw new NullPointerException("name is null");
    }

    /* renamed from: a */
    public static C3877b m11673a(@NonNull String str) {
        return new C3877b(str);
    }

    public boolean equals(@Nullable Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof C3877b)) {
            return false;
        }
        return this.f7174a.equals(((C3877b) obj).f7174a);
    }

    public int hashCode() {
        return this.f7174a.hashCode() ^ 1000003;
    }

    @NonNull
    public String toString() {
        return "Encoding{name=\"" + this.f7174a + "\"}";
    }

    /* renamed from: a */
    public String mo23495a() {
        return this.f7174a;
    }
}
