package p119e.p144d.p145a.p157c.p161c.p164c;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/* renamed from: e.d.a.c.c.c.b */
/* compiled from: com.google.firebase:firebase-iid@@20.0.2 */
public interface C4014b {
    /* renamed from: a */
    ExecutorService mo23647a(ThreadFactory threadFactory, int i);

    /* renamed from: a */
    ScheduledExecutorService mo23648a(int i, ThreadFactory threadFactory, int i2);
}
