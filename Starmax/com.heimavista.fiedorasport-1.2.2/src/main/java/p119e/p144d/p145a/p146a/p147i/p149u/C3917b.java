package p119e.p144d.p145a.p146a.p147i.p149u;

/* renamed from: e.d.a.a.i.u.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3917b {
    /* renamed from: a */
    public static <TInput, TResult, TException extends Throwable> TResult m11796a(int i, TInput tinput, C3916a<TInput, TResult, TException> aVar, C3918c<TInput, TResult> cVar) {
        TResult apply;
        if (i < 1) {
            return aVar.apply(tinput);
        }
        do {
            apply = aVar.apply(tinput);
            tinput = cVar.mo13529a(tinput, apply);
            if (tinput == null) {
                break;
            }
            i--;
        } while (i >= 1);
        return apply;
    }
}
