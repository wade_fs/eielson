package p119e.p144d.p145a.p157c.p167e;

import java.util.concurrent.CancellationException;

/* renamed from: e.d.a.c.e.y */
final class C4085y implements Runnable {

    /* renamed from: P */
    private final /* synthetic */ C4065h f7448P;

    /* renamed from: Q */
    private final /* synthetic */ C4084x f7449Q;

    C4085y(C4084x xVar, C4065h hVar) {
        this.f7449Q = xVar;
        this.f7448P = hVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult>
     arg types: [java.util.concurrent.Executor, e.d.a.c.e.x]
     candidates:
      e.d.a.c.e.h.a(android.app.Activity, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.a):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.e<? super ?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.g):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult>
     arg types: [java.util.concurrent.Executor, e.d.a.c.e.x]
     candidates:
      e.d.a.c.e.h.a(android.app.Activity, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.a):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.c<?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.d):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.e<? super ?>):e.d.a.c.e.h<TResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.g):e.d.a.c.e.h<TContinuationResult>
      e.d.a.c.e.h.a(java.util.concurrent.Executor, e.d.a.c.e.b):e.d.a.c.e.h<TResult> */
    public final void run() {
        try {
            C4065h a = this.f7449Q.f7446b.mo22259a(this.f7448P.mo23709b());
            if (a == null) {
                this.f7449Q.mo22541a(new NullPointerException("Continuation returned null"));
                return;
            }
            a.mo23702a(C4067j.f7413b, (C4062e<? super Object>) this.f7449Q);
            a.mo23701a(C4067j.f7413b, (C4060d) this.f7449Q);
            a.mo23699a(C4067j.f7413b, (C4055b) this.f7449Q);
        } catch (C4063f e) {
            if (e.getCause() instanceof Exception) {
                this.f7449Q.mo22541a((Exception) e.getCause());
            } else {
                this.f7449Q.mo22541a(e);
            }
        } catch (CancellationException unused) {
            this.f7449Q.mo23692a();
        } catch (Exception e2) {
            this.f7449Q.mo22541a(e2);
        }
    }
}
