package p119e.p144d.p145a.p157c.p167e;

import android.os.Handler;
import android.os.Looper;
import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.j */
public final class C4067j {

    /* renamed from: a */
    public static final Executor f7412a = new C4068a();

    /* renamed from: b */
    static final Executor f7413b = new C4056b0();

    /* renamed from: e.d.a.c.e.j$a */
    private static final class C4068a implements Executor {

        /* renamed from: a */
        private final Handler f7414a = new Handler(Looper.getMainLooper());

        public final void execute(@NonNull Runnable runnable) {
            this.f7414a.post(runnable);
        }
    }
}
