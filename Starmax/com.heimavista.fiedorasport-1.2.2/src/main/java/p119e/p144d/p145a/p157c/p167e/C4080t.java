package p119e.p144d.p145a.p157c.p167e;

import androidx.annotation.NonNull;
import java.util.concurrent.Executor;

/* renamed from: e.d.a.c.e.t */
final class C4080t<TResult> implements C4086z<TResult> {

    /* renamed from: a */
    private final Executor f7435a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Object f7436b = new Object();
    /* access modifiers changed from: private */

    /* renamed from: c */
    public C4060d f7437c;

    public C4080t(@NonNull Executor executor, @NonNull C4060d dVar) {
        this.f7435a = executor;
        this.f7437c = dVar;
    }

    /* renamed from: a */
    public final void mo23726a(@NonNull C4065h<TResult> hVar) {
        if (!hVar.mo23714e() && !hVar.mo23712c()) {
            synchronized (this.f7436b) {
                if (this.f7437c != null) {
                    this.f7435a.execute(new C4081u(this, hVar));
                }
            }
        }
    }

    public final void cancel() {
        synchronized (this.f7436b) {
            this.f7437c = null;
        }
    }
}
