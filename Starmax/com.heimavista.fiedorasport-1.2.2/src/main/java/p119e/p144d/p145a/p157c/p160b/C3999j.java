package p119e.p144d.p145a.p157c.p160b;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import p119e.p144d.p145a.p157c.p160b.C3986a;

/* renamed from: e.d.a.c.b.j */
final class C3999j implements C3986a.C3987a {

    /* renamed from: a */
    private final /* synthetic */ FrameLayout f7360a;

    /* renamed from: b */
    private final /* synthetic */ LayoutInflater f7361b;

    /* renamed from: c */
    private final /* synthetic */ ViewGroup f7362c;

    /* renamed from: d */
    private final /* synthetic */ Bundle f7363d;

    /* renamed from: e */
    private final /* synthetic */ C3986a f7364e;

    C3999j(C3986a aVar, FrameLayout frameLayout, LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f7364e = aVar;
        this.f7360a = frameLayout;
        this.f7361b = layoutInflater;
        this.f7362c = viewGroup;
        this.f7363d = bundle;
    }

    /* renamed from: a */
    public final void mo23631a(C3991c cVar) {
        this.f7360a.removeAllViews();
        this.f7360a.addView(this.f7364e.f7346a.mo18349a(this.f7361b, this.f7362c, this.f7363d));
    }

    public final int getState() {
        return 2;
    }
}
