package p119e.p144d.p145a.p146a.p147i.p150v;

import android.content.Context;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1733g;
import com.google.android.datatransport.runtime.scheduling.jobscheduling.C1749s;
import p119e.p144d.p145a.p146a.p147i.p150v.p151j.C3934c;
import p119e.p144d.p145a.p146a.p147i.p153x.C3971a;
import p224f.p225b.C4936d;
import p224f.p225b.Factory;
import p226g.p227a.Provider;

/* renamed from: e.d.a.a.i.v.i */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public final class C3927i implements Factory<C1749s> {

    /* renamed from: a */
    private final Provider<Context> f7261a;

    /* renamed from: b */
    private final Provider<C3934c> f7262b;

    /* renamed from: c */
    private final Provider<C1733g> f7263c;

    /* renamed from: d */
    private final Provider<C3971a> f7264d;

    public C3927i(Provider<Context> aVar, Provider<C3934c> aVar2, Provider<C1733g> aVar3, Provider<C3971a> aVar4) {
        this.f7261a = aVar;
        this.f7262b = aVar2;
        this.f7263c = aVar3;
        this.f7264d = aVar4;
    }

    /* renamed from: a */
    public static C3927i m11811a(Provider<Context> aVar, Provider<C3934c> aVar2, Provider<C1733g> aVar3, Provider<C3971a> aVar4) {
        return new C3927i(aVar, aVar2, aVar3, aVar4);
    }

    /* renamed from: a */
    public static C1749s m11810a(Context context, C3934c cVar, C1733g gVar, C3971a aVar) {
        C1749s a = C3926h.m11809a(context, cVar, gVar, aVar);
        C4936d.m17213a(a, "Cannot return null from a non-@Nullable @Provides method");
        return a;
    }

    public C1749s get() {
        return m11810a(this.f7261a.get(), this.f7262b.get(), this.f7263c.get(), this.f7264d.get());
    }
}
