package p119e.p144d.p145a.p146a.p147i;

import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import java.util.Arrays;
import p119e.p144d.p145a.p146a.C3879d;
import p119e.p144d.p145a.p146a.p147i.C3905l;

/* renamed from: e.d.a.a.i.c */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3890c extends C3905l {

    /* renamed from: a */
    private final String f7201a;

    /* renamed from: b */
    private final byte[] f7202b;

    /* renamed from: c */
    private final C3879d f7203c;

    /* renamed from: a */
    public String mo23531a() {
        return this.f7201a;
    }

    @Nullable
    /* renamed from: b */
    public byte[] mo23532b() {
        return this.f7202b;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    /* renamed from: c */
    public C3879d mo23533c() {
        return this.f7203c;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3905l)) {
            return false;
        }
        C3905l lVar = (C3905l) obj;
        if (this.f7201a.equals(super.mo23531a())) {
            if (!Arrays.equals(this.f7202b, lVar instanceof C3890c ? ((C3890c) lVar).f7202b : super.mo23532b()) || !this.f7203c.equals(super.mo23533c())) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        return ((((this.f7201a.hashCode() ^ 1000003) * 1000003) ^ Arrays.hashCode(this.f7202b)) * 1000003) ^ this.f7203c.hashCode();
    }

    /* renamed from: e.d.a.a.i.c$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static final class C3892b extends C3905l.C3906a {

        /* renamed from: a */
        private String f7204a;

        /* renamed from: b */
        private byte[] f7205b;

        /* renamed from: c */
        private C3879d f7206c;

        C3892b() {
        }

        /* renamed from: a */
        public C3905l.C3906a mo23537a(String str) {
            if (str != null) {
                this.f7204a = str;
                return super;
            }
            throw new NullPointerException("Null backendName");
        }

        /* renamed from: a */
        public C3905l.C3906a mo23538a(@Nullable byte[] bArr) {
            this.f7205b = bArr;
            return super;
        }

        /* renamed from: a */
        public C3905l.C3906a mo23536a(C3879d dVar) {
            if (dVar != null) {
                this.f7206c = dVar;
                return super;
            }
            throw new NullPointerException("Null priority");
        }

        /* renamed from: a */
        public C3905l mo23539a() {
            String str = "";
            if (this.f7204a == null) {
                str = str + " backendName";
            }
            if (this.f7206c == null) {
                str = str + " priority";
            }
            if (str.isEmpty()) {
                return new C3890c(this.f7204a, this.f7205b, this.f7206c);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    private C3890c(String str, @Nullable byte[] bArr, C3879d dVar) {
        this.f7201a = str;
        this.f7202b = bArr;
        this.f7203c = dVar;
    }
}
