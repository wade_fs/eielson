package p119e.p144d.p145a.p146a.p147i;

import p119e.p144d.p145a.p146a.C3877b;
import p119e.p144d.p145a.p146a.C3878c;
import p119e.p144d.p145a.p146a.C3880e;
import p119e.p144d.p145a.p146a.p147i.C3903k;

/* renamed from: e.d.a.a.i.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
final class C3887b extends C3903k {

    /* renamed from: a */
    private final C3905l f7191a;

    /* renamed from: b */
    private final String f7192b;

    /* renamed from: c */
    private final C3878c<?> f7193c;

    /* renamed from: d */
    private final C3880e<?, byte[]> f7194d;

    /* renamed from: e */
    private final C3877b f7195e;

    /* renamed from: a */
    public C3877b mo23517a() {
        return this.f7195e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public C3878c<?> mo23518b() {
        return this.f7193c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public C3880e<?, byte[]> mo23519d() {
        return this.f7194d;
    }

    /* renamed from: e */
    public C3905l mo23520e() {
        return this.f7191a;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C3903k)) {
            return false;
        }
        C3903k kVar = (C3903k) obj;
        if (!this.f7191a.equals(super.mo23520e()) || !this.f7192b.equals(super.mo23522f()) || !this.f7193c.equals(super.mo23518b()) || !this.f7194d.equals(super.mo23519d()) || !this.f7195e.equals(super.mo23517a())) {
            return false;
        }
        return true;
    }

    /* renamed from: f */
    public String mo23522f() {
        return this.f7192b;
    }

    public int hashCode() {
        return ((((((((this.f7191a.hashCode() ^ 1000003) * 1000003) ^ this.f7192b.hashCode()) * 1000003) ^ this.f7193c.hashCode()) * 1000003) ^ this.f7194d.hashCode()) * 1000003) ^ this.f7195e.hashCode();
    }

    public String toString() {
        return "SendRequest{transportContext=" + this.f7191a + ", transportName=" + this.f7192b + ", event=" + this.f7193c + ", transformer=" + this.f7194d + ", encoding=" + this.f7195e + "}";
    }

    /* renamed from: e.d.a.a.i.b$b */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    static final class C3889b extends C3903k.C3904a {

        /* renamed from: a */
        private C3905l f7196a;

        /* renamed from: b */
        private String f7197b;

        /* renamed from: c */
        private C3878c<?> f7198c;

        /* renamed from: d */
        private C3880e<?, byte[]> f7199d;

        /* renamed from: e */
        private C3877b f7200e;

        C3889b() {
        }

        /* renamed from: a */
        public C3903k.C3904a mo23528a(C3905l lVar) {
            if (lVar != null) {
                this.f7196a = lVar;
                return super;
            }
            throw new NullPointerException("Null transportContext");
        }

        /* renamed from: a */
        public C3903k.C3904a mo23529a(String str) {
            if (str != null) {
                this.f7197b = str;
                return super;
            }
            throw new NullPointerException("Null transportName");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3903k.C3904a mo23526a(C3878c<?> cVar) {
            if (cVar != null) {
                this.f7198c = cVar;
                return super;
            }
            throw new NullPointerException("Null event");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3903k.C3904a mo23527a(C3880e<?, byte[]> eVar) {
            if (eVar != null) {
                this.f7199d = eVar;
                return super;
            }
            throw new NullPointerException("Null transformer");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3903k.C3904a mo23525a(C3877b bVar) {
            if (bVar != null) {
                this.f7200e = bVar;
                return super;
            }
            throw new NullPointerException("Null encoding");
        }

        /* renamed from: a */
        public C3903k mo23530a() {
            String str = "";
            if (this.f7196a == null) {
                str = str + " transportContext";
            }
            if (this.f7197b == null) {
                str = str + " transportName";
            }
            if (this.f7198c == null) {
                str = str + " event";
            }
            if (this.f7199d == null) {
                str = str + " transformer";
            }
            if (this.f7200e == null) {
                str = str + " encoding";
            }
            if (str.isEmpty()) {
                return new C3887b(this.f7196a, this.f7197b, this.f7198c, this.f7199d, this.f7200e);
            }
            throw new IllegalStateException("Missing required properties:" + str);
        }
    }

    private C3887b(C3905l lVar, String str, C3878c<?> cVar, C3880e<?, byte[]> eVar, C3877b bVar) {
        this.f7191a = lVar;
        this.f7192b = str;
        this.f7193c = cVar;
        this.f7194d = eVar;
        this.f7195e = bVar;
    }
}
