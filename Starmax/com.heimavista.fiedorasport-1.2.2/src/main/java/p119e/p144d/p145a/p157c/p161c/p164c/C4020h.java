package p119e.p144d.p145a.p157c.p161c.p164c;

import java.io.OutputStream;

/* renamed from: e.d.a.c.c.c.h */
/* compiled from: com.google.firebase:firebase-messaging@@20.1.0 */
final class C4020h extends OutputStream {
    C4020h() {
    }

    public final String toString() {
        return "ByteStreams.nullOutputStream()";
    }

    public final void write(int i) {
    }

    public final void write(byte[] bArr) {
        C4019g.m12021a(bArr);
    }

    public final void write(byte[] bArr, int i, int i2) {
        C4019g.m12021a(bArr);
    }
}
