package p119e.p144d.p145a.p146a.p147i.p152w;

import androidx.annotation.WorkerThread;

@WorkerThread
/* renamed from: e.d.a.a.i.w.b */
/* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
public interface C3969b {

    /* renamed from: e.d.a.a.i.w.b$a */
    /* compiled from: com.google.android.datatransport:transport-runtime@@2.2.0 */
    public interface C3970a<T> {
        /* renamed from: s */
        T mo13587s();
    }

    /* renamed from: a */
    <T> T mo23602a(C3970a<T> aVar);
}
