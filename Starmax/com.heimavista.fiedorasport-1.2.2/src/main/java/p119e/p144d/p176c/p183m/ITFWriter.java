package p119e.p144d.p176c.p183m;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.l */
public final class ITFWriter extends OneDimensionalCodeWriter {

    /* renamed from: a */
    private static final int[] f7887a = {1, 1, 1, 1};

    /* renamed from: b */
    private static final int[] f7888b = {3, 1, 1};

    /* renamed from: c */
    private static final int[][] f7889c = {new int[]{1, 1, 3, 3, 1}, new int[]{3, 1, 1, 1, 3}, new int[]{1, 3, 1, 1, 3}, new int[]{3, 3, 1, 1, 1}, new int[]{1, 1, 3, 1, 3}, new int[]{3, 1, 3, 1, 1}, new int[]{1, 3, 3, 1, 1}, new int[]{1, 1, 1, 3, 3}, new int[]{3, 1, 1, 3, 1}, new int[]{1, 3, 1, 3, 1}};

    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.ITF) {
            return super.mo23990a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode ITF, but got ".concat(String.valueOf(aVar)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.m.n.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      e.d.c.m.n.a(boolean[], int, int, int):e.d.c.k.b
      e.d.c.m.n.a(boolean[], int, int[], boolean):int */
    /* renamed from: a */
    public boolean[] mo24096a(String str) {
        int length = str.length();
        if (length % 2 != 0) {
            throw new IllegalArgumentException("The length of the input should be even");
        } else if (length <= 80) {
            boolean[] zArr = new boolean[((length * 9) + 9)];
            int a = OneDimensionalCodeWriter.m12860a(zArr, 0, f7887a, true);
            for (int i = 0; i < length; i += 2) {
                int digit = Character.digit(str.charAt(i), 10);
                int digit2 = Character.digit(str.charAt(i + 1), 10);
                int[] iArr = new int[10];
                for (int i2 = 0; i2 < 5; i2++) {
                    int i3 = i2 * 2;
                    int[][] iArr2 = f7889c;
                    iArr[i3] = iArr2[digit][i2];
                    iArr[i3 + 1] = iArr2[digit2][i2];
                }
                a += OneDimensionalCodeWriter.m12860a(zArr, a, iArr, true);
            }
            OneDimensionalCodeWriter.m12860a(zArr, a, f7888b, true);
            return zArr;
        } else {
            throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got ".concat(String.valueOf(length)));
        }
    }
}
