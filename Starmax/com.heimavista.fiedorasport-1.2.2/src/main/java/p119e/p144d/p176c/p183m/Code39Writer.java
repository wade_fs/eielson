package p119e.p144d.p176c.p183m;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.f */
public final class Code39Writer extends OneDimensionalCodeWriter {
    /* renamed from: b */
    private static String m12846b(String str) {
        int length = str.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt != 0) {
                if (charAt != ' ') {
                    if (charAt == '@') {
                        sb.append("%V");
                    } else if (charAt == '`') {
                        sb.append("%W");
                    } else if (!(charAt == '-' || charAt == '.')) {
                        if (charAt <= 26) {
                            sb.append('$');
                            sb.append((char) ((charAt - 1) + 65));
                        } else if (charAt < ' ') {
                            sb.append('%');
                            sb.append((char) ((charAt - 27) + 65));
                        } else if (charAt <= ',' || charAt == '/' || charAt == ':') {
                            sb.append('/');
                            sb.append((char) ((charAt - '!') + 65));
                        } else if (charAt <= '9') {
                            sb.append((char) ((charAt - '0') + 48));
                        } else if (charAt <= '?') {
                            sb.append('%');
                            sb.append((char) ((charAt - ';') + 70));
                        } else if (charAt <= 'Z') {
                            sb.append((char) ((charAt - 'A') + 65));
                        } else if (charAt <= '_') {
                            sb.append('%');
                            sb.append((char) ((charAt - '[') + 75));
                        } else if (charAt <= 'z') {
                            sb.append('+');
                            sb.append((char) ((charAt - 'a') + 65));
                        } else if (charAt <= 127) {
                            sb.append('%');
                            sb.append((char) ((charAt - '{') + 80));
                        } else {
                            throw new IllegalArgumentException("Requested content contains a non-encodable character: '" + str.charAt(i) + "'");
                        }
                    }
                }
                sb.append(charAt);
            } else {
                sb.append("%U");
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.CODE_39) {
            return super.mo23990a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_39, but got ".concat(String.valueOf(aVar)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.m.n.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      e.d.c.m.n.a(boolean[], int, int, int):e.d.c.k.b
      e.d.c.m.n.a(boolean[], int, int[], boolean):int */
    /* renamed from: a */
    public boolean[] mo24096a(String str) {
        int length = str.length();
        if (length <= 80) {
            int i = 0;
            while (true) {
                if (i >= length) {
                    break;
                } else if ("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(str.charAt(i)) < 0) {
                    str = m12846b(str);
                    length = str.length();
                    if (length > 80) {
                        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got " + length + " (extended full ASCII mode)");
                    }
                } else {
                    i++;
                }
            }
            int[] iArr = new int[9];
            int i2 = length + 25;
            int i3 = 0;
            while (i3 < length) {
                m12845a(Code39Reader.f7884a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(str.charAt(i3))], iArr);
                int i4 = i2;
                for (int i5 = 0; i5 < 9; i5++) {
                    i4 += iArr[i5];
                }
                i3++;
                i2 = i4;
            }
            boolean[] zArr = new boolean[i2];
            m12845a(148, iArr);
            int a = OneDimensionalCodeWriter.m12860a(zArr, 0, iArr, true);
            int[] iArr2 = {1};
            int a2 = a + OneDimensionalCodeWriter.m12860a(zArr, a, iArr2, false);
            for (int i6 = 0; i6 < length; i6++) {
                m12845a(Code39Reader.f7884a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%".indexOf(str.charAt(i6))], iArr);
                int a3 = a2 + OneDimensionalCodeWriter.m12860a(zArr, a2, iArr, true);
                a2 = a3 + OneDimensionalCodeWriter.m12860a(zArr, a3, iArr2, false);
            }
            m12845a(148, iArr);
            OneDimensionalCodeWriter.m12860a(zArr, a2, iArr, true);
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got ".concat(String.valueOf(length)));
    }

    /* renamed from: a */
    private static void m12845a(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            int i3 = 1;
            if (((1 << (8 - i2)) & i) != 0) {
                i3 = 2;
            }
            iArr[i2] = i3;
        }
    }
}
