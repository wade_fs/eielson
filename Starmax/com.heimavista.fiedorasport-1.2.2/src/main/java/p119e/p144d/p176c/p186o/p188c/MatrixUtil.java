package p119e.p144d.p176c.p186o.p188c;

import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;
import p119e.p144d.p176c.WriterException;
import p119e.p144d.p176c.p179k.BitArray;
import p119e.p144d.p176c.p186o.p187b.ErrorCorrectionLevel;
import p119e.p144d.p176c.p186o.p187b.Version;

/* renamed from: e.d.c.o.c.e */
final class MatrixUtil {

    /* renamed from: a */
    private static final int[][] f7962a = {new int[]{1, 1, 1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 1, 1, 1, 0, 1}, new int[]{1, 0, 0, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1, 1, 1}};

    /* renamed from: b */
    private static final int[][] f7963b = {new int[]{1, 1, 1, 1, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 0, 1, 0, 1}, new int[]{1, 0, 0, 0, 1}, new int[]{1, 1, 1, 1, 1}};

    /* renamed from: c */
    private static final int[][] f7964c = {new int[]{-1, -1, -1, -1, -1, -1, -1}, new int[]{6, 18, -1, -1, -1, -1, -1}, new int[]{6, 22, -1, -1, -1, -1, -1}, new int[]{6, 26, -1, -1, -1, -1, -1}, new int[]{6, 30, -1, -1, -1, -1, -1}, new int[]{6, 34, -1, -1, -1, -1, -1}, new int[]{6, 22, 38, -1, -1, -1, -1}, new int[]{6, 24, 42, -1, -1, -1, -1}, new int[]{6, 26, 46, -1, -1, -1, -1}, new int[]{6, 28, 50, -1, -1, -1, -1}, new int[]{6, 30, 54, -1, -1, -1, -1}, new int[]{6, 32, 58, -1, -1, -1, -1}, new int[]{6, 34, 62, -1, -1, -1, -1}, new int[]{6, 26, 46, 66, -1, -1, -1}, new int[]{6, 26, 48, 70, -1, -1, -1}, new int[]{6, 26, 50, 74, -1, -1, -1}, new int[]{6, 30, 54, 78, -1, -1, -1}, new int[]{6, 30, 56, 82, -1, -1, -1}, new int[]{6, 30, 58, 86, -1, -1, -1}, new int[]{6, 34, 62, 90, -1, -1, -1}, new int[]{6, 28, 50, 72, 94, -1, -1}, new int[]{6, 26, 50, 74, 98, -1, -1}, new int[]{6, 30, 54, 78, 102, -1, -1}, new int[]{6, 28, 54, 80, 106, -1, -1}, new int[]{6, 32, 58, 84, 110, -1, -1}, new int[]{6, 30, 58, 86, 114, -1, -1}, new int[]{6, 34, 62, 90, 118, -1, -1}, new int[]{6, 26, 50, 74, 98, 122, -1}, new int[]{6, 30, 54, 78, 102, 126, -1}, new int[]{6, 26, 52, 78, 104, TsExtractor.TS_STREAM_TYPE_HDMV_DTS, -1}, new int[]{6, 30, 56, 82, 108, TsExtractor.TS_STREAM_TYPE_SPLICE_INFO, -1}, new int[]{6, 34, 60, 86, 112, TsExtractor.TS_STREAM_TYPE_DTS, -1}, new int[]{6, 30, 58, 86, 114, 142, -1}, new int[]{6, 34, 62, 90, 118, 146, -1}, new int[]{6, 30, 54, 78, 102, 126, 150}, new int[]{6, 24, 50, 76, 102, 128, 154}, new int[]{6, 28, 54, 80, 106, 132, 158}, new int[]{6, 32, 58, 84, 110, 136, 162}, new int[]{6, 26, 54, 82, 110, TsExtractor.TS_STREAM_TYPE_DTS, 166}, new int[]{6, 30, 58, 86, 114, 142, 170}};

    /* renamed from: d */
    private static final int[][] f7965d = {new int[]{8, 0}, new int[]{8, 1}, new int[]{8, 2}, new int[]{8, 3}, new int[]{8, 4}, new int[]{8, 5}, new int[]{8, 7}, new int[]{8, 8}, new int[]{7, 8}, new int[]{5, 8}, new int[]{4, 8}, new int[]{3, 8}, new int[]{2, 8}, new int[]{1, 8}, new int[]{0, 8}};

    /* renamed from: a */
    static void m12978a(ByteMatrix bVar) {
        bVar.mo24130a((byte) -1);
    }

    /* renamed from: b */
    private static void m12981b(ByteMatrix bVar) {
        if (bVar.mo24129a(8, bVar.mo24134b() - 8) != 0) {
            bVar.mo24131a(8, bVar.mo24134b() - 8, 1);
            return;
        }
        throw new WriterException();
    }

    /* renamed from: b */
    private static boolean m12982b(int i) {
        return i == -1;
    }

    /* renamed from: c */
    static void m12984c(Version cVar, ByteMatrix bVar) {
        if (cVar.mo24119c() >= 7) {
            BitArray aVar = new BitArray();
            m12976a(cVar, aVar);
            int i = 0;
            int i2 = 17;
            while (i < 6) {
                int i3 = i2;
                for (int i4 = 0; i4 < 3; i4++) {
                    boolean a = aVar.mo24022a(i3);
                    i3--;
                    bVar.mo24132a(i, (bVar.mo24134b() - 11) + i4, a);
                    bVar.mo24132a((bVar.mo24134b() - 11) + i4, i, a);
                }
                i++;
                i2 = i3;
            }
        }
    }

    /* renamed from: d */
    private static void m12987d(ByteMatrix bVar) {
        int i = 8;
        while (i < bVar.mo24135c() - 8) {
            int i2 = i + 1;
            int i3 = i2 % 2;
            if (m12982b(bVar.mo24129a(i, 6))) {
                bVar.mo24131a(i, 6, i3);
            }
            if (m12982b(bVar.mo24129a(6, i))) {
                bVar.mo24131a(6, i, i3);
            }
            i = i2;
        }
    }

    /* renamed from: a */
    static void m12973a(BitArray aVar, ErrorCorrectionLevel aVar2, Version cVar, int i, ByteMatrix bVar) {
        m12978a(bVar);
        m12977a(cVar, bVar);
        m12975a(aVar2, i, bVar);
        m12984c(cVar, bVar);
        m12972a(aVar, i, bVar);
    }

    /* renamed from: b */
    private static void m12979b(int i, int i2, ByteMatrix bVar) {
        for (int i3 = 0; i3 < 5; i3++) {
            int[] iArr = f7963b[i3];
            for (int i4 = 0; i4 < 5; i4++) {
                bVar.mo24131a(i + i4, i2 + i3, iArr[i4]);
            }
        }
    }

    /* renamed from: b */
    private static void m12980b(Version cVar, ByteMatrix bVar) {
        if (cVar.mo24119c() >= 2) {
            int[] iArr = f7964c[cVar.mo24119c() - 1];
            for (int i : iArr) {
                if (i >= 0) {
                    for (int i2 : iArr) {
                        if (i2 >= 0 && m12982b(bVar.mo24129a(i2, i))) {
                            m12979b(i2 - 2, i - 2, bVar);
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a */
    static void m12977a(Version cVar, ByteMatrix bVar) {
        m12985c(bVar);
        m12981b(bVar);
        m12980b(cVar, bVar);
        m12987d(bVar);
    }

    /* renamed from: c */
    private static void m12983c(int i, int i2, ByteMatrix bVar) {
        for (int i3 = 0; i3 < 7; i3++) {
            int[] iArr = f7962a[i3];
            for (int i4 = 0; i4 < 7; i4++) {
                bVar.mo24131a(i + i4, i2 + i3, iArr[i4]);
            }
        }
    }

    /* renamed from: d */
    private static void m12986d(int i, int i2, ByteMatrix bVar) {
        int i3 = 0;
        while (i3 < 7) {
            int i4 = i2 + i3;
            if (m12982b(bVar.mo24129a(i, i4))) {
                bVar.mo24131a(i, i4, 0);
                i3++;
            } else {
                throw new WriterException();
            }
        }
    }

    /* renamed from: c */
    private static void m12985c(ByteMatrix bVar) {
        int length = f7962a[0].length;
        m12983c(0, 0, bVar);
        m12983c(bVar.mo24135c() - length, 0, bVar);
        m12983c(0, bVar.mo24135c() - length, bVar);
        m12971a(0, 7, bVar);
        m12971a(bVar.mo24135c() - 8, 7, bVar);
        m12971a(0, bVar.mo24135c() - 8, bVar);
        m12986d(7, 0, bVar);
        m12986d((bVar.mo24134b() - 7) - 1, 0, bVar);
        m12986d(7, bVar.mo24134b() - 7, bVar);
    }

    /* renamed from: a */
    static void m12975a(ErrorCorrectionLevel aVar, int i, ByteMatrix bVar) {
        BitArray aVar2 = new BitArray();
        m12974a(aVar, i, aVar2);
        for (int i2 = 0; i2 < aVar2.mo24017a(); i2++) {
            boolean a = aVar2.mo24022a((aVar2.mo24017a() - 1) - i2);
            int[] iArr = f7965d[i2];
            bVar.mo24132a(iArr[0], iArr[1], a);
            if (i2 < 8) {
                bVar.mo24132a((bVar.mo24135c() - i2) - 1, 8, a);
            } else {
                bVar.mo24132a(8, (bVar.mo24134b() - 7) + (i2 - 8), a);
            }
        }
    }

    /* renamed from: a */
    static void m12972a(BitArray aVar, int i, ByteMatrix bVar) {
        boolean z;
        int c = bVar.mo24135c() - 1;
        int b = bVar.mo24134b() - 1;
        int i2 = 0;
        int i3 = -1;
        while (c > 0) {
            if (c == 6) {
                c--;
            }
            while (b >= 0 && b < bVar.mo24134b()) {
                int i4 = i2;
                for (int i5 = 0; i5 < 2; i5++) {
                    int i6 = c - i5;
                    if (m12982b(bVar.mo24129a(i6, b))) {
                        if (i4 < aVar.mo24017a()) {
                            z = aVar.mo24022a(i4);
                            i4++;
                        } else {
                            z = false;
                        }
                        if (i != -1 && MaskUtil.m12963a(i, i6, b)) {
                            z = !z;
                        }
                        bVar.mo24132a(i6, b, z);
                    }
                }
                b += i3;
                i2 = i4;
            }
            i3 = -i3;
            b += i3;
            c -= 2;
        }
        if (i2 != aVar.mo24017a()) {
            throw new WriterException("Not all bits consumed: " + i2 + '/' + aVar.mo24017a());
        }
    }

    /* renamed from: a */
    static int m12969a(int i) {
        return 32 - Integer.numberOfLeadingZeros(i);
    }

    /* renamed from: a */
    static int m12970a(int i, int i2) {
        if (i2 != 0) {
            int a = m12969a(i2);
            int i3 = i << (a - 1);
            while (m12969a(i3) >= a) {
                i3 ^= i2 << (m12969a(i3) - a);
            }
            return i3;
        }
        throw new IllegalArgumentException("0 polynomial");
    }

    /* renamed from: a */
    static void m12974a(ErrorCorrectionLevel aVar, int i, BitArray aVar2) {
        if (QRCode.m12988b(i)) {
            int a = (aVar.mo24113a() << 3) | i;
            aVar2.mo24018a(a, 5);
            aVar2.mo24018a(m12970a(a, 1335), 10);
            BitArray aVar3 = new BitArray();
            aVar3.mo24018a(21522, 15);
            aVar2.mo24024b(aVar3);
            if (aVar2.mo24017a() != 15) {
                throw new WriterException("should not happen but we got: " + aVar2.mo24017a());
            }
            return;
        }
        throw new WriterException("Invalid mask pattern");
    }

    /* renamed from: a */
    static void m12976a(Version cVar, BitArray aVar) {
        aVar.mo24018a(cVar.mo24119c(), 6);
        aVar.mo24018a(m12970a(cVar.mo24119c(), 7973), 12);
        if (aVar.mo24017a() != 18) {
            throw new WriterException("should not happen but we got: " + aVar.mo24017a());
        }
    }

    /* renamed from: a */
    private static void m12971a(int i, int i2, ByteMatrix bVar) {
        int i3 = 0;
        while (i3 < 8) {
            int i4 = i + i3;
            if (m12982b(bVar.mo24129a(i4, i2))) {
                bVar.mo24131a(i4, i2, 0);
                i3++;
            } else {
                throw new WriterException();
            }
        }
    }
}
