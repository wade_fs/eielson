package p119e.p144d.p176c.p179k.p180d;

import java.util.ArrayList;
import java.util.List;

/* renamed from: e.d.c.k.d.c */
public final class ReedSolomonEncoder {

    /* renamed from: a */
    private final GenericGF f7840a;

    /* renamed from: b */
    private final List<GenericGFPoly> f7841b = new ArrayList();

    public ReedSolomonEncoder(GenericGF aVar) {
        this.f7840a = aVar;
        this.f7841b.add(new GenericGFPoly(aVar, new int[]{1}));
    }

    /* renamed from: a */
    private GenericGFPoly m12748a(int i) {
        if (i >= this.f7841b.size()) {
            List<GenericGFPoly> list = this.f7841b;
            GenericGFPoly bVar = list.get(list.size() - 1);
            for (int size = this.f7841b.size(); size <= i; size++) {
                GenericGF aVar = this.f7840a;
                bVar = bVar.mo24055c(new GenericGFPoly(aVar, new int[]{1, aVar.mo24042a((size - 1) + aVar.mo24041a())}));
                this.f7841b.add(bVar);
            }
        }
        return this.f7841b.get(i);
    }

    /* renamed from: a */
    public void mo24058a(int[] iArr, int i) {
        if (i != 0) {
            int length = iArr.length - i;
            if (length > 0) {
                GenericGFPoly a = m12748a(i);
                int[] iArr2 = new int[length];
                System.arraycopy(iArr, 0, iArr2, 0, length);
                int[] a2 = new GenericGFPoly(this.f7840a, iArr2).mo24050a(i, 1).mo24054b(a)[1].mo24052a();
                int length2 = i - a2.length;
                for (int i2 = 0; i2 < length2; i2++) {
                    iArr[length + i2] = 0;
                }
                System.arraycopy(a2, 0, iArr, length + length2, a2.length);
                return;
            }
            throw new IllegalArgumentException("No data bytes provided");
        }
        throw new IllegalArgumentException("No error correction bytes");
    }
}
