package p119e.p144d.p176c.p177j.p178b;

import p119e.p144d.p176c.p179k.BitArray;

/* renamed from: e.d.c.j.b.g */
abstract class Token {

    /* renamed from: b */
    static final Token f7786b = new SimpleToken(null, 0, 0);

    /* renamed from: a */
    private final Token f7787a;

    Token(Token gVar) {
        this.f7787a = gVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Token mo24014a() {
        return this.f7787a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public abstract void mo23998a(BitArray aVar, byte[] bArr);

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public final Token mo24016b(int i, int i2) {
        return new BinaryShiftToken(this, i, i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public final Token mo24015a(int i, int i2) {
        return new SimpleToken(this, i, i2);
    }
}
