package p119e.p144d.p176c.p186o;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.Writer;
import p119e.p144d.p176c.p179k.BitMatrix;
import p119e.p144d.p176c.p186o.p187b.ErrorCorrectionLevel;
import p119e.p144d.p176c.p186o.p188c.ByteMatrix;
import p119e.p144d.p176c.p186o.p188c.C4196c;
import p119e.p144d.p176c.p186o.p188c.QRCode;

/* renamed from: e.d.c.o.a */
public final class QRCodeWriter implements Writer {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (aVar != BarcodeFormat.QR_CODE) {
            throw new IllegalArgumentException("Can only encode QR_CODE, but got ".concat(String.valueOf(aVar)));
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions are too small: " + i + 'x' + i2);
        } else {
            ErrorCorrectionLevel aVar2 = ErrorCorrectionLevel.f7928Q;
            int i3 = 4;
            if (map != null) {
                if (map.containsKey(EncodeHintType.ERROR_CORRECTION)) {
                    aVar2 = ErrorCorrectionLevel.valueOf(map.get(EncodeHintType.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(EncodeHintType.MARGIN)) {
                    i3 = Integer.parseInt(map.get(EncodeHintType.MARGIN).toString());
                }
            }
            return m12913a(C4196c.m12947a(str, aVar2, map), i, i2, i3);
        }
    }

    /* renamed from: a */
    private static BitMatrix m12913a(QRCode fVar, int i, int i2, int i3) {
        ByteMatrix a = fVar.mo24137a();
        if (a != null) {
            int c = a.mo24135c();
            int b = a.mo24134b();
            int i4 = i3 << 1;
            int i5 = c + i4;
            int i6 = i4 + b;
            int max = Math.max(i, i5);
            int max2 = Math.max(i2, i6);
            int min = Math.min(max / i5, max2 / i6);
            int i7 = (max - (c * min)) / 2;
            int i8 = (max2 - (b * min)) / 2;
            BitMatrix bVar = new BitMatrix(max, max2);
            int i9 = 0;
            while (i9 < b) {
                int i10 = i7;
                int i11 = 0;
                while (i11 < c) {
                    if (a.mo24129a(i11, i9) == 1) {
                        bVar.mo24031a(i10, i8, min, min);
                    }
                    i11++;
                    i10 += min;
                }
                i9++;
                i8 += min;
            }
            return bVar;
        }
        throw new IllegalStateException();
    }
}
