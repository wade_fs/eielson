package p119e.p144d.p176c.p183m;

import com.facebook.appevents.AppEventsConstants;
import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.Writer;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.o */
public final class UPCAWriter implements Writer {

    /* renamed from: a */
    private final EAN13Writer f7890a = new EAN13Writer();

    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.UPC_A) {
            return this.f7890a.mo23990a(AppEventsConstants.EVENT_PARAM_VALUE_NO.concat(String.valueOf(str)), BarcodeFormat.EAN_13, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode UPC-A, but got ".concat(String.valueOf(aVar)));
    }
}
