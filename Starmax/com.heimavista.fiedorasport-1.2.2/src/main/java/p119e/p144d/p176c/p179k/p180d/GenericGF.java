package p119e.p144d.p176c.p179k.p180d;

/* renamed from: e.d.c.k.d.a */
public final class GenericGF {

    /* renamed from: g */
    public static final GenericGF f7826g = new GenericGF(4201, 4096, 1);

    /* renamed from: h */
    public static final GenericGF f7827h = new GenericGF(1033, 1024, 1);

    /* renamed from: i */
    public static final GenericGF f7828i = new GenericGF(67, 64, 1);

    /* renamed from: j */
    public static final GenericGF f7829j = new GenericGF(19, 16, 1);

    /* renamed from: k */
    public static final GenericGF f7830k = new GenericGF(285, 256, 0);

    /* renamed from: l */
    public static final GenericGF f7831l = new GenericGF(301, 256, 1);

    /* renamed from: a */
    private final int[] f7832a;

    /* renamed from: b */
    private final int[] f7833b;

    /* renamed from: c */
    private final GenericGFPoly f7834c;

    /* renamed from: d */
    private final int f7835d;

    /* renamed from: e */
    private final int f7836e;

    /* renamed from: f */
    private final int f7837f;

    public GenericGF(int i, int i2, int i3) {
        this.f7836e = i;
        this.f7835d = i2;
        this.f7837f = i3;
        this.f7832a = new int[i2];
        this.f7833b = new int[i2];
        int i4 = 1;
        for (int i5 = 0; i5 < i2; i5++) {
            this.f7832a[i5] = i4;
            i4 <<= 1;
            if (i4 >= i2) {
                i4 = (i4 ^ i) & (i2 - 1);
            }
        }
        for (int i6 = 0; i6 < i2 - 1; i6++) {
            this.f7833b[this.f7832a[i6]] = i6;
        }
        this.f7834c = new GenericGFPoly(this, new int[]{0});
        new GenericGFPoly(this, new int[]{1});
    }

    /* renamed from: c */
    static int m12732c(int i, int i2) {
        return i ^ i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public GenericGFPoly mo24043a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f7834c;
        } else {
            int[] iArr = new int[(i + 1)];
            iArr[0] = i2;
            return new GenericGFPoly(this, iArr);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public GenericGFPoly mo24046b() {
        return this.f7834c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo24047c(int i) {
        if (i != 0) {
            return this.f7833b[i];
        }
        throw new IllegalArgumentException();
    }

    public String toString() {
        return "GF(0x" + Integer.toHexString(this.f7836e) + ',' + this.f7835d + ')';
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo24044b(int i) {
        if (i != 0) {
            return this.f7832a[(this.f7835d - this.f7833b[i]) - 1];
        }
        throw new ArithmeticException();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo24045b(int i, int i2) {
        if (i == 0 || i2 == 0) {
            return 0;
        }
        int[] iArr = this.f7832a;
        int[] iArr2 = this.f7833b;
        return iArr[(iArr2[i] + iArr2[i2]) % (this.f7835d - 1)];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo24042a(int i) {
        return this.f7832a[i];
    }

    /* renamed from: a */
    public int mo24041a() {
        return this.f7837f;
    }
}
