package p119e.p144d.p176c.p177j.p178b;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;

/* renamed from: e.d.c.j.b.d */
public final class HighLevelEncoder {

    /* renamed from: b */
    static final String[] f7774b = {"UPPER", "LOWER", "DIGIT", "MIXED", "PUNCT"};

    /* renamed from: c */
    static final int[][] f7775c = {new int[]{0, 327708, 327710, 327709, 656318}, new int[]{590318, 0, 327710, 327709, 656318}, new int[]{262158, 590300, 0, 590301, 932798}, new int[]{327709, 327708, 656318, 0, 327710}, new int[]{327711, 656380, 656382, 656381, 0}};

    /* renamed from: d */
    private static final int[][] f7776d;

    /* renamed from: e */
    static final int[][] f7777e;

    /* renamed from: a */
    private final byte[] f7778a;

    /* renamed from: e.d.c.j.b.d$a */
    /* compiled from: HighLevelEncoder */
    class C4189a implements Comparator<State> {
        C4189a(HighLevelEncoder dVar) {
        }

        /* renamed from: a */
        public int compare(State fVar, State fVar2) {
            return fVar.mo24009b() - fVar2.mo24009b();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException}
     arg types: [java.lang.Class<int>, int[]]
     candidates:
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int):java.lang.Object throws java.lang.NegativeArraySizeException}
      ClspMth{java.lang.reflect.Array.newInstance(java.lang.Class<?>, int[]):java.lang.Object VARARG throws java.lang.IllegalArgumentException, java.lang.NegativeArraySizeException} */
    static {
        Class<int> cls = int.class;
        int[][] iArr = (int[][]) Array.newInstance((Class<?>) cls, 5, 256);
        f7776d = iArr;
        iArr[0][32] = 1;
        for (int i = 65; i <= 90; i++) {
            f7776d[0][i] = (i - 65) + 2;
        }
        f7776d[1][32] = 1;
        for (int i2 = 97; i2 <= 122; i2++) {
            f7776d[1][i2] = (i2 - 97) + 2;
        }
        f7776d[2][32] = 1;
        for (int i3 = 48; i3 <= 57; i3++) {
            f7776d[2][i3] = (i3 - 48) + 2;
        }
        int[][] iArr2 = f7776d;
        iArr2[2][44] = 12;
        iArr2[2][46] = 13;
        int[] iArr3 = {0, 32, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 27, 28, 29, 30, 31, 64, 92, 94, 95, 96, 124, 126, 127};
        for (int i4 = 0; i4 < 28; i4++) {
            f7776d[3][iArr3[i4]] = i4;
        }
        int[] iArr4 = {0, 13, 0, 0, 0, 0, 33, 39, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 58, 59, 60, 61, 62, 63, 91, 93, 123, 125};
        for (int i5 = 0; i5 < 31; i5++) {
            if (iArr4[i5] > 0) {
                f7776d[4][iArr4[i5]] = i5;
            }
        }
        int[][] iArr5 = (int[][]) Array.newInstance((Class<?>) cls, 6, 6);
        f7777e = iArr5;
        for (int[] iArr6 : iArr5) {
            Arrays.fill(iArr6, -1);
        }
        int[][] iArr7 = f7777e;
        iArr7[0][4] = 0;
        iArr7[1][4] = 0;
        iArr7[1][0] = 28;
        iArr7[3][4] = 0;
        iArr7[2][4] = 0;
        iArr7[2][0] = 15;
    }

    public HighLevelEncoder(byte[] bArr) {
        this.f7778a = bArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x0042  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0049  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p119e.p144d.p176c.p179k.BitArray mo24000a() {
        /*
            r8 = this;
            e.d.c.j.b.f r0 = p119e.p144d.p176c.p177j.p178b.State.f7781e
            java.util.List r0 = java.util.Collections.singletonList(r0)
            r1 = 0
            r2 = r0
            r0 = 0
        L_0x0009:
            byte[] r3 = r8.f7778a
            int r4 = r3.length
            if (r0 >= r4) goto L_0x0050
            int r4 = r0 + 1
            int r5 = r3.length
            if (r4 >= r5) goto L_0x0016
            byte r3 = r3[r4]
            goto L_0x0017
        L_0x0016:
            r3 = 0
        L_0x0017:
            byte[] r5 = r8.f7778a
            byte r5 = r5[r0]
            r6 = 13
            if (r5 == r6) goto L_0x003b
            r6 = 44
            r7 = 32
            if (r5 == r6) goto L_0x0037
            r6 = 46
            if (r5 == r6) goto L_0x0033
            r6 = 58
            if (r5 == r6) goto L_0x002f
        L_0x002d:
            r3 = 0
            goto L_0x0040
        L_0x002f:
            if (r3 != r7) goto L_0x002d
            r3 = 5
            goto L_0x0040
        L_0x0033:
            if (r3 != r7) goto L_0x002d
            r3 = 3
            goto L_0x0040
        L_0x0037:
            if (r3 != r7) goto L_0x002d
            r3 = 4
            goto L_0x0040
        L_0x003b:
            r5 = 10
            if (r3 != r5) goto L_0x002d
            r3 = 2
        L_0x0040:
            if (r3 <= 0) goto L_0x0049
            java.util.Collection r0 = m12693a(r2, r0, r3)
            r2 = r0
            r0 = r4
            goto L_0x004d
        L_0x0049:
            java.util.Collection r2 = r8.m12692a(r2, r0)
        L_0x004d:
            int r0 = r0 + 1
            goto L_0x0009
        L_0x0050:
            e.d.c.j.b.d$a r0 = new e.d.c.j.b.d$a
            r0.<init>(r8)
            java.lang.Object r0 = java.util.Collections.min(r2, r0)
            e.d.c.j.b.f r0 = (p119e.p144d.p176c.p177j.p178b.State) r0
            byte[] r1 = r8.f7778a
            e.d.c.k.a r0 = r0.mo24007a(r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p176c.p177j.p178b.HighLevelEncoder.mo24000a():e.d.c.k.a");
    }

    /* renamed from: a */
    private Collection<State> m12692a(Iterable<State> iterable, int i) {
        LinkedList linkedList = new LinkedList();
        for (State fVar : iterable) {
            m12695a(fVar, i, linkedList);
        }
        return m12691a(linkedList);
    }

    /* renamed from: a */
    private void m12695a(State fVar, int i, Collection<State> collection) {
        char c = (char) (this.f7778a[i] & 255);
        boolean z = f7776d[fVar.mo24012c()][c] > 0;
        State fVar2 = null;
        for (int i2 = 0; i2 <= 4; i2++) {
            int i3 = f7776d[i2][c];
            if (i3 > 0) {
                if (fVar2 == null) {
                    fVar2 = fVar.mo24010b(i);
                }
                if (!z || i2 == fVar.mo24012c() || i2 == 2) {
                    collection.add(fVar2.mo24006a(i2, i3));
                }
                if (!z && f7777e[fVar.mo24012c()][i2] >= 0) {
                    collection.add(fVar2.mo24011b(i2, i3));
                }
            }
        }
        if (fVar.mo24004a() > 0 || f7776d[fVar.mo24012c()][c] == 0) {
            collection.add(fVar.mo24005a(i));
        }
    }

    /* renamed from: a */
    private static Collection<State> m12693a(Iterable<State> iterable, int i, int i2) {
        LinkedList linkedList = new LinkedList();
        for (State fVar : iterable) {
            m12694a(fVar, i, i2, linkedList);
        }
        return m12691a(linkedList);
    }

    /* renamed from: a */
    private static void m12694a(State fVar, int i, int i2, Collection<State> collection) {
        State b = fVar.mo24010b(i);
        collection.add(b.mo24006a(4, i2));
        if (fVar.mo24012c() != 4) {
            collection.add(b.mo24011b(4, i2));
        }
        if (i2 == 3 || i2 == 4) {
            collection.add(b.mo24006a(2, 16 - i2).mo24006a(2, 1));
        }
        if (fVar.mo24004a() > 0) {
            collection.add(fVar.mo24005a(i).mo24005a(i + 1));
        }
    }

    /* renamed from: a */
    private static Collection<State> m12691a(Iterable<State> iterable) {
        LinkedList linkedList = new LinkedList();
        for (State fVar : iterable) {
            boolean z = true;
            Iterator it = linkedList.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                State fVar2 = (State) it.next();
                if (fVar2.mo24008a(fVar)) {
                    z = false;
                    break;
                } else if (fVar.mo24008a(fVar2)) {
                    it.remove();
                }
            }
            if (z) {
                linkedList.add(fVar);
            }
        }
        return linkedList;
    }
}
