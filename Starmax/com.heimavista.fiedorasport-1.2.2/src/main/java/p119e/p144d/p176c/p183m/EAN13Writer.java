package p119e.p144d.p176c.p183m;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.FormatException;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.j */
public final class EAN13Writer extends UPCEANWriter {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.EAN_13) {
            return super.mo23990a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_13, but got ".concat(String.valueOf(aVar)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.m.n.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      e.d.c.m.n.a(boolean[], int, int, int):e.d.c.k.b
      e.d.c.m.n.a(boolean[], int, int[], boolean):int */
    /* renamed from: a */
    public boolean[] mo24096a(String str) {
        int length = str.length();
        if (length == 12) {
            try {
                str = str + UPCEANReader.m12867b(str);
            } catch (FormatException e) {
                throw new IllegalArgumentException(e);
            }
        } else if (length == 13) {
            try {
                if (!UPCEANReader.m12866a(str)) {
                    throw new IllegalArgumentException("Contents do not pass checksum");
                }
            } catch (FormatException unused) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 12 or 13 digits long, but got ".concat(String.valueOf(length)));
        }
        int i = EAN13Reader.f7886f[Character.digit(str.charAt(0), 10)];
        boolean[] zArr = new boolean[95];
        int a = OneDimensionalCodeWriter.m12860a(zArr, 0, UPCEANReader.f7891a, true) + 0;
        for (int i2 = 1; i2 <= 6; i2++) {
            int digit = Character.digit(str.charAt(i2), 10);
            if (((i >> (6 - i2)) & 1) == 1) {
                digit += 10;
            }
            a += OneDimensionalCodeWriter.m12860a(zArr, a, UPCEANReader.f7895e[digit], false);
        }
        int a2 = a + OneDimensionalCodeWriter.m12860a(zArr, a, UPCEANReader.f7892b, false);
        for (int i3 = 7; i3 <= 12; i3++) {
            a2 += OneDimensionalCodeWriter.m12860a(zArr, a2, UPCEANReader.f7894d[Character.digit(str.charAt(i3), 10)], true);
        }
        OneDimensionalCodeWriter.m12860a(zArr, a2, UPCEANReader.f7891a, true);
        return zArr;
    }
}
