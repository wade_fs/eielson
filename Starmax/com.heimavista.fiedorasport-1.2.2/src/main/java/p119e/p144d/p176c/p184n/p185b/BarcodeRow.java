package p119e.p144d.p176c.p184n.p185b;

/* renamed from: e.d.c.n.b.b */
final class BarcodeRow {

    /* renamed from: a */
    private final byte[] f7901a;

    /* renamed from: b */
    private int f7902b = 0;

    BarcodeRow(int i) {
        this.f7901a = new byte[i];
    }

    /* renamed from: a */
    private void m12879a(int i, boolean z) {
        this.f7901a[i] = z ? (byte) 1 : 0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo24101a(boolean z, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            int i3 = this.f7902b;
            this.f7902b = i3 + 1;
            m12879a(i3, z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public byte[] mo24102a(int i) {
        byte[] bArr = new byte[(this.f7901a.length * i)];
        for (int i2 = 0; i2 < bArr.length; i2++) {
            bArr[i2] = this.f7901a[i2 / i];
        }
        return bArr;
    }
}
