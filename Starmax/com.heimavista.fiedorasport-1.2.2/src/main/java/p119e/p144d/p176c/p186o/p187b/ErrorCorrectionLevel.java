package p119e.p144d.p176c.p186o.p187b;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: e.d.c.o.b.a */
public final class ErrorCorrectionLevel extends Enum<ErrorCorrectionLevel> {

    /* renamed from: Q */
    public static final ErrorCorrectionLevel f7928Q = new ErrorCorrectionLevel("L", 0, 1);

    /* renamed from: R */
    public static final ErrorCorrectionLevel f7929R = new ErrorCorrectionLevel("M", 1, 0);

    /* renamed from: S */
    public static final ErrorCorrectionLevel f7930S = new ErrorCorrectionLevel("Q", 2, 3);

    /* renamed from: T */
    public static final ErrorCorrectionLevel f7931T = new ErrorCorrectionLevel("H", 3, 2);

    /* renamed from: U */
    private static final /* synthetic */ ErrorCorrectionLevel[] f7932U;

    /* renamed from: P */
    private final int f7933P;

    static {
        ErrorCorrectionLevel aVar = f7928Q;
        ErrorCorrectionLevel aVar2 = f7929R;
        ErrorCorrectionLevel aVar3 = f7930S;
        ErrorCorrectionLevel aVar4 = f7931T;
        f7932U = new ErrorCorrectionLevel[]{aVar, aVar2, aVar3, aVar4};
        ErrorCorrectionLevel[] aVarArr = {aVar2, aVar, aVar4, aVar3};
    }

    private ErrorCorrectionLevel(String str, int i, int i2) {
        this.f7933P = i2;
    }

    public static ErrorCorrectionLevel valueOf(String str) {
        return (ErrorCorrectionLevel) Enum.valueOf(ErrorCorrectionLevel.class, str);
    }

    public static ErrorCorrectionLevel[] values() {
        return (ErrorCorrectionLevel[]) f7932U.clone();
    }

    /* renamed from: a */
    public int mo24113a() {
        return this.f7933P;
    }
}
