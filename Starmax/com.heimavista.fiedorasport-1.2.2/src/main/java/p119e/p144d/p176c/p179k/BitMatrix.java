package p119e.p144d.p176c.p179k;

import java.util.Arrays;

/* renamed from: e.d.c.k.b */
public final class BitMatrix implements Cloneable {

    /* renamed from: P */
    private final int f7790P;

    /* renamed from: Q */
    private final int f7791Q;

    /* renamed from: R */
    private final int f7792R;

    /* renamed from: S */
    private final int[] f7793S;

    public BitMatrix(int i) {
        this(i, i);
    }

    /* renamed from: a */
    public boolean mo24032a(int i, int i2) {
        return ((this.f7793S[(i2 * this.f7792R) + (i / 32)] >>> (i & 31)) & 1) != 0;
    }

    /* renamed from: b */
    public void mo24034b(int i, int i2) {
        int i3 = (i2 * this.f7792R) + (i / 32);
        int[] iArr = this.f7793S;
        iArr[i3] = (1 << (i & 31)) | iArr[i3];
    }

    /* renamed from: c */
    public int mo24035c() {
        return this.f7790P;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BitMatrix)) {
            return false;
        }
        BitMatrix bVar = (BitMatrix) obj;
        if (this.f7790P == bVar.f7790P && this.f7791Q == bVar.f7791Q && this.f7792R == bVar.f7792R && Arrays.equals(this.f7793S, bVar.f7793S)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i = this.f7790P;
        return (((((((i * 31) + i) * 31) + this.f7791Q) * 31) + this.f7792R) * 31) + Arrays.hashCode(this.f7793S);
    }

    public String toString() {
        return mo24029a("X ", "  ");
    }

    public BitMatrix(int i, int i2) {
        if (i <= 0 || i2 <= 0) {
            throw new IllegalArgumentException("Both dimensions must be greater than 0");
        }
        this.f7790P = i;
        this.f7791Q = i2;
        this.f7792R = (i + 31) / 32;
        this.f7793S = new int[(this.f7792R * i2)];
    }

    public BitMatrix clone() {
        return new BitMatrix(this.f7790P, this.f7791Q, this.f7792R, (int[]) this.f7793S.clone());
    }

    /* renamed from: a */
    public void mo24030a() {
        int length = this.f7793S.length;
        for (int i = 0; i < length; i++) {
            this.f7793S[i] = 0;
        }
    }

    /* renamed from: b */
    public int mo24033b() {
        return this.f7791Q;
    }

    /* renamed from: a */
    public void mo24031a(int i, int i2, int i3, int i4) {
        if (i2 < 0 || i < 0) {
            throw new IllegalArgumentException("Left and top must be nonnegative");
        } else if (i4 <= 0 || i3 <= 0) {
            throw new IllegalArgumentException("Height and width must be at least 1");
        } else {
            int i5 = i3 + i;
            int i6 = i4 + i2;
            if (i6 > this.f7791Q || i5 > this.f7790P) {
                throw new IllegalArgumentException("The region must fit inside the matrix");
            }
            while (i2 < i6) {
                int i7 = this.f7792R * i2;
                for (int i8 = i; i8 < i5; i8++) {
                    int[] iArr = this.f7793S;
                    int i9 = (i8 / 32) + i7;
                    iArr[i9] = iArr[i9] | (1 << (i8 & 31));
                }
                i2++;
            }
        }
    }

    private BitMatrix(int i, int i2, int i3, int[] iArr) {
        this.f7790P = i;
        this.f7791Q = i2;
        this.f7792R = i3;
        this.f7793S = iArr;
    }

    /* renamed from: a */
    public String mo24029a(String str, String str2) {
        return m12722a(str, str2, "\n");
    }

    /* renamed from: a */
    private String m12722a(String str, String str2, String str3) {
        StringBuilder sb = new StringBuilder(this.f7791Q * (this.f7790P + 1));
        for (int i = 0; i < this.f7791Q; i++) {
            for (int i2 = 0; i2 < this.f7790P; i2++) {
                sb.append(mo24032a(i2, i) ? str : str2);
            }
            sb.append(str3);
        }
        return sb.toString();
    }
}
