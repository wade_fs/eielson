package p119e.p144d.p176c.p181l.p182b;

/* renamed from: e.d.c.l.b.l */
public enum SymbolShapeHint {
    FORCE_NONE,
    FORCE_SQUARE,
    FORCE_RECTANGLE
}
