package p119e.p144d.p176c;

import java.util.Map;
import p119e.p144d.p176c.p177j.AztecWriter;
import p119e.p144d.p176c.p179k.BitMatrix;
import p119e.p144d.p176c.p181l.DataMatrixWriter;
import p119e.p144d.p176c.p183m.CodaBarWriter;
import p119e.p144d.p176c.p183m.Code128Writer;
import p119e.p144d.p176c.p183m.Code39Writer;
import p119e.p144d.p176c.p183m.Code93Writer;
import p119e.p144d.p176c.p183m.EAN13Writer;
import p119e.p144d.p176c.p183m.EAN8Writer;
import p119e.p144d.p176c.p183m.ITFWriter;
import p119e.p144d.p176c.p183m.UPCAWriter;
import p119e.p144d.p176c.p183m.UPCEWriter;
import p119e.p144d.p176c.p184n.PDF417Writer;
import p119e.p144d.p176c.p186o.QRCodeWriter;

/* renamed from: e.d.c.e */
public final class MultiFormatWriter implements Writer {

    /* renamed from: e.d.c.e$a */
    /* compiled from: MultiFormatWriter */
    static /* synthetic */ class C4187a {

        /* renamed from: a */
        static final /* synthetic */ int[] f7767a = new int[BarcodeFormat.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|(3:25|26|28)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(28:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|28) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                e.d.c.a[] r0 = p119e.p144d.p176c.BarcodeFormat.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a = r0
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0014 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.EAN_8     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x001f }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.UPC_E     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x002a }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.EAN_13     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0035 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.UPC_A     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0040 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.QR_CODE     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x004b }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.CODE_39     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0056 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.CODE_93     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0062 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.CODE_128     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x006e }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.ITF     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x007a }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.PDF_417     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0086 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.CODABAR     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x0092 }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.DATA_MATRIX     // Catch:{ NoSuchFieldError -> 0x0092 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0092 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0092 }
            L_0x0092:
                int[] r0 = p119e.p144d.p176c.MultiFormatWriter.C4187a.f7767a     // Catch:{ NoSuchFieldError -> 0x009e }
                e.d.c.a r1 = p119e.p144d.p176c.BarcodeFormat.AZTEC     // Catch:{ NoSuchFieldError -> 0x009e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009e }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009e }
            L_0x009e:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p176c.MultiFormatWriter.C4187a.<clinit>():void");
        }
    }

    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        Writer hVar;
        switch (C4187a.f7767a[aVar.ordinal()]) {
            case 1:
                hVar = new EAN8Writer();
                break;
            case 2:
                hVar = new UPCEWriter();
                break;
            case 3:
                hVar = new EAN13Writer();
                break;
            case 4:
                hVar = new UPCAWriter();
                break;
            case 5:
                hVar = new QRCodeWriter();
                break;
            case 6:
                hVar = new Code39Writer();
                break;
            case 7:
                hVar = new Code93Writer();
                break;
            case 8:
                hVar = new Code128Writer();
                break;
            case 9:
                hVar = new ITFWriter();
                break;
            case 10:
                hVar = new PDF417Writer();
                break;
            case 11:
                hVar = new CodaBarWriter();
                break;
            case 12:
                hVar = new DataMatrixWriter();
                break;
            case 13:
                hVar = new AztecWriter();
                break;
            default:
                throw new IllegalArgumentException("No encoder available for format ".concat(String.valueOf(aVar)));
        }
        return hVar.mo23990a(str, aVar, i, i2, map);
    }
}
