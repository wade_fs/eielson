package p119e.p144d.p176c.p183m;

import java.util.ArrayList;
import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.d */
public final class Code128Writer extends OneDimensionalCodeWriter {

    /* renamed from: e.d.c.m.d$a */
    /* compiled from: Code128Writer */
    private enum C4192a {
        UNCODABLE,
        ONE_DIGIT,
        TWO_DIGITS,
        FNC_1
    }

    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.CODE_128) {
            return super.mo23990a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_128, but got ".concat(String.valueOf(aVar)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.m.n.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      e.d.c.m.n.a(boolean[], int, int, int):e.d.c.k.b
      e.d.c.m.n.a(boolean[], int, int[], boolean):int */
    /* renamed from: a */
    public boolean[] mo24096a(String str) {
        int i;
        int length = str.length();
        if (length <= 0 || length > 80) {
            throw new IllegalArgumentException("Contents length should be between 1 and 80 characters, but got ".concat(String.valueOf(length)));
        }
        int i2 = 0;
        for (int i3 = 0; i3 < length; i3++) {
            char charAt = str.charAt(i3);
            switch (charAt) {
                case 241:
                case 242:
                case 243:
                case 244:
                    break;
                default:
                    if (charAt <= 127) {
                        break;
                    } else {
                        throw new IllegalArgumentException("Bad character in input: ".concat(String.valueOf(charAt)));
                    }
            }
        }
        ArrayList<int[]> arrayList = new ArrayList<>();
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 1;
        while (i4 < length) {
            int a = m12841a(str, i4, i6);
            int i8 = 100;
            if (a == i6) {
                switch (str.charAt(i4)) {
                    case 241:
                        i8 = 102;
                        break;
                    case 242:
                        i8 = 97;
                        break;
                    case 243:
                        i8 = 96;
                        break;
                    case 244:
                        if (i6 == 101) {
                            i8 = 101;
                            break;
                        }
                        break;
                    default:
                        if (i6 != 100) {
                            if (i6 == 101) {
                                i8 = str.charAt(i4) - ' ';
                                if (i8 < 0) {
                                    i8 += 96;
                                    break;
                                }
                            } else {
                                i8 = Integer.parseInt(str.substring(i4, i4 + 2));
                                i4++;
                                break;
                            }
                        } else {
                            i8 = str.charAt(i4) - ' ';
                            break;
                        }
                        break;
                }
                i4++;
            } else {
                i = i6 == 0 ? a != 100 ? a != 101 ? 105 : 103 : 104 : a;
                i6 = a;
            }
            arrayList.add(Code128Reader.f7878a[i]);
            i5 += i * i7;
            if (i4 != 0) {
                i7++;
            }
        }
        arrayList.add(Code128Reader.f7878a[i5 % 103]);
        arrayList.add(Code128Reader.f7878a[106]);
        int i9 = 0;
        for (int[] iArr : arrayList) {
            int i10 = i9;
            for (int i11 : iArr) {
                i10 += i11;
            }
            i9 = i10;
        }
        boolean[] zArr = new boolean[i9];
        for (int[] iArr2 : arrayList) {
            i2 += OneDimensionalCodeWriter.m12860a(zArr, i2, iArr2, true);
        }
        return zArr;
    }

    /* renamed from: a */
    private static C4192a m12842a(CharSequence charSequence, int i) {
        int length = charSequence.length();
        if (i >= length) {
            return C4192a.UNCODABLE;
        }
        char charAt = charSequence.charAt(i);
        if (charAt == 241) {
            return C4192a.FNC_1;
        }
        if (charAt < '0' || charAt > '9') {
            return C4192a.UNCODABLE;
        }
        int i2 = i + 1;
        if (i2 >= length) {
            return C4192a.ONE_DIGIT;
        }
        char charAt2 = charSequence.charAt(i2);
        if (charAt2 < '0' || charAt2 > '9') {
            return C4192a.ONE_DIGIT;
        }
        return C4192a.TWO_DIGITS;
    }

    /* renamed from: a */
    private static int m12841a(CharSequence charSequence, int i, int i2) {
        C4192a a;
        C4192a a2;
        char charAt;
        C4192a a3 = m12842a(charSequence, i);
        if (a3 == C4192a.ONE_DIGIT) {
            return 100;
        }
        if (a3 == C4192a.UNCODABLE) {
            if (i >= charSequence.length() || ((charAt = charSequence.charAt(i)) >= ' ' && (i2 != 101 || charAt >= '`'))) {
                return 100;
            }
            return 101;
        } else if (i2 == 99) {
            return 99;
        } else {
            if (i2 != 100) {
                if (a3 == C4192a.FNC_1) {
                    a3 = m12842a(charSequence, i + 1);
                }
                if (a3 == C4192a.TWO_DIGITS) {
                    return 99;
                }
                return 100;
            } else if (a3 == C4192a.FNC_1 || (a = m12842a(charSequence, i + 2)) == C4192a.UNCODABLE || a == C4192a.ONE_DIGIT) {
                return 100;
            } else {
                if (a != C4192a.FNC_1) {
                    int i3 = i + 4;
                    while (true) {
                        a2 = m12842a(charSequence, i3);
                        if (a2 != C4192a.TWO_DIGITS) {
                            break;
                        }
                        i3 += 2;
                    }
                    if (a2 == C4192a.ONE_DIGIT) {
                        return 100;
                    }
                    return 99;
                } else if (m12842a(charSequence, i + 3) == C4192a.TWO_DIGITS) {
                    return 99;
                } else {
                    return 100;
                }
            }
        }
    }
}
