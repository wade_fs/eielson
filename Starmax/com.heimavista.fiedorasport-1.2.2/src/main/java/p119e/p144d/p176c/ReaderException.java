package p119e.p144d.p176c;

/* renamed from: e.d.c.g */
public abstract class ReaderException extends Exception {

    /* renamed from: P */
    protected static final boolean f7768P = (System.getProperty("surefire.test.class.path") != null);

    /* renamed from: Q */
    protected static final StackTraceElement[] f7769Q = new StackTraceElement[0];

    ReaderException() {
    }

    public final synchronized Throwable fillInStackTrace() {
        return null;
    }
}
