package p119e.p144d.p176c.p183m;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.h */
public class Code93Writer extends OneDimensionalCodeWriter {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.CODE_93) {
            return super.mo23990a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode CODE_93, but got ".concat(String.valueOf(aVar)));
    }

    /* renamed from: a */
    public boolean[] mo24096a(String str) {
        int length = str.length();
        if (length <= 80) {
            int[] iArr = new int[9];
            m12851a(Code93Reader.f7885a[47], iArr);
            boolean[] zArr = new boolean[(((str.length() + 2 + 2) * 9) + 1)];
            int a = m12850a(zArr, 0, iArr);
            for (int i = 0; i < length; i++) {
                m12851a(Code93Reader.f7885a["0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(str.charAt(i))], iArr);
                a += m12850a(zArr, a, iArr);
            }
            int a2 = m12849a(str, 20);
            m12851a(Code93Reader.f7885a[a2], iArr);
            int a3 = a + m12850a(zArr, a, iArr);
            m12851a(Code93Reader.f7885a[m12849a(str + "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".charAt(a2), 15)], iArr);
            int a4 = a3 + m12850a(zArr, a3, iArr);
            m12851a(Code93Reader.f7885a[47], iArr);
            zArr[a4 + m12850a(zArr, a4, iArr)] = true;
            return zArr;
        }
        throw new IllegalArgumentException("Requested contents should be less than 80 digits long, but got ".concat(String.valueOf(length)));
    }

    /* renamed from: a */
    private static void m12851a(int i, int[] iArr) {
        for (int i2 = 0; i2 < 9; i2++) {
            int i3 = 1;
            if (((1 << (8 - i2)) & i) == 0) {
                i3 = 0;
            }
            iArr[i2] = i3;
        }
    }

    /* renamed from: a */
    private static int m12850a(boolean[] zArr, int i, int[] iArr) {
        int length = iArr.length;
        int i2 = i;
        int i3 = 0;
        while (i3 < length) {
            int i4 = i2 + 1;
            zArr[i2] = iArr[i3] != 0;
            i3++;
            i2 = i4;
        }
        return 9;
    }

    /* renamed from: a */
    private static int m12849a(String str, int i) {
        int i2 = 0;
        int i3 = 1;
        for (int length = str.length() - 1; length >= 0; length--) {
            i2 += "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".indexOf(str.charAt(length)) * i3;
            i3++;
            if (i3 > i) {
                i3 = 1;
            }
        }
        return i2 % 47;
    }
}
