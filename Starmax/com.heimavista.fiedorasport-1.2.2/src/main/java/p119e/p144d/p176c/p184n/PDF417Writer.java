package p119e.p144d.p176c.p184n;

import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.Writer;
import p119e.p144d.p176c.p179k.BitMatrix;
import p119e.p144d.p176c.p184n.p185b.Compaction;
import p119e.p144d.p176c.p184n.p185b.Dimensions;
import p119e.p144d.p176c.p184n.p185b.PDF417;

/* renamed from: e.d.c.n.a */
public final class PDF417Writer implements Writer {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        int i3;
        int i4;
        if (aVar == BarcodeFormat.PDF_417) {
            PDF417 eVar = new PDF417();
            int i5 = 30;
            int i6 = 2;
            if (map != null) {
                if (map.containsKey(EncodeHintType.PDF417_COMPACT)) {
                    eVar.mo24112a(Boolean.valueOf(map.get(EncodeHintType.PDF417_COMPACT).toString()).booleanValue());
                }
                if (map.containsKey(EncodeHintType.PDF417_COMPACTION)) {
                    eVar.mo24109a(Compaction.valueOf(map.get(EncodeHintType.PDF417_COMPACTION).toString()));
                }
                if (map.containsKey(EncodeHintType.PDF417_DIMENSIONS)) {
                    Dimensions dVar = (Dimensions) map.get(EncodeHintType.PDF417_DIMENSIONS);
                    eVar.mo24108a(dVar.mo24103a(), dVar.mo24105c(), dVar.mo24104b(), dVar.mo24106d());
                }
                if (map.containsKey(EncodeHintType.MARGIN)) {
                    i5 = Integer.parseInt(map.get(EncodeHintType.MARGIN).toString());
                }
                if (map.containsKey(EncodeHintType.ERROR_CORRECTION)) {
                    i6 = Integer.parseInt(map.get(EncodeHintType.ERROR_CORRECTION).toString());
                }
                if (map.containsKey(EncodeHintType.CHARACTER_SET)) {
                    eVar.mo24111a(Charset.forName(map.get(EncodeHintType.CHARACTER_SET).toString()));
                }
                i3 = i5;
                i4 = i6;
            } else {
                i4 = 2;
                i3 = 30;
            }
            return m12872a(eVar, str, i4, i, i2, i3);
        }
        throw new IllegalArgumentException("Can only encode PDF_417, but got ".concat(String.valueOf(aVar)));
    }

    /* renamed from: a */
    private static BitMatrix m12872a(PDF417 eVar, String str, int i, int i2, int i3, int i4) {
        boolean z;
        eVar.mo24110a(str, i);
        byte[][] a = eVar.mo24107a().mo24099a(1, 4);
        if ((i3 > i2) != (a[0].length < a.length)) {
            a = m12874a(a);
            z = true;
        } else {
            z = false;
        }
        int length = i2 / a[0].length;
        int length2 = i3 / a.length;
        if (length >= length2) {
            length = length2;
        }
        if (length <= 1) {
            return m12873a(a, i4);
        }
        byte[][] a2 = eVar.mo24107a().mo24099a(length, length << 2);
        if (z) {
            a2 = m12874a(a2);
        }
        return m12873a(a2, i4);
    }

    /* renamed from: a */
    private static BitMatrix m12873a(byte[][] bArr, int i) {
        int i2 = i * 2;
        BitMatrix bVar = new BitMatrix(bArr[0].length + i2, bArr.length + i2);
        bVar.mo24030a();
        int b = (bVar.mo24033b() - i) - 1;
        int i3 = 0;
        while (i3 < bArr.length) {
            byte[] bArr2 = bArr[i3];
            for (int i4 = 0; i4 < bArr[0].length; i4++) {
                if (bArr2[i4] == 1) {
                    bVar.mo24034b(i4 + i, b);
                }
            }
            i3++;
            b--;
        }
        return bVar;
    }

    /* renamed from: a */
    private static byte[][] m12874a(byte[][] bArr) {
        byte[][] bArr2 = (byte[][]) Array.newInstance(byte.class, bArr[0].length, bArr.length);
        for (int i = 0; i < bArr.length; i++) {
            int length = (bArr.length - i) - 1;
            for (int i2 = 0; i2 < bArr[0].length; i2++) {
                bArr2[i2][length] = bArr[i][i2];
            }
        }
        return bArr2;
    }
}
