package p119e.p144d.p176c.p177j.p178b;

import java.util.LinkedList;
import p119e.p144d.p176c.p179k.BitArray;

/* renamed from: e.d.c.j.b.f */
final class State {

    /* renamed from: e */
    static final State f7781e = new State(Token.f7786b, 0, 0, 0);

    /* renamed from: a */
    private final int f7782a;

    /* renamed from: b */
    private final Token f7783b;

    /* renamed from: c */
    private final int f7784c;

    /* renamed from: d */
    private final int f7785d;

    private State(Token gVar, int i, int i2, int i3) {
        this.f7783b = gVar;
        this.f7782a = i;
        this.f7784c = i2;
        this.f7785d = i3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo24004a() {
        return this.f7784c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo24009b() {
        return this.f7785d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public int mo24012c() {
        return this.f7782a;
    }

    public String toString() {
        return String.format("%s bits=%d bytes=%d", HighLevelEncoder.f7774b[this.f7782a], Integer.valueOf(this.f7785d), Integer.valueOf(this.f7784c));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public State mo24006a(int i, int i2) {
        int i3 = this.f7785d;
        Token gVar = this.f7783b;
        int i4 = this.f7782a;
        if (i != i4) {
            int i5 = HighLevelEncoder.f7775c[i4][i];
            int i6 = 65535 & i5;
            int i7 = i5 >> 16;
            gVar = gVar.mo24015a(i6, i7);
            i3 += i7;
        }
        int i8 = i == 2 ? 4 : 5;
        return new State(gVar.mo24015a(i2, i8), i, 0, i3 + i8);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public State mo24011b(int i, int i2) {
        Token gVar = this.f7783b;
        int i3 = this.f7782a == 2 ? 4 : 5;
        return new State(gVar.mo24015a(HighLevelEncoder.f7777e[this.f7782a][i], i3).mo24015a(i2, 5), this.f7782a, 0, this.f7785d + i3 + 5);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public State mo24010b(int i) {
        int i2 = this.f7784c;
        if (i2 == 0) {
            return this;
        }
        return new State(this.f7783b.mo24016b(i - i2, i2), this.f7782a, 0, this.f7785d);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public State mo24005a(int i) {
        Token gVar = this.f7783b;
        int i2 = this.f7782a;
        int i3 = this.f7785d;
        if (i2 == 4 || i2 == 2) {
            int i4 = HighLevelEncoder.f7775c[i2][0];
            int i5 = 65535 & i4;
            int i6 = i4 >> 16;
            gVar = gVar.mo24015a(i5, i6);
            i3 += i6;
            i2 = 0;
        }
        int i7 = this.f7784c;
        State fVar = new State(gVar, i2, this.f7784c + 1, i3 + ((i7 == 0 || i7 == 31) ? 18 : i7 == 62 ? 9 : 8));
        return fVar.f7784c == 2078 ? fVar.mo24010b(i + 1) : fVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo24008a(State fVar) {
        int i;
        int i2 = this.f7785d + (HighLevelEncoder.f7775c[this.f7782a][fVar.f7782a] >> 16);
        int i3 = fVar.f7784c;
        if (i3 > 0 && ((i = this.f7784c) == 0 || i > i3)) {
            i2 += 10;
        }
        return i2 <= fVar.f7785d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public BitArray mo24007a(byte[] bArr) {
        LinkedList<Token> linkedList = new LinkedList<>();
        for (Token gVar = mo24010b(bArr.length).f7783b; gVar != null; gVar = gVar.mo24014a()) {
            linkedList.addFirst(gVar);
        }
        BitArray aVar = new BitArray();
        for (Token gVar2 : linkedList) {
            gVar2.mo23998a(aVar, bArr);
        }
        return aVar;
    }
}
