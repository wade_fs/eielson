package p119e.p144d.p176c.p184n.p185b;

import java.lang.reflect.Array;

/* renamed from: e.d.c.n.b.a */
public final class BarcodeMatrix {

    /* renamed from: a */
    private final BarcodeRow[] f7897a;

    /* renamed from: b */
    private int f7898b;

    /* renamed from: c */
    private final int f7899c;

    /* renamed from: d */
    private final int f7900d;

    BarcodeMatrix(int i, int i2) {
        this.f7897a = new BarcodeRow[i];
        int length = this.f7897a.length;
        for (int i3 = 0; i3 < length; i3++) {
            this.f7897a[i3] = new BarcodeRow(((i2 + 4) * 17) + 1);
        }
        this.f7900d = i2 * 17;
        this.f7899c = i;
        this.f7898b = -1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public BarcodeRow mo24098a() {
        return this.f7897a[this.f7898b];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo24100b() {
        this.f7898b++;
    }

    /* renamed from: a */
    public byte[][] mo24099a(int i, int i2) {
        byte[][] bArr = (byte[][]) Array.newInstance(byte.class, this.f7899c * i2, this.f7900d * i);
        int i3 = this.f7899c * i2;
        for (int i4 = 0; i4 < i3; i4++) {
            bArr[(i3 - i4) - 1] = this.f7897a[i4 / i2].mo24102a(i);
        }
        return bArr;
    }
}
