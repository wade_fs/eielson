package p119e.p144d.p176c.p186o.p188c;

import androidx.recyclerview.widget.ItemTouchHelper;
import p119e.p144d.p176c.p186o.p187b.ErrorCorrectionLevel;
import p119e.p144d.p176c.p186o.p187b.Mode;
import p119e.p144d.p176c.p186o.p187b.Version;

/* renamed from: e.d.c.o.c.f */
public final class QRCode {

    /* renamed from: a */
    private Mode f7966a;

    /* renamed from: b */
    private ErrorCorrectionLevel f7967b;

    /* renamed from: c */
    private Version f7968c;

    /* renamed from: d */
    private int f7969d = -1;

    /* renamed from: e */
    private ByteMatrix f7970e;

    /* renamed from: b */
    public static boolean m12988b(int i) {
        return i >= 0 && i < 8;
    }

    /* renamed from: a */
    public ByteMatrix mo24137a() {
        return this.f7970e;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((int) ItemTouchHelper.Callback.DEFAULT_DRAG_ANIMATION_DURATION);
        sb.append("<<\n");
        sb.append(" mode: ");
        sb.append(this.f7966a);
        sb.append("\n ecLevel: ");
        sb.append(this.f7967b);
        sb.append("\n version: ");
        sb.append(this.f7968c);
        sb.append("\n maskPattern: ");
        sb.append(this.f7969d);
        if (this.f7970e == null) {
            sb.append("\n matrix: null\n");
        } else {
            sb.append("\n matrix:\n");
            sb.append(this.f7970e);
        }
        sb.append(">>\n");
        return sb.toString();
    }

    /* renamed from: a */
    public void mo24140a(Mode bVar) {
        this.f7966a = bVar;
    }

    /* renamed from: a */
    public void mo24139a(ErrorCorrectionLevel aVar) {
        this.f7967b = aVar;
    }

    /* renamed from: a */
    public void mo24141a(Version cVar) {
        this.f7968c = cVar;
    }

    /* renamed from: a */
    public void mo24138a(int i) {
        this.f7969d = i;
    }

    /* renamed from: a */
    public void mo24142a(ByteMatrix bVar) {
        this.f7970e = bVar;
    }
}
