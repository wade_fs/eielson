package p119e.p144d.p176c;

/* renamed from: e.d.c.d */
public final class FormatException extends ReaderException {

    /* renamed from: R */
    private static final FormatException f7766R;

    static {
        FormatException dVar = new FormatException();
        f7766R = dVar;
        dVar.setStackTrace(ReaderException.f7769Q);
    }

    private FormatException() {
    }

    /* renamed from: a */
    public static FormatException m12669a() {
        return ReaderException.f7768P ? new FormatException() : f7766R;
    }
}
