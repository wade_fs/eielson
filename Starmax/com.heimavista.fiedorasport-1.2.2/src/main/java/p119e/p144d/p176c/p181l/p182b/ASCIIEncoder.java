package p119e.p144d.p176c.p181l.p182b;

import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;

/* renamed from: e.d.c.l.b.a */
final class ASCIIEncoder implements C4190g {
    ASCIIEncoder() {
    }

    /* renamed from: a */
    public int mo24059a() {
        return 0;
    }

    /* renamed from: a */
    public void mo24060a(EncoderContext hVar) {
        if (C4191j.m12807a(hVar.mo24080d(), hVar.f7851f) >= 2) {
            hVar.mo24071a(m12753a(hVar.mo24080d().charAt(hVar.f7851f), hVar.mo24080d().charAt(hVar.f7851f + 1)));
            hVar.f7851f += 2;
            return;
        }
        char c = hVar.mo24078c();
        int a = C4191j.m12808a(hVar.mo24080d(), hVar.f7851f, mo24059a());
        if (a != mo24059a()) {
            if (a == 1) {
                hVar.mo24071a(230);
                hVar.mo24077b(1);
            } else if (a == 2) {
                hVar.mo24071a(239);
                hVar.mo24077b(2);
            } else if (a == 3) {
                hVar.mo24071a(238);
                hVar.mo24077b(3);
            } else if (a == 4) {
                hVar.mo24071a(240);
                hVar.mo24077b(4);
            } else if (a == 5) {
                hVar.mo24071a(231);
                hVar.mo24077b(5);
            } else {
                throw new IllegalStateException("Illegal mode: ".concat(String.valueOf(a)));
            }
        } else if (C4191j.m12814c(c)) {
            hVar.mo24071a(235);
            hVar.mo24071a((char) ((c - 128) + 1));
            hVar.f7851f++;
        } else {
            hVar.mo24071a((char) (c + 1));
            hVar.f7851f++;
        }
    }

    /* renamed from: a */
    private static char m12753a(char c, char c2) {
        if (C4191j.m12813b(c) && C4191j.m12813b(c2)) {
            return (char) (((c - '0') * 10) + (c2 - '0') + TsExtractor.TS_STREAM_TYPE_HDMV_DTS);
        }
        throw new IllegalArgumentException("not digits: " + c + c2);
    }
}
