package p119e.p144d.p176c.p183m;

/* renamed from: e.d.c.m.a */
public final class CodaBarReader extends OneDReader {

    /* renamed from: a */
    static final char[] f7872a = "0123456789-$:/.+ABCD".toCharArray();

    /* renamed from: b */
    static final int[] f7873b = {3, 6, 9, 96, 18, 66, 33, 36, 48, 72, 12, 24, 69, 81, 84, 21, 26, 41, 11, 14};

    /* renamed from: a */
    static boolean m12839a(char[] cArr, char c) {
        if (cArr != null) {
            for (char c2 : cArr) {
                if (c2 == c) {
                    return true;
                }
            }
        }
        return false;
    }
}
