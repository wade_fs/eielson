package p119e.p144d.p176c.p186o.p188c;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Map;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.WriterException;
import p119e.p144d.p176c.p179k.BitArray;
import p119e.p144d.p176c.p179k.CharacterSetECI;
import p119e.p144d.p176c.p179k.p180d.GenericGF;
import p119e.p144d.p176c.p179k.p180d.ReedSolomonEncoder;
import p119e.p144d.p176c.p186o.p187b.ErrorCorrectionLevel;
import p119e.p144d.p176c.p186o.p187b.Mode;
import p119e.p144d.p176c.p186o.p187b.Version;

/* renamed from: e.d.c.o.c.c */
/* compiled from: Encoder */
public final class C4196c {

    /* renamed from: a */
    private static final int[] f7960a = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 36, -1, -1, -1, 37, 38, -1, -1, -1, -1, 39, 40, -1, 41, 42, 43, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 44, -1, -1, -1, -1, -1, -1, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, -1, -1, -1, -1, -1};

    /* renamed from: e.d.c.o.c.c$a */
    /* compiled from: Encoder */
    static /* synthetic */ class C4197a {

        /* renamed from: a */
        static final /* synthetic */ int[] f7961a = new int[Mode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                e.d.c.o.b.b[] r0 = p119e.p144d.p176c.p186o.p187b.Mode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p144d.p176c.p186o.p188c.C4196c.C4197a.f7961a = r0
                int[] r0 = p119e.p144d.p176c.p186o.p188c.C4196c.C4197a.f7961a     // Catch:{ NoSuchFieldError -> 0x0014 }
                e.d.c.o.b.b r1 = p119e.p144d.p176c.p186o.p187b.Mode.NUMERIC     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p144d.p176c.p186o.p188c.C4196c.C4197a.f7961a     // Catch:{ NoSuchFieldError -> 0x001f }
                e.d.c.o.b.b r1 = p119e.p144d.p176c.p186o.p187b.Mode.ALPHANUMERIC     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p144d.p176c.p186o.p188c.C4196c.C4197a.f7961a     // Catch:{ NoSuchFieldError -> 0x002a }
                e.d.c.o.b.b r1 = p119e.p144d.p176c.p186o.p187b.Mode.BYTE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p144d.p176c.p186o.p188c.C4196c.C4197a.f7961a     // Catch:{ NoSuchFieldError -> 0x0035 }
                e.d.c.o.b.b r1 = p119e.p144d.p176c.p186o.p187b.Mode.KANJI     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p176c.p186o.p188c.C4196c.C4197a.<clinit>():void");
        }
    }

    /* renamed from: a */
    private static int m12942a(ByteMatrix bVar) {
        return MaskUtil.m12961a(bVar) + MaskUtil.m12966b(bVar) + MaskUtil.m12967c(bVar) + MaskUtil.m12968d(bVar);
    }

    /* renamed from: b */
    static void m12960b(CharSequence charSequence, BitArray aVar) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int charAt = charSequence.charAt(i) - '0';
            int i2 = i + 2;
            if (i2 < length) {
                aVar.mo24018a((charAt * 100) + ((charSequence.charAt(i + 1) - '0') * 10) + (charSequence.charAt(i2) - '0'), 10);
                i += 3;
            } else {
                i++;
                if (i < length) {
                    aVar.mo24018a((charAt * 10) + (charSequence.charAt(i) - '0'), 7);
                    i = i2;
                } else {
                    aVar.mo24018a(charAt, 4);
                }
            }
        }
    }

    /* renamed from: a */
    public static QRCode m12947a(String str, ErrorCorrectionLevel aVar, Map<EncodeHintType, ?> map) {
        Version cVar;
        CharacterSetECI a;
        boolean z = true;
        boolean z2 = map != null && map.containsKey(EncodeHintType.CHARACTER_SET);
        String obj = z2 ? map.get(EncodeHintType.CHARACTER_SET).toString() : "ISO-8859-1";
        Mode a2 = m12944a(str, obj);
        BitArray aVar2 = new BitArray();
        if (a2 == Mode.BYTE && z2 && (a = CharacterSetECI.m12730a(obj)) != null) {
            m12951a(a, aVar2);
        }
        if (map == null || !map.containsKey(EncodeHintType.GS1_FORMAT)) {
            z = false;
        }
        if (z && Boolean.valueOf(map.get(EncodeHintType.GS1_FORMAT).toString()).booleanValue()) {
            m12952a(Mode.FNC1_FIRST_POSITION, aVar2);
        }
        m12952a(a2, aVar2);
        BitArray aVar3 = new BitArray();
        m12956a(str, a2, aVar3, obj);
        if (map == null || !map.containsKey(EncodeHintType.QR_VERSION)) {
            cVar = m12946a(aVar, a2, aVar2, aVar3);
        } else {
            cVar = Version.m12918a(Integer.parseInt(map.get(EncodeHintType.QR_VERSION).toString()));
            if (!m12957a(m12941a(a2, aVar2, aVar3, cVar), cVar, aVar)) {
                throw new WriterException("Data too big for requested version");
            }
        }
        BitArray aVar4 = new BitArray();
        aVar4.mo24020a(aVar2);
        m12950a(a2 == Mode.BYTE ? aVar3.mo24023b() : str.length(), cVar, a2, aVar4);
        aVar4.mo24020a(aVar3);
        Version.C4195b a3 = cVar.mo24117a(aVar);
        int b = cVar.mo24118b() - a3.mo24126d();
        m12949a(b, aVar4);
        BitArray a4 = m12943a(aVar4, cVar.mo24118b(), b, a3.mo24125c());
        QRCode fVar = new QRCode();
        fVar.mo24139a(aVar);
        fVar.mo24140a(a2);
        fVar.mo24141a(cVar);
        int a5 = cVar.mo24116a();
        ByteMatrix bVar = new ByteMatrix(a5, a5);
        int a6 = m12940a(a4, aVar, cVar, bVar);
        fVar.mo24138a(a6);
        MatrixUtil.m12973a(a4, aVar, cVar, a6, bVar);
        fVar.mo24142a(bVar);
        return fVar;
    }

    /* renamed from: a */
    private static Version m12946a(ErrorCorrectionLevel aVar, Mode bVar, BitArray aVar2, BitArray aVar3) {
        return m12945a(m12941a(bVar, aVar2, aVar3, m12945a(m12941a(bVar, aVar2, aVar3, Version.m12918a(1)), aVar)), aVar);
    }

    /* renamed from: a */
    private static int m12941a(Mode bVar, BitArray aVar, BitArray aVar2, Version cVar) {
        return aVar.mo24017a() + bVar.mo24115a(cVar) + aVar2.mo24017a();
    }

    /* renamed from: a */
    static int m12939a(int i) {
        int[] iArr = f7960a;
        if (i < iArr.length) {
            return iArr[i];
        }
        return -1;
    }

    /* renamed from: a */
    private static Mode m12944a(String str, String str2) {
        if ("Shift_JIS".equals(str2) && m12958a(str)) {
            return Mode.KANJI;
        }
        boolean z = false;
        boolean z2 = false;
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (charAt >= '0' && charAt <= '9') {
                z2 = true;
            } else if (m12939a(charAt) == -1) {
                return Mode.BYTE;
            } else {
                z = true;
            }
        }
        if (z) {
            return Mode.ALPHANUMERIC;
        }
        if (z2) {
            return Mode.NUMERIC;
        }
        return Mode.BYTE;
    }

    /* renamed from: a */
    private static boolean m12958a(String str) {
        try {
            byte[] bytes = str.getBytes("Shift_JIS");
            int length = bytes.length;
            if (length % 2 != 0) {
                return false;
            }
            for (int i = 0; i < length; i += 2) {
                byte b = bytes[i] & 255;
                if ((b < 129 || b > 159) && (b < 224 || b > 235)) {
                    return false;
                }
            }
            return true;
        } catch (UnsupportedEncodingException unused) {
            return false;
        }
    }

    /* renamed from: a */
    private static int m12940a(BitArray aVar, ErrorCorrectionLevel aVar2, Version cVar, ByteMatrix bVar) {
        int i = Integer.MAX_VALUE;
        int i2 = -1;
        for (int i3 = 0; i3 < 8; i3++) {
            MatrixUtil.m12973a(aVar, aVar2, cVar, i3, bVar);
            int a = m12942a(bVar);
            if (a < i) {
                i2 = i3;
                i = a;
            }
        }
        return i2;
    }

    /* renamed from: a */
    private static Version m12945a(int i, ErrorCorrectionLevel aVar) {
        for (int i2 = 1; i2 <= 40; i2++) {
            Version a = Version.m12918a(i2);
            if (m12957a(i, a, aVar)) {
                return a;
            }
        }
        throw new WriterException("Data too big");
    }

    /* renamed from: a */
    private static boolean m12957a(int i, Version cVar, ErrorCorrectionLevel aVar) {
        return cVar.mo24118b() - cVar.mo24117a(aVar).mo24126d() >= (i + 7) / 8;
    }

    /* renamed from: a */
    static void m12949a(int i, BitArray aVar) {
        int i2 = i << 3;
        if (aVar.mo24017a() <= i2) {
            for (int i3 = 0; i3 < 4 && aVar.mo24017a() < i2; i3++) {
                aVar.mo24021a(false);
            }
            int a = aVar.mo24017a() & 7;
            if (a > 0) {
                while (a < 8) {
                    aVar.mo24021a(false);
                    a++;
                }
            }
            int b = i - aVar.mo24023b();
            for (int i4 = 0; i4 < b; i4++) {
                aVar.mo24018a((i4 & 1) == 0 ? 236 : 17, 8);
            }
            if (aVar.mo24017a() != i2) {
                throw new WriterException("Bits size does not equal capacity");
            }
            return;
        }
        throw new WriterException("data bits cannot fit in the QR Code" + aVar.mo24017a() + " > " + i2);
    }

    /* renamed from: a */
    static void m12948a(int i, int i2, int i3, int i4, int[] iArr, int[] iArr2) {
        if (i4 < i3) {
            int i5 = i % i3;
            int i6 = i3 - i5;
            int i7 = i / i3;
            int i8 = i7 + 1;
            int i9 = i2 / i3;
            int i10 = i9 + 1;
            int i11 = i7 - i9;
            int i12 = i8 - i10;
            if (i11 != i12) {
                throw new WriterException("EC bytes mismatch");
            } else if (i3 != i6 + i5) {
                throw new WriterException("RS blocks mismatch");
            } else if (i != ((i9 + i11) * i6) + ((i10 + i12) * i5)) {
                throw new WriterException("Total bytes mismatch");
            } else if (i4 < i6) {
                iArr[0] = i9;
                iArr2[0] = i11;
            } else {
                iArr[0] = i10;
                iArr2[0] = i12;
            }
        } else {
            throw new WriterException("Block ID too large");
        }
    }

    /* renamed from: a */
    static BitArray m12943a(BitArray aVar, int i, int i2, int i3) {
        int i4 = i;
        int i5 = i2;
        int i6 = i3;
        if (aVar.mo24023b() == i5) {
            ArrayList<BlockPair> arrayList = new ArrayList<>(i6);
            int i7 = 0;
            int i8 = 0;
            int i9 = 0;
            for (int i10 = 0; i10 < i6; i10++) {
                int[] iArr = new int[1];
                int[] iArr2 = new int[1];
                m12948a(i, i2, i3, i10, iArr, iArr2);
                int i11 = iArr[0];
                byte[] bArr = new byte[i11];
                aVar.mo24019a(i7 << 3, bArr, 0, i11);
                byte[] a = m12959a(bArr, iArr2[0]);
                arrayList.add(new BlockPair(bArr, a));
                i8 = Math.max(i8, i11);
                i9 = Math.max(i9, a.length);
                i7 += iArr[0];
            }
            if (i5 == i7) {
                BitArray aVar2 = new BitArray();
                for (int i12 = 0; i12 < i8; i12++) {
                    for (BlockPair aVar3 : arrayList) {
                        byte[] a2 = aVar3.mo24127a();
                        if (i12 < a2.length) {
                            aVar2.mo24018a(a2[i12], 8);
                        }
                    }
                }
                for (int i13 = 0; i13 < i9; i13++) {
                    for (BlockPair aVar4 : arrayList) {
                        byte[] b = aVar4.mo24128b();
                        if (i13 < b.length) {
                            aVar2.mo24018a(b[i13], 8);
                        }
                    }
                }
                if (i4 == aVar2.mo24023b()) {
                    return aVar2;
                }
                throw new WriterException("Interleaving error: " + i4 + " and " + aVar2.mo24023b() + " differ.");
            }
            throw new WriterException("Data bytes does not match offset");
        }
        throw new WriterException("Number of bits and data bytes does not match");
    }

    /* renamed from: a */
    static byte[] m12959a(byte[] bArr, int i) {
        int length = bArr.length;
        int[] iArr = new int[(length + i)];
        for (int i2 = 0; i2 < length; i2++) {
            iArr[i2] = bArr[i2] & 255;
        }
        new ReedSolomonEncoder(GenericGF.f7830k).mo24058a(iArr, i);
        byte[] bArr2 = new byte[i];
        for (int i3 = 0; i3 < i; i3++) {
            bArr2[i3] = (byte) iArr[length + i3];
        }
        return bArr2;
    }

    /* renamed from: a */
    static void m12952a(Mode bVar, BitArray aVar) {
        aVar.mo24018a(bVar.mo24114a(), 4);
    }

    /* renamed from: a */
    static void m12950a(int i, Version cVar, Mode bVar, BitArray aVar) {
        int a = bVar.mo24115a(cVar);
        int i2 = 1 << a;
        if (i < i2) {
            aVar.mo24018a(i, a);
            return;
        }
        throw new WriterException(i + " is bigger than " + (i2 - 1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.o.c.c.a(java.lang.CharSequence, e.d.c.k.a):void
     arg types: [java.lang.String, e.d.c.k.a]
     candidates:
      e.d.c.o.c.c.a(java.lang.String, java.lang.String):e.d.c.o.b.b
      e.d.c.o.c.c.a(int, e.d.c.o.b.a):e.d.c.o.b.c
      e.d.c.o.c.c.a(int, e.d.c.k.a):void
      e.d.c.o.c.c.a(e.d.c.k.c, e.d.c.k.a):void
      e.d.c.o.c.c.a(e.d.c.o.b.b, e.d.c.k.a):void
      e.d.c.o.c.c.a(java.lang.String, e.d.c.k.a):void
      e.d.c.o.c.c.a(byte[], int):byte[]
      e.d.c.o.c.c.a(java.lang.CharSequence, e.d.c.k.a):void */
    /* renamed from: a */
    static void m12956a(String str, Mode bVar, BitArray aVar, String str2) {
        int i = C4197a.f7961a[bVar.ordinal()];
        if (i == 1) {
            m12960b(str, aVar);
        } else if (i == 2) {
            m12953a((CharSequence) str, aVar);
        } else if (i == 3) {
            m12955a(str, aVar, str2);
        } else if (i == 4) {
            m12954a(str, aVar);
        } else {
            throw new WriterException("Invalid mode: ".concat(String.valueOf(bVar)));
        }
    }

    /* renamed from: a */
    static void m12953a(CharSequence charSequence, BitArray aVar) {
        int length = charSequence.length();
        int i = 0;
        while (i < length) {
            int a = m12939a(charSequence.charAt(i));
            if (a != -1) {
                int i2 = i + 1;
                if (i2 < length) {
                    int a2 = m12939a(charSequence.charAt(i2));
                    if (a2 != -1) {
                        aVar.mo24018a((a * 45) + a2, 11);
                        i += 2;
                    } else {
                        throw new WriterException();
                    }
                } else {
                    aVar.mo24018a(a, 6);
                    i = i2;
                }
            } else {
                throw new WriterException();
            }
        }
    }

    /* renamed from: a */
    static void m12955a(String str, BitArray aVar, String str2) {
        try {
            for (byte b : str.getBytes(str2)) {
                aVar.mo24018a(b, 8);
            }
        } catch (UnsupportedEncodingException e) {
            throw new WriterException(e);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0035 A[LOOP:0: B:4:0x0008->B:17:0x0035, LOOP_END] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0044 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void m12954a(java.lang.String r6, p119e.p144d.p176c.p179k.BitArray r7) {
        /*
            java.lang.String r0 = "Shift_JIS"
            byte[] r6 = r6.getBytes(r0)     // Catch:{ UnsupportedEncodingException -> 0x004d }
            int r0 = r6.length
            r1 = 0
        L_0x0008:
            if (r1 >= r0) goto L_0x004c
            byte r2 = r6[r1]
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r1 + 1
            byte r3 = r6[r3]
            r3 = r3 & 255(0xff, float:3.57E-43)
            int r2 = r2 << 8
            r2 = r2 | r3
            r3 = 33088(0x8140, float:4.6366E-41)
            r4 = -1
            if (r2 < r3) goto L_0x0024
            r5 = 40956(0x9ffc, float:5.7392E-41)
            if (r2 > r5) goto L_0x0024
        L_0x0022:
            int r2 = r2 - r3
            goto L_0x0033
        L_0x0024:
            r3 = 57408(0xe040, float:8.0446E-41)
            if (r2 < r3) goto L_0x0032
            r3 = 60351(0xebbf, float:8.457E-41)
            if (r2 > r3) goto L_0x0032
            r3 = 49472(0xc140, float:6.9325E-41)
            goto L_0x0022
        L_0x0032:
            r2 = -1
        L_0x0033:
            if (r2 == r4) goto L_0x0044
            int r3 = r2 >> 8
            int r3 = r3 * 192
            r2 = r2 & 255(0xff, float:3.57E-43)
            int r3 = r3 + r2
            r2 = 13
            r7.mo24018a(r3, r2)
            int r1 = r1 + 2
            goto L_0x0008
        L_0x0044:
            e.d.c.i r6 = new e.d.c.i
            java.lang.String r7 = "Invalid byte sequence"
            r6.<init>(r7)
            throw r6
        L_0x004c:
            return
        L_0x004d:
            r6 = move-exception
            e.d.c.i r7 = new e.d.c.i
            r7.<init>(r6)
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p144d.p176c.p186o.p188c.C4196c.m12954a(java.lang.String, e.d.c.k.a):void");
    }

    /* renamed from: a */
    private static void m12951a(CharacterSetECI cVar, BitArray aVar) {
        aVar.mo24018a(Mode.ECI.mo24114a(), 4);
        aVar.mo24018a(cVar.mo24040a(), 8);
    }
}
