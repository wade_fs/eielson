package p119e.p144d.p176c;

import java.util.Map;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.h */
public interface Writer {
    /* renamed from: a */
    BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map);
}
