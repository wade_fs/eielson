package p119e.p144d.p176c.p177j.p178b;

import p119e.p144d.p176c.p179k.BitArray;
import p119e.p144d.p176c.p179k.BitMatrix;
import p119e.p144d.p176c.p179k.p180d.GenericGF;
import p119e.p144d.p176c.p179k.p180d.ReedSolomonEncoder;

/* renamed from: e.d.c.j.b.c */
/* compiled from: Encoder */
public final class C4188c {

    /* renamed from: a */
    private static final int[] f7773a = {4, 6, 6, 8, 8, 8, 8, 8, 8, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 12, 12, 12, 12, 12, 12, 12, 12, 12, 12};

    /* renamed from: a */
    private static int m12682a(int i, boolean z) {
        return ((z ? 88 : 112) + (i << 4)) * i;
    }

    /* renamed from: a */
    public static AztecCode m12683a(byte[] bArr, int i, int i2) {
        int i3;
        int i4;
        int i5;
        boolean z;
        BitArray aVar;
        int i6;
        BitArray aVar2;
        BitArray a = new HighLevelEncoder(bArr).mo24000a();
        int i7 = 11;
        int a2 = ((a.mo24017a() * i) / 100) + 11;
        int a3 = a.mo24017a() + a2;
        int i8 = 32;
        int i9 = 0;
        int i10 = 1;
        if (i2 != 0) {
            z = i2 < 0;
            i4 = Math.abs(i2);
            if (z) {
                i8 = 4;
            }
            if (i4 <= i8) {
                i5 = m12682a(i4, z);
                i3 = f7773a[i4];
                int i11 = i5 - (i5 % i3);
                aVar = m12684a(a, i3);
                if (aVar.mo24017a() + a2 > i11) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                } else if (z && aVar.mo24017a() > (i3 << 6)) {
                    throw new IllegalArgumentException("Data to large for user specified layer");
                }
            } else {
                throw new IllegalArgumentException(String.format("Illegal value %s for layers", Integer.valueOf(i2)));
            }
        } else {
            BitArray aVar3 = null;
            int i12 = 0;
            int i13 = 0;
            while (i12 <= 32) {
                boolean z2 = i12 <= 3;
                int i14 = z2 ? i12 + 1 : i12;
                int a4 = m12682a(i14, z2);
                if (a3 <= a4) {
                    if (aVar3 == null || i13 != f7773a[i14]) {
                        i3 = f7773a[i14];
                        aVar2 = m12684a(a, i3);
                    } else {
                        int i15 = i13;
                        aVar2 = aVar3;
                        i3 = i15;
                    }
                    int i16 = a4 - (a4 % i3);
                    if ((!z2 || aVar2.mo24017a() <= (i3 << 6)) && aVar2.mo24017a() + a2 <= i16) {
                        aVar = aVar2;
                        z = z2;
                        i4 = i14;
                        i5 = a4;
                    } else {
                        BitArray aVar4 = aVar2;
                        i13 = i3;
                        aVar3 = aVar4;
                    }
                }
                i12++;
                i9 = 0;
                i10 = 1;
            }
            throw new IllegalArgumentException("Data too large for an Aztec code");
        }
        BitArray b = m12690b(aVar, i5, i3);
        int a5 = aVar.mo24017a() / i3;
        BitArray a6 = m12685a(z, i4, a5);
        if (!z) {
            i7 = 14;
        }
        int i17 = i7 + (i4 << 2);
        int[] iArr = new int[i17];
        int i18 = 2;
        if (z) {
            for (int i19 = 0; i19 < iArr.length; i19++) {
                iArr[i19] = i19;
            }
            i6 = i17;
        } else {
            int i20 = i17 / 2;
            i6 = i17 + 1 + (((i20 - 1) / 15) * 2);
            int i21 = i6 / 2;
            for (int i22 = 0; i22 < i20; i22++) {
                int i23 = (i22 / 15) + i22;
                iArr[(i20 - i22) - i10] = (i21 - i23) - 1;
                iArr[i20 + i22] = i23 + i21 + i10;
            }
        }
        BitMatrix bVar = new BitMatrix(i6);
        int i24 = 0;
        int i25 = 0;
        while (i24 < i4) {
            int i26 = ((i4 - i24) << i18) + (z ? 9 : 12);
            int i27 = 0;
            while (i27 < i26) {
                int i28 = i27 << 1;
                while (i9 < i18) {
                    if (b.mo24022a(i25 + i28 + i9)) {
                        int i29 = i24 << 1;
                        bVar.mo24034b(iArr[i29 + i9], iArr[i29 + i27]);
                    }
                    if (b.mo24022a((i26 << 1) + i25 + i28 + i9)) {
                        int i30 = i24 << 1;
                        bVar.mo24034b(iArr[i30 + i27], iArr[((i17 - 1) - i30) - i9]);
                    }
                    if (b.mo24022a((i26 << 2) + i25 + i28 + i9)) {
                        int i31 = (i17 - 1) - (i24 << 1);
                        bVar.mo24034b(iArr[i31 - i9], iArr[i31 - i27]);
                    }
                    if (b.mo24022a((i26 * 6) + i25 + i28 + i9)) {
                        int i32 = i24 << 1;
                        bVar.mo24034b(iArr[((i17 - 1) - i32) - i27], iArr[i32 + i9]);
                    }
                    i9++;
                    i18 = 2;
                }
                i27++;
                i9 = 0;
                i18 = 2;
            }
            i25 += i26 << 3;
            i24++;
            i9 = 0;
            i18 = 2;
        }
        m12688a(bVar, z, i6, a6);
        if (z) {
            m12687a(bVar, i6 / 2, 5);
        } else {
            int i33 = i6 / 2;
            m12687a(bVar, i33, 7);
            int i34 = 0;
            int i35 = 0;
            while (i34 < (i17 / 2) - 1) {
                for (int i36 = i33 & 1; i36 < i6; i36 += 2) {
                    int i37 = i33 - i35;
                    bVar.mo24034b(i37, i36);
                    int i38 = i33 + i35;
                    bVar.mo24034b(i38, i36);
                    bVar.mo24034b(i36, i37);
                    bVar.mo24034b(i36, i38);
                }
                i34 += 15;
                i35 += 16;
            }
        }
        AztecCode aVar5 = new AztecCode();
        aVar5.mo23995a(z);
        aVar5.mo23997c(i6);
        aVar5.mo23996b(i4);
        aVar5.mo23993a(a5);
        aVar5.mo23994a(bVar);
        return aVar5;
    }

    /* renamed from: b */
    private static BitArray m12690b(BitArray aVar, int i, int i2) {
        ReedSolomonEncoder cVar = new ReedSolomonEncoder(m12686a(i2));
        int i3 = i / i2;
        int[] a = m12689a(aVar, i2, i3);
        cVar.mo24058a(a, i3 - (aVar.mo24017a() / i2));
        BitArray aVar2 = new BitArray();
        aVar2.mo24018a(0, i % i2);
        for (int i4 : a) {
            aVar2.mo24018a(i4, i2);
        }
        return aVar2;
    }

    /* renamed from: a */
    private static void m12687a(BitMatrix bVar, int i, int i2) {
        for (int i3 = 0; i3 < i2; i3 += 2) {
            int i4 = i - i3;
            int i5 = i4;
            while (true) {
                int i6 = i + i3;
                if (i5 > i6) {
                    break;
                }
                bVar.mo24034b(i5, i4);
                bVar.mo24034b(i5, i6);
                bVar.mo24034b(i4, i5);
                bVar.mo24034b(i6, i5);
                i5++;
            }
        }
        int i7 = i - i2;
        bVar.mo24034b(i7, i7);
        int i8 = i7 + 1;
        bVar.mo24034b(i8, i7);
        bVar.mo24034b(i7, i8);
        int i9 = i + i2;
        bVar.mo24034b(i9, i7);
        bVar.mo24034b(i9, i8);
        bVar.mo24034b(i9, i9 - 1);
    }

    /* renamed from: a */
    static BitArray m12685a(boolean z, int i, int i2) {
        BitArray aVar = new BitArray();
        if (z) {
            aVar.mo24018a(i - 1, 2);
            aVar.mo24018a(i2 - 1, 6);
            return m12690b(aVar, 28, 4);
        }
        aVar.mo24018a(i - 1, 5);
        aVar.mo24018a(i2 - 1, 11);
        return m12690b(aVar, 40, 4);
    }

    /* renamed from: a */
    private static void m12688a(BitMatrix bVar, boolean z, int i, BitArray aVar) {
        int i2 = i / 2;
        int i3 = 0;
        if (z) {
            while (i3 < 7) {
                int i4 = (i2 - 3) + i3;
                if (aVar.mo24022a(i3)) {
                    bVar.mo24034b(i4, i2 - 5);
                }
                if (aVar.mo24022a(i3 + 7)) {
                    bVar.mo24034b(i2 + 5, i4);
                }
                if (aVar.mo24022a(20 - i3)) {
                    bVar.mo24034b(i4, i2 + 5);
                }
                if (aVar.mo24022a(27 - i3)) {
                    bVar.mo24034b(i2 - 5, i4);
                }
                i3++;
            }
            return;
        }
        while (i3 < 10) {
            int i5 = (i2 - 5) + i3 + (i3 / 5);
            if (aVar.mo24022a(i3)) {
                bVar.mo24034b(i5, i2 - 7);
            }
            if (aVar.mo24022a(i3 + 10)) {
                bVar.mo24034b(i2 + 7, i5);
            }
            if (aVar.mo24022a(29 - i3)) {
                bVar.mo24034b(i5, i2 + 7);
            }
            if (aVar.mo24022a(39 - i3)) {
                bVar.mo24034b(i2 - 7, i5);
            }
            i3++;
        }
    }

    /* renamed from: a */
    private static int[] m12689a(BitArray aVar, int i, int i2) {
        int[] iArr = new int[i2];
        int a = aVar.mo24017a() / i;
        for (int i3 = 0; i3 < a; i3++) {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                i4 |= aVar.mo24022a((i3 * i) + i5) ? 1 << ((i - i5) - 1) : 0;
            }
            iArr[i3] = i4;
        }
        return iArr;
    }

    /* renamed from: a */
    private static GenericGF m12686a(int i) {
        if (i == 4) {
            return GenericGF.f7829j;
        }
        if (i == 6) {
            return GenericGF.f7828i;
        }
        if (i == 8) {
            return GenericGF.f7831l;
        }
        if (i == 10) {
            return GenericGF.f7827h;
        }
        if (i == 12) {
            return GenericGF.f7826g;
        }
        throw new IllegalArgumentException("Unsupported word size ".concat(String.valueOf(i)));
    }

    /* renamed from: a */
    static BitArray m12684a(BitArray aVar, int i) {
        BitArray aVar2 = new BitArray();
        int a = aVar.mo24017a();
        int i2 = (1 << i) - 2;
        int i3 = 0;
        while (i3 < a) {
            int i4 = 0;
            for (int i5 = 0; i5 < i; i5++) {
                int i6 = i3 + i5;
                if (i6 >= a || aVar.mo24022a(i6)) {
                    i4 |= 1 << ((i - 1) - i5);
                }
            }
            int i7 = i4 & i2;
            if (i7 == i2) {
                aVar2.mo24018a(i7, i);
            } else if (i7 == 0) {
                aVar2.mo24018a(i4 | 1, i);
            } else {
                aVar2.mo24018a(i4, i);
                i3 += i;
            }
            i3--;
            i3 += i;
        }
        return aVar2;
    }
}
