package p119e.p144d.p176c.p184n.p185b;

/* renamed from: e.d.c.n.b.d */
public final class Dimensions {

    /* renamed from: a */
    private final int f7908a;

    /* renamed from: b */
    private final int f7909b;

    /* renamed from: c */
    private final int f7910c;

    /* renamed from: d */
    private final int f7911d;

    /* renamed from: a */
    public int mo24103a() {
        return this.f7909b;
    }

    /* renamed from: b */
    public int mo24104b() {
        return this.f7911d;
    }

    /* renamed from: c */
    public int mo24105c() {
        return this.f7908a;
    }

    /* renamed from: d */
    public int mo24106d() {
        return this.f7910c;
    }
}
