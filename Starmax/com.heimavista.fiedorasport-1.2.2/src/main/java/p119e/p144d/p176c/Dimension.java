package p119e.p144d.p176c;

/* renamed from: e.d.c.b */
public final class Dimension {

    /* renamed from: a */
    private final int f7751a;

    /* renamed from: b */
    private final int f7752b;

    /* renamed from: a */
    public int mo23985a() {
        return this.f7752b;
    }

    /* renamed from: b */
    public int mo23986b() {
        return this.f7751a;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Dimension) {
            Dimension bVar = (Dimension) obj;
            if (this.f7751a == bVar.f7751a && this.f7752b == bVar.f7752b) {
                return true;
            }
            return false;
        }
        return false;
    }

    public int hashCode() {
        return (this.f7751a * 32713) + this.f7752b;
    }

    public String toString() {
        return this.f7751a + "x" + this.f7752b;
    }
}
