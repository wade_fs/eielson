package p119e.p144d.p176c.p183m;

import p119e.p144d.p176c.FormatException;

/* renamed from: e.d.c.m.p */
public abstract class UPCEANReader extends OneDReader {

    /* renamed from: a */
    static final int[] f7891a = {1, 1, 1};

    /* renamed from: b */
    static final int[] f7892b = {1, 1, 1, 1, 1};

    /* renamed from: c */
    static final int[] f7893c = {1, 1, 1, 1, 1, 1};

    /* renamed from: d */
    static final int[][] f7894d = {new int[]{3, 2, 1, 1}, new int[]{2, 2, 2, 1}, new int[]{2, 1, 2, 2}, new int[]{1, 4, 1, 1}, new int[]{1, 1, 3, 2}, new int[]{1, 2, 3, 1}, new int[]{1, 1, 1, 4}, new int[]{1, 3, 1, 2}, new int[]{1, 2, 1, 3}, new int[]{3, 1, 1, 2}};

    /* renamed from: e */
    static final int[][] f7895e = new int[20][];

    static {
        System.arraycopy(f7894d, 0, f7895e, 0, 10);
        for (int i = 10; i < 20; i++) {
            int[] iArr = f7894d[i - 10];
            int[] iArr2 = new int[iArr.length];
            for (int i2 = 0; i2 < iArr.length; i2++) {
                iArr2[i2] = iArr[(iArr.length - i2) - 1];
            }
            f7895e[i] = iArr2;
        }
    }

    /* renamed from: a */
    static boolean m12866a(CharSequence charSequence) {
        int length = charSequence.length();
        if (length == 0) {
            return false;
        }
        int i = length - 1;
        return m12867b(charSequence.subSequence(0, i)) == Character.digit(charSequence.charAt(i), 10);
    }

    /* renamed from: b */
    static int m12867b(CharSequence charSequence) {
        int length = charSequence.length();
        int i = 0;
        for (int i2 = length - 1; i2 >= 0; i2 -= 2) {
            int charAt = charSequence.charAt(i2) - '0';
            if (charAt < 0 || charAt > 9) {
                throw FormatException.m12669a();
            }
            i += charAt;
        }
        int i3 = i * 3;
        for (int i4 = length - 2; i4 >= 0; i4 -= 2) {
            int charAt2 = charSequence.charAt(i4) - '0';
            if (charAt2 < 0 || charAt2 > 9) {
                throw FormatException.m12669a();
            }
            i3 += charAt2;
        }
        return (1000 - i3) % 10;
    }
}
