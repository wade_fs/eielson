package p119e.p144d.p176c.p183m;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.FormatException;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.m.k */
public final class EAN8Writer extends UPCEANWriter {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        if (aVar == BarcodeFormat.EAN_8) {
            return super.mo23990a(str, aVar, i, i2, map);
        }
        throw new IllegalArgumentException("Can only encode EAN_8, but got ".concat(String.valueOf(aVar)));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.m.n.a(boolean[], int, int[], boolean):int
     arg types: [boolean[], int, int[], int]
     candidates:
      e.d.c.m.n.a(boolean[], int, int, int):e.d.c.k.b
      e.d.c.m.n.a(boolean[], int, int[], boolean):int */
    /* renamed from: a */
    public boolean[] mo24096a(String str) {
        int length = str.length();
        if (length == 7) {
            try {
                str = str + UPCEANReader.m12867b(str);
            } catch (FormatException e) {
                throw new IllegalArgumentException(e);
            }
        } else if (length == 8) {
            try {
                if (!UPCEANReader.m12866a(str)) {
                    throw new IllegalArgumentException("Contents do not pass checksum");
                }
            } catch (FormatException unused) {
                throw new IllegalArgumentException("Illegal contents");
            }
        } else {
            throw new IllegalArgumentException("Requested contents should be 8 digits long, but got ".concat(String.valueOf(length)));
        }
        boolean[] zArr = new boolean[67];
        int a = OneDimensionalCodeWriter.m12860a(zArr, 0, UPCEANReader.f7891a, true) + 0;
        for (int i = 0; i <= 3; i++) {
            a += OneDimensionalCodeWriter.m12860a(zArr, a, UPCEANReader.f7894d[Character.digit(str.charAt(i), 10)], false);
        }
        int a2 = a + OneDimensionalCodeWriter.m12860a(zArr, a, UPCEANReader.f7892b, false);
        for (int i2 = 4; i2 <= 7; i2++) {
            a2 += OneDimensionalCodeWriter.m12860a(zArr, a2, UPCEANReader.f7894d[Character.digit(str.charAt(i2), 10)], true);
        }
        OneDimensionalCodeWriter.m12860a(zArr, a2, UPCEANReader.f7891a, true);
        return zArr;
    }
}
