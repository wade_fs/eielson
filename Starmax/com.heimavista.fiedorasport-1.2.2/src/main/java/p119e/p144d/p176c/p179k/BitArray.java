package p119e.p144d.p176c.p179k;

import java.util.Arrays;
import p241io.jsonwebtoken.JwtParser;

/* renamed from: e.d.c.k.a */
public final class BitArray implements Cloneable {

    /* renamed from: P */
    private int[] f7788P;

    /* renamed from: Q */
    private int f7789Q;

    public BitArray() {
        this.f7789Q = 0;
        this.f7788P = new int[1];
    }

    /* renamed from: c */
    private static int[] m12713c(int i) {
        return new int[((i + 31) / 32)];
    }

    /* renamed from: a */
    public int mo24017a() {
        return this.f7789Q;
    }

    /* renamed from: b */
    public int mo24023b() {
        return (this.f7789Q + 7) / 8;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof BitArray)) {
            return false;
        }
        BitArray aVar = (BitArray) obj;
        if (this.f7789Q != aVar.f7789Q || !Arrays.equals(this.f7788P, aVar.f7788P)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.f7789Q * 31) + Arrays.hashCode(this.f7788P);
    }

    public String toString() {
        int i = this.f7789Q;
        StringBuilder sb = new StringBuilder(i + (i / 8) + 1);
        for (int i2 = 0; i2 < this.f7789Q; i2++) {
            if ((i2 & 7) == 0) {
                sb.append(' ');
            }
            sb.append(mo24022a(i2) ? 'X' : JwtParser.SEPARATOR_CHAR);
        }
        return sb.toString();
    }

    /* renamed from: b */
    private void m12712b(int i) {
        if (i > (this.f7788P.length << 5)) {
            int[] c = m12713c(i);
            int[] iArr = this.f7788P;
            System.arraycopy(iArr, 0, c, 0, iArr.length);
            this.f7788P = c;
        }
    }

    /* renamed from: a */
    public boolean mo24022a(int i) {
        return ((1 << (i & 31)) & this.f7788P[i / 32]) != 0;
    }

    public BitArray clone() {
        return new BitArray((int[]) this.f7788P.clone(), this.f7789Q);
    }

    /* renamed from: a */
    public void mo24021a(boolean z) {
        m12712b(this.f7789Q + 1);
        if (z) {
            int[] iArr = this.f7788P;
            int i = this.f7789Q;
            int i2 = i / 32;
            iArr[i2] = (1 << (i & 31)) | iArr[i2];
        }
        this.f7789Q++;
    }

    BitArray(int[] iArr, int i) {
        this.f7788P = iArr;
        this.f7789Q = i;
    }

    /* renamed from: a */
    public void mo24018a(int i, int i2) {
        if (i2 < 0 || i2 > 32) {
            throw new IllegalArgumentException("Num bits must be between 0 and 32");
        }
        m12712b(this.f7789Q + i2);
        while (i2 > 0) {
            boolean z = true;
            if (((i >> (i2 - 1)) & 1) != 1) {
                z = false;
            }
            mo24021a(z);
            i2--;
        }
    }

    /* renamed from: b */
    public void mo24024b(BitArray aVar) {
        if (this.f7789Q == aVar.f7789Q) {
            int i = 0;
            while (true) {
                int[] iArr = this.f7788P;
                if (i < iArr.length) {
                    iArr[i] = iArr[i] ^ aVar.f7788P[i];
                    i++;
                } else {
                    return;
                }
            }
        } else {
            throw new IllegalArgumentException("Sizes don't match");
        }
    }

    /* renamed from: a */
    public void mo24020a(BitArray aVar) {
        int i = aVar.f7789Q;
        m12712b(this.f7789Q + i);
        for (int i2 = 0; i2 < i; i2++) {
            mo24021a(aVar.mo24022a(i2));
        }
    }

    /* renamed from: a */
    public void mo24019a(int i, byte[] bArr, int i2, int i3) {
        int i4 = i;
        int i5 = 0;
        while (i5 < i3) {
            int i6 = i4;
            int i7 = 0;
            for (int i8 = 0; i8 < 8; i8++) {
                if (mo24022a(i6)) {
                    i7 |= 1 << (7 - i8);
                }
                i6++;
            }
            bArr[i2 + i5] = (byte) i7;
            i5++;
            i4 = i6;
        }
    }
}
