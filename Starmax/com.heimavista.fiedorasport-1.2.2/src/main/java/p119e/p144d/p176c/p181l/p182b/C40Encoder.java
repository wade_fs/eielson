package p119e.p144d.p176c.p181l.p182b;

/* renamed from: e.d.c.l.b.c */
class C40Encoder implements C4190g {
    C40Encoder() {
    }

    /* renamed from: b */
    static void m12761b(EncoderContext hVar, StringBuilder sb) {
        hVar.mo24075a(m12760a(sb, 0));
        sb.delete(0, 3);
    }

    /* renamed from: a */
    public int mo24062a() {
        return 1;
    }

    /* renamed from: a */
    public void mo24060a(EncoderContext hVar) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!hVar.mo24084h()) {
                break;
            }
            char c = hVar.mo24078c();
            hVar.f7851f++;
            int a = mo24063a(c, sb);
            int a2 = hVar.mo24070a() + ((sb.length() / 3) << 1);
            hVar.mo24079c(a2);
            int a3 = hVar.mo24083g().mo24088a() - a2;
            if (hVar.mo24084h()) {
                if (sb.length() % 3 == 0 && C4191j.m12808a(hVar.mo24080d(), hVar.f7851f, mo24062a()) != mo24062a()) {
                    hVar.mo24077b(0);
                    break;
                }
            } else {
                StringBuilder sb2 = new StringBuilder();
                if (sb.length() % 3 == 2 && (a3 < 2 || a3 > 2)) {
                    a = m12759a(hVar, sb, sb2, a);
                }
                while (sb.length() % 3 == 1 && ((a <= 3 && a3 != 1) || a > 3)) {
                    a = m12759a(hVar, sb, sb2, a);
                }
            }
        }
        mo24064a(hVar, sb);
    }

    /* renamed from: a */
    private int m12759a(EncoderContext hVar, StringBuilder sb, StringBuilder sb2, int i) {
        int length = sb.length();
        sb.delete(length - i, length);
        hVar.f7851f--;
        int a = mo24063a(hVar.mo24078c(), sb2);
        hVar.mo24086j();
        return a;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo24064a(EncoderContext hVar, StringBuilder sb) {
        int length = sb.length() % 3;
        int a = hVar.mo24070a() + ((sb.length() / 3) << 1);
        hVar.mo24079c(a);
        int a2 = hVar.mo24083g().mo24088a() - a;
        if (length == 2) {
            sb.append(0);
            while (sb.length() >= 3) {
                m12761b(hVar, sb);
            }
            if (hVar.mo24084h()) {
                hVar.mo24071a(254);
            }
        } else if (a2 == 1 && length == 1) {
            while (sb.length() >= 3) {
                m12761b(hVar, sb);
            }
            if (hVar.mo24084h()) {
                hVar.mo24071a(254);
            }
            hVar.f7851f--;
        } else if (length == 0) {
            while (sb.length() >= 3) {
                m12761b(hVar, sb);
            }
            if (a2 > 0 || hVar.mo24084h()) {
                hVar.mo24071a(254);
            }
        } else {
            throw new IllegalStateException("Unexpected case. Please report!");
        }
        hVar.mo24077b(0);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo24063a(char c, StringBuilder sb) {
        if (c == ' ') {
            sb.append(3);
            return 1;
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
            return 1;
        } else if (c >= 'A' && c <= 'Z') {
            sb.append((char) ((c - 'A') + 14));
            return 1;
        } else if (c < ' ') {
            sb.append(0);
            sb.append(c);
            return 2;
        } else if (c >= '!' && c <= '/') {
            sb.append(1);
            sb.append((char) (c - '!'));
            return 2;
        } else if (c >= ':' && c <= '@') {
            sb.append(1);
            sb.append((char) ((c - ':') + 15));
            return 2;
        } else if (c >= '[' && c <= '_') {
            sb.append(1);
            sb.append((char) ((c - '[') + 22));
            return 2;
        } else if (c < '`' || c > 127) {
            sb.append("\u0001\u001e");
            return mo24063a((char) (c - 128), sb) + 2;
        } else {
            sb.append(2);
            sb.append((char) (c - '`'));
            return 2;
        }
    }

    /* renamed from: a */
    private static String m12760a(CharSequence charSequence, int i) {
        int charAt = (charSequence.charAt(i) * 1600) + (charSequence.charAt(i + 1) * '(') + charSequence.charAt(i + 2) + 1;
        return new String(new char[]{(char) (charAt / 256), (char) (charAt % 256)});
    }
}
