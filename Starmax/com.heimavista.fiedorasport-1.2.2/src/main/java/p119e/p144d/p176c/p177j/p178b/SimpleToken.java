package p119e.p144d.p176c.p177j.p178b;

import p119e.p144d.p176c.p179k.BitArray;

/* renamed from: e.d.c.j.b.e */
final class SimpleToken extends Token {

    /* renamed from: c */
    private final short f7779c;

    /* renamed from: d */
    private final short f7780d;

    SimpleToken(Token gVar, int i, int i2) {
        super(super);
        this.f7779c = (short) i;
        this.f7780d = (short) i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo23998a(BitArray aVar, byte[] bArr) {
        aVar.mo24018a(this.f7779c, this.f7780d);
    }

    public String toString() {
        short s = this.f7779c;
        short s2 = this.f7780d;
        short s3 = (s & ((1 << s2) - 1)) | (1 << s2);
        return "<" + Integer.toBinaryString(s3 | (1 << this.f7780d)).substring(1) + '>';
    }
}
