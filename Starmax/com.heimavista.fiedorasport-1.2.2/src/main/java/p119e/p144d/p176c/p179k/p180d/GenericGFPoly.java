package p119e.p144d.p176c.p179k.p180d;

/* renamed from: e.d.c.k.d.b */
final class GenericGFPoly {

    /* renamed from: a */
    private final GenericGF f7838a;

    /* renamed from: b */
    private final int[] f7839b;

    GenericGFPoly(GenericGF aVar, int[] iArr) {
        if (iArr.length != 0) {
            this.f7838a = aVar;
            int length = iArr.length;
            if (length <= 1 || iArr[0] != 0) {
                this.f7839b = iArr;
                return;
            }
            int i = 1;
            while (i < length && iArr[i] == 0) {
                i++;
            }
            if (i == length) {
                this.f7839b = new int[]{0};
                return;
            }
            this.f7839b = new int[(length - i)];
            int[] iArr2 = this.f7839b;
            System.arraycopy(iArr, i, iArr2, 0, iArr2.length);
            return;
        }
        throw new IllegalArgumentException();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int[] mo24052a() {
        return this.f7839b;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public int mo24053b() {
        return this.f7839b.length - 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public boolean mo24056c() {
        return this.f7839b[0] == 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(mo24053b() * 8);
        for (int b = mo24053b(); b >= 0; b--) {
            int a = mo24049a(b);
            if (a != 0) {
                if (a < 0) {
                    sb.append(" - ");
                    a = -a;
                } else if (sb.length() > 0) {
                    sb.append(" + ");
                }
                if (b == 0 || a != 1) {
                    int c = this.f7838a.mo24047c(a);
                    if (c == 0) {
                        sb.append('1');
                    } else if (c == 1) {
                        sb.append('a');
                    } else {
                        sb.append("a^");
                        sb.append(c);
                    }
                }
                if (b != 0) {
                    if (b == 1) {
                        sb.append('x');
                    } else {
                        sb.append("x^");
                        sb.append(b);
                    }
                }
            }
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo24049a(int i) {
        int[] iArr = this.f7839b;
        return iArr[(iArr.length - 1) - i];
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public GenericGFPoly[] mo24054b(GenericGFPoly bVar) {
        if (!this.f7838a.equals(bVar.f7838a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (!bVar.mo24056c()) {
            GenericGFPoly b = this.f7838a.mo24046b();
            int b2 = this.f7838a.mo24044b(bVar.mo24049a(bVar.mo24053b()));
            GenericGFPoly bVar2 = b;
            GenericGFPoly bVar3 = this;
            while (bVar3.mo24053b() >= bVar.mo24053b() && !bVar3.mo24056c()) {
                int b3 = bVar3.mo24053b() - bVar.mo24053b();
                int b4 = this.f7838a.mo24045b(bVar3.mo24049a(bVar3.mo24053b()), b2);
                GenericGFPoly a = bVar.mo24050a(b3, b4);
                bVar2 = bVar2.mo24051a(this.f7838a.mo24043a(b3, b4));
                bVar3 = bVar3.mo24051a(a);
            }
            return new GenericGFPoly[]{bVar2, bVar3};
        } else {
            throw new IllegalArgumentException("Divide by 0");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public GenericGFPoly mo24055c(GenericGFPoly bVar) {
        if (!this.f7838a.equals(bVar.f7838a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (mo24056c() || bVar.mo24056c()) {
            return this.f7838a.mo24046b();
        } else {
            int[] iArr = this.f7839b;
            int length = iArr.length;
            int[] iArr2 = bVar.f7839b;
            int length2 = iArr2.length;
            int[] iArr3 = new int[((length + length2) - 1)];
            for (int i = 0; i < length; i++) {
                int i2 = iArr[i];
                for (int i3 = 0; i3 < length2; i3++) {
                    int i4 = i + i3;
                    iArr3[i4] = GenericGF.m12732c(iArr3[i4], this.f7838a.mo24045b(i2, iArr2[i3]));
                }
            }
            return new GenericGFPoly(this.f7838a, iArr3);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public GenericGFPoly mo24051a(GenericGFPoly bVar) {
        if (!this.f7838a.equals(bVar.f7838a)) {
            throw new IllegalArgumentException("GenericGFPolys do not have same GenericGF field");
        } else if (mo24056c()) {
            return bVar;
        } else {
            if (bVar.mo24056c()) {
                return this;
            }
            int[] iArr = this.f7839b;
            int[] iArr2 = bVar.f7839b;
            if (iArr.length > iArr2.length) {
                int[] iArr3 = iArr;
                iArr = iArr2;
                iArr2 = iArr3;
            }
            int[] iArr4 = new int[iArr2.length];
            int length = iArr2.length - iArr.length;
            System.arraycopy(iArr2, 0, iArr4, 0, length);
            for (int i = length; i < iArr2.length; i++) {
                iArr4[i] = GenericGF.m12732c(iArr[i - length], iArr2[i]);
            }
            return new GenericGFPoly(this.f7838a, iArr4);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public GenericGFPoly mo24050a(int i, int i2) {
        if (i < 0) {
            throw new IllegalArgumentException();
        } else if (i2 == 0) {
            return this.f7838a.mo24046b();
        } else {
            int length = this.f7839b.length;
            int[] iArr = new int[(i + length)];
            for (int i3 = 0; i3 < length; i3++) {
                iArr[i3] = this.f7838a.mo24045b(this.f7839b[i3], i2);
            }
            return new GenericGFPoly(this.f7838a, iArr);
        }
    }
}
