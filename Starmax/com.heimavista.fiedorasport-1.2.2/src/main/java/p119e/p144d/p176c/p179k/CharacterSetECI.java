package p119e.p144d.p176c.p179k;

import com.google.android.exoplayer2.C1750C;
import java.util.HashMap;
import java.util.Map;

/* renamed from: e.d.c.k.c */
public enum CharacterSetECI {
    Cp437(new int[]{0, 2}, new String[0]),
    ISO8859_1(new int[]{1, 3}, "ISO-8859-1"),
    ISO8859_2(4, "ISO-8859-2"),
    ISO8859_3(5, "ISO-8859-3"),
    ISO8859_4(6, "ISO-8859-4"),
    ISO8859_5(7, "ISO-8859-5"),
    ISO8859_6(8, "ISO-8859-6"),
    ISO8859_7(9, "ISO-8859-7"),
    ISO8859_8(10, "ISO-8859-8"),
    ISO8859_9(11, "ISO-8859-9"),
    ISO8859_10(12, "ISO-8859-10"),
    ISO8859_11(13, "ISO-8859-11"),
    ISO8859_13(15, "ISO-8859-13"),
    ISO8859_14(16, "ISO-8859-14"),
    ISO8859_15(17, "ISO-8859-15"),
    ISO8859_16(18, "ISO-8859-16"),
    SJIS(20, "Shift_JIS"),
    Cp1250(21, "windows-1250"),
    Cp1251(22, "windows-1251"),
    Cp1252(23, "windows-1252"),
    Cp1256(24, "windows-1256"),
    UnicodeBigUnmarked(25, "UTF-16BE", "UnicodeBig"),
    UTF8(26, C1750C.UTF8_NAME),
    ASCII(new int[]{27, 170}, C1750C.ASCII_NAME),
    Big5(28),
    GB18030(29, "GB2312", "EUC_CN", "GBK"),
    EUC_KR(30, "EUC-KR");
    

    /* renamed from: s0 */
    private static final Map<Integer, CharacterSetECI> f7821s0 = new HashMap();

    /* renamed from: t0 */
    private static final Map<String, CharacterSetECI> f7822t0 = new HashMap();

    /* renamed from: P */
    private final int[] f7824P;

    /* renamed from: Q */
    private final String[] f7825Q;

    static {
        CharacterSetECI[] values = values();
        for (CharacterSetECI cVar : values) {
            for (int i : cVar.f7824P) {
                f7821s0.put(Integer.valueOf(i), cVar);
            }
            f7822t0.put(super.name(), cVar);
            for (String str : cVar.f7825Q) {
                f7822t0.put(str, cVar);
            }
        }
    }

    private CharacterSetECI(int i) {
        this(r3, r4, new int[]{i}, new String[0]);
    }

    /* renamed from: a */
    public int mo24040a() {
        return this.f7824P[0];
    }

    private CharacterSetECI(int i, String... strArr) {
        this.f7824P = new int[]{i};
        this.f7825Q = strArr;
    }

    /* renamed from: a */
    public static CharacterSetECI m12730a(String str) {
        return f7822t0.get(str);
    }

    private CharacterSetECI(int[] iArr, String... strArr) {
        this.f7824P = iArr;
        this.f7825Q = strArr;
    }
}
