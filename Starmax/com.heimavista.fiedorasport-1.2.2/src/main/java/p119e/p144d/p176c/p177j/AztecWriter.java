package p119e.p144d.p176c.p177j;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.Writer;
import p119e.p144d.p176c.p177j.p178b.AztecCode;
import p119e.p144d.p176c.p177j.p178b.C4188c;
import p119e.p144d.p176c.p179k.BitMatrix;

/* renamed from: e.d.c.j.a */
public final class AztecWriter implements Writer {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        int i3;
        int i4;
        Charset charset;
        Charset charset2 = StandardCharsets.ISO_8859_1;
        int i5 = 33;
        if (map != null) {
            if (map.containsKey(EncodeHintType.CHARACTER_SET)) {
                charset2 = Charset.forName(map.get(EncodeHintType.CHARACTER_SET).toString());
            }
            if (map.containsKey(EncodeHintType.ERROR_CORRECTION)) {
                i5 = Integer.parseInt(map.get(EncodeHintType.ERROR_CORRECTION).toString());
            }
            if (map.containsKey(EncodeHintType.AZTEC_LAYERS)) {
                charset = charset2;
                i4 = i5;
                i3 = Integer.parseInt(map.get(EncodeHintType.AZTEC_LAYERS).toString());
                return m12673a(str, aVar, i, i2, charset, i4, i3);
            }
            charset = charset2;
            i4 = i5;
        } else {
            charset = charset2;
            i4 = 33;
        }
        i3 = 0;
        return m12673a(str, aVar, i, i2, charset, i4, i3);
    }

    /* renamed from: a */
    private static BitMatrix m12673a(String str, BarcodeFormat aVar, int i, int i2, Charset charset, int i3, int i4) {
        if (aVar == BarcodeFormat.AZTEC) {
            return m12672a(C4188c.m12683a(str.getBytes(charset), i3, i4), i, i2);
        }
        throw new IllegalArgumentException("Can only encode AZTEC, but got ".concat(String.valueOf(aVar)));
    }

    /* renamed from: a */
    private static BitMatrix m12672a(AztecCode aVar, int i, int i2) {
        BitMatrix a = aVar.mo23992a();
        if (a != null) {
            int c = a.mo24035c();
            int b = a.mo24033b();
            int max = Math.max(i, c);
            int max2 = Math.max(i2, b);
            int min = Math.min(max / c, max2 / b);
            int i3 = (max - (c * min)) / 2;
            int i4 = (max2 - (b * min)) / 2;
            BitMatrix bVar = new BitMatrix(max, max2);
            int i5 = 0;
            while (i5 < b) {
                int i6 = i3;
                int i7 = 0;
                while (i7 < c) {
                    if (a.mo24032a(i7, i5)) {
                        bVar.mo24031a(i6, i4, min, min);
                    }
                    i7++;
                    i6 += min;
                }
                i5++;
                i4 += min;
            }
            return bVar;
        }
        throw new IllegalStateException();
    }
}
