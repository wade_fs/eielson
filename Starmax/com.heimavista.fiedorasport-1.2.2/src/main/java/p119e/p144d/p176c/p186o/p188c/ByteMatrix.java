package p119e.p144d.p176c.p186o.p188c;

import java.lang.reflect.Array;
import java.util.Arrays;

/* renamed from: e.d.c.o.c.b */
public final class ByteMatrix {

    /* renamed from: a */
    private final byte[][] f7957a;

    /* renamed from: b */
    private final int f7958b;

    /* renamed from: c */
    private final int f7959c;

    public ByteMatrix(int i, int i2) {
        this.f7957a = (byte[][]) Array.newInstance(byte.class, i2, i);
        this.f7958b = i;
        this.f7959c = i2;
    }

    /* renamed from: a */
    public byte mo24129a(int i, int i2) {
        return this.f7957a[i2][i];
    }

    /* renamed from: b */
    public int mo24134b() {
        return this.f7959c;
    }

    /* renamed from: c */
    public int mo24135c() {
        return this.f7958b;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder((this.f7958b * 2 * this.f7959c) + 2);
        for (int i = 0; i < this.f7959c; i++) {
            byte[] bArr = this.f7957a[i];
            for (int i2 = 0; i2 < this.f7958b; i2++) {
                byte b = bArr[i2];
                if (b == 0) {
                    sb.append(" 0");
                } else if (b != 1) {
                    sb.append("  ");
                } else {
                    sb.append(" 1");
                }
            }
            sb.append(10);
        }
        return sb.toString();
    }

    /* renamed from: a */
    public byte[][] mo24133a() {
        return this.f7957a;
    }

    /* renamed from: a */
    public void mo24131a(int i, int i2, int i3) {
        this.f7957a[i2][i] = (byte) i3;
    }

    /* renamed from: a */
    public void mo24132a(int i, int i2, boolean z) {
        this.f7957a[i2][i] = z ? (byte) 1 : 0;
    }

    /* renamed from: a */
    public void mo24130a(byte b) {
        for (byte[] bArr : this.f7957a) {
            Arrays.fill(bArr, b);
        }
    }
}
