package p119e.p144d.p176c.p177j.p178b;

import p119e.p144d.p176c.p179k.BitArray;

/* renamed from: e.d.c.j.b.b */
final class BinaryShiftToken extends Token {

    /* renamed from: c */
    private final short f7771c;

    /* renamed from: d */
    private final short f7772d;

    BinaryShiftToken(Token gVar, int i, int i2) {
        super(super);
        this.f7771c = (short) i;
        this.f7772d = (short) i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(int, int):int}
     arg types: [short, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(float, float):float}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int} */
    /* renamed from: a */
    public void mo23998a(BitArray aVar, byte[] bArr) {
        int i = 0;
        while (true) {
            short s = this.f7772d;
            if (i < s) {
                if (i == 0 || (i == 31 && s <= 62)) {
                    aVar.mo24018a(31, 5);
                    short s2 = this.f7772d;
                    if (s2 > 62) {
                        aVar.mo24018a(s2 - 31, 16);
                    } else if (i == 0) {
                        aVar.mo24018a(Math.min((int) s2, 31), 5);
                    } else {
                        aVar.mo24018a(s2 - 31, 5);
                    }
                }
                aVar.mo24018a(bArr[this.f7771c + i], 8);
                i++;
            } else {
                return;
            }
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("<");
        sb.append((int) this.f7771c);
        sb.append("::");
        sb.append((this.f7771c + this.f7772d) - 1);
        sb.append('>');
        return sb.toString();
    }
}
