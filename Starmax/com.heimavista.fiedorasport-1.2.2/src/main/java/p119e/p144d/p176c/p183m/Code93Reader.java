package p119e.p144d.p176c.p183m;

import com.facebook.internal.FacebookRequestErrorClassification;

/* renamed from: e.d.c.m.g */
public final class Code93Reader extends OneDReader {

    /* renamed from: a */
    static final int[] f7885a;

    static {
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ-. $/+%abcd*".toCharArray();
        int[] iArr = {276, 328, 324, 322, 296, 292, 290, 336, 274, 266, 424, 420, 418, 404, 402, 394, 360, 356, 354, 308, 282, 344, 332, 326, 300, 278, 436, 434, 428, 422, 406, 410, 364, 358, 310, 314, 302, 468, 466, FacebookRequestErrorClassification.ESC_APP_NOT_INSTALLED, 366, 374, 430, 294, 474, 470, 306, 350};
        f7885a = iArr;
        int i = iArr[47];
    }
}
