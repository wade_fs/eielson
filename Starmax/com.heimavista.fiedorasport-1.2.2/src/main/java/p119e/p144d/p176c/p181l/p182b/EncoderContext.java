package p119e.p144d.p176c.p181l.p182b;

import java.nio.charset.StandardCharsets;
import p119e.p144d.p176c.Dimension;

/* renamed from: e.d.c.l.b.h */
final class EncoderContext {

    /* renamed from: a */
    private final String f7846a;

    /* renamed from: b */
    private SymbolShapeHint f7847b;

    /* renamed from: c */
    private Dimension f7848c;

    /* renamed from: d */
    private Dimension f7849d;

    /* renamed from: e */
    private final StringBuilder f7850e;

    /* renamed from: f */
    int f7851f;

    /* renamed from: g */
    private int f7852g;

    /* renamed from: h */
    private SymbolInfo f7853h;

    /* renamed from: i */
    private int f7854i;

    EncoderContext(String str) {
        byte[] bytes = str.getBytes(StandardCharsets.ISO_8859_1);
        StringBuilder sb = new StringBuilder(bytes.length);
        int length = bytes.length;
        int i = 0;
        while (i < length) {
            char c = (char) (bytes[i] & 255);
            if (c != '?' || str.charAt(i) == '?') {
                sb.append(c);
                i++;
            } else {
                throw new IllegalArgumentException("Message contains characters outside ISO-8859-1 encoding.");
            }
        }
        this.f7846a = sb.toString();
        this.f7847b = SymbolShapeHint.FORCE_NONE;
        this.f7850e = new StringBuilder(str.length());
        this.f7852g = -1;
    }

    /* renamed from: l */
    private int m12784l() {
        return this.f7846a.length() - this.f7854i;
    }

    /* renamed from: a */
    public void mo24074a(SymbolShapeHint lVar) {
        this.f7847b = lVar;
    }

    /* renamed from: b */
    public StringBuilder mo24076b() {
        return this.f7850e;
    }

    /* renamed from: c */
    public char mo24078c() {
        return this.f7846a.charAt(this.f7851f);
    }

    /* renamed from: d */
    public String mo24080d() {
        return this.f7846a;
    }

    /* renamed from: e */
    public int mo24081e() {
        return this.f7852g;
    }

    /* renamed from: f */
    public int mo24082f() {
        return m12784l() - this.f7851f;
    }

    /* renamed from: g */
    public SymbolInfo mo24083g() {
        return this.f7853h;
    }

    /* renamed from: h */
    public boolean mo24084h() {
        return this.f7851f < m12784l();
    }

    /* renamed from: i */
    public void mo24085i() {
        this.f7852g = -1;
    }

    /* renamed from: j */
    public void mo24086j() {
        this.f7853h = null;
    }

    /* renamed from: k */
    public void mo24087k() {
        mo24079c(mo24070a());
    }

    /* renamed from: a */
    public void mo24073a(Dimension bVar, Dimension bVar2) {
        this.f7848c = bVar;
        this.f7849d = bVar2;
    }

    /* renamed from: b */
    public void mo24077b(int i) {
        this.f7852g = i;
    }

    /* renamed from: c */
    public void mo24079c(int i) {
        SymbolInfo kVar = this.f7853h;
        if (kVar == null || i > kVar.mo24088a()) {
            this.f7853h = SymbolInfo.m12821a(i, this.f7847b, this.f7848c, this.f7849d, true);
        }
    }

    /* renamed from: a */
    public void mo24072a(int i) {
        this.f7854i = i;
    }

    /* renamed from: a */
    public void mo24075a(String str) {
        this.f7850e.append(str);
    }

    /* renamed from: a */
    public void mo24071a(char c) {
        this.f7850e.append(c);
    }

    /* renamed from: a */
    public int mo24070a() {
        return this.f7850e.length();
    }
}
