package p119e.p144d.p176c.p186o.p187b;

import com.google.android.exoplayer2.extractor.p085ts.TsExtractor;

/* renamed from: e.d.c.o.b.c */
public final class Version {

    /* renamed from: d */
    private static final Version[] f7947d = m12919d();

    /* renamed from: a */
    private final int f7948a;

    /* renamed from: b */
    private final C4195b[] f7949b;

    /* renamed from: c */
    private final int f7950c;

    /* renamed from: e.d.c.o.b.c$a */
    /* compiled from: Version */
    public static final class C4194a {

        /* renamed from: a */
        private final int f7951a;

        /* renamed from: b */
        private final int f7952b;

        C4194a(int i, int i2) {
            this.f7951a = i;
            this.f7952b = i2;
        }

        /* renamed from: a */
        public int mo24121a() {
            return this.f7951a;
        }

        /* renamed from: b */
        public int mo24122b() {
            return this.f7952b;
        }
    }

    /* renamed from: e.d.c.o.b.c$b */
    /* compiled from: Version */
    public static final class C4195b {

        /* renamed from: a */
        private final int f7953a;

        /* renamed from: b */
        private final C4194a[] f7954b;

        C4195b(int i, C4194a... aVarArr) {
            this.f7953a = i;
            this.f7954b = aVarArr;
        }

        /* renamed from: a */
        public C4194a[] mo24123a() {
            return this.f7954b;
        }

        /* renamed from: b */
        public int mo24124b() {
            return this.f7953a;
        }

        /* renamed from: c */
        public int mo24125c() {
            int i = 0;
            for (C4194a aVar : this.f7954b) {
                i += aVar.mo24121a();
            }
            return i;
        }

        /* renamed from: d */
        public int mo24126d() {
            return this.f7953a * mo24125c();
        }
    }

    private Version(int i, int[] iArr, C4195b... bVarArr) {
        this.f7948a = i;
        this.f7949b = bVarArr;
        int b = bVarArr[0].mo24124b();
        C4194a[] a = bVarArr[0].mo24123a();
        int i2 = 0;
        for (C4194a aVar : a) {
            i2 += aVar.mo24121a() * (aVar.mo24122b() + b);
        }
        this.f7950c = i2;
    }

    /* renamed from: d */
    private static Version[] m12919d() {
        return new Version[]{new Version(1, new int[0], new C4195b(7, new C4194a(1, 19)), new C4195b(10, new C4194a(1, 16)), new C4195b(13, new C4194a(1, 13)), new C4195b(17, new C4194a(1, 9))), new Version(2, new int[]{6, 18}, new C4195b(10, new C4194a(1, 34)), new C4195b(16, new C4194a(1, 28)), new C4195b(22, new C4194a(1, 22)), new C4195b(28, new C4194a(1, 16))), new Version(3, new int[]{6, 22}, new C4195b(15, new C4194a(1, 55)), new C4195b(26, new C4194a(1, 44)), new C4195b(18, new C4194a(2, 17)), new C4195b(22, new C4194a(2, 13))), new Version(4, new int[]{6, 26}, new C4195b(20, new C4194a(1, 80)), new C4195b(18, new C4194a(2, 32)), new C4195b(26, new C4194a(2, 24)), new C4195b(16, new C4194a(4, 9))), new Version(5, new int[]{6, 30}, new C4195b(26, new C4194a(1, 108)), new C4195b(24, new C4194a(2, 43)), new C4195b(18, new C4194a(2, 15), new C4194a(2, 16)), new C4195b(22, new C4194a(2, 11), new C4194a(2, 12))), new Version(6, new int[]{6, 34}, new C4195b(18, new C4194a(2, 68)), new C4195b(16, new C4194a(4, 27)), new C4195b(24, new C4194a(4, 19)), new C4195b(28, new C4194a(4, 15))), new Version(7, new int[]{6, 22, 38}, new C4195b(20, new C4194a(2, 78)), new C4195b(18, new C4194a(4, 31)), new C4195b(18, new C4194a(2, 14), new C4194a(4, 15)), new C4195b(26, new C4194a(4, 13), new C4194a(1, 14))), new Version(8, new int[]{6, 24, 42}, new C4195b(24, new C4194a(2, 97)), new C4195b(22, new C4194a(2, 38), new C4194a(2, 39)), new C4195b(22, new C4194a(4, 18), new C4194a(2, 19)), new C4195b(26, new C4194a(4, 14), new C4194a(2, 15))), new Version(9, new int[]{6, 26, 46}, new C4195b(30, new C4194a(2, 116)), new C4195b(22, new C4194a(3, 36), new C4194a(2, 37)), new C4195b(20, new C4194a(4, 16), new C4194a(4, 17)), new C4195b(24, new C4194a(4, 12), new C4194a(4, 13))), new Version(10, new int[]{6, 28, 50}, new C4195b(18, new C4194a(2, 68), new C4194a(2, 69)), new C4195b(26, new C4194a(4, 43), new C4194a(1, 44)), new C4195b(24, new C4194a(6, 19), new C4194a(2, 20)), new C4195b(28, new C4194a(6, 15), new C4194a(2, 16))), new Version(11, new int[]{6, 30, 54}, new C4195b(20, new C4194a(4, 81)), new C4195b(30, new C4194a(1, 50), new C4194a(4, 51)), new C4195b(28, new C4194a(4, 22), new C4194a(4, 23)), new C4195b(24, new C4194a(3, 12), new C4194a(8, 13))), new Version(12, new int[]{6, 32, 58}, new C4195b(24, new C4194a(2, 92), new C4194a(2, 93)), new C4195b(22, new C4194a(6, 36), new C4194a(2, 37)), new C4195b(26, new C4194a(4, 20), new C4194a(6, 21)), new C4195b(28, new C4194a(7, 14), new C4194a(4, 15))), new Version(13, new int[]{6, 34, 62}, new C4195b(26, new C4194a(4, 107)), new C4195b(22, new C4194a(8, 37), new C4194a(1, 38)), new C4195b(24, new C4194a(8, 20), new C4194a(4, 21)), new C4195b(22, new C4194a(12, 11), new C4194a(4, 12))), new Version(14, new int[]{6, 26, 46, 66}, new C4195b(30, new C4194a(3, 115), new C4194a(1, 116)), new C4195b(24, new C4194a(4, 40), new C4194a(5, 41)), new C4195b(20, new C4194a(11, 16), new C4194a(5, 17)), new C4195b(24, new C4194a(11, 12), new C4194a(5, 13))), new Version(15, new int[]{6, 26, 48, 70}, new C4195b(22, new C4194a(5, 87), new C4194a(1, 88)), new C4195b(24, new C4194a(5, 41), new C4194a(5, 42)), new C4195b(30, new C4194a(5, 24), new C4194a(7, 25)), new C4195b(24, new C4194a(11, 12), new C4194a(7, 13))), new Version(16, new int[]{6, 26, 50, 74}, new C4195b(24, new C4194a(5, 98), new C4194a(1, 99)), new C4195b(28, new C4194a(7, 45), new C4194a(3, 46)), new C4195b(24, new C4194a(15, 19), new C4194a(2, 20)), new C4195b(30, new C4194a(3, 15), new C4194a(13, 16))), new Version(17, new int[]{6, 30, 54, 78}, new C4195b(28, new C4194a(1, 107), new C4194a(5, 108)), new C4195b(28, new C4194a(10, 46), new C4194a(1, 47)), new C4195b(28, new C4194a(1, 22), new C4194a(15, 23)), new C4195b(28, new C4194a(2, 14), new C4194a(17, 15))), new Version(18, new int[]{6, 30, 56, 82}, new C4195b(30, new C4194a(5, 120), new C4194a(1, 121)), new C4195b(26, new C4194a(9, 43), new C4194a(4, 44)), new C4195b(28, new C4194a(17, 22), new C4194a(1, 23)), new C4195b(28, new C4194a(2, 14), new C4194a(19, 15))), new Version(19, new int[]{6, 30, 58, 86}, new C4195b(28, new C4194a(3, 113), new C4194a(4, 114)), new C4195b(26, new C4194a(3, 44), new C4194a(11, 45)), new C4195b(26, new C4194a(17, 21), new C4194a(4, 22)), new C4195b(26, new C4194a(9, 13), new C4194a(16, 14))), new Version(20, new int[]{6, 34, 62, 90}, new C4195b(28, new C4194a(3, 107), new C4194a(5, 108)), new C4195b(26, new C4194a(3, 41), new C4194a(13, 42)), new C4195b(30, new C4194a(15, 24), new C4194a(5, 25)), new C4195b(28, new C4194a(15, 15), new C4194a(10, 16))), new Version(21, new int[]{6, 28, 50, 72, 94}, new C4195b(28, new C4194a(4, 116), new C4194a(4, 117)), new C4195b(26, new C4194a(17, 42)), new C4195b(28, new C4194a(17, 22), new C4194a(6, 23)), new C4195b(30, new C4194a(19, 16), new C4194a(6, 17))), new Version(22, new int[]{6, 26, 50, 74, 98}, new C4195b(28, new C4194a(2, 111), new C4194a(7, 112)), new C4195b(28, new C4194a(17, 46)), new C4195b(30, new C4194a(7, 24), new C4194a(16, 25)), new C4195b(24, new C4194a(34, 13))), new Version(23, new int[]{6, 30, 54, 78, 102}, new C4195b(30, new C4194a(4, 121), new C4194a(5, 122)), new C4195b(28, new C4194a(4, 47), new C4194a(14, 48)), new C4195b(30, new C4194a(11, 24), new C4194a(14, 25)), new C4195b(30, new C4194a(16, 15), new C4194a(14, 16))), new Version(24, new int[]{6, 28, 54, 80, 106}, new C4195b(30, new C4194a(6, 117), new C4194a(4, 118)), new C4195b(28, new C4194a(6, 45), new C4194a(14, 46)), new C4195b(30, new C4194a(11, 24), new C4194a(16, 25)), new C4195b(30, new C4194a(30, 16), new C4194a(2, 17))), new Version(25, new int[]{6, 32, 58, 84, 110}, new C4195b(26, new C4194a(8, 106), new C4194a(4, 107)), new C4195b(28, new C4194a(8, 47), new C4194a(13, 48)), new C4195b(30, new C4194a(7, 24), new C4194a(22, 25)), new C4195b(30, new C4194a(22, 15), new C4194a(13, 16))), new Version(26, new int[]{6, 30, 58, 86, 114}, new C4195b(28, new C4194a(10, 114), new C4194a(2, 115)), new C4195b(28, new C4194a(19, 46), new C4194a(4, 47)), new C4195b(28, new C4194a(28, 22), new C4194a(6, 23)), new C4195b(30, new C4194a(33, 16), new C4194a(4, 17))), new Version(27, new int[]{6, 34, 62, 90, 118}, new C4195b(30, new C4194a(8, 122), new C4194a(4, 123)), new C4195b(28, new C4194a(22, 45), new C4194a(3, 46)), new C4195b(30, new C4194a(8, 23), new C4194a(26, 24)), new C4195b(30, new C4194a(12, 15), new C4194a(28, 16))), new Version(28, new int[]{6, 26, 50, 74, 98, 122}, new C4195b(30, new C4194a(3, 117), new C4194a(10, 118)), new C4195b(28, new C4194a(3, 45), new C4194a(23, 46)), new C4195b(30, new C4194a(4, 24), new C4194a(31, 25)), new C4195b(30, new C4194a(11, 15), new C4194a(31, 16))), new Version(29, new int[]{6, 30, 54, 78, 102, 126}, new C4195b(30, new C4194a(7, 116), new C4194a(7, 117)), new C4195b(28, new C4194a(21, 45), new C4194a(7, 46)), new C4195b(30, new C4194a(1, 23), new C4194a(37, 24)), new C4195b(30, new C4194a(19, 15), new C4194a(26, 16))), new Version(30, new int[]{6, 26, 52, 78, 104, TsExtractor.TS_STREAM_TYPE_HDMV_DTS}, new C4195b(30, new C4194a(5, 115), new C4194a(10, 116)), new C4195b(28, new C4194a(19, 47), new C4194a(10, 48)), new C4195b(30, new C4194a(15, 24), new C4194a(25, 25)), new C4195b(30, new C4194a(23, 15), new C4194a(25, 16))), new Version(31, new int[]{6, 30, 56, 82, 108, TsExtractor.TS_STREAM_TYPE_SPLICE_INFO}, new C4195b(30, new C4194a(13, 115), new C4194a(3, 116)), new C4195b(28, new C4194a(2, 46), new C4194a(29, 47)), new C4195b(30, new C4194a(42, 24), new C4194a(1, 25)), new C4195b(30, new C4194a(23, 15), new C4194a(28, 16))), new Version(32, new int[]{6, 34, 60, 86, 112, TsExtractor.TS_STREAM_TYPE_DTS}, new C4195b(30, new C4194a(17, 115)), new C4195b(28, new C4194a(10, 46), new C4194a(23, 47)), new C4195b(30, new C4194a(10, 24), new C4194a(35, 25)), new C4195b(30, new C4194a(19, 15), new C4194a(35, 16))), new Version(33, new int[]{6, 30, 58, 86, 114, 142}, new C4195b(30, new C4194a(17, 115), new C4194a(1, 116)), new C4195b(28, new C4194a(14, 46), new C4194a(21, 47)), new C4195b(30, new C4194a(29, 24), new C4194a(19, 25)), new C4195b(30, new C4194a(11, 15), new C4194a(46, 16))), new Version(34, new int[]{6, 34, 62, 90, 118, 146}, new C4195b(30, new C4194a(13, 115), new C4194a(6, 116)), new C4195b(28, new C4194a(14, 46), new C4194a(23, 47)), new C4195b(30, new C4194a(44, 24), new C4194a(7, 25)), new C4195b(30, new C4194a(59, 16), new C4194a(1, 17))), new Version(35, new int[]{6, 30, 54, 78, 102, 126, 150}, new C4195b(30, new C4194a(12, 121), new C4194a(7, 122)), new C4195b(28, new C4194a(12, 47), new C4194a(26, 48)), new C4195b(30, new C4194a(39, 24), new C4194a(14, 25)), new C4195b(30, new C4194a(22, 15), new C4194a(41, 16))), new Version(36, new int[]{6, 24, 50, 76, 102, 128, 154}, new C4195b(30, new C4194a(6, 121), new C4194a(14, 122)), new C4195b(28, new C4194a(6, 47), new C4194a(34, 48)), new C4195b(30, new C4194a(46, 24), new C4194a(10, 25)), new C4195b(30, new C4194a(2, 15), new C4194a(64, 16))), new Version(37, new int[]{6, 28, 54, 80, 106, 132, 158}, new C4195b(30, new C4194a(17, 122), new C4194a(4, 123)), new C4195b(28, new C4194a(29, 46), new C4194a(14, 47)), new C4195b(30, new C4194a(49, 24), new C4194a(10, 25)), new C4195b(30, new C4194a(24, 15), new C4194a(46, 16))), new Version(38, new int[]{6, 32, 58, 84, 110, 136, 162}, new C4195b(30, new C4194a(4, 122), new C4194a(18, 123)), new C4195b(28, new C4194a(13, 46), new C4194a(32, 47)), new C4195b(30, new C4194a(48, 24), new C4194a(14, 25)), new C4195b(30, new C4194a(42, 15), new C4194a(32, 16))), new Version(39, new int[]{6, 26, 54, 82, 110, TsExtractor.TS_STREAM_TYPE_DTS, 166}, new C4195b(30, new C4194a(20, 117), new C4194a(4, 118)), new C4195b(28, new C4194a(40, 47), new C4194a(7, 48)), new C4195b(30, new C4194a(43, 24), new C4194a(22, 25)), new C4195b(30, new C4194a(10, 15), new C4194a(67, 16))), new Version(40, new int[]{6, 30, 58, 86, 114, 142, 170}, new C4195b(30, new C4194a(19, 118), new C4194a(6, 119)), new C4195b(28, new C4194a(18, 47), new C4194a(31, 48)), new C4195b(30, new C4194a(34, 24), new C4194a(34, 25)), new C4195b(30, new C4194a(20, 15), new C4194a(61, 16)))};
    }

    /* renamed from: a */
    public int mo24116a() {
        return (this.f7948a * 4) + 17;
    }

    /* renamed from: b */
    public int mo24118b() {
        return this.f7950c;
    }

    /* renamed from: c */
    public int mo24119c() {
        return this.f7948a;
    }

    public String toString() {
        return String.valueOf(this.f7948a);
    }

    /* renamed from: a */
    public C4195b mo24117a(ErrorCorrectionLevel aVar) {
        return this.f7949b[aVar.ordinal()];
    }

    /* renamed from: a */
    public static Version m12918a(int i) {
        if (i > 0 && i <= 40) {
            return f7947d[i - 1];
        }
        throw new IllegalArgumentException();
    }
}
