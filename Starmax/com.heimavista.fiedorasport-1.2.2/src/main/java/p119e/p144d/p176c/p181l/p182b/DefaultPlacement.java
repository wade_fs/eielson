package p119e.p144d.p176c.p181l.p182b;

import java.util.Arrays;

/* renamed from: e.d.c.l.b.e */
public class DefaultPlacement {

    /* renamed from: a */
    private final CharSequence f7842a;

    /* renamed from: b */
    private final int f7843b;

    /* renamed from: c */
    private final int f7844c;

    /* renamed from: d */
    private final byte[] f7845d;

    public DefaultPlacement(CharSequence charSequence, int i, int i2) {
        this.f7842a = charSequence;
        this.f7844c = i;
        this.f7843b = i2;
        this.f7845d = new byte[(i * i2)];
        Arrays.fill(this.f7845d, (byte) -1);
    }

    /* renamed from: b */
    private boolean m12773b(int i, int i2) {
        return this.f7845d[(i2 * this.f7844c) + i] >= 0;
    }

    /* renamed from: c */
    private void m12774c(int i) {
        m12770a(this.f7843b - 3, 0, i, 1);
        m12770a(this.f7843b - 2, 0, i, 2);
        m12770a(this.f7843b - 1, 0, i, 3);
        m12770a(0, this.f7844c - 2, i, 4);
        m12770a(0, this.f7844c - 1, i, 5);
        m12770a(1, this.f7844c - 1, i, 6);
        m12770a(2, this.f7844c - 1, i, 7);
        m12770a(3, this.f7844c - 1, i, 8);
    }

    /* renamed from: d */
    private void m12775d(int i) {
        m12770a(this.f7843b - 1, 0, i, 1);
        m12770a(this.f7843b - 1, this.f7844c - 1, i, 2);
        m12770a(0, this.f7844c - 3, i, 3);
        m12770a(0, this.f7844c - 2, i, 4);
        m12770a(0, this.f7844c - 1, i, 5);
        m12770a(1, this.f7844c - 3, i, 6);
        m12770a(1, this.f7844c - 2, i, 7);
        m12770a(1, this.f7844c - 1, i, 8);
    }

    /* renamed from: a */
    public final boolean mo24068a(int i, int i2) {
        return this.f7845d[(i2 * this.f7844c) + i] == 1;
    }

    /* renamed from: a */
    private void m12771a(int i, int i2, boolean z) {
        this.f7845d[(i2 * this.f7844c) + i] = z ? (byte) 1 : 0;
    }

    /* renamed from: b */
    private void m12772b(int i) {
        m12770a(this.f7843b - 3, 0, i, 1);
        m12770a(this.f7843b - 2, 0, i, 2);
        m12770a(this.f7843b - 1, 0, i, 3);
        m12770a(0, this.f7844c - 4, i, 4);
        m12770a(0, this.f7844c - 3, i, 5);
        m12770a(0, this.f7844c - 2, i, 6);
        m12770a(0, this.f7844c - 1, i, 7);
        m12770a(1, this.f7844c - 1, i, 8);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.l.b.e.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      e.d.c.l.b.e.a(int, int, int):void
      e.d.c.l.b.e.a(int, int, boolean):void */
    /* renamed from: a */
    public final void mo24067a() {
        int i;
        int i2;
        int i3 = 4;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            if (i3 == this.f7843b && i4 == 0) {
                m12768a(i5);
                i5++;
            }
            if (i3 == this.f7843b - 2 && i4 == 0 && this.f7844c % 4 != 0) {
                m12772b(i5);
                i5++;
            }
            if (i3 == this.f7843b - 2 && i4 == 0 && this.f7844c % 8 == 4) {
                m12774c(i5);
                i5++;
            }
            if (i3 == this.f7843b + 4 && i4 == 2 && this.f7844c % 8 == 0) {
                m12775d(i5);
                i5++;
            }
            do {
                if (i3 < this.f7843b && i4 >= 0 && !m12773b(i4, i3)) {
                    m12769a(i3, i4, i5);
                    i5++;
                }
                i3 -= 2;
                i4 += 2;
                if (i3 < 0) {
                    break;
                }
            } while (i4 < this.f7844c);
            int i6 = i3 + 1;
            int i7 = i4 + 3;
            do {
                if (i6 >= 0 && i7 < this.f7844c && !m12773b(i7, i6)) {
                    m12769a(i6, i7, i5);
                    i5++;
                }
                i6 += 2;
                i7 -= 2;
                if (i6 >= this.f7843b) {
                    break;
                }
            } while (i7 >= 0);
            i3 = i6 + 3;
            i4 = i7 + 1;
            i = this.f7843b;
            if (i3 >= i && i4 >= (i2 = this.f7844c)) {
                break;
            }
        }
        if (!m12773b(i2 - 1, i - 1)) {
            m12771a(this.f7844c - 1, this.f7843b - 1, true);
            m12771a(this.f7844c - 2, this.f7843b - 2, true);
        }
    }

    /* renamed from: a */
    private void m12770a(int i, int i2, int i3, int i4) {
        if (i < 0) {
            int i5 = this.f7843b;
            i += i5;
            i2 += 4 - ((i5 + 4) % 8);
        }
        if (i2 < 0) {
            int i6 = this.f7844c;
            i2 += i6;
            i += 4 - ((i6 + 4) % 8);
        }
        boolean z = true;
        if ((this.f7842a.charAt(i3) & (1 << (8 - i4))) == 0) {
            z = false;
        }
        m12771a(i2, i, z);
    }

    /* renamed from: a */
    private void m12769a(int i, int i2, int i3) {
        int i4 = i - 2;
        int i5 = i2 - 2;
        m12770a(i4, i5, i3, 1);
        int i6 = i2 - 1;
        m12770a(i4, i6, i3, 2);
        int i7 = i - 1;
        m12770a(i7, i5, i3, 3);
        m12770a(i7, i6, i3, 4);
        m12770a(i7, i2, i3, 5);
        m12770a(i, i5, i3, 6);
        m12770a(i, i6, i3, 7);
        m12770a(i, i2, i3, 8);
    }

    /* renamed from: a */
    private void m12768a(int i) {
        m12770a(this.f7843b - 1, 0, i, 1);
        m12770a(this.f7843b - 1, 1, i, 2);
        m12770a(this.f7843b - 1, 2, i, 3);
        m12770a(0, this.f7844c - 2, i, 4);
        m12770a(0, this.f7844c - 1, i, 5);
        m12770a(1, this.f7844c - 1, i, 6);
        m12770a(2, this.f7844c - 1, i, 7);
        m12770a(3, this.f7844c - 1, i, 8);
    }
}
