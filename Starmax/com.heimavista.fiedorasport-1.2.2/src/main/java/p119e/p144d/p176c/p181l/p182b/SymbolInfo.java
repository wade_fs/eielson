package p119e.p144d.p176c.p181l.p182b;

import com.google.android.exoplayer2.extractor.p085ts.PsExtractor;
import p119e.p144d.p176c.Dimension;

/* renamed from: e.d.c.l.b.k */
public class SymbolInfo {

    /* renamed from: i */
    private static SymbolInfo[] f7859i = {new SymbolInfo(false, 3, 5, 8, 8, 1), new SymbolInfo(false, 5, 7, 10, 10, 1), new SymbolInfo(true, 5, 7, 16, 6, 1), new SymbolInfo(false, 8, 10, 12, 12, 1), new SymbolInfo(true, 10, 11, 14, 6, 2), new SymbolInfo(false, 12, 12, 14, 14, 1), new SymbolInfo(true, 16, 14, 24, 10, 1), new SymbolInfo(false, 18, 14, 16, 16, 1), new SymbolInfo(false, 22, 18, 18, 18, 1), new SymbolInfo(true, 22, 18, 16, 10, 2), new SymbolInfo(false, 30, 20, 20, 20, 1), new SymbolInfo(true, 32, 24, 16, 14, 2), new SymbolInfo(false, 36, 24, 22, 22, 1), new SymbolInfo(false, 44, 28, 24, 24, 1), new SymbolInfo(true, 49, 28, 22, 14, 2), new SymbolInfo(false, 62, 36, 14, 14, 4), new SymbolInfo(false, 86, 42, 16, 16, 4), new SymbolInfo(false, 114, 48, 18, 18, 4), new SymbolInfo(false, 144, 56, 20, 20, 4), new SymbolInfo(false, 174, 68, 22, 22, 4), new SymbolInfo(false, 204, 84, 24, 24, 4, 102, 42), new SymbolInfo(false, 280, 112, 14, 14, 16, 140, 56), new SymbolInfo(false, 368, 144, 16, 16, 16, 92, 36), new SymbolInfo(false, 456, PsExtractor.AUDIO_STREAM, 18, 18, 16, 114, 48), new SymbolInfo(false, 576, 224, 20, 20, 16, 144, 56), new SymbolInfo(false, 696, 272, 22, 22, 16, 174, 68), new SymbolInfo(false, 816, 336, 24, 24, 16, 136, 56), new SymbolInfo(false, 1050, 408, 18, 18, 36, 175, 68), new SymbolInfo(false, 1304, 496, 20, 20, 36, 163, 62), new DataMatrixSymbolInfo144()};

    /* renamed from: a */
    private final boolean f7860a;

    /* renamed from: b */
    private final int f7861b;

    /* renamed from: c */
    private final int f7862c;

    /* renamed from: d */
    public final int f7863d;

    /* renamed from: e */
    public final int f7864e;

    /* renamed from: f */
    private final int f7865f;

    /* renamed from: g */
    private final int f7866g;

    /* renamed from: h */
    private final int f7867h;

    public SymbolInfo(boolean z, int i, int i2, int i3, int i4, int i5) {
        this(z, i, i2, i3, i4, i5, i, i2);
    }

    /* renamed from: a */
    public static SymbolInfo m12821a(int i, SymbolShapeHint lVar, Dimension bVar, Dimension bVar2, boolean z) {
        SymbolInfo[] kVarArr = f7859i;
        for (SymbolInfo kVar : kVarArr) {
            if ((lVar != SymbolShapeHint.FORCE_SQUARE || !kVar.f7860a) && ((lVar != SymbolShapeHint.FORCE_RECTANGLE || kVar.f7860a) && ((bVar == null || (kVar.mo24094g() >= bVar.mo23986b() && kVar.mo24093f() >= bVar.mo23985a())) && ((bVar2 == null || (kVar.mo24094g() <= bVar2.mo23986b() && kVar.mo24093f() <= bVar2.mo23985a())) && i <= kVar.f7861b)))) {
                return kVar;
            }
        }
        if (!z) {
            return null;
        }
        throw new IllegalArgumentException("Can't find a symbol arrangement that matches the message. Data codewords: ".concat(String.valueOf(i)));
    }

    /* renamed from: h */
    private int m12822h() {
        int i = this.f7865f;
        int i2 = 1;
        if (i != 1) {
            i2 = 2;
            if (!(i == 2 || i == 4)) {
                if (i == 16) {
                    return 4;
                }
                if (i == 36) {
                    return 6;
                }
                throw new IllegalStateException("Cannot handle this number of data regions");
            }
        }
        return i2;
    }

    /* renamed from: i */
    private int m12823i() {
        int i = this.f7865f;
        if (i == 1 || i == 2) {
            return 1;
        }
        if (i == 4) {
            return 2;
        }
        if (i == 16) {
            return 4;
        }
        if (i == 36) {
            return 6;
        }
        throw new IllegalStateException("Cannot handle this number of data regions");
    }

    /* renamed from: b */
    public final int mo24089b() {
        return this.f7862c;
    }

    /* renamed from: c */
    public int mo24066c() {
        return this.f7861b / this.f7866g;
    }

    /* renamed from: d */
    public final int mo24091d() {
        return m12823i() * this.f7864e;
    }

    /* renamed from: e */
    public final int mo24092e() {
        return m12822h() * this.f7863d;
    }

    /* renamed from: f */
    public final int mo24093f() {
        return mo24091d() + (m12823i() << 1);
    }

    /* renamed from: g */
    public final int mo24094g() {
        return mo24092e() + (m12822h() << 1);
    }

    public final String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f7860a ? "Rectangular Symbol:" : "Square Symbol:");
        sb.append(" data region ");
        sb.append(this.f7863d);
        sb.append('x');
        sb.append(this.f7864e);
        sb.append(", symbol size ");
        sb.append(mo24094g());
        sb.append('x');
        sb.append(mo24093f());
        sb.append(", symbol data size ");
        sb.append(mo24092e());
        sb.append('x');
        sb.append(mo24091d());
        sb.append(", codewords ");
        sb.append(this.f7861b);
        sb.append('+');
        sb.append(this.f7862c);
        return sb.toString();
    }

    SymbolInfo(boolean z, int i, int i2, int i3, int i4, int i5, int i6, int i7) {
        this.f7860a = z;
        this.f7861b = i;
        this.f7862c = i2;
        this.f7863d = i3;
        this.f7864e = i4;
        this.f7865f = i5;
        this.f7866g = i6;
        this.f7867h = i7;
    }

    /* renamed from: b */
    public final int mo24090b(int i) {
        return this.f7867h;
    }

    /* renamed from: a */
    public final int mo24088a() {
        return this.f7861b;
    }

    /* renamed from: a */
    public int mo24065a(int i) {
        return this.f7866g;
    }
}
