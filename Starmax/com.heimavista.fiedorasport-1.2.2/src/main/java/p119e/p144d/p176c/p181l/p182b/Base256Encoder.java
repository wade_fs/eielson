package p119e.p144d.p176c.p181l.p182b;

import androidx.core.view.InputDeviceCompat;
import androidx.recyclerview.widget.ItemTouchHelper;

/* renamed from: e.d.c.l.b.b */
final class Base256Encoder implements C4190g {
    Base256Encoder() {
    }

    /* renamed from: a */
    public int mo24061a() {
        return 5;
    }

    /* renamed from: a */
    public void mo24060a(EncoderContext hVar) {
        StringBuilder sb = new StringBuilder();
        sb.append(0);
        while (true) {
            if (!hVar.mo24084h()) {
                break;
            }
            sb.append(hVar.mo24078c());
            hVar.f7851f++;
            if (C4191j.m12808a(hVar.mo24080d(), hVar.f7851f, mo24061a()) != mo24061a()) {
                hVar.mo24077b(0);
                break;
            }
        }
        int length = sb.length() - 1;
        int a = hVar.mo24070a() + length + 1;
        hVar.mo24079c(a);
        boolean z = hVar.mo24083g().mo24088a() - a > 0;
        if (hVar.mo24084h() || z) {
            if (length <= 249) {
                sb.setCharAt(0, (char) length);
            } else if (length <= 1555) {
                sb.setCharAt(0, (char) ((length / ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION) + 249));
                sb.insert(1, (char) (length % ItemTouchHelper.Callback.DEFAULT_SWIPE_ANIMATION_DURATION));
            } else {
                throw new IllegalStateException("Message length not in valid ranges: ".concat(String.valueOf(length)));
            }
        }
        int length2 = sb.length();
        for (int i = 0; i < length2; i++) {
            hVar.mo24071a(m12756a(sb.charAt(i), hVar.mo24070a() + 1));
        }
    }

    /* renamed from: a */
    private static char m12756a(char c, int i) {
        int i2 = c + ((i * 149) % 255) + 1;
        return i2 <= 255 ? (char) i2 : (char) (i2 + InputDeviceCompat.SOURCE_ANY);
    }
}
