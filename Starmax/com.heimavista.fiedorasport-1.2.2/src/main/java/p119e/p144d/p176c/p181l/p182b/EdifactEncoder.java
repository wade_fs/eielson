package p119e.p144d.p176c.p181l.p182b;

/* renamed from: e.d.c.l.b.f */
final class EdifactEncoder implements C4190g {
    EdifactEncoder() {
    }

    /* renamed from: a */
    public int mo24069a() {
        return 4;
    }

    /* renamed from: a */
    public void mo24060a(EncoderContext hVar) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!hVar.mo24084h()) {
                break;
            }
            m12779a(hVar.mo24078c(), sb);
            hVar.f7851f++;
            if (sb.length() >= 4) {
                hVar.mo24075a(m12778a(sb, 0));
                sb.delete(0, 4);
                if (C4191j.m12808a(hVar.mo24080d(), hVar.f7851f, mo24069a()) != mo24069a()) {
                    hVar.mo24077b(0);
                    break;
                }
            }
        }
        sb.append(31);
        m12780a(hVar, sb);
    }

    /* renamed from: a */
    private static void m12780a(EncoderContext hVar, CharSequence charSequence) {
        try {
            int length = charSequence.length();
            if (length != 0) {
                boolean z = true;
                if (length == 1) {
                    hVar.mo24087k();
                    int a = hVar.mo24083g().mo24088a() - hVar.mo24070a();
                    int f = hVar.mo24082f();
                    if (f > a) {
                        hVar.mo24079c(hVar.mo24070a() + 1);
                        a = hVar.mo24083g().mo24088a() - hVar.mo24070a();
                    }
                    if (f <= a && a <= 2) {
                        hVar.mo24077b(0);
                        return;
                    }
                }
                if (length <= 4) {
                    int i = length - 1;
                    String a2 = m12778a(charSequence, 0);
                    if (!(!hVar.mo24084h()) || i > 2) {
                        z = false;
                    }
                    if (i <= 2) {
                        hVar.mo24079c(hVar.mo24070a() + i);
                        if (hVar.mo24083g().mo24088a() - hVar.mo24070a() >= 3) {
                            hVar.mo24079c(hVar.mo24070a() + a2.length());
                            z = false;
                        }
                    }
                    if (z) {
                        hVar.mo24086j();
                        hVar.f7851f -= i;
                    } else {
                        hVar.mo24075a(a2);
                    }
                    hVar.mo24077b(0);
                    return;
                }
                throw new IllegalStateException("Count must not exceed 4");
            }
        } finally {
            hVar.mo24077b(0);
        }
    }

    /* renamed from: a */
    private static void m12779a(char c, StringBuilder sb) {
        if (c >= ' ' && c <= '?') {
            sb.append(c);
        } else if (c < '@' || c > '^') {
            C4191j.m12812a(c);
            throw null;
        } else {
            sb.append((char) (c - '@'));
        }
    }

    /* renamed from: a */
    private static String m12778a(CharSequence charSequence, int i) {
        int length = charSequence.length() - i;
        if (length != 0) {
            char charAt = charSequence.charAt(i);
            char c = 0;
            char charAt2 = length >= 2 ? charSequence.charAt(i + 1) : 0;
            char charAt3 = length >= 3 ? charSequence.charAt(i + 2) : 0;
            if (length >= 4) {
                c = charSequence.charAt(i + 3);
            }
            int i2 = (charAt << 18) + (charAt2 << 12) + (charAt3 << 6) + c;
            char c2 = (char) ((i2 >> 8) & 255);
            char c3 = (char) (i2 & 255);
            StringBuilder sb = new StringBuilder(3);
            sb.append((char) ((i2 >> 16) & 255));
            if (length >= 2) {
                sb.append(c2);
            }
            if (length >= 3) {
                sb.append(c3);
            }
            return sb.toString();
        }
        throw new IllegalStateException("StringBuilder must not be empty");
    }
}
