package p119e.p144d.p176c.p186o.p187b;

/* renamed from: e.d.c.o.b.b */
public enum Mode {
    TERMINATOR(new int[]{0, 0, 0}, 0),
    NUMERIC(new int[]{10, 12, 14}, 1),
    ALPHANUMERIC(new int[]{9, 11, 13}, 2),
    STRUCTURED_APPEND(new int[]{0, 0, 0}, 3),
    BYTE(new int[]{8, 16, 16}, 4),
    ECI(new int[]{0, 0, 0}, 7),
    KANJI(new int[]{8, 10, 12}, 8),
    FNC1_FIRST_POSITION(new int[]{0, 0, 0}, 5),
    FNC1_SECOND_POSITION(new int[]{0, 0, 0}, 9),
    HANZI(new int[]{8, 10, 12}, 13);
    

    /* renamed from: P */
    private final int[] f7945P;

    /* renamed from: Q */
    private final int f7946Q;

    private Mode(int[] iArr, int i) {
        this.f7945P = iArr;
        this.f7946Q = i;
    }

    /* renamed from: a */
    public int mo24115a(Version cVar) {
        int c = cVar.mo24119c();
        return this.f7945P[c <= 9 ? 0 : c <= 26 ? (char) 1 : 2];
    }

    /* renamed from: a */
    public int mo24114a() {
        return this.f7946Q;
    }
}
