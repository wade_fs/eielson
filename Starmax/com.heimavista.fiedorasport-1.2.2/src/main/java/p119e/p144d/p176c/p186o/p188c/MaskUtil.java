package p119e.p144d.p176c.p186o.p188c;

/* renamed from: e.d.c.o.c.d */
final class MaskUtil {
    /* renamed from: a */
    static int m12961a(ByteMatrix bVar) {
        return m12962a(bVar, true) + m12962a(bVar, false);
    }

    /* renamed from: b */
    static int m12966b(ByteMatrix bVar) {
        byte[][] a = bVar.mo24133a();
        int c = bVar.mo24135c();
        int b = bVar.mo24134b();
        int i = 0;
        int i2 = 0;
        while (i < b - 1) {
            byte[] bArr = a[i];
            int i3 = i2;
            int i4 = 0;
            while (i4 < c - 1) {
                byte b2 = bArr[i4];
                int i5 = i4 + 1;
                if (b2 == bArr[i5]) {
                    int i6 = i + 1;
                    if (b2 == a[i6][i4] && b2 == a[i6][i5]) {
                        i3++;
                    }
                }
                i4 = i5;
            }
            i++;
            i2 = i3;
        }
        return i2 * 3;
    }

    /* renamed from: c */
    static int m12967c(ByteMatrix bVar) {
        byte[][] a = bVar.mo24133a();
        int c = bVar.mo24135c();
        int b = bVar.mo24134b();
        int i = 0;
        int i2 = 0;
        while (i < b) {
            int i3 = i2;
            for (int i4 = 0; i4 < c; i4++) {
                byte[] bArr = a[i];
                int i5 = i4 + 6;
                if (i5 < c && bArr[i4] == 1 && bArr[i4 + 1] == 0 && bArr[i4 + 2] == 1 && bArr[i4 + 3] == 1 && bArr[i4 + 4] == 1 && bArr[i4 + 5] == 0 && bArr[i5] == 1 && (m12964a(bArr, i4 - 4, i4) || m12964a(bArr, i4 + 7, i4 + 11))) {
                    i3++;
                }
                int i6 = i + 6;
                if (i6 < b && a[i][i4] == 1 && a[i + 1][i4] == 0 && a[i + 2][i4] == 1 && a[i + 3][i4] == 1 && a[i + 4][i4] == 1 && a[i + 5][i4] == 0 && a[i6][i4] == 1 && (m12965a(a, i4, i - 4, i) || m12965a(a, i4, i + 7, i + 11))) {
                    i3++;
                }
            }
            i++;
            i2 = i3;
        }
        return i2 * 40;
    }

    /* renamed from: d */
    static int m12968d(ByteMatrix bVar) {
        byte[][] a = bVar.mo24133a();
        int c = bVar.mo24135c();
        int b = bVar.mo24134b();
        int i = 0;
        int i2 = 0;
        while (i < b) {
            byte[] bArr = a[i];
            int i3 = i2;
            for (int i4 = 0; i4 < c; i4++) {
                if (bArr[i4] == 1) {
                    i3++;
                }
            }
            i++;
            i2 = i3;
        }
        int b2 = bVar.mo24134b() * bVar.mo24135c();
        return ((Math.abs((i2 << 1) - b2) * 10) / b2) * 10;
    }

    /* renamed from: a */
    private static boolean m12964a(byte[] bArr, int i, int i2) {
        int min = Math.min(i2, bArr.length);
        for (int max = Math.max(i, 0); max < min; max++) {
            if (bArr[max] == 1) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    private static boolean m12965a(byte[][] bArr, int i, int i2, int i3) {
        int min = Math.min(i3, bArr.length);
        for (int max = Math.max(i2, 0); max < min; max++) {
            if (bArr[max][i] == 1) {
                return false;
            }
        }
        return true;
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    /* renamed from: a */
    static boolean m12963a(int i, int i2, int i3) {
        int i4;
        int i5;
        switch (i) {
            case 0:
                i3 += i2;
                i4 = i3 & 1;
                break;
            case 1:
                i4 = i3 & 1;
                break;
            case 2:
                i4 = i2 % 3;
                break;
            case 3:
                i4 = (i3 + i2) % 3;
                break;
            case 4:
                i3 /= 2;
                i2 /= 3;
                i3 += i2;
                i4 = i3 & 1;
                break;
            case 5:
                int i6 = i3 * i2;
                i4 = (i6 & 1) + (i6 % 3);
                break;
            case 6:
                int i7 = i3 * i2;
                i5 = (i7 & 1) + (i7 % 3);
                i4 = i5 & 1;
                break;
            case 7:
                i5 = ((i3 * i2) % 3) + ((i3 + i2) & 1);
                i4 = i5 & 1;
                break;
            default:
                throw new IllegalArgumentException("Invalid mask pattern: ".concat(String.valueOf(i)));
        }
        if (i4 == 0) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    private static int m12962a(ByteMatrix bVar, boolean z) {
        int b = z ? bVar.mo24134b() : bVar.mo24135c();
        int c = z ? bVar.mo24135c() : bVar.mo24134b();
        byte[][] a = bVar.mo24133a();
        int i = 0;
        for (int i2 = 0; i2 < b; i2++) {
            int i3 = i;
            int i4 = 0;
            byte b2 = -1;
            for (int i5 = 0; i5 < c; i5++) {
                byte b3 = z ? a[i2][i5] : a[i5][i2];
                if (b3 == b2) {
                    i4++;
                } else {
                    if (i4 >= 5) {
                        i3 += (i4 - 5) + 3;
                    }
                    i4 = 1;
                    b2 = b3;
                }
            }
            if (i4 >= 5) {
                i3 += (i4 - 5) + 3;
            }
            i = i3;
        }
        return i;
    }
}
