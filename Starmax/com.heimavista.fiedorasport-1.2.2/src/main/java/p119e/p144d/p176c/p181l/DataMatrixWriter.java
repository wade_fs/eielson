package p119e.p144d.p176c.p181l;

import java.util.Map;
import p119e.p144d.p176c.BarcodeFormat;
import p119e.p144d.p176c.Dimension;
import p119e.p144d.p176c.EncodeHintType;
import p119e.p144d.p176c.Writer;
import p119e.p144d.p176c.p179k.BitMatrix;
import p119e.p144d.p176c.p181l.p182b.C4191j;
import p119e.p144d.p176c.p181l.p182b.DefaultPlacement;
import p119e.p144d.p176c.p181l.p182b.ErrorCorrection;
import p119e.p144d.p176c.p181l.p182b.SymbolInfo;
import p119e.p144d.p176c.p181l.p182b.SymbolShapeHint;
import p119e.p144d.p176c.p186o.p188c.ByteMatrix;

/* renamed from: e.d.c.l.a */
public final class DataMatrixWriter implements Writer {
    /* renamed from: a */
    public BitMatrix mo23990a(String str, BarcodeFormat aVar, int i, int i2, Map<EncodeHintType, ?> map) {
        Dimension bVar;
        if (str.isEmpty()) {
            throw new IllegalArgumentException("Found empty contents");
        } else if (aVar != BarcodeFormat.DATA_MATRIX) {
            throw new IllegalArgumentException("Can only encode DATA_MATRIX, but got ".concat(String.valueOf(aVar)));
        } else if (i < 0 || i2 < 0) {
            throw new IllegalArgumentException("Requested dimensions can't be negative: " + i + 'x' + i2);
        } else {
            SymbolShapeHint lVar = SymbolShapeHint.FORCE_NONE;
            Dimension bVar2 = null;
            if (map != null) {
                SymbolShapeHint lVar2 = (SymbolShapeHint) map.get(EncodeHintType.DATA_MATRIX_SHAPE);
                if (lVar2 != null) {
                    lVar = lVar2;
                }
                bVar = (Dimension) map.get(EncodeHintType.MIN_SIZE);
                if (bVar == null) {
                    bVar = null;
                }
                Dimension bVar3 = (Dimension) map.get(EncodeHintType.MAX_SIZE);
                if (bVar3 != null) {
                    bVar2 = bVar3;
                }
            } else {
                bVar = null;
            }
            String a = C4191j.m12811a(str, lVar, bVar, bVar2);
            SymbolInfo a2 = SymbolInfo.m12821a(a.length(), lVar, bVar, bVar2, true);
            DefaultPlacement eVar = new DefaultPlacement(ErrorCorrection.m12805a(a, a2), a2.mo24092e(), a2.mo24091d());
            eVar.mo24067a();
            return m12750a(eVar, a2, i, i2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.d.c.o.c.b.a(int, int, boolean):void
     arg types: [int, int, int]
     candidates:
      e.d.c.o.c.b.a(int, int, int):void
      e.d.c.o.c.b.a(int, int, boolean):void */
    /* renamed from: a */
    private static BitMatrix m12750a(DefaultPlacement eVar, SymbolInfo kVar, int i, int i2) {
        int e = kVar.mo24092e();
        int d = kVar.mo24091d();
        ByteMatrix bVar = new ByteMatrix(kVar.mo24094g(), kVar.mo24093f());
        int i3 = 0;
        for (int i4 = 0; i4 < d; i4++) {
            if (i4 % kVar.f7864e == 0) {
                int i5 = 0;
                for (int i6 = 0; i6 < kVar.mo24094g(); i6++) {
                    bVar.mo24132a(i5, i3, i6 % 2 == 0);
                    i5++;
                }
                i3++;
            }
            int i7 = 0;
            for (int i8 = 0; i8 < e; i8++) {
                if (i8 % kVar.f7863d == 0) {
                    bVar.mo24132a(i7, i3, true);
                    i7++;
                }
                bVar.mo24132a(i7, i3, eVar.mo24068a(i8, i4));
                i7++;
                int i9 = kVar.f7863d;
                if (i8 % i9 == i9 - 1) {
                    bVar.mo24132a(i7, i3, i4 % 2 == 0);
                    i7++;
                }
            }
            i3++;
            int i10 = kVar.f7864e;
            if (i4 % i10 == i10 - 1) {
                int i11 = 0;
                for (int i12 = 0; i12 < kVar.mo24094g(); i12++) {
                    bVar.mo24132a(i11, i3, true);
                    i11++;
                }
                i3++;
            }
        }
        return m12751a(bVar, i, i2);
    }

    /* renamed from: a */
    private static BitMatrix m12751a(ByteMatrix bVar, int i, int i2) {
        BitMatrix bVar2;
        int c = bVar.mo24135c();
        int b = bVar.mo24134b();
        int max = Math.max(i, c);
        int max2 = Math.max(i2, b);
        int min = Math.min(max / c, max2 / b);
        int i3 = (max - (c * min)) / 2;
        int i4 = (max2 - (b * min)) / 2;
        if (i2 < b || i < c) {
            bVar2 = new BitMatrix(c, b);
            i3 = 0;
            i4 = 0;
        } else {
            bVar2 = new BitMatrix(i, i2);
        }
        bVar2.mo24030a();
        int i5 = 0;
        while (i5 < b) {
            int i6 = i3;
            int i7 = 0;
            while (i7 < c) {
                if (bVar.mo24129a(i7, i5) == 1) {
                    bVar2.mo24031a(i6, i4, min, min);
                }
                i7++;
                i6 += min;
            }
            i5++;
            i4 += min;
        }
        return bVar2;
    }
}
