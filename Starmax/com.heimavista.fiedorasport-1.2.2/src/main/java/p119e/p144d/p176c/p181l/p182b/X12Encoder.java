package p119e.p144d.p176c.p181l.p182b;

/* renamed from: e.d.c.l.b.n */
final class X12Encoder extends C40Encoder {
    X12Encoder() {
    }

    /* renamed from: a */
    public int mo24062a() {
        return 3;
    }

    /* renamed from: a */
    public void mo24060a(EncoderContext hVar) {
        StringBuilder sb = new StringBuilder();
        while (true) {
            if (!hVar.mo24084h()) {
                break;
            }
            char c = hVar.mo24078c();
            hVar.f7851f++;
            mo24063a(c, sb);
            if (sb.length() % 3 == 0) {
                C40Encoder.m12761b(hVar, sb);
                if (C4191j.m12808a(hVar.mo24080d(), hVar.f7851f, mo24062a()) != mo24062a()) {
                    hVar.mo24077b(0);
                    break;
                }
            }
        }
        mo24064a(hVar, sb);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int mo24063a(char c, StringBuilder sb) {
        if (c == 13) {
            sb.append(0);
        } else if (c == ' ') {
            sb.append(3);
        } else if (c == '*') {
            sb.append(1);
        } else if (c == '>') {
            sb.append(2);
        } else if (c >= '0' && c <= '9') {
            sb.append((char) ((c - '0') + 4));
        } else if (c < 'A' || c > 'Z') {
            C4191j.m12812a(c);
            throw null;
        } else {
            sb.append((char) ((c - 'A') + 14));
        }
        return 1;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo24064a(EncoderContext hVar, StringBuilder sb) {
        hVar.mo24087k();
        int a = hVar.mo24083g().mo24088a() - hVar.mo24070a();
        hVar.f7851f -= sb.length();
        if (hVar.mo24082f() > 1 || a > 1 || hVar.mo24082f() != a) {
            hVar.mo24071a(254);
        }
        if (hVar.mo24081e() < 0) {
            hVar.mo24077b(0);
        }
    }
}
