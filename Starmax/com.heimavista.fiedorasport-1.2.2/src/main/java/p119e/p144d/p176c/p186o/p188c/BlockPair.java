package p119e.p144d.p176c.p186o.p188c;

/* renamed from: e.d.c.o.c.a */
final class BlockPair {

    /* renamed from: a */
    private final byte[] f7955a;

    /* renamed from: b */
    private final byte[] f7956b;

    BlockPair(byte[] bArr, byte[] bArr2) {
        this.f7955a = bArr;
        this.f7956b = bArr2;
    }

    /* renamed from: a */
    public byte[] mo24127a() {
        return this.f7955a;
    }

    /* renamed from: b */
    public byte[] mo24128b() {
        return this.f7956b;
    }
}
