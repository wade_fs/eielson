package p119e.p124b.p125a;

import android.text.TextUtils;
import android.webkit.MimeTypeMap;
import java.io.Closeable;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.o */
public class ProxyCacheUtils {

    /* renamed from: a */
    private static final Logger f6907a = LoggerFactory.m18313a("ProxyCacheUtils");

    /* renamed from: a */
    static void m11150a(byte[] bArr, long j, int i) {
        C3832l.m11128a(bArr, "Buffer must be not null!");
        boolean z = true;
        C3832l.m11130a(j >= 0, "Data offset must be positive!");
        if (i < 0 || i > bArr.length) {
            z = false;
        }
        C3832l.m11130a(z, "Length must be in range [0..buffer.length]");
    }

    /* renamed from: b */
    static String m11151b(String str) {
        try {
            return URLDecoder.decode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Error decoding url", e);
        }
    }

    /* renamed from: c */
    static String m11152c(String str) {
        try {
            return URLEncoder.encode(str, "utf-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Error encoding url", e);
        }
    }

    /* renamed from: d */
    static String m11153d(String str) {
        MimeTypeMap singleton = MimeTypeMap.getSingleton();
        String fileExtensionFromUrl = MimeTypeMap.getFileExtensionFromUrl(str);
        if (TextUtils.isEmpty(fileExtensionFromUrl)) {
            return null;
        }
        return singleton.getMimeTypeFromExtension(fileExtensionFromUrl);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a.b.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.io.IOException]
     candidates:
      k.a.b.a(java.lang.String, java.lang.Object):void
      k.a.b.a(java.lang.String, java.lang.Throwable):void */
    /* renamed from: a */
    static void m11149a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                f6907a.mo28149a("Error closing resource", (Throwable) e);
            }
        }
    }

    /* renamed from: a */
    public static String m11147a(String str) {
        try {
            return m11148a(MessageDigest.getInstance("MD5").digest(str.getBytes()));
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException(e);
        }
    }

    /* renamed from: a */
    private static String m11148a(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(String.format("%02x", Byte.valueOf(bArr[i])));
        }
        return stringBuffer.toString();
    }
}
