package p119e.p124b.p125a.p127t;

import android.content.Context;

/* renamed from: e.b.a.t.d */
public class SourceInfoStorageFactory {
    /* renamed from: a */
    public static SourceInfoStorage m11186a(Context context) {
        return new DatabaseSourceInfoStorage(context);
    }

    /* renamed from: a */
    public static SourceInfoStorage m11185a() {
        return new NoSourceInfoStorage();
    }
}
