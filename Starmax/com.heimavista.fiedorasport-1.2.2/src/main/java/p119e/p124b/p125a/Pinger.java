package p119e.p124b.p125a;

import java.io.OutputStream;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.Socket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.k */
class Pinger {

    /* renamed from: d */
    private static final Logger f6892d = LoggerFactory.m18313a("Pinger");

    /* renamed from: a */
    private final ExecutorService f6893a = Executors.newSingleThreadExecutor();

    /* renamed from: b */
    private final String f6894b;

    /* renamed from: c */
    private final int f6895c;

    /* renamed from: e.b.a.k$b */
    /* compiled from: Pinger */
    private class C3831b implements Callable<Boolean> {
        private C3831b() {
        }

        public Boolean call() {
            return Boolean.valueOf(Pinger.this.m11123c());
        }
    }

    Pinger(String str, int i) {
        C3832l.m11127a(str);
        this.f6894b = str;
        this.f6895c = i;
    }

    /* renamed from: b */
    private String m11122b() {
        return String.format(Locale.US, "http://%s:%d/%s", this.f6894b, Integer.valueOf(this.f6895c), "ping");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a.b.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, e.b.a.n]
     candidates:
      k.a.b.a(java.lang.String, java.lang.Object):void
      k.a.b.a(java.lang.String, java.lang.Throwable):void */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public boolean m11123c() {
        HttpUrlSource hVar = new HttpUrlSource(m11122b());
        try {
            byte[] bytes = "ping ok".getBytes();
            hVar.mo23173a(0);
            byte[] bArr = new byte[bytes.length];
            hVar.read(bArr);
            boolean equals = Arrays.equals(bytes, bArr);
            Logger bVar = f6892d;
            bVar.mo28151c("Ping response: `" + new String(bArr) + "`, pinged? " + equals);
            return equals;
        } catch (ProxyCacheException e) {
            f6892d.mo28149a("Error reading ping response", (Throwable) e);
            return false;
        } finally {
            hVar.close();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a.b.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, e.b.a.n]
     candidates:
      k.a.b.a(java.lang.String, java.lang.Object):void
      k.a.b.a(java.lang.String, java.lang.Throwable):void */
    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo23182a(int i, int i2) {
        C3832l.m11129a(i >= 1);
        C3832l.m11129a(i2 > 0);
        int i3 = i2;
        int i4 = 0;
        while (i4 < i) {
            try {
                if (((Boolean) this.f6893a.submit(new C3831b()).get((long) i3, TimeUnit.MILLISECONDS)).booleanValue()) {
                    return true;
                }
                i4++;
                i3 *= 2;
            } catch (TimeoutException unused) {
                Logger bVar = f6892d;
                bVar.mo28152d("Error pinging server (attempt: " + i4 + ", timeout: " + i3 + "). ");
            } catch (InterruptedException | ExecutionException e) {
                f6892d.mo28149a("Error pinging server due to unexpected error", e);
            }
        }
        String format = String.format(Locale.US, "Error pinging server (attempts: %d, max timeout: %d). If you see this message, please, report at https://github.com/danikula/AndroidVideoCache/issues/134. Default proxies are: %s", Integer.valueOf(i4), Integer.valueOf(i3 / 2), m11120a());
        f6892d.mo28149a(format, (Throwable) new ProxyCacheException(format));
        return false;
    }

    /* renamed from: a */
    private List<Proxy> m11120a() {
        try {
            return ProxySelector.getDefault().select(new URI(m11122b()));
        } catch (URISyntaxException e) {
            throw new IllegalStateException(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public boolean mo23183a(String str) {
        return "ping".equals(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo23181a(Socket socket) {
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write("HTTP/1.1 200 OK\n\n".getBytes());
        outputStream.write("ping ok".getBytes());
    }
}
