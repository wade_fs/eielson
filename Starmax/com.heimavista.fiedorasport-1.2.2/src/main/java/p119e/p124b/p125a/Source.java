package p119e.p124b.p125a;

/* renamed from: e.b.a.p */
public interface Source {
    /* renamed from: a */
    void mo23173a(long j);

    void close();

    long length();

    int read(byte[] bArr);
}
