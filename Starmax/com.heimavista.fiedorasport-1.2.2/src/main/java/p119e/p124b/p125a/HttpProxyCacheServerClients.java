package p119e.p124b.p125a;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.io.File;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import p119e.p124b.p125a.p126s.FileCache;

/* renamed from: e.b.a.g */
final class HttpProxyCacheServerClients {

    /* renamed from: a */
    private final AtomicInteger f6875a = new AtomicInteger(0);

    /* renamed from: b */
    private final String f6876b;

    /* renamed from: c */
    private volatile HttpProxyCache f6877c;

    /* renamed from: d */
    private final List<CacheListener> f6878d = new CopyOnWriteArrayList();

    /* renamed from: e */
    private final CacheListener f6879e;

    /* renamed from: f */
    private final Config f6880f;

    /* renamed from: e.b.a.g$a */
    /* compiled from: HttpProxyCacheServerClients */
    private static final class C3829a extends Handler implements CacheListener {

        /* renamed from: P */
        private final String f6881P;

        /* renamed from: Q */
        private final List<CacheListener> f6882Q;

        public C3829a(String str, List<CacheListener> list) {
            super(Looper.getMainLooper());
            this.f6881P = str;
            this.f6882Q = list;
        }

        /* renamed from: a */
        public void mo23158a(File file, String str, int i) {
            Message obtainMessage = obtainMessage();
            obtainMessage.arg1 = i;
            obtainMessage.obj = file;
            sendMessage(obtainMessage);
        }

        public void handleMessage(Message message) {
            for (CacheListener bVar : this.f6882Q) {
                bVar.mo23158a((File) message.obj, this.f6881P, message.arg1);
            }
        }
    }

    public HttpProxyCacheServerClients(String str, Config cVar) {
        C3832l.m11127a(str);
        this.f6876b = str;
        C3832l.m11127a(cVar);
        this.f6880f = cVar;
        this.f6879e = new C3829a(str, this.f6878d);
    }

    /* renamed from: b */
    private synchronized void m11106b() {
        if (this.f6875a.decrementAndGet() <= 0) {
            this.f6877c.mo23186a();
            this.f6877c = null;
        }
    }

    /* renamed from: c */
    private HttpProxyCache m11107c() {
        HttpProxyCache eVar = new HttpProxyCache(new HttpUrlSource(this.f6876b, this.f6880f.f6849d), new FileCache(this.f6880f.mo23159a(this.f6876b), this.f6880f.f6848c));
        eVar.mo23162a(this.f6879e);
        return eVar;
    }

    /* renamed from: d */
    private synchronized void m11108d() {
        this.f6877c = this.f6877c == null ? m11107c() : this.f6877c;
    }

    /* renamed from: a */
    public void mo23170a(GetRequest dVar, Socket socket) {
        m11108d();
        try {
            this.f6875a.incrementAndGet();
            this.f6877c.mo23163a(dVar, socket);
        } finally {
            m11106b();
        }
    }

    /* renamed from: a */
    public int mo23169a() {
        return this.f6875a.get();
    }
}
