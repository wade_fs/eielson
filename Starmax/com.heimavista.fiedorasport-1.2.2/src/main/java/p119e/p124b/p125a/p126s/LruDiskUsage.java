package p119e.p124b.p125a.p126s;

import java.io.File;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.s.e */
public abstract class LruDiskUsage implements DiskUsage {

    /* renamed from: b */
    private static final Logger f6916b = LoggerFactory.m18313a("LruDiskUsage");

    /* renamed from: a */
    private final ExecutorService f6917a = Executors.newSingleThreadExecutor();

    /* renamed from: e.b.a.s.e$a */
    /* compiled from: LruDiskUsage */
    private class C3837a implements Callable<Void> {

        /* renamed from: a */
        private final File f6918a;

        public C3837a(File file) {
            this.f6918a = file;
        }

        public Void call() {
            LruDiskUsage.this.m11173b(this.f6918a);
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m11173b(File file) {
        Files.m11168e(file);
        m11174b(Files.m11164a(file.getParentFile()));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract boolean mo23195a(File file, long j, int i);

    /* renamed from: a */
    public void mo23191a(File file) {
        this.f6917a.submit(new C3837a(file));
    }

    /* renamed from: a */
    private long m11171a(List<File> list) {
        long j = 0;
        for (File file : list) {
            j += file.length();
        }
        return j;
    }

    /* renamed from: b */
    private void m11174b(List<File> list) {
        long a = m11171a(list);
        int size = list.size();
        for (File file : list) {
            if (!mo23195a(file, a, size)) {
                long length = file.length();
                if (file.delete()) {
                    size--;
                    a -= length;
                    f6916b.mo28151c("Cache file " + file + " is deleted because it exceeds cache limit");
                } else {
                    f6916b.mo28146a("Error deleting file " + file + " for trimming cache");
                }
            }
        }
    }
}
