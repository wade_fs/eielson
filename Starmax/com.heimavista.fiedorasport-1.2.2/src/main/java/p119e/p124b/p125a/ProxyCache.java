package p119e.p124b.p125a;

import java.lang.Thread;
import java.util.concurrent.atomic.AtomicInteger;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.m */
class ProxyCache {

    /* renamed from: i */
    private static final Logger f6897i = LoggerFactory.m18313a("ProxyCache");

    /* renamed from: a */
    private final Source f6898a;

    /* renamed from: b */
    private final Cache f6899b;

    /* renamed from: c */
    private final Object f6900c = new Object();

    /* renamed from: d */
    private final Object f6901d = new Object();

    /* renamed from: e */
    private final AtomicInteger f6902e;

    /* renamed from: f */
    private volatile Thread f6903f;

    /* renamed from: g */
    private volatile boolean f6904g;

    /* renamed from: h */
    private volatile int f6905h = -1;

    /* renamed from: e.b.a.m$b */
    /* compiled from: ProxyCache */
    private class C3834b implements Runnable {
        private C3834b() {
        }

        public void run() {
            ProxyCache.this.m11138f();
        }
    }

    public ProxyCache(Source pVar, Cache aVar) {
        C3832l.m11127a(pVar);
        this.f6898a = pVar;
        C3832l.m11127a(aVar);
        this.f6899b = aVar;
        this.f6902e = new AtomicInteger();
    }

    /* renamed from: b */
    private void m11133b() {
        int i = this.f6902e.get();
        if (i >= 1) {
            this.f6902e.set(0);
            throw new ProxyCacheException("Error reading source " + i + " times");
        }
    }

    /* renamed from: c */
    private void m11135c() {
        try {
            this.f6898a.close();
        } catch (ProxyCacheException e) {
            mo23188a(new ProxyCacheException("Error closing source " + this.f6898a, e));
        }
    }

    /* renamed from: d */
    private boolean m11136d() {
        return Thread.currentThread().isInterrupted() || this.f6904g;
    }

    /* renamed from: e */
    private void m11137e() {
        this.f6905h = 100;
        mo23161a(this.f6905h);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
        r2 = r2 + ((long) r5);
     */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m11138f() {
        /*
            r8 = this;
            r0 = -1
            r2 = 0
            e.b.a.a r4 = r8.f6899b     // Catch:{ all -> 0x0049 }
            long r2 = r4.available()     // Catch:{ all -> 0x0049 }
            e.b.a.p r4 = r8.f6898a     // Catch:{ all -> 0x0049 }
            r4.mo23173a(r2)     // Catch:{ all -> 0x0049 }
            e.b.a.p r4 = r8.f6898a     // Catch:{ all -> 0x0049 }
            long r0 = r4.length()     // Catch:{ all -> 0x0049 }
            r4 = 8192(0x2000, float:1.14794E-41)
            byte[] r4 = new byte[r4]     // Catch:{ all -> 0x0049 }
        L_0x0019:
            e.b.a.p r5 = r8.f6898a     // Catch:{ all -> 0x0049 }
            int r5 = r5.read(r4)     // Catch:{ all -> 0x0049 }
            r6 = -1
            if (r5 == r6) goto L_0x0042
            java.lang.Object r6 = r8.f6901d     // Catch:{ all -> 0x0049 }
            monitor-enter(r6)     // Catch:{ all -> 0x0049 }
            boolean r7 = r8.m11136d()     // Catch:{ all -> 0x003f }
            if (r7 == 0) goto L_0x0033
            monitor-exit(r6)     // Catch:{ all -> 0x003f }
            r8.m11135c()
            r8.m11134b(r2, r0)
            return
        L_0x0033:
            e.b.a.a r7 = r8.f6899b     // Catch:{ all -> 0x003f }
            r7.mo23153a(r4, r5)     // Catch:{ all -> 0x003f }
            monitor-exit(r6)     // Catch:{ all -> 0x003f }
            long r5 = (long) r5
            long r2 = r2 + r5
            r8.m11134b(r2, r0)     // Catch:{ all -> 0x0049 }
            goto L_0x0019
        L_0x003f:
            r4 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x003f }
            throw r4     // Catch:{ all -> 0x0049 }
        L_0x0042:
            r8.m11140h()     // Catch:{ all -> 0x0049 }
            r8.m11137e()     // Catch:{ all -> 0x0049 }
            goto L_0x0052
        L_0x0049:
            r4 = move-exception
            java.util.concurrent.atomic.AtomicInteger r5 = r8.f6902e     // Catch:{ all -> 0x0059 }
            r5.incrementAndGet()     // Catch:{ all -> 0x0059 }
            r8.mo23188a(r4)     // Catch:{ all -> 0x0059 }
        L_0x0052:
            r8.m11135c()
            r8.m11134b(r2, r0)
            return
        L_0x0059:
            r4 = move-exception
            r8.m11135c()
            r8.m11134b(r2, r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p124b.p125a.ProxyCache.m11138f():void");
    }

    /* renamed from: g */
    private synchronized void m11139g() {
        boolean z = (this.f6903f == null || this.f6903f.getState() == Thread.State.TERMINATED) ? false : true;
        if (!this.f6904g && !this.f6899b.mo23154a() && !z) {
            C3834b bVar = new C3834b();
            this.f6903f = new Thread(bVar, "Source reader for " + this.f6898a);
            this.f6903f.start();
        }
    }

    /* renamed from: h */
    private void m11140h() {
        synchronized (this.f6901d) {
            if (!m11136d() && this.f6899b.available() == this.f6898a.length()) {
                this.f6899b.complete();
            }
        }
    }

    /* renamed from: i */
    private void m11141i() {
        synchronized (this.f6900c) {
            try {
                this.f6900c.wait(1000);
            } catch (InterruptedException e) {
                throw new ProxyCacheException("Waiting source data is interrupted!", e);
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23161a(int i) {
        throw null;
    }

    /* renamed from: a */
    public int mo23185a(byte[] bArr, long j, int i) {
        ProxyCacheUtils.m11150a(bArr, j, i);
        while (!this.f6899b.mo23154a() && this.f6899b.available() < ((long) i) + j && !this.f6904g) {
            m11139g();
            m11141i();
            m11133b();
        }
        int a = this.f6899b.mo23152a(bArr, j, i);
        if (this.f6899b.mo23154a() && this.f6905h != 100) {
            this.f6905h = 100;
            mo23161a(100);
        }
        return a;
    }

    /* renamed from: b */
    private void m11134b(long j, long j2) {
        mo23187a(j, j2);
        synchronized (this.f6900c) {
            this.f6900c.notifyAll();
        }
    }

    /* renamed from: a */
    public void mo23186a() {
        synchronized (this.f6901d) {
            Logger bVar = f6897i;
            bVar.mo28150b("Shutdown proxy for " + this.f6898a);
            try {
                this.f6904g = true;
                if (this.f6903f != null) {
                    this.f6903f.interrupt();
                }
                this.f6899b.close();
            } catch (ProxyCacheException e) {
                mo23188a(e);
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23187a(long j, long j2) {
        boolean z = true;
        int i = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        int i2 = i == 0 ? 100 : (int) ((((float) j) / ((float) j2)) * 100.0f);
        boolean z2 = i2 != this.f6905h;
        if (i < 0) {
            z = false;
        }
        if (z && z2) {
            mo23161a(i2);
        }
        this.f6905h = i2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo23188a(Throwable th) {
        if (th instanceof InterruptedProxyCacheException) {
            f6897i.mo28150b("ProxyCache is interrupted");
        } else {
            f6897i.mo28149a("ProxyCache error", th);
        }
    }
}
