package p119e.p124b.p125a.p126s;

import java.io.File;

/* renamed from: e.b.a.s.g */
public class TotalSizeLruDiskUsage extends LruDiskUsage {

    /* renamed from: c */
    private final long f6920c;

    public TotalSizeLruDiskUsage(long j) {
        if (j > 0) {
            this.f6920c = j;
            return;
        }
        throw new IllegalArgumentException("Max size must be positive number!");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo23195a(File file, long j, int i) {
        return j <= this.f6920c;
    }
}
