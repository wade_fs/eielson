package p119e.p124b.p125a.p126s;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.s.d */
class Files {

    /* renamed from: a */
    private static final Logger f6915a = LoggerFactory.m18313a("Files");

    /* renamed from: e.b.a.s.d$b */
    /* compiled from: Files */
    private static final class C3836b implements Comparator<File> {
        private C3836b() {
        }

        /* renamed from: a */
        private int m11169a(long j, long j2) {
            int i = (j > j2 ? 1 : (j == j2 ? 0 : -1));
            if (i < 0) {
                return -1;
            }
            return i == 0 ? 0 : 1;
        }

        /* renamed from: a */
        public int compare(File file, File file2) {
            return m11169a(file.lastModified(), file2.lastModified());
        }
    }

    /* renamed from: a */
    static List<File> m11164a(File file) {
        LinkedList linkedList = new LinkedList();
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return linkedList;
        }
        List<File> asList = Arrays.asList(listFiles);
        Collections.sort(asList, new C3836b());
        return asList;
    }

    /* renamed from: b */
    static void m11165b(File file) {
        if (file.exists()) {
            if (!file.isDirectory()) {
                throw new IOException("File " + file + " is not directory!");
            }
        } else if (!file.mkdirs()) {
            throw new IOException(String.format("Directory %s can't be created", file.getAbsolutePath()));
        }
    }

    /* renamed from: c */
    static void m11166c(File file) {
        long length = file.length();
        if (length == 0) {
            m11167d(file);
            return;
        }
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rwd");
        long j = length - 1;
        randomAccessFile.seek(j);
        byte readByte = randomAccessFile.readByte();
        randomAccessFile.seek(j);
        randomAccessFile.write(readByte);
        randomAccessFile.close();
    }

    /* renamed from: d */
    private static void m11167d(File file) {
        if (!file.delete() || !file.createNewFile()) {
            throw new IOException("Error recreate zero-size file " + file);
        }
    }

    /* renamed from: e */
    static void m11168e(File file) {
        if (file.exists()) {
            long currentTimeMillis = System.currentTimeMillis();
            if (!file.setLastModified(currentTimeMillis)) {
                m11166c(file);
                if (file.lastModified() < currentTimeMillis) {
                    f6915a.mo28148a("Last modified date {} is not set for file {}", new Date(file.lastModified()), file.getAbsolutePath());
                }
            }
        }
    }
}
