package p119e.p124b.p125a;

/* renamed from: e.b.a.a */
public interface Cache {
    /* renamed from: a */
    int mo23152a(byte[] bArr, long j, int i);

    /* renamed from: a */
    void mo23153a(byte[] bArr, int i);

    /* renamed from: a */
    boolean mo23154a();

    long available();

    void close();

    void complete();
}
