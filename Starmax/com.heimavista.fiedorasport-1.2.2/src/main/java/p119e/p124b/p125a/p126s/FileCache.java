package p119e.p124b.p125a.p126s;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import p119e.p124b.p125a.Cache;
import p119e.p124b.p125a.ProxyCacheException;

/* renamed from: e.b.a.s.b */
public class FileCache implements Cache {

    /* renamed from: a */
    private final DiskUsage f6912a;

    /* renamed from: b */
    public File f6913b;

    /* renamed from: c */
    private RandomAccessFile f6914c;

    public FileCache(File file, DiskUsage aVar) {
        File file2;
        if (aVar != null) {
            try {
                this.f6912a = aVar;
                Files.m11165b(file.getParentFile());
                boolean exists = file.exists();
                if (exists) {
                    file2 = file;
                } else {
                    File parentFile = file.getParentFile();
                    file2 = new File(parentFile, file.getName() + ".download");
                }
                this.f6913b = file2;
                this.f6914c = new RandomAccessFile(this.f6913b, exists ? "r" : "rw");
            } catch (IOException e) {
                throw new ProxyCacheException("Error using file " + file + " as disc cache", e);
            }
        } else {
            throw new NullPointerException();
        }
    }

    /* renamed from: a */
    public synchronized int mo23152a(byte[] bArr, long j, int i) {
        try {
            this.f6914c.seek(j);
        } catch (IOException e) {
            throw new ProxyCacheException(String.format("Error reading %d bytes with offset %d from file[%d bytes] to buffer[%d bytes]", Integer.valueOf(i), Long.valueOf(j), Long.valueOf(available()), Integer.valueOf(bArr.length)), e);
        }
        return this.f6914c.read(bArr, 0, i);
    }

    public synchronized long available() {
        try {
        } catch (IOException e) {
            throw new ProxyCacheException("Error reading length of file " + this.f6913b, e);
        }
        return (long) ((int) this.f6914c.length());
    }

    public synchronized void close() {
        try {
            this.f6914c.close();
            this.f6912a.mo23191a(this.f6913b);
        } catch (IOException e) {
            throw new ProxyCacheException("Error closing file " + this.f6913b, e);
        }
    }

    public synchronized void complete() {
        if (!mo23154a()) {
            close();
            File file = new File(this.f6913b.getParentFile(), this.f6913b.getName().substring(0, this.f6913b.getName().length() - 9));
            if (this.f6913b.renameTo(file)) {
                this.f6913b = file;
                try {
                    this.f6914c = new RandomAccessFile(this.f6913b, "r");
                    this.f6912a.mo23191a(this.f6913b);
                } catch (IOException e) {
                    throw new ProxyCacheException("Error opening " + this.f6913b + " as disc cache", e);
                }
            } else {
                throw new ProxyCacheException("Error renaming file " + this.f6913b + " to " + file + " for completion!");
            }
        }
    }

    /* renamed from: a */
    public synchronized void mo23153a(byte[] bArr, int i) {
        try {
            if (!mo23154a()) {
                this.f6914c.seek(available());
                this.f6914c.write(bArr, 0, i);
            } else {
                throw new ProxyCacheException("Error append cache: cache file " + this.f6913b + " is completed!");
            }
        } catch (IOException e) {
            throw new ProxyCacheException(String.format("Error writing %d bytes to %s from buffer with size %d", Integer.valueOf(i), this.f6914c, Integer.valueOf(bArr.length)), e);
        }
    }

    /* renamed from: a */
    public synchronized boolean mo23154a() {
        return !m11159a(this.f6913b);
    }

    /* renamed from: a */
    private boolean m11159a(File file) {
        return file.getName().endsWith(".download");
    }
}
