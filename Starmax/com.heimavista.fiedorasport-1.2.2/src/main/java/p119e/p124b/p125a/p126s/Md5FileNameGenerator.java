package p119e.p124b.p125a.p126s;

import android.text.TextUtils;
import p119e.p124b.p125a.ProxyCacheUtils;

/* renamed from: e.b.a.s.f */
public class Md5FileNameGenerator implements FileNameGenerator {
    /* renamed from: b */
    private String m11177b(String str) {
        int lastIndexOf = str.lastIndexOf(46);
        return (lastIndexOf == -1 || lastIndexOf <= str.lastIndexOf(47) || (lastIndexOf + 2) + 4 <= str.length()) ? "" : str.substring(lastIndexOf + 1, str.length());
    }

    /* renamed from: a */
    public String mo23192a(String str) {
        String b = m11177b(str);
        String a = ProxyCacheUtils.m11147a(str);
        if (TextUtils.isEmpty(b)) {
            return a;
        }
        return a + "." + b;
    }
}
