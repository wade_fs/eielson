package p119e.p124b.p125a;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Locale;
import p119e.p124b.p125a.p126s.FileCache;

/* renamed from: e.b.a.e */
class HttpProxyCache extends ProxyCache {

    /* renamed from: j */
    private final HttpUrlSource f6855j;

    /* renamed from: k */
    private final FileCache f6856k;

    /* renamed from: l */
    private CacheListener f6857l;

    public HttpProxyCache(HttpUrlSource hVar, FileCache bVar) {
        super(hVar, bVar);
        this.f6856k = bVar;
        this.f6855j = hVar;
    }

    /* renamed from: b */
    private String m11081b(GetRequest dVar) {
        String a = this.f6855j.mo23172a();
        boolean z = !TextUtils.isEmpty(a);
        long available = this.f6856k.mo23154a() ? this.f6856k.available() : this.f6855j.length();
        boolean z2 = available >= 0;
        long j = dVar.f6854c ? available - dVar.f6853b : available;
        boolean z3 = z2 && dVar.f6854c;
        StringBuilder sb = new StringBuilder();
        sb.append(dVar.f6854c ? "HTTP/1.1 206 PARTIAL CONTENT\n" : "HTTP/1.1 200 OK\n");
        sb.append("Accept-Ranges: bytes\n");
        String str = "";
        sb.append(z2 ? m11078a("Content-Length: %d\n", Long.valueOf(j)) : str);
        sb.append(z3 ? m11078a("Content-Range: bytes %d-%d/%d\n", Long.valueOf(dVar.f6853b), Long.valueOf(available - 1), Long.valueOf(available)) : str);
        if (z) {
            str = m11078a("Content-Type: %s\n", a);
        }
        sb.append(str);
        sb.append("\n");
        return sb.toString();
    }

    /* renamed from: a */
    public void mo23162a(CacheListener bVar) {
        this.f6857l = bVar;
    }

    /* renamed from: a */
    public void mo23163a(GetRequest dVar, Socket socket) {
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(socket.getOutputStream());
        bufferedOutputStream.write(m11081b(dVar).getBytes(C1750C.UTF8_NAME));
        long j = dVar.f6853b;
        if (m11080a(dVar)) {
            m11079a(bufferedOutputStream, j);
        } else {
            m11082b(bufferedOutputStream, j);
        }
    }

    /* renamed from: a */
    private boolean m11080a(GetRequest dVar) {
        long length = this.f6855j.length();
        boolean z = length > 0;
        long available = this.f6856k.available();
        if (!z || !dVar.f6854c || ((float) dVar.f6853b) <= ((float) available) + (((float) length) * 0.2f)) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    private void m11079a(OutputStream outputStream, long j) {
        byte[] bArr = new byte[8192];
        while (true) {
            int a = mo23185a(bArr, j, bArr.length);
            if (a != -1) {
                outputStream.write(bArr, 0, a);
                j += (long) a;
            } else {
                outputStream.flush();
                return;
            }
        }
    }

    /* renamed from: b */
    private void m11082b(OutputStream outputStream, long j) {
        HttpUrlSource hVar = new HttpUrlSource(this.f6855j);
        try {
            hVar.mo23173a((long) ((int) j));
            byte[] bArr = new byte[8192];
            while (true) {
                int read = hVar.read(bArr);
                if (read != -1) {
                    outputStream.write(bArr, 0, read);
                } else {
                    outputStream.flush();
                    return;
                }
            }
        } finally {
            hVar.close();
        }
    }

    /* renamed from: a */
    private String m11078a(String str, Object... objArr) {
        return String.format(Locale.US, str, objArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo23161a(int i) {
        CacheListener bVar = this.f6857l;
        if (bVar != null) {
            bVar.mo23158a(this.f6856k.f6913b, this.f6855j.mo23174b(), i);
        }
    }
}
