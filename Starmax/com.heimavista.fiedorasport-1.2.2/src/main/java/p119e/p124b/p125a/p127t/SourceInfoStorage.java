package p119e.p124b.p125a.p127t;

import p119e.p124b.p125a.SourceInfo;

/* renamed from: e.b.a.t.c */
public interface SourceInfoStorage {
    /* renamed from: a */
    void mo23197a(String str, SourceInfo qVar);

    SourceInfo get(String str);
}
