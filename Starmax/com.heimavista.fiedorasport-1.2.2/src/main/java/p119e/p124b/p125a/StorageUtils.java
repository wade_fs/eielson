package p119e.p124b.p125a;

import android.content.Context;
import android.os.Environment;
import java.io.File;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.r */
final class StorageUtils {

    /* renamed from: a */
    private static final Logger f6911a = LoggerFactory.m18313a("StorageUtils");

    /* renamed from: a */
    private static File m11156a(Context context, boolean z) {
        String str;
        try {
            str = Environment.getExternalStorageState();
        } catch (NullPointerException unused) {
            str = "";
        }
        File a = (!z || !"mounted".equals(str)) ? null : m11155a(context);
        if (a == null) {
            a = context.getCacheDir();
        }
        if (a != null) {
            return a;
        }
        String str2 = "/data/data/" + context.getPackageName() + "/cache/";
        f6911a.mo28152d("Can't define system cache directory! '" + str2 + "%s' will be used.");
        return new File(str2);
    }

    /* renamed from: b */
    public static File m11157b(Context context) {
        return new File(m11156a(context, true), "video-cache");
    }

    /* renamed from: a */
    private static File m11155a(Context context) {
        File file = new File(new File(new File(new File(Environment.getExternalStorageDirectory(), "Android"), "data"), context.getPackageName()), "cache");
        if (file.exists() || file.mkdirs()) {
            return file;
        }
        f6911a.mo28152d("Unable to create external cache directory");
        return null;
    }
}
