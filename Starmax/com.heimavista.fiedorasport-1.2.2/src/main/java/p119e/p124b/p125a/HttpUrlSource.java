package p119e.p124b.p125a;

import android.text.TextUtils;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.URL;
import p119e.p124b.p125a.p127t.SourceInfoStorage;
import p119e.p124b.p125a.p127t.SourceInfoStorageFactory;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.h */
public class HttpUrlSource implements Source {

    /* renamed from: e */
    private static final Logger f6883e = LoggerFactory.m18313a("HttpUrlSource");

    /* renamed from: a */
    private final SourceInfoStorage f6884a;

    /* renamed from: b */
    private SourceInfo f6885b;

    /* renamed from: c */
    private HttpURLConnection f6886c;

    /* renamed from: d */
    private InputStream f6887d;

    public HttpUrlSource(String str) {
        this(str, SourceInfoStorageFactory.m11185a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a.b.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.io.IOException]
     candidates:
      k.a.b.a(java.lang.String, java.lang.Object):void
      k.a.b.a(java.lang.String, java.lang.Throwable):void */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0094  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m11115c() {
        /*
            r7 = this;
            k.a.b r0 = p119e.p124b.p125a.HttpUrlSource.f6883e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Read content info from "
            r1.append(r2)
            e.b.a.q r2 = r7.f6885b
            java.lang.String r2 = r2.f6908a
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r0.mo28150b(r1)
            r0 = 0
            r2 = 10000(0x2710, float:1.4013E-41)
            r3 = 0
            java.net.HttpURLConnection r0 = r7.m11114a(r0, r2)     // Catch:{ IOException -> 0x006a, all -> 0x0067 }
            long r1 = r7.m11112a(r0)     // Catch:{ IOException -> 0x0065 }
            java.lang.String r4 = r0.getContentType()     // Catch:{ IOException -> 0x0065 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ IOException -> 0x0065 }
            e.b.a.q r5 = new e.b.a.q     // Catch:{ IOException -> 0x0065 }
            e.b.a.q r6 = r7.f6885b     // Catch:{ IOException -> 0x0065 }
            java.lang.String r6 = r6.f6908a     // Catch:{ IOException -> 0x0065 }
            r5.<init>(r6, r1, r4)     // Catch:{ IOException -> 0x0065 }
            r7.f6885b = r5     // Catch:{ IOException -> 0x0065 }
            e.b.a.t.c r1 = r7.f6884a     // Catch:{ IOException -> 0x0065 }
            e.b.a.q r2 = r7.f6885b     // Catch:{ IOException -> 0x0065 }
            java.lang.String r2 = r2.f6908a     // Catch:{ IOException -> 0x0065 }
            e.b.a.q r4 = r7.f6885b     // Catch:{ IOException -> 0x0065 }
            r1.mo23197a(r2, r4)     // Catch:{ IOException -> 0x0065 }
            k.a.b r1 = p119e.p124b.p125a.HttpUrlSource.f6883e     // Catch:{ IOException -> 0x0065 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0065 }
            r2.<init>()     // Catch:{ IOException -> 0x0065 }
            java.lang.String r4 = "Source info fetched: "
            r2.append(r4)     // Catch:{ IOException -> 0x0065 }
            e.b.a.q r4 = r7.f6885b     // Catch:{ IOException -> 0x0065 }
            r2.append(r4)     // Catch:{ IOException -> 0x0065 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0065 }
            r1.mo28150b(r2)     // Catch:{ IOException -> 0x0065 }
            p119e.p124b.p125a.ProxyCacheUtils.m11149a(r3)
            if (r0 == 0) goto L_0x008e
            goto L_0x008b
        L_0x0063:
            r1 = move-exception
            goto L_0x008f
        L_0x0065:
            r1 = move-exception
            goto L_0x006c
        L_0x0067:
            r1 = move-exception
            r0 = r3
            goto L_0x008f
        L_0x006a:
            r1 = move-exception
            r0 = r3
        L_0x006c:
            k.a.b r2 = p119e.p124b.p125a.HttpUrlSource.f6883e     // Catch:{ all -> 0x0063 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0063 }
            r4.<init>()     // Catch:{ all -> 0x0063 }
            java.lang.String r5 = "Error fetching info from "
            r4.append(r5)     // Catch:{ all -> 0x0063 }
            e.b.a.q r5 = r7.f6885b     // Catch:{ all -> 0x0063 }
            java.lang.String r5 = r5.f6908a     // Catch:{ all -> 0x0063 }
            r4.append(r5)     // Catch:{ all -> 0x0063 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0063 }
            r2.mo28149a(r4, r1)     // Catch:{ all -> 0x0063 }
            p119e.p124b.p125a.ProxyCacheUtils.m11149a(r3)
            if (r0 == 0) goto L_0x008e
        L_0x008b:
            r0.disconnect()
        L_0x008e:
            return
        L_0x008f:
            p119e.p124b.p125a.ProxyCacheUtils.m11149a(r3)
            if (r0 == 0) goto L_0x0097
            r0.disconnect()
        L_0x0097:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p124b.p125a.HttpUrlSource.m11115c():void");
    }

    /* renamed from: a */
    public void mo23173a(long j) {
        try {
            this.f6886c = m11114a(j, -1);
            String contentType = this.f6886c.getContentType();
            this.f6887d = new BufferedInputStream(this.f6886c.getInputStream(), 8192);
            this.f6885b = new SourceInfo(this.f6885b.f6908a, m11113a(this.f6886c, j, this.f6886c.getResponseCode()), contentType);
            this.f6884a.mo23197a(this.f6885b.f6908a, this.f6885b);
        } catch (IOException e) {
            throw new ProxyCacheException("Error opening connection for " + this.f6885b.f6908a + " with offset " + j, e);
        }
    }

    /* renamed from: b */
    public String mo23174b() {
        return this.f6885b.f6908a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a.b.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.lang.ArrayIndexOutOfBoundsException]
     candidates:
      k.a.b.a(java.lang.String, java.lang.Object):void
      k.a.b.a(java.lang.String, java.lang.Throwable):void */
    public void close() {
        HttpURLConnection httpURLConnection = this.f6886c;
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (IllegalArgumentException | NullPointerException e) {
                throw new RuntimeException("Wait... but why? WTF!? Really shouldn't happen any more after fixing https://github.com/danikula/AndroidVideoCache/issues/43. If you read it on your device log, please, notify me danikula@gmail.com or create issue here https://github.com/danikula/AndroidVideoCache/issues.", e);
            } catch (ArrayIndexOutOfBoundsException e2) {
                f6883e.mo28149a("Error closing connection correctly. Should happen only on Android L. If anybody know how to fix it, please visit https://github.com/danikula/AndroidVideoCache/issues/88. Until good solution is not know, just ignore this issue :(", (Throwable) e2);
            }
        }
    }

    public synchronized long length() {
        if (this.f6885b.f6909b == -2147483648L) {
            m11115c();
        }
        return this.f6885b.f6909b;
    }

    public int read(byte[] bArr) {
        InputStream inputStream = this.f6887d;
        if (inputStream != null) {
            try {
                return inputStream.read(bArr, 0, bArr.length);
            } catch (InterruptedIOException e) {
                throw new InterruptedProxyCacheException("Reading source " + this.f6885b.f6908a + " is interrupted", e);
            } catch (IOException e2) {
                throw new ProxyCacheException("Error reading data from " + this.f6885b.f6908a, e2);
            }
        } else {
            throw new ProxyCacheException("Error reading data from " + this.f6885b.f6908a + ": connection is absent!");
        }
    }

    public String toString() {
        return "HttpUrlSource{sourceInfo='" + this.f6885b + "}";
    }

    public HttpUrlSource(String str, SourceInfoStorage cVar) {
        C3832l.m11127a(cVar);
        this.f6884a = cVar;
        SourceInfo qVar = cVar.get(str);
        this.f6885b = qVar == null ? new SourceInfo(str, -2147483648L, ProxyCacheUtils.m11153d(str)) : qVar;
    }

    public HttpUrlSource(HttpUrlSource hVar) {
        this.f6885b = hVar.f6885b;
        this.f6884a = hVar.f6884a;
    }

    /* renamed from: a */
    private long m11113a(HttpURLConnection httpURLConnection, long j, int i) {
        long a = m11112a(httpURLConnection);
        if (i == 200) {
            return a;
        }
        if (i == 206) {
            return a + j;
        }
        return this.f6885b.f6909b;
    }

    /* renamed from: a */
    private long m11112a(HttpURLConnection httpURLConnection) {
        String headerField = httpURLConnection.getHeaderField("Content-Length");
        if (headerField == null) {
            return -1;
        }
        return Long.parseLong(headerField);
    }

    /* renamed from: a */
    private HttpURLConnection m11114a(long j, int i) {
        String str;
        HttpURLConnection httpURLConnection;
        boolean z;
        String str2 = this.f6885b.f6908a;
        int i2 = 0;
        do {
            Logger bVar = f6883e;
            StringBuilder sb = new StringBuilder();
            sb.append("Open connection ");
            int i3 = (j > 0 ? 1 : (j == 0 ? 0 : -1));
            if (i3 > 0) {
                str = " with offset " + j;
            } else {
                str = "";
            }
            sb.append(str);
            sb.append(" to ");
            sb.append(str2);
            bVar.mo28150b(sb.toString());
            httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            if (i3 > 0) {
                httpURLConnection.setRequestProperty("Range", "bytes=" + j + "-");
            }
            if (i > 0) {
                httpURLConnection.setConnectTimeout(i);
                httpURLConnection.setReadTimeout(i);
            }
            int responseCode = httpURLConnection.getResponseCode();
            z = responseCode == 301 || responseCode == 302 || responseCode == 303;
            if (z) {
                str2 = httpURLConnection.getHeaderField("Location");
                i2++;
                httpURLConnection.disconnect();
            }
            if (i2 > 5) {
                throw new ProxyCacheException("Too many redirects: " + i2);
            }
        } while (z);
        return httpURLConnection;
    }

    /* renamed from: a */
    public synchronized String mo23172a() {
        if (TextUtils.isEmpty(this.f6885b.f6910c)) {
            m11115c();
        }
        return this.f6885b.f6910c;
    }
}
