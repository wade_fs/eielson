package p119e.p124b.p125a;

import java.io.IOException;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.util.Arrays;
import java.util.List;

/* renamed from: e.b.a.i */
class IgnoreHostProxySelector extends ProxySelector {

    /* renamed from: d */
    private static final List<Proxy> f6888d = Arrays.asList(Proxy.NO_PROXY);

    /* renamed from: a */
    private final ProxySelector f6889a;

    /* renamed from: b */
    private final String f6890b;

    /* renamed from: c */
    private final int f6891c;

    IgnoreHostProxySelector(ProxySelector superR, String str, int i) {
        C3832l.m11127a(superR);
        this.f6889a = superR;
        C3832l.m11127a(str);
        this.f6890b = str;
        this.f6891c = i;
    }

    /* renamed from: a */
    static void m11119a(String str, int i) {
        ProxySelector.setDefault(new IgnoreHostProxySelector(ProxySelector.getDefault(), str, i));
    }

    public void connectFailed(URI uri, SocketAddress socketAddress, IOException iOException) {
        this.f6889a.connectFailed(uri, socketAddress, iOException);
    }

    public List<Proxy> select(URI uri) {
        return this.f6890b.equals(uri.getHost()) && this.f6891c == uri.getPort() ? f6888d : this.f6889a.select(uri);
    }
}
