package p119e.p124b.p125a;

/* renamed from: e.b.a.n */
public class ProxyCacheException extends Exception {
    public ProxyCacheException(String str) {
        super(str + ". Version: 2.7.0");
    }

    public ProxyCacheException(String str, Throwable th) {
        super(str + ". Version: 2.7.0", th);
    }
}
