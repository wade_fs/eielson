package p119e.p124b.p125a;

/* renamed from: e.b.a.j */
public class InterruptedProxyCacheException extends ProxyCacheException {
    public InterruptedProxyCacheException(String str, Throwable th) {
        super(str, th);
    }
}
