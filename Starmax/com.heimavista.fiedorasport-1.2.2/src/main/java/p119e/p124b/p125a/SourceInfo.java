package p119e.p124b.p125a;

/* renamed from: e.b.a.q */
public class SourceInfo {

    /* renamed from: a */
    public final String f6908a;

    /* renamed from: b */
    public final long f6909b;

    /* renamed from: c */
    public final String f6910c;

    public SourceInfo(String str, long j, String str2) {
        this.f6908a = str;
        this.f6909b = j;
        this.f6910c = str2;
    }

    public String toString() {
        return "SourceInfo{url='" + this.f6908a + '\'' + ", length=" + this.f6909b + ", mime='" + this.f6910c + '\'' + '}';
    }
}
