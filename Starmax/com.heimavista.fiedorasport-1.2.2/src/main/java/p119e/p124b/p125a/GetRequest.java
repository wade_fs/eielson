package p119e.p124b.p125a;

import android.text.TextUtils;
import com.google.android.exoplayer2.C1750C;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: e.b.a.d */
class GetRequest {

    /* renamed from: d */
    private static final Pattern f6850d = Pattern.compile("[R,r]ange:[ ]?bytes=(\\d*)-");

    /* renamed from: e */
    private static final Pattern f6851e = Pattern.compile("GET /(.*) HTTP");

    /* renamed from: a */
    public final String f6852a;

    /* renamed from: b */
    public final long f6853b;

    /* renamed from: c */
    public final boolean f6854c;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public GetRequest(String str) {
        C3832l.m11127a(str);
        long a = m11075a(str);
        this.f6853b = Math.max(0L, a);
        this.f6854c = a >= 0;
        this.f6852a = m11077b(str);
    }

    /* renamed from: a */
    public static GetRequest m11076a(InputStream inputStream) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, C1750C.UTF8_NAME));
        StringBuilder sb = new StringBuilder();
        while (true) {
            String readLine = bufferedReader.readLine();
            if (TextUtils.isEmpty(readLine)) {
                return new GetRequest(sb.toString());
            }
            sb.append(readLine);
            sb.append(10);
        }
    }

    /* renamed from: b */
    private String m11077b(String str) {
        Matcher matcher = f6851e.matcher(str);
        if (matcher.find()) {
            return matcher.group(1);
        }
        throw new IllegalArgumentException("Invalid request `" + str + "`: url not found!");
    }

    public String toString() {
        return "GetRequest{rangeOffset=" + this.f6853b + ", partial=" + this.f6854c + ", uri='" + this.f6852a + '\'' + '}';
    }

    /* renamed from: a */
    private long m11075a(String str) {
        Matcher matcher = f6850d.matcher(str);
        if (matcher.find()) {
            return Long.parseLong(matcher.group(1));
        }
        return -1;
    }
}
