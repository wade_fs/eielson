package p119e.p124b.p125a;

import java.io.File;
import p119e.p124b.p125a.p126s.DiskUsage;
import p119e.p124b.p125a.p126s.FileNameGenerator;
import p119e.p124b.p125a.p127t.SourceInfoStorage;

/* renamed from: e.b.a.c */
class Config {

    /* renamed from: a */
    public final File f6846a;

    /* renamed from: b */
    public final FileNameGenerator f6847b;

    /* renamed from: c */
    public final DiskUsage f6848c;

    /* renamed from: d */
    public final SourceInfoStorage f6849d;

    Config(File file, FileNameGenerator cVar, DiskUsage aVar, SourceInfoStorage cVar2) {
        this.f6846a = file;
        this.f6847b = cVar;
        this.f6848c = aVar;
        this.f6849d = cVar2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public File mo23159a(String str) {
        return new File(this.f6846a, this.f6847b.mo23192a(str));
    }
}
