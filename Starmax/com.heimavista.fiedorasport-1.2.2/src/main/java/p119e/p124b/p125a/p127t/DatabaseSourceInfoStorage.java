package p119e.p124b.p125a.p127t;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import p119e.p124b.p125a.C3832l;
import p119e.p124b.p125a.SourceInfo;

/* renamed from: e.b.a.t.a */
class DatabaseSourceInfoStorage extends SQLiteOpenHelper implements SourceInfoStorage {

    /* renamed from: P */
    private static final String[] f6921P = {"_id", "url", "length", "mime"};

    DatabaseSourceInfoStorage(Context context) {
        super(context, "AndroidVideoCache.db", (SQLiteDatabase.CursorFactory) null, 1);
        C3832l.m11127a(context);
    }

    /* renamed from: a */
    public void mo23197a(String str, SourceInfo qVar) {
        C3832l.m11131a(str, qVar);
        boolean z = get(str) != null;
        ContentValues a = m11180a(qVar);
        if (z) {
            getWritableDatabase().update("SourceInfo", a, "url=?", new String[]{str});
        } else {
            getWritableDatabase().insert("SourceInfo", null, a);
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v2, types: [e.b.a.q] */
    /* JADX WARN: Type inference failed for: r0v4 */
    /* JADX WARN: Type inference failed for: r0v6 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0037  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public p119e.p124b.p125a.SourceInfo get(java.lang.String r11) {
        /*
            r10 = this;
            p119e.p124b.p125a.C3832l.m11127a(r11)
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r10.getReadableDatabase()     // Catch:{ all -> 0x0034 }
            java.lang.String r2 = "SourceInfo"
            java.lang.String[] r3 = p119e.p124b.p125a.p127t.DatabaseSourceInfoStorage.f6921P     // Catch:{ all -> 0x0034 }
            java.lang.String r4 = "url=?"
            r5 = 1
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ all -> 0x0034 }
            r6 = 0
            r5[r6] = r11     // Catch:{ all -> 0x0034 }
            r6 = 0
            r7 = 0
            r8 = 0
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0034 }
            if (r11 == 0) goto L_0x002e
            boolean r1 = r11.moveToFirst()     // Catch:{ all -> 0x0029 }
            if (r1 != 0) goto L_0x0024
            goto L_0x002e
        L_0x0024:
            e.b.a.q r0 = r10.m11181a(r11)     // Catch:{ all -> 0x0029 }
            goto L_0x002e
        L_0x0029:
            r0 = move-exception
            r9 = r0
            r0 = r11
            r11 = r9
            goto L_0x0035
        L_0x002e:
            if (r11 == 0) goto L_0x0033
            r11.close()
        L_0x0033:
            return r0
        L_0x0034:
            r11 = move-exception
        L_0x0035:
            if (r0 == 0) goto L_0x003a
            r0.close()
        L_0x003a:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p124b.p125a.p127t.DatabaseSourceInfoStorage.get(java.lang.String):e.b.a.q");
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        C3832l.m11127a(sQLiteDatabase);
        sQLiteDatabase.execSQL("CREATE TABLE SourceInfo (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,url TEXT NOT NULL,mime TEXT,length INTEGER);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        throw new IllegalStateException("Should not be called. There is no any migration");
    }

    /* renamed from: a */
    private SourceInfo m11181a(Cursor cursor) {
        return new SourceInfo(cursor.getString(cursor.getColumnIndexOrThrow("url")), cursor.getLong(cursor.getColumnIndexOrThrow("length")), cursor.getString(cursor.getColumnIndexOrThrow("mime")));
    }

    /* renamed from: a */
    private ContentValues m11180a(SourceInfo qVar) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("url", qVar.f6908a);
        contentValues.put("length", Long.valueOf(qVar.f6909b));
        contentValues.put("mime", qVar.f6910c);
        return contentValues;
    }
}
