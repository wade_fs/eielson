package p119e.p124b.p125a;

import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import p119e.p124b.p125a.p126s.DiskUsage;
import p119e.p124b.p125a.p126s.FileNameGenerator;
import p119e.p124b.p125a.p126s.Md5FileNameGenerator;
import p119e.p124b.p125a.p126s.TotalSizeLruDiskUsage;
import p119e.p124b.p125a.p127t.SourceInfoStorage;
import p119e.p124b.p125a.p127t.SourceInfoStorageFactory;
import p245k.p246a.Logger;
import p245k.p246a.LoggerFactory;

/* renamed from: e.b.a.f */
public class HttpProxyCacheServer {

    /* renamed from: i */
    private static final Logger f6858i = LoggerFactory.m18313a("HttpProxyCacheServer");

    /* renamed from: a */
    private final Object f6859a;

    /* renamed from: b */
    private final ExecutorService f6860b;

    /* renamed from: c */
    private final Map<String, HttpProxyCacheServerClients> f6861c;

    /* renamed from: d */
    private final ServerSocket f6862d;

    /* renamed from: e */
    private final int f6863e;

    /* renamed from: f */
    private final Thread f6864f;

    /* renamed from: g */
    private final Config f6865g;

    /* renamed from: h */
    private final Pinger f6866h;

    /* renamed from: e.b.a.f$a */
    /* compiled from: HttpProxyCacheServer */
    public static final class C3826a {

        /* renamed from: a */
        private File f6867a;

        /* renamed from: b */
        private FileNameGenerator f6868b = new Md5FileNameGenerator();

        /* renamed from: c */
        private DiskUsage f6869c = new TotalSizeLruDiskUsage(536870912);

        /* renamed from: d */
        private SourceInfoStorage f6870d;

        public C3826a(Context context) {
            this.f6870d = SourceInfoStorageFactory.m11186a(context);
            this.f6867a = StorageUtils.m11157b(context);
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public Config m11104a() {
            return new Config(this.f6867a, this.f6868b, this.f6869c, this.f6870d);
        }
    }

    /* renamed from: e.b.a.f$b */
    /* compiled from: HttpProxyCacheServer */
    private final class C3827b implements Runnable {

        /* renamed from: P */
        private final Socket f6871P;

        public C3827b(Socket socket) {
            this.f6871P = socket;
        }

        public void run() {
            HttpProxyCacheServer.this.m11098d(this.f6871P);
        }
    }

    /* renamed from: e.b.a.f$c */
    /* compiled from: HttpProxyCacheServer */
    private final class C3828c implements Runnable {

        /* renamed from: P */
        private final CountDownLatch f6873P;

        public C3828c(CountDownLatch countDownLatch) {
            this.f6873P = countDownLatch;
        }

        public void run() {
            this.f6873P.countDown();
            HttpProxyCacheServer.this.m11095c();
        }
    }

    public HttpProxyCacheServer(Context context) {
        this(new C3826a(context).m11104a());
    }

    /* renamed from: c */
    private String m11094c(String str) {
        return String.format(Locale.US, "http://%s:%d/%s", "127.0.0.1", Integer.valueOf(this.f6863e), ProxyCacheUtils.m11152c(str));
    }

    /* renamed from: d */
    private File m11097d(String str) {
        Config cVar = this.f6865g;
        return new File(cVar.f6846a, cVar.f6847b.mo23192a(str));
    }

    /* renamed from: e */
    private HttpProxyCacheServerClients m11099e(String str) {
        HttpProxyCacheServerClients gVar;
        synchronized (this.f6859a) {
            gVar = this.f6861c.get(str);
            if (gVar == null) {
                gVar = new HttpProxyCacheServerClients(str, this.f6865g);
                this.f6861c.put(str, gVar);
            }
        }
        return gVar;
    }

    /* renamed from: b */
    public boolean mo23166b(String str) {
        C3832l.m11128a(str, "Url can't be null!");
        return m11097d(str).exists();
    }

    private HttpProxyCacheServer(Config cVar) {
        this.f6859a = new Object();
        this.f6860b = Executors.newFixedThreadPool(8);
        this.f6861c = new ConcurrentHashMap();
        C3832l.m11127a(cVar);
        this.f6865g = cVar;
        try {
            this.f6862d = new ServerSocket(0, 8, InetAddress.getByName("127.0.0.1"));
            this.f6863e = this.f6862d.getLocalPort();
            IgnoreHostProxySelector.m11119a("127.0.0.1", this.f6863e);
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.f6864f = new Thread(new C3828c(countDownLatch));
            this.f6864f.start();
            countDownLatch.await();
            this.f6866h = new Pinger("127.0.0.1", this.f6863e);
            Logger bVar = f6858i;
            bVar.mo28151c("Proxy cache server started. Is it alive? " + m11093b());
        } catch (IOException | InterruptedException e) {
            this.f6860b.shutdown();
            throw new IllegalStateException("Error starting local proxy server", e);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m11095c() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                Socket accept = this.f6862d.accept();
                Logger bVar = f6858i;
                bVar.mo28150b("Accept new socket " + accept);
                this.f6860b.submit(new C3827b(accept));
            } catch (IOException e) {
                m11090a(new ProxyCacheException("Error during waiting connection", e));
                return;
            }
        }
    }

    /* renamed from: b */
    private boolean m11093b() {
        return this.f6866h.mo23182a(3, 70);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.b.a.f.a(java.lang.String, boolean):java.lang.String
     arg types: [java.lang.String, int]
     candidates:
      e.b.a.f.a(e.b.a.f, java.net.Socket):void
      e.b.a.f.a(java.lang.String, boolean):java.lang.String */
    /* renamed from: a */
    public String mo23164a(String str) {
        return mo23165a(str, true);
    }

    /* renamed from: b */
    private void m11092b(Socket socket) {
        try {
            if (!socket.isInputShutdown()) {
                socket.shutdownInput();
            }
        } catch (SocketException unused) {
            f6858i.mo28150b("Releasing input stream… Socket is closed by client.");
        } catch (IOException e) {
            m11090a(new ProxyCacheException("Error closing socket input stream", e));
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0060 */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m11098d(java.net.Socket r6) {
        /*
            r5 = this;
            java.lang.String r0 = "Opened connections: "
            java.io.InputStream r1 = r6.getInputStream()     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            e.b.a.d r1 = p119e.p124b.p125a.GetRequest.m11076a(r1)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            k.a.b r2 = p119e.p124b.p125a.HttpProxyCacheServer.f6858i     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            r3.<init>()     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            java.lang.String r4 = "Request to cache proxy:"
            r3.append(r4)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            r3.append(r1)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            java.lang.String r3 = r3.toString()     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            r2.mo28150b(r3)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            java.lang.String r2 = r1.f6852a     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            java.lang.String r2 = p119e.p124b.p125a.ProxyCacheUtils.m11151b(r2)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            e.b.a.k r3 = r5.f6866h     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            boolean r3 = r3.mo23183a(r2)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            if (r3 == 0) goto L_0x0034
            e.b.a.k r1 = r5.f6866h     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            r1.mo23181a(r6)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            goto L_0x003b
        L_0x0034:
            e.b.a.g r2 = r5.m11099e(r2)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
            r2.mo23170a(r1, r6)     // Catch:{ SocketException -> 0x0060, n -> 0x004a, IOException -> 0x0048 }
        L_0x003b:
            r5.m11100e(r6)
            k.a.b r6 = p119e.p124b.p125a.HttpProxyCacheServer.f6858i
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            goto L_0x0071
        L_0x0046:
            r1 = move-exception
            goto L_0x0083
        L_0x0048:
            r1 = move-exception
            goto L_0x004b
        L_0x004a:
            r1 = move-exception
        L_0x004b:
            e.b.a.n r2 = new e.b.a.n     // Catch:{ all -> 0x0046 }
            java.lang.String r3 = "Error processing request"
            r2.<init>(r3, r1)     // Catch:{ all -> 0x0046 }
            r5.m11090a(r2)     // Catch:{ all -> 0x0046 }
            r5.m11100e(r6)
            k.a.b r6 = p119e.p124b.p125a.HttpProxyCacheServer.f6858i
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            goto L_0x0071
        L_0x0060:
            k.a.b r1 = p119e.p124b.p125a.HttpProxyCacheServer.f6858i     // Catch:{ all -> 0x0046 }
            java.lang.String r2 = "Closing socket… Socket is closed by client."
            r1.mo28150b(r2)     // Catch:{ all -> 0x0046 }
            r5.m11100e(r6)
            k.a.b r6 = p119e.p124b.p125a.HttpProxyCacheServer.f6858i
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
        L_0x0071:
            r1.append(r0)
            int r0 = r5.m11086a()
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            r6.mo28150b(r0)
            return
        L_0x0083:
            r5.m11100e(r6)
            k.a.b r6 = p119e.p124b.p125a.HttpProxyCacheServer.f6858i
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r0)
            int r0 = r5.m11086a()
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r6.mo28150b(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p124b.p125a.HttpProxyCacheServer.m11098d(java.net.Socket):void");
    }

    /* renamed from: a */
    public String mo23165a(String str, boolean z) {
        if (!z || !mo23166b(str)) {
            return m11093b() ? m11094c(str) : str;
        }
        File d = m11097d(str);
        m11089a(d);
        return Uri.fromFile(d).toString();
    }

    /* renamed from: c */
    private void m11096c(Socket socket) {
        try {
            if (!socket.isOutputShutdown()) {
                socket.shutdownOutput();
            }
        } catch (IOException e) {
            f6858i.mo28147a("Failed to close socket on proxy side: {}. It seems client have already closed connection.", e.getMessage());
        }
    }

    /* renamed from: e */
    private void m11100e(Socket socket) {
        m11092b(socket);
        m11096c(socket);
        m11091a(socket);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: k.a.b.a(java.lang.String, java.lang.Throwable):void
     arg types: [java.lang.String, java.io.IOException]
     candidates:
      k.a.b.a(java.lang.String, java.lang.Object):void
      k.a.b.a(java.lang.String, java.lang.Throwable):void */
    /* renamed from: a */
    private void m11089a(File file) {
        try {
            this.f6865g.f6848c.mo23191a(file);
        } catch (IOException e) {
            Logger bVar = f6858i;
            bVar.mo28149a("Error touching file " + file, (Throwable) e);
        }
    }

    /* renamed from: a */
    private int m11086a() {
        int i;
        synchronized (this.f6859a) {
            i = 0;
            for (HttpProxyCacheServerClients gVar : this.f6861c.values()) {
                i += gVar.mo23169a();
            }
        }
        return i;
    }

    /* renamed from: a */
    private void m11091a(Socket socket) {
        try {
            if (!socket.isClosed()) {
                socket.close();
            }
        } catch (IOException e) {
            m11090a(new ProxyCacheException("Error closing socket", e));
        }
    }

    /* renamed from: a */
    private void m11090a(Throwable th) {
        f6858i.mo28149a("HttpProxyCacheServer error", th);
    }
}
