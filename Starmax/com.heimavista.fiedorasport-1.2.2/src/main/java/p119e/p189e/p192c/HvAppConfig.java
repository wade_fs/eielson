package p119e.p189e.p192c;

import com.blankj.utilcode.util.ResourceUtils;
import com.heimavista.utils.ParamJsonData;

/* renamed from: e.e.c.a */
public class HvAppConfig {

    /* renamed from: a */
    private ParamJsonData f7983a;

    /* renamed from: e.e.c.a$b */
    /* compiled from: HvAppConfig */
    private static class C4204b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final HvAppConfig f7984a = new HvAppConfig();
    }

    /* renamed from: a */
    public static HvAppConfig m13024a() {
        return C4204b.f7984a;
    }

    private HvAppConfig() {
        this.f7983a = new ParamJsonData(ResourceUtils.m1240b("config/config.json"));
    }

    /* renamed from: a */
    public String mo24165a(String str) {
        return this.f7983a.mo25325a(str, "");
    }

    /* renamed from: a */
    public String mo24166a(String str, String str2) {
        return this.f7983a.mo25326a(new String[]{str, str2}, "");
    }

    /* renamed from: a */
    public Integer mo24164a(String str, Integer num) {
        return this.f7983a.mo25323a(str, num);
    }
}
