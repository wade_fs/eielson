package p119e.p189e.p218g;

import android.os.Bundle;
import android.text.TextUtils;
import com.blankj.utilcode.util.SPUtils;
import com.heimavista.api.HvApiEncryptUtils;
import com.heimavista.utils.ParamJsonData;
import java.util.ArrayList;
import java.util.List;
import p119e.p189e.p191b.HvApp;

/* renamed from: e.e.g.a */
public class MemberControl {

    /* renamed from: e */
    public static final String f11153e;

    /* renamed from: f */
    public static final String f11154f;

    /* renamed from: g */
    public static final String f11155g;

    /* renamed from: h */
    public static final String f11156h;

    /* renamed from: a */
    private MemberDelegate f11157a;

    /* renamed from: b */
    private String f11158b;

    /* renamed from: c */
    private ParamJsonData f11159c;

    /* renamed from: d */
    private ParamJsonData f11160d;

    /* renamed from: e.e.g.a$b */
    /* compiled from: MemberControl */
    private static class C4926b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static MemberControl f11161a = new MemberControl();
    }

    static {
        Class<MemberControl> cls = MemberControl.class;
        f11153e = cls.getName() + ".login.SUCCESS";
        f11154f = cls.getName() + ".login.FAILED";
        f11155g = cls.getName() + ".info.SUCCESS";
        f11156h = cls.getName() + ".logout.SUCCESS";
    }

    /* renamed from: s */
    public static MemberControl m17125s() {
        return C4926b.f11161a;
    }

    /* renamed from: t */
    private MemberDelegate m17126t() {
        if (this.f11157a == null) {
            this.f11157a = new MemberDelegateImp();
        }
        return this.f11157a;
    }

    /* renamed from: u */
    private SPUtils m17127u() {
        return SPUtils.m1243c("member_data");
    }

    /* renamed from: a */
    public void mo27173a(String str) {
        m17127u().mo9873a();
        Bundle bundle = null;
        this.f11158b = null;
        this.f11159c = null;
        if (!TextUtils.isEmpty(str)) {
            bundle = new Bundle();
            bundle.putString("errMsg", str);
        }
        HvApp.m13010c().mo24158a(f11156h, bundle);
    }

    /* renamed from: b */
    public void mo27175b(String str) {
        this.f11160d = new ParamJsonData(str, true);
        m17127u().mo9883b("ExtraInfo", HvApiEncryptUtils.m10002b(str));
        HvApp.m13010c().mo24157a(f11155g);
    }

    /* renamed from: c */
    public void mo27177c(String str) {
        this.f11158b = str;
        m17127u().mo9883b("loginInfo", HvApiEncryptUtils.m10002b(str));
    }

    /* renamed from: d */
    public void mo27179d(String str) {
        this.f11159c = new ParamJsonData(str, true);
        m17127u().mo9883b("MemInfo", HvApiEncryptUtils.m10002b(str));
        HvApp.m13010c().mo24157a(f11155g);
    }

    /* renamed from: e */
    public ParamJsonData mo27180e() {
        if (this.f11159c == null) {
            String a = m17127u().mo9872a("MemInfo", (String) null);
            if (a != null) {
                a = HvApiEncryptUtils.m9999a(a);
            }
            if (TextUtils.isEmpty(a)) {
                a = "{}";
            }
            this.f11159c = new ParamJsonData(a, true);
        }
        return this.f11159c;
    }

    /* renamed from: f */
    public String[] mo27181f() {
        return m17126t().mo27194a();
    }

    /* renamed from: g */
    public String mo27182g() {
        return mo27180e().mo25325a("memName", "");
    }

    /* renamed from: h */
    public String mo27183h() {
        return mo27180e().mo25325a("memPhoto", "");
    }

    /* renamed from: i */
    public String mo27184i() {
        return mo27180e().mo25325a("memRealName", "");
    }

    /* renamed from: j */
    public String mo27185j() {
        return mo27180e().mo25325a("mobile", "");
    }

    /* renamed from: k */
    public List<String> mo27186k() {
        ArrayList arrayList = new ArrayList();
        for (Object obj : new ParamJsonData(mo27180e().mo25325a("snsTypes", "[]")).mo25327a()) {
            arrayList.add(String.valueOf(obj));
        }
        return arrayList;
    }

    /* renamed from: l */
    public String mo27187l() {
        return mo27180e().mo25325a("userNbr", "");
    }

    /* renamed from: m */
    public boolean mo27188m() {
        return mo27186k().contains("fb");
    }

    /* renamed from: n */
    public boolean mo27189n() {
        return mo27186k().contains("gp");
    }

    /* renamed from: o */
    public boolean mo27190o() {
        return mo27186k().contains("line");
    }

    /* renamed from: p */
    public boolean mo27191p() {
        return mo27186k().contains("wx");
    }

    /* renamed from: q */
    public boolean mo27192q() {
        return !TextUtils.isEmpty(mo27174b());
    }

    /* renamed from: r */
    public void mo27193r() {
        mo27173a(null);
    }

    private MemberControl() {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.lang.String, int]
     candidates:
      com.heimavista.utils.f.a(java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.f.a(java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String[], java.lang.String):java.lang.String
      com.heimavista.utils.f.a(java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: c */
    public int mo27176c() {
        return mo27180e().mo25323a("memBirthYm", (Integer) 0).intValue();
    }

    /* renamed from: b */
    public String mo27174b() {
        if (this.f11158b == null) {
            String a = m17127u().mo9872a("loginInfo", (String) null);
            if (a != null) {
                a = HvApiEncryptUtils.m9999a(a);
            }
            this.f11158b = a;
        }
        return this.f11158b;
    }

    /* renamed from: d */
    public String mo27178d() {
        return mo27180e().mo25325a("memGender", "");
    }

    /* renamed from: a */
    public ParamJsonData mo27172a() {
        if (this.f11160d == null) {
            String a = m17127u().mo9872a("ExtraInfo", (String) null);
            if (a != null) {
                a = HvApiEncryptUtils.m9999a(a);
            }
            if (TextUtils.isEmpty(a)) {
                a = "{}";
            }
            this.f11160d = new ParamJsonData(a, true);
        }
        return this.f11160d;
    }
}
