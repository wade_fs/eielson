package p119e.p189e.p219h;

import java.io.File;
import java.util.Map;
import org.xutils.C5217x;
import org.xutils.common.Callback;
import org.xutils.http.RequestParams;

/* renamed from: e.e.h.g */
public class HttpRequestInterfaceXUtils implements HttpRequestInterface {

    /* renamed from: e.e.h.g$a */
    /* compiled from: HttpRequestInterfaceXUtils */
    private static class C4929a implements Callback.CommonCallback<String> {

        /* renamed from: a */
        HttpCallback f11169a;

        C4929a(HttpCallback aVar) {
            this.f11169a = aVar;
        }

        public void onCancelled(Callback.CancelledException cancelledException) {
            HttpCallback aVar = this.f11169a;
            if (aVar != null) {
                aVar.mo22404b(cancelledException.getMessage());
            }
        }

        public void onError(Throwable th, boolean z) {
            HttpCallback aVar = this.f11169a;
            if (aVar != null) {
                aVar.onError(th.getMessage());
            }
        }

        public void onFinished() {
            HttpCallback aVar = this.f11169a;
            if (aVar != null) {
                aVar.onFinished();
            }
        }

        public void onSuccess(String str) {
            HttpCallback aVar = this.f11169a;
            if (aVar != null) {
                aVar.mo22403a(str);
            }
        }
    }

    /* renamed from: e.e.h.g$b */
    /* compiled from: HttpRequestInterfaceXUtils */
    private static class C4930b implements HttpCancelable {

        /* renamed from: a */
        Callback.Cancelable f11170a;

        C4930b(Callback.Cancelable cancelable) {
            this.f11170a = cancelable;
        }

        public void cancel() {
            Callback.Cancelable cancelable = this.f11170a;
            if (cancelable != null) {
                cancelable.cancel();
            }
        }
    }

    /* renamed from: e.e.h.g$c */
    /* compiled from: HttpRequestInterfaceXUtils */
    private static class C4931c implements HttpResponse {

        /* renamed from: a */
        String f11171a;

        C4931c(String str) {
            this.f11171a = str;
        }

        /* renamed from: a */
        public boolean mo27219a() {
            return this.f11171a == null;
        }

        /* renamed from: b */
        public String mo27220b() {
            return this.f11171a;
        }
    }

    /* renamed from: c */
    private RequestParams m17184c(HttpRequestConfig eVar, HttpParams cVar) {
        RequestParams requestParams = new RequestParams();
        requestParams.setUri(eVar.mo27209a());
        requestParams.setAsJsonContent(eVar.mo27210b());
        if (cVar != null) {
            for (Map.Entry entry : cVar.mo27199a()) {
                String str = (String) entry.getKey();
                Object value = entry.getValue();
                if (value instanceof File) {
                    requestParams.addBodyParameter(str, (File) value);
                } else {
                    requestParams.addBodyParameter(str, String.valueOf(value));
                }
            }
        }
        return requestParams;
    }

    /* renamed from: a */
    public HttpCancelable mo27213a(HttpRequestConfig eVar, HttpParams cVar, HttpCallback aVar) {
        return new C4930b(C5217x.http().post(m17184c(eVar, cVar), new C4929a(aVar)));
    }

    /* renamed from: b */
    public HttpCancelable mo27216b(HttpRequestConfig eVar, HttpParams cVar, HttpCallback aVar) {
        return new C4930b(C5217x.http().get(m17184c(eVar, cVar), new C4929a(aVar)));
    }

    /* renamed from: a */
    public HttpResponse mo27214a(HttpRequestConfig eVar, HttpParams cVar) {
        String str;
        try {
            str = (String) C5217x.http().postSync(m17184c(eVar, cVar), String.class);
        } catch (Throwable th) {
            th.printStackTrace();
            str = null;
        }
        return new C4931c(str);
    }

    /* renamed from: b */
    public HttpResponse mo27217b(HttpRequestConfig eVar, HttpParams cVar) {
        String str;
        try {
            str = (String) C5217x.http().getSync(m17184c(eVar, cVar), String.class);
        } catch (Throwable th) {
            th.printStackTrace();
            str = null;
        }
        return new C4931c(str);
    }

    /* renamed from: b */
    private RequestParams m17183b(HttpRequestConfig eVar, String str) {
        RequestParams requestParams = new RequestParams();
        requestParams.setUri(eVar.mo27209a());
        requestParams.setBodyContent(str);
        return requestParams;
    }

    /* renamed from: a */
    public HttpResponse mo27215a(HttpRequestConfig eVar, String str) {
        String str2;
        try {
            str2 = (String) C5217x.http().postSync(m17183b(eVar, str), String.class);
        } catch (Throwable th) {
            th.printStackTrace();
            str2 = null;
        }
        return new C4931c(str2);
    }
}
