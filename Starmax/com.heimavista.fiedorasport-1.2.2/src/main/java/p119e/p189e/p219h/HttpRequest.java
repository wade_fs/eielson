package p119e.p189e.p219h;

import androidx.annotation.NonNull;
import com.blankj.utilcode.util.LogUtils;
import java.io.File;
import java.util.Map;
import p119e.p189e.p191b.HvApp;

/* renamed from: e.e.h.d */
public class HttpRequest {

    /* renamed from: a */
    private HttpRequestConfig f11163a;

    /* renamed from: b */
    private HttpRequestInterface f11164b;

    public HttpRequest(@NonNull HttpRequestConfig eVar, @NonNull HttpRequestInterface fVar) {
        this.f11163a = eVar;
        this.f11164b = fVar;
    }

    /* renamed from: c */
    private void m17166c(HttpParams cVar) {
        if (HvApp.m13010c().mo24159b()) {
            LogUtils.m1123a(m17164a(this.f11163a, cVar), cVar.mo27202b());
        }
    }

    /* renamed from: a */
    public HttpCancelable mo27204a(HttpParams cVar, HttpCallback aVar) {
        m17166c(cVar);
        return this.f11164b.mo27216b(this.f11163a, cVar, aVar);
    }

    /* renamed from: b */
    public HttpCancelable mo27207b(HttpParams cVar, HttpCallback aVar) {
        m17166c(cVar);
        return this.f11164b.mo27213a(this.f11163a, cVar, aVar);
    }

    /* renamed from: a */
    public HttpResponse mo27205a(HttpParams cVar) {
        m17166c(cVar);
        return this.f11164b.mo27217b(this.f11163a, cVar);
    }

    /* renamed from: b */
    public HttpResponse mo27208b(HttpParams cVar) {
        m17166c(cVar);
        return this.f11164b.mo27214a(this.f11163a, cVar);
    }

    public HttpRequest(HttpRequestConfig eVar) {
        this(eVar, new HttpRequestInterfaceXUtils());
    }

    /* renamed from: b */
    private void m17165b(String str) {
        if (HvApp.m13010c().mo24159b()) {
            LogUtils.m1123a(this.f11163a.mo27209a(), str);
        }
    }

    /* renamed from: a */
    public HttpResponse mo27206a(String str) {
        m17165b(str);
        return this.f11164b.mo27215a(this.f11163a, str);
    }

    /* renamed from: a */
    public static String m17164a(HttpRequestConfig eVar, HttpParams cVar) {
        StringBuilder sb = new StringBuilder();
        String a = eVar.mo27209a();
        sb.append(a);
        if (!a.contains("?")) {
            sb.append("?");
        } else {
            sb.append("&");
        }
        for (Map.Entry entry : cVar.mo27199a()) {
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            if (!(value instanceof File)) {
                sb.append(str);
                sb.append("=");
                sb.append(value);
                sb.append("&");
            }
        }
        String sb2 = sb.toString();
        return sb2.substring(0, sb2.length() - 1);
    }
}
