package p119e.p189e.p219h;

import com.heimavista.utils.MapUtil;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

/* renamed from: e.e.h.c */
public class HttpParams {

    /* renamed from: a */
    private LinkedHashMap<String, Object> f11162a = new LinkedHashMap<>();

    public HttpParams() {
        new LinkedHashMap();
    }

    /* renamed from: a */
    public HttpParams mo27196a(String str, Object obj) {
        this.f11162a.put(str, obj);
        return this;
    }

    /* renamed from: b */
    public Map<String, Object> mo27202b() {
        return this.f11162a;
    }

    /* renamed from: c */
    public JSONObject mo27203c() {
        return MapUtil.m15245a(this.f11162a);
    }

    /* renamed from: a */
    public HttpParams mo27197a(Map<String, Object> map) {
        this.f11162a.putAll(map);
        return this;
    }

    /* renamed from: b */
    public Object mo27201b(String str) {
        return this.f11162a.get(str);
    }

    /* renamed from: a */
    public HttpParams mo27195a(HttpParams cVar) {
        if (cVar != null) {
            this.f11162a.putAll(cVar.mo27202b());
        }
        return this;
    }

    /* renamed from: a */
    public Set<Map.Entry<String, Object>> mo27199a() {
        return this.f11162a.entrySet();
    }

    /* renamed from: a */
    public boolean mo27200a(String str) {
        return this.f11162a.containsKey(str);
    }

    /* renamed from: a */
    public String mo27198a(String str, String str2) {
        return MapUtil.m15243a(this.f11162a, str, str2);
    }
}
