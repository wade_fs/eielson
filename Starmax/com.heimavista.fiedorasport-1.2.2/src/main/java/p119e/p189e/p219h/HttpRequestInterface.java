package p119e.p189e.p219h;

/* renamed from: e.e.h.f */
public interface HttpRequestInterface {
    /* renamed from: a */
    HttpCancelable mo27213a(HttpRequestConfig eVar, HttpParams cVar, HttpCallback aVar);

    /* renamed from: a */
    HttpResponse mo27214a(HttpRequestConfig eVar, HttpParams cVar);

    /* renamed from: a */
    HttpResponse mo27215a(HttpRequestConfig eVar, String str);

    /* renamed from: b */
    HttpCancelable mo27216b(HttpRequestConfig eVar, HttpParams cVar, HttpCallback aVar);

    /* renamed from: b */
    HttpResponse mo27217b(HttpRequestConfig eVar, HttpParams cVar);
}
