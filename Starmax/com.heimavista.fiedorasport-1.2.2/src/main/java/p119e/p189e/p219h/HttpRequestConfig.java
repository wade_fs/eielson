package p119e.p189e.p219h;

/* renamed from: e.e.h.e */
public class HttpRequestConfig {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f11165a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f11166b;

    /* renamed from: e.e.h.e$b */
    /* compiled from: HttpRequestConfig */
    public static class C4928b {

        /* renamed from: a */
        private String f11167a;

        /* renamed from: b */
        private boolean f11168b = false;

        /* renamed from: a */
        public C4928b mo27211a(String str) {
            this.f11167a = str;
            return this;
        }

        /* renamed from: a */
        public HttpRequestConfig mo27212a() {
            HttpRequestConfig eVar = new HttpRequestConfig();
            String unused = eVar.f11165a = this.f11167a;
            boolean unused2 = eVar.f11166b = this.f11168b;
            return eVar;
        }
    }

    /* renamed from: b */
    public boolean mo27210b() {
        return this.f11166b;
    }

    private HttpRequestConfig() {
        this.f11166b = false;
    }

    /* renamed from: a */
    public String mo27209a() {
        return this.f11165a;
    }
}
