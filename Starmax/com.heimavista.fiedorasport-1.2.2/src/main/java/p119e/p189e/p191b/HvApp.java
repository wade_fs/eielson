package p119e.p189e.p191b;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.multidex.MultiDex;
import com.blankj.utilcode.util.EncryptUtils;
import com.blankj.utilcode.util.FileIOUtils;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.utils.ClassUtil;
import java.io.File;
import java.util.UUID;
import org.xutils.C5217x;
import p119e.p189e.p192c.HvAppConfig;

/* renamed from: e.e.b.a */
public class HvApp extends Application {

    /* renamed from: P */
    private static HvApp f7980P;

    /* renamed from: c */
    public static HvApp m13010c() {
        return f7980P;
    }

    /* renamed from: a */
    public void mo24158a(String str, Bundle bundle) {
        Intent intent = new Intent();
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        intent.setAction(str);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        if (ClassUtil.m15236a("androidx.multidex.MultiDex")) {
            MultiDex.install(this);
        }
    }

    /* renamed from: b */
    public boolean mo24159b() {
        return HvAppConfig.m13024a().mo24164a("debug", 1).intValue() == 1;
    }

    public void onCreate() {
        super.onCreate();
        f7980P = this;
        C5217x.Ext.init(super);
        C5217x.Ext.setDebug(mo24159b());
    }

    /* renamed from: a */
    public void mo24157a(String str) {
        mo24158a(str, (Bundle) null);
    }

    /* renamed from: a */
    public void mo24156a(BroadcastReceiver broadcastReceiver, String... strArr) {
        IntentFilter intentFilter = new IntentFilter();
        for (String str : strArr) {
            intentFilter.addAction(str);
        }
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);
    }

    /* renamed from: a */
    public void mo24155a(BroadcastReceiver broadcastReceiver) {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
    }

    /* renamed from: a */
    public String mo24154a() {
        SharedPreferences sharedPreferences = getSharedPreferences("device", 0);
        String string = sharedPreferences.getString("UUID", "");
        if (TextUtils.isEmpty(string)) {
            byte[] bytes = EncryptUtils.m1061a(Build.MANUFACTURER + Build.MODEL + Build.BRAND + Build.DEVICE).toLowerCase().substring(0, 16).getBytes();
            byte[] bytes2 = EncryptUtils.m1061a("heimavista").toLowerCase().substring(0, 16).getBytes();
            File file = new File(new File(Environment.getExternalStorageDirectory().getAbsolutePath(), ".heimavista"), "UUID");
            String e = FileIOUtils.m1088e(file);
            if (!TextUtils.isEmpty(e)) {
                string = new String(EncryptUtils.m1063a(e, bytes, "AES/CBC/PKCS5Padding", bytes2));
            }
            if (TextUtils.isEmpty(string)) {
                string = UUID.randomUUID().toString();
                FileIOUtils.m1084b(file, EncryptUtils.m1073d(string.getBytes(), bytes, "AES/CBC/PKCS5Padding", bytes2));
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("UUID", string);
            edit.apply();
        }
        LogUtils.m1123a("uuid=" + string);
        return string;
    }
}
