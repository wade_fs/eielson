package p119e.p189e.p191b;

import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.RingtoneManager;
import android.os.Build;
import android.text.TextUtils;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.google.android.exoplayer2.util.MimeTypes;

/* renamed from: e.e.b.b */
public class HvNotify {

    /* renamed from: a */
    private int f7981a;

    /* renamed from: e.e.b.b$b */
    /* compiled from: HvNotify */
    private static class C4202b {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static HvNotify f7982a = new HvNotify();
    }

    /* renamed from: a */
    private synchronized int m13017a() {
        int i;
        if (this.f7981a >= 9999) {
            this.f7981a = 0;
        }
        i = this.f7981a + 1;
        this.f7981a = i;
        return i;
    }

    /* renamed from: b */
    public static HvNotify m13018b() {
        return C4202b.f7982a;
    }

    private HvNotify() {
        this.f7981a = 0;
    }

    /* renamed from: a */
    public void mo24162a(Context context, String str, String str2, int i, int i2, String str3, String str4, String str5, PendingIntent pendingIntent) {
        mo24163a(context, str, str2, i, str3, HvApp.m13010c().getApplicationInfo().icon, BitmapFactory.decodeResource(context.getResources(), context.getApplicationInfo().icon), pendingIntent, str4, str5, 0, i2);
    }

    /* renamed from: a */
    public void mo24163a(Context context, String str, String str2, int i, String str3, int i2, Bitmap bitmap, PendingIntent pendingIntent, CharSequence charSequence, String str4, long j, int i3) {
        long j2 = j;
        String string = TextUtils.isEmpty(str3) ? context.getString(HvApp.m13010c().getApplicationInfo().labelRes) : str3;
        int i4 = 0;
        AudioManager audioManager = (AudioManager) context.getSystemService(MimeTypes.BASE_TYPE_AUDIO);
        if (audioManager != null) {
            i4 = audioManager.getRingerMode() == 2 ? 1 : 2;
        }
        NotificationCompat.Builder largeIcon = new NotificationCompat.Builder(context, str).setContentTitle(string).setContentText(charSequence).setTicker(str4).setAutoCancel(true).setSound(RingtoneManager.getDefaultUri(2)).setContentIntent(pendingIntent).setLights(-16711936, 1000, 300).setDefaults(i4).setSmallIcon(i2).setLargeIcon(bitmap);
        if (j2 > 0) {
            largeIcon.setTimeoutAfter(j2);
        }
        mo24161a(context, i3, str, str2, i, largeIcon);
    }

    /* renamed from: a */
    public void mo24161a(Context context, int i, String str, String str2, int i2, @NonNull NotificationCompat.Builder builder) {
        NotificationChannel notificationChannel;
        NotificationManagerCompat from = NotificationManagerCompat.from(context);
        if (Build.VERSION.SDK_INT < 26 || from.getNotificationChannel(str) != null) {
            notificationChannel = null;
        } else {
            notificationChannel = new NotificationChannel(str, str2, i2);
            notificationChannel.setDescription(str2);
            notificationChannel.setShowBadge(true);
            notificationChannel.canShowBadge();
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(-16711936);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500});
        }
        mo24160a(context, i, notificationChannel, builder);
    }

    /* renamed from: a */
    public void mo24160a(Context context, int i, @Nullable NotificationChannel notificationChannel, @NonNull NotificationCompat.Builder builder) {
        NotificationManagerCompat from = NotificationManagerCompat.from(context);
        if (Build.VERSION.SDK_INT >= 26 && notificationChannel != null && from.getNotificationChannel(notificationChannel.getId()) == null) {
            from.createNotificationChannel(notificationChannel);
        }
        if (i <= 0) {
            i = m13017a();
        }
        from.notify(i, builder.build());
    }
}
