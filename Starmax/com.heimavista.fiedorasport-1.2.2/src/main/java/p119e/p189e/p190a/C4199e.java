package p119e.p189e.p190a;

import android.content.Context;
import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;

/* renamed from: e.e.a.e */
/* compiled from: ContextAction */
public interface C4199e {
    /* renamed from: a */
    Resources mo24146a();

    /* renamed from: a */
    <S> S mo24147a(@NonNull Class cls);

    /* renamed from: a */
    String mo24148a(@StringRes int i);

    Context getContext();
}
