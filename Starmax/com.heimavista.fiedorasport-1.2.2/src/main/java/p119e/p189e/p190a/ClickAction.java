package p119e.p189e.p190a;

import android.view.View;
import androidx.annotation.IdRes;

/* renamed from: e.e.a.b */
public final /* synthetic */ class ClickAction {
    /* renamed from: a */
    public static void m12995a(C4198c cVar, View view) {
    }

    /* renamed from: a */
    public static void m12996a(@IdRes C4198c _this, int... iArr) {
        for (int i : iArr) {
            View findViewById = _this.findViewById(i);
            if (findViewById != null) {
                findViewById.setOnClickListener(_this);
            }
        }
    }
}
