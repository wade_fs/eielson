package p119e.p189e.p190a;

import android.view.View;
import androidx.annotation.IdRes;

/* renamed from: e.e.a.c */
/* compiled from: ClickAction */
public interface C4198c extends View.OnClickListener {
    /* renamed from: a */
    void mo24144a(@IdRes int... iArr);

    <V extends View> V findViewById(@IdRes int i);
}
