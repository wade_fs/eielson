package p119e.p189e.p190a;

import com.heimavista.widget.dialog.R$style;

/* renamed from: e.e.a.a */
public interface AnimAction {

    /* renamed from: a */
    public static final int f7971a;

    /* renamed from: b */
    public static final int f7972b;

    /* renamed from: c */
    public static final int f7973c = R$style.IOSAnimStyle;

    /* renamed from: d */
    public static final int f7974d = R$style.TopAnimStyle;

    /* renamed from: e */
    public static final int f7975e = R$style.BottomAnimStyle;

    /* renamed from: f */
    public static final int f7976f = R$style.LeftAnimStyle;

    /* renamed from: g */
    public static final int f7977g = R$style.RightAnimStyle;

    /* renamed from: h */
    public static final int f7978h = R$style.LoadingDialogStyle;

    static {
        int i = R$style.ScaleAnimStyle;
        f7971a = i;
        f7972b = i;
    }
}
