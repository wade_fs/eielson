package p119e.p189e.p190a;

import android.os.Handler;
import android.os.Looper;

/* renamed from: e.e.a.g */
/* compiled from: HandlerAction */
public interface C4200g {

    /* renamed from: a */
    public static final Handler f7979a = new Handler(Looper.getMainLooper());

    /* renamed from: a */
    boolean mo24150a(Runnable runnable, long j);

    /* renamed from: b */
    void mo24151b();

    boolean post(Runnable runnable);

    boolean postDelayed(Runnable runnable, long j);
}
