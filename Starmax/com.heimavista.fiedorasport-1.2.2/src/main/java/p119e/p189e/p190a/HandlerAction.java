package p119e.p189e.p190a;

import android.os.SystemClock;

/* renamed from: e.e.a.f */
public final /* synthetic */ class HandlerAction {
    /* renamed from: a */
    public static boolean m13005a(C4200g _this, Runnable runnable) {
        return _this.postDelayed(runnable, 0);
    }

    /* renamed from: b */
    public static boolean m13007b(C4200g _this, Runnable runnable, long j) {
        if (j < 0) {
            j = 0;
        }
        return _this.mo24150a(runnable, SystemClock.uptimeMillis() + j);
    }

    /* renamed from: a */
    public static boolean m13006a(C4200g _this, Runnable runnable, long j) {
        return C4200g.f7979a.postAtTime(runnable, _this, j);
    }

    /* renamed from: a */
    public static void m13004a(C4200g _this) {
        C4200g.f7979a.removeCallbacksAndMessages(_this);
    }
}
