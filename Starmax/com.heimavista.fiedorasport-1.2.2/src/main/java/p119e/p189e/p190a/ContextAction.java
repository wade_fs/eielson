package p119e.p189e.p190a;

import android.content.res.Resources;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.core.content.ContextCompat;

/* renamed from: e.e.a.d */
public final /* synthetic */ class ContextAction {
    /* renamed from: a */
    public static Resources m12998a(C4199e _this) {
        return _this.getContext().getResources();
    }

    /* renamed from: a */
    public static String m13000a(@StringRes C4199e _this, int i) {
        return _this.getContext().getString(i);
    }

    /* Incorrect method signature, types: java.lang.Class<S> */
    /* renamed from: a */
    public static <S> Object m12999a(@NonNull C4199e _this, Class cls) {
        return ContextCompat.getSystemService(_this.getContext(), cls);
    }
}
