package p119e.p189e.p217f;

import android.os.Bundle;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.heimavista.utils.ClassUtil;
import p119e.p189e.p191b.HvApp;

/* renamed from: e.e.f.a */
public class GAManage {

    /* renamed from: a */
    private FirebaseAnalytics f11151a;

    /* renamed from: e.e.f.a$b */
    /* compiled from: GAManage */
    private static final class C4924b {

        /* renamed from: a */
        static GAManage f11152a = new GAManage();
    }

    /* renamed from: a */
    public static GAManage m17122a() {
        return C4924b.f11152a;
    }

    /* renamed from: b */
    private void m17123b() {
        if (ClassUtil.m15236a("com.google.android.gms.measurement.AppMeasurementService")) {
            this.f11151a = FirebaseAnalytics.getInstance(HvApp.m13010c());
        }
    }

    private GAManage() {
        m17123b();
    }

    /* renamed from: a */
    public void mo27171a(String str, String str2, String str3, Long l) {
        if (this.f11151a != null) {
            Bundle bundle = new Bundle();
            if (str != null) {
                bundle.putString("item_category", str);
            }
            if (str3 != null) {
                bundle.putString("item_name", str3);
            }
            if (l != null) {
                bundle.putLong("value", l.longValue());
            }
            this.f11151a.mo22096a(str2, bundle);
        }
    }
}
