package p119e.p189e.p193d;

import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: e.e.d.a */
public abstract class DbObject extends OrmBase {

    /* renamed from: b */
    private String[] f7985b = null;

    /* renamed from: c */
    private Map<String, OrmColumnType> f7986c = null;

    /* renamed from: d */
    private Map<String, Object> f7987d = null;

    /* renamed from: e.e.d.a$a */
    /* compiled from: DbObject */
    static /* synthetic */ class C4205a {

        /* renamed from: a */
        static final /* synthetic */ int[] f7988a = new int[OrmColumnType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                e.e.d.e[] r0 = p119e.p189e.p193d.OrmColumnType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                p119e.p189e.p193d.DbObject.C4205a.f7988a = r0
                int[] r0 = p119e.p189e.p193d.DbObject.C4205a.f7988a     // Catch:{ NoSuchFieldError -> 0x0014 }
                e.e.d.e r1 = p119e.p189e.p193d.OrmColumnType.SHORT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = p119e.p189e.p193d.DbObject.C4205a.f7988a     // Catch:{ NoSuchFieldError -> 0x001f }
                e.e.d.e r1 = p119e.p189e.p193d.OrmColumnType.LONG     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = p119e.p189e.p193d.DbObject.C4205a.f7988a     // Catch:{ NoSuchFieldError -> 0x002a }
                e.e.d.e r1 = p119e.p189e.p193d.OrmColumnType.INT     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = p119e.p189e.p193d.DbObject.C4205a.f7988a     // Catch:{ NoSuchFieldError -> 0x0035 }
                e.e.d.e r1 = p119e.p189e.p193d.OrmColumnType.DOUBLE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = p119e.p189e.p193d.DbObject.C4205a.f7988a     // Catch:{ NoSuchFieldError -> 0x0040 }
                e.e.d.e r1 = p119e.p189e.p193d.OrmColumnType.FLOAT     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = p119e.p189e.p193d.DbObject.C4205a.f7988a     // Catch:{ NoSuchFieldError -> 0x004b }
                e.e.d.e r1 = p119e.p189e.p193d.OrmColumnType.STRING     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: p119e.p189e.p193d.DbObject.C4205a.<clinit>():void");
        }
    }

    /* renamed from: o */
    private String[] mo24232o() {
        Map<String, OrmColumnType> b = mo24175b();
        if (b == null) {
            return null;
        }
        String[] strArr = new String[b.size()];
        int i = 0;
        for (Map.Entry<String, OrmColumnType> entry : b.entrySet()) {
            strArr[i] = (String) entry.getKey();
            i++;
        }
        return strArr;
    }

    /* renamed from: a */
    public void mo24171a(SQLiteDatabase sQLiteDatabase) {
        Map<String, OrmColumnType> b = mo24175b();
        if (b != null) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("create table ");
            stringBuffer.append(mo24185a());
            stringBuffer.append("(");
            boolean z = true;
            for (Map.Entry<String, OrmColumnType> entry : b.entrySet()) {
                m13029a(stringBuffer, entry, z);
                z = false;
            }
            m13030a(stringBuffer, mo24204h());
            stringBuffer.append(")");
            sQLiteDatabase.execSQL(stringBuffer.toString());
        }
    }

    /* renamed from: b */
    public Map<String, OrmColumnType> mo24175b() {
        if (this.f7986c == null) {
            this.f7986c = new HashMap();
            mo24176b(this.f7986c);
        }
        return this.f7986c;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo24176b(Map<String, OrmColumnType> map);

    /* renamed from: c */
    public int mo24178c(String str, String[] strArr) {
        return mo24183g().mo24221b(this, str, strArr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public String[] mo24179f() {
        if (this.f7985b == null) {
            this.f7985b = mo24232o();
        }
        return this.f7985b;
    }

    /* renamed from: g */
    public void mo24180g(String str) {
        mo24183g().mo24220a(str);
    }

    /* renamed from: l */
    public long mo24181l() {
        mo24183g().mo24223b();
        long l = super.mo24181l();
        mo24183g().mo24225c();
        return l;
    }

    /* renamed from: n */
    public int mo24182n() {
        return mo24183g().mo24221b(this, (String) null, (String[]) null);
    }

    /* renamed from: b */
    public int mo24174b(String str, String[] strArr) {
        return mo24183g().mo24209a(this, str, strArr);
    }

    /* renamed from: b */
    public void mo24177b(JSONObject jSONObject) {
        mo24196a(mo24170a(jSONObject));
    }

    /* renamed from: a */
    private void m13029a(StringBuffer stringBuffer, Map.Entry<String, OrmColumnType> entry, boolean z) {
        String key = entry.getKey();
        OrmColumnType value = entry.getValue();
        if (!z) {
            stringBuffer.append(",");
        }
        stringBuffer.append(key);
        switch (C4205a.f7988a[value.ordinal()]) {
            case 1:
            case 2:
            case 3:
                stringBuffer.append(" integer");
                break;
            case 4:
                stringBuffer.append(" double");
                break;
            case 5:
                stringBuffer.append(" float");
                break;
            case 6:
                stringBuffer.append(" varchar");
                break;
            default:
                stringBuffer.append(" varchar");
                break;
        }
        Map<String, Object> map = this.f7987d;
        if (map != null && map.containsKey(key)) {
            stringBuffer.append(" default ");
            stringBuffer.append(this.f7987d.get(key));
        }
    }

    /* renamed from: a */
    private void m13030a(StringBuffer stringBuffer, String[] strArr) {
        if (strArr != null) {
            int length = strArr.length;
            stringBuffer.append(",primary key(");
            for (int i = 0; i < length; i++) {
                String str = strArr[i];
                if (i > 0) {
                    stringBuffer.append(",");
                }
                stringBuffer.append(str);
            }
            stringBuffer.append(")");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24172a(String str, OrmColumnType eVar) {
        this.f7986c.put(str, eVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo24173a(String str, OrmColumnType eVar, Object obj) {
        mo24172a(str, eVar);
        if (obj != null && !TextUtils.isEmpty(String.valueOf(obj))) {
            if (this.f7987d == null) {
                this.f7987d = new HashMap();
            }
            this.f7987d.put(str, obj);
        }
    }

    /* renamed from: a */
    public List<Map<String, Object>> mo24167a(String str, String[] strArr, String str2, Integer num, Integer num2) {
        return mo24183g().mo24216a(this, null, str, strArr, str2, num, num2);
    }

    /* renamed from: a */
    public List<Map<String, Object>> mo24168a(String[] strArr, String str, String[] strArr2, String str2, Integer num, Integer num2) {
        return mo24183g().mo24216a(this, strArr, str, strArr2, str2, num, num2);
    }

    /* renamed from: a */
    public Map<String, Object> mo24169a(String str, String[] strArr, String str2) {
        return mo24183g().mo24222b(this, null, str, strArr, str2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Map<String, Object> mo24170a(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        HashMap hashMap = new HashMap();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, jSONObject.opt(next));
        }
        return hashMap;
    }
}
