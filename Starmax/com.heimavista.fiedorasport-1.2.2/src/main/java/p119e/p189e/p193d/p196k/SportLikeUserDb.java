package p119e.p189e.p193d.p196k;

import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.k.a */
public class SportLikeUserDb extends DbObjectBase {
    /* renamed from: j */
    public static List<SportLikeUserDb> m13373j(String str) {
        List<Map<String, Object>> a = new SportLikeUserDb().mo24167a("sportId=?", new String[]{str}, "timestamp desc", null, null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            SportLikeUserDb aVar = new SportLikeUserDb();
            aVar.mo24196a(map);
            arrayList.add(aVar);
        }
        return arrayList;
    }

    /* renamed from: k */
    public static boolean m13374k(String str) {
        if (new SportLikeUserDb().mo24174b("userNbr=? and sportId=?", new String[]{MemberControl.m17125s().mo27187l(), str}) > 0) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "sport_likeUser_det";
    }

    /* renamed from: a */
    public void mo24341a(long j) {
        mo24194a("timestamp", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("sportId", OrmColumnType.STRING);
        mo24172a("timestamp", OrmColumnType.LONG);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "sportId"};
    }

    /* renamed from: i */
    public void mo24343i(String str) {
        mo24195a("userNbr", str);
    }

    /* renamed from: o */
    public long mo24232o() {
        return mo24201e("timestamp").longValue();
    }

    /* renamed from: p */
    public String mo24344p() {
        return mo24203f("userNbr");
    }

    /* renamed from: a */
    public static void m13372a(String str, String str2, long j) {
        SportLikeUserDb aVar = new SportLikeUserDb();
        aVar.mo24342h(str);
        aVar.mo24343i(str2);
        aVar.mo24341a(j);
        aVar.mo24181l();
    }

    /* renamed from: h */
    public void mo24342h(String str) {
        mo24195a("sportId", str);
    }
}
