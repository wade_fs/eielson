package p119e.p189e.p193d.p195j;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.heimavista.api.buddy.HvApiGetUserInfo;
import com.heimavista.fiedorasport.lib.R$mipmap;
import com.heimavista.fiedorasport.p199j.AvatarUtils;
import java.util.Map;
import org.json.JSONObject;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p193d.p194i.BandBaseDb;

/* renamed from: e.e.d.j.f */
public class UserInfoDb extends BandBaseDb {

    /* renamed from: e.e.d.j.f$a */
    /* compiled from: UserInfoDb */
    static class C4210a extends ThreadUtils.C0877e<UserInfoDb> {

        /* renamed from: U */
        final /* synthetic */ String f8002U;

        /* renamed from: V */
        final /* synthetic */ Context f8003V;

        /* renamed from: W */
        final /* synthetic */ TextView f8004W;

        /* renamed from: X */
        final /* synthetic */ ImageView f8005X;

        /* renamed from: Y */
        final /* synthetic */ boolean f8006Y;

        C4210a(String str, Context context, TextView textView, ImageView imageView, boolean z) {
            this.f8002U = str;
            this.f8003V = context;
            this.f8004W = textView;
            this.f8005X = imageView;
            this.f8006Y = z;
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(UserInfoDb fVar) {
            if (fVar != null) {
                UserInfoDb.m13340a(this.f8003V, this.f8002U, this.f8004W, this.f8005X, this.f8006Y);
            }
        }

        /* renamed from: b */
        public UserInfoDb m13364b() {
            return new HvApiGetUserInfo().requestGetUserSync(this.f8002U);
        }
    }

    /* renamed from: e.e.d.j.f$b */
    /* compiled from: UserInfoDb */
    static class C4211b extends ThreadUtils.C0877e<UserInfoDb> {

        /* renamed from: U */
        final /* synthetic */ String f8007U;

        /* renamed from: V */
        final /* synthetic */ Context f8008V;

        /* renamed from: W */
        final /* synthetic */ AvatarUtils.C4219e f8009W;

        C4211b(String str, Context context, AvatarUtils.C4219e eVar) {
            this.f8007U = str;
            this.f8008V = context;
            this.f8009W = eVar;
        }

        /* renamed from: a */
        public void mo9796a(Throwable th) {
        }

        /* renamed from: c */
        public void mo9799c() {
        }

        /* renamed from: a */
        public void mo9795a(UserInfoDb fVar) {
            if (fVar != null) {
                UserInfoDb.m13341a(this.f8008V, this.f8007U, this.f8009W);
            }
        }

        /* renamed from: b */
        public UserInfoDb m13370b() {
            return new HvApiGetUserInfo().requestGetUserSync(this.f8007U);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.j.f.a(android.content.Context, java.lang.String, android.widget.TextView, android.widget.ImageView, boolean):void
     arg types: [android.content.Context, java.lang.String, android.widget.TextView, android.widget.ImageView, int]
     candidates:
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String, java.lang.Integer, java.lang.Integer):java.util.List<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.j.f.a(android.content.Context, java.lang.String, android.widget.TextView, android.widget.ImageView, boolean):void */
    /* renamed from: a */
    public static void m13339a(Context context, String str, TextView textView, ImageView imageView) {
        m13340a(context, str, textView, imageView, false);
    }

    /* renamed from: c */
    public static UserInfoDb m13343c(JSONObject jSONObject) {
        if (jSONObject == null || !jSONObject.has("userNbr")) {
            return null;
        }
        UserInfoDb fVar = new UserInfoDb();
        String optString = jSONObject.optString("userNbr");
        super.mo24238j(optString);
        fVar.mo24335o(jSONObject.optString("userName"));
        fVar.mo24333m(jSONObject.optString("gender"));
        fVar.mo24332l(jSONObject.optString("avatar"));
        super.mo24235a(jSONObject.optLong("timestamp"));
        BuddyListDb l = BuddyListDb.m13250l(optString);
        if (l != null) {
            fVar.mo24334n(l.mo24306s());
        }
        return fVar;
    }

    /* renamed from: p */
    public static UserInfoDb m13344p(String str) {
        UserInfoDb fVar = new UserInfoDb();
        Map<String, Object> a = fVar.mo24169a("userNbr=?", new String[]{str}, (String) null);
        if (a != null) {
            fVar.mo24196a(a);
        }
        return fVar;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "buddy_userInfo_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        super.mo24176b(map);
        mo24172a("userName", OrmColumnType.STRING);
        mo24172a("gender", OrmColumnType.STRING);
        mo24172a("avatar", OrmColumnType.STRING);
        mo24172a("remark", OrmColumnType.STRING);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr"};
    }

    /* renamed from: k */
    public void mo24331k(String str) {
        mo24178c("userNbr=?", new String[]{str});
    }

    /* renamed from: l */
    public void mo24332l(String str) {
        mo24195a("avatar", str);
    }

    /* renamed from: m */
    public void mo24333m(String str) {
        mo24195a("gender", str);
    }

    /* renamed from: n */
    public void mo24334n(String str) {
        mo24195a("remark", str);
    }

    /* renamed from: o */
    public void mo24335o(String str) {
        mo24195a("userName", str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo24241r() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public boolean mo24243t() {
        return false;
    }

    /* renamed from: v */
    public String mo24336v() {
        return mo24203f("avatar");
    }

    /* renamed from: w */
    public String mo24337w() {
        String f = mo24203f("remark");
        return TextUtils.isEmpty(f) ? mo24338x() : f;
    }

    /* renamed from: x */
    public String mo24338x() {
        return mo24203f("userName");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void
     arg types: [android.widget.ImageView, java.lang.String, int, int]
     candidates:
      com.heimavista.fiedorasport.j.d.a(android.content.Context, int, e.e.d.j.f, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(android.content.Context, java.lang.String, int, com.heimavista.fiedorasport.j.d$e):void
      com.heimavista.fiedorasport.j.d.a(com.heimavista.fiedorasport.j.d$e, android.graphics.Bitmap, java.lang.String, java.lang.String):void
      com.heimavista.fiedorasport.j.d.a(android.widget.ImageView, java.lang.String, int, boolean):void */
    /* renamed from: a */
    public static void m13340a(Context context, String str, TextView textView, ImageView imageView, boolean z) {
        UserInfoDb p = m13344p(str);
        if (!TextUtils.isEmpty(super.mo24240q())) {
            if (textView != null) {
                BuddyListDb l = BuddyListDb.m13250l(str);
                if (l == null || TextUtils.isEmpty(l.mo24306s())) {
                    textView.setText(p.mo24338x());
                } else {
                    textView.setText(l.mo24306s());
                }
            }
            if (imageView != null) {
                AvatarUtils.m13393a(imageView, p.mo24336v(), R$mipmap.login_user, true);
                if (z) {
                    ColorMatrix colorMatrix = new ColorMatrix();
                    colorMatrix.setSaturation(0.0f);
                    imageView.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
                    return;
                }
                imageView.setColorFilter((ColorFilter) null);
            }
        } else if (NetworkUtils.m858c()) {
            ThreadUtils.m959b(new C4210a(str, context, textView, imageView, z));
        }
    }

    /* renamed from: b */
    public static void m13342b(String str, String str2) {
        UserInfoDb fVar = new UserInfoDb();
        fVar.mo24334n(str2);
        fVar.mo24188a("userNbr=?", new String[]{str});
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x004b, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m13341a(android.content.Context r3, java.lang.String r4, @androidx.annotation.NonNull com.heimavista.fiedorasport.p199j.AvatarUtils.C4219e r5) {
        /*
            java.lang.Class<e.e.d.j.f> r0 = p119e.p189e.p193d.p195j.UserInfoDb.class
            monitor-enter(r0)
            e.e.g.a r1 = p119e.p189e.p218g.MemberControl.m17125s()     // Catch:{ all -> 0x004c }
            java.lang.String r1 = r1.mo27187l()     // Catch:{ all -> 0x004c }
            boolean r1 = r4.equals(r1)     // Catch:{ all -> 0x004c }
            if (r1 == 0) goto L_0x002e
            e.e.d.j.f r1 = new e.e.d.j.f     // Catch:{ all -> 0x004c }
            r1.<init>()     // Catch:{ all -> 0x004c }
            r1.mo24238j(r4)     // Catch:{ all -> 0x004c }
            int r2 = com.heimavista.fiedorasport.lib.R$string.f6599me     // Catch:{ all -> 0x004c }
            java.lang.String r2 = r3.getString(r2)     // Catch:{ all -> 0x004c }
            r1.mo24335o(r2)     // Catch:{ all -> 0x004c }
            e.e.g.a r2 = p119e.p189e.p218g.MemberControl.m17125s()     // Catch:{ all -> 0x004c }
            java.lang.String r2 = r2.mo27183h()     // Catch:{ all -> 0x004c }
            r1.mo24332l(r2)     // Catch:{ all -> 0x004c }
            goto L_0x0032
        L_0x002e:
            e.e.d.j.f r1 = m13344p(r4)     // Catch:{ all -> 0x004c }
        L_0x0032:
            if (r1 != 0) goto L_0x0045
            boolean r1 = com.blankj.utilcode.util.NetworkUtils.m858c()     // Catch:{ all -> 0x004c }
            if (r1 != 0) goto L_0x003c
            monitor-exit(r0)
            return
        L_0x003c:
            e.e.d.j.f$b r1 = new e.e.d.j.f$b     // Catch:{ all -> 0x004c }
            r1.<init>(r4, r3, r5)     // Catch:{ all -> 0x004c }
            com.blankj.utilcode.util.ThreadUtils.m959b(r1)     // Catch:{ all -> 0x004c }
            goto L_0x004a
        L_0x0045:
            r4 = 40
            com.heimavista.fiedorasport.p199j.AvatarUtils.m13390a(r3, r4, r1, r5)     // Catch:{ all -> 0x004c }
        L_0x004a:
            monitor-exit(r0)
            return
        L_0x004c:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p189e.p193d.p195j.UserInfoDb.m13341a(android.content.Context, java.lang.String, com.heimavista.fiedorasport.j.d$e):void");
    }
}
