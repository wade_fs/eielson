package p119e.p189e.p193d.p194i;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.entity.band.SportTraceInfo;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: e.e.d.i.g */
public class SportTraceDataDb extends BandBaseDb {
    /* renamed from: a */
    public static void m13208a(SportTraceInfo fVar) {
        SportTraceDataDb gVar = new SportTraceDataDb();
        gVar.mo24195a("userNbr", fVar.f6289a);
        gVar.mo24195a("sportId", fVar.f6290b);
        gVar.mo24193a("type", Integer.valueOf(fVar.f6291c));
        gVar.mo24194a("timestamp", Long.valueOf(fVar.f6292d));
        gVar.mo24191a("la", Double.valueOf(fVar.f6293e));
        gVar.mo24191a("lo", Double.valueOf(fVar.f6294f));
        gVar.mo24193a("duration", Integer.valueOf(fVar.f6295g));
        gVar.mo24193a("distance", Integer.valueOf(fVar.f6296h));
        gVar.mo24190a("sync", Boolean.valueOf(fVar.f6297i));
        gVar.mo24181l();
    }

    /* renamed from: c */
    private SportTraceInfo m13209c(Map<String, Object> map) {
        mo24196a(map);
        SportTraceInfo fVar = new SportTraceInfo();
        fVar.f6289a = mo24203f("userNbr");
        fVar.f6290b = mo24203f("sportId");
        fVar.f6291c = mo24200d("type").intValue();
        fVar.f6292d = mo24201e("timestamp").longValue();
        fVar.f6293e = mo24197b("la").doubleValue();
        fVar.f6294f = mo24197b("lo").doubleValue();
        fVar.f6295g = mo24200d("duration").intValue();
        fVar.f6296h = mo24200d("distance").intValue();
        fVar.f6297i = mo24189a("sync").booleanValue();
        return fVar;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "band_sportTrace_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.g.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        super.mo24176b(map);
        mo24172a("sportId", OrmColumnType.STRING);
        mo24172a("type", OrmColumnType.INT);
        mo24172a("la", OrmColumnType.DOUBLE);
        mo24172a("lo", OrmColumnType.DOUBLE);
        mo24173a("duration", OrmColumnType.INT, (Object) 0);
        mo24173a("distance", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "sportId", "timestamp"};
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo24241r() {
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public boolean mo24243t() {
        return false;
    }

    /* renamed from: b */
    public List<SportTraceInfo> mo24272b(String str, String str2) {
        List<Map<String, Object>> a = mo24167a("userNbr=? and sportId=?", new String[]{str, str2}, "timestamp asc", null, null);
        if (a.isEmpty()) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            arrayList.add(m13209c(map));
        }
        return arrayList;
    }
}
