package p119e.p189e.p193d.p195j;

import android.database.sqlite.SQLiteDatabase;
import com.blankj.utilcode.util.LogUtils;
import com.heimavista.entity.band.SleepDayInfo;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: e.e.d.j.d */
/* compiled from: SleepDataDb */
public class C4208d extends DbObjectBase {
    /* renamed from: a */
    public static void m13322a(SleepInfo cVar) {
        C4208d dVar = new C4208d();
        dVar.mo24195a("userNbr", cVar.f6262a);
        dVar.mo24194a("timestamp", Long.valueOf(cVar.f6263b));
        dVar.mo24195a("timezone", cVar.f6264c);
        dVar.mo24193a("type", Integer.valueOf(cVar.f6265d));
        dVar.mo24194a(TtmlNode.START, Long.valueOf(cVar.f6266e));
        dVar.mo24194a(TtmlNode.END, Long.valueOf(cVar.f6267f));
        dVar.mo24193a("sleep", Integer.valueOf(cVar.f6268g));
        dVar.mo24190a("sync", Boolean.valueOf(cVar.f6269h));
        dVar.mo24181l();
    }

    /* renamed from: c */
    private SleepInfo m13324c(Map<String, Object> map) {
        mo24196a(map);
        SleepInfo cVar = new SleepInfo();
        cVar.f6262a = mo24203f("userNbr");
        cVar.f6263b = mo24201e("timestamp").longValue();
        cVar.f6264c = mo24203f("timezone");
        cVar.f6265d = mo24200d("type").intValue();
        cVar.f6266e = mo24201e(TtmlNode.START).longValue();
        cVar.f6267f = mo24201e(TtmlNode.END).longValue();
        cVar.f6268g = mo24200d("sleep").intValue();
        cVar.f6269h = mo24189a("sync").booleanValue();
        LogUtils.m1139c(cVar.toString());
        return cVar;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "buddy_sleepData_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.j.d.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, long]
     candidates:
      e.e.d.j.d.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("timestamp", OrmColumnType.LONG);
        mo24172a("timezone", OrmColumnType.STRING);
        mo24173a("type", OrmColumnType.INT, (Object) 1);
        mo24173a(TtmlNode.START, OrmColumnType.LONG, (Object) 0L);
        mo24173a(TtmlNode.END, OrmColumnType.LONG, (Object) 0L);
        mo24173a("sleep", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "timestamp"};
    }

    /* renamed from: b */
    private List<SleepInfo> m13323b(String str, int i) {
        int i2 = -i;
        Calendar d = TimeUtil.m13448d(i2);
        d.add(10, -3);
        Calendar d2 = TimeUtil.m13448d(i2);
        d2.add(10, 9);
        d2.add(13, 1);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : mo24167a("userNbr=? and sleep>0 and timestamp between ? and ?", new String[]{str, String.valueOf(d.getTimeInMillis() / 1000), String.valueOf(d2.getTimeInMillis() / 1000)}, "timestamp asc", null, null)) {
            arrayList.add(m13324c(map));
        }
        return arrayList;
    }

    /* renamed from: a */
    private SleepDayInfo m13321a(List<SleepInfo> list) {
        SleepDayInfo bVar = new SleepDayInfo();
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            SleepInfo cVar = list.get(i);
            if (i == 0) {
                i2++;
                bVar.f6252a = cVar.f6266e;
            } else if (cVar.f6266e - list.get(i - 1).f6267f > 43200) {
                i2++;
            }
            if (i == size - 1) {
                bVar.f6253b = cVar.f6267f;
            }
            int a = i + m13320a(0, i, cVar, list);
            arrayList.add(cVar);
            int i3 = cVar.f6268g;
            bVar.f6257f += i3;
            int i4 = cVar.f6265d;
            if (i4 == 1) {
                bVar.f6258g += i3;
                bVar.f6254c += i3;
            } else if (i4 == 2) {
                bVar.f6258g += i3;
                bVar.f6255d += i3;
            } else if (i4 == 3) {
                bVar.f6256e += i3;
                bVar.f6259h++;
            }
            i = a + 1;
        }
        bVar.f6261j = arrayList;
        bVar.f6260i = i2;
        return bVar;
    }

    /* renamed from: a */
    private int m13320a(int i, int i2, SleepInfo cVar, List<SleepInfo> list) {
        int i3 = i2 + 1;
        if (i3 >= list.size()) {
            return i;
        }
        long j = list.get(i3).f6266e;
        return (j <= cVar.f6266e || j >= cVar.f6267f) ? i : m13320a(i + 1, i3, cVar, list);
    }

    /* renamed from: a */
    public SleepDayInfo mo24329a(String str, int i) {
        SleepDayInfo a = m13321a(m13323b(str, i));
        a.f6260i = 1;
        return a;
    }
}
