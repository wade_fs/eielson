package p119e.p189e.p193d;

/* renamed from: e.e.d.e */
public enum OrmColumnType {
    SHORT,
    INT,
    LONG,
    FLOAT,
    DOUBLE,
    STRING
}
