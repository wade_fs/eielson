package p119e.p189e.p193d;

import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: e.e.d.h */
public class TickDb extends DbObjectBase {
    /* renamed from: r */
    public static List<TickDb> m13105r() {
        List<Map<String, Object>> a = new TickDb().mo24167a(null, null, null, null, null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            TickDb hVar = new TickDb();
            hVar.mo24196a(map);
            arrayList.add(hVar);
        }
        return arrayList;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "lastTime_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("op", OrmColumnType.STRING);
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("tick", OrmColumnType.LONG);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"op", "userNbr"};
    }

    /* renamed from: o */
    public String mo24232o() {
        return mo24203f("op");
    }

    /* renamed from: p */
    public long mo24233p() {
        return mo24201e("tick").longValue();
    }

    /* renamed from: q */
    public String mo24234q() {
        return mo24203f("userNbr");
    }
}
