package p119e.p189e.p193d.p194i;

import android.database.sqlite.SQLiteDatabase;
import android.util.LongSparseArray;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.heimavista.utils.MapUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.i.d */
public class HeartRateDataDb extends DbObjectBase {
    /* renamed from: c */
    public static HeartRateDataDb m13155c(int i) {
        return m13148a(MemberControl.m17125s().mo27187l(), i);
    }

    /* renamed from: r */
    public static List<HeartRateDataDb> m13156r() {
        HeartRateDataDb dVar = new HeartRateDataDb();
        ArrayList arrayList = new ArrayList();
        List<Map<String, Object>> a = dVar.mo24167a("userNbr=? and heartrate>0 and type!=3 and timestamp <?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(System.currentTimeMillis() / 1000)}, "timestamp desc", null, null);
        int size = a.size();
        HeartRateDataDb dVar2 = null;
        long j = 0;
        int i = 0;
        for (int i2 = 0; i2 < size; i2++) {
            i++;
            HeartRateDataDb dVar3 = new HeartRateDataDb();
            dVar3.mo24196a(a.get(i2));
            if (j == 0) {
                j = dVar3.mo24259p();
            }
            if ((dVar3.mo24259p() / 60) - (j / 60) == 0) {
                if (dVar2 == null) {
                    dVar2 = dVar3;
                } else {
                    dVar2.mo24252a(dVar2.mo24232o() + dVar3.mo24232o());
                }
                if (i2 == size - 1) {
                    dVar2.mo24252a(dVar2.mo24232o() / i);
                    arrayList.add(dVar2);
                }
            } else {
                if (dVar2 != null) {
                    dVar2.mo24252a(dVar2.mo24232o() / (i - 1));
                    arrayList.add(dVar2);
                    if (i2 == size - 1) {
                        arrayList.add(dVar3);
                    }
                } else {
                    arrayList.add(dVar3);
                }
                dVar2 = null;
                i = 0;
            }
            j = dVar3.mo24259p();
        }
        return arrayList;
    }

    /* renamed from: s */
    public static HeartRateDataDb m13157s() {
        return m13154b(false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, long]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long */
    /* renamed from: t */
    public static long m13158t() {
        Map<String, Object> a = new HeartRateDataDb().mo24169a("userNbr=?", new String[]{MemberControl.m17125s().mo27187l()}, "timestamp desc");
        if (a != null) {
            return MapUtil.m15242a(a, "timestamp", (Long) 0L).longValue();
        }
        return 0;
    }

    /* renamed from: u */
    public static JSONArray m13159u() {
        List<Map<String, Object>> a = new HeartRateDataDb().mo24167a("userNbr=? and sync=0 and timestamp>0 and type!=3 and timestamp <?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(System.currentTimeMillis() / 1000)}, "timestamp asc", null, null);
        JSONArray jSONArray = null;
        if (a.isEmpty()) {
            return null;
        }
        for (Map map : a) {
            if (jSONArray == null) {
                jSONArray = new JSONArray();
            }
            HeartRateDataDb dVar = new HeartRateDataDb();
            dVar.mo24196a(map);
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(dVar.mo24259p());
            jSONArray2.put(dVar.mo24260q());
            jSONArray2.put(dVar.mo24232o());
            jSONArray.put(jSONArray2);
        }
        return jSONArray;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "band_heartData_det";
    }

    /* renamed from: a */
    public void mo24253a(long j) {
        mo24194a("timestamp", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.d.a(java.lang.String, long, long):com.heimavista.entity.band.d
      e.e.d.i.d.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("timezone", OrmColumnType.STRING);
        mo24172a("timestamp", OrmColumnType.LONG);
        mo24172a("timeFormat", OrmColumnType.STRING);
        mo24172a("heartrate", OrmColumnType.INT);
        mo24173a("type", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "timestamp"};
    }

    /* renamed from: i */
    public void mo24257i(String str) {
        mo24195a("timezone", str);
    }

    /* renamed from: j */
    public void mo24258j(String str) {
        mo24195a("userNbr", str);
    }

    /* renamed from: o */
    public int mo24232o() {
        return mo24200d("heartrate").intValue();
    }

    /* renamed from: p */
    public long mo24259p() {
        return mo24201e("timestamp").longValue();
    }

    /* renamed from: q */
    public String mo24260q() {
        return mo24203f("timezone");
    }

    /* renamed from: a */
    public void mo24252a(int i) {
        mo24193a("heartrate", Integer.valueOf(i));
    }

    /* renamed from: h */
    public void mo24256h(String str) {
        mo24195a("timeFormat", str);
    }

    /* renamed from: a */
    public void mo24254a(boolean z) {
        mo24193a("sync", Integer.valueOf(z ? 1 : 0));
    }

    /* renamed from: a */
    public static void m13152a(String str, String str2, long j, int i, boolean z, int i2) {
        HeartRateDataDb dVar = new HeartRateDataDb();
        dVar.mo24258j(str);
        dVar.mo24257i(str2);
        dVar.mo24256h(TimeUtils.m1009b(1000 * j));
        dVar.mo24253a(j);
        dVar.mo24252a(i);
        dVar.mo24254a(z);
        dVar.mo24255b(i2);
        dVar.mo24181l();
    }

    /* renamed from: b */
    public void mo24255b(int i) {
        mo24193a("type", Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.util.Map, java.lang.String, long]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.util.Map, java.lang.String, int]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: b */
    public static LongSparseArray<Integer> m13153b(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        instance.add(6, 1);
        List<Map<String, Object>> a = new HeartRateDataDb().mo24168a(new String[]{"timestamp", "heartrate"}, "userNbr=? and heartrate>0 and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(j / 1000), String.valueOf(instance.getTimeInMillis() / 1000)}, "timestamp desc", (Integer) null, (Integer) null);
        LongSparseArray<Integer> longSparseArray = new LongSparseArray<>();
        for (Map map : a) {
            longSparseArray.put(MapUtil.m15242a((Map<String, Object>) map, "timestamp", (Long) 0L).longValue(), MapUtil.m15241a((Map<String, Object>) map, "heartrate", (Integer) 0));
        }
        return longSparseArray;
    }

    /* renamed from: a */
    public static void m13151a(String str, long j, int i, int i2) {
        HeartRateDataDb dVar = new HeartRateDataDb();
        dVar.mo24258j(str);
        dVar.mo24257i(TimeUtil.m13442a());
        dVar.mo24256h(TimeUtils.m1009b(1000 * j));
        dVar.mo24253a(j);
        dVar.mo24252a(i);
        dVar.mo24255b(i2);
        dVar.mo24181l();
    }

    /* renamed from: a */
    public SportInfo mo24251a(String str, long j, long j2) {
        List<Map<String, Object>> a = mo24167a("userNbr=? and timestamp between ? and ? ", new String[]{str, String.valueOf(j), String.valueOf(j2)}, "timestamp asc", null, null);
        SportInfo dVar = new SportInfo();
        if (!a.isEmpty()) {
            for (Map<String, Object> map : a) {
                mo24196a(map);
                int intValue = mo24200d("heartrate").intValue();
                int i = dVar.f6280k;
                if (i == 0) {
                    dVar.f6280k = intValue;
                } else {
                    dVar.f6280k = Math.min(intValue, i);
                }
                int i2 = dVar.f6281l;
                if (i2 == 0) {
                    dVar.f6281l = intValue;
                } else {
                    dVar.f6281l = Math.max(intValue, i2);
                }
                int i3 = dVar.f6279j;
                if (i3 == 0) {
                    dVar.f6279j = intValue;
                } else {
                    dVar.f6279j = (intValue + i3) >> 1;
                }
            }
        }
        return dVar;
    }

    /* renamed from: b */
    public static HeartRateDataDb m13154b(boolean z) {
        return m13149a(MemberControl.m17125s().mo27187l(), z);
    }

    /* renamed from: a */
    public static HeartRateDataDb m13148a(String str, int i) {
        int i2 = -i;
        return m13150a(new HeartRateDataDb().mo24167a("userNbr=? and heartrate>0 and timestamp between ? and ? and type!=3 and timestamp <?", new String[]{str, String.valueOf(TimeUtil.m13448d(i2).getTimeInMillis() / 1000), String.valueOf(TimeUtil.m13445a(i2).getTimeInMillis() / 1000), String.valueOf(System.currentTimeMillis() / 1000)}, "timestamp desc", null, null));
    }

    /* renamed from: a */
    public static HeartRateDataDb m13149a(String str, boolean z) {
        HeartRateDataDb dVar = new HeartRateDataDb();
        String str2 = "userNbr=? and heartrate>0 and type<3";
        if (z) {
            str2 = str2 + " and timestamp between " + (TimeUtil.m13448d(0).getTimeInMillis() / 1000) + " and " + (TimeUtil.m13445a(0).getTimeInMillis() / 1000);
        }
        return m13150a(dVar.mo24167a(str2 + " and timestamp <?", new String[]{str, String.valueOf(System.currentTimeMillis() / 1000)}, "timestamp desc", null, null));
    }

    /* renamed from: a */
    private static HeartRateDataDb m13150a(List<Map<String, Object>> list) {
        if (list.isEmpty()) {
            return null;
        }
        int size = list.size();
        HeartRateDataDb dVar = null;
        long j = 0;
        int i = 0;
        int i2 = 0;
        while (true) {
            if (i >= size) {
                break;
            }
            i2++;
            HeartRateDataDb dVar2 = new HeartRateDataDb();
            dVar2.mo24196a(list.get(i));
            if (j == 0) {
                j = dVar2.mo24259p();
            }
            if ((dVar2.mo24259p() / 60) - (j / 60) == 0) {
                if (dVar == null) {
                    dVar = dVar2;
                } else {
                    dVar.mo24252a(dVar.mo24232o() + dVar2.mo24232o());
                }
                if (i == size - 1) {
                    dVar.mo24252a(dVar.mo24232o() / i2);
                    break;
                }
            } else if (dVar != null) {
                dVar.mo24252a(dVar.mo24232o() / (i2 - 1));
                break;
            } else {
                dVar = dVar2;
            }
            j = dVar2.mo24259p();
            i++;
        }
        return dVar;
    }
}
