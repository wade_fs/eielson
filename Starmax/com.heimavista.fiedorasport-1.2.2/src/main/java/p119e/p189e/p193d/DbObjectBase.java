package p119e.p189e.p193d;

/* renamed from: e.e.d.b */
public abstract class DbObjectBase extends DbObject {

    /* renamed from: e.e.d.b$a */
    /* compiled from: DbObjectBase */
    private static class C4206a {

        /* renamed from: a */
        static OrmDB f7989a = new C4207a();

        /* renamed from: e.e.d.b$a$a */
        /* compiled from: DbObjectBase */
        static class C4207a extends OrmDB {
            C4207a() {
            }

            /* access modifiers changed from: protected */
            /* renamed from: a */
            public String mo24184a() {
                return "basic.db";
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public OrmDB mo24183g() {
        return C4206a.f7989a;
    }
}
