package p119e.p189e.p193d.p195j;

import android.database.sqlite.SQLiteDatabase;
import com.blankj.utilcode.util.LogUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.j.b */
public class BuddyShareDb extends DbObjectBase {
    /* renamed from: i */
    public static BuddyShareDb m13288i(String str) {
        BuddyShareDb bVar = new BuddyShareDb();
        Map<String, Object> a = bVar.mo24169a("ownerUserNbr=? and userNbr=?", new String[]{MemberControl.m17125s().mo27187l(), str}, "");
        if (a != null) {
            bVar.mo24196a(a);
        } else {
            bVar.mo24318h(str);
            bVar.mo24317d(1);
            bVar.mo24316c(1);
            bVar.mo24315b(1);
            bVar.mo24314a(1);
        }
        LogUtils.m1139c("mady op=getBuddyShare " + bVar.mo24205i());
        return bVar;
    }

    /* renamed from: t */
    public static void m13289t() {
        new BuddyShareDb().mo24178c("ownerUserNbr=?", new String[]{MemberControl.m17125s().mo27187l()});
    }

    /* renamed from: u */
    public static List<BuddyShareDb> m13290u() {
        List<Map<String, Object>> a = new BuddyShareDb().mo24167a("ownerUserNbr=?", new String[]{MemberControl.m17125s().mo27187l()}, null, null, null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            BuddyShareDb bVar = new BuddyShareDb();
            bVar.mo24196a(map);
            arrayList.add(bVar);
        }
        return arrayList;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "buddy_share_det";
    }

    /* renamed from: a */
    public void mo24314a(int i) {
        mo24193a("isShareHeart", Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.j.b.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("ownerUserNbr", OrmColumnType.STRING);
        mo24173a("isShareStep", OrmColumnType.INT, (Object) 1);
        mo24173a("isShareSleep", OrmColumnType.INT, (Object) 1);
        mo24173a("isShareHeart", OrmColumnType.INT, (Object) 1);
        mo24173a("isShareLoc", OrmColumnType.INT, (Object) 1);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* renamed from: c */
    public void mo24316c(int i) {
        mo24193a("isShareSleep", Integer.valueOf(i));
    }

    /* renamed from: d */
    public void mo24317d(int i) {
        mo24193a("isShareStep", Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "ownerUserNbr"};
    }

    /* renamed from: o */
    public String mo24232o() {
        return mo24203f("userNbr");
    }

    /* renamed from: p */
    public boolean mo24319p() {
        return mo24200d("isShareHeart").intValue() == 1;
    }

    /* renamed from: q */
    public boolean mo24320q() {
        return mo24200d("isShareLoc").intValue() == 1;
    }

    /* renamed from: r */
    public boolean mo24321r() {
        return mo24200d("isShareSleep").intValue() == 1;
    }

    /* renamed from: s */
    public boolean mo24322s() {
        return mo24200d("isShareStep").intValue() == 1;
    }

    /* renamed from: h */
    public void mo24318h(String str) {
        mo24195a("userNbr", str);
        mo24195a("ownerUserNbr", MemberControl.m17125s().mo27187l());
    }

    /* renamed from: b */
    public void mo24315b(int i) {
        mo24193a("isShareLoc", Integer.valueOf(i));
    }
}
