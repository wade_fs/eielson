package p119e.p189e.p193d;

import android.database.sqlite.SQLiteDatabase;
import java.util.Map;

/* renamed from: e.e.d.c */
public interface IOrmTable {
    /* renamed from: a */
    String mo24185a();

    /* renamed from: a */
    void mo24171a(SQLiteDatabase sQLiteDatabase);

    /* renamed from: a */
    void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2);

    /* renamed from: b */
    Map<String, OrmColumnType> mo24175b();

    /* renamed from: c */
    int mo24187c();
}
