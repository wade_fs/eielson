package p119e.p189e.p193d.p194i;

import android.database.sqlite.SQLiteDatabase;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: e.e.d.i.a */
public abstract class BandBaseDb extends DbObjectBase {
    /* renamed from: a */
    public void mo24235a(long j) {
        mo24194a("timestamp", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        if (mo24244u()) {
            mo24172a("userNbr", OrmColumnType.STRING);
        }
        if (mo24243t()) {
            mo24172a("timezone", OrmColumnType.STRING);
        }
        if (mo24241r()) {
            mo24172a("timeFormat", OrmColumnType.STRING);
        }
        if (mo24242s()) {
            mo24172a("timestamp", OrmColumnType.LONG);
        }
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "timestamp"};
    }

    /* renamed from: i */
    public void mo24237i(String str) {
        mo24195a("timezone", str);
    }

    /* renamed from: j */
    public void mo24238j(String str) {
        mo24195a("userNbr", str);
    }

    /* renamed from: o */
    public long mo24232o() {
        return mo24201e("timestamp").longValue();
    }

    /* renamed from: p */
    public String mo24239p() {
        return mo24203f("timezone");
    }

    /* renamed from: q */
    public String mo24240q() {
        return mo24203f("userNbr");
    }

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public boolean mo24241r() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: s */
    public boolean mo24242s() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: t */
    public boolean mo24243t() {
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: u */
    public boolean mo24244u() {
        return true;
    }

    /* renamed from: h */
    public void mo24236h(String str) {
        mo24195a("timeFormat", str);
    }
}
