package p119e.p189e.p193d.p194i;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.p109h.BandDataManager;
import com.heimavista.fiedorasport.p199j.C4222l;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.heimavista.utils.MapUtil;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.i.h */
public class StepDataDb extends BandBaseDb {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.util.Map, java.lang.String, int]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: e */
    public static StepInfo m13222e(int i) {
        StepDataDb hVar = new StepDataDb();
        Calendar instance = Calendar.getInstance();
        int i2 = -i;
        instance.add(2, i2);
        int actualMaximum = instance.getActualMaximum(5);
        String[] strArr = {"sum(cal)", "sum(distance)", "sum(stepTime)", "sum(step)", "count(*)"};
        List<Map<String, Object>> a = hVar.mo24168a(strArr, "userNbr=? and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(TimeUtil.m13449e(i2).getTimeInMillis() / 1000), String.valueOf(TimeUtil.m13446b(i2).getTimeInMillis() / 1000)}, "timestamp desc", null, 1);
        StepInfo stepInfo = new StepInfo();
        if (a.isEmpty()) {
            return new StepInfo();
        }
        Map map = a.get(0);
        stepInfo.f6247U = MapUtil.m15241a((Map<String, Object>) map, strArr[0], (Integer) 0).intValue();
        stepInfo.f6246T = MapUtil.m15241a((Map<String, Object>) map, strArr[1], (Integer) 0).intValue();
        stepInfo.f6248V = MapUtil.m15241a((Map<String, Object>) map, strArr[2], (Integer) 0).intValue();
        stepInfo.f6245S = MapUtil.m15241a((Map<String, Object>) map, strArr[3], (Integer) 0).intValue();
        stepInfo.f6249W = MapUtil.m15241a(map, strArr[4], Integer.valueOf(actualMaximum)).intValue();
        return stepInfo;
    }

    /* renamed from: f */
    public static StepInfo m13223f(int i) {
        StepDataDb hVar = new StepDataDb();
        int i2 = -i;
        Map<String, Object> a = hVar.mo24169a("userNbr=? and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(TimeUtil.m13448d(i2).getTimeInMillis() / 1000), String.valueOf(TimeUtil.m13445a(i2).getTimeInMillis() / 1000)}, "timestamp desc");
        if (a != null) {
            return hVar.m13221c(a);
        }
        return new StepInfo();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.util.Map, java.lang.String, int]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: g */
    public static StepInfo m13224g(int i) {
        int i2 = -i;
        String[] strArr = {"sum(cal)", "sum(distance)", "sum(stepTime)", "sum(step)", "count(*)"};
        List<Map<String, Object>> a = new StepDataDb().mo24168a(strArr, "userNbr=? and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(TimeUtil.m13450f(i2).getTimeInMillis() / 1000), String.valueOf(TimeUtil.m13447c(i2).getTimeInMillis() / 1000)}, "timestamp desc", null, 1);
        StepInfo stepInfo = new StepInfo();
        if (a.isEmpty()) {
            return new StepInfo();
        }
        Map map = a.get(0);
        stepInfo.f6247U = MapUtil.m15241a((Map<String, Object>) map, strArr[0], (Integer) 0).intValue();
        stepInfo.f6246T = MapUtil.m15241a((Map<String, Object>) map, strArr[1], (Integer) 0).intValue();
        stepInfo.f6248V = MapUtil.m15241a((Map<String, Object>) map, strArr[2], (Integer) 0).intValue();
        stepInfo.f6245S = MapUtil.m15241a((Map<String, Object>) map, strArr[3], (Integer) 0).intValue();
        stepInfo.f6249W = MapUtil.m15241a((Map<String, Object>) map, strArr[4], (Integer) 7).intValue();
        return stepInfo;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, long]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long */
    /* renamed from: x */
    public static long m13225x() {
        Map<String, Object> a = new StepDataDb().mo24169a("userNbr=? and step>0", new String[]{MemberControl.m17125s().mo27187l()}, "timestamp desc");
        if (a != null) {
            return MapUtil.m15242a(a, "timestamp", (Long) 0L).longValue();
        }
        return 0;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "band_stepData_det";
    }

    /* renamed from: a */
    public void mo24235a(long j) {
        mo24194a("timestamp", Long.valueOf(j));
        mo24195a("timeFormat", TimeUtils.m998a(j * 1000, "yyyy-MM-dd"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.h.a(java.lang.String, int, long):void
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        super.mo24176b(map);
        mo24173a("cal", OrmColumnType.INT, (Object) 0);
        mo24173a("distance", OrmColumnType.INT, (Object) 0);
        mo24173a("stepTime", OrmColumnType.INT, (Object) 0);
        mo24173a("step", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public void mo24276c(int i) {
        mo24193a("step", Integer.valueOf(i));
    }

    /* renamed from: d */
    public void mo24277d(int i) {
        mo24193a("stepTime", Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "timeFormat"};
    }

    /* renamed from: j */
    public void mo24238j(String str) {
        mo24195a("userNbr", str);
    }

    /* renamed from: v */
    public JSONArray mo24278v() {
        List<Map<String, Object>> a = mo24167a("userNbr=? and sync=0 and timestamp between 0 and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(TimeUtil.m13445a(-1))}, "timestamp asc", null, null);
        JSONArray jSONArray = null;
        if (a.isEmpty()) {
            return null;
        }
        int size = a.size();
        for (int i = 0; i < size; i++) {
            StepInfo c = m13221c(a.get(i));
            if (i < size - 1) {
                if (jSONArray == null) {
                    jSONArray = new JSONArray();
                }
                JSONArray jSONArray2 = new JSONArray();
                jSONArray2.put(c.f6243Q);
                jSONArray2.put(c.f6244R);
                jSONArray2.put(c.f6247U);
                jSONArray2.put(c.f6246T);
                jSONArray2.put(c.f6248V);
                jSONArray2.put(c.f6245S);
                jSONArray.put(jSONArray2);
            } else if (BandDataManager.m10546a(c.f6243Q) < c.f6245S) {
                if (jSONArray == null) {
                    jSONArray = new JSONArray();
                }
                JSONArray jSONArray3 = new JSONArray();
                jSONArray3.put(c.f6243Q);
                jSONArray3.put(c.f6244R);
                jSONArray3.put(c.f6247U);
                jSONArray3.put(c.f6246T);
                jSONArray3.put(c.f6248V);
                jSONArray3.put(c.f6245S);
                jSONArray.put(jSONArray3);
            }
        }
        return jSONArray;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.util.Map, java.lang.String, int]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: w */
    public StepInfo mo24279w() {
        StepInfo stepInfo = new StepInfo();
        String[] strArr = {"sum(step)", "sum(stepTime)", "count(*)"};
        List<Map<String, Object>> a = mo24168a(strArr, "userNbr=?", new String[]{MemberControl.m17125s().mo27187l()}, "timestamp desc", null, 1);
        if (a.isEmpty()) {
            return stepInfo;
        }
        Map map = a.get(0);
        stepInfo.f6245S = MapUtil.m15241a((Map<String, Object>) map, strArr[0], (Integer) 0).intValue();
        stepInfo.f6246T = C4222l.m13463c(stepInfo.f6245S);
        stepInfo.f6247U = C4222l.m13461b(stepInfo.f6245S);
        stepInfo.f6248V = MapUtil.m15241a((Map<String, Object>) map, strArr[1], (Integer) 0).intValue();
        stepInfo.f6249W = MapUtil.m15241a((Map<String, Object>) map, strArr[2], (Integer) 0).intValue();
        return stepInfo;
    }

    /* renamed from: c */
    private StepInfo m13221c(Map<String, Object> map) {
        mo24196a(map);
        StepInfo stepInfo = new StepInfo();
        stepInfo.f6242P = mo24203f("userNbr");
        stepInfo.f6243Q = mo24201e("timestamp").longValue();
        stepInfo.f6244R = mo24203f("timezone");
        stepInfo.f6247U = mo24200d("cal").intValue();
        stepInfo.f6246T = mo24200d("distance").intValue();
        stepInfo.f6248V = mo24200d("stepTime").intValue();
        stepInfo.f6245S = mo24200d("step").intValue();
        stepInfo.f6249W = 1;
        stepInfo.f6250X = mo24189a("sync").booleanValue();
        LogUtils.m1139c(stepInfo.toString());
        return stepInfo;
    }

    /* renamed from: a */
    public void mo24273a(int i) {
        mo24193a("cal", Integer.valueOf(i));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.d.a(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.h.a(long, long):void
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void */
    /* renamed from: a */
    public static void m13219a(String str, int i, long j) {
        StepDataDb hVar = new StepDataDb();
        hVar.mo24277d(i);
        hVar.mo24235a(j);
        hVar.mo24190a("sync", (Boolean) false);
        hVar.mo24188a("userNbr=? and timeFormat=?", new String[]{str, TimeUtils.m998a(j * 1000, "yyyy-MM-dd")});
    }

    /* renamed from: b */
    public void mo24275b(int i) {
        mo24193a("distance", Integer.valueOf(i));
    }

    /* renamed from: a */
    public static void m13218a(StepInfo stepInfo) {
        StepDataDb hVar = new StepDataDb();
        hVar.mo24195a("userNbr", stepInfo.f6242P);
        hVar.mo24235a(stepInfo.f6243Q);
        hVar.mo24195a("timezone", stepInfo.f6244R);
        hVar.mo24273a(stepInfo.f6247U);
        hVar.mo24275b(stepInfo.f6246T);
        hVar.mo24277d(stepInfo.f6248V);
        hVar.mo24276c(stepInfo.f6245S);
        hVar.mo24190a("sync", Boolean.valueOf(stepInfo.f6250X));
        hVar.mo24181l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.d.a(java.lang.String, java.lang.Boolean):void
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.h.a(long, long):void
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void */
    /* renamed from: a */
    public static void m13220a(String str, long j, int i, int i2) {
        StepDataDb hVar = new StepDataDb();
        hVar.mo24195a("userNbr", str);
        hVar.mo24235a(j);
        hVar.mo24195a("timezone", TimeUtil.m13442a());
        hVar.mo24273a(C4222l.m13461b(i2));
        hVar.mo24275b(C4222l.m13463c(i2));
        hVar.mo24277d(i);
        hVar.mo24276c(i2);
        hVar.mo24190a("sync", (Boolean) false);
        hVar.mo24181l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.d.a(java.lang.String, java.lang.Integer):void
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.h.a(long, long):void
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void */
    /* renamed from: a */
    public void mo24274a(long j, long j2) {
        mo24193a("sync", (Integer) 1);
        mo24188a("userNbr=? and sync=0 and timestamp>=? and timestamp<=?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(j), String.valueOf(j2)});
    }
}
