package p119e.p189e.p193d.p195j;

import android.database.sqlite.SQLiteDatabase;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: e.e.d.j.e */
/* compiled from: StepDataDb */
public class C4209e extends DbObjectBase {
    /* renamed from: a */
    public static void m13331a(StepInfo stepInfo) {
        C4209e eVar = new C4209e();
        eVar.mo24195a("userNbr", stepInfo.f6242P);
        eVar.mo24194a("timestamp", Long.valueOf(stepInfo.f6243Q));
        eVar.mo24195a("timeFormat", TimeUtils.m998a(stepInfo.f6243Q * 1000, "yyyy-MM-dd"));
        eVar.mo24195a("timezone", stepInfo.f6244R);
        eVar.mo24193a("cal", Integer.valueOf(stepInfo.f6247U));
        eVar.mo24193a("distance", Integer.valueOf(stepInfo.f6246T));
        eVar.mo24193a("stepTime", Integer.valueOf(stepInfo.f6248V));
        eVar.mo24193a("step", Integer.valueOf(stepInfo.f6245S));
        eVar.mo24190a("sync", Boolean.valueOf(stepInfo.f6250X));
        eVar.mo24181l();
    }

    /* renamed from: c */
    private StepInfo m13332c(Map<String, Object> map) {
        mo24196a(map);
        StepInfo stepInfo = new StepInfo();
        stepInfo.f6242P = mo24203f("userNbr");
        stepInfo.f6243Q = mo24201e("timestamp").longValue();
        stepInfo.f6244R = mo24203f("timezone");
        stepInfo.f6247U = mo24200d("cal").intValue();
        stepInfo.f6246T = mo24200d("distance").intValue();
        stepInfo.f6248V = mo24200d("stepTime").intValue();
        stepInfo.f6245S = mo24200d("step").intValue();
        stepInfo.f6250X = mo24189a("sync").booleanValue();
        LogUtils.m1139c(stepInfo.toString());
        return stepInfo;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "buddy_stepData_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.j.e.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("timestamp", OrmColumnType.LONG);
        mo24172a("timeFormat", OrmColumnType.STRING);
        mo24172a("timezone", OrmColumnType.STRING);
        mo24173a("cal", OrmColumnType.INT, (Object) 0);
        mo24173a("distance", OrmColumnType.INT, (Object) 0);
        mo24173a("stepTime", OrmColumnType.INT, (Object) 0);
        mo24173a("step", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "timeFormat"};
    }

    /* renamed from: a */
    public StepInfo mo24330a(String str, int i) {
        int i2 = -i;
        Map<String, Object> a = mo24169a("userNbr=? and timestamp between ? and ?", new String[]{str, String.valueOf(TimeUtil.m13448d(i2).getTimeInMillis() / 1000), String.valueOf(TimeUtil.m13445a(i2).getTimeInMillis() / 1000)}, "timestamp desc");
        if (a != null) {
            return m13332c(a);
        }
        return new StepInfo();
    }
}
