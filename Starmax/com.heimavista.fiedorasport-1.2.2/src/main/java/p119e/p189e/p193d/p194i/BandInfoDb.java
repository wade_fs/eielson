package p119e.p189e.p193d.p194i;

import android.database.sqlite.SQLiteDatabase;
import java.util.Map;
import org.json.JSONObject;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.i.b */
public class BandInfoDb extends DbObjectBase {
    /* renamed from: c */
    public static void m13129c(JSONObject jSONObject) {
        BandInfoDb bVar = new BandInfoDb();
        bVar.mo24195a("userNbr", MemberControl.m17125s().mo27187l());
        bVar.mo24177b(jSONObject);
        bVar.mo24181l();
    }

    /* renamed from: a */
    public String mo24185a() {
        return "bandInfo_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("bandId", OrmColumnType.STRING);
        mo24172a("deviceName", OrmColumnType.STRING);
        mo24172a("deviceMacAddress", OrmColumnType.STRING);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "bandId"};
    }
}
