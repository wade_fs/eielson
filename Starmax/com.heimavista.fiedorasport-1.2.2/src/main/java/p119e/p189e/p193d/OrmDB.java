package p119e.p189e.p193d;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.blankj.utilcode.util.LogUtils;
import com.facebook.internal.ServerProtocol;
import com.heimavista.utils.ArrayUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import p119e.p189e.p191b.HvApp;

/* renamed from: e.e.d.f */
public abstract class OrmDB {

    /* renamed from: a */
    protected final Lock f7998a = new ReentrantLock();

    /* renamed from: b */
    protected OrmSQLiteOpenHelper f7999b = new OrmSQLiteOpenHelper(HvApp.m13010c(), mo24184a());

    /* renamed from: c */
    protected SQLiteDatabase f8000c = this.f7999b.getReadableDatabase();

    /* renamed from: d */
    protected SQLiteDatabase f8001d = this.f7999b.getWritableDatabase();

    /* renamed from: d */
    private int m13083d(String str) {
        int i = 0;
        Cursor query = this.f8000c.query(false, this.f7999b.mo24229a(), new String[]{ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION}, "name=?", new String[]{str}, null, null, null, null);
        if (query != null) {
            if (query.moveToFirst()) {
                i = query.getInt(0);
            }
            query.close();
        }
        return i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract String mo24184a();

    /* renamed from: a */
    public ArrayList<Map<String, Object>> mo24217a(boolean z, String str, String[] strArr, Map<String, OrmColumnType> map, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
        Object obj;
        ArrayList<Map<String, Object>> arrayList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();
        sb.append("select ");
        if (z) {
            sb.append("distinct ");
        }
        if (strArr == null || strArr.length <= 0) {
            sb.append("*");
        } else {
            sb.append(ArrayUtil.m15235a(strArr, ","));
        }
        sb.append(" from ");
        sb.append(str);
        if (!TextUtils.isEmpty(str2)) {
            sb.append(" where ");
            sb.append(str2);
        }
        if (!TextUtils.isEmpty(str3)) {
            sb.append(" group by ");
            sb.append(str3);
            if (!TextUtils.isEmpty(str4)) {
                sb.append(" having ");
                sb.append(str4);
            }
        }
        if (!TextUtils.isEmpty(str5)) {
            sb.append(" order by ");
            sb.append(str5);
        }
        if (!TextUtils.isEmpty(str6)) {
            sb.append(" limit ");
            sb.append(str6);
        }
        String sb2 = sb.toString();
        Cursor rawQuery = this.f8000c.rawQuery(sb2, strArr2);
        if (HvApp.m13010c().mo24159b()) {
            LogUtils.m1139c(sb2, strArr2);
        }
        if (rawQuery != null) {
            if (rawQuery.getCount() > 0) {
                rawQuery.moveToFirst();
                do {
                    HashMap hashMap = new HashMap();
                    int columnCount = rawQuery.getColumnCount();
                    for (int i = 0; i < columnCount; i++) {
                        String columnName = rawQuery.getColumnName(i);
                        OrmColumnType eVar = map.get(columnName);
                        if (OrmColumnType.DOUBLE.equals(eVar)) {
                            obj = Double.valueOf(rawQuery.getDouble(i));
                        } else if (OrmColumnType.FLOAT.equals(eVar)) {
                            obj = Float.valueOf(rawQuery.getFloat(i));
                        } else if (OrmColumnType.INT.equals(eVar)) {
                            obj = Integer.valueOf(rawQuery.getInt(i));
                        } else if (OrmColumnType.LONG.equals(eVar)) {
                            obj = Long.valueOf(rawQuery.getLong(i));
                        } else if (OrmColumnType.SHORT.equals(eVar)) {
                            obj = Short.valueOf(rawQuery.getShort(i));
                        } else {
                            obj = rawQuery.getString(i);
                        }
                        hashMap.put(columnName, obj);
                    }
                    arrayList.add(hashMap);
                } while (rawQuery.moveToNext());
            }
            rawQuery.close();
        }
        return arrayList;
    }

    /* renamed from: b */
    public void mo24223b() {
        this.f7998a.lock();
    }

    /* renamed from: c */
    public void mo24225c() {
        this.f7998a.unlock();
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        this.f8000c.close();
        this.f8001d.close();
        super.finalize();
    }

    /* renamed from: b */
    private int m13081b(String str, String str2, String[] strArr) {
        Cursor query = this.f8000c.query(str, new String[]{"count(*)"}, str2, strArr, null, null, null);
        if (query == null) {
            return 0;
        }
        query.moveToFirst();
        int i = query.getInt(0);
        query.close();
        return i;
    }

    /* renamed from: c */
    private boolean m13082c(String str, String str2, String[] strArr) {
        return m13081b(str, str2, strArr) > 0;
    }

    /* renamed from: c */
    public boolean mo24226c(IOrmTable cVar, String str, String[] strArr) {
        return mo24209a(cVar, str, strArr) > 0;
    }

    /* renamed from: c */
    public boolean mo24227c(String str) {
        Cursor rawQuery = this.f8000c.rawQuery("select * from sqlite_master where name='" + str + "'", null);
        boolean z = false;
        if (rawQuery != null) {
            if (rawQuery.getCount() != 0) {
                z = true;
            }
            rawQuery.close();
        }
        return z;
    }

    /* renamed from: b */
    public boolean mo24224b(String str) {
        return m13082c("sqlite_master", "type=? and name=?", new String[]{"table", str});
    }

    /* renamed from: b */
    public int mo24221b(IOrmTable cVar, String str, String[] strArr) {
        mo24219a(cVar);
        return mo24211a(cVar.mo24185a(), str, strArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.c, int, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, int, int]
     candidates:
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: b */
    public Map<String, Object> mo24222b(IOrmTable cVar, String[] strArr, String str, String[] strArr2, String str2) {
        ArrayList<Map<String, Object>> a = mo24214a(cVar, false, strArr, str, strArr2, (String) null, (String) null, str2, (Integer) 0, (Integer) 1);
        if (a.size() > 0) {
            return a.get(0);
        }
        return null;
    }

    /* renamed from: a */
    public int mo24212a(String str, Map<String, Object> map, String str2, String[] strArr) {
        return this.f8001d.update(str, m13079a(map), str2, strArr);
    }

    /* renamed from: a */
    public int mo24211a(String str, String str2, String[] strArr) {
        return this.f8001d.delete(str, str2, strArr);
    }

    /* renamed from: a */
    private void m13080a(String str, int i) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ServerProtocol.FALLBACK_DIALOG_PARAM_VERSION, Integer.valueOf(i));
        if (m13082c(this.f7999b.mo24229a(), "name=?", new String[]{str})) {
            this.f8001d.update(this.f7999b.mo24229a(), contentValues, "name=?", new String[]{str});
            return;
        }
        contentValues.put("name", str);
        this.f8001d.insert(this.f7999b.mo24229a(), null, contentValues);
    }

    /* renamed from: a */
    public synchronized void mo24219a(IOrmTable cVar) {
        if (!mo24224b(cVar.mo24185a())) {
            cVar.mo24171a(this.f8001d);
            m13080a(cVar.mo24185a(), cVar.mo24187c());
        } else {
            int d = m13083d(cVar.mo24185a());
            if (d < cVar.mo24187c()) {
                cVar.mo24186a(this.f8001d, d, cVar.mo24187c());
                m13080a(cVar.mo24185a(), cVar.mo24187c());
            }
        }
    }

    /* renamed from: a */
    public int mo24209a(IOrmTable cVar, String str, String[] strArr) {
        mo24219a(cVar);
        return m13081b(cVar.mo24185a(), str, strArr);
    }

    /* renamed from: a */
    public static ContentValues m13079a(Map<String, Object> map) {
        ContentValues contentValues = new ContentValues();
        for (Map.Entry entry : map.entrySet()) {
            String str = (String) entry.getKey();
            Object value = entry.getValue();
            if (!(str == null || value == null)) {
                contentValues.put(str, String.valueOf(value));
            }
        }
        return contentValues;
    }

    /* renamed from: a */
    public long mo24213a(IOrmTable cVar, Map<String, Object> map) {
        mo24219a(cVar);
        return this.f8001d.replace(cVar.mo24185a(), null, m13079a(map));
    }

    /* renamed from: a */
    public int mo24210a(IOrmTable cVar, Map<String, Object> map, String str, String[] strArr) {
        mo24219a(cVar);
        return mo24212a(cVar.mo24185a(), map, str, strArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [int, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: a */
    public ArrayList<Map<String, Object>> mo24215a(IOrmTable cVar, String[] strArr, String str, String[] strArr2, String str2) {
        mo24219a(cVar);
        return mo24217a(false, cVar.mo24185a(), strArr, cVar.mo24175b(), str, strArr2, (String) null, (String) null, str2, (String) null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.c, int, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], java.lang.String, java.lang.Integer, java.lang.Integer]
     candidates:
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: a */
    public ArrayList<Map<String, Object>> mo24216a(IOrmTable cVar, String[] strArr, String str, String[] strArr2, String str2, Integer num, Integer num2) {
        return mo24214a(cVar, false, strArr, str, strArr2, (String) null, (String) null, str2, num, num2);
    }

    /* renamed from: a */
    public ArrayList<Map<String, Object>> mo24214a(IOrmTable cVar, boolean z, String[] strArr, String str, String[] strArr2, String str2, String str3, String str4, Integer num, Integer num2) {
        String str5;
        Integer num3 = num;
        mo24219a(cVar);
        if (num2 == null || num2.intValue() <= 0) {
            str5 = null;
        } else {
            str5 = String.valueOf(num2);
            if (num3 != null) {
                str5 = num3 + "," + str5;
            }
        }
        return mo24217a(z, cVar.mo24185a(), strArr, cVar.mo24175b(), str, strArr2, str2, str3, str4, str5);
    }

    /* renamed from: a */
    public ArrayList<Map<String, Object>> mo24218a(IOrmTable[] cVarArr, boolean z, String[] strArr, String str, String[] strArr2, String str2, String str3, String str4, Integer num, Integer num2) {
        IOrmTable[] cVarArr2 = cVarArr;
        Integer num3 = num;
        StringBuffer stringBuffer = new StringBuffer();
        HashMap hashMap = new HashMap();
        for (int i = 0; i < cVarArr2.length; i++) {
            mo24219a(cVarArr2[i]);
            stringBuffer.append(cVarArr2[i].mo24185a());
            if (i != cVarArr2.length - 1) {
                stringBuffer.append(",");
            }
            hashMap.putAll(cVarArr2[i].mo24175b());
        }
        String str5 = null;
        if (num2 != null && num2.intValue() > 0) {
            str5 = String.valueOf(num2);
            if (num3 != null) {
                str5 = num3 + "," + str5;
            }
        }
        return mo24217a(z, stringBuffer.toString(), strArr, hashMap, str, strArr2, str2, str3, str4, str5);
    }

    /* renamed from: a */
    public void mo24220a(String str) {
        mo24211a(this.f7999b.mo24229a(), "name=?", new String[]{str});
        SQLiteDatabase sQLiteDatabase = this.f8001d;
        sQLiteDatabase.execSQL("drop table if exists " + str);
    }
}
