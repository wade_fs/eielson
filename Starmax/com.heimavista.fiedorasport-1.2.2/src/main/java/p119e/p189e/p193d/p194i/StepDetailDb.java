package p119e.p189e.p193d.p194i;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.StepDetailInfo;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.i.i */
public class StepDetailDb extends BandBaseDb {
    /* renamed from: c */
    private StepDetailInfo m13238c(Map<String, Object> map) {
        mo24196a(map);
        StepDetailInfo gVar = new StepDetailInfo();
        gVar.f6298a = mo24203f("userNbr");
        gVar.f6299b = mo24201e("timestamp").longValue();
        gVar.f6300c = mo24203f("timezone");
        gVar.f6301d = mo24200d("type").intValue();
        gVar.f6302e = mo24201e(TtmlNode.START).longValue();
        gVar.f6303f = mo24201e(TtmlNode.END).longValue();
        gVar.f6304g = mo24200d("cal").intValue();
        gVar.f6305h = mo24200d("distance").intValue();
        gVar.f6306i = mo24200d("stepTime").intValue();
        gVar.f6307j = mo24200d("step").intValue();
        gVar.f6308k = mo24189a("sync").booleanValue();
        LogUtils.m1139c(gVar.toString());
        return gVar;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "band_stepDetail_det";
    }

    /* renamed from: a */
    public void mo24281a(StepDetailInfo gVar) {
        mo24195a("userNbr", gVar.f6298a);
        mo24194a("timestamp", Long.valueOf(gVar.f6299b));
        mo24195a("timeFormat", TimeUtils.m1009b(gVar.f6299b * 1000));
        mo24195a("timezone", gVar.f6300c);
        mo24193a("type", Integer.valueOf(gVar.f6301d));
        mo24194a(TtmlNode.START, Long.valueOf(gVar.f6302e));
        mo24194a(TtmlNode.END, Long.valueOf(gVar.f6303f));
        mo24193a("cal", Integer.valueOf(gVar.f6304g));
        mo24193a("distance", Integer.valueOf(gVar.f6305h));
        mo24193a("stepTime", Integer.valueOf(gVar.f6306i));
        mo24193a("step", Integer.valueOf(gVar.f6307j));
        mo24190a("sync", Boolean.valueOf(gVar.f6308k));
        LogUtils.m1139c(gVar.toString());
        mo24181l();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, long]
     candidates:
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        super.mo24176b(map);
        mo24173a("type", OrmColumnType.INT, (Object) 1);
        mo24173a(TtmlNode.START, OrmColumnType.LONG, (Object) 0L);
        mo24173a(TtmlNode.END, OrmColumnType.LONG, (Object) 0L);
        mo24173a("cal", OrmColumnType.INT, (Object) 0);
        mo24173a("distance", OrmColumnType.INT, (Object) 0);
        mo24173a("stepTime", OrmColumnType.INT, (Object) 0);
        mo24173a("step", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: v */
    public JSONArray mo24282v() {
        List<Map<String, Object>> a = mo24167a("userNbr=? and sync=0 and timestamp>0", new String[]{MemberControl.m17125s().mo27187l()}, "timestamp asc", null, null);
        JSONArray jSONArray = null;
        if (a.isEmpty()) {
            return null;
        }
        for (Map map : a) {
            if (jSONArray == null) {
                jSONArray = new JSONArray();
            }
            StepDetailInfo c = m13238c(map);
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(c.f6299b);
            jSONArray2.put(c.f6300c);
            jSONArray2.put(c.f6301d);
            jSONArray2.put(c.f6302e);
            jSONArray2.put(c.f6303f);
            jSONArray2.put(c.f6307j);
            jSONArray2.put(c.f6305h);
            jSONArray2.put(c.f6304g);
            jSONArray.put(jSONArray2);
        }
        return jSONArray;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.d.a(java.lang.String, java.lang.Integer):void
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.i.a(long, long):void
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void */
    /* renamed from: a */
    public void mo24280a(long j, long j2) {
        mo24193a("sync", (Integer) 1);
        mo24188a("userNbr=? and sync=0 and timestamp>=? and timestamp<=?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(j), String.valueOf(j2)});
    }
}
