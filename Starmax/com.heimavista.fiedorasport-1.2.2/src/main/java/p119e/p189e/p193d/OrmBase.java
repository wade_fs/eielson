package p119e.p189e.p193d;

import com.heimavista.utils.MapUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: e.e.d.d */
public abstract class OrmBase implements IOrmTable {

    /* renamed from: a */
    private Map<String, Object> f7990a = new HashMap();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, int]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: a */
    public Boolean mo24189a(String str) {
        Integer num = 1;
        return Boolean.valueOf(num.equals(MapUtil.m15241a(this.f7990a, str, (Integer) 0)));
    }

    /* renamed from: b */
    public Double mo24197b(String str) {
        return MapUtil.m15239a(this.f7990a, str, Double.valueOf(0.0d));
    }

    /* renamed from: c */
    public Float mo24198c(String str) {
        return MapUtil.m15240a(this.f7990a, str, Float.valueOf(0.0f));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, int]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer */
    /* renamed from: d */
    public Integer mo24200d(String str) {
        return MapUtil.m15241a(this.f7990a, str, (Integer) 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, long]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long */
    /* renamed from: e */
    public Long mo24201e(String str) {
        return MapUtil.m15242a(this.f7990a, str, (Long) 0L);
    }

    /* renamed from: f */
    public String mo24203f(String str) {
        return MapUtil.m15243a(this.f7990a, str, "");
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public abstract String[] mo24179f();

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public abstract OrmDB mo24183g();

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public abstract String[] mo24204h();

    /* renamed from: i */
    public Map<String, Object> mo24205i() {
        return this.f7990a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.d, int, ?[OBJECT, ARRAY], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[boolean, int, float, short, byte, char]]
     candidates:
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: j */
    public void mo24206j() {
        String[] strArr;
        String[] h = mo24204h();
        StringBuffer stringBuffer = new StringBuffer();
        if (h != null) {
            String[] strArr2 = new String[h.length];
            for (int i = 0; i < h.length; i++) {
                stringBuffer.append(h[i]);
                stringBuffer.append("=?");
                if (i != h.length - 1) {
                    stringBuffer.append(" and ");
                }
                strArr2[i] = String.valueOf(this.f7990a.get(h[i]));
            }
            strArr = strArr2;
        } else {
            strArr = null;
        }
        ArrayList<Map<String, Object>> a = mo24183g().mo24214a((IOrmTable) this, false, (String[]) null, stringBuffer.toString(), strArr, (String) null, (String) null, (String) null, (Integer) null, (Integer) 1);
        if (a != null && a.size() > 0) {
            mo24196a(a.get(0));
        }
    }

    /* renamed from: k */
    public boolean mo24207k() {
        return mo24183g().mo24227c(mo24185a());
    }

    /* renamed from: l */
    public long mo24181l() {
        return mo24183g().mo24213a(this, mo24205i());
    }

    /* renamed from: m */
    public int mo24208m() {
        String[] strArr;
        String[] h = mo24204h();
        StringBuffer stringBuffer = new StringBuffer();
        if (h != null) {
            strArr = new String[h.length];
            for (int i = 0; i < h.length; i++) {
                stringBuffer.append(h[i]);
                stringBuffer.append("=?");
                if (i != h.length - 1) {
                    stringBuffer.append(" and ");
                }
                strArr[i] = String.valueOf(this.f7990a.get(h[i]));
            }
        } else {
            strArr = null;
        }
        return mo24183g().mo24210a(this, mo24205i(), stringBuffer.toString(), strArr);
    }

    /* renamed from: d */
    public int mo24199d() {
        String[] strArr;
        String[] h = mo24204h();
        StringBuffer stringBuffer = new StringBuffer();
        if (h != null) {
            strArr = new String[h.length];
            for (int i = 0; i < h.length; i++) {
                stringBuffer.append(h[i]);
                stringBuffer.append("=?");
                if (i != h.length - 1) {
                    stringBuffer.append(" and ");
                }
                strArr[i] = String.valueOf(this.f7990a.get(h[i]));
            }
        } else {
            strArr = null;
        }
        return mo24183g().mo24221b(this, stringBuffer.toString(), strArr);
    }

    /* renamed from: e */
    public boolean mo24202e() {
        String[] strArr;
        String[] h = mo24204h();
        StringBuffer stringBuffer = new StringBuffer();
        if (h != null) {
            strArr = new String[h.length];
            for (int i = 0; i < h.length; i++) {
                stringBuffer.append(h[i]);
                stringBuffer.append("=?");
                if (i != h.length - 1) {
                    stringBuffer.append(" and ");
                }
                strArr[i] = String.valueOf(this.f7990a.get(h[i]));
            }
        } else {
            strArr = null;
        }
        return mo24183g().mo24226c(this, stringBuffer.toString(), strArr);
    }

    /* renamed from: a */
    public void mo24193a(String str, Integer num) {
        this.f7990a.put(str, num);
    }

    /* renamed from: a */
    public void mo24195a(String str, String str2) {
        this.f7990a.put(str, str2);
    }

    /* renamed from: a */
    public void mo24192a(String str, Float f) {
        this.f7990a.put(str, f);
    }

    /* renamed from: a */
    public void mo24191a(String str, Double d) {
        this.f7990a.put(str, d);
    }

    /* renamed from: a */
    public void mo24190a(String str, Boolean bool) {
        this.f7990a.put(str, Integer.valueOf(Boolean.TRUE.equals(bool) ? 1 : 0));
    }

    /* renamed from: a */
    public void mo24194a(String str, Long l) {
        this.f7990a.put(str, l);
    }

    /* renamed from: a */
    public void mo24196a(Map<String, Object> map) {
        String[] f = mo24179f();
        int length = f.length;
        for (int i = 0; i < length; i++) {
            Object obj = map.get(f[i]);
            if (obj != null) {
                this.f7990a.put(f[i], obj);
            }
        }
    }

    /* renamed from: a */
    public int mo24188a(String str, String[] strArr) {
        return mo24183g().mo24210a(this, mo24205i(), str, strArr);
    }
}
