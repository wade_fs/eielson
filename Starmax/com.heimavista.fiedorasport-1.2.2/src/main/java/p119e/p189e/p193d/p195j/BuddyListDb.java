package p119e.p189e.p193d.p195j;

import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.j.a */
public class BuddyListDb extends DbObjectBase {
    /* renamed from: E */
    public static void m13244E() {
        new BuddyListDb().mo24178c("ownerUserNbr=?", new String[]{MemberControl.m17125s().mo27187l()});
    }

    /* renamed from: F */
    public static int m13245F() {
        return new BuddyListDb().mo24174b("ownerUserNbr=? and userStat=9", new String[]{MemberControl.m17125s().mo27187l()});
    }

    /* renamed from: G */
    public static List<BuddyListDb> m13246G() {
        List<Map<String, Object>> a = new BuddyListDb().mo24167a("ownerUserNbr=? and userNbr!=? and userNbr is not null", new String[]{MemberControl.m17125s().mo27187l(), MemberControl.m17125s().mo27187l()}, "userTimestamp desc", null, null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            BuddyListDb aVar = new BuddyListDb();
            aVar.mo24196a(map);
            arrayList.add(aVar);
        }
        return arrayList;
    }

    /* renamed from: H */
    public static List<BuddyListDb> m13247H() {
        List<Map<String, Object>> a = new BuddyListDb().mo24167a("ownerUserNbr=? and userNbr!=? and userNbr is not null and userStat=?", new String[]{MemberControl.m17125s().mo27187l(), MemberControl.m17125s().mo27187l(), "9"}, "userTimestamp desc", null, null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            BuddyListDb aVar = new BuddyListDb();
            aVar.mo24196a(map);
            arrayList.add(aVar);
        }
        return arrayList;
    }

    /* renamed from: k */
    public static void m13249k(String str) {
        new BuddyListDb().mo24178c("ownerUserNbr=? and userNbr=?", new String[]{MemberControl.m17125s().mo27187l(), str});
    }

    /* renamed from: l */
    public static BuddyListDb m13250l(String str) {
        BuddyListDb aVar = new BuddyListDb();
        Map<String, Object> a = aVar.mo24169a("ownerUserNbr=? and userNbr=?", new String[]{MemberControl.m17125s().mo27187l(), str}, "");
        if (a != null) {
            aVar.mo24196a(a);
        } else {
            aVar.mo24298g(-1);
            aVar.mo24299h(-1);
            aVar.mo24288a(-1L);
            aVar.mo24297f(-1);
            aVar.mo24292c(-1L);
            aVar.mo24290b(-1L);
            aVar.mo24296e(-1L);
            aVar.mo24294d(-1L);
        }
        return aVar;
    }

    /* renamed from: A */
    public boolean mo24283A() {
        return mo24200d("isShareHeart").intValue() == 1;
    }

    /* renamed from: B */
    public boolean mo24284B() {
        return mo24200d("isShareLoc").intValue() == 1;
    }

    /* renamed from: C */
    public boolean mo24285C() {
        return mo24200d("isShareSleep").intValue() == 1;
    }

    /* renamed from: D */
    public boolean mo24286D() {
        return mo24200d("isShareStep").intValue() == 1;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "buddyList_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String a = mo24185a();
        while (i < i2) {
            if (i == 0) {
                sQLiteDatabase.execSQL("alter table " + a + " add " + "preUserStat" + " integer default 1");
                sQLiteDatabase.execSQL("alter table " + a + " add " + "isShareStep" + " integer default 1");
                sQLiteDatabase.execSQL("alter table " + a + " add " + "isShareSleep" + " integer default 1");
                sQLiteDatabase.execSQL("alter table " + a + " add " + "isShareHeart" + " integer default 1");
                sQLiteDatabase.execSQL("alter table " + a + " add " + "isShareLoc" + " integer default 1");
                i = 1;
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("ownerUserNbr", OrmColumnType.STRING);
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("userStat", OrmColumnType.INT);
        mo24172a("timestamp", OrmColumnType.LONG);
        mo24172a("userTimestamp", OrmColumnType.LONG);
        mo24172a("curStepTimestamp", OrmColumnType.LONG);
        mo24172a("stepTimestamp", OrmColumnType.LONG);
        mo24172a("sleepTimestamp", OrmColumnType.LONG);
        mo24172a("heartTimestamp", OrmColumnType.LONG);
        mo24172a("sportTimestamp", OrmColumnType.LONG);
        mo24172a("sportLikeTimestamp", OrmColumnType.LONG);
        mo24172a("remark", OrmColumnType.STRING);
        mo24172a("preUserStat", OrmColumnType.INT);
        mo24172a("isShareStep", OrmColumnType.INT);
        mo24172a("isShareSleep", OrmColumnType.INT);
        mo24172a("isShareHeart", OrmColumnType.INT);
        mo24172a("isShareLoc", OrmColumnType.INT);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 1;
    }

    /* renamed from: c */
    public void mo24292c(long j) {
        mo24194a("sleepTimestamp", Long.valueOf(j));
    }

    /* renamed from: d */
    public void mo24294d(long j) {
        mo24194a("sportLikeTimestamp", Long.valueOf(j));
    }

    /* renamed from: e */
    public void mo24295e(int i) {
        mo24193a("preUserStat", Integer.valueOf(mo24312y()));
        mo24193a("userStat", Integer.valueOf(i));
    }

    /* renamed from: f */
    public void mo24297f(long j) {
        mo24194a("stepTimestamp", Long.valueOf(j));
    }

    /* renamed from: g */
    public void mo24298g(long j) {
        mo24194a("timestamp", Long.valueOf(j));
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"ownerUserNbr", "userNbr"};
    }

    /* renamed from: i */
    public void mo24301i(String str) {
        mo24195a("remark", str);
    }

    /* renamed from: j */
    public void mo24302j(String str) {
        mo24195a("userNbr", str);
    }

    /* renamed from: o */
    public long mo24232o() {
        return mo24201e("curStepTimestamp").longValue();
    }

    /* renamed from: p */
    public long mo24303p() {
        return mo24201e("heartTimestamp").longValue();
    }

    /* renamed from: q */
    public long mo24304q() {
        return Math.max(mo24310w(), Math.max(mo24313z(), Math.max(mo24232o(), Math.max(mo24309v(), Math.max(mo24307t(), Math.max(mo24303p(), mo24308u()))))));
    }

    /* renamed from: r */
    public int mo24305r() {
        return mo24200d("preUserStat").intValue();
    }

    /* renamed from: s */
    public String mo24306s() {
        return mo24203f("remark");
    }

    /* renamed from: t */
    public long mo24307t() {
        return mo24201e("sleepTimestamp").longValue();
    }

    /* renamed from: u */
    public long mo24308u() {
        return mo24201e("sportTimestamp").longValue();
    }

    /* renamed from: v */
    public long mo24309v() {
        return mo24201e("stepTimestamp").longValue();
    }

    /* renamed from: w */
    public long mo24310w() {
        return mo24201e("timestamp").longValue();
    }

    /* renamed from: x */
    public String mo24311x() {
        return mo24203f("userNbr");
    }

    /* renamed from: y */
    public int mo24312y() {
        return mo24200d("userStat").intValue();
    }

    /* renamed from: z */
    public long mo24313z() {
        return mo24201e("userTimestamp").longValue();
    }

    /* renamed from: c */
    public void mo24291c(int i) {
        mo24193a("isShareSleep", Integer.valueOf(i));
    }

    /* renamed from: d */
    public void mo24293d(int i) {
        mo24193a("isShareStep", Integer.valueOf(i));
    }

    /* renamed from: h */
    public void mo24300h(String str) {
        mo24195a("ownerUserNbr", str);
    }

    /* renamed from: e */
    public void mo24296e(long j) {
        mo24194a("sportTimestamp", Long.valueOf(j));
    }

    /* renamed from: h */
    public void mo24299h(long j) {
        mo24194a("userTimestamp", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo24288a(long j) {
        mo24194a("curStepTimestamp", Long.valueOf(j));
    }

    /* renamed from: a */
    public void mo24287a(int i) {
        mo24193a("isShareHeart", Integer.valueOf(i));
    }

    /* renamed from: b */
    public void mo24290b(long j) {
        mo24194a("heartTimestamp", Long.valueOf(j));
    }

    /* renamed from: b */
    public void mo24289b(int i) {
        mo24193a("isShareLoc", Integer.valueOf(i));
    }

    /* renamed from: b */
    public static void m13248b(String str, String str2) {
        BuddyListDb aVar = new BuddyListDb();
        aVar.mo24301i(str2);
        aVar.mo24188a("ownerUserNbr=? and userNbr=?", new String[]{MemberControl.m17125s().mo27187l(), str});
    }
}
