package p119e.p189e.p193d.p194i;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.SleepDayInfo;
import com.heimavista.entity.band.SleepInfo;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import com.heimavista.utils.MapUtil;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.i.e */
public class SleepDataDb extends BandBaseDb {
    /* renamed from: a */
    public static void m13178a(SleepInfo cVar) {
        SleepDataDb eVar = new SleepDataDb();
        int b = eVar.mo24174b("timestamp>=? and timestamp<=?", new String[]{String.valueOf(cVar.f6266e), String.valueOf(cVar.f6267f)});
        LogUtils.m1139c("madySleep " + TimeUtils.m1009b(cVar.f6266e * 1000) + " " + TimeUtils.m1009b(cVar.f6267f * 1000) + " " + cVar.f6265d + " " + cVar.f6268g + " " + eVar.mo24174b("timestamp>=? and timestamp<=?", new String[]{String.valueOf(cVar.f6266e), String.valueOf(cVar.f6267f)}) + " " + eVar.mo24174b("timestamp>? and timestamp<=?", new String[]{String.valueOf(cVar.f6266e), String.valueOf(cVar.f6267f)}) + " " + eVar.mo24174b("timestamp>=? and timestamp<?", new String[]{String.valueOf(cVar.f6266e), String.valueOf(cVar.f6267f)}));
        if (b == 0) {
            eVar.mo24195a("userNbr", cVar.f6262a);
            eVar.mo24194a("timestamp", Long.valueOf(cVar.f6263b));
            eVar.mo24195a("timeFormat", TimeUtils.m1009b(cVar.f6263b * 1000));
            eVar.mo24195a("timezone", cVar.f6264c);
            eVar.mo24193a("type", Integer.valueOf(cVar.f6265d));
            eVar.mo24194a(TtmlNode.START, Long.valueOf(cVar.f6266e));
            eVar.mo24194a(TtmlNode.END, Long.valueOf(cVar.f6267f));
            eVar.mo24193a("sleep", Integer.valueOf(cVar.f6268g));
            eVar.mo24190a("sync", Boolean.valueOf(cVar.f6269h));
            eVar.mo24181l();
        }
    }

    /* renamed from: c */
    private SleepInfo m13179c(Map<String, Object> map) {
        mo24196a(map);
        SleepInfo cVar = new SleepInfo();
        cVar.f6262a = mo24203f("userNbr");
        cVar.f6263b = mo24201e("timestamp").longValue();
        cVar.f6264c = mo24203f("timezone");
        cVar.f6265d = mo24200d("type").intValue();
        cVar.f6266e = mo24201e(TtmlNode.START).longValue();
        cVar.f6267f = mo24201e(TtmlNode.END).longValue();
        cVar.f6268g = mo24200d("sleep").intValue();
        cVar.f6269h = mo24189a("sync").booleanValue();
        LogUtils.m1139c(cVar.toString());
        return cVar;
    }

    /* renamed from: d */
    private List<SleepInfo> m13180d(int i) {
        int i2 = -i;
        Calendar d = TimeUtil.m13448d(i2);
        d.add(10, -4);
        Calendar d2 = TimeUtil.m13448d(i2);
        d2.add(10, 10);
        d2.add(13, 1);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : mo24167a("userNbr=? and sleep>0 and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(d.getTimeInMillis() / 1000), String.valueOf(d2.getTimeInMillis() / 1000)}, "timestamp asc", null, null)) {
            arrayList.add(m13179c(map));
        }
        return arrayList;
    }

    /* renamed from: e */
    private List<SleepInfo> m13181e(int i) {
        Calendar e = TimeUtil.m13449e(-i);
        e.add(10, -4);
        Calendar e2 = TimeUtil.m13449e(1 - i);
        e2.add(10, -10);
        e2.add(13, 1);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : mo24167a("userNbr=? and sleep>0 and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(e.getTimeInMillis() / 1000), String.valueOf(e2.getTimeInMillis() / 1000)}, "timestamp asc", null, null)) {
            arrayList.add(m13179c(map));
        }
        return arrayList;
    }

    /* renamed from: f */
    private List<SleepInfo> m13182f(int i) {
        Calendar f = TimeUtil.m13450f(-i);
        f.add(10, -4);
        Calendar f2 = TimeUtil.m13450f(1 - i);
        f2.add(10, -10);
        f2.add(13, 1);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : mo24167a("userNbr=? and sleep>0 and timestamp between ? and ?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(f.getTimeInMillis() / 1000), String.valueOf(f2.getTimeInMillis() / 1000)}, "timestamp asc", null, null)) {
            arrayList.add(m13179c(map));
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, long]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long */
    /* renamed from: w */
    public static long m13183w() {
        Map<String, Object> a = new SleepDataDb().mo24169a("userNbr=?", new String[]{MemberControl.m17125s().mo27187l()}, "timestamp desc");
        if (a != null) {
            return MapUtil.m15242a(a, TtmlNode.END, (Long) 0L).longValue();
        }
        return 0;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "band_sleepData_det";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, long]
     candidates:
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        super.mo24176b(map);
        mo24173a("type", OrmColumnType.INT, (Object) 1);
        mo24173a(TtmlNode.START, OrmColumnType.LONG, (Object) 0L);
        mo24173a(TtmlNode.END, OrmColumnType.LONG, (Object) 0L);
        mo24173a("sleep", OrmColumnType.INT, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: v */
    public JSONArray mo24265v() {
        List<Map<String, Object>> a = mo24167a("userNbr=? and sync=0 and timestamp>0", new String[]{MemberControl.m17125s().mo27187l()}, "timestamp asc", null, null);
        JSONArray jSONArray = null;
        if (a.isEmpty()) {
            return null;
        }
        for (Map map : a) {
            if (jSONArray == null) {
                jSONArray = new JSONArray();
            }
            SleepInfo c = m13179c(map);
            JSONArray jSONArray2 = new JSONArray();
            jSONArray2.put(c.f6263b);
            jSONArray2.put(c.f6264c);
            jSONArray2.put(c.f6265d);
            jSONArray2.put(c.f6266e);
            jSONArray2.put(c.f6267f);
            jSONArray2.put(c.f6268g);
            jSONArray.put(jSONArray2);
        }
        return jSONArray;
    }

    /* renamed from: b */
    public SleepDayInfo mo24263b(int i) {
        Calendar instance = Calendar.getInstance();
        instance.add(2, -i);
        SleepDayInfo a = m13177a(m13181e(i));
        int i2 = instance.get(2);
        int i3 = i2 == 1 ? TimeUtils.m1011b(instance.getTime()) ? 29 : 28 : (i2 == 3 || i2 == 5 || i2 == 8 || i2 == 10) ? 30 : 31;
        if (a.f6260i > i3) {
            a.f6260i = i3;
        }
        return a;
    }

    /* renamed from: c */
    public SleepDayInfo mo24264c(int i) {
        SleepDayInfo a = m13177a(m13182f(i));
        if (a.f6260i > 7) {
            a.f6260i = 7;
        }
        return a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.d.a(java.lang.String, java.lang.Integer):void
     arg types: [java.lang.String, int]
     candidates:
      e.e.d.i.e.a(long, long):void
      e.e.d.a.a(java.lang.StringBuffer, java.lang.String[]):void
      e.e.d.a.a(java.lang.String, e.e.d.e):void
      e.e.d.d.a(java.lang.String, java.lang.String[]):int
      e.e.d.d.a(java.lang.String, java.lang.Boolean):void
      e.e.d.d.a(java.lang.String, java.lang.Double):void
      e.e.d.d.a(java.lang.String, java.lang.Float):void
      e.e.d.d.a(java.lang.String, java.lang.Long):void
      e.e.d.d.a(java.lang.String, java.lang.String):void
      e.e.d.d.a(java.lang.String, java.lang.Integer):void */
    /* renamed from: a */
    public void mo24262a(long j, long j2) {
        mo24193a("sync", (Integer) 1);
        LogUtils.m1139c("op=postSleep madySleep tick " + TimeUtils.m1009b(j * 1000) + " " + TimeUtils.m1009b(1000 * j2));
        mo24188a("userNbr=? and sync=0 and start>=? and end<=?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(j), String.valueOf(j2)});
    }

    /* renamed from: a */
    private SleepDayInfo m13177a(List<SleepInfo> list) {
        SleepDayInfo bVar = new SleepDayInfo();
        ArrayList arrayList = new ArrayList();
        int size = list.size();
        int i = 0;
        int i2 = 0;
        while (i < size) {
            SleepInfo cVar = list.get(i);
            if (i == 0) {
                i2++;
                bVar.f6252a = cVar.f6266e;
            } else if (cVar.f6266e - list.get(i - 1).f6267f > 43200) {
                i2++;
            }
            if (i == size - 1) {
                bVar.f6253b = cVar.f6267f;
            }
            int a = i + m13176a(0, i, cVar, list);
            arrayList.add(cVar);
            int i3 = cVar.f6268g;
            bVar.f6257f += i3;
            int i4 = cVar.f6265d;
            if (i4 == 1) {
                bVar.f6258g += i3;
                bVar.f6254c += i3;
            } else if (i4 == 2) {
                bVar.f6258g += i3;
                bVar.f6255d += i3;
            } else if (i4 == 3) {
                bVar.f6256e += i3;
                if (a > 0) {
                    bVar.f6259h++;
                }
            }
            i = a + 1;
        }
        bVar.f6261j = arrayList;
        bVar.f6260i = i2;
        return bVar;
    }

    /* renamed from: a */
    private int m13176a(int i, int i2, SleepInfo cVar, List<SleepInfo> list) {
        int i3 = i2 + 1;
        if (i3 >= list.size()) {
            return i;
        }
        long j = list.get(i3).f6266e;
        return (j <= cVar.f6266e || j >= cVar.f6267f) ? i : m13176a(i + 1, i3, cVar, list);
    }

    /* renamed from: a */
    public SleepDayInfo mo24261a(int i) {
        SleepDayInfo a = m13177a(m13180d(i));
        a.f6260i = 1;
        return a;
    }
}
