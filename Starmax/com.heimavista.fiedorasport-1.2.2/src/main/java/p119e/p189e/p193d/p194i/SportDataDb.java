package p119e.p189e.p193d.p194i;

import android.database.sqlite.SQLiteDatabase;
import com.heimavista.entity.band.SportInfo;
import com.heimavista.utils.MapUtil;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.IOrmTable;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p193d.OrmDB;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.i.f */
public class SportDataDb extends DbObjectBase {
    /* renamed from: a */
    public static void m13192a(String str, String str2, int i, long j) {
        SportDataDb fVar = new SportDataDb();
        fVar.mo24193a("cnt", Integer.valueOf(i));
        fVar.mo24194a("likesTimestamp", Long.valueOf(j));
        fVar.mo24188a("userNbr=? and sportId=?", new String[]{str, str2});
    }

    /* renamed from: c */
    private SportInfo m13193c(Map<String, Object> map) {
        mo24196a(map);
        SportInfo dVar = new SportInfo();
        dVar.f6270a = mo24203f("userNbr");
        dVar.f6272c = mo24203f("sportId");
        dVar.f6273d = mo24200d("type").intValue();
        dVar.f6271b = mo24203f("timezone");
        dVar.f6274e = mo24201e(TtmlNode.START).longValue();
        dVar.f6275f = mo24201e(TtmlNode.END).longValue();
        dVar.f6276g = mo24200d("duration").intValue();
        dVar.f6277h = mo24200d("distance").intValue();
        dVar.f6278i = mo24200d("cal").intValue();
        dVar.f6279j = mo24200d("avgHeartRate").intValue();
        dVar.f6280k = mo24200d("minHeartRate").intValue();
        dVar.f6281l = mo24200d("maxHeartRate").intValue();
        dVar.f6282m = mo24200d("cnt").intValue();
        dVar.f6283n = mo24201e("likesTimestamp").longValue();
        dVar.f6284o = mo24189a("sync").booleanValue();
        return dVar;
    }

    /* renamed from: q */
    private String m13194q() {
        return "userNbr=? and duration>180 and start>0 ";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long
     arg types: [java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, long]
     candidates:
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Double):java.lang.Double
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Float):java.lang.Float
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Integer):java.lang.Integer
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.String):java.lang.String
      com.heimavista.utils.e.a(java.util.Map<java.lang.String, java.lang.Object>, java.lang.String, java.lang.Long):java.lang.Long */
    /* renamed from: r */
    public static long m13195r() {
        Map<String, Object> a = new SportDataDb().mo24169a("userNbr=?", new String[]{MemberControl.m17125s().mo27187l()}, "end desc");
        if (a != null) {
            return MapUtil.m15242a(a, TtmlNode.END, (Long) 0L).longValue();
        }
        return 0;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "band_sportData_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.f.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("sportId", OrmColumnType.STRING);
        mo24172a("type", OrmColumnType.INT);
        mo24172a("timezone", OrmColumnType.STRING);
        mo24172a(TtmlNode.START, OrmColumnType.LONG);
        mo24172a(TtmlNode.END, OrmColumnType.LONG);
        mo24173a("duration", OrmColumnType.INT, (Object) 0);
        mo24173a("distance", OrmColumnType.INT, (Object) 0);
        mo24173a("cal", OrmColumnType.INT, (Object) 0);
        mo24173a("avgHeartRate", OrmColumnType.INT, (Object) 0);
        mo24173a("minHeartRate", OrmColumnType.INT, (Object) 0);
        mo24173a("maxHeartRate", OrmColumnType.INT, (Object) 0);
        mo24173a("cnt", OrmColumnType.INT, (Object) 0);
        mo24173a("likesTimestamp", OrmColumnType.LONG, (Object) 0);
        mo24173a("sync", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "sportId"};
    }

    /* renamed from: o */
    public SportInfo mo24232o() {
        Map<String, Object> a = mo24169a(m13194q(), new String[]{MemberControl.m17125s().mo27187l()}, "end desc");
        if (a == null) {
            return null;
        }
        return m13193c(a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.i.f, int, ?[OBJECT, ARRAY], java.lang.String, java.lang.String[], java.lang.String, ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: p */
    public List<SportInfo> mo24271p() {
        OrmDB g = mo24183g();
        ArrayList<Map<String, Object>> a = g.mo24214a((IOrmTable) this, false, (String[]) null, m13194q() + "and " + "cnt" + ">0", new String[]{MemberControl.m17125s().mo27187l()}, TtmlNode.END, (String) null, "end desc", (Integer) null, (Integer) null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            arrayList.add(m13193c(map));
        }
        return arrayList;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.i.f, int, ?[OBJECT, ARRAY], java.lang.String, java.lang.String[], java.lang.String, ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: h */
    public List<SportInfo> mo24270h(String str) {
        ArrayList<Map<String, Object>> a = mo24183g().mo24214a((IOrmTable) this, false, (String[]) null, m13194q(), new String[]{str}, TtmlNode.END, (String) null, "end desc", (Integer) null, (Integer) null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            arrayList.add(m13193c(map));
        }
        return arrayList;
    }

    /* renamed from: a */
    public static void m13191a(SportInfo dVar) {
        SportDataDb fVar = new SportDataDb();
        fVar.mo24195a("userNbr", dVar.f6270a);
        fVar.mo24195a("sportId", dVar.f6272c);
        fVar.mo24193a("type", Integer.valueOf(dVar.f6273d));
        fVar.mo24195a("timezone", dVar.f6271b);
        fVar.mo24194a(TtmlNode.START, Long.valueOf(dVar.f6274e));
        fVar.mo24194a(TtmlNode.END, Long.valueOf(dVar.f6275f));
        fVar.mo24193a("duration", Integer.valueOf(dVar.f6276g));
        fVar.mo24193a("distance", Integer.valueOf(dVar.f6277h));
        fVar.mo24193a("cal", Integer.valueOf(dVar.f6278i));
        fVar.mo24193a("avgHeartRate", Integer.valueOf(dVar.f6279j));
        fVar.mo24193a("minHeartRate", Integer.valueOf(dVar.f6280k));
        fVar.mo24193a("maxHeartRate", Integer.valueOf(dVar.f6281l));
        fVar.mo24190a("sync", Boolean.valueOf(dVar.f6284o));
        fVar.mo24181l();
    }

    /* renamed from: b */
    public SportInfo mo24268b(String str, String str2) {
        Map<String, Object> a = mo24169a(m13194q() + "and " + "sportId" + "=?", new String[]{str2, str}, (String) null);
        if (a != null) {
            return m13193c(a);
        }
        return null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
     arg types: [e.e.d.i.f, int, ?[OBJECT, ARRAY], java.lang.String, java.lang.String[], java.lang.String, ?[OBJECT, ARRAY], java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY]]
     candidates:
      e.e.d.f.a(boolean, java.lang.String, java.lang.String[], java.util.Map<java.lang.String, e.e.d.e>, java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c[], boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>>
      e.e.d.f.a(e.e.d.c, boolean, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.Integer, java.lang.Integer):java.util.ArrayList<java.util.Map<java.lang.String, java.lang.Object>> */
    /* renamed from: b */
    public List<SportInfo> mo24269b(int i) {
        OrmDB g = mo24183g();
        ArrayList<Map<String, Object>> a = g.mo24214a((IOrmTable) this, false, (String[]) null, m13194q() + " and " + "type" + "=?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(i)}, TtmlNode.END, (String) null, "end desc", (Integer) null, (Integer) null);
        ArrayList arrayList = new ArrayList();
        for (Map<String, Object> map : a) {
            arrayList.add(m13193c(map));
        }
        return arrayList;
    }

    /* renamed from: a */
    public SportInfo mo24266a(int i) {
        Map<String, Object> a = mo24169a(m13194q() + "and " + "type" + "=?", new String[]{MemberControl.m17125s().mo27187l(), String.valueOf(i)}, "end desc");
        if (a == null) {
            return new SportInfo();
        }
        return m13193c(a);
    }

    /* renamed from: a */
    public void mo24267a(String str, List<Object> list) {
        if (!list.isEmpty()) {
            for (Object obj : list) {
                mo24178c(m13194q() + " and " + "sportId" + "=?", new String[]{str, String.valueOf(obj)});
            }
        }
    }
}
