package p119e.p189e.p193d.p195j;

import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import java.util.Map;
import p119e.p189e.p193d.DbObjectBase;
import p119e.p189e.p193d.OrmColumnType;
import p119e.p189e.p218g.MemberControl;

/* renamed from: e.e.d.j.c */
public class LocDb extends DbObjectBase {
    /* renamed from: a */
    public String mo24185a() {
        return "buddy_loc_det";
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        String a = mo24185a();
        while (i < i2) {
            if (i == 0) {
                try {
                    sQLiteDatabase.execSQL("alter table " + a + " add " + "battery" + " integer default 0");
                    sQLiteDatabase.execSQL("alter table " + a + " add " + "batteryStat" + " integer default 0  ");
                    i = 1;
                } catch (SQLException unused) {
                }
            } else if (i == 1) {
                sQLiteDatabase.execSQL("alter table " + a + " add " + "locStat" + " integer default 0  ");
                i = 2;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.j.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        mo24172a("userId", OrmColumnType.STRING);
        mo24172a("userNbr", OrmColumnType.STRING);
        mo24172a("lo", OrmColumnType.DOUBLE);
        mo24172a("la", OrmColumnType.DOUBLE);
        mo24172a("speed", OrmColumnType.FLOAT);
        mo24172a("timestamp", OrmColumnType.INT);
        mo24173a("battery", OrmColumnType.INT, (Object) 0);
        mo24173a("batteryStat", OrmColumnType.INT, (Object) 0);
        mo24173a("locStat", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userId", "userNbr"};
    }

    /* renamed from: o */
    public int mo24232o() {
        return mo24200d("battery").intValue();
    }

    /* renamed from: p */
    public double mo24323p() {
        return mo24197b("la").doubleValue();
    }

    /* renamed from: q */
    public double mo24324q() {
        return mo24197b("lo").doubleValue();
    }

    /* renamed from: r */
    public int mo24325r() {
        return mo24200d("locStat").intValue();
    }

    /* renamed from: s */
    public float mo24326s() {
        return mo24198c("speed").floatValue();
    }

    /* renamed from: t */
    public int mo24327t() {
        return mo24200d("timestamp").intValue();
    }

    /* renamed from: u */
    public String mo24328u() {
        return mo24203f("userNbr");
    }

    /* renamed from: h */
    public static LocDb m13307h(String str) {
        LocDb cVar = new LocDb();
        Map<String, Object> a = cVar.mo24169a("userId=? and userNbr=?", new String[]{MemberControl.m17125s().mo27187l(), str}, (String) null);
        if (a == null) {
            return null;
        }
        cVar.mo24196a(a);
        return cVar;
    }

    /* renamed from: a */
    public static void m13306a(String str, String str2, double d, double d2, float f, int i, int i2, int i3, int i4) {
        LocDb cVar = new LocDb();
        cVar.mo24195a("userId", str);
        cVar.mo24195a("userNbr", str2);
        cVar.mo24191a("lo", Double.valueOf(d2));
        cVar.mo24191a("la", Double.valueOf(d));
        cVar.mo24192a("speed", Float.valueOf(f));
        cVar.mo24193a("timestamp", Integer.valueOf(i));
        cVar.mo24193a("battery", Integer.valueOf(i2));
        cVar.mo24193a("batteryStat", Integer.valueOf(i3));
        cVar.mo24193a("locStat", Integer.valueOf(i4));
        cVar.mo24181l();
    }
}
