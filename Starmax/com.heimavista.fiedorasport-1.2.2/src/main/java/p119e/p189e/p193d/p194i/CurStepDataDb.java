package p119e.p189e.p193d.p194i;

import android.database.sqlite.SQLiteDatabase;
import com.blankj.utilcode.util.TimeUtils;
import com.heimavista.entity.band.StepInfo;
import com.heimavista.fiedorasport.p199j.TimeUtil;
import java.util.Map;
import p119e.p189e.p193d.OrmColumnType;

/* renamed from: e.e.d.i.c */
public class CurStepDataDb extends BandBaseDb {
    /* renamed from: k */
    public static CurStepDataDb m13136k(String str) {
        CurStepDataDb cVar = new CurStepDataDb();
        Map<String, Object> a = cVar.mo24169a("userNbr=? and timestamp between ? and ?", new String[]{str, String.valueOf(TimeUtil.m13448d(0).getTimeInMillis() / 1000), String.valueOf(TimeUtil.m13445a(0).getTimeInMillis() / 1000)}, "timestamp desc");
        if (a != null) {
            cVar.mo24196a(a);
        }
        return cVar;
    }

    /* renamed from: a */
    public String mo24185a() {
        return "curStep_det";
    }

    /* renamed from: a */
    public void mo24245a(int i) {
        mo24193a("cal", Integer.valueOf(i));
    }

    /* renamed from: a */
    public void mo24186a(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void
     arg types: [java.lang.String, e.e.d.e, int]
     candidates:
      e.e.d.i.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.i.a.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.StringBuffer, java.util.Map$Entry<java.lang.String, e.e.d.e>, boolean):void
      e.e.d.a.a(java.lang.String, java.lang.String[], java.lang.String):java.util.Map<java.lang.String, java.lang.Object>
      e.e.d.c.a(android.database.sqlite.SQLiteDatabase, int, int):void
      e.e.d.a.a(java.lang.String, e.e.d.e, java.lang.Object):void */
    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo24176b(Map<String, OrmColumnType> map) {
        super.mo24176b(map);
        mo24173a("step", OrmColumnType.INT, (Object) 0);
        mo24173a("distance", OrmColumnType.INT, (Object) 0);
        mo24173a("cal", OrmColumnType.INT, (Object) 0);
    }

    /* renamed from: c */
    public int mo24187c() {
        return 0;
    }

    /* renamed from: c */
    public void mo24247c(int i) {
        mo24193a("step", Integer.valueOf(i));
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String[] mo24204h() {
        return new String[]{"userNbr", "timeFormat"};
    }

    /* renamed from: v */
    public int mo24248v() {
        return mo24200d("cal").intValue();
    }

    /* renamed from: w */
    public int mo24249w() {
        return mo24200d("distance").intValue();
    }

    /* renamed from: x */
    public int mo24250x() {
        return mo24200d("step").intValue();
    }

    /* renamed from: a */
    public static void m13135a(StepInfo stepInfo) {
        CurStepDataDb cVar = new CurStepDataDb();
        super.mo24238j(stepInfo.f6242P);
        super.mo24237i(stepInfo.f6244R);
        super.mo24235a(stepInfo.f6243Q);
        super.mo24236h(TimeUtils.m998a(stepInfo.f6243Q * 1000, "yyyy-MM-dd"));
        cVar.mo24247c(stepInfo.f6245S);
        cVar.mo24246b(stepInfo.f6246T);
        cVar.mo24245a(stepInfo.f6247U);
        cVar.mo24181l();
    }

    /* renamed from: b */
    public void mo24246b(int i) {
        mo24193a("distance", Integer.valueOf(i));
    }
}
