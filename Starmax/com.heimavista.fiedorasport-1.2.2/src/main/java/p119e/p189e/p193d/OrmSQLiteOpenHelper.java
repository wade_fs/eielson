package p119e.p189e.p193d;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/* renamed from: e.e.d.g */
public class OrmSQLiteOpenHelper extends SQLiteOpenHelper {
    public OrmSQLiteOpenHelper(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
    }

    /* renamed from: a */
    public String mo24229a() {
        return "tableVersion";
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table tableVersion(");
        stringBuffer.append("name varchar not null default '',");
        stringBuffer.append("version int not null default 0,");
        stringBuffer.append("primary key(name)");
        stringBuffer.append(")");
        sQLiteDatabase.execSQL(stringBuffer.toString());
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }
}
