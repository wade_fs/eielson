package p119e.p220f.p221a.p222a;

import android.os.Build;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Arrays;
import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

/* renamed from: e.f.a.a.b */
public class TLSSocketFactory extends SSLSocketFactory {

    /* renamed from: e */
    private static final String[] f11173e = {"RC4", "DES", "PSK", "_DHE_"};
    @NonNull

    /* renamed from: a */
    private final SSLSocketFactory f11174a;

    /* renamed from: b */
    private boolean f11175b;
    @NonNull

    /* renamed from: c */
    private Class<?> f11176c;
    @Nullable

    /* renamed from: d */
    private Method f11177d;

    /* renamed from: e.f.a.a.b$b */
    /* compiled from: TLSSocketFactory */
    private static class C4933b extends SSLSocket {

        /* renamed from: P */
        protected final SSLSocket f11178P;

        C4933b(SSLSocket sSLSocket) {
            this.f11178P = super;
        }

        public void addHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
            this.f11178P.addHandshakeCompletedListener(handshakeCompletedListener);
        }

        public void bind(SocketAddress socketAddress) {
            this.f11178P.bind(socketAddress);
        }

        public synchronized void close() {
            this.f11178P.close();
        }

        public void connect(SocketAddress socketAddress) {
            this.f11178P.connect(socketAddress);
        }

        public boolean equals(Object obj) {
            return this.f11178P.equals(obj);
        }

        public SocketChannel getChannel() {
            return this.f11178P.getChannel();
        }

        public boolean getEnableSessionCreation() {
            return this.f11178P.getEnableSessionCreation();
        }

        public String[] getEnabledCipherSuites() {
            return this.f11178P.getEnabledCipherSuites();
        }

        public String[] getEnabledProtocols() {
            return this.f11178P.getEnabledProtocols();
        }

        public InetAddress getInetAddress() {
            return this.f11178P.getInetAddress();
        }

        public InputStream getInputStream() {
            return this.f11178P.getInputStream();
        }

        public boolean getKeepAlive() {
            return this.f11178P.getKeepAlive();
        }

        public InetAddress getLocalAddress() {
            return this.f11178P.getLocalAddress();
        }

        public int getLocalPort() {
            return this.f11178P.getLocalPort();
        }

        public SocketAddress getLocalSocketAddress() {
            return this.f11178P.getLocalSocketAddress();
        }

        public boolean getNeedClientAuth() {
            return this.f11178P.getNeedClientAuth();
        }

        public boolean getOOBInline() {
            return this.f11178P.getOOBInline();
        }

        public OutputStream getOutputStream() {
            return this.f11178P.getOutputStream();
        }

        public int getPort() {
            return this.f11178P.getPort();
        }

        public synchronized int getReceiveBufferSize() {
            return this.f11178P.getReceiveBufferSize();
        }

        public SocketAddress getRemoteSocketAddress() {
            return this.f11178P.getRemoteSocketAddress();
        }

        public boolean getReuseAddress() {
            return this.f11178P.getReuseAddress();
        }

        public synchronized int getSendBufferSize() {
            return this.f11178P.getSendBufferSize();
        }

        public SSLSession getSession() {
            return this.f11178P.getSession();
        }

        public int getSoLinger() {
            return this.f11178P.getSoLinger();
        }

        public synchronized int getSoTimeout() {
            return this.f11178P.getSoTimeout();
        }

        public String[] getSupportedCipherSuites() {
            return this.f11178P.getSupportedCipherSuites();
        }

        public String[] getSupportedProtocols() {
            return this.f11178P.getSupportedProtocols();
        }

        public boolean getTcpNoDelay() {
            return this.f11178P.getTcpNoDelay();
        }

        public int getTrafficClass() {
            return this.f11178P.getTrafficClass();
        }

        public boolean getUseClientMode() {
            return this.f11178P.getUseClientMode();
        }

        public boolean getWantClientAuth() {
            return this.f11178P.getWantClientAuth();
        }

        public boolean isBound() {
            return this.f11178P.isBound();
        }

        public boolean isClosed() {
            return this.f11178P.isClosed();
        }

        public boolean isConnected() {
            return this.f11178P.isConnected();
        }

        public boolean isInputShutdown() {
            return this.f11178P.isInputShutdown();
        }

        public boolean isOutputShutdown() {
            return this.f11178P.isOutputShutdown();
        }

        public void removeHandshakeCompletedListener(HandshakeCompletedListener handshakeCompletedListener) {
            this.f11178P.removeHandshakeCompletedListener(handshakeCompletedListener);
        }

        public void sendUrgentData(int i) {
            this.f11178P.sendUrgentData(i);
        }

        public void setEnableSessionCreation(boolean z) {
            this.f11178P.setEnableSessionCreation(z);
        }

        public void setEnabledCipherSuites(String[] strArr) {
            this.f11178P.setEnabledCipherSuites(strArr);
        }

        public void setEnabledProtocols(String[] strArr) {
            this.f11178P.setEnabledProtocols(strArr);
        }

        public void setKeepAlive(boolean z) {
            this.f11178P.setKeepAlive(z);
        }

        public void setNeedClientAuth(boolean z) {
            this.f11178P.setNeedClientAuth(z);
        }

        public void setOOBInline(boolean z) {
            this.f11178P.setOOBInline(z);
        }

        public void setPerformancePreferences(int i, int i2, int i3) {
            this.f11178P.setPerformancePreferences(i, i2, i3);
        }

        public synchronized void setReceiveBufferSize(int i) {
            this.f11178P.setReceiveBufferSize(i);
        }

        public void setReuseAddress(boolean z) {
            this.f11178P.setReuseAddress(z);
        }

        public synchronized void setSendBufferSize(int i) {
            this.f11178P.setSendBufferSize(i);
        }

        public void setSoLinger(boolean z, int i) {
            this.f11178P.setSoLinger(z, i);
        }

        public synchronized void setSoTimeout(int i) {
            this.f11178P.setSoTimeout(i);
        }

        public void setTcpNoDelay(boolean z) {
            this.f11178P.setTcpNoDelay(z);
        }

        public void setTrafficClass(int i) {
            this.f11178P.setTrafficClass(i);
        }

        public void setUseClientMode(boolean z) {
            this.f11178P.setUseClientMode(z);
        }

        public void setWantClientAuth(boolean z) {
            this.f11178P.setWantClientAuth(z);
        }

        public void shutdownInput() {
            this.f11178P.shutdownInput();
        }

        public void shutdownOutput() {
            this.f11178P.shutdownOutput();
        }

        public void startHandshake() {
            this.f11178P.startHandshake();
        }

        public String toString() {
            return this.f11178P.toString();
        }

        public void connect(SocketAddress socketAddress, int i) {
            this.f11178P.connect(socketAddress, i);
        }
    }

    /* renamed from: e.f.a.a.b$c */
    /* compiled from: TLSSocketFactory */
    private static class C4934c extends C4933b {
        public void setEnabledProtocols(String[] strArr) {
            if (strArr != null && strArr.length == 1 && "SSLv3".equals(strArr[0])) {
                ArrayList arrayList = new ArrayList(Arrays.asList(super.f11178P.getEnabledProtocols()));
                if (arrayList.size() > 1) {
                    arrayList.remove("SSLv3");
                }
                strArr = (String[]) arrayList.toArray(new String[arrayList.size()]);
            }
            super.setEnabledProtocols(strArr);
        }

        private C4934c(SSLSocket sSLSocket) {
            super(sSLSocket);
        }
    }

    public TLSSocketFactory(@NonNull SSLSocketFactory sSLSocketFactory) {
        this(super, true);
    }

    /* renamed from: a */
    private static String[] m17197a() {
        if (Build.VERSION.SDK_INT < 16) {
            return new String[]{"TLSv1"};
        }
        return new String[]{"TLSv1.2"};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:8:? A[ExcHandler: NoSuchMethodException (unused java.lang.NoSuchMethodException), SYNTHETIC, Splitter:B:5:0x000b] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m17199b() {
        /*
            r5 = this;
            java.lang.String r0 = "com.android.org.conscrypt.OpenSSLSocketImpl"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x0009 }
            r5.f11176c = r0     // Catch:{ ClassNotFoundException -> 0x0009 }
            goto L_0x0011
        L_0x0009:
            java.lang.String r0 = "org.apache.harmony.xnet.provider.jsse.OpenSSLSocketImpl"
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
            r5.f11176c = r0     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
        L_0x0011:
            java.lang.Class<?> r0 = r5.f11176c     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
            java.lang.String r1 = "setHostname"
            r2 = 1
            java.lang.Class[] r2 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
            r3 = 0
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            r2[r3] = r4     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
            java.lang.reflect.Method r0 = r0.getMethod(r1, r2)     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
            r5.f11177d = r0     // Catch:{ NoSuchMethodException -> 0x0023, NoSuchMethodException -> 0x0023 }
        L_0x0023:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: p119e.p220f.p221a.p222a.TLSSocketFactory.m17199b():void");
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) {
        Socket createSocket = this.f11174a.createSocket(socket, str, i, z);
        m17196a(createSocket, str);
        return mo27221a(createSocket);
    }

    public String[] getDefaultCipherSuites() {
        if (this.f11175b) {
            return m17198a(this.f11174a.getDefaultCipherSuites());
        }
        return this.f11174a.getDefaultCipherSuites();
    }

    public String[] getSupportedCipherSuites() {
        if (this.f11175b) {
            return m17198a(this.f11174a.getSupportedCipherSuites());
        }
        return this.f11174a.getSupportedCipherSuites();
    }

    public TLSSocketFactory(@NonNull SSLSocketFactory sSLSocketFactory, boolean z) {
        this.f11175b = true;
        this.f11174a = super;
        this.f11175b = z;
        m17199b();
    }

    /* renamed from: a */
    private void m17196a(Socket socket, String str) {
        Method method;
        if (this.f11176c.isInstance(socket) && (method = this.f11177d) != null) {
            try {
                method.invoke(socket, str);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (IllegalAccessException e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    public Socket createSocket(String str, int i) {
        Socket createSocket = this.f11174a.createSocket(str, i);
        m17196a(createSocket, str);
        return mo27221a(createSocket);
    }

    public Socket createSocket(InetAddress inetAddress, int i) {
        return mo27221a(this.f11174a.createSocket(inetAddress, i));
    }

    /* renamed from: a */
    public Socket mo27221a(Socket socket) {
        if (!(socket instanceof SSLSocket)) {
            return socket;
        }
        SSLSocket sSLSocket = (SSLSocket) socket;
        sSLSocket.setEnabledProtocols(m17197a());
        if (this.f11175b) {
            sSLSocket.setEnabledCipherSuites(m17198a(sSLSocket.getEnabledCipherSuites()));
        }
        return new C4934c(sSLSocket);
    }

    public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress2, int i2) {
        return mo27221a(this.f11174a.createSocket(inetAddress, i, inetAddress2, i2));
    }

    public Socket createSocket(String str, int i, InetAddress inetAddress, int i2) {
        Socket createSocket = this.f11174a.createSocket(str, i, inetAddress, i2);
        m17196a(createSocket, str);
        return mo27221a(createSocket);
    }

    public Socket createSocket() {
        return mo27221a(this.f11174a.createSocket());
    }

    /* renamed from: a */
    private static String[] m17198a(String[] strArr) {
        ArrayList arrayList = new ArrayList(Arrays.asList(strArr));
        for (String str : strArr) {
            for (String str2 : f11173e) {
                if (str.contains(str2)) {
                    arrayList.remove(str);
                }
            }
        }
        return (String[]) arrayList.toArray(new String[arrayList.size()]);
    }
}
