package p119e.p220f.p221a.p222a;

import android.util.Base64;
import androidx.annotation.NonNull;
import java.security.SecureRandom;

/* renamed from: e.f.a.a.a */
public final class SecurityUtils {

    /* renamed from: a */
    private static final SecureRandom f11172a = new SecureRandom();

    /* renamed from: a */
    public static Object m17194a(Object obj) {
        return "#####";
    }

    @NonNull
    /* renamed from: a */
    public static String m17195a(int i) {
        byte[] bArr = new byte[i];
        f11172a.nextBytes(bArr);
        return Base64.encodeToString(bArr, 11);
    }
}
