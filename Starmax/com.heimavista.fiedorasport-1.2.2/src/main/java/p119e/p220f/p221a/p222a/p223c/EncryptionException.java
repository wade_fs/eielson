package p119e.p220f.p221a.p222a.p223c;

import androidx.annotation.Nullable;

/* renamed from: e.f.a.a.c.a */
public class EncryptionException extends RuntimeException {
    public EncryptionException(@Nullable String str) {
        super(str);
    }

    public EncryptionException(@Nullable Throwable th) {
        super(th);
    }
}
