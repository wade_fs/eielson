package p119e.p220f.p221a.p222a.p223c;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import com.google.android.exoplayer2.C1750C;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

@WorkerThread
/* renamed from: e.f.a.a.c.b */
public class StringCipher {
    @NonNull

    /* renamed from: a */
    private final Object f11179a = new Object();
    @NonNull

    /* renamed from: b */
    private final String f11180b;

    /* renamed from: c */
    private final int f11181c;

    /* renamed from: d */
    private boolean f11182d;
    @NonNull

    /* renamed from: e */
    private final SecureRandom f11183e;
    @NonNull

    /* renamed from: f */
    private final SecretKeyFactory f11184f;
    @NonNull

    /* renamed from: g */
    private final Cipher f11185g;
    @NonNull

    /* renamed from: h */
    private final Mac f11186h;
    @Nullable

    /* renamed from: i */
    private C4935a f11187i;

    /* renamed from: e.f.a.a.c.b$a */
    /* compiled from: StringCipher */
    private static class C4935a {
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: a */
        public final SecretKey f11188a;
        /* access modifiers changed from: private */
        @NonNull

        /* renamed from: b */
        public final SecretKey f11189b;

        C4935a(@NonNull SecretKey secretKey, @NonNull SecretKey secretKey2) {
            this.f11188a = secretKey;
            this.f11189b = secretKey2;
        }
    }

    public StringCipher(@NonNull String str, int i, boolean z) {
        this.f11180b = str;
        this.f11181c = i;
        this.f11182d = z;
        try {
            this.f11183e = new SecureRandom();
            this.f11184f = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            this.f11185g = Cipher.getInstance("AES/CBC/PKCS5Padding");
            this.f11186h = Mac.getInstance("HmacSHA256");
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException(e);
        }
    }

    @NonNull
    /* renamed from: c */
    private byte[] m17202c(@NonNull Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(this.f11180b, 0);
        String string = sharedPreferences.getString("salt", null);
        if (!TextUtils.isEmpty(string)) {
            return Base64.decode(string, 0);
        }
        byte[] bArr = new byte[16];
        this.f11183e.nextBytes(bArr);
        sharedPreferences.edit().putString("salt", Base64.encodeToString(bArr, 0)).apply();
        return bArr;
    }

    @NonNull
    /* renamed from: d */
    private C4935a m17203d(@NonNull Context context) {
        String b = m17201b(context);
        try {
            byte[] encoded = this.f11184f.generateSecret(new PBEKeySpec(b.toCharArray(), m17202c(context), this.f11181c, 512)).getEncoded();
            return new C4935a(new SecretKeySpec(Arrays.copyOfRange(encoded, 0, 32), "AES"), new SecretKeySpec(Arrays.copyOfRange(encoded, 32, encoded.length), "HmacSHA256"));
        } catch (InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: a */
    public void mo27291a(@NonNull Context context) {
        synchronized (this.f11179a) {
            if (this.f11187i == null) {
                this.f11187i = m17203d(context);
            }
        }
    }

    @NonNull
    /* renamed from: b */
    public String mo27292b(@NonNull Context context, @NonNull String str) {
        String encodeToString;
        synchronized (this.f11179a) {
            mo27291a(context);
            try {
                byte[] bArr = new byte[this.f11185g.getBlockSize()];
                this.f11183e.nextBytes(bArr);
                this.f11185g.init(1, this.f11187i.f11188a, new IvParameterSpec(bArr));
                byte[] doFinal = this.f11185g.doFinal(str.getBytes(C1750C.UTF8_NAME));
                byte[] bArr2 = new byte[(bArr.length + doFinal.length + 32)];
                System.arraycopy(bArr, 0, bArr2, 0, bArr.length);
                int length = bArr.length + 0;
                System.arraycopy(doFinal, 0, bArr2, length, doFinal.length);
                this.f11186h.init(this.f11187i.f11189b);
                this.f11186h.update(bArr2, 0, bArr.length + doFinal.length);
                byte[] doFinal2 = this.f11186h.doFinal();
                System.arraycopy(doFinal2, 0, bArr2, length + doFinal.length, doFinal2.length);
                encodeToString = Base64.encodeToString(bArr2, 0);
            } catch (BadPaddingException e) {
                throw new EncryptionException(e);
            } catch (UnsupportedEncodingException e2) {
                e = e2;
                throw new RuntimeException(e);
            } catch (InvalidKeyException e3) {
                e = e3;
                throw new RuntimeException(e);
            } catch (IllegalBlockSizeException e4) {
                e = e4;
                throw new RuntimeException(e);
            } catch (InvalidAlgorithmParameterException e5) {
                e = e5;
                throw new RuntimeException(e);
            }
        }
        return encodeToString;
    }

    @NonNull
    /* renamed from: a */
    public String mo27290a(@NonNull Context context, @NonNull String str) {
        String str2;
        synchronized (this.f11179a) {
            mo27291a(context);
            try {
                byte[] decode = Base64.decode(str, 0);
                byte[] copyOfRange = Arrays.copyOfRange(decode, decode.length - 32, decode.length);
                this.f11186h.init(this.f11187i.f11189b);
                this.f11186h.update(decode, 0, decode.length - 32);
                if (MessageDigest.isEqual(this.f11186h.doFinal(), copyOfRange)) {
                    this.f11185g.init(2, this.f11187i.f11188a, new IvParameterSpec(decode, 0, 16));
                    str2 = new String(this.f11185g.doFinal(decode, 16, (decode.length - 16) - 32), C1750C.UTF8_NAME);
                } else {
                    throw new EncryptionException("Cipher text has been tampered with.");
                }
            } catch (BadPaddingException e) {
                throw new EncryptionException(e);
            } catch (UnsupportedEncodingException | InvalidAlgorithmParameterException | InvalidKeyException | IllegalBlockSizeException e2) {
                throw new RuntimeException(e2);
            }
        }
        return str2;
    }

    @NonNull
    /* renamed from: b */
    private String m17201b(@NonNull Context context) {
        String string = Settings.Secure.getString(context.getContentResolver(), "android_id");
        String str = this.f11182d ? Build.SERIAL : "";
        return Build.MODEL + Build.MANUFACTURER + str + string + context.getPackageName();
    }
}
