package android.support.p009v4.media.session;

import android.media.session.PlaybackState;
import android.os.Bundle;
import androidx.annotation.RequiresApi;

@RequiresApi(22)
/* renamed from: android.support.v4.media.session.f */
class PlaybackStateCompatApi22 {
    /* renamed from: a */
    public static Bundle m676a(Object obj) {
        return ((PlaybackState) obj).getExtras();
    }
}
