package android.support.p009v4.media;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: android.support.v4.media.RatingCompat */
public final class RatingCompat implements Parcelable {
    public static final Parcelable.Creator<RatingCompat> CREATOR = new C0184a();

    /* renamed from: P */
    private final int f327P;

    /* renamed from: Q */
    private final float f328Q;

    /* renamed from: android.support.v4.media.RatingCompat$a */
    static class C0184a implements Parcelable.Creator<RatingCompat> {
        C0184a() {
        }

        public RatingCompat createFromParcel(Parcel parcel) {
            return new RatingCompat(parcel.readInt(), parcel.readFloat());
        }

        public RatingCompat[] newArray(int i) {
            return new RatingCompat[i];
        }
    }

    RatingCompat(int i, float f) {
        this.f327P = i;
        this.f328Q = f;
    }

    public int describeContents() {
        return this.f327P;
    }

    public String toString() {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append("Rating:style=");
        sb.append(this.f327P);
        sb.append(" rating=");
        float f = this.f328Q;
        if (f < 0.0f) {
            str = "unrated";
        } else {
            str = String.valueOf(f);
        }
        sb.append(str);
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f327P);
        parcel.writeFloat(this.f328Q);
    }
}
