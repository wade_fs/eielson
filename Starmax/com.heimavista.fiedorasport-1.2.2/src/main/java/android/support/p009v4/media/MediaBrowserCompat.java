package android.support.p009v4.media;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.BadParcelableException;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.p009v4.media.MediaBrowserCompatApi21;
import android.support.p009v4.media.MediaBrowserCompatApi26;
import android.support.p009v4.media.session.IMediaSession;
import android.support.p009v4.media.session.MediaSessionCompat;
import android.support.p009v4.p010os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.collection.ArrayMap;
import androidx.core.app.BundleCompat;
import androidx.media.MediaBrowserCompatUtils;
import androidx.media.MediaBrowserProtocol;
import androidx.media.MediaBrowserServiceCompat;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/* renamed from: android.support.v4.media.MediaBrowserCompat */
public final class MediaBrowserCompat {

    /* renamed from: b */
    static final boolean f256b = Log.isLoggable("MediaBrowserCompat", 3);

    /* renamed from: a */
    private final C0164e f257a;

    /* renamed from: android.support.v4.media.MediaBrowserCompat$CustomActionResultReceiver */
    private static class CustomActionResultReceiver extends ResultReceiver {

        /* renamed from: S */
        private final String f258S;

        /* renamed from: T */
        private final Bundle f259T;

        /* renamed from: U */
        private final C0162c f260U;

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo235a(int i, Bundle bundle) {
            if (this.f260U != null) {
                MediaSessionCompat.m615a(bundle);
                if (i == -1) {
                    this.f260U.mo253a(this.f258S, this.f259T, bundle);
                } else if (i == 0) {
                    this.f260U.mo255c(this.f258S, this.f259T, bundle);
                } else if (i != 1) {
                    Log.w("MediaBrowserCompat", "Unknown result code: " + i + " (extras=" + this.f259T + ", resultData=" + bundle + ")");
                } else {
                    this.f260U.mo254b(this.f258S, this.f259T, bundle);
                }
            }
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$ItemReceiver */
    private static class ItemReceiver extends ResultReceiver {

        /* renamed from: S */
        private final String f261S;

        /* renamed from: T */
        private final C0163d f262T;

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo235a(int i, Bundle bundle) {
            MediaSessionCompat.m615a(bundle);
            if (i != 0 || bundle == null || !bundle.containsKey(MediaBrowserServiceCompat.KEY_MEDIA_ITEM)) {
                this.f262T.mo257a(this.f261S);
                return;
            }
            Parcelable parcelable = bundle.getParcelable(MediaBrowserServiceCompat.KEY_MEDIA_ITEM);
            if (parcelable == null || (parcelable instanceof MediaItem)) {
                this.f262T.mo256a((MediaItem) parcelable);
            } else {
                this.f262T.mo257a(this.f261S);
            }
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$SearchResultReceiver */
    private static class SearchResultReceiver extends ResultReceiver {

        /* renamed from: S */
        private final String f265S;

        /* renamed from: T */
        private final Bundle f266T;

        /* renamed from: U */
        private final C0175k f267U;

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo235a(int i, Bundle bundle) {
            MediaSessionCompat.m615a(bundle);
            if (i != 0 || bundle == null || !bundle.containsKey(MediaBrowserServiceCompat.KEY_SEARCH_RESULTS)) {
                this.f267U.mo274a(this.f265S, this.f266T);
                return;
            }
            Parcelable[] parcelableArray = bundle.getParcelableArray(MediaBrowserServiceCompat.KEY_SEARCH_RESULTS);
            ArrayList arrayList = null;
            if (parcelableArray != null) {
                arrayList = new ArrayList();
                for (Parcelable parcelable : parcelableArray) {
                    arrayList.add((MediaItem) parcelable);
                }
            }
            this.f267U.mo275a(this.f265S, this.f266T, arrayList);
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$a */
    private static class C0158a extends Handler {

        /* renamed from: a */
        private final WeakReference<C0174j> f268a;

        /* renamed from: b */
        private WeakReference<Messenger> f269b;

        C0158a(C0174j jVar) {
            this.f268a = new WeakReference<>(jVar);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo241a(Messenger messenger) {
            this.f269b = new WeakReference<>(messenger);
        }

        public void handleMessage(Message message) {
            WeakReference<Messenger> weakReference = this.f269b;
            if (weakReference != null && weakReference.get() != null && this.f268a.get() != null) {
                Bundle data = message.getData();
                MediaSessionCompat.m615a(data);
                C0174j jVar = this.f268a.get();
                Messenger messenger = this.f269b.get();
                try {
                    int i = message.what;
                    if (i == 1) {
                        Bundle bundle = data.getBundle(MediaBrowserProtocol.DATA_ROOT_HINTS);
                        MediaSessionCompat.m615a(bundle);
                        jVar.mo262a(messenger, data.getString(MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), (MediaSessionCompat.Token) data.getParcelable(MediaBrowserProtocol.DATA_MEDIA_SESSION_TOKEN), bundle);
                    } else if (i == 2) {
                        jVar.mo261a(messenger);
                    } else if (i != 3) {
                        Log.w("MediaBrowserCompat", "Unhandled message: " + message + "\n  Client version: " + 1 + "\n  Service version: " + message.arg1);
                    } else {
                        Bundle bundle2 = data.getBundle(MediaBrowserProtocol.DATA_OPTIONS);
                        MediaSessionCompat.m615a(bundle2);
                        Bundle bundle3 = data.getBundle(MediaBrowserProtocol.DATA_NOTIFY_CHILDREN_CHANGED_OPTIONS);
                        MediaSessionCompat.m615a(bundle3);
                        jVar.mo263a(messenger, data.getString(MediaBrowserProtocol.DATA_MEDIA_ITEM_ID), data.getParcelableArrayList(MediaBrowserProtocol.DATA_MEDIA_ITEM_LIST), bundle2, bundle3);
                    }
                } catch (BadParcelableException unused) {
                    Log.e("MediaBrowserCompat", "Could not unparcel the data.");
                    if (message.what == 1) {
                        jVar.mo261a(messenger);
                    }
                }
            }
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$b */
    public static class C0159b {
        C0160a mConnectionCallbackInternal;
        final Object mConnectionCallbackObj;

        /* renamed from: android.support.v4.media.MediaBrowserCompat$b$a */
        interface C0160a {
            /* renamed from: c */
            void mo247c();

            /* renamed from: d */
            void mo248d();

            /* renamed from: e */
            void mo249e();
        }

        /* renamed from: android.support.v4.media.MediaBrowserCompat$b$b */
        private class C0161b implements MediaBrowserCompatApi21.C0185a {
            C0161b() {
            }

            /* renamed from: c */
            public void mo250c() {
                C0160a aVar = C0159b.this.mConnectionCallbackInternal;
                if (aVar != null) {
                    aVar.mo247c();
                }
                C0159b.this.onConnectionSuspended();
            }

            /* renamed from: d */
            public void mo251d() {
                C0160a aVar = C0159b.this.mConnectionCallbackInternal;
                if (aVar != null) {
                    aVar.mo248d();
                }
                C0159b.this.onConnected();
            }

            /* renamed from: e */
            public void mo252e() {
                C0160a aVar = C0159b.this.mConnectionCallbackInternal;
                if (aVar != null) {
                    aVar.mo249e();
                }
                C0159b.this.onConnectionFailed();
            }
        }

        public C0159b() {
            if (Build.VERSION.SDK_INT >= 21) {
                this.mConnectionCallbackObj = MediaBrowserCompatApi21.m537a((MediaBrowserCompatApi21.C0185a) new C0161b());
            } else {
                this.mConnectionCallbackObj = null;
            }
        }

        public void onConnected() {
            throw null;
        }

        public void onConnectionFailed() {
            throw null;
        }

        public void onConnectionSuspended() {
            throw null;
        }

        /* access modifiers changed from: package-private */
        public void setInternalConnectionCallback(C0160a aVar) {
            this.mConnectionCallbackInternal = aVar;
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$c */
    public static abstract class C0162c {
        /* renamed from: a */
        public abstract void mo253a(String str, Bundle bundle, Bundle bundle2);

        /* renamed from: b */
        public abstract void mo254b(String str, Bundle bundle, Bundle bundle2);

        /* renamed from: c */
        public abstract void mo255c(String str, Bundle bundle, Bundle bundle2);
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$d */
    public static abstract class C0163d {
        /* renamed from: a */
        public abstract void mo256a(MediaItem mediaItem);

        /* renamed from: a */
        public abstract void mo257a(@NonNull String str);
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$e */
    interface C0164e {
        /* renamed from: a */
        void mo258a();

        /* renamed from: b */
        void mo259b();

        @NonNull
        /* renamed from: f */
        MediaSessionCompat.Token mo260f();
    }

    @RequiresApi(23)
    /* renamed from: android.support.v4.media.MediaBrowserCompat$g */
    static class C0166g extends C0165f {
        C0166g(Context context, ComponentName componentName, C0159b bVar, Bundle bundle) {
            super(context, componentName, bVar, bundle);
        }
    }

    @RequiresApi(26)
    /* renamed from: android.support.v4.media.MediaBrowserCompat$h */
    static class C0167h extends C0166g {
        C0167h(Context context, ComponentName componentName, C0159b bVar, Bundle bundle) {
            super(context, componentName, bVar, bundle);
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$j */
    interface C0174j {
        /* renamed from: a */
        void mo261a(Messenger messenger);

        /* renamed from: a */
        void mo262a(Messenger messenger, String str, MediaSessionCompat.Token token, Bundle bundle);

        /* renamed from: a */
        void mo263a(Messenger messenger, String str, List list, Bundle bundle, Bundle bundle2);
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$k */
    public static abstract class C0175k {
        /* renamed from: a */
        public abstract void mo274a(@NonNull String str, Bundle bundle);

        /* renamed from: a */
        public abstract void mo275a(@NonNull String str, Bundle bundle, @NonNull List<MediaItem> list);
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$m */
    private static class C0177m {

        /* renamed from: a */
        private final List<C0178n> f301a = new ArrayList();

        /* renamed from: b */
        private final List<Bundle> f302b = new ArrayList();

        /* renamed from: a */
        public List<C0178n> mo282a() {
            return this.f301a;
        }

        /* renamed from: b */
        public List<Bundle> mo283b() {
            return this.f302b;
        }

        /* renamed from: a */
        public C0178n mo281a(Bundle bundle) {
            for (int i = 0; i < this.f302b.size(); i++) {
                if (MediaBrowserCompatUtils.areSameOptions(this.f302b.get(i), bundle)) {
                    return this.f301a.get(i);
                }
            }
            return null;
        }
    }

    public MediaBrowserCompat(Context context, ComponentName componentName, C0159b bVar, Bundle bundle) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 26) {
            this.f257a = new C0167h(context, componentName, bVar, bundle);
        } else if (i >= 23) {
            this.f257a = new C0166g(context, componentName, bVar, bundle);
        } else if (i >= 21) {
            this.f257a = new C0165f(context, componentName, bVar, bundle);
        } else {
            this.f257a = new C0168i(context, componentName, bVar, bundle);
        }
    }

    /* renamed from: a */
    public void mo232a() {
        this.f257a.mo259b();
    }

    /* renamed from: b */
    public void mo233b() {
        this.f257a.mo258a();
    }

    @NonNull
    /* renamed from: c */
    public MediaSessionCompat.Token mo234c() {
        return this.f257a.mo260f();
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$i */
    static class C0168i implements C0164e, C0174j {

        /* renamed from: a */
        final Context f279a;

        /* renamed from: b */
        final ComponentName f280b;

        /* renamed from: c */
        final C0159b f281c;

        /* renamed from: d */
        final Bundle f282d;

        /* renamed from: e */
        final C0158a f283e = new C0158a(this);

        /* renamed from: f */
        private final ArrayMap<String, C0177m> f284f = new ArrayMap<>();

        /* renamed from: g */
        int f285g = 1;

        /* renamed from: h */
        C0171c f286h;

        /* renamed from: i */
        C0176l f287i;

        /* renamed from: j */
        Messenger f288j;

        /* renamed from: k */
        private String f289k;

        /* renamed from: l */
        private MediaSessionCompat.Token f290l;

        /* renamed from: android.support.v4.media.MediaBrowserCompat$i$a */
        class C0169a implements Runnable {
            C0169a() {
            }

            public void run() {
                C0168i iVar = C0168i.this;
                if (iVar.f285g != 0) {
                    iVar.f285g = 2;
                    if (!MediaBrowserCompat.f256b || iVar.f286h == null) {
                        C0168i iVar2 = C0168i.this;
                        if (iVar2.f287i != null) {
                            throw new RuntimeException("mServiceBinderWrapper should be null. Instead it is " + C0168i.this.f287i);
                        } else if (iVar2.f288j == null) {
                            Intent intent = new Intent(MediaBrowserServiceCompat.SERVICE_INTERFACE);
                            intent.setComponent(C0168i.this.f280b);
                            C0168i iVar3 = C0168i.this;
                            iVar3.f286h = new C0171c();
                            boolean z = false;
                            try {
                                z = C0168i.this.f279a.bindService(intent, C0168i.this.f286h, 1);
                            } catch (Exception unused) {
                                Log.e("MediaBrowserCompat", "Failed binding to service " + C0168i.this.f280b);
                            }
                            if (!z) {
                                C0168i.this.mo265d();
                                C0168i.this.f281c.onConnectionFailed();
                            }
                            if (MediaBrowserCompat.f256b) {
                                Log.d("MediaBrowserCompat", "connect...");
                                C0168i.this.mo264c();
                            }
                        } else {
                            throw new RuntimeException("mCallbacksMessenger should be null. Instead it is " + C0168i.this.f288j);
                        }
                    } else {
                        throw new RuntimeException("mServiceConnection should be null. Instead it is " + C0168i.this.f286h);
                    }
                }
            }
        }

        /* renamed from: android.support.v4.media.MediaBrowserCompat$i$b */
        class C0170b implements Runnable {
            C0170b() {
            }

            public void run() {
                C0168i iVar = C0168i.this;
                Messenger messenger = iVar.f288j;
                if (messenger != null) {
                    try {
                        iVar.f287i.mo277a(messenger);
                    } catch (RemoteException unused) {
                        Log.w("MediaBrowserCompat", "RemoteException during connect for " + C0168i.this.f280b);
                    }
                }
                C0168i iVar2 = C0168i.this;
                int i = iVar2.f285g;
                iVar2.mo265d();
                if (i != 0) {
                    C0168i.this.f285g = i;
                }
                if (MediaBrowserCompat.f256b) {
                    Log.d("MediaBrowserCompat", "disconnect...");
                    C0168i.this.mo264c();
                }
            }
        }

        public C0168i(Context context, ComponentName componentName, C0159b bVar, Bundle bundle) {
            Bundle bundle2;
            if (context == null) {
                throw new IllegalArgumentException("context must not be null");
            } else if (componentName == null) {
                throw new IllegalArgumentException("service component must not be null");
            } else if (bVar != null) {
                this.f279a = context;
                this.f280b = componentName;
                this.f281c = bVar;
                if (bundle == null) {
                    bundle2 = null;
                } else {
                    bundle2 = new Bundle(bundle);
                }
                this.f282d = bundle2;
            } else {
                throw new IllegalArgumentException("connection callback must not be null");
            }
        }

        /* renamed from: a */
        public void mo258a() {
            this.f285g = 0;
            this.f283e.post(new C0170b());
        }

        /* renamed from: b */
        public void mo259b() {
            int i = this.f285g;
            if (i == 0 || i == 1) {
                this.f285g = 2;
                this.f283e.post(new C0169a());
                return;
            }
            throw new IllegalStateException("connect() called while neigther disconnecting nor disconnected (state=" + m488a(this.f285g) + ")");
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public void mo264c() {
            Log.d("MediaBrowserCompat", "MediaBrowserCompat...");
            Log.d("MediaBrowserCompat", "  mServiceComponent=" + this.f280b);
            Log.d("MediaBrowserCompat", "  mCallback=" + this.f281c);
            Log.d("MediaBrowserCompat", "  mRootHints=" + this.f282d);
            Log.d("MediaBrowserCompat", "  mState=" + m488a(this.f285g));
            Log.d("MediaBrowserCompat", "  mServiceConnection=" + this.f286h);
            Log.d("MediaBrowserCompat", "  mServiceBinderWrapper=" + this.f287i);
            Log.d("MediaBrowserCompat", "  mCallbacksMessenger=" + this.f288j);
            Log.d("MediaBrowserCompat", "  mRootId=" + this.f289k);
            Log.d("MediaBrowserCompat", "  mMediaSessionToken=" + this.f290l);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: d */
        public void mo265d() {
            C0171c cVar = this.f286h;
            if (cVar != null) {
                this.f279a.unbindService(cVar);
            }
            this.f285g = 1;
            this.f286h = null;
            this.f287i = null;
            this.f288j = null;
            this.f283e.mo241a(null);
            this.f289k = null;
            this.f290l = null;
        }

        /* renamed from: e */
        public boolean mo266e() {
            return this.f285g == 3;
        }

        @NonNull
        /* renamed from: f */
        public MediaSessionCompat.Token mo260f() {
            if (mo266e()) {
                return this.f290l;
            }
            throw new IllegalStateException("getSessionToken() called while not connected(state=" + this.f285g + ")");
        }

        /* renamed from: android.support.v4.media.MediaBrowserCompat$i$c */
        private class C0171c implements ServiceConnection {

            /* renamed from: android.support.v4.media.MediaBrowserCompat$i$c$a */
            class C0172a implements Runnable {

                /* renamed from: P */
                final /* synthetic */ ComponentName f294P;

                /* renamed from: Q */
                final /* synthetic */ IBinder f295Q;

                C0172a(ComponentName componentName, IBinder iBinder) {
                    this.f294P = componentName;
                    this.f295Q = iBinder;
                }

                public void run() {
                    if (MediaBrowserCompat.f256b) {
                        Log.d("MediaBrowserCompat", "MediaServiceConnection.onServiceConnected name=" + this.f294P + " binder=" + this.f295Q);
                        C0168i.this.mo264c();
                    }
                    if (C0171c.this.mo269a("onServiceConnected")) {
                        C0168i iVar = C0168i.this;
                        iVar.f287i = new C0176l(this.f295Q, iVar.f282d);
                        C0168i iVar2 = C0168i.this;
                        iVar2.f288j = new Messenger(iVar2.f283e);
                        C0168i iVar3 = C0168i.this;
                        iVar3.f283e.mo241a(iVar3.f288j);
                        C0168i.this.f285g = 2;
                        try {
                            if (MediaBrowserCompat.f256b) {
                                Log.d("MediaBrowserCompat", "ServiceCallbacks.onConnect...");
                                C0168i.this.mo264c();
                            }
                            C0168i.this.f287i.mo276a(C0168i.this.f279a, C0168i.this.f288j);
                        } catch (RemoteException unused) {
                            Log.w("MediaBrowserCompat", "RemoteException during connect for " + C0168i.this.f280b);
                            if (MediaBrowserCompat.f256b) {
                                Log.d("MediaBrowserCompat", "ServiceCallbacks.onConnect...");
                                C0168i.this.mo264c();
                            }
                        }
                    }
                }
            }

            /* renamed from: android.support.v4.media.MediaBrowserCompat$i$c$b */
            class C0173b implements Runnable {

                /* renamed from: P */
                final /* synthetic */ ComponentName f297P;

                C0173b(ComponentName componentName) {
                    this.f297P = componentName;
                }

                public void run() {
                    if (MediaBrowserCompat.f256b) {
                        Log.d("MediaBrowserCompat", "MediaServiceConnection.onServiceDisconnected name=" + this.f297P + " this=" + this + " mServiceConnection=" + C0168i.this.f286h);
                        C0168i.this.mo264c();
                    }
                    if (C0171c.this.mo269a("onServiceDisconnected")) {
                        C0168i iVar = C0168i.this;
                        iVar.f287i = null;
                        iVar.f288j = null;
                        iVar.f283e.mo241a(null);
                        C0168i iVar2 = C0168i.this;
                        iVar2.f285g = 4;
                        iVar2.f281c.onConnectionSuspended();
                    }
                }
            }

            C0171c() {
            }

            /* renamed from: a */
            private void m499a(Runnable runnable) {
                if (Thread.currentThread() == C0168i.this.f283e.getLooper().getThread()) {
                    runnable.run();
                } else {
                    C0168i.this.f283e.post(runnable);
                }
            }

            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                m499a(new C0172a(componentName, iBinder));
            }

            public void onServiceDisconnected(ComponentName componentName) {
                m499a(new C0173b(componentName));
            }

            /* access modifiers changed from: package-private */
            /* renamed from: a */
            public boolean mo269a(String str) {
                int i;
                C0168i iVar = C0168i.this;
                if (iVar.f286h == this && (i = iVar.f285g) != 0 && i != 1) {
                    return true;
                }
                int i2 = C0168i.this.f285g;
                if (i2 == 0 || i2 == 1) {
                    return false;
                }
                Log.i("MediaBrowserCompat", str + " for " + C0168i.this.f280b + " with mServiceConnection=" + C0168i.this.f286h + " this=" + this);
                return false;
            }
        }

        /* renamed from: a */
        public void mo262a(Messenger messenger, String str, MediaSessionCompat.Token token, Bundle bundle) {
            if (m489a(messenger, "onConnect")) {
                if (this.f285g != 2) {
                    Log.w("MediaBrowserCompat", "onConnect from service while mState=" + m488a(this.f285g) + "... ignoring");
                    return;
                }
                this.f289k = str;
                this.f290l = token;
                this.f285g = 3;
                if (MediaBrowserCompat.f256b) {
                    Log.d("MediaBrowserCompat", "ServiceCallbacks.onConnect...");
                    mo264c();
                }
                this.f281c.onConnected();
                try {
                    for (Map.Entry entry : this.f284f.entrySet()) {
                        String str2 = (String) entry.getKey();
                        C0177m mVar = (C0177m) entry.getValue();
                        List<C0178n> a = mVar.mo282a();
                        List<Bundle> b = mVar.mo283b();
                        for (int i = 0; i < a.size(); i++) {
                            this.f287i.mo278a(str2, a.get(i).f303a, b.get(i), this.f288j);
                        }
                    }
                } catch (RemoteException unused) {
                    Log.d("MediaBrowserCompat", "addSubscription failed with RemoteException.");
                }
            }
        }

        /* renamed from: a */
        public void mo261a(Messenger messenger) {
            Log.e("MediaBrowserCompat", "onConnectFailed for " + this.f280b);
            if (m489a(messenger, "onConnectFailed")) {
                if (this.f285g != 2) {
                    Log.w("MediaBrowserCompat", "onConnect from service while mState=" + m488a(this.f285g) + "... ignoring");
                    return;
                }
                mo265d();
                this.f281c.onConnectionFailed();
            }
        }

        /* renamed from: a */
        public void mo263a(Messenger messenger, String str, List list, Bundle bundle, Bundle bundle2) {
            if (m489a(messenger, "onLoadChildren")) {
                if (MediaBrowserCompat.f256b) {
                    Log.d("MediaBrowserCompat", "onLoadChildren for " + this.f280b + " id=" + str);
                }
                C0177m mVar = this.f284f.get(str);
                if (mVar != null) {
                    C0178n a = mVar.mo281a(bundle);
                    if (a == null) {
                        return;
                    }
                    if (bundle == null) {
                        if (list == null) {
                            a.mo284a(str);
                        } else {
                            a.mo286a(str, list);
                        }
                    } else if (list == null) {
                        a.mo285a(str, bundle);
                    } else {
                        a.mo287a(str, list, bundle);
                    }
                } else if (MediaBrowserCompat.f256b) {
                    Log.d("MediaBrowserCompat", "onLoadChildren for id that isn't subscribed id=" + str);
                }
            }
        }

        /* renamed from: a */
        private static String m488a(int i) {
            if (i == 0) {
                return "CONNECT_STATE_DISCONNECTING";
            }
            if (i == 1) {
                return "CONNECT_STATE_DISCONNECTED";
            }
            if (i == 2) {
                return "CONNECT_STATE_CONNECTING";
            }
            if (i == 3) {
                return "CONNECT_STATE_CONNECTED";
            }
            if (i == 4) {
                return "CONNECT_STATE_SUSPENDED";
            }
            return "UNKNOWN/" + i;
        }

        /* renamed from: a */
        private boolean m489a(Messenger messenger, String str) {
            int i;
            if (this.f288j == messenger && (i = this.f285g) != 0 && i != 1) {
                return true;
            }
            int i2 = this.f285g;
            if (i2 == 0 || i2 == 1) {
                return false;
            }
            Log.i("MediaBrowserCompat", str + " for " + this.f280b + " with mCallbacksMessenger=" + this.f288j + " this=" + this);
            return false;
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$n */
    public static abstract class C0178n {

        /* renamed from: a */
        final IBinder f303a = new Binder();

        /* renamed from: b */
        WeakReference<C0177m> f304b;

        public C0178n() {
            int i = Build.VERSION.SDK_INT;
            if (i >= 26) {
                MediaBrowserCompatApi26.m549a(new C0180b());
            } else if (i >= 21) {
                MediaBrowserCompatApi21.m538a((MediaBrowserCompatApi21.C0188d) new C0179a());
            }
        }

        /* renamed from: a */
        public void mo284a(@NonNull String str) {
        }

        /* renamed from: a */
        public void mo285a(@NonNull String str, @NonNull Bundle bundle) {
        }

        /* renamed from: a */
        public void mo286a(@NonNull String str, @NonNull List<MediaItem> list) {
        }

        /* renamed from: a */
        public void mo287a(@NonNull String str, @NonNull List<MediaItem> list, @NonNull Bundle bundle) {
        }

        /* renamed from: android.support.v4.media.MediaBrowserCompat$n$b */
        private class C0180b extends C0179a implements MediaBrowserCompatApi26.C0190a {
            C0180b() {
                super();
            }

            /* renamed from: a */
            public void mo292a(@NonNull String str, List<?> list, @NonNull Bundle bundle) {
                C0178n.this.mo287a(str, MediaItem.m462a(list), bundle);
            }

            /* renamed from: a */
            public void mo291a(@NonNull String str, @NonNull Bundle bundle) {
                C0178n.this.mo285a(str, bundle);
            }
        }

        /* renamed from: android.support.v4.media.MediaBrowserCompat$n$a */
        private class C0179a implements MediaBrowserCompatApi21.C0188d {
            C0179a() {
            }

            /* renamed from: a */
            public void mo289a(@NonNull String str, List<?> list) {
                WeakReference<C0177m> weakReference = C0178n.this.f304b;
                C0177m mVar = weakReference == null ? null : weakReference.get();
                if (mVar == null) {
                    C0178n.this.mo286a(str, MediaItem.m462a(list));
                    return;
                }
                List<MediaItem> a = MediaItem.m462a(list);
                List<C0178n> a2 = mVar.mo282a();
                List<Bundle> b = mVar.mo283b();
                for (int i = 0; i < a2.size(); i++) {
                    Bundle bundle = b.get(i);
                    if (bundle == null) {
                        C0178n.this.mo286a(str, a);
                    } else {
                        C0178n.this.mo287a(str, mo288a(a, bundle), bundle);
                    }
                }
            }

            public void onError(@NonNull String str) {
                C0178n.this.mo284a(str);
            }

            /* access modifiers changed from: package-private */
            /* renamed from: a */
            public List<MediaItem> mo288a(List<MediaItem> list, Bundle bundle) {
                if (list == null) {
                    return null;
                }
                int i = bundle.getInt("android.media.browse.extra.PAGE", -1);
                int i2 = bundle.getInt("android.media.browse.extra.PAGE_SIZE", -1);
                if (i == -1 && i2 == -1) {
                    return list;
                }
                int i3 = i2 * i;
                int i4 = i3 + i2;
                if (i < 0 || i2 < 1 || i3 >= list.size()) {
                    return Collections.emptyList();
                }
                if (i4 > list.size()) {
                    i4 = list.size();
                }
                return list.subList(i3, i4);
            }
        }
    }

    @RequiresApi(21)
    /* renamed from: android.support.v4.media.MediaBrowserCompat$f */
    static class C0165f implements C0164e, C0174j, C0159b.C0160a {

        /* renamed from: a */
        final Context f271a;

        /* renamed from: b */
        protected final Object f272b;

        /* renamed from: c */
        protected final Bundle f273c;

        /* renamed from: d */
        protected final C0158a f274d = new C0158a(this);

        /* renamed from: e */
        private final ArrayMap<String, C0177m> f275e = new ArrayMap<>();

        /* renamed from: f */
        protected C0176l f276f;

        /* renamed from: g */
        protected Messenger f277g;

        /* renamed from: h */
        private MediaSessionCompat.Token f278h;

        C0165f(Context context, ComponentName componentName, C0159b bVar, Bundle bundle) {
            Bundle bundle2;
            this.f271a = context;
            if (bundle == null) {
                bundle2 = new Bundle();
            }
            this.f273c = bundle2;
            this.f273c.putInt(MediaBrowserProtocol.EXTRA_CLIENT_VERSION, 1);
            bVar.setInternalConnectionCallback(this);
            this.f272b = MediaBrowserCompatApi21.m536a(context, componentName, bVar.mConnectionCallbackObj, this.f273c);
        }

        /* renamed from: a */
        public void mo258a() {
            Messenger messenger;
            C0176l lVar = this.f276f;
            if (!(lVar == null || (messenger = this.f277g) == null)) {
                try {
                    lVar.mo280b(messenger);
                } catch (RemoteException unused) {
                    Log.i("MediaBrowserCompat", "Remote error unregistering client messenger.");
                }
            }
            MediaBrowserCompatApi21.m540b(this.f272b);
        }

        /* renamed from: a */
        public void mo261a(Messenger messenger) {
        }

        /* renamed from: a */
        public void mo262a(Messenger messenger, String str, MediaSessionCompat.Token token, Bundle bundle) {
        }

        /* renamed from: b */
        public void mo259b() {
            MediaBrowserCompatApi21.m539a(this.f272b);
        }

        /* renamed from: c */
        public void mo247c() {
            this.f276f = null;
            this.f277g = null;
            this.f278h = null;
            this.f274d.mo241a(null);
        }

        /* renamed from: d */
        public void mo248d() {
            Bundle c = MediaBrowserCompatApi21.m541c(this.f272b);
            if (c != null) {
                c.getInt(MediaBrowserProtocol.EXTRA_SERVICE_VERSION, 0);
                IBinder binder = BundleCompat.getBinder(c, MediaBrowserProtocol.EXTRA_MESSENGER_BINDER);
                if (binder != null) {
                    this.f276f = new C0176l(binder, this.f273c);
                    this.f277g = new Messenger(this.f274d);
                    this.f274d.mo241a(this.f277g);
                    try {
                        this.f276f.mo279b(this.f271a, this.f277g);
                    } catch (RemoteException unused) {
                        Log.i("MediaBrowserCompat", "Remote error registering client messenger.");
                    }
                }
                IMediaSession a = IMediaSession.C0211a.m642a(BundleCompat.getBinder(c, MediaBrowserProtocol.EXTRA_SESSION_BINDER));
                if (a != null) {
                    this.f278h = MediaSessionCompat.Token.m620a(MediaBrowserCompatApi21.m542d(this.f272b), a);
                }
            }
        }

        /* renamed from: e */
        public void mo249e() {
        }

        @NonNull
        /* renamed from: f */
        public MediaSessionCompat.Token mo260f() {
            if (this.f278h == null) {
                this.f278h = MediaSessionCompat.Token.m619a(MediaBrowserCompatApi21.m542d(this.f272b));
            }
            return this.f278h;
        }

        /* renamed from: a */
        public void mo263a(Messenger messenger, String str, List list, Bundle bundle, Bundle bundle2) {
            if (this.f277g == messenger) {
                C0177m mVar = this.f275e.get(str);
                if (mVar != null) {
                    C0178n a = mVar.mo281a(bundle);
                    if (a == null) {
                        return;
                    }
                    if (bundle == null) {
                        if (list == null) {
                            a.mo284a(str);
                        } else {
                            a.mo286a(str, list);
                        }
                    } else if (list == null) {
                        a.mo285a(str, bundle);
                    } else {
                        a.mo287a(str, list, bundle);
                    }
                } else if (MediaBrowserCompat.f256b) {
                    Log.d("MediaBrowserCompat", "onLoadChildren for id that isn't subscribed id=" + str);
                }
            }
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$l */
    private static class C0176l {

        /* renamed from: a */
        private Messenger f299a;

        /* renamed from: b */
        private Bundle f300b;

        public C0176l(IBinder iBinder, Bundle bundle) {
            this.f299a = new Messenger(iBinder);
            this.f300b = bundle;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo276a(Context context, Messenger messenger) {
            Bundle bundle = new Bundle();
            bundle.putString(MediaBrowserProtocol.DATA_PACKAGE_NAME, context.getPackageName());
            bundle.putBundle(MediaBrowserProtocol.DATA_ROOT_HINTS, this.f300b);
            m506a(1, bundle, messenger);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo279b(Context context, Messenger messenger) {
            Bundle bundle = new Bundle();
            bundle.putString(MediaBrowserProtocol.DATA_PACKAGE_NAME, context.getPackageName());
            bundle.putBundle(MediaBrowserProtocol.DATA_ROOT_HINTS, this.f300b);
            m506a(6, bundle, messenger);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo277a(Messenger messenger) {
            m506a(2, null, messenger);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo280b(Messenger messenger) {
            m506a(7, null, messenger);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo278a(String str, IBinder iBinder, Bundle bundle, Messenger messenger) {
            Bundle bundle2 = new Bundle();
            bundle2.putString(MediaBrowserProtocol.DATA_MEDIA_ITEM_ID, str);
            BundleCompat.putBinder(bundle2, MediaBrowserProtocol.DATA_CALLBACK_TOKEN, iBinder);
            bundle2.putBundle(MediaBrowserProtocol.DATA_OPTIONS, bundle);
            m506a(3, bundle2, messenger);
        }

        /* renamed from: a */
        private void m506a(int i, Bundle bundle, Messenger messenger) {
            Message obtain = Message.obtain();
            obtain.what = i;
            obtain.arg1 = 1;
            obtain.setData(bundle);
            obtain.replyTo = messenger;
            this.f299a.send(obtain);
        }
    }

    /* renamed from: android.support.v4.media.MediaBrowserCompat$MediaItem */
    public static class MediaItem implements Parcelable {
        public static final Parcelable.Creator<MediaItem> CREATOR = new C0157a();

        /* renamed from: P */
        private final int f263P;

        /* renamed from: Q */
        private final MediaDescriptionCompat f264Q;

        /* renamed from: android.support.v4.media.MediaBrowserCompat$MediaItem$a */
        static class C0157a implements Parcelable.Creator<MediaItem> {
            C0157a() {
            }

            public MediaItem createFromParcel(Parcel parcel) {
                return new MediaItem(parcel);
            }

            public MediaItem[] newArray(int i) {
                return new MediaItem[i];
            }
        }

        public MediaItem(@NonNull MediaDescriptionCompat mediaDescriptionCompat, int i) {
            if (mediaDescriptionCompat == null) {
                throw new IllegalArgumentException("description cannot be null");
            } else if (!TextUtils.isEmpty(mediaDescriptionCompat.mo294b())) {
                this.f263P = i;
                this.f264Q = mediaDescriptionCompat;
            } else {
                throw new IllegalArgumentException("description must have a non-empty media id");
            }
        }

        /* renamed from: a */
        public static MediaItem m461a(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new MediaItem(MediaDescriptionCompat.m523a(MediaBrowserCompatApi21.C0187c.m546a(obj)), MediaBrowserCompatApi21.C0187c.m547b(obj));
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "MediaItem{" + "mFlags=" + this.f263P + ", mDescription=" + this.f264Q + '}';
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeInt(this.f263P);
            this.f264Q.writeToParcel(parcel, i);
        }

        /* renamed from: a */
        public static List<MediaItem> m462a(List<?> list) {
            if (list == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            ArrayList arrayList = new ArrayList(list.size());
            for (Object obj : list) {
                arrayList.add(m461a(obj));
            }
            return arrayList;
        }

        MediaItem(Parcel parcel) {
            this.f263P = parcel.readInt();
            this.f264Q = MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
        }
    }
}
