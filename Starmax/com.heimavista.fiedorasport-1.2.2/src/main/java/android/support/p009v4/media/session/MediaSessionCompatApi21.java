package android.support.p009v4.media.session;

import android.media.session.MediaSession;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
/* renamed from: android.support.v4.media.session.d */
class MediaSessionCompatApi21 {

    /* renamed from: android.support.v4.media.session.d$a */
    /* compiled from: MediaSessionCompatApi21 */
    static class C0216a {
        /* renamed from: a */
        public static Object m661a(Object obj) {
            return ((MediaSession.QueueItem) obj).getDescription();
        }

        /* renamed from: b */
        public static long m662b(Object obj) {
            return ((MediaSession.QueueItem) obj).getQueueId();
        }
    }

    /* renamed from: a */
    public static Object m660a(Object obj) {
        if (obj instanceof MediaSession.Token) {
            return obj;
        }
        throw new IllegalArgumentException("token is not a valid MediaSession.Token object");
    }
}
