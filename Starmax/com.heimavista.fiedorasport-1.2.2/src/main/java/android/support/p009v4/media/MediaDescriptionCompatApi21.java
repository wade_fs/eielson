package android.support.p009v4.media;

import android.graphics.Bitmap;
import android.media.MediaDescription;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
/* renamed from: android.support.v4.media.c */
class MediaDescriptionCompatApi21 {

    /* renamed from: android.support.v4.media.c$a */
    /* compiled from: MediaDescriptionCompatApi21 */
    static class C0192a {
        /* renamed from: a */
        public static Object m561a() {
            return new MediaDescription.Builder();
        }

        /* renamed from: b */
        public static void m568b(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setSubtitle(charSequence);
        }

        /* renamed from: c */
        public static void m569c(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setTitle(charSequence);
        }

        /* renamed from: a */
        public static void m567a(Object obj, String str) {
            ((MediaDescription.Builder) obj).setMediaId(str);
        }

        /* renamed from: a */
        public static void m566a(Object obj, CharSequence charSequence) {
            ((MediaDescription.Builder) obj).setDescription(charSequence);
        }

        /* renamed from: a */
        public static void m563a(Object obj, Bitmap bitmap) {
            ((MediaDescription.Builder) obj).setIconBitmap(bitmap);
        }

        /* renamed from: a */
        public static void m564a(Object obj, Uri uri) {
            ((MediaDescription.Builder) obj).setIconUri(uri);
        }

        /* renamed from: a */
        public static void m565a(Object obj, Bundle bundle) {
            ((MediaDescription.Builder) obj).setExtras(bundle);
        }

        /* renamed from: a */
        public static Object m562a(Object obj) {
            return ((MediaDescription.Builder) obj).build();
        }
    }

    /* renamed from: a */
    public static CharSequence m552a(Object obj) {
        return ((MediaDescription) obj).getDescription();
    }

    /* renamed from: b */
    public static Bundle m555b(Object obj) {
        return ((MediaDescription) obj).getExtras();
    }

    /* renamed from: c */
    public static Bitmap m556c(Object obj) {
        return ((MediaDescription) obj).getIconBitmap();
    }

    /* renamed from: d */
    public static Uri m557d(Object obj) {
        return ((MediaDescription) obj).getIconUri();
    }

    /* renamed from: e */
    public static String m558e(Object obj) {
        return ((MediaDescription) obj).getMediaId();
    }

    /* renamed from: f */
    public static CharSequence m559f(Object obj) {
        return ((MediaDescription) obj).getSubtitle();
    }

    /* renamed from: g */
    public static CharSequence m560g(Object obj) {
        return ((MediaDescription) obj).getTitle();
    }

    /* renamed from: a */
    public static void m554a(Object obj, Parcel parcel, int i) {
        ((MediaDescription) obj).writeToParcel(parcel, i);
    }

    /* renamed from: a */
    public static Object m553a(Parcel parcel) {
        return MediaDescription.CREATOR.createFromParcel(parcel);
    }
}
