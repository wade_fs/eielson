package android.support.p009v4.media;

import android.media.browse.MediaBrowser;
import android.os.Bundle;
import android.support.p009v4.media.MediaBrowserCompatApi21;
import android.support.p009v4.media.session.MediaSessionCompat;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import java.util.List;

@RequiresApi(26)
/* renamed from: android.support.v4.media.b */
class MediaBrowserCompatApi26 {

    /* renamed from: android.support.v4.media.b$a */
    /* compiled from: MediaBrowserCompatApi26 */
    interface C0190a extends MediaBrowserCompatApi21.C0188d {
        /* renamed from: a */
        void mo291a(@NonNull String str, @NonNull Bundle bundle);

        /* renamed from: a */
        void mo292a(@NonNull String str, List<?> list, @NonNull Bundle bundle);
    }

    /* renamed from: android.support.v4.media.b$b */
    /* compiled from: MediaBrowserCompatApi26 */
    static class C0191b<T extends C0190a> extends MediaBrowserCompatApi21.C0189e<T> {
        C0191b(T t) {
            super(t);
        }

        public void onChildrenLoaded(@NonNull String str, List<MediaBrowser.MediaItem> list, @NonNull Bundle bundle) {
            MediaSessionCompat.m615a(bundle);
            ((C0190a) super.f330a).mo292a(str, list, bundle);
        }

        public void onError(@NonNull String str, @NonNull Bundle bundle) {
            MediaSessionCompat.m615a(bundle);
            ((C0190a) super.f330a).mo291a(str, bundle);
        }
    }

    /* renamed from: a */
    static Object m549a(C0190a aVar) {
        return new C0191b(aVar);
    }
}
