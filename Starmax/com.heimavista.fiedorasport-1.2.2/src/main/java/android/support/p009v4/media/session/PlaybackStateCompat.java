package android.support.p009v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.p009v4.media.session.PlaybackStateCompatApi21;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.media.session.PlaybackStateCompat */
public final class PlaybackStateCompat implements Parcelable {
    public static final Parcelable.Creator<PlaybackStateCompat> CREATOR = new C0209a();

    /* renamed from: P */
    final int f354P;

    /* renamed from: Q */
    final long f355Q;

    /* renamed from: R */
    final long f356R;

    /* renamed from: S */
    final float f357S;

    /* renamed from: T */
    final long f358T;

    /* renamed from: U */
    final int f359U;

    /* renamed from: V */
    final CharSequence f360V;

    /* renamed from: W */
    final long f361W;

    /* renamed from: X */
    List<CustomAction> f362X;

    /* renamed from: Y */
    final long f363Y;

    /* renamed from: Z */
    final Bundle f364Z;

    /* renamed from: android.support.v4.media.session.PlaybackStateCompat$a */
    static class C0209a implements Parcelable.Creator<PlaybackStateCompat> {
        C0209a() {
        }

        public PlaybackStateCompat createFromParcel(Parcel parcel) {
            return new PlaybackStateCompat(parcel);
        }

        public PlaybackStateCompat[] newArray(int i) {
            return new PlaybackStateCompat[i];
        }
    }

    PlaybackStateCompat(int i, long j, long j2, float f, long j3, int i2, CharSequence charSequence, long j4, List<CustomAction> list, long j5, Bundle bundle) {
        this.f354P = i;
        this.f355Q = j;
        this.f356R = j2;
        this.f357S = f;
        this.f358T = j3;
        this.f359U = i2;
        this.f360V = charSequence;
        this.f361W = j4;
        this.f362X = new ArrayList(list);
        this.f363Y = j5;
        this.f364Z = bundle;
    }

    /* renamed from: a */
    public static int m625a(long j) {
        if (j == 4) {
            return 126;
        }
        if (j == 2) {
            return 127;
        }
        if (j == 32) {
            return 87;
        }
        if (j == 16) {
            return 88;
        }
        if (j == 1) {
            return 86;
        }
        if (j == 64) {
            return 90;
        }
        if (j == 8) {
            return 89;
        }
        return j == 512 ? 85 : 0;
    }

    /* renamed from: a */
    public static PlaybackStateCompat m626a(Object obj) {
        ArrayList arrayList;
        Bundle bundle = null;
        if (obj == null || Build.VERSION.SDK_INT < 21) {
            return null;
        }
        List<Object> d = PlaybackStateCompatApi21.m666d(obj);
        if (d != null) {
            ArrayList arrayList2 = new ArrayList(d.size());
            for (Object obj2 : d) {
                arrayList2.add(CustomAction.m627a(obj2));
            }
            arrayList = arrayList2;
        } else {
            arrayList = null;
        }
        if (Build.VERSION.SDK_INT >= 22) {
            bundle = PlaybackStateCompatApi22.m676a(obj);
        }
        return new PlaybackStateCompat(PlaybackStateCompatApi21.m671i(obj), PlaybackStateCompatApi21.m670h(obj), PlaybackStateCompatApi21.m665c(obj), PlaybackStateCompatApi21.m669g(obj), PlaybackStateCompatApi21.m663a(obj), 0, PlaybackStateCompatApi21.m667e(obj), PlaybackStateCompatApi21.m668f(obj), arrayList, PlaybackStateCompatApi21.m664b(obj), bundle);
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "PlaybackState {" + "state=" + this.f354P + ", position=" + this.f355Q + ", buffered position=" + this.f356R + ", speed=" + this.f357S + ", updated=" + this.f361W + ", actions=" + this.f358T + ", error code=" + this.f359U + ", error message=" + this.f360V + ", custom actions=" + this.f362X + ", active item id=" + this.f363Y + "}";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f354P);
        parcel.writeLong(this.f355Q);
        parcel.writeFloat(this.f357S);
        parcel.writeLong(this.f361W);
        parcel.writeLong(this.f356R);
        parcel.writeLong(this.f358T);
        TextUtils.writeToParcel(this.f360V, parcel, i);
        parcel.writeTypedList(this.f362X);
        parcel.writeLong(this.f363Y);
        parcel.writeBundle(this.f364Z);
        parcel.writeInt(this.f359U);
    }

    /* renamed from: android.support.v4.media.session.PlaybackStateCompat$CustomAction */
    public static final class CustomAction implements Parcelable {
        public static final Parcelable.Creator<CustomAction> CREATOR = new C0208a();

        /* renamed from: P */
        private final String f365P;

        /* renamed from: Q */
        private final CharSequence f366Q;

        /* renamed from: R */
        private final int f367R;

        /* renamed from: S */
        private final Bundle f368S;

        /* renamed from: android.support.v4.media.session.PlaybackStateCompat$CustomAction$a */
        static class C0208a implements Parcelable.Creator<CustomAction> {
            C0208a() {
            }

            public CustomAction createFromParcel(Parcel parcel) {
                return new CustomAction(parcel);
            }

            public CustomAction[] newArray(int i) {
                return new CustomAction[i];
            }
        }

        CustomAction(String str, CharSequence charSequence, int i, Bundle bundle) {
            this.f365P = str;
            this.f366Q = charSequence;
            this.f367R = i;
            this.f368S = bundle;
        }

        /* renamed from: a */
        public static CustomAction m627a(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new CustomAction(PlaybackStateCompatApi21.C0217a.m672a(obj), PlaybackStateCompatApi21.C0217a.m675d(obj), PlaybackStateCompatApi21.C0217a.m674c(obj), PlaybackStateCompatApi21.C0217a.m673b(obj));
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "Action:mName='" + ((Object) this.f366Q) + ", mIcon=" + this.f367R + ", mExtras=" + this.f368S;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f365P);
            TextUtils.writeToParcel(this.f366Q, parcel, i);
            parcel.writeInt(this.f367R);
            parcel.writeBundle(this.f368S);
        }

        CustomAction(Parcel parcel) {
            this.f365P = parcel.readString();
            this.f366Q = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
            this.f367R = parcel.readInt();
            this.f368S = parcel.readBundle(MediaSessionCompat.class.getClassLoader());
        }
    }

    PlaybackStateCompat(Parcel parcel) {
        this.f354P = parcel.readInt();
        this.f355Q = parcel.readLong();
        this.f357S = parcel.readFloat();
        this.f361W = parcel.readLong();
        this.f356R = parcel.readLong();
        this.f358T = parcel.readLong();
        this.f360V = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f362X = parcel.createTypedArrayList(CustomAction.CREATOR);
        this.f363Y = parcel.readLong();
        this.f364Z = parcel.readBundle(MediaSessionCompat.class.getClassLoader());
        this.f359U = parcel.readInt();
    }
}
