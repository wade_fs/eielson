package android.support.p009v4.media;

import android.content.ComponentName;
import android.content.Context;
import android.media.browse.MediaBrowser;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import java.util.List;

@RequiresApi(21)
/* renamed from: android.support.v4.media.a */
class MediaBrowserCompatApi21 {

    /* renamed from: android.support.v4.media.a$a */
    /* compiled from: MediaBrowserCompatApi21 */
    interface C0185a {
        /* renamed from: c */
        void mo250c();

        /* renamed from: d */
        void mo251d();

        /* renamed from: e */
        void mo252e();
    }

    /* renamed from: android.support.v4.media.a$b */
    /* compiled from: MediaBrowserCompatApi21 */
    static class C0186b<T extends C0185a> extends MediaBrowser.ConnectionCallback {

        /* renamed from: a */
        protected final T f329a;

        public C0186b(T t) {
            this.f329a = t;
        }

        public void onConnected() {
            this.f329a.mo251d();
        }

        public void onConnectionFailed() {
            this.f329a.mo252e();
        }

        public void onConnectionSuspended() {
            this.f329a.mo250c();
        }
    }

    /* renamed from: android.support.v4.media.a$c */
    /* compiled from: MediaBrowserCompatApi21 */
    static class C0187c {
        /* renamed from: a */
        public static Object m546a(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getDescription();
        }

        /* renamed from: b */
        public static int m547b(Object obj) {
            return ((MediaBrowser.MediaItem) obj).getFlags();
        }
    }

    /* renamed from: android.support.v4.media.a$d */
    /* compiled from: MediaBrowserCompatApi21 */
    interface C0188d {
        /* renamed from: a */
        void mo289a(@NonNull String str, List<?> list);

        void onError(@NonNull String str);
    }

    /* renamed from: android.support.v4.media.a$e */
    /* compiled from: MediaBrowserCompatApi21 */
    static class C0189e<T extends C0188d> extends MediaBrowser.SubscriptionCallback {

        /* renamed from: a */
        protected final T f330a;

        public C0189e(T t) {
            this.f330a = t;
        }

        public void onChildrenLoaded(@NonNull String str, List<MediaBrowser.MediaItem> list) {
            this.f330a.mo289a(str, list);
        }

        public void onError(@NonNull String str) {
            this.f330a.onError(str);
        }
    }

    /* renamed from: a */
    public static Object m537a(C0185a aVar) {
        return new C0186b(aVar);
    }

    /* renamed from: b */
    public static void m540b(Object obj) {
        ((MediaBrowser) obj).disconnect();
    }

    /* renamed from: c */
    public static Bundle m541c(Object obj) {
        return ((MediaBrowser) obj).getExtras();
    }

    /* renamed from: d */
    public static Object m542d(Object obj) {
        return ((MediaBrowser) obj).getSessionToken();
    }

    /* renamed from: a */
    public static Object m536a(Context context, ComponentName componentName, Object obj, Bundle bundle) {
        return new MediaBrowser(context, componentName, (MediaBrowser.ConnectionCallback) obj, bundle);
    }

    /* renamed from: a */
    public static void m539a(Object obj) {
        ((MediaBrowser) obj).connect();
    }

    /* renamed from: a */
    public static Object m538a(C0188d dVar) {
        return new C0189e(dVar);
    }
}
