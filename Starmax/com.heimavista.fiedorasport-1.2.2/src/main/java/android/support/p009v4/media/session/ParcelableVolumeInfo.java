package android.support.p009v4.media.session;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: android.support.v4.media.session.ParcelableVolumeInfo */
public class ParcelableVolumeInfo implements Parcelable {
    public static final Parcelable.Creator<ParcelableVolumeInfo> CREATOR = new C0207a();

    /* renamed from: P */
    public int f349P;

    /* renamed from: Q */
    public int f350Q;

    /* renamed from: R */
    public int f351R;

    /* renamed from: S */
    public int f352S;

    /* renamed from: T */
    public int f353T;

    /* renamed from: android.support.v4.media.session.ParcelableVolumeInfo$a */
    static class C0207a implements Parcelable.Creator<ParcelableVolumeInfo> {
        C0207a() {
        }

        public ParcelableVolumeInfo createFromParcel(Parcel parcel) {
            return new ParcelableVolumeInfo(parcel);
        }

        public ParcelableVolumeInfo[] newArray(int i) {
            return new ParcelableVolumeInfo[i];
        }
    }

    public ParcelableVolumeInfo(Parcel parcel) {
        this.f349P = parcel.readInt();
        this.f351R = parcel.readInt();
        this.f352S = parcel.readInt();
        this.f353T = parcel.readInt();
        this.f350Q = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f349P);
        parcel.writeInt(this.f351R);
        parcel.writeInt(this.f352S);
        parcel.writeInt(this.f353T);
        parcel.writeInt(this.f350Q);
    }
}
