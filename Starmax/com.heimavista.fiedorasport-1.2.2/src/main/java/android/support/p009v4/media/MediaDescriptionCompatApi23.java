package android.support.p009v4.media;

import android.media.MediaDescription;
import android.net.Uri;
import androidx.annotation.RequiresApi;

@RequiresApi(23)
/* renamed from: android.support.v4.media.d */
class MediaDescriptionCompatApi23 {

    /* renamed from: android.support.v4.media.d$a */
    /* compiled from: MediaDescriptionCompatApi23 */
    static class C0193a {
        /* renamed from: a */
        public static void m571a(Object obj, Uri uri) {
            ((MediaDescription.Builder) obj).setMediaUri(uri);
        }
    }

    /* renamed from: a */
    public static Uri m570a(Object obj) {
        return ((MediaDescription) obj).getMediaUri();
    }
}
