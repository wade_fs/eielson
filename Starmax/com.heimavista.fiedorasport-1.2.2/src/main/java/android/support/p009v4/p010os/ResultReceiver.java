package android.support.p009v4.p010os;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.support.p009v4.p010os.IResultReceiver;
import androidx.annotation.RestrictTo;

@SuppressLint({"BanParcelableUsage"})
@RestrictTo({RestrictTo.Scope.LIBRARY_GROUP_PREFIX})
/* renamed from: android.support.v4.os.ResultReceiver */
public class ResultReceiver implements Parcelable {
    public static final Parcelable.Creator<ResultReceiver> CREATOR = new C0218a();

    /* renamed from: P */
    final boolean f371P = false;

    /* renamed from: Q */
    final Handler f372Q = null;

    /* renamed from: R */
    IResultReceiver f373R;

    /* renamed from: android.support.v4.os.ResultReceiver$a */
    static class C0218a implements Parcelable.Creator<ResultReceiver> {
        C0218a() {
        }

        public ResultReceiver createFromParcel(Parcel parcel) {
            return new ResultReceiver(parcel);
        }

        public ResultReceiver[] newArray(int i) {
            return new ResultReceiver[i];
        }
    }

    /* renamed from: android.support.v4.os.ResultReceiver$b */
    class C0219b extends IResultReceiver.C0221a {
        C0219b() {
        }

        /* renamed from: a */
        public void mo413a(int i, Bundle bundle) {
            ResultReceiver resultReceiver = ResultReceiver.this;
            Handler handler = resultReceiver.f372Q;
            if (handler != null) {
                handler.post(new C0220c(i, bundle));
            } else {
                resultReceiver.mo235a(i, bundle);
            }
        }
    }

    /* renamed from: android.support.v4.os.ResultReceiver$c */
    class C0220c implements Runnable {

        /* renamed from: P */
        final int f375P;

        /* renamed from: Q */
        final Bundle f376Q;

        C0220c(int i, Bundle bundle) {
            this.f375P = i;
            this.f376Q = bundle;
        }

        public void run() {
            ResultReceiver.this.mo235a(this.f375P, this.f376Q);
        }
    }

    ResultReceiver(Parcel parcel) {
        this.f373R = IResultReceiver.C0221a.m681a(parcel.readStrongBinder());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo235a(int i, Bundle bundle) {
    }

    /* renamed from: b */
    public void mo408b(int i, Bundle bundle) {
        if (this.f371P) {
            Handler handler = this.f372Q;
            if (handler != null) {
                handler.post(new C0220c(i, bundle));
            } else {
                mo235a(i, bundle);
            }
        } else {
            IResultReceiver aVar = this.f373R;
            if (aVar != null) {
                try {
                    aVar.mo413a(i, bundle);
                } catch (RemoteException unused) {
                }
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        synchronized (this) {
            if (this.f373R == null) {
                this.f373R = new C0219b();
            }
            parcel.writeStrongBinder(this.f373R.asBinder());
        }
    }
}
