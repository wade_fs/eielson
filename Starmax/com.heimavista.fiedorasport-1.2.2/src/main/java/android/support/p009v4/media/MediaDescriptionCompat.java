package android.support.p009v4.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.p009v4.media.MediaDescriptionCompatApi21;
import android.support.p009v4.media.MediaDescriptionCompatApi23;
import android.text.TextUtils;
import androidx.annotation.Nullable;

/* renamed from: android.support.v4.media.MediaDescriptionCompat */
public final class MediaDescriptionCompat implements Parcelable {
    public static final Parcelable.Creator<MediaDescriptionCompat> CREATOR = new C0181a();

    /* renamed from: P */
    private final String f307P;

    /* renamed from: Q */
    private final CharSequence f308Q;

    /* renamed from: R */
    private final CharSequence f309R;

    /* renamed from: S */
    private final CharSequence f310S;

    /* renamed from: T */
    private final Bitmap f311T;

    /* renamed from: U */
    private final Uri f312U;

    /* renamed from: V */
    private final Bundle f313V;

    /* renamed from: W */
    private final Uri f314W;

    /* renamed from: X */
    private Object f315X;

    /* renamed from: android.support.v4.media.MediaDescriptionCompat$a */
    static class C0181a implements Parcelable.Creator<MediaDescriptionCompat> {
        C0181a() {
        }

        public MediaDescriptionCompat createFromParcel(Parcel parcel) {
            if (Build.VERSION.SDK_INT < 21) {
                return new MediaDescriptionCompat(parcel);
            }
            return MediaDescriptionCompat.m523a(MediaDescriptionCompatApi21.m553a(parcel));
        }

        public MediaDescriptionCompat[] newArray(int i) {
            return new MediaDescriptionCompat[i];
        }
    }

    /* renamed from: android.support.v4.media.MediaDescriptionCompat$b */
    public static final class C0182b {

        /* renamed from: a */
        private String f316a;

        /* renamed from: b */
        private CharSequence f317b;

        /* renamed from: c */
        private CharSequence f318c;

        /* renamed from: d */
        private CharSequence f319d;

        /* renamed from: e */
        private Bitmap f320e;

        /* renamed from: f */
        private Uri f321f;

        /* renamed from: g */
        private Bundle f322g;

        /* renamed from: h */
        private Uri f323h;

        /* renamed from: a */
        public C0182b mo304a(@Nullable String str) {
            this.f316a = str;
            return this;
        }

        /* renamed from: b */
        public C0182b mo307b(@Nullable CharSequence charSequence) {
            this.f318c = charSequence;
            return this;
        }

        /* renamed from: c */
        public C0182b mo308c(@Nullable CharSequence charSequence) {
            this.f317b = charSequence;
            return this;
        }

        /* renamed from: a */
        public C0182b mo303a(@Nullable CharSequence charSequence) {
            this.f319d = charSequence;
            return this;
        }

        /* renamed from: b */
        public C0182b mo306b(@Nullable Uri uri) {
            this.f323h = uri;
            return this;
        }

        /* renamed from: a */
        public C0182b mo300a(@Nullable Bitmap bitmap) {
            this.f320e = bitmap;
            return this;
        }

        /* renamed from: a */
        public C0182b mo301a(@Nullable Uri uri) {
            this.f321f = uri;
            return this;
        }

        /* renamed from: a */
        public C0182b mo302a(@Nullable Bundle bundle) {
            this.f322g = bundle;
            return this;
        }

        /* renamed from: a */
        public MediaDescriptionCompat mo305a() {
            return new MediaDescriptionCompat(this.f316a, this.f317b, this.f318c, this.f319d, this.f320e, this.f321f, this.f322g, this.f323h);
        }
    }

    MediaDescriptionCompat(String str, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, Bitmap bitmap, Uri uri, Bundle bundle, Uri uri2) {
        this.f307P = str;
        this.f308Q = charSequence;
        this.f309R = charSequence2;
        this.f310S = charSequence3;
        this.f311T = bitmap;
        this.f312U = uri;
        this.f313V = bundle;
        this.f314W = uri2;
    }

    /* renamed from: a */
    public Object mo293a() {
        if (this.f315X != null || Build.VERSION.SDK_INT < 21) {
            return this.f315X;
        }
        Object a = MediaDescriptionCompatApi21.C0192a.m561a();
        MediaDescriptionCompatApi21.C0192a.m567a(a, this.f307P);
        MediaDescriptionCompatApi21.C0192a.m569c(a, this.f308Q);
        MediaDescriptionCompatApi21.C0192a.m568b(a, this.f309R);
        MediaDescriptionCompatApi21.C0192a.m566a(a, this.f310S);
        MediaDescriptionCompatApi21.C0192a.m563a(a, this.f311T);
        MediaDescriptionCompatApi21.C0192a.m564a(a, this.f312U);
        Bundle bundle = this.f313V;
        if (Build.VERSION.SDK_INT < 23 && this.f314W != null) {
            if (bundle == null) {
                bundle = new Bundle();
                bundle.putBoolean("android.support.v4.media.description.NULL_BUNDLE_FLAG", true);
            }
            bundle.putParcelable("android.support.v4.media.description.MEDIA_URI", this.f314W);
        }
        MediaDescriptionCompatApi21.C0192a.m565a(a, bundle);
        if (Build.VERSION.SDK_INT >= 23) {
            MediaDescriptionCompatApi23.C0193a.m571a(a, this.f314W);
        }
        this.f315X = MediaDescriptionCompatApi21.C0192a.m562a(a);
        return this.f315X;
    }

    @Nullable
    /* renamed from: b */
    public String mo294b() {
        return this.f307P;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return ((Object) this.f308Q) + ", " + ((Object) this.f309R) + ", " + ((Object) this.f310S);
    }

    public void writeToParcel(Parcel parcel, int i) {
        if (Build.VERSION.SDK_INT < 21) {
            parcel.writeString(this.f307P);
            TextUtils.writeToParcel(this.f308Q, parcel, i);
            TextUtils.writeToParcel(this.f309R, parcel, i);
            TextUtils.writeToParcel(this.f310S, parcel, i);
            parcel.writeParcelable(this.f311T, i);
            parcel.writeParcelable(this.f312U, i);
            parcel.writeBundle(this.f313V);
            parcel.writeParcelable(this.f314W, i);
            return;
        }
        MediaDescriptionCompatApi21.m554a(mo293a(), parcel, i);
    }

    MediaDescriptionCompat(Parcel parcel) {
        this.f307P = parcel.readString();
        this.f308Q = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f309R = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        this.f310S = (CharSequence) TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
        ClassLoader classLoader = MediaDescriptionCompat.class.getClassLoader();
        this.f311T = (Bitmap) parcel.readParcelable(classLoader);
        this.f312U = (Uri) parcel.readParcelable(classLoader);
        this.f313V = parcel.readBundle(classLoader);
        this.f314W = (Uri) parcel.readParcelable(classLoader);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0069  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x006d  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.support.p009v4.media.MediaDescriptionCompat m523a(java.lang.Object r8) {
        /*
            r0 = 0
            if (r8 == 0) goto L_0x0080
            int r1 = android.os.Build.VERSION.SDK_INT
            r2 = 21
            if (r1 < r2) goto L_0x0080
            android.support.v4.media.MediaDescriptionCompat$b r1 = new android.support.v4.media.MediaDescriptionCompat$b
            r1.<init>()
            java.lang.String r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m558e(r8)
            r1.mo304a(r2)
            java.lang.CharSequence r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m560g(r8)
            r1.mo308c(r2)
            java.lang.CharSequence r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m559f(r8)
            r1.mo307b(r2)
            java.lang.CharSequence r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m552a(r8)
            r1.mo303a(r2)
            android.graphics.Bitmap r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m556c(r8)
            r1.mo300a(r2)
            android.net.Uri r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m557d(r8)
            r1.mo301a(r2)
            android.os.Bundle r2 = android.support.p009v4.media.MediaDescriptionCompatApi21.m555b(r8)
            java.lang.String r3 = "android.support.v4.media.description.MEDIA_URI"
            if (r2 == 0) goto L_0x004a
            android.support.p009v4.media.session.MediaSessionCompat.m615a(r2)
            android.os.Parcelable r4 = r2.getParcelable(r3)
            android.net.Uri r4 = (android.net.Uri) r4
            goto L_0x004b
        L_0x004a:
            r4 = r0
        L_0x004b:
            if (r4 == 0) goto L_0x0063
            java.lang.String r5 = "android.support.v4.media.description.NULL_BUNDLE_FLAG"
            boolean r6 = r2.containsKey(r5)
            if (r6 == 0) goto L_0x005d
            int r6 = r2.size()
            r7 = 2
            if (r6 != r7) goto L_0x005d
            goto L_0x0064
        L_0x005d:
            r2.remove(r3)
            r2.remove(r5)
        L_0x0063:
            r0 = r2
        L_0x0064:
            r1.mo302a(r0)
            if (r4 == 0) goto L_0x006d
            r1.mo306b(r4)
            goto L_0x007a
        L_0x006d:
            int r0 = android.os.Build.VERSION.SDK_INT
            r2 = 23
            if (r0 < r2) goto L_0x007a
            android.net.Uri r0 = android.support.p009v4.media.MediaDescriptionCompatApi23.m570a(r8)
            r1.mo306b(r0)
        L_0x007a:
            android.support.v4.media.MediaDescriptionCompat r0 = r1.mo305a()
            r0.f315X = r8
        L_0x0080:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: android.support.p009v4.media.MediaDescriptionCompat.m523a(java.lang.Object):android.support.v4.media.MediaDescriptionCompat");
    }
}
