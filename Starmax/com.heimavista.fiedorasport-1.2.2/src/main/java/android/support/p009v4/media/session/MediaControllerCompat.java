package android.support.p009v4.media.session;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.support.p009v4.media.MediaMetadataCompat;
import android.support.p009v4.media.session.IMediaControllerCallback;
import android.support.p009v4.media.session.IMediaSession;
import android.support.p009v4.media.session.MediaControllerCompatApi21;
import android.support.p009v4.media.session.MediaSessionCompat;
import android.util.Log;
import android.view.KeyEvent;
import androidx.annotation.GuardedBy;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.BundleCompat;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/* renamed from: android.support.v4.media.session.MediaControllerCompat */
public final class MediaControllerCompat {

    /* renamed from: a */
    private final C0199b f331a;

    @RequiresApi(21)
    /* renamed from: android.support.v4.media.session.MediaControllerCompat$MediaControllerImplApi21 */
    static class MediaControllerImplApi21 implements C0199b {

        /* renamed from: a */
        protected final Object f332a;

        /* renamed from: b */
        final Object f333b = new Object();
        @GuardedBy("mLock")

        /* renamed from: c */
        private final List<C0195a> f334c = new ArrayList();

        /* renamed from: d */
        private HashMap<C0195a, C0194a> f335d = new HashMap<>();

        /* renamed from: e */
        final MediaSessionCompat.Token f336e;

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$MediaControllerImplApi21$ExtraBinderRequestResultReceiver */
        private static class ExtraBinderRequestResultReceiver extends ResultReceiver {

            /* renamed from: P */
            private WeakReference<MediaControllerImplApi21> f337P;

            ExtraBinderRequestResultReceiver(MediaControllerImplApi21 mediaControllerImplApi21) {
                super(null);
                this.f337P = new WeakReference<>(mediaControllerImplApi21);
            }

            /* access modifiers changed from: protected */
            public void onReceiveResult(int i, Bundle bundle) {
                MediaControllerImplApi21 mediaControllerImplApi21 = this.f337P.get();
                if (mediaControllerImplApi21 != null && bundle != null) {
                    synchronized (mediaControllerImplApi21.f333b) {
                        mediaControllerImplApi21.f336e.mo373a(IMediaSession.C0211a.m642a(BundleCompat.getBinder(bundle, "android.support.v4.media.session.EXTRA_BINDER")));
                        mediaControllerImplApi21.f336e.mo372a(bundle.getBundle("android.support.v4.media.session.SESSION_TOKEN2_BUNDLE"));
                        mediaControllerImplApi21.mo326a();
                    }
                }
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$MediaControllerImplApi21$a */
        private static class C0194a extends C0195a.C0198c {
            C0194a(C0195a aVar) {
                super(aVar);
            }

            /* renamed from: a */
            public void mo330a(MediaMetadataCompat mediaMetadataCompat) {
                throw new AssertionError();
            }

            /* renamed from: c */
            public void mo334c(Bundle bundle) {
                throw new AssertionError();
            }

            /* renamed from: l */
            public void mo335l() {
                throw new AssertionError();
            }

            /* renamed from: a */
            public void mo333a(List<MediaSessionCompat.QueueItem> list) {
                throw new AssertionError();
            }

            /* renamed from: a */
            public void mo332a(CharSequence charSequence) {
                throw new AssertionError();
            }

            /* renamed from: a */
            public void mo331a(ParcelableVolumeInfo parcelableVolumeInfo) {
                throw new AssertionError();
            }
        }

        public MediaControllerImplApi21(Context context, MediaSessionCompat.Token token) {
            this.f336e = token;
            this.f332a = MediaControllerCompatApi21.m645a(context, this.f336e.mo374b());
            if (this.f332a == null) {
                throw new RemoteException();
            } else if (this.f336e.mo371a() == null) {
                m574b();
            }
        }

        /* renamed from: b */
        private void m574b() {
            mo327a("android.support.v4.media.session.command.GET_EXTRA_BINDER", null, new ExtraBinderRequestResultReceiver(this));
        }

        /* renamed from: a */
        public boolean mo328a(KeyEvent keyEvent) {
            return MediaControllerCompatApi21.m648a(this.f332a, keyEvent);
        }

        /* renamed from: a */
        public void mo327a(String str, Bundle bundle, ResultReceiver resultReceiver) {
            MediaControllerCompatApi21.m647a(this.f332a, str, bundle, resultReceiver);
        }

        /* access modifiers changed from: package-private */
        @GuardedBy("mLock")
        /* renamed from: a */
        public void mo326a() {
            if (this.f336e.mo371a() != null) {
                for (C0195a aVar : this.f334c) {
                    C0194a aVar2 = new C0194a(aVar);
                    this.f335d.put(aVar, aVar2);
                    aVar.f339b = aVar2;
                    try {
                        this.f336e.mo371a().mo397a(aVar2);
                        aVar.mo337a(13, null, null);
                    } catch (RemoteException e) {
                        Log.e("MediaControllerCompat", "Dead object in registerCallback.", e);
                    }
                }
                this.f334c.clear();
            }
        }
    }

    /* renamed from: android.support.v4.media.session.MediaControllerCompat$a */
    public static abstract class C0195a implements IBinder.DeathRecipient {

        /* renamed from: a */
        C0196a f338a;

        /* renamed from: b */
        IMediaControllerCallback f339b;

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$a$a */
        private class C0196a extends Handler {
        }

        public C0195a() {
            if (Build.VERSION.SDK_INT >= 21) {
                MediaControllerCompatApi21.m646a(new C0197b(this));
            } else {
                this.f339b = new C0198c(this);
            }
        }

        /* renamed from: a */
        public void mo336a() {
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo337a(int i, Object obj, Bundle bundle) {
            C0196a aVar = this.f338a;
            if (aVar != null) {
                Message obtainMessage = aVar.obtainMessage(i, obj);
                obtainMessage.setData(bundle);
                obtainMessage.sendToTarget();
            }
        }

        /* renamed from: a */
        public void mo338a(Bundle bundle) {
        }

        /* renamed from: a */
        public void mo339a(MediaMetadataCompat mediaMetadataCompat) {
        }

        /* renamed from: a */
        public void mo340a(C0203f fVar) {
        }

        /* renamed from: a */
        public void mo341a(PlaybackStateCompat playbackStateCompat) {
        }

        /* renamed from: a */
        public void mo342a(CharSequence charSequence) {
        }

        /* renamed from: a */
        public void mo343a(String str, Bundle bundle) {
        }

        /* renamed from: a */
        public void mo344a(List<MediaSessionCompat.QueueItem> list) {
        }

        public void binderDied() {
            mo337a(8, null, null);
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$a$c */
        private static class C0198c extends IMediaControllerCallback.C0210a {

            /* renamed from: a */
            private final WeakReference<C0195a> f341a;

            C0198c(C0195a aVar) {
                this.f341a = new WeakReference<>(aVar);
            }

            /* renamed from: E */
            public void mo354E() {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(13, null, null);
                }
            }

            /* renamed from: I */
            public void mo355I(int i) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(12, Integer.valueOf(i), null);
                }
            }

            /* renamed from: a */
            public void mo357a(String str, Bundle bundle) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(1, str, bundle);
                }
            }

            /* renamed from: c */
            public void mo334c(Bundle bundle) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(7, bundle, null);
                }
            }

            /* renamed from: i */
            public void mo358i(boolean z) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(11, Boolean.valueOf(z), null);
                }
            }

            /* renamed from: j */
            public void mo359j(boolean z) {
            }

            /* renamed from: l */
            public void mo335l() {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(8, null, null);
                }
            }

            public void onRepeatModeChanged(int i) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(9, Integer.valueOf(i), null);
                }
            }

            /* renamed from: a */
            public void mo356a(PlaybackStateCompat playbackStateCompat) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(2, playbackStateCompat, null);
                }
            }

            /* renamed from: a */
            public void mo330a(MediaMetadataCompat mediaMetadataCompat) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(3, mediaMetadataCompat, null);
                }
            }

            /* renamed from: a */
            public void mo333a(List<MediaSessionCompat.QueueItem> list) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(5, list, null);
                }
            }

            /* renamed from: a */
            public void mo332a(CharSequence charSequence) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(6, charSequence, null);
                }
            }

            /* renamed from: a */
            public void mo331a(ParcelableVolumeInfo parcelableVolumeInfo) {
                C0195a aVar = this.f341a.get();
                if (aVar != null) {
                    aVar.mo337a(4, parcelableVolumeInfo != null ? new C0203f(parcelableVolumeInfo.f349P, parcelableVolumeInfo.f350Q, parcelableVolumeInfo.f351R, parcelableVolumeInfo.f352S, parcelableVolumeInfo.f353T) : null, null);
                }
            }
        }

        /* renamed from: android.support.v4.media.session.MediaControllerCompat$a$b */
        private static class C0197b implements MediaControllerCompatApi21.C0213a {

            /* renamed from: a */
            private final WeakReference<C0195a> f340a;

            C0197b(C0195a aVar) {
                this.f340a = new WeakReference<>(aVar);
            }

            /* renamed from: a */
            public void mo349a(String str, Bundle bundle) {
                C0195a aVar = this.f340a.get();
                if (aVar == null) {
                    return;
                }
                if (aVar.f339b == null || Build.VERSION.SDK_INT >= 23) {
                    aVar.mo343a(str, bundle);
                }
            }

            /* renamed from: b */
            public void mo351b(Object obj) {
                C0195a aVar = this.f340a.get();
                if (aVar != null && aVar.f339b == null) {
                    aVar.mo341a(PlaybackStateCompat.m626a(obj));
                }
            }

            /* renamed from: c */
            public void mo352c(Bundle bundle) {
                C0195a aVar = this.f340a.get();
                if (aVar != null) {
                    aVar.mo338a(bundle);
                }
            }

            /* renamed from: l */
            public void mo353l() {
                C0195a aVar = this.f340a.get();
                if (aVar != null) {
                    aVar.mo336a();
                }
            }

            /* renamed from: a */
            public void mo348a(Object obj) {
                C0195a aVar = this.f340a.get();
                if (aVar != null) {
                    aVar.mo339a(MediaMetadataCompat.m535a(obj));
                }
            }

            /* renamed from: a */
            public void mo350a(List<?> list) {
                C0195a aVar = this.f340a.get();
                if (aVar != null) {
                    aVar.mo344a(MediaSessionCompat.QueueItem.m618a(list));
                }
            }

            /* renamed from: a */
            public void mo347a(CharSequence charSequence) {
                C0195a aVar = this.f340a.get();
                if (aVar != null) {
                    aVar.mo342a(charSequence);
                }
            }

            /* renamed from: a */
            public void mo346a(int i, int i2, int i3, int i4, int i5) {
                C0195a aVar = this.f340a.get();
                if (aVar != null) {
                    aVar.mo340a(new C0203f(i, i2, i3, i4, i5));
                }
            }
        }
    }

    /* renamed from: android.support.v4.media.session.MediaControllerCompat$b */
    interface C0199b {
        /* renamed from: a */
        boolean mo328a(KeyEvent keyEvent);
    }

    @RequiresApi(23)
    /* renamed from: android.support.v4.media.session.MediaControllerCompat$c */
    static class C0200c extends MediaControllerImplApi21 {
        public C0200c(Context context, MediaSessionCompat.Token token) {
            super(context, token);
        }
    }

    @RequiresApi(24)
    /* renamed from: android.support.v4.media.session.MediaControllerCompat$d */
    static class C0201d extends C0200c {
        public C0201d(Context context, MediaSessionCompat.Token token) {
            super(context, token);
        }
    }

    /* renamed from: android.support.v4.media.session.MediaControllerCompat$e */
    static class C0202e implements C0199b {

        /* renamed from: a */
        private IMediaSession f342a;

        public C0202e(MediaSessionCompat.Token token) {
            this.f342a = IMediaSession.C0211a.m642a((IBinder) token.mo374b());
        }

        /* renamed from: a */
        public boolean mo328a(KeyEvent keyEvent) {
            if (keyEvent != null) {
                try {
                    this.f342a.mo398a(keyEvent);
                    return false;
                } catch (RemoteException e) {
                    Log.e("MediaControllerCompat", "Dead object in dispatchMediaButtonEvent.", e);
                    return false;
                }
            } else {
                throw new IllegalArgumentException("event may not be null.");
            }
        }
    }

    /* renamed from: android.support.v4.media.session.MediaControllerCompat$f */
    public static final class C0203f {
        C0203f(int i, int i2, int i3, int i4, int i5) {
        }
    }

    public MediaControllerCompat(Context context, @NonNull MediaSessionCompat.Token token) {
        new HashSet();
        if (token != null) {
            int i = Build.VERSION.SDK_INT;
            if (i >= 24) {
                this.f331a = new C0201d(context, token);
            } else if (i >= 23) {
                this.f331a = new C0200c(context, token);
            } else if (i >= 21) {
                this.f331a = new MediaControllerImplApi21(context, token);
            } else {
                this.f331a = new C0202e(token);
            }
        } else {
            throw new IllegalArgumentException("sessionToken must not be null");
        }
    }

    /* renamed from: a */
    public boolean mo325a(KeyEvent keyEvent) {
        if (keyEvent != null) {
            return this.f331a.mo328a(keyEvent);
        }
        throw new IllegalArgumentException("KeyEvent may not be null");
    }
}
