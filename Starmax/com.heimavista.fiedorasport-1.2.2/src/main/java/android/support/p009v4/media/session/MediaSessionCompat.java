package android.support.p009v4.media.session;

import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.support.p009v4.media.MediaDescriptionCompat;
import android.support.p009v4.media.session.MediaSessionCompatApi21;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import java.util.ArrayList;
import java.util.List;

/* renamed from: android.support.v4.media.session.MediaSessionCompat */
public class MediaSessionCompat {

    /* renamed from: a */
    private final MediaControllerCompat f343a;

    @RestrictTo({RestrictTo.Scope.LIBRARY})
    /* renamed from: android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper */
    public static final class ResultReceiverWrapper implements Parcelable {
        public static final Parcelable.Creator<ResultReceiverWrapper> CREATOR = new C0205a();

        /* renamed from: P */
        ResultReceiver f346P;

        /* renamed from: android.support.v4.media.session.MediaSessionCompat$ResultReceiverWrapper$a */
        static class C0205a implements Parcelable.Creator<ResultReceiverWrapper> {
            C0205a() {
            }

            public ResultReceiverWrapper createFromParcel(Parcel parcel) {
                return new ResultReceiverWrapper(parcel);
            }

            public ResultReceiverWrapper[] newArray(int i) {
                return new ResultReceiverWrapper[i];
            }
        }

        ResultReceiverWrapper(Parcel parcel) {
            this.f346P = (ResultReceiver) ResultReceiver.CREATOR.createFromParcel(parcel);
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.f346P.writeToParcel(parcel, i);
        }
    }

    /* renamed from: android.support.v4.media.session.MediaSessionCompat$Token */
    public static final class Token implements Parcelable {
        public static final Parcelable.Creator<Token> CREATOR = new C0206a();

        /* renamed from: P */
        private final Object f347P;

        /* renamed from: Q */
        private IMediaSession f348Q;

        /* renamed from: android.support.v4.media.session.MediaSessionCompat$Token$a */
        static class C0206a implements Parcelable.Creator<Token> {
            C0206a() {
            }

            public Token createFromParcel(Parcel parcel) {
                Object obj;
                if (Build.VERSION.SDK_INT >= 21) {
                    obj = parcel.readParcelable(null);
                } else {
                    obj = parcel.readStrongBinder();
                }
                return new Token(obj);
            }

            public Token[] newArray(int i) {
                return new Token[i];
            }
        }

        Token(Object obj) {
            this(obj, null, null);
        }

        /* renamed from: a */
        public static Token m619a(Object obj) {
            return m620a(obj, null);
        }

        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        /* renamed from: a */
        public void mo372a(Bundle bundle) {
        }

        /* renamed from: b */
        public Object mo374b() {
            return this.f347P;
        }

        public int describeContents() {
            return 0;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof Token)) {
                return false;
            }
            Token token = (Token) obj;
            Object obj2 = this.f347P;
            if (obj2 != null) {
                Object obj3 = token.f347P;
                if (obj3 == null) {
                    return false;
                }
                return obj2.equals(obj3);
            } else if (token.f347P == null) {
                return true;
            } else {
                return false;
            }
        }

        public int hashCode() {
            Object obj = this.f347P;
            if (obj == null) {
                return 0;
            }
            return obj.hashCode();
        }

        public void writeToParcel(Parcel parcel, int i) {
            if (Build.VERSION.SDK_INT >= 21) {
                parcel.writeParcelable((Parcelable) this.f347P, i);
            } else {
                parcel.writeStrongBinder((IBinder) this.f347P);
            }
        }

        Token(Object obj, IMediaSession bVar) {
            this(obj, bVar, null);
        }

        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        /* renamed from: a */
        public static Token m620a(Object obj, IMediaSession bVar) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            MediaSessionCompatApi21.m660a(obj);
            return new Token(obj, bVar);
        }

        Token(Object obj, IMediaSession bVar, Bundle bundle) {
            this.f347P = obj;
            this.f348Q = bVar;
        }

        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        /* renamed from: a */
        public IMediaSession mo371a() {
            return this.f348Q;
        }

        @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
        /* renamed from: a */
        public void mo373a(IMediaSession bVar) {
            this.f348Q = bVar;
        }
    }

    /* renamed from: a */
    public MediaControllerCompat mo361a() {
        return this.f343a;
    }

    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    /* renamed from: a */
    public static void m615a(@Nullable Bundle bundle) {
        if (bundle != null) {
            bundle.setClassLoader(MediaSessionCompat.class.getClassLoader());
        }
    }

    /* renamed from: android.support.v4.media.session.MediaSessionCompat$QueueItem */
    public static final class QueueItem implements Parcelable {
        public static final Parcelable.Creator<QueueItem> CREATOR = new C0204a();

        /* renamed from: P */
        private final MediaDescriptionCompat f344P;

        /* renamed from: Q */
        private final long f345Q;

        /* renamed from: android.support.v4.media.session.MediaSessionCompat$QueueItem$a */
        static class C0204a implements Parcelable.Creator<QueueItem> {
            C0204a() {
            }

            public QueueItem createFromParcel(Parcel parcel) {
                return new QueueItem(parcel);
            }

            public QueueItem[] newArray(int i) {
                return new QueueItem[i];
            }
        }

        private QueueItem(Object obj, MediaDescriptionCompat mediaDescriptionCompat, long j) {
            if (mediaDescriptionCompat == null) {
                throw new IllegalArgumentException("Description cannot be null.");
            } else if (j != -1) {
                this.f344P = mediaDescriptionCompat;
                this.f345Q = j;
            } else {
                throw new IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
            }
        }

        /* renamed from: a */
        public static QueueItem m617a(Object obj) {
            if (obj == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            return new QueueItem(obj, MediaDescriptionCompat.m523a(MediaSessionCompatApi21.C0216a.m661a(obj)), MediaSessionCompatApi21.C0216a.m662b(obj));
        }

        public int describeContents() {
            return 0;
        }

        public String toString() {
            return "MediaSession.QueueItem {Description=" + this.f344P + ", Id=" + this.f345Q + " }";
        }

        public void writeToParcel(Parcel parcel, int i) {
            this.f344P.writeToParcel(parcel, i);
            parcel.writeLong(this.f345Q);
        }

        QueueItem(Parcel parcel) {
            this.f344P = MediaDescriptionCompat.CREATOR.createFromParcel(parcel);
            this.f345Q = parcel.readLong();
        }

        /* renamed from: a */
        public static List<QueueItem> m618a(List<?> list) {
            if (list == null || Build.VERSION.SDK_INT < 21) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            for (Object obj : list) {
                arrayList.add(m617a(obj));
            }
            return arrayList;
        }
    }
}
