package android.support.p009v4.media.session;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.MediaMetadata;
import android.media.session.MediaController;
import android.media.session.MediaSession;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.view.KeyEvent;
import androidx.annotation.RequiresApi;
import java.util.List;

@RequiresApi(21)
/* renamed from: android.support.v4.media.session.c */
class MediaControllerCompatApi21 {

    /* renamed from: android.support.v4.media.session.c$a */
    /* compiled from: MediaControllerCompatApi21 */
    public interface C0213a {
        /* renamed from: a */
        void mo346a(int i, int i2, int i3, int i4, int i5);

        /* renamed from: a */
        void mo347a(CharSequence charSequence);

        /* renamed from: a */
        void mo348a(Object obj);

        /* renamed from: a */
        void mo349a(String str, Bundle bundle);

        /* renamed from: a */
        void mo350a(List<?> list);

        /* renamed from: b */
        void mo351b(Object obj);

        /* renamed from: c */
        void mo352c(Bundle bundle);

        /* renamed from: l */
        void mo353l();
    }

    /* renamed from: android.support.v4.media.session.c$b */
    /* compiled from: MediaControllerCompatApi21 */
    static class C0214b<T extends C0213a> extends MediaController.Callback {

        /* renamed from: a */
        protected final T f370a;

        public C0214b(T t) {
            this.f370a = t;
        }

        public void onAudioInfoChanged(MediaController.PlaybackInfo playbackInfo) {
            this.f370a.mo346a(playbackInfo.getPlaybackType(), C0215c.m659b(playbackInfo), playbackInfo.getVolumeControl(), playbackInfo.getMaxVolume(), playbackInfo.getCurrentVolume());
        }

        public void onExtrasChanged(Bundle bundle) {
            MediaSessionCompat.m615a(bundle);
            this.f370a.mo352c(bundle);
        }

        public void onMetadataChanged(MediaMetadata mediaMetadata) {
            this.f370a.mo348a(mediaMetadata);
        }

        public void onPlaybackStateChanged(PlaybackState playbackState) {
            this.f370a.mo351b(playbackState);
        }

        public void onQueueChanged(List<MediaSession.QueueItem> list) {
            this.f370a.mo350a(list);
        }

        public void onQueueTitleChanged(CharSequence charSequence) {
            this.f370a.mo347a(charSequence);
        }

        public void onSessionDestroyed() {
            this.f370a.mo353l();
        }

        public void onSessionEvent(String str, Bundle bundle) {
            MediaSessionCompat.m615a(bundle);
            this.f370a.mo349a(str, bundle);
        }
    }

    /* renamed from: android.support.v4.media.session.c$c */
    /* compiled from: MediaControllerCompatApi21 */
    public static class C0215c {
        /* renamed from: a */
        public static AudioAttributes m658a(Object obj) {
            return ((MediaController.PlaybackInfo) obj).getAudioAttributes();
        }

        /* renamed from: b */
        public static int m659b(Object obj) {
            return m657a(m658a(obj));
        }

        /* renamed from: a */
        private static int m657a(AudioAttributes audioAttributes) {
            if ((audioAttributes.getFlags() & 1) == 1) {
                return 7;
            }
            if ((audioAttributes.getFlags() & 4) == 4) {
                return 6;
            }
            switch (audioAttributes.getUsage()) {
                case 1:
                case 11:
                case 12:
                case 14:
                    return 3;
                case 2:
                    return 0;
                case 3:
                    return 8;
                case 4:
                    return 4;
                case 5:
                case 7:
                case 8:
                case 9:
                case 10:
                    return 5;
                case 6:
                    return 2;
                case 13:
                    return 1;
                default:
                    return 3;
            }
        }
    }

    /* renamed from: a */
    public static Object m645a(Context context, Object obj) {
        return new MediaController(context, (MediaSession.Token) obj);
    }

    /* renamed from: a */
    public static Object m646a(C0213a aVar) {
        return new C0214b(aVar);
    }

    /* renamed from: a */
    public static boolean m648a(Object obj, KeyEvent keyEvent) {
        return ((MediaController) obj).dispatchMediaButtonEvent(keyEvent);
    }

    /* renamed from: a */
    public static void m647a(Object obj, String str, Bundle bundle, ResultReceiver resultReceiver) {
        ((MediaController) obj).sendCommand(str, bundle, resultReceiver);
    }
}
