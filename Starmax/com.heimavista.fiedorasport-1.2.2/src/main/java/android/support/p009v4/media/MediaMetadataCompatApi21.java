package android.support.p009v4.media;

import android.media.MediaMetadata;
import android.os.Parcel;
import androidx.annotation.RequiresApi;

@RequiresApi(21)
/* renamed from: android.support.v4.media.e */
class MediaMetadataCompatApi21 {
    /* renamed from: a */
    public static void m572a(Object obj, Parcel parcel, int i) {
        ((MediaMetadata) obj).writeToParcel(parcel, i);
    }
}
