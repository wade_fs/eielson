package com.wade.myapplication;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.polidea.rxandroidble2.LogConstants;
import com.polidea.rxandroidble2.LogOptions;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.exceptions.BleException;
import com.polidea.rxandroidble2.exceptions.BleScanException;
import com.polidea.rxandroidble2.scan.ScanFilter;
import com.polidea.rxandroidble2.scan.ScanSettings;

import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;

public class MainActivity extends Activity {
    private static final String TAG = "MyLog";
    private static final int SCAN_REQUEST_CODE = 42;
    private Button btStart, btStop;
    private RxBleClient rxBleClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btStart = findViewById(R.id.start);
        btStop  = findViewById(R.id.stop);
        rxBleClient = RxBleClient.create(this);
        if (!rxBleClient.isScanRuntimePermissionGranted()) {
            LocationPermission.requestLocationPermission(this, rxBleClient);
            Log.d(TAG, "not isScanRuntimePermissionGranted");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        btStart.setEnabled(false);
        btStop.setEnabled(true);
        if (isMyServiceRunning(MyService.class)) return;
        Intent startIntent = new Intent(this, MyService.class);
        startIntent.setAction("start");
        startForegroundService(startIntent);
        Handler handler=new Handler();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                if (isMyServiceRunning(MyService.class)) {
                    startIntent.setAction("scanning");
                } else handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(r, 1000);
    }
    public void start(View v) {
        if (isMyServiceRunning(MyService.class)) return;
        Intent startIntent = new Intent(this, MyService.class);
        startIntent.setAction("start");
        startForegroundService(startIntent);
    }

    public void stop(View v) {
        onStop();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!isMyServiceRunning(MyService.class)) {
            Log.d(TAG, "No Service to stop");
            return;
        }
        Intent stopIntent = new Intent(this, MyService.class);
        Log.d(TAG, "stopBackgroundBleScan... ");
        stopIntent.setAction("stop");
        startService(stopIntent);
    }

    // проверка, запущен ли сервис
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(final int requestCode, @NonNull final String[] permissions,
                                           @NonNull final int[] grantResults) {
        if (LocationPermission.isRequestLocationPermissionGranted(requestCode, permissions, grantResults, rxBleClient)) {
            Intent startIntent = new Intent(this, MyService.class);
            startIntent.setAction("start");
            startService(startIntent);
        }
    }
}
