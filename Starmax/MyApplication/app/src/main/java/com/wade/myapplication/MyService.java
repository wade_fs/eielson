package com.wade.myapplication;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.polidea.rxandroidble2.LogConstants;
import com.polidea.rxandroidble2.LogOptions;
import com.polidea.rxandroidble2.RxBleClient;
import com.polidea.rxandroidble2.exceptions.BleException;
import com.polidea.rxandroidble2.exceptions.BleScanException;
import com.polidea.rxandroidble2.scan.BackgroundScanner;
import com.polidea.rxandroidble2.scan.ScanFilter;
import com.polidea.rxandroidble2.scan.ScanResult;
import com.polidea.rxandroidble2.scan.ScanSettings;

import java.util.List;

import io.reactivex.exceptions.UndeliverableException;
import io.reactivex.plugins.RxJavaPlugins;

public class MyService extends Service {
    private static final String TAG = "MyLog";
    private Handler h;
    private Runnable r;
    private static final int SCAN_REQUEST_CODE = 42;
    BroadcastReceiver mReceiver;
    public RxBleClient rxBleClient;
    private PendingIntent callbackIntent;

    // use this as an inner class like here or as a top-level class
    public class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            BackgroundScanner backgroundScanner = rxBleClient.getBackgroundScanner();

            try {
                final List<ScanResult> scanResults = backgroundScanner.onScanResultReceived(intent);
                String sr = HexString.bytesToHex(scanResults.get(0).getScanRecord().getBytes());
                if (sr.startsWith("00:12:A1"))
                    Log.i(TAG, "ScanReceiver results received: " + scanResults);
            } catch (BleScanException exception) {
                Log.w(TAG, "Failed to scan devices", exception);
            }
        }

        // constructor
        public MyReceiver(){

        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind()");
        return null;
    }

    private Notification updateNotification() {
        String info = "溫度 36.0";
        Log.d(TAG, "updateNotification() "+info);
        Context context = getApplicationContext();

        PendingIntent action = PendingIntent.getActivity(context,
                0, new Intent(context, MainActivity.class),
                PendingIntent.FLAG_CANCEL_CURRENT); // Flag indicating that if the described PendingIntent already exists, the current one should be canceled before generating a new one.

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {

            String CHANNEL_ID = "alex_channel";

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "AlexChannel",
                    NotificationManager.IMPORTANCE_NONE);
            channel.setDescription("Alex channel description");
            manager.createNotificationChannel(channel);

            builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        }
        else
        {
            builder = new NotificationCompat.Builder(context);
        }

        return builder.setContentIntent(action)
                .setContentTitle(info)
                .setTicker(info)
                .setContentText(info)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                        R.mipmap.ic_launcher_foreground))
                .setContentIntent(action)
                .setOngoing(true).build();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand() "+intent.toString()+ " / "+intent.getAction());
        if (intent.getAction().contains("start")) {
            h = new Handler();
            r = new Runnable() {
                @Override
                public void run() {
                    startForeground(101, updateNotification());
                    h.postDelayed(this, 1000);
                }
            };
            scanBleDeviceInBackground();
            h.post(r);
        } else if (intent.getAction().contains("stop")) {
            if (rxBleClient != null) {
                Log.d(TAG, "try to stop both foreground service and ble scan service");
                MyService.rxBleClient.getBackgroundScanner().stopBackgroundBleScan(callbackIntent);
                unregisterReceiver(mReceiver);
            }
            h.removeCallbacks(r);
            stopForeground(true);
            stopSelf();
        }

        return Service.START_STICKY;
    }

    private void scanBleDeviceInBackground() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                if (rxBleClient == null) {
                    Log.d(TAG, "rxBleClient is null");
                    IntentFilter filter = new IntentFilter();
                    filter.addAction("action");
                    filter.addAction("anotherAction");

                    rxBleClient = RxBleClient.create(this);
                    mReceiver = new MyReceiver();
                    registerReceiver(mReceiver, filter);
                }
                if (callbackIntent == null) {
                    Log.d(TAG, "callbackIntent is null");
                    callbackIntent = PendingIntent.getBroadcast(this, SCAN_REQUEST_CODE,
                            new Intent(this, MyService.class), 0);
                }
                rxBleClient.getBackgroundScanner().scanBleDeviceInBackground(
                        callbackIntent,
                        new ScanSettings.Builder()
                                .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                                .build(),
                        new ScanFilter.Builder()
                                // .setDeviceAddress("00:12:A1:09:00:15")
                                // add custom filters if needed
                                .build()
                );
                Log.d(TAG, "scanBleDeviceInBackground to start background scan");
            } catch (BleScanException scanException) {
                Log.d(TAG, "Failed to start background scan", scanException);
            }
        }
    }
}
