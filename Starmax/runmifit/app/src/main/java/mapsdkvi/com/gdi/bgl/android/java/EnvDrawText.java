package mapsdkvi.com.gdi.bgl.android.java;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.TextPaint;
import android.util.SparseArray;
import com.baidu.mapapi.common.SysOSUtil;

public class EnvDrawText {

    /* renamed from: a */
    private static final String f7930a = EnvDrawText.class.getSimpleName();
    public static boolean bBmpChange = false;
    public static Bitmap bmp = null;
    public static int[] buffer = null;
    public static SparseArray<C3410a> fontCache = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x0325, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized int[] drawText(java.lang.String r26, int r27, int r28, int[] r29, int r30, int r31, int r32, int r33, int r34) {
        /*
            r0 = r26
            r1 = r29
            r2 = r30
            r3 = r31
            r4 = r32
            r5 = r33
            r6 = r34
            java.lang.Class<mapsdkvi.com.gdi.bgl.android.java.EnvDrawText> r7 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.class
            monitor-enter(r7)
            android.graphics.Canvas r9 = new android.graphics.Canvas     // Catch:{ all -> 0x0326 }
            r9.<init>()     // Catch:{ all -> 0x0326 }
            android.text.TextPaint r10 = new android.text.TextPaint     // Catch:{ all -> 0x0326 }
            r10.<init>()     // Catch:{ all -> 0x0326 }
            java.lang.String r11 = com.baidu.mapapi.common.SysOSUtil.getPhoneType()     // Catch:{ all -> 0x0326 }
            r12 = 0
            if (r11 == 0) goto L_0x002c
            java.lang.String r13 = "vivo X3L"
            boolean r11 = r11.equals(r13)     // Catch:{ all -> 0x0326 }
            if (r11 == 0) goto L_0x002c
            r11 = 0
            goto L_0x002e
        L_0x002c:
            r11 = r28
        L_0x002e:
            r10.reset()     // Catch:{ all -> 0x0326 }
            r13 = 1
            r10.setSubpixelText(r13)     // Catch:{ all -> 0x0326 }
            r10.setAntiAlias(r13)     // Catch:{ all -> 0x0326 }
            r14 = r27
            float r14 = (float) r14     // Catch:{ all -> 0x0326 }
            r10.setTextSize(r14)     // Catch:{ all -> 0x0326 }
            r15 = 0
            r10.setShadowLayer(r15, r15, r15, r12)     // Catch:{ all -> 0x0326 }
            r8 = 2
            if (r11 == r13) goto L_0x0058
            if (r11 == r8) goto L_0x0051
            android.graphics.Typeface r15 = android.graphics.Typeface.DEFAULT     // Catch:{ all -> 0x0326 }
            android.graphics.Typeface r15 = android.graphics.Typeface.create(r15, r12)     // Catch:{ all -> 0x0326 }
        L_0x004d:
            r10.setTypeface(r15)     // Catch:{ all -> 0x0326 }
            goto L_0x005f
        L_0x0051:
            android.graphics.Typeface r15 = android.graphics.Typeface.DEFAULT     // Catch:{ all -> 0x0326 }
            android.graphics.Typeface r15 = android.graphics.Typeface.create(r15, r8)     // Catch:{ all -> 0x0326 }
            goto L_0x004d
        L_0x0058:
            android.graphics.Typeface r15 = android.graphics.Typeface.DEFAULT     // Catch:{ all -> 0x0326 }
            android.graphics.Typeface r15 = android.graphics.Typeface.create(r15, r13)     // Catch:{ all -> 0x0326 }
            goto L_0x004d
        L_0x005f:
            if (r5 == 0) goto L_0x0074
            float r15 = (float) r5     // Catch:{ all -> 0x0326 }
            r10.setStrokeWidth(r15)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Cap r15 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x0326 }
            r10.setStrokeCap(r15)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Join r15 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x0326 }
            r10.setStrokeJoin(r15)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Style r15 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0326 }
            r10.setStyle(r15)     // Catch:{ all -> 0x0326 }
        L_0x0074:
            r10.setSubpixelText(r13)     // Catch:{ all -> 0x0326 }
            r10.setAntiAlias(r13)     // Catch:{ all -> 0x0326 }
            if (r11 == 0) goto L_0x008f
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r15 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0326 }
            if (r15 == 0) goto L_0x008f
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r15 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0326 }
            java.lang.Object r11 = r15.get(r11)     // Catch:{ all -> 0x0326 }
            mapsdkvi.com.gdi.bgl.android.java.a r11 = (mapsdkvi.com.gdi.bgl.android.java.C3410a) r11     // Catch:{ all -> 0x0326 }
            if (r11 == 0) goto L_0x008f
            android.graphics.Typeface r11 = r11.f7931a     // Catch:{ all -> 0x0326 }
            r10.setTypeface(r11)     // Catch:{ all -> 0x0326 }
        L_0x008f:
            r10.setTextSize(r14)     // Catch:{ all -> 0x0326 }
            r11 = 92
            int r14 = r0.indexOf(r11, r12)     // Catch:{ all -> 0x0326 }
            r15 = -1
            r17 = 3
            r18 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r19 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            r11 = 4
            r21 = r9
            if (r14 != r15) goto L_0x0174
            android.graphics.Paint$FontMetrics r6 = r10.getFontMetrics()     // Catch:{ all -> 0x0326 }
            int r14 = r26.length()     // Catch:{ all -> 0x0326 }
            float r14 = android.text.Layout.getDesiredWidth(r0, r12, r14, r10)     // Catch:{ all -> 0x0326 }
            double r14 = (double) r14
            java.lang.Double.isNaN(r14)
            double r14 = r14 + r19
            int r14 = (int) r14
            float r15 = r6.descent     // Catch:{ all -> 0x0326 }
            float r8 = r6.ascent     // Catch:{ all -> 0x0326 }
            float r15 = r15 - r8
            double r8 = (double) r15     // Catch:{ all -> 0x0326 }
            double r8 = java.lang.Math.ceil(r8)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            r1[r12] = r14     // Catch:{ all -> 0x0326 }
            r1[r13] = r8     // Catch:{ all -> 0x0326 }
            int r9 = r1.length     // Catch:{ all -> 0x0326 }
            if (r9 != r11) goto L_0x00fd
            double r13 = (double) r14     // Catch:{ all -> 0x0326 }
            double r13 = java.lang.Math.log(r13)     // Catch:{ all -> 0x0326 }
            r19 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r22 = java.lang.Math.log(r19)     // Catch:{ all -> 0x0326 }
            double r13 = r13 / r22
            double r13 = java.lang.Math.ceil(r13)     // Catch:{ all -> 0x0326 }
            int r9 = (int) r13     // Catch:{ all -> 0x0326 }
            double r13 = (double) r9     // Catch:{ all -> 0x0326 }
            r11 = r19
            double r13 = java.lang.Math.pow(r11, r13)     // Catch:{ all -> 0x0326 }
            int r13 = (int) r13     // Catch:{ all -> 0x0326 }
            r24 = r10
            double r9 = (double) r8     // Catch:{ all -> 0x0326 }
            double r8 = java.lang.Math.log(r9)     // Catch:{ all -> 0x0326 }
            double r19 = java.lang.Math.log(r11)     // Catch:{ all -> 0x0326 }
            double r8 = r8 / r19
            double r8 = java.lang.Math.ceil(r8)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            double r8 = (double) r8     // Catch:{ all -> 0x0326 }
            double r8 = java.lang.Math.pow(r11, r8)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            r12 = r13
            goto L_0x0100
        L_0x00fd:
            r24 = r10
            r12 = r14
        L_0x0100:
            if (r12 != 0) goto L_0x0107
            if (r8 == 0) goto L_0x0105
            goto L_0x0107
        L_0x0105:
            r8 = 0
            r12 = 0
        L_0x0107:
            int r9 = r1.length     // Catch:{ all -> 0x0326 }
            r10 = 4
            if (r9 != r10) goto L_0x0110
            r9 = 2
            r1[r9] = r12     // Catch:{ all -> 0x0326 }
            r1[r17] = r8     // Catch:{ all -> 0x0326 }
        L_0x0110:
            if (r12 <= 0) goto L_0x0129
            if (r8 <= 0) goto L_0x0129
            android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0326 }
            android.graphics.Bitmap r1 = android.graphics.Bitmap.createBitmap(r12, r8, r1)     // Catch:{ all -> 0x0326 }
            if (r1 != 0) goto L_0x0121
            r9 = 0
            int[] r0 = new int[r9]     // Catch:{ all -> 0x0326 }
            monitor-exit(r7)
            return r0
        L_0x0121:
            r10 = r21
            r10.setBitmap(r1)     // Catch:{ all -> 0x0326 }
            r16 = r1
            goto L_0x012d
        L_0x0129:
            r10 = r21
            r16 = 0
        L_0x012d:
            r1 = r4 & r18
            if (r1 != 0) goto L_0x0138
            r1 = 16777215(0xffffff, float:2.3509886E-38)
            r10.drawColor(r1)     // Catch:{ all -> 0x0326 }
            goto L_0x013b
        L_0x0138:
            r10.drawColor(r4)     // Catch:{ all -> 0x0326 }
        L_0x013b:
            if (r5 == 0) goto L_0x015e
            float r1 = (float) r5     // Catch:{ all -> 0x0326 }
            r11 = r24
            r11.setStrokeWidth(r1)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Cap r1 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x0326 }
            r11.setStrokeCap(r1)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Join r1 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x0326 }
            r11.setStrokeJoin(r1)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0326 }
            r11.setStyle(r1)     // Catch:{ all -> 0x0326 }
            r11.setColor(r3)     // Catch:{ all -> 0x0326 }
            float r1 = r6.ascent     // Catch:{ all -> 0x0326 }
            r3 = 0
            float r15 = r3 - r1
            r10.drawText(r0, r3, r15, r11)     // Catch:{ all -> 0x0326 }
            goto L_0x0160
        L_0x015e:
            r11 = r24
        L_0x0160:
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0326 }
            r11.setStyle(r1)     // Catch:{ all -> 0x0326 }
            r11.setColor(r2)     // Catch:{ all -> 0x0326 }
            float r1 = r6.ascent     // Catch:{ all -> 0x0326 }
            r2 = 0
            float r15 = r2 - r1
            r10.drawText(r0, r2, r15, r11)     // Catch:{ all -> 0x0326 }
        L_0x0170:
            r0 = r16
            goto L_0x030c
        L_0x0174:
            r11 = r10
            r10 = r21
            int r8 = r14 + 1
            r9 = 0
            java.lang.String r12 = r0.substring(r9, r14)     // Catch:{ all -> 0x0326 }
            float r12 = r11.measureText(r12)     // Catch:{ all -> 0x0326 }
            int r12 = (int) r12     // Catch:{ all -> 0x0326 }
            r9 = 92
            r14 = 2
        L_0x0186:
            int r15 = r0.indexOf(r9, r8)     // Catch:{ all -> 0x0326 }
            if (r15 <= 0) goto L_0x019f
            java.lang.String r8 = r0.substring(r8, r15)     // Catch:{ all -> 0x0326 }
            float r8 = r11.measureText(r8)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            if (r8 <= r12) goto L_0x0198
            r12 = r8
        L_0x0198:
            int r8 = r15 + 1
            int r14 = r14 + 1
            r9 = 92
            goto L_0x0186
        L_0x019f:
            int r9 = r26.length()     // Catch:{ all -> 0x0326 }
            if (r8 == r9) goto L_0x01bb
            int r9 = r26.length()     // Catch:{ all -> 0x0326 }
            java.lang.String r8 = r0.substring(r8, r9)     // Catch:{ all -> 0x0326 }
            float r8 = android.text.Layout.getDesiredWidth(r8, r11)     // Catch:{ all -> 0x0326 }
            double r8 = (double) r8
            java.lang.Double.isNaN(r8)
            double r8 = r8 + r19
            int r8 = (int) r8
            if (r8 <= r12) goto L_0x01bb
            r12 = r8
        L_0x01bb:
            android.graphics.Paint$FontMetrics r8 = r11.getFontMetrics()     // Catch:{ all -> 0x0326 }
            float r9 = r8.descent     // Catch:{ all -> 0x0326 }
            float r15 = r8.ascent     // Catch:{ all -> 0x0326 }
            float r9 = r9 - r15
            r15 = r14
            double r13 = (double) r9     // Catch:{ all -> 0x0326 }
            double r13 = java.lang.Math.ceil(r13)     // Catch:{ all -> 0x0326 }
            int r13 = (int) r13     // Catch:{ all -> 0x0326 }
            int r14 = r13 * r15
            r9 = 0
            r1[r9] = r12     // Catch:{ all -> 0x0326 }
            r15 = 1
            r1[r15] = r14     // Catch:{ all -> 0x0326 }
            int r15 = r1.length     // Catch:{ all -> 0x0326 }
            r9 = 4
            if (r15 != r9) goto L_0x0214
            r20 = r8
            double r8 = (double) r12     // Catch:{ all -> 0x0326 }
            double r8 = java.lang.Math.log(r8)     // Catch:{ all -> 0x0326 }
            r24 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r22 = java.lang.Math.log(r24)     // Catch:{ all -> 0x0326 }
            double r8 = r8 / r22
            double r8 = java.lang.Math.ceil(r8)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            double r8 = (double) r8     // Catch:{ all -> 0x0326 }
            r21 = r13
            r12 = r24
            double r8 = java.lang.Math.pow(r12, r8)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            double r12 = (double) r14     // Catch:{ all -> 0x0326 }
            double r12 = java.lang.Math.log(r12)     // Catch:{ all -> 0x0326 }
            r24 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r22 = java.lang.Math.log(r24)     // Catch:{ all -> 0x0326 }
            double r12 = r12 / r22
            double r12 = java.lang.Math.ceil(r12)     // Catch:{ all -> 0x0326 }
            int r9 = (int) r12     // Catch:{ all -> 0x0326 }
            double r12 = (double) r9     // Catch:{ all -> 0x0326 }
            r22 = r8
            r8 = r24
            double r8 = java.lang.Math.pow(r8, r12)     // Catch:{ all -> 0x0326 }
            int r8 = (int) r8     // Catch:{ all -> 0x0326 }
            r12 = r22
            goto L_0x0219
        L_0x0214:
            r20 = r8
            r21 = r13
            r8 = r14
        L_0x0219:
            if (r12 != 0) goto L_0x0220
            if (r8 == 0) goto L_0x021e
            goto L_0x0220
        L_0x021e:
            r8 = 0
            r12 = 0
        L_0x0220:
            int r9 = r1.length     // Catch:{ all -> 0x0326 }
            r13 = 4
            if (r9 != r13) goto L_0x0229
            r9 = 2
            r1[r9] = r12     // Catch:{ all -> 0x0326 }
            r1[r17] = r8     // Catch:{ all -> 0x0326 }
        L_0x0229:
            if (r12 <= 0) goto L_0x0240
            if (r8 <= 0) goto L_0x0240
            android.graphics.Bitmap$Config r9 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x0326 }
            android.graphics.Bitmap r13 = android.graphics.Bitmap.createBitmap(r12, r8, r9)     // Catch:{ all -> 0x0326 }
            if (r13 != 0) goto L_0x023a
            r9 = 0
            int[] r0 = new int[r9]     // Catch:{ all -> 0x0326 }
            monitor-exit(r7)
            return r0
        L_0x023a:
            r10.setBitmap(r13)     // Catch:{ all -> 0x0326 }
            r16 = r13
            goto L_0x0242
        L_0x0240:
            r16 = 0
        L_0x0242:
            r13 = r4 & r18
            if (r13 != 0) goto L_0x024d
            r13 = 16777215(0xffffff, float:2.3509886E-38)
            r10.drawColor(r13)     // Catch:{ all -> 0x0326 }
            goto L_0x0250
        L_0x024d:
            r10.drawColor(r4)     // Catch:{ all -> 0x0326 }
        L_0x0250:
            android.graphics.Paint$Align r4 = getTextAlignedType(r34)     // Catch:{ all -> 0x0326 }
            r11.setTextAlign(r4)     // Catch:{ all -> 0x0326 }
            r4 = 1
            if (r6 != r4) goto L_0x025d
            r1 = 0
            r6 = 0
            goto L_0x0268
        L_0x025d:
            r4 = 2
            if (r6 != r4) goto L_0x0264
            r6 = 0
            r1 = r1[r6]     // Catch:{ all -> 0x0326 }
            goto L_0x0268
        L_0x0264:
            r6 = 0
            r1 = r1[r6]     // Catch:{ all -> 0x0326 }
            int r1 = r1 / r4
        L_0x0268:
            r4 = 0
        L_0x0269:
            r9 = 92
            int r13 = r0.indexOf(r9, r6)     // Catch:{ all -> 0x0326 }
            if (r13 <= 0) goto L_0x02bf
            java.lang.String r6 = r0.substring(r6, r13)     // Catch:{ all -> 0x0326 }
            r11.measureText(r6)     // Catch:{ all -> 0x0326 }
            int r13 = r13 + 1
            if (r5 == 0) goto L_0x02a1
            float r14 = (float) r5     // Catch:{ all -> 0x0326 }
            r11.setStrokeWidth(r14)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Cap r14 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x0326 }
            r11.setStrokeCap(r14)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Join r14 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x0326 }
            r11.setStrokeJoin(r14)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Style r14 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0326 }
            r11.setStyle(r14)     // Catch:{ all -> 0x0326 }
            r11.setColor(r3)     // Catch:{ all -> 0x0326 }
            float r14 = (float) r1     // Catch:{ all -> 0x0326 }
            int r15 = r4 * r21
            float r15 = (float) r15     // Catch:{ all -> 0x0326 }
            r27 = r8
            r9 = r20
            float r8 = r9.ascent     // Catch:{ all -> 0x0326 }
            float r15 = r15 - r8
            r10.drawText(r6, r14, r15, r11)     // Catch:{ all -> 0x0326 }
            goto L_0x02a5
        L_0x02a1:
            r27 = r8
            r9 = r20
        L_0x02a5:
            android.graphics.Paint$Style r8 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0326 }
            r11.setStyle(r8)     // Catch:{ all -> 0x0326 }
            r11.setColor(r2)     // Catch:{ all -> 0x0326 }
            float r8 = (float) r1     // Catch:{ all -> 0x0326 }
            int r14 = r4 * r21
            float r14 = (float) r14     // Catch:{ all -> 0x0326 }
            float r15 = r9.ascent     // Catch:{ all -> 0x0326 }
            float r14 = r14 - r15
            r10.drawText(r6, r8, r14, r11)     // Catch:{ all -> 0x0326 }
            int r4 = r4 + 1
            r8 = r27
            r20 = r9
            r6 = r13
            goto L_0x0269
        L_0x02bf:
            r27 = r8
            r9 = r20
            int r8 = r26.length()     // Catch:{ all -> 0x0326 }
            if (r6 == r8) goto L_0x0308
            int r8 = r26.length()     // Catch:{ all -> 0x0326 }
            java.lang.String r0 = r0.substring(r6, r8)     // Catch:{ all -> 0x0326 }
            android.text.Layout.getDesiredWidth(r0, r11)     // Catch:{ all -> 0x0326 }
            if (r5 == 0) goto L_0x02f6
            float r5 = (float) r5     // Catch:{ all -> 0x0326 }
            r11.setStrokeWidth(r5)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Cap r5 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x0326 }
            r11.setStrokeCap(r5)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Join r5 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x0326 }
            r11.setStrokeJoin(r5)     // Catch:{ all -> 0x0326 }
            android.graphics.Paint$Style r5 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x0326 }
            r11.setStyle(r5)     // Catch:{ all -> 0x0326 }
            r11.setColor(r3)     // Catch:{ all -> 0x0326 }
            float r3 = (float) r1     // Catch:{ all -> 0x0326 }
            int r13 = r4 * r21
            float r5 = (float) r13     // Catch:{ all -> 0x0326 }
            float r6 = r9.ascent     // Catch:{ all -> 0x0326 }
            float r5 = r5 - r6
            r10.drawText(r0, r3, r5, r11)     // Catch:{ all -> 0x0326 }
        L_0x02f6:
            android.graphics.Paint$Style r3 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x0326 }
            r11.setStyle(r3)     // Catch:{ all -> 0x0326 }
            r11.setColor(r2)     // Catch:{ all -> 0x0326 }
            float r1 = (float) r1     // Catch:{ all -> 0x0326 }
            int r4 = r4 * r21
            float r2 = (float) r4     // Catch:{ all -> 0x0326 }
            float r3 = r9.ascent     // Catch:{ all -> 0x0326 }
            float r2 = r2 - r3
            r10.drawText(r0, r1, r2, r11)     // Catch:{ all -> 0x0326 }
        L_0x0308:
            r8 = r27
            goto L_0x0170
        L_0x030c:
            int r12 = r12 * r8
            int[] r1 = new int[r12]     // Catch:{ all -> 0x0326 }
            if (r0 == 0) goto L_0x0319
            java.nio.IntBuffer r2 = java.nio.IntBuffer.wrap(r1)     // Catch:{ all -> 0x0326 }
            r0.copyPixelsToBuffer(r2)     // Catch:{ all -> 0x0326 }
        L_0x0319:
            if (r0 == 0) goto L_0x0324
            boolean r2 = r0.isRecycled()     // Catch:{ all -> 0x0326 }
            if (r2 != 0) goto L_0x0324
            r0.recycle()     // Catch:{ all -> 0x0326 }
        L_0x0324:
            monitor-exit(r7)
            return r1
        L_0x0326:
            r0 = move-exception
            monitor-exit(r7)
            goto L_0x032a
        L_0x0329:
            throw r0
        L_0x032a:
            goto L_0x0329
        */
        throw new UnsupportedOperationException("Method not decompiled: mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.drawText(java.lang.String, int, int, int[], int, int, int, int, int):int[]");
    }

    public static Bitmap drawTextAlpha(String str, int i, int i2, int i3) {
        String str2 = str;
        int i4 = i3;
        Canvas canvas = new Canvas();
        TextPaint textPaint = new TextPaint();
        String phoneType = SysOSUtil.getPhoneType();
        int i5 = 0;
        int i6 = (phoneType == null || !phoneType.equals("vivo X3L")) ? i2 : 0;
        textPaint.reset();
        textPaint.setSubpixelText(false);
        textPaint.setAntiAlias(false);
        textPaint.setTextSize((float) i);
        int i7 = 2;
        textPaint.setTypeface(i6 != 1 ? i6 != 2 ? Typeface.create(Typeface.DEFAULT, 0) : Typeface.create(Typeface.DEFAULT, 2) : Typeface.create(Typeface.DEFAULT, 1));
        float f = (((float) i4) * 1.3f) + 0.5f;
        int i8 = 92;
        int indexOf = str2.indexOf(92, 0);
        Bitmap bitmap = null;
        if (indexOf == -1) {
            Paint.FontMetrics fontMetrics = textPaint.getFontMetrics();
            int desiredWidth = (int) (Layout.getDesiredWidth(str2, 0, str.length(), textPaint) + f);
            int ceil = (int) Math.ceil((double) (fontMetrics.descent - fontMetrics.ascent));
            if (desiredWidth > 0 && ceil > 0) {
                bitmap = Bitmap.createBitmap(desiredWidth, ceil, Bitmap.Config.ALPHA_8);
                if (bitmap == null) {
                    return bitmap;
                }
                bitmap.eraseColor(0);
                canvas.setBitmap(bitmap);
            }
            textPaint.setStyle(Paint.Style.FILL);
            canvas.drawText(str2, f * 0.5f, 0.0f - fontMetrics.ascent, textPaint);
        } else {
            int i9 = indexOf + 1;
            double desiredWidth2 = (double) Layout.getDesiredWidth(str2.substring(0, indexOf), textPaint);
            Double.isNaN(desiredWidth2);
            int i10 = (int) (desiredWidth2 + 0.5d);
            while (true) {
                int indexOf2 = str2.indexOf(i8, i9);
                if (indexOf2 <= 0) {
                    break;
                }
                double desiredWidth3 = (double) Layout.getDesiredWidth(str2.substring(i9, indexOf2), textPaint);
                Double.isNaN(desiredWidth3);
                int i11 = (int) (desiredWidth3 + 0.5d);
                if (i11 > i10) {
                    i10 = i11;
                }
                i9 = indexOf2 + 1;
                i7++;
                i8 = 92;
            }
            if (i9 != str.length()) {
                double desiredWidth4 = (double) Layout.getDesiredWidth(str2.substring(i9, str.length()), textPaint);
                Double.isNaN(desiredWidth4);
                int i12 = (int) (desiredWidth4 + 0.5d);
                if (i12 > i10) {
                    i10 = i12;
                }
            }
            Paint.FontMetrics fontMetrics2 = textPaint.getFontMetrics();
            int ceil2 = (int) Math.ceil((double) (fontMetrics2.descent - fontMetrics2.ascent));
            int i13 = i10 + i4;
            int i14 = i7 * ceil2;
            if (i13 > 0 && i14 > 0) {
                bitmap = Bitmap.createBitmap(i13, i14, Bitmap.Config.ALPHA_8);
                if (bitmap == null) {
                    return bitmap;
                }
                bitmap.eraseColor(0);
                canvas.setBitmap(bitmap);
            }
            textPaint.setTextAlign(getTextAlignedType(3));
            float f2 = ((float) i13) - (f * 0.5f);
            int i15 = 0;
            while (true) {
                int indexOf3 = str2.indexOf(92, i5);
                if (indexOf3 <= 0) {
                    break;
                }
                String substring = str2.substring(i5, indexOf3);
                Layout.getDesiredWidth(substring, textPaint);
                textPaint.setStyle(Paint.Style.FILL);
                canvas.drawText(substring, f2, ((float) (i15 * ceil2)) - fontMetrics2.ascent, textPaint);
                i15++;
                i5 = indexOf3 + 1;
            }
            if (i5 != str.length()) {
                String substring2 = str2.substring(i5, str.length());
                Layout.getDesiredWidth(substring2, textPaint);
                textPaint.setStyle(Paint.Style.FILL);
                canvas.drawText(substring2, f2, ((float) (i15 * ceil2)) - fontMetrics2.ascent, textPaint);
            }
        }
        return bitmap;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void}
     arg types: [int, int, int, int]
     candidates:
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, long):void}
      ClspMth{android.graphics.Paint.setShadowLayer(float, float, float, int):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x0231, code lost:
        r1 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0232, code lost:
        r4 = r0.indexOf(92, r3);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x0238, code lost:
        if (r4 <= 0) goto L_0x0285;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x023a, code lost:
        r3 = r0.substring(r3, r4);
        android.text.Layout.getDesiredWidth(r3, r10);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:104:0x0243, code lost:
        if (r5 == 0) goto L_0x0268;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0245, code lost:
        r10.setStrokeWidth((float) r5);
        r10.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        r10.setStrokeJoin(android.graphics.Paint.Join.ROUND);
        r10.setStyle(android.graphics.Paint.Style.STROKE);
        r10.setColor(r30);
        r9.drawText(r3, (float) r12, ((float) (r1 * r15)) - r13.ascent, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:107:0x026a, code lost:
        r10.setStyle(android.graphics.Paint.Style.FILL);
        r10.setColor(r29);
        r9.drawText(r3, (float) r12, ((float) (r1 * r15)) - r13.ascent, r10);
        r1 = r1 + 1;
        r3 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0285, code lost:
        r2 = r29;
        r6 = r30;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x028d, code lost:
        if (r3 == r25.length()) goto L_0x02ce;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:0x028f, code lost:
        r0 = r0.substring(r3, r25.length());
        android.text.Layout.getDesiredWidth(r0, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:0x029a, code lost:
        if (r5 == 0) goto L_0x02bc;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:0x029c, code lost:
        r10.setStrokeWidth((float) r5);
        r10.setStrokeCap(android.graphics.Paint.Cap.ROUND);
        r10.setStrokeJoin(android.graphics.Paint.Join.ROUND);
        r10.setStyle(android.graphics.Paint.Style.STROKE);
        r10.setColor(r6);
        r9.drawText(r0, (float) r12, ((float) (r1 * r15)) - r13.ascent, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02bc, code lost:
        r10.setStyle(android.graphics.Paint.Style.FILL);
        r10.setColor(r2);
        r9.drawText(r0, (float) r12, ((float) (r1 * r15)) - r13.ascent, r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x02cf, code lost:
        return r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x017e, code lost:
        if (r11 == r25.length()) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x018c, code lost:
        r11 = (double) android.text.Layout.getDesiredWidth(r0.substring(r11, r25.length()), r10);
        java.lang.Double.isNaN(r11);
        r11 = (int) (r11 + 0.5d);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0193, code lost:
        if (r11 <= r8) goto L_0x0196;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0195, code lost:
        r8 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:?, code lost:
        r11 = r10.getFontMetrics();
        r12 = (int) java.lang.Math.ceil((double) (r11.descent - r11.ascent));
        r14 = r14 * r12;
        r1[0] = r8;
        r1[1] = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x01af, code lost:
        if (r1.length != 4) goto L_0x01e5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x01b1, code lost:
        r13 = r11;
        r15 = r12;
        r8 = (int) java.lang.Math.pow(2.0d, (double) ((int) java.lang.Math.ceil(java.lang.Math.log((double) r8) / java.lang.Math.log(2.0d))));
        r14 = (int) java.lang.Math.pow(2.0d, (double) ((int) java.lang.Math.ceil(java.lang.Math.log((double) r14) / java.lang.Math.log(2.0d))));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x01e5, code lost:
        r13 = r11;
        r15 = r12;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x01e7, code lost:
        r12 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x01e8, code lost:
        if (r12 != 0) goto L_0x01ef;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x01ea, code lost:
        if (r14 == 0) goto L_0x01ed;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x01ed, code lost:
        r12 = 0;
        r14 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01f1, code lost:
        if (r1.length != 4) goto L_0x01f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x01f3, code lost:
        r1[2] = r12;
        r1[3] = r14;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x01f8, code lost:
        if (r12 <= 0) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x01fa, code lost:
        if (r14 <= 0) goto L_0x020a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01fc, code lost:
        r8 = android.graphics.Bitmap.createBitmap(r12, r14, android.graphics.Bitmap.Config.ARGB_8888);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0202, code lost:
        if (r8 != null) goto L_0x0206;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0205, code lost:
        return r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r9.setBitmap(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x020a, code lost:
        r8 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x020d, code lost:
        if ((r4 & android.support.v4.view.ViewCompat.MEASURED_STATE_MASK) != 0) goto L_0x0216;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x020f, code lost:
        r9.drawColor((int) android.support.v4.view.ViewCompat.MEASURED_SIZE_MASK);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0216, code lost:
        r9.drawColor(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0219, code lost:
        r10.setTextAlign(getTextAlignedType(r33));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:94:0x0221, code lost:
        if (r6 != 1) goto L_0x0226;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:95:0x0223, code lost:
        r3 = 0;
        r12 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x0226, code lost:
        r3 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0228, code lost:
        if (r6 != 2) goto L_0x022d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x022a, code lost:
        r12 = r1[0];
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x022d, code lost:
        r12 = r1[0] / 2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized android.graphics.Bitmap drawTextExt(java.lang.String r25, int r26, int r27, int[] r28, int r29, int r30, int r31, int r32, int r33) {
        /*
            r0 = r25
            r1 = r28
            r2 = r29
            r3 = r30
            r4 = r31
            r5 = r32
            r6 = r33
            java.lang.Class<mapsdkvi.com.gdi.bgl.android.java.EnvDrawText> r7 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.class
            monitor-enter(r7)
            android.graphics.Canvas r9 = new android.graphics.Canvas     // Catch:{ all -> 0x02d0 }
            r9.<init>()     // Catch:{ all -> 0x02d0 }
            android.text.TextPaint r10 = new android.text.TextPaint     // Catch:{ all -> 0x02d0 }
            r10.<init>()     // Catch:{ all -> 0x02d0 }
            java.lang.String r11 = com.baidu.mapapi.common.SysOSUtil.getPhoneType()     // Catch:{ all -> 0x02d0 }
            r12 = 0
            if (r11 == 0) goto L_0x002c
            java.lang.String r13 = "vivo X3L"
            boolean r11 = r11.equals(r13)     // Catch:{ all -> 0x02d0 }
            if (r11 == 0) goto L_0x002c
            r11 = 0
            goto L_0x002e
        L_0x002c:
            r11 = r27
        L_0x002e:
            r10.reset()     // Catch:{ all -> 0x02d0 }
            r13 = 1
            r10.setSubpixelText(r13)     // Catch:{ all -> 0x02d0 }
            r10.setAntiAlias(r13)     // Catch:{ all -> 0x02d0 }
            r14 = r26
            float r14 = (float) r14     // Catch:{ all -> 0x02d0 }
            r10.setTextSize(r14)     // Catch:{ all -> 0x02d0 }
            r14 = 0
            r10.setShadowLayer(r14, r14, r14, r12)     // Catch:{ all -> 0x02d0 }
            r15 = 2
            if (r11 == r13) goto L_0x0058
            if (r11 == r15) goto L_0x0051
            android.graphics.Typeface r11 = android.graphics.Typeface.DEFAULT     // Catch:{ all -> 0x02d0 }
            android.graphics.Typeface r11 = android.graphics.Typeface.create(r11, r12)     // Catch:{ all -> 0x02d0 }
        L_0x004d:
            r10.setTypeface(r11)     // Catch:{ all -> 0x02d0 }
            goto L_0x005f
        L_0x0051:
            android.graphics.Typeface r11 = android.graphics.Typeface.DEFAULT     // Catch:{ all -> 0x02d0 }
            android.graphics.Typeface r11 = android.graphics.Typeface.create(r11, r15)     // Catch:{ all -> 0x02d0 }
            goto L_0x004d
        L_0x0058:
            android.graphics.Typeface r11 = android.graphics.Typeface.DEFAULT     // Catch:{ all -> 0x02d0 }
            android.graphics.Typeface r11 = android.graphics.Typeface.create(r11, r13)     // Catch:{ all -> 0x02d0 }
            goto L_0x004d
        L_0x005f:
            if (r5 == 0) goto L_0x0074
            float r11 = (float) r5     // Catch:{ all -> 0x02d0 }
            r10.setStrokeWidth(r11)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Cap r11 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeCap(r11)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Join r11 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeJoin(r11)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Style r11 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r11)     // Catch:{ all -> 0x02d0 }
        L_0x0074:
            r11 = 92
            int r8 = r0.indexOf(r11, r12)     // Catch:{ all -> 0x02d0 }
            r11 = -1
            r16 = 3
            r17 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r18 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            if (r8 != r11) goto L_0x0141
            android.graphics.Paint$FontMetrics r6 = r10.getFontMetrics()     // Catch:{ all -> 0x02d0 }
            int r8 = r25.length()     // Catch:{ all -> 0x02d0 }
            float r8 = android.text.Layout.getDesiredWidth(r0, r12, r8, r10)     // Catch:{ all -> 0x02d0 }
            double r14 = (double) r8
            java.lang.Double.isNaN(r14)
            double r14 = r14 + r18
            int r8 = (int) r14
            float r11 = r6.descent     // Catch:{ all -> 0x02d0 }
            float r14 = r6.ascent     // Catch:{ all -> 0x02d0 }
            float r11 = r11 - r14
            double r14 = (double) r11     // Catch:{ all -> 0x02d0 }
            double r14 = java.lang.Math.ceil(r14)     // Catch:{ all -> 0x02d0 }
            int r11 = (int) r14     // Catch:{ all -> 0x02d0 }
            r1[r12] = r8     // Catch:{ all -> 0x02d0 }
            r1[r13] = r11     // Catch:{ all -> 0x02d0 }
            int r13 = r1.length     // Catch:{ all -> 0x02d0 }
            r14 = 4
            if (r13 != r14) goto L_0x00da
            double r13 = (double) r8     // Catch:{ all -> 0x02d0 }
            double r13 = java.lang.Math.log(r13)     // Catch:{ all -> 0x02d0 }
            r18 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r20 = java.lang.Math.log(r18)     // Catch:{ all -> 0x02d0 }
            double r13 = r13 / r20
            double r13 = java.lang.Math.ceil(r13)     // Catch:{ all -> 0x02d0 }
            int r8 = (int) r13     // Catch:{ all -> 0x02d0 }
            double r13 = (double) r8     // Catch:{ all -> 0x02d0 }
            r2 = r18
            double r13 = java.lang.Math.pow(r2, r13)     // Catch:{ all -> 0x02d0 }
            int r8 = (int) r13     // Catch:{ all -> 0x02d0 }
            double r13 = (double) r11     // Catch:{ all -> 0x02d0 }
            double r13 = java.lang.Math.log(r13)     // Catch:{ all -> 0x02d0 }
            double r18 = java.lang.Math.log(r2)     // Catch:{ all -> 0x02d0 }
            double r13 = r13 / r18
            double r13 = java.lang.Math.ceil(r13)     // Catch:{ all -> 0x02d0 }
            int r11 = (int) r13     // Catch:{ all -> 0x02d0 }
            double r13 = (double) r11     // Catch:{ all -> 0x02d0 }
            double r2 = java.lang.Math.pow(r2, r13)     // Catch:{ all -> 0x02d0 }
            int r2 = (int) r2     // Catch:{ all -> 0x02d0 }
            r11 = r2
        L_0x00da:
            if (r8 != 0) goto L_0x00e1
            if (r11 == 0) goto L_0x00df
            goto L_0x00e1
        L_0x00df:
            r8 = 0
            r11 = 0
        L_0x00e1:
            int r2 = r1.length     // Catch:{ all -> 0x02d0 }
            r3 = 4
            if (r2 != r3) goto L_0x00ea
            r2 = 2
            r1[r2] = r8     // Catch:{ all -> 0x02d0 }
            r1[r16] = r11     // Catch:{ all -> 0x02d0 }
        L_0x00ea:
            if (r8 <= 0) goto L_0x00fc
            if (r11 <= 0) goto L_0x00fc
            android.graphics.Bitmap$Config r1 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x02d0 }
            android.graphics.Bitmap r8 = android.graphics.Bitmap.createBitmap(r8, r11, r1)     // Catch:{ all -> 0x02d0 }
            if (r8 != 0) goto L_0x00f8
            monitor-exit(r7)
            return r8
        L_0x00f8:
            r9.setBitmap(r8)     // Catch:{ all -> 0x02d0 }
            goto L_0x00fd
        L_0x00fc:
            r8 = 0
        L_0x00fd:
            r1 = r4 & r17
            if (r1 != 0) goto L_0x0108
            r1 = 16777215(0xffffff, float:2.3509886E-38)
            r9.drawColor(r1)     // Catch:{ all -> 0x02d0 }
            goto L_0x010b
        L_0x0108:
            r9.drawColor(r4)     // Catch:{ all -> 0x02d0 }
        L_0x010b:
            if (r5 == 0) goto L_0x012d
            float r1 = (float) r5     // Catch:{ all -> 0x02d0 }
            r10.setStrokeWidth(r1)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Cap r1 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeCap(r1)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Join r1 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeJoin(r1)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r1)     // Catch:{ all -> 0x02d0 }
            r2 = r30
            r10.setColor(r2)     // Catch:{ all -> 0x02d0 }
            float r1 = r6.ascent     // Catch:{ all -> 0x02d0 }
            r2 = 0
            float r14 = r2 - r1
            r9.drawText(r0, r2, r14, r10)     // Catch:{ all -> 0x02d0 }
        L_0x012d:
            android.graphics.Paint$Style r1 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r1)     // Catch:{ all -> 0x02d0 }
            r3 = r29
            r10.setColor(r3)     // Catch:{ all -> 0x02d0 }
            float r1 = r6.ascent     // Catch:{ all -> 0x02d0 }
            r2 = 0
            float r14 = r2 - r1
            r9.drawText(r0, r2, r14, r10)     // Catch:{ all -> 0x02d0 }
            goto L_0x02ce
        L_0x0141:
            r24 = r3
            r3 = r2
            r2 = r24
            int r11 = r8 + 1
            java.lang.String r8 = r0.substring(r12, r8)     // Catch:{ all -> 0x02d0 }
            float r8 = android.text.Layout.getDesiredWidth(r8, r10)     // Catch:{ all -> 0x02d0 }
            double r14 = (double) r8
            java.lang.Double.isNaN(r14)
            double r14 = r14 + r18
            int r8 = (int) r14
            r14 = 2
        L_0x0158:
            r15 = 92
            int r13 = r0.indexOf(r15, r11)     // Catch:{ all -> 0x02d0 }
            if (r13 <= 0) goto L_0x017a
            java.lang.String r11 = r0.substring(r11, r13)     // Catch:{ all -> 0x02d0 }
            float r11 = android.text.Layout.getDesiredWidth(r11, r10)     // Catch:{ all -> 0x02d0 }
            r22 = r13
            double r12 = (double) r11
            java.lang.Double.isNaN(r12)
            double r12 = r12 + r18
            int r11 = (int) r12
            if (r11 <= r8) goto L_0x0174
            r8 = r11
        L_0x0174:
            int r11 = r22 + 1
            int r14 = r14 + 1
            r12 = 0
            goto L_0x0158
        L_0x017a:
            int r12 = r25.length()     // Catch:{ all -> 0x02d0 }
            if (r11 == r12) goto L_0x0196
            int r12 = r25.length()     // Catch:{ all -> 0x02d0 }
            java.lang.String r11 = r0.substring(r11, r12)     // Catch:{ all -> 0x02d0 }
            float r11 = android.text.Layout.getDesiredWidth(r11, r10)     // Catch:{ all -> 0x02d0 }
            double r11 = (double) r11
            java.lang.Double.isNaN(r11)
            double r11 = r11 + r18
            int r11 = (int) r11
            if (r11 <= r8) goto L_0x0196
            r8 = r11
        L_0x0196:
            android.graphics.Paint$FontMetrics r11 = r10.getFontMetrics()     // Catch:{ all -> 0x02d0 }
            float r12 = r11.descent     // Catch:{ all -> 0x02d0 }
            float r13 = r11.ascent     // Catch:{ all -> 0x02d0 }
            float r12 = r12 - r13
            double r12 = (double) r12     // Catch:{ all -> 0x02d0 }
            double r12 = java.lang.Math.ceil(r12)     // Catch:{ all -> 0x02d0 }
            int r12 = (int) r12     // Catch:{ all -> 0x02d0 }
            int r14 = r14 * r12
            r13 = 0
            r1[r13] = r8     // Catch:{ all -> 0x02d0 }
            r13 = 1
            r1[r13] = r14     // Catch:{ all -> 0x02d0 }
            int r13 = r1.length     // Catch:{ all -> 0x02d0 }
            r15 = 4
            if (r13 != r15) goto L_0x01e5
            r13 = r11
            r15 = r12
            double r11 = (double) r8     // Catch:{ all -> 0x02d0 }
            double r11 = java.lang.Math.log(r11)     // Catch:{ all -> 0x02d0 }
            r22 = 4611686018427387904(0x4000000000000000, double:2.0)
            double r20 = java.lang.Math.log(r22)     // Catch:{ all -> 0x02d0 }
            double r11 = r11 / r20
            double r11 = java.lang.Math.ceil(r11)     // Catch:{ all -> 0x02d0 }
            int r8 = (int) r11     // Catch:{ all -> 0x02d0 }
            double r11 = (double) r8     // Catch:{ all -> 0x02d0 }
            r2 = r22
            double r11 = java.lang.Math.pow(r2, r11)     // Catch:{ all -> 0x02d0 }
            int r8 = (int) r11     // Catch:{ all -> 0x02d0 }
            double r11 = (double) r14     // Catch:{ all -> 0x02d0 }
            double r11 = java.lang.Math.log(r11)     // Catch:{ all -> 0x02d0 }
            double r20 = java.lang.Math.log(r2)     // Catch:{ all -> 0x02d0 }
            double r11 = r11 / r20
            double r11 = java.lang.Math.ceil(r11)     // Catch:{ all -> 0x02d0 }
            int r11 = (int) r11     // Catch:{ all -> 0x02d0 }
            double r11 = (double) r11     // Catch:{ all -> 0x02d0 }
            double r2 = java.lang.Math.pow(r2, r11)     // Catch:{ all -> 0x02d0 }
            int r2 = (int) r2     // Catch:{ all -> 0x02d0 }
            r14 = r2
            goto L_0x01e7
        L_0x01e5:
            r13 = r11
            r15 = r12
        L_0x01e7:
            r12 = r8
            if (r12 != 0) goto L_0x01ef
            if (r14 == 0) goto L_0x01ed
            goto L_0x01ef
        L_0x01ed:
            r12 = 0
            r14 = 0
        L_0x01ef:
            int r2 = r1.length     // Catch:{ all -> 0x02d0 }
            r3 = 4
            if (r2 != r3) goto L_0x01f8
            r2 = 2
            r1[r2] = r12     // Catch:{ all -> 0x02d0 }
            r1[r16] = r14     // Catch:{ all -> 0x02d0 }
        L_0x01f8:
            if (r12 <= 0) goto L_0x020a
            if (r14 <= 0) goto L_0x020a
            android.graphics.Bitmap$Config r2 = android.graphics.Bitmap.Config.ARGB_8888     // Catch:{ all -> 0x02d0 }
            android.graphics.Bitmap r8 = android.graphics.Bitmap.createBitmap(r12, r14, r2)     // Catch:{ all -> 0x02d0 }
            if (r8 != 0) goto L_0x0206
            monitor-exit(r7)
            return r8
        L_0x0206:
            r9.setBitmap(r8)     // Catch:{ all -> 0x02d0 }
            goto L_0x020b
        L_0x020a:
            r8 = 0
        L_0x020b:
            r2 = r4 & r17
            if (r2 != 0) goto L_0x0216
            r2 = 16777215(0xffffff, float:2.3509886E-38)
            r9.drawColor(r2)     // Catch:{ all -> 0x02d0 }
            goto L_0x0219
        L_0x0216:
            r9.drawColor(r4)     // Catch:{ all -> 0x02d0 }
        L_0x0219:
            android.graphics.Paint$Align r2 = getTextAlignedType(r33)     // Catch:{ all -> 0x02d0 }
            r10.setTextAlign(r2)     // Catch:{ all -> 0x02d0 }
            r2 = 1
            if (r6 != r2) goto L_0x0226
            r3 = 0
            r12 = 0
            goto L_0x0231
        L_0x0226:
            r2 = 2
            r3 = 0
            if (r6 != r2) goto L_0x022d
            r12 = r1[r3]     // Catch:{ all -> 0x02d0 }
            goto L_0x0231
        L_0x022d:
            r1 = r1[r3]     // Catch:{ all -> 0x02d0 }
            int r12 = r1 / 2
        L_0x0231:
            r1 = 0
        L_0x0232:
            r2 = 92
            int r4 = r0.indexOf(r2, r3)     // Catch:{ all -> 0x02d0 }
            if (r4 <= 0) goto L_0x0285
            java.lang.String r3 = r0.substring(r3, r4)     // Catch:{ all -> 0x02d0 }
            android.text.Layout.getDesiredWidth(r3, r10)     // Catch:{ all -> 0x02d0 }
            int r4 = r4 + 1
            if (r5 == 0) goto L_0x0268
            float r6 = (float) r5     // Catch:{ all -> 0x02d0 }
            r10.setStrokeWidth(r6)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Cap r6 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeCap(r6)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Join r6 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeJoin(r6)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Style r6 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r6)     // Catch:{ all -> 0x02d0 }
            r6 = r30
            r10.setColor(r6)     // Catch:{ all -> 0x02d0 }
            float r11 = (float) r12     // Catch:{ all -> 0x02d0 }
            int r14 = r1 * r15
            float r14 = (float) r14     // Catch:{ all -> 0x02d0 }
            float r2 = r13.ascent     // Catch:{ all -> 0x02d0 }
            float r14 = r14 - r2
            r9.drawText(r3, r11, r14, r10)     // Catch:{ all -> 0x02d0 }
            goto L_0x026a
        L_0x0268:
            r6 = r30
        L_0x026a:
            android.graphics.Paint$Style r2 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r2)     // Catch:{ all -> 0x02d0 }
            r2 = r29
            r10.setColor(r2)     // Catch:{ all -> 0x02d0 }
            float r11 = (float) r12     // Catch:{ all -> 0x02d0 }
            int r14 = r1 * r15
            float r14 = (float) r14     // Catch:{ all -> 0x02d0 }
            r27 = r4
            float r4 = r13.ascent     // Catch:{ all -> 0x02d0 }
            float r14 = r14 - r4
            r9.drawText(r3, r11, r14, r10)     // Catch:{ all -> 0x02d0 }
            int r1 = r1 + 1
            r3 = r27
            goto L_0x0232
        L_0x0285:
            r2 = r29
            r6 = r30
            int r4 = r25.length()     // Catch:{ all -> 0x02d0 }
            if (r3 == r4) goto L_0x02ce
            int r4 = r25.length()     // Catch:{ all -> 0x02d0 }
            java.lang.String r0 = r0.substring(r3, r4)     // Catch:{ all -> 0x02d0 }
            android.text.Layout.getDesiredWidth(r0, r10)     // Catch:{ all -> 0x02d0 }
            if (r5 == 0) goto L_0x02bc
            float r3 = (float) r5     // Catch:{ all -> 0x02d0 }
            r10.setStrokeWidth(r3)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Cap r3 = android.graphics.Paint.Cap.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeCap(r3)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Join r3 = android.graphics.Paint.Join.ROUND     // Catch:{ all -> 0x02d0 }
            r10.setStrokeJoin(r3)     // Catch:{ all -> 0x02d0 }
            android.graphics.Paint$Style r3 = android.graphics.Paint.Style.STROKE     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r3)     // Catch:{ all -> 0x02d0 }
            r10.setColor(r6)     // Catch:{ all -> 0x02d0 }
            float r3 = (float) r12     // Catch:{ all -> 0x02d0 }
            int r4 = r1 * r15
            float r4 = (float) r4     // Catch:{ all -> 0x02d0 }
            float r5 = r13.ascent     // Catch:{ all -> 0x02d0 }
            float r4 = r4 - r5
            r9.drawText(r0, r3, r4, r10)     // Catch:{ all -> 0x02d0 }
        L_0x02bc:
            android.graphics.Paint$Style r3 = android.graphics.Paint.Style.FILL     // Catch:{ all -> 0x02d0 }
            r10.setStyle(r3)     // Catch:{ all -> 0x02d0 }
            r10.setColor(r2)     // Catch:{ all -> 0x02d0 }
            float r2 = (float) r12     // Catch:{ all -> 0x02d0 }
            int r1 = r1 * r15
            float r1 = (float) r1     // Catch:{ all -> 0x02d0 }
            float r3 = r13.ascent     // Catch:{ all -> 0x02d0 }
            float r1 = r1 - r3
            r9.drawText(r0, r2, r1, r10)     // Catch:{ all -> 0x02d0 }
        L_0x02ce:
            monitor-exit(r7)
            return r8
        L_0x02d0:
            r0 = move-exception
            monitor-exit(r7)
            goto L_0x02d4
        L_0x02d3:
            throw r0
        L_0x02d4:
            goto L_0x02d3
        */
        throw new UnsupportedOperationException("Method not decompiled: mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.drawTextExt(java.lang.String, int, int, int[], int, int, int, int, int):android.graphics.Bitmap");
    }

    private static Paint.Align getTextAlignedType(int i) {
        return 1 == i ? Paint.Align.LEFT : 2 == i ? Paint.Align.RIGHT : Paint.Align.CENTER;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0034 A[LOOP:0: B:11:0x0032->B:12:0x0034, LOOP_END] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static short[] getTextSize(java.lang.String r8, int r9, int r10) {
        /*
            int r0 = r8.length()
            if (r0 != 0) goto L_0x0008
            r8 = 0
            return r8
        L_0x0008:
            android.text.TextPaint r1 = new android.text.TextPaint
            r1.<init>()
            r2 = 1
            r1.setSubpixelText(r2)
            r1.setAntiAlias(r2)
            float r9 = (float) r9
            r1.setTextSize(r9)
            r9 = 0
            if (r10 == r2) goto L_0x0028
            r2 = 2
            if (r10 == r2) goto L_0x0028
            android.graphics.Typeface r10 = android.graphics.Typeface.DEFAULT
            android.graphics.Typeface r10 = android.graphics.Typeface.create(r10, r9)
        L_0x0024:
            r1.setTypeface(r10)
            goto L_0x002f
        L_0x0028:
            android.graphics.Typeface r10 = android.graphics.Typeface.DEFAULT
            android.graphics.Typeface r10 = android.graphics.Typeface.create(r10, r2)
            goto L_0x0024
        L_0x002f:
            short[] r10 = new short[r0]
            r2 = 0
        L_0x0032:
            if (r2 >= r0) goto L_0x0047
            int r3 = r2 + 1
            float r4 = android.text.Layout.getDesiredWidth(r8, r9, r3, r1)
            double r4 = (double) r4
            r6 = 4602678819172646912(0x3fe0000000000000, double:0.5)
            java.lang.Double.isNaN(r4)
            double r4 = r4 + r6
            int r4 = (int) r4
            short r4 = (short) r4
            r10[r2] = r4
            r2 = r3
            goto L_0x0032
        L_0x0047:
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.getTextSize(java.lang.String, int, int):short[]");
    }

    public static float[] getTextSizeExt(String str, int i, int i2) {
        if (str.length() == 0) {
            return null;
        }
        Paint paint = new Paint();
        paint.setSubpixelText(true);
        paint.setAntiAlias(true);
        paint.setTextSize((float) i);
        paint.setTypeface(i2 != 1 ? i2 != 2 ? Typeface.create(Typeface.DEFAULT, 0) : Typeface.create(Typeface.DEFAULT, 2) : Typeface.create(Typeface.DEFAULT, 1));
        return new float[]{paint.measureText(str), paint.descent() - paint.ascent()};
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0037, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void registFontCache(int r2, android.graphics.Typeface r3) {
        /*
            java.lang.Class<mapsdkvi.com.gdi.bgl.android.java.EnvDrawText> r0 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.class
            monitor-enter(r0)
            if (r2 == 0) goto L_0x003b
            if (r3 != 0) goto L_0x0008
            goto L_0x003b
        L_0x0008:
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r1 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0038 }
            if (r1 != 0) goto L_0x0013
            android.util.SparseArray r1 = new android.util.SparseArray     // Catch:{ all -> 0x0038 }
            r1.<init>()     // Catch:{ all -> 0x0038 }
            mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache = r1     // Catch:{ all -> 0x0038 }
        L_0x0013:
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r1 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0038 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x0038 }
            mapsdkvi.com.gdi.bgl.android.java.a r1 = (mapsdkvi.com.gdi.bgl.android.java.C3410a) r1     // Catch:{ all -> 0x0038 }
            if (r1 != 0) goto L_0x0030
            mapsdkvi.com.gdi.bgl.android.java.a r1 = new mapsdkvi.com.gdi.bgl.android.java.a     // Catch:{ all -> 0x0038 }
            r1.<init>()     // Catch:{ all -> 0x0038 }
            r1.f7931a = r3     // Catch:{ all -> 0x0038 }
            int r3 = r1.f7932b     // Catch:{ all -> 0x0038 }
            int r3 = r3 + 1
            r1.f7932b = r3     // Catch:{ all -> 0x0038 }
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r3 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0038 }
            r3.put(r2, r1)     // Catch:{ all -> 0x0038 }
            goto L_0x0036
        L_0x0030:
            int r2 = r1.f7932b     // Catch:{ all -> 0x0038 }
            int r2 = r2 + 1
            r1.f7932b = r2     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r0)
            return
        L_0x0038:
            r2 = move-exception
            monitor-exit(r0)
            throw r2
        L_0x003b:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.registFontCache(int, android.graphics.Typeface):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void removeFontCache(int r3) {
        /*
            java.lang.Class<mapsdkvi.com.gdi.bgl.android.java.EnvDrawText> r0 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.class
            monitor-enter(r0)
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r1 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0020 }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ all -> 0x0020 }
            mapsdkvi.com.gdi.bgl.android.java.a r1 = (mapsdkvi.com.gdi.bgl.android.java.C3410a) r1     // Catch:{ all -> 0x0020 }
            if (r1 != 0) goto L_0x000f
            monitor-exit(r0)
            return
        L_0x000f:
            int r2 = r1.f7932b     // Catch:{ all -> 0x0020 }
            int r2 = r2 + -1
            r1.f7932b = r2     // Catch:{ all -> 0x0020 }
            int r1 = r1.f7932b     // Catch:{ all -> 0x0020 }
            if (r1 != 0) goto L_0x001e
            android.util.SparseArray<mapsdkvi.com.gdi.bgl.android.java.a> r1 = mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.fontCache     // Catch:{ all -> 0x0020 }
            r1.remove(r3)     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r0)
            return
        L_0x0020:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: mapsdkvi.com.gdi.bgl.android.java.EnvDrawText.removeFontCache(int):void");
    }
}
