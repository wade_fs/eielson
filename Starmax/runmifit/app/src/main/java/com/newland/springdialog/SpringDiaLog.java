package com.newland.springdialog;

import android.app.Activity;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.tencent.bugly.beta.tinker.TinkerReport;

public class SpringDiaLog {
    /* access modifiers changed from: private */
    public ViewGroup androidContentView;
    /* access modifiers changed from: private */
    public double heightY;
    private boolean isCanceledOnTouchOutside = true;
    private boolean isOverScreen = true;
    private boolean isShowCloseButton = true;
    private boolean isShowing;
    /* access modifiers changed from: private */
    public boolean isUseAnimation = true;
    /* access modifiers changed from: private */
    public RelativeLayout mAnimationView;
    private int mBackGroudImg = -1;
    private ImageView mCloseButton;
    private int mCloseButtonImg = -1;
    /* access modifiers changed from: private */
    public View.OnClickListener mCloseButtonListener;
    private RelativeLayout mContainerView;
    private View mContentView;
    private int mContentViewHeight = TinkerReport.KEY_LOADED_PACKAGE_CHECK_SIGNATURE;
    private int mContentViewWidth = 280;
    private FrameLayout mContentView_FrameLayout;
    private Activity mContext;
    /* access modifiers changed from: private */
    public View mRootView;
    private int mStartAnimAngle = 270;
    /* access modifiers changed from: private */
    public double widthX;

    public SpringDiaLog(Activity activity, View view) {
        this.mContext = activity;
        this.mContentView = view;
        initView();
    }

    private void initView() {
        initDisplayOpinion();
        double sqrt = Math.sqrt((double) ((DisplayUtil.screenhightPx * DisplayUtil.screenhightPx) + (DisplayUtil.screenWidthPx * DisplayUtil.screenWidthPx)));
        this.heightY = (-Math.sin(Math.toRadians((double) this.mStartAnimAngle))) * sqrt;
        this.widthX = Math.cos(Math.toRadians((double) this.mStartAnimAngle)) * sqrt;
        if (this.isOverScreen) {
            this.androidContentView = (ViewGroup) this.mContext.getWindow().getDecorView();
        } else {
            this.androidContentView = (ViewGroup) this.mContext.getWindow().findViewById(16908290);
        }
        this.mRootView = LayoutInflater.from(this.mContext).inflate(C2364R.layout.spring_dialog_layout, (ViewGroup) null);
        View view = this.mRootView;
        if (view != null) {
            this.mCloseButton = (ImageView) view.findViewById(C2364R.C2367id.iv_close);
            this.mCloseButton.setImageDrawable(this.mContext.getResources().getDrawable(C2364R.C2366drawable.closebutton));
            this.mContainerView = (RelativeLayout) this.mRootView.findViewById(C2364R.C2367id.contentView);
            this.mAnimationView = (RelativeLayout) this.mRootView.findViewById(C2364R.C2367id.anim_container);
            this.mContentView_FrameLayout = (FrameLayout) this.mRootView.findViewById(C2364R.C2367id.fl_content_container);
            return;
        }
        Log.e("控件初始化失败", "LayoutInflater获取根视图失败！");
    }

    private void initDisplayOpinion() {
        DisplayMetrics displayMetrics = this.mContext.getResources().getDisplayMetrics();
        DisplayUtil.density = displayMetrics.density;
        DisplayUtil.densityDPI = displayMetrics.densityDpi;
        DisplayUtil.screenWidthPx = displayMetrics.widthPixels;
        DisplayUtil.screenhightPx = displayMetrics.heightPixels;
        DisplayUtil.screenWidthDip = (float) DisplayUtil.px2dip(this.mContext, (float) displayMetrics.widthPixels);
        DisplayUtil.screenHightDip = (float) DisplayUtil.px2dip(this.mContext, (float) displayMetrics.heightPixels);
    }

    public void show() {
        if (this.mRootView != null) {
            this.isShowing = true;
            if (this.isShowCloseButton) {
                this.mCloseButton.setOnClickListener(new View.OnClickListener() {
                    /* class com.newland.springdialog.SpringDiaLog.C23681 */

                    public void onClick(View view) {
                        if (SpringDiaLog.this.mCloseButtonListener != null) {
                            SpringDiaLog.this.mCloseButtonListener.onClick(view);
                        }
                        if (!SpringDiaLog.this.isUseAnimation) {
                            SpringDiaLog.this.androidContentView.removeView(SpringDiaLog.this.mRootView);
                            return;
                        }
                        AnimSpring.getInstance(SpringDiaLog.this.mAnimationView).startTranslationAnim(0.0d, 0.0d, -SpringDiaLog.this.widthX, -SpringDiaLog.this.heightY);
                        new Handler().postDelayed(new Runnable() {
                            /* class com.newland.springdialog.SpringDiaLog.C23681.C23691 */

                            public void run() {
                                SpringDiaLog.this.androidContentView.removeView(SpringDiaLog.this.mRootView);
                            }
                        }, 400);
                    }
                });
            } else {
                this.mCloseButton.setVisibility(View.GONE);
                if (this.isCanceledOnTouchOutside) {
                    this.mRootView.setOnClickListener(new View.OnClickListener() {
                        /* class com.newland.springdialog.SpringDiaLog.C23702 */

                        public void onClick(View view) {
                            if (!SpringDiaLog.this.isUseAnimation) {
                                SpringDiaLog.this.androidContentView.removeView(SpringDiaLog.this.mRootView);
                                return;
                            }
                            AnimSpring.getInstance(SpringDiaLog.this.mAnimationView).startTranslationAnim(0.0d, 0.0d, -SpringDiaLog.this.widthX, -SpringDiaLog.this.heightY);
                            new Handler().postDelayed(new Runnable() {
                                /* class com.newland.springdialog.SpringDiaLog.C23702.C23711 */

                                public void run() {
                                    SpringDiaLog.this.androidContentView.removeView(SpringDiaLog.this.mRootView);
                                }
                            }, 400);
                        }
                    });
                }
            }
            if (this.mCloseButtonImg != -1) {
                this.mCloseButton.setImageDrawable(this.mContext.getResources().getDrawable(this.mCloseButtonImg));
            }
            if (this.mBackGroudImg != -1) {
                this.mContainerView.setBackground(this.mContext.getResources().getDrawable(this.mBackGroudImg));
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.width = DisplayUtil.dip2px(this.mContext, (float) this.mContentViewWidth);
            layoutParams.height = DisplayUtil.dip2px(this.mContext, (float) this.mContentViewHeight);
            this.mContainerView.setLayoutParams(layoutParams);
            this.mContentView_FrameLayout.removeAllViews();
            this.mContentView_FrameLayout.addView(this.mContentView);
            ViewGroup.LayoutParams layoutParams2 = new ViewGroup.LayoutParams(-1, -1);
            this.androidContentView.removeView(this.mRootView);
            this.androidContentView.addView(this.mRootView, layoutParams2);
            if (this.isUseAnimation) {
                double sqrt = Math.sqrt((double) ((DisplayUtil.screenhightPx * DisplayUtil.screenhightPx) + (DisplayUtil.screenWidthPx * DisplayUtil.screenWidthPx)));
                this.heightY = (-Math.sin(Math.toRadians((double) this.mStartAnimAngle))) * sqrt;
                this.widthX = Math.cos(Math.toRadians((double) this.mStartAnimAngle)) * sqrt;
                AnimSpring.getInstance(this.mAnimationView).startTranslationAnim(this.widthX, this.heightY, 0.0d, 0.0d);
                return;
            }
            return;
        }
        Log.e("控件初始化失败", "LayoutInflater获取根视图失败！");
    }

    public void close() {
        if (this.isShowing) {
            if (!this.isUseAnimation) {
                this.androidContentView.removeView(this.mRootView);
            } else {
                AnimSpring.getInstance(this.mAnimationView).startTranslationAnim(0.0d, 0.0d, -this.widthX, -this.heightY);
                new Handler().postDelayed(new Runnable() {
                    /* class com.newland.springdialog.SpringDiaLog.C23723 */

                    public void run() {
                        SpringDiaLog.this.androidContentView.removeView(SpringDiaLog.this.mRootView);
                    }
                }, 400);
            }
            this.isShowing = false;
            return;
        }
        Log.e("关闭失败", "弹框未显示！");
    }

    public SpringDiaLog setBackGroudImg(int i) {
        this.mBackGroudImg = i;
        return this;
    }

    public SpringDiaLog setCloseButtonImg(int i) {
        this.mCloseButtonImg = i;
        return this;
    }

    public SpringDiaLog setCloseButtonListener(View.OnClickListener onClickListener) {
        this.mCloseButtonListener = onClickListener;
        return this;
    }

    public boolean isOverScreen() {
        return this.isOverScreen;
    }

    public SpringDiaLog setOverScreen(boolean z) {
        this.isOverScreen = z;
        return this;
    }

    public SpringDiaLog setShowCloseButton(boolean z) {
        this.isShowCloseButton = z;
        return this;
    }

    public SpringDiaLog setCanceledOnTouchOutside(boolean z) {
        this.isCanceledOnTouchOutside = z;
        return this;
    }

    public SpringDiaLog setStartAnimAngle(int i) {
        this.mStartAnimAngle = i;
        return this;
    }

    public SpringDiaLog setContentViewWidth(int i) {
        this.mContentViewWidth = i;
        return this;
    }

    public SpringDiaLog setContentViewHeight(int i) {
        this.mContentViewHeight = i;
        return this;
    }

    public SpringDiaLog setUseAnimation(boolean z) {
        this.isUseAnimation = z;
        return this;
    }

    public ViewGroup getAndroidContentView() {
        return this.androidContentView;
    }

    public View getRootView() {
        return this.mRootView;
    }
}
