package com.newland.springdialog;

import android.view.View;
import com.facebook.rebound.Spring;
import com.facebook.rebound.SpringConfig;
import com.facebook.rebound.SpringListener;
import com.facebook.rebound.SpringSystem;

public class AnimSpring {
    public static AnimSpring animSpring;
    public static SpringSystem springSystem;
    /* access modifiers changed from: private */
    public View animView;
    private double friction = -1.0d;
    public SpringConfig springConfig = SpringConfig.fromBouncinessAndSpeed(8.0d, 2.0d);
    private double tension = -1.0d;

    public AnimSpring(View view) {
        this.animView = view;
    }

    public static synchronized AnimSpring getInstance(View view) {
        AnimSpring animSpring2;
        synchronized (AnimSpring.class) {
            animSpring = new AnimSpring(view);
            if (springSystem == null) {
                springSystem = SpringSystem.create();
            }
            animSpring2 = animSpring;
        }
        return animSpring2;
    }

    public AnimSpring setTension(double d) {
        this.tension = d;
        double d2 = this.friction;
        if (d2 != -1.0d) {
            this.springConfig = SpringConfig.fromBouncinessAndSpeed(d, d2);
        } else {
            this.springConfig = SpringConfig.fromBouncinessAndSpeed(d, 2.0d);
        }
        return this;
    }

    public AnimSpring setFriction(double d) {
        this.friction = d;
        double d2 = this.tension;
        if (d2 != -1.0d) {
            this.springConfig = SpringConfig.fromBouncinessAndSpeed(d2, d);
        } else {
            this.springConfig = SpringConfig.fromBouncinessAndSpeed(8.0d, d);
        }
        return this;
    }

    public AnimSpring startTranslationAnim(double d, double d2, double d3, double d4) {
        Spring createSpring = springSystem.createSpring();
        Spring createSpring2 = springSystem.createSpring();
        createSpring.setSpringConfig(this.springConfig);
        createSpring2.setSpringConfig(this.springConfig);
        createSpring.setCurrentValue(d);
        createSpring2.setCurrentValue(d2);
        createSpring.setEndValue(d3);
        createSpring2.setEndValue(d4);
        createSpring.addListener(new SpringListener() {
            /* class com.newland.springdialog.AnimSpring.C23591 */

            public void onSpringActivate(Spring spring) {
            }

            public void onSpringAtRest(Spring spring) {
            }

            public void onSpringEndStateChange(Spring spring) {
            }

            public void onSpringUpdate(Spring spring) {
                AnimSpring.this.animView.setTranslationX((float) spring.getCurrentValue());
            }
        });
        createSpring2.addListener(new SpringListener() {
            /* class com.newland.springdialog.AnimSpring.C23602 */

            public void onSpringActivate(Spring spring) {
            }

            public void onSpringAtRest(Spring spring) {
            }

            public void onSpringEndStateChange(Spring spring) {
            }

            public void onSpringUpdate(Spring spring) {
                AnimSpring.this.animView.setTranslationY((float) spring.getCurrentValue());
            }
        });
        return this;
    }

    public AnimSpring startRotateAnim(float f, float f2) {
        Spring createSpring = springSystem.createSpring();
        createSpring.setSpringConfig(this.springConfig);
        createSpring.setCurrentValue((double) f);
        createSpring.setEndValue((double) f2);
        createSpring.addListener(new SpringListener() {
            /* class com.newland.springdialog.AnimSpring.C23613 */

            public void onSpringActivate(Spring spring) {
            }

            public void onSpringAtRest(Spring spring) {
            }

            public void onSpringEndStateChange(Spring spring) {
            }

            public void onSpringUpdate(Spring spring) {
                AnimSpring.this.animView.setRotation((float) spring.getCurrentValue());
            }
        });
        return this;
    }

    public AnimSpring startScaleAnim(double d, double d2, double d3, double d4) {
        Spring createSpring = springSystem.createSpring();
        Spring createSpring2 = springSystem.createSpring();
        createSpring.setSpringConfig(this.springConfig);
        createSpring2.setSpringConfig(this.springConfig);
        createSpring.setCurrentValue(d);
        createSpring2.setCurrentValue(d2);
        createSpring.setEndValue(d3);
        createSpring2.setEndValue(d4);
        createSpring.addListener(new SpringListener() {
            /* class com.newland.springdialog.AnimSpring.C23624 */

            public void onSpringActivate(Spring spring) {
            }

            public void onSpringAtRest(Spring spring) {
            }

            public void onSpringEndStateChange(Spring spring) {
            }

            public void onSpringUpdate(Spring spring) {
                AnimSpring.this.animView.setScaleX((float) spring.getCurrentValue());
            }
        });
        createSpring2.addListener(new SpringListener() {
            /* class com.newland.springdialog.AnimSpring.C23635 */

            public void onSpringActivate(Spring spring) {
            }

            public void onSpringAtRest(Spring spring) {
            }

            public void onSpringEndStateChange(Spring spring) {
            }

            public void onSpringUpdate(Spring spring) {
                AnimSpring.this.animView.setScaleY((float) spring.getCurrentValue());
            }
        });
        return this;
    }
}
