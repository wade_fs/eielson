package com.tamic.novate.util;

import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.MimeTypeMap;
import com.baidu.mobstat.Config;
import com.runmifit.android.util.ChangeCharset;
import com.tamic.novate.config.ConfigLoader;
import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import no.nordicsemi.android.dfu.DfuBaseService;

public class FileUtil {
    private static final Pattern CONTENT_DISPOSITION_PATTERN = Pattern.compile("attachment;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$", 2);
    private static final Pattern CONTENT_DISPOSITION_PATTERN_2 = Pattern.compile("inline;\\s*filename\\s*=\\s*(\"?)([^\"]*)\\1\\s*$", 2);
    private static final boolean DEBUG = false;
    private static final int DEFAULT_COMPRESS_QUALITY = 100;
    public static final String DEFAULT_FILENAME = ("novatedownfile" + FILE_SUFFIX);
    private static final String DEFAULT_FILETAG = "DownLoads";
    private static String FILE_SUFFIX = ".tmpl";
    public static final String HIDDEN_PREFIX = ".";
    public static final String MIME_TYPE_APP = "application/*";
    public static final String MIME_TYPE_AUDIO = "audio/*";
    public static final String MIME_TYPE_IMAGE = "image/*";
    public static final String MIME_TYPE_TEXT = "text/*";
    public static final String MIME_TYPE_VIDEO = "video/*";
    private static final String PATH_DOCUMENT = "document";
    private static final String PATH_TREE = "tree";
    static final String TAG = "FileUtils";
    public static Comparator<File> sComparator = new Comparator<File>() {
        /* class com.tamic.novate.util.FileUtil.C27901 */

        public int compare(File file, File file2) {
            return file.getName().toLowerCase().compareTo(file2.getName().toLowerCase());
        }
    };
    public static FileFilter sDirFilter = new FileFilter() {
        /* class com.tamic.novate.util.FileUtil.C27923 */

        public boolean accept(File file) {
            return file.isDirectory() && !file.getName().startsWith(FileUtil.HIDDEN_PREFIX);
        }
    };
    public static FileFilter sFileFilter = new FileFilter() {
        /* class com.tamic.novate.util.FileUtil.C27912 */

        public boolean accept(File file) {
            return file.isFile() && !file.getName().startsWith(FileUtil.HIDDEN_PREFIX);
        }
    };

    private static boolean isGBK(byte[] bArr) {
        return true;
    }

    private FileUtil() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String getFilePathFromUri(android.content.Context r8, android.net.Uri r9, java.lang.String r10, java.lang.String[] r11) {
        /*
            java.lang.String r0 = "_data"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            r7 = 0
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch:{ all -> 0x0031 }
            r6 = 0
            r2 = r9
            r4 = r10
            r5 = r11
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0031 }
            if (r8 == 0) goto L_0x002b
            boolean r9 = r8.moveToFirst()     // Catch:{ all -> 0x0029 }
            if (r9 == 0) goto L_0x002b
            int r9 = r8.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r9 = r8.getString(r9)     // Catch:{ all -> 0x0029 }
            if (r8 == 0) goto L_0x0028
            r8.close()
        L_0x0028:
            return r9
        L_0x0029:
            r9 = move-exception
            goto L_0x0033
        L_0x002b:
            if (r8 == 0) goto L_0x0030
            r8.close()
        L_0x0030:
            return r7
        L_0x0031:
            r9 = move-exception
            r8 = r7
        L_0x0033:
            if (r8 == 0) goto L_0x0038
            r8.close()
        L_0x0038:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.util.FileUtil.getFilePathFromUri(android.content.Context, android.net.Uri, java.lang.String, java.lang.String[]):java.lang.String");
    }

    public static String getSDPath() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return Environment.getExternalStorageDirectory().toString();
        }
        Log.e("SDCard", "no sdcard found!");
        return null;
    }

    public static String getBasePath(Context context) {
        if (getSDPath() == null) {
            return context.getCacheDir().getAbsolutePath();
        }
        File file = new File(getSDPath() + File.separator + context.getPackageName());
        if (!file.exists()) {
            file.mkdir();
        }
        return file.getAbsolutePath();
    }

    public static String getDownLoadPath(Context context) {
        if (getSDPath() == null) {
            File file = new File(context.getExternalFilesDir(null) + File.separator + DEFAULT_FILETAG);
            if (!file.exists()) {
                file.mkdirs();
            }
            return context.getExternalFilesDir(null) + File.separator + DEFAULT_FILETAG + File.separator;
        }
        return getBasePath(context) + File.separator + DEFAULT_FILETAG + File.separator;
    }

    private static void closeStream(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String loadFromAssets(Context context, String str) {
        BufferedReader bufferedReader = null;
        try {
            InputStream open = context.getResources().getAssets().open(str);
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(open));
            try {
                char[] cArr = new char[1024];
                StringBuffer stringBuffer = new StringBuffer(open.available());
                while (true) {
                    int read = bufferedReader2.read(cArr);
                    if (read != -1) {
                        stringBuffer.append(String.valueOf(cArr, 0, read));
                    } else {
                        String stringBuffer2 = stringBuffer.toString();
                        closeStream(bufferedReader2);
                        return stringBuffer2;
                    }
                }
            } catch (Exception e) {
                e = e;
                bufferedReader = bufferedReader2;
                try {
                    e.printStackTrace();
                    closeStream(bufferedReader);
                    return "";
                } catch (Throwable th) {
                    th = th;
                    bufferedReader2 = bufferedReader;
                    closeStream(bufferedReader2);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                closeStream(bufferedReader2);
                throw th;
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            closeStream(bufferedReader);
            return "";
        }
    }

    public static void deleteFile(String str) {
        try {
            File file = new File(str);
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void deleteFiles(ArrayList<String> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            deleteFile(arrayList.get(i));
        }
    }

    public static File getUirFile(Uri uri) {
        return new File(uri.getPath());
    }

    public static String getExtension(String str) {
        if (str == null) {
            return null;
        }
        int lastIndexOf = str.lastIndexOf(HIDDEN_PREFIX);
        return lastIndexOf >= 0 ? str.substring(lastIndexOf) : "";
    }

    public static boolean isLocal(String str) {
        return str != null && !str.startsWith("http://") && !str.startsWith("https://");
    }

    public static boolean isMediaUri(Uri uri) {
        return "media".equalsIgnoreCase(uri.getAuthority());
    }

    public static Uri getUri(File file) {
        if (file != null) {
            return Uri.fromFile(file);
        }
        return null;
    }

    public static File getPathWithoutFilename(File file) {
        if (file == null) {
            return null;
        }
        if (file.isDirectory()) {
            return file;
        }
        String name = file.getName();
        String absolutePath = file.getAbsolutePath();
        String substring = absolutePath.substring(0, absolutePath.length() - name.length());
        if (substring.endsWith("/")) {
            substring = substring.substring(0, substring.length() - 1);
        }
        return new File(substring);
    }

    public static String getMimeType(File file) {
        String extension = getExtension(file.getName());
        return extension.length() > 0 ? MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1)) : DfuBaseService.MIME_TYPE_OCTET_STREAM;
    }

    public static String getMimeType(Context context, Uri uri) {
        return getMimeType(new File(getPath(context, uri)));
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0035  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getDataColumn(android.content.Context r8, android.net.Uri r9, java.lang.String r10, java.lang.String[] r11) {
        /*
            java.lang.String r0 = "_data"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            r7 = 0
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch:{ all -> 0x0031 }
            r6 = 0
            r2 = r9
            r4 = r10
            r5 = r11
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0031 }
            if (r8 == 0) goto L_0x002b
            boolean r9 = r8.moveToFirst()     // Catch:{ all -> 0x0029 }
            if (r9 == 0) goto L_0x002b
            int r9 = r8.getColumnIndexOrThrow(r0)     // Catch:{ all -> 0x0029 }
            java.lang.String r9 = r8.getString(r9)     // Catch:{ all -> 0x0029 }
            if (r8 == 0) goto L_0x0028
            r8.close()
        L_0x0028:
            return r9
        L_0x0029:
            r9 = move-exception
            goto L_0x0033
        L_0x002b:
            if (r8 == 0) goto L_0x0030
            r8.close()
        L_0x0030:
            return r7
        L_0x0031:
            r9 = move-exception
            r8 = r7
        L_0x0033:
            if (r8 == 0) goto L_0x0038
            r8.close()
        L_0x0038:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.util.FileUtil.getDataColumn(android.content.Context, android.net.Uri, java.lang.String, java.lang.String[]):java.lang.String");
    }

    public static String getPath(Context context, Uri uri) {
        Uri uri2 = null;
        if (!(Build.VERSION.SDK_INT >= 19) || !DocumentsContract.isDocumentUri(context, uri)) {
            if ("content".equalsIgnoreCase(uri.getScheme())) {
                if (isGooglePhotosUri(uri)) {
                    return uri.getLastPathSegment();
                }
                return getDataColumn(context, uri, null, null);
            } else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }
        } else if (isExternalStorageDocument(uri)) {
            String[] split = DocumentsContract.getDocumentId(uri).split(Config.TRACE_TODAY_VISIT_SPLIT);
            if ("primary".equalsIgnoreCase(split[0])) {
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            }
        } else if (isDownloadsDocument(uri)) {
            return getDataColumn(context, ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(DocumentsContract.getDocumentId(uri)).longValue()), null, null);
        } else if (isMediaDocument(uri)) {
            String[] split2 = DocumentsContract.getDocumentId(uri).split(Config.TRACE_TODAY_VISIT_SPLIT);
            String str = split2[0];
            if ("image".equals(str)) {
                uri2 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            } else if ("video".equals(str)) {
                uri2 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
            } else if ("audio".equals(str)) {
                uri2 = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
            }
            return getDataColumn(context, uri2, "_id=?", new String[]{split2[1]});
        }
        return null;
    }

    public static File getFile(Context context, Uri uri) {
        String path;
        if (uri == null || (path = getPath(context, uri)) == null || !isLocal(path)) {
            return null;
        }
        return new File(path);
    }

    public static String getReadableFileSize(int i) {
        float f;
        DecimalFormat decimalFormat = new DecimalFormat("###.#");
        String str = " KB";
        if (i > 1024) {
            f = (float) (i / 1024);
            if (f > 1024.0f) {
                f /= 1024.0f;
                if (f > 1024.0f) {
                    f /= 1024.0f;
                    str = " GB";
                } else {
                    str = " MB";
                }
            }
        } else {
            f = 0.0f;
        }
        return String.valueOf(decimalFormat.format((double) f) + str);
    }

    public static Bitmap getThumbnail(Context context, File file) {
        return getThumbnail(context, getUri(file), getMimeType(file));
    }

    public static Bitmap getThumbnail(Context context, Uri uri) {
        return getThumbnail(context, uri, getMimeType(context, uri));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        if (r9 != null) goto L_0x004a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x004a, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005b, code lost:
        if (r9 != null) goto L_0x004a;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0056  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap getThumbnail(android.content.Context r8, android.net.Uri r9, java.lang.String r10) {
        /*
            boolean r0 = isMediaUri(r9)
            r1 = 0
            if (r0 != 0) goto L_0x000f
            java.lang.String r8 = "FileUtils"
            java.lang.String r9 = "You can only retrieve thumbnails for images and videos."
            android.util.Log.e(r8, r9)
            return r1
        L_0x000f:
            if (r9 == 0) goto L_0x005e
            android.content.ContentResolver r8 = r8.getContentResolver()
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r2 = r8
            r3 = r9
            android.database.Cursor r9 = r2.query(r3, r4, r5, r6, r7)     // Catch:{ Exception -> 0x005a, all -> 0x0052 }
            boolean r0 = r9.moveToFirst()     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            if (r0 == 0) goto L_0x0048
            r0 = 0
            int r0 = r9.getInt(r0)     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            java.lang.String r2 = "video"
            boolean r2 = r10.contains(r2)     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            r3 = 1
            if (r2 == 0) goto L_0x003a
            long r4 = (long) r0     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            android.graphics.Bitmap r8 = android.provider.MediaStore.Video.Thumbnails.getThumbnail(r8, r4, r3, r1)     // Catch:{ Exception -> 0x0050, all -> 0x004e }
        L_0x0038:
            r1 = r8
            goto L_0x0048
        L_0x003a:
            java.lang.String r2 = "image/*"
            boolean r10 = r10.contains(r2)     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            if (r10 == 0) goto L_0x0048
            long r4 = (long) r0     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            android.graphics.Bitmap r8 = android.provider.MediaStore.Images.Thumbnails.getThumbnail(r8, r4, r3, r1)     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            goto L_0x0038
        L_0x0048:
            if (r9 == 0) goto L_0x005e
        L_0x004a:
            r9.close()
            goto L_0x005e
        L_0x004e:
            r8 = move-exception
            goto L_0x0054
        L_0x0050:
            goto L_0x005b
        L_0x0052:
            r8 = move-exception
            r9 = r1
        L_0x0054:
            if (r9 == 0) goto L_0x0059
            r9.close()
        L_0x0059:
            throw r8
        L_0x005a:
            r9 = r1
        L_0x005b:
            if (r9 == 0) goto L_0x005e
            goto L_0x004a
        L_0x005e:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.util.FileUtil.getThumbnail(android.content.Context, android.net.Uri, java.lang.String):android.graphics.Bitmap");
    }

    public static Intent createGetContentIntent() {
        Intent intent = new Intent("android.intent.action.GET_CONTENT");
        intent.setType("*/*");
        intent.addCategory("android.intent.category.OPENABLE");
        return intent;
    }

    public static String getFileNameWithURL(String str) {
        int lastIndexOf;
        String str2 = null;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        String decode = Uri.decode(str);
        if (TextUtils.isEmpty(decode)) {
            return null;
        }
        int lastIndexOf2 = decode.lastIndexOf(HIDDEN_PREFIX);
        String substring = (lastIndexOf2 == -1 || decode.length() - lastIndexOf2 >= 5) ? null : decode.substring(lastIndexOf2);
        int indexOf = decode.indexOf("?");
        if (indexOf > 0) {
            decode = decode.substring(0, indexOf);
        }
        int indexOf2 = decode.indexOf("#");
        if (indexOf2 > 0) {
            decode = decode.substring(0, indexOf2);
        }
        if (!decode.endsWith("/") && (lastIndexOf = decode.lastIndexOf("/") + 1) > 0) {
            str2 = decode.substring(lastIndexOf);
        }
        if (TextUtils.isEmpty(substring) || TextUtils.isEmpty(str2) || str2.contains(HIDDEN_PREFIX)) {
            return str2;
        }
        return str2 + substring;
    }

    private static String decodeContentdisposition(String str) {
        String parseContentDispostion = parseContentDispostion(str);
        if (TextUtils.isEmpty(parseContentDispostion)) {
            return parseContentDispostion;
        }
        try {
            byte[] bytes = parseContentDispostion.getBytes("utf-8");
            if (isUTF8(bytes, bytes.length)) {
                return new String(bytes, "utf-8");
            }
            if (isGBK(bytes)) {
                return new String(bytes, ChangeCharset.GBK);
            }
            return new String(bytes, ChangeCharset.GB2312);
        } catch (UnsupportedEncodingException unused) {
            Log.d(TAG, "UnsupportedEncodingException");
            return null;
        }
    }

    private static String parseContentDispostion(String str) {
        try {
            Matcher matcher = CONTENT_DISPOSITION_PATTERN.matcher(str);
            if (matcher.find()) {
                return matcher.group(2);
            }
            Matcher matcher2 = CONTENT_DISPOSITION_PATTERN_2.matcher(str);
            if (matcher2.find()) {
                return matcher2.group(2);
            }
            return null;
        } catch (Exception unused) {
            Log.d(TAG, "fail to parse content dispostion");
            return null;
        }
    }

    private static boolean isUTF8(byte[] bArr, int i) {
        int length = bArr.length;
        int i2 = 0;
        int i3 = 0;
        while (true) {
            int i4 = 1;
            if (i2 >= length || i3 >= i) {
                return true;
            }
            int i5 = i2 + 1;
            byte b = bArr[i2];
            if (b < 0) {
                if (b < -64 || b > -3) {
                    return false;
                }
                if (b > -4) {
                    i4 = 5;
                } else if (b > -8) {
                    i4 = 4;
                } else if (b > -16) {
                    i4 = 3;
                } else if (b > -32) {
                    i4 = 2;
                }
                if (i5 + i4 > length) {
                    return false;
                }
                int i6 = 0;
                while (i6 < i4) {
                    if (bArr[i5] >= -64) {
                        return false;
                    }
                    i6++;
                    i5++;
                }
            }
            i2 = i5;
            i3++;
        }
        return false;
    }

    public static String getFileName(String str, String str2) {
        String decodeContentdisposition = decodeContentdisposition(str);
        if (TextUtils.isEmpty(decodeContentdisposition)) {
            decodeContentdisposition = getFileNameWithURL(str2);
        }
        return TextUtils.isEmpty(decodeContentdisposition) ? DEFAULT_FILENAME : decodeContentdisposition;
    }

    public static String generateFileKey(String str, String str2) {
        return System.currentTimeMillis() + str + str2;
    }

    public static File createDownloadFile(String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            str2 = DEFAULT_FILENAME;
        }
        if (TextUtils.isEmpty(str)) {
            str = getDownLoadPath(ConfigLoader.getContext());
        }
        return createFile(str, str2);
    }

    public static File createFile(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            throw new RuntimeException("you should define downloadFolder path!");
        } else if (!TextUtils.isEmpty(str2)) {
            File file = new File(str);
            if (!file.exists()) {
                file.mkdirs();
            }
            File file2 = new File(str, str2);
            if (file2.exists()) {
                file2.delete();
            }
            return file2;
        } else {
            throw new RuntimeException("you should define downloadFileName !");
        }
    }

    public static boolean exists(File file) {
        return file.exists();
    }
}
