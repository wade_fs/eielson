package com.tamic.novate;

import android.content.Context;
import android.text.TextUtils;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.util.LogWraper;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

public class CacheInterceptor implements Interceptor {
    protected static final int maxStale = 259200;
    protected static final int maxStaleOnline = 60;
    protected String cacheControlValue_Offline;
    protected String cacheControlValue_Online;
    protected Context context;

    public CacheInterceptor(Context context2) {
        this(context2, String.format("max-age=%d", 60));
    }

    public CacheInterceptor(Context context2, String str) {
        this(context2, str, String.format("max-age=%d", Integer.valueOf((int) maxStale)));
    }

    public CacheInterceptor(Context context2, String str, String str2) {
        this.context = context2;
        this.cacheControlValue_Offline = str;
        this.cacheControlValue_Online = str2;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        chain.request();
        Response proceed = chain.proceed(chain.request());
        String header = proceed.header(HttpHeaders.CACHE_CONTROL);
        LogWraper.m6225d(Novate.TAG, "60s load cache:" + header);
        if (TextUtils.isEmpty(header) || header.contains("no-store") || header.contains("no-cache") || header.contains("must-revalidate") || header.contains("max-age") || header.contains("max-stale")) {
            return proceed.newBuilder().removeHeader(HttpHeaders.PRAGMA).removeHeader(HttpHeaders.CACHE_CONTROL).header(HttpHeaders.CACHE_CONTROL, "public, max-age=259200").build();
        }
        return proceed;
    }
}
