package com.tamic.novate.download;

import com.tamic.novate.Throwable;
import com.tamic.novate.callback.ResponseCallback;
import okhttp3.Call;
import okhttp3.ResponseBody;

public abstract class UpLoadCallback extends ResponseCallback {
    public void onCancel(Object obj, Throwable throwable) {
    }

    public void onError(Object obj, Throwable throwable) {
    }

    public Object onHandleResponse(ResponseBody responseBody) throws Exception {
        return responseBody;
    }

    public void onNext(Object obj, Call call, Object obj2) {
    }

    public abstract void onProgress(Object obj, int i, long j, boolean z);

    public void onProgress(Object obj, float f, long j, long j2) {
        super.onProgress(obj, f, j, j2);
        onProgress(obj, (int) f, j, j == j2);
    }
}
