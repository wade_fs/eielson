package com.tamic.novate.download;

import android.os.Handler;
import android.os.Looper;
import com.tamic.novate.exception.NovateException;
import com.tamic.novate.util.Utils;

public class NovateDownLoadManager {
    public static final String TAG = "Novate:DownLoadManager";
    private static String defPath = "";
    private static String fileSuffix = ".tmpl";
    public static boolean isCancel = false;
    public static boolean isDownLoading = false;
    private static NovateDownLoadManager sInstance;
    /* access modifiers changed from: private */
    public DownLoadCallBack callBack;
    private Handler handler = new Handler(Looper.getMainLooper());
    private String key;

    public NovateDownLoadManager(DownLoadCallBack downLoadCallBack) {
        this.callBack = downLoadCallBack;
    }

    public static synchronized NovateDownLoadManager getInstance(DownLoadCallBack downLoadCallBack) {
        NovateDownLoadManager novateDownLoadManager;
        synchronized (NovateDownLoadManager.class) {
            if (sInstance == null) {
                sInstance = new NovateDownLoadManager(downLoadCallBack);
            }
            novateDownLoadManager = sInstance;
        }
        return novateDownLoadManager;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x02e8, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x02e9, code lost:
        r21 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:0x02ee, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x02ef, code lost:
        r9 = r24;
        r21 = r26;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x02e8 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:65:0x022a] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0316 A[SYNTHETIC, Splitter:B:120:0x0316] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x031b A[Catch:{ IOException -> 0x032e }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0325 A[Catch:{ IOException -> 0x032e }] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x032a A[Catch:{ IOException -> 0x032e }] */
    /* JADX WARNING: Removed duplicated region for block: B:137:? A[Catch:{ IOException -> 0x032e }, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean writeResponseBodyToDisk(java.lang.String r25, java.lang.String r26, java.lang.String r27, android.content.Context r28, okhttp3.ResponseBody r29) {
        /*
            r24 = this;
            r9 = r24
            r0 = r25
            r1 = r27
            r2 = r28
            java.lang.String r10 = "file downloaded: "
            r11 = 0
            java.lang.String r12 = "Novate:DownLoadManager"
            if (r29 != 0) goto L_0x0042
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            java.lang.String r2 = " : ResponseBody is null"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            com.tamic.novate.util.LogWraper.m6229e(r12, r1)
            java.lang.NullPointerException r1 = new java.lang.NullPointerException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "the "
            r2.append(r3)
            r2.append(r0)
            java.lang.String r0 = " ResponseBody is null"
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r1.<init>(r0)
            r9.finalonError(r1)
            return r11
        L_0x0042:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Key:-->"
            r3.append(r4)
            r3.append(r0)
            java.lang.String r3 = r3.toString()
            com.tamic.novate.util.LogWraper.m6237v(r12, r3)
            okhttp3.MediaType r3 = r29.contentType()
            if (r3 == 0) goto L_0x0065
            okhttp3.MediaType r3 = r29.contentType()
            java.lang.String r3 = r3.toString()
            goto L_0x006c
        L_0x0065:
            java.lang.String r3 = "MediaType-->,无法获取"
            com.tamic.novate.util.LogWraper.m6225d(r12, r3)
            java.lang.String r3 = ""
        L_0x006c:
            boolean r4 = android.text.TextUtils.isEmpty(r3)
            if (r4 != 0) goto L_0x00a6
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "contentType:>>>>"
            r4.append(r5)
            okhttp3.MediaType r5 = r29.contentType()
            java.lang.String r5 = r5.toString()
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            com.tamic.novate.util.LogWraper.m6225d(r12, r4)
            com.tamic.novate.download.MimeType r4 = com.tamic.novate.download.MimeType.getInstance()
            java.lang.String r4 = r4.getSuffix(r3)
            boolean r4 = android.text.TextUtils.isEmpty(r4)
            if (r4 != 0) goto L_0x00a6
            com.tamic.novate.download.MimeType r4 = com.tamic.novate.download.MimeType.getInstance()
            java.lang.String r3 = r4.getSuffix(r3)
            com.tamic.novate.download.NovateDownLoadManager.fileSuffix = r3
        L_0x00a6:
            boolean r3 = android.text.TextUtils.isEmpty(r27)
            if (r3 != 0) goto L_0x00c5
            java.lang.String r3 = "."
            boolean r3 = r1.contains(r3)
            if (r3 != 0) goto L_0x00c5
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            java.lang.String r1 = com.tamic.novate.download.NovateDownLoadManager.fileSuffix
            r3.append(r1)
            java.lang.String r1 = r3.toString()
        L_0x00c5:
            r13 = r1
            r1 = 0
            if (r26 != 0) goto L_0x0110
            java.io.File r3 = new java.io.File
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.io.File r5 = r2.getExternalFilesDir(r1)
            r4.append(r5)
            java.lang.String r5 = java.io.File.separator
            r4.append(r5)
            java.lang.String r5 = "DownLoads"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.<init>(r4)
            boolean r4 = r3.exists()
            if (r4 != 0) goto L_0x00f1
            r3.mkdirs()
        L_0x00f1:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.io.File r2 = r2.getExternalFilesDir(r1)
            r3.append(r2)
            java.lang.String r2 = java.io.File.separator
            r3.append(r2)
            r3.append(r5)
            java.lang.String r2 = java.io.File.separator
            r3.append(r2)
            java.lang.String r2 = r3.toString()
            r14 = r2
            goto L_0x0112
        L_0x0110:
            r14 = r26
        L_0x0112:
            java.io.File r2 = new java.io.File
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r14)
            r3.append(r13)
            java.lang.String r3 = r3.toString()
            r2.<init>(r3)
            boolean r2 = r2.exists()
            if (r2 == 0) goto L_0x012f
            com.tamic.novate.util.FileUtil.deleteFile(r14)
        L_0x012f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "path:-->"
            r2.append(r3)
            r2.append(r14)
            java.lang.String r2 = r2.toString()
            com.tamic.novate.util.LogWraper.m6225d(r12, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "name:->"
            r2.append(r3)
            r2.append(r13)
            java.lang.String r2 = r2.toString()
            com.tamic.novate.util.LogWraper.m6225d(r12, r2)
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x032e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x032e }
            r3.<init>()     // Catch:{ IOException -> 0x032e }
            r3.append(r14)     // Catch:{ IOException -> 0x032e }
            r3.append(r13)     // Catch:{ IOException -> 0x032e }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x032e }
            r2.<init>(r3)     // Catch:{ IOException -> 0x032e }
            r3 = 4096(0x1000, float:5.74E-42)
            byte[] r15 = new byte[r3]     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            long r6 = r29.contentLength()     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            r3.<init>()     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            java.lang.String r4 = "file length: "
            r3.append(r4)     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            r3.append(r6)     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            com.tamic.novate.util.LogWraper.m6225d(r12, r3)     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            java.io.InputStream r8 = r29.byteStream()     // Catch:{ IOException -> 0x030f, all -> 0x030a }
            java.io.FileOutputStream r5 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0303, all -> 0x02fe }
            r5.<init>(r2)     // Catch:{ IOException -> 0x0303, all -> 0x02fe }
            r16 = 0
            r3 = r16
            r1 = 0
        L_0x0195:
            int r2 = r8.read(r15)     // Catch:{ IOException -> 0x02f9, all -> 0x02f4 }
            r11 = -1
            java.lang.String r0 = " of "
            if (r2 != r11) goto L_0x0224
            r5.flush()     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1.<init>()     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1.append(r10)     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1.append(r3)     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1.append(r0)     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1.append(r6)     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            com.tamic.novate.util.LogWraper.m6225d(r12, r1)     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1 = 0
            com.tamic.novate.download.NovateDownLoadManager.isDownLoading = r1     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            com.tamic.novate.download.DownLoadCallBack r1 = r9.callBack     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            if (r1 == 0) goto L_0x01fb
            android.os.Handler r11 = r9.handler     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            com.tamic.novate.download.NovateDownLoadManager$2 r15 = new com.tamic.novate.download.NovateDownLoadManager$2     // Catch:{ IOException -> 0x0219, all -> 0x020e }
            r1 = r15
            r2 = r24
            r26 = r8
            r8 = r3
            r3 = r25
            r4 = r14
            r14 = r5
            r5 = r13
            r27 = r6
            r1.<init>(r3, r4, r5, r6)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            r11.post(r15)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            r1.<init>()     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            r1.append(r10)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            r1.append(r8)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            r1.append(r0)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            r5 = r27
            r1.append(r5)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            java.lang.String r0 = r1.toString()     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            com.tamic.novate.util.LogWraper.m6225d(r12, r0)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            java.lang.String r0 = "file downloaded: is sucess"
            com.tamic.novate.util.LogWraper.m6225d(r12, r0)     // Catch:{ IOException -> 0x01f9, all -> 0x01f7 }
            goto L_0x01fe
        L_0x01f7:
            r0 = move-exception
            goto L_0x0212
        L_0x01f9:
            r0 = move-exception
            goto L_0x021d
        L_0x01fb:
            r14 = r5
            r26 = r8
        L_0x01fe:
            if (r26 == 0) goto L_0x0209
            r26.close()     // Catch:{ IOException -> 0x0204 }
            goto L_0x0209
        L_0x0204:
            r0 = move-exception
            r9 = r24
            goto L_0x032f
        L_0x0209:
            r14.close()     // Catch:{ IOException -> 0x0204 }
            r0 = 1
            return r0
        L_0x020e:
            r0 = move-exception
            r14 = r5
            r26 = r8
        L_0x0212:
            r9 = r24
            r21 = r26
            r11 = r14
            goto L_0x0323
        L_0x0219:
            r0 = move-exception
            r14 = r5
            r26 = r8
        L_0x021d:
            r9 = r24
            r1 = r26
            r11 = r14
            goto L_0x0311
        L_0x0224:
            r11 = r5
            r5 = r6
            r26 = r8
            r8 = r3
            r3 = 0
            r11.write(r15, r3, r2)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            long r2 = (long) r2     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            long r8 = r8 + r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r2.<init>()     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            java.lang.String r3 = "file download: "
            r2.append(r3)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r2.append(r8)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r2.append(r0)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r2.append(r5)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            java.lang.String r0 = r2.toString()     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            com.tamic.novate.util.LogWraper.m6225d(r12, r0)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r2 = -1
            int r0 = (r5 > r2 ? 1 : (r5 == r2 ? 0 : -1))
            if (r0 == 0) goto L_0x0263
            int r0 = (r5 > r16 ? 1 : (r5 == r16 ? 0 : -1))
            if (r0 != 0) goto L_0x0254
            goto L_0x0263
        L_0x0254:
            r2 = 100
            long r2 = r2 * r8
            long r2 = r2 / r5
            int r0 = (int) r2
            r4 = r0
            goto L_0x0267
        L_0x025c:
            r0 = move-exception
            r9 = r24
            r1 = r26
            goto L_0x0311
        L_0x0263:
            r0 = 100
            r4 = 100
        L_0x0267:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r0.<init>()     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            java.lang.String r2 = "file download progress : "
            r0.append(r2)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            r0.append(r4)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            java.lang.String r0 = r0.toString()     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            com.tamic.novate.util.LogWraper.m6225d(r12, r0)     // Catch:{ IOException -> 0x02ee, all -> 0x02e8 }
            if (r1 == 0) goto L_0x028b
            if (r4 < r1) goto L_0x0280
            goto L_0x028b
        L_0x0280:
            r21 = r26
            r19 = r5
            r22 = r8
            r18 = r10
            r9 = r24
            goto L_0x02d0
        L_0x028b:
            int r0 = r1 + 1
            r7 = r24
            com.tamic.novate.download.DownLoadCallBack r1 = r7.callBack     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            if (r1 == 0) goto L_0x02c3
            android.os.Handler r1 = new android.os.Handler     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            android.os.Looper r2 = android.os.Looper.getMainLooper()     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            r1.<init>(r2)     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            r7.handler = r1     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            android.os.Handler r3 = r7.handler     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            com.tamic.novate.download.NovateDownLoadManager$1 r2 = new com.tamic.novate.download.NovateDownLoadManager$1     // Catch:{ IOException -> 0x02e3, all -> 0x02de }
            r1 = r2
            r27 = r0
            r0 = r2
            r2 = r24
            r18 = r10
            r10 = r3
            r3 = r25
            r19 = r5
            r5 = r8
            r21 = r26
            r22 = r8
            r9 = r7
            r7 = r19
            r1.<init>(r3, r4, r5, r7)     // Catch:{ IOException -> 0x02c1, all -> 0x02be }
            r10.post(r0)     // Catch:{ IOException -> 0x02c1, all -> 0x02be }
            goto L_0x02ce
        L_0x02be:
            r0 = move-exception
            goto L_0x0323
        L_0x02c1:
            r0 = move-exception
            goto L_0x0307
        L_0x02c3:
            r21 = r26
            r27 = r0
            r19 = r5
            r22 = r8
            r18 = r10
            r9 = r7
        L_0x02ce:
            r1 = r27
        L_0x02d0:
            r0 = r25
            r5 = r11
            r10 = r18
            r6 = r19
            r8 = r21
            r3 = r22
            r11 = 0
            goto L_0x0195
        L_0x02de:
            r0 = move-exception
            r21 = r26
            r9 = r7
            goto L_0x0323
        L_0x02e3:
            r0 = move-exception
            r21 = r26
            r9 = r7
            goto L_0x0307
        L_0x02e8:
            r0 = move-exception
            r9 = r24
            r21 = r26
            goto L_0x0323
        L_0x02ee:
            r0 = move-exception
            r9 = r24
            r21 = r26
            goto L_0x0307
        L_0x02f4:
            r0 = move-exception
            r11 = r5
            r21 = r8
            goto L_0x0323
        L_0x02f9:
            r0 = move-exception
            r11 = r5
            r21 = r8
            goto L_0x0307
        L_0x02fe:
            r0 = move-exception
            r21 = r8
            r11 = r1
            goto L_0x0323
        L_0x0303:
            r0 = move-exception
            r21 = r8
            r11 = r1
        L_0x0307:
            r1 = r21
            goto L_0x0311
        L_0x030a:
            r0 = move-exception
            r11 = r1
            r21 = r11
            goto L_0x0323
        L_0x030f:
            r0 = move-exception
            r11 = r1
        L_0x0311:
            r9.finalonError(r0)     // Catch:{ all -> 0x0320 }
            if (r1 == 0) goto L_0x0319
            r1.close()     // Catch:{ IOException -> 0x032e }
        L_0x0319:
            if (r11 == 0) goto L_0x031e
            r11.close()     // Catch:{ IOException -> 0x032e }
        L_0x031e:
            r1 = 0
            return r1
        L_0x0320:
            r0 = move-exception
            r21 = r1
        L_0x0323:
            if (r21 == 0) goto L_0x0328
            r21.close()     // Catch:{ IOException -> 0x032e }
        L_0x0328:
            if (r11 == 0) goto L_0x032d
            r11.close()     // Catch:{ IOException -> 0x032e }
        L_0x032d:
            throw r0     // Catch:{ IOException -> 0x032e }
        L_0x032e:
            r0 = move-exception
        L_0x032f:
            r9.finalonError(r0)
            r1 = 0
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.download.NovateDownLoadManager.writeResponseBodyToDisk(java.lang.String, java.lang.String, java.lang.String, android.content.Context, okhttp3.ResponseBody):boolean");
    }

    private void finalonError(final Exception exc) {
        if (this.callBack != null) {
            if (Utils.checkMain()) {
                this.callBack.onError(NovateException.handleException(exc));
            } else {
                this.handler.post(new Runnable() {
                    /* class com.tamic.novate.download.NovateDownLoadManager.C27863 */

                    public void run() {
                        NovateDownLoadManager.this.callBack.onError(NovateException.handleException(exc));
                    }
                });
            }
        }
    }
}
