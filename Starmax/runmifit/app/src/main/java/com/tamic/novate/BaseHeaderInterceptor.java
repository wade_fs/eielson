package com.tamic.novate;

import com.tamic.novate.AbsRequestInterceptor;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Set;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class BaseHeaderInterceptor<T> extends AbsRequestInterceptor {
    private Map<String, T> headers;

    public BaseHeaderInterceptor(Map<String, T> map) {
        this(map, AbsRequestInterceptor.Type.ADD);
    }

    public BaseHeaderInterceptor(Map<String, T> map, AbsRequestInterceptor.Type type) {
        this.headers = map;
        super.control = type;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.proceed(interceptor(chain.request()));
    }

    private static String getValueEncoded(String str) throws UnsupportedEncodingException {
        if (str == null) {
            return "null";
        }
        String replace = str.replace("\n", "");
        int length = replace.length();
        for (int i = 0; i < length; i++) {
            char charAt = replace.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                return URLEncoder.encode(replace, "UTF-8");
            }
        }
        return replace;
    }

    /* access modifiers changed from: package-private */
    public Request interceptor(Request request) throws UnsupportedEncodingException {
        String str;
        String str2;
        Request.Builder newBuilder = request.newBuilder();
        Map<String, T> map = this.headers;
        if (map != null && map.size() > 0) {
            Set<String> keySet = this.headers.keySet();
            int i = C27521.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type[super.control.ordinal()];
            if (i == 1) {
                for (String str3 : keySet) {
                    if (this.headers.get(str3) == null) {
                        str = "";
                    } else {
                        str = getValueEncoded((String) this.headers.get(str3));
                    }
                    newBuilder.addHeader(str3, str).build();
                }
            } else if (i == 2) {
                for (String str4 : keySet) {
                    if (this.headers.get(str4) == null) {
                        str2 = "";
                    } else {
                        str2 = getValueEncoded((String) this.headers.get(str4));
                    }
                    newBuilder.header(str4, str2).build();
                }
            }
        }
        return newBuilder.build();
    }

    /* renamed from: com.tamic.novate.BaseHeaderInterceptor$1 */
    static /* synthetic */ class C27521 {
        static final /* synthetic */ int[] $SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type = new int[AbsRequestInterceptor.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.tamic.novate.AbsRequestInterceptor$Type[] r0 = com.tamic.novate.AbsRequestInterceptor.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.tamic.novate.BaseHeaderInterceptor.C27521.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type = r0
                int[] r0 = com.tamic.novate.BaseHeaderInterceptor.C27521.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.tamic.novate.AbsRequestInterceptor$Type r1 = com.tamic.novate.AbsRequestInterceptor.Type.ADD     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.tamic.novate.BaseHeaderInterceptor.C27521.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type     // Catch:{ NoSuchFieldError -> 0x001f }
                com.tamic.novate.AbsRequestInterceptor$Type r1 = com.tamic.novate.AbsRequestInterceptor.Type.UPDATE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.tamic.novate.BaseHeaderInterceptor.C27521.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type     // Catch:{ NoSuchFieldError -> 0x002a }
                com.tamic.novate.AbsRequestInterceptor$Type r1 = com.tamic.novate.AbsRequestInterceptor.Type.REMOVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.BaseHeaderInterceptor.C27521.<clinit>():void");
        }
    }
}
