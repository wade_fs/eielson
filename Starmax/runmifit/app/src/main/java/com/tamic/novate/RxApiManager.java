package com.tamic.novate;

import android.util.ArrayMap;
import p042rx.Subscription;

public class RxApiManager implements RxActionManager<Object> {
    private static RxApiManager sInstance;
    private ArrayMap<Object, Subscription> maps = new ArrayMap<>();

    public static RxApiManager get() {
        if (sInstance == null) {
            synchronized (RxApiManager.class) {
                if (sInstance == null) {
                    sInstance = new RxApiManager();
                }
            }
        }
        return sInstance;
    }

    private RxApiManager() {
    }

    public void add(Object obj, Subscription subscription) {
        this.maps.put(obj, subscription);
    }

    public void remove(Object obj) {
        if (!this.maps.isEmpty()) {
            this.maps.remove(obj);
        }
    }

    public void removeAll() {
        if (!this.maps.isEmpty()) {
            this.maps.clear();
        }
    }

    public void cancel(Object obj) {
        if (!this.maps.isEmpty() && this.maps.get(obj) != null && !this.maps.get(obj).isUnsubscribed()) {
            this.maps.get(obj).unsubscribe();
            this.maps.remove(obj);
        }
    }

    public void cancelAll() {
        if (!this.maps.isEmpty()) {
            for (Object obj : this.maps.keySet()) {
                cancel(obj);
            }
        }
    }
}
