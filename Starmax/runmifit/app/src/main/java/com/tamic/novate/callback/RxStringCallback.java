package com.tamic.novate.callback;

import android.util.Log;
import com.tamic.novate.Novate;
import okhttp3.Call;
import okhttp3.ResponseBody;

public abstract class RxStringCallback extends ResponseCallback<String, ResponseBody> {
    public abstract void onNext(Object obj, String str);

    public String onHandleResponse(ResponseBody responseBody) throws Exception {
        String str = new String(responseBody.bytes());
        Log.d(Novate.TAG, str);
        return str;
    }

    public void onNext(Object obj, Call call, String str) {
        onNext(obj, str);
    }
}
