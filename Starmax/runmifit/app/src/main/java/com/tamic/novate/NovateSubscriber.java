package com.tamic.novate;

import android.content.Context;
import com.tamic.novate.Novate;
import com.tamic.novate.util.LogWraper;
import com.tamic.novate.util.ReflectionUtil;
import java.lang.reflect.Type;
import okhttp3.ResponseBody;

class NovateSubscriber<T> extends BaseSubscriber<ResponseBody> {
    private Novate.ResponseCallBack<T> callBack;
    private Type finalNeedType;

    public NovateSubscriber(Context context, Novate.ResponseCallBack responseCallBack) {
        super(context);
        this.callBack = responseCallBack;
    }

    public void onStart() {
        super.onStart();
        Type[] parameterizedTypeswithInterfaces = ReflectionUtil.getParameterizedTypeswithInterfaces(this.callBack);
        if (ReflectionUtil.methodHandler(parameterizedTypeswithInterfaces) == null || ReflectionUtil.methodHandler(parameterizedTypeswithInterfaces).size() == 0) {
            LogWraper.m6229e(Novate.TAG, "callBack<T> 中T不合法: -->" + this.finalNeedType);
            throw new NullPointerException("callBack<T> 中T不合法");
        }
        this.finalNeedType = ReflectionUtil.methodHandler(parameterizedTypeswithInterfaces).get(0);
        Novate.ResponseCallBack<T> responseCallBack = this.callBack;
        if (responseCallBack != null) {
            responseCallBack.onStart();
        }
    }

    public void onCompleted() {
        Novate.ResponseCallBack<T> responseCallBack = this.callBack;
        if (responseCallBack != null) {
            responseCallBack.onCompleted();
        }
    }

    public void onError(Throwable throwable) {
        Novate.ResponseCallBack<T> responseCallBack = this.callBack;
        if (responseCallBack != null) {
            responseCallBack.onError(throwable);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<?>]
     candidates:
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.Class):T
      com.google.gson.Gson.fromJson(com.google.gson.JsonElement, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(com.google.gson.stream.JsonReader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.Class):T
      com.google.gson.Gson.fromJson(java.io.Reader, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.reflect.Type):T
      com.google.gson.Gson.fromJson(java.lang.String, java.lang.Class):T */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0131 A[Catch:{ Exception -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x013c A[Catch:{ Exception -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0141 A[Catch:{ Exception -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x015c A[Catch:{ Exception -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0184 A[Catch:{ Exception -> 0x01c5 }] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x018f A[Catch:{ Exception -> 0x01c5 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onNext(okhttp3.ResponseBody r15) {
        /*
            r14 = this;
            java.lang.String r0 = "dataResponse 无法解析为:"
            java.lang.String r1 = "data"
            java.lang.String r2 = "Novate"
            byte[] r15 = r15.bytes()     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r3 = new java.lang.String     // Catch:{ Exception -> 0x01e9 }
            r3.<init>(r15)     // Catch:{ Exception -> 0x01e9 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01e9 }
            r15.<init>()     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r4 = "ResponseBody:"
            r15.append(r4)     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r4 = r3.trim()     // Catch:{ Exception -> 0x01e9 }
            r15.append(r4)     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x01e9 }
            com.tamic.novate.util.LogWraper.m6225d(r2, r15)     // Catch:{ Exception -> 0x01e9 }
            com.tamic.novate.Novate$ResponseCallBack<T> r15 = r14.callBack     // Catch:{ Exception -> 0x01e9 }
            if (r15 == 0) goto L_0x01f8
            r15 = 1
            java.lang.String r4 = ""
            r5 = 0
            org.json.JSONObject r6 = new org.json.JSONObject     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = r3.trim()     // Catch:{ Exception -> 0x011f }
            r6.<init>(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = "code"
            int r15 = r6.optInt(r7)     // Catch:{ Exception -> 0x011f }
            java.lang.String r7 = "msg"
            java.lang.String r4 = r6.optString(r7)     // Catch:{ Exception -> 0x011a }
            boolean r7 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x011a }
            if (r7 == 0) goto L_0x0050
            java.lang.String r7 = "message"
            java.lang.String r4 = r6.optString(r7)     // Catch:{ Exception -> 0x011a }
        L_0x0050:
            boolean r7 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x011a }
            if (r7 == 0) goto L_0x005c
            java.lang.String r7 = "error"
            java.lang.String r4 = r6.optString(r7)     // Catch:{ Exception -> 0x011a }
        L_0x005c:
            com.tamic.novate.NovateResponse r7 = new com.tamic.novate.NovateResponse     // Catch:{ Exception -> 0x011a }
            r7.<init>()     // Catch:{ Exception -> 0x011a }
            r7.setCode(r15)     // Catch:{ Exception -> 0x0116 }
            r7.setMessage(r4)     // Catch:{ Exception -> 0x0116 }
            java.lang.Object r8 = r6.opt(r1)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0116 }
            boolean r9 = r8.isEmpty()     // Catch:{ Exception -> 0x0116 }
            java.lang.String r10 = "result"
            if (r9 == 0) goto L_0x007b
            java.lang.String r8 = r6.optString(r10)     // Catch:{ Exception -> 0x0116 }
        L_0x007b:
            boolean r9 = r8.isEmpty()     // Catch:{ Exception -> 0x0116 }
            if (r9 == 0) goto L_0x0084
            r7.setResult(r5)     // Catch:{ Exception -> 0x0116 }
        L_0x0084:
            boolean r9 = r8.isEmpty()     // Catch:{ Exception -> 0x0116 }
            r11 = 0
            if (r9 != 0) goto L_0x00e7
            char r9 = r8.charAt(r11)     // Catch:{ Exception -> 0x0116 }
            r12 = 123(0x7b, float:1.72E-43)
            if (r9 != r12) goto L_0x00e7
            org.json.JSONObject r1 = r6.optJSONObject(r1)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0116 }
            boolean r8 = r1.isEmpty()     // Catch:{ Exception -> 0x0116 }
            if (r8 == 0) goto L_0x00a9
            org.json.JSONObject r1 = r6.optJSONObject(r10)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0116 }
        L_0x00a9:
            com.google.gson.Gson r6 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0116 }
            r6.<init>()     // Catch:{ Exception -> 0x0116 }
            java.lang.reflect.Type r8 = r14.finalNeedType     // Catch:{ Exception -> 0x0116 }
            java.lang.Object r8 = com.tamic.novate.util.ReflectionUtil.newInstance(r8)     // Catch:{ Exception -> 0x0116 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x0116 }
            java.lang.Object r1 = r6.fromJson(r1, r8)     // Catch:{ Exception -> 0x0116 }
            android.content.Context r6 = r14.context     // Catch:{ Exception -> 0x00e1 }
            boolean r6 = com.tamic.novate.config.ConfigLoader.isFormat(r6)     // Catch:{ Exception -> 0x00e1 }
            if (r6 == 0) goto L_0x0113
            if (r1 == 0) goto L_0x00c7
            goto L_0x0113
        L_0x00c7:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00e1 }
            r6.<init>()     // Catch:{ Exception -> 0x00e1 }
            r6.append(r0)     // Catch:{ Exception -> 0x00e1 }
            java.lang.reflect.Type r8 = r14.finalNeedType     // Catch:{ Exception -> 0x00e1 }
            r6.append(r8)     // Catch:{ Exception -> 0x00e1 }
            java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x00e1 }
            com.tamic.novate.util.LogWraper.m6229e(r2, r6)     // Catch:{ Exception -> 0x00e1 }
            com.tamic.novate.exception.FormatException r6 = new com.tamic.novate.exception.FormatException     // Catch:{ Exception -> 0x00e1 }
            r6.<init>()     // Catch:{ Exception -> 0x00e1 }
            throw r6     // Catch:{ Exception -> 0x00e1 }
        L_0x00e1:
            r6 = move-exception
            r13 = r6
            r6 = r15
            r15 = r1
            r1 = r13
            goto L_0x0123
        L_0x00e7:
            boolean r1 = r8.isEmpty()     // Catch:{ Exception -> 0x0116 }
            if (r1 != 0) goto L_0x0112
            char r1 = r8.charAt(r11)     // Catch:{ Exception -> 0x0116 }
            r6 = 91
            if (r1 == r6) goto L_0x00f6
            goto L_0x0112
        L_0x00f6:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0116 }
            r1.<init>()     // Catch:{ Exception -> 0x0116 }
            java.lang.String r6 = "data为数对象无法转换: --- "
            r1.append(r6)     // Catch:{ Exception -> 0x0116 }
            java.lang.reflect.Type r6 = r14.finalNeedType     // Catch:{ Exception -> 0x0116 }
            r1.append(r6)     // Catch:{ Exception -> 0x0116 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0116 }
            com.tamic.novate.util.LogWraper.m6229e(r2, r1)     // Catch:{ Exception -> 0x0116 }
            java.lang.ClassCastException r1 = new java.lang.ClassCastException     // Catch:{ Exception -> 0x0116 }
            r1.<init>()     // Catch:{ Exception -> 0x0116 }
            throw r1     // Catch:{ Exception -> 0x0116 }
        L_0x0112:
            r1 = r5
        L_0x0113:
            r6 = r15
            r15 = r1
            goto L_0x013a
        L_0x0116:
            r1 = move-exception
            r6 = r15
            r15 = r5
            goto L_0x0123
        L_0x011a:
            r1 = move-exception
            r6 = r15
            r15 = r5
            r7 = r15
            goto L_0x0123
        L_0x011f:
            r1 = move-exception
            r15 = r5
            r7 = r15
            r6 = 1
        L_0x0123:
            r1.printStackTrace()     // Catch:{ Exception -> 0x01c5 }
            java.lang.String r8 = r1.getLocalizedMessage()     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.util.LogWraper.m6229e(r2, r8)     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.Novate$ResponseCallBack<T> r8 = r14.callBack     // Catch:{ Exception -> 0x01c5 }
            if (r8 == 0) goto L_0x013a
            com.tamic.novate.Novate$ResponseCallBack<T> r8 = r14.callBack     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.Throwable r1 = com.tamic.novate.exception.NovateException.handleException(r1)     // Catch:{ Exception -> 0x01c5 }
            r8.onError(r1)     // Catch:{ Exception -> 0x01c5 }
        L_0x013a:
            if (r15 == 0) goto L_0x013f
            r7.setData(r15)     // Catch:{ Exception -> 0x01c5 }
        L_0x013f:
            if (r15 != 0) goto L_0x0154
            android.content.Context r1 = r14.context     // Catch:{ Exception -> 0x01c5 }
            boolean r1 = r7.isOk(r1)     // Catch:{ Exception -> 0x01c5 }
            if (r1 == 0) goto L_0x0154
            java.lang.String r15 = "Response data 数据获取失败！"
            com.tamic.novate.util.LogWraper.m6225d(r2, r15)     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.Novate$ResponseCallBack<T> r15 = r14.callBack     // Catch:{ Exception -> 0x01c5 }
            r15.onsuccess(r6, r4, r5, r3)     // Catch:{ Exception -> 0x01c5 }
            return
        L_0x0154:
            android.content.Context r1 = r14.context     // Catch:{ Exception -> 0x01c5 }
            boolean r1 = com.tamic.novate.config.ConfigLoader.isFormat(r1)     // Catch:{ Exception -> 0x01c5 }
            if (r1 == 0) goto L_0x0179
            if (r7 == 0) goto L_0x015f
            goto L_0x0179
        L_0x015f:
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01c5 }
            r15.<init>()     // Catch:{ Exception -> 0x01c5 }
            r15.append(r0)     // Catch:{ Exception -> 0x01c5 }
            java.lang.reflect.Type r0 = r14.finalNeedType     // Catch:{ Exception -> 0x01c5 }
            r15.append(r0)     // Catch:{ Exception -> 0x01c5 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.util.LogWraper.m6229e(r2, r15)     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.exception.FormatException r15 = new com.tamic.novate.exception.FormatException     // Catch:{ Exception -> 0x01c5 }
            r15.<init>()     // Catch:{ Exception -> 0x01c5 }
            throw r15     // Catch:{ Exception -> 0x01c5 }
        L_0x0179:
            r7.setData(r15)     // Catch:{ Exception -> 0x01c5 }
            android.content.Context r0 = r14.context     // Catch:{ Exception -> 0x01c5 }
            boolean r0 = r7.isOk(r0)     // Catch:{ Exception -> 0x01c5 }
            if (r0 == 0) goto L_0x018f
            com.tamic.novate.Novate$ResponseCallBack<T> r0 = r14.callBack     // Catch:{ Exception -> 0x01c5 }
            r0.onsuccess(r6, r4, r15, r3)     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.Novate$ResponseCallBack<T> r15 = r14.callBack     // Catch:{ Exception -> 0x01c5 }
            r15.onSuccee(r7)     // Catch:{ Exception -> 0x01c5 }
            goto L_0x01f8
        L_0x018f:
            java.lang.String r15 = r7.getMsg()     // Catch:{ Exception -> 0x01c5 }
            if (r15 == 0) goto L_0x019a
            java.lang.String r15 = r7.getMsg()     // Catch:{ Exception -> 0x01c5 }
            goto L_0x01b2
        L_0x019a:
            java.lang.String r15 = r7.getError()     // Catch:{ Exception -> 0x01c5 }
            if (r15 == 0) goto L_0x01a5
            java.lang.String r15 = r7.getError()     // Catch:{ Exception -> 0x01c5 }
            goto L_0x01b2
        L_0x01a5:
            java.lang.String r15 = r7.getMessage()     // Catch:{ Exception -> 0x01c5 }
            if (r15 == 0) goto L_0x01b0
            java.lang.String r15 = r7.getMessage()     // Catch:{ Exception -> 0x01c5 }
            goto L_0x01b2
        L_0x01b0:
            java.lang.String r15 = "api未知异常"
        L_0x01b2:
            com.tamic.novate.exception.ServerException r0 = new com.tamic.novate.exception.ServerException     // Catch:{ Exception -> 0x01c5 }
            int r1 = r7.getCode()     // Catch:{ Exception -> 0x01c5 }
            r0.<init>(r1, r15)     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.Novate$ResponseCallBack<T> r15 = r14.callBack     // Catch:{ Exception -> 0x01c5 }
            com.tamic.novate.Throwable r0 = com.tamic.novate.exception.NovateException.handleException(r0)     // Catch:{ Exception -> 0x01c5 }
            r15.onError(r0)     // Catch:{ Exception -> 0x01c5 }
            goto L_0x01f8
        L_0x01c5:
            r15 = move-exception
            r15.printStackTrace()     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r15 = r15.getLocalizedMessage()     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r15 = r15.toString()     // Catch:{ Exception -> 0x01e9 }
            com.tamic.novate.util.LogWraper.m6229e(r2, r15)     // Catch:{ Exception -> 0x01e9 }
            com.tamic.novate.Novate$ResponseCallBack<T> r15 = r14.callBack     // Catch:{ Exception -> 0x01e9 }
            if (r15 == 0) goto L_0x01f8
            com.tamic.novate.Novate$ResponseCallBack<T> r15 = r14.callBack     // Catch:{ Exception -> 0x01e9 }
            java.lang.NullPointerException r0 = new java.lang.NullPointerException     // Catch:{ Exception -> 0x01e9 }
            java.lang.String r1 = "Response 解析失败！"
            r0.<init>(r1)     // Catch:{ Exception -> 0x01e9 }
            com.tamic.novate.Throwable r0 = com.tamic.novate.exception.NovateException.handleException(r0)     // Catch:{ Exception -> 0x01e9 }
            r15.onError(r0)     // Catch:{ Exception -> 0x01e9 }
            goto L_0x01f8
        L_0x01e9:
            r15 = move-exception
            r15.printStackTrace()
            com.tamic.novate.Novate$ResponseCallBack<T> r0 = r14.callBack
            if (r0 == 0) goto L_0x01f8
            com.tamic.novate.Throwable r15 = com.tamic.novate.exception.NovateException.handleException(r15)
            r0.onError(r15)
        L_0x01f8:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.NovateSubscriber.onNext(okhttp3.ResponseBody):void");
    }
}
