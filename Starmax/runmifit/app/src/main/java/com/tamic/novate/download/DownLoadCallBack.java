package com.tamic.novate.download;

import com.tamic.novate.Throwable;

public abstract class DownLoadCallBack {
    public void onCancel() {
    }

    public void onCompleted() {
    }

    public abstract void onError(Throwable throwable);

    public void onProgress(String str, int i, long j, long j2) {
    }

    public void onStart(String str) {
    }

    public abstract void onSucess(String str, String str2, String str3, long j);
}
