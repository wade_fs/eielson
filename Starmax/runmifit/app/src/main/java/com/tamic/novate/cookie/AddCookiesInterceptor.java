package com.tamic.novate.cookie;

import android.content.Context;
import com.tamic.novate.util.LogWraper;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import p042rx.Observable;
import p042rx.functions.Action1;

public class AddCookiesInterceptor implements Interceptor {
    private Context context;
    /* access modifiers changed from: private */
    public String lang;

    public AddCookiesInterceptor(Context context2, String str) {
        this.context = context2;
        this.lang = str;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        if (chain == null) {
            LogWraper.m6225d("novate", "Addchain == null");
        }
        final Request.Builder newBuilder = chain.request().newBuilder();
        Observable.just(this.context.getSharedPreferences("cookie", 0).getString("cookie", "")).subscribe(new Action1<String>() {
            /* class com.tamic.novate.cookie.AddCookiesInterceptor.C27801 */

            public void call(String str) {
                if (str.contains("lang=ch")) {
                    str = str.replace("lang=ch", "lang=" + AddCookiesInterceptor.this.lang);
                }
                if (str.contains("lang=en")) {
                    str = str.replace("lang=en", "lang=" + AddCookiesInterceptor.this.lang);
                }
                LogWraper.m6225d("novate", "AddCookiesInterceptor: " + str);
                newBuilder.addHeader("cookie", str);
            }
        });
        return chain.proceed(newBuilder.build());
    }
}
