package com.tamic.novate.callback;

import okhttp3.ResponseBody;

public abstract class RxResultCallback<T> extends RxGenericsCallback<T, ResponseBody> {
}
