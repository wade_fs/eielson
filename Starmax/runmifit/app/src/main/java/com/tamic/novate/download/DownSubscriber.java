package com.tamic.novate.download;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.tamic.novate.BaseSubscriber;
import com.tamic.novate.Throwable;
import com.tamic.novate.util.LogWraper;
import com.tamic.novate.util.Utils;
import okhttp3.ResponseBody;

public class DownSubscriber<ResponseBody extends ResponseBody> extends BaseSubscriber<ResponseBody> {
    /* access modifiers changed from: private */
    public DownLoadCallBack callBack;
    private Context context;
    private String key;
    private String name;
    private String path;

    public DownSubscriber(String str, String str2, String str3, DownLoadCallBack downLoadCallBack, Context context2) {
        super(context2);
        this.key = str;
        this.path = str2;
        this.name = str3;
        this.callBack = downLoadCallBack;
        this.context = context2;
    }

    public void onStart() {
        super.onStart();
        DownLoadCallBack downLoadCallBack = this.callBack;
        if (downLoadCallBack != null) {
            downLoadCallBack.onStart(this.key);
        }
    }

    public void onCompleted() {
        DownLoadCallBack downLoadCallBack = this.callBack;
        if (downLoadCallBack != null) {
            downLoadCallBack.onCompleted();
        }
    }

    public void onError(Throwable throwable) {
        LogWraper.m6229e(NovateDownLoadManager.TAG, "DownSubscriber:>>>> onError:" + throwable.getMessage());
        if (this.callBack != null) {
            final Throwable throwable2 = new Throwable(throwable, -100, throwable.getMessage());
            if (Utils.checkMain()) {
                this.callBack.onError(throwable2);
            } else {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    /* class com.tamic.novate.download.DownSubscriber.C27831 */

                    public void run() {
                        DownSubscriber.this.callBack.onError(throwable2);
                    }
                });
            }
        }
    }

    public void onNext(ResponseBody responsebody) {
        LogWraper.m6225d(NovateDownLoadManager.TAG, "DownSubscriber:>>>> onNext");
        new NovateDownLoadManager(this.callBack).writeResponseBodyToDisk(this.key, this.path, this.name, this.context, responsebody);
    }
}
