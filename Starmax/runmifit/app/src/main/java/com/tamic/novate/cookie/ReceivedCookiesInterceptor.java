package com.tamic.novate.cookie;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;
import p042rx.Observable;
import p042rx.functions.Action1;
import p042rx.functions.Func1;

public class ReceivedCookiesInterceptor implements Interceptor {
    private Context context;
    SharedPreferences sharedPreferences;

    public ReceivedCookiesInterceptor(Context context2) {
        this.context = context2;
        this.sharedPreferences = context2.getSharedPreferences("cookie", 0);
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        if (chain == null) {
            Log.d("http", "Receivedchain == null");
        }
        Response proceed = chain.proceed(chain.request());
        Log.d("http", "originalResponse" + proceed.toString());
        if (!proceed.headers("set-cookie").isEmpty()) {
            final StringBuffer stringBuffer = new StringBuffer();
            Observable.from(proceed.headers("set-cookie")).map(new Func1<String, String>() {
                /* class com.tamic.novate.cookie.ReceivedCookiesInterceptor.C27822 */

                public String call(String str) {
                    return str.split(";")[0];
                }
            }).subscribe(new Action1<String>() {
                /* class com.tamic.novate.cookie.ReceivedCookiesInterceptor.C27811 */

                public void call(String str) {
                    StringBuffer stringBuffer = stringBuffer;
                    stringBuffer.append(str);
                    stringBuffer.append(";");
                }
            });
            SharedPreferences.Editor edit = this.sharedPreferences.edit();
            edit.putString("cookie", stringBuffer.toString());
            Log.d("http", "ReceivedCookiesInterceptor" + stringBuffer.toString());
            edit.commit();
        }
        return proceed;
    }
}
