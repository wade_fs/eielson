package com.tamic.novate;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.util.Log;
import com.google.api.client.http.HttpMethods;
import com.google.gson.JsonParseException;
import com.tamic.novate.AbsRequestInterceptor;
import com.tamic.novate.cache.CookieCacheImpl;
import com.tamic.novate.callback.ResponseCallback;
import com.tamic.novate.config.ConfigLoader;
import com.tamic.novate.cookie.AddCookiesInterceptor;
import com.tamic.novate.cookie.NovateCookieManager;
import com.tamic.novate.cookie.ReceivedCookiesInterceptor;
import com.tamic.novate.cookie.SharedPrefsCookiePersistor;
import com.tamic.novate.download.DownLoadCallBack;
import com.tamic.novate.download.DownSubscriber;
import com.tamic.novate.exception.NovateException;
import com.tamic.novate.request.NovateRequest;
import com.tamic.novate.request.NovateRequestBody;
import com.tamic.novate.request.RequestInterceptor;
import com.tamic.novate.util.FileUtil;
import com.tamic.novate.util.LogWraper;
import com.tamic.novate.util.Utils;
import java.io.File;
import java.io.InputStream;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import okhttp3.Cache;
import okhttp3.Call;
import okhttp3.CertificatePinner;
import okhttp3.ConnectionPool;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import p042rx.Observable;
import p042rx.Subscriber;
import p042rx.android.schedulers.AndroidSchedulers;
import p042rx.functions.Action0;
import p042rx.functions.Func1;
import p042rx.schedulers.Schedulers;
import retrofit2.CallAdapter;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.FieldMap;
import retrofit2.http.Part;

public final class Novate {
    private static final long DEFAULT_CACHEMAXSIZE = 10485760;
    private static final long DEFAULT_KEEP_ALIVEDURATION = 8;
    private static final int DEFAULT_MAXIDLE_CONNECTIONS = 5;
    /* access modifiers changed from: private */
    public static int DEFAULT_MAX_STALE = 259200;
    private static final int DEFAULT_TIMEOUT = 15;
    public static final String KEY_CACHE = "Novate_Http_cache";
    public static final String TAG = "Novate";
    public static BaseApiService apiManager;
    /* access modifiers changed from: private */
    public static Map<String, String> headers;
    /* access modifiers changed from: private */
    public static Context mContext;
    /* access modifiers changed from: private */
    public static OkHttpClient okHttpClient;
    /* access modifiers changed from: private */
    public static OkHttpClient.Builder okhttpBuilder;
    /* access modifiers changed from: private */
    public static Map<String, String> parameters;
    /* access modifiers changed from: private */
    public static Retrofit retrofit;
    /* access modifiers changed from: private */
    public static Retrofit.Builder retrofitBuilder;
    /* access modifiers changed from: private */
    public final List<CallAdapter.Factory> adapterFactories;
    /* access modifiers changed from: private */
    public final String baseUrl;
    /* access modifiers changed from: private */
    public final Call.Factory callFactory;
    /* access modifiers changed from: private */
    public final Executor callbackExecutor;
    /* access modifiers changed from: private */
    public final List<Converter.Factory> converterFactories;
    private Map<Object, Observable<ResponseBody>> downMaps = new HashMap<Object, Observable<ResponseBody>>() {
        /* class com.tamic.novate.Novate.C27541 */
    };
    private Observable<ResponseBody> downObservable;
    private Observable.Transformer exceptTransformer = null;
    final Observable.Transformer onDdoTransformer = new Observable.Transformer() {
        /* class com.tamic.novate.Novate.C27552 */

        public Object call(Object obj) {
            return ((Observable) obj).doOnUnsubscribe(new Action0() {
                /* class com.tamic.novate.Novate.C27552.C27561 */

                public void call() {
                }
            });
        }
    };
    final Observable.Transformer schedulersTransformer = new Observable.Transformer() {
        /* class com.tamic.novate.Novate.C27573 */

        public Object call(Object obj) {
            return ((Observable) obj).subscribeOn(Schedulers.m8197io()).unsubscribeOn(Schedulers.m8197io()).observeOn(AndroidSchedulers.mainThread());
        }
    };
    final Observable.Transformer schedulersTransformerDown = new Observable.Transformer() {
        /* class com.tamic.novate.Novate.C27584 */

        public Object call(Object obj) {
            return ((Observable) obj).subscribeOn(Schedulers.m8197io()).subscribeOn(Schedulers.newThread()).unsubscribeOn(Schedulers.m8197io()).observeOn(Schedulers.m8197io());
        }
    };
    /* access modifiers changed from: private */
    public final boolean validateEagerly;

    @Deprecated
    public interface ResponseCallBack<T> {
        void onCompleted();

        void onError(Throwable throwable);

        void onStart();

        @Deprecated
        void onSuccee(NovateResponse<T> novateResponse);

        void onsuccess(int i, String str, T t, String str2);
    }

    Novate(Call.Factory factory, String str, Map<String, String> map, Map<String, String> map2, BaseApiService baseApiService, List<Converter.Factory> list, List<CallAdapter.Factory> list2, Executor executor, boolean z) {
        this.callFactory = factory;
        this.baseUrl = str;
        headers = map;
        parameters = map2;
        apiManager = baseApiService;
        this.converterFactories = list;
        this.adapterFactories = list2;
        this.callbackExecutor = executor;
        this.validateEagerly = z;
    }

    public <T> T create(Class<T> cls) {
        return retrofit.create(cls);
    }

    public <T> T call(Observable<T> observable, BaseSubscriber<T> baseSubscriber) {
        return observable.compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) baseSubscriber);
    }

    public <T> T call(Observable<T> observable, ResponseCallback responseCallback) {
        return observable.compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(observable.getClass().getSimpleName(), responseCallback));
    }

    public <T> Observable<T> schedulersIo(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.m8197io()).unsubscribeOn(Schedulers.m8197io()).observeOn(Schedulers.m8197io());
    }

    public <T> Observable<T> schedulersMain(Observable<T> observable) {
        return observable.subscribeOn(Schedulers.m8197io()).unsubscribeOn(Schedulers.m8197io()).observeOn(AndroidSchedulers.mainThread());
    }

    public <T> T execute(NovateRequest novateRequest, ResponseCallback<T, ResponseBody> responseCallback) {
        return call(novateRequest, new RxSubscriber(novateRequest.tag(), responseCallback));
    }

    public <T> T execute(NovateRequest novateRequest, BaseSubscriber<T> baseSubscriber) {
        return call(novateRequest, baseSubscriber);
    }

    private <T> T call(NovateRequest novateRequest, BaseSubscriber<T> baseSubscriber) {
        return createRx(novateRequest).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) baseSubscriber);
    }

    private Observable<RequestBody> createRx(NovateRequest novateRequest) {
        if (novateRequest.method().equals("GET")) {
            return apiManager.executeGet(novateRequest.url(), novateRequest.params());
        }
        if (novateRequest.method().equals("POST")) {
            return apiManager.executePost(novateRequest.url(), novateRequest.params());
        }
        if (novateRequest.method().equals(HttpMethods.PUT)) {
            return apiManager.executePut(novateRequest.url(), novateRequest.params());
        }
        if (novateRequest.method().equals(HttpMethods.DELETE)) {
            return apiManager.executeDelete(novateRequest.url(), novateRequest.params());
        }
        return apiManager.executeGet(novateRequest.url(), novateRequest.params());
    }

    public <T> T executeGet(String str, Map<String, Object> map, ResponseCallBack<T> responseCallBack) {
        return apiManager.executeGet(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public <T> T rxGet(String str, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxGet(str, str, null, responseCallback);
    }

    public <T> T rxGet(String str, Map<String, Object> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxGet(str, str, map, responseCallback);
    }

    public <T> Observable<T> rxGet(String str, Map<String, Object> map) {
        return apiManager.executeGet(str, map);
    }

    public <T> T rxGet(String str, String str2, Map<String, Object> map, ResponseCallback<T, ResponseBody> responseCallback) {
        if (map != null) {
            return apiManager.executeGet(str2, map).compose(new OndoTransformer(str, responseCallback)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(str, responseCallback).addContext(mContext));
        }
        throw new NullPointerException(" maps is not null!");
    }

    public <T> T rxPost(String str, @FieldMap(encoded = true) Map<String, Object> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxPost(str, str, map, responseCallback);
    }

    public <T> T rxPost(String str, String str2, @FieldMap(encoded = true) Map<String, Object> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.executePost(str2, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(str, responseCallback).addContext(mContext));
    }

    public <T> T rxPut(String str, @FieldMap(encoded = true) Map<String, T> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxPut(str, str, map, responseCallback);
    }

    public <T> T rxPut(String str, String str2, @FieldMap(encoded = true) Map<String, T> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.executePut(str2, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(str, responseCallback).addContext(mContext));
    }

    public <T> T rxDelete(String str, Map<String, T> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxDelete(str, str, map, responseCallback);
    }

    public <T> T rxDelete(String str, String str2, Map<String, T> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.executeDelete(str2, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(str, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithPart(String str, String str2, File file, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPart(str, str2, ContentType.IMAGE, file, responseCallback);
    }

    public <T> T rxUploadWithPart(String str, File file, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPart(str, str, ContentType.IMAGE, file, responseCallback);
    }

    public <T> T rxUploadWithPart(Object obj, String str, ContentType contentType, File file, ResponseCallback<T, ResponseBody> responseCallback) {
        if (file.exists()) {
            if (responseCallback == null) {
                responseCallback = ResponseCallback.CALLBACK_DEFAULT;
            }
            return rxUploadWithPart(obj, str, MultipartBody.Part.createFormData("image", file.getName(), Utils.createRequestBody(file, contentType, responseCallback)), responseCallback);
        }
        throw new Resources.NotFoundException(file.getPath() + "file 路径无法找到");
    }

    public <T> T rxUploadWithPart(String str, MultipartBody.Part part, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPart(str, str, part, responseCallback);
    }

    public <T> T rxUploadWithPart(Object obj, String str, MultipartBody.Part part, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.uploadFlieWithPart(str, part).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUpload(String str, RequestBody requestBody, MultipartBody.Part part, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUpload(str, str, requestBody, part, responseCallback);
    }

    public <T> T rxUpload(Object obj, String str, RequestBody requestBody, MultipartBody.Part part, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.uploadFlie(str, requestBody, part).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithBody(String str, File file, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithBody(str, str, ContentType.IMAGE, file, responseCallback);
    }

    public <T> T rxUploadWithBody(Object obj, String str, File file, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithBody(obj, str, ContentType.IMAGE, file, responseCallback);
    }

    public <T> T rxUploadWithBody(Object obj, String str, ContentType contentType, File file, ResponseCallback<T, ResponseBody> responseCallback) {
        if (file.exists()) {
            if (responseCallback == null) {
                responseCallback = ResponseCallback.CALLBACK_DEFAULT;
            }
            responseCallback.setTag(obj);
            return rxUploadWithBody(obj, str, Utils.createRequestBody(file, contentType, responseCallback), responseCallback);
        }
        throw new Resources.NotFoundException(file.getPath() + "file 路径无法找到");
    }

    public <T> T rxUploadWithBody(String str, NovateRequestBody novateRequestBody, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithBody(str, str, novateRequestBody, responseCallback);
    }

    public <T> T rxUploadWithBody(Object obj, String str, RequestBody requestBody, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.postRequestBody(str, requestBody).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithPartMapByFile(String str, Map<String, File> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPartMapByFile(str, str, ContentType.IMAGE, map, responseCallback);
    }

    public <T> T rxUploadWithBodyMap(String str, Map<String, RequestBody> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithBodyMap(str, str, map, responseCallback);
    }

    public <T> T rxUploadWithPartMapByFile(Object obj, String str, ContentType contentType, Map<String, File> map, ResponseCallback<T, ResponseBody> responseCallback) {
        if (responseCallback == null) {
            responseCallback = ResponseCallback.CALLBACK_DEFAULT;
        }
        return apiManager.uploadFlieWithPartMap(str, Utils.createParts("image", map, contentType, responseCallback)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithPartListByFile(String str, List<File> list, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPartListByFile(str, str, ContentType.IMAGE, list, responseCallback);
    }

    public <T> T rxUploadWithPartListByFile(Object obj, String str, List<File> list, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPartListByFile(obj, str, ContentType.IMAGE, list, responseCallback);
    }

    public <T> T rxUploadWithPartListByFile(Object obj, String str, ContentType contentType, List<File> list, ResponseCallback<T, ResponseBody> responseCallback) {
        if (responseCallback == null) {
            responseCallback = ResponseCallback.CALLBACK_DEFAULT;
        }
        return apiManager.uploadFlieWithPartList(str, Utils.createPartLists("image", list, contentType, responseCallback)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithBodyMapByFile(String str, Map<String, File> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithBodyMapByFile(str, str, ContentType.IMAGE, map, responseCallback);
    }

    public <T> T rxUploadWithBodyMapByFile(Object obj, String str, Map<String, File> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithBodyMapByFile(obj, str, ContentType.IMAGE, map, responseCallback);
    }

    public <T> T rxUploadWithBodyMapByFile(Object obj, String str, ContentType contentType, Map<String, File> map, ResponseCallback<T, ResponseBody> responseCallback) {
        HashMap hashMap = new HashMap();
        if (responseCallback == null) {
            responseCallback = ResponseCallback.CALLBACK_DEFAULT;
        }
        if (map != null && map.size() > 0) {
            for (String str2 : map.keySet()) {
                File file = map.get(str2);
                if (!FileUtil.exists(file)) {
                    hashMap.put(str2, Utils.createRequestBody(file, contentType, responseCallback));
                } else {
                    throw new Resources.NotFoundException(file.getPath() + "file 路径无法找到");
                }
            }
        }
        return apiManager.uploadFiles(str, hashMap).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithBodyMap(Object obj, String str, Map<String, RequestBody> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.uploadFiles(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxUploadWithPartMap(String str, Map<String, MultipartBody.Part> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxUploadWithPartMap(str, str, map, responseCallback);
    }

    public <T> T rxUploadWithPartMap(Object obj, String str, Map<String, MultipartBody.Part> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.uploadFlieWithPartMap(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxDownload(String str, ResponseCallback responseCallback) {
        return rxDownload(str, str, responseCallback);
    }

    public <T> T rxDownload(Object obj, String str, ResponseCallback responseCallback) {
        if (this.downMaps.get(obj) == null) {
            this.downObservable = apiManager.downloadFile(str);
        } else {
            this.downObservable = this.downMaps.get(obj);
        }
        this.downMaps.put(obj, this.downObservable);
        return this.downObservable.compose(this.schedulersTransformerDown).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback));
    }

    public <T> T rxForm(String str, @FieldMap(encoded = true) Map<String, Object> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxForm(str, str, map, responseCallback);
    }

    public <T> T rxForm(Object obj, String str, @FieldMap(encoded = true) Map<String, Object> map, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.postForm(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback));
    }

    public <T> T rxBody(String str, Object obj, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxBody(str, str, obj, responseCallback);
    }

    public <T> T rxBody(Object obj, String str, Object obj2, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.executePostBody(str, obj2).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T rxJson(String str, String str2, ResponseCallback<T, ResponseBody> responseCallback) {
        return rxJson(str, str, str2, responseCallback);
    }

    public <T> T rxJson(Object obj, String str, String str2, ResponseCallback<T, ResponseBody> responseCallback) {
        return apiManager.postRequestBody(str, Utils.createJson(str2)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new RxSubscriber(obj, responseCallback).addContext(mContext));
    }

    public <T> T executeGet(Class<T> cls, String str, Map<String, Object> map, ResponseCallBack<T> responseCallBack) {
        return apiManager.executeGet(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    private class OndoTransformer implements Observable.Transformer {
        private ResponseCallback callback;
        private Object tag;

        public OndoTransformer(Object obj, ResponseCallback responseCallback) {
            this.tag = obj;
            this.callback = responseCallback;
        }

        public Object call(Object obj) {
            return ((Observable) obj).doOnUnsubscribe(new Action0() {
                /* class com.tamic.novate.Novate.OndoTransformer.C27601 */

                public void call() {
                }
            });
        }
    }

    public <T> Observable.Transformer<NovateResponse<T>, T> handleErrTransformer() {
        Observable.Transformer<NovateResponse<T>, T> transformer = this.exceptTransformer;
        if (transformer != null) {
            return transformer;
        }
        C27595 r0 = new Observable.Transformer() {
            /* class com.tamic.novate.Novate.C27595 */

            public Object call(Object obj) {
                return ((Observable) obj).onErrorResumeNext(new HttpResponseFunc());
            }
        };
        this.exceptTransformer = r0;
        return r0;
    }

    private static class HttpResponseFunc<T> implements Func1<Throwable, Observable<T>> {
        private HttpResponseFunc() {
        }

        public Observable<T> call(Throwable th) {
            return Observable.error(NovateException.handleException(th));
        }
    }

    private class HandleFuc<T> implements Func1<NovateResponse<T>, T> {
        private HandleFuc() {
        }

        public T call(NovateResponse<T> novateResponse) {
            if (novateResponse != null && (novateResponse.getData() != null || novateResponse.getResult() != null)) {
                return novateResponse.getData();
            }
            throw new JsonParseException("后端数据不对");
        }
    }

    public <T> T get(String str, Map<String, Object> map, BaseSubscriber<ResponseBody> baseSubscriber) {
        return apiManager.executeGet(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) baseSubscriber);
    }

    public <T> T post(String str, @FieldMap(encoded = true) Map<String, Object> map, BaseSubscriber<ResponseBody> baseSubscriber) {
        return apiManager.executePost(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) baseSubscriber);
    }

    public <T> T executePost(String str, @FieldMap(encoded = true) Map<String, Object> map, ResponseCallBack<T> responseCallBack) {
        return apiManager.executePost(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public <T> T form(String str, @FieldMap(encoded = true) Map<String, Object> map, Subscriber<ResponseBody> subscriber) {
        return apiManager.postForm(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T executeForm(String str, @FieldMap(encoded = true) Map<String, Object> map, ResponseCallBack<T> responseCallBack) {
        return apiManager.postForm(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public void body(String str, Object obj, Subscriber<ResponseBody> subscriber) {
        apiManager.executePostBody(str, obj).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T executeBody(String str, Object obj, ResponseCallBack<T> responseCallBack) {
        responseCallBack.getClass().getGenericInterfaces();
        return apiManager.executePostBody(str, obj).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public <T> T json(String str, String str2, Subscriber<ResponseBody> subscriber) {
        return apiManager.postRequestBody(str, Utils.createJson(str2)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T executeJson(String str, String str2, ResponseCallBack<T> responseCallBack) {
        return apiManager.postRequestBody(str, Utils.createJson(str2)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public <T> T delete(String str, Map<String, T> map, BaseSubscriber<ResponseBody> baseSubscriber) {
        return apiManager.executeDelete(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) baseSubscriber);
    }

    public <T> T executeDelete(String str, Map<String, T> map, ResponseCallBack<T> responseCallBack) {
        responseCallBack.getClass().getGenericInterfaces();
        return apiManager.executeDelete(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public <T> T put(String str, @FieldMap(encoded = true) Map<String, T> map, BaseSubscriber<ResponseBody> baseSubscriber) {
        return apiManager.executePut(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) baseSubscriber);
    }

    public <T> T executePut(String str, @FieldMap(encoded = true) Map<String, T> map, ResponseCallBack<T> responseCallBack) {
        return apiManager.executePut(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) new NovateSubscriber(mContext, responseCallBack));
    }

    public <T> T test(String str, Map<String, T> map, Subscriber<ResponseBody> subscriber) {
        return apiManager.getTest(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).retryWhen(new RetryWithDelay(3, 3000)).subscribe((Subscriber) subscriber);
    }

    public <T> T upload(String str, RequestBody requestBody, Subscriber<ResponseBody> subscriber) {
        return apiManager.postRequestBody(str, requestBody).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T uploadImage(String str, File file, Subscriber<ResponseBody> subscriber) {
        return apiManager.upLoadImage(str, Utils.createImage(file)).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T uploadFlie(String str, RequestBody requestBody, Subscriber<ResponseBody> subscriber) {
        return apiManager.postRequestBody(str, requestBody).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T uploadFlie(String str, RequestBody requestBody, MultipartBody.Part part, Subscriber<ResponseBody> subscriber) {
        return apiManager.uploadFlie(str, requestBody, part).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T uploadFlies(String str, Map<String, RequestBody> map, Subscriber<ResponseBody> subscriber) {
        return apiManager.uploadFiles(str, map).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T uploadFileWithPartMap(String str, Map<String, RequestBody> map, @Part("file") MultipartBody.Part part, Subscriber<ResponseBody> subscriber) {
        return apiManager.uploadFileWithPartMap(str, map, part).compose(this.schedulersTransformer).compose(handleErrTransformer()).subscribe((Subscriber) subscriber);
    }

    public <T> T download(String str, DownLoadCallBack downLoadCallBack) {
        return download(str, FileUtil.getFileNameWithURL(str), downLoadCallBack);
    }

    public <T> T download(String str, String str2, DownLoadCallBack downLoadCallBack) {
        return download(FileUtil.generateFileKey(str, str2), str, null, str2, downLoadCallBack);
    }

    public <T> T downloadMin(String str, DownLoadCallBack downLoadCallBack) {
        return downloadMin(FileUtil.generateFileKey(str, FileUtil.getFileNameWithURL(str)), str, downLoadCallBack);
    }

    public <T> T downloadMin(String str, String str2, DownLoadCallBack downLoadCallBack) {
        return downloadMin(str, str2, FileUtil.getFileNameWithURL(str2), downLoadCallBack);
    }

    public <T> T downloadMin(String str, String str2, String str3, DownLoadCallBack downLoadCallBack) {
        return downloadMin(str, str2, null, str3, downLoadCallBack);
    }

    public <T> T downloadMin(String str, String str2, String str3, String str4, DownLoadCallBack downLoadCallBack) {
        if (TextUtils.isEmpty(str)) {
            str = FileUtil.generateFileKey(str2, FileUtil.getFileNameWithURL(str2));
        }
        String str5 = str;
        if (this.downMaps.get(str5) == null) {
            this.downObservable = apiManager.downloadSmallFile(str2);
        } else {
            this.downObservable = this.downMaps.get(str5);
        }
        this.downMaps.put(str5, this.downObservable);
        return executeDownload(str5, str2, str3, str4, downLoadCallBack);
    }

    public <T> T download(String str, String str2, String str3, String str4, DownLoadCallBack downLoadCallBack) {
        if (TextUtils.isEmpty(str)) {
            str = FileUtil.generateFileKey(str2, FileUtil.getFileNameWithURL(str2));
        }
        return executeDownload(str, str2, str3, str4, downLoadCallBack);
    }

    private <T> T executeDownload(String str, String str2, String str3, String str4, DownLoadCallBack downLoadCallBack) {
        if (this.downMaps.get(str) == null) {
            this.downObservable = apiManager.downloadFile(str2);
        } else {
            this.downObservable = this.downMaps.get(str);
        }
        this.downMaps.put(str, this.downObservable);
        return this.downObservable.compose(this.schedulersTransformerDown).compose(handleErrTransformer()).subscribe((Subscriber) new DownSubscriber(str, str3, str4, downLoadCallBack, mContext));
    }

    public Builder newBuilder() {
        return new Builder(this);
    }

    public static final class Builder {
        private Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR;
        private Interceptor REWRITE_CACHE_CONTROL_INTERCEPTOR_OFFLINE;
        private List<CallAdapter.Factory> adapterFactories = new ArrayList();
        private String baseUrl;
        private Cache cache = null;
        private long cacheMaxSize = 10485760;
        private int cacheTimeout = Novate.DEFAULT_MAX_STALE;
        private CallAdapter.Factory callAdapterFactory;
        private Call.Factory callFactory;
        private Executor callbackExecutor;
        private List<InputStream> certificateList;
        private CertificatePinner certificatePinner;
        private int connectTimeout = 15;
        private ConnectionPool connectionPool;
        private Context context;
        private List<Converter.Factory> converterFactories = new ArrayList();
        private Converter.Factory converterFactory;
        private NovateCookieManager cookieManager;
        private long default_keep_aliveduration = 8;
        private int default_maxidle_connections = 5;
        private HostnameVerifier hostnameVerifier;
        private File httpCacheDirectory;
        private Boolean isCache = true;
        private Boolean isCookie = false;
        private Boolean isLog = false;
        private Boolean isSkip = false;
        private Proxy proxy;
        private int readTimeout = 15;
        private SSLSocketFactory sslSocketFactory;
        private Object tag;
        private boolean validateEagerly;
        private int writeTimeout = 15;

        public Builder(Context context2) {
            OkHttpClient.Builder unused = Novate.okhttpBuilder = new OkHttpClient.Builder();
            Retrofit.Builder unused2 = Novate.retrofitBuilder = new Retrofit.Builder();
            if (context2 instanceof Activity) {
                this.context = ((Activity) context2).getApplicationContext();
            } else {
                this.context = context2;
            }
        }

        public Builder(Novate novate) {
            this.callFactory = novate.callFactory;
            this.baseUrl = novate.baseUrl;
            this.converterFactories = novate.converterFactories;
            this.adapterFactories = novate.adapterFactories;
            this.callbackExecutor = novate.callbackExecutor;
            this.validateEagerly = novate.validateEagerly;
            Novate.okhttpBuilder.addInterceptor(Novate.okhttpBuilder.interceptors().iterator().next());
        }

        public Builder client(OkHttpClient okHttpClient) {
            Novate.retrofitBuilder.client((OkHttpClient) Utils.checkNotNull(okHttpClient, "client == null"));
            return this;
        }

        public Builder callFactory(Call.Factory factory) {
            this.callFactory = (Call.Factory) Utils.checkNotNull(factory, "factory == null");
            return this;
        }

        public Builder connectTimeout(int i) {
            return connectTimeout(i, TimeUnit.SECONDS);
        }

        public Builder writeTimeout(int i) {
            return writeTimeout(i, TimeUnit.SECONDS);
        }

        public Builder readTimeout(int i) {
            return readTimeout(i, TimeUnit.SECONDS);
        }

        public Builder tag(Object obj) {
            this.tag = obj;
            return this;
        }

        public Builder addLog(boolean z) {
            this.isLog = Boolean.valueOf(z);
            return this;
        }

        public Builder addCookie(boolean z) {
            this.isCookie = Boolean.valueOf(z);
            return this;
        }

        public Builder addCacheMaxSize(int i) {
            this.cacheMaxSize = (long) i;
            return this;
        }

        public Builder addCache(boolean z) {
            this.isCache = Boolean.valueOf(z);
            return this;
        }

        public Builder proxy(Proxy proxy2) {
            this.proxy = proxy2;
            Novate.okhttpBuilder.proxy((Proxy) Utils.checkNotNull(proxy2, "proxy == null"));
            return this;
        }

        public Builder writeTimeout(int i, TimeUnit timeUnit) {
            this.writeTimeout = i;
            if (i >= 0) {
                Novate.okhttpBuilder.writeTimeout((long) i, timeUnit);
            }
            return this;
        }

        public Builder readTimeout(int i, TimeUnit timeUnit) {
            if (this.readTimeout > 0) {
                this.readTimeout = i;
                Novate.okhttpBuilder.readTimeout((long) this.readTimeout, timeUnit);
            }
            return this;
        }

        public Builder connectionPool(ConnectionPool connectionPool2) {
            if (connectionPool2 != null) {
                this.connectionPool = connectionPool2;
                return this;
            }
            throw new NullPointerException("connectionPool == null");
        }

        public Builder connectTimeout(int i, TimeUnit timeUnit) {
            this.readTimeout = Utils.checkDuration("timeout", (long) i, timeUnit);
            if (i >= 0) {
                this.readTimeout = i;
                Novate.okhttpBuilder.connectTimeout((long) this.readTimeout, timeUnit);
            }
            return this;
        }

        public Builder baseUrl(String str) {
            this.baseUrl = (String) Utils.checkNotNull(str, "baseUrl == null");
            return this;
        }

        public Builder addConverterFactory(Converter.Factory factory) {
            this.converterFactory = factory;
            return this;
        }

        public Builder addCallAdapterFactory(CallAdapter.Factory factory) {
            this.callAdapterFactory = factory;
            return this;
        }

        public <T> Builder header(Map<String, T> map) {
            Novate.okhttpBuilder.addInterceptor(new BaseHeaderInterceptor((Map) Utils.checkNotNull(map, "header == null"), AbsRequestInterceptor.Type.UPDATE));
            return this;
        }

        public <T> Builder addHeader(Map<String, T> map) {
            Novate.okhttpBuilder.addInterceptor(new BaseHeaderInterceptor((Map) Utils.checkNotNull(map, "header == null")));
            return this;
        }

        public <T> Builder parameters(Map<String, T> map) {
            Novate.okhttpBuilder.addInterceptor(new BaseParameters((Map) Utils.checkNotNull(map, "parameters == null"), AbsRequestInterceptor.Type.UPDATE));
            return this;
        }

        public <T> Builder addParameters(Map<String, T> map) {
            Novate.okhttpBuilder.addInterceptor(new BaseParameters((Map) Utils.checkNotNull(map, "parameters == null")));
            return this;
        }

        public Builder addInterceptor(Interceptor interceptor) {
            Novate.okhttpBuilder.addInterceptor((Interceptor) Utils.checkNotNull(interceptor, "interceptor == null"));
            return this;
        }

        public Builder callbackExecutor(Executor executor) {
            this.callbackExecutor = (Executor) Utils.checkNotNull(executor, "executor == null");
            return this;
        }

        public Builder validateEagerly(boolean z) {
            this.validateEagerly = z;
            return this;
        }

        public Builder cookieManager(NovateCookieManager novateCookieManager) {
            if (novateCookieManager != null) {
                this.cookieManager = novateCookieManager;
                return this;
            }
            throw new NullPointerException("cookieManager == null");
        }

        public Builder skipSSLSocketFactory(boolean z) {
            this.isSkip = Boolean.valueOf(z);
            return this;
        }

        public Builder addSSLSocketFactory(SSLSocketFactory sSLSocketFactory) {
            if (sSLSocketFactory != null) {
                this.sslSocketFactory = sSLSocketFactory;
                return this;
            }
            throw new NullPointerException("sslSocketFactory == null");
        }

        public Builder addHostnameVerifier(HostnameVerifier hostnameVerifier2) {
            this.hostnameVerifier = hostnameVerifier2;
            return this;
        }

        public Builder addCertificatePinner(CertificatePinner certificatePinner2) {
            this.certificatePinner = certificatePinner2;
            return this;
        }

        public Builder addSSL(String[] strArr, int[] iArr) {
            if (strArr == null) {
                throw new NullPointerException("hosts == null");
            } else if (iArr != null) {
                addSSLSocketFactory(NovateHttpsFactroy.getSSLSocketFactory(this.context, iArr));
                addHostnameVerifier(NovateHttpsFactroy.getHostnameVerifier(strArr));
                return this;
            } else {
                throw new NullPointerException("ids == null");
            }
        }

        public Builder addNetworkInterceptor(Interceptor interceptor) {
            if (interceptor != null) {
                Novate.okhttpBuilder.addNetworkInterceptor(interceptor);
                return this;
            }
            throw new NullPointerException("interceptor == null");
        }

        public Builder addCache(Cache cache2) {
            return addCache(cache2, this.cacheTimeout);
        }

        public Builder addCache(Cache cache2, int i) {
            addCache(cache2, String.format("max-age=%d", Integer.valueOf(i)));
            return this;
        }

        private Builder addCache(Cache cache2, String str) {
            this.REWRITE_CACHE_CONTROL_INTERCEPTOR = new CacheInterceptor(Novate.mContext, str);
            this.REWRITE_CACHE_CONTROL_INTERCEPTOR_OFFLINE = new CacheInterceptorOffline(Novate.mContext, str);
            addNetworkInterceptor(this.REWRITE_CACHE_CONTROL_INTERCEPTOR);
            addNetworkInterceptor(this.REWRITE_CACHE_CONTROL_INTERCEPTOR_OFFLINE);
            addInterceptor(this.REWRITE_CACHE_CONTROL_INTERCEPTOR_OFFLINE);
            this.cache = cache2;
            return this;
        }

        public Novate build() {
            if (this.baseUrl == null) {
                throw new IllegalStateException("Base URL required.");
            } else if (Novate.okhttpBuilder == null) {
                throw new IllegalStateException("okhttpBuilder required.");
            } else if (Novate.retrofitBuilder != null) {
                Context unused = Novate.mContext = this.context;
                ConfigLoader.init(this.context);
                Novate.retrofitBuilder.baseUrl(this.baseUrl);
                if (this.converterFactory == null) {
                    this.converterFactory = GsonConverterFactory.create();
                }
                Novate.retrofitBuilder.addConverterFactory(this.converterFactory);
                if (this.callAdapterFactory == null) {
                    this.callAdapterFactory = RxJavaCallAdapterFactory.create();
                }
                Novate.retrofitBuilder.addCallAdapterFactory(this.callAdapterFactory);
                LogWraper.setDebug(this.isLog.booleanValue());
                if (this.tag != null) {
                    Novate.okhttpBuilder.addInterceptor(new RequestInterceptor(this.tag));
                }
                if (this.isLog.booleanValue()) {
                    Novate.okhttpBuilder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.HEADERS));
                    Novate.okhttpBuilder.addNetworkInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
                }
                if (this.isSkip.booleanValue()) {
                    Novate.okhttpBuilder.sslSocketFactory(NovateHttpsFactroy.getSSLSocketFactory(), NovateHttpsFactroy.creatX509TrustManager());
                    Novate.okhttpBuilder.hostnameVerifier(NovateHttpsFactroy.creatSkipHostnameVerifier());
                }
                if (!this.isSkip.booleanValue() && this.sslSocketFactory != null) {
                    Novate.okhttpBuilder.sslSocketFactory(this.sslSocketFactory);
                }
                if (this.hostnameVerifier != null) {
                    Novate.okhttpBuilder.hostnameVerifier(this.hostnameVerifier);
                }
                if (this.httpCacheDirectory == null) {
                    this.httpCacheDirectory = new File(Novate.mContext.getCacheDir(), Novate.KEY_CACHE);
                }
                if (this.isCache.booleanValue()) {
                    try {
                        if (this.cache == null) {
                            this.cache = new Cache(this.httpCacheDirectory, this.cacheMaxSize);
                        }
                        addCache(this.cache);
                    } catch (Exception e) {
                        Log.e("OKHttp", "Could not create http cache", e);
                    }
                    if (this.cache == null) {
                        this.cache = new Cache(this.httpCacheDirectory, this.cacheMaxSize);
                    }
                }
                if (this.cache != null) {
                    Novate.okhttpBuilder.cache(this.cache);
                }
                if (this.connectionPool == null) {
                    this.connectionPool = new ConnectionPool(this.default_maxidle_connections, this.default_keep_aliveduration, TimeUnit.SECONDS);
                }
                Novate.okhttpBuilder.connectionPool(this.connectionPool);
                if (this.proxy != null) {
                    Novate.okhttpBuilder.proxy(this.proxy);
                }
                if (this.isCookie.booleanValue() && this.cookieManager == null) {
                    Novate.okhttpBuilder.cookieJar(new NovateCookieManager(new CookieCacheImpl(), new SharedPrefsCookiePersistor(this.context)));
                }
                if (this.isCookie.booleanValue()) {
                    Novate.okhttpBuilder.addInterceptor(new ReceivedCookiesInterceptor(this.context));
                    Novate.okhttpBuilder.addInterceptor(new AddCookiesInterceptor(this.context, ""));
                }
                if (this.cookieManager != null) {
                    Novate.okhttpBuilder.cookieJar(this.cookieManager);
                }
                if (this.callFactory != null) {
                    Novate.retrofitBuilder.callFactory(this.callFactory);
                }
                OkHttpClient unused2 = Novate.okHttpClient = Novate.okhttpBuilder.build();
                Novate.retrofitBuilder.client(Novate.okHttpClient);
                Retrofit unused3 = Novate.retrofit = Novate.retrofitBuilder.build();
                Novate.apiManager = (BaseApiService) Novate.retrofit.create(BaseApiService.class);
                return new Novate(this.callFactory, this.baseUrl, Novate.headers, Novate.parameters, Novate.apiManager, this.converterFactories, this.adapterFactories, this.callbackExecutor, this.validateEagerly);
            } else {
                throw new IllegalStateException("retrofitBuilder required.");
            }
        }
    }

    public class RetryWithDelay implements Func1<Observable<? extends Throwable>, Observable<?>> {
        /* access modifiers changed from: private */
        public final int maxRetries;
        /* access modifiers changed from: private */
        public int retryCount;
        /* access modifiers changed from: private */
        public final int retryDelayMillis;

        static /* synthetic */ int access$1504(RetryWithDelay retryWithDelay) {
            int i = retryWithDelay.retryCount + 1;
            retryWithDelay.retryCount = i;
            return i;
        }

        public /* bridge */ /* synthetic */ Object call(Object obj) {
            return call((Observable<? extends Throwable>) ((Observable) obj));
        }

        public RetryWithDelay(int i, int i2) {
            this.maxRetries = i;
            this.retryDelayMillis = i2;
        }

        public Observable<?> call(Observable<? extends Throwable> observable) {
            return observable.flatMap(new Func1<Throwable, Observable<?>>() {
                /* class com.tamic.novate.Novate.RetryWithDelay.C27611 */

                public Observable<?> call(Throwable throwable) {
                    if (RetryWithDelay.access$1504(RetryWithDelay.this) > RetryWithDelay.this.maxRetries) {
                        return Observable.error(throwable);
                    }
                    LogWraper.m6229e("tamic", "Novate get error, it will try after " + RetryWithDelay.this.retryDelayMillis + " millisecond, retry count: " + RetryWithDelay.this.retryCount);
                    return Observable.timer((long) RetryWithDelay.this.retryDelayMillis, TimeUnit.MILLISECONDS);
                }
            });
        }
    }
}
