package com.tamic.novate.callback;

public interface IGenericsConvert<E> {
    <T> T transform(E e, Class<T> cls) throws Exception;
}
