package com.tamic.novate.util;

import android.os.Environment;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;

public final class LogWraper {
    public static final boolean DEBUG_DEBUG = true;
    public static final boolean DEBUG_ERROR = true;
    public static final boolean DEBUG_EXCEPT = true;
    public static final boolean DEBUG_INFO = true;
    public static final boolean DEBUG_PERFORMENCE = true;
    public static final boolean DEBUG_VERBOSE = true;
    public static final boolean DEBUG_WARN = true;
    public static final String LOG_TAG = "down";
    private static String mFolderName = (Environment.getExternalStorageDirectory() + File.separator + "Tamic" + File.separator + LOG_TAG + File.separator + "log" + File.separator);
    private static boolean mIsLogToFile = false;
    private static String mLogFileName = null;
    private static String mLogFileNameLogcat = null;
    private static FileOutputStream mLogcaOutfilestream = null;
    private static FileOutputStream mOutfilestream = null;
    private static boolean sDebug = false;

    private enum LogLevel {
        DEBUG,
        ERROR,
        INFO,
        VERBOSE,
        WARN
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(mFolderName);
        sb.append("fastDownlaoder_log.txt");
        mLogFileName = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(mFolderName);
        sb2.append("fastDownlaoder_lasttime_log.txt");
        mLogFileNameLogcat = sb2.toString();
    }

    private LogWraper() {
    }

    public static void setDebug(boolean z) {
        sDebug = z;
    }

    public static boolean isDebug() {
        return sDebug;
    }

    /* renamed from: d */
    public static void m6225d(String str, String str2) {
        if (sDebug) {
            doLog(LogLevel.DEBUG, str, str2, null);
        }
    }

    /* renamed from: d */
    public static void m6224d(String str) {
        if (sDebug) {
            doLog(LogLevel.DEBUG, LOG_TAG, str, null);
        }
    }

    /* renamed from: d */
    public static void m6227d(String str, Throwable th) {
        if (sDebug) {
            doLog(LogLevel.DEBUG, LOG_TAG, str, th);
        }
    }

    /* renamed from: d */
    public static void m6226d(String str, String str2, Throwable th) {
        if (sDebug) {
            doLog(LogLevel.DEBUG, str, str2, th);
        }
    }

    /* renamed from: p */
    public static void m6234p(String str) {
        if (sDebug) {
            doLog(LogLevel.DEBUG, LOG_TAG, str, null);
        }
    }

    /* renamed from: p */
    public static void m6235p(String str, String str2) {
        if (sDebug) {
            doLog(LogLevel.DEBUG, str, str2, null);
        }
    }

    /* renamed from: e */
    public static void m6229e(String str, String str2) {
        doLog(LogLevel.ERROR, str, str2, null);
    }

    /* renamed from: e */
    public static void m6228e(String str) {
        doLog(LogLevel.ERROR, LOG_TAG, str, null);
    }

    /* renamed from: e */
    public static void m6230e(String str, Throwable th) {
        doLog(LogLevel.ERROR, LOG_TAG, str, th);
    }

    /* renamed from: i */
    public static void m6232i(String str, String str2) {
        if (sDebug) {
            doLog(LogLevel.INFO, str, str2, null);
        }
    }

    /* renamed from: i */
    public static void m6231i(String str) {
        if (sDebug) {
            doLog(LogLevel.INFO, LOG_TAG, str, null);
        }
    }

    /* renamed from: i */
    public static void m6233i(String str, Throwable th) {
        if (sDebug) {
            doLog(LogLevel.INFO, LOG_TAG, str, th);
        }
    }

    /* renamed from: v */
    public static void m6237v(String str, String str2) {
        if (sDebug) {
            doLog(LogLevel.VERBOSE, str, str2, null);
        }
    }

    /* renamed from: v */
    public static void m6236v(String str) {
        if (sDebug) {
            doLog(LogLevel.VERBOSE, LOG_TAG, str, null);
        }
    }

    /* renamed from: v */
    public static void m6238v(String str, Throwable th) {
        if (sDebug) {
            doLog(LogLevel.VERBOSE, LOG_TAG, str, th);
        }
    }

    /* renamed from: w */
    public static void m6239w(String str) {
        if (sDebug) {
            doLog(LogLevel.WARN, LOG_TAG, str, null);
        }
    }

    /* renamed from: w */
    public static void m6240w(String str, String str2) {
        if (sDebug) {
            doLog(LogLevel.WARN, str, str2, null);
        }
    }

    /* renamed from: w */
    public static void m6241w(String str, String str2, Throwable th) {
        if (sDebug) {
            doLog(LogLevel.WARN, str, str2, th);
        }
    }

    /* renamed from: w */
    public static void m6242w(String str, Throwable th) {
        if (sDebug) {
            doLog(LogLevel.WARN, LOG_TAG, str, th);
        }
    }

    public static void printStackTrace(Exception exc) {
        if (sDebug) {
            exc.printStackTrace();
        }
    }

    /* renamed from: com.tamic.novate.util.LogWraper$1 */
    static /* synthetic */ class C27931 {
        static final /* synthetic */ int[] $SwitchMap$com$tamic$novate$util$LogWraper$LogLevel = new int[LogLevel.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.tamic.novate.util.LogWraper$LogLevel[] r0 = com.tamic.novate.util.LogWraper.LogLevel.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.tamic.novate.util.LogWraper.C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel = r0
                int[] r0 = com.tamic.novate.util.LogWraper.C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.tamic.novate.util.LogWraper$LogLevel r1 = com.tamic.novate.util.LogWraper.LogLevel.DEBUG     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.tamic.novate.util.LogWraper.C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel     // Catch:{ NoSuchFieldError -> 0x001f }
                com.tamic.novate.util.LogWraper$LogLevel r1 = com.tamic.novate.util.LogWraper.LogLevel.ERROR     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.tamic.novate.util.LogWraper.C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel     // Catch:{ NoSuchFieldError -> 0x002a }
                com.tamic.novate.util.LogWraper$LogLevel r1 = com.tamic.novate.util.LogWraper.LogLevel.INFO     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.tamic.novate.util.LogWraper.C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.tamic.novate.util.LogWraper$LogLevel r1 = com.tamic.novate.util.LogWraper.LogLevel.VERBOSE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = com.tamic.novate.util.LogWraper.C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.tamic.novate.util.LogWraper$LogLevel r1 = com.tamic.novate.util.LogWraper.LogLevel.WARN     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.util.LogWraper.C27931.<clinit>():void");
        }
    }

    private static void doLog(LogLevel logLevel, String str, String str2, Throwable th) {
        if (str2 == null) {
            str2 = "";
        }
        int i = C27931.$SwitchMap$com$tamic$novate$util$LogWraper$LogLevel[logLevel.ordinal()];
        if (i != 1) {
            if (i != 2) {
                if (i != 3) {
                    if (i != 4) {
                        if (i == 5) {
                            if (th == null) {
                                Log.w(str, str2);
                            } else {
                                Log.w(str, str2, th);
                            }
                        }
                    } else if (th == null) {
                        Log.v(str, str2);
                    } else {
                        Log.v(str, str2, th);
                    }
                } else if (th == null) {
                    Log.i(str, str2);
                } else {
                    Log.i(str, str2, th);
                }
            } else if (th == null) {
                Log.e(str, str2);
            } else {
                Log.e(str, str2, th);
            }
        } else if (th == null) {
            Log.d(str, str2);
        } else {
            Log.d(str, str2, th);
        }
        if (mIsLogToFile) {
            flushToFile(str, str2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0097 A[SYNTHETIC, Splitter:B:37:0x0097] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x009e A[Catch:{ IOException -> 0x00a6 }] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00ae A[SYNTHETIC, Splitter:B:46:0x00ae] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b5 A[Catch:{ IOException -> 0x00bd }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void dumpLogcat() {
        /*
            r0 = 0
            java.lang.String r1 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.lang.String r2 = "mounted"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            if (r1 != 0) goto L_0x001e
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x0019 }
            if (r1 == 0) goto L_0x001d
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x0019 }
            r1.close()     // Catch:{ IOException -> 0x0019 }
            com.tamic.novate.util.LogWraper.mLogcaOutfilestream = r0     // Catch:{ IOException -> 0x0019 }
            goto L_0x001d
        L_0x0019:
            r0 = move-exception
            r0.printStackTrace()
        L_0x001d:
            return
        L_0x001e:
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.lang.String r2 = com.tamic.novate.util.LogWraper.mFolderName     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            if (r2 != 0) goto L_0x002e
            r1.mkdirs()     // Catch:{ Exception -> 0x0090, all -> 0x008d }
        L_0x002e:
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            if (r1 != 0) goto L_0x003b
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.lang.String r2 = com.tamic.novate.util.LogWraper.mLogFileNameLogcat     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            com.tamic.novate.util.LogWraper.mLogcaOutfilestream = r1     // Catch:{ Exception -> 0x0090, all -> 0x008d }
        L_0x003b:
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.lang.String r2 = "logcat -v time -d"
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.io.InputStream r1 = r1.getInputStream()     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            r2.<init>(r1)     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0090, all -> 0x008d }
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
        L_0x0057:
            if (r2 == 0) goto L_0x0074
            java.io.FileOutputStream r3 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            java.lang.String r4 = "UTF-8"
            byte[] r2 = r2.getBytes(r4)     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            r3.write(r2)     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            java.io.FileOutputStream r2 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            java.lang.String r3 = "\n"
            byte[] r3 = r3.getBytes()     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            r2.write(r3)     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            java.lang.String r2 = r1.readLine()     // Catch:{ Exception -> 0x0088, all -> 0x0083 }
            goto L_0x0057
        L_0x0074:
            r1.close()     // Catch:{ IOException -> 0x00a6 }
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x00a6 }
            if (r1 == 0) goto L_0x00aa
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x00a6 }
            r1.close()     // Catch:{ IOException -> 0x00a6 }
            com.tamic.novate.util.LogWraper.mLogcaOutfilestream = r0     // Catch:{ IOException -> 0x00a6 }
            goto L_0x00aa
        L_0x0083:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x00ac
        L_0x0088:
            r2 = move-exception
            r5 = r2
            r2 = r1
            r1 = r5
            goto L_0x0092
        L_0x008d:
            r1 = move-exception
            r2 = r0
            goto L_0x00ac
        L_0x0090:
            r1 = move-exception
            r2 = r0
        L_0x0092:
            r1.printStackTrace()     // Catch:{ all -> 0x00ab }
            if (r2 == 0) goto L_0x009a
            r2.close()     // Catch:{ IOException -> 0x00a6 }
        L_0x009a:
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x00a6 }
            if (r1 == 0) goto L_0x00aa
            java.io.FileOutputStream r1 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x00a6 }
            r1.close()     // Catch:{ IOException -> 0x00a6 }
            com.tamic.novate.util.LogWraper.mLogcaOutfilestream = r0     // Catch:{ IOException -> 0x00a6 }
            goto L_0x00aa
        L_0x00a6:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00aa:
            return
        L_0x00ab:
            r1 = move-exception
        L_0x00ac:
            if (r2 == 0) goto L_0x00b1
            r2.close()     // Catch:{ IOException -> 0x00bd }
        L_0x00b1:
            java.io.FileOutputStream r2 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x00bd }
            if (r2 == 0) goto L_0x00c1
            java.io.FileOutputStream r2 = com.tamic.novate.util.LogWraper.mLogcaOutfilestream     // Catch:{ IOException -> 0x00bd }
            r2.close()     // Catch:{ IOException -> 0x00bd }
            com.tamic.novate.util.LogWraper.mLogcaOutfilestream = r0     // Catch:{ IOException -> 0x00bd }
            goto L_0x00c1
        L_0x00bd:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00c1:
            goto L_0x00c3
        L_0x00c2:
            throw r1
        L_0x00c3:
            goto L_0x00c2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.util.LogWraper.dumpLogcat():void");
    }

    private static void flushToFile(String str, String str2) {
        if (Environment.getExternalStorageState().equals("mounted")) {
            try {
                File file = new File(mFolderName);
                if (!file.exists()) {
                    file.mkdirs();
                }
                if (mOutfilestream == null) {
                    mOutfilestream = new FileOutputStream(mLogFileName);
                }
                mOutfilestream.write((str + " : " + str2).getBytes("UTF-8"));
                mOutfilestream.write("\n".getBytes());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void setWriteToFile(boolean z) {
        mIsLogToFile = z;
    }

    public static void logException(String str, Exception exc) {
        if (exc != null) {
            try {
                if (sDebug) {
                    exc.printStackTrace();
                }
                m6225d(str, "========================= Exception Happened !!================================");
                m6225d(str, exc.getMessage());
                for (StackTraceElement stackTraceElement : exc.getStackTrace()) {
                    m6225d(str, stackTraceElement.toString());
                }
                m6225d(str, "========================= Exception Ended !!================================");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void printInvokeTrace(String str) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        for (int i = 1; i < stackTrace.length; i++) {
            m6224d(str + ":  " + stackTrace[i].toString());
        }
    }

    public static void printInvokeTrace(String str, int i) {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        int min = Math.min(i, stackTrace.length);
        for (int i2 = 1; i2 < min; i2++) {
            m6224d(str + ":  " + stackTrace[i2].toString());
        }
    }
}
