package com.tamic.novate;

import android.content.Context;
import com.tamic.novate.util.LogWraper;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

public class NovateHttpsFactroy {
    private static HostnameVerifier TRUSTED_VERIFIER;

    protected static SSLSocketFactory getSSLSocketFactory(Context context, int[] iArr) {
        if (context != null) {
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                KeyStore instance2 = KeyStore.getInstance(KeyStore.getDefaultType());
                instance2.load(null, null);
                for (int i = 0; i < iArr.length; i++) {
                    InputStream openRawResource = context.getResources().openRawResource(iArr[i]);
                    instance2.setCertificateEntry(String.valueOf(i), instance.generateCertificate(openRawResource));
                    if (openRawResource != null) {
                        openRawResource.close();
                    }
                }
                SSLContext instance3 = SSLContext.getInstance("TLS");
                TrustManagerFactory instance4 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                instance4.init(instance2);
                instance3.init(null, instance4.getTrustManagers(), new SecureRandom());
                return instance3.getSocketFactory();
            } catch (CertificateException e) {
                e.printStackTrace();
                return null;
            } catch (NoSuchAlgorithmException e2) {
                e2.printStackTrace();
                return null;
            } catch (KeyStoreException e3) {
                e3.printStackTrace();
                return null;
            } catch (IOException e4) {
                e4.printStackTrace();
                return null;
            } catch (KeyManagementException e5) {
                e5.printStackTrace();
                return null;
            }
        } else {
            throw new NullPointerException("context == null");
        }
    }

    public static HostnameVerifier getHostnameVerifier(final String[] strArr) {
        return new HostnameVerifier() {
            /* class com.tamic.novate.NovateHttpsFactroy.C27621 */

            public boolean verify(String str, SSLSession sSLSession) {
                boolean z = false;
                for (String str2 : strArr) {
                    if (str2.equalsIgnoreCase(str)) {
                        z = true;
                    }
                }
                return z;
            }
        };
    }

    protected static String getPins(Context context, int i) {
        InputStream openRawResource = context.getResources().openRawResource(i);
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = new byte[4096];
        while (true) {
            try {
                int read = openRawResource.read(bArr);
                if (read == -1) {
                    return stringBuffer.toString();
                }
                stringBuffer.append(new String(bArr, 0, read));
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public static SSLSocketFactory getSSLSocketFactory() {
        try {
            SSLContext instance = SSLContext.getInstance("SSL");
            instance.init(null, creatTrustManager(), new SecureRandom());
            return instance.getSocketFactory();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static SSLSocketFactory creatSSLSocketFactory(Context context, String str) {
        InputStream inputStream;
        if (context != null) {
            try {
                inputStream = context.getResources().getAssets().open(str);
            } catch (IOException e) {
                e.printStackTrace();
                LogWraper.m6229e(Novate.TAG, "SSLSocketFactory open file error :" + e.toString());
                inputStream = null;
            }
            try {
                Certificate generateCertificate = CertificateFactory.getInstance("X.509").generateCertificate(inputStream);
                KeyStore instance = KeyStore.getInstance(KeyStore.getDefaultType());
                instance.load(null, null);
                instance.setCertificateEntry(str, generateCertificate);
                TrustManagerFactory instance2 = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                instance2.init(instance);
                SSLContext instance3 = SSLContext.getInstance("TLS");
                instance3.init(null, instance2.getTrustManagers(), new SecureRandom());
                return instance3.getSocketFactory();
            } catch (Exception e2) {
                LogWraper.m6229e(Novate.TAG, "SSLSocketFactory generate error :" + e2.toString());
                return null;
            }
        } else {
            throw new NullPointerException("context == null");
        }
    }

    public static HostnameVerifier creatSkipHostnameVerifier() {
        return new HostnameVerifier() {
            /* class com.tamic.novate.NovateHttpsFactroy.C27632 */

            public boolean verify(String str, SSLSession sSLSession) {
                return true;
            }
        };
    }

    public static X509TrustManager creatX509TrustManager() {
        return new X509TrustManager() {
            /* class com.tamic.novate.NovateHttpsFactroy.C27643 */

            public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
            }

            public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        };
    }

    public static TrustManager[] creatTrustManager() {
        return new TrustManager[]{new X509TrustManager() {
            /* class com.tamic.novate.NovateHttpsFactroy.C27654 */

            public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
            }

            public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
            }

            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }
        }};
    }
}
