package com.tamic.novate;

import java.util.List;
import java.util.Map;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import p042rx.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface BaseApiService {
    @Streaming
    @GET
    Observable<ResponseBody> downloadFile(@Url String str);

    @GET
    Observable<ResponseBody> downloadSmallFile(@Url String str);

    @DELETE
    <T> Observable<ResponseBody> executeDelete(@Url String str, @QueryMap Map<String, Object> map);

    @GET
    <T> Observable<ResponseBody> executeGet(@Url String str, @QueryMap Map<String, Object> map);

    @FormUrlEncoded
    @POST
    <T> Observable<ResponseBody> executePost(@Url String str, @FieldMap Map<String, Object> map);

    @POST
    Observable<ResponseBody> executePostBody(@Url String str, @Body Object obj);

    @PUT
    <T> Observable<ResponseBody> executePut(@Url String str, @FieldMap Map<String, Object> map);

    @GET
    <T> Observable<ResponseBody> getTest(@Url String str, @QueryMap Map<String, Object> map);

    @FormUrlEncoded
    @POST
    <T> Observable<ResponseBody> postForm(@Url String str, @FieldMap Map<String, Object> map);

    @POST
    Observable<ResponseBody> postRequestBody(@Url String str, @Body RequestBody requestBody);

    @POST
    @Multipart
    Observable<ResponseBody> upLoadImage(@Url String str, @Part("image\"; filename=\"image.jpg") RequestBody requestBody);

    @POST
    Observable<ResponseBody> uploadFile(@Url String str, @Body RequestBody requestBody);

    @POST
    @Multipart
    Observable<ResponseBody> uploadFileWithPartMap(@Url String str, @PartMap Map<String, RequestBody> map, @Part MultipartBody.Part part);

    @POST
    Observable<ResponseBody> uploadFiles(@Url String str, @Body Map<String, RequestBody> map);

    @POST
    @Multipart
    Observable<ResponseBody> uploadFlie(@Url String str, @Part("description") RequestBody requestBody, @Part("image\"; filename=\"image.jpg") MultipartBody.Part part);

    @POST
    @Multipart
    Observable<ResponseBody> uploadFlieWithPart(@Url String str, @Part MultipartBody.Part part);

    @POST
    @Multipart
    Observable<ResponseBody> uploadFlieWithPartList(@Url String str, @Part List<MultipartBody.Part> list);

    @POST
    @Multipart
    Observable<ResponseBody> uploadFlieWithPartMap(@Url String str, @PartMap Map<String, MultipartBody.Part> map);
}
