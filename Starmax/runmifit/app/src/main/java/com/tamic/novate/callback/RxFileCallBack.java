package com.tamic.novate.callback;

import android.os.Handler;
import android.os.Looper;
import com.tamic.novate.Throwable;
import com.tamic.novate.util.FileUtil;
import com.tamic.novate.util.LogWraper;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import okhttp3.Call;
import okhttp3.ResponseBody;

public abstract class RxFileCallBack extends ResponseCallback<File, ResponseBody> {
    private String destFileDir;
    private String destFileName;
    FileOutputStream fos;
    private int interval;

    /* renamed from: is */
    InputStream f5276is;
    private int progress;
    private long sum;
    private int updateCount;

    public abstract void onNext(Object obj, File file);

    public abstract void onProgress(Object obj, float f, long j, long j2);

    public RxFileCallBack() {
        this.destFileName = FileUtil.DEFAULT_FILENAME;
        this.fos = null;
        this.f5276is = null;
        this.sum = 0;
        this.updateCount = 0;
        this.interval = 1;
        this.progress = 0;
    }

    public RxFileCallBack(String str) {
        this("", str);
    }

    public RxFileCallBack(String str, String str2) {
        this.destFileName = FileUtil.DEFAULT_FILENAME;
        this.fos = null;
        this.f5276is = null;
        this.sum = 0;
        this.updateCount = 0;
        this.interval = 1;
        this.progress = 0;
        this.destFileDir = str;
        this.destFileName = str2;
    }

    public File onHandleResponse(ResponseBody responseBody) throws Exception {
        return transform(responseBody);
    }

    public File transform(ResponseBody responseBody) throws Exception {
        return onNextFile(responseBody);
    }

    public File onNextFile(ResponseBody responseBody) throws Exception {
        byte[] bArr = new byte[2048];
        try {
            this.f5276is = responseBody.byteStream();
            long contentLength = responseBody.contentLength();
            if (contentLength < 2048) {
                this.interval = 0;
            } else {
                this.interval = 1;
            }
            File createDownloadFile = FileUtil.createDownloadFile(this.destFileDir, this.destFileName);
            FileOutputStream fileOutputStream = new FileOutputStream(createDownloadFile);
            while (true) {
                int read = this.f5276is.read(bArr);
                if (read != -1) {
                    this.sum += (long) read;
                    fileOutputStream.write(bArr, 0, read);
                    final long j = this.sum;
                    if (contentLength != -1) {
                        if (contentLength != 0) {
                            this.progress = (int) ((100 * j) / contentLength);
                            LogWraper.m6225d(this.TAG, "file download progress : " + this.progress);
                            if (this.updateCount != 0 || this.progress >= this.updateCount) {
                                this.updateCount += this.interval;
                                this.handler = new Handler(Looper.getMainLooper());
                                final int i = this.progress;
                                final long j2 = contentLength;
                                this.handler.post(new Runnable() {
                                    /* class com.tamic.novate.callback.RxFileCallBack.C27761 */

                                    public void run() {
                                        RxFileCallBack rxFileCallBack = RxFileCallBack.this;
                                        rxFileCallBack.onProgress(rxFileCallBack.tag, (float) i, j, j2);
                                    }
                                });
                            }
                        }
                    }
                    this.progress = 100;
                    LogWraper.m6225d(this.TAG, "file download progress : " + this.progress);
                    if (this.updateCount != 0) {
                    }
                    this.updateCount += this.interval;
                    this.handler = new Handler(Looper.getMainLooper());
                    final int i2 = this.progress;
                    final long j22 = contentLength;
                    this.handler.post(new Runnable() {
                        /* class com.tamic.novate.callback.RxFileCallBack.C27761 */

                        public void run() {
                            RxFileCallBack rxFileCallBack = RxFileCallBack.this;
                            rxFileCallBack.onProgress(rxFileCallBack.tag, (float) i2, j, j22);
                        }
                    });
                } else {
                    fileOutputStream.flush();
                    return createDownloadFile;
                }
            }
        } finally {
            onRelease();
        }
    }

    public void onNext(final Object obj, Call call, final File file) {
        this.handler.post(new Runnable() {
            /* class com.tamic.novate.callback.RxFileCallBack.C27772 */

            public void run() {
                RxFileCallBack.this.onNext(obj, file);
            }
        });
    }

    public void onRelease() {
        super.onRelease();
        InputStream inputStream = this.f5276is;
        if (inputStream != null) {
            try {
                inputStream.close();
                if (this.fos != null) {
                    this.fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
                onError(this.tag, new Throwable(e, -100, "file write io Exception"));
            }
        }
    }
}
