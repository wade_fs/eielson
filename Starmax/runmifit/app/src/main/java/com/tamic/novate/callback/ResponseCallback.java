package com.tamic.novate.callback;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.tamic.novate.Throwable;
import com.tamic.novate.exception.NovateException;
import com.tamic.novate.util.Utils;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import okhttp3.ResponseBody;

public abstract class ResponseCallback<T, E> implements Callback, IGenericsConvert<E> {
    public static ResponseCallback CALLBACK_DEFAULT = new ResponseCallback() {
        /* class com.tamic.novate.callback.ResponseCallback.C27693 */

        public void onCancel(Object obj, Throwable throwable) {
        }

        public Object onHandleResponse(ResponseBody responseBody) throws Exception {
            return responseBody;
        }

        public void onNext(Object obj, Call call, Object obj2) {
        }

        public Object transform(Object obj, Class cls) {
            return obj;
        }

        public void onFailure(Call call, IOException iOException) {
            onError(call.request().tag(), NovateException.handleException(iOException));
        }

        public void onResponse(Call call, Response response) throws IOException {
            try {
                onHandleResponse(response.body());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public void onError(Object obj, Throwable throwable) {
            onRelease();
        }

        public void onCompleted(Object obj) {
            ResponseCallback.super.onCompleted(obj);
            onRelease();
        }
    };
    protected String TAG = "novateCallback";
    private Context context;
    protected Handler handler;
    protected Object tag;

    public boolean isReponseOk(Object obj, ResponseBody responseBody) {
        return true;
    }

    public abstract void onCancel(Object obj, Throwable throwable);

    public void onCompleted(Object obj) {
    }

    public abstract void onError(Object obj, Throwable throwable);

    public abstract T onHandleResponse(ResponseBody responseBody) throws Exception;

    public abstract void onNext(Object obj, Call call, T t);

    public void onProgress(Object obj, float f, long j, long j2) {
    }

    public void onProgress(Object obj, int i, long j, long j2, long j3) {
    }

    public void onRelease() {
    }

    public void onStart(Object obj) {
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [T, E] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T transform(E r1, java.lang.Class<T> r2) throws java.lang.Exception {
        /*
            r0 = this;
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.callback.ResponseCallback.transform(java.lang.Object, java.lang.Class):java.lang.Object");
    }

    public Handler getHandler() {
        if (this.handler != null) {
            return null;
        }
        Handler handler2 = new Handler(Looper.getMainLooper());
        this.handler = handler2;
        return handler2;
    }

    public void setHandler(Handler handler2) {
        this.handler = handler2;
    }

    public ResponseCallback(Object obj) {
        this.tag = obj;
    }

    public Object getTag() {
        return this.tag;
    }

    public void setTag(Object obj) {
        this.tag = obj;
    }

    public ResponseCallback() {
        if (this.handler == null) {
            this.handler = new Handler(Looper.getMainLooper());
        }
    }

    /* access modifiers changed from: protected */
    public void finalOnError(final Exception exc) {
        if (Utils.checkMain()) {
            onError(this.tag, NovateException.handleException(exc));
        } else {
            this.handler.post(new Runnable() {
                /* class com.tamic.novate.callback.ResponseCallback.C27671 */

                public void run() {
                    ResponseCallback responseCallback = ResponseCallback.this;
                    responseCallback.onError(responseCallback.tag, NovateException.handleException(exc));
                }
            });
        }
    }

    public void onFailure(final Call call, final IOException iOException) {
        if (Utils.checkMain()) {
            onError(this.tag, NovateException.handleException(iOException));
        } else {
            this.handler.post(new Runnable() {
                /* class com.tamic.novate.callback.ResponseCallback.C27682 */

                public void run() {
                    ResponseCallback.this.onError(call.request().tag(), NovateException.handleException(iOException));
                }
            });
        }
    }

    public void onResponse(Call call, Response response) throws IOException {
        if (call.isCanceled()) {
            onCancel(call.request().tag(), new Throwable(null, -200, "已取消"));
        }
        this.tag = call.request().tag();
        if (isReponseOk(this.tag, response.body())) {
            try {
                onHandleResponse(response.body());
            } catch (Exception e) {
                e.printStackTrace();
                onError(this.tag, NovateException.handleException(e));
            }
        }
    }
}
