package com.tamic.novate.callback;

import android.content.Context;
import android.util.Log;
import com.google.common.net.HttpHeaders;
import java.io.File;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.ResponseBody;
import okio.BufferedSink;

public abstract class RxDownCallback extends ResponseCallback<File, ResponseBody> {
    protected static final int REFRESH_INTEVAL = 1000;
    private final String RANGE = HttpHeaders.RANGE;
    private final String TAG = "DownLoadService";
    private Context context;
    private String destFileDir;
    private String destFileName;
    protected long mBytesThistime;
    protected long mLastRefreshTime;
    long mStarttime = 0;
    protected BufferedSink sink;

    public abstract void onNext(Object obj, File file);

    public abstract void onProgress(Object obj, int i, long j, long j2, long j3);

    public File onHandleResponse(ResponseBody responseBody) throws Exception {
        return transform(responseBody);
    }

    public File transform(ResponseBody responseBody) throws Exception {
        return onNextFile(responseBody, true);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r10.sink.writeAll(r0);
        r10.sink.flush();
        r10.sink.close();
        r10.handler.post(new com.tamic.novate.callback.RxDownCallback.C27712(r10));
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.File onNextFile(okhttp3.ResponseBody r23, boolean r24) throws java.io.EOFException {
        /*
            r22 = this;
            r10 = r22
            java.lang.String r11 = "DownLoadService"
            java.lang.String r0 = r10.destFileDir
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 == 0) goto L_0x0014
            android.content.Context r0 = r10.context
            java.lang.String r0 = com.tamic.novate.util.FileUtil.getBasePath(r0)
            r10.destFileDir = r0
        L_0x0014:
            long r13 = r23.contentLength()     // Catch:{ IOException -> 0x0110 }
            okio.BufferedSource r0 = r23.source()     // Catch:{ IOException -> 0x0110 }
            java.lang.String r1 = r10.destFileDir     // Catch:{ IOException -> 0x0110 }
            java.lang.String r2 = r10.destFileName     // Catch:{ IOException -> 0x0110 }
            java.io.File r15 = com.tamic.novate.util.FileUtil.createDownloadFile(r1, r2)     // Catch:{ IOException -> 0x0110 }
            okio.Sink r1 = okio.Okio.sink(r15)     // Catch:{ IOException -> 0x0110 }
            okio.BufferedSink r1 = okio.Okio.buffer(r1)     // Catch:{ IOException -> 0x0110 }
            r10.sink = r1     // Catch:{ IOException -> 0x0110 }
            r16 = 0
            r1 = r16
        L_0x0032:
            okio.BufferedSink r3 = r10.sink     // Catch:{ IOException -> 0x0110 }
            okio.Buffer r3 = r3.buffer()     // Catch:{ IOException -> 0x0110 }
            r4 = 2048(0x800, double:1.0118E-320)
            long r3 = r0.read(r3, r4)     // Catch:{ IOException -> 0x0110 }
            r5 = -1
            int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r7 == 0) goto L_0x00d7
            long r8 = r1 + r3
            long r1 = r10.mBytesThistime     // Catch:{ IOException -> 0x0110 }
            long r1 = r1 + r8
            r10.mBytesThistime = r1     // Catch:{ IOException -> 0x0110 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0110 }
            long r5 = r10.mStarttime     // Catch:{ IOException -> 0x0110 }
            r18 = 1000(0x3e8, double:4.94E-321)
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x0060
            long r5 = r8 * r18
            r20 = r13
            long r12 = r10.mStarttime     // Catch:{ IOException -> 0x0110 }
            long r1 = r1 - r12
            long r5 = r5 / r1
            goto L_0x0064
        L_0x0060:
            r20 = r13
            r5 = r16
        L_0x0064:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0110 }
            r1.<init>()     // Catch:{ IOException -> 0x0110 }
            java.lang.String r2 = "fileSize: "
            r1.append(r2)     // Catch:{ IOException -> 0x0110 }
            r1.append(r3)     // Catch:{ IOException -> 0x0110 }
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0110 }
            com.tamic.novate.util.LogWraper.m6225d(r11, r1)     // Catch:{ IOException -> 0x0110 }
            r1 = 100
            long r3 = r3 * r1
            long r3 = r3 / r20
            int r3 = (int) r3     // Catch:{ IOException -> 0x0110 }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ IOException -> 0x0110 }
            long r12 = r10.mLastRefreshTime     // Catch:{ IOException -> 0x0110 }
            long r1 = r1 - r12
            int r4 = (r1 > r18 ? 1 : (r1 == r18 ? 0 : -1))
            if (r4 < 0) goto L_0x009f
            android.os.Handler r12 = r10.handler     // Catch:{ IOException -> 0x0110 }
            com.tamic.novate.callback.RxDownCallback$1 r13 = new com.tamic.novate.callback.RxDownCallback$1     // Catch:{ IOException -> 0x0110 }
            r1 = r13
            r2 = r22
            r4 = r5
            r6 = r8
            r23 = r15
            r14 = r8
            r8 = r20
            r1.<init>(r3, r4, r6, r8)     // Catch:{ IOException -> 0x0110 }
            r12.post(r13)     // Catch:{ IOException -> 0x0110 }
            goto L_0x00a2
        L_0x009f:
            r23 = r15
            r14 = r8
        L_0x00a2:
            int r1 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r1 <= 0) goto L_0x00cf
            double r1 = (double) r14
            r3 = r20
            double r5 = (double) r3
            r7 = 4609434218613702656(0x3ff8000000000000, double:1.5)
            java.lang.Double.isNaN(r5)
            double r5 = r5 * r7
            int r7 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r7 <= 0) goto L_0x00d1
            okio.BufferedSink r1 = r10.sink     // Catch:{ IOException -> 0x0110 }
            r1.writeAll(r0)     // Catch:{ IOException -> 0x0110 }
            okio.BufferedSink r1 = r10.sink     // Catch:{ IOException -> 0x0110 }
            r1.flush()     // Catch:{ IOException -> 0x0110 }
            okio.BufferedSink r1 = r10.sink     // Catch:{ IOException -> 0x0110 }
            r1.close()     // Catch:{ IOException -> 0x0110 }
            android.os.Handler r1 = r10.handler     // Catch:{ IOException -> 0x0110 }
            com.tamic.novate.callback.RxDownCallback$2 r2 = new com.tamic.novate.callback.RxDownCallback$2     // Catch:{ IOException -> 0x0110 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0110 }
            r1.post(r2)     // Catch:{ IOException -> 0x0110 }
            goto L_0x00db
        L_0x00cf:
            r3 = r20
        L_0x00d1:
            r1 = r14
            r15 = r23
            r13 = r3
            goto L_0x0032
        L_0x00d7:
            r3 = r13
            r23 = r15
            r14 = r1
        L_0x00db:
            okio.BufferedSink r1 = r10.sink     // Catch:{ IOException -> 0x0110 }
            r1.writeAll(r0)     // Catch:{ IOException -> 0x0110 }
            okio.BufferedSink r0 = r10.sink     // Catch:{ IOException -> 0x0110 }
            r0.flush()     // Catch:{ IOException -> 0x0110 }
            okio.BufferedSink r0 = r10.sink     // Catch:{ IOException -> 0x0110 }
            r0.close()     // Catch:{ IOException -> 0x0110 }
            int r0 = (r14 > r3 ? 1 : (r14 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x00ff
            android.os.Handler r0 = r10.handler     // Catch:{ IOException -> 0x0110 }
            com.tamic.novate.callback.RxDownCallback$3 r1 = new com.tamic.novate.callback.RxDownCallback$3     // Catch:{ IOException -> 0x0110 }
            r2 = r23
            r1.<init>(r2)     // Catch:{ IOException -> 0x0110 }
            r0.post(r1)     // Catch:{ IOException -> 0x0110 }
            r22.close()
            r1 = 0
            return r1
        L_0x00ff:
            android.os.Handler r0 = r10.handler     // Catch:{ IOException -> 0x0110 }
            com.tamic.novate.callback.RxDownCallback$4 r1 = new com.tamic.novate.callback.RxDownCallback$4     // Catch:{ IOException -> 0x0110 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0110 }
            r0.post(r1)     // Catch:{ IOException -> 0x0110 }
        L_0x0109:
            r22.close()
            r1 = 0
            return r1
        L_0x010e:
            r0 = move-exception
            goto L_0x013b
        L_0x0110:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x010e }
            java.lang.String r1 = r0.getMessage()     // Catch:{ all -> 0x010e }
            java.lang.String r2 = "No space left on device"
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x010e }
            if (r1 == 0) goto L_0x0130
            java.lang.String r1 = "SD卡满了"
            com.tamic.novate.util.LogWraper.m6225d(r11, r1)     // Catch:{ all -> 0x010e }
            android.os.Handler r1 = r10.handler     // Catch:{ all -> 0x010e }
            com.tamic.novate.callback.RxDownCallback$5 r2 = new com.tamic.novate.callback.RxDownCallback$5     // Catch:{ all -> 0x010e }
            r2.<init>(r0)     // Catch:{ all -> 0x010e }
            r1.post(r2)     // Catch:{ all -> 0x010e }
            goto L_0x0109
        L_0x0130:
            android.os.Handler r1 = r10.handler     // Catch:{ all -> 0x010e }
            com.tamic.novate.callback.RxDownCallback$6 r2 = new com.tamic.novate.callback.RxDownCallback$6     // Catch:{ all -> 0x010e }
            r2.<init>(r0)     // Catch:{ all -> 0x010e }
            r1.post(r2)     // Catch:{ all -> 0x010e }
            goto L_0x0109
        L_0x013b:
            r22.close()
            goto L_0x0140
        L_0x013f:
            throw r0
        L_0x0140:
            goto L_0x013f
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.callback.RxDownCallback.onNextFile(okhttp3.ResponseBody, boolean):java.io.File");
    }

    public void onRelease() {
        super.onRelease();
        close();
    }

    /* access modifiers changed from: protected */
    public void close() {
        BufferedSink bufferedSink = this.sink;
        if (bufferedSink != null) {
            try {
                bufferedSink.close();
                this.sink = null;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("DownLoadService", "sink  is already closed!");
            }
        }
    }

    public void onNext(Object obj, Call call, File file) {
        onNext(obj, file);
    }
}
