package com.tamic.novate;

import android.content.Context;
import com.tamic.novate.callback.ResponseCallback;
import com.tamic.novate.exception.NovateException;
import okhttp3.ResponseBody;

public class RxSubscriber<T, E> extends BaseSubscriber<ResponseBody> {
    private ResponseCallback<T, E> callBack;
    private Context context;
    private Object tag = null;

    public RxSubscriber(Object obj, ResponseCallback<T, E> responseCallback) {
        if (responseCallback == null) {
            this.callBack = ResponseCallback.CALLBACK_DEFAULT;
        } else {
            this.callBack = responseCallback;
        }
        this.callBack.setTag(obj);
        this.tag = obj;
    }

    public Context context() {
        return this.context;
    }

    public RxSubscriber addContext(Context context2) {
        this.context = context2;
        return this;
    }

    public void onStart() {
        super.onStart();
        ResponseCallback<T, E> responseCallback = this.callBack;
        if (responseCallback != null) {
            responseCallback.onStart(this.tag);
        }
    }

    public void onCompleted() {
        ResponseCallback<T, E> responseCallback = this.callBack;
        if (responseCallback != null) {
            responseCallback.onCompleted(this.tag);
            this.callBack.onRelease();
        }
    }

    public void onError(Throwable throwable) {
        ResponseCallback<T, E> responseCallback = this.callBack;
        if (responseCallback != null) {
            responseCallback.onError(this.tag, throwable);
            this.callBack.onRelease();
        }
    }

    public void onNext(ResponseBody responseBody) {
        try {
            if (this.callBack.isReponseOk(this.tag, responseBody)) {
                this.callBack.onNext(this.tag, null, this.callBack.onHandleResponse(responseBody));
            }
        } catch (Exception e) {
            e.printStackTrace();
            ResponseCallback<T, E> responseCallback = this.callBack;
            if (responseCallback != null) {
                responseCallback.onError(this.tag, NovateException.handleException(e));
            }
        }
    }
}
