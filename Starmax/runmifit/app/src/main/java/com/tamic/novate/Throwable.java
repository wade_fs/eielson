package com.tamic.novate;

public class Throwable extends Exception {
    private int code;
    private String message;

    public Throwable(Throwable th, int i) {
        super(th);
        this.code = i;
    }

    public Throwable(Throwable th, int i, String str) {
        super(th);
        this.code = i;
        this.message = str;
    }

    public void setCode(int i) {
        this.code = i;
    }

    public int getCode() {
        return this.code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }
}
