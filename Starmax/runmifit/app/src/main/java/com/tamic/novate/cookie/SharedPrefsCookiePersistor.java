package com.tamic.novate.cookie;

import android.content.Context;
import android.content.SharedPreferences;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import okhttp3.Cookie;

public class SharedPrefsCookiePersistor implements CookiePersistor {
    private static final String COOKIE_PREFS = "Novate_Cookies_Prefs";
    private static final String LOG_TAG = "Novate>>PersistentCookieStore";
    private final SharedPreferences sharedPreferences;

    public SharedPrefsCookiePersistor(Context context) {
        this(context.getSharedPreferences(COOKIE_PREFS, 0));
    }

    public SharedPrefsCookiePersistor(SharedPreferences sharedPreferences2) {
        this.sharedPreferences = sharedPreferences2;
    }

    public List<Cookie> loadAll() {
        ArrayList arrayList = new ArrayList(this.sharedPreferences.getAll().size());
        for (Map.Entry<String, ?> entry : this.sharedPreferences.getAll().entrySet()) {
            arrayList.add(new SerializableCookie().decode((String) entry.getValue()));
        }
        return arrayList;
    }

    public void saveAll(Collection<Cookie> collection) {
        SharedPreferences.Editor edit = this.sharedPreferences.edit();
        for (Cookie cookie : collection) {
            edit.putString(createCookieKey(cookie), new SerializableCookie().encode(cookie));
        }
        edit.commit();
    }

    public void removeAll(Collection<Cookie> collection) {
        SharedPreferences.Editor edit = this.sharedPreferences.edit();
        for (Cookie cookie : collection) {
            edit.remove(createCookieKey(cookie));
        }
        edit.commit();
    }

    private static String createCookieKey(Cookie cookie) {
        StringBuilder sb = new StringBuilder();
        sb.append(cookie.secure() ? "https" : "http");
        sb.append("://");
        sb.append(cookie.domain());
        sb.append(cookie.path());
        sb.append("|");
        sb.append(cookie.name());
        return sb.toString();
    }

    public void clear() {
        this.sharedPreferences.edit().clear().commit();
    }
}
