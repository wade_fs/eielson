package com.tamic.novate.callback;

import android.text.TextUtils;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tamic.novate.Novate;
import com.tamic.novate.NovateResponse;
import com.tamic.novate.util.LogWraper;
import com.tamic.novate.util.Utils;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import okhttp3.Call;
import okhttp3.ResponseBody;
import org.json.JSONObject;

public abstract class RxGenericsCallback<T, E> extends ResponseCallback<T, E> {
    protected int code = -1;
    protected T dataResponse = null;
    protected String dataStr = "";
    protected String msg = "";

    public abstract void onNext(Object obj, int i, String str, T t);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tamic.novate.callback.RxGenericsCallback.transform(java.lang.String, java.lang.Class):T
     arg types: [java.lang.String, java.lang.Class<java.lang.String>]
     candidates:
      com.tamic.novate.callback.ResponseCallback.transform(java.lang.Object, java.lang.Class):T
      com.tamic.novate.callback.IGenericsConvert.transform(java.lang.Object, java.lang.Class):T
      com.tamic.novate.callback.RxGenericsCallback.transform(java.lang.String, java.lang.Class):T */
    public T onHandleResponse(ResponseBody responseBody) throws Exception {
        Class<String> cls = (Class) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        if (cls == String.class) {
            return new String(responseBody.bytes());
        }
        String str = new String(responseBody.bytes());
        LogWraper.m6225d(Novate.TAG, str);
        return transform(str, (Class) cls);
    }

    public void onNext(final Object obj, Call call, T t) {
        if (Utils.checkMain()) {
            onNext(obj, this.code, this.msg, this.dataResponse);
        } else {
            this.handler.post(new Runnable() {
                /* class com.tamic.novate.callback.RxGenericsCallback.C27781 */

                public void run() {
                    RxGenericsCallback rxGenericsCallback = RxGenericsCallback.this;
                    rxGenericsCallback.onNext(obj, rxGenericsCallback.code, RxGenericsCallback.this.msg, RxGenericsCallback.this.dataResponse);
                }
            });
        }
    }

    public T transform(String str, Class cls) throws ClassCastException {
        if (cls == NovateResponse.class) {
            return new Gson().fromJson(str, cls);
        }
        LogWraper.m6229e(this.TAG, str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.code = jSONObject.optInt("code");
            this.msg = jSONObject.optString("msg");
            if (TextUtils.isEmpty(this.msg)) {
                this.msg = jSONObject.optString("error");
            }
            if (TextUtils.isEmpty(this.msg)) {
                this.msg = jSONObject.optString("message");
            }
            this.dataStr = jSONObject.opt("data").toString();
            if (TextUtils.isEmpty(this.dataStr)) {
                this.dataStr = jSONObject.opt("result").toString();
            }
            if (this.dataStr.charAt(0) == '{') {
                this.dataResponse = new Gson().fromJson(this.dataStr, cls);
            } else if (this.dataStr.charAt(0) == '[') {
                this.dataStr = jSONObject.optJSONArray("data").toString();
                if (TextUtils.isEmpty(this.dataStr)) {
                    this.dataStr = jSONObject.optJSONArray("result").toString();
                }
                this.dataResponse = new Gson().fromJson(this.dataStr, new TypeToken<List<T>>() {
                    /* class com.tamic.novate.callback.RxGenericsCallback.C27792 */
                }.getType());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.dataResponse;
    }
}
