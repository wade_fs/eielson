package com.tamic.novate.config;

import android.content.Context;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.tamic.novate.Novate;
import com.tamic.novate.util.FileUtil;
import com.tamic.novate.util.LogWraper;
import java.util.HashMap;

public class ConfigLoader {
    private static final String CONFIG_NAME = "novate-config.json";
    private static Context appContext;
    private static Config config;
    private static ConfigLoader sInstance;

    public static Context getContext() {
        return appContext;
    }

    public static void init(Context context) {
        appContext = context;
    }

    public static boolean checkSucess(Context context, int i) {
        if (loadConfig(context) == null) {
            return true;
        }
        LogWraper.m6237v(Novate.TAG, "web :" + i + ">>>>>>>>>>>>isOk：" + config.getSucessCode().contains(String.valueOf(i)));
        return config.getSucessCode().contains(String.valueOf(i));
    }

    public static Config loadConfig(Context context) {
        Config config2 = config;
        if (config2 != null) {
            return config2;
        }
        String loadFromAssets = FileUtil.loadFromAssets(context, CONFIG_NAME);
        if (TextUtils.isEmpty(loadFromAssets)) {
            LogWraper.m6229e(Novate.TAG, "缺乏默认配置 <novate-config.json>文件，请加入");
            return null;
        }
        try {
            config = (Config) new Gson().fromJson(loadFromAssets, Config.class);
            Config config3 = (Config) new Gson().fromJson(loadFromAssets, Config.class);
            config = config3;
            return config3;
        } catch (JsonSyntaxException unused) {
            LogWraper.m6229e(Novate.TAG, "loaderConfig 配置数据无法解析: 请正确配置 <novate-config.json>文件");
            return null;
        }
    }

    public static boolean isFormat(Context context) {
        if (loadConfig(context) == null) {
            return false;
        }
        return TextUtils.equals(config.getIsFormat(), "true");
    }

    public static HashMap<String, String> getErrorConfig() {
        return config.getErrorInfo();
    }
}
