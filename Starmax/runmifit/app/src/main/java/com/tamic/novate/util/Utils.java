package com.tamic.novate.util;

import android.content.res.Resources;
import android.net.Uri;
import android.os.Looper;
import android.text.TextUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tamic.novate.ContentType;
import com.tamic.novate.callback.ResponseCallback;
import com.tamic.novate.request.NovateRequestBody;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class Utils {
    public static final String MULTIPART_APK_DATA = "application/vnd.android.package-archive";
    public static final String MULTIPART_AUDIO_DATA = "audio/*";
    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
    public static final String MULTIPART_IMAGE_DATA = "image/*; charset=utf-8";
    public static final String MULTIPART_JAVA_DATA = "java/jpg";
    public static final String MULTIPART_JSON_DATA = "application/json; charset=utf-8";
    public static final String MULTIPART_MESSAGE_DATA = "message/rfc822";
    public static final String MULTIPART_TEXT_DATA = "text/plain";
    public static final String MULTIPART_VIDEO_DATA = "video/*";

    public static <T> T checkNotNull(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }

    public static boolean checkMain() {
        return Thread.currentThread() == Looper.getMainLooper().getThread();
    }

    public static RequestBody createJson(String str) {
        checkNotNull(str, "json not null!");
        return RequestBody.create(MediaType.parse(MULTIPART_JSON_DATA), str);
    }

    public static RequestBody createText(String str) {
        checkNotNull(str, "text not null!");
        return RequestBody.create(MediaType.parse(MULTIPART_TEXT_DATA), str);
    }

    public static RequestBody createString(String str) {
        checkNotNull(str, "name not null!");
        return RequestBody.create(MediaType.parse("multipart/form-data; charset=utf-8"), str);
    }

    public static RequestBody createFile(File file) {
        checkNotNull(file, "file not null!");
        return RequestBody.create(MediaType.parse("multipart/form-data; charset=utf-8"), file);
    }

    public static RequestBody createBody(File file, ContentType contentType) {
        checkNotNull(file, "file not be null!");
        checkNotNull(file, "type not be null!");
        return createBody(file, typeToString(contentType));
    }

    public static RequestBody createBody(File file, String str) {
        checkNotNull(file, "file not null!");
        if (!TextUtils.isEmpty(str)) {
            return RequestBody.create(MediaType.parse(str), file);
        }
        throw new NullPointerException("contentType not be null");
    }

    public static RequestBody createImage(File file) {
        checkNotNull(file, "file not null!");
        return RequestBody.create(MediaType.parse(MULTIPART_IMAGE_DATA), file);
    }

    public static RequestBody createPartFromString(String str) {
        return RequestBody.create(MediaType.parse("multipart/form-data; charset=utf-8"), str);
    }

    public static String typeToString(ContentType contentType) {
        switch (contentType) {
            case APK:
                return "application/vnd.android.package-archive";
            case VIDEO:
                return "video/*";
            case AUDIO:
                return "audio/*";
            case JAVA:
                return MULTIPART_JAVA_DATA;
            case IMAGE:
                return MULTIPART_IMAGE_DATA;
            case TEXT:
                return MULTIPART_TEXT_DATA;
            case JSON:
                return MULTIPART_JSON_DATA;
            case FORM:
                return MULTIPART_FORM_DATA;
            case MESSAGE:
                return MULTIPART_MESSAGE_DATA;
            default:
                return MULTIPART_IMAGE_DATA;
        }
    }

    public static NovateRequestBody createRequestBody(File file, ContentType contentType) {
        return createRequestBody(file, contentType, null);
    }

    public static NovateRequestBody createRequestBody(File file, ContentType contentType, ResponseCallback responseCallback) {
        return new NovateRequestBody(createBody(file, contentType), responseCallback);
    }

    public static MultipartBody.Part createPart(String str, File file) {
        return MultipartBody.Part.createFormData(str, file.getName(), RequestBody.create(MediaType.parse("multipart/form-data; charset=utf-8"), file));
    }

    public static MultipartBody.Part createPart(String str, File file, ContentType contentType) {
        return MultipartBody.Part.createFormData(str, file.getName(), RequestBody.create(MediaType.parse(typeToString(contentType) + "; charset=utf-8"), file));
    }

    private MultipartBody.Part prepareFilePart(String str, Uri uri) {
        File uirFile = FileUtil.getUirFile(uri);
        return MultipartBody.Part.createFormData(str, uirFile.getName(), RequestBody.create(MediaType.parse(MULTIPART_FORM_DATA), uirFile));
    }

    public static Map<String, MultipartBody.Part> createParts(String str, Map<String, File> map, ContentType contentType, ResponseCallback responseCallback) {
        HashMap hashMap = new HashMap();
        if (map != null && map.size() > 0) {
            for (String str2 : map.keySet()) {
                File file = map.get(str2);
                if (FileUtil.exists(file)) {
                    hashMap.put(str2, MultipartBody.Part.createFormData(str, file.getName(), createRequestBody(file, contentType, responseCallback)));
                } else {
                    throw new Resources.NotFoundException(file.getPath() + "file 路径无法找到");
                }
            }
        }
        return hashMap;
    }

    public static List<MultipartBody.Part> createPartLists(String str, List<File> list, ContentType contentType, ResponseCallback responseCallback) {
        ArrayList arrayList = new ArrayList();
        if (list != null && list.size() > 0) {
            for (File file : list) {
                if (FileUtil.exists(file)) {
                    arrayList.add(MultipartBody.Part.createFormData(str, file.getName(), createRequestBody(file, contentType, responseCallback)));
                } else {
                    throw new Resources.NotFoundException(file.getPath() + "file 路径无法找到");
                }
            }
        }
        return arrayList;
    }

    public static NovateRequestBody createNovateRequestBody(RequestBody requestBody, ResponseCallback responseCallback) {
        return new NovateRequestBody(requestBody, responseCallback);
    }

    public static <T> List<T> jsonToList(String str, Class<T> cls) {
        if (str == null) {
            return null;
        }
        return (List) new Gson().fromJson(str, new TypeToken<T>() {
            /* class com.tamic.novate.util.Utils.C27941 */
        }.getType());
    }

    public static int checkDuration(String str, long j, TimeUnit timeUnit) {
        if (j < 0) {
            throw new IllegalArgumentException(str + " < 0");
        } else if (timeUnit != null) {
            long millis = timeUnit.toMillis(j);
            if (millis > 2147483647L) {
                throw new IllegalArgumentException(str + " too large.");
            } else if (millis != 0 || j <= 0) {
                return (int) millis;
            } else {
                throw new IllegalArgumentException(str + " too small.");
            }
        } else {
            throw new NullPointerException("unit == null");
        }
    }
}
