package com.tamic.novate.request;

import android.net.Uri;
import com.google.api.client.http.HttpMethods;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.util.Utils;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Map;
import okhttp3.CacheControl;
import okhttp3.Headers;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.internal.http.HttpMethod;

public final class NovateRequest<T> {
    private static Builder builder;
    private static Map<String, Object> params;
    /* access modifiers changed from: private */
    public final RequestBody body;
    private volatile CacheControl cacheControl;
    /* access modifiers changed from: private */
    public final Headers headers;
    /* access modifiers changed from: private */
    public final String method;
    /* access modifiers changed from: private */
    public final Object tag;
    /* access modifiers changed from: private */
    public final String url;

    public NovateRequest create(Request request) {
        return null;
    }

    private NovateRequest(Builder builder2) {
        this.url = builder2.url;
        this.method = builder2.method;
        this.headers = builder2.headers.build();
        this.body = builder2.body;
        this.tag = builder2.tag != null ? builder2.tag : this;
    }

    public String url() {
        return this.url;
    }

    public String method() {
        return this.method;
    }

    public Headers headers() {
        return this.headers;
    }

    public String header(String str) {
        return this.headers.get(str);
    }

    public List<String> headers(String str) {
        return this.headers.values(str);
    }

    public RequestBody body() {
        return this.body;
    }

    public Object tag() {
        return this.tag;
    }

    public Map<String, Object> params() {
        return params;
    }

    public Builder newBuilder() {
        return new Builder();
    }

    public CacheControl cacheControl() {
        CacheControl cacheControl2 = this.cacheControl;
        if (cacheControl2 != null) {
            return cacheControl2;
        }
        CacheControl parse = CacheControl.parse(this.headers);
        this.cacheControl = parse;
        return parse;
    }

    public boolean isHttps() {
        return HttpUrl.parse(this.url).isHttps();
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Request{method=");
        sb.append(this.method);
        sb.append(", url=");
        sb.append(this.url);
        sb.append(", tag=");
        Object obj = this.tag;
        if (obj == this) {
            obj = null;
        }
        sb.append(obj);
        sb.append('}');
        return sb.toString();
    }

    public static class Builder {
        /* access modifiers changed from: private */
        public RequestBody body;
        /* access modifiers changed from: private */
        public Headers.Builder headers;
        /* access modifiers changed from: private */
        public String method;
        /* access modifiers changed from: private */
        public Object tag;
        /* access modifiers changed from: private */
        public String url;

        public Builder addParameter(String str, Object obj) {
            return this;
        }

        public Builder() {
            this.method = "GET";
            this.headers = new Headers.Builder();
        }

        private Builder(NovateRequest novateRequest) {
            this.url = novateRequest.url;
            this.method = novateRequest.method;
            this.body = novateRequest.body;
            this.tag = novateRequest.tag;
            this.headers = novateRequest.headers.newBuilder();
        }

        public Builder url(HttpUrl httpUrl) {
            if (httpUrl != null) {
                this.url = httpUrl.url().toString();
                return this;
            }
            throw new NullPointerException("url == null");
        }

        public Builder url(String str) {
            if (str != null) {
                if (str.regionMatches(true, 0, "ws:", 0, 3)) {
                    str = "http:" + str.substring(3);
                } else if (str.regionMatches(true, 0, "wss:", 0, 4)) {
                    str = "https:" + str.substring(4);
                }
                HttpUrl parse = HttpUrl.parse(str);
                if (parse != null) {
                    return url(parse);
                }
                throw new IllegalArgumentException("unexpected url: " + str);
            }
            throw new NullPointerException("url == null");
        }

        public Builder url(URL url2) {
            if (url2 != null) {
                HttpUrl httpUrl = HttpUrl.get(url2);
                if (httpUrl != null) {
                    return url(httpUrl);
                }
                throw new IllegalArgumentException("unexpected url: " + url2);
            }
            throw new NullPointerException("url == null");
        }

        public Builder header(String str, String str2) {
            this.headers.set(str, str2);
            return this;
        }

        public Builder addHeader(String str, String str2) {
            this.headers.add(str, str2);
            return this;
        }

        public Builder headers(String str, String str2) {
            this.headers.set(str, str2);
            return this;
        }

        public Builder removeHeader(String str) {
            this.headers.removeAll(str);
            return this;
        }

        public Builder headers(Map<String, String> map) {
            if (map != null && map.size() > 0) {
                for (String str : map.keySet()) {
                    this.headers.add(str, map.get(str) == null ? "" : map.get(str));
                }
            }
            return this;
        }

        public Builder cacheControl(CacheControl cacheControl) {
            String cacheControl2 = cacheControl.toString();
            if (cacheControl2.isEmpty()) {
                return removeHeader(HttpHeaders.CACHE_CONTROL);
            }
            return header(HttpHeaders.CACHE_CONTROL, cacheControl2);
        }

        public Builder get() {
            return method("GET", null);
        }

        public Builder head() {
            return method(HttpMethods.HEAD, null);
        }

        public Builder post(RequestBody requestBody) {
            return method("POST", requestBody);
        }

        public Builder delete(RequestBody requestBody) {
            return method(HttpMethods.DELETE, requestBody);
        }

        public Builder delete() {
            return delete(RequestBody.create((MediaType) null, new byte[0]));
        }

        public Builder put(RequestBody requestBody) {
            return method(HttpMethods.PUT, requestBody);
        }

        public Builder patch(RequestBody requestBody) {
            return method(HttpMethods.PATCH, requestBody);
        }

        public Builder method(String str, RequestBody requestBody) {
            if (str == null) {
                throw new NullPointerException("method == null");
            } else if (str.length() == 0) {
                throw new IllegalArgumentException("method.length() == 0");
            } else if (requestBody != null && !HttpMethod.permitsRequestBody(str)) {
                throw new IllegalArgumentException("method " + str + " must not have a request body.");
            } else if (requestBody != null || !HttpMethod.requiresRequestBody(str)) {
                this.method = str;
                this.body = requestBody;
                return this;
            } else {
                throw new IllegalArgumentException("method " + str + " must have a request body.");
            }
        }

        public Builder tag(Object obj) {
            this.tag = obj;
            return this;
        }

        public NovateRequest build() {
            if (this.url != null) {
                return new NovateRequest(this);
            }
            throw new IllegalStateException("url == null");
        }
    }

    private NovateRequest(String str, String str2, Headers headers2, RequestBody requestBody, Object obj) {
        this.url = str;
        this.method = str2;
        this.headers = headers2;
        this.body = requestBody;
        this.tag = obj;
    }

    public NovateRequest addParameter(String str, Object obj) {
        if (obj instanceof String) {
            params.put(str, Utils.createText((String) obj));
        } else if (obj instanceof File) {
            Map<String, Object> map = params;
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append("\"; filename=\"");
            File file = (File) obj;
            sb.append(file.getName());
            sb.append("");
            map.put(sb.toString(), Utils.createFile(file));
        }
        return this;
    }

    public NovateRequest addFilesByUri(String str, List<Uri> list) {
        for (int i = 0; i < list.size(); i++) {
            File file = new File(list.get(i).getPath());
            Map<String, Object> map = params;
            map.put(str + i + "\"; filename=\"" + file.getName() + "", Utils.createImage(file));
        }
        return this;
    }

    public void cleanParams() {
        Map<String, Object> map = params;
        if (map != null) {
            map.clear();
        }
    }
}
