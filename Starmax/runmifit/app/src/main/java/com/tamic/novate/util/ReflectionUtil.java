package com.tamic.novate.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ReflectionUtil {
    private static final String TYPE_CLASS_NAME_PREFIX = "class ";
    private static final String TYPE_INTERFACE_NAME_PREFIX = "interface ";

    private ReflectionUtil() {
    }

    public static String getClassName(Type type) {
        if (type == null) {
            return "";
        }
        String obj = type.toString();
        if (obj.startsWith(TYPE_CLASS_NAME_PREFIX)) {
            return obj.substring(6);
        }
        return obj.startsWith(TYPE_INTERFACE_NAME_PREFIX) ? obj.substring(10) : obj;
    }

    public static Class<?> getClass(Type type) throws ClassNotFoundException {
        String className = getClassName(type);
        if (className == null || className.isEmpty()) {
            return null;
        }
        return Class.forName(className);
    }

    public static Object newInstance(Type type) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Class<?> cls = getClass(type);
        if (cls == null) {
            return null;
        }
        return cls.newInstance();
    }

    public static Type[] getParameterizedTypes(Object obj) {
        Type genericSuperclass = obj.getClass().getGenericSuperclass();
        if (!ParameterizedType.class.isAssignableFrom(genericSuperclass.getClass())) {
            return null;
        }
        return ((ParameterizedType) genericSuperclass).getActualTypeArguments();
    }

    public static Type[] getParameterizedTypeswithInterfaces(Object obj) {
        return obj.getClass().getGenericInterfaces();
    }

    public static boolean hasDefaultConstructor(Class<?> cls) throws SecurityException {
        try {
            cls.getConstructor(new Class[0]);
            return true;
        } catch (NoSuchMethodException unused) {
            return false;
        }
    }

    public static Class<?> getFieldClass(Class<?> cls, String str) {
        if (cls == null || str == null || str.isEmpty()) {
            return null;
        }
        Field[] declaredFields = cls.getDeclaredFields();
        for (Field field : declaredFields) {
            field.setAccessible(true);
            if (field.getName().equalsIgnoreCase(str)) {
                return field.getType();
            }
        }
        return null;
    }

    public static Class<?> getMethodReturnType(Class<?> cls, String str) {
        if (cls == null || str == null || str.isEmpty()) {
            return null;
        }
        String lowerCase = str.toLowerCase();
        Method[] declaredMethods = cls.getDeclaredMethods();
        for (Method method : declaredMethods) {
            if (method.getName().equals(lowerCase)) {
                return method.getReturnType();
            }
        }
        return null;
    }

    public static Object getEnumConstant(Class<?> cls, String str) {
        if (cls == null || str == null || str.isEmpty()) {
            return null;
        }
        return Enum.valueOf(cls, str);
    }

    public static List<Type> methodHandler(Type[] typeArr) {
        ArrayList arrayList = new ArrayList();
        for (Type type : typeArr) {
            System.out.println("  " + type);
            if (type instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                for (Type type2 : actualTypeArguments) {
                    arrayList.add(type2);
                    if (type2 instanceof ParameterizedType) {
                        for (Type type3 : ((ParameterizedType) type2).getActualTypeArguments()) {
                            arrayList.add(type3);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    private static Type methodHandler2(Type[] typeArr) {
        ArrayList arrayList = new ArrayList();
        Type type = null;
        for (Type type2 : typeArr) {
            System.out.println("  " + type2);
            if (type2 instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) type2).getActualTypeArguments();
                r7 = type;
                for (Type type3 : actualTypeArguments) {
                    arrayList.add(type3);
                    if (type3 instanceof ParameterizedType) {
                        Type[] actualTypeArguments2 = ((ParameterizedType) type3).getActualTypeArguments();
                        r10 = type3;
                        for (Type type4 : actualTypeArguments2) {
                        }
                        type3 = type4;
                    }
                }
                type = type3;
            }
        }
        return type;
    }

    public static Type getClassType(Class<?> cls) {
        return getClassType(((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments());
    }

    public static List<Type> getClassTypes(Class<?> cls) {
        return getClassTypes(((ParameterizedType) cls.getGenericSuperclass()).getActualTypeArguments());
    }

    public static List<Type> getClassTypes(Type[] typeArr) {
        ArrayList arrayList = new ArrayList();
        for (Type type : typeArr) {
            System.out.println("  " + type);
            if (type instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
                for (Type type2 : actualTypeArguments) {
                    arrayList.add(type2);
                    if (type2 instanceof ParameterizedType) {
                        for (Type type3 : ((ParameterizedType) type2).getActualTypeArguments()) {
                            arrayList.add(type3);
                        }
                    }
                }
            }
        }
        return arrayList;
    }

    public static Type getClassType(Type[] typeArr) {
        ArrayList arrayList = new ArrayList();
        Type type = null;
        for (Type type2 : typeArr) {
            System.out.println("  " + type2);
            if (type2 instanceof ParameterizedType) {
                Type[] actualTypeArguments = ((ParameterizedType) type2).getActualTypeArguments();
                r7 = type;
                for (Type type3 : actualTypeArguments) {
                    arrayList.add(type3);
                    if (type3 instanceof ParameterizedType) {
                        Type[] actualTypeArguments2 = ((ParameterizedType) type3).getActualTypeArguments();
                        r10 = type3;
                        for (Type type4 : actualTypeArguments2) {
                        }
                        type3 = type4;
                    }
                }
                type = type3;
            }
        }
        return type;
    }
}
