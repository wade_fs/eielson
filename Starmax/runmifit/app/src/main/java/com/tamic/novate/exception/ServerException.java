package com.tamic.novate.exception;

public class ServerException extends RuntimeException {
    public int code;
    public String message;

    public ServerException(int i, String str) {
        super(str);
        this.code = i;
        this.message = str;
    }
}
