package com.tamic.novate.exception;

public class FormatException extends RuntimeException {
    public int code = -200;
    public String message = "服务端返回数据格式异常";

    public int getCode() {
        return this.code;
    }

    public void setCode(int i) {
        this.code = i;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }
}
