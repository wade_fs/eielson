package com.tamic.novate;

import p042rx.Subscription;

public interface RxActionManager<T> {
    void add(T t, Subscription subscription);

    void cancel(T t);

    void cancelAll();

    void remove(T t);
}
