package com.tamic.novate;

public enum ContentType {
    JSON,
    TEXT,
    AUDIO,
    VIDEO,
    IMAGE,
    JAVA,
    MESSAGE,
    APK,
    FORM
}
