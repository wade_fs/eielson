package com.tamic.novate.cache;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import okhttp3.Cookie;

public class CookieCacheImpl implements CookieCache {
    /* access modifiers changed from: private */
    public Set<IdentifiableCookie> cookies = new HashSet();

    public void addAll(Collection<Cookie> collection) {
        for (IdentifiableCookie identifiableCookie : IdentifiableCookie.decorateAll(collection)) {
            this.cookies.remove(identifiableCookie);
            this.cookies.add(identifiableCookie);
        }
    }

    public void clear() {
        this.cookies.clear();
    }

    public Iterator<Cookie> iterator() {
        return new SetCookieCacheIterator();
    }

    private class SetCookieCacheIterator implements Iterator<Cookie> {
        private Iterator<IdentifiableCookie> iterator;

        public SetCookieCacheIterator() {
            this.iterator = CookieCacheImpl.this.cookies.iterator();
        }

        public boolean hasNext() {
            return this.iterator.hasNext();
        }

        public Cookie next() {
            return this.iterator.next().getCookie();
        }

        public void remove() {
            this.iterator.remove();
        }
    }
}
