package com.tamic.novate.callback;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.io.IOException;
import okhttp3.Call;
import okhttp3.ResponseBody;

public abstract class RxBitmapCallback extends ResponseCallback<Bitmap, ResponseBody> {
    public abstract void onNext(Object obj, Bitmap bitmap);

    public Bitmap onHandleResponse(ResponseBody responseBody) throws IOException {
        return transform(responseBody, Bitmap.class);
    }

    public Bitmap transform(ResponseBody responseBody, Class cls) {
        return BitmapFactory.decodeStream(responseBody.byteStream());
    }

    public void onNext(Object obj, Call call, Bitmap bitmap) {
        onNext(obj, bitmap);
    }
}
