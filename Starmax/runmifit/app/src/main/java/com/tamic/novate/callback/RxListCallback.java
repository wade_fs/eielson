package com.tamic.novate.callback;

import android.text.TextUtils;
import android.util.Log;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.gson.Gson;
import com.tamic.novate.Novate;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import okhttp3.ResponseBody;
import org.json.JSONObject;

public abstract class RxListCallback<T> extends RxGenericsCallback<T, ResponseBody> {
    private Type collectionType;

    public RxListCallback() {
    }

    public RxListCallback(Type type) {
        this.collectionType = type;
    }

    public T onHandleResponse(ResponseBody responseBody) throws Exception {
        if (this.collectionType == null) {
            this.collectionType = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        String str = new String(responseBody.bytes());
        Log.d(Novate.TAG, str);
        return transform(str, (Class) null);
    }

    public T transform(String str, Class cls) throws ClassCastException {
        Log.e("xxx", str);
        try {
            JSONObject jSONObject = new JSONObject(str);
            this.code = jSONObject.optInt("code");
            this.msg = jSONObject.optString("msg");
            if (TextUtils.isEmpty(this.msg)) {
                this.msg = jSONObject.optString("error");
            }
            if (TextUtils.isEmpty(this.msg)) {
                this.msg = jSONObject.optString("message");
            }
            this.dataStr = jSONObject.optJSONArray("data").toString();
            if (this.dataStr.isEmpty()) {
                this.dataStr = jSONObject.optJSONArray("result").toString();
            }
            this.dataResponse = new Gson().fromJson(this.dataStr, this.collectionType);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.dataResponse;
    }
}
