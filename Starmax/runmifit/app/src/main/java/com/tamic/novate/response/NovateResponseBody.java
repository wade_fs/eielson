package com.tamic.novate.response;

import com.tamic.novate.callback.ResponseCallback;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

public class NovateResponseBody extends ResponseBody {
    private BufferedSource bufferedSource;
    /* access modifiers changed from: private */
    public long frequency = 0;
    /* access modifiers changed from: private */
    public long previousTime;
    /* access modifiers changed from: private */
    public final ResponseCallback progressListener;
    private final ResponseBody responseBody;

    public NovateResponseBody(ResponseBody responseBody2, ResponseCallback responseCallback) {
        this.responseBody = super;
        this.progressListener = responseCallback;
    }

    public MediaType contentType() {
        return this.responseBody.contentType();
    }

    public long contentLength() {
        return this.responseBody.contentLength();
    }

    public BufferedSource source() {
        if (this.bufferedSource == null) {
            this.bufferedSource = Okio.buffer(source(this.responseBody.source()));
        }
        return this.bufferedSource;
    }

    private Source source(Source source) {
        this.previousTime = System.currentTimeMillis();
        return new ForwardingSource(source) {
            /* class com.tamic.novate.response.NovateResponseBody.C27881 */
            long totalBytesRead = 0;
            int updateCount = 0;

            public long read(Buffer buffer, long j) throws IOException {
                long read = super.read(buffer, j);
                final long contentLength = NovateResponseBody.this.contentLength();
                if (contentLength > 2048) {
                    long unused = NovateResponseBody.this.frequency = 1;
                }
                this.totalBytesRead += read != -1 ? read : 0;
                long currentTimeMillis = (System.currentTimeMillis() - NovateResponseBody.this.previousTime) / 1000;
                if (currentTimeMillis == 0) {
                    currentTimeMillis++;
                }
                long j2 = this.totalBytesRead;
                final long j3 = j2 / currentTimeMillis;
                final int i = (int) ((j2 * 100) / contentLength);
                int i2 = this.updateCount;
                if (i2 == 0 || i >= i2) {
                    this.updateCount = (int) (((long) this.updateCount) + NovateResponseBody.this.frequency);
                    if (NovateResponseBody.this.progressListener != null) {
                        NovateResponseBody.this.progressListener.getHandler().post(new Runnable() {
                            /* class com.tamic.novate.response.NovateResponseBody.C27881.C27891 */

                            public void run() {
                                NovateResponseBody.this.progressListener.onProgress(NovateResponseBody.this.progressListener.getTag(), i, j3, C27881.this.totalBytesRead, contentLength);
                            }
                        });
                    }
                }
                return read;
            }
        };
    }
}
