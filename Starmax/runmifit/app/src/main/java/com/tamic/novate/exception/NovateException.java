package com.tamic.novate.exception;

import android.net.ParseException;
import android.text.TextUtils;
import com.google.gson.JsonParseException;
import com.tamic.novate.Novate;
import com.tamic.novate.Throwable;
import com.tamic.novate.config.ConfigLoader;
import com.tamic.novate.util.LogWraper;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.security.cert.CertPathValidatorException;
import java.util.HashMap;
import javax.net.ssl.SSLHandshakeException;
import javax.net.ssl.SSLPeerUnverifiedException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import retrofit2.adapter.rxjava.HttpException;

public class NovateException {
    private static final int ACCESS_DENIED = 302;
    private static final int BAD_GATEWAY = 502;
    private static final int FORBIDDEN = 403;
    private static final int GATEWAY_TIMEOUT = 504;
    private static final int HANDEL_ERRROR = 417;
    private static final int INTERNAL_SERVER_ERROR = 500;
    private static final int NOT_FOUND = 404;
    private static final int REQUEST_TIMEOUT = 408;
    private static final int SERVICE_UNAVAILABLE = 503;
    private static final int UNAUTHORIZED = 401;

    public static Throwable handleException(Throwable th) {
        LogWraper.m6229e(Novate.TAG, th.getMessage());
        LogWraper.m6229e(Novate.TAG, th.getCause() != null ? th.getCause().getMessage() : "");
        boolean z = th instanceof ServerException;
        if (!z && (th instanceof HttpException)) {
            Throwable throwable = new Throwable(th, ((HttpException) th).code());
            int code = throwable.getCode();
            if (code == 302) {
                throwable.setMessage("网络错误");
            } else if (code == 401) {
                throwable.setMessage("未授权的请求");
            } else if (code == REQUEST_TIMEOUT) {
                throwable.setMessage("请求超时");
            } else if (code != HANDEL_ERRROR) {
                if (code == 500) {
                    throwable.setMessage("服务器出错");
                } else if (code == 403) {
                    throwable.setMessage("禁止访问");
                } else if (code != 404) {
                    switch (code) {
                        case 502:
                            break;
                        case 503:
                            throwable.setMessage("服务器不可用");
                            break;
                        case 504:
                            throwable.setMessage("网关响应超时");
                            break;
                        default:
                            if (!TextUtils.isEmpty(throwable.getMessage())) {
                                if (!TextUtils.isEmpty(throwable.getMessage()) || th.getLocalizedMessage() == null) {
                                    if (TextUtils.isEmpty(throwable.getMessage())) {
                                        throwable.setMessage("未知错误");
                                        break;
                                    }
                                } else {
                                    throwable.setMessage(th.getLocalizedMessage());
                                    break;
                                }
                            } else {
                                throwable.setMessage(th.getMessage());
                                break;
                            }
                            break;
                    }
                } else {
                    throwable.setMessage("服务器地址未找到");
                }
                throwable.setMessage("无效的请求");
            } else {
                throwable.setMessage("接口处理失败");
            }
            return throwable;
        } else if (z) {
            ServerException serverException = (ServerException) th;
            Throwable throwable2 = new Throwable(serverException, serverException.code);
            HashMap<String, String> errorConfig = ConfigLoader.getErrorConfig();
            if (errorConfig == null || !errorConfig.containsKey(String.valueOf(serverException.code))) {
                throwable2.setMessage(serverException.getMessage());
                return throwable2;
            }
            throwable2.setMessage(errorConfig.get(String.valueOf(serverException.code)));
            return throwable2;
        } else if ((th instanceof JsonParseException) || (th instanceof JSONException) || (th instanceof ParseException)) {
            Throwable throwable3 = new Throwable(th, 1001);
            throwable3.setMessage("解析错误");
            return throwable3;
        } else if (th instanceof ConnectException) {
            Throwable throwable4 = new Throwable(th, 1002);
            throwable4.setMessage("连接失败");
            return throwable4;
        } else if (th instanceof SSLHandshakeException) {
            Throwable throwable5 = new Throwable(th, 1005);
            throwable5.setMessage("证书验证失败");
            return throwable5;
        } else if (th instanceof CertPathValidatorException) {
            LogWraper.m6229e(Novate.TAG, th.getMessage());
            Throwable throwable6 = new Throwable(th, 1007);
            throwable6.setMessage("证书路径没找到");
            return throwable6;
        } else if (th instanceof SSLPeerUnverifiedException) {
            LogWraper.m6229e(Novate.TAG, th.getMessage());
            Throwable throwable7 = new Throwable(th, 1007);
            throwable7.setMessage("无有效的SSL证书");
            return throwable7;
        } else if (th instanceof ConnectTimeoutException) {
            Throwable throwable8 = new Throwable(th, 1006);
            throwable8.setMessage("连接超时");
            return throwable8;
        } else if (th instanceof SocketTimeoutException) {
            Throwable throwable9 = new Throwable(th, 1006);
            throwable9.setMessage("连接超时");
            return throwable9;
        } else if (th instanceof ClassCastException) {
            Throwable throwable10 = new Throwable(th, 1008);
            throwable10.setMessage("类型转换出错");
            return throwable10;
        } else if (th instanceof NullPointerException) {
            Throwable throwable11 = new Throwable(th, -100);
            throwable11.setMessage("数据有空");
            return throwable11;
        } else if (th instanceof FormatException) {
            FormatException formatException = (FormatException) th;
            Throwable throwable12 = new Throwable(th, formatException.code);
            throwable12.setMessage(formatException.message);
            return throwable12;
        } else if (th instanceof UnknownHostException) {
            LogWraper.m6229e(Novate.TAG, th.getMessage());
            Throwable throwable13 = new Throwable(th, 404);
            throwable13.setMessage("服务器地址未找到,请检查网络或Url");
            return throwable13;
        } else {
            LogWraper.m6229e(Novate.TAG, th.getMessage());
            Throwable throwable14 = new Throwable(th, 1000);
            throwable14.setMessage(th.getMessage());
            return throwable14;
        }
    }

    public class ERROR {
        public static final int FORMAT_ERROR = 1008;
        public static final int HTTP_ERROR = 1003;
        public static final int NETWORD_ERROR = 1002;
        public static final int NULL = -100;
        public static final int PARSE_ERROR = 1001;
        public static final int SSL_ERROR = 1005;
        public static final int SSL_NOT_FOUND = 1007;
        public static final int TIMEOUT_ERROR = 1006;
        public static final int UNKNOWN = 1000;

        public ERROR() {
        }
    }
}
