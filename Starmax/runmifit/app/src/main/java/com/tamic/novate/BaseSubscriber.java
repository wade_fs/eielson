package com.tamic.novate;

import android.content.Context;
import com.tamic.novate.exception.NovateException;
import com.tamic.novate.util.LogWraper;
import p042rx.Subscriber;

public abstract class BaseSubscriber<T> extends Subscriber<T> {
    protected Context context;

    public abstract void onError(Throwable throwable);

    public BaseSubscriber(Context context2) {
        this.context = context2;
    }

    public BaseSubscriber() {
    }

    public final void onError(Throwable th) {
        if (th == null || th.getMessage() == null) {
            LogWraper.m6237v(Novate.TAG, "Throwable  || Message == Null");
        } else {
            LogWraper.m6237v(Novate.TAG, th.getMessage());
        }
        if (th instanceof Throwable) {
            LogWraper.m6229e(Novate.TAG, "--> e instanceof Throwable");
            LogWraper.m6229e(Novate.TAG, "--> " + th.getCause().toString());
            onError((Throwable) th);
        } else {
            LogWraper.m6229e(Novate.TAG, "e !instanceof Throwable");
            String message = th.getCause() != null ? th.getCause().getMessage() : "";
            LogWraper.m6229e(Novate.TAG, "--> " + message);
            onError(NovateException.handleException(th));
        }
        onCompleted();
    }

    public void onStart() {
        super.onStart();
        LogWraper.m6237v(Novate.TAG, "-->http is start");
    }

    public void onCompleted() {
        LogWraper.m6237v(Novate.TAG, "-->http is Complete");
    }
}
