package com.tamic.novate;

import android.content.Context;
import com.tamic.novate.config.ConfigLoader;

public class NovateResponse<T> {
    private int code = 1;
    private T data;
    private String error;
    private String message;
    private String msg;
    private T result;

    public int getCode() {
        return this.code;
    }

    public void setCode(int i) {
        this.code = i;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String str) {
        this.error = str;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String str) {
        this.message = str;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String str) {
        this.msg = str;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T t) {
        this.data = t;
    }

    public boolean isOk(Context context) {
        return ConfigLoader.checkSucess(context, getCode());
    }

    public T getResult() {
        return this.result;
    }

    public void setResult(T t) {
        this.result = t;
    }

    public String toString() {
        return "NovateResponse{code=" + this.code + ", msg='" + this.msg + '\'' + ", error='" + this.error + '\'' + ", message='" + this.message + '\'' + ", data=" + ((Object) this.data) + '}';
    }
}
