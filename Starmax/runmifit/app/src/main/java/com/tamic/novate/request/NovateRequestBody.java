package com.tamic.novate.request;

import com.tamic.novate.callback.ResponseCallback;
import java.io.IOException;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.ForwardingSink;
import okio.Okio;
import okio.Sink;

public class NovateRequestBody extends RequestBody {
    /* access modifiers changed from: private */
    public Object Tag;
    protected ResponseCallback callback;
    protected CountingSink countingSink;
    /* access modifiers changed from: private */
    public long previousTime;
    protected RequestBody requestBody;

    public NovateRequestBody(RequestBody requestBody2, ResponseCallback responseCallback) {
        this(super, responseCallback, responseCallback.getTag());
    }

    public NovateRequestBody(RequestBody requestBody2, ResponseCallback responseCallback, Object obj) {
        this.requestBody = super;
        this.callback = responseCallback;
        this.Tag = obj;
    }

    public MediaType contentType() {
        return this.requestBody.contentType();
    }

    public long contentLength() {
        try {
            return this.requestBody.contentLength();
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void writeTo(BufferedSink bufferedSink) throws IOException {
        this.previousTime = System.currentTimeMillis();
        this.countingSink = new CountingSink(bufferedSink);
        BufferedSink buffer = Okio.buffer(this.countingSink);
        this.requestBody.writeTo(buffer);
        buffer.flush();
    }

    public Object getTag() {
        return this.Tag;
    }

    public NovateRequestBody setTag(Object obj) {
        this.Tag = obj;
        return this;
    }

    protected final class CountingSink extends ForwardingSink {
        private long bytesWritten = 0;
        long contentLength = 0;

        public CountingSink(Sink sink) {
            super(sink);
        }

        public void write(Buffer buffer, long j) throws IOException {
            Object obj;
            super.write(buffer, j);
            if (this.contentLength == 0) {
                this.contentLength = NovateRequestBody.this.contentLength();
            }
            this.bytesWritten += j;
            if (NovateRequestBody.this.callback != null) {
                long currentTimeMillis = (System.currentTimeMillis() - NovateRequestBody.this.previousTime) / 1000;
                if (currentTimeMillis == 0) {
                    currentTimeMillis++;
                }
                long j2 = this.bytesWritten;
                long j3 = j2 / currentTimeMillis;
                int i = (int) ((j2 * 100) / this.contentLength);
                NovateRequestBody.this.callback.onProgress(NovateRequestBody.this.Tag == null ? "" : NovateRequestBody.this.Tag, (float) i, this.bytesWritten, this.contentLength);
                ResponseCallback responseCallback = NovateRequestBody.this.callback;
                if (NovateRequestBody.this.Tag == null) {
                    obj = "";
                } else {
                    obj = NovateRequestBody.this.Tag;
                }
                responseCallback.onProgress(obj, i, j3, this.bytesWritten, this.contentLength);
            }
        }
    }
}
