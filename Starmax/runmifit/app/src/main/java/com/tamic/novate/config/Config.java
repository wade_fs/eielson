package com.tamic.novate.config;

import java.util.HashMap;
import java.util.List;

public class Config {
    HashMap<String, String> error;
    private String isFormat;
    private List<String> sucessCode;

    public String getIsFormat() {
        return this.isFormat;
    }

    public void setIsFormat(String str) {
        this.isFormat = str;
    }

    public HashMap<String, String> getErrorInfo() {
        return this.error;
    }

    public void setErrorInfo(HashMap<String, String> hashMap) {
        this.error = hashMap;
    }

    public List<String> getSucessCode() {
        return this.sucessCode;
    }

    public void setSucessCode(List<String> list) {
        this.sucessCode = list;
    }
}
