package com.tamic.novate;

import com.tamic.novate.AbsRequestInterceptor;
import java.io.IOException;
import java.util.Map;
import okhttp3.Interceptor;
import okhttp3.Response;

public class BaseParameters<T> extends AbsRequestInterceptor {
    private Map<String, T> parameters;

    public BaseParameters(Map<String, T> map) {
        this(map, AbsRequestInterceptor.Type.ADD);
    }

    public BaseParameters(Map<String, T> map, AbsRequestInterceptor.Type type) {
        this.parameters = map;
        this.control = type;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.proceed(interceptor(chain.request()));
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        if (r2 != 3) goto L_0x009e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public okhttp3.Request interceptor(okhttp3.Request r7) throws java.io.UnsupportedEncodingException {
        /*
            r6 = this;
            r7.newBuilder()
            okhttp3.HttpUrl r0 = r7.url()
            okhttp3.HttpUrl$Builder r0 = r0.newBuilder()
            java.util.Map<java.lang.String, T> r1 = r6.parameters
            if (r1 == 0) goto L_0x009e
            int r1 = r1.size()
            if (r1 <= 0) goto L_0x009e
            java.util.Map<java.lang.String, T> r1 = r6.parameters
            java.util.Set r1 = r1.keySet()
            int[] r2 = com.tamic.novate.BaseParameters.C27531.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type
            com.tamic.novate.AbsRequestInterceptor$Type r3 = r6.control
            int r3 = r3.ordinal()
            r2 = r2[r3]
            r3 = 1
            java.lang.String r4 = ""
            if (r2 == r3) goto L_0x0074
            r3 = 2
            if (r2 == r3) goto L_0x0032
            r3 = 3
            if (r2 == r3) goto L_0x005c
            goto L_0x009e
        L_0x0032:
            java.util.Iterator r2 = r1.iterator()
        L_0x0036:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x005c
            java.lang.Object r3 = r2.next()
            java.lang.String r3 = (java.lang.String) r3
            java.util.Map<java.lang.String, T> r5 = r6.parameters
            java.lang.Object r5 = r5.get(r3)
            if (r5 != 0) goto L_0x004c
            r5 = r4
            goto L_0x0054
        L_0x004c:
            java.util.Map<java.lang.String, T> r5 = r6.parameters
            java.lang.Object r5 = r5.get(r3)
            java.lang.String r5 = (java.lang.String) r5
        L_0x0054:
            okhttp3.HttpUrl$Builder r3 = r0.setQueryParameter(r3, r5)
            r3.build()
            goto L_0x0036
        L_0x005c:
            java.util.Iterator r1 = r1.iterator()
        L_0x0060:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x009e
            java.lang.Object r2 = r1.next()
            java.lang.String r2 = (java.lang.String) r2
            okhttp3.HttpUrl$Builder r2 = r0.removeAllQueryParameters(r2)
            r2.build()
            goto L_0x0060
        L_0x0074:
            java.util.Iterator r1 = r1.iterator()
        L_0x0078:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x009e
            java.lang.Object r2 = r1.next()
            java.lang.String r2 = (java.lang.String) r2
            java.util.Map<java.lang.String, T> r3 = r6.parameters
            java.lang.Object r3 = r3.get(r2)
            if (r3 != 0) goto L_0x008e
            r3 = r4
            goto L_0x0096
        L_0x008e:
            java.util.Map<java.lang.String, T> r3 = r6.parameters
            java.lang.Object r3 = r3.get(r2)
            java.lang.String r3 = (java.lang.String) r3
        L_0x0096:
            okhttp3.HttpUrl$Builder r2 = r0.addQueryParameter(r2, r3)
            r2.build()
            goto L_0x0078
        L_0x009e:
            okhttp3.HttpUrl r0 = r0.build()
            okhttp3.Request$Builder r7 = r7.newBuilder()
            okhttp3.Request$Builder r7 = r7.url(r0)
            okhttp3.Request r7 = r7.build()
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.BaseParameters.interceptor(okhttp3.Request):okhttp3.Request");
    }

    /* renamed from: com.tamic.novate.BaseParameters$1 */
    static /* synthetic */ class C27531 {
        static final /* synthetic */ int[] $SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type = new int[AbsRequestInterceptor.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.tamic.novate.AbsRequestInterceptor$Type[] r0 = com.tamic.novate.AbsRequestInterceptor.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.tamic.novate.BaseParameters.C27531.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type = r0
                int[] r0 = com.tamic.novate.BaseParameters.C27531.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.tamic.novate.AbsRequestInterceptor$Type r1 = com.tamic.novate.AbsRequestInterceptor.Type.ADD     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.tamic.novate.BaseParameters.C27531.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type     // Catch:{ NoSuchFieldError -> 0x001f }
                com.tamic.novate.AbsRequestInterceptor$Type r1 = com.tamic.novate.AbsRequestInterceptor.Type.UPDATE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.tamic.novate.BaseParameters.C27531.$SwitchMap$com$tamic$novate$AbsRequestInterceptor$Type     // Catch:{ NoSuchFieldError -> 0x002a }
                com.tamic.novate.AbsRequestInterceptor$Type r1 = com.tamic.novate.AbsRequestInterceptor.Type.REMOVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tamic.novate.BaseParameters.C27531.<clinit>():void");
        }
    }
}
