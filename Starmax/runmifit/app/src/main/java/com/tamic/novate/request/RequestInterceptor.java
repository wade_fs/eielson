package com.tamic.novate.request;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Response;

public class RequestInterceptor<T> implements Interceptor {
    private NovateRequest request;
    private Object tag;

    public RequestInterceptor(Object obj) {
        this.tag = obj;
    }

    public RequestInterceptor(NovateRequest novateRequest) {
        this.request = novateRequest;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        return chain.proceed(chain.request().newBuilder().tag(this.tag).build());
    }
}
