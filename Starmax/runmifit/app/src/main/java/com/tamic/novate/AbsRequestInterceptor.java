package com.tamic.novate;

import java.io.UnsupportedEncodingException;
import okhttp3.Interceptor;
import okhttp3.Request;

public abstract class AbsRequestInterceptor implements Interceptor {
    public Type control;

    public enum Type {
        ADD,
        UPDATE,
        REMOVE
    }

    /* access modifiers changed from: package-private */
    public abstract Request interceptor(Request request) throws UnsupportedEncodingException;

    public Type getControlType() {
        return this.control;
    }

    public void setControlType(Type type) {
        this.control = type;
    }
}
