package com.tamic.novate;

import android.content.Context;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.util.LogWraper;
import com.tamic.novate.util.NetworkUtil;
import java.io.IOException;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class CacheInterceptorOffline extends CacheInterceptor {
    public CacheInterceptorOffline(Context context) {
        super(context);
    }

    public CacheInterceptorOffline(Context context, String str) {
        super(context, str);
    }

    public CacheInterceptorOffline(Context context, String str, String str2) {
        super(context, str, str2);
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request request = chain.request();
        LogWraper.m6225d(Novate.TAG, "request url :" + request.url().url());
        LogWraper.m6225d(Novate.TAG, "request tag :" + request.tag().toString());
        LogWraper.m6225d(Novate.TAG, "request header :" + request.headers().toString());
        if (NetworkUtil.isNetworkAvailable(this.context)) {
            return chain.proceed(request);
        }
        LogWraper.m6225d(Novate.TAG, " no network load cache:" + request.cacheControl().toString());
        Response.Builder removeHeader = chain.proceed(request.newBuilder().cacheControl(CacheControl.FORCE_CACHE).cacheControl(CacheControl.FORCE_NETWORK).build()).newBuilder().removeHeader(HttpHeaders.PRAGMA).removeHeader(HttpHeaders.CACHE_CONTROL);
        return removeHeader.header(HttpHeaders.CACHE_CONTROL, "public, only-if-cached, " + this.cacheControlValue_Offline).build();
    }
}
