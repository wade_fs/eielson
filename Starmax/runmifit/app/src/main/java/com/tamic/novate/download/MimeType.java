package com.tamic.novate.download;

import android.text.TextUtils;
import com.tamic.novate.util.FileUtil;
import java.util.ArrayList;
import java.util.List;

public class MimeType {
    public static final String APK = "application/vnd.android.package-archive";
    public static final String AVI = "avi";
    public static final String BMP = "bmp";
    public static final String BZ2 = "bz2";
    public static final String CAB = "cab";
    public static final String DOC = "doc";
    public static final String DOCX = "docx";
    public static final String FLV = "flv";
    public static final String GIF = "gif";

    /* renamed from: GZ */
    public static final String f5277GZ = "gz";
    public static final String GZIP = "gzip";
    public static final String HTML = "html";
    public static final String JPEG = "jpeg";
    public static final String JPG = "jpg";
    public static final String JSON = "json";
    public static final String MKV = "mkv";
    public static final String MP3 = "mp3";
    public static final String MP4 = "mp4";
    public static final String MPEG = "mpeg";
    public static final String MPG = "mpg";
    public static final String PDF = "pdf";
    public static final String PNG = "png";
    public static final String PPT = "ppt";
    public static final String PPTX = "pptx";
    public static final String RAR = "rar";

    /* renamed from: RM */
    public static final String f5278RM = "rm";
    public static final String RMVB = "rmvb";
    public static final String TXT = "txt";
    public static final String ThreeGP = "3gp";
    public static final String WAV = "wav";
    public static final String WMA = "wma";
    public static final String WMV = "wmv";
    public static final String XLS = "xls";
    public static final String XLSX = "xlsx";
    public static final String XML = "xml";

    /* renamed from: Z7 */
    public static final String f5279Z7 = "7z";
    public static final String ZIP = "zip";
    private static MimeType sInstance;
    private List<String> mMimeTypeList = new ArrayList();

    private MimeType() {
        loadList();
    }

    private void loadList() {
        this.mMimeTypeList.add(TXT);
        this.mMimeTypeList.add(HTML);
        this.mMimeTypeList.add(XML);
        this.mMimeTypeList.add("application/vnd.android.package-archive");
        this.mMimeTypeList.add(GIF);
        this.mMimeTypeList.add(MPG);
        this.mMimeTypeList.add(JPG);
        this.mMimeTypeList.add(JPEG);
        this.mMimeTypeList.add(PNG);
        this.mMimeTypeList.add(BMP);
        this.mMimeTypeList.add(WMV);
        this.mMimeTypeList.add(f5278RM);
        this.mMimeTypeList.add(FLV);
        this.mMimeTypeList.add(MP3);
        this.mMimeTypeList.add(MKV);
        this.mMimeTypeList.add(AVI);
        this.mMimeTypeList.add(MP4);
        this.mMimeTypeList.add(MPEG);
        this.mMimeTypeList.add(WAV);
        this.mMimeTypeList.add(WMA);
        this.mMimeTypeList.add(ThreeGP);
        this.mMimeTypeList.add(RMVB);
        this.mMimeTypeList.add(JSON);
        this.mMimeTypeList.add(RAR);
        this.mMimeTypeList.add(ZIP);
        this.mMimeTypeList.add(f5279Z7);
        this.mMimeTypeList.add(GZIP);
        this.mMimeTypeList.add(BZ2);
        this.mMimeTypeList.add(CAB);
        this.mMimeTypeList.add(f5277GZ);
        this.mMimeTypeList.add(PDF);
        this.mMimeTypeList.add(DOC);
        this.mMimeTypeList.add(DOCX);
        this.mMimeTypeList.add(XLS);
        this.mMimeTypeList.add(XLSX);
        this.mMimeTypeList.add(PPT);
        this.mMimeTypeList.add(PPTX);
    }

    public String getSuffix(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        for (String str2 : this.mMimeTypeList) {
            if (str.contains(str2)) {
                return FileUtil.HIDDEN_PREFIX + str2;
            }
        }
        return null;
    }

    public static MimeType getInstance() {
        if (sInstance == null) {
            sInstance = new MimeType();
        }
        return sInstance;
    }
}
