package com.tencent.bugly.beta.global;

import android.graphics.drawable.Drawable;
import android.view.MotionEvent;
import android.view.View;
import com.tencent.bugly.proguard.C2903an;

/* renamed from: com.tencent.bugly.beta.global.c */
/* compiled from: BUGLY */
public class C2806c implements View.OnTouchListener {

    /* renamed from: a */
    final int f5332a;

    /* renamed from: b */
    final Object[] f5333b;

    public C2806c(int i, Object... objArr) {
        this.f5332a = i;
        this.f5333b = objArr;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        try {
            if (this.f5332a != 1) {
                return false;
            }
            int action = motionEvent.getAction();
            if (action == 0 || action == 2) {
                view.setBackgroundDrawable((Drawable) this.f5333b[0]);
            } else {
                view.setBackgroundDrawable((Drawable) this.f5333b[1]);
            }
            return false;
        } catch (Exception e) {
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
        }
    }
}
