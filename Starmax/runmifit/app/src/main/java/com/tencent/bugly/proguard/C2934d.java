package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/* renamed from: com.tencent.bugly.proguard.d */
/* compiled from: BUGLY */
public class C2934d extends C2933c {

    /* renamed from: e */
    protected HashMap<String, byte[]> f6187e = null;

    /* renamed from: f */
    C2941k f6188f = new C2941k();

    /* renamed from: g */
    private HashMap<String, Object> f6189g = new HashMap<>();

    /* renamed from: a */
    public /* bridge */ /* synthetic */ void mo34580a(String str) {
        super.mo34580a(str);
    }

    /* renamed from: b */
    public void mo34585b() {
        this.f6187e = new HashMap<>();
    }

    /* renamed from: a */
    public <T> void mo34581a(String str, Object obj) {
        if (this.f6187e == null) {
            super.mo34581a(str, obj);
        } else if (str == null) {
            throw new IllegalArgumentException("put key can not is null");
        } else if (obj == null) {
            throw new IllegalArgumentException("put value can not is null");
        } else if (!(obj instanceof Set)) {
            C2943l lVar = new C2943l();
            lVar.mo34638a(this.f6184c);
            lVar.mo34647a(obj, 0);
            this.f6187e.put(str, C2945n.m7125a(lVar.mo34639a()));
        } else {
            throw new IllegalArgumentException("can not support Set");
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v15, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: byte[]} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [T, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> T mo34584b(java.lang.String r5, T r6) throws com.tencent.bugly.proguard.C2919b {
        /*
            r4 = this;
            java.util.HashMap<java.lang.String, byte[]> r0 = r4.f6187e
            r1 = 0
            if (r0 == 0) goto L_0x0034
            boolean r0 = r0.containsKey(r5)
            if (r0 != 0) goto L_0x000c
            return r1
        L_0x000c:
            java.util.HashMap<java.lang.String, java.lang.Object> r0 = r4.f6189g
            boolean r0 = r0.containsKey(r5)
            if (r0 == 0) goto L_0x001b
            java.util.HashMap<java.lang.String, java.lang.Object> r6 = r4.f6189g
            java.lang.Object r5 = r6.get(r5)
            return r5
        L_0x001b:
            java.util.HashMap<java.lang.String, byte[]> r0 = r4.f6187e
            java.lang.Object r0 = r0.get(r5)
            byte[] r0 = (byte[]) r0
            java.lang.Object r6 = r4.m7018a(r0, r6)     // Catch:{ Exception -> 0x002d }
            if (r6 == 0) goto L_0x002c
            r4.m7019c(r5, r6)     // Catch:{ Exception -> 0x002d }
        L_0x002c:
            return r6
        L_0x002d:
            r5 = move-exception
            com.tencent.bugly.proguard.b r6 = new com.tencent.bugly.proguard.b
            r6.<init>(r5)
            throw r6
        L_0x0034:
            java.util.HashMap r0 = r4.f6182a
            boolean r0 = r0.containsKey(r5)
            if (r0 != 0) goto L_0x003d
            return r1
        L_0x003d:
            java.util.HashMap<java.lang.String, java.lang.Object> r0 = r4.f6189g
            boolean r0 = r0.containsKey(r5)
            if (r0 == 0) goto L_0x004c
            java.util.HashMap<java.lang.String, java.lang.Object> r6 = r4.f6189g
            java.lang.Object r5 = r6.get(r5)
            return r5
        L_0x004c:
            java.util.HashMap r0 = r4.f6182a
            java.lang.Object r0 = r0.get(r5)
            java.util.HashMap r0 = (java.util.HashMap) r0
            r1 = 0
            byte[] r2 = new byte[r1]
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0078
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getKey()
            java.lang.String r2 = (java.lang.String) r2
            java.lang.Object r0 = r0.getValue()
            r2 = r0
            byte[] r2 = (byte[]) r2
        L_0x0078:
            com.tencent.bugly.proguard.k r0 = r4.f6188f     // Catch:{ Exception -> 0x008f }
            r0.mo34627a(r2)     // Catch:{ Exception -> 0x008f }
            com.tencent.bugly.proguard.k r0 = r4.f6188f     // Catch:{ Exception -> 0x008f }
            java.lang.String r2 = r4.f6184c     // Catch:{ Exception -> 0x008f }
            r0.mo34617a(r2)     // Catch:{ Exception -> 0x008f }
            com.tencent.bugly.proguard.k r0 = r4.f6188f     // Catch:{ Exception -> 0x008f }
            r2 = 1
            java.lang.Object r6 = r0.mo34620a(r6, r1, r2)     // Catch:{ Exception -> 0x008f }
            r4.m7019c(r5, r6)     // Catch:{ Exception -> 0x008f }
            return r6
        L_0x008f:
            r5 = move-exception
            com.tencent.bugly.proguard.b r6 = new com.tencent.bugly.proguard.b
            r6.<init>(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2934d.mo34584b(java.lang.String, java.lang.Object):java.lang.Object");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.lang.Object, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    private Object m7018a(byte[] bArr, Object obj) {
        this.f6188f.mo34627a(bArr);
        this.f6188f.mo34617a(this.f6184c);
        return this.f6188f.mo34620a(obj, 0, true);
    }

    /* renamed from: c */
    private void m7019c(String str, Object obj) {
        this.f6189g.put(str, obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public byte[] mo34583a() {
        if (this.f6187e == null) {
            return super.mo34583a();
        }
        C2943l lVar = new C2943l(0);
        lVar.mo34638a(this.f6184c);
        lVar.mo34650a((Map) this.f6187e, 0);
        return C2945n.m7125a(lVar.mo34639a());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* renamed from: a */
    public void mo34582a(byte[] bArr) {
        try {
            super.mo34582a(bArr);
        } catch (Exception unused) {
            this.f6188f.mo34627a(bArr);
            this.f6188f.mo34617a(this.f6184c);
            HashMap hashMap = new HashMap(1);
            hashMap.put("", new byte[0]);
            this.f6187e = this.f6188f.mo34622a((Map) hashMap, 0, false);
        }
    }
}
