package com.tencent.bugly.proguard;

import android.content.Context;
import com.tencent.bugly.crashreport.common.info.C2851a;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/* renamed from: com.tencent.bugly.proguard.ap */
/* compiled from: BUGLY */
public class C2905ap {

    /* renamed from: a */
    public static boolean f6033a = true;

    /* renamed from: b */
    private static SimpleDateFormat f6034b = null;

    /* renamed from: c */
    private static int f6035c = 5120;

    /* renamed from: d */
    private static StringBuilder f6036d = null;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static StringBuilder f6037e = null;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static boolean f6038f = false;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static C2907a f6039g = null;

    /* renamed from: h */
    private static String f6040h = null;

    /* renamed from: i */
    private static String f6041i = null;

    /* renamed from: j */
    private static Context f6042j = null;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public static String f6043k = null;

    /* renamed from: l */
    private static boolean f6044l = false;

    /* renamed from: m */
    private static boolean f6045m = false;

    /* renamed from: n */
    private static int f6046n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public static final Object f6047o = new Object();

    static {
        try {
            f6034b = new SimpleDateFormat("MM-dd HH:mm:ss");
        } catch (Throwable unused) {
        }
    }

    /* renamed from: b */
    private static boolean m6879b(String str, String str2, String str3) {
        try {
            C2851a b = C2851a.m6471b();
            if (b == null || b.f5611M == null) {
                return false;
            }
            return b.f5611M.appendLogToNative(str, str2, str3);
        } catch (Throwable th) {
            if (C2903an.m6858a(th)) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    /* renamed from: f */
    private static String m6884f() {
        try {
            C2851a b = C2851a.m6471b();
            if (b == null || b.f5611M == null) {
                return null;
            }
            return b.f5611M.getLogFromNative();
        } catch (Throwable th) {
            if (C2903an.m6858a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0070, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m6873a(android.content.Context r3) {
        /*
            java.lang.Class<com.tencent.bugly.proguard.ap> r0 = com.tencent.bugly.proguard.C2905ap.class
            monitor-enter(r0)
            boolean r1 = com.tencent.bugly.proguard.C2905ap.f6044l     // Catch:{ all -> 0x0071 }
            if (r1 != 0) goto L_0x006f
            if (r3 == 0) goto L_0x006f
            boolean r1 = com.tencent.bugly.proguard.C2905ap.f6033a     // Catch:{ all -> 0x0071 }
            if (r1 != 0) goto L_0x000e
            goto L_0x006f
        L_0x000e:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r2 = 0
            r1.<init>(r2)     // Catch:{ all -> 0x006a }
            com.tencent.bugly.proguard.C2905ap.f6037e = r1     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r1.<init>(r2)     // Catch:{ all -> 0x006a }
            com.tencent.bugly.proguard.C2905ap.f6036d = r1     // Catch:{ all -> 0x006a }
            com.tencent.bugly.proguard.C2905ap.f6042j = r3     // Catch:{ all -> 0x006a }
            com.tencent.bugly.crashreport.common.info.a r3 = com.tencent.bugly.crashreport.common.info.C2851a.m6470a(r3)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = r3.f5660e     // Catch:{ all -> 0x006a }
            com.tencent.bugly.proguard.C2905ap.f6040h = r1     // Catch:{ all -> 0x006a }
            r3.getClass()     // Catch:{ all -> 0x006a }
            java.lang.String r3 = ""
            com.tencent.bugly.proguard.C2905ap.f6041i = r3     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r3.<init>()     // Catch:{ all -> 0x006a }
            android.content.Context r1 = com.tencent.bugly.proguard.C2905ap.f6042j     // Catch:{ all -> 0x006a }
            java.io.File r1 = r1.getFilesDir()     // Catch:{ all -> 0x006a }
            java.lang.String r1 = r1.getPath()     // Catch:{ all -> 0x006a }
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "/"
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "buglylog_"
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = com.tencent.bugly.proguard.C2905ap.f6040h     // Catch:{ all -> 0x006a }
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = "_"
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = com.tencent.bugly.proguard.C2905ap.f6041i     // Catch:{ all -> 0x006a }
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r1 = ".txt"
            r3.append(r1)     // Catch:{ all -> 0x006a }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x006a }
            com.tencent.bugly.proguard.C2905ap.f6043k = r3     // Catch:{ all -> 0x006a }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x006a }
            com.tencent.bugly.proguard.C2905ap.f6046n = r3     // Catch:{ all -> 0x006a }
        L_0x006a:
            r3 = 1
            com.tencent.bugly.proguard.C2905ap.f6044l = r3     // Catch:{ all -> 0x0071 }
            monitor-exit(r0)
            return
        L_0x006f:
            monitor-exit(r0)
            return
        L_0x0071:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2905ap.m6873a(android.content.Context):void");
    }

    /* renamed from: a */
    public static void m6872a(int i) {
        synchronized (f6047o) {
            f6035c = i;
            if (i < 0) {
                f6035c = 0;
            } else if (i > 10240) {
                f6035c = 10240;
            }
        }
    }

    /* renamed from: a */
    public static void m6876a(boolean z) {
        C2903an.m6857a("[LogUtil] Whether can record user log into native: " + z, new Object[0]);
        f6045m = z;
    }

    /* renamed from: a */
    public static void m6875a(String str, String str2, Throwable th) {
        if (th != null) {
            String message = th.getMessage();
            if (message == null) {
                message = "";
            }
            m6874a(str, str2, message + 10 + C2908aq.m6925b(th));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0053, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void m6874a(java.lang.String r3, java.lang.String r4, java.lang.String r5) {
        /*
            java.lang.Class<com.tencent.bugly.proguard.ap> r0 = com.tencent.bugly.proguard.C2905ap.class
            monitor-enter(r0)
            boolean r1 = com.tencent.bugly.proguard.C2905ap.f6044l     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0052
            boolean r1 = com.tencent.bugly.proguard.C2905ap.f6033a     // Catch:{ all -> 0x0054 }
            if (r1 != 0) goto L_0x000c
            goto L_0x0052
        L_0x000c:
            boolean r1 = com.tencent.bugly.proguard.C2905ap.f6045m     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0018
            boolean r1 = m6879b(r3, r4, r5)     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0018
            monitor-exit(r0)
            return
        L_0x0018:
            int r1 = android.os.Process.myTid()     // Catch:{ all -> 0x0054 }
            long r1 = (long) r1     // Catch:{ all -> 0x0054 }
            java.lang.String r3 = m6871a(r3, r4, r5, r1)     // Catch:{ all -> 0x0054 }
            java.lang.Object r4 = com.tencent.bugly.proguard.C2905ap.f6047o     // Catch:{ all -> 0x0054 }
            monitor-enter(r4)     // Catch:{ all -> 0x0054 }
            java.lang.StringBuilder r5 = com.tencent.bugly.proguard.C2905ap.f6037e     // Catch:{ all -> 0x004f }
            r5.append(r3)     // Catch:{ all -> 0x004f }
            java.lang.StringBuilder r3 = com.tencent.bugly.proguard.C2905ap.f6037e     // Catch:{ all -> 0x004f }
            int r3 = r3.length()     // Catch:{ all -> 0x004f }
            int r5 = com.tencent.bugly.proguard.C2905ap.f6035c     // Catch:{ all -> 0x004f }
            if (r3 > r5) goto L_0x0036
            monitor-exit(r4)     // Catch:{ all -> 0x004f }
            monitor-exit(r0)
            return
        L_0x0036:
            boolean r3 = com.tencent.bugly.proguard.C2905ap.f6038f     // Catch:{ all -> 0x004f }
            if (r3 == 0) goto L_0x003d
            monitor-exit(r4)     // Catch:{ all -> 0x004f }
            monitor-exit(r0)
            return
        L_0x003d:
            r3 = 1
            com.tencent.bugly.proguard.C2905ap.f6038f = r3     // Catch:{ all -> 0x004f }
            com.tencent.bugly.proguard.am r3 = com.tencent.bugly.proguard.C2901am.m6848a()     // Catch:{ all -> 0x004f }
            com.tencent.bugly.proguard.ap$1 r5 = new com.tencent.bugly.proguard.ap$1     // Catch:{ all -> 0x004f }
            r5.<init>()     // Catch:{ all -> 0x004f }
            r3.mo34547a(r5)     // Catch:{ all -> 0x004f }
            monitor-exit(r4)     // Catch:{ all -> 0x004f }
            monitor-exit(r0)
            return
        L_0x004f:
            r3 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x004f }
            throw r3     // Catch:{ all -> 0x0054 }
        L_0x0052:
            monitor-exit(r0)
            return
        L_0x0054:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2905ap.m6874a(java.lang.String, java.lang.String, java.lang.String):void");
    }

    /* renamed from: a */
    private static String m6871a(String str, String str2, String str3, long j) {
        String str4;
        f6036d.setLength(0);
        if (str3.length() > 30720) {
            str3 = str3.substring(str3.length() - 30720, str3.length() - 1);
        }
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = f6034b;
        if (simpleDateFormat != null) {
            str4 = simpleDateFormat.format(date);
        } else {
            str4 = date.toString();
        }
        StringBuilder sb = f6036d;
        sb.append(str4);
        sb.append(" ");
        sb.append(f6046n);
        sb.append(" ");
        sb.append(j);
        sb.append(" ");
        sb.append(str);
        sb.append(" ");
        sb.append(str2);
        sb.append(": ");
        sb.append(str3);
        sb.append("\u0001\r\n");
        return f6036d.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.aq.a(java.io.File, int, boolean):java.lang.String
     arg types: [java.io.File, int, int]
     candidates:
      com.tencent.bugly.proguard.aq.a(android.content.Context, int, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.aq.a(android.content.Context, java.lang.String, long):boolean
      com.tencent.bugly.proguard.aq.a(java.io.File, java.io.File, int):boolean
      com.tencent.bugly.proguard.aq.a(int, byte[], byte[]):byte[]
      com.tencent.bugly.proguard.aq.a(java.io.File, java.lang.String, java.lang.String):byte[]
      com.tencent.bugly.proguard.aq.a(byte[], int, java.lang.String):byte[]
      com.tencent.bugly.proguard.aq.a(java.io.File, int, boolean):java.lang.String */
    /* renamed from: a */
    public static byte[] m6877a() {
        if (!f6033a) {
            return null;
        }
        if (f6045m) {
            C2903an.m6857a("[LogUtil] Get user log from native.", new Object[0]);
            String f = m6884f();
            if (f != null) {
                C2903an.m6857a("[LogUtil] Got user log from native: %d bytes", Integer.valueOf(f.length()));
                return C2908aq.m6919a((File) null, f, "BuglyNativeLog.txt");
            }
        }
        StringBuilder sb = new StringBuilder();
        synchronized (f6047o) {
            if (f6039g != null && f6039g.f6048a && f6039g.f6049b != null && f6039g.f6049b.length() > 0) {
                sb.append(C2908aq.m6900a(f6039g.f6049b, 30720, true));
            }
            if (f6037e != null && f6037e.length() > 0) {
                sb.append(f6037e.toString());
            }
        }
        return C2908aq.m6919a((File) null, sb.toString(), "BuglyLog.txt");
    }

    /* renamed from: com.tencent.bugly.proguard.ap$a */
    /* compiled from: BUGLY */
    public static class C2907a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public boolean f6048a;
        /* access modifiers changed from: private */

        /* renamed from: b */
        public File f6049b;

        /* renamed from: c */
        private String f6050c;

        /* renamed from: d */
        private long f6051d;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public long f6052e = 30720;

        public C2907a(String str) {
            if (str != null && !str.equals("")) {
                this.f6050c = str;
                this.f6048a = m6886a();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public boolean m6886a() {
            try {
                this.f6049b = new File(this.f6050c);
                if (this.f6049b.exists() && !this.f6049b.delete()) {
                    this.f6048a = false;
                    return false;
                } else if (this.f6049b.createNewFile()) {
                    return true;
                } else {
                    this.f6048a = false;
                    return false;
                }
            } catch (Throwable th) {
                C2903an.m6858a(th);
                this.f6048a = false;
                return false;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException}
         arg types: [java.io.File, int]
         candidates:
          ClspMth{java.io.FileOutputStream.<init>(java.lang.String, boolean):void throws java.io.FileNotFoundException}
          ClspMth{java.io.FileOutputStream.<init>(java.io.File, boolean):void throws java.io.FileNotFoundException} */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0036 A[SYNTHETIC, Splitter:B:19:0x0036] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean mo34553a(java.lang.String r10) {
            /*
                r9 = this;
                boolean r0 = r9.f6048a
                r1 = 0
                if (r0 != 0) goto L_0x0006
                return r1
            L_0x0006:
                r0 = 0
                java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ all -> 0x002d }
                java.io.File r3 = r9.f6049b     // Catch:{ all -> 0x002d }
                r4 = 1
                r2.<init>(r3, r4)     // Catch:{ all -> 0x002d }
                java.lang.String r0 = "UTF-8"
                byte[] r10 = r10.getBytes(r0)     // Catch:{ all -> 0x002b }
                r2.write(r10)     // Catch:{ all -> 0x002b }
                r2.flush()     // Catch:{ all -> 0x002b }
                r2.close()     // Catch:{ all -> 0x002b }
                long r5 = r9.f6051d     // Catch:{ all -> 0x002b }
                int r10 = r10.length     // Catch:{ all -> 0x002b }
                long r7 = (long) r10     // Catch:{ all -> 0x002b }
                long r5 = r5 + r7
                r9.f6051d = r5     // Catch:{ all -> 0x002b }
                r9.f6048a = r4     // Catch:{ all -> 0x002b }
                r2.close()     // Catch:{ IOException -> 0x002a }
            L_0x002a:
                return r4
            L_0x002b:
                r10 = move-exception
                goto L_0x002f
            L_0x002d:
                r10 = move-exception
                r2 = r0
            L_0x002f:
                com.tencent.bugly.proguard.C2903an.m6858a(r10)     // Catch:{ all -> 0x003a }
                r9.f6048a = r1     // Catch:{ all -> 0x003a }
                if (r2 == 0) goto L_0x0039
                r2.close()     // Catch:{ IOException -> 0x0039 }
            L_0x0039:
                return r1
            L_0x003a:
                r10 = move-exception
                if (r2 == 0) goto L_0x0040
                r2.close()     // Catch:{ IOException -> 0x0040 }
            L_0x0040:
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2905ap.C2907a.mo34553a(java.lang.String):boolean");
        }
    }
}
