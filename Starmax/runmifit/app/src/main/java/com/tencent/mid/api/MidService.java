package com.tencent.mid.api;

import android.content.Context;
import com.tencent.mid.p053a.C3017g;
import com.tencent.mid.p054b.C3031g;

public class MidService {

    /* renamed from: a */
    private static boolean f6538a = true;

    public static String getMidRequestHost() {
        return null;
    }

    public static String getMidRequestUrl() {
        return null;
    }

    public static void setMidRequestUrl(String str) {
    }

    public static void requestMid(Context context, MidCallback midCallback) {
        if (midCallback == null) {
            throw new IllegalArgumentException("error, callback is null!");
        } else if (context == null) {
            midCallback.onFail(MidConstants.ERROR_ARGUMENT, "content is null!");
        } else {
            C3017g.m7368a(context.getApplicationContext(), midCallback);
        }
    }

    public static String getLocalMidOnly(Context context) {
        return C3031g.m7437a(context).mo34921f();
    }

    public static String getMid(Context context) {
        return C3017g.m7373b(context);
    }

    public static boolean isMidValid(String str) {
        return C3017g.m7371a(str);
    }

    public static void enableDebug(boolean z) {
        C3017g.m7369a(z);
    }

    public static boolean isEnableDebug() {
        return C3017g.m7370a();
    }

    public static boolean isEnableReportWifiList() {
        return f6538a;
    }

    public static void setEnableReportWifiList(boolean z) {
        f6538a = z;
    }

    public static String getNewMid(Context context) {
        return C3031g.m7437a(context).mo34912b();
    }
}
