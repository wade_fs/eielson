package com.tencent.bugly.beta.utils;

import android.os.Handler;
import android.os.Looper;

/* renamed from: com.tencent.bugly.beta.utils.e */
/* compiled from: BUGLY */
public class C2836e {

    /* renamed from: a */
    private static Handler f5540a;

    /* renamed from: a */
    private static Handler m6397a() {
        if (f5540a == null && Looper.getMainLooper() != null) {
            f5540a = new Handler(Looper.getMainLooper());
        }
        return f5540a;
    }

    /* renamed from: a */
    public static void m6398a(Runnable runnable) {
        if (m6397a() != null) {
            f5540a.post(runnable);
        }
    }

    /* renamed from: b */
    public static void m6400b(Runnable runnable) {
        if (m6397a() != null) {
            f5540a.removeCallbacks(runnable);
        }
    }

    /* renamed from: a */
    public static void m6399a(Runnable runnable, long j) {
        if (m6397a() != null) {
            f5540a.postDelayed(runnable, j);
        }
    }
}
