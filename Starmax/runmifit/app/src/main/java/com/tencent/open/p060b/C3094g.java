package com.tencent.open.p060b;

import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.os.SystemClock;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.api.client.http.UrlEncodedParser;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.download.MimeType;
import com.tencent.connect.common.Constants;
import com.tencent.mid.api.MidEntity;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3122f;
import com.tencent.open.utils.C3126i;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import java.io.Serializable;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.Executor;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.open.b.g */
/* compiled from: ProGuard */
public class C3094g {

    /* renamed from: a */
    protected static C3094g f6765a;

    /* renamed from: b */
    protected Random f6766b = new Random();

    /* renamed from: c */
    protected List<Serializable> f6767c = Collections.synchronizedList(new ArrayList());

    /* renamed from: d */
    protected List<Serializable> f6768d = Collections.synchronizedList(new ArrayList());

    /* renamed from: e */
    protected HandlerThread f6769e = null;

    /* renamed from: f */
    protected Handler f6770f;

    /* renamed from: g */
    protected Executor f6771g = C3126i.m7761b();

    /* renamed from: h */
    protected Executor f6772h = C3126i.m7761b();

    /* renamed from: a */
    public static synchronized C3094g m7676a() {
        C3094g gVar;
        synchronized (C3094g.class) {
            if (f6765a == null) {
                f6765a = new C3094g();
            }
            gVar = f6765a;
        }
        return gVar;
    }

    private C3094g() {
        if (this.f6769e == null) {
            this.f6769e = new HandlerThread("opensdk.report.handlerthread", 10);
            this.f6769e.start();
        }
        if (this.f6769e.isAlive() && this.f6769e.getLooper() != null) {
            this.f6770f = new Handler(this.f6769e.getLooper()) {
                /* class com.tencent.open.p060b.C3094g.C30951 */

                public void handleMessage(Message message) {
                    int i = message.what;
                    if (i == 1000) {
                        C3094g.this.mo35137b();
                    } else if (i == 1001) {
                        C3094g.this.mo35140e();
                    }
                    super.handleMessage(message);
                }
            };
        }
    }

    /* renamed from: a */
    public void mo35131a(final Bundle bundle, String str, final boolean z) {
        if (bundle != null) {
            C3082f.m7628a("openSDK_LOG.ReportManager", "-->reportVia, bundle: " + bundle.toString());
            if (mo35136a("report_via", str) || z) {
                this.f6771g.execute(new Runnable() {
                    /* class com.tencent.open.p060b.C3094g.C30962 */

                    public void run() {
                        try {
                            Bundle bundle = new Bundle();
                            bundle.putString("uin", Constants.DEFAULT_UIN);
                            bundle.putString(MidEntity.TAG_IMEI, C3090c.m7661b(C3121e.m7727a()));
                            bundle.putString(MidEntity.TAG_IMSI, C3090c.m7662c(C3121e.m7727a()));
                            bundle.putString("android_id", C3090c.m7663d(C3121e.m7727a()));
                            bundle.putString(MidEntity.TAG_MAC, C3090c.m7658a());
                            bundle.putString("platform", "1");
                            bundle.putString("os_ver", Build.VERSION.RELEASE);
                            bundle.putString("position", C3131k.m7784c(C3121e.m7727a()));
                            bundle.putString("network", C3088a.m7653a(C3121e.m7727a()));
                            bundle.putString("language", C3090c.m7660b());
                            bundle.putString("resolution", C3090c.m7659a(C3121e.m7727a()));
                            bundle.putString("apn", C3088a.m7654b(C3121e.m7727a()));
                            bundle.putString("model_name", Build.MODEL);
                            bundle.putString("timezone", TimeZone.getDefault().getID());
                            bundle.putString("sdk_ver", Constants.SDK_VERSION);
                            bundle.putString("qz_ver", C3131k.m7787d(C3121e.m7727a(), Constants.PACKAGE_QZONE));
                            bundle.putString("qq_ver", C3131k.m7785c(C3121e.m7727a(), "com.tencent.mobileqq"));
                            bundle.putString("qua", C3131k.m7790e(C3121e.m7727a(), C3121e.m7729b()));
                            bundle.putString("packagename", C3121e.m7729b());
                            bundle.putString("app_ver", C3131k.m7787d(C3121e.m7727a(), C3121e.m7729b()));
                            if (bundle != null) {
                                bundle.putAll(bundle);
                            }
                            C3094g.this.f6768d.add(new C3089b(bundle));
                            int size = C3094g.this.f6768d.size();
                            int a = C3122f.m7733a(C3121e.m7727a(), (String) null).mo35169a("Agent_ReportTimeInterval");
                            if (a == 0) {
                                a = 10000;
                            }
                            if (!C3094g.this.mo35135a("report_via", size)) {
                                if (!z) {
                                    if (!C3094g.this.f6770f.hasMessages(1001)) {
                                        Message obtain = Message.obtain();
                                        obtain.what = 1001;
                                        C3094g.this.f6770f.sendMessageDelayed(obtain, (long) a);
                                        return;
                                    }
                                    return;
                                }
                            }
                            C3094g.this.mo35140e();
                            C3094g.this.f6770f.removeMessages(1001);
                        } catch (Exception e) {
                            C3082f.m7632b("openSDK_LOG.ReportManager", "--> reporVia, exception in sub thread.", e);
                        }
                    }
                });
            }
        }
    }

    /* renamed from: a */
    public void mo35132a(String str, long j, long j2, long j3, int i) {
        mo35133a(str, j, j2, j3, i, "", false);
    }

    /* renamed from: a */
    public void mo35133a(String str, long j, long j2, long j3, int i, String str2, boolean z) {
        int i2 = i;
        C3082f.m7628a("openSDK_LOG.ReportManager", "-->reportCgi, command: " + str + " | startTime: " + j + " | reqSize:" + j2 + " | rspSize: " + j3 + " | responseCode: " + i2 + " | detail: " + str2);
        if (mo35136a("report_cgi", "" + i2) || z) {
            final long j4 = j;
            final String str3 = str;
            final String str4 = str2;
            final int i3 = i;
            final long j5 = j2;
            final long j6 = j3;
            final boolean z2 = z;
            this.f6772h.execute(new Runnable() {
                /* class com.tencent.open.p060b.C3094g.C30973 */

                public void run() {
                    try {
                        long elapsedRealtime = SystemClock.elapsedRealtime() - j4;
                        Bundle bundle = new Bundle();
                        String a = C3088a.m7653a(C3121e.m7727a());
                        bundle.putString("apn", a);
                        bundle.putString("appid", "1000067");
                        bundle.putString("commandid", str3);
                        bundle.putString("detail", str4);
                        StringBuilder sb = new StringBuilder();
                        sb.append("network=");
                        sb.append(a);
                        sb.append('&');
                        sb.append("sdcard=");
                        sb.append(Environment.getExternalStorageState().equals("mounted") ? 1 : 0);
                        sb.append('&');
                        sb.append("wifi=");
                        sb.append(C3088a.m7657e(C3121e.m7727a()));
                        bundle.putString("deviceInfo", sb.toString());
                        int a2 = 100 / C3094g.this.mo35130a(i3);
                        if (a2 <= 0) {
                            a2 = 1;
                        } else if (a2 > 100) {
                            a2 = 100;
                        }
                        bundle.putString("frequency", a2 + "");
                        bundle.putString("reqSize", j5 + "");
                        bundle.putString("resultCode", i3 + "");
                        bundle.putString("rspSize", j6 + "");
                        bundle.putString("timeCost", elapsedRealtime + "");
                        bundle.putString("uin", Constants.DEFAULT_UIN);
                        C3094g.this.f6767c.add(new C3089b(bundle));
                        int size = C3094g.this.f6767c.size();
                        int a3 = C3122f.m7733a(C3121e.m7727a(), (String) null).mo35169a("Agent_ReportTimeInterval");
                        if (a3 == 0) {
                            a3 = 10000;
                        }
                        if (!C3094g.this.mo35135a("report_cgi", size)) {
                            if (!z2) {
                                if (!C3094g.this.f6770f.hasMessages(1000)) {
                                    Message obtain = Message.obtain();
                                    obtain.what = 1000;
                                    C3094g.this.f6770f.sendMessageDelayed(obtain, (long) a3);
                                    return;
                                }
                                return;
                            }
                        }
                        C3094g.this.mo35137b();
                        C3094g.this.f6770f.removeMessages(1000);
                    } catch (Exception e) {
                        C3082f.m7632b("openSDK_LOG.ReportManager", "--> reportCGI, exception in sub thread.", e);
                    }
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo35137b() {
        this.f6772h.execute(new Runnable() {
            /* class com.tencent.open.p060b.C3094g.C30984 */

            /*  JADX ERROR: IF instruction can be used only in fallback mode
                jadx.core.utils.exceptions.CodegenException: IF instruction can be used only in fallback mode
                	at jadx.core.codegen.InsnGen.fallbackOnlyInsn(InsnGen.java:583)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:489)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:252)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
                	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:193)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:66)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:142)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:62)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
                	at jadx.core.codegen.RegionGen.makeTryCatch(RegionGen.java:305)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:68)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:337)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:290)
                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:259)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                	at java.util.ArrayList.forEach(ArrayList.java:1257)
                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
                	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
                	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:255)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:246)
                	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:681)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:611)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:366)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:233)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:122)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:106)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:793)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:733)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:370)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:252)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:215)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:208)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:337)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:290)
                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:259)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                	at java.util.ArrayList.forEach(ArrayList.java:1257)
                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
                	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
                	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:255)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:246)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:226)
                	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:115)
                	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:81)
                	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:45)
                	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:34)
                	at jadx.core.codegen.CodeGen.generate(CodeGen.java:22)
                	at jadx.core.ProcessClass.generateCode(ProcessClass.java:60)
                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:314)
                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:293)
                */
            public void run() {
                /*
                    r14 = this;
                    java.lang.String r0 = "report_cgi"
                    java.lang.String r1 = "https://wspeed.qq.com/w.cgi"
                    java.lang.String r2 = "-->doReportCgi, doupload exception"
                    java.lang.String r3 = "openSDK_LOG.ReportManager"
                    com.tencent.open.b.g r4 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00b8 }
                    android.os.Bundle r4 = r4.mo35138c()     // Catch:{ Exception -> 0x00b8 }
                    if (r4 != 0) goto L_0x0011
                    return
                L_0x0011:
                    android.content.Context r5 = com.tencent.open.utils.C3121e.m7727a()     // Catch:{ Exception -> 0x00b8 }
                    r6 = 0
                    com.tencent.open.utils.f r5 = com.tencent.open.utils.C3122f.m7733a(r5, r6)     // Catch:{ Exception -> 0x00b8 }
                    java.lang.String r7 = "Common_HttpRetryCount"
                    int r5 = r5.mo35169a(r7)     // Catch:{ Exception -> 0x00b8 }
                    if (r5 != 0) goto L_0x0023
                    r5 = 3
                L_0x0023:
                    java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00b8 }
                    r7.<init>()     // Catch:{ Exception -> 0x00b8 }
                    java.lang.String r8 = "-->doReportCgi, retryCount: "
                    r7.append(r8)     // Catch:{ Exception -> 0x00b8 }
                    r7.append(r5)     // Catch:{ Exception -> 0x00b8 }
                    java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x00b8 }
                    com.tencent.open.p059a.C3082f.m7631b(r3, r7)     // Catch:{ Exception -> 0x00b8 }
                    r7 = 0
                    r8 = 0
                L_0x0039:
                    r9 = 1
                    int r8 = r8 + r9
                    android.content.Context r10 = com.tencent.open.utils.C3121e.m7727a()     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    org.apache.http.client.HttpClient r10 = com.tencent.open.utils.HttpUtils.getHttpClient(r10, r6, r1)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    org.apache.http.client.methods.HttpPost r11 = new org.apache.http.client.methods.HttpPost     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r11.<init>(r1)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    java.lang.String r12 = "Accept-Encoding"
                    java.lang.String r13 = "gzip"
                    r11.addHeader(r12, r13)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    java.lang.String r12 = "Content-Type"
                    java.lang.String r13 = "application/x-www-form-urlencoded"
                    r11.setHeader(r12, r13)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    java.lang.String r12 = com.tencent.open.utils.HttpUtils.encodeUrl(r4)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    byte[] r12 = com.tencent.open.utils.C3131k.m7799i(r12)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    org.apache.http.entity.ByteArrayEntity r13 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r13.<init>(r12)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r11.setEntity(r13)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    org.apache.http.HttpResponse r10 = r10.execute(r11)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    org.apache.http.StatusLine r10 = r10.getStatusLine()     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    int r10 = r10.getStatusCode()     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r11.<init>()     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    java.lang.String r12 = "-->doReportCgi, statusCode: "
                    r11.append(r12)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r11.append(r10)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    java.lang.String r11 = r11.toString()     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    com.tencent.open.p059a.C3082f.m7631b(r3, r11)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r11 = 200(0xc8, float:2.8E-43)
                    if (r10 != r11) goto L_0x00a3
                    com.tencent.open.b.f r10 = com.tencent.open.p060b.C3093f.m7672a()     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r10.mo35127b(r0)     // Catch:{ ConnectTimeoutException -> 0x009d, SocketTimeoutException -> 0x0098, Exception -> 0x0093 }
                    r7 = 1
                    goto L_0x00a3
                L_0x0093:
                    r1 = move-exception
                    com.tencent.open.p059a.C3082f.m7632b(r3, r2, r1)     // Catch:{ Exception -> 0x00b8 }
                    goto L_0x00a3
                L_0x0098:
                    r9 = move-exception
                    com.tencent.open.p059a.C3082f.m7632b(r3, r2, r9)     // Catch:{ Exception -> 0x00b8 }
                    goto L_0x00a1
                L_0x009d:
                    r9 = move-exception
                    com.tencent.open.p059a.C3082f.m7632b(r3, r2, r9)     // Catch:{ Exception -> 0x00b8 }
                L_0x00a1:
                    if (r8 < r5) goto L_0x0039
                L_0x00a3:
                    if (r7 != 0) goto L_0x00b0
                    com.tencent.open.b.f r1 = com.tencent.open.p060b.C3093f.m7672a()     // Catch:{ Exception -> 0x00b8 }
                    com.tencent.open.b.g r2 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00b8 }
                    java.util.List<java.io.Serializable> r2 = r2.f6767c     // Catch:{ Exception -> 0x00b8 }
                    r1.mo35126a(r0, r2)     // Catch:{ Exception -> 0x00b8 }
                L_0x00b0:
                    com.tencent.open.b.g r0 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00b8 }
                    java.util.List<java.io.Serializable> r0 = r0.f6767c     // Catch:{ Exception -> 0x00b8 }
                    r0.clear()     // Catch:{ Exception -> 0x00b8 }
                    goto L_0x00be
                L_0x00b8:
                    r0 = move-exception
                    java.lang.String r1 = "-->doReportCgi, doupload exception out."
                    com.tencent.open.p059a.C3082f.m7632b(r3, r1, r0)
                L_0x00be:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3094g.C30984.run():void");
            }
        });
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0055, code lost:
        if (r5.f6766b.nextInt(100) < r6) goto L_0x0057;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x003f, code lost:
        if (r5.f6766b.nextInt(100) < r6) goto L_0x0057;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo35136a(java.lang.String r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "-->availableFrequency, report: "
            r0.append(r1)
            r0.append(r6)
            java.lang.String r1 = " | ext: "
            r0.append(r1)
            r0.append(r7)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "openSDK_LOG.ReportManager"
            com.tencent.open.p059a.C3082f.m7631b(r1, r0)
            boolean r0 = android.text.TextUtils.isEmpty(r6)
            r2 = 0
            if (r0 == 0) goto L_0x0026
            return r2
        L_0x0026:
            java.lang.String r0 = "report_cgi"
            boolean r0 = r6.equals(r0)
            r3 = 1
            r4 = 100
            if (r0 == 0) goto L_0x0043
            int r6 = java.lang.Integer.parseInt(r7)     // Catch:{ Exception -> 0x0042 }
            int r6 = r5.mo35130a(r6)
            java.util.Random r7 = r5.f6766b
            int r7 = r7.nextInt(r4)
            if (r7 >= r6) goto L_0x005b
            goto L_0x0057
        L_0x0042:
            return r2
        L_0x0043:
            java.lang.String r0 = "report_via"
            boolean r6 = r6.equals(r0)
            if (r6 == 0) goto L_0x0059
            int r6 = com.tencent.open.p060b.C3092e.m7671a(r7)
            java.util.Random r7 = r5.f6766b
            int r7 = r7.nextInt(r4)
            if (r7 >= r6) goto L_0x005b
        L_0x0057:
            r2 = 1
            goto L_0x005b
        L_0x0059:
            r6 = 100
        L_0x005b:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r0 = "-->availableFrequency, result: "
            r7.append(r0)
            r7.append(r2)
            java.lang.String r0 = " | frequency: "
            r7.append(r0)
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            com.tencent.open.p059a.C3082f.m7631b(r1, r6)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3094g.mo35136a(java.lang.String, java.lang.String):boolean");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0019, code lost:
        if (r0 == 0) goto L_0x001b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0033, code lost:
        if (r0 == 0) goto L_0x001b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x005f A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0061 A[RETURN] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo35135a(java.lang.String r5, int r6) {
        /*
            r4 = this;
            java.lang.String r0 = "report_cgi"
            boolean r0 = r5.equals(r0)
            r1 = 5
            r2 = 0
            r3 = 0
            if (r0 == 0) goto L_0x001d
            android.content.Context r0 = com.tencent.open.utils.C3121e.m7727a()
            com.tencent.open.utils.f r0 = com.tencent.open.utils.C3122f.m7733a(r0, r2)
            java.lang.String r2 = "Common_CGIReportMaxcount"
            int r0 = r0.mo35169a(r2)
            if (r0 != 0) goto L_0x0037
        L_0x001b:
            r0 = 5
            goto L_0x0037
        L_0x001d:
            java.lang.String r0 = "report_via"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0036
            android.content.Context r0 = com.tencent.open.utils.C3121e.m7727a()
            com.tencent.open.utils.f r0 = com.tencent.open.utils.C3122f.m7733a(r0, r2)
            java.lang.String r2 = "Agent_ReportBatchCount"
            int r0 = r0.mo35169a(r2)
            if (r0 != 0) goto L_0x0037
            goto L_0x001b
        L_0x0036:
            r0 = 0
        L_0x0037:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "-->availableCount, report: "
            r1.append(r2)
            r1.append(r5)
            java.lang.String r5 = " | dataSize: "
            r1.append(r5)
            r1.append(r6)
            java.lang.String r5 = " | maxcount: "
            r1.append(r5)
            r1.append(r0)
            java.lang.String r5 = r1.toString()
            java.lang.String r1 = "openSDK_LOG.ReportManager"
            com.tencent.open.p059a.C3082f.m7631b(r1, r5)
            if (r6 < r0) goto L_0x0061
            r5 = 1
            return r5
        L_0x0061:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3094g.mo35135a(java.lang.String, int):boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo35130a(int i) {
        if (i == 0) {
            int a = C3122f.m7733a(C3121e.m7727a(), (String) null).mo35169a("Common_CGIReportFrequencySuccess");
            if (a == 0) {
                return 10;
            }
            return a;
        }
        int a2 = C3122f.m7733a(C3121e.m7727a(), (String) null).mo35169a("Common_CGIReportFrequencyFailed");
        if (a2 == 0) {
            return 100;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Bundle mo35138c() {
        if (this.f6767c.size() == 0) {
            return null;
        }
        C3089b bVar = (C3089b) this.f6767c.get(0);
        if (bVar == null) {
            C3082f.m7631b("openSDK_LOG.ReportManager", "-->prepareCgiData, the 0th cgireportitem is null.");
            return null;
        }
        String str = bVar.f6756a.get("appid");
        List<Serializable> a = C3093f.m7672a().mo35125a("report_cgi");
        if (a != null) {
            this.f6767c.addAll(a);
        }
        C3082f.m7631b("openSDK_LOG.ReportManager", "-->prepareCgiData, mCgiList size: " + this.f6767c.size());
        if (this.f6767c.size() == 0) {
            return null;
        }
        Bundle bundle = new Bundle();
        try {
            bundle.putString("appid", str);
            bundle.putString("releaseversion", Constants.SDK_VERSION_REPORT);
            bundle.putString(Config.DEVICE_PART, Build.DEVICE);
            bundle.putString("qua", Constants.SDK_QUA);
            bundle.putString("key", "apn,frequency,commandid,resultcode,tmcost,reqsize,rspsize,detail,touin,deviceinfo");
            for (int i = 0; i < this.f6767c.size(); i++) {
                C3089b bVar2 = (C3089b) this.f6767c.get(i);
                bundle.putString(i + "_1", bVar2.f6756a.get("apn"));
                bundle.putString(i + "_2", bVar2.f6756a.get("frequency"));
                bundle.putString(i + "_3", bVar2.f6756a.get("commandid"));
                bundle.putString(i + "_4", bVar2.f6756a.get("resultCode"));
                bundle.putString(i + "_5", bVar2.f6756a.get("timeCost"));
                bundle.putString(i + "_6", bVar2.f6756a.get("reqSize"));
                bundle.putString(i + "_7", bVar2.f6756a.get("rspSize"));
                bundle.putString(i + "_8", bVar2.f6756a.get("detail"));
                bundle.putString(i + "_9", bVar2.f6756a.get("uin"));
                bundle.putString(i + "_10", C3090c.m7664e(C3121e.m7727a()) + "&" + bVar2.f6756a.get("deviceInfo"));
            }
            C3082f.m7628a("openSDK_LOG.ReportManager", "-->prepareCgiData, end. params: " + bundle.toString());
            return bundle;
        } catch (Exception e) {
            C3082f.m7632b("openSDK_LOG.ReportManager", "-->prepareCgiData, exception.", e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Bundle mo35139d() {
        List<Serializable> a = C3093f.m7672a().mo35125a("report_via");
        if (a != null) {
            this.f6768d.addAll(a);
        }
        C3082f.m7631b("openSDK_LOG.ReportManager", "-->prepareViaData, mViaList size: " + this.f6768d.size());
        if (this.f6768d.size() == 0) {
            return null;
        }
        JSONArray jSONArray = new JSONArray();
        Iterator<Serializable> it = this.f6768d.iterator();
        while (it.hasNext()) {
            JSONObject jSONObject = new JSONObject();
            C3089b bVar = (C3089b) it.next();
            for (String str : bVar.f6756a.keySet()) {
                try {
                    String str2 = bVar.f6756a.get(str);
                    if (str2 == null) {
                        str2 = "";
                    }
                    jSONObject.put(str, str2);
                } catch (JSONException e) {
                    C3082f.m7632b("openSDK_LOG.ReportManager", "-->prepareViaData, put bundle to json array exception", e);
                }
            }
            jSONArray.put(jSONObject);
        }
        C3082f.m7628a("openSDK_LOG.ReportManager", "-->prepareViaData, JSONArray array: " + jSONArray.toString());
        Bundle bundle = new Bundle();
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("data", jSONArray);
            bundle.putString("data", jSONObject2.toString());
            return bundle;
        } catch (JSONException e2) {
            C3082f.m7632b("openSDK_LOG.ReportManager", "-->prepareViaData, put bundle to json array exception", e2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo35140e() {
        this.f6771g.execute(new Runnable() {
            /* class com.tencent.open.p060b.C3094g.C30995 */

            /* JADX WARNING: Code restructure failed: missing block: B:13:0x0051, code lost:
                r7 = -4;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:18:0x005a, code lost:
                if (android.text.TextUtils.isEmpty(r0.f6870a) == false) goto L_0x005c;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:22:0x0067, code lost:
                r0 = r4;
                r6 = -6;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:24:0x006b, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:27:0x0070, code lost:
                r6 = com.tencent.open.utils.HttpUtils.getErrorCodeFromException(r0);
                r0 = r16;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:28:0x0074, code lost:
                r0 = move-exception;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:30:?, code lost:
                r6 = java.lang.Integer.parseInt(r0.getMessage().replace(com.tencent.open.utils.HttpUtils.HttpStatusException.ERROR_INFO, ""));
             */
            /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
                r1.f6786a.f6768d.clear();
                com.tencent.open.p059a.C3082f.m7631b("openSDK_LOG.ReportManager", "doReportVia, NetworkUnavailableException.");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:33:0x0092, code lost:
                return;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:37:0x009b, code lost:
                r9 = android.os.SystemClock.elapsedRealtime();
                r0 = r16;
                r6 = -8;
             */
            /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a5, code lost:
                r9 = android.os.SystemClock.elapsedRealtime();
                r0 = r16;
                r6 = -7;
             */
            /* JADX WARNING: Failed to process nested try/catch */
            /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0086 */
            /* JADX WARNING: Removed duplicated region for block: B:23:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:8:0x0038] */
            /* JADX WARNING: Removed duplicated region for block: B:24:0x006b A[ExcHandler: IOException (r0v25 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:8:0x0038] */
            /* JADX WARNING: Removed duplicated region for block: B:28:0x0074 A[ExcHandler: HttpStatusException (r0v22 'e' com.tencent.open.utils.HttpUtils$HttpStatusException A[CUSTOM_DECLARE]), Splitter:B:8:0x0038] */
            /* JADX WARNING: Removed duplicated region for block: B:31:0x0086 A[SYNTHETIC, Splitter:B:31:0x0086] */
            /* JADX WARNING: Removed duplicated region for block: B:38:? A[ExcHandler: SocketTimeoutException (unused java.net.SocketTimeoutException), SYNTHETIC, Splitter:B:8:0x0038] */
            /* JADX WARNING: Removed duplicated region for block: B:40:? A[ExcHandler: ConnectTimeoutException (unused org.apache.http.conn.ConnectTimeoutException), SYNTHETIC, Splitter:B:8:0x0038] */
            /* JADX WARNING: Removed duplicated region for block: B:46:0x00ca A[SYNTHETIC, Splitter:B:46:0x00ca] */
            /* JADX WARNING: Removed duplicated region for block: B:48:0x00d2 A[Catch:{ Exception -> 0x00f9 }] */
            /* JADX WARNING: Removed duplicated region for block: B:52:0x00b1 A[SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r28 = this;
                    r1 = r28
                    java.lang.String r2 = "openSDK_LOG.ReportManager"
                    com.tencent.open.b.g r0 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00f9 }
                    android.os.Bundle r3 = r0.mo35139d()     // Catch:{ Exception -> 0x00f9 }
                    if (r3 != 0) goto L_0x000d
                    return
                L_0x000d:
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f9 }
                    r0.<init>()     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r4 = "-->doReportVia, params: "
                    r0.append(r4)     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r4 = r3.toString()     // Catch:{ Exception -> 0x00f9 }
                    r0.append(r4)     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00f9 }
                    com.tencent.open.p059a.C3082f.m7628a(r2, r0)     // Catch:{ Exception -> 0x00f9 }
                    int r4 = com.tencent.open.p060b.C3092e.m7670a()     // Catch:{ Exception -> 0x00f9 }
                    long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x00f9 }
                    r0 = 0
                    r9 = r5
                    r5 = 0
                    r6 = 0
                    r11 = 0
                    r13 = 0
                L_0x0035:
                    r15 = 1
                    int r16 = r0 + 1
                    android.content.Context r0 = com.tencent.open.utils.C3121e.m7727a()     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    java.lang.String r7 = "https://appsupport.qq.com/cgi-bin/appstage/mstats_batch_report"
                    java.lang.String r8 = "POST"
                    com.tencent.open.utils.k$a r0 = com.tencent.open.utils.HttpUtils.openUrl2(r0, r7, r8, r3)     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    java.lang.String r7 = r0.f6870a     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    org.json.JSONObject r7 = com.tencent.open.utils.C3131k.m7788d(r7)     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    java.lang.String r8 = "ret"
                    int r7 = r7.getInt(r8)     // Catch:{ JSONException -> 0x0051, ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    goto L_0x0052
                L_0x0051:
                    r7 = -4
                L_0x0052:
                    if (r7 == 0) goto L_0x005c
                    java.lang.String r7 = r0.f6870a     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    boolean r7 = android.text.TextUtils.isEmpty(r7)     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    if (r7 != 0) goto L_0x005f
                L_0x005c:
                    r16 = r4
                    r5 = 1
                L_0x005f:
                    long r11 = r0.f6871b     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    long r7 = r0.f6872c     // Catch:{ ConnectTimeoutException -> 0x00a5, SocketTimeoutException -> 0x009b, JSONException -> 0x0093, NetworkUnavailableException -> 0x0086, HttpStatusException -> 0x0074, IOException -> 0x006b, Exception -> 0x0067 }
                    r13 = r7
                    r0 = r16
                    goto L_0x00af
                L_0x0067:
                    r0 = -6
                    r0 = r4
                    r6 = -6
                    goto L_0x0096
                L_0x006b:
                    r0 = move-exception
                    int r0 = com.tencent.open.utils.HttpUtils.getErrorCodeFromException(r0)     // Catch:{ Exception -> 0x00f9 }
                    r6 = r0
                    r0 = r16
                    goto L_0x0096
                L_0x0074:
                    r0 = move-exception
                    java.lang.String r0 = r0.getMessage()     // Catch:{ Exception -> 0x00b1 }
                    java.lang.String r3 = "http status code error:"
                    java.lang.String r4 = ""
                    java.lang.String r0 = r0.replace(r3, r4)     // Catch:{ Exception -> 0x00b1 }
                    int r6 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00b1 }
                    goto L_0x00b1
                L_0x0086:
                    com.tencent.open.b.g r0 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00f9 }
                    java.util.List<java.io.Serializable> r0 = r0.f6768d     // Catch:{ Exception -> 0x00f9 }
                    r0.clear()     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r0 = "doReportVia, NetworkUnavailableException."
                    com.tencent.open.p059a.C3082f.m7631b(r2, r0)     // Catch:{ Exception -> 0x00f9 }
                    return
                L_0x0093:
                    r0 = r16
                    r6 = -4
                L_0x0096:
                    r11 = 0
                    r13 = 0
                    goto L_0x00af
                L_0x009b:
                    long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x00f9 }
                    r0 = -8
                    r9 = r6
                    r0 = r16
                    r6 = -8
                    goto L_0x0096
                L_0x00a5:
                    long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x00f9 }
                    r0 = -7
                    r9 = r6
                    r0 = r16
                    r6 = -7
                    goto L_0x0096
                L_0x00af:
                    if (r0 < r4) goto L_0x0035
                L_0x00b1:
                    r25 = r6
                    r19 = r9
                    r21 = r11
                    r23 = r13
                    com.tencent.open.b.g r0 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r18 = "mapp_apptrace_sdk"
                    r26 = 0
                    r27 = 0
                    r17 = r0
                    r17.mo35133a(r18, r19, r21, r23, r25, r26, r27)     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r0 = "report_via"
                    if (r5 == 0) goto L_0x00d2
                    com.tencent.open.b.f r3 = com.tencent.open.p060b.C3093f.m7672a()     // Catch:{ Exception -> 0x00f9 }
                    r3.mo35127b(r0)     // Catch:{ Exception -> 0x00f9 }
                    goto L_0x00dd
                L_0x00d2:
                    com.tencent.open.b.f r3 = com.tencent.open.p060b.C3093f.m7672a()     // Catch:{ Exception -> 0x00f9 }
                    com.tencent.open.b.g r4 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00f9 }
                    java.util.List<java.io.Serializable> r4 = r4.f6768d     // Catch:{ Exception -> 0x00f9 }
                    r3.mo35126a(r0, r4)     // Catch:{ Exception -> 0x00f9 }
                L_0x00dd:
                    com.tencent.open.b.g r0 = com.tencent.open.p060b.C3094g.this     // Catch:{ Exception -> 0x00f9 }
                    java.util.List<java.io.Serializable> r0 = r0.f6768d     // Catch:{ Exception -> 0x00f9 }
                    r0.clear()     // Catch:{ Exception -> 0x00f9 }
                    java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f9 }
                    r0.<init>()     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r3 = "-->doReportVia, uploadSuccess: "
                    r0.append(r3)     // Catch:{ Exception -> 0x00f9 }
                    r0.append(r5)     // Catch:{ Exception -> 0x00f9 }
                    java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x00f9 }
                    com.tencent.open.p059a.C3082f.m7631b(r2, r0)     // Catch:{ Exception -> 0x00f9 }
                    goto L_0x00ff
                L_0x00f9:
                    r0 = move-exception
                    java.lang.String r3 = "-->doReportVia, exception in serial executor."
                    com.tencent.open.p059a.C3082f.m7632b(r2, r3, r0)
                L_0x00ff:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3094g.C30995.run():void");
            }
        });
    }

    /* renamed from: a */
    public void mo35134a(String str, String str2, Bundle bundle, boolean z) {
        final Bundle bundle2 = bundle;
        final String str3 = str;
        final boolean z2 = z;
        final String str4 = str2;
        C3126i.m7760a(new Runnable() {
            /* class com.tencent.open.p060b.C3094g.C31006 */

            /* JADX WARNING: Removed duplicated region for block: B:45:0x00e2 A[Catch:{ Exception -> 0x00f4 }] */
            /* JADX WARNING: Removed duplicated region for block: B:46:0x00e8 A[Catch:{ Exception -> 0x00f4 }] */
            /* JADX WARNING: Removed duplicated region for block: B:53:0x00e0 A[EDGE_INSN: B:53:0x00e0->B:44:0x00e0 ?: BREAK  , SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r10 = this;
                    java.lang.String r0 = "openSDK_LOG.ReportManager"
                    android.os.Bundle r1 = r2     // Catch:{ Exception -> 0x00f4 }
                    if (r1 != 0) goto L_0x000c
                    java.lang.String r1 = "-->httpRequest, params is null!"
                    com.tencent.open.p059a.C3082f.m7636e(r0, r1)     // Catch:{ Exception -> 0x00f4 }
                    return
                L_0x000c:
                    int r1 = com.tencent.open.p060b.C3092e.m7670a()     // Catch:{ Exception -> 0x00f4 }
                    if (r1 != 0) goto L_0x0013
                    r1 = 3
                L_0x0013:
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00f4 }
                    r2.<init>()     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r3 = "-->httpRequest, retryCount: "
                    r2.append(r3)     // Catch:{ Exception -> 0x00f4 }
                    r2.append(r1)     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x00f4 }
                    com.tencent.open.p059a.C3082f.m7631b(r0, r2)     // Catch:{ Exception -> 0x00f4 }
                    android.content.Context r2 = com.tencent.open.utils.C3121e.m7727a()     // Catch:{ Exception -> 0x00f4 }
                    r3 = 0
                    java.lang.String r4 = r3     // Catch:{ Exception -> 0x00f4 }
                    org.apache.http.client.HttpClient r2 = com.tencent.open.utils.HttpUtils.getHttpClient(r2, r3, r4)     // Catch:{ Exception -> 0x00f4 }
                    android.os.Bundle r3 = r2     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r3 = com.tencent.open.utils.HttpUtils.encodeUrl(r3)     // Catch:{ Exception -> 0x00f4 }
                    boolean r4 = r4     // Catch:{ Exception -> 0x00f4 }
                    if (r4 == 0) goto L_0x0040
                    java.lang.String r3 = java.net.URLEncoder.encode(r3)     // Catch:{ Exception -> 0x00f4 }
                L_0x0040:
                    java.lang.String r4 = r5     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r4 = r4.toUpperCase()     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r5 = "GET"
                    boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x00f4 }
                    if (r4 == 0) goto L_0x0062
                    java.lang.StringBuffer r4 = new java.lang.StringBuffer     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r5 = r3     // Catch:{ Exception -> 0x00f4 }
                    r4.<init>(r5)     // Catch:{ Exception -> 0x00f4 }
                    r4.append(r3)     // Catch:{ Exception -> 0x00f4 }
                    org.apache.http.client.methods.HttpGet r3 = new org.apache.http.client.methods.HttpGet     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00f4 }
                    r3.<init>(r4)     // Catch:{ Exception -> 0x00f4 }
                    goto L_0x0084
                L_0x0062:
                    java.lang.String r4 = r5     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r4 = r4.toUpperCase()     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r5 = "POST"
                    boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x00f4 }
                    if (r4 == 0) goto L_0x00ee
                    org.apache.http.client.methods.HttpPost r4 = new org.apache.http.client.methods.HttpPost     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r5 = r3     // Catch:{ Exception -> 0x00f4 }
                    r4.<init>(r5)     // Catch:{ Exception -> 0x00f4 }
                    byte[] r3 = com.tencent.open.utils.C3131k.m7799i(r3)     // Catch:{ Exception -> 0x00f4 }
                    org.apache.http.entity.ByteArrayEntity r5 = new org.apache.http.entity.ByteArrayEntity     // Catch:{ Exception -> 0x00f4 }
                    r5.<init>(r3)     // Catch:{ Exception -> 0x00f4 }
                    r4.setEntity(r5)     // Catch:{ Exception -> 0x00f4 }
                    r3 = r4
                L_0x0084:
                    java.lang.String r4 = "Accept-Encoding"
                    java.lang.String r5 = "gzip"
                    r3.addHeader(r4, r5)     // Catch:{ Exception -> 0x00f4 }
                    java.lang.String r4 = "Content-Type"
                    java.lang.String r5 = "application/x-www-form-urlencoded"
                    r3.addHeader(r4, r5)     // Catch:{ Exception -> 0x00f4 }
                    r4 = 0
                    r5 = 0
                L_0x0094:
                    r6 = 1
                    int r4 = r4 + r6
                    org.apache.http.HttpResponse r7 = r2.execute(r3)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    org.apache.http.StatusLine r7 = r7.getStatusLine()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    int r7 = r7.getStatusCode()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    r8.<init>()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    java.lang.String r9 = "-->httpRequest, statusCode: "
                    r8.append(r9)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    r8.append(r7)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    java.lang.String r8 = r8.toString()     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    com.tencent.open.p059a.C3082f.m7631b(r0, r8)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    r8 = 200(0xc8, float:2.8E-43)
                    if (r7 == r8) goto L_0x00c0
                    java.lang.String r7 = "-->ReportCenter httpRequest : HttpStatuscode != 200"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r7)     // Catch:{ ConnectTimeoutException -> 0x00d9, SocketTimeoutException -> 0x00d3, Exception -> 0x00cd }
                    goto L_0x00e0
                L_0x00c0:
                    java.lang.String r5 = "-->ReportCenter httpRequest Thread success"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r5)     // Catch:{ ConnectTimeoutException -> 0x00cb, SocketTimeoutException -> 0x00c9, Exception -> 0x00c7 }
                    r5 = 1
                    goto L_0x00e0
                L_0x00c7:
                    r5 = 1
                    goto L_0x00cd
                L_0x00c9:
                    r5 = 1
                    goto L_0x00d3
                L_0x00cb:
                    r5 = 1
                    goto L_0x00d9
                L_0x00cd:
                    java.lang.String r1 = "-->ReportCenter httpRequest Exception"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r1)     // Catch:{ Exception -> 0x00f4 }
                    goto L_0x00e0
                L_0x00d3:
                    java.lang.String r7 = "-->ReportCenter httpRequest SocketTimeoutException"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r7)     // Catch:{ Exception -> 0x00f4 }
                    goto L_0x00de
                L_0x00d9:
                    java.lang.String r7 = "-->ReportCenter httpRequest ConnectTimeoutException"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r7)     // Catch:{ Exception -> 0x00f4 }
                L_0x00de:
                    if (r4 < r1) goto L_0x0094
                L_0x00e0:
                    if (r5 != r6) goto L_0x00e8
                    java.lang.String r1 = "-->ReportCenter httpRequest Thread request success"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r1)     // Catch:{ Exception -> 0x00f4 }
                    goto L_0x00f9
                L_0x00e8:
                    java.lang.String r1 = "-->ReportCenter httpRequest Thread request failed"
                    com.tencent.open.p059a.C3082f.m7631b(r0, r1)     // Catch:{ Exception -> 0x00f4 }
                    goto L_0x00f9
                L_0x00ee:
                    java.lang.String r1 = "-->httpRequest unkonw request method return."
                    com.tencent.open.p059a.C3082f.m7636e(r0, r1)     // Catch:{ Exception -> 0x00f4 }
                    return
                L_0x00f4:
                    java.lang.String r1 = "-->httpRequest, exception in serial executor."
                    com.tencent.open.p059a.C3082f.m7631b(r0, r1)
                L_0x00f9:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3094g.C31006.run():void");
            }
        });
    }
}
