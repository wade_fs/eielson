package com.tencent.open.p060b;

import android.os.Bundle;
import java.io.Serializable;
import java.util.HashMap;

/* renamed from: com.tencent.open.b.b */
/* compiled from: ProGuard */
public class C3089b implements Serializable {

    /* renamed from: a */
    public final HashMap<String, String> f6756a = new HashMap<>();

    public C3089b(Bundle bundle) {
        if (bundle != null) {
            for (String str : bundle.keySet()) {
                this.f6756a.put(str, bundle.getString(str));
            }
        }
    }
}
