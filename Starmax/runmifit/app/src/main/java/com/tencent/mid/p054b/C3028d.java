package com.tencent.mid.p054b;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.tencent.mid.util.C3038d;

/* renamed from: com.tencent.mid.b.d */
public class C3028d extends C3030f {
    /* renamed from: a */
    public int mo34886a() {
        return 4;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo34889b() {
        return true;
    }

    public C3028d(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public String mo34890c() {
        return mo34893b(mo34901h());
    }

    /* renamed from: b */
    public String mo34893b(String str) {
        String string;
        synchronized (this) {
            C3038d dVar = f6553b;
            dVar.mo34950b("read mid from sharedPreferences， key=" + str);
            string = PreferenceManager.getDefaultSharedPreferences(this.f6554c).getString(str, null);
        }
        return string;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34888a(String str) {
        mo34892a(mo34901h(), str);
    }

    /* renamed from: a */
    public void mo34892a(String str, String str2) {
        synchronized (this) {
            f6553b.mo34950b("write mid to sharedPreferences");
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f6554c).edit();
            edit.putString(str, str2);
            edit.commit();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C3025a mo34891d() {
        C3025a aVar;
        synchronized (this) {
            aVar = new C3025a(PreferenceManager.getDefaultSharedPreferences(this.f6554c).getString(mo34900g(), null));
            C3038d dVar = f6553b;
            dVar.mo34950b("read CheckEntity from sharedPreferences:" + aVar.toString());
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34887a(C3025a aVar) {
        synchronized (this) {
            C3038d dVar = f6553b;
            dVar.mo34950b("write CheckEntity to sharedPreferences:" + aVar.toString());
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(this.f6554c).edit();
            edit.putString(mo34900g(), aVar.toString());
            edit.commit();
        }
    }
}
