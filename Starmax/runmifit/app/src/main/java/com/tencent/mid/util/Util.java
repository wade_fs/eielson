package com.tencent.mid.util;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ProviderInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.net.Uri;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.util.Base64;
import android.util.Log;
import com.baidu.mobstat.Config;
import com.tencent.mid.api.MidConstants;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.api.MidProvider;
import com.tencent.mid.api.MidService;
import com.tencent.mid.p054b.C3031g;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.zip.GZIPInputStream;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import org.apache.http.HttpHost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Util {

    /* renamed from: a */
    private static C3038d f6565a;

    /* renamed from: b */
    private static Random f6566b;
    public static int errorCount;

    public static synchronized C3038d getLogger() {
        C3038d dVar;
        synchronized (Util.class) {
            if (f6565a == null) {
                f6565a = new C3038d("MID");
            }
            dVar = f6565a;
        }
        return dVar;
    }

    public static boolean isStringValid(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }

    public static boolean isMidValid(MidEntity midEntity) {
        return midEntity != null && isMidValid(midEntity.getMid());
    }

    public static boolean isMidValid(String str) {
        return str != null && str.trim().length() >= 40;
    }

    public static MidEntity getNewerMidEntity(MidEntity midEntity, MidEntity midEntity2) {
        if (midEntity != null && midEntity2 != null) {
            return midEntity.compairTo(midEntity2) >= 0 ? midEntity : midEntity2;
        }
        if (midEntity != null) {
            return midEntity;
        }
        if (midEntity2 != null) {
            return midEntity2;
        }
        return null;
    }

    public static boolean equal(MidEntity midEntity, MidEntity midEntity2) {
        return (midEntity == null || midEntity2 == null) ? midEntity == null && midEntity2 == null : midEntity.compairTo(midEntity2) == 0;
    }

    public static boolean checkPermission(Context context, String str) {
        try {
            if (context.getPackageManager().checkPermission(str, context.getPackageName()) == 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            Log.e("MID", "checkPermission error", th);
            return false;
        }
    }

    public static boolean isWifiNet(Context context) {
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
        return (activeNetworkInfo == null || activeNetworkInfo.getTypeName() == null || !activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) ? false : true;
    }

    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            if (!checkPermission(context, "android.permission.INTERNET")) {
                return false;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
                if (activeNetworkInfo.isConnectedOrConnecting()) {
                    return true;
                }
                Log.w("MID", "Network error is not exist");
                return false;
            }
            errorCount++;
            int i = errorCount;
            if (i <= 5) {
                return true;
            }
            if (i >= 10 && i >= 10) {
                errorCount = 0;
            }
            return false;
        } catch (Throwable th) {
            Log.e("MID", "isNetworkAvailable error", th);
        }
    }

    public static String md5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes("UTF-8"));
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                stringBuffer.append((int) b);
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return str;
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return str;
        }
    }

    public static void jsonPut(JSONObject jSONObject, String str, String str2) {
        if (isStringValid(str2)) {
            jSONObject.put(str, str2);
        }
    }

    public static String decode(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(C3039e.m7503b(Base64.decode(str.getBytes("UTF-8"), 0)), "UTF-8").trim().replace("\t", "").replace("\n", "").replace("\r", "");
        } catch (Throwable th) {
            Log.e("MID", "decode error", th);
            return str;
        }
    }

    public static String encode(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(Base64.encode(C3039e.m7501a(str.getBytes("UTF-8")), 0), "UTF-8").trim().replace("\t", "").replace("\n", "").replace("\r", "");
        } catch (Throwable th) {
            Log.e("MID", "encode error", th);
            return str;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getImei(android.content.Context r4) {
        /*
            java.lang.String r0 = ""
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            boolean r1 = checkPermission(r4, r1)     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x0019
            java.lang.String r1 = "phone"
            java.lang.Object r4 = r4.getSystemService(r1)     // Catch:{ all -> 0x0021 }
            android.telephony.TelephonyManager r4 = (android.telephony.TelephonyManager) r4     // Catch:{ all -> 0x0021 }
            java.lang.String r4 = r4.getDeviceId()     // Catch:{ all -> 0x0021 }
            if (r4 == 0) goto L_0x003d
            return r4
        L_0x0019:
            com.tencent.mid.util.d r4 = com.tencent.mid.util.Util.f6565a     // Catch:{ all -> 0x0021 }
            java.lang.String r1 = "Could not get permission of android.permission.READ_PHONE_STATE"
            r4.mo34952d(r1)     // Catch:{ all -> 0x0021 }
            goto L_0x003c
        L_0x0021:
            r4 = move-exception
            com.tencent.mid.util.d r1 = com.tencent.mid.util.Util.f6565a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "get device id error:"
            r2.append(r3)
            java.lang.String r4 = r4.toString()
            r2.append(r4)
            java.lang.String r4 = r2.toString()
            r1.mo34952d(r4)
        L_0x003c:
            r4 = r0
        L_0x003d:
            if (r4 == 0) goto L_0x0040
            goto L_0x0041
        L_0x0040:
            r4 = r0
        L_0x0041:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.util.Util.getImei(android.content.Context):java.lang.String");
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x003f  */
    /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getImsi(android.content.Context r4) {
        /*
            java.lang.String r0 = ""
            java.lang.String r1 = "android.permission.READ_PHONE_STATE"
            boolean r1 = checkPermission(r4, r1)     // Catch:{ all -> 0x0021 }
            if (r1 == 0) goto L_0x0019
            java.lang.String r1 = "phone"
            java.lang.Object r4 = r4.getSystemService(r1)     // Catch:{ all -> 0x0021 }
            android.telephony.TelephonyManager r4 = (android.telephony.TelephonyManager) r4     // Catch:{ all -> 0x0021 }
            java.lang.String r1 = r4.getSubscriberId()     // Catch:{ all -> 0x0021 }
            if (r4 == 0) goto L_0x003d
            return r1
        L_0x0019:
            com.tencent.mid.util.d r4 = com.tencent.mid.util.Util.f6565a     // Catch:{ all -> 0x0021 }
            java.lang.String r1 = "Could not get permission of android.permission.READ_PHONE_STATE"
            r4.mo34952d(r1)     // Catch:{ all -> 0x0021 }
            goto L_0x003c
        L_0x0021:
            r4 = move-exception
            com.tencent.mid.util.d r1 = com.tencent.mid.util.Util.f6565a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "get subscriber id error:"
            r2.append(r3)
            java.lang.String r4 = r4.toString()
            r2.append(r4)
            java.lang.String r4 = r2.toString()
            r1.mo34952d(r4)
        L_0x003c:
            r1 = r0
        L_0x003d:
            if (r1 == 0) goto L_0x0040
            r0 = r1
        L_0x0040:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.util.Util.getImsi(android.content.Context):java.lang.String");
    }

    public static String getWifiMacAddress(Context context) {
        String str;
        if (checkPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (wifiManager == null) {
                    return "";
                }
                str = wifiManager.getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                C3038d dVar = f6565a;
                dVar.mo34952d("get wifi address error" + e);
                return "";
            }
        } else {
            f6565a.mo34952d("Could not get permission of android.permission.ACCESS_WIFI_STATE");
            str = "";
        }
        if (str != null) {
            return str;
        }
        return "";
    }

    public static HttpHost getHttpProxy() {
        if (Proxy.getDefaultHost() != null) {
            return new HttpHost(Proxy.getDefaultHost(), Proxy.getDefaultPort());
        }
        return null;
    }

    public static HttpHost getHttpProxy(Context context) {
        NetworkInfo activeNetworkInfo;
        String extraInfo;
        if (context == null) {
            return null;
        }
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0 || (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
                return null;
            }
            if ((activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) || (extraInfo = activeNetworkInfo.getExtraInfo()) == null) {
                return null;
            }
            if (!extraInfo.equals("cmwap") && !extraInfo.equals("3gwap")) {
                if (!extraInfo.equals("uniwap")) {
                    if (extraInfo.equals("ctwap")) {
                        return new HttpHost("10.0.0.200", 80);
                    }
                    return null;
                }
            }
            return new HttpHost("10.0.0.172", 80);
        } catch (Throwable th) {
            f6565a.mo34954f(th);
        }
    }

    public static byte[] deocdeGZipContent(byte[] bArr) {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read != -1) {
                byteArrayOutputStream.write(bArr2, 0, read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayInputStream.close();
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            }
        }
    }

    public static String getHttpAddr(Context context) {
        return "http://" + C3034b.m7478a(context).mo34939c();
    }

    public static byte[] getHMAC(String str, String str2) {
        try {
            SecretKeySpec secretKeySpec = new SecretKeySpec(str.getBytes(), "hmacmd5");
            Mac instance = Mac.getInstance("HmacSHA1");
            instance.init(secretKeySpec);
            instance.update(str2.getBytes());
            return instance.doFinal();
        } catch (Exception e) {
            f6565a.mo34949b(e);
            return null;
        }
    }

    /* renamed from: a */
    private static synchronized Random m7465a() {
        Random random;
        synchronized (Util.class) {
            if (f6566b == null) {
                f6566b = new Random();
            }
            random = f6566b;
        }
        return random;
    }

    public static int getRandInt() {
        return m7465a().nextInt(Integer.MAX_VALUE);
    }

    public static String bytesToStr(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        int length = bArr.length;
        for (int i = 0; i < length; i++) {
            stringBuffer.append(((int) bArr[i]) + "");
        }
        return stringBuffer.toString();
    }

    public static byte[] strToBytes(String str) {
        byte[] bArr = new byte[str.length()];
        for (int i = 0; i < str.length(); i++) {
            bArr[i] = (byte) str.charAt(i);
        }
        return bArr;
    }

    public static WifiInfo getWifiInfo(Context context) {
        WifiManager wifiManager;
        if (!checkPermission(context, "android.permission.ACCESS_WIFI_STATE") || (wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi")) == null) {
            return null;
        }
        return wifiManager.getConnectionInfo();
    }

    public static String getWiFiBBSID(Context context) {
        try {
            WifiInfo wifiInfo = getWifiInfo(context);
            if (wifiInfo != null) {
                return wifiInfo.getBSSID();
            }
            return null;
        } catch (Throwable th) {
            f6565a.mo34952d(th.toString());
            return null;
        }
    }

    public static String getWiFiSSID(Context context) {
        try {
            WifiInfo wifiInfo = getWifiInfo(context);
            if (wifiInfo != null) {
                return wifiInfo.getSSID();
            }
            return null;
        } catch (Throwable th) {
            f6565a.mo34952d(th.toString());
            return null;
        }
    }

    public static JSONArray getWifiTopN(Context context, int i) {
        List<ScanResult> scanResults;
        try {
            if (!MidService.isEnableReportWifiList()) {
                return null;
            }
            if (!checkPermission(context, "android.permission.INTERNET") || !checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                f6565a.mo34952d("can not get the permisson of android.permission.INTERNET");
                return null;
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (!(wifiManager == null || (scanResults = wifiManager.getScanResults()) == null || scanResults.size() <= 0)) {
                Collections.sort(scanResults, new Comparator<ScanResult>() {
                    /* class com.tencent.mid.util.Util.C30321 */

                    /* renamed from: a */
                    public int compare(ScanResult scanResult, ScanResult scanResult2) {
                        int abs = Math.abs(scanResult.level);
                        int abs2 = Math.abs(scanResult2.level);
                        if (abs > abs2) {
                            return 1;
                        }
                        return abs == abs2 ? 0 : -1;
                    }
                });
                JSONArray jSONArray = new JSONArray();
                int i2 = 0;
                while (true) {
                    if (i2 >= scanResults.size()) {
                        break;
                    } else if (i2 >= i) {
                        break;
                    } else {
                        ScanResult scanResult = scanResults.get(i2);
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("bs", scanResult.BSSID);
                        jSONObject.put("ss", scanResult.SSID);
                        jSONArray.put(jSONObject);
                        i2++;
                    }
                }
                return jSONArray;
            }
            return null;
        } catch (Throwable th) {
            f6565a.mo34952d(th.toString());
        }
    }

    public static JSONArray getSensors(Context context) {
        List<Sensor> sensorList;
        try {
            SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
            if (sensorManager == null || (sensorList = sensorManager.getSensorList(-1)) == null || sensorList.size() <= 0) {
                return null;
            }
            JSONArray jSONArray = new JSONArray();
            for (int i = 0; i < sensorList.size(); i++) {
                Sensor sensor = sensorList.get(i);
                JSONObject jSONObject = new JSONObject();
                jSONObject.put(Config.FEED_LIST_NAME, sensor.getName());
                jSONObject.put("vendor", sensor.getVendor());
                jSONArray.put(jSONObject);
            }
            return jSONArray;
        } catch (Throwable th) {
            f6565a.mo34952d(th.toString());
            return null;
        }
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }

    public static JSONArray queryMids(Context context, int i) {
        MidEntity midEntity;
        MidEntity midEntity2;
        C3038d dVar = f6565a;
        dVar.mo34956h("queryMids, midType=" + i);
        JSONArray jSONArray = new JSONArray();
        Map<String, MidEntity> midsByApps = getMidsByApps(context, i == 2 ? 3 : 2);
        if (midsByApps != null && midsByApps.size() > 0) {
            for (Map.Entry entry : midsByApps.entrySet()) {
                String str = (String) entry.getKey();
                MidEntity midEntity3 = (MidEntity) entry.getValue();
                if (midEntity3 != null && midEntity3.isMidValid()) {
                    try {
                        JSONObject a = m7466a(midEntity3);
                        a.put("loc", "priv");
                        if (str.equals(context.getPackageName())) {
                            a.put("app", 1);
                        }
                        a.put("pkg", str);
                        jSONArray.put(a);
                    } catch (Exception unused) {
                    }
                }
            }
        }
        if (i == 2) {
            midEntity = C3031g.m7437a(context).mo34917d();
        } else {
            midEntity = C3031g.m7437a(context).mo34926i();
        }
        C3038d dVar2 = f6565a;
        dVar2.mo34950b("settingEntity:" + midEntity);
        if (midEntity != null && midEntity.isMidValid()) {
            try {
                JSONObject a2 = m7466a(midEntity);
                a2.put("loc", "pub");
                a2.put("lc", "set");
                jSONArray.put(a2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (i == 2) {
            midEntity2 = C3031g.m7437a(context).mo34919e();
        } else {
            midEntity2 = C3031g.m7437a(context).mo34927j();
        }
        C3038d dVar3 = f6565a;
        dVar3.mo34950b("sdCardEntity:" + midEntity2);
        if (midEntity2 != null && midEntity2.isMidValid()) {
            try {
                JSONObject a3 = m7466a(midEntity2);
                a3.put("loc", "pub");
                a3.put("lc", Config.FEED_LIST_MAPPING);
                jSONArray.put(a3);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
        return jSONArray;
    }

    /* renamed from: a */
    private static JSONObject m7466a(MidEntity midEntity) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("mid", midEntity.getMid());
        jSONObject.put("ts", midEntity.getTimestamps() / 1000);
        return jSONObject;
    }

    public static String getPackageAuth(String str) {
        return "content://" + getPackageAuthName(str);
    }

    public static String getPackageAuthName(String str) {
        return str + MidConstants.PROVIDER_AUTH_SUFFIX;
    }

    public static Map<String, MidEntity> getMidsByApps(Context context, int i) {
        MidEntity parse;
        HashMap hashMap = new HashMap(4);
        Map<String, ProviderInfo> queryMatchContentProviders = queryMatchContentProviders(context);
        StringBuilder sb = new StringBuilder();
        sb.append(">>>   queryMatchContentProviders size:");
        sb.append(queryMatchContentProviders != null ? queryMatchContentProviders.size() : 0);
        Log.i("MID", sb.toString());
        MidEntity midEntity = null;
        if (i == 2) {
            midEntity = C3031g.m7437a(context).mo34925h();
        } else if (i == 3) {
            midEntity = C3031g.m7437a(context).mo34915c();
        }
        if (isMidValid(midEntity)) {
            hashMap.put(context.getPackageName(), midEntity);
        }
        if (!(queryMatchContentProviders == null || queryMatchContentProviders.size() == 0)) {
            for (String str : queryMatchContentProviders.keySet()) {
                try {
                    String str2 = getPackageAuth(str) + "/" + i;
                    String type = context.getContentResolver().getType(Uri.parse(str2));
                    Log.d("MID", ">>>   mid cmd:" + str2 + ", return:" + type);
                    if (!isEmpty(type) && (parse = MidEntity.parse(type)) != null && parse.isMidValid()) {
                        hashMap.put(str, parse);
                    }
                } catch (Throwable th) {
                    f6565a.mo34954f(th);
                }
            }
            Log.d("MID", ">>>   appPrivateMidMap size:" + hashMap.size() + ",content:");
            for (Map.Entry entry : hashMap.entrySet()) {
                Log.w("MID", ">>>   pkg:" + ((String) entry.getKey()) + ",midEntity:" + ((MidEntity) entry.getValue()).toString());
            }
        }
        return hashMap;
    }

    public static void insertMid2Provider(Context context, String str, String str2) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("mid", str2);
            ContentResolver contentResolver = context.getContentResolver();
            contentResolver.insert(Uri.parse(getPackageAuth(str) + "/" + 10), contentValues);
        } catch (Throwable unused) {
        }
    }

    public static void insertMid2OldProvider(Context context, String str, String str2) {
        try {
            ContentValues contentValues = new ContentValues();
            contentValues.put("mid", str2);
            ContentResolver contentResolver = context.getContentResolver();
            contentResolver.insert(Uri.parse(getPackageAuth(str) + "/" + 11), contentValues);
        } catch (Throwable unused) {
        }
    }

    public static Map<String, ProviderInfo> queryMatchContentProviders(Context context) {
        HashMap hashMap = new HashMap();
        for (ProviderInfo providerInfo : context.getPackageManager().queryContentProviders(null, 0, 0)) {
            if (providerInfo.name.equals(MidProvider.class.getName()) && providerInfo.authority.equals(getPackageAuthName(providerInfo.packageName))) {
                hashMap.put(providerInfo.packageName, providerInfo);
            }
        }
        return hashMap;
    }

    public static void updateIfLocalInvalid(Context context, String str) {
        if (isMidValid(str)) {
            MidEntity midEntity = new MidEntity();
            midEntity.setImei(getImei(context));
            midEntity.setMac(getWifiMacAddress(context));
            midEntity.setMid(str);
            midEntity.setTimestamps(System.currentTimeMillis());
            C3031g.m7437a(context).mo34922f(midEntity);
        }
    }

    public static String getDateString(int i) {
        try {
            Calendar instance = Calendar.getInstance();
            instance.roll(6, i);
            return new SimpleDateFormat("yyyyMMdd").format(instance.getTime());
        } catch (Throwable unused) {
            return "00";
        }
    }

    public static Map<String, ProviderInfo> getLocalXGAppList(Context context) {
        HashMap hashMap = new HashMap();
        for (ProviderInfo providerInfo : context.getPackageManager().queryContentProviders(null, 0, 0)) {
            if (providerInfo.name.equals("com.tencent.android.tpush.XGPushProvider") && providerInfo.authority.equals(getProviderAuth(providerInfo.packageName))) {
                hashMap.put(providerInfo.packageName, providerInfo);
                Log.d("MID.XG", providerInfo.authority + "," + providerInfo.packageName + "," + providerInfo.name);
            }
        }
        return hashMap;
    }

    public static String getProviderAuth(String str) {
        return str + ".AUTH_XGPUSH";
    }

    public static String getToken(Context context, String str) {
        String type = context.getContentResolver().getType(Uri.parse("content://" + str + ".AUTH_XGPUSH" + "/" + "tokenByMid"));
        if (type != null) {
            try {
                type = new String(Base64.decode(type.getBytes(), 0), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        Log.i("MID.XG", "get token from pkg:" + str + ", token:" + type);
        if (type == null || type.trim().length() != 40) {
            return null;
        }
        return type;
    }

    public static Map<String, Integer> queryAllToken(Context context) {
        Map<String, ProviderInfo> localXGAppList = getLocalXGAppList(context);
        HashMap hashMap = new HashMap();
        if (!(localXGAppList == null || localXGAppList.size() == 0)) {
            for (String str : localXGAppList.keySet()) {
                String token = getToken(context, str);
                if (isMidValid(token)) {
                    Integer num = (Integer) hashMap.get(token);
                    if (num == null) {
                        hashMap.put(token, 1);
                    } else {
                        hashMap.put(token, Integer.valueOf(num.intValue() + 1));
                    }
                }
            }
        }
        return hashMap;
    }

    public static String selectMaxCountXgAppToken(Context context) {
        Map<String, Integer> queryAllToken = queryAllToken(context);
        String str = null;
        if (queryAllToken != null && queryAllToken.size() > 0) {
            int i = 0;
            for (Map.Entry entry : queryAllToken.entrySet()) {
                if (((Integer) entry.getValue()).intValue() > i) {
                    int intValue = ((Integer) entry.getValue()).intValue();
                    i = intValue;
                    str = (String) entry.getKey();
                }
            }
        }
        return str;
    }
}
