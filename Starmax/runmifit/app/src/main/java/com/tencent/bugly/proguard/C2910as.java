package com.tencent.bugly.proguard;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.tencent.bugly.proguard.as */
/* compiled from: BUGLY */
public class C2910as extends Thread {

    /* renamed from: b */
    private static boolean f6061b = false;

    /* renamed from: c */
    private static Context f6062c;

    /* renamed from: a */
    final List<C2909ar> f6063a = Collections.synchronizedList(new ArrayList());

    /* renamed from: d */
    private List<C2912at> f6064d = Collections.synchronizedList(new ArrayList());

    /* renamed from: com.tencent.bugly.proguard.as$a */
    /* compiled from: BUGLY */
    private static class C2911a {

        /* renamed from: a */
        public static final C2910as f6065a = new C2910as();
    }

    /* renamed from: a */
    public static C2910as m6950a(Context context) {
        f6062c = context;
        return C2911a.f6065a;
    }

    /* renamed from: a */
    public void mo34562a() {
        mo34563a(new Handler(Looper.getMainLooper()));
    }

    /* renamed from: b */
    public void mo34566b() {
        mo34567b(new Handler(Looper.getMainLooper()));
    }

    /* renamed from: a */
    public void mo34563a(Handler handler) {
        mo34564a(handler, 5000);
    }

    /* renamed from: a */
    public void mo34564a(Handler handler, long j) {
        if (handler == null) {
            C2903an.m6865e("addThread handler should not be null", new Object[0]);
            return;
        }
        String name = handler.getLooper().getThread().getName();
        for (int i = 0; i < this.f6063a.size(); i++) {
            if (this.f6063a.get(i).mo34559e().equals(handler.getLooper().getThread().getName())) {
                C2903an.m6865e("addThread fail ,this thread has been added in monitor queue", new Object[0]);
                return;
            }
        }
        this.f6063a.add(new C2909ar(handler, name, j));
    }

    /* renamed from: b */
    public void mo34567b(Handler handler) {
        if (handler == null) {
            C2903an.m6865e("removeThread handler should not be null", new Object[0]);
            return;
        }
        for (int i = 0; i < this.f6063a.size(); i++) {
            if (this.f6063a.get(i).mo34559e().equals(handler.getLooper().getThread().getName())) {
                C2903an.m6863c("remove handler::%s", this.f6063a.get(i));
                this.f6063a.remove(i);
            }
        }
    }

    /* renamed from: c */
    public boolean mo34569c() {
        if (!isAlive()) {
            return false;
        }
        interrupt();
        f6061b = true;
        return true;
    }

    /* renamed from: d */
    public boolean mo34570d() {
        if (isAlive()) {
            return false;
        }
        f6061b = false;
        start();
        return true;
    }

    /* renamed from: e */
    private int m6952e() {
        int i = 0;
        for (int i2 = 0; i2 < this.f6063a.size(); i2++) {
            i = Math.max(i, this.f6063a.get(i2).mo34557c());
        }
        return i;
    }

    /* renamed from: a */
    public void mo34565a(C2912at atVar) {
        if (this.f6064d.contains(atVar)) {
            C2903an.m6865e("addThreadMonitorListeners fail ,this threadMonitorListener has been added in monitor queue", new Object[0]);
        } else {
            this.f6064d.add(atVar);
        }
    }

    /* renamed from: b */
    public void mo34568b(C2912at atVar) {
        this.f6064d.remove(atVar);
    }

    public void run() {
        setName("Bugly-ThreadMonitor");
        while (!f6061b) {
            for (int i = 0; i < this.f6063a.size(); i++) {
                this.f6063a.get(i).mo34554a();
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            for (long j = 2000; j > 0 && !isInterrupted(); j = 2000 - (SystemClock.uptimeMillis() - uptimeMillis)) {
                try {
                    sleep(j);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            int e2 = m6952e();
            if (!(e2 == 0 || e2 == 1)) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < this.f6063a.size(); i2++) {
                    C2909ar arVar = this.f6063a.get(i2);
                    if (arVar.mo34556b()) {
                        arrayList.add(arVar);
                        arVar.mo34555a(Long.MAX_VALUE);
                        C2903an.m6864d("to avoid upload block state repeated. as thread is blocked ,it may not be monitor until thread is unblock or this state has not been dealed with.", new Object[0]);
                    }
                }
                int i3 = 0;
                boolean z = false;
                while (i3 < arrayList.size()) {
                    C2909ar arVar2 = (C2909ar) arrayList.get(i3);
                    Thread d = arVar2.mo34558d();
                    boolean z2 = z;
                    for (int i4 = 0; i4 < this.f6064d.size(); i4++) {
                        if (this.f6064d.get(i4).mo34372a(super)) {
                            z2 = true;
                        }
                    }
                    if (!z2) {
                        m6951a(arVar2);
                    }
                    i3++;
                    z = z2;
                }
            }
        }
    }

    /* renamed from: a */
    private void m6951a(C2909ar arVar) {
        if (arVar.mo34559e().contains("main")) {
            arVar.mo34560f();
            C2903an.m6864d("although thread is blocked ,may not be anr error,so restore handler check wait time and restart check main thread", new Object[0]);
        }
    }
}
