package com.tencent.stat;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.baidu.mobstat.Config;
import com.google.android.gms.dynamite.ProviderConstants;
import com.runmifit.android.app.Constant;
import com.tencent.bugly.beta.tinker.TinkerReport;
import com.tencent.mid.api.MidService;
import com.tencent.open.SocialConstants;
import com.tencent.stat.app.api.AppInstallSourceMrg;
import com.tencent.stat.common.DeviceInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatConstants;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.StatPreferences;
import com.tencent.stat.common.Util;
import com.tencent.stat.common.X5Helper;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class StatConfig {

    /* renamed from: A */
    private static HashSet<String> f6906A = null;

    /* renamed from: B */
    private static HashSet<String> f6907B = new HashSet<>();

    /* renamed from: C */
    private static int f6908C = 100000;

    /* renamed from: D */
    private static int f6909D = 30;

    /* renamed from: E */
    private static int f6910E = 10;

    /* renamed from: F */
    private static int f6911F = 100;

    /* renamed from: G */
    private static int f6912G = 30;

    /* renamed from: H */
    private static int f6913H = 1;

    /* renamed from: I */
    private static String f6914I = null;

    /* renamed from: J */
    private static String f6915J;

    /* renamed from: K */
    private static String f6916K;

    /* renamed from: L */
    private static String f6917L = "mta_channel";

    /* renamed from: M */
    private static int f6918M = TinkerReport.KEY_APPLIED_VERSION_CHECK;

    /* renamed from: N */
    private static String f6919N;

    /* renamed from: O */
    private static int f6920O = 1024;

    /* renamed from: P */
    private static long f6921P = 0;

    /* renamed from: Q */
    private static long f6922Q = 300000;

    /* renamed from: R */
    private static volatile String f6923R = StatConstants.MTA_REPORT_FULL_URL;

    /* renamed from: S */
    private static int f6924S = 0;

    /* renamed from: T */
    private static volatile int f6925T = 0;

    /* renamed from: U */
    private static int f6926U = 20;

    /* renamed from: V */
    private static int f6927V = 0;

    /* renamed from: W */
    private static boolean f6928W = false;

    /* renamed from: X */
    private static int f6929X = 4096;

    /* renamed from: Y */
    private static boolean f6930Y = true;

    /* renamed from: Z */
    private static String f6931Z = null;

    /* renamed from: a */
    static C3142a f6932a = new C3142a(2);

    /* renamed from: aa */
    private static boolean f6933aa = false;

    /* renamed from: ab */
    private static StatCustomLogger f6934ab = null;

    /* renamed from: ac */
    private static StatExceptionCallBack f6935ac = null;

    /* renamed from: ad */
    private static String f6936ad = null;

    /* renamed from: ae */
    private static JSONObject f6937ae = null;

    /* renamed from: af */
    private static HashSet<String> f6938af = new HashSet<>();

    /* renamed from: ag */
    private static long f6939ag = 800;

    /* renamed from: ah */
    private static StatActionListener f6940ah = null;

    /* renamed from: ai */
    private static boolean f6941ai = true;

    /* renamed from: aj */
    private static Map<String, Object> f6942aj = new HashMap();

    /* renamed from: ak */
    private static volatile boolean f6943ak = true;

    /* renamed from: b */
    static C3142a f6944b = new C3142a(1);

    /* renamed from: c */
    static String f6945c = "__HIBERNATE__";

    /* renamed from: d */
    static String f6946d = "__HIBERNATE__TIME";

    /* renamed from: e */
    static String f6947e = "__MTA_KILL__";

    /* renamed from: f */
    static String f6948f = "";

    /* renamed from: g */
    static boolean f6949g = false;

    /* renamed from: h */
    static int f6950h = 500;

    /* renamed from: i */
    static long f6951i = 10000;
    public static boolean isAutoExceptionCaught = true;

    /* renamed from: j */
    static boolean f6952j = true;

    /* renamed from: k */
    static volatile String f6953k = StatConstants.MTA_SERVER;

    /* renamed from: l */
    static boolean f6954l = true;

    /* renamed from: m */
    static int f6955m = 0;

    /* renamed from: n */
    static long f6956n = 10000;

    /* renamed from: o */
    static int f6957o = 512;

    /* renamed from: p */
    static StatDataTransfer f6958p = null;

    /* renamed from: q */
    static boolean f6959q = false;

    /* renamed from: r */
    static boolean f6960r = true;

    /* renamed from: s */
    static boolean f6961s = true;

    /* renamed from: t */
    static long f6962t = 7;

    /* renamed from: u */
    static JSONObject f6963u = new JSONObject();
    /* access modifiers changed from: private */

    /* renamed from: v */
    public static StatLogger f6964v = StatCommonHelper.getLogger();

    /* renamed from: w */
    private static StatReportStrategy f6965w = StatReportStrategy.APP_LAUNCH;

    /* renamed from: x */
    private static boolean f6966x = false;

    /* renamed from: y */
    private static boolean f6967y = true;

    /* renamed from: z */
    private static int f6968z = 30000;

    public enum AccountType {
        f6970QQ,
        MAIL,
        WX,
        MOBILE
    }

    public enum CurrencyType {
        CNY,
        USD
    }

    /* renamed from: a */
    static boolean m7823a(int i, int i2, int i3) {
        return i >= i2 && i <= i3;
    }

    /* renamed from: com.tencent.stat.StatConfig$a */
    static class C3142a {

        /* renamed from: a */
        int f6974a;

        /* renamed from: b */
        JSONObject f6975b = new JSONObject();

        /* renamed from: c */
        String f6976c = "";

        /* renamed from: d */
        int f6977d = 0;

        /* renamed from: e */
        private boolean f6978e = false;

        /* renamed from: a */
        public void mo35255a(Context context) {
            StatPreferences.putString(context, mo35254a(), mo35256b().toString());
        }

        /* renamed from: b */
        public boolean mo35257b(Context context) {
            if (this.f6978e) {
                return true;
            }
            this.f6978e = true;
            String string = StatPreferences.getString(context, mo35254a(), "");
            if (TextUtils.isEmpty(string)) {
                return false;
            }
            m7839a(string);
            return true;
        }

        /* renamed from: a */
        public String mo35254a() {
            return "com.tencent.mta.cfg.store" + this.f6974a;
        }

        public C3142a(int i) {
            this.f6974a = i;
        }

        /* renamed from: b */
        public JSONObject mo35256b() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(SocialConstants.PARAM_TYPE, this.f6974a);
                jSONObject.put("props", this.f6975b);
                jSONObject.put("md5sum", this.f6976c);
                jSONObject.put(ProviderConstants.API_COLNAME_FEATURE_VERSION, this.f6977d);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return jSONObject;
        }

        /* renamed from: a */
        private void m7839a(String str) {
            try {
                if (StatConfig.isDebugEnable()) {
                    StatLogger g = StatConfig.f6964v;
                    g.mo35381d("load config begin:" + str);
                }
                JSONObject jSONObject = new JSONObject(str);
                this.f6974a = jSONObject.optInt(SocialConstants.PARAM_TYPE);
                this.f6975b = new JSONObject(jSONObject.optString("props", ""));
                this.f6976c = jSONObject.optString("md5sum", "");
                this.f6977d = jSONObject.optInt(ProviderConstants.API_COLNAME_FEATURE_VERSION, 0);
                if (StatConfig.isDebugEnable()) {
                    StatLogger g2 = StatConfig.f6964v;
                    g2.mo35381d("load config end, type=" + this.f6974a + " ,props=" + this.f6975b + " ,version=" + this.f6977d);
                }
                StatConfig.m7815a();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public String mo35258c() {
            return this.f6975b.toString();
        }
    }

    public static StatReportStrategy getStatSendStrategy() {
        return f6965w;
    }

    public static void setStatSendStrategy(StatReportStrategy statReportStrategy) {
        f6965w = statReportStrategy;
        if (statReportStrategy != StatReportStrategy.PERIOD) {
            StatServiceImpl.f7006d = 0;
        }
        if (isDebugEnable()) {
            StatLogger statLogger = f6964v;
            statLogger.mo35381d("Change to statSendStrategy: " + statReportStrategy);
        }
    }

    public static boolean isDebugEnable() {
        return f6966x;
    }

    public static void setDebugEnable(boolean z) {
        f6966x = z;
        StatCommonHelper.getLogger().setDebugEnable(z);
    }

    public static boolean isEnableStatService() {
        return f6967y;
    }

    public static void setEnableStatService(boolean z) {
        f6967y = z;
        if (!z) {
            f6964v.warn("!!!!!!MTA StatService has been disabled!!!!!!");
        }
    }

    public static int getSessionTimoutMillis() {
        return f6968z;
    }

    public static void setSessionTimoutMillis(int i) {
        if (!m7823a(i, 1000, (int) Constant.DAY)) {
            f6964v.error("setSessionTimoutMillis can not exceed the range of [1000, 24 * 60 * 60 * 1000].");
        } else {
            f6968z = i;
        }
    }

    /* renamed from: a */
    static boolean m7824a(String str) {
        HashSet<String> hashSet = f6906A;
        if (hashSet == null || hashSet.size() == 0 || !StatCommonHelper.isStringValid(str)) {
            return false;
        }
        return f6906A.contains(str.toLowerCase());
    }

    /* renamed from: b */
    static void m7829b(String str) {
        if (StatCommonHelper.isStringValid(str)) {
            String[] split = str.toLowerCase().split(";");
            if (split.length > 0) {
                if (f6906A == null) {
                    f6906A = new HashSet<>(split.length);
                }
                f6906A.addAll(Arrays.asList(split));
            }
        }
    }

    public static boolean isEventIdInDontReportEventIdsSet(String str) {
        HashSet<String> hashSet = f6907B;
        if (hashSet == null || hashSet.size() == 0 || !StatCommonHelper.isStringValid(str)) {
            return false;
        }
        return f6907B.contains(str.toLowerCase());
    }

    public static void updateDontReportEventIdsSet(String str) {
        if (StatCommonHelper.isStringValid(str)) {
            String[] split = str.toLowerCase().split(";");
            if (split.length > 0) {
                if (f6907B == null) {
                    f6907B = new HashSet<>(split.length);
                }
                f6907B.addAll(Arrays.asList(split));
            }
        }
    }

    /* renamed from: a */
    static void m7815a() {
        C3142a aVar = f6944b;
        if (aVar != null) {
            m7829b(aVar.f6975b.optString("__INSTANT_EI_LIST__", null));
            updateDontReportEventIdsSet(f6944b.f6975b.optString("__DONT_REPORT_EI_LIST__", null));
        }
    }

    public static int getMaxImportantDataSendRetryCount() {
        return f6911F;
    }

    public static void setMaxImportantDataSendRetryCount(int i) {
        if (i > 100) {
            f6911F = i;
        }
    }

    public static int getMaxBatchReportCount() {
        return f6912G;
    }

    public static void setMaxBatchReportCount(int i) {
        if (!m7823a(i, 2, 1000)) {
            f6964v.error("setMaxBatchReportCount can not exceed the range of [2,1000].");
        } else {
            f6912G = i;
        }
    }

    public static void setMaxSendRetryCount(int i) {
        if (!m7823a(i, 1, 1000)) {
            f6964v.error("setMaxSendRetryCount can not exceed the range of [1,1000].");
        } else {
            f6910E = i;
        }
    }

    public static int getMaxSendRetryCount() {
        return f6910E;
    }

    public static int getNumEventsCommitPerSec() {
        return f6913H;
    }

    public static void setNumEventsCommitPerSec(int i) {
        if (i > 0) {
            f6913H = i;
        }
    }

    /* renamed from: b */
    static int m7826b() {
        return f6909D;
    }

    public static int getMaxStoreEventCount() {
        return f6908C;
    }

    public static void setMaxStoreEventCount(int i) {
        if (!m7823a(i, 0, 500000)) {
            f6964v.error("setMaxStoreEventCount can not exceed the range of [0, 500000].");
        } else {
            f6908C = i;
        }
    }

    public static String getCustomProperty(Context context, String str) {
        init(context);
        return getCustomProperty(str);
    }

    public static String getCustomProperty(Context context, String str, String str2) {
        init(context);
        return getCustomProperty(str, str2);
    }

    public static String getCustomProperty(String str) {
        try {
            m7838h();
            return f6932a.f6975b.getString(str);
        } catch (Throwable th) {
            f6964v.mo35396w(th);
            return null;
        }
    }

    public static void init(Context context) {
        f6932a.mo35257b(context);
        f6944b.mo35257b(context);
        m7815a();
    }

    /* renamed from: h */
    private static void m7838h() {
        Context context = StatServiceImpl.getContext(null);
        if (context != null) {
            init(context);
        }
    }

    public static String getCustomProperty(String str, String str2) {
        try {
            m7838h();
            String string = f6932a.f6975b.getString(str);
            if (string != null) {
                return string;
            }
            return str2;
        } catch (Throwable th) {
            f6964v.mo35396w(th);
        }
    }

    public static String getSDKProperty(String str) {
        try {
            m7838h();
            return f6944b.f6975b.getString(str);
        } catch (Throwable th) {
            f6964v.mo35396w(th);
            return null;
        }
    }

    public static String getSDKProperty(String str, String str2) {
        try {
            m7838h();
            String string = f6944b.f6975b.getString(str);
            if (string != null) {
                return string;
            }
            return str2;
        } catch (Throwable unused) {
            StatLogger statLogger = f6964v;
            statLogger.mo35381d("can't find SDK Properties key:" + str);
        }
    }

    /* renamed from: a */
    static void m7821a(Context context, JSONObject jSONObject) {
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase(Integer.toString(f6944b.f6974a))) {
                    m7819a(context, f6944b, jSONObject.getJSONObject(next));
                    m7815a();
                } else if (next.equalsIgnoreCase(Integer.toString(f6932a.f6974a))) {
                    m7819a(context, f6932a, jSONObject.getJSONObject(next));
                } else if (next.equalsIgnoreCase("rs")) {
                    StatReportStrategy statReportStrategy = StatReportStrategy.getStatReportStrategy(jSONObject.getInt(next));
                    if (statReportStrategy != null) {
                        f6965w = statReportStrategy;
                        if (isDebugEnable()) {
                            StatLogger statLogger = f6964v;
                            statLogger.mo35381d("Change to ReportStrategy:" + statReportStrategy.name());
                        }
                    }
                } else {
                    return;
                }
            }
        } catch (JSONException e) {
            f6964v.mo35384e((Throwable) e);
        }
    }

    /* renamed from: a */
    static void m7819a(Context context, C3142a aVar, JSONObject jSONObject) {
        boolean z = false;
        try {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next.equalsIgnoreCase("v")) {
                    int i = jSONObject.getInt(next);
                    if (aVar.f6977d != i) {
                        z = true;
                    }
                    aVar.f6977d = i;
                } else if (next.equalsIgnoreCase("c")) {
                    String string = jSONObject.getString("c");
                    if (string.length() > 0) {
                        aVar.f6975b = new JSONObject(string);
                    }
                } else if (next.equalsIgnoreCase(Config.MODEL)) {
                    aVar.f6976c = jSONObject.getString(Config.MODEL);
                }
            }
            if (z) {
                C3226e a = C3226e.m8037a(C3220d.m8017a());
                if (a != null) {
                    a.mo35425a(aVar);
                }
                if (aVar.f6974a == f6944b.f6974a) {
                    m7822a(aVar.f6975b);
                    m7830b(aVar.f6975b);
                }
            }
            m7818a(context, aVar);
        } catch (JSONException e) {
            f6964v.mo35384e((Throwable) e);
        } catch (Throwable th) {
            f6964v.mo35384e(th);
        }
    }

    /* renamed from: a */
    static void m7822a(JSONObject jSONObject) {
        try {
            int i = jSONObject.getInt("rs");
            StatLogger statLogger = f6964v;
            statLogger.mo35388i("updateReportStrategy:" + jSONObject + ",sendStrategy:" + i);
            StatReportStrategy statReportStrategy = StatReportStrategy.getStatReportStrategy(i);
            if (statReportStrategy != null) {
                setStatSendStrategy(statReportStrategy);
            }
        } catch (JSONException unused) {
            if (isDebugEnable()) {
                f6964v.mo35388i("rs not found.");
            }
        }
    }

    /* renamed from: a */
    static void m7818a(Context context, C3142a aVar) throws JSONException {
        if (aVar.f6974a == f6944b.f6974a) {
            f6944b = aVar;
            m7822a(f6944b.f6975b);
            m7815a();
        } else if (aVar.f6974a == f6932a.f6974a) {
            f6932a = aVar;
        }
    }

    /* renamed from: b */
    static void m7828b(Context context, JSONObject jSONObject) {
        int i;
        try {
            String optString = jSONObject.optString(f6947e);
            if (StatCommonHelper.isStringValid(optString)) {
                JSONObject jSONObject2 = new JSONObject(optString);
                if (jSONObject2.length() != 0) {
                    boolean z = false;
                    if (!jSONObject2.isNull("sm")) {
                        Object obj = jSONObject2.get("sm");
                        if (obj instanceof Integer) {
                            i = ((Integer) obj).intValue();
                        } else {
                            i = obj instanceof String ? Integer.valueOf((String) obj).intValue() : 0;
                        }
                        if (i > 0) {
                            if (isDebugEnable()) {
                                StatLogger statLogger = f6964v;
                                statLogger.mo35388i("match sleepTime:" + i + " minutes");
                            }
                            StatPreferences.putLong(context, f6946d, System.currentTimeMillis() + ((long) (i * 60 * 1000)));
                            setEnableStatService(false);
                            f6964v.warn("MTA is disable for current SDK version");
                        }
                    }
                    boolean z2 = true;
                    if (m7825a(jSONObject2, "sv", StatConstants.VERSION)) {
                        f6964v.mo35388i("match sdk version:3.4.2");
                        z = true;
                    }
                    if (m7825a(jSONObject2, "md", Build.MODEL)) {
                        StatLogger statLogger2 = f6964v;
                        statLogger2.mo35388i("match MODEL:" + Build.MODEL);
                        z = true;
                    }
                    if (m7825a(jSONObject2, "av", StatCommonHelper.getCurAppVersion(context))) {
                        StatLogger statLogger3 = f6964v;
                        statLogger3.mo35388i("match app version:" + StatCommonHelper.getCurAppVersion(context));
                        z = true;
                    }
                    if (m7825a(jSONObject2, "mf", Build.MANUFACTURER + "")) {
                        StatLogger statLogger4 = f6964v;
                        statLogger4.mo35388i("match MANUFACTURER:" + Build.MANUFACTURER + "");
                        z = true;
                    }
                    if (m7825a(jSONObject2, "osv", Build.VERSION.SDK_INT + "")) {
                        StatLogger statLogger5 = f6964v;
                        statLogger5.mo35388i("match android SDK version:" + Build.VERSION.SDK_INT);
                        z = true;
                    }
                    if (m7825a(jSONObject2, "ov", Build.VERSION.SDK_INT + "")) {
                        StatLogger statLogger6 = f6964v;
                        statLogger6.mo35388i("match android SDK version:" + Build.VERSION.SDK_INT);
                        z = true;
                    }
                    if (m7825a(jSONObject2, DeviceInfo.TAG_IMEI, C3226e.m8037a(context).mo35429b(context).getImei())) {
                        StatLogger statLogger7 = f6964v;
                        statLogger7.mo35388i("match imei:" + C3226e.m8037a(context).mo35429b(context).getImei());
                        z = true;
                    }
                    if (m7825a(jSONObject2, "mid", getLocalMidOnly(context))) {
                        StatLogger statLogger8 = f6964v;
                        statLogger8.mo35388i("match mid:" + getLocalMidOnly(context));
                    } else {
                        z2 = z;
                    }
                    if (z2) {
                        m7817a(StatCommonHelper.getSDKLongVersion(StatConstants.VERSION));
                    }
                }
            }
        } catch (Exception e) {
            f6964v.mo35384e((Throwable) e);
        }
    }

    /* renamed from: a */
    static boolean m7825a(JSONObject jSONObject, String str, String str2) {
        if (jSONObject.isNull(str)) {
            return false;
        }
        String optString = jSONObject.optString(str);
        return StatCommonHelper.isStringValid(str2) && StatCommonHelper.isStringValid(optString) && str2.equalsIgnoreCase(optString);
    }

    /* renamed from: b */
    static void m7830b(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.length() != 0) {
            try {
                m7828b(C3220d.m8017a(), jSONObject);
                String string = jSONObject.getString(f6945c);
                if (isDebugEnable()) {
                    StatLogger statLogger = f6964v;
                    statLogger.mo35381d("hibernateVer:" + string + ", current version:" + StatConstants.VERSION);
                }
                long sDKLongVersion = StatCommonHelper.getSDKLongVersion(string);
                if (StatCommonHelper.getSDKLongVersion(StatConstants.VERSION) <= sDKLongVersion) {
                    m7817a(sDKLongVersion);
                }
            } catch (JSONException unused) {
                f6964v.mo35381d("__HIBERNATE__ not found.");
            }
        }
    }

    /* renamed from: a */
    static void m7817a(long j) {
        StatPreferences.putLong(C3220d.m8017a(), f6945c, j);
        setEnableStatService(false);
        f6964v.warn("MTA is disable for current SDK version");
    }

    /* renamed from: a */
    static void m7820a(Context context, String str) {
        if (str != null) {
            StatPreferences.putString(context, "_mta_ky_tag_", Util.encode(str));
        }
    }

    /* renamed from: a */
    static String m7814a(Context context) {
        return Util.decode(StatPreferences.getString(context, "_mta_ky_tag_", null));
    }

    public static synchronized String getAppKey(Context context) {
        synchronized (StatConfig.class) {
            if (f6915J != null) {
                String str = f6915J;
                return str;
            }
            if (context != null) {
                if (f6915J == null) {
                    f6915J = StatCommonHelper.getAppKey(context);
                }
            }
            if (f6915J == null || f6915J.trim().length() == 0) {
                f6964v.mo35383e("AppKey can not be null or empty, please read Developer's Guide first!");
            }
            String str2 = f6915J;
            return str2;
        }
    }

    /* renamed from: c */
    private static boolean m7832c(String str) {
        if (str == null) {
            return false;
        }
        String str2 = f6915J;
        if (str2 == null) {
            f6915J = str;
            return true;
        } else if (str2.contains(str)) {
            return false;
        } else {
            f6915J += "|" + str;
            return true;
        }
    }

    public static void setAppKey(Context context, String str) {
        if (context == null) {
            f6964v.error("ctx in StatConfig.setAppKey() is null");
        } else if (str == null || str.length() > 256) {
            f6964v.error("appkey in StatConfig.setAppKey() is null or exceed 256 bytes");
        } else {
            if (f6915J == null) {
                f6915J = m7814a(context);
            }
            if (m7832c(str) || m7832c(StatCommonHelper.getAppKey(context))) {
                m7820a(context, f6915J);
            }
        }
    }

    @Deprecated
    public static void setAppKey(String str) {
        if (str == null) {
            f6964v.error("appkey in StatConfig.setAppKey() is null");
        } else if (str.length() > 256) {
            f6964v.error("The length of appkey cann't exceed 256 bytes.");
        } else {
            f6915J = str;
        }
    }

    public static synchronized String getInstallChannel(Context context) {
        synchronized (StatConfig.class) {
            if (f6916K != null) {
                String str = f6916K;
                return str;
            }
            String string = StatPreferences.getString(context, f6917L, "");
            if (TextUtils.isEmpty(string) || m7834d(string)) {
                string = StatCommonHelper.getInstallChannel(context);
            }
            f6916K = string;
            if (f6916K == null || f6916K.trim().length() == 0) {
                f6964v.mo35396w("installChannel can not be null or empty, please read Developer's Guide first!");
            }
            String str2 = f6916K;
            return str2;
        }
    }

    /* renamed from: d */
    private static boolean m7834d(String str) {
        return "Wechat_Sdk".equalsIgnoreCase(str) || "QQConnect".equalsIgnoreCase(str);
    }

    public static void setInstallChannel(String str) {
        if (str.length() > 128) {
            f6964v.error("the length of installChannel can not exceed the range of 128 bytes.");
        } else if (!m7834d(str)) {
            f6916K = str;
        }
    }

    public static void setInstallChannel(Context context, String str) {
        if (str.length() > 128) {
            f6964v.error("the length of installChannel can not exceed the range of 128 bytes.");
        } else if (!m7834d(str)) {
            f6916K = str;
            StatPreferences.putString(context, f6917L, str);
        }
    }

    public static String getQQ(Context context) {
        return StatPreferences.getString(context, "mta.acc.qq", f6948f);
    }

    public static void setQQ(Context context, String str) {
        StatPreferences.putString(context, "mta.acc.qq", str);
        f6948f = str;
    }

    public static void setSendPeriodMinutes(int i) {
        if (!m7823a(i, 1, 10080)) {
            f6964v.error("setSendPeriodMinutes can not exceed the range of [1, 7*24*60] minutes.");
        } else {
            f6918M = i;
        }
    }

    public static String getMTAPreferencesFileName() {
        return f6919N;
    }

    public static void setMTAPreferencesFileName(String str) {
        f6919N = str;
    }

    public static int getNumOfMethodsCalledLimit() {
        return f6950h;
    }

    public static void setNumOfMethodsCalledLimit(int i, long j) {
        f6950h = i;
        if (j >= 1000) {
            f6951i = j;
        }
    }

    public static long getMsPeriodForMethodsCalledLimitClear() {
        return f6951i;
    }

    public static int getSendPeriodMinutes() {
        return f6918M;
    }

    public static int getMaxParallelTimmingEvents() {
        return f6920O;
    }

    public static void setMaxParallelTimmingEvents(int i) {
        if (!m7823a(i, 1, 4096)) {
            f6964v.error("setMaxParallelTimmingEvents can not exceed the range of [1, 4096].");
        } else {
            f6920O = i;
        }
    }

    public static boolean isEnableSmartReporting() {
        return f6952j;
    }

    public static void setEnableSmartReporting(boolean z) {
        f6952j = z;
    }

    public static boolean isAutoExceptionCaught() {
        return isAutoExceptionCaught;
    }

    public static void setAutoExceptionCaught(boolean z) {
        if (m7831c()) {
            f6964v.debug("QQ Connect call setAutoExceptionCaught, passed.");
        } else {
            isAutoExceptionCaught = z;
        }
    }

    /* renamed from: c */
    static boolean m7831c() {
        try {
            for (StackTraceElement stackTraceElement : Thread.currentThread().getStackTrace()) {
                if (stackTraceElement.getClassName().startsWith("com.tencent.connect.")) {
                    return true;
                }
            }
        } catch (Throwable unused) {
        }
        return false;
    }

    public static String getStatReportUrl() {
        return f6923R;
    }

    public static void setStatReportUrl(String str) {
        if (str == null || str.length() == 0) {
            f6964v.error("statReportUrl cannot be null or empty.");
            return;
        }
        f6923R = str;
        try {
            f6953k = new URI(f6923R).getHost();
        } catch (Exception e) {
            f6964v.mo35396w(e);
        }
        if (isDebugEnable()) {
            StatLogger statLogger = f6964v;
            statLogger.mo35388i("url:" + f6923R + ", domain:" + f6953k);
        }
    }

    public static String getStatReportHost() {
        return f6953k;
    }

    public static int getMaxSessionStatReportCount() {
        return f6924S;
    }

    public static void setMaxSessionStatReportCount(int i) {
        if (i < 0) {
            f6964v.error("maxSessionStatReportCount cannot be less than 0.");
        } else {
            f6924S = i;
        }
    }

    public static int getCurSessionStatReportCount() {
        return f6925T;
    }

    /* renamed from: a */
    static synchronized void m7816a(int i) {
        synchronized (StatConfig.class) {
            f6925T = i;
        }
    }

    public static int getMaxDaySessionNumbers() {
        return f6926U;
    }

    public static void setMaxDaySessionNumbers(int i) {
        if (i <= 0) {
            f6964v.mo35383e("maxDaySessionNumbers must be greater than 0.");
        } else {
            f6926U = i;
        }
    }

    /* renamed from: d */
    static void m7833d() {
        f6927V++;
    }

    /* renamed from: b */
    static void m7827b(int i) {
        if (i >= 0) {
            f6927V = i;
        }
    }

    /* renamed from: e */
    static int m7835e() {
        return f6927V;
    }

    public static int getMaxReportEventLength() {
        return f6929X;
    }

    public static void setMaxReportEventLength(int i) {
        if (i <= 0) {
            f6964v.error("maxReportEventLength on setMaxReportEventLength() must greater than 0.");
        } else {
            f6929X = i;
        }
    }

    public static boolean isEnableConcurrentProcess() {
        return f6930Y;
    }

    public static void setEnableConcurrentProcess(boolean z) {
        f6930Y = z;
    }

    public static String getCustomUserId(Context context) {
        if (context == null) {
            f6964v.error("Context for getCustomUid is null.");
            return null;
        }
        if (f6931Z == null) {
            try {
                f6931Z = StatPreferences.getString(context, "MTA_CUSTOM_UID", "");
            } catch (ClassCastException e) {
                f6964v.mo35384e((Throwable) e);
            }
        }
        return f6931Z;
    }

    public static void setCustomUserId(Context context, String str) {
        if (context == null) {
            f6964v.error("Context for setCustomUid is null.");
            return;
        }
        StatPreferences.putString(context, "MTA_CUSTOM_UID", str);
        f6931Z = str;
    }

    public static void setNativeCrashDebugEnable(boolean z) {
        StatNativeCrashReport.setNativeCrashDebugEnable(z);
    }

    public static void initNativeCrashReport(Context context, String str) {
        if (isEnableStatService()) {
            if (context == null) {
                f6964v.error("The Context of StatConfig.initNativeCrashReport() can not be null!");
            } else {
                StatNativeCrashReport.initNativeCrash(context, str);
            }
        }
    }

    public static String getMid(Context context) {
        return MidService.getMid(context);
    }

    public static String getLocalMidOnly(Context context) {
        return MidService.getLocalMidOnly(context);
    }

    public static void setXGProMode(boolean z) {
        f6933aa = z;
    }

    public static boolean isXGProMode() {
        return f6933aa;
    }

    public static StatCustomLogger getCustomLogger() {
        return f6934ab;
    }

    public static void setCustomLogger(StatCustomLogger statCustomLogger) {
        f6934ab = statCustomLogger;
    }

    public static boolean isReportEventsByOrder() {
        return f6954l;
    }

    public static void setReportEventsByOrder(boolean z) {
        f6954l = z;
    }

    public static int getNumEventsCachedInMemory() {
        return f6955m;
    }

    public static void setNumEventsCachedInMemory(int i) {
        if (i >= 0) {
            f6955m = i;
        }
    }

    public static long getFlushDBSpaceMS() {
        return f6956n;
    }

    public static void setFlushDBSpaceMS(long j) {
        if (j > 0) {
            f6956n = j;
        }
    }

    public static int getReportCompressedSize() {
        return f6957o;
    }

    public static void setReportCompressedSize(int i) {
        if (i > 0) {
            f6957o = i;
        }
    }

    public static StatDataTransfer getDataTransfer() {
        return f6958p;
    }

    public static void setDataTransfer(StatDataTransfer statDataTransfer) {
        f6958p = statDataTransfer;
    }

    /* renamed from: f */
    protected static StatExceptionCallBack m7836f() {
        return f6935ac;
    }

    public static void setStatExCallBack(StatExceptionCallBack statExceptionCallBack) {
        f6935ac = statExceptionCallBack;
    }

    public static String getAppVersion() {
        return f6936ad;
    }

    public static void setAppVersion(String str) {
        f6936ad = str;
    }

    public static JSONObject getCustomGlobalReportContent() {
        return f6937ae;
    }

    public static void setCustomGlobalReportContent(JSONObject jSONObject) {
        f6937ae = jSONObject;
    }

    public static void addSkipSessionReportAppkey(String str) {
        if (StatCommonHelper.isStringValid(str)) {
            f6938af.add(str);
        }
    }

    public static boolean shouldSkipSessionReport(String str) {
        return f6938af.contains(str);
    }

    public static void removeSkipSessionReportAppkey(String str) {
        f6938af.remove(str);
    }

    public static long getBackgroundDelayTimestamp() {
        return f6939ag;
    }

    public static void setBackgroundDelayTimestamp(long j) {
        if (j <= 0 || j >= 10000) {
            Log.e(StatConstants.LOG_TAG, "setBackgroundDelayTimestamp falied, timestamp:" + j + " is invalid.");
            return;
        }
        f6939ag = j;
    }

    public static void enableCommitEventAtBackground(final Context context, boolean z) {
        if (!z) {
            StatActionListener statActionListener = f6940ah;
            if (statActionListener != null) {
                StatService.removeActionListener(statActionListener);
                f6940ah = null;
            }
        } else if (f6940ah == null) {
            f6940ah = new StatActionListener() {
                /* class com.tencent.stat.StatConfig.C31411 */

                public void onBecameForeground() {
                }

                public void onBecameBackground() {
                    StatService.commitEvents(context, -1);
                }
            };
            StatService.addActionListener(f6940ah);
        }
    }

    public static boolean isAntoActivityLifecycleStat() {
        return f6959q;
    }

    public static void setAntoActivityLifecycleStat(boolean z) {
        f6959q = z;
    }

    public static boolean isAutoTrackBackgroundEvent() {
        return f6960r;
    }

    public static void setAutoTrackBackgroundEvent(boolean z) {
        f6960r = z;
    }

    public static boolean isAutoTrackAppsEvent() {
        return f6961s;
    }

    public static void setAutoTrackAppsEvent(boolean z) {
        f6961s = z;
    }

    public static boolean isEnableReportWifiList() {
        return f6941ai;
    }

    public static void setEnableReportWifiList(boolean z) {
        f6941ai = z;
    }

    public static long getStoredRecordExpiredDays() {
        return f6962t;
    }

    public static void setStoredRecordExpiredDays(long j) {
        if (j > 0) {
            f6962t = j;
        }
    }

    public static void setCrashKeyValue(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            f6964v.mo35383e("setCrashKeyValue key or value not valid.");
            return;
        }
        try {
            f6963u.put(str, str2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void addCrashTrackLog(String str) {
        StatTrackLog.log(str);
    }

    public static void printLog(int i, String str, String str2) {
        if (f6966x) {
            switch (i) {
                case 2:
                    Log.v(str, str2);
                    return;
                case 3:
                    Log.d(str, str2);
                    return;
                case 4:
                    Log.i(str, str2);
                    return;
                case 5:
                    Log.w(str, str2);
                    return;
                case 6:
                case 7:
                    Log.e(str, str2);
                    return;
                default:
                    Log.i(str, str2);
                    return;
            }
        }
    }

    public static JSONObject getCrashKeyValue() {
        return f6963u;
    }

    public static Map<String, Object> getCustomReportMap() {
        return f6942aj;
    }

    public static void addCustomReportKeyValue(String str, Object obj) {
        f6942aj.put(str, obj);
    }

    public static void getCustomReportKeyValue(String str) {
        if (str != null) {
            f6942aj.get(str);
        }
    }

    public static void delCustomReportKeyValue(String str) {
        if (str != null) {
            f6942aj.remove(str);
        }
    }

    public static void setTLinkStatus(boolean z) {
        AppInstallSourceMrg.setEnable(z);
    }

    public static boolean getTLinkStatus() {
        return AppInstallSourceMrg.isEnable();
    }

    public static void invokeTBSSdkOnUiThread(Context context) {
        X5Helper.initOnUiThread(context);
    }

    public static boolean isEnableAutoMonitorActivityCycle() {
        return f6943ak;
    }

    public static void setEnableAutoMonitorActivityCycle(boolean z) {
        f6943ak = z;
    }
}
