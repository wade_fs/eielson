package com.tencent.open.p060b;

import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3122f;

/* renamed from: com.tencent.open.b.e */
/* compiled from: ProGuard */
public class C3092e {
    /* renamed from: a */
    public static int m7671a(String str) {
        int a;
        if (C3121e.m7727a() == null || (a = C3122f.m7733a(C3121e.m7727a(), str).mo35169a("Common_BusinessReportFrequency")) == 0) {
            return 100;
        }
        return a;
    }

    /* renamed from: a */
    public static int m7670a() {
        int a = C3122f.m7733a(C3121e.m7727a(), (String) null).mo35169a("Common_HttpRetryCount");
        if (a == 0) {
            return 2;
        }
        return a;
    }
}
