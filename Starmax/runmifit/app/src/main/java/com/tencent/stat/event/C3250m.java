package com.tencent.stat.event;

import android.content.Context;
import com.tencent.open.SocialConstants;
import com.tencent.stat.StatConfig;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.m */
public class C3250m extends Event {

    /* renamed from: a */
    private String f7391a;

    /* renamed from: s */
    private String f7392s;

    public boolean equals(Object obj) {
        return obj != null && this == obj;
    }

    public int hashCode() {
        return toString().hashCode();
    }

    public C3250m(Context context, int i, String str, StatConfig.AccountType accountType) {
        super(context, i, null);
        String str2 = "";
        this.f7391a = str2;
        this.f7392s = str2;
        int i2 = C32511.f7393a[accountType.ordinal()];
        if (i2 == 1) {
            str2 = "qq";
        } else if (i2 == 2) {
            str2 = "mobile";
        } else if (i2 == 3) {
            str2 = "mail";
        } else if (i2 == 4) {
            str2 = "wx";
        }
        this.f7391a = str2;
        this.f7392s = str;
    }

    /* renamed from: com.tencent.stat.event.m$1 */
    static /* synthetic */ class C32511 {

        /* renamed from: a */
        static final /* synthetic */ int[] f7393a = new int[StatConfig.AccountType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.tencent.stat.StatConfig$AccountType[] r0 = com.tencent.stat.StatConfig.AccountType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                com.tencent.stat.event.C3250m.C32511.f7393a = r0
                int[] r0 = com.tencent.stat.event.C3250m.C32511.f7393a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.tencent.stat.StatConfig$AccountType r1 = com.tencent.stat.StatConfig.AccountType.f6970QQ     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = com.tencent.stat.event.C3250m.C32511.f7393a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.tencent.stat.StatConfig$AccountType r1 = com.tencent.stat.StatConfig.AccountType.MOBILE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = com.tencent.stat.event.C3250m.C32511.f7393a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.tencent.stat.StatConfig$AccountType r1 = com.tencent.stat.StatConfig.AccountType.MAIL     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = com.tencent.stat.event.C3250m.C32511.f7393a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.tencent.stat.StatConfig$AccountType r1 = com.tencent.stat.StatConfig.AccountType.WX     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.event.C3250m.C32511.<clinit>():void");
        }
    }

    public EventType getType() {
        return EventType.REG_ACCOUNT;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        Util.jsonPut(jSONObject, "ua", StatCommonHelper.getUserAgent(this.f7355q));
        JSONObject jSONObject2 = new JSONObject();
        Util.jsonPut(jSONObject2, "acct", this.f7392s);
        Util.jsonPut(jSONObject2, SocialConstants.PARAM_TYPE, this.f7391a);
        jSONObject.put("reg", jSONObject2);
        return true;
    }
}
