package com.tencent.stat.common;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;

/* renamed from: com.tencent.stat.common.b */
public class C3207b {

    /* renamed from: a */
    private static StatLogger f7251a = StatCommonHelper.getLogger();

    /* renamed from: b */
    private static String m7980b(Context context, String str, String str2) {
        File dir = context.getDir(str, 0);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir.getAbsolutePath() + "/" + str2;
    }

    /* renamed from: a */
    public static void m7979a(Context context, String str, String str2, String str3) {
        FileOutputStream fileOutputStream;
        f7251a.mo35381d("enter writeFile fname:" + str2);
        try {
            String b = m7980b(context, str, str2);
            fileOutputStream = new FileOutputStream(b);
            try {
                fileOutputStream.write(str3.getBytes());
                f7251a.mo35381d("success writeFile fname:" + b + ",size:" + str3.length());
            } catch (Throwable th) {
                th = th;
            }
        } catch (Throwable th2) {
            th = th2;
            fileOutputStream = null;
            try {
                f7251a.mo35384e(th);
                StatCommonHelper.safeClose(fileOutputStream);
            } catch (Throwable th3) {
                StatCommonHelper.safeClose(fileOutputStream);
                throw th3;
            }
        }
        StatCommonHelper.safeClose(fileOutputStream);
    }

    /* renamed from: a */
    public static void m7978a(Context context, String str, String str2) {
        String b = m7980b(context, str, str2);
        new File(b).delete();
        StatLogger statLogger = f7251a;
        statLogger.mo35381d("success deleteFile fname:" + b);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (3) to help type inference */
    /* JADX WARN: Type inference failed for: r2v1, types: [java.io.Closeable] */
    /* JADX WARN: Type inference failed for: r2v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m7977a(android.content.Context r5, java.lang.String r6) {
        /*
            com.tencent.stat.common.StatLogger r5 = com.tencent.stat.common.C3207b.f7251a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "enter readFile fileFullPath:"
            r0.append(r1)
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            r5.mo35381d(r0)
            r5 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ all -> 0x006d }
            r0.<init>(r6)     // Catch:{ all -> 0x006d }
            int r1 = r0.available()     // Catch:{ all -> 0x006a }
            byte[] r1 = new byte[r1]     // Catch:{ all -> 0x006a }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x006a }
            r2.<init>()     // Catch:{ all -> 0x006a }
        L_0x0027:
            int r3 = r0.read(r1)     // Catch:{ all -> 0x0067 }
            r4 = -1
            if (r3 == r4) goto L_0x0032
            r2.write(r1)     // Catch:{ all -> 0x0067 }
            goto L_0x0027
        L_0x0032:
            byte[] r1 = r2.toByteArray()     // Catch:{ all -> 0x0067 }
            r2.close()     // Catch:{ all -> 0x0067 }
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x0067 }
            r3.<init>(r1)     // Catch:{ all -> 0x0067 }
            com.tencent.stat.common.StatLogger r5 = com.tencent.stat.common.C3207b.f7251a     // Catch:{ all -> 0x0065 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0065 }
            r1.<init>()     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = "success readFile fname:"
            r1.append(r4)     // Catch:{ all -> 0x0065 }
            r1.append(r6)     // Catch:{ all -> 0x0065 }
            java.lang.String r6 = ",content length:"
            r1.append(r6)     // Catch:{ all -> 0x0065 }
            int r6 = r3.length()     // Catch:{ all -> 0x0065 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0065 }
            r1.append(r6)     // Catch:{ all -> 0x0065 }
            java.lang.String r6 = r1.toString()     // Catch:{ all -> 0x0065 }
            r5.mo35381d(r6)     // Catch:{ all -> 0x0065 }
            goto L_0x0076
        L_0x0065:
            r6 = move-exception
            goto L_0x0071
        L_0x0067:
            r6 = move-exception
            r3 = r5
            goto L_0x0071
        L_0x006a:
            r6 = move-exception
            r2 = r5
            goto L_0x0070
        L_0x006d:
            r6 = move-exception
            r0 = r5
            r2 = r0
        L_0x0070:
            r3 = r2
        L_0x0071:
            com.tencent.stat.common.StatLogger r5 = com.tencent.stat.common.C3207b.f7251a     // Catch:{ all -> 0x007d }
            r5.mo35384e(r6)     // Catch:{ all -> 0x007d }
        L_0x0076:
            com.tencent.stat.common.StatCommonHelper.safeClose(r0)
            com.tencent.stat.common.StatCommonHelper.safeClose(r2)
            return r3
        L_0x007d:
            r5 = move-exception
            com.tencent.stat.common.StatCommonHelper.safeClose(r0)
            com.tencent.stat.common.StatCommonHelper.safeClose(r2)
            goto L_0x0086
        L_0x0085:
            throw r5
        L_0x0086:
            goto L_0x0085
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.common.C3207b.m7977a(android.content.Context, java.lang.String):java.lang.String");
    }
}
