package com.tencent.open;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import com.google.android.gms.auth.api.proxy.AuthApiStatusCodes;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class SocialOperation extends BaseApi {
    public static final String GAME_FRIEND_ADD_MESSAGE = "add_msg";
    public static final String GAME_FRIEND_LABEL = "friend_label";
    public static final String GAME_FRIEND_OPENID = "fopen_id";
    public static final String GAME_SIGNATURE = "signature";
    public static final String GAME_UNION_ID = "unionid";
    public static final String GAME_UNION_NAME = "union_name";
    public static final String GAME_ZONE_ID = "zoneid";

    public SocialOperation(QQToken qQToken) {
        super(qQToken);
    }

    public void makeFriend(Activity activity, Bundle bundle) {
        C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->makeFriend()  -- start");
        if (bundle == null) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->makeFriend params is null");
            C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_MAKE_FRIEND, Constants.VIA_REPORT_TYPE_MAKE_FRIEND, "18", "1");
            return;
        }
        String string = bundle.getString(GAME_FRIEND_OPENID);
        if (TextUtils.isEmpty(string)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->make friend, fOpenid is empty.");
            C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_MAKE_FRIEND, Constants.VIA_REPORT_TYPE_MAKE_FRIEND, "18", "1");
            return;
        }
        String string2 = bundle.getString(GAME_FRIEND_LABEL);
        String string3 = bundle.getString(GAME_FRIEND_ADD_MESSAGE);
        String a = C3131k.m7770a(activity);
        String openId = this.f6457c.getOpenId();
        String appId = this.f6457c.getAppId();
        C3082f.m7628a("openSDK_LOG.GameAppOperation", "-->make friend, fOpenid: " + string + " | label: " + string2 + " | message: " + string3 + " | openid: " + openId + " | appid:" + appId);
        StringBuffer stringBuffer = new StringBuffer("mqqapi://opensdk/add_friend?src_type=app&version=1");
        StringBuilder sb = new StringBuilder();
        sb.append("&fopen_id=");
        sb.append(Base64.encodeToString(C3131k.m7799i(string), 2));
        stringBuffer.append(sb.toString());
        if (!TextUtils.isEmpty(openId)) {
            stringBuffer.append("&open_id=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
        }
        if (!TextUtils.isEmpty(appId)) {
            stringBuffer.append("&app_id=" + appId);
        }
        if (!TextUtils.isEmpty(string2)) {
            stringBuffer.append("&friend_label=" + Base64.encodeToString(C3131k.m7799i(string2), 2));
        }
        if (!TextUtils.isEmpty(string3)) {
            stringBuffer.append("&add_msg=" + Base64.encodeToString(C3131k.m7799i(string3), 2));
        }
        if (!TextUtils.isEmpty(a)) {
            stringBuffer.append("&app_name=" + Base64.encodeToString(C3131k.m7799i(a), 2));
        }
        C3082f.m7628a("openSDK_LOG.GameAppOperation", "-->make friend, url: " + stringBuffer.toString());
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(stringBuffer.toString()));
        if (!mo34804a(intent) || C3131k.m7795f(activity, "5.1.0")) {
            C3082f.m7635d("openSDK_LOG.GameAppOperation", "-->make friend, there is no activity.");
            m7563a(activity);
            C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_MAKE_FRIEND, Constants.VIA_REPORT_TYPE_MAKE_FRIEND, "18", "1");
        } else {
            C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->makeFriend target activity found, qqver greater than 5.1.0");
            try {
                activity.startActivity(intent);
                C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_MAKE_FRIEND, Constants.VIA_REPORT_TYPE_MAKE_FRIEND, "18", "0");
            } catch (Exception e) {
                C3082f.m7632b("openSDK_LOG.GameAppOperation", "-->make friend, start activity exception.", e);
                m7563a(activity);
                C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_MAKE_FRIEND, Constants.VIA_REPORT_TYPE_MAKE_FRIEND, "18", "1");
            }
        }
        C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->makeFriend()  -- end");
    }

    public void unBindGroup(Context context, String str, final IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.GameAppOperation", "unBindQQGroup()");
        if (context == null) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->uinBindGroup, activity is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1001, "param acitivty is null", "activity param of api can not be null."));
            }
        } else if (TextUtils.isEmpty(str)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->unBindGroup, params is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1006, "param organizationId is null", "organizationId param of api can not be null."));
            }
        } else {
            String appId = this.f6457c.getAppId();
            if (TextUtils.isEmpty(appId)) {
                C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->unBindGroup, appid is empty.");
                if (iUiListener != null) {
                    iUiListener.onError(new UiError(1003, "param appId is null", "appid is null please login."));
                    return;
                }
                return;
            }
            C30661 r2 = new IUiListener() {
                /* class com.tencent.open.SocialOperation.C30661 */

                public void onCancel() {
                }

                public void onComplete(Object obj) {
                    C3082f.m7635d("openSDK_LOG.GameAppOperation", "-->unbind group resp is: " + obj);
                    if (obj == null) {
                        IUiListener iUiListener = iUiListener;
                        if (iUiListener != null) {
                            iUiListener.onError(new UiError(4001, "服务端错误，请稍后重试", "资格检查回包为null。"));
                            return;
                        }
                        return;
                    }
                    JSONObject jSONObject = (JSONObject) obj;
                    IUiListener iUiListener2 = iUiListener;
                    if (iUiListener2 != null) {
                        iUiListener2.onComplete(jSONObject);
                    }
                }

                public void onError(UiError uiError) {
                    C3082f.m7628a("openSDK_LOG.GameAppOperation", "-->unbindQQGroup, error: " + uiError);
                    IUiListener iUiListener = iUiListener;
                    if (iUiListener != null) {
                        iUiListener.onError(uiError);
                    }
                }
            };
            Bundle a = mo34798a();
            a.putString("appid", appId);
            a.putString("orgid", str);
            HttpUtils.requestAsync(this.f6457c, context, "https://graph.qq.com/cgi-bin/qunopensdk/unbind", a, "GET", new BaseApi.TempRequestListener(r2));
            C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->unBindQQGroup() do.");
        }
    }

    public void joinGroup(final Activity activity, String str, final IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.GameAppOperation", "joinQQGroup()");
        if (activity == null) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->joinGroup, activity is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1001, "param acitivty is null", "activity param of api can not be null."));
            }
        } else if (TextUtils.isEmpty(str)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->joinGroup, params is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1006, "param organizationId is null", "organizationId param of api can not be null."));
            }
        } else {
            final Intent intent = new Intent();
            String appId = this.f6457c.getAppId();
            if (TextUtils.isEmpty(appId)) {
                C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->joinGroup, appid is empty.");
                if (iUiListener != null) {
                    iUiListener.onError(new UiError(1003, "appid is null", "appid is null, please login."));
                    return;
                }
                return;
            }
            String openId = this.f6457c.getOpenId();
            if (TextUtils.isEmpty(openId)) {
                C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->joinGroup, openid is empty.");
                if (iUiListener != null) {
                    iUiListener.onError(new UiError(1004, "openid is null", "openid is null, please login."));
                    return;
                }
                return;
            }
            StringBuffer stringBuffer = new StringBuffer("mqqapi://opensdk/join_group?src_type=app&version=1");
            stringBuffer.append("&openid=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
            stringBuffer.append("&appid=" + Base64.encodeToString(C3131k.m7799i(appId), 2));
            stringBuffer.append("&organization_id=" + Base64.encodeToString(C3131k.m7799i(str), 2));
            stringBuffer.append("&sdk_version=" + Base64.encodeToString(C3131k.m7799i(Constants.SDK_VERSION), 2));
            intent.setData(Uri.parse(stringBuffer.toString()));
            if (!mo34804a(intent) || C3125h.m7757c(activity, "8.1.0") < 0) {
                C3082f.m7635d("openSDK_LOG.GameAppOperation", "-->bind group, there is no activity, show download page.");
                m7563a(activity);
                return;
            }
            C30672 r3 = new IUiListener() {
                /* class com.tencent.open.SocialOperation.C30672 */

                public void onCancel() {
                }

                public void onComplete(Object obj) {
                    C3082f.m7635d("openSDK_LOG.GameAppOperation", "-->join group resp is: " + obj);
                    if (obj == null) {
                        IUiListener iUiListener = iUiListener;
                        if (iUiListener != null) {
                            iUiListener.onError(new UiError(4001, "服务端错误，请稍后重试", "资格检查回包为null。"));
                        }
                    } else if (((JSONObject) obj).optInt("bind") == 1) {
                        try {
                            SocialOperation.this.mo34800a(activity, Constants.REQUEST_JOIN_GROUP, intent, false);
                        } catch (Exception e) {
                            C3082f.m7632b("openSDK_LOG.GameAppOperation", "-->join group, start activity exception.", e);
                            SocialOperation.this.m7563a(activity);
                        }
                    } else {
                        IUiListener iUiListener2 = iUiListener;
                        if (iUiListener2 != null) {
                            iUiListener2.onError(new UiError(AuthApiStatusCodes.AUTH_API_SERVER_ERROR, "该组织未绑群，无法加入", "该组织未绑群，无法加入。"));
                        }
                    }
                }

                public void onError(UiError uiError) {
                    C3082f.m7628a("openSDK_LOG.GameAppOperation", "-->joinQQGroup, error: " + uiError);
                    IUiListener iUiListener = iUiListener;
                    if (iUiListener != null) {
                        iUiListener.onError(uiError);
                    }
                }
            };
            Bundle a = mo34798a();
            a.putString("appid", appId);
            a.putString("orgid", str);
            HttpUtils.requestAsync(this.f6457c, activity, "https://graph.qq.com/cgi-bin/qunopensdk/check_group", a, "GET", new BaseApi.TempRequestListener(r3));
            C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->joinQQGroup() do.");
        }
    }

    public void bindQQGroup(final Activity activity, String str, String str2, final IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->bindQQGroup()  -- start");
        if (activity == null) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->bindQQGroup, activity is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1001, "param acitivty is null", "activity param of api can not be null."));
                return;
            }
            return;
        }
        StringBuffer stringBuffer = new StringBuffer("mqqapi://opensdk/bind_group?src_type=app&version=1");
        String appId = this.f6457c.getAppId();
        if (TextUtils.isEmpty(appId)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->bindQQGroup, appId is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1003, "appid is null", "please login."));
                return;
            }
            return;
        }
        String openId = this.f6457c.getOpenId();
        if (TextUtils.isEmpty(openId)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->bindQQGroup, openid is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1004, "openid params is null", "please login."));
                return;
            }
            return;
        }
        String a = C3131k.m7770a(activity);
        if (TextUtils.isEmpty(a)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->bindQQGroup, appname is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1005, "appName params is null", ""));
            }
        } else if (TextUtils.isEmpty(str)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->bindQQGroup, organization id is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1006, "organizationId params is null", ""));
            }
        } else if (TextUtils.isEmpty(str2)) {
            C3082f.m7636e("openSDK_LOG.GameAppOperation", "-->bindQQGroup, organization name is empty.");
            if (iUiListener != null) {
                iUiListener.onError(new UiError(1007, "organizationName params is null", ""));
            }
        } else {
            stringBuffer.append("&app_name=" + Base64.encodeToString(C3131k.m7799i(a), 2));
            stringBuffer.append("&organization_id=" + Base64.encodeToString(C3131k.m7799i(str), 2));
            stringBuffer.append("&organization_name=" + Base64.encodeToString(C3131k.m7799i(str2), 2));
            stringBuffer.append("&openid=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
            stringBuffer.append("&appid=" + Base64.encodeToString(C3131k.m7799i(appId), 2));
            stringBuffer.append("&sdk_version=" + Base64.encodeToString(C3131k.m7799i(Constants.SDK_VERSION), 2));
            C3082f.m7628a("openSDK_LOG.GameAppOperation", "-->bindQQGroup, url: " + stringBuffer.toString());
            Uri parse = Uri.parse(stringBuffer.toString());
            final Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(parse);
            if (!mo34804a(intent) || C3125h.m7757c(activity, "8.1.0") < 0) {
                C3082f.m7635d("openSDK_LOG.GameAppOperation", "-->bind group, there is no activity, show download page.");
                m7563a(activity);
                return;
            }
            C30683 r12 = new IUiListener() {
                /* class com.tencent.open.SocialOperation.C30683 */

                public void onCancel() {
                }

                public void onComplete(Object obj) {
                    C3082f.m7635d("openSDK_LOG.GameAppOperation", "-->bind group resp is: " + obj);
                    if (obj == null) {
                        IUiListener iUiListener = iUiListener;
                        if (iUiListener != null) {
                            iUiListener.onError(new UiError(4001, "服务端错误，请稍后重试", "资格检查回包为null。"));
                        }
                    } else if (((JSONObject) obj).optInt("bind") != 1) {
                        try {
                            SocialOperation.this.mo34800a(activity, Constants.REQUEST_BIND_GROUP, intent, false);
                        } catch (Exception e) {
                            C3082f.m7632b("openSDK_LOG.GameAppOperation", "-->bind group, start activity exception.", e);
                            SocialOperation.this.m7563a(activity);
                        }
                    } else {
                        IUiListener iUiListener2 = iUiListener;
                        if (iUiListener2 != null) {
                            iUiListener2.onError(new UiError(AuthApiStatusCodes.AUTH_API_CLIENT_ERROR, "该群已绑定！", "绑定过的群不能再次绑定。"));
                        }
                        C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->bindQQGroup() binded return.");
                    }
                }

                public void onError(UiError uiError) {
                    C3082f.m7628a("openSDK_LOG.GameAppOperation", "-->bindQQGroup, error: " + uiError);
                    IUiListener iUiListener = iUiListener;
                    if (iUiListener != null) {
                        iUiListener.onError(uiError);
                    }
                }
            };
            Bundle a2 = mo34798a();
            a2.putString("appid", appId);
            a2.putString("orgid", str);
            HttpUtils.requestAsync(this.f6457c, activity, "https://graph.qq.com/cgi-bin/qunopensdk/check_group", a2, "GET", new BaseApi.TempRequestListener(r12));
            C3082f.m7634c("openSDK_LOG.GameAppOperation", "-->bindQQGroup() do.");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m7563a(Activity activity) {
        m7564a(activity, "");
    }

    /* renamed from: a */
    private void m7564a(Activity activity, String str) {
        new TDialog(activity, "", mo34799a(str), null, this.f6457c).show();
    }
}
