package com.tencent.connect.auth;

import android.content.SharedPreferences;
import android.util.Base64;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3120d;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3131k;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class QQToken {
    public static final int AUTH_QQ = 2;
    public static final int AUTH_QZONE = 3;
    public static final int AUTH_WEB = 1;

    /* renamed from: f */
    private static SharedPreferences f6341f;

    /* renamed from: a */
    private String f6342a;

    /* renamed from: b */
    private String f6343b;

    /* renamed from: c */
    private String f6344c;

    /* renamed from: d */
    private int f6345d = 1;

    /* renamed from: e */
    private long f6346e = -1;

    public QQToken(String str) {
        this.f6342a = str;
    }

    public boolean isSessionValid() {
        return this.f6343b != null && System.currentTimeMillis() < this.f6346e;
    }

    public String getAppId() {
        return this.f6342a;
    }

    public void setAppId(String str) {
        this.f6342a = str;
    }

    public String getAccessToken() {
        return this.f6343b;
    }

    public void setAccessToken(String str, String str2) throws NumberFormatException {
        this.f6343b = str;
        this.f6346e = 0;
        if (str2 != null) {
            this.f6346e = System.currentTimeMillis() + (Long.parseLong(str2) * 1000);
        }
    }

    public String getOpenId() {
        return this.f6344c;
    }

    public void setOpenId(String str) {
        this.f6344c = str;
    }

    public int getAuthSource() {
        return this.f6345d;
    }

    public void setAuthSource(int i) {
        this.f6345d = i;
    }

    public long getExpireTimeInSecond() {
        return this.f6346e;
    }

    public void saveSession(JSONObject jSONObject) {
        try {
            m7194a(this.f6342a, jSONObject);
        } catch (Exception e) {
            C3082f.m7634c("QQToken", "login saveSession" + e.toString());
        }
    }

    public JSONObject loadSession(String str) {
        try {
            return m7193a(str);
        } catch (Exception e) {
            C3082f.m7634c("QQToken", "login loadSession" + e.toString());
            return null;
        }
    }

    /* renamed from: a */
    private static synchronized SharedPreferences m7192a() {
        SharedPreferences sharedPreferences;
        synchronized (QQToken.class) {
            if (f6341f == null) {
                f6341f = C3121e.m7727a().getSharedPreferences("token_info_file", 0);
            }
            sharedPreferences = f6341f;
        }
        return sharedPreferences;
    }

    /* renamed from: a */
    private static synchronized JSONObject m7193a(String str) {
        synchronized (QQToken.class) {
            if (C3121e.m7727a() == null) {
                C3082f.m7634c("QQToken", "loadJsonPreference context null");
                return null;
            } else if (str == null) {
                return null;
            } else {
                String string = m7192a().getString(Base64.encodeToString(C3131k.m7799i(str), 2), null);
                if (string == null) {
                    C3082f.m7634c("QQToken", "loadJsonPreference encoded value null");
                    return null;
                }
                try {
                    JSONObject jSONObject = new JSONObject(C3120d.m7726b(string, "asdfghjk"));
                    return jSONObject;
                } catch (Exception e) {
                    C3082f.m7634c("QQToken", "loadJsonPreference decode" + e.toString());
                    return null;
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x005f, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0065, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized void m7194a(java.lang.String r8, org.json.JSONObject r9) {
        /*
            java.lang.Class<com.tencent.connect.auth.QQToken> r0 = com.tencent.connect.auth.QQToken.class
            monitor-enter(r0)
            android.content.Context r1 = com.tencent.open.utils.C3121e.m7727a()     // Catch:{ all -> 0x0066 }
            if (r1 != 0) goto L_0x0012
            java.lang.String r8 = "QQToken"
            java.lang.String r9 = "saveJsonPreference context null"
            com.tencent.open.p059a.C3082f.m7634c(r8, r9)     // Catch:{ all -> 0x0066 }
            monitor-exit(r0)
            return
        L_0x0012:
            if (r8 == 0) goto L_0x0064
            if (r9 != 0) goto L_0x0017
            goto L_0x0064
        L_0x0017:
            java.lang.String r1 = "expires_in"
            java.lang.String r1 = r9.getString(r1)     // Catch:{ Exception -> 0x0062 }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0062 }
            if (r2 != 0) goto L_0x0060
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0062 }
            long r4 = java.lang.Long.parseLong(r1)     // Catch:{ Exception -> 0x0062 }
            r6 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r6
            long r2 = r2 + r4
            java.lang.String r1 = "expires_time"
            r9.put(r1, r2)     // Catch:{ Exception -> 0x0062 }
            byte[] r8 = com.tencent.open.utils.C3131k.m7799i(r8)     // Catch:{ all -> 0x0066 }
            r1 = 2
            java.lang.String r8 = android.util.Base64.encodeToString(r8, r1)     // Catch:{ all -> 0x0066 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x0066 }
            java.lang.String r1 = "asdfghjk"
            java.lang.String r9 = com.tencent.open.utils.C3120d.m7725a(r9, r1)     // Catch:{ all -> 0x0066 }
            if (r8 == 0) goto L_0x005e
            if (r9 != 0) goto L_0x004d
            goto L_0x005e
        L_0x004d:
            android.content.SharedPreferences r1 = m7192a()     // Catch:{ all -> 0x0066 }
            android.content.SharedPreferences$Editor r1 = r1.edit()     // Catch:{ all -> 0x0066 }
            android.content.SharedPreferences$Editor r8 = r1.putString(r8, r9)     // Catch:{ all -> 0x0066 }
            r8.commit()     // Catch:{ all -> 0x0066 }
            monitor-exit(r0)
            return
        L_0x005e:
            monitor-exit(r0)
            return
        L_0x0060:
            monitor-exit(r0)
            return
        L_0x0062:
            monitor-exit(r0)
            return
        L_0x0064:
            monitor-exit(r0)
            return
        L_0x0066:
            r8 = move-exception
            monitor-exit(r0)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connect.auth.QQToken.m7194a(java.lang.String, org.json.JSONObject):void");
    }

    public void removeSession(String str) {
        m7192a().edit().remove(Base64.encodeToString(C3131k.m7799i(str), 2)).commit();
    }
}
