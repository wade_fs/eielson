package com.tencent.open.p059a;

import android.util.Log;

/* renamed from: com.tencent.open.a.e */
/* compiled from: ProGuard */
public final class C3081e extends C3085i {

    /* renamed from: a */
    public static final C3081e f6741a = new C3081e();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35076a(int i, Thread thread, long j, String str, String str2, Throwable th) {
        if (i == 1) {
            Log.v(str, str2, th);
        } else if (i == 2) {
            Log.d(str, str2, th);
        } else if (i == 4) {
            Log.i(str, str2, th);
        } else if (i == 8) {
            Log.w(str, str2, th);
        } else if (i == 16) {
            Log.e(str, str2, th);
        } else if (i == 32) {
            Log.e(str, str2, th);
        }
    }
}
