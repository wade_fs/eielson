package com.tencent.open.p060b;

import android.os.SystemClock;
import com.tencent.open.utils.C3131k;

/* renamed from: com.tencent.open.b.d */
/* compiled from: ProGuard */
public class C3091d {

    /* renamed from: a */
    protected static C3091d f6762a;

    protected C3091d() {
    }

    /* renamed from: a */
    public static synchronized C3091d m7665a() {
        C3091d dVar;
        synchronized (C3091d.class) {
            if (f6762a == null) {
                f6762a = new C3091d();
            }
            dVar = f6762a;
        }
        return dVar;
    }

    /* renamed from: a */
    public void mo35122a(String str, String str2, String str3, String str4, String str5, String str6) {
        C3094g.m7676a().mo35131a(C3131k.m7766a(str, str3, str4, str5, str2, str6), str2, true);
    }

    /* renamed from: a */
    public void mo35123a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8) {
        C3094g.m7676a().mo35131a(C3131k.m7768a(str, str4, str5, str3, str2, str6, "", str7, str8, "", "", ""), str2, false);
    }

    /* renamed from: a */
    public void mo35124a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10) {
        C3094g.m7676a().mo35131a(C3131k.m7768a(str, str4, str5, str3, str2, str6, str7, "", "", str8, str9, str10), str2, false);
    }

    /* renamed from: a */
    public void mo35121a(int i, String str, String str2, String str3, String str4, Long l, int i2, int i3, String str5) {
        long elapsedRealtime = SystemClock.elapsedRealtime() - l.longValue();
        if (l.longValue() == 0 || elapsedRealtime < 0) {
            elapsedRealtime = 0;
        }
        StringBuffer stringBuffer = new StringBuffer("https://huatuocode.huatuo.qq.com");
        stringBuffer.append("?domain=mobile.opensdk.com&cgi=opensdk&type=");
        stringBuffer.append(i);
        stringBuffer.append("&code=");
        stringBuffer.append(i2);
        stringBuffer.append("&time=");
        stringBuffer.append(elapsedRealtime);
        stringBuffer.append("&rate=");
        stringBuffer.append(i3);
        stringBuffer.append("&uin=");
        stringBuffer.append(str2);
        stringBuffer.append("&data=");
        C3094g.m7676a().mo35134a(stringBuffer.toString(), "GET", C3131k.m7767a(String.valueOf(i), String.valueOf(i2), String.valueOf(elapsedRealtime), String.valueOf(i3), str, str2, str3, str4, str5), true);
    }
}
