package com.tencent.open.utils;

/* renamed from: com.tencent.open.utils.l */
/* compiled from: ProGuard */
public final class C3134l implements Cloneable {

    /* renamed from: a */
    private long f6873a;

    public C3134l(long j) {
        this.f6873a = j;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C3134l) || this.f6873a != ((C3134l) obj).mo35182b()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public byte[] mo35181a() {
        long j = this.f6873a;
        return new byte[]{(byte) ((int) (255 & j)), (byte) ((int) ((65280 & j) >> 8)), (byte) ((int) ((16711680 & j) >> 16)), (byte) ((int) ((j & 4278190080L) >> 24))};
    }

    /* renamed from: b */
    public long mo35182b() {
        return this.f6873a;
    }

    public int hashCode() {
        return (int) this.f6873a;
    }
}
