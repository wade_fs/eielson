package com.tencent.mid.p054b;

import android.content.Context;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.mid.b.g */
public class C3031g {

    /* renamed from: h */
    private static C3031g f6556h;

    /* renamed from: a */
    Map<Integer, C3030f> f6557a = null;

    /* renamed from: b */
    MidEntity f6558b = null;

    /* renamed from: c */
    Map<Integer, C3030f> f6559c = null;

    /* renamed from: d */
    boolean f6560d = true;

    /* renamed from: e */
    private Map<Integer, C3030f> f6561e = null;

    /* renamed from: f */
    private Context f6562f = null;

    /* renamed from: g */
    private C3038d f6563g = Util.getLogger();

    /* renamed from: i */
    private MidEntity f6564i = null;

    private C3031g(Context context) {
        this.f6562f = context.getApplicationContext();
        this.f6561e = new HashMap(3);
        this.f6561e.put(1, new C3029e(context, 3));
        this.f6561e.put(2, new C3027c(context, 3));
        this.f6561e.put(4, new C3028d(context, 3));
    }

    /* renamed from: a */
    public static synchronized C3031g m7437a(Context context) {
        C3031g gVar;
        synchronized (C3031g.class) {
            if (f6556h == null) {
                f6556h = new C3031g(context);
            }
            gVar = f6556h;
        }
        return gVar;
    }

    /* renamed from: l */
    private Map<Integer, C3030f> m7438l() {
        if (this.f6557a == null) {
            this.f6557a = new HashMap(3);
            this.f6557a.put(1, new C3029e(this.f6562f, 1000001));
            this.f6557a.put(2, new C3027c(this.f6562f, 1000001));
            this.f6557a.put(4, new C3028d(this.f6562f, 1000001));
        }
        return this.f6557a;
    }

    /* renamed from: a */
    public MidEntity mo34904a() {
        m7438l();
        if (!Util.isMidValid(this.f6558b)) {
            this.f6558b = mo34906a(new ArrayList(Arrays.asList(4, 1, 2)), this.f6557a);
        }
        C3038d dVar = this.f6563g;
        dVar.mo34956h("readNewVersionMidEntity:" + this.f6558b);
        return this.f6558b;
    }

    /* renamed from: b */
    public String mo34912b() {
        mo34904a();
        return Util.isMidValid(this.f6558b) ? this.f6558b.getMid() : "";
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mid.b.g.a(com.tencent.mid.api.MidEntity, boolean):void
     arg types: [com.tencent.mid.api.MidEntity, int]
     candidates:
      com.tencent.mid.b.g.a(int, java.util.Map<java.lang.Integer, com.tencent.mid.b.f>):com.tencent.mid.api.MidEntity
      com.tencent.mid.b.g.a(java.util.List<java.lang.Integer>, java.util.Map<java.lang.Integer, com.tencent.mid.b.f>):com.tencent.mid.api.MidEntity
      com.tencent.mid.b.g.a(int, int):void
      com.tencent.mid.b.g.a(com.tencent.mid.api.MidEntity, boolean):void */
    /* renamed from: a */
    public void mo34908a(MidEntity midEntity) {
        mo34909a(midEntity, true);
    }

    /* renamed from: a */
    public void mo34909a(MidEntity midEntity, boolean z) {
        Context context;
        if (midEntity.getTimestamps() <= 0) {
            midEntity.setTimestamps(System.currentTimeMillis());
        }
        C3038d dVar = this.f6563g;
        dVar.mo34956h("writeNewVersionMidEntity midEntity:" + midEntity);
        for (Map.Entry<Integer, C3030f> entry : m7438l().entrySet()) {
            ((C3030f) entry.getValue()).mo34894a(midEntity);
        }
        if (z && (context = this.f6562f) != null) {
            Util.insertMid2Provider(context, context.getPackageName(), midEntity.toString());
        }
    }

    /* renamed from: c */
    public MidEntity mo34915c() {
        return m7436a(4, m7438l());
    }

    /* renamed from: d */
    public MidEntity mo34917d() {
        return m7436a(1, m7438l());
    }

    /* renamed from: e */
    public MidEntity mo34919e() {
        return m7436a(2, m7438l());
    }

    /* renamed from: b */
    public void mo34913b(MidEntity midEntity) {
        m7438l();
        C3030f fVar = this.f6557a.get(4);
        if (fVar != null) {
            fVar.mo34894a(midEntity);
        }
    }

    /* renamed from: c */
    public void mo34916c(MidEntity midEntity) {
        m7438l();
        C3030f fVar = this.f6557a.get(1);
        if (fVar != null) {
            fVar.mo34894a(midEntity);
        }
        C3030f fVar2 = this.f6557a.get(2);
        if (fVar2 != null) {
            fVar2.mo34894a(midEntity);
        }
    }

    /* renamed from: m */
    private Map<Integer, C3030f> m7439m() {
        if (this.f6559c == null) {
            this.f6559c = new HashMap(3);
            this.f6559c.put(1, new C3029e(this.f6562f, 0));
            this.f6559c.put(2, new C3027c(this.f6562f, 0));
            this.f6559c.put(4, new C3028d(this.f6562f, 0));
        }
        return this.f6559c;
    }

    /* renamed from: f */
    public String mo34921f() {
        try {
            mo34923g();
            if (this.f6564i != null) {
                return this.f6564i.getMid();
            }
            return "0";
        } catch (Throwable th) {
            C3038d dVar = this.f6563g;
            dVar.mo34954f("readMidString " + th);
            return "0";
        }
    }

    /* renamed from: g */
    public MidEntity mo34923g() {
        if (!Util.isMidValid(this.f6564i)) {
            this.f6563g.mo34956h("read the new one");
            this.f6564i = mo34906a(new ArrayList(Arrays.asList(4)), this.f6561e);
        }
        if (!Util.isMidValid(this.f6564i)) {
            this.f6563g.mo34956h("load from the old one");
            MidEntity a = mo34906a(new ArrayList(Arrays.asList(4)), m7439m());
            if (Util.isMidValid(a)) {
                C3038d dVar = this.f6563g;
                dVar.mo34952d("copy old mid:" + a.getMid() + " to new version.");
                this.f6564i = a;
                mo34922f(this.f6564i);
            }
        }
        if (!Util.isMidValid(this.f6564i)) {
            this.f6563g.mo34956h("query other app");
            Map<String, MidEntity> midsByApps = Util.getMidsByApps(this.f6562f, 2);
            if (midsByApps != null && midsByApps.size() > 0) {
                Iterator<Map.Entry<String, MidEntity>> it = midsByApps.entrySet().iterator();
                while (true) {
                    if (it.hasNext()) {
                        MidEntity midEntity = (MidEntity) it.next().getValue();
                        if (midEntity != null && midEntity.isMidValid()) {
                            this.f6564i = midEntity;
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
        }
        if (!Util.isMidValid(this.f6564i)) {
            this.f6563g.mo34956h("read the new one");
            this.f6564i = mo34906a(new ArrayList(Arrays.asList(4, 1, 2)), this.f6561e);
        }
        if (!Util.isMidValid(this.f6564i)) {
            this.f6563g.mo34956h("load from the old one");
            MidEntity a2 = mo34906a(new ArrayList(Arrays.asList(1, 2, 4)), m7439m());
            if (Util.isMidValid(a2)) {
                C3038d dVar2 = this.f6563g;
                dVar2.mo34952d("copy old mid:" + a2.getMid() + " to new version.");
                this.f6564i = a2;
                mo34922f(this.f6564i);
            }
        }
        if (this.f6560d) {
            this.f6563g.mo34956h("firstRead");
            MidEntity h = mo34925h();
            if (h == null || !h.isMidValid()) {
                mo34918d(this.f6564i);
            }
            this.f6560d = false;
        }
        MidEntity midEntity2 = this.f6564i;
        return midEntity2 != null ? midEntity2 : new MidEntity();
    }

    /* renamed from: a */
    public MidEntity mo34905a(List<Integer> list) {
        return mo34906a(list, this.f6561e);
    }

    /* renamed from: d */
    public void mo34918d(MidEntity midEntity) {
        C3030f fVar = this.f6561e.get(4);
        if (fVar != null) {
            fVar.mo34894a(midEntity);
        }
    }

    /* renamed from: e */
    public void mo34920e(MidEntity midEntity) {
        C3030f fVar = this.f6561e.get(1);
        if (fVar != null) {
            fVar.mo34894a(midEntity);
        }
        C3030f fVar2 = this.f6561e.get(2);
        if (fVar2 != null) {
            fVar2.mo34894a(midEntity);
        }
    }

    /* renamed from: h */
    public MidEntity mo34925h() {
        return m7436a(4, this.f6561e);
    }

    /* renamed from: i */
    public MidEntity mo34926i() {
        return m7436a(1, this.f6561e);
    }

    /* renamed from: j */
    public MidEntity mo34927j() {
        return m7436a(2, this.f6561e);
    }

    /* renamed from: a */
    private MidEntity m7436a(int i, Map<Integer, C3030f> map) {
        C3030f fVar;
        if (this.f6561e == null || (fVar = map.get(Integer.valueOf(i))) == null) {
            return null;
        }
        return fVar.mo34902i();
    }

    /* renamed from: a */
    public MidEntity mo34906a(List<Integer> list, Map<Integer, C3030f> map) {
        MidEntity i;
        if (!(list == null || list.size() == 0 || map == null || map.size() == 0)) {
            for (Integer num : list) {
                C3030f fVar = map.get(num);
                if (fVar != null && (i = fVar.mo34902i()) != null && i.isMidValid()) {
                    return i;
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo34907a(int i, int i2) {
        C3025a k = mo34928k();
        if (i > 0) {
            k.mo34883c(i);
        }
        if (i2 > 0) {
            k.mo34878a(i2);
        }
        k.mo34879a(System.currentTimeMillis());
        k.mo34881b(0);
        mo34910a(k);
    }

    /* renamed from: k */
    public C3025a mo34928k() {
        return mo34911b(new ArrayList(Arrays.asList(1, 4)));
    }

    /* renamed from: b */
    public C3025a mo34911b(List<Integer> list) {
        C3025a j;
        if (!(list == null || list.size() == 0)) {
            for (Integer num : list) {
                C3030f fVar = this.f6561e.get(num);
                if (fVar != null && (j = fVar.mo34903j()) != null) {
                    return j;
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo34910a(C3025a aVar) {
        if (aVar.mo34880b() <= 0) {
            aVar.mo34879a(System.currentTimeMillis());
        }
        for (Map.Entry<Integer, C3030f> entry : this.f6561e.entrySet()) {
            ((C3030f) entry.getValue()).mo34895b(aVar);
        }
    }

    /* renamed from: f */
    public void mo34922f(MidEntity midEntity) {
        if (midEntity.getTimestamps() <= 0) {
            midEntity.setTimestamps(System.currentTimeMillis());
        }
        for (Map.Entry<Integer, C3030f> entry : this.f6561e.entrySet()) {
            ((C3030f) entry.getValue()).mo34894a(midEntity);
        }
    }

    /* renamed from: b */
    public void mo34914b(MidEntity midEntity, boolean z) {
        Context context;
        if (midEntity.getTimestamps() <= 0) {
            midEntity.setTimestamps(System.currentTimeMillis());
        }
        for (Map.Entry<Integer, C3030f> entry : this.f6561e.entrySet()) {
            ((C3030f) entry.getValue()).mo34894a(midEntity);
        }
        if (z && (context = this.f6562f) != null) {
            Util.insertMid2OldProvider(context, context.getPackageName(), midEntity.toString());
        }
    }

    /* renamed from: g */
    public void mo34924g(MidEntity midEntity) {
        if (midEntity.getTimestamps() <= 0) {
            midEntity.setTimestamps(System.currentTimeMillis());
        }
        for (Map.Entry<Integer, C3030f> entry : m7439m().entrySet()) {
            ((C3030f) entry.getValue()).mo34894a(midEntity);
        }
    }
}
