package com.tencent.bugly;

import com.tencent.bugly.crashreport.common.info.C2851a;
import java.util.Map;

/* compiled from: BUGLY */
public class BuglyStrategy {

    /* renamed from: a */
    private String f5280a;

    /* renamed from: b */
    private String f5281b;

    /* renamed from: c */
    private String f5282c;

    /* renamed from: d */
    private long f5283d;

    /* renamed from: e */
    private String f5284e;

    /* renamed from: f */
    private String f5285f;

    /* renamed from: g */
    private boolean f5286g = true;

    /* renamed from: h */
    private boolean f5287h = true;

    /* renamed from: i */
    private boolean f5288i = true;

    /* renamed from: j */
    private Class<?> f5289j = null;

    /* renamed from: k */
    private boolean f5290k = true;

    /* renamed from: l */
    private boolean f5291l = true;

    /* renamed from: m */
    private boolean f5292m = true;

    /* renamed from: n */
    private boolean f5293n = false;

    /* renamed from: o */
    private C2796a f5294o;

    public synchronized BuglyStrategy setBuglyLogUpload(boolean z) {
        this.f5290k = z;
        return this;
    }

    public synchronized BuglyStrategy setRecordUserInfoOnceADay(boolean z) {
        this.f5293n = z;
        return this;
    }

    public synchronized BuglyStrategy setUploadProcess(boolean z) {
        this.f5292m = z;
        return this;
    }

    public synchronized boolean isUploadProcess() {
        return this.f5292m;
    }

    public synchronized boolean isBuglyLogUpload() {
        return this.f5290k;
    }

    public synchronized boolean recordUserInfoOnceADay() {
        return this.f5293n;
    }

    public boolean isReplaceOldChannel() {
        return this.f5291l;
    }

    public void setReplaceOldChannel(boolean z) {
        this.f5291l = z;
    }

    public synchronized String getAppVersion() {
        return this.f5280a == null ? C2851a.m6471b().f5671p : this.f5280a;
    }

    public synchronized BuglyStrategy setAppVersion(String str) {
        this.f5280a = str;
        return this;
    }

    public synchronized BuglyStrategy setUserInfoActivity(Class<?> cls) {
        this.f5289j = cls;
        return this;
    }

    public synchronized Class<?> getUserInfoActivity() {
        return this.f5289j;
    }

    public synchronized String getAppChannel() {
        return this.f5281b == null ? C2851a.m6471b().f5673r : this.f5281b;
    }

    public synchronized BuglyStrategy setAppChannel(String str) {
        this.f5281b = str;
        return this;
    }

    public synchronized String getAppPackageName() {
        return this.f5282c == null ? C2851a.m6471b().f5659d : this.f5282c;
    }

    public synchronized BuglyStrategy setAppPackageName(String str) {
        this.f5282c = str;
        return this;
    }

    public synchronized long getAppReportDelay() {
        return this.f5283d;
    }

    public synchronized BuglyStrategy setAppReportDelay(long j) {
        this.f5283d = j;
        return this;
    }

    public synchronized String getLibBuglySOFilePath() {
        return this.f5284e;
    }

    public synchronized BuglyStrategy setLibBuglySOFilePath(String str) {
        this.f5284e = str;
        return this;
    }

    public synchronized String getDeviceID() {
        return this.f5285f;
    }

    public synchronized BuglyStrategy setDeviceID(String str) {
        this.f5285f = str;
        return this;
    }

    public synchronized boolean isEnableNativeCrashMonitor() {
        return this.f5286g;
    }

    public synchronized BuglyStrategy setEnableNativeCrashMonitor(boolean z) {
        this.f5286g = z;
        return this;
    }

    public synchronized BuglyStrategy setEnableUserInfo(boolean z) {
        this.f5288i = z;
        return this;
    }

    public synchronized boolean isEnableUserInfo() {
        return this.f5288i;
    }

    public synchronized boolean isEnableANRCrashMonitor() {
        return this.f5287h;
    }

    public synchronized BuglyStrategy setEnableANRCrashMonitor(boolean z) {
        this.f5287h = z;
        return this;
    }

    public synchronized C2796a getCrashHandleCallback() {
        return this.f5294o;
    }

    public synchronized BuglyStrategy setCrashHandleCallback(C2796a aVar) {
        this.f5294o = aVar;
        return this;
    }

    /* renamed from: com.tencent.bugly.BuglyStrategy$a */
    /* compiled from: BUGLY */
    public static class C2796a {
        public static final int CRASHTYPE_ANR = 4;
        public static final int CRASHTYPE_BLOCK = 7;
        public static final int CRASHTYPE_COCOS2DX_JS = 5;
        public static final int CRASHTYPE_COCOS2DX_LUA = 6;
        public static final int CRASHTYPE_JAVA_CATCH = 1;
        public static final int CRASHTYPE_JAVA_CRASH = 0;
        public static final int CRASHTYPE_NATIVE = 2;
        public static final int CRASHTYPE_U3D = 3;
        public static final int MAX_USERDATA_KEY_LENGTH = 100;
        public static final int MAX_USERDATA_VALUE_LENGTH = 30000;

        public synchronized Map<String, String> onCrashHandleStart(int i, String str, String str2, String str3) {
            return null;
        }

        public synchronized byte[] onCrashHandleStart2GetExtraDatas(int i, String str, String str2, String str3) {
            return null;
        }
    }
}
