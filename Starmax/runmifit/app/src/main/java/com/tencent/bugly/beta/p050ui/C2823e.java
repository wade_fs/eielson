package com.tencent.bugly.beta.p050ui;

import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.global.C2805b;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.proguard.C2903an;

/* renamed from: com.tencent.bugly.beta.ui.e */
/* compiled from: BUGLY */
public class C2823e extends C2816a {

    /* renamed from: n */
    protected TextView f5433n;

    /* renamed from: a */
    public boolean mo34157a(int i, KeyEvent keyEvent) {
        return false;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f5408l = C2808e.f5336E.f5376j;
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        if (this.f5408l == 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            this.f5433n = new TextView(this.f5397a);
            this.f5433n.setLayoutParams(layoutParams);
            TextView textView = this.f5433n;
            this.f5406j.getClass();
            textView.setTextColor(Color.parseColor("#757575"));
            this.f5433n.setTextSize(16.0f);
            this.f5433n.setTag(Beta.TAG_TIP_MESSAGE);
            this.f5405i.addView(this.f5433n);
        } else if (onCreateView != null) {
            this.f5433n = (TextView) onCreateView.findViewWithTag(Beta.TAG_TIP_MESSAGE);
        }
        try {
            this.f5433n.setText("检测到当前版本需要重启，是否重启应用？");
            this.f5402f.setText("更新提示");
            mo34151a("取消", new C2805b(8, this), "重启应用", new C2805b(7, this));
        } catch (Exception e) {
            if (this.f5408l != 0) {
                C2903an.m6865e("please confirm your argument: [Beta.tipsDialogLayoutId] is correct", new Object[0]);
            }
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
        }
        return onCreateView;
    }
}
