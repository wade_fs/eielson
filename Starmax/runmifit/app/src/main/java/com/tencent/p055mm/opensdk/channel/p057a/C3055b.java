package com.tencent.p055mm.opensdk.channel.p057a;

import com.tencent.p055mm.opensdk.p056a.C3048b;

/* renamed from: com.tencent.mm.opensdk.channel.a.b */
public final class C3055b {
    /* renamed from: a */
    public static byte[] m7544a(String str, int i, String str2) {
        StringBuffer stringBuffer = new StringBuffer();
        if (str != null) {
            stringBuffer.append(str);
        }
        stringBuffer.append(i);
        stringBuffer.append(str2);
        stringBuffer.append("mMcShCsTr");
        return C3048b.m7540c(stringBuffer.toString().substring(1, 9).getBytes()).getBytes();
    }
}
