package com.tencent.bugly.proguard;

import android.support.v4.app.NotificationCompat;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.y */
/* compiled from: BUGLY */
public final class C2959y extends C2944m implements Cloneable {

    /* renamed from: q */
    static C2956v f6277q;

    /* renamed from: r */
    static C2955u f6278r;

    /* renamed from: s */
    static C2955u f6279s;

    /* renamed from: t */
    static Map<String, String> f6280t;

    /* renamed from: u */
    static final /* synthetic */ boolean f6281u = (!C2959y.class.desiredAssertionStatus());

    /* renamed from: a */
    public String f6282a = "";

    /* renamed from: b */
    public String f6283b = "";

    /* renamed from: c */
    public long f6284c = 0;

    /* renamed from: d */
    public int f6285d = 0;

    /* renamed from: e */
    public C2956v f6286e = null;

    /* renamed from: f */
    public C2955u f6287f = null;

    /* renamed from: g */
    public byte f6288g = 0;

    /* renamed from: h */
    public int f6289h = 0;

    /* renamed from: i */
    public long f6290i = 0;

    /* renamed from: j */
    public C2955u f6291j = null;

    /* renamed from: k */
    public String f6292k = "";

    /* renamed from: l */
    public Map<String, String> f6293l = null;

    /* renamed from: m */
    public String f6294m = "";

    /* renamed from: n */
    public int f6295n = 0;

    /* renamed from: o */
    public long f6296o = 0;

    /* renamed from: p */
    public int f6297p = 0;

    /* renamed from: a */
    public long mo34695a() {
        return this.f6284c;
    }

    /* renamed from: b */
    public C2955u mo34696b() {
        return this.f6287f;
    }

    public C2959y() {
    }

    public C2959y(String str, String str2, long j, int i, C2956v vVar, C2955u uVar, byte b, int i2, long j2, C2955u uVar2, String str3, Map<String, String> map, String str4, int i3, long j3, int i4) {
        this.f6282a = str;
        this.f6283b = str2;
        this.f6284c = j;
        this.f6285d = i;
        this.f6286e = vVar;
        this.f6287f = uVar;
        this.f6288g = b;
        this.f6289h = i2;
        this.f6290i = j2;
        this.f6291j = uVar2;
        this.f6292k = str3;
        this.f6293l = map;
        this.f6294m = str4;
        this.f6295n = i3;
        this.f6296o = j3;
        this.f6297p = i4;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C2959y yVar = (C2959y) obj;
        if (!C2945n.m7123a(this.f6282a, yVar.f6282a) || !C2945n.m7123a(this.f6283b, yVar.f6283b) || !C2945n.m7122a(this.f6284c, yVar.f6284c) || !C2945n.m7121a(this.f6285d, yVar.f6285d) || !C2945n.m7123a(this.f6286e, yVar.f6286e) || !C2945n.m7123a(this.f6287f, yVar.f6287f) || !C2945n.m7120a(this.f6288g, yVar.f6288g) || !C2945n.m7121a(this.f6289h, yVar.f6289h) || !C2945n.m7122a(this.f6290i, yVar.f6290i) || !C2945n.m7123a(this.f6291j, yVar.f6291j) || !C2945n.m7123a(this.f6292k, yVar.f6292k) || !C2945n.m7123a(this.f6293l, yVar.f6293l) || !C2945n.m7123a(this.f6294m, yVar.f6294m) || !C2945n.m7121a(this.f6295n, yVar.f6295n) || !C2945n.m7122a(this.f6296o, yVar.f6296o) || !C2945n.m7121a(this.f6297p, yVar.f6297p)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f6281u) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
     arg types: [com.tencent.bugly.proguard.v, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
     arg types: [com.tencent.bugly.proguard.u, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34648a(this.f6282a, 0);
        lVar.mo34648a(this.f6283b, 1);
        lVar.mo34645a(this.f6284c, 2);
        lVar.mo34644a(this.f6285d, 3);
        lVar.mo34646a((C2944m) this.f6286e, 4);
        lVar.mo34646a((C2944m) this.f6287f, 5);
        lVar.mo34660b(this.f6288g, 6);
        lVar.mo34644a(this.f6289h, 7);
        lVar.mo34645a(this.f6290i, 8);
        C2955u uVar = this.f6291j;
        if (uVar != null) {
            lVar.mo34646a((C2944m) super, 9);
        }
        String str = this.f6292k;
        if (str != null) {
            lVar.mo34648a(str, 10);
        }
        Map<String, String> map = this.f6293l;
        if (map != null) {
            lVar.mo34650a((Map) map, 11);
        }
        String str2 = this.f6294m;
        if (str2 != null) {
            lVar.mo34648a(str2, 12);
        }
        lVar.mo34644a(this.f6295n, 13);
        lVar.mo34645a(this.f6296o, 14);
        lVar.mo34644a(this.f6297p, 15);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
     arg types: [com.tencent.bugly.proguard.v, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
     arg types: [com.tencent.bugly.proguard.u, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6282a = kVar.mo34621a(0, true);
        this.f6283b = kVar.mo34621a(1, true);
        this.f6284c = kVar.mo34618a(this.f6284c, 2, true);
        this.f6285d = kVar.mo34616a(this.f6285d, 3, true);
        if (f6277q == null) {
            f6277q = new C2956v();
        }
        this.f6286e = (C2956v) kVar.mo34619a((C2944m) f6277q, 4, true);
        if (f6278r == null) {
            f6278r = new C2955u();
        }
        this.f6287f = (C2955u) kVar.mo34619a((C2944m) f6278r, 5, true);
        this.f6288g = kVar.mo34613a(this.f6288g, 6, true);
        this.f6289h = kVar.mo34616a(this.f6289h, 7, false);
        this.f6290i = kVar.mo34618a(this.f6290i, 8, false);
        if (f6279s == null) {
            f6279s = new C2955u();
        }
        this.f6291j = (C2955u) kVar.mo34619a((C2944m) f6279s, 9, false);
        this.f6292k = kVar.mo34621a(10, false);
        if (f6280t == null) {
            f6280t = new HashMap();
            f6280t.put("", "");
        }
        this.f6293l = (Map) kVar.mo34620a((Object) f6280t, 11, false);
        this.f6294m = kVar.mo34621a(12, false);
        this.f6295n = kVar.mo34616a(this.f6295n, 13, false);
        this.f6296o = kVar.mo34618a(this.f6296o, 14, false);
        this.f6297p = kVar.mo34616a(this.f6297p, 15, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [com.tencent.bugly.proguard.v, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [com.tencent.bugly.proguard.u, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i */
    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34601a(this.f6282a, "title");
        iVar.mo34601a(this.f6283b, "newFeature");
        iVar.mo34598a(this.f6284c, "publishTime");
        iVar.mo34597a(this.f6285d, "publishType");
        iVar.mo34599a((C2944m) this.f6286e, "appBasicInfo");
        iVar.mo34599a((C2944m) this.f6287f, "apkBaseInfo");
        iVar.mo34593a(this.f6288g, "updateStrategy");
        iVar.mo34597a(this.f6289h, "popTimes");
        iVar.mo34598a(this.f6290i, "popInterval");
        iVar.mo34599a((C2944m) this.f6291j, "diffApkInfo");
        iVar.mo34601a(this.f6292k, "netType");
        iVar.mo34603a((Map) this.f6293l, "reserved");
        iVar.mo34601a(this.f6294m, "strategyId");
        iVar.mo34597a(this.f6295n, NotificationCompat.CATEGORY_STATUS);
        iVar.mo34598a(this.f6296o, "updateTime");
        iVar.mo34597a(this.f6297p, "updateType");
    }
}
