package com.tencent.bugly.beta.p050ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.global.C2805b;

/* renamed from: com.tencent.bugly.beta.ui.BetaActivity */
/* compiled from: BUGLY */
public class BetaActivity extends FragmentActivity {
    public Runnable onDestroyRunnable = null;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            requestWindowFeature(1);
            if (Beta.dialogFullScreen) {
                getWindow().setFlags(1024, 1024);
            }
            View findViewById = getWindow().getDecorView().findViewById(16908290);
            if (findViewById != null) {
                findViewById.setOnClickListener(new C2805b(1, this, findViewById));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        int intExtra = getIntent().getIntExtra("frag", -1);
        C2820b bVar = C2825g.f5436a.get(Integer.valueOf(intExtra));
        if (bVar != null) {
            getSupportFragmentManager().beginTransaction().add(16908290, bVar).commit();
            C2825g.f5436a.remove(Integer.valueOf(intExtra));
            return;
        }
        finish();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Runnable runnable = this.onDestroyRunnable;
        if (runnable != null) {
            runnable.run();
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        Fragment findFragmentById = getSupportFragmentManager().findFragmentById(16908290);
        boolean z = false;
        try {
            if (findFragmentById instanceof C2820b) {
                z = ((C2820b) findFragmentById).mo34157a(i, keyEvent);
            }
        } catch (Exception unused) {
        }
        if (!z) {
            return super.onKeyDown(i, keyEvent);
        }
        return true;
    }
}
