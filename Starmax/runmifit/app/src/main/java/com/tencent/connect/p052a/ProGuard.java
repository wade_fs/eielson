package com.tencent.connect.p052a;

import android.content.Context;
import android.text.TextUtils;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.Constants;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3122f;
import java.lang.reflect.Method;

/* renamed from: com.tencent.connect.a.a */
public class ProGuard {

    /* renamed from: a */
    private static Class<?> f6304a = null;

    /* renamed from: b */
    private static Class<?> f6305b = null;

    /* renamed from: c */
    private static Method f6306c = null;

    /* renamed from: d */
    private static Method f6307d = null;

    /* renamed from: e */
    private static Method f6308e = null;

    /* renamed from: f */
    private static Method f6309f = null;

    /* renamed from: g */
    private static boolean f6310g = false;

    /* renamed from: a */
    public static boolean m7168a(Context context, QQToken qQToken) {
        return C3122f.m7733a(context, qQToken.getAppId()).mo35170b("Common_ta_enable");
    }

    /* renamed from: b */
    public static void m7169b(Context context, QQToken qQToken) {
        try {
            if (m7168a(context, qQToken)) {
                f6309f.invoke(f6304a, true);
                return;
            }
            f6309f.invoke(f6304a, false);
        } catch (Exception e) {
            C3082f.m7636e("OpenConfig", "checkStatStatus exception: " + e.getStackTrace().toString());
        }
    }

    /* renamed from: c */
    public static void m7170c(Context context, QQToken qQToken) {
        String str = "Aqc" + qQToken.getAppId();
        try {
            f6304a = Class.forName("com.tencent.stat.StatConfig");
            f6305b = Class.forName("com.tencent.stat.StatService");
            f6306c = f6305b.getMethod("reportQQ", Context.class, String.class);
            f6307d = f6305b.getMethod("trackCustomEvent", Context.class, String.class, String[].class);
            f6308e = f6305b.getMethod("commitEvents", Context.class, Integer.TYPE);
            f6309f = f6304a.getMethod("setEnableStatService", Boolean.TYPE);
            m7169b(context, qQToken);
            f6304a.getMethod("setAutoExceptionCaught", Boolean.TYPE).invoke(f6304a, false);
            f6304a.getMethod("setEnableSmartReporting", Boolean.TYPE).invoke(f6304a, true);
            f6304a.getMethod("setSendPeriodMinutes", Integer.TYPE).invoke(f6304a, 1440);
            Class<?> cls = Class.forName("com.tencent.stat.StatReportStrategy");
            f6304a.getMethod("setStatSendStrategy", cls).invoke(f6304a, cls.getField("PERIOD").get(null));
            f6305b.getMethod("startStatService", Context.class, String.class, String.class).invoke(f6305b, context, str, Class.forName("com.tencent.stat.common.StatConstants").getField("VERSION").get(null));
            f6310g = true;
        } catch (Exception e) {
            C3082f.m7636e("OpenConfig", "start4QQConnect exception: " + e.getStackTrace().toString());
        }
    }

    /* renamed from: d */
    public static void m7171d(Context context, QQToken qQToken) {
        if (!TextUtils.isEmpty(qQToken.getOpenId())) {
            C3091d.m7665a().mo35123a(qQToken.getOpenId(), qQToken.getAppId(), "2", "1", Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE, "0", "0", "0");
        }
        if (f6310g) {
            m7169b(context, qQToken);
            if (qQToken.getOpenId() != null) {
                try {
                    f6306c.invoke(f6305b, context, qQToken.getOpenId());
                } catch (Exception e) {
                    C3082f.m7636e("OpenConfig", "reportQQ exception: " + e.getStackTrace().toString());
                }
            }
        }
    }

    /* renamed from: a */
    public static void m7167a(Context context, QQToken qQToken, String str, String... strArr) {
        if (f6310g) {
            m7169b(context, qQToken);
            try {
                f6307d.invoke(f6305b, context, str, strArr);
            } catch (Exception e) {
                C3082f.m7636e("OpenConfig", "trackCustomEvent exception: " + e.getStackTrace().toString());
            }
        }
    }
}
