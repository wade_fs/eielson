package com.tencent.bugly.crashreport.crash;

/* renamed from: com.tencent.bugly.crashreport.crash.a */
/* compiled from: BUGLY */
public class C2858a implements Comparable<C2858a> {

    /* renamed from: a */
    public long f5778a = -1;

    /* renamed from: b */
    public long f5779b = -1;

    /* renamed from: c */
    public String f5780c = null;

    /* renamed from: d */
    public boolean f5781d = false;

    /* renamed from: e */
    public boolean f5782e = false;

    /* renamed from: f */
    public int f5783f = 0;

    /* renamed from: a */
    public int compareTo(C2858a aVar) {
        if (aVar == null) {
            return 1;
        }
        long j = this.f5779b - aVar.f5779b;
        if (j > 0) {
            return 1;
        }
        return j < 0 ? -1 : 0;
    }
}
