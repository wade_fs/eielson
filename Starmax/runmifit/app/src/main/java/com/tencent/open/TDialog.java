package com.tencent.open;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.google.android.gms.common.util.CrashUtils;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.Constants;
import com.tencent.open.C3070a;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3094g;
import com.tencent.open.p061c.C3109b;
import com.tencent.open.utils.C3124g;
import com.tencent.open.utils.C3131k;
import com.tencent.tauth.AuthActivity;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class TDialog extends C3086b {

    /* renamed from: c */
    static final FrameLayout.LayoutParams f6678c = new FrameLayout.LayoutParams(-1, -1);

    /* renamed from: d */
    static Toast f6679d = null;

    /* renamed from: f */
    private static WeakReference<ProgressDialog> f6680f;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public WeakReference<Context> f6681e;

    /* renamed from: g */
    private String f6682g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public OnTimeListener f6683h;

    /* renamed from: i */
    private IUiListener f6684i;

    /* renamed from: j */
    private FrameLayout f6685j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public C3109b f6686k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public Handler f6687l;

    /* renamed from: m */
    private boolean f6688m = false;

    /* renamed from: n */
    private QQToken f6689n = null;

    /* compiled from: ProGuard */
    private class THandler extends Handler {

        /* renamed from: b */
        private OnTimeListener f6698b;

        public THandler(OnTimeListener onTimeListener, Looper looper) {
            super(looper);
            this.f6698b = onTimeListener;
        }

        public void handleMessage(Message message) {
            C3082f.m7631b("openSDK_LOG.TDialog", "--handleMessage--msg.WHAT = " + message.what);
            int i = message.what;
            if (i == 1) {
                this.f6698b.m7580a((String) message.obj);
            } else if (i == 2) {
                this.f6698b.onCancel();
            } else if (i != 3) {
                if (i != 4 && i == 5 && TDialog.this.f6681e != null && TDialog.this.f6681e.get() != null) {
                    TDialog.m7577d((Context) TDialog.this.f6681e.get(), (String) message.obj);
                }
            } else if (TDialog.this.f6681e != null && TDialog.this.f6681e.get() != null) {
                TDialog.m7575c((Context) TDialog.this.f6681e.get(), (String) message.obj);
            }
        }
    }

    /* compiled from: ProGuard */
    private static class OnTimeListener implements IUiListener {

        /* renamed from: a */
        String f6692a;

        /* renamed from: b */
        String f6693b;

        /* renamed from: c */
        private WeakReference<Context> f6694c;

        /* renamed from: d */
        private String f6695d;

        /* renamed from: e */
        private IUiListener f6696e;

        public OnTimeListener(Context context, String str, String str2, String str3, IUiListener iUiListener) {
            this.f6694c = new WeakReference<>(context);
            this.f6695d = str;
            this.f6692a = str2;
            this.f6693b = str3;
            this.f6696e = iUiListener;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m7580a(String str) {
            try {
                onComplete(C3131k.m7788d(str));
            } catch (JSONException e) {
                e.printStackTrace();
                onError(new UiError(-4, Constants.MSG_JSON_ERROR, str));
            }
        }

        public void onComplete(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            C3094g a = C3094g.m7676a();
            a.mo35133a(this.f6695d + "_H5", SystemClock.elapsedRealtime(), 0, 0, jSONObject.optInt("ret", -6), this.f6692a, false);
            IUiListener iUiListener = this.f6696e;
            if (iUiListener != null) {
                iUiListener.onComplete(jSONObject);
                this.f6696e = null;
            }
        }

        public void onError(UiError uiError) {
            String str;
            if (uiError.errorMessage != null) {
                str = uiError.errorMessage + this.f6692a;
            } else {
                str = this.f6692a;
            }
            C3094g a = C3094g.m7676a();
            a.mo35133a(this.f6695d + "_H5", SystemClock.elapsedRealtime(), 0, 0, uiError.errorCode, str, false);
            IUiListener iUiListener = this.f6696e;
            if (iUiListener != null) {
                iUiListener.onError(uiError);
                this.f6696e = null;
            }
        }

        public void onCancel() {
            IUiListener iUiListener = this.f6696e;
            if (iUiListener != null) {
                iUiListener.onCancel();
                this.f6696e = null;
            }
        }
    }

    public TDialog(Context context, String str, String str2, IUiListener iUiListener, QQToken qQToken) {
        super(context, 16973840);
        this.f6681e = new WeakReference<>(context);
        this.f6682g = str2;
        this.f6683h = new OnTimeListener(context, str, str2, qQToken.getAppId(), iUiListener);
        this.f6687l = new THandler(this.f6683h, context.getMainLooper());
        this.f6684i = iUiListener;
        this.f6689n = qQToken;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        m7569a();
        m7572b();
    }

    public void onBackPressed() {
        OnTimeListener onTimeListener = this.f6683h;
        if (onTimeListener != null) {
            onTimeListener.onCancel();
        }
        super.onBackPressed();
    }

    /* renamed from: a */
    private void m7569a() {
        new TextView(this.f6681e.get()).setText("test");
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.f6686k = new C3109b(this.f6681e.get());
        this.f6686k.setLayoutParams(layoutParams);
        this.f6685j = new FrameLayout(this.f6681e.get());
        layoutParams.gravity = 17;
        this.f6685j.setLayoutParams(layoutParams);
        this.f6685j.addView(this.f6686k);
        setContentView(this.f6685j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35050a(String str) {
        C3082f.m7631b("openSDK_LOG.TDialog", "--onConsoleMessage--");
        try {
            this.f6752a.mo35069a(this.f6686k, str);
        } catch (Exception unused) {
        }
    }

    /* renamed from: b */
    private void m7572b() {
        this.f6686k.setVerticalScrollBarEnabled(false);
        this.f6686k.setHorizontalScrollBarEnabled(false);
        this.f6686k.setWebViewClient(new FbWebViewClient());
        this.f6686k.setWebChromeClient(this.f6753b);
        this.f6686k.clearFormData();
        WebSettings settings = this.f6686k.getSettings();
        if (settings != null) {
            settings.setSavePassword(false);
            settings.setSaveFormData(false);
            settings.setCacheMode(-1);
            settings.setNeedInitialFocus(false);
            settings.setBuiltInZoomControls(true);
            settings.setSupportZoom(true);
            settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            settings.setJavaScriptEnabled(true);
            WeakReference<Context> weakReference = this.f6681e;
            if (!(weakReference == null || weakReference.get() == null)) {
                settings.setDatabaseEnabled(true);
                settings.setDatabasePath(this.f6681e.get().getApplicationContext().getDir("databases", 0).getPath());
            }
            settings.setDomStorageEnabled(true);
            this.f6752a.mo35067a(new JsListener(), "sdk_js_if");
            this.f6686k.loadUrl(this.f6682g);
            this.f6686k.setLayoutParams(f6678c);
            this.f6686k.setVisibility(View.INVISIBLE);
            this.f6686k.getSettings().setSavePassword(false);
        }
    }

    /* compiled from: ProGuard */
    private class JsListener extends C3070a.C3072b {
        private JsListener() {
        }

        public void onAddShare(String str) {
            C3082f.m7631b("openSDK_LOG.TDialog", "JsListener onAddShare");
            onComplete(str);
        }

        public void onInvite(String str) {
            onComplete(str);
        }

        public void onCancelAddShare(String str) {
            C3082f.m7636e("openSDK_LOG.TDialog", "JsListener onCancelAddShare" + str);
            onCancel("cancel");
        }

        public void onCancelLogin() {
            onCancel("");
        }

        public void onCancelInvite() {
            C3082f.m7636e("openSDK_LOG.TDialog", "JsListener onCancelInvite");
            onCancel("");
        }

        public void onComplete(String str) {
            TDialog.this.f6687l.obtainMessage(1, str).sendToTarget();
            C3082f.m7636e("openSDK_LOG.TDialog", "JsListener onComplete" + str);
            TDialog.this.dismiss();
        }

        public void onCancel(String str) {
            C3082f.m7636e("openSDK_LOG.TDialog", "JsListener onCancel --msg = " + str);
            TDialog.this.f6687l.obtainMessage(2, str).sendToTarget();
            TDialog.this.dismiss();
        }

        public void showMsg(String str) {
            TDialog.this.f6687l.obtainMessage(3, str).sendToTarget();
        }

        public void onLoad(String str) {
            TDialog.this.f6687l.obtainMessage(4, str).sendToTarget();
        }
    }

    /* compiled from: ProGuard */
    private class FbWebViewClient extends WebViewClient {
        private FbWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Uri uri;
            C3082f.m7628a("openSDK_LOG.TDialog", "Redirect URL: " + str);
            if (str.startsWith(C3124g.m7744a().mo35172a((Context) TDialog.this.f6681e.get(), "auth://tauth.qq.com/"))) {
                TDialog.this.f6683h.onComplete(C3131k.m7786c(str));
                if (TDialog.this.isShowing()) {
                    TDialog.this.dismiss();
                }
                return true;
            } else if (str.startsWith(Constants.CANCEL_URI)) {
                TDialog.this.f6683h.onCancel();
                if (TDialog.this.isShowing()) {
                    TDialog.this.dismiss();
                }
                return true;
            } else if (str.startsWith(Constants.CLOSE_URI)) {
                if (TDialog.this.isShowing()) {
                    TDialog.this.dismiss();
                }
                return true;
            } else if (str.startsWith(Constants.DOWNLOAD_URI) || str.endsWith(".apk")) {
                try {
                    if (str.startsWith(Constants.DOWNLOAD_URI)) {
                        uri = Uri.parse(Uri.decode(str.substring(11)));
                    } else {
                        uri = Uri.parse(Uri.decode(str));
                    }
                    Intent intent = new Intent("android.intent.action.VIEW", uri);
                    intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
                    if (!(TDialog.this.f6681e == null || TDialog.this.f6681e.get() == null)) {
                        ((Context) TDialog.this.f6681e.get()).startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return true;
            } else if (str.startsWith("auth://progress")) {
                return true;
            } else {
                return false;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            TDialog.this.f6683h.onError(new UiError(i, str, str2));
            if (!(TDialog.this.f6681e == null || TDialog.this.f6681e.get() == null)) {
                Toast.makeText((Context) TDialog.this.f6681e.get(), "网络连接异常或系统错误", 0).show();
            }
            TDialog.this.dismiss();
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            C3082f.m7628a("openSDK_LOG.TDialog", "Webview loading URL: " + str);
            super.onPageStarted(webView, str, bitmap);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            TDialog.this.f6686k.setVisibility(View.VISIBLE);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m7575c(Context context, String str) {
        try {
            JSONObject d = C3131k.m7788d(str);
            int i = d.getInt(SocialConstants.PARAM_TYPE);
            String string = d.getString("msg");
            if (i == 0) {
                if (f6679d == null) {
                    f6679d = Toast.makeText(context, string, 0);
                } else {
                    f6679d.setView(f6679d.getView());
                    f6679d.setText(string);
                    f6679d.setDuration(0);
                }
                f6679d.show();
            } else if (i == 1) {
                if (f6679d == null) {
                    f6679d = Toast.makeText(context, string, 1);
                } else {
                    f6679d.setView(f6679d.getView());
                    f6679d.setText(string);
                    f6679d.setDuration(1);
                }
                f6679d.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static void m7577d(Context context, String str) {
        if (context != null && str != null) {
            try {
                JSONObject d = C3131k.m7788d(str);
                int i = d.getInt(AuthActivity.ACTION_KEY);
                String string = d.getString("msg");
                if (i == 1) {
                    if (f6680f != null) {
                        if (f6680f.get() != null) {
                            f6680f.get().setMessage(string);
                            if (!f6680f.get().isShowing()) {
                                f6680f.get().show();
                                return;
                            }
                            return;
                        }
                    }
                    ProgressDialog progressDialog = new ProgressDialog(context);
                    progressDialog.setMessage(string);
                    f6680f = new WeakReference<>(progressDialog);
                    progressDialog.show();
                } else if (i == 0 && f6680f != null && f6680f.get() != null && f6680f.get().isShowing()) {
                    f6680f.get().dismiss();
                    f6680f = null;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
