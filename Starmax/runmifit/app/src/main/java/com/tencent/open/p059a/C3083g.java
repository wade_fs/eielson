package com.tencent.open.p059a;

import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.tencent.open.a.g */
/* compiled from: ProGuard */
public class C3083g implements Iterable<String> {

    /* renamed from: a */
    private ConcurrentLinkedQueue<String> f6746a;

    /* renamed from: b */
    private AtomicInteger f6747b;

    public C3083g() {
        this.f6746a = null;
        this.f6747b = null;
        this.f6746a = new ConcurrentLinkedQueue<>();
        this.f6747b = new AtomicInteger(0);
    }

    /* renamed from: a */
    public int mo35107a(String str) {
        int length = str.length();
        this.f6746a.add(str);
        return this.f6747b.addAndGet(length);
    }

    /* renamed from: a */
    public void mo35108a(Writer writer, char[] cArr) throws IOException {
        if (writer != null && cArr != null && cArr.length != 0) {
            int length = cArr.length;
            Iterator<String> it = iterator();
            int i = length;
            int i2 = 0;
            while (it.hasNext()) {
                String next = it.next();
                int length2 = next.length();
                int i3 = 0;
                while (length2 > 0) {
                    int i4 = i > length2 ? length2 : i;
                    int i5 = i3 + i4;
                    next.getChars(i3, i5, cArr, i2);
                    i -= i4;
                    i2 += i4;
                    length2 -= i4;
                    if (i == 0) {
                        writer.write(cArr, 0, length);
                        i = length;
                        i3 = i5;
                        i2 = 0;
                    } else {
                        i3 = i5;
                    }
                }
            }
            if (i2 > 0) {
                writer.write(cArr, 0, i2);
            }
            writer.flush();
        }
    }

    /* renamed from: a */
    public int mo35106a() {
        return this.f6747b.get();
    }

    /* renamed from: b */
    public void mo35109b() {
        this.f6746a.clear();
        this.f6747b.set(0);
    }

    public Iterator<String> iterator() {
        return this.f6746a.iterator();
    }
}
