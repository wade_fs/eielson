package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatAccount;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.a */
public class C3236a extends Event {

    /* renamed from: a */
    private StatAccount f7360a = null;

    public C3236a(Context context, int i, StatAccount statAccount, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7360a = statAccount;
    }

    public EventType getType() {
        return EventType.ADDITION;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        Util.jsonPut(jSONObject, "qq", this.f7360a.getAccount());
        jSONObject.put("acc", this.f7360a.toJsonString());
        return true;
    }
}
