package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.C3204a;
import com.tencent.stat.common.StatCommonHelper;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.n */
public class C3252n extends Event {

    /* renamed from: t */
    private static int f7394t = 1;

    /* renamed from: a */
    private C3204a f7395a;

    /* renamed from: s */
    private JSONObject f7396s = null;

    public C3252n(Context context, int i, JSONObject jSONObject, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7395a = new C3204a(context);
        this.f7396s = jSONObject;
    }

    public EventType getType() {
        return EventType.SESSION_ENV;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        int i = f7394t;
        if (i == 1) {
            jSONObject.put("hs", i);
            f7394t = 0;
        }
        if (super.f7348h != null) {
            jSONObject.put("ut", super.f7348h.getUserType());
        }
        JSONObject jSONObject2 = this.f7396s;
        if (jSONObject2 != null) {
            jSONObject.put("cfg", jSONObject2);
        }
        if (StatCommonHelper.needCheckTime(this.f7355q)) {
            jSONObject.put("ncts", 1);
        }
        this.f7395a.mo35400a(jSONObject, (Thread) null);
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35467a(JSONObject jSONObject) {
        try {
            jSONObject.put("app", StatCommonHelper.getAppList(this.f7355q));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
