package com.tencent.mid.api;

import android.content.Context;
import android.content.SharedPreferences;

/* renamed from: com.tencent.mid.api.a */
public class C3024a {

    /* renamed from: a */
    private static C3024a f6539a;

    /* renamed from: b */
    private Context f6540b = null;

    /* renamed from: c */
    private SharedPreferences f6541c = null;

    /* renamed from: d */
    private String f6542d = "__QQ_MID_STR__";

    /* renamed from: a */
    public SharedPreferences mo34874a() {
        return this.f6541c;
    }

    private C3024a(Context context) {
        this.f6540b = context.getApplicationContext();
        Context context2 = this.f6540b;
        this.f6541c = context2.getSharedPreferences(this.f6540b.getPackageName() + ".mid.world.ro", 0);
    }

    /* renamed from: a */
    public void mo34875a(String str) {
        if (str == null || !str.equals(mo34876b())) {
            this.f6541c.edit().putString(this.f6542d, str).commit();
        }
    }

    /* renamed from: b */
    public String mo34876b() {
        return this.f6541c.getString(this.f6542d, null);
    }

    /* renamed from: a */
    public static C3024a m7384a(Context context) {
        if (f6539a == null) {
            synchronized (C3024a.class) {
                if (f6539a == null) {
                    f6539a = new C3024a(context);
                }
            }
        }
        return f6539a;
    }
}
