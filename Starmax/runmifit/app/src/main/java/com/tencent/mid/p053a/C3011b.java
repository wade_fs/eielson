package com.tencent.mid.p053a;

import com.google.common.net.HttpHeaders;
import com.tamic.novate.download.MimeType;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.C3040f;
import com.tencent.mid.util.Util;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import no.nordicsemi.android.dfu.BuildConfig;

/* renamed from: com.tencent.mid.a.b */
public class C3011b {

    /* renamed from: a */
    private HttpHost f6501a;

    /* renamed from: b */
    private DefaultHttpClient f6502b;

    /* renamed from: c */
    private String f6503c;

    /* renamed from: d */
    private Map<String, String> f6504d;

    /* renamed from: e */
    private C3038d f6505e;

    /* renamed from: f */
    private int f6506f;

    /* renamed from: a */
    public C3014d mo34836a(String str, byte[] bArr, String str2, int i) {
        String a = mo34837a(str);
        C3038d dVar = this.f6505e;
        dVar.mo34950b("[" + a + "]Send request(" + bArr.length + "bytes):" + bArr);
        HttpPost httpPost = new HttpPost(a);
        httpPost.setHeader(HttpHeaders.CONNECTION, "Keep-Alive");
        httpPost.removeHeaders(HttpHeaders.CACHE_CONTROL);
        httpPost.removeHeaders(HttpHeaders.USER_AGENT);
        if (this.f6501a != null) {
            httpPost.addHeader("X-Online-Host", this.f6503c);
            httpPost.addHeader(HttpHeaders.ACCEPT, "*/*");
            httpPost.addHeader(HttpHeaders.CONTENT_TYPE, MimeType.JSON);
        } else {
            this.f6502b.getParams().removeParameter("http.route.default-proxy");
        }
        if (this.f6501a == null) {
            httpPost.addHeader(HttpHeaders.CONTENT_ENCODING, str2);
        } else {
            httpPost.addHeader("X-Content-Encoding", str2);
        }
        httpPost.setEntity(new ByteArrayEntity(bArr));
        HttpResponse execute = this.f6502b.execute(httpPost);
        HttpEntity entity = execute.getEntity();
        int statusCode = execute.getStatusLine().getStatusCode();
        long contentLength = entity.getContentLength();
        C3038d dVar2 = this.f6505e;
        dVar2.mo34950b("recv response status code:" + statusCode + ", content length:" + contentLength);
        byte[] byteArray = EntityUtils.toByteArray(entity);
        String str3 = "";
        Header firstHeader = execute.getFirstHeader(HttpHeaders.CONTENT_ENCODING);
        if (firstHeader != null) {
            if (firstHeader.getValue().toUpperCase().contains("AES")) {
                str3 = new String(C3013c.m7347a(C3013c.m7346a()).mo34840a(i).mo34934b(byteArray), "UTF-8");
            }
            if (firstHeader.getValue().toUpperCase().contains("RSA")) {
                str3 = C3040f.m7508b(byteArray);
            }
            if (firstHeader.getValue().toUpperCase().contains("IDENTITY")) {
                str3 = new String(byteArray, "UTF-8");
            }
        }
        C3038d dVar3 = this.f6505e;
        dVar3.mo34950b("recv response status code:" + statusCode + ", content :" + str3);
        return new C3014d(statusCode, str3);
    }

    public C3011b(String str, Map<String, String> map) {
        this.f6501a = null;
        this.f6502b = null;
        this.f6503c = null;
        this.f6504d = null;
        this.f6505e = null;
        this.f6506f = 30000;
        this.f6505e = Util.getLogger();
        this.f6501a = Util.getHttpProxy();
        BasicHttpParams basicHttpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(basicHttpParams, this.f6506f);
        HttpConnectionParams.setSoTimeout(basicHttpParams, this.f6506f);
        this.f6502b = new DefaultHttpClient(basicHttpParams);
        C3038d dVar = this.f6505e;
        StringBuilder sb = new StringBuilder();
        sb.append("proxy==");
        HttpHost httpHost = this.f6501a;
        sb.append(httpHost == null ? "null" : httpHost.getHostName());
        dVar.mo34950b(sb.toString());
        if (this.f6501a != null) {
            this.f6502b.getParams().setParameter("http.route.default-proxy", this.f6501a);
        }
        HttpHost httpHost2 = this.f6501a;
        if (httpHost2 != null && httpHost2.getHostName().equals("10.0.0.200")) {
            this.f6502b.getCredentialsProvider().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("ctwap@mycdma.cn", "vnet.mobi"));
        }
        Logger.getLogger("org.apache.http.wire").setLevel(Level.FINEST);
        Logger.getLogger("org.apache.http.headers").setLevel(Level.FINEST);
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.SimpleLog");
        System.setProperty("org.apache.commons.logging.simplelog.showdatetime", "true");
        System.setProperty("org.apache.commons.logging.simplelog.log.httpclient.wire", BuildConfig.BUILD_TYPE);
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http", BuildConfig.BUILD_TYPE);
        System.setProperty("org.apache.commons.logging.simplelog.log.org.apache.http.headers", BuildConfig.BUILD_TYPE);
        this.f6502b.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy() {
            /* class com.tencent.mid.p053a.C3011b.C30121 */

            public long getKeepAliveDuration(HttpResponse httpResponse, HttpContext httpContext) {
                long keepAliveDuration = C3011b.super.getKeepAliveDuration(httpResponse, httpContext);
                if (keepAliveDuration == -1) {
                    return 20000;
                }
                return keepAliveDuration;
            }
        });
        this.f6503c = str;
        this.f6504d = map;
    }

    /* renamed from: a */
    public void mo34838a() {
        DefaultHttpClient defaultHttpClient = this.f6502b;
        if (defaultHttpClient != null) {
            defaultHttpClient.getConnectionManager().shutdown();
            this.f6502b = null;
            this.f6503c = null;
            this.f6504d = null;
            this.f6501a = null;
        }
    }

    /* renamed from: b */
    private String m7342b() {
        StringBuilder sb = new StringBuilder();
        Map<String, String> map = this.f6504d;
        if (!(map == null || map.size() == 0)) {
            int i = 0;
            for (Map.Entry entry : this.f6504d.entrySet()) {
                String str = i == 0 ? "?" : "&";
                i++;
                sb.append(str);
                sb.append((String) entry.getKey());
                sb.append("=");
                sb.append((String) entry.getValue());
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    public String mo34837a(String str) {
        return this.f6503c + str + m7342b();
    }
}
