package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatSpecifyReportedInfo;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.d */
public class C3240d extends Event {

    /* renamed from: a */
    private JSONObject f7367a = null;

    /* renamed from: a */
    public void mo35467a(JSONObject jSONObject) {
        this.f7367a = jSONObject;
    }

    public C3240d(Context context, int i, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
    }

    public EventType getType() {
        return EventType.CUSTOM_PROPERTY;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        JSONObject jSONObject2 = this.f7367a;
        if (jSONObject2 == null) {
            return false;
        }
        jSONObject.put("cp", jSONObject2);
        return false;
    }
}
