package com.tencent.bugly.beta.download;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/* compiled from: BUGLY */
public abstract class DownloadTask {
    public static final int COMPLETE = 1;
    public static final int DELETED = 4;
    public static final int DOWNLOADING = 2;
    public static final int FAILED = 5;
    public static final int INIT = 0;
    public static final int PAUSED = 3;
    public static final int TYPE_HOTFIX = 2;
    public static final int TYPE_UPGRADE = 1;

    /* renamed from: a */
    protected String f5311a;

    /* renamed from: b */
    protected String f5312b;

    /* renamed from: c */
    protected String f5313c;

    /* renamed from: d */
    protected List<DownloadListener> f5314d = new CopyOnWriteArrayList();

    /* renamed from: e */
    protected long f5315e;

    /* renamed from: f */
    protected long f5316f;

    /* renamed from: g */
    protected boolean f5317g = true;

    /* renamed from: h */
    protected String f5318h = "";

    /* renamed from: i */
    protected int f5319i = 0;

    /* renamed from: j */
    protected int f5320j = 1;

    public abstract void delete(boolean z);

    public abstract void download();

    public abstract long getCostTime();

    public abstract File getSaveFile();

    public abstract int getStatus();

    public abstract void stop();

    protected DownloadTask(String str, String str2, String str3, String str4) {
        this.f5311a = str;
        this.f5312b = str2;
        this.f5313c = str3;
        this.f5318h = str4;
    }

    public long getTotalLength() {
        return this.f5316f;
    }

    public void setTotalLength(long j) {
        this.f5316f = j;
    }

    public long getSavedLength() {
        return this.f5315e;
    }

    public void setSavedLength(long j) {
        this.f5315e = j;
    }

    public String getDownloadUrl() {
        return this.f5311a;
    }

    public void setNeededNotify(boolean z) {
        this.f5317g = z;
    }

    public String getMD5() {
        return this.f5318h;
    }

    public int getDownloadType() {
        return this.f5320j;
    }

    public void setDownloadType(int i) {
        this.f5320j = i;
    }

    public void addListener(DownloadListener downloadListener) {
        if (downloadListener != null && !this.f5314d.contains(downloadListener)) {
            this.f5314d.add(downloadListener);
        }
    }

    public boolean removeListener(DownloadListener downloadListener) {
        return downloadListener != null && this.f5314d.remove(downloadListener);
    }

    public boolean isNeededNotify() {
        return this.f5317g;
    }
}
