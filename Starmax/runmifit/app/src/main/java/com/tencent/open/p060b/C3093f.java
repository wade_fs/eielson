package com.tencent.open.p060b;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tencent.open.utils.C3121e;

/* renamed from: com.tencent.open.b.f */
/* compiled from: ProGuard */
public class C3093f extends SQLiteOpenHelper {

    /* renamed from: a */
    protected static final String[] f6763a = {"key"};

    /* renamed from: b */
    protected static C3093f f6764b;

    /* renamed from: a */
    public static synchronized C3093f m7672a() {
        C3093f fVar;
        synchronized (C3093f.class) {
            if (f6764b == null) {
                f6764b = new C3093f(C3121e.m7727a());
            }
            fVar = f6764b;
        }
        return fVar;
    }

    public C3093f(Context context) {
        super(context, "sdk_report.db", (SQLiteDatabase.CursorFactory) null, 2);
    }

    public void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS via_cgi_report( _id INTEGER PRIMARY KEY,key TEXT,type TEXT,blob BLOB);");
    }

    public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        sQLiteDatabase.execSQL("DROP TABLE IF EXISTS via_cgi_report");
        onCreate(sQLiteDatabase);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(11:19|20|21|22|23|24|25|26|27|(2:50|51)|52) */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:29|(0)|35|36|37|38) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:42|(0)|45|46|47|(0)|52|(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0089, code lost:
        if (r1 == null) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x008b, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x009f, code lost:
        if (r1 != null) goto L_0x008b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:73:0x00a3, code lost:
        return r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x0056 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x0065 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x0068 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x006f */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0062 A[SYNTHETIC, Splitter:B:33:0x0062] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x006c A[SYNTHETIC, Splitter:B:43:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0075 A[SYNTHETIC, Splitter:B:50:0x0075] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0086 A[SYNTHETIC, Splitter:B:58:0x0086] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00a6 A[SYNTHETIC, Splitter:B:75:0x00a6] */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00ab  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0084 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<java.io.Serializable> mo35125a(java.lang.String r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x00af }
            r0.<init>()     // Catch:{ all -> 0x00af }
            java.util.List r0 = java.util.Collections.synchronizedList(r0)     // Catch:{ all -> 0x00af }
            boolean r1 = android.text.TextUtils.isEmpty(r12)     // Catch:{ all -> 0x00af }
            if (r1 == 0) goto L_0x0012
            monitor-exit(r11)
            return r0
        L_0x0012:
            android.database.sqlite.SQLiteDatabase r1 = r11.getReadableDatabase()     // Catch:{ all -> 0x00af }
            if (r1 != 0) goto L_0x001a
            monitor-exit(r11)
            return r0
        L_0x001a:
            r10 = 0
            java.lang.String r3 = "via_cgi_report"
            r4 = 0
            java.lang.String r5 = "type = ?"
            r2 = 1
            java.lang.String[] r6 = new java.lang.String[r2]     // Catch:{ Exception -> 0x0092 }
            r2 = 0
            r6[r2] = r12     // Catch:{ Exception -> 0x0092 }
            r7 = 0
            r8 = 0
            r9 = 0
            r2 = r1
            android.database.Cursor r12 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ Exception -> 0x0092 }
            if (r12 == 0) goto L_0x0084
            int r2 = r12.getCount()     // Catch:{ Exception -> 0x0081, all -> 0x007f }
            if (r2 <= 0) goto L_0x0084
            r12.moveToFirst()     // Catch:{ Exception -> 0x0081, all -> 0x007f }
        L_0x0039:
            java.lang.String r2 = "blob"
            int r2 = r12.getColumnIndex(r2)     // Catch:{ Exception -> 0x0081, all -> 0x007f }
            byte[] r2 = r12.getBlob(r2)     // Catch:{ Exception -> 0x0081, all -> 0x007f }
            java.io.ByteArrayInputStream r3 = new java.io.ByteArrayInputStream     // Catch:{ Exception -> 0x0081, all -> 0x007f }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0081, all -> 0x007f }
            java.io.ObjectInputStream r2 = new java.io.ObjectInputStream     // Catch:{ Exception -> 0x0069, all -> 0x005e }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0069, all -> 0x005e }
            java.lang.Object r4 = r2.readObject()     // Catch:{ Exception -> 0x006a, all -> 0x005c }
            java.io.Serializable r4 = (java.io.Serializable) r4     // Catch:{ Exception -> 0x006a, all -> 0x005c }
            r2.close()     // Catch:{ IOException -> 0x0056 }
        L_0x0056:
            r3.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x0073
        L_0x005a:
            goto L_0x0073
        L_0x005c:
            r4 = move-exception
            goto L_0x0060
        L_0x005e:
            r4 = move-exception
            r2 = r10
        L_0x0060:
            if (r2 == 0) goto L_0x0065
            r2.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0065:
            r3.close()     // Catch:{ IOException -> 0x0068 }
        L_0x0068:
            throw r4     // Catch:{ Exception -> 0x0081, all -> 0x007f }
        L_0x0069:
            r2 = r10
        L_0x006a:
            if (r2 == 0) goto L_0x006f
            r2.close()     // Catch:{ IOException -> 0x006f }
        L_0x006f:
            r3.close()     // Catch:{ IOException -> 0x0072 }
        L_0x0072:
            r4 = r10
        L_0x0073:
            if (r4 == 0) goto L_0x0078
            r0.add(r4)     // Catch:{ Exception -> 0x0081, all -> 0x007f }
        L_0x0078:
            boolean r2 = r12.moveToNext()     // Catch:{ Exception -> 0x0081, all -> 0x007f }
            if (r2 != 0) goto L_0x0039
            goto L_0x0084
        L_0x007f:
            r0 = move-exception
            goto L_0x00a4
        L_0x0081:
            r2 = move-exception
            r10 = r12
            goto L_0x0093
        L_0x0084:
            if (r12 == 0) goto L_0x0089
            r12.close()     // Catch:{ all -> 0x00af }
        L_0x0089:
            if (r1 == 0) goto L_0x00a2
        L_0x008b:
            r1.close()     // Catch:{ all -> 0x00af }
            goto L_0x00a2
        L_0x008f:
            r0 = move-exception
            r12 = r10
            goto L_0x00a4
        L_0x0092:
            r2 = move-exception
        L_0x0093:
            java.lang.String r12 = "openSDK_LOG.ReportDatabaseHelper"
            java.lang.String r3 = "getReportItemFromDB has exception."
            com.tencent.open.p059a.C3082f.m7632b(r12, r3, r2)     // Catch:{ all -> 0x008f }
            if (r10 == 0) goto L_0x009f
            r10.close()     // Catch:{ all -> 0x00af }
        L_0x009f:
            if (r1 == 0) goto L_0x00a2
            goto L_0x008b
        L_0x00a2:
            monitor-exit(r11)
            return r0
        L_0x00a4:
            if (r12 == 0) goto L_0x00a9
            r12.close()     // Catch:{ all -> 0x00af }
        L_0x00a9:
            if (r1 == 0) goto L_0x00ae
            r1.close()     // Catch:{ all -> 0x00af }
        L_0x00ae:
            throw r0     // Catch:{ all -> 0x00af }
        L_0x00af:
            r12 = move-exception
            monitor-exit(r11)
            goto L_0x00b3
        L_0x00b2:
            throw r12
        L_0x00b3:
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3093f.mo35125a(java.lang.String):java.util.List");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:36|37|(0)|42|43|44|45) */
    /* JADX WARNING: Can't wrap try/catch for region: R(9:26|27|(6:28|29|30|31|32|33)|34|35|52|54|55|81) */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x0080, code lost:
        if (r1 != null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x0082, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0092, code lost:
        if (r1 == null) goto L_0x0095;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x0096, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x004e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x005b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:44:0x005e */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0058 A[SYNTHETIC, Splitter:B:40:0x0058] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0062 A[SYNTHETIC, Splitter:B:50:0x0062] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo35126a(java.lang.String r9, java.util.List<java.io.Serializable> r10) {
        /*
            r8 = this;
            monitor-enter(r8)
            int r0 = r10.size()     // Catch:{ all -> 0x00a0 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r8)
            return
        L_0x0009:
            r1 = 20
            if (r0 > r1) goto L_0x000e
            goto L_0x0010
        L_0x000e:
            r0 = 20
        L_0x0010:
            boolean r1 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x0018
            monitor-exit(r8)
            return
        L_0x0018:
            r8.mo35127b(r9)     // Catch:{ all -> 0x00a0 }
            android.database.sqlite.SQLiteDatabase r1 = r8.getWritableDatabase()     // Catch:{ all -> 0x00a0 }
            if (r1 != 0) goto L_0x0023
            monitor-exit(r8)
            return
        L_0x0023:
            r1.beginTransaction()     // Catch:{ all -> 0x00a0 }
            android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ Exception -> 0x0088 }
            r2.<init>()     // Catch:{ Exception -> 0x0088 }
            r3 = 0
        L_0x002c:
            if (r3 >= r0) goto L_0x007a
            java.lang.Object r4 = r10.get(r3)     // Catch:{ Exception -> 0x0088 }
            java.io.Serializable r4 = (java.io.Serializable) r4     // Catch:{ Exception -> 0x0088 }
            if (r4 == 0) goto L_0x0074
            java.lang.String r5 = "type"
            r2.put(r5, r9)     // Catch:{ Exception -> 0x0088 }
            java.io.ByteArrayOutputStream r5 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0088 }
            r6 = 512(0x200, float:7.175E-43)
            r5.<init>(r6)     // Catch:{ Exception -> 0x0088 }
            r6 = 0
            java.io.ObjectOutputStream r7 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x005f, all -> 0x0055 }
            r7.<init>(r5)     // Catch:{ IOException -> 0x005f, all -> 0x0055 }
            r7.writeObject(r4)     // Catch:{ IOException -> 0x0060, all -> 0x0052 }
            r7.close()     // Catch:{ IOException -> 0x004e }
        L_0x004e:
            r5.close()     // Catch:{ IOException -> 0x0066 }
            goto L_0x0066
        L_0x0052:
            r9 = move-exception
            r6 = r7
            goto L_0x0056
        L_0x0055:
            r9 = move-exception
        L_0x0056:
            if (r6 == 0) goto L_0x005b
            r6.close()     // Catch:{ IOException -> 0x005b }
        L_0x005b:
            r5.close()     // Catch:{ IOException -> 0x005e }
        L_0x005e:
            throw r9     // Catch:{ Exception -> 0x0088 }
        L_0x005f:
            r7 = r6
        L_0x0060:
            if (r7 == 0) goto L_0x004e
            r7.close()     // Catch:{ IOException -> 0x004e }
            goto L_0x004e
        L_0x0066:
            java.lang.String r4 = "blob"
            byte[] r5 = r5.toByteArray()     // Catch:{ Exception -> 0x0088 }
            r2.put(r4, r5)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r4 = "via_cgi_report"
            r1.insert(r4, r6, r2)     // Catch:{ Exception -> 0x0088 }
        L_0x0074:
            r2.clear()     // Catch:{ Exception -> 0x0088 }
            int r3 = r3 + 1
            goto L_0x002c
        L_0x007a:
            r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0088 }
            r1.endTransaction()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x0095
        L_0x0082:
            r1.close()     // Catch:{ all -> 0x00a0 }
            goto L_0x0095
        L_0x0086:
            r9 = move-exception
            goto L_0x0097
        L_0x0088:
            java.lang.String r9 = "openSDK_LOG.ReportDatabaseHelper"
            java.lang.String r10 = "saveReportItemToDB has exception."
            com.tencent.open.p059a.C3082f.m7636e(r9, r10)     // Catch:{ all -> 0x0086 }
            r1.endTransaction()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x0095
            goto L_0x0082
        L_0x0095:
            monitor-exit(r8)
            return
        L_0x0097:
            r1.endTransaction()     // Catch:{ all -> 0x00a0 }
            if (r1 == 0) goto L_0x009f
            r1.close()     // Catch:{ all -> 0x00a0 }
        L_0x009f:
            throw r9     // Catch:{ all -> 0x00a0 }
        L_0x00a0:
            r9 = move-exception
            monitor-exit(r8)
            goto L_0x00a4
        L_0x00a3:
            throw r9
        L_0x00a4:
            goto L_0x00a3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3093f.mo35126a(java.lang.String, java.util.List):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x001e, code lost:
        if (r0 != null) goto L_0x0020;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:?, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x002e, code lost:
        if (r0 == null) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0032, code lost:
        return;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo35127b(java.lang.String r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = android.text.TextUtils.isEmpty(r6)     // Catch:{ all -> 0x0039 }
            if (r0 == 0) goto L_0x0009
            monitor-exit(r5)
            return
        L_0x0009:
            android.database.sqlite.SQLiteDatabase r0 = r5.getWritableDatabase()     // Catch:{ all -> 0x0039 }
            if (r0 != 0) goto L_0x0011
            monitor-exit(r5)
            return
        L_0x0011:
            java.lang.String r1 = "via_cgi_report"
            java.lang.String r2 = "type = ?"
            r3 = 1
            java.lang.String[] r3 = new java.lang.String[r3]     // Catch:{ Exception -> 0x0026 }
            r4 = 0
            r3[r4] = r6     // Catch:{ Exception -> 0x0026 }
            r0.delete(r1, r2, r3)     // Catch:{ Exception -> 0x0026 }
            if (r0 == 0) goto L_0x0031
        L_0x0020:
            r0.close()     // Catch:{ all -> 0x0039 }
            goto L_0x0031
        L_0x0024:
            r6 = move-exception
            goto L_0x0033
        L_0x0026:
            r6 = move-exception
            java.lang.String r1 = "openSDK_LOG.ReportDatabaseHelper"
            java.lang.String r2 = "clearReportItem has exception."
            com.tencent.open.p059a.C3082f.m7632b(r1, r2, r6)     // Catch:{ all -> 0x0024 }
            if (r0 == 0) goto L_0x0031
            goto L_0x0020
        L_0x0031:
            monitor-exit(r5)
            return
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ all -> 0x0039 }
        L_0x0038:
            throw r6     // Catch:{ all -> 0x0039 }
        L_0x0039:
            r6 = move-exception
            monitor-exit(r5)
            goto L_0x003d
        L_0x003c:
            throw r6
        L_0x003d:
            goto L_0x003c
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.p060b.C3093f.mo35127b(java.lang.String):void");
    }
}
