package com.tencent.mid.p054b;

import android.content.Context;
import android.os.Environment;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;

/* renamed from: com.tencent.mid.b.c */
public class C3027c extends C3030f {
    /* renamed from: a */
    public int mo34886a() {
        return 2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34887a(C3025a aVar) {
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C3025a mo34891d() {
        return null;
    }

    public C3027c(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo34889b() {
        try {
            if (!Util.checkPermission(this.f6554c, "android.permission.WRITE_EXTERNAL_STORAGE") || !"mounted".equals(Environment.getExternalStorageState())) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            C3038d dVar = f6553b;
            dVar.mo34950b("checkPermission " + th);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:1|2|3|4|5|(2:6|(2:8|(1:20)(3:21|12|13))(0))|14|15) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x005d */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String mo34890c() {
        /*
            r6 = this;
            monitor-enter(r6)
            com.tencent.mid.util.d r0 = com.tencent.mid.p054b.C3027c.f6553b     // Catch:{ all -> 0x005f }
            java.lang.String r1 = "read mid from InternalStorage"
            r0.mo34950b(r1)     // Catch:{ all -> 0x005f }
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x005f }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x005f }
            java.lang.String r2 = r6.mo34899f()     // Catch:{ all -> 0x005f }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x005f }
            r1 = 0
            java.util.List r0 = com.tencent.mid.p054b.C3026b.m7397a(r0)     // Catch:{ IOException -> 0x005d }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ IOException -> 0x005d }
        L_0x001e:
            boolean r2 = r0.hasNext()     // Catch:{ IOException -> 0x005d }
            if (r2 == 0) goto L_0x005d
            java.lang.Object r2 = r0.next()     // Catch:{ IOException -> 0x005d }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ IOException -> 0x005d }
            java.lang.String r3 = ","
            java.lang.String[] r2 = r2.split(r3)     // Catch:{ IOException -> 0x005d }
            int r3 = r2.length     // Catch:{ IOException -> 0x005d }
            r4 = 2
            if (r3 != r4) goto L_0x001e
            r3 = 0
            r3 = r2[r3]     // Catch:{ IOException -> 0x005d }
            java.lang.String r4 = r6.mo34901h()     // Catch:{ IOException -> 0x005d }
            boolean r3 = r3.equals(r4)     // Catch:{ IOException -> 0x005d }
            if (r3 == 0) goto L_0x001e
            com.tencent.mid.util.d r0 = com.tencent.mid.p054b.C3027c.f6553b     // Catch:{ IOException -> 0x005d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x005d }
            r3.<init>()     // Catch:{ IOException -> 0x005d }
            java.lang.String r4 = "read mid from InternalStorage:"
            r3.append(r4)     // Catch:{ IOException -> 0x005d }
            r4 = 1
            r5 = r2[r4]     // Catch:{ IOException -> 0x005d }
            r3.append(r5)     // Catch:{ IOException -> 0x005d }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x005d }
            r0.mo34950b(r3)     // Catch:{ IOException -> 0x005d }
            r0 = r2[r4]     // Catch:{ IOException -> 0x005d }
            r1 = r0
        L_0x005d:
            monitor-exit(r6)     // Catch:{ all -> 0x005f }
            return r1
        L_0x005f:
            r0 = move-exception
            monitor-exit(r6)     // Catch:{ all -> 0x005f }
            goto L_0x0063
        L_0x0062:
            throw r0
        L_0x0063:
            goto L_0x0062
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.p054b.C3027c.mo34890c():java.lang.String");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:10|(0)|17|18) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x006e */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x006b A[SYNTHETIC, Splitter:B:15:0x006b] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0072 A[Catch:{ IOException -> 0x006f, all -> 0x0067 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo34888a(java.lang.String r4) {
        /*
            r3 = this;
            monitor-enter(r3)
            com.tencent.mid.util.d r0 = com.tencent.mid.p054b.C3027c.f6553b     // Catch:{ all -> 0x0075 }
            java.lang.String r1 = "write mid to InternalStorage"
            r0.mo34950b(r1)     // Catch:{ all -> 0x0075 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0075 }
            r0.<init>()     // Catch:{ all -> 0x0075 }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x0075 }
            r0.append(r1)     // Catch:{ all -> 0x0075 }
            java.lang.String r1 = "/"
            r0.append(r1)     // Catch:{ all -> 0x0075 }
            java.lang.String r1 = r3.mo34898e()     // Catch:{ all -> 0x0075 }
            r0.append(r1)     // Catch:{ all -> 0x0075 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0075 }
            com.tencent.mid.p054b.C3026b.m7396a(r0)     // Catch:{ all -> 0x0075 }
            java.io.File r0 = new java.io.File     // Catch:{ all -> 0x0075 }
            java.io.File r1 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ all -> 0x0075 }
            java.lang.String r2 = r3.mo34899f()     // Catch:{ all -> 0x0075 }
            r0.<init>(r1, r2)     // Catch:{ all -> 0x0075 }
            r1 = 0
            java.io.FileWriter r2 = new java.io.FileWriter     // Catch:{ IOException -> 0x006f, all -> 0x0067 }
            r2.<init>(r0)     // Catch:{ IOException -> 0x006f, all -> 0x0067 }
            java.io.BufferedWriter r0 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x006f, all -> 0x0067 }
            r0.<init>(r2)     // Catch:{ IOException -> 0x006f, all -> 0x0067 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            r1.<init>()     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            java.lang.String r2 = r3.mo34901h()     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            r1.append(r2)     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            java.lang.String r2 = ","
            r1.append(r2)     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            r1.append(r4)     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = r1.toString()     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            r0.write(r4)     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
            java.lang.String r4 = "\n"
            r0.write(r4)     // Catch:{ IOException -> 0x0065, all -> 0x0063 }
        L_0x005f:
            r0.close()     // Catch:{ Exception -> 0x0073 }
            goto L_0x0073
        L_0x0063:
            r4 = move-exception
            goto L_0x0069
        L_0x0065:
            goto L_0x0070
        L_0x0067:
            r4 = move-exception
            r0 = r1
        L_0x0069:
            if (r0 == 0) goto L_0x006e
            r0.close()     // Catch:{ Exception -> 0x006e }
        L_0x006e:
            throw r4     // Catch:{ all -> 0x0075 }
        L_0x006f:
            r0 = r1
        L_0x0070:
            if (r0 == 0) goto L_0x0073
            goto L_0x005f
        L_0x0073:
            monitor-exit(r3)     // Catch:{ all -> 0x0075 }
            return
        L_0x0075:
            r4 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x0075 }
            goto L_0x0079
        L_0x0078:
            throw r4
        L_0x0079:
            goto L_0x0078
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.p054b.C3027c.mo34888a(java.lang.String):void");
    }
}
