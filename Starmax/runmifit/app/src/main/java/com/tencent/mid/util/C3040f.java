package com.tencent.mid.util;

import android.util.Base64;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/* renamed from: com.tencent.mid.util.f */
public class C3040f {

    /* renamed from: a */
    private static RSAPublicKey f6609a;

    /* renamed from: a */
    public static void m7505a(String str) {
        try {
            f6609a = (RSAPublicKey) KeyFactory.getInstance("RSA").generatePublic(new X509EncodedKeySpec(Base64.decode(str, 0)));
        } catch (NoSuchAlgorithmException unused) {
            throw new Exception("无此算法");
        } catch (InvalidKeySpecException unused2) {
            throw new Exception("公钥非法");
        } catch (NullPointerException unused3) {
            throw new Exception("公钥数据为空");
        }
    }

    /* renamed from: a */
    public static byte[] m7506a(byte[] bArr) {
        byte[] bArr2;
        if (f6609a != null) {
            Cipher instance = Cipher.getInstance("RSA/NONE/PKCS1PADDING");
            instance.init(1, f6609a);
            int length = bArr.length;
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            int i = 0;
            int i2 = 0;
            while (true) {
                int i3 = length - i;
                if (i3 > 0) {
                    if (i3 > 117) {
                        bArr2 = instance.doFinal(bArr, i, 117);
                    } else {
                        bArr2 = instance.doFinal(bArr, i, i3);
                    }
                    byteArrayOutputStream.write(bArr2, 0, bArr2.length);
                    i2++;
                    i = i2 * 117;
                } else {
                    byte[] byteArray = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    return byteArray;
                }
            }
        } else {
            throw new Exception("加密公钥为空, 请设置");
        }
    }

    /* renamed from: b */
    public static String m7508b(byte[] bArr) {
        if (f6609a != null) {
            try {
                Cipher instance = Cipher.getInstance("RSA/NONE/PKCS1PADDING");
                instance.init(2, f6609a);
                String str = "";
                byte[][] a = m7507a(bArr, f6609a.getModulus().bitLength() / 8);
                for (byte[] bArr2 : a) {
                    str = str + new String(instance.doFinal(bArr2), "UTF-8");
                }
                return str;
            } catch (NoSuchAlgorithmException unused) {
                throw new Exception("无此解密算法");
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
                return null;
            } catch (InvalidKeyException unused2) {
                throw new Exception("解密私钥非法,请检查");
            } catch (IllegalBlockSizeException unused3) {
                throw new Exception("密文长度非法");
            } catch (BadPaddingException unused4) {
                throw new Exception("密文数据已损坏");
            }
        } else {
            throw new Exception("解密公钥为空, 请设置");
        }
    }

    /* renamed from: a */
    public static byte[][] m7507a(byte[] bArr, int i) {
        int length = bArr.length / i;
        int length2 = bArr.length % i;
        int i2 = length + (length2 != 0 ? 1 : 0);
        byte[][] bArr2 = new byte[i2][];
        for (int i3 = 0; i3 < i2; i3++) {
            byte[] bArr3 = new byte[i];
            if (i3 != i2 - 1 || length2 == 0) {
                System.arraycopy(bArr, i3 * i, bArr3, 0, i);
            } else {
                System.arraycopy(bArr, i3 * i, bArr3, 0, length2);
            }
            bArr2[i3] = bArr3;
        }
        return bArr2;
    }
}
