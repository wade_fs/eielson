package com.tencent.stat;

import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;

public class StatTrackLog {
    public static int MAX_FETCH_LIMIT = 50;
    public static int MAX_LIMIT = 100;

    /* renamed from: a */
    private static ArrayList<String> f7156a = new ArrayList<>();

    /* renamed from: b */
    private static boolean f7157b = true;

    /* renamed from: c */
    private static String f7158c = "track.mta";

    public static boolean isEnableLogcatOutput() {
        return f7157b;
    }

    public static void setEnableLogcatOutput(boolean z) {
        f7157b = z;
    }

    public static void log(Object obj) {
        if (obj != null) {
            String obj2 = obj.toString();
            if (f7157b) {
                Log.d(f7158c, obj2);
            }
            f7156a.add(obj2);
            int size = f7156a.size();
            int i = MAX_LIMIT;
            if (size > i) {
                f7156a = (ArrayList) f7156a.subList(size - (i / 2), size);
            }
        }
    }

    public static String fetchLog() {
        ArrayList arrayList;
        StringBuffer stringBuffer = new StringBuffer();
        if (f7156a.size() > MAX_FETCH_LIMIT) {
            ArrayList<String> arrayList2 = f7156a;
            arrayList = (ArrayList) arrayList2.subList(arrayList2.size() - MAX_FETCH_LIMIT, f7156a.size());
        } else {
            arrayList = (ArrayList) f7156a.clone();
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            stringBuffer.append(((String) it.next()) + "\n");
        }
        return stringBuffer.toString();
    }
}
