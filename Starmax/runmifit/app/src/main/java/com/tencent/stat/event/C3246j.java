package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatSpecifyReportedInfo;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.j */
public class C3246j extends C3252n {
    public EventType getType() {
        return EventType.ONCE;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        super.onEncode(jSONObject);
        mo35467a(jSONObject);
        jSONObject.put("ft", 1);
        return true;
    }

    public C3246j(Context context, int i, JSONObject jSONObject, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, jSONObject, statSpecifyReportedInfo);
    }
}
