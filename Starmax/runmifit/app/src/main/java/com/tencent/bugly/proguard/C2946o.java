package com.tencent.bugly.proguard;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import java.lang.ref.WeakReference;

/* renamed from: com.tencent.bugly.proguard.o */
/* compiled from: BUGLY */
public class C2946o {

    /* renamed from: a */
    private static String f6219a = "com.tencent.bugly";

    /* renamed from: b */
    private static Handler f6220b = null;

    /* renamed from: c */
    private static String f6221c = null;

    /* renamed from: d */
    private static String f6222d = null;

    /* renamed from: e */
    private static String f6223e = null;

    /* renamed from: f */
    private static boolean f6224f = false;

    /* renamed from: g */
    private static int f6225g = 0;

    /* renamed from: h */
    private static WeakReference<Activity> f6226h = null;

    /* renamed from: i */
    private static WeakReference<Context> f6227i = null;

    /* renamed from: j */
    private static boolean f6228j = false;
}
