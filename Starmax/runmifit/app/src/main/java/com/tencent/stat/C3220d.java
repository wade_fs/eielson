package com.tencent.stat;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.Settings;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.google.common.net.HttpHeaders;
import com.tamic.novate.download.MimeType;
import com.tamic.novate.util.Utils;
import com.tencent.mid.api.MidService;
import com.tencent.mid.util.Util;
import com.tencent.open.SocialConstants;
import com.tencent.stat.common.C3209d;
import com.tencent.stat.common.C3210e;
import com.tencent.stat.common.C3212g;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatConstants;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.event.Event;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultConnectionKeepAliveStrategy;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.d */
class C3220d {

    /* renamed from: d */
    private static StatLogger f7272d = StatCommonHelper.getLogger();

    /* renamed from: e */
    private static C3220d f7273e = null;

    /* renamed from: f */
    private static Context f7274f = null;

    /* renamed from: a */
    DefaultHttpClient f7275a = null;

    /* renamed from: b */
    Handler f7276b = null;

    /* renamed from: c */
    StringBuilder f7277c = new StringBuilder(4096);

    /* renamed from: g */
    private long f7278g = 0;

    private C3220d(Context context) {
        try {
            HandlerThread handlerThread = new HandlerThread("StatDispatcher");
            handlerThread.start();
            this.f7276b = new Handler(handlerThread.getLooper());
            f7274f = context.getApplicationContext();
            this.f7278g = System.currentTimeMillis() / 1000;
            BasicHttpParams basicHttpParams = new BasicHttpParams();
            HttpConnectionParams.setStaleCheckingEnabled(basicHttpParams, false);
            HttpConnectionParams.setConnectionTimeout(basicHttpParams, 10000);
            HttpConnectionParams.setSoTimeout(basicHttpParams, 10000);
            this.f7275a = new DefaultHttpClient(basicHttpParams);
            this.f7275a.setKeepAliveStrategy(new DefaultConnectionKeepAliveStrategy() {
                /* class com.tencent.stat.C3220d.C32211 */

                public long getKeepAliveDuration(HttpResponse httpResponse, HttpContext httpContext) {
                    long keepAliveDuration = C3220d.super.getKeepAliveDuration(httpResponse, httpContext);
                    if (keepAliveDuration == -1) {
                        return 30000;
                    }
                    return keepAliveDuration;
                }
            });
        } catch (Throwable th) {
            f7272d.mo35384e(th);
        }
    }

    /* renamed from: a */
    static void m8020a(Context context) {
        f7274f = context.getApplicationContext();
    }

    /* renamed from: a */
    static Context m8017a() {
        return f7274f;
    }

    /* renamed from: b */
    static C3220d m8022b(Context context) {
        if (f7273e == null) {
            synchronized (C3220d.class) {
                if (f7273e == null) {
                    f7273e = new C3220d(context);
                }
            }
        }
        return f7273e;
    }

    /* renamed from: a */
    private void m8021a(JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("mid");
            if (Util.isMidValid(optString)) {
                if (StatConfig.isDebugEnable()) {
                    StatLogger statLogger = f7272d;
                    statLogger.mo35388i("update mid:" + optString);
                }
                Util.updateIfLocalInvalid(f7274f, optString);
            }
            if (!jSONObject.isNull("cfg")) {
                StatConfig.m7821a(f7274f, jSONObject.getJSONObject("cfg"));
            }
            if (!jSONObject.isNull("ncts")) {
                int i = jSONObject.getInt("ncts");
                int currentTimeMillis = (int) (((long) i) - (System.currentTimeMillis() / 1000));
                if (StatConfig.isDebugEnable()) {
                    StatLogger statLogger2 = f7272d;
                    statLogger2.mo35388i("server time:" + i + ", diff time:" + currentTimeMillis);
                }
                StatCommonHelper.updateCheckTime(f7274f);
                StatCommonHelper.writeDiffTimeFromServer(f7274f, currentTimeMillis);
            }
        } catch (Throwable th) {
            f7272d.mo35396w(th);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35412a(List<?> list, StatDispatchCallback statDispatchCallback) {
        Throwable th;
        List<?> list2 = list;
        if (list2 != null && !list.isEmpty()) {
            int size = list.size();
            try {
                this.f7277c.delete(0, this.f7277c.length());
                this.f7277c.append("[");
                for (int i = 0; i < size; i++) {
                    this.f7277c.append(list2.get(i).toString());
                    if (i != size - 1) {
                        this.f7277c.append(",");
                    }
                }
                this.f7277c.append("]");
                String sb = this.f7277c.toString();
                int length = sb.length();
                String str = StatConfig.getStatReportUrl() + "/?index=" + this.f7278g;
                this.f7278g++;
                if (StatConfig.isDebugEnable()) {
                    f7272d.mo35388i("[" + str + "]Send request(" + length + "bytes), content:" + sb);
                }
                HttpPost httpPost = new HttpPost(str);
                httpPost.addHeader(HttpHeaders.ACCEPT_ENCODING, MimeType.GZIP);
                httpPost.setHeader(HttpHeaders.CONNECTION, "Keep-Alive");
                httpPost.removeHeaders(HttpHeaders.CACHE_CONTROL);
                HttpHost httpProxy = NetworkManager.getInstance(f7274f).getHttpProxy();
                httpPost.addHeader(HttpHeaders.CONTENT_ENCODING, "rc4");
                if (httpProxy == null) {
                    this.f7275a.getParams().removeParameter("http.route.default-proxy");
                } else {
                    if (StatConfig.isDebugEnable()) {
                        f7272d.mo35381d("proxy:" + httpProxy.toHostString());
                    }
                    httpPost.addHeader("X-Content-Encoding", "rc4");
                    this.f7275a.getParams().setParameter("http.route.default-proxy", httpProxy);
                    httpPost.addHeader("X-Online-Host", StatConfig.f6953k);
                    httpPost.addHeader(HttpHeaders.ACCEPT, "*/*");
                    httpPost.addHeader(HttpHeaders.CONTENT_TYPE, MimeType.JSON);
                }
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(length);
                byte[] bytes = sb.getBytes("UTF-8");
                int length2 = bytes.length;
                if (length > StatConfig.f6957o) {
                    httpPost.removeHeaders(HttpHeaders.CONTENT_ENCODING);
                    String str2 = "rc4" + ",gzip";
                    httpPost.addHeader(HttpHeaders.CONTENT_ENCODING, str2);
                    if (httpProxy != null) {
                        httpPost.removeHeaders("X-Content-Encoding");
                        httpPost.addHeader("X-Content-Encoding", str2);
                    }
                    byteArrayOutputStream.write(new byte[4]);
                    GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
                    gZIPOutputStream.write(bytes);
                    gZIPOutputStream.close();
                    bytes = byteArrayOutputStream.toByteArray();
                    ByteBuffer.wrap(bytes, 0, 4).putInt(length2);
                    if (StatConfig.isDebugEnable()) {
                        f7272d.mo35381d("before Gzip:" + length2 + " bytes, after Gzip:" + bytes.length + " bytes");
                    }
                }
                httpPost.setEntity(new ByteArrayEntity(C3210e.m7985a(bytes)));
                HttpResponse execute = this.f7275a.execute(httpPost);
                HttpEntity entity = execute.getEntity();
                int statusCode = execute.getStatusLine().getStatusCode();
                long contentLength = entity.getContentLength();
                if (StatConfig.isDebugEnable()) {
                    f7272d.mo35388i("http recv response status code:" + statusCode + ", content length:" + contentLength);
                }
                if (contentLength == 0) {
                    f7272d.mo35383e("Server response no data.");
                    if (statDispatchCallback != null) {
                        statDispatchCallback.onDispatchFailure();
                    }
                    EntityUtils.toString(entity);
                    return;
                }
                if (contentLength > 0) {
                    InputStream content = entity.getContent();
                    DataInputStream dataInputStream = new DataInputStream(content);
                    byte[] bArr = new byte[((int) entity.getContentLength())];
                    dataInputStream.readFully(bArr);
                    content.close();
                    dataInputStream.close();
                    Header firstHeader = execute.getFirstHeader(HttpHeaders.CONTENT_ENCODING);
                    if (firstHeader != null) {
                        if (firstHeader.getValue().equalsIgnoreCase("gzip,rc4")) {
                            bArr = C3210e.m7987b(StatCommonHelper.deocdeGZipContent(bArr));
                        } else if (firstHeader.getValue().equalsIgnoreCase("rc4,gzip")) {
                            bArr = StatCommonHelper.deocdeGZipContent(C3210e.m7987b(bArr));
                        } else if (firstHeader.getValue().equalsIgnoreCase(MimeType.GZIP)) {
                            bArr = StatCommonHelper.deocdeGZipContent(bArr);
                        } else if (firstHeader.getValue().equalsIgnoreCase("rc4")) {
                            bArr = C3210e.m7987b(bArr);
                        }
                    }
                    String str3 = new String(bArr, "UTF-8");
                    if (StatConfig.isDebugEnable()) {
                        f7272d.mo35388i("http get response data:" + str3);
                    }
                    JSONObject jSONObject = new JSONObject(str3);
                    if (statusCode == 200) {
                        m8021a(jSONObject);
                        if (statDispatchCallback != null) {
                            if (jSONObject.optInt("ret") == 0) {
                                statDispatchCallback.onDispatchSuccess();
                            } else {
                                f7272d.error("response error data.");
                                statDispatchCallback.onDispatchFailure();
                            }
                        }
                    } else {
                        f7272d.error("Server response error code:" + statusCode + ", error:" + new String(bArr, "UTF-8"));
                        if (statDispatchCallback != null) {
                            statDispatchCallback.onDispatchFailure();
                        }
                    }
                    content.close();
                } else {
                    EntityUtils.toString(entity);
                }
                byteArrayOutputStream.close();
                th = null;
                if (th != null) {
                    f7272d.error(th);
                    if (statDispatchCallback != null) {
                        try {
                            statDispatchCallback.onDispatchFailure();
                        } catch (Throwable th2) {
                            f7272d.mo35384e(th2);
                        }
                    }
                    if (th instanceof OutOfMemoryError) {
                        this.f7277c = null;
                        System.gc();
                        this.f7277c = new StringBuilder(2048);
                    } else if (!(th instanceof UnknownHostException)) {
                        boolean z = th instanceof SocketTimeoutException;
                    }
                    NetworkManager.getInstance(f7274f).onDispatchFailed();
                }
            } catch (Throwable th3) {
                th = th3;
            }
        }
    }

    /* renamed from: c */
    private long m8023c() {
        try {
            return System.currentTimeMillis() / 1000;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m8018a(int i, String str) {
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("data", "null");
            jSONObject.put("msg", "error:" + str);
            jSONObject.put("code", String.valueOf(i));
            return jSONObject.toString();
        } catch (Throwable th) {
            f7272d.mo35384e(th);
            return "{\"code\":-1,\"msg\":\"调用失败\",\"data\":null}";
        }
    }

    /* renamed from: a */
    public String mo35405a(String str, String str2) throws IOException {
        String appKey = StatConfig.getAppKey(f7274f);
        String localMidOnly = MidService.getLocalMidOnly(f7274f);
        String curAppVersion = StatCommonHelper.getCurAppVersion(f7274f);
        String str3 = Build.VERSION.RELEASE;
        String deviceModel = StatCommonHelper.getDeviceModel(f7274f);
        String linkedWay = StatCommonHelper.getLinkedWay(f7274f);
        String str4 = m8023c() + "";
        HashMap hashMap = new HashMap();
        hashMap.put("UserName", "user");
        hashMap.put("AppKey", appKey);
        hashMap.put("Mid", localMidOnly);
        hashMap.put("Content", str);
        hashMap.put("AppVersion", curAppVersion);
        hashMap.put("OSVersion", str3);
        hashMap.put("Model", deviceModel);
        hashMap.put("NetWork", linkedWay);
        hashMap.put("FileType", SocialConstants.PARAM_IMG_URL);
        hashMap.put("TimeStamp", str4);
        StringBuilder sb = new StringBuilder();
        sb.append(com.tencent.stat.common.Util.decode(StatConstants.FB_KEY));
        sb.append("AppKey=" + appKey);
        sb.append("AppVersion=" + curAppVersion);
        sb.append("Mid=" + localMidOnly);
        sb.append("Model=" + deviceModel);
        sb.append("NetWork=" + linkedWay);
        sb.append("OSVersion=" + str3);
        sb.append("TimeStamp=" + str4);
        hashMap.put("Sign", StatCommonHelper.md5sum(sb.toString()));
        C3209d.m7984a(f7274f);
        HashMap hashMap2 = new HashMap();
        hashMap2.put("log", new File(f7274f.getFilesDir() + "/feedback.log"));
        if (str2 != null && str2.trim().length() > 0) {
            File file = new File(f7274f.getFilesDir() + "/feedback.png");
            C3212g.m7998a(str2, file);
            hashMap2.put("screenshot", file);
        }
        return mo35406a(hashMap, hashMap2);
    }

    /* renamed from: a */
    public String mo35406a(Map<String, String> map, Map<String, File> map2) throws IOException {
        String str;
        String uuid = UUID.randomUUID().toString();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("http://mta.qq.com/mta/api/ctr_feedback/add_feedback").openConnection();
        httpURLConnection.setReadTimeout(5000);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setRequestMethod("POST");
        httpURLConnection.setRequestProperty("connection", "keep-alive");
        httpURLConnection.setRequestProperty("Charsert", "UTF-8");
        httpURLConnection.setRequestProperty(HttpHeaders.CONTENT_TYPE, Utils.MULTIPART_FORM_DATA + ";boundary=" + uuid);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : map.entrySet()) {
            sb.append("--");
            sb.append(uuid);
            sb.append("\r\n");
            sb.append("Content-Disposition: form-data; name=\"" + ((String) entry.getKey()) + "\"" + "\r\n");
            StringBuilder sb2 = new StringBuilder();
            sb2.append("Content-Type: text/plain; charset=");
            sb2.append("UTF-8");
            sb2.append("\r\n");
            sb.append(sb2.toString());
            sb.append("Content-Transfer-Encoding: 8bit" + "\r\n");
            sb.append("\r\n");
            sb.append((String) entry.getValue());
            sb.append("\r\n");
        }
        DataOutputStream dataOutputStream = new DataOutputStream(httpURLConnection.getOutputStream());
        dataOutputStream.write(sb.toString().getBytes());
        if (map2 != null) {
            for (Map.Entry entry2 : map2.entrySet()) {
                StringBuilder sb3 = new StringBuilder();
                sb3.append("--");
                sb3.append(uuid);
                sb3.append("\r\n");
                sb3.append("Content-Disposition: form-data; name=\"" + ((String) entry2.getKey()) + "\"; filename=\"" + ((File) entry2.getValue()).getName() + "\"" + "\r\n");
                StringBuilder sb4 = new StringBuilder();
                sb4.append("Content-Type: application/octet-stream; charset=");
                sb4.append("UTF-8");
                sb4.append("\r\n");
                sb3.append(sb4.toString());
                sb3.append("\r\n");
                dataOutputStream.write(sb3.toString().getBytes());
                FileInputStream fileInputStream = new FileInputStream((File) entry2.getValue());
                byte[] bArr = new byte[1024];
                while (true) {
                    int read = fileInputStream.read(bArr);
                    if (read == -1) {
                        break;
                    }
                    dataOutputStream.write(bArr, 0, read);
                }
                fileInputStream.close();
                dataOutputStream.write("\r\n".getBytes());
            }
        }
        dataOutputStream.write(("--" + uuid + "--" + "\r\n").getBytes());
        dataOutputStream.flush();
        StringBuilder sb5 = new StringBuilder();
        if (httpURLConnection.getResponseCode() == 200) {
            InputStream inputStream = httpURLConnection.getInputStream();
            while (true) {
                int read2 = inputStream.read();
                if (read2 == -1) {
                    break;
                }
                sb5.append((char) read2);
            }
            str = sb5.toString();
        } else {
            str = m8018a(-1, "" + httpURLConnection.getResponseCode());
        }
        dataOutputStream.close();
        httpURLConnection.disconnect();
        return str;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x01c3 A[Catch:{ all -> 0x0204 }] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x01c6 A[Catch:{ all -> 0x0204 }] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01ea A[DONT_GENERATE] */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x01ef  */
    /* JADX WARNING: Removed duplicated region for block: B:79:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo35411a(java.lang.String r13, java.util.Map<java.lang.String, java.lang.String> r14, com.tencent.stat.StatFBDispatchCallback r15) {
        /*
            r12 = this;
            java.lang.String r0 = "UTF-8"
            java.lang.String r1 = "Content-Encoding"
            java.lang.String r2 = "rc4"
            java.lang.String r3 = ""
            r4 = -1
            org.apache.http.client.methods.HttpPost r5 = new org.apache.http.client.methods.HttpPost     // Catch:{ all -> 0x01bd }
            r5.<init>(r13)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = "Connection"
            java.lang.String r6 = "Keep-Alive"
            r5.setHeader(r13, r6)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = "Cache-Control"
            r5.removeHeaders(r13)     // Catch:{ all -> 0x01bd }
            android.content.Context r13 = com.tencent.stat.C3220d.f7274f     // Catch:{ all -> 0x01bd }
            com.tencent.stat.NetworkManager r13 = com.tencent.stat.NetworkManager.getInstance(r13)     // Catch:{ all -> 0x01bd }
            org.apache.http.HttpHost r13 = r13.getHttpProxy()     // Catch:{ all -> 0x01bd }
            r5.addHeader(r1, r2)     // Catch:{ all -> 0x01bd }
            java.lang.String r6 = "http.route.default-proxy"
            if (r13 != 0) goto L_0x0035
            org.apache.http.impl.client.DefaultHttpClient r13 = r12.f7275a     // Catch:{ all -> 0x01bd }
            org.apache.http.params.HttpParams r13 = r13.getParams()     // Catch:{ all -> 0x01bd }
            r13.removeParameter(r6)     // Catch:{ all -> 0x01bd }
            goto L_0x0073
        L_0x0035:
            boolean r7 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x01bd }
            if (r7 == 0) goto L_0x0055
            com.tencent.stat.common.StatLogger r7 = com.tencent.stat.C3220d.f7272d     // Catch:{ all -> 0x01bd }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bd }
            r8.<init>()     // Catch:{ all -> 0x01bd }
            java.lang.String r9 = "proxy:"
            r8.append(r9)     // Catch:{ all -> 0x01bd }
            java.lang.String r9 = r13.toHostString()     // Catch:{ all -> 0x01bd }
            r8.append(r9)     // Catch:{ all -> 0x01bd }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x01bd }
            r7.mo35381d(r8)     // Catch:{ all -> 0x01bd }
        L_0x0055:
            org.apache.http.impl.client.DefaultHttpClient r7 = r12.f7275a     // Catch:{ all -> 0x01bd }
            org.apache.http.params.HttpParams r7 = r7.getParams()     // Catch:{ all -> 0x01bd }
            r7.setParameter(r6, r13)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = "X-Online-Host"
            java.lang.String r6 = "mta.qq.com:80"
            r5.addHeader(r13, r6)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = "Accept"
            java.lang.String r6 = "*/*"
            r5.addHeader(r13, r6)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = "Content-Type"
            java.lang.String r6 = "text/plain"
            r5.addHeader(r13, r6)     // Catch:{ all -> 0x01bd }
        L_0x0073:
            java.util.ArrayList r13 = new java.util.ArrayList     // Catch:{ all -> 0x01bd }
            r13.<init>()     // Catch:{ all -> 0x01bd }
            java.util.Set r14 = r14.entrySet()     // Catch:{ all -> 0x01bd }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ all -> 0x01bd }
        L_0x0080:
            boolean r6 = r14.hasNext()     // Catch:{ all -> 0x01bd }
            if (r6 == 0) goto L_0x00a1
            java.lang.Object r6 = r14.next()     // Catch:{ all -> 0x01bd }
            java.util.Map$Entry r6 = (java.util.Map.Entry) r6     // Catch:{ all -> 0x01bd }
            org.apache.http.message.BasicNameValuePair r7 = new org.apache.http.message.BasicNameValuePair     // Catch:{ all -> 0x01bd }
            java.lang.Object r8 = r6.getKey()     // Catch:{ all -> 0x01bd }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x01bd }
            java.lang.Object r6 = r6.getValue()     // Catch:{ all -> 0x01bd }
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ all -> 0x01bd }
            r7.<init>(r8, r6)     // Catch:{ all -> 0x01bd }
            r13.add(r7)     // Catch:{ all -> 0x01bd }
            goto L_0x0080
        L_0x00a1:
            org.apache.http.client.entity.UrlEncodedFormEntity r14 = new org.apache.http.client.entity.UrlEncodedFormEntity     // Catch:{ all -> 0x01bd }
            r14.<init>(r13, r0)     // Catch:{ all -> 0x01bd }
            r5.setEntity(r14)     // Catch:{ all -> 0x01bd }
            org.apache.http.impl.client.DefaultHttpClient r13 = r12.f7275a     // Catch:{ all -> 0x01bd }
            org.apache.http.HttpResponse r13 = r13.execute(r5)     // Catch:{ all -> 0x01bd }
            org.apache.http.HttpEntity r14 = r13.getEntity()     // Catch:{ all -> 0x01bd }
            org.apache.http.StatusLine r5 = r13.getStatusLine()     // Catch:{ all -> 0x01bd }
            int r5 = r5.getStatusCode()     // Catch:{ all -> 0x01bd }
            long r6 = r14.getContentLength()     // Catch:{ all -> 0x01bd }
            boolean r8 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x01bd }
            if (r8 == 0) goto L_0x00e3
            com.tencent.stat.common.StatLogger r8 = com.tencent.stat.C3220d.f7272d     // Catch:{ all -> 0x01bd }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bd }
            r9.<init>()     // Catch:{ all -> 0x01bd }
            java.lang.String r10 = "http recv response status code:"
            r9.append(r10)     // Catch:{ all -> 0x01bd }
            r9.append(r5)     // Catch:{ all -> 0x01bd }
            java.lang.String r10 = ", content length:"
            r9.append(r10)     // Catch:{ all -> 0x01bd }
            r9.append(r6)     // Catch:{ all -> 0x01bd }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x01bd }
            r8.mo35388i(r9)     // Catch:{ all -> 0x01bd }
        L_0x00e3:
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 <= 0) goto L_0x01a1
            java.io.InputStream r6 = r14.getContent()     // Catch:{ all -> 0x01bd }
            java.io.DataInputStream r7 = new java.io.DataInputStream     // Catch:{ all -> 0x01bd }
            r7.<init>(r6)     // Catch:{ all -> 0x01bd }
            long r8 = r14.getContentLength()     // Catch:{ all -> 0x01bd }
            int r14 = (int) r8     // Catch:{ all -> 0x01bd }
            byte[] r14 = new byte[r14]     // Catch:{ all -> 0x01bd }
            r7.readFully(r14)     // Catch:{ all -> 0x01bd }
            r6.close()     // Catch:{ all -> 0x01bd }
            r7.close()     // Catch:{ all -> 0x01bd }
            org.apache.http.Header r13 = r13.getFirstHeader(r1)     // Catch:{ all -> 0x01bd }
            if (r13 == 0) goto L_0x0151
            java.lang.String r1 = r13.getValue()     // Catch:{ all -> 0x01bd }
            java.lang.String r6 = "gzip,rc4"
            boolean r1 = r1.equalsIgnoreCase(r6)     // Catch:{ all -> 0x01bd }
            if (r1 == 0) goto L_0x011d
            byte[] r13 = com.tencent.stat.common.StatCommonHelper.deocdeGZipContent(r14)     // Catch:{ all -> 0x01bd }
            byte[] r14 = com.tencent.stat.common.C3210e.m7987b(r13)     // Catch:{ all -> 0x01bd }
            goto L_0x0151
        L_0x011d:
            java.lang.String r1 = r13.getValue()     // Catch:{ all -> 0x01bd }
            java.lang.String r6 = "rc4,gzip"
            boolean r1 = r1.equalsIgnoreCase(r6)     // Catch:{ all -> 0x01bd }
            if (r1 == 0) goto L_0x0132
            byte[] r13 = com.tencent.stat.common.C3210e.m7987b(r14)     // Catch:{ all -> 0x01bd }
            byte[] r14 = com.tencent.stat.common.StatCommonHelper.deocdeGZipContent(r13)     // Catch:{ all -> 0x01bd }
            goto L_0x0151
        L_0x0132:
            java.lang.String r1 = r13.getValue()     // Catch:{ all -> 0x01bd }
            java.lang.String r6 = "gzip"
            boolean r1 = r1.equalsIgnoreCase(r6)     // Catch:{ all -> 0x01bd }
            if (r1 == 0) goto L_0x0143
            byte[] r14 = com.tencent.stat.common.StatCommonHelper.deocdeGZipContent(r14)     // Catch:{ all -> 0x01bd }
            goto L_0x0151
        L_0x0143:
            java.lang.String r13 = r13.getValue()     // Catch:{ all -> 0x01bd }
            boolean r13 = r13.equalsIgnoreCase(r2)     // Catch:{ all -> 0x01bd }
            if (r13 == 0) goto L_0x0151
            byte[] r14 = com.tencent.stat.common.C3210e.m7987b(r14)     // Catch:{ all -> 0x01bd }
        L_0x0151:
            r13 = 200(0xc8, float:2.8E-43)
            if (r5 != r13) goto L_0x0177
            java.lang.String r13 = new java.lang.String     // Catch:{ all -> 0x01bd }
            r13.<init>(r14, r0)     // Catch:{ all -> 0x01bd }
            boolean r14 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x01b8 }
            if (r14 == 0) goto L_0x01b1
            com.tencent.stat.common.StatLogger r14 = com.tencent.stat.C3220d.f7272d     // Catch:{ all -> 0x01b8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x01b8 }
            r0.<init>()     // Catch:{ all -> 0x01b8 }
            java.lang.String r1 = "http get response data:"
            r0.append(r1)     // Catch:{ all -> 0x01b8 }
            r0.append(r13)     // Catch:{ all -> 0x01b8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01b8 }
            r14.mo35388i(r0)     // Catch:{ all -> 0x01b8 }
            goto L_0x01b1
        L_0x0177:
            com.tencent.stat.common.StatLogger r13 = com.tencent.stat.C3220d.f7272d     // Catch:{ all -> 0x01bd }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bd }
            r14.<init>()     // Catch:{ all -> 0x01bd }
            java.lang.String r0 = "Server response error code:"
            r14.append(r0)     // Catch:{ all -> 0x01bd }
            r14.append(r5)     // Catch:{ all -> 0x01bd }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x01bd }
            r13.error(r14)     // Catch:{ all -> 0x01bd }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bd }
            r13.<init>()     // Catch:{ all -> 0x01bd }
            r13.append(r3)     // Catch:{ all -> 0x01bd }
            r13.append(r5)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = r12.m8018a(r4, r13)     // Catch:{ all -> 0x01bd }
            goto L_0x01b1
        L_0x01a1:
            com.tencent.stat.common.StatLogger r13 = com.tencent.stat.C3220d.f7272d     // Catch:{ all -> 0x01bd }
            java.lang.String r0 = "Server response no data."
            r13.mo35383e(r0)     // Catch:{ all -> 0x01bd }
            java.lang.String r13 = "no data"
            java.lang.String r13 = r12.m8018a(r4, r13)     // Catch:{ all -> 0x01bd }
            org.apache.http.util.EntityUtils.toString(r14)     // Catch:{ all -> 0x01b8 }
        L_0x01b1:
            if (r15 == 0) goto L_0x01b6
            r15.onFBDispatch(r13)
        L_0x01b6:
            r13 = 0
            goto L_0x01ed
        L_0x01b8:
            r14 = move-exception
            r11 = r14
            r14 = r13
            r13 = r11
            goto L_0x01bf
        L_0x01bd:
            r13 = move-exception
            r14 = r3
        L_0x01bf:
            boolean r0 = r13 instanceof java.lang.OutOfMemoryError     // Catch:{ all -> 0x0204 }
            if (r0 == 0) goto L_0x01c6
            java.lang.String r3 = "OOM"
            goto L_0x01d3
        L_0x01c6:
            boolean r0 = r13 instanceof java.net.UnknownHostException     // Catch:{ all -> 0x0204 }
            if (r0 == 0) goto L_0x01cd
            java.lang.String r3 = "UnknownHost"
            goto L_0x01d3
        L_0x01cd:
            boolean r0 = r13 instanceof java.net.SocketTimeoutException     // Catch:{ all -> 0x0204 }
            if (r0 == 0) goto L_0x01d3
            java.lang.String r3 = "SocketTimeOut"
        L_0x01d3:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0204 }
            r0.<init>()     // Catch:{ all -> 0x0204 }
            r0.append(r3)     // Catch:{ all -> 0x0204 }
            java.lang.String r1 = "exception happen"
            r0.append(r1)     // Catch:{ all -> 0x0204 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0204 }
            java.lang.String r14 = r12.m8018a(r4, r0)     // Catch:{ all -> 0x0204 }
            if (r15 == 0) goto L_0x01ed
            r15.onFBDispatch(r14)
        L_0x01ed:
            if (r13 == 0) goto L_0x0203
            com.tencent.stat.common.StatLogger r14 = com.tencent.stat.C3220d.f7272d
            r14.error(r13)
            boolean r14 = r13 instanceof java.lang.OutOfMemoryError
            if (r14 == 0) goto L_0x01fc
            java.lang.System.gc()
            goto L_0x0203
        L_0x01fc:
            boolean r14 = r13 instanceof java.net.UnknownHostException
            if (r14 == 0) goto L_0x0201
            goto L_0x0203
        L_0x0201:
            boolean r13 = r13 instanceof java.net.SocketTimeoutException
        L_0x0203:
            return
        L_0x0204:
            r13 = move-exception
            if (r15 == 0) goto L_0x020a
            r15.onFBDispatch(r14)
        L_0x020a:
            goto L_0x020c
        L_0x020b:
            throw r13
        L_0x020c:
            goto L_0x020b
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3220d.mo35411a(java.lang.String, java.util.Map, com.tencent.stat.StatFBDispatchCallback):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35407a(int i, int i2, StatFBDispatchCallback statFBDispatchCallback) {
        String appKey = StatConfig.getAppKey(f7274f);
        String localMidOnly = MidService.getLocalMidOnly(f7274f);
        String str = m8023c() + "";
        StringBuilder sb = new StringBuilder();
        sb.append(com.tencent.stat.common.Util.decode(StatConstants.FB_KEY));
        sb.append("AppKey=" + appKey);
        sb.append("Mid=" + localMidOnly);
        sb.append("TimeStamp=" + str);
        String md5sum = StatCommonHelper.md5sum(sb.toString());
        HashMap hashMap = new HashMap();
        hashMap.put("AppKey", appKey);
        hashMap.put("Limit", Integer.toString(i2));
        hashMap.put("Mid", localMidOnly);
        hashMap.put("Offset", Integer.toString(i));
        hashMap.put("TimeStamp", str);
        hashMap.put("Sign", md5sum);
        mo35411a("http://mta.qq.com/mta/api/ctr_feedback/get_feedback", hashMap, statFBDispatchCallback);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35410a(String str, String str2, StatFBDispatchCallback statFBDispatchCallback) {
        String appKey = StatConfig.getAppKey(f7274f);
        String localMidOnly = MidService.getLocalMidOnly(f7274f);
        String str3 = m8023c() + "";
        StringBuilder sb = new StringBuilder();
        sb.append(com.tencent.stat.common.Util.decode(StatConstants.FB_KEY));
        sb.append("AppKey=" + appKey);
        sb.append("FeedbackId=" + str);
        sb.append("Mid=" + localMidOnly);
        sb.append("TimeStamp=" + str3);
        String md5sum = StatCommonHelper.md5sum(sb.toString());
        HashMap hashMap = new HashMap();
        hashMap.put("AppKey", appKey);
        hashMap.put("Mid", localMidOnly);
        hashMap.put("FeedbackId", str);
        hashMap.put("Content", str2);
        hashMap.put("TimeStamp", str3);
        hashMap.put("Sign", md5sum);
        mo35411a("http://mta.qq.com/mta/api/ctr_feedback/reply_feedback", hashMap, statFBDispatchCallback);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo35416b(final List<?> list, final StatDispatchCallback statDispatchCallback) {
        Handler handler = this.f7276b;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3220d.C32222 */

                public void run() {
                    C3220d.this.mo35412a(list, statDispatchCallback);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo35413b() {
        Settings.System.putString(f7274f.getContentResolver(), Config.APP_VERSION_CODE, "b");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35408a(Event event, StatDispatchCallback statDispatchCallback) {
        mo35416b(Arrays.asList(event.toJsonString()), statDispatchCallback);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35409a(String str, StatDispatchCallback statDispatchCallback) {
        mo35416b(Arrays.asList(str), statDispatchCallback);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo35415b(final String str, final String str2, final StatFBDispatchCallback statFBDispatchCallback) {
        Handler handler = this.f7276b;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3220d.C32233 */

                public void run() {
                    StatFBDispatchCallback statFBDispatchCallback;
                    String str = "";
                    try {
                        str = C3220d.this.mo35405a(str, str2);
                        statFBDispatchCallback = statFBDispatchCallback;
                        if (statFBDispatchCallback == null) {
                            return;
                        }
                    } catch (Exception e) {
                        str = C3220d.this.m8018a(-1, "IOException happen");
                        e.printStackTrace();
                        statFBDispatchCallback = statFBDispatchCallback;
                        if (statFBDispatchCallback == null) {
                            return;
                        }
                    } catch (Throwable th) {
                        StatFBDispatchCallback statFBDispatchCallback2 = statFBDispatchCallback;
                        if (statFBDispatchCallback2 != null) {
                            statFBDispatchCallback2.onFBDispatch(str);
                        }
                        throw th;
                    }
                    statFBDispatchCallback.onFBDispatch(str);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo35414b(final int i, final int i2, final StatFBDispatchCallback statFBDispatchCallback) {
        Handler handler = this.f7276b;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3220d.C32244 */

                public void run() {
                    C3220d.this.mo35407a(i, i2, statFBDispatchCallback);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo35417c(final String str, final String str2, final StatFBDispatchCallback statFBDispatchCallback) {
        Handler handler = this.f7276b;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3220d.C32255 */

                public void run() {
                    C3220d.this.mo35410a(str, str2, statFBDispatchCallback);
                }
            });
        }
    }
}
