package com.tencent.bugly.beta.upgrade;

import android.os.Parcelable;
import android.text.TextUtils;
import com.tencent.bugly.beta.download.BetaReceiver;
import com.tencent.bugly.beta.download.C2801a;
import com.tencent.bugly.beta.download.DownloadListener;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2807d;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.beta.global.C2809f;
import com.tencent.bugly.beta.p050ui.C2826h;
import com.tencent.bugly.beta.utils.C2836e;
import com.tencent.bugly.proguard.C2893ah;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import com.tencent.bugly.proguard.C2930bk;
import com.tencent.bugly.proguard.C2944m;
import com.tencent.bugly.proguard.C2947p;
import com.tencent.bugly.proguard.C2957w;
import com.tencent.bugly.proguard.C2959y;
import com.tencent.bugly.proguard.C2960z;
import java.io.File;
import java.util.HashMap;

/* renamed from: com.tencent.bugly.beta.upgrade.c */
/* compiled from: BUGLY */
public class C2829c {

    /* renamed from: a */
    public static C2829c f5467a = new C2829c();

    /* renamed from: b */
    public BetaGrayStrategy f5468b;

    /* renamed from: c */
    public DownloadTask f5469c;

    /* renamed from: d */
    public DownloadListener f5470d;

    /* renamed from: e */
    public UpgradeListener f5471e;

    /* renamed from: f */
    public UpgradeStateListener f5472f;

    /* renamed from: g */
    public boolean f5473g;

    /* renamed from: h */
    public C2807d f5474h;

    /* renamed from: i */
    public C2807d f5475i;

    /* renamed from: j */
    public int f5476j;

    /* renamed from: k */
    private final Object f5477k = new Object();

    /* renamed from: l */
    private final Object f5478l = new Object();

    /* renamed from: m */
    private DownloadListener f5479m = new C2801a(3, this, 0);

    /* renamed from: n */
    private C2827a f5480n = null;

    /* renamed from: o */
    private C2807d f5481o;

    /* renamed from: p */
    private boolean f5482p;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.upgrade.BetaGrayStrategy]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* renamed from: a */
    public BetaGrayStrategy mo34188a(C2959y yVar) {
        BetaGrayStrategy betaGrayStrategy;
        BetaGrayStrategy betaGrayStrategy2;
        C2959y yVar2 = yVar;
        synchronized (this.f5477k) {
            BetaGrayStrategy betaGrayStrategy3 = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
            betaGrayStrategy = null;
            if (betaGrayStrategy3 != null && betaGrayStrategy3.f5455a == null) {
                C2804a.m6269a("st.bch");
                betaGrayStrategy3 = null;
            }
            if (!(betaGrayStrategy3 == null || betaGrayStrategy3.f5455a == null || (betaGrayStrategy3.f5455a.f6286e.f6254c > C2808e.f5336E.f5389w && betaGrayStrategy3.f5455a.f6295n == 1 && ((betaGrayStrategy3.f5455a.f6287f == null || !TextUtils.equals(C2808e.f5336E.f5388v, betaGrayStrategy3.f5455a.f6287f.f6246a)) && betaGrayStrategy3.f5455a.f6297p != 3)))) {
                C2804a.m6269a("st.bch");
                this.f5469c = null;
                betaGrayStrategy3 = null;
            }
            if (yVar2 != null && yVar2.f6286e.f6254c < C2808e.f5336E.f5389w) {
                C2903an.m6857a("versionCode is too small, discard remote strategy: [new: %d] [current: %d]", Integer.valueOf(yVar2.f6286e.f6254c), Integer.valueOf(C2808e.f5336E.f5389w));
                yVar2 = null;
            }
            if (yVar2 != null) {
                if (yVar2.f6295n == 2 && betaGrayStrategy3 != null && betaGrayStrategy3.f5455a != null && !TextUtils.isEmpty(yVar2.f6294m) && !TextUtils.isEmpty(betaGrayStrategy3.f5455a.f6294m) && TextUtils.equals(yVar2.f6294m, betaGrayStrategy3.f5455a.f6294m)) {
                    C2903an.m6857a("callback strategy: %s", yVar2.f6294m);
                    C2804a.m6269a("st.bch");
                    C2808e.f5336E.f5382p.mo34042a(betaGrayStrategy3.f5455a.f6287f.f6247b, C2808e.f5336E.f5386t.getAbsolutePath(), null, null).delete(true);
                    betaGrayStrategy3 = null;
                }
                if (yVar2.f6295n != 1) {
                    C2903an.m6857a("invalid strategy: %s", yVar2.f6294m);
                    yVar2 = null;
                }
            }
            if (yVar2 != null) {
                if (betaGrayStrategy3 == null || betaGrayStrategy3.f5455a == null || TextUtils.isEmpty(yVar2.f6294m) || TextUtils.isEmpty(betaGrayStrategy3.f5455a.f6294m) || !TextUtils.equals(yVar2.f6294m, betaGrayStrategy3.f5455a.f6294m)) {
                    betaGrayStrategy2 = new BetaGrayStrategy();
                } else {
                    betaGrayStrategy2 = new BetaGrayStrategy(C2908aq.m6942d(C2908aq.m6918a(betaGrayStrategy3)));
                    C2903an.m6857a("same strategyId:[new: %s] [current: %s] keep has popup times: %d, interval: %d", yVar2.f6294m, betaGrayStrategy3.f5455a.f6294m, Integer.valueOf(betaGrayStrategy2.f5456b), Long.valueOf(yVar2.f6290i));
                }
                betaGrayStrategy2.f5455a = yVar2;
                betaGrayStrategy2.f5459e = System.currentTimeMillis();
                if (betaGrayStrategy3 != null) {
                    if (!betaGrayStrategy3.f5455a.f6287f.f6247b.equals(yVar2.f6287f.f6247b)) {
                        if (this.f5469c == null) {
                            C2808e.f5336E.f5382p.mo34042a(betaGrayStrategy3.f5455a.f6287f.f6247b, C2808e.f5336E.f5386t.getAbsolutePath(), null, null).delete(true);
                            File[] listFiles = C2808e.f5336E.f5386t.listFiles();
                            for (File file : listFiles) {
                                if (!file.delete()) {
                                    C2903an.m6865e("cannot deleteCache file:%s", file.getAbsolutePath());
                                }
                            }
                        } else {
                            BetaReceiver.netListeners.remove(this.f5469c.getDownloadUrl());
                            this.f5469c.delete(true);
                            this.f5469c = null;
                        }
                    }
                    if (betaGrayStrategy3.f5455a.f6297p == 3) {
                        File file2 = C2808e.f5336E.f5344H;
                        if (file2 != null && file2.exists() && file2.delete()) {
                            C2903an.m6857a("delete tmpPatchFile", new Object[0]);
                        }
                        File file3 = C2808e.f5336E.f5343G;
                        if (file3 != null && file3.exists() && file3.delete()) {
                            C2808e.f5336E.f5348L = "";
                            C2903an.m6857a("delete patchFile", new Object[0]);
                        }
                    }
                }
                C2804a.m6270a("st.bch", (Parcelable) betaGrayStrategy2);
                C2903an.m6857a("onUpgradeReceived: %s [type: %d]", yVar2, Integer.valueOf(yVar2.f6288g));
                C2947p.f6229a.mo34667a(new C2957w("rcv", System.currentTimeMillis(), (byte) 0, 0, yVar2.f6286e, yVar2.f6294m, yVar2.f6297p, null));
                betaGrayStrategy = betaGrayStrategy2;
            } else if (!(betaGrayStrategy3 == null || betaGrayStrategy3.f5455a == null || betaGrayStrategy3.f5455a.f6297p == 3)) {
                C2804a.m6269a("st.bch");
            }
        }
        return betaGrayStrategy;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00d3, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0143, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x016d, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x01cd, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo34191a(boolean r11, boolean r12, int r13, com.tencent.bugly.proguard.C2959y r14, java.lang.String r15) {
        /*
            r10 = this;
            java.lang.Object r15 = r10.f5477k
            monitor-enter(r15)
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r14 = r10.mo34188a(r14)     // Catch:{ all -> 0x01ce }
            r10.f5468b = r14     // Catch:{ all -> 0x01ce }
            r10.f5473g = r11     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.UpgradeListener r14 = r10.f5471e     // Catch:{ all -> 0x01ce }
            r0 = 4
            r1 = 5
            r2 = 2
            r3 = 3
            r4 = 1
            r5 = 0
            if (r14 == 0) goto L_0x0098
            java.lang.String r14 = "你已放弃让SDK来处理策略"
            java.lang.Object[] r6 = new java.lang.Object[r5]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.C2903an.m6857a(r14, r6)     // Catch:{ all -> 0x01ce }
            r10.f5476j = r3     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r14 = r10.f5468b     // Catch:{ all -> 0x01ce }
            if (r14 != 0) goto L_0x0029
            java.lang.String r14 = "betaStrategy is null"
            java.lang.Object[] r6 = new java.lang.Object[r5]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.C2903an.m6857a(r14, r6)     // Catch:{ all -> 0x01ce }
        L_0x0029:
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r14 = r10.f5468b     // Catch:{ all -> 0x01ce }
            r6 = 0
            if (r14 == 0) goto L_0x0054
            com.tencent.bugly.beta.download.DownloadTask r14 = r10.f5469c     // Catch:{ all -> 0x01ce }
            if (r14 != 0) goto L_0x0054
            com.tencent.bugly.beta.global.e r14 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.download.b r14 = r14.f5382p     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r7 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r7 = r7.f5455a     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.u r7 = r7.f6287f     // Catch:{ all -> 0x01ce }
            java.lang.String r7 = r7.f6247b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.global.e r8 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            java.io.File r8 = r8.f5386t     // Catch:{ all -> 0x01ce }
            java.lang.String r8 = r8.getAbsolutePath()     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r9 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r9 = r9.f5455a     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.u r9 = r9.f6287f     // Catch:{ all -> 0x01ce }
            java.lang.String r9 = r9.f6246a     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.download.DownloadTask r14 = r14.mo34042a(r7, r8, r6, r9)     // Catch:{ all -> 0x01ce }
            r10.f5469c = r14     // Catch:{ all -> 0x01ce }
        L_0x0054:
            com.tencent.bugly.beta.download.DownloadTask r14 = r10.f5469c     // Catch:{ all -> 0x01ce }
            if (r14 != 0) goto L_0x006b
            java.lang.String r14 = "用户自定义activity，创建task失败 [strategy:%s]"
            java.lang.Object[] r7 = new java.lang.Object[r4]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r8 = r10.f5468b     // Catch:{ all -> 0x01ce }
            r7[r5] = r8     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.C2903an.m6857a(r14, r7)     // Catch:{ all -> 0x01ce }
            r10.f5468b = r6     // Catch:{ all -> 0x01ce }
            java.lang.String r14 = "st.bch"
            com.tencent.bugly.beta.global.C2804a.m6269a(r14)     // Catch:{ all -> 0x01ce }
            goto L_0x0072
        L_0x006b:
            com.tencent.bugly.beta.download.DownloadTask r14 = r10.f5469c     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.download.DownloadListener r6 = r10.f5479m     // Catch:{ all -> 0x01ce }
            r14.addListener(r6)     // Catch:{ all -> 0x01ce }
        L_0x0072:
            com.tencent.bugly.beta.global.d r14 = new com.tencent.bugly.beta.global.d     // Catch:{ all -> 0x01ce }
            r6 = 16
            java.lang.Object[] r7 = new java.lang.Object[r1]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.UpgradeListener r8 = r10.f5471e     // Catch:{ all -> 0x01ce }
            r7[r5] = r8     // Catch:{ all -> 0x01ce }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x01ce }
            r7[r4] = r8     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r8 = r10.f5468b     // Catch:{ all -> 0x01ce }
            r7[r2] = r8     // Catch:{ all -> 0x01ce }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x01ce }
            r7[r3] = r8     // Catch:{ all -> 0x01ce }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r12)     // Catch:{ all -> 0x01ce }
            r7[r0] = r8     // Catch:{ all -> 0x01ce }
            r14.<init>(r6, r7)     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r14)     // Catch:{ all -> 0x01ce }
        L_0x0098:
            r14 = 18
            if (r13 == 0) goto L_0x00d4
            if (r11 == 0) goto L_0x00d4
            if (r12 != 0) goto L_0x00d4
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            if (r13 != 0) goto L_0x00d4
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r12 = r10.f5472f     // Catch:{ all -> 0x01ce }
            if (r12 == 0) goto L_0x00c4
            com.tencent.bugly.beta.global.d r12 = new com.tencent.bugly.beta.global.d     // Catch:{ all -> 0x01ce }
            java.lang.Object[] r13 = new java.lang.Object[r3]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r0 = r10.f5472f     // Catch:{ all -> 0x01ce }
            r13[r5] = r0     // Catch:{ all -> 0x01ce }
            r0 = -1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x01ce }
            r13[r4] = r0     // Catch:{ all -> 0x01ce }
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x01ce }
            r13[r2] = r11     // Catch:{ all -> 0x01ce }
            r12.<init>(r14, r13)     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r12)     // Catch:{ all -> 0x01ce }
            goto L_0x00d2
        L_0x00c4:
            com.tencent.bugly.beta.global.d r11 = new com.tencent.bugly.beta.global.d     // Catch:{ all -> 0x01ce }
            java.lang.Object[] r12 = new java.lang.Object[r4]     // Catch:{ all -> 0x01ce }
            java.lang.String r13 = com.tencent.bugly.beta.Beta.strToastCheckUpgradeError     // Catch:{ all -> 0x01ce }
            r12[r5] = r13     // Catch:{ all -> 0x01ce }
            r11.<init>(r1, r12)     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r11)     // Catch:{ all -> 0x01ce }
        L_0x00d2:
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x00d4:
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x019b
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r13 = r13.f5455a     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x019b
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r13 = r10.f5472f     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x00fc
            com.tencent.bugly.beta.global.d r13 = new com.tencent.bugly.beta.global.d     // Catch:{ all -> 0x01ce }
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r6 = r10.f5472f     // Catch:{ all -> 0x01ce }
            r1[r5] = r6     // Catch:{ all -> 0x01ce }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x01ce }
            r1[r4] = r5     // Catch:{ all -> 0x01ce }
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x01ce }
            r1[r2] = r5     // Catch:{ all -> 0x01ce }
            r13.<init>(r14, r1)     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r13)     // Catch:{ all -> 0x01ce }
        L_0x00fc:
            com.tencent.bugly.beta.upgrade.UpgradeListener r13 = r10.f5471e     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x0102
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x0102:
            if (r11 != 0) goto L_0x016e
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r13 = r13.f5455a     // Catch:{ all -> 0x01ce }
            byte r13 = r13.f6288g     // Catch:{ all -> 0x01ce }
            if (r13 == r2) goto L_0x016e
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            boolean r13 = r13.f5458d     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x0144
            com.tencent.bugly.beta.global.e r13 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            boolean r13 = r13.f5371e     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x0144
            com.tencent.bugly.beta.global.e r13 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            android.content.Context r13 = r13.f5385s     // Catch:{ all -> 0x01ce }
            int r13 = com.tencent.bugly.beta.global.C2804a.m6256a(r13)     // Catch:{ all -> 0x01ce }
            if (r13 != r4) goto L_0x0128
            com.tencent.bugly.beta.global.e r13 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            boolean r13 = r13.f5355S     // Catch:{ all -> 0x01ce }
            if (r13 != 0) goto L_0x0138
        L_0x0128:
            com.tencent.bugly.beta.global.e r13 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            android.content.Context r13 = r13.f5385s     // Catch:{ all -> 0x01ce }
            int r13 = com.tencent.bugly.beta.global.C2804a.m6256a(r13)     // Catch:{ all -> 0x01ce }
            if (r13 != r0) goto L_0x013d
            com.tencent.bugly.beta.global.e r13 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            boolean r13 = r13.f5356T     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x013d
        L_0x0138:
            r10.m6316c()     // Catch:{ all -> 0x01ce }
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x013d:
            if (r12 != 0) goto L_0x0142
            r10.m6314a(r11)     // Catch:{ all -> 0x01ce }
        L_0x0142:
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x0144:
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            long r13 = r13.f5457c     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r1 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r1 = r1.f5455a     // Catch:{ all -> 0x01ce }
            long r1 = r1.f6290i     // Catch:{ all -> 0x01ce }
            long r13 = r13 + r1
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x01ce }
            int r5 = (r13 > r1 ? 1 : (r13 == r1 ? 0 : -1))
            if (r5 > 0) goto L_0x016c
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r13 = r13.f5455a     // Catch:{ all -> 0x01ce }
            int r13 = r13.f6289h     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r14 = r10.f5468b     // Catch:{ all -> 0x01ce }
            int r14 = r14.f5456b     // Catch:{ all -> 0x01ce }
            int r13 = r13 - r14
            if (r13 <= 0) goto L_0x016c
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r13 = r10.f5468b     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.y r13 = r13.f5455a     // Catch:{ all -> 0x01ce }
            byte r13 = r13.f6288g     // Catch:{ all -> 0x01ce }
            if (r13 != r3) goto L_0x016e
        L_0x016c:
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x016e:
            if (r12 != 0) goto L_0x01cc
            com.tencent.bugly.beta.global.e r12 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            android.content.Context r12 = r12.f5385s     // Catch:{ all -> 0x01ce }
            int r12 = com.tencent.bugly.beta.global.C2804a.m6256a(r12)     // Catch:{ all -> 0x01ce }
            if (r12 != r4) goto L_0x0180
            com.tencent.bugly.beta.global.e r12 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            boolean r12 = r12.f5355S     // Catch:{ all -> 0x01ce }
            if (r12 != 0) goto L_0x0190
        L_0x0180:
            com.tencent.bugly.beta.global.e r12 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            android.content.Context r12 = r12.f5385s     // Catch:{ all -> 0x01ce }
            int r12 = com.tencent.bugly.beta.global.C2804a.m6256a(r12)     // Catch:{ all -> 0x01ce }
            if (r12 != r0) goto L_0x0197
            com.tencent.bugly.beta.global.e r12 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01ce }
            boolean r12 = r12.f5356T     // Catch:{ all -> 0x01ce }
            if (r12 == 0) goto L_0x0197
        L_0x0190:
            if (r11 != 0) goto L_0x0197
            r10.m6316c()     // Catch:{ all -> 0x01ce }
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x0197:
            r10.m6314a(r11)     // Catch:{ all -> 0x01ce }
            goto L_0x01cc
        L_0x019b:
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r13 = r10.f5472f     // Catch:{ all -> 0x01ce }
            if (r13 == 0) goto L_0x01ba
            com.tencent.bugly.beta.global.d r12 = new com.tencent.bugly.beta.global.d     // Catch:{ all -> 0x01ce }
            java.lang.Object[] r13 = new java.lang.Object[r3]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r0 = r10.f5472f     // Catch:{ all -> 0x01ce }
            r13[r5] = r0     // Catch:{ all -> 0x01ce }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x01ce }
            r13[r4] = r0     // Catch:{ all -> 0x01ce }
            java.lang.Boolean r11 = java.lang.Boolean.valueOf(r11)     // Catch:{ all -> 0x01ce }
            r13[r2] = r11     // Catch:{ all -> 0x01ce }
            r12.<init>(r14, r13)     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r12)     // Catch:{ all -> 0x01ce }
            goto L_0x01cc
        L_0x01ba:
            if (r11 == 0) goto L_0x01cc
            if (r12 != 0) goto L_0x01cc
            com.tencent.bugly.beta.global.d r11 = new com.tencent.bugly.beta.global.d     // Catch:{ all -> 0x01ce }
            java.lang.Object[] r12 = new java.lang.Object[r4]     // Catch:{ all -> 0x01ce }
            java.lang.String r13 = com.tencent.bugly.beta.Beta.strToastYourAreTheLatestVersion     // Catch:{ all -> 0x01ce }
            r12[r5] = r13     // Catch:{ all -> 0x01ce }
            r11.<init>(r1, r12)     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r11)     // Catch:{ all -> 0x01ce }
        L_0x01cc:
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            return
        L_0x01ce:
            r11 = move-exception
            monitor-exit(r15)     // Catch:{ all -> 0x01ce }
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.upgrade.C2829c.mo34191a(boolean, boolean, int, com.tencent.bugly.proguard.y, java.lang.String):void");
    }

    /* renamed from: a */
    private C2959y m6313a() {
        BetaGrayStrategy betaGrayStrategy = this.f5468b;
        if (betaGrayStrategy == null) {
            return null;
        }
        return betaGrayStrategy.f5455a;
    }

    /* renamed from: b */
    private DownloadTask m6315b() {
        C2959y a = m6313a();
        if (a == null) {
            return null;
        }
        if (this.f5469c == null) {
            this.f5469c = C2808e.f5336E.f5382p.mo34042a(a.f6287f.f6247b, C2808e.f5336E.f5386t.getAbsolutePath(), null, this.f5468b.f5455a.f6287f.f6246a);
        }
        return this.f5469c;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.upgrade.BetaGrayStrategy]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* renamed from: c */
    private void m6316c() {
        C2959y a = m6313a();
        if (a != null) {
            if (this.f5469c == null) {
                this.f5469c = m6315b();
            }
            if (this.f5469c != null) {
                C2804a.m6270a("st.bch", (Parcelable) this.f5468b);
                BetaReceiver.addTask(this.f5469c);
                if (this.f5469c.getStatus() != 1) {
                    this.f5469c.download();
                } else if (this.f5473g && C2804a.m6266a(C2808e.f5336E.f5385s, this.f5469c.getSaveFile(), a.f6287f.f6246a)) {
                    C2947p.f6229a.mo34667a(new C2957w("install", System.currentTimeMillis(), (byte) 0, 0, a.f6286e, a.f6294m, a.f6297p, null));
                } else if (C2808e.f5336E.f5370d) {
                    m6314a(this.f5473g);
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.upgrade.BetaGrayStrategy]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* renamed from: a */
    private void m6314a(boolean z) {
        C2959y a = m6313a();
        if (a != null) {
            boolean z2 = false;
            if (System.currentTimeMillis() <= a.mo34695a() - 86400000) {
                C2903an.m6865e(System.currentTimeMillis() + "ms", new Object[0]);
                return;
            }
            C2809f.f5393a.mo34057a(C2808e.f5336E.f5382p, a.f6293l);
            if (this.f5469c == null) {
                this.f5469c = m6315b();
            }
            DownloadTask downloadTask = this.f5469c;
            if (downloadTask != null) {
                if (z || downloadTask.getStatus() != 2) {
                    this.f5469c.addListener(this.f5479m);
                    DownloadListener downloadListener = this.f5470d;
                    if (downloadListener != null) {
                        this.f5469c.addListener(downloadListener);
                    }
                    C2826h hVar = C2826h.f5440v;
                    hVar.mo34164a(a, this.f5469c);
                    hVar.f5447r = new C2807d(3, this.f5468b, this.f5469c);
                    hVar.f5448s = new C2807d(4, this.f5468b, this.f5469c, Boolean.valueOf(z));
                    this.f5468b.f5457c = System.currentTimeMillis();
                    C2804a.m6270a("st.bch", (Parcelable) this.f5468b);
                    if (z) {
                        C2809f.f5393a.mo34059a(new C2807d(2, hVar, Boolean.valueOf(z)), 3000);
                        return;
                    }
                    C2809f fVar = C2809f.f5393a;
                    Object[] objArr = new Object[2];
                    objArr[0] = hVar;
                    if (z || a.f6288g == 2) {
                        z2 = true;
                    }
                    objArr[1] = Boolean.valueOf(z2);
                    fVar.mo34058a(new C2807d(2, objArr));
                    return;
                }
                C2903an.m6857a("Task is downloading %s %s", a.f6294m, this.f5469c.getDownloadUrl());
            }
        }
    }

    /* renamed from: a */
    public void mo34190a(boolean z, boolean z2, int i) {
        boolean z3 = z;
        synchronized (this.f5478l) {
            BetaGrayStrategy betaGrayStrategy = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
            if (z3 && betaGrayStrategy != null && betaGrayStrategy.f5455a != null && betaGrayStrategy.f5455a.f6297p == 3) {
                betaGrayStrategy = null;
            }
            BetaGrayStrategy betaGrayStrategy2 = betaGrayStrategy;
            if (this.f5480n != null && !this.f5480n.f5465d) {
                if (this.f5482p == z3) {
                    synchronized (this.f5480n) {
                        this.f5480n.f5464c[0] = Boolean.valueOf(z);
                        this.f5480n.f5464c[1] = Boolean.valueOf(z2);
                    }
                    this.f5481o.f5335b[0] = false;
                    if ((z3 && !z2) || !(betaGrayStrategy2 == null || betaGrayStrategy2.f5455a == null || betaGrayStrategy2.f5455a.f6297p != 2)) {
                        C2836e.m6400b(this.f5481o);
                        C2836e.m6399a(this.f5481o, 6000);
                    }
                }
            }
            this.f5482p = z3;
            if (this.f5480n != null) {
                this.f5480n.f5465d = true;
            }
            this.f5480n = new C2827a(1, 804, Boolean.valueOf(z), Boolean.valueOf(z2), betaGrayStrategy2);
            this.f5481o = new C2807d(12, false, this.f5480n);
            String str = "";
            long j = 0;
            if (betaGrayStrategy2 != null) {
                try {
                    if (betaGrayStrategy2.f5455a != null) {
                        str = betaGrayStrategy2.f5455a.f6294m;
                        j = betaGrayStrategy2.f5455a.f6296o;
                    }
                } catch (Throwable th) {
                    if (!C2903an.m6861b(th)) {
                        th.printStackTrace();
                    }
                }
            }
            String str2 = str;
            long j2 = j;
            HashMap hashMap = new HashMap();
            hashMap.put("G16", C2808e.f5336E.f5348L);
            C2828b.f5466a.mo34186a(804, C2893ah.m6794a((C2944m) new C2960z(z3 ? 1 : 0, str2, j2, hashMap)), this.f5480n, z, C2808e.f5336E.f5342F.f5460a.f6158e);
            C2836e.m6400b(this.f5481o);
            C2836e.m6399a(this.f5481o, 6000);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.upgrade.BetaUploadStrategy]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* renamed from: a */
    public void mo34189a(C2930bk bkVar) {
        if (C2808e.f5336E.f5342F == null) {
            C2808e.f5336E.f5342F = new BetaUploadStrategy();
        }
        if (bkVar != null && C2808e.f5336E.f5342F.f5461b != bkVar.f6161h) {
            C2808e.f5336E.f5342F.f5461b = bkVar.f6161h;
            C2808e.f5336E.f5342F.f5460a.f6155b = bkVar.f6155b;
            C2808e.f5336E.f5342F.f5460a.f6156c = bkVar.f6156c;
            C2808e.f5336E.f5342F.f5460a.f6161h = bkVar.f6161h;
            if (C2908aq.m6940c(bkVar.f6157d)) {
                C2808e.f5336E.f5342F.f5460a.f6157d = bkVar.f6157d;
            }
            if (C2908aq.m6940c(bkVar.f6158e)) {
                C2808e.f5336E.f5342F.f5460a.f6158e = bkVar.f6158e;
            }
            if (bkVar.f6159f != null && !TextUtils.isEmpty(bkVar.f6159f.f6149a)) {
                C2808e.f5336E.f5342F.f5460a.f6159f.f6149a = bkVar.f6159f.f6149a;
            }
            if (bkVar.f6160g != null && bkVar.f6160g.size() > 0) {
                C2808e.f5336E.f5342F.f5460a.f6160g = bkVar.f6160g;
            }
            if (C2908aq.m6940c(bkVar.f6162i)) {
                C2808e.f5336E.f5342F.f5460a.f6162i = bkVar.f6162i;
            }
            if (C2908aq.m6940c(bkVar.f6163j)) {
                C2808e.f5336E.f5342F.f5460a.f6163j = bkVar.f6163j;
            }
            C2804a.m6270a("us.bch", (Parcelable) C2808e.f5336E.f5342F);
        }
    }
}
