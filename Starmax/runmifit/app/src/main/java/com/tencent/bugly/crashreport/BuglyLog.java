package com.tencent.bugly.crashreport;

import android.util.Log;
import com.tencent.bugly.C2797b;
import com.tencent.bugly.proguard.C2905ap;
import freemarker.template.Template;

/* compiled from: BUGLY */
public class BuglyLog {
    /* renamed from: v */
    public static void m6406v(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C2797b.f5303c) {
            Log.v(str, str2);
        }
        C2905ap.m6874a("V", str, str2);
    }

    /* renamed from: d */
    public static void m6402d(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C2797b.f5303c) {
            Log.d(str, str2);
        }
        C2905ap.m6874a(Template.DEFAULT_NAMESPACE_PREFIX, str, str2);
    }

    /* renamed from: i */
    public static void m6405i(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C2797b.f5303c) {
            Log.i(str, str2);
        }
        C2905ap.m6874a("I", str, str2);
    }

    /* renamed from: w */
    public static void m6407w(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C2797b.f5303c) {
            Log.w(str, str2);
        }
        C2905ap.m6874a("W", str, str2);
    }

    /* renamed from: e */
    public static void m6403e(String str, String str2) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C2797b.f5303c) {
            Log.e(str, str2);
        }
        C2905ap.m6874a("E", str, str2);
    }

    /* renamed from: e */
    public static void m6404e(String str, String str2, Throwable th) {
        if (str == null) {
            str = "";
        }
        if (str2 == null) {
            str2 = "null";
        }
        if (C2797b.f5303c) {
            Log.e(str, str2, th);
        }
        C2905ap.m6875a("E", str, th);
    }

    public static void setCache(int i) {
        C2905ap.m6872a(i);
    }
}
