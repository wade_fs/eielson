package com.tencent.bugly.proguard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.tencent.bugly.BUGLY;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.ae */
/* compiled from: BUGLY */
public class C2889ae {

    /* renamed from: a */
    public static boolean f5939a = false;

    /* renamed from: b */
    private static C2889ae f5940b;

    /* renamed from: c */
    private static C2891af f5941c;

    private C2889ae(Context context, List<BUGLY> list) {
        f5941c = new C2891af(context, list);
    }

    /* renamed from: a */
    public static synchronized C2889ae m6758a(Context context, List<BUGLY> list) {
        C2889ae aeVar;
        synchronized (C2889ae.class) {
            if (f5940b == null) {
                f5940b = new C2889ae(context, list);
            }
            aeVar = f5940b;
        }
        return aeVar;
    }

    /* renamed from: a */
    public static synchronized C2889ae m6757a() {
        C2889ae aeVar;
        synchronized (C2889ae.class) {
            aeVar = f5940b;
        }
        return aeVar;
    }

    /* renamed from: a */
    public long mo34489a(String str, ContentValues contentValues, C2888ad adVar, boolean z) {
        if (z) {
            return m6754a(str, contentValues, adVar);
        }
        C2890a aVar = new C2890a(1, adVar);
        aVar.mo34506a(str, contentValues);
        C2901am.m6848a().mo34547a(aVar);
        return 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(boolean, java.lang.String, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.bugly.proguard.ad, boolean):android.database.Cursor
     arg types: [int, java.lang.String, java.lang.String[], java.lang.String, java.lang.String[], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], com.tencent.bugly.proguard.ad, boolean]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, boolean, java.lang.String, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.bugly.proguard.ad):android.database.Cursor
      com.tencent.bugly.proguard.ae.a(boolean, java.lang.String, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.bugly.proguard.ad, boolean):android.database.Cursor */
    /* renamed from: a */
    public Cursor mo34490a(String str, String[] strArr, String str2, String[] strArr2, C2888ad adVar, boolean z) {
        return mo34491a(false, str, strArr, str2, strArr2, (String) null, (String) null, (String) null, (String) null, adVar, z);
    }

    /* renamed from: a */
    public Cursor mo34491a(boolean z, String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6, C2888ad adVar, boolean z2) {
        if (z2) {
            return m6756a(z, str, strArr, str2, strArr2, str3, str4, str5, str6, adVar);
        }
        C2890a aVar = new C2890a(3, adVar);
        aVar.mo34508a(z, str, strArr, str2, strArr2, str3, str4, str5, str6);
        C2901am.m6848a().mo34547a(aVar);
        return null;
    }

    /* renamed from: a */
    public int mo34488a(String str, String str2, String[] strArr, C2888ad adVar, boolean z) {
        if (z) {
            return m6752a(str, str2, strArr, adVar);
        }
        C2890a aVar = new C2890a(2, adVar);
        aVar.mo34507a(str, str2, strArr);
        C2901am.m6848a().mo34547a(aVar);
        return 0;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003b, code lost:
        if (r0 != null) goto L_0x003d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003d, code lost:
        r0.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005a, code lost:
        if (r0 != null) goto L_0x003d;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized long m6754a(java.lang.String r8, android.content.ContentValues r9, com.tencent.bugly.proguard.C2888ad r10) {
        /*
            r7 = this;
            monitor-enter(r7)
            r0 = 0
            r1 = 0
            com.tencent.bugly.proguard.af r3 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x0043 }
            android.database.sqlite.SQLiteDatabase r0 = r3.getWritableDatabase()     // Catch:{ all -> 0x0043 }
            if (r0 == 0) goto L_0x002e
            if (r9 == 0) goto L_0x002e
            java.lang.String r3 = "_id"
            long r3 = r0.replace(r8, r3, r9)     // Catch:{ all -> 0x0043 }
            r9 = 0
            r5 = 1
            int r6 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r6 < 0) goto L_0x0024
            java.lang.String r6 = "[Database] insert %s success."
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0043 }
            r5[r9] = r8     // Catch:{ all -> 0x0043 }
            com.tencent.bugly.proguard.C2903an.m6863c(r6, r5)     // Catch:{ all -> 0x0043 }
            goto L_0x002d
        L_0x0024:
            java.lang.String r6 = "[Database] replace %s error."
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0043 }
            r5[r9] = r8     // Catch:{ all -> 0x0043 }
            com.tencent.bugly.proguard.C2903an.m6864d(r6, r5)     // Catch:{ all -> 0x0043 }
        L_0x002d:
            r1 = r3
        L_0x002e:
            if (r10 == 0) goto L_0x0037
            java.lang.Long r8 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0041 }
            r10.mo34487a(r8)     // Catch:{ all -> 0x0041 }
        L_0x0037:
            boolean r8 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0041 }
            if (r8 == 0) goto L_0x005d
            if (r0 == 0) goto L_0x005d
        L_0x003d:
            r0.close()     // Catch:{ all -> 0x0041 }
            goto L_0x005d
        L_0x0041:
            r8 = move-exception
            goto L_0x0073
        L_0x0043:
            r8 = move-exception
            boolean r9 = com.tencent.bugly.proguard.C2903an.m6858a(r8)     // Catch:{ all -> 0x005f }
            if (r9 != 0) goto L_0x004d
            r8.printStackTrace()     // Catch:{ all -> 0x005f }
        L_0x004d:
            if (r10 == 0) goto L_0x0056
            java.lang.Long r8 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0041 }
            r10.mo34487a(r8)     // Catch:{ all -> 0x0041 }
        L_0x0056:
            boolean r8 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0041 }
            if (r8 == 0) goto L_0x005d
            if (r0 == 0) goto L_0x005d
            goto L_0x003d
        L_0x005d:
            monitor-exit(r7)
            return r1
        L_0x005f:
            r8 = move-exception
            if (r10 == 0) goto L_0x0069
            java.lang.Long r9 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x0041 }
            r10.mo34487a(r9)     // Catch:{ all -> 0x0041 }
        L_0x0069:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0041 }
            if (r9 == 0) goto L_0x0072
            if (r0 == 0) goto L_0x0072
            r0.close()     // Catch:{ all -> 0x0041 }
        L_0x0072:
            throw r8     // Catch:{ all -> 0x0041 }
        L_0x0073:
            monitor-exit(r7)
            goto L_0x0076
        L_0x0075:
            throw r8
        L_0x0076:
            goto L_0x0075
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6754a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0032, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0020, code lost:
        if (r1 != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:?, code lost:
        r1.mo34487a(r2);
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized android.database.Cursor m6756a(boolean r14, java.lang.String r15, java.lang.String[] r16, java.lang.String r17, java.lang.String[] r18, java.lang.String r19, java.lang.String r20, java.lang.String r21, java.lang.String r22, com.tencent.bugly.proguard.C2888ad r23) {
        /*
            r13 = this;
            r1 = r23
            monitor-enter(r13)
            r2 = 0
            com.tencent.bugly.proguard.af r0 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x0028 }
            android.database.sqlite.SQLiteDatabase r3 = r0.getWritableDatabase()     // Catch:{ all -> 0x0028 }
            if (r3 == 0) goto L_0x0020
            r4 = r14
            r5 = r15
            r6 = r16
            r7 = r17
            r8 = r18
            r9 = r19
            r10 = r20
            r11 = r21
            r12 = r22
            android.database.Cursor r2 = r3.query(r4, r5, r6, r7, r8, r9, r10, r11, r12)     // Catch:{ all -> 0x0028 }
        L_0x0020:
            if (r1 == 0) goto L_0x0035
        L_0x0022:
            r1.mo34487a(r2)     // Catch:{ all -> 0x0026 }
            goto L_0x0035
        L_0x0026:
            r0 = move-exception
            goto L_0x003e
        L_0x0028:
            r0 = move-exception
            boolean r3 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x0037 }
            if (r3 != 0) goto L_0x0032
            r0.printStackTrace()     // Catch:{ all -> 0x0037 }
        L_0x0032:
            if (r1 == 0) goto L_0x0035
            goto L_0x0022
        L_0x0035:
            monitor-exit(r13)
            return r2
        L_0x0037:
            r0 = move-exception
            if (r1 == 0) goto L_0x003d
            r1.mo34487a(r2)     // Catch:{ all -> 0x0026 }
        L_0x003d:
            throw r0     // Catch:{ all -> 0x0026 }
        L_0x003e:
            monitor-exit(r13)
            goto L_0x0041
        L_0x0040:
            throw r0
        L_0x0041:
            goto L_0x0040
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6756a(boolean, java.lang.String, java.lang.String[], java.lang.String, java.lang.String[], java.lang.String, java.lang.String, java.lang.String, java.lang.String, com.tencent.bugly.proguard.ad):android.database.Cursor");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001c, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001e, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003b, code lost:
        if (r1 != null) goto L_0x001e;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized int m6752a(java.lang.String r4, java.lang.String r5, java.lang.String[] r6, com.tencent.bugly.proguard.C2888ad r7) {
        /*
            r3 = this;
            monitor-enter(r3)
            r0 = 0
            r1 = 0
            com.tencent.bugly.proguard.af r2 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x0024 }
            android.database.sqlite.SQLiteDatabase r1 = r2.getWritableDatabase()     // Catch:{ all -> 0x0024 }
            if (r1 == 0) goto L_0x000f
            int r0 = r1.delete(r4, r5, r6)     // Catch:{ all -> 0x0024 }
        L_0x000f:
            if (r7 == 0) goto L_0x0018
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0022 }
            r7.mo34487a(r4)     // Catch:{ all -> 0x0022 }
        L_0x0018:
            boolean r4 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0022 }
            if (r4 == 0) goto L_0x003e
            if (r1 == 0) goto L_0x003e
        L_0x001e:
            r1.close()     // Catch:{ all -> 0x0022 }
            goto L_0x003e
        L_0x0022:
            r4 = move-exception
            goto L_0x0054
        L_0x0024:
            r4 = move-exception
            boolean r5 = com.tencent.bugly.proguard.C2903an.m6858a(r4)     // Catch:{ all -> 0x0040 }
            if (r5 != 0) goto L_0x002e
            r4.printStackTrace()     // Catch:{ all -> 0x0040 }
        L_0x002e:
            if (r7 == 0) goto L_0x0037
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0022 }
            r7.mo34487a(r4)     // Catch:{ all -> 0x0022 }
        L_0x0037:
            boolean r4 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0022 }
            if (r4 == 0) goto L_0x003e
            if (r1 == 0) goto L_0x003e
            goto L_0x001e
        L_0x003e:
            monitor-exit(r3)
            return r0
        L_0x0040:
            r4 = move-exception
            if (r7 == 0) goto L_0x004a
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x0022 }
            r7.mo34487a(r5)     // Catch:{ all -> 0x0022 }
        L_0x004a:
            boolean r5 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0022 }
            if (r5 == 0) goto L_0x0053
            if (r1 == 0) goto L_0x0053
            r1.close()     // Catch:{ all -> 0x0022 }
        L_0x0053:
            throw r4     // Catch:{ all -> 0x0022 }
        L_0x0054:
            monitor-exit(r3)
            goto L_0x0057
        L_0x0056:
            throw r4
        L_0x0057:
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6752a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int");
    }

    /* renamed from: a */
    public boolean mo34497a(int i, String str, byte[] bArr, C2888ad adVar, boolean z) {
        if (z) {
            return m6762a(i, str, bArr, adVar);
        }
        C2890a aVar = new C2890a(4, adVar);
        aVar.mo34505a(i, str, bArr);
        C2901am.m6848a().mo34547a(aVar);
        return true;
    }

    /* renamed from: a */
    public Map<String, byte[]> mo34494a(int i, C2888ad adVar, boolean z) {
        if (z) {
            return m6759a(i, adVar);
        }
        C2890a aVar = new C2890a(5, adVar);
        aVar.mo34503a(i);
        C2901am.m6848a().mo34547a(aVar);
        return null;
    }

    /* renamed from: a */
    public boolean mo34496a(int i, String str, C2888ad adVar, boolean z) {
        if (z) {
            return m6761a(i, str, adVar);
        }
        C2890a aVar = new C2890a(6, adVar);
        aVar.mo34504a(i, str);
        C2901am.m6848a().mo34547a(aVar);
        return false;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x002b, code lost:
        if (r8 != null) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x002e, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (r8 != null) goto L_0x0019;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0019, code lost:
        r8.mo34487a(java.lang.Boolean.valueOf(r0));
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m6762a(int r5, java.lang.String r6, byte[] r7, com.tencent.bugly.proguard.C2888ad r8) {
        /*
            r4 = this;
            r0 = 0
            com.tencent.bugly.proguard.ag r1 = new com.tencent.bugly.proguard.ag     // Catch:{ all -> 0x0021 }
            r1.<init>()     // Catch:{ all -> 0x0021 }
            long r2 = (long) r5     // Catch:{ all -> 0x0021 }
            r1.f5964a = r2     // Catch:{ all -> 0x0021 }
            r1.f5969f = r6     // Catch:{ all -> 0x0021 }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0021 }
            r1.f5968e = r5     // Catch:{ all -> 0x0021 }
            r1.f5970g = r7     // Catch:{ all -> 0x0021 }
            boolean r0 = r4.m6766d(r1)     // Catch:{ all -> 0x0021 }
            if (r8 == 0) goto L_0x002e
        L_0x0019:
            java.lang.Boolean r5 = java.lang.Boolean.valueOf(r0)
            r8.mo34487a(r5)
            goto L_0x002e
        L_0x0021:
            r5 = move-exception
            boolean r6 = com.tencent.bugly.proguard.C2903an.m6858a(r5)     // Catch:{ all -> 0x002f }
            if (r6 != 0) goto L_0x002b
            r5.printStackTrace()     // Catch:{ all -> 0x002f }
        L_0x002b:
            if (r8 == 0) goto L_0x002e
            goto L_0x0019
        L_0x002e:
            return r0
        L_0x002f:
            r5 = move-exception
            if (r8 == 0) goto L_0x0039
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r0)
            r8.mo34487a(r6)
        L_0x0039:
            goto L_0x003b
        L_0x003a:
            throw r5
        L_0x003b:
            goto L_0x003a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6762a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002b, code lost:
        if (r5 != null) goto L_0x002d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002d, code lost:
        r5.mo34487a(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
        if (r5 == null) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003e, code lost:
        return r0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0038 A[Catch:{ all -> 0x003f }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.Map<java.lang.String, byte[]> m6759a(int r4, com.tencent.bugly.proguard.C2888ad r5) {
        /*
            r3 = this;
            r0 = 0
            java.util.List r4 = r3.m6765c(r4)     // Catch:{ all -> 0x0031 }
            if (r4 == 0) goto L_0x002b
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x0031 }
            r1.<init>()     // Catch:{ all -> 0x0031 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0028 }
        L_0x0010:
            boolean r0 = r4.hasNext()     // Catch:{ all -> 0x0028 }
            if (r0 == 0) goto L_0x0026
            java.lang.Object r0 = r4.next()     // Catch:{ all -> 0x0028 }
            com.tencent.bugly.proguard.ag r0 = (com.tencent.bugly.proguard.C2892ag) r0     // Catch:{ all -> 0x0028 }
            byte[] r2 = r0.f5970g     // Catch:{ all -> 0x0028 }
            if (r2 == 0) goto L_0x0010
            java.lang.String r0 = r0.f5969f     // Catch:{ all -> 0x0028 }
            r1.put(r0, r2)     // Catch:{ all -> 0x0028 }
            goto L_0x0010
        L_0x0026:
            r0 = r1
            goto L_0x002b
        L_0x0028:
            r4 = move-exception
            r0 = r1
            goto L_0x0032
        L_0x002b:
            if (r5 == 0) goto L_0x003e
        L_0x002d:
            r5.mo34487a(r0)
            goto L_0x003e
        L_0x0031:
            r4 = move-exception
        L_0x0032:
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r4)     // Catch:{ all -> 0x003f }
            if (r1 != 0) goto L_0x003b
            r4.printStackTrace()     // Catch:{ all -> 0x003f }
        L_0x003b:
            if (r5 == 0) goto L_0x003e
            goto L_0x002d
        L_0x003e:
            return r0
        L_0x003f:
            r4 = move-exception
            if (r5 == 0) goto L_0x0045
            r5.mo34487a(r0)
        L_0x0045:
            goto L_0x0047
        L_0x0046:
            throw r4
        L_0x0047:
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6759a(int, com.tencent.bugly.proguard.ad):java.util.Map");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0046, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0051, code lost:
        return false;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0047=Splitter:B:28:0x0047, B:41:0x005e=Splitter:B:41:0x005e} */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean mo34498a(com.tencent.bugly.proguard.C2892ag r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 0
            if (r9 != 0) goto L_0x0006
            monitor-exit(r8)
            return r0
        L_0x0006:
            r1 = 0
            com.tencent.bugly.proguard.af r2 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x0054 }
            android.database.sqlite.SQLiteDatabase r1 = r2.getWritableDatabase()     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0047
            android.content.ContentValues r2 = r8.mo34499b(r9)     // Catch:{ all -> 0x0054 }
            if (r2 == 0) goto L_0x0047
            java.lang.String r3 = "t_lr"
            java.lang.String r4 = "_id"
            long r2 = r1.replace(r3, r4, r2)     // Catch:{ all -> 0x0054 }
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 < 0) goto L_0x003c
            java.lang.String r4 = "[Database] insert %s success."
            r5 = 1
            java.lang.Object[] r6 = new java.lang.Object[r5]     // Catch:{ all -> 0x0054 }
            java.lang.String r7 = "t_lr"
            r6[r0] = r7     // Catch:{ all -> 0x0054 }
            com.tencent.bugly.proguard.C2903an.m6863c(r4, r6)     // Catch:{ all -> 0x0054 }
            r9.f5964a = r2     // Catch:{ all -> 0x0054 }
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x003a
            if (r1 == 0) goto L_0x003a
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x003a:
            monitor-exit(r8)
            return r5
        L_0x003c:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x0045
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0045:
            monitor-exit(r8)
            return r0
        L_0x0047:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x0050
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0050:
            monitor-exit(r8)
            return r0
        L_0x0052:
            r9 = move-exception
            goto L_0x0074
        L_0x0054:
            r9 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r9)     // Catch:{ all -> 0x0069 }
            if (r2 != 0) goto L_0x005e
            r9.printStackTrace()     // Catch:{ all -> 0x0069 }
        L_0x005e:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x0067
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0067:
            monitor-exit(r8)
            return r0
        L_0x0069:
            r9 = move-exception
            boolean r0 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0073
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0073:
            throw r9     // Catch:{ all -> 0x0052 }
        L_0x0074:
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.mo34498a(com.tencent.bugly.proguard.ag):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:20:0x003b, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0046, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0051, code lost:
        return false;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:28:0x0047=Splitter:B:28:0x0047, B:41:0x005e=Splitter:B:41:0x005e} */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean m6766d(com.tencent.bugly.proguard.C2892ag r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 0
            if (r9 != 0) goto L_0x0006
            monitor-exit(r8)
            return r0
        L_0x0006:
            r1 = 0
            com.tencent.bugly.proguard.af r2 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x0054 }
            android.database.sqlite.SQLiteDatabase r1 = r2.getWritableDatabase()     // Catch:{ all -> 0x0054 }
            if (r1 == 0) goto L_0x0047
            android.content.ContentValues r2 = r8.mo34502c(r9)     // Catch:{ all -> 0x0054 }
            if (r2 == 0) goto L_0x0047
            java.lang.String r3 = "t_pf"
            java.lang.String r4 = "_id"
            long r2 = r1.replace(r3, r4, r2)     // Catch:{ all -> 0x0054 }
            r4 = 0
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 < 0) goto L_0x003c
            java.lang.String r4 = "[Database] insert %s success."
            r5 = 1
            java.lang.Object[] r6 = new java.lang.Object[r5]     // Catch:{ all -> 0x0054 }
            java.lang.String r7 = "t_pf"
            r6[r0] = r7     // Catch:{ all -> 0x0054 }
            com.tencent.bugly.proguard.C2903an.m6863c(r4, r6)     // Catch:{ all -> 0x0054 }
            r9.f5964a = r2     // Catch:{ all -> 0x0054 }
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x003a
            if (r1 == 0) goto L_0x003a
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x003a:
            monitor-exit(r8)
            return r5
        L_0x003c:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x0045
            if (r1 == 0) goto L_0x0045
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0045:
            monitor-exit(r8)
            return r0
        L_0x0047:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x0050
            if (r1 == 0) goto L_0x0050
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0050:
            monitor-exit(r8)
            return r0
        L_0x0052:
            r9 = move-exception
            goto L_0x0074
        L_0x0054:
            r9 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r9)     // Catch:{ all -> 0x0069 }
            if (r2 != 0) goto L_0x005e
            r9.printStackTrace()     // Catch:{ all -> 0x0069 }
        L_0x005e:
            boolean r9 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r9 == 0) goto L_0x0067
            if (r1 == 0) goto L_0x0067
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0067:
            monitor-exit(r8)
            return r0
        L_0x0069:
            r9 = move-exception
            boolean r0 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0052 }
            if (r0 == 0) goto L_0x0073
            if (r1 == 0) goto L_0x0073
            r1.close()     // Catch:{ all -> 0x0052 }
        L_0x0073:
            throw r9     // Catch:{ all -> 0x0052 }
        L_0x0074:
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6766d(com.tencent.bugly.proguard.ag):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0042, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00ba, code lost:
        return r2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00c2 A[Catch:{ all -> 0x00d4 }] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00c7 A[SYNTHETIC, Splitter:B:54:0x00c7] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.util.List<com.tencent.bugly.proguard.C2892ag> mo34493a(int r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            com.tencent.bugly.proguard.af r0 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x00e6 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x00e6 }
            r9 = 0
            if (r0 == 0) goto L_0x00e4
            if (r11 < 0) goto L_0x0024
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x001f }
            r1.<init>()     // Catch:{ all -> 0x001f }
            java.lang.String r2 = "_tp = "
            r1.append(r2)     // Catch:{ all -> 0x001f }
            r1.append(r11)     // Catch:{ all -> 0x001f }
            java.lang.String r11 = r1.toString()     // Catch:{ all -> 0x001f }
            r4 = r11
            goto L_0x0025
        L_0x001f:
            r11 = move-exception
            r1 = r11
            r11 = r9
            goto L_0x00bc
        L_0x0024:
            r4 = r9
        L_0x0025:
            java.lang.String r2 = "t_lr"
            r3 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r1 = r0
            android.database.Cursor r11 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x001f }
            if (r11 != 0) goto L_0x0043
            if (r11 == 0) goto L_0x0038
            r11.close()     // Catch:{ all -> 0x00e6 }
        L_0x0038:
            boolean r11 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00e6 }
            if (r11 == 0) goto L_0x0041
            if (r0 == 0) goto L_0x0041
            r0.close()     // Catch:{ all -> 0x00e6 }
        L_0x0041:
            monitor-exit(r10)
            return r9
        L_0x0043:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bb }
            r1.<init>()     // Catch:{ all -> 0x00bb }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x00bb }
            r2.<init>()     // Catch:{ all -> 0x00bb }
        L_0x004d:
            boolean r3 = r11.moveToNext()     // Catch:{ all -> 0x00bb }
            r4 = 0
            if (r3 == 0) goto L_0x0083
            com.tencent.bugly.proguard.ag r3 = r10.mo34492a(r11)     // Catch:{ all -> 0x00bb }
            if (r3 == 0) goto L_0x005e
            r2.add(r3)     // Catch:{ all -> 0x00bb }
            goto L_0x004d
        L_0x005e:
            java.lang.String r3 = "_id"
            int r3 = r11.getColumnIndex(r3)     // Catch:{ all -> 0x007b }
            long r5 = r11.getLong(r3)     // Catch:{ all -> 0x007b }
            java.lang.String r3 = " or "
            r1.append(r3)     // Catch:{ all -> 0x007b }
            java.lang.String r3 = "_id"
            r1.append(r3)     // Catch:{ all -> 0x007b }
            java.lang.String r3 = " = "
            r1.append(r3)     // Catch:{ all -> 0x007b }
            r1.append(r5)     // Catch:{ all -> 0x007b }
            goto L_0x004d
        L_0x007b:
            java.lang.String r3 = "[Database] unknown id."
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00bb }
            com.tencent.bugly.proguard.C2903an.m6864d(r3, r4)     // Catch:{ all -> 0x00bb }
            goto L_0x004d
        L_0x0083:
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00bb }
            int r3 = r1.length()     // Catch:{ all -> 0x00bb }
            if (r3 <= 0) goto L_0x00ab
            r3 = 4
            java.lang.String r1 = r1.substring(r3)     // Catch:{ all -> 0x00bb }
            java.lang.String r3 = "t_lr"
            int r1 = r0.delete(r3, r1, r9)     // Catch:{ all -> 0x00bb }
            java.lang.String r3 = "[Database] deleted %s illegal data %d"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00bb }
            java.lang.String r6 = "t_lr"
            r5[r4] = r6     // Catch:{ all -> 0x00bb }
            r4 = 1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x00bb }
            r5[r4] = r1     // Catch:{ all -> 0x00bb }
            com.tencent.bugly.proguard.C2903an.m6864d(r3, r5)     // Catch:{ all -> 0x00bb }
        L_0x00ab:
            if (r11 == 0) goto L_0x00b0
            r11.close()     // Catch:{ all -> 0x00e6 }
        L_0x00b0:
            boolean r11 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00e6 }
            if (r11 == 0) goto L_0x00b9
            if (r0 == 0) goto L_0x00b9
            r0.close()     // Catch:{ all -> 0x00e6 }
        L_0x00b9:
            monitor-exit(r10)
            return r2
        L_0x00bb:
            r1 = move-exception
        L_0x00bc:
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r1)     // Catch:{ all -> 0x00d4 }
            if (r2 != 0) goto L_0x00c5
            r1.printStackTrace()     // Catch:{ all -> 0x00d4 }
        L_0x00c5:
            if (r11 == 0) goto L_0x00ca
            r11.close()     // Catch:{ all -> 0x00e6 }
        L_0x00ca:
            boolean r11 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00e6 }
            if (r11 == 0) goto L_0x00e4
            if (r0 == 0) goto L_0x00e4
            r0.close()     // Catch:{ all -> 0x00e6 }
            goto L_0x00e4
        L_0x00d4:
            r1 = move-exception
            if (r11 == 0) goto L_0x00da
            r11.close()     // Catch:{ all -> 0x00e6 }
        L_0x00da:
            boolean r11 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00e6 }
            if (r11 == 0) goto L_0x00e3
            if (r0 == 0) goto L_0x00e3
            r0.close()     // Catch:{ all -> 0x00e6 }
        L_0x00e3:
            throw r1     // Catch:{ all -> 0x00e6 }
        L_0x00e4:
            monitor-exit(r10)
            return r9
        L_0x00e6:
            r11 = move-exception
            monitor-exit(r10)
            goto L_0x00ea
        L_0x00e9:
            throw r11
        L_0x00ea:
            goto L_0x00e9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.mo34493a(int):java.util.List");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x006c, code lost:
        if (com.tencent.bugly.proguard.C2889ae.f5939a != false) goto L_0x006e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0090, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34495a(java.util.List<com.tencent.bugly.proguard.C2892ag> r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            if (r6 == 0) goto L_0x008f
            int r0 = r6.size()     // Catch:{ all -> 0x008c }
            if (r0 != 0) goto L_0x000b
            goto L_0x008f
        L_0x000b:
            com.tencent.bugly.proguard.af r0 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x008c }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x008c }
            if (r0 == 0) goto L_0x008a
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x008c }
            r1.<init>()     // Catch:{ all -> 0x008c }
            java.util.Iterator r6 = r6.iterator()     // Catch:{ all -> 0x008c }
        L_0x001c:
            boolean r2 = r6.hasNext()     // Catch:{ all -> 0x008c }
            if (r2 == 0) goto L_0x003d
            java.lang.Object r2 = r6.next()     // Catch:{ all -> 0x008c }
            com.tencent.bugly.proguard.ag r2 = (com.tencent.bugly.proguard.C2892ag) r2     // Catch:{ all -> 0x008c }
            java.lang.String r3 = " or "
            r1.append(r3)     // Catch:{ all -> 0x008c }
            java.lang.String r3 = "_id"
            r1.append(r3)     // Catch:{ all -> 0x008c }
            java.lang.String r3 = " = "
            r1.append(r3)     // Catch:{ all -> 0x008c }
            long r2 = r2.f5964a     // Catch:{ all -> 0x008c }
            r1.append(r2)     // Catch:{ all -> 0x008c }
            goto L_0x001c
        L_0x003d:
            java.lang.String r6 = r1.toString()     // Catch:{ all -> 0x008c }
            int r2 = r6.length()     // Catch:{ all -> 0x008c }
            if (r2 <= 0) goto L_0x004c
            r2 = 4
            java.lang.String r6 = r6.substring(r2)     // Catch:{ all -> 0x008c }
        L_0x004c:
            r2 = 0
            r1.setLength(r2)     // Catch:{ all -> 0x008c }
            java.lang.String r1 = "t_lr"
            r3 = 0
            int r6 = r0.delete(r1, r6, r3)     // Catch:{ all -> 0x0072 }
            java.lang.String r1 = "[Database] deleted %s data %d"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0072 }
            java.lang.String r4 = "t_lr"
            r3[r2] = r4     // Catch:{ all -> 0x0072 }
            r2 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0072 }
            r3[r2] = r6     // Catch:{ all -> 0x0072 }
            com.tencent.bugly.proguard.C2903an.m6863c(r1, r3)     // Catch:{ all -> 0x0072 }
            boolean r6 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x008c }
            if (r6 == 0) goto L_0x008a
        L_0x006e:
            r0.close()     // Catch:{ all -> 0x008c }
            goto L_0x008a
        L_0x0072:
            r6 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r6)     // Catch:{ all -> 0x0081 }
            if (r1 != 0) goto L_0x007c
            r6.printStackTrace()     // Catch:{ all -> 0x0081 }
        L_0x007c:
            boolean r6 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x008c }
            if (r6 == 0) goto L_0x008a
            goto L_0x006e
        L_0x0081:
            r6 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x008c }
            if (r1 == 0) goto L_0x0089
            r0.close()     // Catch:{ all -> 0x008c }
        L_0x0089:
            throw r6     // Catch:{ all -> 0x008c }
        L_0x008a:
            monitor-exit(r5)
            return
        L_0x008c:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        L_0x008f:
            monitor-exit(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.mo34495a(java.util.List):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003f, code lost:
        if (r0 != null) goto L_0x0041;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34501b(int r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            com.tencent.bugly.proguard.af r0 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x0062 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x0062 }
            if (r0 == 0) goto L_0x0060
            r1 = 0
            if (r6 < 0) goto L_0x0020
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x001e }
            r2.<init>()     // Catch:{ all -> 0x001e }
            java.lang.String r3 = "_tp = "
            r2.append(r3)     // Catch:{ all -> 0x001e }
            r2.append(r6)     // Catch:{ all -> 0x001e }
            java.lang.String r6 = r2.toString()     // Catch:{ all -> 0x001e }
            goto L_0x0021
        L_0x001e:
            r6 = move-exception
            goto L_0x0045
        L_0x0020:
            r6 = r1
        L_0x0021:
            java.lang.String r2 = "t_lr"
            int r6 = r0.delete(r2, r6, r1)     // Catch:{ all -> 0x001e }
            java.lang.String r1 = "[Database] deleted %s data %d"
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x001e }
            r3 = 0
            java.lang.String r4 = "t_lr"
            r2[r3] = r4     // Catch:{ all -> 0x001e }
            r3 = 1
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x001e }
            r2[r3] = r6     // Catch:{ all -> 0x001e }
            com.tencent.bugly.proguard.C2903an.m6863c(r1, r2)     // Catch:{ all -> 0x001e }
            boolean r6 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0062 }
            if (r6 == 0) goto L_0x0060
            if (r0 == 0) goto L_0x0060
        L_0x0041:
            r0.close()     // Catch:{ all -> 0x0062 }
            goto L_0x0060
        L_0x0045:
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r6)     // Catch:{ all -> 0x0055 }
            if (r1 != 0) goto L_0x004e
            r6.printStackTrace()     // Catch:{ all -> 0x0055 }
        L_0x004e:
            boolean r6 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0062 }
            if (r6 == 0) goto L_0x0060
            if (r0 == 0) goto L_0x0060
            goto L_0x0041
        L_0x0055:
            r6 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x0062 }
            if (r1 == 0) goto L_0x005f
            if (r0 == 0) goto L_0x005f
            r0.close()     // Catch:{ all -> 0x0062 }
        L_0x005f:
            throw r6     // Catch:{ all -> 0x0062 }
        L_0x0060:
            monitor-exit(r5)
            return
        L_0x0062:
            r6 = move-exception
            monitor-exit(r5)
            goto L_0x0066
        L_0x0065:
            throw r6
        L_0x0066:
            goto L_0x0065
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.mo34501b(int):void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public ContentValues mo34499b(C2892ag agVar) {
        if (agVar == null) {
            return null;
        }
        try {
            ContentValues contentValues = new ContentValues();
            if (agVar.f5964a > 0) {
                contentValues.put("_id", Long.valueOf(agVar.f5964a));
            }
            contentValues.put("_tp", Integer.valueOf(agVar.f5965b));
            contentValues.put("_pc", agVar.f5966c);
            contentValues.put("_th", agVar.f5967d);
            contentValues.put("_tm", Long.valueOf(agVar.f5968e));
            if (agVar.f5970g != null) {
                contentValues.put("_dt", agVar.f5970g);
            }
            return contentValues;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C2892ag mo34492a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            C2892ag agVar = new C2892ag();
            agVar.f5964a = cursor.getLong(cursor.getColumnIndex("_id"));
            agVar.f5965b = cursor.getInt(cursor.getColumnIndex("_tp"));
            agVar.f5966c = cursor.getString(cursor.getColumnIndex("_pc"));
            agVar.f5967d = cursor.getString(cursor.getColumnIndex("_th"));
            agVar.f5968e = cursor.getLong(cursor.getColumnIndex("_tm"));
            agVar.f5970g = cursor.getBlob(cursor.getColumnIndex("_dt"));
            return agVar;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0039, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00bf, code lost:
        return r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00c9, code lost:
        if (r1 != null) goto L_0x00cb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00cb, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00e6, code lost:
        if (r1 != null) goto L_0x00cb;
     */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00da A[Catch:{ all -> 0x00eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00df A[SYNTHETIC, Splitter:B:59:0x00df] */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x00e6  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized java.util.List<com.tencent.bugly.proguard.C2892ag> m6765c(int r12) {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = 0
            com.tencent.bugly.proguard.af r1 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x00d1 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ all -> 0x00d1 }
            if (r1 == 0) goto L_0x00c5
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c2 }
            r2.<init>()     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = "_id = "
            r2.append(r3)     // Catch:{ all -> 0x00c2 }
            r2.append(r12)     // Catch:{ all -> 0x00c2 }
            java.lang.String r10 = r2.toString()     // Catch:{ all -> 0x00c2 }
            java.lang.String r3 = "t_pf"
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r2 = r1
            r5 = r10
            android.database.Cursor r2 = r2.query(r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x00c2 }
            if (r2 != 0) goto L_0x003a
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ all -> 0x00cf }
        L_0x002f:
            boolean r12 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00cf }
            if (r12 == 0) goto L_0x0038
            if (r1 == 0) goto L_0x0038
            r1.close()     // Catch:{ all -> 0x00cf }
        L_0x0038:
            monitor-exit(r11)
            return r0
        L_0x003a:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c0 }
            r3.<init>()     // Catch:{ all -> 0x00c0 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ all -> 0x00c0 }
            r4.<init>()     // Catch:{ all -> 0x00c0 }
        L_0x0044:
            boolean r5 = r2.moveToNext()     // Catch:{ all -> 0x00c0 }
            r6 = 0
            if (r5 == 0) goto L_0x007a
            com.tencent.bugly.proguard.ag r5 = r11.mo34500b(r2)     // Catch:{ all -> 0x00c0 }
            if (r5 == 0) goto L_0x0055
            r4.add(r5)     // Catch:{ all -> 0x00c0 }
            goto L_0x0044
        L_0x0055:
            java.lang.String r5 = "_tp"
            int r5 = r2.getColumnIndex(r5)     // Catch:{ all -> 0x0072 }
            java.lang.String r5 = r2.getString(r5)     // Catch:{ all -> 0x0072 }
            java.lang.String r7 = " or "
            r3.append(r7)     // Catch:{ all -> 0x0072 }
            java.lang.String r7 = "_tp"
            r3.append(r7)     // Catch:{ all -> 0x0072 }
            java.lang.String r7 = " = "
            r3.append(r7)     // Catch:{ all -> 0x0072 }
            r3.append(r5)     // Catch:{ all -> 0x0072 }
            goto L_0x0044
        L_0x0072:
            java.lang.String r5 = "[Database] unknown id."
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x00c0 }
            com.tencent.bugly.proguard.C2903an.m6864d(r5, r6)     // Catch:{ all -> 0x00c0 }
            goto L_0x0044
        L_0x007a:
            int r5 = r3.length()     // Catch:{ all -> 0x00c0 }
            if (r5 <= 0) goto L_0x00b0
            java.lang.String r5 = " and "
            r3.append(r5)     // Catch:{ all -> 0x00c0 }
            java.lang.String r5 = "_id"
            r3.append(r5)     // Catch:{ all -> 0x00c0 }
            java.lang.String r5 = " = "
            r3.append(r5)     // Catch:{ all -> 0x00c0 }
            r3.append(r12)     // Catch:{ all -> 0x00c0 }
            r12 = 4
            java.lang.String r12 = r10.substring(r12)     // Catch:{ all -> 0x00c0 }
            java.lang.String r3 = "t_pf"
            int r12 = r1.delete(r3, r12, r0)     // Catch:{ all -> 0x00c0 }
            java.lang.String r3 = "[Database] deleted %s illegal data %d."
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00c0 }
            java.lang.String r7 = "t_pf"
            r5[r6] = r7     // Catch:{ all -> 0x00c0 }
            r6 = 1
            java.lang.Integer r12 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x00c0 }
            r5[r6] = r12     // Catch:{ all -> 0x00c0 }
            com.tencent.bugly.proguard.C2903an.m6864d(r3, r5)     // Catch:{ all -> 0x00c0 }
        L_0x00b0:
            if (r2 == 0) goto L_0x00b5
            r2.close()     // Catch:{ all -> 0x00cf }
        L_0x00b5:
            boolean r12 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00cf }
            if (r12 == 0) goto L_0x00be
            if (r1 == 0) goto L_0x00be
            r1.close()     // Catch:{ all -> 0x00cf }
        L_0x00be:
            monitor-exit(r11)
            return r4
        L_0x00c0:
            r12 = move-exception
            goto L_0x00d4
        L_0x00c2:
            r12 = move-exception
            r2 = r0
            goto L_0x00d4
        L_0x00c5:
            boolean r12 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00cf }
            if (r12 == 0) goto L_0x00e9
            if (r1 == 0) goto L_0x00e9
        L_0x00cb:
            r1.close()     // Catch:{ all -> 0x00cf }
            goto L_0x00e9
        L_0x00cf:
            r12 = move-exception
            goto L_0x00fb
        L_0x00d1:
            r12 = move-exception
            r1 = r0
            r2 = r1
        L_0x00d4:
            boolean r3 = com.tencent.bugly.proguard.C2903an.m6858a(r12)     // Catch:{ all -> 0x00eb }
            if (r3 != 0) goto L_0x00dd
            r12.printStackTrace()     // Catch:{ all -> 0x00eb }
        L_0x00dd:
            if (r2 == 0) goto L_0x00e2
            r2.close()     // Catch:{ all -> 0x00cf }
        L_0x00e2:
            boolean r12 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00cf }
            if (r12 == 0) goto L_0x00e9
            if (r1 == 0) goto L_0x00e9
            goto L_0x00cb
        L_0x00e9:
            monitor-exit(r11)
            return r0
        L_0x00eb:
            r12 = move-exception
            if (r2 == 0) goto L_0x00f1
            r2.close()     // Catch:{ all -> 0x00cf }
        L_0x00f1:
            boolean r0 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x00cf }
            if (r0 == 0) goto L_0x00fa
            if (r1 == 0) goto L_0x00fa
            r1.close()     // Catch:{ all -> 0x00cf }
        L_0x00fa:
            throw r12     // Catch:{ all -> 0x00cf }
        L_0x00fb:
            monitor-exit(r11)
            goto L_0x00fe
        L_0x00fd:
            throw r12
        L_0x00fe:
            goto L_0x00fd
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6765c(int):java.util.List");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0077, code lost:
        if (r2 != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0079, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0097, code lost:
        if (r2 != null) goto L_0x0079;
     */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0087 A[Catch:{ all -> 0x009c }] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x008c A[SYNTHETIC, Splitter:B:29:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0097  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean m6761a(int r6, java.lang.String r7, com.tencent.bugly.proguard.C2888ad r8) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            com.tencent.bugly.proguard.af r2 = com.tencent.bugly.proguard.C2889ae.f5941c     // Catch:{ all -> 0x007f }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ all -> 0x007f }
            if (r2 == 0) goto L_0x006a
            boolean r3 = com.tencent.bugly.proguard.C2908aq.m6915a(r7)     // Catch:{ all -> 0x0068 }
            if (r3 == 0) goto L_0x0023
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r7.<init>()     // Catch:{ all -> 0x0068 }
            java.lang.String r3 = "_id = "
            r7.append(r3)     // Catch:{ all -> 0x0068 }
            r7.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x0068 }
            goto L_0x004b
        L_0x0023:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0068 }
            r3.<init>()     // Catch:{ all -> 0x0068 }
            java.lang.String r4 = "_id = "
            r3.append(r4)     // Catch:{ all -> 0x0068 }
            r3.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = " and "
            r3.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = "_tp"
            r3.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = " = \""
            r3.append(r6)     // Catch:{ all -> 0x0068 }
            r3.append(r7)     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = "\""
            r3.append(r6)     // Catch:{ all -> 0x0068 }
            java.lang.String r6 = r3.toString()     // Catch:{ all -> 0x0068 }
        L_0x004b:
            java.lang.String r7 = "t_pf"
            int r6 = r2.delete(r7, r6, r0)     // Catch:{ all -> 0x0068 }
            java.lang.String r7 = "[Database] deleted %s data %d"
            r0 = 2
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0068 }
            java.lang.String r3 = "t_pf"
            r0[r1] = r3     // Catch:{ all -> 0x0068 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0068 }
            r4 = 1
            r0[r4] = r3     // Catch:{ all -> 0x0068 }
            com.tencent.bugly.proguard.C2903an.m6863c(r7, r0)     // Catch:{ all -> 0x0068 }
            if (r6 <= 0) goto L_0x006a
            r1 = 1
            goto L_0x006a
        L_0x0068:
            r6 = move-exception
            goto L_0x0081
        L_0x006a:
            if (r8 == 0) goto L_0x0073
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x007d }
            r8.mo34487a(r6)     // Catch:{ all -> 0x007d }
        L_0x0073:
            boolean r6 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x007d }
            if (r6 == 0) goto L_0x009a
            if (r2 == 0) goto L_0x009a
        L_0x0079:
            r2.close()     // Catch:{ all -> 0x007d }
            goto L_0x009a
        L_0x007d:
            r6 = move-exception
            goto L_0x00b0
        L_0x007f:
            r6 = move-exception
            r2 = r0
        L_0x0081:
            boolean r7 = com.tencent.bugly.proguard.C2903an.m6858a(r6)     // Catch:{ all -> 0x009c }
            if (r7 != 0) goto L_0x008a
            r6.printStackTrace()     // Catch:{ all -> 0x009c }
        L_0x008a:
            if (r8 == 0) goto L_0x0093
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x007d }
            r8.mo34487a(r6)     // Catch:{ all -> 0x007d }
        L_0x0093:
            boolean r6 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x007d }
            if (r6 == 0) goto L_0x009a
            if (r2 == 0) goto L_0x009a
            goto L_0x0079
        L_0x009a:
            monitor-exit(r5)
            return r1
        L_0x009c:
            r6 = move-exception
            if (r8 == 0) goto L_0x00a6
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x007d }
            r8.mo34487a(r7)     // Catch:{ all -> 0x007d }
        L_0x00a6:
            boolean r7 = com.tencent.bugly.proguard.C2889ae.f5939a     // Catch:{ all -> 0x007d }
            if (r7 == 0) goto L_0x00af
            if (r2 == 0) goto L_0x00af
            r2.close()     // Catch:{ all -> 0x007d }
        L_0x00af:
            throw r6     // Catch:{ all -> 0x007d }
        L_0x00b0:
            monitor-exit(r5)
            goto L_0x00b3
        L_0x00b2:
            throw r6
        L_0x00b3:
            goto L_0x00b2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2889ae.m6761a(int, java.lang.String, com.tencent.bugly.proguard.ad):boolean");
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public ContentValues mo34502c(C2892ag agVar) {
        if (agVar != null && !C2908aq.m6915a(agVar.f5969f)) {
            try {
                ContentValues contentValues = new ContentValues();
                if (agVar.f5964a > 0) {
                    contentValues.put("_id", Long.valueOf(agVar.f5964a));
                }
                contentValues.put("_tp", agVar.f5969f);
                contentValues.put("_tm", Long.valueOf(agVar.f5968e));
                if (agVar.f5970g != null) {
                    contentValues.put("_dt", agVar.f5970g);
                }
                return contentValues;
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public C2892ag mo34500b(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            C2892ag agVar = new C2892ag();
            agVar.f5964a = cursor.getLong(cursor.getColumnIndex("_id"));
            agVar.f5968e = cursor.getLong(cursor.getColumnIndex("_tm"));
            agVar.f5969f = cursor.getString(cursor.getColumnIndex("_tp"));
            agVar.f5970g = cursor.getBlob(cursor.getColumnIndex("_dt"));
            return agVar;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: com.tencent.bugly.proguard.ae$a */
    /* compiled from: BUGLY */
    class C2890a extends Thread {

        /* renamed from: b */
        private int f5943b;

        /* renamed from: c */
        private C2888ad f5944c;

        /* renamed from: d */
        private String f5945d;

        /* renamed from: e */
        private ContentValues f5946e;

        /* renamed from: f */
        private boolean f5947f;

        /* renamed from: g */
        private String[] f5948g;

        /* renamed from: h */
        private String f5949h;

        /* renamed from: i */
        private String[] f5950i;

        /* renamed from: j */
        private String f5951j;

        /* renamed from: k */
        private String f5952k;

        /* renamed from: l */
        private String f5953l;

        /* renamed from: m */
        private String f5954m;

        /* renamed from: n */
        private String f5955n;

        /* renamed from: o */
        private String[] f5956o;

        /* renamed from: p */
        private int f5957p;

        /* renamed from: q */
        private String f5958q;

        /* renamed from: r */
        private byte[] f5959r;

        public C2890a(int i, C2888ad adVar) {
            this.f5943b = i;
            this.f5944c = adVar;
        }

        /* renamed from: a */
        public void mo34506a(String str, ContentValues contentValues) {
            this.f5945d = str;
            this.f5946e = contentValues;
        }

        /* renamed from: a */
        public void mo34508a(boolean z, String str, String[] strArr, String str2, String[] strArr2, String str3, String str4, String str5, String str6) {
            this.f5947f = z;
            this.f5945d = str;
            this.f5948g = strArr;
            this.f5949h = str2;
            this.f5950i = strArr2;
            this.f5951j = str3;
            this.f5952k = str4;
            this.f5953l = str5;
            this.f5954m = str6;
        }

        /* renamed from: a */
        public void mo34507a(String str, String str2, String[] strArr) {
            this.f5945d = str;
            this.f5955n = str2;
            this.f5956o = strArr;
        }

        /* renamed from: a */
        public void mo34505a(int i, String str, byte[] bArr) {
            this.f5957p = i;
            this.f5958q = str;
            this.f5959r = bArr;
        }

        /* renamed from: a */
        public void mo34503a(int i) {
            this.f5957p = i;
        }

        /* renamed from: a */
        public void mo34504a(int i, String str) {
            this.f5957p = i;
            this.f5958q = str;
        }

        public void run() {
            switch (this.f5943b) {
                case 1:
                    long unused = C2889ae.this.m6754a(this.f5945d, this.f5946e, this.f5944c);
                    return;
                case 2:
                    int unused2 = C2889ae.this.m6752a(this.f5945d, this.f5955n, this.f5956o, this.f5944c);
                    return;
                case 3:
                    Cursor a = C2889ae.this.m6756a(this.f5947f, this.f5945d, this.f5948g, this.f5949h, this.f5950i, this.f5951j, this.f5952k, this.f5953l, this.f5954m, this.f5944c);
                    if (a != null) {
                        a.close();
                        return;
                    }
                    return;
                case 4:
                    boolean unused3 = C2889ae.this.m6762a(this.f5957p, this.f5958q, this.f5959r, this.f5944c);
                    return;
                case 5:
                    Map unused4 = C2889ae.this.m6759a(this.f5957p, this.f5944c);
                    return;
                case 6:
                    boolean unused5 = C2889ae.this.m6761a(this.f5957p, this.f5958q, this.f5944c);
                    return;
                default:
                    return;
            }
        }
    }
}
