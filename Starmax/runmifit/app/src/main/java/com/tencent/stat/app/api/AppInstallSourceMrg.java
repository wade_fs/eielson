package com.tencent.stat.app.api;

import android.content.Context;
import android.content.Intent;
import com.tencent.stat.StatServiceImpl;
import com.tencent.stat.app.p062a.C3195a;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;

public class AppInstallSourceMrg {

    /* renamed from: a */
    private static Context f7169a;

    /* renamed from: b */
    private static AppInstallSourceMrg f7170b;

    /* renamed from: c */
    private static StatLogger f7171c = StatCommonHelper.getLogger();

    /* renamed from: d */
    private static boolean f7172d = false;

    private AppInstallSourceMrg(Context context) {
        f7169a = context;
    }

    public static boolean isEnable() {
        return f7172d;
    }

    public static void setEnable(boolean z) {
        f7172d = z;
    }

    public static AppInstallSourceMrg getInstance(Context context) {
        if (f7170b == null) {
            synchronized (AppInstallSourceMrg.class) {
                if (f7170b == null) {
                    f7170b = new AppInstallSourceMrg(context);
                }
            }
        }
        return f7170b;
    }

    public void reportAppOpenEvent(Intent intent) {
        if (intent != null) {
            m7932a(intent);
        }
    }

    public void reportInstallEvent() {
        m7932a(null);
    }

    /* renamed from: a */
    private void m7932a(Intent intent) {
        try {
            if (!f7172d) {
                f7171c.warn("App install tracking is disable.");
                return;
            }
            C3195a aVar = new C3195a(f7169a, null);
            if (intent != null) {
                aVar.mo35365a(2);
                intent.getScheme();
            }
            StatServiceImpl.reportEvent(f7169a, aVar, null);
        } catch (Throwable th) {
            StatLogger statLogger = f7171c;
            statLogger.mo35383e("report installed error" + th.toString());
        }
    }
}
