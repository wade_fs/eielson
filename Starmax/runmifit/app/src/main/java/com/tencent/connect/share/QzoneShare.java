package com.tencent.connect.share;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.open.TDialog;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3119c;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3131k;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class QzoneShare extends BaseApi {
    public static final String SHARE_TO_QQ_APP_NAME = "appName";
    public static final String SHARE_TO_QQ_AUDIO_URL = "audio_url";
    public static final String SHARE_TO_QQ_EXT_INT = "cflag";
    public static final String SHARE_TO_QQ_EXT_STR = "share_qq_ext_str";
    public static final String SHARE_TO_QQ_IMAGE_LOCAL_URL = "imageLocalUrl";
    public static final String SHARE_TO_QQ_IMAGE_URL = "imageUrl";
    public static final String SHARE_TO_QQ_SITE = "site";
    public static final String SHARE_TO_QQ_SUMMARY = "summary";
    public static final String SHARE_TO_QQ_TARGET_URL = "targetUrl";
    public static final String SHARE_TO_QQ_TITLE = "title";
    public static final String SHARE_TO_QZONE_EXTMAP = "extMap";
    public static final String SHARE_TO_QZONE_KEY_TYPE = "req_type";
    public static final int SHARE_TO_QZONE_TYPE_APP = 6;
    public static final int SHARE_TO_QZONE_TYPE_IMAGE = 5;
    public static final int SHARE_TO_QZONE_TYPE_IMAGE_TEXT = 1;
    public static final int SHARE_TO_QZONE_TYPE_MINI_PROGRAM = 7;
    public static final int SHARE_TO_QZONE_TYPE_NO_TYPE = 0;

    /* renamed from: a */
    private boolean f6486a = true;

    /* renamed from: d */
    private boolean f6487d = false;

    /* renamed from: e */
    private boolean f6488e = false;

    /* renamed from: f */
    private boolean f6489f = false;
    public String mViaShareQzoneType = "";

    public void releaseResource() {
    }

    public QzoneShare(Context context, QQToken qQToken) {
        super(qQToken);
    }

    public void shareToQzone(Activity activity, Bundle bundle, IUiListener iUiListener) {
        String str;
        String str2;
        String str3;
        final Activity activity2 = activity;
        final Bundle bundle2 = bundle;
        final IUiListener iUiListener2 = iUiListener;
        C3082f.m7634c("openSDK_LOG.QzoneShare", "shareToQzone() -- start");
        if (bundle2 == null) {
            iUiListener2.onError(new UiError(-6, Constants.MSG_PARAM_NULL_ERROR, null));
            C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() params is null");
            C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.MSG_PARAM_NULL_ERROR);
            return;
        }
        String string = bundle2.getString("title");
        String string2 = bundle2.getString("summary");
        String string3 = bundle2.getString("targetUrl");
        String string4 = bundle2.getString(QQShare.SHARE_TO_QQ_MINI_PROGRAM_APPID);
        String string5 = bundle2.getString(QQShare.SHARE_TO_QQ_MINI_PROGRAM_PATH);
        ArrayList<String> stringArrayList = bundle2.getStringArrayList("imageUrl");
        String a = C3131k.m7770a(activity);
        String str4 = string3;
        String str5 = "imageUrl";
        if (a == null) {
            a = bundle2.getString("appName");
            str = "summary";
            str2 = "appName";
        } else {
            str2 = "appName";
            if (a.length() > 20) {
                StringBuilder sb = new StringBuilder();
                str = "summary";
                sb.append(a.substring(0, 20));
                sb.append("...");
                a = sb.toString();
            } else {
                str = "summary";
            }
        }
        int i = bundle2.getInt("req_type");
        C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() get SHARE_TO_QZONE_KEY_TYPE: " + i);
        if (i == 1) {
            this.mViaShareQzoneType = "1";
        } else if (i == 5) {
            this.mViaShareQzoneType = "2";
        } else if (i != 6) {
            this.mViaShareQzoneType = "1";
        } else {
            this.mViaShareQzoneType = "4";
        }
        if (i == 1) {
            C3082f.m7636e("openSDK_LOG.QzoneShare", "-->shareToQzone, SHARE_TO_QZONE_TYPE_IMAGE_TEXT needTitle = true");
            this.f6486a = true;
            this.f6487d = false;
            this.f6488e = true;
            this.f6489f = false;
        } else if (i == 5) {
            iUiListener2.onError(new UiError(-5, Constants.MSG_SHARE_TYPE_ERROR, null));
            C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() error--end请选择支持的分享类型");
            C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() 请选择支持的分享类型");
            return;
        } else if (i != 6) {
            if (i != 7) {
                if (!C3131k.m7792e(string) || !C3131k.m7792e(string2)) {
                    this.f6486a = true;
                } else if (stringArrayList == null || stringArrayList.size() == 0) {
                    string = "来自" + a + "的分享";
                    this.f6486a = true;
                } else {
                    this.f6486a = false;
                }
                this.f6487d = false;
                C3082f.m7636e("openSDK_LOG.QzoneShare", "-->shareToQzone, default needTitle = true, shareType = " + i);
                this.f6488e = true;
                this.f6489f = false;
            } else {
                if (TextUtils.isEmpty(string4) || TextUtils.isEmpty(string5)) {
                    iUiListener2.onError(new UiError(-5, Constants.MSG_PARAM_ERROR, "appid or path empty."));
                }
                this.f6488e = false;
                this.f6489f = false;
                this.f6486a = false;
            }
        } else if (C3131k.m7796g(activity2, "5.0.0")) {
            iUiListener2.onError(new UiError(-15, Constants.MSG_PARAM_APPSHARE_TOO_LOW, null));
            C3082f.m7636e("openSDK_LOG.QzoneShare", "-->shareToQzone, app share is not support below qq5.0.");
            C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone, app share is not support below qq5.0.");
            return;
        } else {
            String format = String.format("http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s&isOpenAppID=1", this.f6457c.getAppId(), "mqq");
            bundle2.putString("targetUrl", format);
            str4 = format;
        }
        if (C3131k.m7782b() || !C3131k.m7796g(activity2, "4.5.0")) {
            if (this.f6486a) {
                if (TextUtils.isEmpty(str4)) {
                    iUiListener2.onError(new UiError(-5, Constants.MSG_PARAM_TARGETURL_NULL_ERROR, null));
                    C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() targetUrl null error--end");
                    C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.MSG_PARAM_TARGETURL_NULL_ERROR);
                    return;
                } else if (!C3131k.m7797g(str4)) {
                    iUiListener2.onError(new UiError(-5, Constants.MSG_PARAM_TARGETURL_ERROR, null));
                    C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() targetUrl error--end");
                    C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.MSG_PARAM_TARGETURL_ERROR);
                    return;
                }
            }
            if (this.f6487d) {
                bundle2.putString("title", "");
                bundle2.putString(str, "");
            } else {
                String str6 = str;
                if (!this.f6488e || !C3131k.m7792e(string)) {
                    if (C3131k.m7792e(string) || string.length() <= 200) {
                        str3 = null;
                    } else {
                        str3 = null;
                        bundle2.putString("title", C3131k.m7772a(string, 200, (String) null, (String) null));
                    }
                    if (!C3131k.m7792e(string2) && string2.length() > 600) {
                        bundle2.putString(str6, C3131k.m7772a(string2, (int) com.runmifit.android.app.Constants.DEFAULT_WEIGHT_KG, str3, str3));
                    }
                } else {
                    iUiListener2.onError(new UiError(-6, Constants.MSG_PARAM_TITLE_NULL_ERROR, null));
                    C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() title is null--end");
                    C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() title is null");
                    return;
                }
            }
            if (!TextUtils.isEmpty(a)) {
                bundle2.putString(str2, a);
            }
            if (stringArrayList != null && (stringArrayList == null || stringArrayList.size() != 0)) {
                int i2 = 0;
                while (i2 < stringArrayList.size()) {
                    String str7 = stringArrayList.get(i2);
                    if (!C3131k.m7797g(str7) && !C3131k.m7798h(str7)) {
                        stringArrayList.remove(i2);
                        i2--;
                    }
                    i2++;
                }
                if (stringArrayList.size() == 0) {
                    iUiListener2.onError(new UiError(-6, Constants.MSG_PARAM_IMAGE_URL_FORMAT_ERROR, null));
                    C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() MSG_PARAM_IMAGE_URL_FORMAT_ERROR--end");
                    C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() 非法的图片地址!");
                    return;
                }
                bundle2.putStringArrayList(str5, stringArrayList);
            } else if (this.f6489f) {
                iUiListener2.onError(new UiError(-6, Constants.MSG_PARAM_IMAGE_ERROR, null));
                C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() imageUrl is null -- end");
                C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone() imageUrl is null");
                return;
            }
            if (!C3131k.m7796g(activity2, "4.6.0")) {
                C3082f.m7634c("openSDK_LOG.QzoneShare", "shareToQzone() qqver greater than 4.6.0");
                C3005a.m7338a(activity2, stringArrayList, new C3119c() {
                    /* class com.tencent.connect.share.QzoneShare.C30041 */

                    /* renamed from: a */
                    public void mo34826a(int i, String str) {
                        iUiListener2.onError(new UiError(-6, Constants.MSG_PARAM_IMAGE_URL_FORMAT_ERROR, null));
                    }

                    /* renamed from: a */
                    public void mo34827a(int i, ArrayList<String> arrayList) {
                        if (i == 0) {
                            bundle2.putStringArrayList("imageUrl", arrayList);
                        }
                        QzoneShare.this.m7330b(activity2, bundle2, iUiListener2);
                    }
                });
            } else if (C3125h.m7757c(activity2, "4.2.0") < 0 || C3125h.m7757c(activity2, "4.6.0") >= 0) {
                C3082f.m7635d("openSDK_LOG.QzoneShare", "shareToQzone() qqver below 4.2.0, will show download dialog");
                new TDialog(activity, "", mo34799a(""), null, this.f6457c).show();
            } else {
                C3082f.m7635d("openSDK_LOG.QzoneShare", "shareToQzone() qqver between 4.2.0 and 4.6.0, will use qqshare");
                QQShare qQShare = new QQShare(activity2, this.f6457c);
                if (stringArrayList != null && stringArrayList.size() > 0) {
                    String str8 = stringArrayList.get(0);
                    if (i != 5 || C3131k.m7798h(str8)) {
                        bundle2.putString("imageLocalUrl", str8);
                    } else {
                        iUiListener2.onError(new UiError(-6, Constants.MSG_PARAM_IMAGE_URL_MUST_BE_LOCAL, null));
                        C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone()手Q版本过低，纯图分享不支持网路图片");
                        C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "shareToQzone()手Q版本过低，纯图分享不支持网路图片");
                        return;
                    }
                }
                if (!C3131k.m7796g(activity2, "4.5.0")) {
                    bundle2.putInt("cflag", 1);
                }
                qQShare.shareToQQ(activity2, bundle2, iUiListener2);
            }
            C3082f.m7634c("openSDK_LOG.QzoneShare", "shareToQzone() --end");
            return;
        }
        iUiListener2.onError(new UiError(-6, Constants.MSG_SHARE_NOSD_ERROR, null));
        C3082f.m7636e("openSDK_LOG.QzoneShare", "shareToQzone() sdcard is null--end");
        C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(4), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.MSG_SHARE_NOSD_ERROR);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connect.common.BaseApi.a(android.app.Activity, int, android.content.Intent, boolean):void
     arg types: [android.app.Activity, ?, android.content.Intent, int]
     candidates:
      com.tencent.connect.share.QzoneShare.a(com.tencent.connect.share.QzoneShare, android.app.Activity, android.os.Bundle, com.tencent.tauth.IUiListener):void
      com.tencent.connect.common.BaseApi.a(android.app.Activity, int, android.content.Intent, boolean):void */
    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00e5  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0161  */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x0184  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x019e  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x01c1  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x01e4  */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0207  */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x024b  */
    /* JADX WARNING: Removed duplicated region for block: B:67:0x0272  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0299  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x02c0  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x02e3  */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x036f  */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x0389  */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x03b4  */
    /* JADX WARNING: Removed duplicated region for block: B:92:0x03fc  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m7330b(android.app.Activity r25, android.os.Bundle r26, com.tencent.tauth.IUiListener r27) {
        /*
            r24 = this;
            r1 = r24
            r2 = r25
            r0 = r26
            r3 = r27
            java.lang.String r4 = "openSDK_LOG.QzoneShare"
            java.lang.String r5 = "doshareToQzone() --start"
            com.tencent.open.p059a.C3082f.m7634c(r4, r5)
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            java.lang.String r6 = "mqqapi://share/to_qzone?src_type=app&version=1&file_type=news"
            r5.<init>(r6)
            java.lang.String r6 = "imageUrl"
            java.util.ArrayList r6 = r0.getStringArrayList(r6)
            java.lang.String r7 = "title"
            java.lang.String r7 = r0.getString(r7)
            java.lang.String r8 = "summary"
            java.lang.String r8 = r0.getString(r8)
            java.lang.String r9 = "targetUrl"
            java.lang.String r9 = r0.getString(r9)
            java.lang.String r10 = "audio_url"
            java.lang.String r10 = r0.getString(r10)
            r11 = 1
            java.lang.String r12 = "req_type"
            int r11 = r0.getInt(r12, r11)
            java.lang.String r12 = "appName"
            java.lang.String r12 = r0.getString(r12)
            java.lang.String r13 = "mini_program_appid"
            java.lang.String r13 = r0.getString(r13)
            java.lang.String r14 = "mini_program_path"
            java.lang.String r14 = r0.getString(r14)
            java.lang.String r15 = "mini_program_type"
            java.lang.String r15 = r0.getString(r15)
            r3 = 0
            java.lang.String r2 = "cflag"
            int r2 = r0.getInt(r2, r3)
            java.lang.String r3 = "share_qq_ext_str"
            java.lang.String r3 = r0.getString(r3)
            java.lang.String r17 = ""
            r18 = r2
            java.lang.String r2 = "extMap"
            android.os.Bundle r0 = r0.getBundle(r2)     // Catch:{ Exception -> 0x00b7 }
            if (r0 == 0) goto L_0x00b0
            java.util.Set r2 = r0.keySet()     // Catch:{ Exception -> 0x00b7 }
            r19 = r3
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ Exception -> 0x00ae }
            r3.<init>()     // Catch:{ Exception -> 0x00ae }
            java.util.Iterator r20 = r2.iterator()     // Catch:{ Exception -> 0x00ae }
        L_0x007b:
            boolean r21 = r20.hasNext()     // Catch:{ Exception -> 0x00ae }
            if (r21 == 0) goto L_0x009d
            java.lang.Object r21 = r20.next()     // Catch:{ Exception -> 0x00ae }
            r22 = r15
            r15 = r21
            java.lang.String r15 = (java.lang.String) r15     // Catch:{ Exception -> 0x0099 }
            r21 = r14
            java.lang.Object r14 = r0.get(r15)     // Catch:{ Exception -> 0x00ac }
            r3.put(r15, r14)     // Catch:{ Exception -> 0x00ac }
            r14 = r21
            r15 = r22
            goto L_0x007b
        L_0x0099:
            r0 = move-exception
            r21 = r14
            goto L_0x00be
        L_0x009d:
            r21 = r14
            r22 = r15
            int r0 = r2.size()     // Catch:{ Exception -> 0x00ac }
            if (r0 <= 0) goto L_0x00c3
            java.lang.String r17 = r3.toString()     // Catch:{ Exception -> 0x00ac }
            goto L_0x00c3
        L_0x00ac:
            r0 = move-exception
            goto L_0x00be
        L_0x00ae:
            r0 = move-exception
            goto L_0x00ba
        L_0x00b0:
            r19 = r3
            r21 = r14
            r22 = r15
            goto L_0x00c3
        L_0x00b7:
            r0 = move-exception
            r19 = r3
        L_0x00ba:
            r21 = r14
            r22 = r15
        L_0x00be:
            java.lang.String r2 = "ShareToQzone()  --error parse extmap"
            com.tencent.open.p059a.C3082f.m7632b(r4, r2, r0)
        L_0x00c3:
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r0 = r0.getAppId()
            com.tencent.connect.auth.QQToken r2 = r1.f6457c
            java.lang.String r2 = r2.getOpenId()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r14 = "openId:"
            r3.append(r14)
            r3.append(r2)
            java.lang.String r3 = r3.toString()
            com.tencent.open.p059a.C3082f.m7628a(r4, r3)
            if (r6 == 0) goto L_0x0138
            java.lang.StringBuffer r14 = new java.lang.StringBuffer
            r14.<init>()
            int r15 = r6.size()
            r3 = 9
            if (r15 <= r3) goto L_0x00f3
            goto L_0x00f7
        L_0x00f3:
            int r3 = r6.size()
        L_0x00f7:
            r15 = 0
        L_0x00f8:
            if (r15 >= r3) goto L_0x0117
            java.lang.Object r20 = r6.get(r15)
            java.lang.String r20 = (java.lang.String) r20
            r23 = r6
            java.lang.String r6 = java.net.URLEncoder.encode(r20)
            r14.append(r6)
            int r6 = r3 + -1
            if (r15 == r6) goto L_0x0112
            java.lang.String r6 = ";"
            r14.append(r6)
        L_0x0112:
            int r15 = r15 + 1
            r6 = r23
            goto L_0x00f8
        L_0x0117:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "&image_url="
            r3.append(r6)
            java.lang.String r6 = r14.toString()
            byte[] r6 = com.tencent.open.utils.C3131k.m7799i(r6)
            r14 = 2
            java.lang.String r6 = android.util.Base64.encodeToString(r6, r14)
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            r5.append(r3)
        L_0x0138:
            boolean r3 = android.text.TextUtils.isEmpty(r7)
            if (r3 != 0) goto L_0x015b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "&title="
            r3.append(r6)
            byte[] r6 = com.tencent.open.utils.C3131k.m7799i(r7)
            r7 = 2
            java.lang.String r6 = android.util.Base64.encodeToString(r6, r7)
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            r5.append(r3)
        L_0x015b:
            boolean r3 = android.text.TextUtils.isEmpty(r8)
            if (r3 != 0) goto L_0x017e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "&description="
            r3.append(r6)
            byte[] r6 = com.tencent.open.utils.C3131k.m7799i(r8)
            r7 = 2
            java.lang.String r6 = android.util.Base64.encodeToString(r6, r7)
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            r5.append(r3)
        L_0x017e:
            boolean r3 = android.text.TextUtils.isEmpty(r0)
            if (r3 != 0) goto L_0x0198
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r6 = "&share_id="
            r3.append(r6)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r5.append(r0)
        L_0x0198:
            boolean r0 = android.text.TextUtils.isEmpty(r9)
            if (r0 != 0) goto L_0x01bb
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "&url="
            r0.append(r3)
            byte[] r3 = com.tencent.open.utils.C3131k.m7799i(r9)
            r6 = 2
            java.lang.String r3 = android.util.Base64.encodeToString(r3, r6)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x01bb:
            boolean r0 = android.text.TextUtils.isEmpty(r12)
            if (r0 != 0) goto L_0x01de
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "&app_name="
            r0.append(r3)
            byte[] r3 = com.tencent.open.utils.C3131k.m7799i(r12)
            r6 = 2
            java.lang.String r3 = android.util.Base64.encodeToString(r3, r6)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x01de:
            boolean r0 = com.tencent.open.utils.C3131k.m7792e(r2)
            if (r0 != 0) goto L_0x0201
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "&open_id="
            r0.append(r3)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r2)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x0201:
            boolean r0 = com.tencent.open.utils.C3131k.m7792e(r10)
            if (r0 != 0) goto L_0x0224
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&audioUrl="
            r0.append(r2)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r10)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x0224:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&req_type="
            r0.append(r2)
            java.lang.String r2 = java.lang.String.valueOf(r11)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r2)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
            boolean r0 = android.text.TextUtils.isEmpty(r13)
            if (r0 != 0) goto L_0x026c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&mini_program_appid="
            r0.append(r2)
            java.lang.String r2 = java.lang.String.valueOf(r13)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r2)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x026c:
            boolean r0 = android.text.TextUtils.isEmpty(r21)
            if (r0 != 0) goto L_0x0293
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&mini_program_path="
            r0.append(r2)
            java.lang.String r2 = java.lang.String.valueOf(r21)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r2)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x0293:
            boolean r0 = android.text.TextUtils.isEmpty(r22)
            if (r0 != 0) goto L_0x02ba
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&mini_program_type="
            r0.append(r2)
            java.lang.String r2 = java.lang.String.valueOf(r22)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r2)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x02ba:
            boolean r0 = com.tencent.open.utils.C3131k.m7792e(r19)
            if (r0 != 0) goto L_0x02dd
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&share_qq_ext_str="
            r0.append(r2)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r19)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x02dd:
            boolean r0 = android.text.TextUtils.isEmpty(r17)
            if (r0 != 0) goto L_0x0300
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&share_qzone_ext_str="
            r0.append(r2)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r17)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
        L_0x0300:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "&cflag="
            r0.append(r2)
            java.lang.String r2 = java.lang.String.valueOf(r18)
            byte[] r2 = com.tencent.open.utils.C3131k.m7799i(r2)
            r3 = 2
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r3)
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            r5.append(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "doshareToQzone, url: "
            r0.append(r2)
            java.lang.String r2 = r5.toString()
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            com.tencent.open.p059a.C3082f.m7628a(r4, r0)
            android.content.Context r0 = com.tencent.open.utils.C3121e.m7727a()
            com.tencent.connect.auth.QQToken r2 = r1.f6457c
            java.lang.String r3 = "shareToNativeQQ"
            java.lang.String[] r3 = new java.lang.String[]{r3}
            java.lang.String r6 = "requireApi"
            com.tencent.connect.p052a.ProGuard.m7167a(r0, r2, r6, r3)
            android.content.Intent r0 = new android.content.Intent
            java.lang.String r2 = "android.intent.action.VIEW"
            r0.<init>(r2)
            java.lang.String r2 = r5.toString()
            android.net.Uri r2 = android.net.Uri.parse(r2)
            r0.setData(r2)
            java.lang.String r2 = r25.getPackageName()
            java.lang.String r3 = "pkg_name"
            r0.putExtra(r3, r2)
            java.lang.String r2 = "4.6.0"
            r3 = r25
            boolean r2 = com.tencent.open.utils.C3131k.m7796g(r3, r2)
            if (r2 == 0) goto L_0x0389
            boolean r2 = r1.mo34804a(r0)
            if (r2 == 0) goto L_0x0383
            com.tencent.connect.common.UIListenerManager r2 = com.tencent.connect.common.UIListenerManager.getInstance()
            r5 = 11104(0x2b60, float:1.556E-41)
            r6 = r27
            r2.setListenerWithRequestcode(r5, r6)
            r1.mo34801a(r3, r0, r5)
        L_0x0383:
            java.lang.String r2 = "doShareToQzone() -- QQ Version is < 4.6.0"
            com.tencent.open.p059a.C3082f.m7634c(r4, r2)
            goto L_0x03ad
        L_0x0389:
            r6 = r27
            r2 = 0
            java.lang.String r5 = "doShareToQzone() -- QQ Version is > 4.6.0"
            com.tencent.open.p059a.C3082f.m7634c(r4, r5)
            com.tencent.connect.common.UIListenerManager r5 = com.tencent.connect.common.UIListenerManager.getInstance()
            java.lang.String r7 = "shareToQzone"
            java.lang.Object r5 = r5.setListnerWithAction(r7, r6)
            if (r5 == 0) goto L_0x03a2
            java.lang.String r5 = "doShareToQzone() -- do listener onCancel()"
            com.tencent.open.p059a.C3082f.m7634c(r4, r5)
        L_0x03a2:
            boolean r4 = r1.mo34804a(r0)
            if (r4 == 0) goto L_0x03ad
            r4 = 10104(0x2778, float:1.4159E-41)
            r1.mo34800a(r3, r4, r0, r2)
        L_0x03ad:
            boolean r0 = r1.mo34804a(r0)
            r2 = 4
            if (r0 == 0) goto L_0x03fc
            com.tencent.open.b.d r3 = com.tencent.open.p060b.C3091d.m7665a()
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r4 = r0.getOpenId()
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r5 = r0.getAppId()
            java.lang.String r10 = r1.mViaShareQzoneType
            java.lang.String r6 = "ANDROIDQQ.SHARETOQZ.XX"
            java.lang.String r7 = "11"
            java.lang.String r8 = "3"
            java.lang.String r9 = "0"
            java.lang.String r11 = "0"
            java.lang.String r12 = "1"
            java.lang.String r13 = "0"
            r3.mo35124a(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            com.tencent.open.b.d r14 = com.tencent.open.p060b.C3091d.m7665a()
            r15 = 0
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r18 = r0.getAppId()
            java.lang.String r19 = java.lang.String.valueOf(r2)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r20 = java.lang.Long.valueOf(r2)
            r21 = 0
            r22 = 1
            java.lang.String r16 = "SHARE_CHECK_SDK"
            java.lang.String r17 = "1000"
            java.lang.String r23 = ""
            r14.mo35121a(r15, r16, r17, r18, r19, r20, r21, r22, r23)
            goto L_0x0443
        L_0x03fc:
            com.tencent.open.b.d r3 = com.tencent.open.p060b.C3091d.m7665a()
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r4 = r0.getOpenId()
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r5 = r0.getAppId()
            java.lang.String r10 = r1.mViaShareQzoneType
            java.lang.String r6 = "ANDROIDQQ.SHARETOQZ.XX"
            java.lang.String r7 = "11"
            java.lang.String r8 = "3"
            java.lang.String r9 = "1"
            java.lang.String r11 = "0"
            java.lang.String r12 = "1"
            java.lang.String r13 = "0"
            r3.mo35124a(r4, r5, r6, r7, r8, r9, r10, r11, r12, r13)
            com.tencent.open.b.d r14 = com.tencent.open.p060b.C3091d.m7665a()
            r15 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r18 = r0.getAppId()
            java.lang.String r19 = java.lang.String.valueOf(r2)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r20 = java.lang.Long.valueOf(r2)
            r21 = 0
            r22 = 1
            java.lang.String r16 = "SHARE_CHECK_SDK"
            java.lang.String r17 = "1000"
            java.lang.String r23 = "hasActivityForIntent fail"
            r14.mo35121a(r15, r16, r17, r18, r19, r20, r21, r22, r23)
        L_0x0443:
            java.lang.String r0 = "openSDK_LOG"
            java.lang.String r2 = "doShareToQzone() --end"
            com.tencent.open.p059a.C3082f.m7634c(r0, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connect.share.QzoneShare.m7330b(android.app.Activity, android.os.Bundle, com.tencent.tauth.IUiListener):void");
    }
}
