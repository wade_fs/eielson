package com.tencent.bugly.beta.global;

import android.app.Activity;
import android.os.Process;
import android.view.View;
import android.view.ViewGroup;
import com.tencent.bugly.beta.download.BetaReceiver;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.p050ui.C2816a;
import com.tencent.bugly.beta.p050ui.C2823e;
import com.tencent.bugly.beta.p050ui.C2826h;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2947p;
import com.tencent.bugly.proguard.C2957w;
import com.tencent.bugly.proguard.C2959y;

/* renamed from: com.tencent.bugly.beta.global.b */
/* compiled from: BUGLY */
public class C2805b implements View.OnClickListener {

    /* renamed from: a */
    final int f5330a;

    /* renamed from: b */
    final Object[] f5331b;

    public C2805b(int i, Object... objArr) {
        this.f5330a = i;
        this.f5331b = objArr;
    }

    public void onClick(View view) {
        try {
            switch (this.f5330a) {
                case 1:
                    if (((ViewGroup) this.f5331b[1]).getChildCount() <= 0) {
                        ((Activity) this.f5331b[0]).finish();
                        C2903an.m6857a("BetaAct closed by empty view", new Object[0]);
                        return;
                    }
                    return;
                case 2:
                    ((C2816a) this.f5331b[0]).mo34150a();
                    return;
                case 3:
                    if (this.f5331b[1] != null) {
                        ((DownloadTask) this.f5331b[1]).download();
                    }
                    ((C2816a) this.f5331b[0]).mo34150a();
                    return;
                case 4:
                    C2826h hVar = (C2826h) this.f5331b[0];
                    DownloadTask downloadTask = hVar.f5446q;
                    C2959y yVar = hVar.f5445p;
                    BetaReceiver.addTask(downloadTask);
                    if (hVar.f5447r != null) {
                        hVar.f5447r.run();
                    }
                    if (downloadTask.getStatus() != 1 || !C2804a.m6266a(C2808e.f5336E.f5385s, downloadTask.getSaveFile(), yVar.f6287f.f6246a)) {
                        downloadTask.download();
                    } else {
                        C2947p.f6229a.mo34667a(new C2957w("install", System.currentTimeMillis(), (byte) 0, 0, yVar.f6286e, yVar.f6294m, yVar.f6297p, null));
                    }
                    if (yVar.f6288g != 2) {
                        hVar.mo34150a();
                        return;
                    }
                    return;
                case 5:
                    C2826h hVar2 = (C2826h) this.f5331b[0];
                    DownloadTask downloadTask2 = hVar2.f5446q;
                    BetaReceiver.netListeners.remove(downloadTask2.getDownloadUrl());
                    downloadTask2.stop();
                    hVar2.mo34163a(downloadTask2);
                    return;
                case 6:
                    C2826h hVar3 = (C2826h) this.f5331b[0];
                    if (hVar3.f5448s != null) {
                        hVar3.f5448s.run();
                    }
                    hVar3.mo34150a();
                    return;
                case 7:
                    C2823e eVar = (C2823e) this.f5331b[0];
                    Process.killProcess(Process.myPid());
                    System.exit(0);
                    return;
                case 8:
                    ((C2823e) this.f5331b[0]).mo34150a();
                    return;
                default:
                    return;
            }
        } catch (Exception e) {
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
        }
    }
}
