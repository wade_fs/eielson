package com.tencent.bugly.proguard;

import android.text.TextUtils;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Proxy;

/* renamed from: com.tencent.bugly.proguard.ao */
/* compiled from: BUGLY */
public class C2904ao {

    /* renamed from: a */
    private static Proxy f6032a;

    /* renamed from: a */
    public static void m6868a(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            f6032a = null;
        } else {
            f6032a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, i));
        }
    }

    /* renamed from: a */
    public static void m6869a(InetAddress inetAddress, int i) {
        if (inetAddress == null) {
            f6032a = null;
        } else {
            f6032a = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(inetAddress, i));
        }
    }

    /* renamed from: a */
    public static Proxy m6867a() {
        return f6032a;
    }
}
