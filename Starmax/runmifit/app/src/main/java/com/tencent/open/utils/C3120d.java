package com.tencent.open.utils;

import android.util.Base64;
import com.tencent.open.p059a.C3082f;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: com.tencent.open.utils.d */
/* compiled from: ProGuard */
public class C3120d {

    /* renamed from: a */
    private static byte[] f6837a = {1, 2, 3, 4, 5, 6, 7, 8};

    /* renamed from: a */
    public static String m7725a(String str, String str2) {
        try {
            IvParameterSpec ivParameterSpec = new IvParameterSpec(f6837a);
            SecretKeySpec secretKeySpec = new SecretKeySpec(str2.getBytes(), "DES");
            Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, ivParameterSpec);
            return Base64.encodeToString(instance.doFinal(str.getBytes()), 0);
        } catch (Exception e) {
            C3082f.m7634c("DESUtils", "encode " + e.toString());
            return null;
        }
    }

    /* renamed from: b */
    public static String m7726b(String str, String str2) {
        try {
            byte[] decode = Base64.decode(str, 0);
            IvParameterSpec ivParameterSpec = new IvParameterSpec(f6837a);
            SecretKeySpec secretKeySpec = new SecretKeySpec(str2.getBytes(), "DES");
            Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
            instance.init(2, secretKeySpec, ivParameterSpec);
            return new String(instance.doFinal(decode));
        } catch (Exception e) {
            C3082f.m7634c("DESUtils", "decode " + e.toString());
            return null;
        }
    }
}
