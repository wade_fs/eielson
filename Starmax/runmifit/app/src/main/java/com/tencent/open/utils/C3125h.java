package com.tencent.open.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import com.tencent.connect.common.Constants;
import com.tencent.open.p059a.C3082f;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.util.List;

/* renamed from: com.tencent.open.utils.h */
/* compiled from: ProGuard */
public class C3125h {
    /* renamed from: a */
    public static String m7749a(int i) {
        if (i == 10103) {
            return "shareToQQ";
        }
        if (i == 10104) {
            return "shareToQzone";
        }
        if (i == 10105) {
            return "addToQQFavorites";
        }
        if (i == 10106) {
            return "sendToMyComputer";
        }
        if (i == 10107) {
            return "shareToTroopBar";
        }
        if (i == 11101) {
            return "action_login";
        }
        if (i == 10100) {
            return "action_request";
        }
        return null;
    }

    /* renamed from: a */
    public static String m7751a(Context context, String str) {
        try {
            return context.getPackageManager().getPackageInfo(str, 0).versionName;
        } catch (PackageManager.NameNotFoundException unused) {
            return null;
        }
    }

    /* renamed from: a */
    public static int m7747a(String str, String str2) {
        if (str == null && str2 == null) {
            return 0;
        }
        if (str != null && str2 == null) {
            return 1;
        }
        if (str == null && str2 != null) {
            return -1;
        }
        String[] split = str.split("\\.");
        String[] split2 = str2.split("\\.");
        int i = 0;
        while (i < split.length && i < split2.length) {
            try {
                int parseInt = Integer.parseInt(split[i]);
                int parseInt2 = Integer.parseInt(split2[i]);
                if (parseInt < parseInt2) {
                    return -1;
                }
                if (parseInt > parseInt2) {
                    return 1;
                }
                i++;
            } catch (NumberFormatException unused) {
                return str.compareTo(str2);
            }
        }
        if (split.length > i) {
            return 1;
        }
        return split2.length > i ? -1 : 0;
    }

    /* renamed from: a */
    public static boolean m7753a(Context context, String str, String str2) {
        C3082f.m7628a("openSDK_LOG.SystemUtils", "OpenUi, validateAppSignatureForPackage");
        try {
            for (Signature signature : context.getPackageManager().getPackageInfo(str, 64).signatures) {
                if (C3131k.m7793f(signature.toCharsString()).equals(str2)) {
                    return true;
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }

    /* renamed from: b */
    public static String m7755b(Context context, String str) {
        String str2 = "";
        C3082f.m7628a("openSDK_LOG.SystemUtils", "OpenUi, getSignValidString");
        try {
            String packageName = context.getPackageName();
            Signature[] signatureArr = context.getPackageManager().getPackageInfo(packageName, 64).signatures;
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(signatureArr[0].toByteArray());
            String a = C3131k.m7773a(instance.digest());
            instance.reset();
            C3082f.m7628a("openSDK_LOG.SystemUtils", "-->sign: " + a);
            instance.update(C3131k.m7799i(packageName + "_" + a + "_" + str + str2));
            str2 = C3131k.m7773a(instance.digest());
            instance.reset();
            StringBuilder sb = new StringBuilder();
            sb.append("-->signEncryped: ");
            sb.append(str2);
            C3082f.m7628a("openSDK_LOG.SystemUtils", sb.toString());
            return str2;
        } catch (Exception e) {
            e.printStackTrace();
            C3082f.m7632b("openSDK_LOG.SystemUtils", "OpenUi, getSignValidString error", e);
            return str2;
        }
    }

    /* renamed from: a */
    public static boolean m7752a(Context context, Intent intent) {
        if (context == null || intent == null || context.getPackageManager().queryIntentActivities(intent, 0).size() == 0) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public static String m7750a(Context context) {
        return context.getApplicationInfo().loadLabel(context.getPackageManager()).toString();
    }

    /* renamed from: c */
    public static int m7757c(Context context, String str) {
        return m7747a(m7751a(context, "com.tencent.mobileqq"), str);
    }

    /* renamed from: d */
    public static int m7758d(Context context, String str) {
        return m7747a(m7751a(context, Constants.PACKAGE_TIM), str);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX INFO: additional move instructions added (2) to help type inference */
    /* JADX WARN: Type inference failed for: r4v1 */
    /* JADX WARN: Type inference failed for: r4v2, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r4v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r4v4 */
    /* JADX WARN: Type inference failed for: r4v5 */
    /* JADX WARN: Type inference failed for: r4v6 */
    /* JADX WARN: Type inference failed for: r4v7 */
    /* JADX WARN: Type inference failed for: r4v11 */
    /* JADX WARN: Type inference failed for: r4v12 */
    /* JADX WARN: Type inference failed for: r4v13 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ad A[SYNTHETIC, Splitter:B:42:0x00ad] */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00b4 A[SYNTHETIC, Splitter:B:46:0x00b4] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00be A[SYNTHETIC, Splitter:B:53:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00c5 A[SYNTHETIC, Splitter:B:57:0x00c5] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m7754a(java.lang.String r10, java.lang.String r11, int r12) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "-->extractSecureLib, libName: "
            r0.append(r1)
            r0.append(r10)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "openSDK_LOG.SystemUtils"
            com.tencent.open.p059a.C3082f.m7634c(r1, r0)
            android.content.Context r0 = com.tencent.open.utils.C3121e.m7727a()
            r2 = 0
            if (r0 != 0) goto L_0x0023
            java.lang.String r10 = "-->extractSecureLib, global context is null. "
            com.tencent.open.p059a.C3082f.m7634c(r1, r10)
            return r2
        L_0x0023:
            java.lang.String r3 = "secure_lib"
            android.content.SharedPreferences r3 = r0.getSharedPreferences(r3, r2)
            java.io.File r4 = new java.io.File
            java.io.File r5 = r0.getFilesDir()
            r4.<init>(r5, r11)
            boolean r5 = r4.exists()
            r6 = 1
            java.lang.String r7 = "version"
            if (r5 != 0) goto L_0x0050
            java.io.File r5 = r4.getParentFile()
            if (r5 == 0) goto L_0x0073
            boolean r5 = r5.mkdirs()
            if (r5 == 0) goto L_0x0073
            r4.createNewFile()     // Catch:{ IOException -> 0x004b }
            goto L_0x0073
        L_0x004b:
            r4 = move-exception
            r4.printStackTrace()
            goto L_0x0073
        L_0x0050:
            int r4 = r3.getInt(r7, r2)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r8 = "-->extractSecureLib, libVersion: "
            r5.append(r8)
            r5.append(r12)
            java.lang.String r8 = " | oldVersion: "
            r5.append(r8)
            r5.append(r4)
            java.lang.String r5 = r5.toString()
            com.tencent.open.p059a.C3082f.m7634c(r1, r5)
            if (r12 != r4) goto L_0x0073
            return r6
        L_0x0073:
            r4 = 0
            android.content.res.AssetManager r5 = r0.getAssets()     // Catch:{ Exception -> 0x00a4, all -> 0x00a1 }
            java.io.InputStream r10 = r5.open(r10)     // Catch:{ Exception -> 0x00a4, all -> 0x00a1 }
            java.io.FileOutputStream r4 = r0.openFileOutput(r11, r2)     // Catch:{ Exception -> 0x009c, all -> 0x009a }
            m7748a(r10, r4)     // Catch:{ Exception -> 0x009c, all -> 0x009a }
            android.content.SharedPreferences$Editor r11 = r3.edit()     // Catch:{ Exception -> 0x009c, all -> 0x009a }
            r11.putInt(r7, r12)     // Catch:{ Exception -> 0x009c, all -> 0x009a }
            r11.commit()     // Catch:{ Exception -> 0x009c, all -> 0x009a }
            if (r10 == 0) goto L_0x0094
            r10.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x0094
        L_0x0093:
        L_0x0094:
            if (r4 == 0) goto L_0x0099
            r4.close()     // Catch:{ IOException -> 0x0099 }
        L_0x0099:
            return r6
        L_0x009a:
            r11 = move-exception
            goto L_0x00bc
        L_0x009c:
            r11 = move-exception
            r9 = r4
            r4 = r10
            r10 = r9
            goto L_0x00a6
        L_0x00a1:
            r11 = move-exception
            r10 = r4
            goto L_0x00bc
        L_0x00a4:
            r11 = move-exception
            r10 = r4
        L_0x00a6:
            java.lang.String r12 = "-->extractSecureLib, when copy lib execption."
            com.tencent.open.p059a.C3082f.m7632b(r1, r12, r11)     // Catch:{ all -> 0x00b8 }
            if (r4 == 0) goto L_0x00b2
            r4.close()     // Catch:{ IOException -> 0x00b1 }
            goto L_0x00b2
        L_0x00b1:
        L_0x00b2:
            if (r10 == 0) goto L_0x00b7
            r10.close()     // Catch:{ IOException -> 0x00b7 }
        L_0x00b7:
            return r2
        L_0x00b8:
            r11 = move-exception
            r9 = r4
            r4 = r10
            r10 = r9
        L_0x00bc:
            if (r10 == 0) goto L_0x00c3
            r10.close()     // Catch:{ IOException -> 0x00c2 }
            goto L_0x00c3
        L_0x00c2:
        L_0x00c3:
            if (r4 == 0) goto L_0x00c8
            r4.close()     // Catch:{ IOException -> 0x00c8 }
        L_0x00c8:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3125h.m7754a(java.lang.String, java.lang.String, int):boolean");
    }

    /* renamed from: a */
    private static long m7748a(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[8192];
        long j = 0;
        while (true) {
            int read = inputStream.read(bArr, 0, bArr.length);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
                j += (long) read;
            } else {
                C3082f.m7634c("openSDK_LOG.SystemUtils", "-->copy, copyed size is: " + j);
                return j;
            }
        }
    }

    /* renamed from: a */
    public static int m7746a(String str) {
        if ("shareToQQ".equals(str)) {
            return Constants.REQUEST_QQ_SHARE;
        }
        if ("shareToQzone".equals(str)) {
            return Constants.REQUEST_QZONE_SHARE;
        }
        if ("addToQQFavorites".equals(str)) {
            return Constants.REQUEST_QQ_FAVORITES;
        }
        if ("sendToMyComputer".equals(str)) {
            return Constants.REQUEST_SEND_TO_MY_COMPUTER;
        }
        if ("shareToTroopBar".equals(str)) {
            return Constants.REQUEST_SHARE_TO_TROOP_BAR;
        }
        if ("action_login".equals(str)) {
            return Constants.REQUEST_LOGIN;
        }
        if ("action_request".equals(str)) {
            return Constants.REQUEST_API;
        }
        return -1;
    }

    /* renamed from: b */
    public static boolean m7756b(Context context) {
        List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
        if (installedPackages != null) {
            for (int i = 0; i < installedPackages.size(); i++) {
                if (installedPackages.get(i).packageName.equals("com.tencent.mobileqq")) {
                    return true;
                }
            }
        }
        return false;
    }
}
