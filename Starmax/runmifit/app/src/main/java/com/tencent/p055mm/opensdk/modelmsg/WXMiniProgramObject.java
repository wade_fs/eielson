package com.tencent.p055mm.opensdk.modelmsg;

import android.os.Bundle;
import android.util.Log;
import com.tencent.p055mm.opensdk.modelmsg.WXMediaMessage;
import com.tencent.p055mm.opensdk.p056a.C3052d;

/* renamed from: com.tencent.mm.opensdk.modelmsg.WXMiniProgramObject */
public class WXMiniProgramObject implements WXMediaMessage.IMediaObject {
    private static final String TAG = "MicroMsg.SDK.WXMiniProgramObject";
    public String path;
    public String userName;
    public String webpageUrl;

    public boolean checkArgs() {
        String str;
        if (C3052d.m7542a(this.webpageUrl)) {
            str = "webPageUrl is null";
        } else if (!C3052d.m7542a(this.userName)) {
            return true;
        } else {
            str = "userName is null";
        }
        Log.e(TAG, str);
        return false;
    }

    public void serialize(Bundle bundle) {
        bundle.putString("_wxminiprogram_webpageurl", this.webpageUrl);
        bundle.putString("_wxminiprogram_username", this.userName);
        bundle.putString("_wxminiprogram_path", this.path);
    }

    public int type() {
        return 36;
    }

    public void unserialize(Bundle bundle) {
        this.webpageUrl = bundle.getString("_wxminiprogram_webpageurl");
        this.userName = bundle.getString("_wxminiprogram_username");
        this.path = bundle.getString("_wxminiprogram_path");
    }
}
