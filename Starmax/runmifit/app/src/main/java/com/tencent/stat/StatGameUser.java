package com.tencent.stat;

public class StatGameUser implements Cloneable {

    /* renamed from: a */
    private String f6985a = "";

    /* renamed from: b */
    private String f6986b = "";

    /* renamed from: c */
    private String f6987c = "";

    public StatGameUser(String str, String str2, String str3) {
        this.f6986b = str;
        this.f6985a = str2;
        this.f6987c = str3;
    }

    public String toString() {
        return "StatGameUser [worldName=" + this.f6985a + ", account=" + this.f6986b + ", level=" + this.f6987c + "]";
    }

    public StatGameUser() {
    }

    public String getWorldName() {
        return this.f6985a;
    }

    public void setWorldName(String str) {
        this.f6985a = str;
    }

    public String getAccount() {
        return this.f6986b;
    }

    public void setAccount(String str) {
        this.f6986b = str;
    }

    public String getLevel() {
        return this.f6987c;
    }

    public void setLevel(String str) {
        this.f6987c = str;
    }

    public StatGameUser clone() {
        try {
            return (StatGameUser) super.clone();
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }
}
