package com.tencent.p055mm.opensdk.diffdev.p058a;

import com.tencent.p055mm.opensdk.diffdev.OAuthListener;
import java.util.ArrayList;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.c */
final class C3058c implements Runnable {

    /* renamed from: h */
    final /* synthetic */ C3057b f6630h;

    C3058c(C3057b bVar) {
        this.f6630h = bVar;
    }

    public final void run() {
        ArrayList<OAuthListener> arrayList = new ArrayList<>();
        arrayList.addAll(this.f6630h.f6629g.f6626d);
        for (OAuthListener oAuthListener : arrayList) {
            oAuthListener.onQrcodeScanned();
        }
    }
}
