package com.tencent.stat.common;

import org.json.JSONException;
import org.json.JSONObject;

public class DeviceInfo {
    public static final int NEW_USER = 0;
    public static final int OLD_USER = 1;
    public static final String TAG_ANDROID_ID = "aid";
    public static final String TAG_FLAG = "__MTA_DEVICE_INFO__";
    public static final String TAG_IMEI = "ui";
    public static final String TAG_MAC = "mc";
    public static final String TAG_MID = "mid";
    public static final String TAG_TIMESTAMPS = "ts";
    public static final String TAG_VERSION = "ver";
    public static final int UPGRADE_USER = 2;

    /* renamed from: a */
    private String f7179a = null;

    /* renamed from: b */
    private String f7180b = null;

    /* renamed from: c */
    private String f7181c = null;

    /* renamed from: d */
    private String f7182d = "0";

    /* renamed from: e */
    private int f7183e;

    /* renamed from: f */
    private int f7184f = 0;

    /* renamed from: g */
    private long f7185g = 0;

    public DeviceInfo() {
    }

    public DeviceInfo(String str, String str2, int i) {
        this.f7179a = str;
        this.f7180b = str2;
        this.f7183e = i;
    }

    public String toString() {
        return mo35373a().toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public JSONObject mo35373a() {
        JSONObject jSONObject = new JSONObject();
        try {
            Util.jsonPut(jSONObject, TAG_IMEI, this.f7179a);
            Util.jsonPut(jSONObject, "mc", this.f7180b);
            Util.jsonPut(jSONObject, "mid", this.f7182d);
            Util.jsonPut(jSONObject, TAG_ANDROID_ID, this.f7181c);
            jSONObject.put("ts", this.f7185g);
            jSONObject.put("ver", this.f7184f);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }

    public String getMid() {
        return this.f7182d;
    }

    public String getImei() {
        return this.f7179a;
    }

    public String getMac() {
        return this.f7180b;
    }

    public void setUserType(int i) {
        this.f7183e = i;
    }

    public int getUserType() {
        return this.f7183e;
    }
}
