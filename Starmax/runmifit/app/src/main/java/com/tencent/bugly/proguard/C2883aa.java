package com.tencent.bugly.proguard;

/* renamed from: com.tencent.bugly.proguard.aa */
/* compiled from: BUGLY */
public final class C2883aa extends C2944m implements Cloneable {

    /* renamed from: c */
    static C2930bk f5916c;

    /* renamed from: d */
    static C2959y f5917d;

    /* renamed from: e */
    static final /* synthetic */ boolean f5918e = (!C2883aa.class.desiredAssertionStatus());

    /* renamed from: a */
    public C2930bk f5919a = null;

    /* renamed from: b */
    public C2959y f5920b = null;

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C2883aa aaVar = (C2883aa) obj;
        if (!C2945n.m7123a(this.f5919a, aaVar.f5919a) || !C2945n.m7123a(this.f5920b, aaVar.f5920b)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f5918e) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
     arg types: [com.tencent.bugly.proguard.bk, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
     arg types: [com.tencent.bugly.proguard.y, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        C2930bk bkVar = this.f5919a;
        if (bkVar != null) {
            lVar.mo34646a((C2944m) super, 0);
        }
        C2959y yVar = this.f5920b;
        if (yVar != null) {
            lVar.mo34646a((C2944m) super, 1);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
     arg types: [com.tencent.bugly.proguard.bk, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
     arg types: [com.tencent.bugly.proguard.y, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        if (f5916c == null) {
            f5916c = new C2930bk();
        }
        this.f5919a = (C2930bk) kVar.mo34619a((C2944m) f5916c, 0, false);
        if (f5917d == null) {
            f5917d = new C2959y();
        }
        this.f5920b = (C2959y) kVar.mo34619a((C2944m) f5917d, 1, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [com.tencent.bugly.proguard.bk, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [com.tencent.bugly.proguard.y, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i */
    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34599a((C2944m) this.f5919a, "baseStrategy");
        iVar.mo34599a((C2944m) this.f5920b, "grayStrategy");
    }
}
