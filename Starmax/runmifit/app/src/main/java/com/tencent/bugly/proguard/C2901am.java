package com.tencent.bugly.proguard;

import com.tencent.bugly.C2797b;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.tencent.bugly.proguard.am */
/* compiled from: BUGLY */
public class C2901am {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static final AtomicInteger f6025a = new AtomicInteger(1);

    /* renamed from: b */
    private static C2901am f6026b;

    /* renamed from: c */
    private ScheduledExecutorService f6027c;

    protected C2901am() {
        this.f6027c = null;
        this.f6027c = Executors.newScheduledThreadPool(3, new ThreadFactory() {
            /* class com.tencent.bugly.proguard.C2901am.C29021 */

            public Thread newThread(Runnable runnable) {
                Thread thread = new Thread(runnable);
                thread.setName("BuglyThread-" + C2901am.f6025a.getAndIncrement());
                return thread;
            }
        });
        ScheduledExecutorService scheduledExecutorService = this.f6027c;
        if (scheduledExecutorService == null || scheduledExecutorService.isShutdown()) {
            C2903an.m6864d("[AsyncTaskHandler] ScheduledExecutorService is not valiable!", new Object[0]);
        }
    }

    /* renamed from: a */
    public static synchronized C2901am m6848a() {
        C2901am amVar;
        synchronized (C2901am.class) {
            if (f6026b == null) {
                f6026b = new C2901am();
            }
            amVar = f6026b;
        }
        return amVar;
    }

    /* renamed from: a */
    public synchronized boolean mo34548a(Runnable runnable, long j) {
        if (!mo34550c()) {
            C2903an.m6864d("[AsyncTaskHandler] Async handler was closed, should not post task.", new Object[0]);
            return false;
        } else if (runnable == null) {
            C2903an.m6864d("[AsyncTaskHandler] Task input is null.", new Object[0]);
            return false;
        } else {
            if (j <= 0) {
                j = 0;
            }
            C2903an.m6863c("[AsyncTaskHandler] Post a delay(time: %dms) task: %s", Long.valueOf(j), runnable.getClass().getName());
            try {
                this.f6027c.schedule(runnable, j, TimeUnit.MILLISECONDS);
                return true;
            } catch (Throwable th) {
                if (C2797b.f5303c) {
                    th.printStackTrace();
                }
                return false;
            }
        }
    }

    /* renamed from: a */
    public synchronized boolean mo34547a(Runnable runnable) {
        if (!mo34550c()) {
            C2903an.m6864d("[AsyncTaskHandler] Async handler was closed, should not post task.", new Object[0]);
            return false;
        } else if (runnable == null) {
            C2903an.m6864d("[AsyncTaskHandler] Task input is null.", new Object[0]);
            return false;
        } else {
            C2903an.m6863c("[AsyncTaskHandler] Post a normal task: %s", runnable.getClass().getName());
            try {
                this.f6027c.execute(runnable);
                return true;
            } catch (Throwable th) {
                if (C2797b.f5303c) {
                    th.printStackTrace();
                }
                return false;
            }
        }
    }

    /* renamed from: b */
    public synchronized void mo34549b() {
        if (this.f6027c != null && !this.f6027c.isShutdown()) {
            C2903an.m6863c("[AsyncTaskHandler] Close async handler.", new Object[0]);
            this.f6027c.shutdownNow();
        }
    }

    /* renamed from: c */
    public synchronized boolean mo34550c() {
        return this.f6027c != null && !this.f6027c.isShutdown();
    }
}
