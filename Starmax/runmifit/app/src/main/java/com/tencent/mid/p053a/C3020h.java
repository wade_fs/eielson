package com.tencent.mid.p053a;

import android.content.Context;
import android.util.Log;
import com.tencent.mid.api.MidCallback;
import com.tencent.mid.api.MidConstants;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.p054b.C3025a;
import com.tencent.mid.p054b.C3031g;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.C3041g;
import com.tencent.mid.util.Util;
import java.util.ArrayList;
import java.util.Arrays;

/* renamed from: com.tencent.mid.a.h */
public class C3020h implements Runnable {

    /* renamed from: a */
    private Context f6523a = null;

    /* renamed from: b */
    private MidCallback f6524b = null;

    /* renamed from: c */
    private int f6525c = 0;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public C3038d f6526d = null;

    public C3020h(Context context, int i, MidCallback midCallback) {
        this.f6523a = context;
        this.f6525c = i;
        this.f6524b = midCallback;
        this.f6526d = Util.getLogger();
    }

    public void run() {
        synchronized (C3020h.class) {
            Log.i("MID", "ServiceRunnable begin, type:" + this.f6525c + ",ver:" + 3.73f);
            try {
                int i = this.f6525c;
                if (i == 1) {
                    MidEntity a = C3017g.m7367a(this.f6523a);
                    if (Util.isMidValid(a)) {
                        this.f6524b.onSuccess(a);
                    } else if (Util.isNetworkAvailable(this.f6523a)) {
                        C3013c.m7347a(this.f6523a).mo34841a(1, new C3016f(this.f6523a), this.f6524b);
                    } else {
                        this.f6524b.onFail(MidConstants.ERROR_NETWORK, "network not available.");
                    }
                } else if (i != 2) {
                    C3038d dVar = this.f6526d;
                    dVar.mo34952d("wrong type:" + this.f6525c);
                } else {
                    m7379b();
                }
            } catch (Throwable th) {
                this.f6526d.mo34954f(th);
            }
            Log.i("MID", "ServiceRunnable end");
        }
    }

    /* renamed from: a */
    private void m7378a() {
        MidEntity a = C3031g.m7437a(this.f6523a).mo34905a(new ArrayList(Arrays.asList(2)));
        MidEntity a2 = C3031g.m7437a(this.f6523a).mo34905a(new ArrayList(Arrays.asList(4)));
        if (Util.equal(a2, a)) {
            this.f6526d.mo34952d("local mid check passed.");
            return;
        }
        MidEntity newerMidEntity = Util.getNewerMidEntity(a2, a);
        C3038d dVar = this.f6526d;
        dVar.mo34952d("local mid check failed, redress with mid:" + newerMidEntity.toString());
        if (C3041g.m7509a(this.f6523a).mo34960b("ten.mid.allowCheckAndRewriteLocal.bool", 0) == 1) {
            C3031g.m7437a(this.f6523a).mo34922f(newerMidEntity);
        }
    }

    /* renamed from: b */
    private void m7379b() {
        C3025a k = C3031g.m7437a(this.f6523a).mo34928k();
        if (k == null) {
            this.f6526d.mo34952d("CheckEntity is null");
            return;
        }
        int c = k.mo34882c() + 1;
        long currentTimeMillis = System.currentTimeMillis() - k.mo34880b();
        if (currentTimeMillis < 0) {
            currentTimeMillis = -currentTimeMillis;
        }
        C3038d dVar = this.f6526d;
        dVar.mo34950b("check entity: " + k.toString() + ",duration:" + currentTimeMillis);
        if ((c > k.mo34884d() && currentTimeMillis > C3010a.f6500a) || currentTimeMillis > ((long) k.mo34877a()) * C3010a.f6500a) {
            m7378a();
            m7380c();
            k.mo34881b(c);
            k.mo34879a(System.currentTimeMillis());
            C3031g.m7437a(this.f6523a).mo34910a(k);
        }
        MidEntity a = C3031g.m7437a(this.f6523a).mo34904a();
        C3038d dVar2 = this.f6526d;
        dVar2.mo34950b("midNewEntity:" + a);
        if (!Util.isMidValid(a)) {
            this.f6526d.mo34950b("request mid_new ");
            C3013c.m7347a(this.f6523a).mo34841a(3, new C3016f(this.f6523a), new MidCallback() {
                /* class com.tencent.mid.p053a.C3020h.C30211 */

                public void onSuccess(Object obj) {
                    C3038d a = C3020h.this.f6526d;
                    a.mo34950b("request new mid success:" + obj);
                }

                public void onFail(int i, String str) {
                    C3038d a = C3020h.this.f6526d;
                    a.mo34950b("request new mid failed, errCode:" + i + ",msg:" + str);
                }
            });
        }
    }

    /* renamed from: c */
    private void m7380c() {
        this.f6526d.mo34950b("checkServer");
        C3013c.m7347a(this.f6523a).mo34841a(2, new C3016f(this.f6523a), new MidCallback() {
            /* class com.tencent.mid.p053a.C3020h.C30222 */

            public void onSuccess(Object obj) {
                C3038d a = C3020h.this.f6526d;
                a.mo34950b("checkServer success:" + obj);
            }

            public void onFail(int i, String str) {
                C3038d a = C3020h.this.f6526d;
                a.mo34950b("checkServer failed, errCode:" + i + ",msg:" + str);
            }
        });
    }
}
