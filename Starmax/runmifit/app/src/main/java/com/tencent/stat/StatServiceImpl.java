package com.tencent.stat;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import com.tencent.stat.StatConfig;
import com.tencent.stat.app.api.AppInstallSourceMrg;
import com.tencent.stat.common.C3204a;
import com.tencent.stat.common.C3207b;
import com.tencent.stat.common.C3208c;
import com.tencent.stat.common.DeviceInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatConstants;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.StatPreferences;
import com.tencent.stat.event.C3236a;
import com.tencent.stat.event.C3237b;
import com.tencent.stat.event.C3238c;
import com.tencent.stat.event.C3240d;
import com.tencent.stat.event.C3241e;
import com.tencent.stat.event.C3242f;
import com.tencent.stat.event.C3243g;
import com.tencent.stat.event.C3244h;
import com.tencent.stat.event.C3245i;
import com.tencent.stat.event.C3246j;
import com.tencent.stat.event.C3247k;
import com.tencent.stat.event.C3248l;
import com.tencent.stat.event.C3250m;
import com.tencent.stat.event.C3252n;
import com.tencent.stat.event.Event;
import com.tencent.stat.event.EventType;
import com.tencent.stat.lifecycle.MtaActivityLifeCycle;
import com.tencent.stat.lifecycle.MtaActivityLifecycleCallback;
import freemarker.log.Logger;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StatServiceImpl {
    /* access modifiers changed from: private */

    /* renamed from: A */
    public static List<StatActionListener> f6999A = new CopyOnWriteArrayList();
    /* access modifiers changed from: private */

    /* renamed from: B */
    public static volatile Runnable f7000B = null;
    /* access modifiers changed from: private */

    /* renamed from: C */
    public static volatile long f7001C = -1;

    /* renamed from: D */
    private static StatSpecifyReportedInfo f7002D = null;

    /* renamed from: a */
    static List<String> f7003a = new ArrayList();

    /* renamed from: b */
    static volatile int f7004b = 0;

    /* renamed from: c */
    static volatile long f7005c = 0;

    /* renamed from: d */
    static volatile long f7006d = 0;

    /* renamed from: e */
    private static Handler f7007e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static volatile Map<C3238c.C3239a, Long> f7008f = new ConcurrentHashMap();

    /* renamed from: g */
    private static volatile Map<String, Properties> f7009g = new ConcurrentHashMap();
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static volatile Map<Integer, Integer> f7010h = new ConcurrentHashMap(10);

    /* renamed from: i */
    private static volatile long f7011i = 0;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static volatile long f7012j = 0;

    /* renamed from: k */
    private static volatile long f7013k = 0;

    /* renamed from: l */
    private static String f7014l = "";
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static volatile int f7015m = 0;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public static volatile String f7016n = "";
    /* access modifiers changed from: private */

    /* renamed from: o */
    public static volatile String f7017o = "";
    /* access modifiers changed from: private */

    /* renamed from: p */
    public static Map<String, Long> f7018p = new ConcurrentHashMap();

    /* renamed from: q */
    private static Map<String, Long> f7019q = new ConcurrentHashMap();
    /* access modifiers changed from: private */

    /* renamed from: r */
    public static StatLogger f7020r = StatCommonHelper.getLogger();

    /* renamed from: s */
    private static volatile boolean f7021s = true;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public static Context f7022t = null;

    /* renamed from: u */
    private static long f7023u = 0;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public static long f7024v = 0;

    /* renamed from: w */
    private static volatile boolean f7025w = false;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public static volatile boolean f7026x = false;
    /* access modifiers changed from: private */

    /* renamed from: y */
    public static volatile boolean f7027y = true;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public static Handler f7028z = null;

    public static long getAppStartupTime() {
        return f7023u;
    }

    public static void initAppContext(Application application) {
        if (application != null) {
            f7022t = application;
        }
    }

    public static long getFrontgroundStartupTime() {
        return f7024v;
    }

    /* renamed from: a */
    static boolean m7863a() {
        if (f7004b < 2) {
            return false;
        }
        f7005c = System.currentTimeMillis();
        return true;
    }

    public static Context getContext(Context context) {
        if (context != null) {
            return context.getApplicationContext();
        }
        return f7022t;
    }

    public static void setContext(Context context) {
        if (context != null) {
            f7022t = context.getApplicationContext();
        }
    }

    public static void testJavaCrash(Context context) {
        C3226e.m8037a(context).mo35430c();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0059, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static synchronized void m7852a(android.content.Context r5) {
        /*
            java.lang.Class<com.tencent.stat.StatServiceImpl> r0 = com.tencent.stat.StatServiceImpl.class
            monitor-enter(r0)
            if (r5 != 0) goto L_0x0007
            monitor-exit(r0)
            return
        L_0x0007:
            android.os.Handler r1 = com.tencent.stat.StatServiceImpl.f7007e     // Catch:{ all -> 0x005a }
            if (r1 != 0) goto L_0x0058
            boolean r1 = m7876b(r5)     // Catch:{ all -> 0x005a }
            if (r1 != 0) goto L_0x0013
            monitor-exit(r0)
            return
        L_0x0013:
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x005a }
            com.tencent.stat.StatServiceImpl.f7023u = r1     // Catch:{ all -> 0x005a }
            android.content.Context r5 = r5.getApplicationContext()     // Catch:{ all -> 0x005a }
            com.tencent.stat.StatServiceImpl.f7022t = r5     // Catch:{ all -> 0x005a }
            android.os.HandlerThread r1 = new android.os.HandlerThread     // Catch:{ all -> 0x005a }
            java.lang.String r2 = "StatService"
            r1.<init>(r2)     // Catch:{ all -> 0x005a }
            r1.start()     // Catch:{ all -> 0x005a }
            android.os.Handler r2 = new android.os.Handler     // Catch:{ all -> 0x005a }
            android.os.Looper r1 = r1.getLooper()     // Catch:{ all -> 0x005a }
            r2.<init>(r1)     // Catch:{ all -> 0x005a }
            com.tencent.stat.StatServiceImpl.f7007e = r2     // Catch:{ all -> 0x005a }
            r1 = 0
            java.lang.String r1 = com.tencent.stat.common.StatCommonHelper.getDateString(r1)     // Catch:{ all -> 0x005a }
            com.tencent.stat.StatServiceImpl.f7014l = r1     // Catch:{ all -> 0x005a }
            long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x005a }
            long r3 = com.tencent.stat.StatConfig.f6951i     // Catch:{ all -> 0x005a }
            long r1 = r1 + r3
            com.tencent.stat.StatServiceImpl.f7012j = r1     // Catch:{ all -> 0x005a }
            m7902t()     // Catch:{ all -> 0x005a }
            com.tencent.stat.a r1 = com.tencent.stat.C3190a.m7914a(r5)     // Catch:{ all -> 0x005a }
            r1.mo35358a()     // Catch:{ all -> 0x005a }
            android.os.Handler r1 = com.tencent.stat.StatServiceImpl.f7007e     // Catch:{ all -> 0x005a }
            com.tencent.stat.StatServiceImpl$1 r2 = new com.tencent.stat.StatServiceImpl$1     // Catch:{ all -> 0x005a }
            r2.<init>(r5)     // Catch:{ all -> 0x005a }
            r1.post(r2)     // Catch:{ all -> 0x005a }
        L_0x0058:
            monitor-exit(r0)
            return
        L_0x005a:
            r5 = move-exception
            monitor-exit(r0)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.StatServiceImpl.m7852a(android.content.Context):void");
    }

    /* renamed from: s */
    private static void m7901s() {
        try {
            DeviceInfo b = C3226e.m8037a(f7022t).mo35429b(f7022t);
            if (b != null && b.getUserType() == 0) {
                AppInstallSourceMrg.getInstance(f7022t).reportInstallEvent();
            }
        } catch (Exception unused) {
        }
    }

    public static String fetchPageFlows() {
        List<String> list = f7003a;
        if (list == null || list.size() == 0) {
            return "";
        }
        StringBuffer stringBuffer = new StringBuffer();
        for (String str : f7003a) {
            stringBuffer.append(str + "\n");
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    static void m7861a(Thread thread, Throwable th) {
        Context context = f7022t;
        C3241e eVar = new C3241e(context, getSessionID(context, false, null), 2, th, thread, (StatSpecifyReportedInfo) null);
        if (eVar.toJsonString().length() <= 1048576) {
            m7860a(eVar);
            StatCrashReporter.getStatCrashReporter(f7022t).mo35262a(thread, th);
            return;
        }
        f7020r.mo35383e("Java Crash event exceeds the limit:1048576, droped.");
    }

    /* renamed from: a */
    static void m7862a(JSONArray jSONArray, Thread thread, String str) {
        Context context = f7022t;
        C3241e eVar = new C3241e(context, getSessionID(context, false, null), 3, jSONArray, thread, (StatSpecifyReportedInfo) null);
        eVar.mo35471b(str);
        final String str2 = StatCommonHelper.getTimeFormat(System.currentTimeMillis()) + ".native" + ".v1.crash";
        C3207b.m7979a(f7022t, "mtajcrash", str2, eVar.toJsonString());
        if ((StatConfig.isDebugEnable() || StatCrashReporter.getStatCrashReporter(f7022t).isEnableInstantReporting()) && NetworkManager.getInstance(f7022t).isNetworkAvailable()) {
            C3220d.m8022b(f7022t).mo35408a(eVar, new StatDispatchCallback() {
                /* class com.tencent.stat.StatServiceImpl.C314612 */

                public void onDispatchFailure() {
                }

                public void onDispatchSuccess() {
                    C3207b.m7978a(StatServiceImpl.f7022t, "mtajcrash", str2);
                    StatServiceImpl.f7020r.debug("native crash has been reported.");
                }
            });
        }
        StatCrashReporter.getStatCrashReporter(f7022t).mo35261a(eVar.mo35468a());
        new File(str).delete();
    }

    /* renamed from: a */
    static void m7860a(C3241e eVar) {
        final String str = StatCommonHelper.getTimeFormat(System.currentTimeMillis()) + ".v1.crash";
        C3207b.m7979a(f7022t, "mtajcrash", str, eVar.toJsonString());
        if ((StatConfig.isDebugEnable() || StatCrashReporter.getStatCrashReporter(f7022t).isEnableInstantReporting()) && NetworkManager.getInstance(f7022t).isNetworkAvailable()) {
            C3220d.m8022b(f7022t).mo35408a(eVar, new StatDispatchCallback() {
                /* class com.tencent.stat.StatServiceImpl.C315823 */

                public void onDispatchFailure() {
                }

                public void onDispatchSuccess() {
                    C3207b.m7978a(StatServiceImpl.f7022t, "mtajcrash", str);
                    StatServiceImpl.f7020r.debug("java crash has been reported.");
                }
            });
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.stat.StatServiceImpl.a(android.content.Context, com.tencent.stat.StatSpecifyReportedInfo, boolean):void
     arg types: [android.content.Context, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.stat.StatServiceImpl.a(android.content.Context, com.tencent.stat.StatAccount, com.tencent.stat.StatSpecifyReportedInfo):void
      com.tencent.stat.StatServiceImpl.a(android.content.Context, java.lang.String, com.tencent.stat.StatSpecifyReportedInfo):void
      com.tencent.stat.StatServiceImpl.a(org.json.JSONArray, java.lang.Thread, java.lang.String):void
      com.tencent.stat.StatServiceImpl.a(android.content.Context, com.tencent.stat.StatSpecifyReportedInfo, boolean):void */
    /* renamed from: b */
    static void m7875b(Thread thread, Throwable th) {
        Context context;
        if (StatConfig.isEnableStatService() && (context = f7022t) != null) {
            try {
                m7856a(context, (StatSpecifyReportedInfo) null, true);
                if (StatConfig.isAutoExceptionCaught()) {
                    m7861a(thread, th);
                    Log.e("MtaSDK.CaughtExp", "MTA has caught the following uncaught exception:");
                    Log.e("MtaSDK.CaughtExp", "", th);
                    Log.e("MtaSDK.CaughtExp", "App uncaught exception end.");
                } else {
                    f7020r.warn("crash happened, but MTA Auto Exception Caught Report is desable.");
                }
                flushDataToDB(f7022t);
                if (StatConfig.m7836f() != null) {
                    StatConfig.m7836f().onMtaException(th);
                }
            } catch (Throwable th2) {
                th2.printStackTrace();
            }
        }
    }

    /* renamed from: b */
    static boolean m7876b(Context context) {
        boolean z;
        long j = StatPreferences.getLong(context, StatConfig.f6945c, 0);
        long sDKLongVersion = StatCommonHelper.getSDKLongVersion(StatConstants.VERSION);
        boolean z2 = false;
        if (sDKLongVersion <= j) {
            StatLogger statLogger = f7020r;
            statLogger.error("MTA is disable for current version:" + sDKLongVersion + ",wakeup version:" + j);
            z = false;
        } else {
            z = true;
        }
        long j2 = StatPreferences.getLong(context, StatConfig.f6946d, 0);
        if (j2 > System.currentTimeMillis()) {
            StatLogger statLogger2 = f7020r;
            statLogger2.error("MTA is disable for current time:" + System.currentTimeMillis() + ",wakeup time:" + j2);
        } else {
            z2 = z;
        }
        StatConfig.setEnableStatService(z2);
        return z2;
    }

    /* renamed from: a */
    static boolean m7865a(String str) {
        return str == null || str.length() == 0;
    }

    public static Handler getHandler(Context context) {
        if (f7007e == null) {
            synchronized (StatServiceImpl.class) {
                if (f7007e == null) {
                    if (context == null) {
                        try {
                            context = f7022t;
                        } catch (Throwable th) {
                            f7020r.error(th);
                            StatConfig.setEnableStatService(false);
                        }
                    }
                    m7852a(context);
                }
            }
        }
        return f7007e;
    }

    /* renamed from: b */
    static JSONObject m7870b() {
        JSONObject jSONObject = new JSONObject();
        try {
            JSONObject jSONObject2 = new JSONObject();
            if (StatConfig.f6944b.f6977d != 0) {
                jSONObject2.put("v", StatConfig.f6944b.f6977d);
            }
            jSONObject.put(Integer.toString(StatConfig.f6944b.f6974a), jSONObject2);
            JSONObject jSONObject3 = new JSONObject();
            if (StatConfig.f6932a.f6977d != 0) {
                jSONObject3.put("v", StatConfig.f6932a.f6977d);
            }
            jSONObject.put(Integer.toString(StatConfig.f6932a.f6974a), jSONObject3);
        } catch (JSONException e) {
            f7020r.mo35384e((Throwable) e);
        }
        return jSONObject;
    }

    /* renamed from: a */
    static void m7855a(Context context, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (getHandler(context) != null) {
            if (StatConfig.isDebugEnable()) {
                f7020r.mo35381d("start new session.");
            }
            if (statSpecifyReportedInfo == null || f7015m == 0) {
                f7015m = StatCommonHelper.getNextSessionID();
            }
            StatConfig.m7816a(0);
            StatConfig.m7833d();
            C3252n nVar = new C3252n(context, f7015m, m7870b(), statSpecifyReportedInfo);
            String reportedAppkey = nVar.getReportedAppkey();
            if (!StatCommonHelper.isStringValid(reportedAppkey) || !StatConfig.shouldSkipSessionReport(reportedAppkey)) {
                new C3184c(nVar).mo35344a();
                m7901s();
                return;
            }
            StatLogger statLogger = f7020r;
            statLogger.mo35396w("appkey :" + reportedAppkey + " skip session report.");
        }
    }

    public static int getSessionID(Context context, boolean z, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        long currentTimeMillis = System.currentTimeMillis();
        boolean z2 = true;
        boolean z3 = z && currentTimeMillis - f7011i >= ((long) StatConfig.getSessionTimoutMillis());
        f7011i = currentTimeMillis;
        if (f7013k == 0) {
            f7013k = StatCommonHelper.getTomorrowStartMilliseconds();
        }
        if (currentTimeMillis >= f7013k) {
            f7013k = StatCommonHelper.getTomorrowStartMilliseconds();
            if (C3226e.m8037a(context).mo35429b(context).getUserType() != 1) {
                C3226e.m8037a(context).mo35429b(context).setUserType(1);
            }
            StatConfig.m7827b(0);
            f7004b = 0;
            f7014l = StatCommonHelper.getDateString(0);
            z3 = true;
        }
        String str = f7014l;
        if (StatCommonHelper.isSpecifyReportedValid(statSpecifyReportedInfo)) {
            str = statSpecifyReportedInfo.getAppKey() + f7014l;
        }
        if (!f7019q.containsKey(str)) {
            z3 = true;
        } else {
            z2 = false;
        }
        if (z3) {
            if (StatCommonHelper.isSpecifyReportedValid(statSpecifyReportedInfo)) {
                m7855a(context, statSpecifyReportedInfo);
            } else if (StatConfig.m7835e() < StatConfig.getMaxDaySessionNumbers()) {
                StatCommonHelper.checkFirstTimeActivate(context);
                m7855a(context, (StatSpecifyReportedInfo) null);
            } else {
                f7020r.mo35383e("Exceed StatConfig.getMaxDaySessionNumbers().");
            }
        }
        if (f7021s || z2) {
            f7019q.put(str, 1L);
            m7872b(context, statSpecifyReportedInfo);
        }
        if (f7021s) {
            StatCommonHelper.isSpecifyReportedValid(statSpecifyReportedInfo);
        }
        f7021s = false;
        return f7015m;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m7874b(Context context, final String str, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        final String str2 = new String(str);
        final Context applicationContext = context.getApplicationContext();
        if (getHandler(applicationContext) != null) {
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C316832 */

                public void run() {
                    try {
                        synchronized (StatServiceImpl.f7018p) {
                            StatLogger e = StatServiceImpl.f7020r;
                            e.mo35381d("inerTrackBeginPage:" + str);
                            StatServiceImpl.f7003a.add(str2);
                            if (StatServiceImpl.f7003a.size() > 40) {
                                StatServiceImpl.f7003a = StatServiceImpl.f7003a.subList(StatServiceImpl.f7003a.size() - 20, StatServiceImpl.f7003a.size());
                            }
                            if (StatServiceImpl.f7018p.size() >= StatConfig.getMaxParallelTimmingEvents()) {
                                StatLogger e2 = StatServiceImpl.f7020r;
                                e2.error("The number of page events exceeds the maximum value " + Integer.toString(StatConfig.getMaxParallelTimmingEvents()));
                                return;
                            }
                            String unused = StatServiceImpl.f7016n = str2;
                            if (StatServiceImpl.f7018p.containsKey(StatServiceImpl.f7016n)) {
                                StatLogger e3 = StatServiceImpl.f7020r;
                                e3.warn("Duplicate PageID : " + StatServiceImpl.f7016n + ", onResume() repeated?");
                            }
                            StatServiceImpl.f7018p.put(StatServiceImpl.f7016n, Long.valueOf(System.currentTimeMillis()));
                            StatServiceImpl.getSessionID(applicationContext, true, statSpecifyReportedInfo);
                        }
                    } catch (Throwable th) {
                        StatServiceImpl.f7020r.mo35384e(th);
                        StatServiceImpl.m7859a(applicationContext, th);
                    }
                }
            });
        }
    }

    public static void trackBeginPage(Context context, String str, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            Context context2 = getContext(context);
            if (context2 == null || str == null || str.length() == 0) {
                f7020r.error("The Context or pageName of StatService.trackBeginPage() can not be null or empty!");
            } else {
                m7874b(context2, str, statSpecifyReportedInfo);
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m7873b(Context context, String str, int i, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        final String str2 = new String(str);
        final Context applicationContext = context.getApplicationContext();
        if (getHandler(applicationContext) != null) {
            final String str3 = str;
            final StatSpecifyReportedInfo statSpecifyReportedInfo2 = statSpecifyReportedInfo;
            final int i2 = i;
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C316933 */

                public void run() {
                    Long l;
                    try {
                        StatServiceImpl.flushDataToDB(applicationContext);
                        synchronized (StatServiceImpl.f7018p) {
                            l = (Long) StatServiceImpl.f7018p.remove(str2);
                        }
                        StatLogger e = StatServiceImpl.f7020r;
                        e.mo35381d("inerTrackEndPage:" + str3 + ",startTime:" + l);
                        if (l != null) {
                            double currentTimeMillis = (double) (System.currentTimeMillis() - l.longValue());
                            Double.isNaN(currentTimeMillis);
                            double d = currentTimeMillis / 1000.0d;
                            if (d <= 0.0d) {
                                d = 0.1d;
                            }
                            double d2 = d;
                            String i = StatServiceImpl.f7017o;
                            if (i != null && i.equals(str2)) {
                                i = "-";
                            }
                            C3247k kVar = new C3247k(applicationContext, i, str2, StatServiceImpl.getSessionID(applicationContext, false, statSpecifyReportedInfo2), d2, statSpecifyReportedInfo2);
                            kVar.addCommonProperty(Logger.LIBRARY_NAME_AUTO, Integer.valueOf(i2));
                            if (!str2.equals(StatServiceImpl.f7016n)) {
                                StatServiceImpl.f7020r.warn("Invalid invocation since previous onResume on diff page.");
                            }
                            new C3184c(kVar).mo35344a();
                            String unused = StatServiceImpl.f7017o = str2;
                            return;
                        }
                        StatLogger e2 = StatServiceImpl.f7020r;
                        e2.mo35383e("Starttime for PageID:" + str2 + " not found, lost onResume()?");
                    } catch (Throwable th) {
                        StatServiceImpl.f7020r.mo35384e(th);
                        StatServiceImpl.m7859a(applicationContext, th);
                    }
                }
            });
        }
    }

    public static void trackEndPage(Context context, String str, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            Context context2 = getContext(context);
            if (context2 == null || str == null || str.length() == 0) {
                f7020r.error("The Context or pageName of StatService.trackEndPage() can not be null or empty!");
            } else {
                m7873b(context2, str, 0, statSpecifyReportedInfo);
            }
        }
    }

    public static void startNewSession(Context context, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.startNewSession() can not be null!");
            } else if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C317034 */

                    public void run() {
                        try {
                            StatServiceImpl.stopSession();
                            StatServiceImpl.getSessionID(context2, true, statSpecifyReportedInfo);
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
        }
    }

    public static void stopSession() {
        f7011i = 0;
    }

    /* renamed from: a */
    static void m7856a(Context context, StatSpecifyReportedInfo statSpecifyReportedInfo, boolean z) {
        if (statSpecifyReportedInfo == null) {
            try {
                statSpecifyReportedInfo = f7002D;
            } catch (Throwable th) {
                f7020r.mo35384e(th);
            }
        }
        StatLogger statLogger = f7020r;
        statLogger.mo35381d("trackBackground lastForegroundTs:" + f7001C);
        if (f7001C > 0 && StatConfig.f6960r) {
            double currentTimeMillis = (double) ((System.currentTimeMillis() - f7001C) - StatConfig.getBackgroundDelayTimestamp());
            Double.isNaN(currentTimeMillis);
            double d = currentTimeMillis / 1000.0d;
            if (d <= 0.0d) {
                d = 0.1d;
            }
            m7853a(f7022t, d, statSpecifyReportedInfo, z);
        }
        f7001C = -1;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.stat.StatServiceImpl.a(android.content.Context, double, com.tencent.stat.StatSpecifyReportedInfo, boolean):void
     arg types: [android.content.Context, double, com.tencent.stat.StatSpecifyReportedInfo, int]
     candidates:
      com.tencent.stat.StatServiceImpl.a(android.content.Context, java.lang.String, int, com.tencent.stat.StatSpecifyReportedInfo):void
      com.tencent.stat.StatServiceImpl.a(android.content.Context, double, com.tencent.stat.StatSpecifyReportedInfo, boolean):void */
    public static void trackBackground(Context context, long j, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        m7853a(context, (double) j, statSpecifyReportedInfo, false);
    }

    /* renamed from: a */
    private static void m7853a(Context context, double d, StatSpecifyReportedInfo statSpecifyReportedInfo, boolean z) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.trackBackground() can not be null!");
            } else if (getHandler(context2) != null) {
                final double d2 = d;
                final StatSpecifyReportedInfo statSpecifyReportedInfo2 = statSpecifyReportedInfo;
                final boolean z2 = z;
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C317135 */

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
                     arg types: [com.tencent.stat.event.b, ?[OBJECT, ARRAY], int, int]
                     candidates:
                      com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
                      com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
                      com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
                      com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
                    public void run() {
                        try {
                            StatLogger e = StatServiceImpl.f7020r;
                            e.mo35388i("trackBackground duration:" + d2);
                            StatServiceImpl.flushDataToDB(context2);
                            C3237b bVar = new C3237b(StatServiceImpl.getContext(context2), StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo2), d2 <= 0.0d ? 0.1d : d2, statSpecifyReportedInfo2);
                            if (!z2) {
                                new C3184c(bVar).mo35344a();
                            } else {
                                C3226e.m8037a(context2).mo35426a((Event) bVar, (StatDispatchCallback) null, false, true);
                            }
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
        }
    }

    public static boolean isEnableAutoMonitorActivityCycle() {
        return f7025w;
    }

    public static boolean isForeground() {
        return f7026x;
    }

    public static boolean isBackground() {
        return !f7026x;
    }

    public static void addActionListener(StatActionListener statActionListener) {
        f6999A.add(statActionListener);
    }

    public static void removeActionListener(StatActionListener statActionListener) {
        f6999A.remove(statActionListener);
    }

    /* renamed from: t */
    private static void m7902t() {
        Application application;
        try {
            if (Integer.valueOf(StatConfig.getSDKProperty("autoTm", "1")).intValue() == 1) {
                if (f7022t instanceof Application) {
                    application = (Application) f7022t;
                } else if (f7022t instanceof Activity) {
                    application = ((Activity) f7022t).getApplication();
                } else {
                    application = (Application) f7022t;
                }
                if (application != null) {
                    registerActivityLifecycleAutoStat(application, null);
                }
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void registerActivityLifecycleAutoStat(Application application, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableAutoMonitorActivityCycle()) {
            f7020r.warn("MTA Auto Monitor Activity Cycle is disable by user!");
        } else if (!f7025w && application != null && Build.VERSION.SDK_INT >= 14) {
            StatConfig.f6959q = true;
            f7022t = application.getApplicationContext();
            getHandler(f7022t);
            f7002D = statSpecifyReportedInfo;
            if (f7028z == null) {
                f7028z = new Handler();
            }
            synchronized (StatServiceImpl.class) {
                if (!f7025w) {
                    C317236 r1 = new MtaActivityLifecycleCallback() {
                        /* class com.tencent.stat.StatServiceImpl.C317236 */

                        public void onActivityCreated(Activity activity, Bundle bundle) {
                        }

                        public void onActivityDestroyed(Activity activity) {
                        }

                        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                        }

                        public void onActivityStarted(Activity activity) {
                        }

                        public void onActivityStopped(Activity activity) {
                        }

                        public void onActivityResumed(Activity activity) {
                            if (StatConfig.isEnableStatService()) {
                                if (StatConfig.f6959q) {
                                    StatServiceImpl.m7874b(activity, StatCommonHelper.getActivityName(activity), statSpecifyReportedInfo);
                                }
                                boolean unused = StatServiceImpl.f7027y = false;
                                boolean z = !StatServiceImpl.f7026x;
                                boolean unused2 = StatServiceImpl.f7026x = true;
                                if (StatServiceImpl.f7000B != null) {
                                    StatServiceImpl.f7028z.removeCallbacks(StatServiceImpl.f7000B);
                                }
                                if (z) {
                                    StatServiceImpl.f7020r.mo35388i("went foreground");
                                    long unused3 = StatServiceImpl.f7024v = System.currentTimeMillis();
                                    for (StatActionListener statActionListener : StatServiceImpl.f6999A) {
                                        try {
                                            statActionListener.onBecameForeground();
                                        } catch (Throwable th) {
                                            th.printStackTrace();
                                        }
                                    }
                                    return;
                                }
                                StatServiceImpl.f7020r.mo35388i("still foreground");
                            }
                        }

                        public void onActivityPaused(Activity activity) {
                            if (StatConfig.isEnableStatService()) {
                                if (StatConfig.f6959q) {
                                    StatServiceImpl.m7873b(activity, StatCommonHelper.getActivityName(activity), 1, statSpecifyReportedInfo);
                                }
                                boolean unused = StatServiceImpl.f7027y = true;
                                if (StatServiceImpl.f7000B != null) {
                                    StatServiceImpl.f7028z.removeCallbacks(StatServiceImpl.f7000B);
                                }
                                StatServiceImpl.f7028z.postDelayed(StatServiceImpl.f7000B = new Runnable() {
                                    /* class com.tencent.stat.StatServiceImpl.C317236.C31731 */

                                    public void run() {
                                        try {
                                            if (!StatServiceImpl.f7026x || !StatServiceImpl.f7027y) {
                                                StatServiceImpl.f7020r.mo35388i("still foreground");
                                                return;
                                            }
                                            StatServiceImpl.f7020r.mo35388i("went background");
                                            for (StatActionListener statActionListener : StatServiceImpl.f6999A) {
                                                statActionListener.onBecameBackground();
                                            }
                                            boolean unused = StatServiceImpl.f7026x = false;
                                        } catch (Throwable th) {
                                            StatServiceImpl.f7020r.mo35384e(th);
                                        }
                                    }
                                }, StatConfig.getBackgroundDelayTimestamp());
                            }
                        }
                    };
                    try {
                        addActionListener(new StatActionListener() {
                            /* class com.tencent.stat.StatServiceImpl.C317437 */

                            public void onBecameForeground() {
                                long unused = StatServiceImpl.f7001C = System.currentTimeMillis();
                            }

                            public void onBecameBackground() {
                                StatServiceImpl.m7856a(StatServiceImpl.f7022t, statSpecifyReportedInfo, C3208c.m7982a());
                            }
                        });
                        f7025w = MtaActivityLifeCycle.registerActivityLifecycleCallbacks(application, r1).booleanValue();
                        StatLogger statLogger = f7020r;
                        statLogger.mo35381d("enableAutoMonitorActivityCycle:" + f7025w + ",isAntoActivityLifecycleStat:" + StatConfig.isAntoActivityLifecycleStat());
                    } catch (Throwable th) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }

    public static void onResume(Context context, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        Context applicationContext = context.getApplicationContext();
        trackBeginPage(applicationContext, StatCommonHelper.getActivityName(applicationContext), statSpecifyReportedInfo);
    }

    public static void setEnvAttributes(Context context, Map<String, String> map) {
        if (map == null || map.size() > 512) {
            f7020r.error("The map in setEnvAttributes can't be null or its size can't exceed 512.");
            return;
        }
        try {
            C3204a.m7974a(context, map);
        } catch (JSONException e) {
            f7020r.mo35384e((Throwable) e);
        }
    }

    public static void reportEvent(Context context, final Event event, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null || event == null) {
                f7020r.error("context or event is null in reportEvent()");
            } else if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C31542 */

                    public void run() {
                        try {
                            new C3184c(event).mo35344a();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
        }
    }

    public static void reportQQ(Context context, final String str, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("context is null in reportQQ()");
            } else if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C31653 */

                    public void run() {
                        String str = str;
                        if (str == null || str.trim().length() == 0) {
                            StatServiceImpl.f7020r.mo35396w("qq num is null or empty.");
                            return;
                        }
                        String str2 = str;
                        StatConfig.f6948f = str2;
                        StatServiceImpl.m7871b(context2, new StatAccount(str2), statSpecifyReportedInfo);
                    }
                });
            }
        }
    }

    public static void reportAccount(Context context, final StatAccount statAccount, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.mo35383e("context is null in reportAccount.");
            } else if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C31754 */

                    public void run() {
                        StatAccount statAccount = statAccount;
                        if (statAccount == null || statAccount.getAccount().trim().length() == 0) {
                            StatServiceImpl.f7020r.mo35396w("account is null or empty.");
                            return;
                        }
                        StatConfig.setQQ(context2, statAccount.getAccount());
                        StatServiceImpl.m7871b(context2, statAccount, statSpecifyReportedInfo);
                    }
                });
            }
        }
    }

    public static void reportGameUser(Context context, final StatGameUser statGameUser, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.reportGameUser() can not be null!");
            } else if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C31765 */

                    public void run() {
                        StatGameUser statGameUser = statGameUser;
                        if (statGameUser == null) {
                            StatServiceImpl.f7020r.error("The gameUser of StatService.reportGameUser() can not be null!");
                        } else if (statGameUser.getAccount() == null || statGameUser.getAccount().length() == 0) {
                            StatServiceImpl.f7020r.error("The account of gameUser on StatService.reportGameUser() can not be null or empty!");
                        } else {
                            try {
                                new C3184c(new C3242f(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), statGameUser, statSpecifyReportedInfo)).mo35344a();
                            } catch (Throwable th) {
                                StatServiceImpl.f7020r.mo35384e(th);
                                StatServiceImpl.m7859a(context2, th);
                            }
                        }
                    }
                });
            }
        }
    }

    public static boolean startStatService(final Context context, String str, String str2, final StatSpecifyReportedInfo statSpecifyReportedInfo) throws MtaSDkException {
        try {
            if (!StatConfig.isEnableStatService()) {
                f7020r.error("MTA StatService is disable.");
                return false;
            }
            if (StatConfig.isDebugEnable()) {
                f7020r.mo35381d("MTA SDK version, current: " + StatConstants.VERSION + " ,required: " + str2);
            }
            if (context != null) {
                if (str2 != null) {
                    if (StatCommonHelper.getSDKLongVersion(StatConstants.VERSION) < StatCommonHelper.getSDKLongVersion(str2)) {
                        f7020r.error(("MTA SDK version conflicted, current: " + StatConstants.VERSION + ",required: " + str2) + ". please delete the current SDK and download the latest one. official website: http://mta.qq.com/ or http://mta.oa.com/");
                        StatConfig.setEnableStatService(false);
                        return false;
                    }
                    String installChannel = StatConfig.getInstallChannel(context);
                    if (installChannel == null || installChannel.length() == 0) {
                        StatConfig.setInstallChannel("-");
                    }
                    if (str != null) {
                        StatConfig.setAppKey(context, str);
                    }
                    if (getHandler(context) == null) {
                        return true;
                    }
                    f7007e.post(new Runnable() {
                        /* class com.tencent.stat.StatServiceImpl.C31776 */

                        public void run() {
                            try {
                                StatServiceImpl.getSessionID(context, false, statSpecifyReportedInfo);
                            } catch (Throwable th) {
                                StatServiceImpl.f7020r.mo35384e(th);
                            }
                        }
                    });
                    return true;
                }
            }
            f7020r.error("Context or mtaSdkVersion in StatService.startStatService() is null, please check it!");
            StatConfig.setEnableStatService(false);
            return false;
        } catch (Throwable th) {
            f7020r.mo35384e(th);
            return false;
        }
    }

    public static void onPause(Context context, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        Context applicationContext = context.getApplicationContext();
        trackEndPage(applicationContext, StatCommonHelper.getActivityName(applicationContext), statSpecifyReportedInfo);
    }

    public static void onStop(Context context, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C31787 */

                    public void run() {
                        Context context = context2;
                        if (context == null) {
                            StatServiceImpl.f7020r.error("The Context of StatService.onStop() can not be null!");
                            return;
                        }
                        StatServiceImpl.flushDataToDB(context);
                        if (!StatServiceImpl.m7863a()) {
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (StatCommonHelper.isBackgroundRunning(context2)) {
                                if (StatConfig.isDebugEnable()) {
                                    StatServiceImpl.f7020r.mo35388i("onStop isBackgroundRunning flushDataToDB");
                                }
                                StatServiceImpl.commitEvents(context2, -1);
                            }
                        }
                    }
                });
            }
        }
    }

    public static void onLowMemory(final Context context) {
        if (StatConfig.isEnableStatService() && getHandler(getContext(context)) != null) {
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C31798 */

                public void run() {
                    StatServiceImpl.flushDataToDB(context);
                }
            });
        }
    }

    public static int reportError(Context context, final String str, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.reportError() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.reportError() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str)) {
                f7020r.error("The length of err for StatService.reportError() exceeds the limit:61440");
                return 1001;
            }
            final Thread currentThread = Thread.currentThread();
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C31809 */

                    public void run() {
                        try {
                            new C3184c(new C3241e(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), str, 0, StatConfig.getMaxReportEventLength(), currentThread, statSpecifyReportedInfo)).mo35344a();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    public static int reportException(Context context, int i, String str) {
        return reportException(context, i, "", str);
    }

    public static int reportException(Context context, int i, String str, String str2) {
        return reportException(context, i, str, str2);
    }

    public static int reportException(Context context, int i, String str, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        return reportException(context, i, -1, "", str, statSpecifyReportedInfo);
    }

    public static int reportException(Context context, int i, long j, String str, String str2, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.reportException() can not be null!");
            return 1000;
        } else if (m7865a(str2)) {
            f7020r.error("The event_id of StatService.reportException() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, str2)) {
                f7020r.error("The length of err for StatService.reportException() exceeds the limit:61440");
                return 1001;
            }
            final Thread currentThread = Thread.currentThread();
            if (getHandler(context2) != null) {
                final StatSpecifyReportedInfo statSpecifyReportedInfo2 = statSpecifyReportedInfo;
                final String str3 = str2;
                final int i2 = i;
                final String str4 = str;
                final long j2 = j;
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C314410 */

                    public void run() {
                        try {
                            C3241e eVar = new C3241e(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo2), str3, i2, StatConfig.getMaxReportEventLength(), currentThread, statSpecifyReportedInfo2);
                            eVar.mo35470a(str4);
                            eVar.mo35469a(j2);
                            new C3184c(eVar).mo35344a();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    /* renamed from: a */
    static void m7859a(Context context, final Throwable th) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.reportSdkSelfException() can not be null!");
                return;
            }
            final Thread currentThread = Thread.currentThread();
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C314511 */

                    public void run() {
                        try {
                            if (StatConfig.isEnableStatService()) {
                                new C3184c(new C3241e(context2, StatServiceImpl.getSessionID(context2, false, null), 99, th, currentThread, C3244h.f7379a)).mo35344a();
                            }
                        } catch (Throwable th) {
                            StatLogger e = StatServiceImpl.f7020r;
                            e.mo35383e("reportSdkSelfException error: " + th);
                        }
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m7871b(Context context, StatAccount statAccount, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        try {
            new C3184c(new C3236a(context, getSessionID(context, false, statSpecifyReportedInfo), statAccount, statSpecifyReportedInfo)).mo35344a();
        } catch (Throwable th) {
            f7020r.mo35384e(th);
            m7859a(context, th);
        }
    }

    public static int reportException(Context context, final Throwable th, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.reportException() can not be null!");
            return 1000;
        } else if (th == null) {
            f7020r.error("The Throwable of StatService.reportException() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(th)) {
                f7020r.error("The length of err for StatService.reportException() exceeds the limit:61440");
                return 1001;
            }
            final Thread currentThread = Thread.currentThread();
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C314713 */

                    public void run() {
                        Context context = context2;
                        new C3184c(new C3241e(context, StatServiceImpl.getSessionID(context, false, statSpecifyReportedInfo), 1, th, currentThread, statSpecifyReportedInfo)).mo35344a();
                    }
                });
            }
            return 0;
        }
    }

    /* renamed from: c */
    static void m7881c(Context context) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (getHandler(context2) != null) {
                f7007e.postDelayed(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C314814 */

                    public void run() {
                        Context context = context2;
                        if (context == null) {
                            StatServiceImpl.f7020r.error("The Context of StatService.reportNativeCrash() can not be null!");
                            return;
                        }
                        try {
                            new Thread(new C3181a(context), "NativeCrashRepoter").start();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                }, (long) (StatCrashReporter.getStatCrashReporter(context2).getReportDelaySecOnStart() * 1000));
            }
        }
    }

    public static int reportCustomProperty(Context context, final JSONObject jSONObject, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.reportCustomProperty() can not be null!");
            return 1000;
        } else if (jSONObject == null) {
            f7020r.error("The jsonObject of StatService.reportCustomProperty() can not be null ");
            return 1000;
        } else if (getHandler(context2) == null) {
            return 0;
        } else {
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C314915 */

                public void run() {
                    try {
                        C3240d dVar = new C3240d(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), statSpecifyReportedInfo);
                        dVar.mo35467a(jSONObject);
                        new C3184c(dVar).mo35344a();
                    } catch (Throwable th) {
                        StatServiceImpl.f7020r.mo35384e(th);
                        StatServiceImpl.m7859a(context2, th);
                    }
                }
            });
            return 0;
        }
    }

    public static int trackCustomEvent(Context context, final String str, final StatSpecifyReportedInfo statSpecifyReportedInfo, final String... strArr) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomEvent() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomEvent() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, strArr)) {
                f7020r.error("The length of event_id/args for StatService.trackCustomEvent() exceeds the limit:61440");
                return 1001;
            }
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C315016 */

                    public void run() {
                        try {
                            C3238c.C3239a aVar = new C3238c.C3239a(str, strArr, null);
                            C3238c cVar = new C3238c(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), aVar.f7364a, statSpecifyReportedInfo);
                            cVar.mo35462a().f7365b = aVar.f7365b;
                            new C3184c(cVar).mo35344a();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    /* renamed from: d */
    static void m7884d(Context context) {
        if (StatConfig.isEnableStatService()) {
            Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.sendNetworkDetector() can not be null!");
                return;
            }
            try {
                C3220d.m8022b(context2).mo35408a(new C3244h(context2), new StatDispatchCallback() {
                    /* class com.tencent.stat.StatServiceImpl.C315117 */

                    public void onDispatchSuccess() {
                        StatServiceImpl.m7880c();
                    }

                    public void onDispatchFailure() {
                        StatServiceImpl.m7883d();
                    }
                });
            } catch (Throwable th) {
                f7020r.mo35384e(th);
            }
        }
    }

    public static void trackCustomKVEvent(Context context, final String str, final Properties properties, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        StatConfig.isEnableStatService();
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomKVEvent() can not be null!");
        }
        if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomKVEvent() can not be null or empty.");
        }
        if (StatCommonHelper.checkArgumentsLength(str, properties)) {
            f7020r.error("The length of event_id/properties for StatService.trackCustomKVEvent() exceeds the limit:61440");
        }
        if (getHandler(context2) != null) {
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C315218 */

                public void run() {
                    try {
                        C3238c.C3239a aVar = new C3238c.C3239a(str, null, properties);
                        C3238c cVar = new C3238c(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), aVar.f7364a, statSpecifyReportedInfo);
                        cVar.mo35462a().f7366c = aVar.f7366c;
                        new C3184c(cVar).mo35344a();
                    } catch (Throwable th) {
                        StatServiceImpl.f7020r.mo35384e(th);
                        StatServiceImpl.m7859a(context2, th);
                    }
                }
            });
        }
    }

    public static void trackRegAccountEvent(Context context, final String str, final StatConfig.AccountType accountType) {
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackRegAccountEvent() can not be null!");
        }
        if (getHandler(context2) != null) {
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C315319 */

                public void run() {
                    try {
                        new C3184c(new C3250m(context2, StatServiceImpl.getSessionID(context2, false, null), str, accountType)).mo35344a();
                    } catch (Throwable th) {
                        StatServiceImpl.f7020r.mo35384e(th);
                        StatServiceImpl.m7859a(context2, th);
                    }
                }
            });
        }
    }

    public static void trackPayEvent(Context context, String str, String str2, double d, StatConfig.CurrencyType currencyType) {
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackRegAccountEvent() can not be null!");
        }
        if (getHandler(context2) != null) {
            final String str3 = str;
            final String str4 = str2;
            final double d2 = d;
            final StatConfig.CurrencyType currencyType2 = currencyType;
            f7007e.post(new Runnable() {
                /* class com.tencent.stat.StatServiceImpl.C315520 */

                public void run() {
                    try {
                        new C3184c(new C3248l(context2, StatServiceImpl.getSessionID(context2, false, null), str3, str4, d2, currencyType2)).mo35344a();
                    } catch (Throwable th) {
                        StatServiceImpl.f7020r.mo35384e(th);
                        StatServiceImpl.m7859a(context2, th);
                    }
                }
            });
        }
    }

    public static void setCommonKeyValueForKVEvent(String str, Properties properties) {
        if (!StatCommonHelper.isStringValid(str)) {
            f7020r.mo35383e("event_id or commonProp for setCommonKeyValueForKVEvent is invalid.");
        } else if (properties == null || properties.size() <= 0) {
            f7009g.remove(str);
        } else {
            f7009g.put(str, (Properties) properties.clone());
        }
    }

    public static Properties getCommonKeyValueForKVEvent(String str) {
        return f7009g.get(str);
    }

    public static int trackCustomBeginEvent(Context context, final String str, StatSpecifyReportedInfo statSpecifyReportedInfo, final String... strArr) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomBeginEvent() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomBeginEvent() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, strArr)) {
                f7020r.error("The length of event_id/args for StatService.trackCustomBeginEvent() exceeds the limit:61440");
                return 1001;
            }
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C315621 */

                    public void run() {
                        try {
                            C3238c.C3239a aVar = new C3238c.C3239a(str, strArr, null);
                            if (StatConfig.isDebugEnable()) {
                                StatLogger e = StatServiceImpl.f7020r;
                                e.mo35388i("add begin key:" + aVar.toString());
                            }
                            if (StatServiceImpl.f7008f.containsKey(aVar)) {
                                StatLogger e2 = StatServiceImpl.f7020r;
                                e2.error("Duplicate CustomEvent key: " + aVar.toString() + ", trackCustomBeginEvent() repeated?");
                            } else if (StatServiceImpl.f7008f.size() <= StatConfig.getMaxParallelTimmingEvents()) {
                                StatServiceImpl.f7008f.put(aVar, Long.valueOf(System.currentTimeMillis()));
                            } else {
                                StatLogger e3 = StatServiceImpl.f7020r;
                                e3.error("The number of timedEvent exceeds the maximum value " + Integer.toString(StatConfig.getMaxParallelTimmingEvents()));
                            }
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    public static int trackCustomEndEvent(Context context, final String str, final StatSpecifyReportedInfo statSpecifyReportedInfo, final String... strArr) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomEndEvent() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomEndEvent() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, strArr)) {
                f7020r.error("The length of event_id/args for StatService.trackCustomEndEvent() exceeds the limit:61440");
                return 1001;
            }
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C315722 */

                    public void run() {
                        try {
                            C3238c.C3239a aVar = new C3238c.C3239a(str, strArr, null);
                            Long l = (Long) StatServiceImpl.f7008f.remove(aVar);
                            if (l != null) {
                                C3238c cVar = new C3238c(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), aVar.f7364a, statSpecifyReportedInfo);
                                cVar.mo35462a().f7365b = aVar.f7365b;
                                double currentTimeMillis = (double) (System.currentTimeMillis() - l.longValue());
                                Double.isNaN(currentTimeMillis);
                                double d = currentTimeMillis / 1000.0d;
                                if (d <= 0.0d) {
                                    d = 0.1d;
                                }
                                cVar.mo35463a(d);
                                new C3184c(cVar).mo35344a();
                                return;
                            }
                            StatLogger e = StatServiceImpl.f7020r;
                            e.error("No start time found for custom event: " + aVar.toString() + ", lost trackCustomBeginEvent()?");
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    public static int trackCustomBeginKVEvent(Context context, final String str, final Properties properties, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomBeginKVEvent() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomBeginKVEvent() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, properties)) {
                f7020r.error("The length of event_id/properties for StatService.trackCustomBeginKVEvent() exceeds the limit:61440");
                return 1001;
            }
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C315924 */

                    public void run() {
                        try {
                            C3238c.C3239a aVar = new C3238c.C3239a(str, null, properties);
                            if (StatConfig.isDebugEnable()) {
                                StatLogger e = StatServiceImpl.f7020r;
                                e.mo35388i("add begin key:" + aVar);
                            }
                            if (StatServiceImpl.f7008f.containsKey(aVar)) {
                                StatLogger e2 = StatServiceImpl.f7020r;
                                e2.warn("Duplicate CustomEvent key: " + aVar.toString() + ", trackCustomBeginKVEvent() repeated?");
                            } else if (StatServiceImpl.f7008f.size() <= StatConfig.getMaxParallelTimmingEvents()) {
                                StatServiceImpl.f7008f.put(aVar, Long.valueOf(System.currentTimeMillis()));
                            } else {
                                StatLogger e3 = StatServiceImpl.f7020r;
                                e3.error("The number of timedEvent exceeds the maximum value " + Integer.toString(StatConfig.getMaxParallelTimmingEvents()));
                            }
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    public static int trackCustomEndKVEvent(Context context, final String str, final Properties properties, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomEndKVEvent() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomEndKVEvent() can not be null or empty.");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, properties)) {
                f7020r.error("The length of event_id/properties for StatService.trackCustomEndKVEvent() exceeds the limit:61440");
                return 1001;
            }
            if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C316025 */

                    public void run() {
                        try {
                            C3238c.C3239a aVar = new C3238c.C3239a(str, null, properties);
                            Long l = (Long) StatServiceImpl.f7008f.remove(aVar);
                            if (l != null) {
                                C3238c cVar = new C3238c(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), aVar.f7364a, statSpecifyReportedInfo);
                                cVar.mo35462a().f7366c = aVar.f7366c;
                                double currentTimeMillis = (double) (System.currentTimeMillis() - l.longValue());
                                Double.isNaN(currentTimeMillis);
                                double d = currentTimeMillis / 1000.0d;
                                if (d <= 0.0d) {
                                    d = 0.1d;
                                }
                                cVar.mo35463a(d);
                                new C3184c(cVar).mo35344a();
                                return;
                            }
                            StatLogger e = StatServiceImpl.f7020r;
                            e.warn("No start time found for custom event: " + aVar.toString() + ", lost trackCustomBeginKVEvent()?");
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    public static void reportAppMonitorStat(Context context, StatAppMonitor statAppMonitor, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.reportAppMonitorStat() can not be null!");
            } else if (statAppMonitor == null) {
                f7020r.error("The StatAppMonitor of StatService.reportAppMonitorStat() can not be null!");
            } else if (statAppMonitor.getInterfaceName() == null) {
                f7020r.error("The interfaceName of StatAppMonitor on StatService.reportAppMonitorStat() can not be null!");
            } else {
                final StatAppMonitor clone = statAppMonitor.clone();
                if (getHandler(context2) != null) {
                    f7007e.post(new Runnable() {
                        /* class com.tencent.stat.StatServiceImpl.C316126 */

                        public void run() {
                            try {
                                new C3184c(new C3243g(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo), clone, statSpecifyReportedInfo)).mo35344a();
                            } catch (Throwable th) {
                                StatServiceImpl.f7020r.mo35384e(th);
                                StatServiceImpl.m7859a(context2, th);
                            }
                        }
                    });
                }
            }
        }
    }

    public static int trackCustomKVTimeIntervalEvent(Context context, String str, Properties properties, int i, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (!StatConfig.isEnableStatService()) {
            return -1;
        }
        final Context context2 = getContext(context);
        if (context2 == null) {
            f7020r.error("The Context of StatService.trackCustomKVTimeIntervalEvent() can not be null!");
            return 1000;
        } else if (m7865a(str)) {
            f7020r.error("The event_id of StatService.trackCustomKVTimeIntervalEvent() can not be null or empty.");
            return 1000;
        } else if (i <= 0) {
            f7020r.error("The intervalSecond of StatService.trackCustomTimeIntervalEvent() can must bigger than 0!");
            return 1000;
        } else {
            if (StatCommonHelper.checkArgumentsLength(str, properties)) {
                f7020r.error("The length of event_id/properties for StatService.trackCustomKVTimeIntervalEvent() exceeds the limit:61440");
                return 1001;
            }
            if (getHandler(context2) != null) {
                final String str2 = str;
                final Properties properties2 = properties;
                final StatSpecifyReportedInfo statSpecifyReportedInfo2 = statSpecifyReportedInfo;
                final int i2 = i;
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C316227 */

                    public void run() {
                        try {
                            C3238c.C3239a aVar = new C3238c.C3239a(str2, null, properties2);
                            C3238c cVar = new C3238c(context2, StatServiceImpl.getSessionID(context2, false, statSpecifyReportedInfo2), aVar.f7364a, statSpecifyReportedInfo2);
                            cVar.mo35462a().f7366c = aVar.f7366c;
                            double d = (double) i2;
                            if (d <= 0.0d) {
                                d = 0.1d;
                            }
                            cVar.mo35463a(d);
                            new C3184c(cVar).mo35344a();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
            return 0;
        }
    }

    public static void commitEvents(Context context, final int i) {
        if (StatConfig.isEnableStatService()) {
            if (StatConfig.isDebugEnable()) {
                StatLogger statLogger = f7020r;
                statLogger.mo35388i("commitEvents, maxNumber=" + i);
            }
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.commitEvents() can not be null!");
            } else if (i < -1 || i == 0) {
                f7020r.error("The maxNumber of StatService.commitEvents() should be -1 or bigger than 0.");
            } else if (NetworkManager.getInstance(context2).isNetworkAvailable() && getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C316328 */

                    public void run() {
                        try {
                            StatServiceImpl.flushDataToDB(context2);
                            C3226e.m8037a(context2).mo35424a(i);
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
        }
    }

    public static void testSpeed(Context context) {
        m7872b(context, (StatSpecifyReportedInfo) null);
    }

    /* renamed from: b */
    static void m7872b(Context context, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.testSpeed() can not be null!");
            } else if (getHandler(context2) != null) {
                f7007e.post(new Runnable() {
                    /* class com.tencent.stat.StatServiceImpl.C316429 */

                    public void run() {
                        try {
                            new Thread(new C3183b(context2, null, statSpecifyReportedInfo), "NetworkMonitorTask").start();
                        } catch (Throwable th) {
                            StatServiceImpl.f7020r.mo35384e(th);
                            StatServiceImpl.m7859a(context2, th);
                        }
                    }
                });
            }
        }
    }

    public static void testSpeed(Context context, Map<String, Integer> map, final StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (StatConfig.isEnableStatService()) {
            final Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.testSpeed() can not be null!");
            } else if (map == null || map.size() == 0) {
                f7020r.error("The domainMap of StatService.testSpeed() can not be null or empty!");
            } else {
                final HashMap hashMap = new HashMap(map);
                if (getHandler(context2) != null) {
                    f7007e.post(new Runnable() {
                        /* class com.tencent.stat.StatServiceImpl.C316630 */

                        public void run() {
                            try {
                                new Thread(new C3183b(context2, hashMap, statSpecifyReportedInfo), "NetworkMonitorTask").start();
                            } catch (Throwable th) {
                                StatServiceImpl.f7020r.mo35384e(th);
                                StatServiceImpl.m7859a(context2, th);
                            }
                        }
                    });
                }
            }
        }
    }

    public static void flushDataToDB(Context context) {
        if (StatConfig.isEnableStatService() && StatConfig.f6955m > 0) {
            Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.testSpeed() can not be null!");
            } else {
                C3226e.m8037a(context2).mo35431d();
            }
        }
    }

    public static void getFeedBackMessage(Context context, int i, int i2, StatFBDispatchCallback statFBDispatchCallback) {
        if (StatConfig.isEnableStatService()) {
            Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.getFeedBackMessage() can not be null!");
                return;
            }
            try {
                C3220d.m8022b(context2).mo35414b(i, i2, statFBDispatchCallback);
            } catch (Throwable th) {
                f7020r.mo35384e(th);
            }
        }
    }

    public static void postFeedBackFiles(Context context, String str, String str2, StatFBDispatchCallback statFBDispatchCallback) {
        if (StatConfig.isEnableStatService()) {
            Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.postFeedBackFiles() can not be null!");
                return;
            }
            try {
                C3220d.m8022b(context2).mo35415b(str, str2, statFBDispatchCallback);
            } catch (Throwable th) {
                f7020r.mo35384e(th);
            }
        }
    }

    public static void replyFeedBackMessage(Context context, String str, String str2, StatFBDispatchCallback statFBDispatchCallback) {
        if (StatConfig.isEnableStatService()) {
            Context context2 = getContext(context);
            if (context2 == null) {
                f7020r.error("The Context of StatService.replyFeedBackMessage() can not be null!");
                return;
            }
            try {
                C3220d.m8022b(context2).mo35417c(str, str2, statFBDispatchCallback);
            } catch (Throwable th) {
                f7020r.mo35384e(th);
            }
        }
    }

    /* renamed from: com.tencent.stat.StatServiceImpl$a */
    static class C3181a implements Runnable {

        /* renamed from: a */
        private Context f7133a = null;

        public C3181a(Context context) {
            this.f7133a = context;
        }

        public void run() {
            Iterator<File> it = StatNativeCrashReport.getCrashEventsFilesList(this.f7133a).iterator();
            while (it.hasNext()) {
                final File next = it.next();
                if (StatConfig.isDebugEnable() && NetworkManager.getInstance(StatServiceImpl.f7022t).isNetworkAvailable() && !StatServiceImpl.m7864a(next)) {
                    C3220d.m8022b(StatServiceImpl.f7022t).mo35409a(C3207b.m7977a(this.f7133a, next.getAbsolutePath()), new StatDispatchCallback() {
                        /* class com.tencent.stat.StatServiceImpl.C3181a.C31821 */

                        public void onDispatchSuccess() {
                            next.delete();
                            StatServiceImpl.m7882c(next);
                        }

                        public void onDispatchFailure() {
                            StatServiceImpl.m7885d(next);
                        }
                    });
                }
            }
        }
    }

    /* renamed from: a */
    static boolean m7864a(File file) {
        if (StatPreferences.getInt(f7022t, m7868b(file), 0) != 65535) {
            return false;
        }
        file.delete();
        return true;
    }

    /* renamed from: b */
    static String m7868b(File file) {
        return "__MTA.CRASH.FILE.SEND.CNT." + file.getName();
    }

    /* renamed from: c */
    static void m7882c(File file) {
        file.delete();
        StatPreferences.putInt(f7022t, m7868b(file), 65535);
    }

    /* renamed from: d */
    static void m7885d(File file) {
        String b = m7868b(file);
        int i = StatPreferences.getInt(f7022t, b, 0) + 1;
        if (i >= StatCrashReporter.getStatCrashReporter(f7022t).getMaxNumOfRetries()) {
            m7882c(file);
        } else {
            StatPreferences.putInt(f7022t, b, i);
        }
    }

    /* renamed from: com.tencent.stat.StatServiceImpl$b */
    static class C3183b implements Runnable {

        /* renamed from: a */
        private Context f7136a = null;

        /* renamed from: b */
        private Map<String, Integer> f7137b = null;

        /* renamed from: c */
        private StatSpecifyReportedInfo f7138c = null;

        public C3183b(Context context, Map<String, Integer> map, StatSpecifyReportedInfo statSpecifyReportedInfo) {
            this.f7136a = context;
            this.f7138c = statSpecifyReportedInfo;
            if (map != null) {
                this.f7137b = map;
            }
        }

        /* renamed from: a */
        private Map<String, Integer> m7904a() {
            String str;
            HashMap hashMap = new HashMap();
            String sDKProperty = StatConfig.getSDKProperty("__MTA_TEST_SPEED__", null);
            if (!(sDKProperty == null || sDKProperty.trim().length() == 0)) {
                for (String str2 : sDKProperty.split(";")) {
                    String[] split = str2.split(",");
                    if (!(split == null || split.length != 2 || (str = split[0]) == null || str.trim().length() == 0)) {
                        try {
                            hashMap.put(str, Integer.valueOf(Integer.valueOf(split[1]).intValue()));
                        } catch (NumberFormatException e) {
                            StatServiceImpl.f7020r.mo35384e((Throwable) e);
                        }
                    }
                }
            }
            return hashMap;
        }

        public void run() {
            try {
                if (this.f7137b == null) {
                    this.f7137b = m7904a();
                }
                if (this.f7137b != null) {
                    if (this.f7137b.size() != 0) {
                        JSONArray jSONArray = new JSONArray();
                        for (Map.Entry entry : this.f7137b.entrySet()) {
                            String str = (String) entry.getKey();
                            if (str != null) {
                                if (str.length() != 0) {
                                    if (((Integer) entry.getValue()) == null) {
                                        StatLogger e = StatServiceImpl.f7020r;
                                        e.mo35396w("port is null for " + str);
                                    } else {
                                        jSONArray.put(m7903a((String) entry.getKey(), ((Integer) entry.getValue()).intValue()).toJSONObject());
                                    }
                                }
                            }
                            StatServiceImpl.f7020r.mo35396w("empty domain name.");
                        }
                        if (jSONArray.length() != 0) {
                            C3245i iVar = new C3245i(this.f7136a, StatServiceImpl.getSessionID(this.f7136a, false, this.f7138c), this.f7138c);
                            iVar.mo35472a(jSONArray.toString());
                            new C3184c(iVar).mo35344a();
                            return;
                        }
                        return;
                    }
                }
                StatServiceImpl.f7020r.mo35388i("empty domain list.");
            } catch (Throwable th) {
                StatServiceImpl.f7020r.mo35384e(th);
            }
        }

        /* renamed from: a */
        private NetworkMonitor m7903a(String str, int i) {
            int i2;
            NetworkMonitor networkMonitor = new NetworkMonitor();
            Socket socket = new Socket();
            try {
                networkMonitor.setDomain(str);
                networkMonitor.setPort(i);
                long currentTimeMillis = System.currentTimeMillis();
                InetSocketAddress inetSocketAddress = new InetSocketAddress(str, i);
                socket.connect(inetSocketAddress, 30000);
                networkMonitor.setMillisecondsConsume(System.currentTimeMillis() - currentTimeMillis);
                networkMonitor.setRemoteIp(inetSocketAddress.getAddress().getHostAddress());
                socket.close();
                try {
                    socket.close();
                } catch (Throwable th) {
                    StatServiceImpl.f7020r.mo35384e(th);
                }
                i2 = 0;
            } catch (IOException e) {
                try {
                    StatServiceImpl.f7020r.mo35384e((Throwable) e);
                    socket.close();
                } catch (Throwable th2) {
                    StatServiceImpl.f7020r.mo35384e(th2);
                }
            } catch (Throwable th3) {
                StatServiceImpl.f7020r.mo35384e(th3);
            }
            networkMonitor.setStatusCode(i2);
            return networkMonitor;
            i2 = -1;
            networkMonitor.setStatusCode(i2);
            return networkMonitor;
            throw th;
        }
    }

    /* renamed from: c */
    static void m7880c() {
        f7004b = 0;
        f7005c = 0;
    }

    /* renamed from: d */
    static void m7883d() {
        f7004b++;
        f7005c = System.currentTimeMillis();
        flushDataToDB(f7022t);
    }

    /* renamed from: e */
    static void m7887e(Context context) {
        f7006d = System.currentTimeMillis() + ((long) (StatConfig.getSendPeriodMinutes() * 60000));
        StatPreferences.putLong(context, "last_period_ts", f7006d);
        commitEvents(context, -1);
    }

    /* renamed from: com.tencent.stat.StatServiceImpl$c */
    public static class C3184c {

        /* renamed from: f */
        private static volatile long f7139f;
        /* access modifiers changed from: private */

        /* renamed from: a */
        public Event f7140a;

        /* renamed from: b */
        private StatReportStrategy f7141b = null;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public boolean f7142c = false;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public Context f7143d = null;

        /* renamed from: e */
        private long f7144e = System.currentTimeMillis();

        public C3184c(Event event) {
            this.f7140a = event;
            this.f7141b = StatConfig.getStatSendStrategy();
            this.f7142c = event.isImportant();
            this.f7143d = event.getContext();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
         arg types: [com.tencent.stat.event.Event, ?[OBJECT, ARRAY], boolean, int]
         candidates:
          com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
          com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
          com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
          com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
         arg types: [com.tencent.stat.event.Event, com.tencent.stat.StatServiceImpl$c$1, boolean, int]
         candidates:
          com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
          com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
          com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
          com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
        /* renamed from: b */
        private void m7908b() {
            if (this.f7140a.getType() == EventType.CUSTOM) {
                String str = ((C3238c) this.f7140a).mo35462a().f7364a;
                if (StatConfig.m7824a(str)) {
                    StatLogger e = StatServiceImpl.f7020r;
                    e.mo35381d("eventid=" + str + " matched, report instant.");
                    m7911d();
                    return;
                } else if (StatConfig.isEventIdInDontReportEventIdsSet(str)) {
                    StatLogger e2 = StatServiceImpl.f7020r;
                    e2.mo35396w("eventid=" + str + " In DontReportEventIdsSet, droped.");
                    return;
                }
            }
            if (this.f7140a.getStatSpecifyReportedInfo() != null && this.f7140a.getStatSpecifyReportedInfo().isSendImmediately()) {
                this.f7141b = StatReportStrategy.INSTANT;
            }
            if (StatConfig.f6952j && NetworkManager.getInstance(StatServiceImpl.f7022t).isWifi()) {
                this.f7141b = StatReportStrategy.INSTANT;
            }
            if (StatConfig.isDebugEnable()) {
                StatLogger e3 = StatServiceImpl.f7020r;
                e3.mo35388i("strategy=" + this.f7141b.name());
            }
            switch (this.f7141b) {
                case INSTANT:
                    m7909c();
                    return;
                case PERIOD:
                    C3226e.m8037a(this.f7143d).mo35426a(this.f7140a, (StatDispatchCallback) null, this.f7142c, false);
                    if (StatConfig.isDebugEnable()) {
                        StatLogger e4 = StatServiceImpl.f7020r;
                        e4.mo35388i("PERIOD currTime=" + this.f7144e + ",nextPeriodSendTs=" + StatServiceImpl.f7006d + ",difftime=" + (StatServiceImpl.f7006d - this.f7144e));
                    }
                    if (StatServiceImpl.f7006d == 0) {
                        StatServiceImpl.f7006d = StatPreferences.getLong(this.f7143d, "last_period_ts", 0);
                        if (this.f7144e > StatServiceImpl.f7006d) {
                            StatServiceImpl.m7887e(this.f7143d);
                        }
                        long sendPeriodMinutes = this.f7144e + ((long) (StatConfig.getSendPeriodMinutes() * 60 * 1000));
                        if (StatServiceImpl.f7006d > sendPeriodMinutes) {
                            StatServiceImpl.f7006d = sendPeriodMinutes;
                        }
                        C3196b.m7935a(this.f7143d).mo35370a();
                    }
                    if (StatConfig.isDebugEnable()) {
                        StatLogger e5 = StatServiceImpl.f7020r;
                        e5.mo35388i("PERIOD currTime=" + this.f7144e + ",nextPeriodSendTs=" + StatServiceImpl.f7006d + ",difftime=" + (StatServiceImpl.f7006d - this.f7144e));
                    }
                    if (this.f7144e > StatServiceImpl.f7006d) {
                        StatServiceImpl.m7887e(this.f7143d);
                        return;
                    }
                    return;
                case APP_LAUNCH:
                case DEVELOPER:
                    C3226e.m8037a(this.f7143d).mo35426a(this.f7140a, (StatDispatchCallback) null, this.f7142c, false);
                    return;
                case BATCH:
                    C3226e.m8037a(this.f7143d).mo35426a(this.f7140a, (StatDispatchCallback) new StatDispatchCallback() {
                        /* class com.tencent.stat.StatServiceImpl.C3184c.C31851 */

                        public void onDispatchSuccess() {
                            StatServiceImpl.m7880c();
                            if (C3226e.m8052b().mo35423a() >= StatConfig.getMaxBatchReportCount()) {
                                C3226e.m8052b().mo35424a(StatConfig.getMaxBatchReportCount());
                            }
                        }

                        public void onDispatchFailure() {
                            StatServiceImpl.m7883d();
                        }
                    }, this.f7142c, true);
                    return;
                case ONLY_WIFI:
                    if (NetworkManager.getInstance(StatServiceImpl.f7022t).getNetworkType() == 1) {
                        m7909c();
                        return;
                    } else {
                        C3226e.m8037a(this.f7143d).mo35426a(this.f7140a, (StatDispatchCallback) null, this.f7142c, false);
                        return;
                    }
                case ONLY_WIFI_NO_CACHE:
                    if (StatCommonHelper.isWiFiActive(this.f7143d)) {
                        m7906a(new StatDispatchCallback() {
                            /* class com.tencent.stat.StatServiceImpl.C3184c.C31862 */

                            public void onDispatchSuccess() {
                                StatServiceImpl.m7880c();
                            }

                            public void onDispatchFailure() {
                                StatServiceImpl.m7883d();
                            }
                        });
                        return;
                    }
                    return;
                default:
                    StatLogger e6 = StatServiceImpl.f7020r;
                    e6.error("Invalid stat strategy:" + StatConfig.getStatSendStrategy());
                    return;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
         arg types: [com.tencent.stat.event.Event, ?[OBJECT, ARRAY], boolean, int]
         candidates:
          com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
          com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
          com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
          com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
        /* renamed from: c */
        private void m7909c() {
            if ((C3226e.m8052b().f7299a <= 0 || !StatConfig.f6954l) && this.f7140a.getType() != EventType.BACKGROUND) {
                m7906a(new StatDispatchCallback() {
                    /* class com.tencent.stat.StatServiceImpl.C3184c.C31873 */

                    public void onDispatchSuccess() {
                        StatServiceImpl.m7880c();
                        if (C3184c.this.f7140a.getType() == EventType.SESSION_ENV && StatCommonHelper.needReportApp(StatServiceImpl.f7022t)) {
                            StatServiceImpl.f7020r.mo35381d("OnceEvent report");
                            C3220d.m8022b(StatServiceImpl.f7022t).mo35408a(new C3246j(StatServiceImpl.f7022t, StatServiceImpl.f7015m, null, C3184c.this.f7140a.getStatSpecifyReportedInfo()), new StatDispatchCallback() {
                                /* class com.tencent.stat.StatServiceImpl.C3184c.C31873.C31881 */

                                public void onDispatchFailure() {
                                }

                                public void onDispatchSuccess() {
                                    StatCommonHelper.updateNextReportTime(StatServiceImpl.f7022t);
                                }
                            });
                        }
                        if (C3226e.m8052b().f7299a > 0) {
                            StatServiceImpl.commitEvents(C3184c.this.f7143d, -1);
                        }
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
                     arg types: [com.tencent.stat.event.Event, ?[OBJECT, ARRAY], boolean, int]
                     candidates:
                      com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
                      com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
                      com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
                      com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
                    public void onDispatchFailure() {
                        C3226e.m8052b().mo35426a(C3184c.this.f7140a, (StatDispatchCallback) null, C3184c.this.f7142c, true);
                        StatServiceImpl.m7883d();
                    }
                });
                return;
            }
            C3226e.m8052b().mo35426a(this.f7140a, (StatDispatchCallback) null, this.f7142c, true);
            C3226e.m8052b().mo35424a(-1);
        }

        /* renamed from: d */
        private void m7911d() {
            m7906a(new StatDispatchCallback() {
                /* class com.tencent.stat.StatServiceImpl.C3184c.C31894 */

                public void onDispatchSuccess() {
                    StatServiceImpl.m7880c();
                }

                /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                 method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
                 arg types: [com.tencent.stat.event.Event, ?[OBJECT, ARRAY], boolean, int]
                 candidates:
                  com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
                  com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
                  com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
                  com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
                public void onDispatchFailure() {
                    C3226e.m8052b().mo35426a(C3184c.this.f7140a, (StatDispatchCallback) null, C3184c.this.f7142c, true);
                    StatServiceImpl.m7883d();
                }
            });
        }

        /* renamed from: a */
        private void m7906a(StatDispatchCallback statDispatchCallback) {
            C3220d.m8022b(StatServiceImpl.f7022t).mo35408a(this.f7140a, statDispatchCallback);
        }

        /* renamed from: e */
        private boolean m7912e() {
            if (StatConfig.f6950h <= 0) {
                return false;
            }
            if (this.f7144e > StatServiceImpl.f7012j) {
                StatServiceImpl.f7010h.clear();
                long unused = StatServiceImpl.f7012j = this.f7144e + StatConfig.f6951i;
                if (StatConfig.isDebugEnable()) {
                    StatLogger e = StatServiceImpl.f7020r;
                    e.mo35388i("clear methodsCalledLimitMap, nextLimitCallClearTime=" + StatServiceImpl.f7012j);
                }
            }
            Integer valueOf = Integer.valueOf(this.f7140a.getType().mo35459a());
            Integer num = (Integer) StatServiceImpl.f7010h.get(valueOf);
            if (num != null) {
                StatServiceImpl.f7010h.put(valueOf, Integer.valueOf(num.intValue() + 1));
                if (num.intValue() <= StatConfig.f6950h) {
                    return false;
                }
                if (StatConfig.isDebugEnable()) {
                    StatLogger e2 = StatServiceImpl.f7020r;
                    e2.mo35383e("event " + this.f7140a.toJsonString() + " was discard, cause of called limit, current:" + num + ", limit:" + StatConfig.f6950h + ", period:" + StatConfig.f6951i + " ms");
                }
                return true;
            }
            StatServiceImpl.f7010h.put(valueOf, 1);
            return false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void
         arg types: [com.tencent.stat.event.Event, ?[OBJECT, ARRAY], boolean, int]
         candidates:
          com.tencent.stat.e.a(android.content.Context, java.lang.String, int, long):void
          com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, int, boolean):void
          com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean, boolean):void
          com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean, boolean):void */
        /* renamed from: a */
        public void mo35344a() {
            if (!m7912e()) {
                if (StatConfig.f6958p != null) {
                    String jsonString = this.f7140a.toJsonString();
                    if (StatConfig.isDebugEnable()) {
                        StatLogger e = StatServiceImpl.f7020r;
                        e.mo35388i("transfer event data:" + jsonString);
                    }
                    StatConfig.f6958p.onTransfer(jsonString);
                    return;
                }
                if (StatConfig.f6955m > 0 && this.f7144e >= f7139f) {
                    StatServiceImpl.flushDataToDB(this.f7143d);
                    f7139f = this.f7144e + StatConfig.f6956n;
                    if (StatConfig.isDebugEnable()) {
                        StatLogger e2 = StatServiceImpl.f7020r;
                        e2.mo35388i("nextFlushTime=" + f7139f);
                    }
                }
                if (NetworkManager.getInstance(this.f7143d).isNetworkAvailable()) {
                    if (StatConfig.isDebugEnable()) {
                        StatLogger e3 = StatServiceImpl.f7020r;
                        e3.mo35388i("sendFailedCount=" + StatServiceImpl.f7004b);
                    }
                    if (!StatServiceImpl.m7863a()) {
                        m7908b();
                        return;
                    }
                    C3226e.m8037a(this.f7143d).mo35426a(this.f7140a, (StatDispatchCallback) null, this.f7142c, false);
                    if (this.f7144e - StatServiceImpl.f7005c > 1800000) {
                        StatServiceImpl.m7884d(this.f7143d);
                        return;
                    }
                    return;
                }
                C3226e.m8037a(this.f7143d).mo35426a(this.f7140a, (StatDispatchCallback) null, this.f7142c, false);
            }
        }
    }
}
