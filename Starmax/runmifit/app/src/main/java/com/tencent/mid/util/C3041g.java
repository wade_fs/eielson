package com.tencent.mid.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;
import java.lang.reflect.Method;

/* renamed from: com.tencent.mid.util.g */
public class C3041g {

    /* renamed from: c */
    private static volatile C3041g f6610c;

    /* renamed from: a */
    private int f6611a = 10;

    /* renamed from: b */
    private int f6612b = 0;

    /* renamed from: d */
    private Context f6613d = null;

    /* renamed from: e */
    private boolean f6614e = false;

    /* renamed from: a */
    public boolean mo34959a(String str, String str2) {
        if (!this.f6614e) {
            return false;
        }
        try {
            return Settings.System.putString(this.f6613d.getContentResolver(), str, str2);
        } catch (Throwable th) {
            int i = this.f6612b;
            this.f6612b = i + 1;
            if (i >= this.f6611a) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    /* renamed from: a */
    public String mo34957a(String str) {
        try {
            return Settings.System.getString(this.f6613d.getContentResolver(), str);
        } catch (Throwable th) {
            int i = this.f6612b;
            this.f6612b = i + 1;
            if (i >= this.f6611a) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public boolean mo34958a(String str, int i) {
        if (!this.f6614e) {
            return false;
        }
        try {
            return Settings.System.putInt(this.f6613d.getContentResolver(), str, i);
        } catch (Throwable th) {
            int i2 = this.f6612b;
            this.f6612b = i2 + 1;
            if (i2 >= this.f6611a) {
                return false;
            }
            th.printStackTrace();
            return false;
        }
    }

    /* renamed from: b */
    public int mo34960b(String str, int i) {
        try {
            return Settings.System.getInt(this.f6613d.getContentResolver(), str, i);
        } catch (Throwable th) {
            int i2 = this.f6612b;
            this.f6612b = i2 + 1;
            if (i2 < this.f6611a) {
                th.printStackTrace();
            }
            return i;
        }
    }

    private C3041g(Context context) {
        this.f6613d = context.getApplicationContext();
        try {
            this.f6614e = Util.checkPermission(this.f6613d, "android.permission.WRITE_SETTINGS");
            if (this.f6614e && Build.VERSION.SDK_INT >= 23) {
                Method declaredMethod = Settings.System.class.getDeclaredMethod("canWrite", Context.class);
                declaredMethod.setAccessible(true);
                this.f6614e = ((Boolean) declaredMethod.invoke(null, this.f6613d)).booleanValue();
            }
        } catch (Throwable th) {
            int i = this.f6612b;
            this.f6612b = i + 1;
            if (i < this.f6611a) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public static C3041g m7509a(Context context) {
        if (f6610c == null) {
            synchronized (C3041g.class) {
                if (f6610c == null) {
                    f6610c = new C3041g(context);
                }
            }
        }
        return f6610c;
    }
}
