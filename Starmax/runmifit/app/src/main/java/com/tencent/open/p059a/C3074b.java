package com.tencent.open.p059a;

import com.tencent.open.p059a.C3076d;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/* renamed from: com.tencent.open.a.b */
/* compiled from: ProGuard */
public class C3074b {

    /* renamed from: a */
    private static SimpleDateFormat f6714a = C3076d.C3080d.m7625a("yy.MM.dd.HH");

    /* renamed from: b */
    private String f6715b = "Tracer.File";

    /* renamed from: c */
    private int f6716c = Integer.MAX_VALUE;

    /* renamed from: d */
    private int f6717d = Integer.MAX_VALUE;

    /* renamed from: e */
    private int f6718e = 4096;

    /* renamed from: f */
    private long f6719f = 10000;

    /* renamed from: g */
    private File f6720g;

    /* renamed from: h */
    private int f6721h = 10;

    /* renamed from: i */
    private String f6722i = ".log";

    /* renamed from: j */
    private long f6723j = Long.MAX_VALUE;

    public C3074b(File file, int i, int i2, int i3, String str, long j, int i4, String str2, long j2) {
        mo35085a(file);
        mo35088b(i);
        mo35083a(i2);
        mo35092c(i3);
        mo35086a(str);
        mo35084a(j);
        mo35094d(i4);
        mo35090b(str2);
        mo35089b(j2);
    }

    /* renamed from: a */
    public File mo35082a() {
        return m7597c(System.currentTimeMillis());
    }

    /* renamed from: c */
    private File m7597c(long j) {
        File b = mo35087b();
        try {
            return new File(b, m7598c(m7599d(j)));
        } catch (Throwable th) {
            th.printStackTrace();
            return b;
        }
    }

    /* renamed from: d */
    private String m7599d(long j) {
        Calendar instance = Calendar.getInstance();
        instance.setTimeInMillis(j);
        return new SimpleDateFormat("yy.MM.dd.HH").format(instance.getTime());
    }

    /* renamed from: c */
    private String m7598c(String str) {
        return "com.tencent.mobileqq_connectSdk." + str + ".log";
    }

    /* renamed from: b */
    public File mo35087b() {
        File e = mo35095e();
        e.mkdirs();
        return e;
    }

    /* renamed from: c */
    public String mo35091c() {
        return this.f6715b;
    }

    /* renamed from: a */
    public void mo35086a(String str) {
        this.f6715b = str;
    }

    /* renamed from: a */
    public void mo35083a(int i) {
        this.f6716c = i;
    }

    /* renamed from: b */
    public void mo35088b(int i) {
        this.f6717d = i;
    }

    /* renamed from: d */
    public int mo35093d() {
        return this.f6718e;
    }

    /* renamed from: c */
    public void mo35092c(int i) {
        this.f6718e = i;
    }

    /* renamed from: a */
    public void mo35084a(long j) {
        this.f6719f = j;
    }

    /* renamed from: e */
    public File mo35095e() {
        return this.f6720g;
    }

    /* renamed from: a */
    public void mo35085a(File file) {
        this.f6720g = file;
    }

    /* renamed from: f */
    public int mo35096f() {
        return this.f6721h;
    }

    /* renamed from: d */
    public void mo35094d(int i) {
        this.f6721h = i;
    }

    /* renamed from: b */
    public void mo35090b(String str) {
        this.f6722i = str;
    }

    /* renamed from: b */
    public void mo35089b(long j) {
        this.f6723j = j;
    }
}
