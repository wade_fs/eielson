package com.tencent.bugly.beta.global;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.util.DisplayMetrics;
import com.tencent.bugly.beta.UpgradeInfo;
import com.tencent.bugly.beta.download.C2802b;
import com.tencent.bugly.beta.download.DownloadListener;
import com.tencent.bugly.beta.interfaces.BetaPatchListener;
import com.tencent.bugly.beta.p050ui.UILifecycleListener;
import com.tencent.bugly.beta.upgrade.BetaUploadStrategy;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.tencent.bugly.beta.global.e */
/* compiled from: BUGLY */
public class C2808e {

    /* renamed from: E */
    public static C2808e f5336E = new C2808e();

    /* renamed from: a */
    public static int f5337a;

    /* renamed from: A */
    public SharedPreferences f5338A;

    /* renamed from: B */
    public DisplayMetrics f5339B;

    /* renamed from: C */
    public boolean f5340C = true;

    /* renamed from: D */
    public boolean f5341D = false;

    /* renamed from: F */
    public BetaUploadStrategy f5342F;

    /* renamed from: G */
    public File f5343G;

    /* renamed from: H */
    public File f5344H;

    /* renamed from: I */
    public File f5345I;

    /* renamed from: J */
    public String f5346J = "";

    /* renamed from: K */
    public String f5347K = "";

    /* renamed from: L */
    public String f5348L = "";

    /* renamed from: M */
    public String f5349M = "";

    /* renamed from: N */
    public boolean f5350N = false;

    /* renamed from: O */
    public int f5351O = 0;

    /* renamed from: P */
    public String f5352P = "";

    /* renamed from: Q */
    public boolean f5353Q = false;

    /* renamed from: R */
    public boolean f5354R = true;

    /* renamed from: S */
    public boolean f5355S = false;

    /* renamed from: T */
    public boolean f5356T = false;

    /* renamed from: U */
    public boolean f5357U = true;

    /* renamed from: V */
    public boolean f5358V = true;

    /* renamed from: W */
    public BetaPatchListener f5359W;

    /* renamed from: X */
    public boolean f5360X = false;

    /* renamed from: Y */
    public boolean f5361Y = true;

    /* renamed from: Z */
    public boolean f5362Z = false;

    /* renamed from: aa */
    public final List<String> f5363aa = new ArrayList();

    /* renamed from: ab */
    public boolean f5364ab = false;

    /* renamed from: ac */
    public boolean f5365ac = false;

    /* renamed from: ad */
    public boolean f5366ad;

    /* renamed from: ae */
    public int f5367ae = 1;

    /* renamed from: b */
    public long f5368b = 3000;

    /* renamed from: c */
    public long f5369c = 0;

    /* renamed from: d */
    public boolean f5370d = true;

    /* renamed from: e */
    public boolean f5371e = true;

    /* renamed from: f */
    public int f5372f;

    /* renamed from: g */
    public int f5373g;

    /* renamed from: h */
    public int f5374h;

    /* renamed from: i */
    public int f5375i;

    /* renamed from: j */
    public int f5376j;

    /* renamed from: k */
    public UILifecycleListener<UpgradeInfo> f5377k;

    /* renamed from: l */
    public File f5378l;

    /* renamed from: m */
    public final List<Class<? extends Activity>> f5379m = new ArrayList();

    /* renamed from: n */
    public final List<Class<? extends Activity>> f5380n = new ArrayList();

    /* renamed from: o */
    public int f5381o;

    /* renamed from: p */
    public C2802b f5382p;

    /* renamed from: q */
    public DownloadListener f5383q;

    /* renamed from: r */
    public File f5384r;

    /* renamed from: s */
    public Context f5385s;

    /* renamed from: t */
    public File f5386t;

    /* renamed from: u */
    public String f5387u;

    /* renamed from: v */
    public String f5388v;

    /* renamed from: w */
    public int f5389w = Integer.MIN_VALUE;

    /* renamed from: x */
    public String f5390x = "";

    /* renamed from: y */
    public String f5391y = "";

    /* renamed from: z */
    public PackageInfo f5392z;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x012e, code lost:
        if (r8.f5386t.mkdirs() != false) goto L_0x0130;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x013e, code lost:
        if (r8.f5384r.mkdirs() != false) goto L_0x018b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x010e  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0110  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01cb A[Catch:{ Exception -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x01d6 A[Catch:{ Exception -> 0x0056 }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x01ee A[Catch:{ Exception -> 0x0056 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34055a(android.content.Context r9) {
        /*
            r8 = this;
            monitor-enter(r8)
            com.tencent.bugly.beta.global.e r0 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x01f7 }
            android.content.Context r1 = r9.getApplicationContext()     // Catch:{ all -> 0x01f7 }
            r0.f5385s = r1     // Catch:{ all -> 0x01f7 }
            android.content.Context r0 = r8.f5385s     // Catch:{ all -> 0x01f7 }
            android.content.pm.PackageManager r0 = r0.getPackageManager()     // Catch:{ all -> 0x01f7 }
            android.content.Context r1 = r8.f5385s     // Catch:{ Exception -> 0x0056 }
            java.lang.String r1 = r1.getPackageName()     // Catch:{ Exception -> 0x0056 }
            r2 = 16384(0x4000, float:2.2959E-41)
            android.content.pm.PackageInfo r1 = r0.getPackageInfo(r1, r2)     // Catch:{ Exception -> 0x0056 }
            r8.f5392z = r1     // Catch:{ Exception -> 0x0056 }
            int r1 = r8.f5389w     // Catch:{ Exception -> 0x0056 }
            r2 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != r2) goto L_0x0029
            android.content.pm.PackageInfo r1 = r8.f5392z     // Catch:{ Exception -> 0x0056 }
            int r1 = r1.versionCode     // Catch:{ Exception -> 0x0056 }
            r8.f5389w = r1     // Catch:{ Exception -> 0x0056 }
        L_0x0029:
            java.lang.String r1 = r8.f5390x     // Catch:{ Exception -> 0x0056 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0056 }
            if (r1 == 0) goto L_0x0037
            android.content.pm.PackageInfo r1 = r8.f5392z     // Catch:{ Exception -> 0x0056 }
            java.lang.String r1 = r1.versionName     // Catch:{ Exception -> 0x0056 }
            r8.f5390x = r1     // Catch:{ Exception -> 0x0056 }
        L_0x0037:
            android.content.pm.PackageInfo r1 = r8.f5392z     // Catch:{ Exception -> 0x0056 }
            android.content.pm.ApplicationInfo r1 = r1.applicationInfo     // Catch:{ Exception -> 0x0056 }
            java.lang.CharSequence r1 = r1.loadLabel(r0)     // Catch:{ Exception -> 0x0056 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x0056 }
            r8.f5391y = r1     // Catch:{ Exception -> 0x0056 }
            java.lang.String r1 = r8.f5352P     // Catch:{ Exception -> 0x0056 }
            boolean r1 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0056 }
            if (r1 == 0) goto L_0x0060
            android.content.Context r1 = r8.f5385s     // Catch:{ Exception -> 0x0056 }
            java.lang.String r2 = "BUGLY_CHANNEL"
            java.lang.String r1 = com.tencent.bugly.beta.global.C2804a.m6262a(r1, r2)     // Catch:{ Exception -> 0x0056 }
            r8.f5352P = r1     // Catch:{ Exception -> 0x0056 }
            goto L_0x0060
        L_0x0056:
            r1 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6861b(r1)     // Catch:{ all -> 0x01f7 }
            if (r2 != 0) goto L_0x0060
            r1.printStackTrace()     // Catch:{ all -> 0x01f7 }
        L_0x0060:
            java.lang.String r1 = Context.WINDOW_SERVICE
            java.lang.Object r1 = r9.getSystemService(r1)     // Catch:{ all -> 0x01f7 }
            android.view.WindowManager r1 = (android.view.WindowManager) r1     // Catch:{ all -> 0x01f7 }
            android.util.DisplayMetrics r2 = new android.util.DisplayMetrics     // Catch:{ all -> 0x01f7 }
            r2.<init>()     // Catch:{ all -> 0x01f7 }
            r8.f5339B = r2     // Catch:{ all -> 0x01f7 }
            if (r1 == 0) goto L_0x0080
            android.view.Display r2 = r1.getDefaultDisplay()     // Catch:{ all -> 0x01f7 }
            if (r2 == 0) goto L_0x0080
            android.view.Display r1 = r1.getDefaultDisplay()     // Catch:{ all -> 0x01f7 }
            android.util.DisplayMetrics r2 = r8.f5339B     // Catch:{ all -> 0x01f7 }
            r1.getMetrics(r2)     // Catch:{ all -> 0x01f7 }
        L_0x0080:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f7 }
            r1.<init>()     // Catch:{ all -> 0x01f7 }
            android.content.Context r2 = r8.f5385s     // Catch:{ all -> 0x01f7 }
            java.lang.String r2 = r2.getPackageName()     // Catch:{ all -> 0x01f7 }
            r1.append(r2)     // Catch:{ all -> 0x01f7 }
            java.lang.String r2 = "/.beta/"
            r1.append(r2)     // Catch:{ all -> 0x01f7 }
            java.lang.String r2 = "/apk/"
            java.lang.String r3 = "/res/"
            java.io.File r4 = r8.f5378l     // Catch:{ all -> 0x01f7 }
            if (r4 == 0) goto L_0x00dd
            java.io.File r4 = r8.f5378l     // Catch:{ all -> 0x01f7 }
            boolean r4 = r4.exists()     // Catch:{ all -> 0x01f7 }
            if (r4 != 0) goto L_0x00a4
            goto L_0x00dd
        L_0x00a4:
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x01f7 }
            java.io.File r5 = r8.f5378l     // Catch:{ all -> 0x01f7 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f7 }
            r6.<init>()     // Catch:{ all -> 0x01f7 }
            java.lang.String r7 = r1.toString()     // Catch:{ all -> 0x01f7 }
            r6.append(r7)     // Catch:{ all -> 0x01f7 }
            r6.append(r2)     // Catch:{ all -> 0x01f7 }
            java.lang.String r2 = r6.toString()     // Catch:{ all -> 0x01f7 }
            r4.<init>(r5, r2)     // Catch:{ all -> 0x01f7 }
            r8.f5386t = r4     // Catch:{ all -> 0x01f7 }
            java.io.File r2 = new java.io.File     // Catch:{ all -> 0x01f7 }
            java.io.File r4 = r8.f5378l     // Catch:{ all -> 0x01f7 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f7 }
            r5.<init>()     // Catch:{ all -> 0x01f7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x01f7 }
            r5.append(r1)     // Catch:{ all -> 0x01f7 }
            r5.append(r3)     // Catch:{ all -> 0x01f7 }
            java.lang.String r1 = r5.toString()     // Catch:{ all -> 0x01f7 }
            r2.<init>(r4, r1)     // Catch:{ all -> 0x01f7 }
            r8.f5384r = r2     // Catch:{ all -> 0x01f7 }
            goto L_0x0100
        L_0x00dd:
            java.io.File r4 = new java.io.File     // Catch:{ all -> 0x01f7 }
            java.lang.String r5 = android.os.Environment.DIRECTORY_DOWNLOADS     // Catch:{ all -> 0x01f7 }
            java.io.File r5 = android.os.Environment.getExternalStoragePublicDirectory(r5)     // Catch:{ all -> 0x01f7 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x01f7 }
            r4.<init>(r5, r1)     // Catch:{ all -> 0x01f7 }
            r8.f5378l = r4     // Catch:{ all -> 0x01f7 }
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x01f7 }
            java.io.File r4 = r8.f5378l     // Catch:{ all -> 0x01f7 }
            r1.<init>(r4, r2)     // Catch:{ all -> 0x01f7 }
            r8.f5386t = r1     // Catch:{ all -> 0x01f7 }
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x01f7 }
            java.io.File r2 = r8.f5378l     // Catch:{ all -> 0x01f7 }
            r1.<init>(r2, r3)     // Catch:{ all -> 0x01f7 }
            r8.f5384r = r1     // Catch:{ all -> 0x01f7 }
        L_0x0100:
            java.lang.String r1 = "android.permission.WRITE_EXTERNAL_STORAGE"
            android.content.pm.PackageInfo r2 = r8.f5392z     // Catch:{ all -> 0x01f7 }
            java.lang.String r2 = r2.packageName     // Catch:{ all -> 0x01f7 }
            int r0 = r0.checkPermission(r1, r2)     // Catch:{ all -> 0x01f7 }
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0110
            r0 = 1
            goto L_0x0111
        L_0x0110:
            r0 = 0
        L_0x0111:
            r3 = 2
            if (r0 == 0) goto L_0x0140
            java.lang.String r0 = "mounted"
            java.lang.String r4 = android.os.Environment.getExternalStorageState()     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.equals(r4)     // Catch:{ Exception -> 0x0181 }
            if (r0 == 0) goto L_0x0140
            java.io.File r0 = r8.f5386t     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.exists()     // Catch:{ Exception -> 0x0181 }
            if (r0 != 0) goto L_0x0130
            java.io.File r0 = r8.f5386t     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.mkdirs()     // Catch:{ Exception -> 0x0181 }
            if (r0 == 0) goto L_0x0140
        L_0x0130:
            java.io.File r0 = r8.f5384r     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.exists()     // Catch:{ Exception -> 0x0181 }
            if (r0 != 0) goto L_0x018b
            java.io.File r0 = r8.f5384r     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.mkdirs()     // Catch:{ Exception -> 0x0181 }
            if (r0 != 0) goto L_0x018b
        L_0x0140:
            android.content.Context r0 = r8.f5385s     // Catch:{ Exception -> 0x0181 }
            java.lang.String r4 = "apk"
            java.io.File r0 = r0.getExternalFilesDir(r4)     // Catch:{ Exception -> 0x0181 }
            r8.f5386t = r0     // Catch:{ Exception -> 0x0181 }
            android.content.Context r0 = r8.f5385s     // Catch:{ Exception -> 0x0181 }
            java.lang.String r4 = "res"
            java.io.File r0 = r0.getExternalFilesDir(r4)     // Catch:{ Exception -> 0x0181 }
            r8.f5384r = r0     // Catch:{ Exception -> 0x0181 }
            java.io.File r0 = r8.f5386t     // Catch:{ Exception -> 0x0181 }
            if (r0 == 0) goto L_0x016c
            java.io.File r0 = r8.f5386t     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.exists()     // Catch:{ Exception -> 0x0181 }
            if (r0 == 0) goto L_0x016c
            java.io.File r0 = r8.f5384r     // Catch:{ Exception -> 0x0181 }
            if (r0 == 0) goto L_0x016c
            java.io.File r0 = r8.f5384r     // Catch:{ Exception -> 0x0181 }
            boolean r0 = r0.exists()     // Catch:{ Exception -> 0x0181 }
            if (r0 != 0) goto L_0x018b
        L_0x016c:
            android.content.Context r0 = r8.f5385s     // Catch:{ Exception -> 0x0181 }
            java.lang.String r4 = "apk"
            java.io.File r0 = r0.getDir(r4, r3)     // Catch:{ Exception -> 0x0181 }
            r8.f5386t = r0     // Catch:{ Exception -> 0x0181 }
            android.content.Context r0 = r8.f5385s     // Catch:{ Exception -> 0x0181 }
            java.lang.String r4 = "res"
            java.io.File r0 = r0.getDir(r4, r2)     // Catch:{ Exception -> 0x0181 }
            r8.f5384r = r0     // Catch:{ Exception -> 0x0181 }
            goto L_0x018b
        L_0x0181:
            r0 = move-exception
            boolean r4 = com.tencent.bugly.proguard.C2903an.m6861b(r0)     // Catch:{ all -> 0x01f7 }
            if (r4 != 0) goto L_0x018b
            r0.printStackTrace()     // Catch:{ all -> 0x01f7 }
        L_0x018b:
            java.lang.String r0 = "apkSaveDir: %s, resSaveDir: %s"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x01f7 }
            java.io.File r4 = r8.f5386t     // Catch:{ all -> 0x01f7 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ all -> 0x01f7 }
            r3[r2] = r4     // Catch:{ all -> 0x01f7 }
            java.io.File r4 = r8.f5384r     // Catch:{ all -> 0x01f7 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ all -> 0x01f7 }
            r3[r1] = r4     // Catch:{ all -> 0x01f7 }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r3)     // Catch:{ all -> 0x01f7 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f7 }
            r0.<init>()     // Catch:{ all -> 0x01f7 }
            java.lang.String r9 = r9.getPackageName()     // Catch:{ all -> 0x01f7 }
            r0.append(r9)     // Catch:{ all -> 0x01f7 }
            java.lang.String r9 = ".BETA_VALUES"
            r0.append(r9)     // Catch:{ all -> 0x01f7 }
            java.lang.String r9 = r0.toString()     // Catch:{ all -> 0x01f7 }
            android.content.Context r0 = r8.f5385s     // Catch:{ all -> 0x01f7 }
            android.content.SharedPreferences r9 = com.tencent.bugly.proguard.C2908aq.m6892a(r9, r0)     // Catch:{ all -> 0x01f7 }
            r8.f5338A = r9     // Catch:{ all -> 0x01f7 }
            java.lang.String r9 = "isFirstRun"
            boolean r9 = com.tencent.bugly.beta.global.C2804a.m6273b(r9, r1)     // Catch:{ all -> 0x01f7 }
            r8.f5340C = r9     // Catch:{ all -> 0x01f7 }
            boolean r9 = r8.f5340C     // Catch:{ all -> 0x01f7 }
            if (r9 == 0) goto L_0x01d0
            java.lang.String r9 = "isFirstRun"
            com.tencent.bugly.beta.global.C2804a.m6265a(r9, r2)     // Catch:{ all -> 0x01f7 }
        L_0x01d0:
            com.tencent.bugly.crashreport.common.info.a r9 = com.tencent.bugly.crashreport.common.info.C2851a.m6471b()     // Catch:{ all -> 0x01f7 }
            if (r9 == 0) goto L_0x01de
            com.tencent.bugly.crashreport.common.info.a r9 = com.tencent.bugly.crashreport.common.info.C2851a.m6471b()     // Catch:{ all -> 0x01f7 }
            boolean r9 = r9.f5610L     // Catch:{ all -> 0x01f7 }
            r8.f5353Q = r9     // Catch:{ all -> 0x01f7 }
        L_0x01de:
            java.lang.String r9 = "us.bch"
            android.os.Parcelable$Creator<com.tencent.bugly.beta.upgrade.BetaUploadStrategy> r0 = com.tencent.bugly.beta.upgrade.BetaUploadStrategy.CREATOR     // Catch:{ all -> 0x01f7 }
            android.os.Parcelable r9 = com.tencent.bugly.beta.global.C2804a.m6261a(r9, r0)     // Catch:{ all -> 0x01f7 }
            com.tencent.bugly.beta.upgrade.BetaUploadStrategy r9 = (com.tencent.bugly.beta.upgrade.BetaUploadStrategy) r9     // Catch:{ all -> 0x01f7 }
            r8.f5342F = r9     // Catch:{ all -> 0x01f7 }
            com.tencent.bugly.beta.upgrade.BetaUploadStrategy r9 = r8.f5342F     // Catch:{ all -> 0x01f7 }
            if (r9 != 0) goto L_0x01f5
            com.tencent.bugly.beta.upgrade.BetaUploadStrategy r9 = new com.tencent.bugly.beta.upgrade.BetaUploadStrategy     // Catch:{ all -> 0x01f7 }
            r9.<init>()     // Catch:{ all -> 0x01f7 }
            r8.f5342F = r9     // Catch:{ all -> 0x01f7 }
        L_0x01f5:
            monitor-exit(r8)
            return
        L_0x01f7:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.global.C2808e.mo34055a(android.content.Context):void");
    }
}
