package com.tencent.bugly.crashreport.crash;

/* renamed from: com.tencent.bugly.crashreport.crash.f */
/* compiled from: BUGLY */
public interface C2876f {
    /* renamed from: a */
    void mo34431a(boolean z);

    /* renamed from: a */
    boolean mo34432a(boolean z, String str, String str2, String str3, String str4, int i, long j, String str5, String str6, String str7, String str8, String str9);

    /* renamed from: a */
    byte[] mo34433a(boolean z, String str, String str2, String str3, int i, long j);

    /* renamed from: b */
    String mo34434b(boolean z, String str, String str2, String str3, int i, long j);

    /* renamed from: b */
    boolean mo34435b(boolean z);
}
