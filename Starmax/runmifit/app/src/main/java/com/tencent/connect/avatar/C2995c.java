package com.tencent.connect.avatar;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.widget.ImageView;

/* renamed from: com.tencent.connect.avatar.c */
/* compiled from: ProGuard */
public class C2995c extends ImageView {

    /* renamed from: a */
    final String f6433a = "TouchView";

    /* renamed from: b */
    public boolean f6434b = false;

    /* renamed from: c */
    private Matrix f6435c = new Matrix();

    /* renamed from: d */
    private Matrix f6436d = new Matrix();

    /* renamed from: e */
    private int f6437e = 0;

    /* renamed from: f */
    private float f6438f = 1.0f;

    /* renamed from: g */
    private float f6439g = 1.0f;

    /* renamed from: h */
    private Bitmap f6440h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public boolean f6441i = false;

    /* renamed from: j */
    private float f6442j;

    /* renamed from: k */
    private float f6443k;

    /* renamed from: l */
    private PointF f6444l = new PointF();

    /* renamed from: m */
    private PointF f6445m = new PointF();

    /* renamed from: n */
    private float f6446n = 1.0f;

    /* renamed from: o */
    private float f6447o = 0.0f;

    /* renamed from: p */
    private Rect f6448p = new Rect();

    /* renamed from: a */
    private void m7295a() {
    }

    public C2995c(Context context) {
        super(context);
        getDrawingRect(this.f6448p);
        m7295a();
    }

    public void setImageBitmap(Bitmap bitmap) {
        super.setImageBitmap(bitmap);
        this.f6440h = bitmap;
        if (bitmap != null) {
            this.f6440h = bitmap;
        }
    }

    /* renamed from: a */
    private float m7294a(MotionEvent motionEvent) {
        if (motionEvent.getPointerCount() < 2) {
            return 0.0f;
        }
        float x = motionEvent.getX(0) - motionEvent.getX(1);
        float y = motionEvent.getY(0) - motionEvent.getY(1);
        return (float) Math.sqrt((double) ((x * x) + (y * y)));
    }

    /* renamed from: a */
    public void mo34782a(Rect rect) {
        this.f6448p = rect;
        if (this.f6440h != null) {
            m7300c();
        }
    }

    /* renamed from: a */
    private void m7296a(PointF pointF) {
        if (this.f6440h != null) {
            float[] fArr = new float[9];
            this.f6435c.getValues(fArr);
            float f = fArr[2];
            float f2 = fArr[5];
            float f3 = fArr[0];
            float width = ((float) this.f6440h.getWidth()) * f3;
            float height = ((float) this.f6440h.getHeight()) * f3;
            float f4 = ((float) this.f6448p.left) - f;
            if (f4 <= 1.0f) {
                f4 = 1.0f;
            }
            float f5 = (f + width) - ((float) this.f6448p.right);
            if (f5 <= 1.0f) {
                f5 = 1.0f;
            }
            float width2 = ((((float) this.f6448p.width()) * f4) / (f5 + f4)) + ((float) this.f6448p.left);
            float f6 = ((float) this.f6448p.top) - f2;
            float f7 = (f2 + height) - ((float) this.f6448p.bottom);
            if (f6 <= 1.0f) {
                f6 = 1.0f;
            }
            if (f7 <= 1.0f) {
                f7 = 1.0f;
            }
            pointF.set(width2, ((((float) this.f6448p.height()) * f6) / (f7 + f6)) + ((float) this.f6448p.top));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0019, code lost:
        if (r0 != 6) goto L_0x00b3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            boolean r0 = r5.f6441i
            r1 = 1
            if (r0 == 0) goto L_0x0006
            return r1
        L_0x0006:
            int r0 = r6.getAction()
            r0 = r0 & 255(0xff, float:3.57E-43)
            if (r0 == 0) goto L_0x0094
            if (r0 == r1) goto L_0x008d
            r2 = 1092616192(0x41200000, float:10.0)
            r3 = 2
            if (r0 == r3) goto L_0x0039
            r4 = 5
            if (r0 == r4) goto L_0x001d
            r6 = 6
            if (r0 == r6) goto L_0x008d
            goto L_0x00b3
        L_0x001d:
            float r6 = r5.m7294a(r6)
            r5.f6446n = r6
            float r6 = r5.f6446n
            int r6 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r6 <= 0) goto L_0x00b3
            android.graphics.Matrix r6 = r5.f6436d
            android.graphics.Matrix r0 = r5.f6435c
            r6.set(r0)
            android.graphics.PointF r6 = r5.f6445m
            r5.m7296a(r6)
            r5.f6437e = r3
            goto L_0x00b3
        L_0x0039:
            int r0 = r5.f6437e
            if (r0 != r1) goto L_0x0061
            android.graphics.Matrix r0 = r5.f6435c
            android.graphics.Matrix r2 = r5.f6436d
            r0.set(r2)
            float r0 = r6.getX()
            android.graphics.PointF r2 = r5.f6444l
            float r2 = r2.x
            float r0 = r0 - r2
            float r6 = r6.getY()
            android.graphics.PointF r2 = r5.f6444l
            float r2 = r2.y
            float r6 = r6 - r2
            android.graphics.Matrix r2 = r5.f6435c
            r2.postTranslate(r0, r6)
            android.graphics.Matrix r6 = r5.f6435c
            r5.setImageMatrix(r6)
            goto L_0x00b3
        L_0x0061:
            if (r0 != r3) goto L_0x00b3
            android.graphics.Matrix r0 = r5.f6435c
            r0.set(r0)
            float r6 = r5.m7294a(r6)
            int r0 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0087
            android.graphics.Matrix r0 = r5.f6435c
            android.graphics.Matrix r2 = r5.f6436d
            r0.set(r2)
            float r0 = r5.f6446n
            float r6 = r6 / r0
            android.graphics.Matrix r0 = r5.f6435c
            android.graphics.PointF r2 = r5.f6445m
            float r2 = r2.x
            android.graphics.PointF r3 = r5.f6445m
            float r3 = r3.y
            r0.postScale(r6, r6, r2, r3)
        L_0x0087:
            android.graphics.Matrix r6 = r5.f6435c
            r5.setImageMatrix(r6)
            goto L_0x00b3
        L_0x008d:
            r5.m7299b()
            r6 = 0
            r5.f6437e = r6
            goto L_0x00b3
        L_0x0094:
            android.graphics.Matrix r0 = r5.f6435c
            android.graphics.Matrix r2 = r5.getImageMatrix()
            r0.set(r2)
            android.graphics.Matrix r0 = r5.f6436d
            android.graphics.Matrix r2 = r5.f6435c
            r0.set(r2)
            android.graphics.PointF r0 = r5.f6444l
            float r2 = r6.getX()
            float r6 = r6.getY()
            r0.set(r2, r6)
            r5.f6437e = r1
        L_0x00b3:
            r5.f6434b = r1
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connect.avatar.C2995c.onTouchEvent(android.view.MotionEvent):boolean");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v0, resolved type: android.view.animation.TranslateAnimation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v4, resolved type: android.view.animation.ScaleAnimation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v1, resolved type: android.view.animation.TranslateAnimation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v7, resolved type: android.view.animation.ScaleAnimation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v2, resolved type: android.view.animation.TranslateAnimation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v10, resolved type: android.view.animation.ScaleAnimation} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v15, resolved type: android.view.animation.ScaleAnimation} */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m7299b() {
        /*
            r17 = this;
            r0 = r17
            android.graphics.Bitmap r1 = r0.f6440h
            if (r1 != 0) goto L_0x0007
            return
        L_0x0007:
            android.graphics.Rect r1 = r0.f6448p
            int r1 = r1.width()
            float r1 = (float) r1
            android.graphics.Rect r2 = r0.f6448p
            int r2 = r2.height()
            float r2 = (float) r2
            r3 = 9
            float[] r3 = new float[r3]
            android.graphics.Matrix r4 = r0.f6435c
            r4.getValues(r3)
            r4 = 2
            r5 = r3[r4]
            r6 = 5
            r7 = r3[r6]
            r8 = 0
            r9 = r3[r8]
            r10 = 0
            float r11 = r0.f6438f
            r12 = 1
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 <= 0) goto L_0x0068
            float r11 = r11 / r9
            r0.f6447o = r11
            android.graphics.Matrix r1 = r0.f6435c
            float r2 = r0.f6447o
            android.graphics.PointF r3 = r0.f6445m
            float r3 = r3.x
            android.graphics.PointF r4 = r0.f6445m
            float r4 = r4.y
            r1.postScale(r2, r2, r3, r4)
            android.graphics.Matrix r1 = r0.f6435c
            r0.setImageMatrix(r1)
            android.view.animation.ScaleAnimation r1 = new android.view.animation.ScaleAnimation
            float r2 = r0.f6447o
            r3 = 1065353216(0x3f800000, float:1.0)
            float r4 = r3 / r2
            r5 = 1065353216(0x3f800000, float:1.0)
            float r6 = r3 / r2
            r7 = 1065353216(0x3f800000, float:1.0)
            android.graphics.PointF r2 = r0.f6445m
            float r8 = r2.x
            android.graphics.PointF r2 = r0.f6445m
            float r9 = r2.y
            r2 = r1
            r3 = r4
            r4 = r5
            r5 = r6
            r6 = r7
            r7 = r8
            r8 = r9
            r2.<init>(r3, r4, r5, r6, r7, r8)
            goto L_0x0109
        L_0x0068:
            float r11 = r0.f6439g
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 >= 0) goto L_0x0096
            float r11 = r11 / r9
            r0.f6447o = r11
            android.graphics.Matrix r1 = r0.f6435c
            float r2 = r0.f6447o
            android.graphics.PointF r3 = r0.f6445m
            float r3 = r3.x
            android.graphics.PointF r4 = r0.f6445m
            float r4 = r4.y
            r1.postScale(r2, r2, r3, r4)
            android.view.animation.ScaleAnimation r1 = new android.view.animation.ScaleAnimation
            r6 = 1065353216(0x3f800000, float:1.0)
            float r9 = r0.f6447o
            r8 = 1065353216(0x3f800000, float:1.0)
            android.graphics.PointF r2 = r0.f6445m
            float r10 = r2.x
            android.graphics.PointF r2 = r0.f6445m
            float r11 = r2.y
            r5 = r1
            r7 = r9
            r5.<init>(r6, r7, r8, r9, r10, r11)
            goto L_0x0109
        L_0x0096:
            android.graphics.Bitmap r11 = r0.f6440h
            int r11 = r11.getWidth()
            float r11 = (float) r11
            float r11 = r11 * r9
            android.graphics.Bitmap r13 = r0.f6440h
            int r13 = r13.getHeight()
            float r13 = (float) r13
            float r13 = r13 * r9
            android.graphics.Rect r9 = r0.f6448p
            int r9 = r9.left
            float r9 = (float) r9
            float r9 = r9 - r5
            android.graphics.Rect r14 = r0.f6448p
            int r14 = r14.top
            float r14 = (float) r14
            float r14 = r14 - r7
            r15 = 0
            int r16 = (r9 > r15 ? 1 : (r9 == r15 ? 0 : -1))
            if (r16 >= 0) goto L_0x00bf
            android.graphics.Rect r5 = r0.f6448p
            int r5 = r5.left
            float r5 = (float) r5
            r8 = 1
        L_0x00bf:
            int r16 = (r14 > r15 ? 1 : (r14 == r15 ? 0 : -1))
            if (r16 >= 0) goto L_0x00c9
            android.graphics.Rect r7 = r0.f6448p
            int r7 = r7.top
            float r7 = (float) r7
            r8 = 1
        L_0x00c9:
            float r9 = r11 - r9
            float r14 = r13 - r14
            int r9 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r9 >= 0) goto L_0x00da
            float r11 = r11 - r1
            android.graphics.Rect r1 = r0.f6448p
            int r1 = r1.left
            float r1 = (float) r1
            float r5 = r1 - r11
            r8 = 1
        L_0x00da:
            int r1 = (r14 > r2 ? 1 : (r14 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x00e7
            float r13 = r13 - r2
            android.graphics.Rect r1 = r0.f6448p
            int r1 = r1.top
            float r1 = (float) r1
            float r7 = r1 - r13
            r8 = 1
        L_0x00e7:
            if (r8 == 0) goto L_0x0103
            r1 = r3[r4]
            float r1 = r1 - r5
            r2 = r3[r6]
            float r2 = r2 - r7
            r3[r4] = r5
            r3[r6] = r7
            android.graphics.Matrix r4 = r0.f6435c
            r4.setValues(r3)
            android.graphics.Matrix r3 = r0.f6435c
            r0.setImageMatrix(r3)
            android.view.animation.TranslateAnimation r10 = new android.view.animation.TranslateAnimation
            r10.<init>(r1, r15, r2, r15)
            goto L_0x0108
        L_0x0103:
            android.graphics.Matrix r1 = r0.f6435c
            r0.setImageMatrix(r1)
        L_0x0108:
            r1 = r10
        L_0x0109:
            if (r1 == 0) goto L_0x0122
            r0.f6441i = r12
            r2 = 300(0x12c, double:1.48E-321)
            r1.setDuration(r2)
            r0.startAnimation(r1)
            java.lang.Thread r1 = new java.lang.Thread
            com.tencent.connect.avatar.c$1 r2 = new com.tencent.connect.avatar.c$1
            r2.<init>()
            r1.<init>(r2)
            r1.start()
        L_0x0122:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connect.avatar.C2995c.m7299b():void");
    }

    /* renamed from: c */
    private void m7300c() {
        if (this.f6440h != null) {
            float[] fArr = new float[9];
            this.f6435c.getValues(fArr);
            float max = Math.max(((float) this.f6448p.width()) / ((float) this.f6440h.getWidth()), ((float) this.f6448p.height()) / ((float) this.f6440h.getHeight()));
            this.f6442j = ((float) this.f6448p.left) - (((((float) this.f6440h.getWidth()) * max) - ((float) this.f6448p.width())) / 2.0f);
            this.f6443k = ((float) this.f6448p.top) - (((((float) this.f6440h.getHeight()) * max) - ((float) this.f6448p.height())) / 2.0f);
            fArr[2] = this.f6442j;
            fArr[5] = this.f6443k;
            fArr[4] = max;
            fArr[0] = max;
            this.f6435c.setValues(fArr);
            this.f6438f = Math.min(2048.0f / ((float) this.f6440h.getWidth()), 2048.0f / ((float) this.f6440h.getHeight()));
            this.f6439g = max;
            float f = this.f6438f;
            float f2 = this.f6439g;
            if (f < f2) {
                this.f6438f = f2;
            }
            setImageMatrix(this.f6435c);
        }
    }
}
