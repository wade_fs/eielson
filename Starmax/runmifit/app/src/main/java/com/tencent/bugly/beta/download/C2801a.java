package com.tencent.bugly.beta.download;

import android.util.Log;
import com.tencent.bugly.beta.global.C2809f;
import com.tencent.bugly.beta.p050ui.C2826h;
import com.tencent.bugly.beta.tinker.TinkerManager;
import com.tencent.bugly.beta.upgrade.C2829c;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2947p;
import com.tencent.bugly.proguard.C2948q;
import com.tencent.bugly.proguard.C2956v;
import com.tencent.bugly.proguard.C2957w;

/* renamed from: com.tencent.bugly.beta.download.a */
/* compiled from: BUGLY */
public class C2801a implements DownloadListener {

    /* renamed from: a */
    final int f5321a;

    /* renamed from: b */
    final Object[] f5322b;

    public C2801a(int i, Object... objArr) {
        this.f5321a = i;
        this.f5322b = objArr;
    }

    public void onReceive(DownloadTask downloadTask) {
        if (this.f5321a == 2) {
            ((C2826h) this.f5322b[0]).mo34163a(downloadTask);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.global.ResBean]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCompleted(com.tencent.bugly.beta.download.DownloadTask r22) {
        /*
            r21 = this;
            r1 = r21
            r0 = r22
            int r2 = r1.f5321a     // Catch:{ Exception -> 0x0319 }
            r3 = 0
            r4 = 1
            if (r2 == r4) goto L_0x026e
            r5 = 2
            if (r2 == r5) goto L_0x0263
            r6 = 0
            r7 = 2080(0x820, float:2.915E-42)
            r8 = 3
            if (r2 == r8) goto L_0x012a
            r8 = 4
            if (r2 == r8) goto L_0x0018
            goto L_0x0323
        L_0x0018:
            java.lang.Object[] r2 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            r2 = r2[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.q r2 = (com.tencent.bugly.proguard.C2948q) r2     // Catch:{ Exception -> 0x0319 }
            java.lang.Object[] r8 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            r8 = r8[r4]     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r8 = (java.lang.Integer) r8     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r9 = r2.f6231b     // Catch:{ Exception -> 0x0319 }
            if (r9 == 0) goto L_0x0129
            com.tencent.bugly.proguard.y r10 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            if (r10 != 0) goto L_0x002e
            goto L_0x0129
        L_0x002e:
            java.lang.String r10 = "patch download success !!!"
            java.lang.Object[] r11 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.C2903an.m6857a(r10, r11)     // Catch:{ Exception -> 0x0319 }
            java.io.File r10 = r22.getSaveFile()     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r11 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.u r11 = r11.f6287f     // Catch:{ Exception -> 0x0319 }
            java.lang.String r11 = r11.f6246a     // Catch:{ Exception -> 0x0319 }
            java.lang.String r12 = "SHA"
            boolean r11 = com.tencent.bugly.beta.global.C2804a.m6268a(r10, r11, r12)     // Catch:{ Exception -> 0x0319 }
            if (r11 == 0) goto L_0x00c4
            java.lang.Object[] r0 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0319 }
            r0[r4] = r5     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r0 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            java.io.File r0 = r0.f5344H     // Catch:{ Exception -> 0x0319 }
            boolean r0 = com.tencent.bugly.beta.global.C2804a.m6267a(r10, r0)     // Catch:{ Exception -> 0x0319 }
            if (r0 == 0) goto L_0x00b2
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0319 }
            r0.<init>()     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = "copy "
            r0.append(r5)     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = r10.getAbsolutePath()     // Catch:{ Exception -> 0x0319 }
            r0.append(r5)     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = " to "
            r0.append(r5)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r5 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            java.io.File r5 = r5.f5344H     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0319 }
            r0.append(r5)     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = " success!"
            r0.append(r5)     // Catch:{ Exception -> 0x0319 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0319 }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r5)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f6232c     // Catch:{ Exception -> 0x0319 }
            if (r0 == 0) goto L_0x0098
            java.lang.String r0 = "delete temp file"
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r5)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f6232c     // Catch:{ Exception -> 0x0319 }
            r0.delete(r4)     // Catch:{ Exception -> 0x0319 }
        L_0x0098:
            java.lang.String r0 = "UPLOAD_PATCH_RESULT"
            com.tencent.bugly.beta.global.C2804a.m6265a(r0, r3)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.tinker.TinkerManager r0 = com.tencent.bugly.beta.tinker.TinkerManager.getInstance()     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r2 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            java.io.File r2 = r2.f5344H     // Catch:{ Exception -> 0x0319 }
            java.lang.String r2 = r2.getAbsolutePath()     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r3 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            boolean r3 = r3.f5358V     // Catch:{ Exception -> 0x0319 }
            r0.onDownloadSuccess(r2, r3)     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x00b2:
            java.lang.String r0 = "copy file failed"
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r2)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.tinker.TinkerManager r0 = com.tencent.bugly.beta.tinker.TinkerManager.getInstance()     // Catch:{ Exception -> 0x0319 }
            java.lang.String r2 = "copy file failure."
            r0.onDownloadFailure(r2)     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x00c4:
            int r10 = r8.intValue()     // Catch:{ Exception -> 0x0319 }
            if (r10 >= r5) goto L_0x010e
            java.lang.Object[] r5 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            int r7 = r8.intValue()     // Catch:{ Exception -> 0x0319 }
            int r7 = r7 + r4
            java.lang.Integer r7 = java.lang.Integer.valueOf(r7)     // Catch:{ Exception -> 0x0319 }
            r5[r4] = r7     // Catch:{ Exception -> 0x0319 }
            r0.delete(r4)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r0 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.b r0 = r0.f5382p     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r4 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.u r4 = r4.f6287f     // Catch:{ Exception -> 0x0319 }
            java.lang.String r4 = r4.f6247b     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r5 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            java.io.File r5 = r5.f5386t     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = r5.getAbsolutePath()     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r7 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.u r7 = r7.f6287f     // Catch:{ Exception -> 0x0319 }
            java.lang.String r7 = r7.f6247b     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r0.mo34042a(r4, r5, r6, r7)     // Catch:{ Exception -> 0x0319 }
            r2.f6232c = r0     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f6232c     // Catch:{ Exception -> 0x0319 }
            r0.setNeededNotify(r3)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f6232c     // Catch:{ Exception -> 0x0319 }
            r0.addListener(r1)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f6232c     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.BetaReceiver.addTask(r0)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f6232c     // Catch:{ Exception -> 0x0319 }
            r0.download()     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x010e:
            java.lang.Object[] r2 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0319 }
            r2[r4] = r3     // Catch:{ Exception -> 0x0319 }
            java.lang.String r2 = "file sha1 verify fail"
            r1.onFailed(r0, r7, r2)     // Catch:{ Exception -> 0x0319 }
            r0.delete(r4)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.tinker.TinkerManager r0 = com.tencent.bugly.beta.tinker.TinkerManager.getInstance()     // Catch:{ Exception -> 0x0319 }
            java.lang.String r2 = "retry download patch too many times."
            r0.onDownloadFailure(r2)     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x0129:
            return
        L_0x012a:
            java.lang.Object[] r2 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            r2 = r2[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.upgrade.c r2 = (com.tencent.bugly.beta.upgrade.C2829c) r2     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r9 = r2.f5468b     // Catch:{ Exception -> 0x0319 }
            if (r9 == 0) goto L_0x0262
            com.tencent.bugly.proguard.y r10 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            if (r10 != 0) goto L_0x013a
            goto L_0x0262
        L_0x013a:
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r10 = r2.f5472f     // Catch:{ Exception -> 0x0319 }
            if (r10 == 0) goto L_0x015c
            com.tencent.bugly.beta.global.d r10 = new com.tencent.bugly.beta.global.d     // Catch:{ Exception -> 0x0319 }
            r11 = 18
            java.lang.Object[] r12 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.upgrade.UpgradeStateListener r13 = r2.f5472f     // Catch:{ Exception -> 0x0319 }
            r12[r3] = r13     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x0319 }
            r12[r4] = r8     // Catch:{ Exception -> 0x0319 }
            boolean r8 = r2.f5473g     // Catch:{ Exception -> 0x0319 }
            java.lang.Boolean r8 = java.lang.Boolean.valueOf(r8)     // Catch:{ Exception -> 0x0319 }
            r12[r5] = r8     // Catch:{ Exception -> 0x0319 }
            r10.<init>(r11, r12)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.utils.C2836e.m6398a(r10)     // Catch:{ Exception -> 0x0319 }
        L_0x015c:
            java.lang.String r8 = "apk download completed"
            java.lang.Object[] r10 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.C2903an.m6857a(r8, r10)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.p r8 = com.tencent.bugly.proguard.C2947p.f6229a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.w r15 = new com.tencent.bugly.proguard.w     // Catch:{ Exception -> 0x0319 }
            java.lang.String r11 = "download"
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0319 }
            r14 = 0
            long r16 = r22.getCostTime()     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r10 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.v r10 = r10.f6286e     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r6 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            java.lang.String r6 = r6.f6294m     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r7 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            int r7 = r7.f6297p     // Catch:{ Exception -> 0x0319 }
            r20 = 0
            r18 = r10
            r10 = r15
            r5 = r15
            r15 = r16
            r17 = r18
            r18 = r6
            r19 = r7
            r10.<init>(r11, r12, r14, r15, r17, r18, r19, r20)     // Catch:{ Exception -> 0x0319 }
            r8.mo34667a(r5)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r5 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            boolean r5 = r5.f5366ad     // Catch:{ Exception -> 0x0319 }
            if (r5 != 0) goto L_0x0199
            return
        L_0x0199:
            java.io.File r5 = r22.getSaveFile()     // Catch:{ Exception -> 0x0319 }
            java.lang.Object[] r6 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            r6 = r6[r4]     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r6 = (java.lang.Integer) r6     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r7 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            android.content.Context r7 = r7.f5385s     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r8 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.u r8 = r8.f6287f     // Catch:{ Exception -> 0x0319 }
            java.lang.String r8 = r8.f6246a     // Catch:{ Exception -> 0x0319 }
            boolean r5 = com.tencent.bugly.beta.global.C2804a.m6266a(r7, r5, r8)     // Catch:{ Exception -> 0x0319 }
            if (r5 == 0) goto L_0x01e5
            java.lang.Object[] r0 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0319 }
            r0[r4] = r2     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.p r0 = com.tencent.bugly.proguard.C2947p.f6229a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.w r2 = new com.tencent.bugly.proguard.w     // Catch:{ Exception -> 0x0319 }
            java.lang.String r11 = "install"
            long r12 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0319 }
            r14 = 0
            r15 = 0
            com.tencent.bugly.proguard.y r3 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.v r3 = r3.f6286e     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r4 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            java.lang.String r4 = r4.f6294m     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r5 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            int r5 = r5.f6297p     // Catch:{ Exception -> 0x0319 }
            r20 = 0
            r10 = r2
            r17 = r3
            r18 = r4
            r19 = r5
            r10.<init>(r11, r12, r14, r15, r17, r18, r19, r20)     // Catch:{ Exception -> 0x0319 }
            r0.mo34667a(r2)     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x01e5:
            int r5 = r6.intValue()     // Catch:{ Exception -> 0x0319 }
            r7 = 2
            if (r5 >= r7) goto L_0x024e
            int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0319 }
            r7 = 24
            if (r5 < r7) goto L_0x0205
            java.lang.Object[] r2 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0319 }
            r2[r4] = r3     // Catch:{ Exception -> 0x0319 }
            java.lang.String r2 = "安装失败，请检查您的App是否兼容7.0设备"
            r3 = 2080(0x820, float:2.915E-42)
            r1.onFailed(r0, r3, r2)     // Catch:{ Exception -> 0x0319 }
            r0.delete(r4)     // Catch:{ Exception -> 0x0319 }
            return
        L_0x0205:
            java.lang.Object[] r3 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            int r5 = r6.intValue()     // Catch:{ Exception -> 0x0319 }
            int r5 = r5 + r4
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x0319 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0319 }
            r0.delete(r4)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r0 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.b r0 = r0.f5382p     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r3 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.u r3 = r3.f6287f     // Catch:{ Exception -> 0x0319 }
            java.lang.String r3 = r3.f6247b     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x0319 }
            java.io.File r4 = r4.f5386t     // Catch:{ Exception -> 0x0319 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r5 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.u r5 = r5.f6287f     // Catch:{ Exception -> 0x0319 }
            java.lang.String r5 = r5.f6247b     // Catch:{ Exception -> 0x0319 }
            r6 = 0
            com.tencent.bugly.beta.download.DownloadTask r0 = r0.mo34042a(r3, r4, r6, r5)     // Catch:{ Exception -> 0x0319 }
            r2.f5469c = r0     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.ui.h r0 = com.tencent.bugly.beta.p050ui.C2826h.f5440v     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.proguard.y r3 = r9.f5455a     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r4 = r2.f5469c     // Catch:{ Exception -> 0x0319 }
            r0.mo34164a(r3, r4)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f5469c     // Catch:{ Exception -> 0x0319 }
            r0.addListener(r1)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f5469c     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.BetaReceiver.addTask(r0)     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.download.DownloadTask r0 = r2.f5469c     // Catch:{ Exception -> 0x0319 }
            r0.download()     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x024e:
            java.lang.Object[] r2 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0319 }
            r2[r4] = r3     // Catch:{ Exception -> 0x0319 }
            java.lang.String r2 = "file md5 verify fail"
            r3 = 2080(0x820, float:2.915E-42)
            r1.onFailed(r0, r3, r2)     // Catch:{ Exception -> 0x0319 }
            r0.delete(r4)     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x0262:
            return
        L_0x0263:
            java.lang.Object[] r2 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            r2 = r2[r3]     // Catch:{ Exception -> 0x0319 }
            com.tencent.bugly.beta.ui.h r2 = (com.tencent.bugly.beta.p050ui.C2826h) r2     // Catch:{ Exception -> 0x0319 }
            r2.mo34163a(r0)     // Catch:{ Exception -> 0x0319 }
            goto L_0x0323
        L_0x026e:
            java.lang.Object[] r0 = r1.f5322b     // Catch:{ Exception -> 0x0319 }
            r2 = r0[r3]     // Catch:{ Exception -> 0x0319 }
            monitor-enter(r2)     // Catch:{ Exception -> 0x0319 }
            java.lang.Object[] r0 = r1.f5322b     // Catch:{ all -> 0x0316 }
            r0 = r0[r4]     // Catch:{ all -> 0x0316 }
            java.util.Map r0 = (java.util.Map) r0     // Catch:{ all -> 0x0316 }
            int r5 = r0.size()     // Catch:{ all -> 0x0316 }
            if (r5 != 0) goto L_0x0281
            monitor-exit(r2)     // Catch:{ all -> 0x0316 }
            return
        L_0x0281:
            java.util.Collection r5 = r0.values()     // Catch:{ all -> 0x0316 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x0316 }
            r6 = 0
        L_0x028a:
            boolean r7 = r5.hasNext()     // Catch:{ all -> 0x0316 }
            if (r7 == 0) goto L_0x029f
            java.lang.Object r7 = r5.next()     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.beta.download.DownloadTask r7 = (com.tencent.bugly.beta.download.DownloadTask) r7     // Catch:{ all -> 0x0316 }
            int r7 = r7.getStatus()     // Catch:{ all -> 0x0316 }
            if (r7 != r4) goto L_0x028a
            int r6 = r6 + 1
            goto L_0x028a
        L_0x029f:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0316 }
            r4.<init>()     // Catch:{ all -> 0x0316 }
            r4.append(r6)     // Catch:{ all -> 0x0316 }
            java.lang.String r5 = " has completed"
            r4.append(r5)     // Catch:{ all -> 0x0316 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0316 }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.proguard.C2903an.m6863c(r4, r5)     // Catch:{ all -> 0x0316 }
            int r4 = r0.size()     // Catch:{ all -> 0x0316 }
            if (r6 >= r4) goto L_0x02bd
            monitor-exit(r2)     // Catch:{ all -> 0x0316 }
            return
        L_0x02bd:
            java.util.Set r4 = r0.keySet()     // Catch:{ all -> 0x0316 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x0316 }
        L_0x02c5:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x0316 }
            if (r5 == 0) goto L_0x0301
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x0316 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ all -> 0x0316 }
            java.lang.Object r6 = r0.get(r5)     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.beta.download.DownloadTask r6 = (com.tencent.bugly.beta.download.DownloadTask) r6     // Catch:{ all -> 0x0316 }
            java.io.File r6 = r6.getSaveFile()     // Catch:{ all -> 0x0316 }
            if (r6 == 0) goto L_0x02c5
            java.lang.Object r6 = r0.get(r5)     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.beta.download.DownloadTask r6 = (com.tencent.bugly.beta.download.DownloadTask) r6     // Catch:{ all -> 0x0316 }
            java.io.File r6 = r6.getSaveFile()     // Catch:{ all -> 0x0316 }
            boolean r6 = r6.exists()     // Catch:{ all -> 0x0316 }
            if (r6 == 0) goto L_0x02c5
            com.tencent.bugly.beta.global.ResBean r6 = com.tencent.bugly.beta.global.ResBean.f5325a     // Catch:{ all -> 0x0316 }
            java.lang.Object r7 = r0.get(r5)     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.beta.download.DownloadTask r7 = (com.tencent.bugly.beta.download.DownloadTask) r7     // Catch:{ all -> 0x0316 }
            java.io.File r7 = r7.getSaveFile()     // Catch:{ all -> 0x0316 }
            java.lang.String r7 = r7.getAbsolutePath()     // Catch:{ all -> 0x0316 }
            r6.mo34046a(r5, r7)     // Catch:{ all -> 0x0316 }
            goto L_0x02c5
        L_0x0301:
            java.lang.String r0 = "rb.bch"
            com.tencent.bugly.beta.global.ResBean r4 = com.tencent.bugly.beta.global.ResBean.f5325a     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.beta.global.C2804a.m6270a(r0, r4)     // Catch:{ all -> 0x0316 }
            java.lang.Object[] r0 = r1.f5322b     // Catch:{ all -> 0x0316 }
            r0 = r0[r3]     // Catch:{ all -> 0x0316 }
            com.tencent.bugly.beta.global.f r0 = (com.tencent.bugly.beta.global.C2809f) r0     // Catch:{ all -> 0x0316 }
            r0.mo34056a()     // Catch:{ all -> 0x0316 }
            r0.mo34060b()     // Catch:{ all -> 0x0316 }
            monitor-exit(r2)     // Catch:{ all -> 0x0316 }
            goto L_0x0323
        L_0x0316:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0316 }
            throw r0     // Catch:{ Exception -> 0x0319 }
        L_0x0319:
            r0 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6861b(r0)
            if (r2 != 0) goto L_0x0323
            r0.printStackTrace()
        L_0x0323:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.download.C2801a.onCompleted(com.tencent.bugly.beta.download.DownloadTask):void");
    }

    public void onFailed(DownloadTask downloadTask, int i, String str) {
        DownloadTask downloadTask2 = downloadTask;
        String str2 = str;
        try {
            int i2 = this.f5321a;
            if (i2 == 1) {
                synchronized (this.f5322b[0]) {
                    C2809f fVar = (C2809f) this.f5322b[0];
                    fVar.mo34056a();
                    fVar.mo34060b();
                }
            } else if (i2 == 2) {
                ((C2826h) this.f5322b[0]).mo34163a(downloadTask2);
            } else if (i2 == 3) {
                C2829c cVar = (C2829c) this.f5322b[0];
                if (downloadTask2 != null) {
                    C2947p pVar = C2947p.f6229a;
                    C2957w wVar = r7;
                    C2957w wVar2 = new C2957w("download", System.currentTimeMillis(), (byte) 1, downloadTask.getCostTime(), cVar.f5468b.f5455a.f6286e, cVar.f5468b.f5455a.f6294m, cVar.f5468b.f5455a.f6297p, null);
                    pVar.mo34667a(wVar);
                }
                C2903an.m6865e("upgrade failed：(%d)%s", Integer.valueOf(i), str2);
                Log.e(C2903an.f6030b, "download fail, please try later");
            } else if (i2 == 4) {
                C2948q qVar = (C2948q) this.f5322b[0];
                if (downloadTask2 != null) {
                    C2947p pVar2 = C2947p.f6229a;
                    long currentTimeMillis = System.currentTimeMillis();
                    long costTime = downloadTask.getCostTime();
                    C2956v vVar = qVar.f6231b.f5455a.f6286e;
                    C2957w wVar3 = r8;
                    C2956v vVar2 = vVar;
                    C2957w wVar4 = new C2957w("download", currentTimeMillis, (byte) 1, costTime, vVar2, qVar.f6231b.f5455a.f6294m, qVar.f6231b.f5455a.f6297p, null);
                    pVar2.mo34667a(wVar3);
                }
                C2903an.m6865e("hotfix download failed：(%d)%s", Integer.valueOf(i), str2);
                TinkerManager.getInstance().onDownloadFailure(str2);
            }
        } catch (Exception e) {
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
        }
    }
}
