package com.tencent.stat;

import android.content.Context;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;
import java.lang.Thread;

/* renamed from: com.tencent.stat.a */
public final class C3190a implements Thread.UncaughtExceptionHandler {

    /* renamed from: a */
    private static StatLogger f7159a = StatCommonHelper.getLogger();

    /* renamed from: c */
    private static volatile C3190a f7160c = null;

    /* renamed from: d */
    private static Thread.UncaughtExceptionHandler f7161d = null;

    /* renamed from: b */
    private Context f7162b = null;

    /* renamed from: e */
    private boolean f7163e = false;

    private C3190a(Context context) {
        this.f7162b = context;
    }

    /* renamed from: a */
    public static C3190a m7914a(Context context) {
        if (f7160c == null) {
            synchronized (C3190a.class) {
                if (f7160c == null) {
                    f7160c = new C3190a(context);
                }
            }
        }
        return f7160c;
    }

    /* renamed from: a */
    public void mo35358a() {
        if (f7161d == null) {
            f7161d = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(f7160c);
            StatLogger statLogger = f7159a;
            statLogger.mo35381d("set up java crash handler:" + f7160c);
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        if (this.f7163e) {
            StatLogger statLogger = f7159a;
            statLogger.mo35383e("already handle the uncaugth exception:" + th);
            return;
        }
        this.f7163e = true;
        f7159a.mo35381d("catch app crash");
        StatServiceImpl.m7875b(thread, th);
        if (f7161d != null) {
            f7159a.mo35381d("Call the original uncaught exception handler.");
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler = f7161d;
            if (!(uncaughtExceptionHandler instanceof C3190a)) {
                uncaughtExceptionHandler.uncaughtException(thread, th);
            }
        }
    }
}
