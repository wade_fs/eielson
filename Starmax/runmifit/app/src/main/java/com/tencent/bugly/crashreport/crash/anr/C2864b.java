package com.tencent.bugly.crashreport.crash.anr;

import android.app.ActivityManager;
import android.content.Context;
import android.os.FileObserver;
import android.os.Process;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.info.C2852b;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.crash.C2867b;
import com.tencent.bugly.crashreport.crash.C2869c;
import com.tencent.bugly.crashreport.crash.CrashDetailBean;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2905ap;
import com.tencent.bugly.proguard.C2908aq;
import com.tencent.bugly.proguard.C2910as;
import com.tencent.bugly.proguard.C2912at;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/* renamed from: com.tencent.bugly.crashreport.crash.anr.b */
/* compiled from: BUGLY */
public class C2864b implements C2912at {

    /* renamed from: a */
    private AtomicInteger f5800a = new AtomicInteger(0);

    /* renamed from: b */
    private long f5801b = -1;

    /* renamed from: c */
    private final Context f5802c;

    /* renamed from: d */
    private final C2851a f5803d;

    /* renamed from: e */
    private final C2901am f5804e;

    /* renamed from: f */
    private final C2854a f5805f;

    /* renamed from: g */
    private final String f5806g;

    /* renamed from: h */
    private final C2867b f5807h;

    /* renamed from: i */
    private FileObserver f5808i;

    /* renamed from: j */
    private boolean f5809j = true;

    public C2864b(Context context, C2854a aVar, C2851a aVar2, C2901am amVar, C2889ae aeVar, C2867b bVar, BuglyStrategy.C2796a aVar3) {
        this.f5802c = C2908aq.m6891a(context);
        this.f5806g = context.getDir("bugly", 0).getAbsolutePath();
        this.f5803d = aVar2;
        this.f5804e = amVar;
        this.f5805f = aVar;
        this.f5807h = bVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ActivityManager.ProcessErrorStateInfo mo34363a(Context context, long j) {
        if (j < 0) {
            j = 0;
        }
        C2903an.m6863c("to find!", new Object[0]);
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        long j2 = j / 500;
        int i = 0;
        while (true) {
            C2903an.m6863c("waiting!", new Object[0]);
            List<ActivityManager.ProcessErrorStateInfo> processesInErrorState = activityManager.getProcessesInErrorState();
            if (processesInErrorState != null) {
                for (ActivityManager.ProcessErrorStateInfo processErrorStateInfo : processesInErrorState) {
                    if (processErrorStateInfo.condition == 2) {
                        C2903an.m6863c("found!", new Object[0]);
                        return processErrorStateInfo;
                    }
                }
            }
            C2908aq.m6928b(500);
            int i2 = i + 1;
            if (((long) i) >= j2) {
                C2903an.m6863c("end!", new Object[0]);
                return null;
            }
            i = i2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public C2863a mo34365a(Context context, ActivityManager.ProcessErrorStateInfo processErrorStateInfo, long j, Map<String, String> map) {
        File filesDir = context.getFilesDir();
        File file = new File(filesDir, "bugly/bugly_trace_" + j + ".txt");
        C2863a aVar = new C2863a();
        aVar.f5795c = j;
        aVar.f5796d = file.getAbsolutePath();
        String str = "";
        aVar.f5793a = processErrorStateInfo != null ? processErrorStateInfo.processName : str;
        aVar.f5798f = processErrorStateInfo != null ? processErrorStateInfo.shortMsg : str;
        if (processErrorStateInfo != null) {
            str = processErrorStateInfo.longMsg;
        }
        aVar.f5797e = str;
        aVar.f5794b = map;
        if (map != null) {
            for (String str2 : map.keySet()) {
                if (str2.startsWith("main(")) {
                    aVar.f5799g = map.get(str2);
                }
            }
        }
        Object[] objArr = new Object[6];
        int i = 0;
        objArr[0] = Long.valueOf(aVar.f5795c);
        objArr[1] = aVar.f5796d;
        objArr[2] = aVar.f5793a;
        objArr[3] = aVar.f5798f;
        objArr[4] = aVar.f5797e;
        if (aVar.f5794b != null) {
            i = aVar.f5794b.size();
        }
        objArr[5] = Integer.valueOf(i);
        C2903an.m6863c("anr tm:%d\ntr:%s\nproc:%s\nsMsg:%s\n lMsg:%s\n threads:%d", objArr);
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public CrashDetailBean mo34364a(C2863a aVar) {
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        try {
            crashDetailBean.f5731C = C2852b.m6550i();
            crashDetailBean.f5732D = C2852b.m6546g();
            crashDetailBean.f5733E = C2852b.m6554k();
            crashDetailBean.f5734F = this.f5803d.mo34319p();
            crashDetailBean.f5735G = this.f5803d.mo34318o();
            crashDetailBean.f5736H = this.f5803d.mo34320q();
            crashDetailBean.f5774w = C2908aq.m6899a(this.f5802c, C2869c.f5825e, C2869c.f5828h);
            crashDetailBean.f5753b = 3;
            crashDetailBean.f5756e = this.f5803d.mo34311h();
            crashDetailBean.f5757f = this.f5803d.f5671p;
            crashDetailBean.f5758g = this.f5803d.mo34326w();
            crashDetailBean.f5764m = this.f5803d.mo34309g();
            crashDetailBean.f5765n = "ANR_EXCEPTION";
            crashDetailBean.f5766o = aVar.f5798f;
            crashDetailBean.f5768q = aVar.f5799g;
            crashDetailBean.f5744P = new HashMap();
            crashDetailBean.f5744P.put("BUGLY_CR_01", aVar.f5797e);
            int i = -1;
            if (crashDetailBean.f5768q != null) {
                i = crashDetailBean.f5768q.indexOf("\n");
            }
            crashDetailBean.f5767p = i > 0 ? crashDetailBean.f5768q.substring(0, i) : "GET_FAIL";
            crashDetailBean.f5769r = aVar.f5795c;
            if (crashDetailBean.f5768q != null) {
                crashDetailBean.f5772u = C2908aq.m6926b(crashDetailBean.f5768q.getBytes());
            }
            crashDetailBean.f5777z = aVar.f5794b;
            crashDetailBean.f5729A = aVar.f5793a;
            crashDetailBean.f5730B = "main(1)";
            crashDetailBean.f5737I = this.f5803d.mo34328y();
            crashDetailBean.f5759h = this.f5803d.mo34325v();
            crashDetailBean.f5760i = this.f5803d.mo34280J();
            crashDetailBean.f5773v = aVar.f5796d;
            crashDetailBean.f5741M = this.f5803d.f5675t;
            crashDetailBean.f5742N = this.f5803d.f5625a;
            crashDetailBean.f5743O = this.f5803d.mo34295a();
            crashDetailBean.f5745Q = this.f5803d.mo34278H();
            crashDetailBean.f5746R = this.f5803d.mo34279I();
            crashDetailBean.f5747S = this.f5803d.mo34272B();
            crashDetailBean.f5748T = this.f5803d.mo34277G();
            this.f5807h.mo34397c(crashDetailBean);
            crashDetailBean.f5776y = C2905ap.m6877a();
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        }
        return crashDetailBean;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0139 A[Catch:{ all -> 0x012e }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x0165 A[SYNTHETIC, Splitter:B:61:0x0165] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0177 A[SYNTHETIC, Splitter:B:69:0x0177] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo34371a(java.lang.String r17, java.lang.String r18, java.lang.String r19) {
        /*
            r16 = this;
            r1 = r18
            r0 = r19
            java.lang.String r2 = "main"
            java.lang.String r3 = ":"
            r4 = 1
            r5 = r17
            com.tencent.bugly.crashreport.crash.anr.TraceFileHelper$a r5 = com.tencent.bugly.crashreport.crash.anr.TraceFileHelper.readTargetDumpInfo(r0, r5, r4)
            r6 = 0
            if (r5 == 0) goto L_0x01c5
            java.util.Map<java.lang.String, java.lang.String[]> r7 = r5.f5792d
            if (r7 == 0) goto L_0x01c5
            java.util.Map<java.lang.String, java.lang.String[]> r7 = r5.f5792d
            int r7 = r7.size()
            if (r7 > 0) goto L_0x0020
            goto L_0x01c5
        L_0x0020:
            java.io.File r0 = new java.io.File
            r0.<init>(r1)
            r7 = 2
            boolean r8 = r0.exists()     // Catch:{ Exception -> 0x0191 }
            if (r8 != 0) goto L_0x0040
            java.io.File r8 = r0.getParentFile()     // Catch:{ Exception -> 0x0191 }
            boolean r8 = r8.exists()     // Catch:{ Exception -> 0x0191 }
            if (r8 != 0) goto L_0x003d
            java.io.File r8 = r0.getParentFile()     // Catch:{ Exception -> 0x0191 }
            r8.mkdirs()     // Catch:{ Exception -> 0x0191 }
        L_0x003d:
            r0.createNewFile()     // Catch:{ Exception -> 0x0191 }
        L_0x0040:
            boolean r8 = r0.exists()
            if (r8 == 0) goto L_0x0187
            boolean r8 = r0.canWrite()
            if (r8 != 0) goto L_0x004e
            goto L_0x0187
        L_0x004e:
            r1 = 0
            java.io.BufferedWriter r8 = new java.io.BufferedWriter     // Catch:{ IOException -> 0x0132 }
            java.io.FileWriter r9 = new java.io.FileWriter     // Catch:{ IOException -> 0x0132 }
            r9.<init>(r0, r6)     // Catch:{ IOException -> 0x0132 }
            r8.<init>(r9)     // Catch:{ IOException -> 0x0132 }
            java.util.Map<java.lang.String, java.lang.String[]> r0 = r5.f5792d     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.Object r0 = r0.get(r2)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String[] r0 = (java.lang.String[]) r0     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r1 = "\n\n"
            java.lang.String r9 = "\n"
            java.lang.String r10 = " :\n"
            r11 = 3
            if (r0 == 0) goto L_0x0099
            int r12 = r0.length     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            if (r12 < r11) goto L_0x0099
            r12 = r0[r6]     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r13 = r0[r4]     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r0 = r0[r7]     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.<init>()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r15 = "\"main\" tid="
            r14.append(r15)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.append(r0)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.append(r10)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.append(r12)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.append(r9)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.append(r13)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14.append(r1)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r0 = r14.toString()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r8.write(r0)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r8.flush()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
        L_0x0099:
            java.util.Map<java.lang.String, java.lang.String[]> r0 = r5.f5792d     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.util.Set r0 = r0.entrySet()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
        L_0x00a3:
            boolean r5 = r0.hasNext()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            if (r5 == 0) goto L_0x0119
            java.lang.Object r5 = r0.next()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.Object r12 = r5.getKey()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            boolean r12 = r12.equals(r2)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            if (r12 == 0) goto L_0x00bc
            goto L_0x00a3
        L_0x00bc:
            java.lang.Object r12 = r5.getValue()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            if (r12 == 0) goto L_0x0117
            java.lang.Object r12 = r5.getValue()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String[] r12 = (java.lang.String[]) r12     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            int r12 = r12.length     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            if (r12 < r11) goto L_0x0117
            java.lang.Object r12 = r5.getValue()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String[] r12 = (java.lang.String[]) r12     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r12 = r12[r6]     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.Object r13 = r5.getValue()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String[] r13 = (java.lang.String[]) r13     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r13 = r13[r4]     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.Object r14 = r5.getValue()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String[] r14 = (java.lang.String[]) r14     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r14 = r14[r7]     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.<init>()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r11 = "\""
            r15.append(r11)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.Object r5 = r5.getKey()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r5)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r5 = "\" tid="
            r15.append(r5)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r14)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r10)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r12)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r9)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r13)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r15.append(r1)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            java.lang.String r5 = r15.toString()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r8.write(r5)     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
            r8.flush()     // Catch:{ IOException -> 0x012b, all -> 0x0129 }
        L_0x0117:
            r11 = 3
            goto L_0x00a3
        L_0x0119:
            r8.close()     // Catch:{ IOException -> 0x011d }
            goto L_0x0128
        L_0x011d:
            r0 = move-exception
            r1 = r0
            boolean r0 = com.tencent.bugly.proguard.C2903an.m6858a(r1)
            if (r0 != 0) goto L_0x0128
            r1.printStackTrace()
        L_0x0128:
            return r4
        L_0x0129:
            r0 = move-exception
            goto L_0x0130
        L_0x012b:
            r0 = move-exception
            r1 = r8
            goto L_0x0133
        L_0x012e:
            r0 = move-exception
            r8 = r1
        L_0x0130:
            r1 = r0
            goto L_0x0175
        L_0x0132:
            r0 = move-exception
        L_0x0133:
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x012e }
            if (r2 != 0) goto L_0x013c
            r0.printStackTrace()     // Catch:{ all -> 0x012e }
        L_0x013c:
            java.lang.String r2 = "dump trace fail %s"
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x012e }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x012e }
            r5.<init>()     // Catch:{ all -> 0x012e }
            java.lang.Class r7 = r0.getClass()     // Catch:{ all -> 0x012e }
            java.lang.String r7 = r7.getName()     // Catch:{ all -> 0x012e }
            r5.append(r7)     // Catch:{ all -> 0x012e }
            r5.append(r3)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x012e }
            r5.append(r0)     // Catch:{ all -> 0x012e }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x012e }
            r4[r6] = r0     // Catch:{ all -> 0x012e }
            com.tencent.bugly.proguard.C2903an.m6865e(r2, r4)     // Catch:{ all -> 0x012e }
            if (r1 == 0) goto L_0x0174
            r1.close()     // Catch:{ IOException -> 0x0169 }
            goto L_0x0174
        L_0x0169:
            r0 = move-exception
            r1 = r0
            boolean r0 = com.tencent.bugly.proguard.C2903an.m6858a(r1)
            if (r0 != 0) goto L_0x0174
            r1.printStackTrace()
        L_0x0174:
            return r6
        L_0x0175:
            if (r8 == 0) goto L_0x0186
            r8.close()     // Catch:{ IOException -> 0x017b }
            goto L_0x0186
        L_0x017b:
            r0 = move-exception
            r2 = r0
            boolean r0 = com.tencent.bugly.proguard.C2903an.m6858a(r2)
            if (r0 != 0) goto L_0x0186
            r2.printStackTrace()
        L_0x0186:
            throw r1
        L_0x0187:
            java.lang.Object[] r0 = new java.lang.Object[r4]
            r0[r6] = r1
            java.lang.String r1 = "backup file create fail %s"
            com.tencent.bugly.proguard.C2903an.m6865e(r1, r0)
            return r6
        L_0x0191:
            r0 = move-exception
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r0)
            if (r2 != 0) goto L_0x019b
            r0.printStackTrace()
        L_0x019b:
            java.lang.Object[] r2 = new java.lang.Object[r7]
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.Class r7 = r0.getClass()
            java.lang.String r7 = r7.getName()
            r5.append(r7)
            r5.append(r3)
            java.lang.String r0 = r0.getMessage()
            r5.append(r0)
            java.lang.String r0 = r5.toString()
            r2[r6] = r0
            r2[r4] = r1
            java.lang.String r0 = "backup file create error! %s  %s"
            com.tencent.bugly.proguard.C2903an.m6865e(r0, r2)
            return r6
        L_0x01c5:
            java.lang.Object[] r1 = new java.lang.Object[r4]
            r1[r6] = r0
            java.lang.String r0 = "not found trace dump for %s"
            com.tencent.bugly.proguard.C2903an.m6865e(r0, r1)
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C2864b.mo34371a(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* renamed from: a */
    public boolean mo34369a() {
        return this.f5800a.get() != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
     arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
     candidates:
      com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.be
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bf
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean>, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bg
      com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
    /* renamed from: a */
    public boolean mo34370a(Context context, String str, ActivityManager.ProcessErrorStateInfo processErrorStateInfo, long j, Map<String, String> map) {
        C2863a a = mo34365a(context, processErrorStateInfo, j, map);
        if (!this.f5805f.mo34339b()) {
            C2903an.m6865e("crash report sync remote fail, will not upload to Bugly , print local for helpful!", new Object[0]);
            C2867b.m6631a("ANR", C2908aq.m6897a(), a.f5793a, "main", a.f5797e, null);
            return false;
        } else if (!this.f5805f.mo34340c().f5696j) {
            C2903an.m6864d("ANR Report is closed!", new Object[0]);
            return false;
        } else {
            C2903an.m6857a("found visiable anr , start to upload!", new Object[0]);
            CrashDetailBean a2 = mo34364a(a);
            if (a2 == null) {
                C2903an.m6865e("pack anr fail!", new Object[0]);
                return false;
            }
            C2869c.m6653a().mo34404a(a2);
            if (a2.f5752a >= 0) {
                C2903an.m6857a("backup anr record success!", new Object[0]);
            } else {
                C2903an.m6864d("backup anr record fail!", new Object[0]);
            }
            if (str != null && new File(str).exists()) {
                this.f5800a.set(3);
                if (mo34371a(str, a.f5796d, a.f5793a)) {
                    C2903an.m6857a("backup trace success", new Object[0]);
                }
            }
            C2867b.m6631a("ANR", C2908aq.m6897a(), a.f5793a, "main", a.f5797e, a2);
            if (!this.f5807h.mo34391a(a2)) {
                this.f5807h.mo34388a(a2, 3000L, true);
            }
            this.f5807h.mo34396b(a2);
            return true;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.aq.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.aq.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.aq.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.aq.a(java.io.File, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.aq.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.aq.a(android.content.Context, java.lang.String):java.util.ArrayList<java.lang.String>
      com.tencent.bugly.proguard.aq.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.aq.a(byte[], int):byte[]
      com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("read trace first dump for create time!", new java.lang.Object[0]);
        r0 = com.tencent.bugly.crashreport.crash.anr.TraceFileHelper.readFirstDumpInfo(r11, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0027, code lost:
        if (r0 == null) goto L_0x002c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0029, code lost:
        r5 = r0.f5791c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x002c, code lost:
        r5 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x002f, code lost:
        if (r5 != -1) goto L_0x003c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0031, code lost:
        com.tencent.bugly.proguard.C2903an.m6864d("trace dump fail could not get time!", new java.lang.Object[0]);
        r5 = java.lang.System.currentTimeMillis();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003c, code lost:
        r7 = r5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0049, code lost:
        if (java.lang.Math.abs(r7 - r10.f5801b) >= 10000) goto L_0x0060;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x004b, code lost:
        com.tencent.bugly.proguard.C2903an.m6864d("should not process ANR too Fre in %d", 10000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:?, code lost:
        r10.f5801b = r7;
        r10.f5800a.set(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:?, code lost:
        r9 = com.tencent.bugly.proguard.C2908aq.m6907a(com.tencent.bugly.crashreport.crash.C2869c.f5826f, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x006d, code lost:
        if (r9 == null) goto L_0x00a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0073, code lost:
        if (r9.size() > 0) goto L_0x0076;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0076, code lost:
        r6 = mo34363a(r10.f5802c, 10000);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007c, code lost:
        if (r6 != null) goto L_0x0086;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x007e, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("proc state is unvisiable!", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x008c, code lost:
        if (r6.pid == android.os.Process.myPid()) goto L_0x009a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008e, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("not mind proc!", r6.processName);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x009a, code lost:
        com.tencent.bugly.proguard.C2903an.m6857a("found visiable anr , start to process!", new java.lang.Object[0]);
        mo34370a(r10.f5802c, r11, r6, r7, r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a9, code lost:
        com.tencent.bugly.proguard.C2903an.m6864d("can't get all thread skip this anr", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00b1, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b2, code lost:
        com.tencent.bugly.proguard.C2903an.m6858a(r11);
        com.tencent.bugly.proguard.C2903an.m6865e("get all thread stack fail!", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00bd, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x00c2, code lost:
        if (com.tencent.bugly.proguard.C2903an.m6858a(r11) == false) goto L_0x00c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00c4, code lost:
        r11.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00c7, code lost:
        com.tencent.bugly.proguard.C2903an.m6865e("handle anr error %s", r11.getClass().toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00de, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00df, code lost:
        r10.f5800a.set(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00e4, code lost:
        throw r11;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void mo34367a(java.lang.String r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f5800a     // Catch:{ all -> 0x00e5 }
            int r0 = r0.get()     // Catch:{ all -> 0x00e5 }
            r1 = 0
            if (r0 == 0) goto L_0x0013
            java.lang.String r11 = "trace started return "
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00e5 }
            com.tencent.bugly.proguard.C2903an.m6863c(r11, r0)     // Catch:{ all -> 0x00e5 }
            monitor-exit(r10)     // Catch:{ all -> 0x00e5 }
            return
        L_0x0013:
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f5800a     // Catch:{ all -> 0x00e5 }
            r2 = 1
            r0.set(r2)     // Catch:{ all -> 0x00e5 }
            monitor-exit(r10)     // Catch:{ all -> 0x00e5 }
            java.lang.String r0 = "read trace first dump for create time!"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r3)     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.crashreport.crash.anr.TraceFileHelper$a r0 = com.tencent.bugly.crashreport.crash.anr.TraceFileHelper.readFirstDumpInfo(r11, r1)     // Catch:{ all -> 0x00bd }
            r3 = -1
            if (r0 == 0) goto L_0x002c
            long r5 = r0.f5791c     // Catch:{ all -> 0x00bd }
            goto L_0x002d
        L_0x002c:
            r5 = r3
        L_0x002d:
            int r0 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r0 != 0) goto L_0x003c
            java.lang.String r0 = "trace dump fail could not get time!"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r3)     // Catch:{ all -> 0x00bd }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00bd }
        L_0x003c:
            r7 = r5
            long r3 = r10.f5801b     // Catch:{ all -> 0x00bd }
            long r3 = r7 - r3
            long r3 = java.lang.Math.abs(r3)     // Catch:{ all -> 0x00bd }
            r5 = 10000(0x2710, double:4.9407E-320)
            int r0 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x0060
            java.lang.String r11 = "should not process ANR too Fre in %d"
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ all -> 0x00bd }
            r3 = 10000(0x2710, float:1.4013E-41)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x00bd }
            r0[r1] = r3     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6864d(r11, r0)     // Catch:{ all -> 0x00bd }
        L_0x005a:
            java.util.concurrent.atomic.AtomicInteger r11 = r10.f5800a
            r11.set(r1)
            return
        L_0x0060:
            r10.f5801b = r7     // Catch:{ all -> 0x00bd }
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f5800a     // Catch:{ all -> 0x00bd }
            r0.set(r2)     // Catch:{ all -> 0x00bd }
            int r0 = com.tencent.bugly.crashreport.crash.C2869c.f5826f     // Catch:{ all -> 0x00b1 }
            java.util.Map r9 = com.tencent.bugly.proguard.C2908aq.m6907a(r0, r1)     // Catch:{ all -> 0x00b1 }
            if (r9 == 0) goto L_0x00a9
            int r0 = r9.size()     // Catch:{ all -> 0x00bd }
            if (r0 > 0) goto L_0x0076
            goto L_0x00a9
        L_0x0076:
            android.content.Context r0 = r10.f5802c     // Catch:{ all -> 0x00bd }
            android.app.ActivityManager$ProcessErrorStateInfo r6 = r10.mo34363a(r0, r5)     // Catch:{ all -> 0x00bd }
            if (r6 != 0) goto L_0x0086
            java.lang.String r11 = "proc state is unvisiable!"
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6863c(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x0086:
            int r0 = r6.pid     // Catch:{ all -> 0x00bd }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x00bd }
            if (r0 == r3) goto L_0x009a
            java.lang.String r11 = "not mind proc!"
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ all -> 0x00bd }
            java.lang.String r3 = r6.processName     // Catch:{ all -> 0x00bd }
            r0[r1] = r3     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6863c(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x009a:
            java.lang.String r0 = "found visiable anr , start to process!"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r3)     // Catch:{ all -> 0x00bd }
            android.content.Context r4 = r10.f5802c     // Catch:{ all -> 0x00bd }
            r3 = r10
            r5 = r11
            r3.mo34370a(r4, r5, r6, r7, r9)     // Catch:{ all -> 0x00bd }
            goto L_0x00d8
        L_0x00a9:
            java.lang.String r11 = "can't get all thread skip this anr"
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6864d(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x00b1:
            r11 = move-exception
            com.tencent.bugly.proguard.C2903an.m6858a(r11)     // Catch:{ all -> 0x00bd }
            java.lang.String r11 = "get all thread stack fail!"
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x00bd }
            com.tencent.bugly.proguard.C2903an.m6865e(r11, r0)     // Catch:{ all -> 0x00bd }
            goto L_0x005a
        L_0x00bd:
            r11 = move-exception
            boolean r0 = com.tencent.bugly.proguard.C2903an.m6858a(r11)     // Catch:{ all -> 0x00de }
            if (r0 != 0) goto L_0x00c7
            r11.printStackTrace()     // Catch:{ all -> 0x00de }
        L_0x00c7:
            java.lang.String r0 = "handle anr error %s"
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x00de }
            java.lang.Class r11 = r11.getClass()     // Catch:{ all -> 0x00de }
            java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x00de }
            r2[r1] = r11     // Catch:{ all -> 0x00de }
            com.tencent.bugly.proguard.C2903an.m6865e(r0, r2)     // Catch:{ all -> 0x00de }
        L_0x00d8:
            java.util.concurrent.atomic.AtomicInteger r11 = r10.f5800a
            r11.set(r1)
            return
        L_0x00de:
            r11 = move-exception
            java.util.concurrent.atomic.AtomicInteger r0 = r10.f5800a
            r0.set(r1)
            throw r11
        L_0x00e5:
            r11 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00e5 }
            goto L_0x00e9
        L_0x00e8:
            throw r11
        L_0x00e9:
            goto L_0x00e8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C2864b.mo34367a(java.lang.String):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0048, code lost:
        return;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34373b() {
        /*
            r4 = this;
            monitor-enter(r4)
            boolean r0 = r4.mo34376d()     // Catch:{ all -> 0x0049 }
            r1 = 0
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "start when started!"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0049 }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r1)     // Catch:{ all -> 0x0049 }
            monitor-exit(r4)
            return
        L_0x0011:
            com.tencent.bugly.crashreport.crash.anr.b$1 r0 = new com.tencent.bugly.crashreport.crash.anr.b$1     // Catch:{ all -> 0x0049 }
            java.lang.String r2 = "/data/anr/"
            r3 = 8
            r0.<init>(r2, r3)     // Catch:{ all -> 0x0049 }
            r4.f5808i = r0     // Catch:{ all -> 0x0049 }
            android.os.FileObserver r0 = r4.f5808i     // Catch:{ all -> 0x0033 }
            r0.startWatching()     // Catch:{ all -> 0x0033 }
            java.lang.String r0 = "start anr monitor!"
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x0033 }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r2)     // Catch:{ all -> 0x0033 }
            com.tencent.bugly.proguard.am r0 = r4.f5804e     // Catch:{ all -> 0x0033 }
            com.tencent.bugly.crashreport.crash.anr.b$2 r2 = new com.tencent.bugly.crashreport.crash.anr.b$2     // Catch:{ all -> 0x0033 }
            r2.<init>()     // Catch:{ all -> 0x0033 }
            r0.mo34547a(r2)     // Catch:{ all -> 0x0033 }
            goto L_0x0047
        L_0x0033:
            r0 = move-exception
            r2 = 0
            r4.f5808i = r2     // Catch:{ all -> 0x0049 }
            java.lang.String r2 = "start anr monitor failed!"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0049 }
            com.tencent.bugly.proguard.C2903an.m6864d(r2, r1)     // Catch:{ all -> 0x0049 }
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x0049 }
            if (r1 != 0) goto L_0x0047
            r0.printStackTrace()     // Catch:{ all -> 0x0049 }
        L_0x0047:
            monitor-exit(r4)
            return
        L_0x0049:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C2864b.mo34373b():void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0033, code lost:
        return;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34375c() {
        /*
            r3 = this;
            monitor-enter(r3)
            boolean r0 = r3.mo34376d()     // Catch:{ all -> 0x0034 }
            r1 = 0
            if (r0 != 0) goto L_0x0011
            java.lang.String r0 = "close when closed!"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0034 }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r1)     // Catch:{ all -> 0x0034 }
            monitor-exit(r3)
            return
        L_0x0011:
            android.os.FileObserver r0 = r3.f5808i     // Catch:{ all -> 0x0021 }
            r0.stopWatching()     // Catch:{ all -> 0x0021 }
            r0 = 0
            r3.f5808i = r0     // Catch:{ all -> 0x0021 }
            java.lang.String r0 = "close anr monitor!"
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x0021 }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r2)     // Catch:{ all -> 0x0021 }
            goto L_0x0032
        L_0x0021:
            r0 = move-exception
            java.lang.String r2 = "stop anr monitor failed!"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0034 }
            com.tencent.bugly.proguard.C2903an.m6864d(r2, r1)     // Catch:{ all -> 0x0034 }
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x0034 }
            if (r1 != 0) goto L_0x0032
            r0.printStackTrace()     // Catch:{ all -> 0x0034 }
        L_0x0032:
            monitor-exit(r3)
            return
        L_0x0034:
            r0 = move-exception
            monitor-exit(r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C2864b.mo34375c():void");
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public synchronized boolean mo34376d() {
        return this.f5808i != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized void mo34368a(boolean z) {
        if (z) {
            mo34373b();
        } else {
            mo34375c();
        }
    }

    /* renamed from: e */
    public synchronized boolean mo34377e() {
        return this.f5809j;
    }

    /* renamed from: c */
    private synchronized void m6608c(boolean z) {
        if (this.f5809j != z) {
            C2903an.m6857a("user change anr %b", Boolean.valueOf(z));
            this.f5809j = z;
        }
    }

    /* renamed from: b */
    public void mo34374b(boolean z) {
        m6608c(z);
        boolean e = mo34377e();
        C2854a a = C2854a.m6574a();
        if (a != null) {
            e = e && a.mo34340c().f5693g;
        }
        if (e != mo34376d()) {
            C2903an.m6857a("anr changed to %b", Boolean.valueOf(e));
            mo34368a(e);
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:20|21) */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("Trace file that has invalid format: " + r11, new java.lang.Object[0]);
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:20:0x004f */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo34378f() {
        /*
            r14 = this;
            long r0 = com.tencent.bugly.proguard.C2908aq.m6923b()
            long r2 = com.tencent.bugly.crashreport.crash.C2869c.f5827g
            long r0 = r0 - r2
            java.io.File r2 = new java.io.File
            java.lang.String r3 = r14.f5806g
            r2.<init>(r3)
            boolean r3 = r2.exists()
            if (r3 == 0) goto L_0x008c
            boolean r3 = r2.isDirectory()
            if (r3 == 0) goto L_0x008c
            java.io.File[] r2 = r2.listFiles()     // Catch:{ all -> 0x0088 }
            if (r2 == 0) goto L_0x0087
            int r3 = r2.length     // Catch:{ all -> 0x0088 }
            if (r3 != 0) goto L_0x0024
            goto L_0x0087
        L_0x0024:
            java.lang.String r3 = "bugly_trace_"
            java.lang.String r4 = ".txt"
            r5 = 12
            int r6 = r2.length     // Catch:{ all -> 0x0088 }
            r7 = 0
            r8 = 0
            r9 = 0
        L_0x002e:
            if (r8 >= r6) goto L_0x0070
            r10 = r2[r8]     // Catch:{ all -> 0x0088 }
            java.lang.String r11 = r10.getName()     // Catch:{ all -> 0x0088 }
            boolean r12 = r11.startsWith(r3)     // Catch:{ all -> 0x0088 }
            if (r12 == 0) goto L_0x006d
            int r12 = r11.indexOf(r4)     // Catch:{ all -> 0x004f }
            if (r12 <= 0) goto L_0x0065
            java.lang.String r12 = r11.substring(r5, r12)     // Catch:{ all -> 0x004f }
            long r11 = java.lang.Long.parseLong(r12)     // Catch:{ all -> 0x004f }
            int r13 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r13 < 0) goto L_0x0065
            goto L_0x006d
        L_0x004f:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0088 }
            r12.<init>()     // Catch:{ all -> 0x0088 }
            java.lang.String r13 = "Trace file that has invalid format: "
            r12.append(r13)     // Catch:{ all -> 0x0088 }
            r12.append(r11)     // Catch:{ all -> 0x0088 }
            java.lang.String r11 = r12.toString()     // Catch:{ all -> 0x0088 }
            java.lang.Object[] r12 = new java.lang.Object[r7]     // Catch:{ all -> 0x0088 }
            com.tencent.bugly.proguard.C2903an.m6863c(r11, r12)     // Catch:{ all -> 0x0088 }
        L_0x0065:
            boolean r10 = r10.delete()     // Catch:{ all -> 0x0088 }
            if (r10 == 0) goto L_0x006d
            int r9 = r9 + 1
        L_0x006d:
            int r8 = r8 + 1
            goto L_0x002e
        L_0x0070:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0088 }
            r0.<init>()     // Catch:{ all -> 0x0088 }
            java.lang.String r1 = "Number of overdue trace files that has deleted: "
            r0.append(r1)     // Catch:{ all -> 0x0088 }
            r0.append(r9)     // Catch:{ all -> 0x0088 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0088 }
            java.lang.Object[] r1 = new java.lang.Object[r7]     // Catch:{ all -> 0x0088 }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r1)     // Catch:{ all -> 0x0088 }
            goto L_0x008c
        L_0x0087:
            return
        L_0x0088:
            r0 = move-exception
            com.tencent.bugly.proguard.C2903an.m6858a(r0)
        L_0x008c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C2864b.mo34378f():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0054, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34366a(com.tencent.bugly.crashreport.common.strategy.StrategyBean r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            if (r6 != 0) goto L_0x0005
            monitor-exit(r5)
            return
        L_0x0005:
            boolean r0 = r6.f5696j     // Catch:{ all -> 0x0055 }
            boolean r1 = r5.mo34376d()     // Catch:{ all -> 0x0055 }
            r2 = 0
            r3 = 1
            if (r0 == r1) goto L_0x001e
            java.lang.String r0 = "server anr changed to %b"
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x0055 }
            boolean r4 = r6.f5696j     // Catch:{ all -> 0x0055 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ all -> 0x0055 }
            r1[r2] = r4     // Catch:{ all -> 0x0055 }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r1)     // Catch:{ all -> 0x0055 }
        L_0x001e:
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0055 }
            r1 = 19
            if (r0 > r1) goto L_0x0048
            boolean r6 = r6.f5696j     // Catch:{ all -> 0x0055 }
            if (r6 == 0) goto L_0x0030
            boolean r6 = r5.mo34377e()     // Catch:{ all -> 0x0055 }
            if (r6 == 0) goto L_0x0030
            r6 = 1
            goto L_0x0031
        L_0x0030:
            r6 = 0
        L_0x0031:
            boolean r0 = r5.mo34376d()     // Catch:{ all -> 0x0055 }
            if (r6 == r0) goto L_0x0053
            java.lang.String r0 = "anr changed to %b"
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x0055 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r6)     // Catch:{ all -> 0x0055 }
            r1[r2] = r3     // Catch:{ all -> 0x0055 }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r1)     // Catch:{ all -> 0x0055 }
            r5.mo34368a(r6)     // Catch:{ all -> 0x0055 }
            goto L_0x0053
        L_0x0048:
            boolean r6 = r6.f5696j     // Catch:{ all -> 0x0055 }
            if (r6 == 0) goto L_0x0050
            r5.mo34380h()     // Catch:{ all -> 0x0055 }
            goto L_0x0053
        L_0x0050:
            r5.mo34381i()     // Catch:{ all -> 0x0055 }
        L_0x0053:
            monitor-exit(r5)
            return
        L_0x0055:
            r6 = move-exception
            monitor-exit(r5)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.anr.C2864b.mo34366a(com.tencent.bugly.crashreport.common.strategy.StrategyBean):void");
    }

    /* renamed from: g */
    public void mo34379g() {
        int i = 0;
        while (true) {
            int i2 = i + 1;
            if (i < 30) {
                try {
                    C2903an.m6857a("try main sleep for make a test anr! try:%d/30 , kill it if you don't want to wait!", Integer.valueOf(i2));
                    C2908aq.m6928b(5000);
                    i = i2;
                } catch (Throwable th) {
                    if (!C2903an.m6858a(th)) {
                        th.printStackTrace();
                        return;
                    }
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.aq.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.aq.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.aq.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.aq.a(java.io.File, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.aq.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.aq.a(android.content.Context, java.lang.String):java.util.ArrayList<java.lang.String>
      com.tencent.bugly.proguard.aq.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.aq.a(byte[], int):byte[]
      com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* renamed from: a */
    public boolean mo34372a(Thread thread) {
        new HashMap();
        if (thread.getName().contains("main")) {
            ActivityManager.ProcessErrorStateInfo a = mo34363a(this.f5802c, 10000);
            if (a == null) {
                C2903an.m6863c("anr handler onThreadBlock proc state is unvisiable!", new Object[0]);
                return false;
            } else if (a.pid != Process.myPid()) {
                C2903an.m6863c("onThreadBlock not mind proc!", a.processName);
                return false;
            } else {
                try {
                    Map<String, String> a2 = C2908aq.m6907a(200000, false);
                    C2903an.m6857a("onThreadBlock found visiable anr , start to process!", new Object[0]);
                    mo34370a(this.f5802c, "", a, System.currentTimeMillis(), a2);
                } catch (Throwable unused) {
                    return false;
                }
            }
        } else {
            C2903an.m6863c("anr handler onThreadBlock only care main thread", new Object[0]);
        }
        return true;
    }

    /* renamed from: h */
    public void mo34380h() {
        C2910as.m6950a(this.f5802c).mo34562a();
        C2910as.m6950a(this.f5802c).mo34565a(this);
        C2910as.m6950a(this.f5802c).mo34570d();
    }

    /* renamed from: i */
    public void mo34381i() {
        C2910as.m6950a(this.f5802c).mo34566b();
        C2910as.m6950a(this.f5802c).mo34568b(this);
        C2910as.m6950a(this.f5802c).mo34569c();
    }
}
