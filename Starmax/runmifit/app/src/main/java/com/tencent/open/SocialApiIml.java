package com.tencent.open;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.CookieSyncManager;
import android.webkit.WebSettings;
import com.tencent.connect.auth.C2983c;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.connect.common.UIListenerManager;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p061c.C3109b;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3122f;
import com.tencent.open.utils.C3124g;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class SocialApiIml extends BaseApi {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public Activity f6661a;

    public SocialApiIml(QQToken qQToken) {
        super(qQToken);
    }

    public SocialApiIml(C2983c cVar, QQToken qQToken) {
        super(cVar, qQToken);
    }

    public void gift(Activity activity, Bundle bundle, IUiListener iUiListener) {
        m7558a(activity, SocialConstants.ACTION_GIFT, bundle, iUiListener);
    }

    public void ask(Activity activity, Bundle bundle, IUiListener iUiListener) {
        m7558a(activity, SocialConstants.ACTION_ASK, bundle, iUiListener);
    }

    /* renamed from: a */
    private void m7558a(Activity activity, String str, Bundle bundle, IUiListener iUiListener) {
        this.f6661a = activity;
        Intent c = mo34807c(SocialConstants.ACTIVITY_FRIEND_CHOOSER);
        if (c == null) {
            C3082f.m7634c("openSDK_LOG.SocialApiIml", "--askgift--friend chooser not found");
            c = mo34807c(SocialConstants.ACTIVITY_ASK_GIFT);
        }
        Intent intent = c;
        bundle.putAll(mo34806b());
        if (SocialConstants.ACTION_ASK.equals(str)) {
            bundle.putString(SocialConstants.PARAM_TYPE, SocialConstants.TYPE_REQUEST);
        } else if (SocialConstants.ACTION_GIFT.equals(str)) {
            bundle.putString(SocialConstants.PARAM_TYPE, SocialConstants.TYPE_FREEGIFT);
        }
        m7557a(activity, intent, str, bundle, C3124g.m7744a().mo35172a(C3121e.m7727a(), "http://qzs.qq.com/open/mobile/request/sdk_request.html?"), iUiListener, false);
    }

    public void invite(Activity activity, Bundle bundle, IUiListener iUiListener) {
        this.f6661a = activity;
        Intent c = mo34807c(SocialConstants.ACTIVITY_FRIEND_CHOOSER);
        if (c == null) {
            C3082f.m7634c("openSDK_LOG.SocialApiIml", "--invite--friend chooser not found");
            c = mo34807c(SocialConstants.ACTIVITY_INVITE);
        }
        bundle.putAll(mo34806b());
        Activity activity2 = activity;
        m7557a(activity2, c, SocialConstants.ACTION_INVITE, bundle, C3124g.m7744a().mo35172a(C3121e.m7727a(), "http://qzs.qq.com/open/mobile/invite/sdk_invite.html?"), iUiListener, false);
    }

    public void story(Activity activity, Bundle bundle, IUiListener iUiListener) {
        this.f6661a = activity;
        Intent c = mo34807c(SocialConstants.ACTIVITY_STORY);
        bundle.putAll(mo34806b());
        m7557a(activity, c, SocialConstants.ACTION_STORY, bundle, C3124g.m7744a().mo35172a(C3121e.m7727a(), "http://qzs.qq.com/open/mobile/sendstory/sdk_sendstory_v1.3.html?"), iUiListener, false);
    }

    /* renamed from: a */
    private void m7557a(Activity activity, Intent intent, String str, Bundle bundle, String str2, IUiListener iUiListener, boolean z) {
        StringBuilder sb = new StringBuilder();
        sb.append("-->handleIntent action = ");
        sb.append(str);
        sb.append(", activityIntent = null ? ");
        boolean z2 = true;
        sb.append(intent == null);
        C3082f.m7634c("openSDK_LOG.SocialApiIml", sb.toString());
        if (intent != null) {
            m7556a(activity, intent, str, bundle, iUiListener);
            return;
        }
        C3122f a = C3122f.m7733a(C3121e.m7727a(), this.f6457c.getAppId());
        if (!z && !a.mo35170b("C_LoginH5")) {
            z2 = false;
        }
        if (z2) {
            m7559a(activity, str, bundle, str2, iUiListener);
        } else {
            mo34802a(activity, bundle, iUiListener);
        }
    }

    /* renamed from: a */
    private void m7556a(Activity activity, Intent intent, String str, Bundle bundle, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.SocialApiIml", "-->handleIntentWithAgent action = " + str);
        intent.putExtra(Constants.KEY_ACTION, str);
        intent.putExtra(Constants.KEY_PARAMS, bundle);
        UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_SOCIAL_API, iUiListener);
        mo34801a(activity, intent, (int) Constants.REQUEST_SOCIAL_API);
    }

    /* renamed from: a */
    private void m7559a(Activity activity, String str, Bundle bundle, String str2, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.SocialApiIml", "-->handleIntentWithH5 action = " + str);
        Intent b = mo34805b("com.tencent.open.agent.AgentActivity");
        C3065a aVar = new C3065a(activity, iUiListener, str, str2, bundle);
        Intent b2 = mo34805b("com.tencent.open.agent.EncryTokenActivity");
        if (b2 == null || b == null || b.getComponent() == null || b2.getComponent() == null || !b.getComponent().getPackageName().equals(b2.getComponent().getPackageName())) {
            C3082f.m7634c("openSDK_LOG.SocialApiIml", "-->handleIntentWithH5--token activity not found");
            String f = C3131k.m7793f("tencent&sdk&qazxc***14969%%" + this.f6457c.getAccessToken() + this.f6457c.getAppId() + this.f6457c.getOpenId() + "qzone3.4");
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(SocialConstants.PARAM_ENCRY_EOKEN, f);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            aVar.onComplete(jSONObject);
            return;
        }
        b2.putExtra("oauth_consumer_key", this.f6457c.getAppId());
        b2.putExtra("openid", this.f6457c.getOpenId());
        b2.putExtra(Constants.PARAM_ACCESS_TOKEN, this.f6457c.getAccessToken());
        b2.putExtra(Constants.KEY_ACTION, SocialConstants.ACTION_CHECK_TOKEN);
        if (mo34804a(b2)) {
            C3082f.m7634c("openSDK_LOG.SocialApiIml", "-->handleIntentWithH5--found token activity");
            UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_SOCIAL_H5, aVar);
            mo34801a(activity, b2, (int) Constants.REQUEST_SOCIAL_H5);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m7560a(Context context, String str, Bundle bundle, String str2, IUiListener iUiListener) {
        C3082f.m7628a("openSDK_LOG.SocialApiIml", "OpenUi, showDialog --start");
        CookieSyncManager.createInstance(context);
        bundle.putString("oauth_consumer_key", this.f6457c.getAppId());
        if (this.f6457c.isSessionValid()) {
            bundle.putString(Constants.PARAM_ACCESS_TOKEN, this.f6457c.getAccessToken());
        }
        String openId = this.f6457c.getOpenId();
        if (openId != null) {
            bundle.putString("openid", openId);
        }
        try {
            bundle.putString(Constants.PARAM_PLATFORM_ID, C3121e.m7727a().getSharedPreferences(Constants.PREFERENCE_PF, 0).getString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF));
        } catch (Exception e) {
            e.printStackTrace();
            bundle.putString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF);
        }
        String str3 = str2 + HttpUtils.encodeUrl(bundle);
        C3082f.m7631b("openSDK_LOG.SocialApiIml", "OpenUi, showDialog TDialog");
        if (SocialConstants.ACTION_CHALLENGE.equals(str) || SocialConstants.ACTION_BRAG.equals(str)) {
            C3082f.m7631b("openSDK_LOG.SocialApiIml", "OpenUi, showDialog PKDialog");
            new C3101c(this.f6661a, str, str3, iUiListener, this.f6457c).show();
            return;
        }
        new TDialog(this.f6661a, str, str3, iUiListener, this.f6457c).show();
    }

    /* renamed from: com.tencent.open.SocialApiIml$a */
    /* compiled from: ProGuard */
    private class C3065a implements IUiListener {

        /* renamed from: b */
        private IUiListener f6663b;

        /* renamed from: c */
        private String f6664c;

        /* renamed from: d */
        private String f6665d;

        /* renamed from: e */
        private Bundle f6666e;

        /* renamed from: f */
        private Activity f6667f;

        C3065a(Activity activity, IUiListener iUiListener, String str, String str2, Bundle bundle) {
            this.f6663b = iUiListener;
            this.f6664c = str;
            this.f6665d = str2;
            this.f6666e = bundle;
        }

        public void onComplete(Object obj) {
            String str;
            try {
                str = ((JSONObject) obj).getString(SocialConstants.PARAM_ENCRY_EOKEN);
            } catch (JSONException e) {
                e.printStackTrace();
                C3082f.m7632b("openSDK_LOG.SocialApiIml", "OpenApi, EncrytokenListener() onComplete error", e);
                str = null;
            }
            this.f6666e.putString("encrytoken", str);
            SocialApiIml socialApiIml = SocialApiIml.this;
            socialApiIml.m7560a(socialApiIml.f6661a, this.f6664c, this.f6666e, this.f6665d, this.f6663b);
            if (TextUtils.isEmpty(str)) {
                C3082f.m7631b("openSDK_LOG.SocialApiIml", "The token get from qq or qzone is empty. Write temp token to localstorage.");
                SocialApiIml.this.writeEncryToken(this.f6667f);
            }
        }

        public void onError(UiError uiError) {
            C3082f.m7631b("openSDK_LOG.SocialApiIml", "OpenApi, EncryptTokenListener() onError" + uiError.errorMessage);
            this.f6663b.onError(uiError);
        }

        public void onCancel() {
            this.f6663b.onCancel();
        }
    }

    public void writeEncryToken(Context context) {
        String str;
        String accessToken = this.f6457c.getAccessToken();
        String appId = this.f6457c.getAppId();
        String openId = this.f6457c.getOpenId();
        if (accessToken == null || accessToken.length() <= 0 || appId == null || appId.length() <= 0 || openId == null || openId.length() <= 0) {
            str = null;
        } else {
            str = C3131k.m7793f("tencent&sdk&qazxc***14969%%" + accessToken + appId + openId + "qzone3.4");
        }
        C3109b bVar = new C3109b(context);
        WebSettings settings = bVar.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        String a = C3124g.m7744a().mo35172a(context, "http://qzs.qq.com");
        bVar.loadDataWithBaseURL(a, "<!DOCTYPE HTML><html lang=\"en-US\"><head><meta charset=\"UTF-8\"><title>localStorage Test</title><script type=\"text/javascript\">document.domain = 'qq.com';localStorage[\"" + this.f6457c.getOpenId() + "_" + this.f6457c.getAppId() + "\"]=\"" + str + "\";</script></head><body></body></html>", "text/html", "utf-8", a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Intent mo34805b(String str) {
        Intent intent = new Intent();
        intent.setClassName(Constants.PACKAGE_QZONE, str);
        Intent intent2 = new Intent();
        intent2.setClassName("com.tencent.mobileqq", str);
        Intent intent3 = new Intent();
        intent3.setClassName(Constants.PACKAGE_QQ_PAD, str);
        if (C3131k.m7789d(C3121e.m7727a()) && C3125h.m7752a(C3121e.m7727a(), intent3)) {
            return intent3;
        }
        if (C3125h.m7752a(C3121e.m7727a(), intent2) && C3125h.m7757c(C3121e.m7727a(), "4.7") >= 0) {
            return intent2;
        }
        if (!C3125h.m7752a(C3121e.m7727a(), intent) || C3125h.m7747a(C3125h.m7751a(C3121e.m7727a(), Constants.PACKAGE_QZONE), "4.2") < 0) {
            return null;
        }
        if (C3125h.m7753a(C3121e.m7727a(), intent.getComponent().getPackageName(), Constants.SIGNATRUE_QZONE)) {
            return intent;
        }
        return null;
    }
}
