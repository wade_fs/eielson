package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.bi */
/* compiled from: BUGLY */
public final class C2928bi extends C2944m {

    /* renamed from: i */
    static byte[] f6139i = new byte[1];

    /* renamed from: j */
    static Map<String, String> f6140j = new HashMap();

    /* renamed from: a */
    public byte f6141a = 0;

    /* renamed from: b */
    public int f6142b = 0;

    /* renamed from: c */
    public byte[] f6143c = null;

    /* renamed from: d */
    public String f6144d = "";

    /* renamed from: e */
    public long f6145e = 0;

    /* renamed from: f */
    public String f6146f = "";

    /* renamed from: g */
    public String f6147g = "";

    /* renamed from: h */
    public Map<String, String> f6148h = null;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34660b(this.f6141a, 0);
        lVar.mo34644a(this.f6142b, 1);
        byte[] bArr = this.f6143c;
        if (bArr != null) {
            lVar.mo34653a(bArr, 2);
        }
        String str = this.f6144d;
        if (str != null) {
            lVar.mo34648a(str, 3);
        }
        lVar.mo34645a(this.f6145e, 4);
        String str2 = this.f6146f;
        if (str2 != null) {
            lVar.mo34648a(str2, 5);
        }
        String str3 = this.f6147g;
        if (str3 != null) {
            lVar.mo34648a(str3, 6);
        }
        Map<String, String> map = this.f6148h;
        if (map != null) {
            lVar.mo34650a((Map) map, 7);
        }
    }

    static {
        f6139i[0] = 0;
        f6140j.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6141a = kVar.mo34613a(this.f6141a, 0, true);
        this.f6142b = kVar.mo34616a(this.f6142b, 1, true);
        this.f6143c = kVar.mo34630a(f6139i, 2, false);
        this.f6144d = kVar.mo34621a(3, false);
        this.f6145e = kVar.mo34618a(this.f6145e, 4, false);
        this.f6146f = kVar.mo34621a(5, false);
        this.f6147g = kVar.mo34621a(6, false);
        this.f6148h = (Map) kVar.mo34620a((Object) f6140j, 7, false);
    }
}
