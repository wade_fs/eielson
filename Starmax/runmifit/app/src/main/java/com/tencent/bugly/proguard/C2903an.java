package com.tencent.bugly.proguard;

import android.util.Log;
import java.util.Locale;

/* renamed from: com.tencent.bugly.proguard.an */
/* compiled from: BUGLY */
public class C2903an {

    /* renamed from: a */
    public static String f6029a = "CrashReportInfo";

    /* renamed from: b */
    public static String f6030b = "CrashReport";

    /* renamed from: c */
    public static boolean f6031c = false;

    /* renamed from: f */
    private static String m6866f(String str, Object... objArr) {
        if (str == null) {
            return "null";
        }
        return (objArr == null || objArr.length == 0) ? str : String.format(Locale.US, str, objArr);
    }

    /* renamed from: a */
    private static boolean m6854a(int i, String str, Object... objArr) {
        if (!f6031c) {
            return false;
        }
        String f = m6866f(str, objArr);
        if (i == 0) {
            Log.i(f6030b, f);
            return true;
        } else if (i == 1) {
            Log.d(f6030b, f);
            return true;
        } else if (i == 2) {
            Log.w(f6030b, f);
            return true;
        } else if (i == 3) {
            Log.e(f6030b, f);
            return true;
        } else if (i != 5) {
            return false;
        } else {
            Log.i(f6029a, f);
            return true;
        }
    }

    /* renamed from: a */
    private static boolean m6855a(int i, Throwable th) {
        if (!f6031c) {
            return false;
        }
        return m6854a(i, C2908aq.m6902a(th), new Object[0]);
    }

    /* renamed from: a */
    public static boolean m6857a(String str, Object... objArr) {
        return m6854a(0, str, objArr);
    }

    /* renamed from: a */
    public static boolean m6856a(Class cls, String str, Object... objArr) {
        return m6854a(0, String.format(Locale.US, "[%s] %s", cls.getSimpleName(), str), objArr);
    }

    /* renamed from: b */
    public static boolean m6860b(String str, Object... objArr) {
        return m6854a(5, str, objArr);
    }

    /* renamed from: c */
    public static boolean m6863c(String str, Object... objArr) {
        return m6854a(1, str, objArr);
    }

    /* renamed from: b */
    public static boolean m6859b(Class cls, String str, Object... objArr) {
        return m6854a(1, String.format(Locale.US, "[%s] %s", cls.getSimpleName(), str), objArr);
    }

    /* renamed from: d */
    public static boolean m6864d(String str, Object... objArr) {
        return m6854a(2, str, objArr);
    }

    /* renamed from: a */
    public static boolean m6858a(Throwable th) {
        return m6855a(2, th);
    }

    /* renamed from: e */
    public static boolean m6865e(String str, Object... objArr) {
        return m6854a(3, str, objArr);
    }

    /* renamed from: c */
    public static boolean m6862c(Class cls, String str, Object... objArr) {
        return m6854a(3, String.format(Locale.US, "[%s] %s", cls.getSimpleName(), str), objArr);
    }

    /* renamed from: b */
    public static boolean m6861b(Throwable th) {
        return m6855a(3, th);
    }
}
