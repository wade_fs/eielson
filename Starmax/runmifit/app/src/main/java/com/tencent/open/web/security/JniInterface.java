package com.tencent.open.web.security;

import android.content.Context;
import com.tencent.connect.auth.AuthAgent;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3121e;
import java.io.File;

/* compiled from: ProGuard */
public class JniInterface {
    public static boolean isJniOk = false;

    public static native boolean BackSpaceChar(boolean z, int i);

    public static native boolean clearAllPWD();

    public static native String getPWDKeyToMD5(String str);

    public static native boolean insetTextToArray(int i, String str, int i2);

    public static void loadSo() {
        if (!isJniOk) {
            try {
                Context a = C3121e.m7727a();
                if (a != null) {
                    if (new File(a.getFilesDir().toString() + "/" + AuthAgent.SECURE_LIB_NAME).exists()) {
                        System.load(a.getFilesDir().toString() + "/" + AuthAgent.SECURE_LIB_NAME);
                        isJniOk = true;
                        C3082f.m7634c("openSDK_LOG.JniInterface", "-->load lib success:" + AuthAgent.SECURE_LIB_NAME);
                        return;
                    }
                    C3082f.m7634c("openSDK_LOG.JniInterface", "-->fail, because so is not exists:" + AuthAgent.SECURE_LIB_NAME);
                    return;
                }
                C3082f.m7634c("openSDK_LOG.JniInterface", "-->load lib fail, because context is null:" + AuthAgent.SECURE_LIB_NAME);
            } catch (Throwable th) {
                C3082f.m7632b("openSDK_LOG.JniInterface", "-->load lib error:" + AuthAgent.SECURE_LIB_NAME, th);
            }
        }
    }
}
