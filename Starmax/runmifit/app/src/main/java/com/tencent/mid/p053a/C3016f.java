package com.tencent.mid.p053a;

import android.content.Context;
import com.tencent.mid.api.MidConstants;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.p054b.C3031g;
import com.tencent.mid.util.C3035c;
import com.tencent.mid.util.Util;
import org.json.JSONObject;

/* renamed from: com.tencent.mid.a.f */
public class C3016f extends C3015e {
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int mo34845a() {
        return 2;
    }

    protected C3016f(Context context) {
        super(context);
    }

    /* renamed from: a */
    private String m7364a(String str) {
        return !Util.isEmpty(str) ? str : "-";
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo34847b(JSONObject jSONObject) {
        jSONObject.put("mid", "0");
        jSONObject.put(MidEntity.TAG_IMEI, m7364a(Util.getImei(this.f6520a)));
        jSONObject.put(MidEntity.TAG_IMSI, m7364a(Util.getImsi(this.f6520a)));
        jSONObject.put(MidEntity.TAG_MAC, m7364a(Util.getWifiMacAddress(this.f6520a)));
        jSONObject.put("ts", System.currentTimeMillis() / 1000);
        MidEntity a = C3017g.m7367a(this.f6520a);
        if (a != null && a.isMidValid()) {
            jSONObject.put("mid", a.getMid());
        }
        String b = C3031g.m7437a(this.f6520a).mo34912b();
        if (Util.isMidValid(b)) {
            jSONObject.put(MidConstants.NEW_MID_TAG, b);
        } else {
            jSONObject.put(MidConstants.NEW_MID_TAG, "0");
        }
        try {
            new C3035c(this.f6520a).mo34943a(jSONObject);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
