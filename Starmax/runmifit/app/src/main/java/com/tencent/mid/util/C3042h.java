package com.tencent.mid.util;

import android.app.ActivityManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import java.io.File;
import java.io.FileFilter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Pattern;
import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONObject;

/* renamed from: com.tencent.mid.util.h */
public class C3042h {

    /* renamed from: a */
    private static String f6615a;

    /* renamed from: b */
    private static String f6616b;

    /* renamed from: c */
    private static String f6617c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static C3038d f6618d = Util.getLogger();

    /* renamed from: e */
    private static String f6619e = null;

    /* renamed from: f */
    private static C3043a f6620f = null;

    /* renamed from: g */
    private static C3045b f6621g = null;

    /* renamed from: com.tencent.mid.util.h$b */
    static class C3045b {
    }

    /* renamed from: a */
    public static String m7515a(Context context) {
        String str = f6617c;
        if (str == null || "" == str) {
            f6617c = m7517b(context);
        }
        return f6617c;
    }

    /* renamed from: b */
    public static String m7517b(Context context) {
        try {
            if (Util.checkPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
                while (networkInterfaces.hasMoreElements()) {
                    Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                    while (true) {
                        if (inetAddresses.hasMoreElements()) {
                            InetAddress nextElement = inetAddresses.nextElement();
                            if (!nextElement.isLoopbackAddress()) {
                                String hostAddress = nextElement.getHostAddress();
                                if (InetAddressUtils.isIPv4Address(hostAddress)) {
                                    return hostAddress;
                                }
                            }
                        }
                    }
                }
                return "";
            }
            f6618d.mo34951c("Can not get the permission of android.permission.ACCESS_WIFI_STATE");
            return "";
        } catch (Exception e) {
            f6618d.mo34949b(e);
        }
    }

    /* renamed from: c */
    public static DisplayMetrics m7518c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    /* renamed from: d */
    public static String m7521d(Context context) {
        TelephonyManager telephonyManager;
        try {
            if (!Util.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
                f6618d.mo34954f("Could not get permission of android.permission.READ_PHONE_STATE");
                return null;
            } else if (!m7523f(context) || (telephonyManager = (TelephonyManager) context.getSystemService("phone")) == null) {
                return null;
            } else {
                return telephonyManager.getSimOperator();
            }
        } catch (Throwable th) {
            f6618d.mo34954f(th);
            return null;
        }
    }

    /* renamed from: e */
    public static String m7522e(Context context) {
        try {
            String str = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (str == null) {
                return "";
            }
            return str;
        } catch (Throwable th) {
            f6618d.mo34954f(th);
            return "";
        }
    }

    /* renamed from: f */
    public static boolean m7523f(Context context) {
        return context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0;
    }

    /* renamed from: g */
    public static String m7524g(Context context) {
        try {
            if (!Util.checkPermission(context, "android.permission.INTERNET") || !Util.checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                f6618d.mo34954f("can not get the permission of android.permission.ACCESS_WIFI_STATE");
                return null;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return null;
            }
            if (!activeNetworkInfo.isConnected()) {
                return null;
            }
            String typeName = activeNetworkInfo.getTypeName();
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (typeName == null) {
                return null;
            }
            if (typeName.equalsIgnoreCase("WIFI")) {
                return "WIFI";
            }
            if (typeName.equalsIgnoreCase("MOBILE")) {
                if (extraInfo == null) {
                    return "MOBILE";
                }
            } else if (extraInfo == null) {
                return typeName;
            }
            return extraInfo;
        } catch (Throwable th) {
            f6618d.mo34954f(th);
            return null;
        }
    }

    /* renamed from: h */
    public static Integer m7525h(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return Integer.valueOf(telephonyManager.getNetworkType());
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: i */
    public static int m7526i(Context context) {
        try {
            if (C3046c.m7537a()) {
                return 1;
            }
            return 0;
        } catch (Throwable th) {
            f6618d.mo34954f(th);
            return 0;
        }
    }

    /* renamed from: com.tencent.mid.util.h$c */
    static class C3046c {

        /* renamed from: a */
        private static int f6622a = -1;

        /* renamed from: a */
        public static boolean m7537a() {
            int i = f6622a;
            if (i == 1) {
                return true;
            }
            if (i == 0) {
                return false;
            }
            String[] strArr = {"/bin", "/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
            int i2 = 0;
            while (i2 < strArr.length) {
                try {
                    if (new File(strArr[i2] + "su").exists()) {
                        f6622a = 1;
                        return true;
                    }
                    i2++;
                } catch (Exception unused) {
                }
            }
            f6622a = 0;
            return false;
        }
    }

    /* renamed from: j */
    public static String m7527j(Context context) {
        String path;
        try {
            if (Util.checkPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                String externalStorageState = Environment.getExternalStorageState();
                if (!(externalStorageState == null || !externalStorageState.equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null)) {
                    StatFs statFs = new StatFs(path);
                    return String.valueOf((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1000000) + "/" + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
                }
                return null;
            }
            f6618d.mo34951c("can not get the permission of android.permission.WRITE_EXTERNAL_STORAGE");
            return null;
        } catch (Throwable th) {
            f6618d.mo34954f(th);
        }
    }

    /* renamed from: a */
    public static String m7514a() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return String.valueOf((((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks())) / 1000000) + "/" + String.valueOf(m7516b() / 1000000);
    }

    /* renamed from: b */
    public static long m7516b() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    /* renamed from: k */
    public static String m7528k(Context context) {
        return String.valueOf(m7532o(context) / 1000000) + "/" + String.valueOf(m7520d() / 1000000);
    }

    /* renamed from: o */
    private static long m7532o(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0035 A[SYNTHETIC, Splitter:B:13:0x0035] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003c A[SYNTHETIC, Splitter:B:20:0x003c] */
    /* renamed from: d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long m7520d() {
        /*
            java.lang.String r0 = "/proc/meminfo"
            r1 = 0
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ IOException -> 0x0039, all -> 0x002f }
            r2.<init>(r0)     // Catch:{ IOException -> 0x0039, all -> 0x002f }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0039, all -> 0x002f }
            r3 = 8192(0x2000, float:1.14794E-41)
            r0.<init>(r2, r3)     // Catch:{ IOException -> 0x0039, all -> 0x002f }
            java.lang.String r1 = r0.readLine()     // Catch:{ IOException -> 0x002d, all -> 0x002b }
            java.lang.String r2 = "\\s+"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ IOException -> 0x002d, all -> 0x002b }
            r2 = 1
            r1 = r1[r2]     // Catch:{ IOException -> 0x002d, all -> 0x002b }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ IOException -> 0x002d, all -> 0x002b }
            int r1 = r1.intValue()     // Catch:{ IOException -> 0x002d, all -> 0x002b }
            int r1 = r1 * 1024
            long r1 = (long) r1
            r0.close()     // Catch:{ Exception -> 0x0041 }
            goto L_0x0041
        L_0x002b:
            r1 = move-exception
            goto L_0x0033
        L_0x002d:
            goto L_0x003a
        L_0x002f:
            r0 = move-exception
            r4 = r1
            r1 = r0
            r0 = r4
        L_0x0033:
            if (r0 == 0) goto L_0x0038
            r0.close()     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            throw r1
        L_0x0039:
            r0 = r1
        L_0x003a:
            if (r0 == 0) goto L_0x003f
            r0.close()     // Catch:{ Exception -> 0x003f }
        L_0x003f:
            r1 = 0
        L_0x0041:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.util.C3042h.m7520d():long");
    }

    /* renamed from: l */
    public static synchronized C3043a m7529l(Context context) {
        C3043a aVar;
        synchronized (C3042h.class) {
            if (f6620f == null) {
                f6620f = new C3043a();
            }
            aVar = f6620f;
        }
        return aVar;
    }

    /* renamed from: m */
    public static JSONObject m7530m(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            m7529l(context);
            int b = C3043a.m7534b();
            if (b > 0) {
                jSONObject.put("fx", b / 1000000);
            }
            m7529l(context);
            int c = C3043a.m7535c();
            if (c > 0) {
                jSONObject.put("fn", c / 1000000);
            }
            m7529l(context);
            int a = C3043a.m7533a();
            if (a > 0) {
                jSONObject.put("n", a);
            }
            m7529l(context);
            String d = C3043a.m7536d();
            if (d != null && d.length() == 0) {
                m7529l(context);
                jSONObject.put("na", C3043a.m7536d());
            }
        } catch (Throwable th) {
            f6618d.mo34954f(th);
        }
        return jSONObject;
    }

    /* renamed from: com.tencent.mid.util.h$a */
    static class C3043a {
        C3043a() {
        }

        /* renamed from: com.tencent.mid.util.h$a$a */
        class C3044a implements FileFilter {
            C3044a() {
            }

            public boolean accept(File file) {
                return Pattern.matches("cpu[0-9]", file.getName());
            }
        }

        /* renamed from: a */
        static int m7533a() {
            try {
                return new File("/sys/devices/system/cpu/").listFiles(new C3044a()).length;
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x005b, code lost:
            if (r1 == null) goto L_0x005e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x004b, code lost:
            if (r1 != null) goto L_0x004d;
         */
        /* renamed from: b */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static int m7534b() {
            /*
                r0 = 0
                r1 = 0
                java.lang.String r2 = ""
                java.lang.String r3 = "/system/bin/cat"
                java.lang.String r4 = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"
                java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ Exception -> 0x0053 }
                java.lang.ProcessBuilder r4 = new java.lang.ProcessBuilder     // Catch:{ Exception -> 0x0053 }
                r4.<init>(r3)     // Catch:{ Exception -> 0x0053 }
                java.lang.Process r3 = r4.start()     // Catch:{ Exception -> 0x0053 }
                java.io.InputStream r1 = r3.getInputStream()     // Catch:{ Exception -> 0x0053 }
                r3 = 24
                byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x0053 }
            L_0x001d:
                int r4 = r1.read(r3)     // Catch:{ Exception -> 0x0053 }
                r5 = -1
                if (r4 == r5) goto L_0x0039
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0053 }
                r4.<init>()     // Catch:{ Exception -> 0x0053 }
                r4.append(r2)     // Catch:{ Exception -> 0x0053 }
                java.lang.String r2 = new java.lang.String     // Catch:{ Exception -> 0x0053 }
                r2.<init>(r3)     // Catch:{ Exception -> 0x0053 }
                r4.append(r2)     // Catch:{ Exception -> 0x0053 }
                java.lang.String r2 = r4.toString()     // Catch:{ Exception -> 0x0053 }
                goto L_0x001d
            L_0x0039:
                java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x0053 }
                int r3 = r2.length()     // Catch:{ Exception -> 0x0053 }
                if (r3 <= 0) goto L_0x004b
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x0053 }
                int r0 = r2.intValue()     // Catch:{ Exception -> 0x0053 }
            L_0x004b:
                if (r1 == 0) goto L_0x005e
            L_0x004d:
                r1.close()     // Catch:{ Exception -> 0x005e }
                goto L_0x005e
            L_0x0051:
                r0 = move-exception
                goto L_0x0061
            L_0x0053:
                r2 = move-exception
                com.tencent.mid.util.d r3 = com.tencent.mid.util.C3042h.f6618d     // Catch:{ all -> 0x0051 }
                r3.mo34949b(r2)     // Catch:{ all -> 0x0051 }
                if (r1 == 0) goto L_0x005e
                goto L_0x004d
            L_0x005e:
                int r0 = r0 * 1000
                return r0
            L_0x0061:
                if (r1 == 0) goto L_0x0066
                r1.close()     // Catch:{ Exception -> 0x0066 }
            L_0x0066:
                goto L_0x0068
            L_0x0067:
                throw r0
            L_0x0068:
                goto L_0x0067
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.util.C3042h.C3043a.m7534b():int");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
            r1.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:16:0x005b, code lost:
            if (r1 == null) goto L_0x005e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x004b, code lost:
            if (r1 != null) goto L_0x004d;
         */
        /* renamed from: c */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static int m7535c() {
            /*
                r0 = 0
                r1 = 0
                java.lang.String r2 = ""
                java.lang.String r3 = "/system/bin/cat"
                java.lang.String r4 = "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq"
                java.lang.String[] r3 = new java.lang.String[]{r3, r4}     // Catch:{ IOException -> 0x0053 }
                java.lang.ProcessBuilder r4 = new java.lang.ProcessBuilder     // Catch:{ IOException -> 0x0053 }
                r4.<init>(r3)     // Catch:{ IOException -> 0x0053 }
                java.lang.Process r3 = r4.start()     // Catch:{ IOException -> 0x0053 }
                java.io.InputStream r1 = r3.getInputStream()     // Catch:{ IOException -> 0x0053 }
                r3 = 24
                byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x0053 }
            L_0x001d:
                int r4 = r1.read(r3)     // Catch:{ IOException -> 0x0053 }
                r5 = -1
                if (r4 == r5) goto L_0x0039
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0053 }
                r4.<init>()     // Catch:{ IOException -> 0x0053 }
                r4.append(r2)     // Catch:{ IOException -> 0x0053 }
                java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x0053 }
                r2.<init>(r3)     // Catch:{ IOException -> 0x0053 }
                r4.append(r2)     // Catch:{ IOException -> 0x0053 }
                java.lang.String r2 = r4.toString()     // Catch:{ IOException -> 0x0053 }
                goto L_0x001d
            L_0x0039:
                java.lang.String r2 = r2.trim()     // Catch:{ IOException -> 0x0053 }
                int r3 = r2.length()     // Catch:{ IOException -> 0x0053 }
                if (r3 <= 0) goto L_0x004b
                java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ IOException -> 0x0053 }
                int r0 = r2.intValue()     // Catch:{ IOException -> 0x0053 }
            L_0x004b:
                if (r1 == 0) goto L_0x005e
            L_0x004d:
                r1.close()     // Catch:{ Exception -> 0x005e }
                goto L_0x005e
            L_0x0051:
                r0 = move-exception
                goto L_0x0061
            L_0x0053:
                r2 = move-exception
                com.tencent.mid.util.d r3 = com.tencent.mid.util.C3042h.f6618d     // Catch:{ all -> 0x0051 }
                r3.mo34949b(r2)     // Catch:{ all -> 0x0051 }
                if (r1 == 0) goto L_0x005e
                goto L_0x004d
            L_0x005e:
                int r0 = r0 * 1000
                return r0
            L_0x0061:
                if (r1 == 0) goto L_0x0066
                r1.close()     // Catch:{ Exception -> 0x0066 }
            L_0x0066:
                goto L_0x0068
            L_0x0067:
                throw r0
            L_0x0068:
                goto L_0x0067
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.util.C3042h.C3043a.m7535c():int");
        }

        /* JADX WARNING: Removed duplicated region for block: B:29:? A[RETURN, SYNTHETIC] */
        /* renamed from: d */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static java.lang.String m7536d() {
            /*
                r0 = 0
                java.io.FileReader r1 = new java.io.FileReader     // Catch:{ all -> 0x002e }
                java.lang.String r2 = "/proc/cpuinfo"
                r1.<init>(r2)     // Catch:{ all -> 0x002e }
                java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x002e }
                r2.<init>(r1)     // Catch:{ all -> 0x002e }
                java.lang.String r0 = r2.readLine()     // Catch:{ all -> 0x002c }
                boolean r1 = android.text.TextUtils.isEmpty(r0)     // Catch:{ all -> 0x002c }
                if (r1 != 0) goto L_0x0028
                java.lang.String r1 = ":\\s+"
                r3 = 2
                java.lang.String[] r0 = r0.split(r1, r3)     // Catch:{ all -> 0x002c }
                int r1 = r0.length     // Catch:{ all -> 0x002c }
                if (r1 <= 0) goto L_0x0028
                r1 = 1
                r0 = r0[r1]     // Catch:{ all -> 0x002c }
                r2.close()     // Catch:{ Exception -> 0x0027 }
            L_0x0027:
                return r0
            L_0x0028:
                r2.close()     // Catch:{ Exception -> 0x003b }
                goto L_0x003b
            L_0x002c:
                r0 = move-exception
                goto L_0x0031
            L_0x002e:
                r1 = move-exception
                r2 = r0
                r0 = r1
            L_0x0031:
                com.tencent.mid.util.d r1 = com.tencent.mid.util.C3042h.f6618d     // Catch:{ all -> 0x003e }
                r1.mo34954f(r0)     // Catch:{ all -> 0x003e }
                if (r2 == 0) goto L_0x003b
                goto L_0x0028
            L_0x003b:
                java.lang.String r0 = ""
                return r0
            L_0x003e:
                r0 = move-exception
                if (r2 == 0) goto L_0x0044
                r2.close()     // Catch:{ Exception -> 0x0044 }
            L_0x0044:
                goto L_0x0046
            L_0x0045:
                throw r0
            L_0x0046:
                goto L_0x0045
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.util.C3042h.C3043a.m7536d():java.lang.String");
        }
    }

    /* renamed from: n */
    public static String m7531n(Context context) {
        List<Sensor> sensorList;
        try {
            SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
            if (sensorManager == null || (sensorList = sensorManager.getSensorList(-1)) == null) {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < sensorList.size(); i++) {
                sb.append(sensorList.get(i).getType());
                if (i != sensorList.size() - 1) {
                    sb.append(",");
                }
            }
            return sb.toString();
        } catch (Throwable th) {
            f6618d.mo34954f(th);
            return "";
        }
    }
}
