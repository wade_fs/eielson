package com.tencent.connect.avatar;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.Toast;
import com.baidu.mobstat.Config;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.connect.common.UIListenerManager;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3131k;
import com.tencent.tauth.IUiListener;

/* compiled from: ProGuard */
public class QQAvatar extends BaseApi {
    public static final String FROM_SDK_AVATAR_SET_IMAGE = "FROM_SDK_AVATAR_SET_IMAGE";

    /* renamed from: a */
    private IUiListener f6430a;

    public QQAvatar(QQToken qQToken) {
        super(qQToken);
    }

    /* renamed from: a */
    private Intent m7288a(Activity activity) {
        Intent intent = new Intent();
        intent.setClass(activity, ImageActivity.class);
        return intent;
    }

    public void setAvatarByQQ(Activity activity, Uri uri, IUiListener iUiListener) {
        IUiListener iUiListener2 = this.f6430a;
        if (iUiListener2 != null) {
            iUiListener2.onCancel();
        }
        this.f6430a = iUiListener;
        if (!C3125h.m7756b(activity)) {
            Toast.makeText(activity.getApplicationContext(), "当前手机未安装QQ，请安装最新版QQ后再试。", 1).show();
        } else if (C3125h.m7757c(activity, "8.0.0") < 0) {
            Toast.makeText(activity.getApplicationContext(), "当前手机QQ版本过低，不支持设置头像功能。", 1).show();
        } else {
            String a = C3131k.m7770a(activity);
            StringBuffer stringBuffer = new StringBuffer("mqqapi://profile/sdk_avatar_edit?");
            if (!TextUtils.isEmpty(a)) {
                if (a.length() > 20) {
                    a = a.substring(0, 20) + "...";
                }
                stringBuffer.append("&app_name=" + Base64.encodeToString(C3131k.m7799i(a), 2));
            }
            String appId = this.f6457c.getAppId();
            String openId = this.f6457c.getOpenId();
            if (!TextUtils.isEmpty(appId)) {
                stringBuffer.append("&share_id=" + appId);
            }
            if (!TextUtils.isEmpty(openId)) {
                stringBuffer.append("&open_id=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
            }
            String a2 = C3131k.m7771a(activity, uri);
            if (!TextUtils.isEmpty(a2)) {
                stringBuffer.append("&set_path=" + Base64.encodeToString(C3131k.m7799i(a2), 2));
            }
            stringBuffer.append("&sdk_version=" + Base64.encodeToString(C3131k.m7799i(Constants.SDK_VERSION), 2));
            C3082f.m7628a("QQAVATAR", "-->set avatar, url: " + stringBuffer.toString());
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra("FROM_WHERE", FROM_SDK_AVATAR_SET_IMAGE);
            intent.putExtra("pkg_name", activity.getPackageName());
            intent.setData(Uri.parse(stringBuffer.toString()));
            if (mo34804a(intent)) {
                UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_EDIT_AVATAR, iUiListener);
                mo34800a(activity, Constants.REQUEST_EDIT_AVATAR, intent, false);
            }
        }
    }

    public void setDynamicAvatar(Activity activity, Uri uri, IUiListener iUiListener) {
        IUiListener iUiListener2 = this.f6430a;
        if (iUiListener2 != null) {
            iUiListener2.onCancel();
        }
        this.f6430a = iUiListener;
        if (!C3125h.m7756b(activity)) {
            Toast.makeText(activity.getApplicationContext(), "当前手机未安装QQ，请安装最新版QQ后再试。", 1).show();
        } else if (C3125h.m7757c(activity, "8.0.5") < 0) {
            Toast.makeText(activity.getApplicationContext(), "当前手机QQ版本过低，不支持设置头像功能。", 1).show();
        } else {
            String a = C3131k.m7770a(activity);
            StringBuffer stringBuffer = new StringBuffer("mqqapi://profile/sdk_dynamic_avatar_edit?");
            if (!TextUtils.isEmpty(a)) {
                if (a.length() > 20) {
                    a = a.substring(0, 20) + "...";
                }
                stringBuffer.append("&app_name=" + Base64.encodeToString(C3131k.m7799i(a), 2));
            }
            String appId = this.f6457c.getAppId();
            String openId = this.f6457c.getOpenId();
            if (!TextUtils.isEmpty(appId)) {
                stringBuffer.append("&share_id=" + appId);
            }
            if (!TextUtils.isEmpty(openId)) {
                stringBuffer.append("&open_id=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
            }
            String a2 = C3131k.m7771a(activity, uri);
            if (!TextUtils.isEmpty(a2)) {
                stringBuffer.append("&video_path=" + Base64.encodeToString(C3131k.m7799i(a2), 2));
            }
            stringBuffer.append("&sdk_version=" + Base64.encodeToString(C3131k.m7799i(Constants.SDK_VERSION), 2));
            C3082f.m7628a("QQAVATAR", "-->set dynamic avatar, url: " + stringBuffer.toString());
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.putExtra("FROM_WHERE", FROM_SDK_AVATAR_SET_IMAGE);
            intent.putExtra("pkg_name", activity.getPackageName());
            intent.setData(Uri.parse(stringBuffer.toString()));
            if (mo34804a(intent)) {
                UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_EDIT_DYNAMIC_AVATAR, iUiListener);
                mo34800a(activity, Constants.REQUEST_EDIT_DYNAMIC_AVATAR, intent, false);
            }
        }
    }

    public void setAvatar(Activity activity, Uri uri, IUiListener iUiListener, int i) {
        IUiListener iUiListener2 = this.f6430a;
        if (iUiListener2 != null) {
            iUiListener2.onCancel();
        }
        this.f6430a = iUiListener;
        Bundle bundle = new Bundle();
        bundle.putString("picture", uri.toString());
        bundle.putInt("exitAnim", i);
        bundle.putString("appid", this.f6457c.getAppId());
        bundle.putString(Constants.PARAM_ACCESS_TOKEN, this.f6457c.getAccessToken());
        bundle.putLong(Constants.PARAM_EXPIRES_IN, this.f6457c.getExpireTimeInSecond());
        bundle.putString("openid", this.f6457c.getOpenId());
        Intent a = m7288a(activity);
        if (mo34804a(a)) {
            m7289a(activity, bundle, a);
            C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_SET_AVATAR, Constants.VIA_REPORT_TYPE_SET_AVATAR, "18", "0");
            return;
        }
        C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_SET_AVATAR, Constants.VIA_REPORT_TYPE_SET_AVATAR, "18", "1");
    }

    /* renamed from: a */
    private void m7289a(Activity activity, Bundle bundle, Intent intent) {
        m7290a(bundle);
        intent.putExtra(Constants.KEY_ACTION, "action_avatar");
        intent.putExtra(Constants.KEY_PARAMS, bundle);
        UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_AVATER, this.f6430a);
        mo34801a(activity, intent, (int) Constants.REQUEST_AVATER);
    }

    /* renamed from: a */
    private void m7290a(Bundle bundle) {
        if (this.f6457c != null) {
            bundle.putString("appid", this.f6457c.getAppId());
            if (this.f6457c.isSessionValid()) {
                bundle.putString(Constants.PARAM_KEY_STR, this.f6457c.getAccessToken());
                bundle.putString(Constants.PARAM_KEY_TYPE, "0x80");
            }
            String openId = this.f6457c.getOpenId();
            if (openId != null) {
                bundle.putString("hopenid", openId);
            }
            bundle.putString("platform", "androidqz");
            try {
                bundle.putString(Constants.PARAM_PLATFORM_ID, C3121e.m7727a().getSharedPreferences(Constants.PREFERENCE_PF, 0).getString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF));
            } catch (Exception e) {
                e.printStackTrace();
                bundle.putString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF);
            }
        }
        bundle.putString("sdkv", Constants.SDK_VERSION);
        bundle.putString("sdkp", Config.APP_VERSION_CODE);
    }
}
