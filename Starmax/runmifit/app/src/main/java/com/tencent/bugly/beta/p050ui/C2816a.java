package com.tencent.bugly.beta.p050ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2806c;
import com.tencent.bugly.beta.global.ResBean;
import com.tencent.bugly.beta.utils.C2836e;

/* renamed from: com.tencent.bugly.beta.ui.a */
/* compiled from: BUGLY */
public abstract class C2816a extends C2820b {

    /* renamed from: a */
    protected Context f5397a;

    /* renamed from: b */
    protected View f5398b;

    /* renamed from: c */
    protected FrameLayout f5399c;

    /* renamed from: d */
    protected LinearLayout f5400d;

    /* renamed from: e */
    protected ImageView f5401e;

    /* renamed from: f */
    protected TextView f5402f;

    /* renamed from: g */
    protected TextView f5403g;

    /* renamed from: h */
    protected TextView f5404h;

    /* renamed from: i */
    protected LinearLayout f5405i;

    /* renamed from: j */
    protected ResBean f5406j;

    /* renamed from: k */
    protected int f5407k;

    /* renamed from: l */
    protected int f5408l;

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f5397a = getActivity();
        this.f5406j = ResBean.f5325a;
        int i = this.f5408l;
        if (i == 0) {
            this.f5398b = new RelativeLayout(this.f5397a);
            ((RelativeLayout) this.f5398b).setGravity(17);
            this.f5398b.setBackgroundColor(Color.argb(100, 0, 0, 0));
            this.f5399c = new FrameLayout(this.f5397a);
            this.f5399c.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
            this.f5400d = new LinearLayout(this.f5397a);
            this.f5400d.setBackgroundColor(-1);
            ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -2);
            this.f5400d.setGravity(17);
            this.f5400d.setLayoutParams(layoutParams);
            this.f5400d.setMinimumWidth(C2804a.m6257a(this.f5397a, 280.0f));
            this.f5400d.setOrientation(1);
            if (this.f5407k == 2) {
                float a = (float) C2804a.m6257a(this.f5397a, 6.0f);
                ShapeDrawable shapeDrawable = new ShapeDrawable(new RoundRectShape(new float[]{a, a, a, a, a, a, a, a}, null, null));
                shapeDrawable.getPaint().setColor(-1);
                shapeDrawable.getPaint().setStyle(Paint.Style.FILL_AND_STROKE);
                this.f5400d.setBackgroundDrawable(shapeDrawable);
            }
            this.f5402f = new TextView(this.f5397a);
            this.f5402f.setGravity(16);
            this.f5402f.setSingleLine();
            TextView textView = this.f5402f;
            this.f5406j.getClass();
            textView.setTextColor(Color.parseColor("#273238"));
            this.f5402f.setTextSize(18.0f);
            this.f5402f.setLayoutParams(layoutParams);
            this.f5402f.setOnClickListener(null);
            this.f5402f.setEllipsize(TextUtils.TruncateAt.END);
            int a2 = C2804a.m6257a(this.f5397a, 16.0f);
            this.f5402f.setPadding(a2, 0, a2, 0);
            this.f5402f.setTypeface(null, 1);
            this.f5402f.setHeight(C2804a.m6257a(this.f5397a, 42.0f));
            this.f5402f.setTag(Beta.TAG_TITLE);
            TextView textView2 = new TextView(this.f5397a);
            textView2.setBackgroundColor(-3355444);
            textView2.setHeight(1);
            ScrollView scrollView = new ScrollView(this.f5397a);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
            layoutParams2.setMargins(0, 0, 0, C2804a.m6257a(this.f5397a, 52.0f));
            scrollView.setLayoutParams(layoutParams2);
            scrollView.setFillViewport(true);
            scrollView.setVerticalScrollBarEnabled(false);
            scrollView.setHorizontalScrollBarEnabled(false);
            this.f5405i = new LinearLayout(this.f5397a);
            this.f5405i.setLayoutParams(new LinearLayout.LayoutParams(-1, -2));
            this.f5405i.setOrientation(1);
            this.f5405i.setPadding(a2, C2804a.m6257a(this.f5397a, 10.0f), a2, 0);
            LinearLayout linearLayout = new LinearLayout(this.f5397a);
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setGravity(17);
            linearLayout.setOrientation(0);
            int i2 = a2 / 2;
            linearLayout.setPadding(i2, a2, i2, a2);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -2);
            layoutParams3.gravity = 80;
            linearLayout.setLayoutParams(layoutParams3);
            LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(0, -2, 1.0f);
            layoutParams4.setMargins(i2, 0, i2, 0);
            this.f5403g = new TextView(this.f5397a);
            this.f5403g.setSingleLine();
            this.f5403g.setGravity(17);
            this.f5403g.setTag(Beta.TAG_CANCEL_BUTTON);
            new RelativeLayout.LayoutParams(-2, -2);
            int a3 = C2804a.m6257a(this.f5397a, 30.0f);
            if (this.f5407k == 2) {
                FrameLayout.LayoutParams layoutParams5 = new FrameLayout.LayoutParams(a3, a3);
                layoutParams5.gravity = 53;
                this.f5403g.setLayoutParams(layoutParams5);
                TextView textView3 = this.f5403g;
                double d = (double) a3;
                Double.isNaN(d);
                textView3.setTextSize((float) (d * 0.3d));
            } else {
                this.f5403g.setLayoutParams(layoutParams4);
                this.f5403g.setTextSize((float) 16);
                TextView textView4 = this.f5403g;
                this.f5406j.getClass();
                textView4.setTextColor(Color.parseColor("#757575"));
                this.f5403g.setPadding(C2804a.m6257a(this.f5397a, 10.0f), C2804a.m6257a(this.f5397a, 5.0f), C2804a.m6257a(this.f5397a, 10.0f), C2804a.m6257a(this.f5397a, 5.0f));
            }
            this.f5404h = new TextView(this.f5397a);
            this.f5404h.setLayoutParams(layoutParams4);
            this.f5404h.setGravity(17);
            this.f5404h.setTextSize((float) 16);
            TextView textView5 = this.f5404h;
            this.f5406j.getClass();
            textView5.setTextColor(Color.parseColor("#273238"));
            this.f5404h.setSingleLine();
            this.f5404h.setPadding(C2804a.m6257a(this.f5397a, 10.0f), C2804a.m6257a(this.f5397a, 5.0f), C2804a.m6257a(this.f5397a, 10.0f), C2804a.m6257a(this.f5397a, 5.0f));
            this.f5404h.setTypeface(null, 1);
            this.f5404h.setTag(Beta.TAG_CONFIRM_BUTTON);
            int a4 = C2804a.m6257a(this.f5397a, 40.0f);
            scrollView.addView(this.f5405i);
            if (this.f5407k == 2) {
                FrameLayout frameLayout = new FrameLayout(this.f5397a);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-2, -2));
                int i3 = a3 / 2;
                int i4 = i3 - 5;
                this.f5399c.setPadding(i3, i4, i4, i3);
                frameLayout.addView(this.f5399c);
                frameLayout.addView(this.f5403g);
                ((RelativeLayout) this.f5398b).addView(frameLayout);
            } else {
                this.f5398b.setPadding(a4, a4, a4, a4);
                ((RelativeLayout) this.f5398b).addView(this.f5399c);
                linearLayout.addView(this.f5403g);
            }
            this.f5400d.addView(this.f5402f);
            this.f5400d.addView(textView2);
            this.f5400d.addView(scrollView);
            this.f5399c.addView(this.f5400d);
            linearLayout.addView(this.f5404h);
            this.f5399c.addView(linearLayout);
            if (this.f5407k == 2) {
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                paint.setAntiAlias(true);
                Bitmap createBitmap = Bitmap.createBitmap(a3, a3, Bitmap.Config.ARGB_8888);
                int i5 = a3 / 2;
                Canvas canvas = new Canvas(createBitmap);
                paint.setColor(-3355444);
                float f = (float) i5;
                canvas.drawCircle(f, f, f, paint);
                canvas.rotate(45.0f, f, f);
                paint.setColor(-7829368);
                int a5 = C2804a.m6257a(this.f5397a, 0.8f);
                float f2 = f * 0.4f;
                float f3 = (float) (i5 - a5);
                float f4 = f * 1.6f;
                float f5 = (float) (i5 + a5);
                Canvas canvas2 = canvas;
                Paint paint2 = paint;
                canvas2.drawRect(f2, f3, f4, f5, paint2);
                canvas2.drawRect(f3, f2, f5, f4, paint2);
                canvas.rotate(-45.0f);
                Bitmap createBitmap2 = Bitmap.createBitmap(a3, a3, Bitmap.Config.ARGB_8888);
                Canvas canvas3 = new Canvas(createBitmap2);
                paint.setColor(-7829368);
                canvas3.drawCircle(f, f, f, paint);
                canvas3.rotate(45.0f, f, f);
                paint.setColor(-3355444);
                Canvas canvas4 = canvas3;
                canvas4.drawRect(f2, f3, f4, f5, paint2);
                canvas4.drawRect(f3, f2, f5, f4, paint2);
                canvas3.rotate(-45.0f);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
                BitmapDrawable bitmapDrawable2 = new BitmapDrawable(createBitmap2);
                this.f5403g.setBackgroundDrawable(bitmapDrawable);
                this.f5403g.setOnTouchListener(new C2806c(1, bitmapDrawable2, bitmapDrawable));
            }
            this.f5398b.setOnClickListener(null);
            AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
            alphaAnimation.setDuration(300);
            this.f5398b.startAnimation(alphaAnimation);
        } else {
            this.f5398b = layoutInflater.inflate(i, (ViewGroup) null);
            this.f5401e = (ImageView) this.f5398b.findViewWithTag(Beta.TAG_IMG_BANNER);
            this.f5402f = (TextView) this.f5398b.findViewWithTag(Beta.TAG_TITLE);
            this.f5403g = (TextView) this.f5398b.findViewWithTag(Beta.TAG_CANCEL_BUTTON);
            this.f5404h = (TextView) this.f5398b.findViewWithTag(Beta.TAG_CONFIRM_BUTTON);
        }
        this.f5403g.setVisibility(View.GONE);
        this.f5404h.setVisibility(View.GONE);
        this.f5403g.setFocusable(true);
        this.f5404h.setFocusable(true);
        this.f5404h.requestFocus();
        return this.f5398b;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f5397a = null;
        this.f5398b = null;
        this.f5399c = null;
        this.f5400d = null;
        this.f5402f = null;
        this.f5401e = null;
        this.f5403g = null;
        this.f5404h = null;
        this.f5405i = null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34151a(String str, View.OnClickListener onClickListener, String str2, View.OnClickListener onClickListener2) {
        final String str3 = str;
        final View.OnClickListener onClickListener3 = onClickListener;
        final String str4 = str2;
        final View.OnClickListener onClickListener4 = onClickListener2;
        C2836e.m6398a(new Runnable() {
            /* class com.tencent.bugly.beta.p050ui.C2816a.C28171 */

            public void run() {
                if (C2816a.this.f5403g != null && C2816a.this.f5404h != null) {
                    if (str3 != null) {
                        C2816a.this.f5403g.setVisibility(View.VISIBLE);
                        if (C2816a.this.f5407k != 2) {
                            C2816a.this.f5403g.setText(str3);
                            if (C2816a.this.f5408l == 0) {
                                C2816a.this.f5403g.getViewTreeObserver().addOnPreDrawListener(new C2822d(2, Integer.valueOf(C2816a.this.f5407k), C2816a.this.f5403g, 1));
                            }
                        }
                        C2816a.this.f5403g.setOnClickListener(onClickListener3);
                    }
                    if (str4 != null) {
                        C2816a.this.f5404h.setVisibility(View.VISIBLE);
                        C2816a.this.f5404h.setText(str4);
                        C2816a.this.f5404h.setOnClickListener(onClickListener4);
                        if (C2816a.this.f5408l == 0) {
                            C2816a.this.f5404h.getViewTreeObserver().addOnPreDrawListener(new C2822d(2, Integer.valueOf(C2816a.this.f5407k), C2816a.this.f5404h, 1));
                        }
                        C2816a.this.f5404h.requestFocus();
                    }
                }
            }
        });
    }

    /* renamed from: a */
    public void mo34150a() {
        if (this.f5398b == null) {
            super.mo34150a();
            return;
        }
        final AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(200);
        C2836e.m6398a(new Runnable() {
            /* class com.tencent.bugly.beta.p050ui.C2816a.C28182 */

            public void run() {
                if (C2816a.this.f5398b != null) {
                    C2816a.this.f5398b.startAnimation(alphaAnimation);
                }
            }
        });
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            /* class com.tencent.bugly.beta.p050ui.C2816a.C28193 */

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                if (C2816a.this.f5398b != null) {
                    C2816a.this.f5398b.setVisibility(View.GONE);
                }
                C2816a.super.mo34150a();
            }
        });
    }
}
