package com.tencent.stat.hybrid;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.tencent.mid.util.Util;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;
import java.net.URLDecoder;
import org.json.JSONObject;

public class StatHybridHandler {
    public static final int HYBRID_VERSION = 1;
    public static final String MTA_HYBRID_UA_FLAG = " TencentMTA/1";

    /* renamed from: a */
    private static StatLogger f7398a = StatCommonHelper.getLogger();

    /* renamed from: b */
    private static StatSpecifyReportedInfo f7399b = null;

    /* renamed from: c */
    private static Context f7400c = null;

    /* renamed from: d */
    private static StatHybridBridge f7401d = new StatHybridBridge();

    public static void init(Context context) {
        f7399b = new StatSpecifyReportedInfo();
        f7399b.setAppKey(StatConfig.getAppKey(context));
        f7399b.setInstallChannel(StatConfig.getInstallChannel(context));
        f7399b.setFromH5(1);
    }

    public static void init(Context context, String str, String str2) {
        f7399b = new StatSpecifyReportedInfo();
        f7399b.setAppKey(str);
        f7399b.setInstallChannel(str2);
        f7399b.setFromH5(1);
    }

    public static StatSpecifyReportedInfo getH5reportInfo() {
        return f7399b;
    }

    public static Context getContext() {
        return f7400c;
    }

    public static void setH5reportInfo(StatSpecifyReportedInfo statSpecifyReportedInfo) {
        f7399b = statSpecifyReportedInfo;
    }

    public static void initWebSettings(WebSettings webSettings) {
        if (webSettings != null) {
            String userAgentString = webSettings.getUserAgentString();
            StatLogger statLogger = f7398a;
            statLogger.mo35381d("org ua:" + userAgentString);
            if (!TextUtils.isEmpty(userAgentString) && !userAgentString.contains(MTA_HYBRID_UA_FLAG)) {
                webSettings.setUserAgentString(userAgentString + MTA_HYBRID_UA_FLAG);
                StatLogger statLogger2 = f7398a;
                statLogger2.mo35381d("new ua:" + webSettings.getUserAgentString());
            }
        }
    }

    public static boolean handleWebViewUrl(WebView webView, String str) {
        try {
            String decode = URLDecoder.decode(str, "UTF-8");
            if (webView == null || Util.isEmpty(decode) || !str.toLowerCase().startsWith("tencentMtaHyb:".toLowerCase())) {
                return false;
            }
            if (f7400c == null) {
                f7400c = webView.getContext().getApplicationContext();
            }
            StatLogger statLogger = f7398a;
            statLogger.mo35381d("decodedURL:" + decode);
            m8104a(webView, decode.substring(14));
            return true;
        } catch (Throwable th) {
            f7398a.mo35384e(th);
            return false;
        }
    }

    /* renamed from: a */
    private static void m8104a(WebView webView, String str) throws Exception {
        JSONObject jSONObject = new JSONObject(str);
        String string = jSONObject.getString("methodName");
        JSONObject jSONObject2 = jSONObject.getJSONObject("args");
        StatLogger statLogger = f7398a;
        statLogger.mo35381d("invoke method:" + string + ",args:" + jSONObject2);
        f7401d.getClass().getMethod(string, JSONObject.class).invoke(f7401d, jSONObject2);
    }
}
