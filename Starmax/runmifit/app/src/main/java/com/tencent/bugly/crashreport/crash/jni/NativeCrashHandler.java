package com.tencent.bugly.crashreport.crash.jni;

import android.content.Context;
import com.tamic.novate.util.FileUtil;
import com.tencent.bugly.crashreport.C2839a;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.crashreport.crash.C2867b;
import com.tencent.bugly.crashreport.crash.C2869c;
import com.tencent.bugly.crashreport.crash.CrashDetailBean;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.io.File;

/* compiled from: BUGLY */
public class NativeCrashHandler implements C2839a {

    /* renamed from: a */
    private static NativeCrashHandler f5895a = null;

    /* renamed from: l */
    private static boolean f5896l = false;

    /* renamed from: m */
    private static boolean f5897m = false;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public static boolean f5898o = true;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public final Context f5899b;

    /* renamed from: c */
    private final C2851a f5900c;

    /* renamed from: d */
    private final C2901am f5901d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public NativeExceptionHandler f5902e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f5903f;

    /* renamed from: g */
    private final boolean f5904g;

    /* renamed from: h */
    private boolean f5905h = false;

    /* renamed from: i */
    private boolean f5906i = false;

    /* renamed from: j */
    private boolean f5907j = false;

    /* renamed from: k */
    private boolean f5908k = false;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public C2867b f5909n;

    /* access modifiers changed from: protected */
    public native boolean appendNativeLog(String str, String str2, String str3);

    /* access modifiers changed from: protected */
    public native boolean appendWholeNativeLog(String str);

    /* access modifiers changed from: protected */
    public native String getNativeKeyValueList();

    /* access modifiers changed from: protected */
    public native String getNativeLog();

    /* access modifiers changed from: protected */
    public native boolean putNativeKeyValue(String str, String str2);

    /* access modifiers changed from: protected */
    public native String regist(String str, boolean z, int i);

    /* access modifiers changed from: protected */
    public native String removeNativeKeyValue(String str);

    /* access modifiers changed from: protected */
    public native void setNativeInfo(int i, String str);

    /* access modifiers changed from: protected */
    public native void testCrash();

    /* access modifiers changed from: protected */
    public native String unregist();

    /* renamed from: a */
    private static void m6706a(String str) {
        C2903an.m6863c("[Native] Check extra jni for Bugly NDK v%s", str);
        String replace = "2.1.1".replace(FileUtil.HIDDEN_PREFIX, "");
        String replace2 = "2.3.0".replace(FileUtil.HIDDEN_PREFIX, "");
        String replace3 = str.replace(FileUtil.HIDDEN_PREFIX, "");
        if (replace3.length() == 2) {
            replace3 = replace3 + "0";
        } else if (replace3.length() == 1) {
            replace3 = replace3 + "00";
        }
        try {
            if (Integer.parseInt(replace3) >= Integer.parseInt(replace)) {
                f5896l = true;
            }
            if (Integer.parseInt(replace3) >= Integer.parseInt(replace2)) {
                f5897m = true;
            }
        } catch (Throwable unused) {
        }
        if (f5897m) {
            C2903an.m6857a("[Native] Info setting jni can be accessed.", new Object[0]);
        } else {
            C2903an.m6864d("[Native] Info setting jni can not be accessed.", new Object[0]);
        }
        if (f5896l) {
            C2903an.m6857a("[Native] Extra jni can be accessed.", new Object[0]);
        } else {
            C2903an.m6864d("[Native] Extra jni can not be accessed.", new Object[0]);
        }
    }

    protected NativeCrashHandler(Context context, C2851a aVar, C2867b bVar, C2854a aVar2, C2901am amVar, boolean z, String str) {
        this.f5899b = C2908aq.m6891a(context);
        try {
            if (C2908aq.m6915a(str)) {
                str = context.getDir("bugly", 0).getAbsolutePath();
            }
        } catch (Throwable unused) {
            str = "/data/data/" + C2851a.m6470a(context).f5659d + "/app_bugly";
        }
        this.f5909n = bVar;
        this.f5903f = str;
        this.f5900c = aVar;
        this.f5901d = amVar;
        this.f5904g = z;
        this.f5902e = new C2880a(context, aVar, bVar, C2854a.m6574a());
    }

    public static synchronized NativeCrashHandler getInstance(Context context, C2851a aVar, C2867b bVar, C2854a aVar2, C2901am amVar, boolean z, String str) {
        NativeCrashHandler nativeCrashHandler;
        synchronized (NativeCrashHandler.class) {
            if (f5895a == null) {
                f5895a = new NativeCrashHandler(context, aVar, bVar, aVar2, amVar, z, str);
            }
            nativeCrashHandler = f5895a;
        }
        return nativeCrashHandler;
    }

    public static synchronized NativeCrashHandler getInstance() {
        NativeCrashHandler nativeCrashHandler;
        synchronized (NativeCrashHandler.class) {
            nativeCrashHandler = f5895a;
        }
        return nativeCrashHandler;
    }

    public synchronized String getDumpFilePath() {
        return this.f5903f;
    }

    public synchronized void setDumpFilePath(String str) {
        this.f5903f = str;
    }

    public static void setShouldHandleInJava(boolean z) {
        f5898o = z;
        NativeCrashHandler nativeCrashHandler = f5895a;
        if (nativeCrashHandler != null) {
            nativeCrashHandler.m6707a(999, "" + z);
        }
    }

    public static boolean isShouldHandleInJava() {
        return f5898o;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:7|8|9|(3:11|12|(3:14|15|16))(2:21|(7:23|24|25|(1:27)(1:28)|29|(1:31)|(7:33|(1:35)|36|(1:38)|39|40|41)))|42|43|44|45) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0110 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34440a(boolean r11) {
        /*
            r10 = this;
            monitor-enter(r10)
            boolean r0 = r10.f5907j     // Catch:{ all -> 0x0116 }
            r1 = 0
            if (r0 == 0) goto L_0x000f
            java.lang.String r11 = "[Native] Native crash report has already registered."
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x0116 }
            com.tencent.bugly.proguard.C2903an.m6864d(r11, r0)     // Catch:{ all -> 0x0116 }
            monitor-exit(r10)
            return
        L_0x000f:
            boolean r0 = r10.f5906i     // Catch:{ all -> 0x0116 }
            r2 = 1
            if (r0 == 0) goto L_0x003c
            java.lang.String r0 = r10.f5903f     // Catch:{ all -> 0x0033 }
            java.lang.String r11 = r10.regist(r0, r11, r2)     // Catch:{ all -> 0x0033 }
            if (r11 == 0) goto L_0x0110
            java.lang.String r0 = "[Native] Native Crash Report enable."
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x0033 }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r3)     // Catch:{ all -> 0x0033 }
            m6706a(r11)     // Catch:{ all -> 0x0033 }
            com.tencent.bugly.crashreport.common.info.a r0 = r10.f5900c     // Catch:{ all -> 0x0033 }
            r0.f5675t = r11     // Catch:{ all -> 0x0033 }
            boolean r11 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f5896l     // Catch:{ all -> 0x0033 }
            com.tencent.bugly.proguard.C2905ap.m6876a(r11)     // Catch:{ all -> 0x0033 }
            r10.f5907j = r2     // Catch:{ all -> 0x0033 }
            monitor-exit(r10)
            return
        L_0x0033:
            java.lang.String r11 = "[Native] Failed to load Bugly SO file."
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x0116 }
            com.tencent.bugly.proguard.C2903an.m6863c(r11, r0)     // Catch:{ all -> 0x0116 }
            goto L_0x0110
        L_0x003c:
            boolean r0 = r10.f5905h     // Catch:{ all -> 0x0116 }
            if (r0 == 0) goto L_0x0110
            java.lang.String r0 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r3 = "registNativeExceptionHandler2"
            r4 = 4
            java.lang.Class[] r5 = new java.lang.Class[r4]     // Catch:{ all -> 0x0110 }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r5[r1] = r6     // Catch:{ all -> 0x0110 }
            java.lang.Class<java.lang.String> r6 = java.lang.String.class
            r5[r2] = r6     // Catch:{ all -> 0x0110 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0110 }
            r7 = 2
            r5[r7] = r6     // Catch:{ all -> 0x0110 }
            java.lang.Class r6 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0110 }
            r8 = 3
            r5[r8] = r6     // Catch:{ all -> 0x0110 }
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x0110 }
            java.lang.String r6 = r10.f5903f     // Catch:{ all -> 0x0110 }
            r4[r1] = r6     // Catch:{ all -> 0x0110 }
            android.content.Context r6 = r10.f5899b     // Catch:{ all -> 0x0110 }
            java.lang.String r6 = com.tencent.bugly.crashreport.common.info.C2852b.m6535a(r6, r1)     // Catch:{ all -> 0x0110 }
            r4[r2] = r6     // Catch:{ all -> 0x0110 }
            r6 = 5
            if (r11 == 0) goto L_0x006c
            r9 = 1
            goto L_0x006d
        L_0x006c:
            r9 = 5
        L_0x006d:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0110 }
            r4[r7] = r9     // Catch:{ all -> 0x0110 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0110 }
            r4[r8] = r9     // Catch:{ all -> 0x0110 }
            r9 = 0
            java.lang.Object r0 = com.tencent.bugly.proguard.C2908aq.m6895a(r0, r3, r9, r5, r4)     // Catch:{ all -> 0x0110 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0110 }
            if (r0 != 0) goto L_0x00b6
            java.lang.String r0 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r3 = "registNativeExceptionHandler"
            java.lang.Class[] r4 = new java.lang.Class[r8]     // Catch:{ all -> 0x0110 }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r4[r1] = r5     // Catch:{ all -> 0x0110 }
            java.lang.Class<java.lang.String> r5 = java.lang.String.class
            r4[r2] = r5     // Catch:{ all -> 0x0110 }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0110 }
            r4[r7] = r5     // Catch:{ all -> 0x0110 }
            java.lang.Object[] r5 = new java.lang.Object[r8]     // Catch:{ all -> 0x0110 }
            java.lang.String r8 = r10.f5903f     // Catch:{ all -> 0x0110 }
            r5[r1] = r8     // Catch:{ all -> 0x0110 }
            android.content.Context r8 = r10.f5899b     // Catch:{ all -> 0x0110 }
            java.lang.String r8 = com.tencent.bugly.crashreport.common.info.C2852b.m6535a(r8, r1)     // Catch:{ all -> 0x0110 }
            r5[r2] = r8     // Catch:{ all -> 0x0110 }
            com.tencent.bugly.crashreport.common.info.a r8 = com.tencent.bugly.crashreport.common.info.C2851a.m6471b()     // Catch:{ all -> 0x0110 }
            int r8 = r8.mo34281K()     // Catch:{ all -> 0x0110 }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x0110 }
            r5[r7] = r8     // Catch:{ all -> 0x0110 }
            java.lang.Object r0 = com.tencent.bugly.proguard.C2908aq.m6895a(r0, r3, r9, r4, r5)     // Catch:{ all -> 0x0110 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x0110 }
        L_0x00b6:
            if (r0 == 0) goto L_0x0110
            r10.f5907j = r2     // Catch:{ all -> 0x0110 }
            com.tencent.bugly.crashreport.common.info.a r3 = com.tencent.bugly.crashreport.common.info.C2851a.m6471b()     // Catch:{ all -> 0x0110 }
            r3.f5675t = r0     // Catch:{ all -> 0x0110 }
            java.lang.String r3 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r4 = "checkExtraJni"
            java.lang.Class[] r5 = new java.lang.Class[r2]     // Catch:{ all -> 0x0110 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r5[r1] = r7     // Catch:{ all -> 0x0110 }
            java.lang.Object[] r7 = new java.lang.Object[r2]     // Catch:{ all -> 0x0110 }
            r7[r1] = r0     // Catch:{ all -> 0x0110 }
            java.lang.Object r0 = com.tencent.bugly.proguard.C2908aq.m6895a(r3, r4, r9, r5, r7)     // Catch:{ all -> 0x0110 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ all -> 0x0110 }
            if (r0 == 0) goto L_0x00e1
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0110 }
            com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f5896l = r0     // Catch:{ all -> 0x0110 }
            boolean r0 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f5896l     // Catch:{ all -> 0x0110 }
            com.tencent.bugly.proguard.C2905ap.m6876a(r0)     // Catch:{ all -> 0x0110 }
        L_0x00e1:
            java.lang.String r0 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r3 = "enableHandler"
            java.lang.Class[] r4 = new java.lang.Class[r2]     // Catch:{ all -> 0x0110 }
            java.lang.Class r5 = java.lang.Boolean.TYPE     // Catch:{ all -> 0x0110 }
            r4[r1] = r5     // Catch:{ all -> 0x0110 }
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ all -> 0x0110 }
            java.lang.Boolean r7 = java.lang.Boolean.valueOf(r2)     // Catch:{ all -> 0x0110 }
            r5[r1] = r7     // Catch:{ all -> 0x0110 }
            com.tencent.bugly.proguard.C2908aq.m6895a(r0, r3, r9, r4, r5)     // Catch:{ all -> 0x0110 }
            if (r11 == 0) goto L_0x00f9
            r6 = 1
        L_0x00f9:
            java.lang.String r11 = "com.tencent.feedback.eup.jni.NativeExceptionUpload"
            java.lang.String r0 = "setLogMode"
            java.lang.Class[] r3 = new java.lang.Class[r2]     // Catch:{ all -> 0x0110 }
            java.lang.Class r4 = java.lang.Integer.TYPE     // Catch:{ all -> 0x0110 }
            r3[r1] = r4     // Catch:{ all -> 0x0110 }
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0110 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0110 }
            r2[r1] = r4     // Catch:{ all -> 0x0110 }
            com.tencent.bugly.proguard.C2908aq.m6895a(r11, r0, r9, r3, r2)     // Catch:{ all -> 0x0110 }
            monitor-exit(r10)
            return
        L_0x0110:
            r10.f5906i = r1     // Catch:{ all -> 0x0116 }
            r10.f5905h = r1     // Catch:{ all -> 0x0116 }
            monitor-exit(r10)
            return
        L_0x0116:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.mo34440a(boolean):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.a(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.a(int, java.lang.String):boolean
      com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.a(java.lang.String, boolean):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00a1, code lost:
        return;
     */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:37:0x00a2=Splitter:B:37:0x00a2, B:31:0x0069=Splitter:B:31:0x0069} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void startNativeMonitor() {
        /*
            r5 = this;
            monitor-enter(r5)
            boolean r0 = r5.f5906i     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x00a2
            boolean r0 = r5.f5905h     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x000b
            goto L_0x00a2
        L_0x000b:
            java.lang.String r0 = "Bugly"
            java.lang.String r1 = "NativeRQD"
            com.tencent.bugly.crashreport.common.info.a r2 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r2 = r2.f5674s     // Catch:{ all -> 0x00a9 }
            boolean r2 = com.tencent.bugly.proguard.C2908aq.m6915a(r2)     // Catch:{ all -> 0x00a9 }
            r3 = 0
            if (r2 != 0) goto L_0x001c
            r2 = 1
            goto L_0x001d
        L_0x001c:
            r2 = 0
        L_0x001d:
            boolean r4 = com.tencent.bugly.crashreport.crash.C2869c.f5822b     // Catch:{ all -> 0x00a9 }
            if (r4 == 0) goto L_0x004c
            if (r2 == 0) goto L_0x0028
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.f5674s     // Catch:{ all -> 0x00a9 }
            goto L_0x0039
        L_0x0028:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a9 }
            r4.<init>()     // Catch:{ all -> 0x00a9 }
            r4.append(r0)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = "-rqd"
            r4.append(r0)     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x00a9 }
        L_0x0039:
            boolean r0 = r5.m6709a(r0, r2)     // Catch:{ all -> 0x00a9 }
            r5.f5906i = r0     // Catch:{ all -> 0x00a9 }
            boolean r0 = r5.f5906i     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x005f
            if (r2 != 0) goto L_0x005f
            boolean r0 = r5.m6709a(r1, r3)     // Catch:{ all -> 0x00a9 }
            r5.f5905h = r0     // Catch:{ all -> 0x00a9 }
            goto L_0x005f
        L_0x004c:
            com.tencent.bugly.crashreport.common.info.a r1 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r1 = r1.f5674s     // Catch:{ all -> 0x00a9 }
            if (r2 != 0) goto L_0x0058
            com.tencent.bugly.crashreport.common.info.a r1 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            r1.getClass()     // Catch:{ all -> 0x00a9 }
            goto L_0x0059
        L_0x0058:
            r0 = r1
        L_0x0059:
            boolean r0 = r5.m6709a(r0, r2)     // Catch:{ all -> 0x00a9 }
            r5.f5906i = r0     // Catch:{ all -> 0x00a9 }
        L_0x005f:
            boolean r0 = r5.f5906i     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x0069
            boolean r0 = r5.f5905h     // Catch:{ all -> 0x00a9 }
            if (r0 != 0) goto L_0x0069
            monitor-exit(r5)
            return
        L_0x0069:
            boolean r0 = r5.f5904g     // Catch:{ all -> 0x00a9 }
            r5.mo34440a(r0)     // Catch:{ all -> 0x00a9 }
            boolean r0 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.f5896l     // Catch:{ all -> 0x00a9 }
            if (r0 == 0) goto L_0x00a0
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.f5671p     // Catch:{ all -> 0x00a9 }
            r5.setNativeAppVersion(r0)     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.f5673r     // Catch:{ all -> 0x00a9 }
            r5.setNativeAppChannel(r0)     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.f5659d     // Catch:{ all -> 0x00a9 }
            r5.setNativeAppPackage(r0)     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            java.lang.String r0 = r0.mo34309g()     // Catch:{ all -> 0x00a9 }
            r5.setNativeUserId(r0)     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            boolean r0 = r0.mo34295a()     // Catch:{ all -> 0x00a9 }
            r5.setNativeIsAppForeground(r0)     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.crashreport.common.info.a r0 = r5.f5900c     // Catch:{ all -> 0x00a9 }
            long r0 = r0.f5625a     // Catch:{ all -> 0x00a9 }
            r5.setNativeLaunchTime(r0)     // Catch:{ all -> 0x00a9 }
        L_0x00a0:
            monitor-exit(r5)
            return
        L_0x00a2:
            boolean r0 = r5.f5904g     // Catch:{ all -> 0x00a9 }
            r5.mo34440a(r0)     // Catch:{ all -> 0x00a9 }
            monitor-exit(r5)
            return
        L_0x00a9:
            r0 = move-exception
            monitor-exit(r5)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.startNativeMonitor():void");
    }

    public void checkUploadRecordCrash() {
        this.f5901d.mo34547a(new Runnable() {
            /* class com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.C28791 */

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
             arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
             candidates:
              com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.be
              com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bf
              com.tencent.bugly.crashreport.crash.b.a(android.content.Context, java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean>, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bg
              com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void
             arg types: [int, java.lang.String]
             candidates:
              com.tencent.bugly.crashreport.crash.jni.b.a(java.lang.String, java.lang.String):java.lang.String
              com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void */
            public void run() {
                if (!C2908aq.m6912a(NativeCrashHandler.this.f5899b, "native_record_lock", 10000)) {
                    C2903an.m6857a("[Native] Failed to lock file for handling native crash record.", new Object[0]);
                    return;
                }
                if (!NativeCrashHandler.f5898o) {
                    boolean unused = NativeCrashHandler.this.m6707a(999, "false");
                }
                CrashDetailBean a = C2881b.m6719a(NativeCrashHandler.this.f5899b, NativeCrashHandler.this.f5903f, NativeCrashHandler.this.f5902e);
                if (a != null) {
                    C2903an.m6857a("[Native] Get crash from native record.", new Object[0]);
                    if (!NativeCrashHandler.this.f5909n.mo34391a(a)) {
                        NativeCrashHandler.this.f5909n.mo34388a(a, 3000L, false);
                    }
                    C2881b.m6725a(false, NativeCrashHandler.this.f5903f);
                }
                NativeCrashHandler.this.mo34443b();
                C2908aq.m6939c(NativeCrashHandler.this.f5899b, "native_record_lock");
            }
        });
    }

    /* renamed from: a */
    private boolean m6709a(String str, boolean z) {
        boolean z2;
        try {
            C2903an.m6857a("[Native] Trying to load so: %s", str);
            if (z) {
                System.load(str);
            } else {
                System.loadLibrary(str);
            }
            try {
                C2903an.m6857a("[Native] Successfully loaded SO: %s", str);
                return true;
            } catch (Throwable th) {
                th = th;
                z2 = true;
            }
        } catch (Throwable th2) {
            th = th2;
            z2 = false;
            C2903an.m6864d(th.getMessage(), new Object[0]);
            C2903an.m6864d("[Native] Failed to load so: %s", str);
            return z2;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized void mo34439a() {
        if (!this.f5907j) {
            C2903an.m6864d("[Native] Native crash report has already unregistered.", new Object[0]);
            return;
        }
        try {
            if (unregist() != null) {
                C2903an.m6857a("[Native] Successfully closed native crash report.", new Object[0]);
                this.f5907j = false;
                return;
            }
        } catch (Throwable unused) {
            C2903an.m6863c("[Native] Failed to close native crash report.", new Object[0]);
        }
        try {
            C2908aq.m6895a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "enableHandler", null, new Class[]{Boolean.TYPE}, new Object[]{false});
            this.f5907j = false;
            C2903an.m6857a("[Native] Successfully closed native crash report.", new Object[0]);
            return;
        } catch (Throwable unused2) {
            C2903an.m6863c("[Native] Failed to close native crash report.", new Object[0]);
            this.f5906i = false;
            this.f5905h = false;
            return;
        }
    }

    public void testNativeCrash() {
        if (!this.f5906i) {
            C2903an.m6864d("[Native] Bugly SO file has not been load.", new Object[0]);
        } else {
            testCrash();
        }
    }

    public void testNativeCrash(boolean z, boolean z2, boolean z3) {
        m6707a(16, "" + z);
        m6707a(17, "" + z2);
        m6707a(18, "" + z3);
        testNativeCrash();
    }

    public NativeExceptionHandler getNativeExceptionHandler() {
        return this.f5902e;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo34443b() {
        long b = C2908aq.m6923b() - C2869c.f5827g;
        long b2 = C2908aq.m6923b() + 86400000;
        File file = new File(this.f5903f);
        if (file.exists() && file.isDirectory()) {
            try {
                File[] listFiles = file.listFiles();
                if (listFiles == null) {
                    return;
                }
                if (listFiles.length != 0) {
                    int i = 0;
                    int i2 = 0;
                    for (File file2 : listFiles) {
                        long lastModified = file2.lastModified();
                        if (lastModified < b || lastModified >= b2) {
                            C2903an.m6857a("[Native] Delete record file: %s", file2.getAbsolutePath());
                            i++;
                            if (file2.delete()) {
                                i2++;
                            }
                        }
                    }
                    C2903an.m6863c("[Native] Number of record files overdue: %d, has deleted: %d", Integer.valueOf(i), Integer.valueOf(i2));
                }
            } catch (Throwable th) {
                C2903an.m6858a(th);
            }
        }
    }

    public void removeEmptyNativeRecordFiles() {
        C2881b.m6730d(this.f5903f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public synchronized void mo34444b(boolean z) {
        if (z) {
            startNativeMonitor();
        } else {
            mo34439a();
        }
    }

    public synchronized boolean isUserOpened() {
        return this.f5908k;
    }

    /* renamed from: c */
    private synchronized void m6712c(boolean z) {
        if (this.f5908k != z) {
            C2903an.m6857a("user change native %b", Boolean.valueOf(z));
            this.f5908k = z;
        }
    }

    public synchronized void setUserOpened(boolean z) {
        m6712c(z);
        boolean isUserOpened = isUserOpened();
        C2854a a = C2854a.m6574a();
        if (a != null) {
            isUserOpened = isUserOpened && a.mo34340c().f5693g;
        }
        if (isUserOpened != this.f5907j) {
            C2903an.m6857a("native changed to %b", Boolean.valueOf(isUserOpened));
            mo34444b(isUserOpened);
        }
    }

    public synchronized void onStrategyChanged(StrategyBean strategyBean) {
        if (strategyBean != null) {
            if (strategyBean.f5693g != this.f5907j) {
                C2903an.m6864d("server native changed to %b", Boolean.valueOf(strategyBean.f5693g));
            }
        }
        boolean z = C2854a.m6574a().mo34340c().f5693g && this.f5908k;
        if (z != this.f5907j) {
            C2903an.m6857a("native changed to %b", Boolean.valueOf(z));
            mo34444b(z);
        }
    }

    public boolean appendLogToNative(String str, String str2, String str3) {
        if (!((!this.f5905h && !this.f5906i) || !f5896l || str == null || str2 == null || str3 == null)) {
            try {
                if (this.f5906i) {
                    return appendNativeLog(str, str2, str3);
                }
                Boolean bool = (Boolean) C2908aq.m6895a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "appendNativeLog", null, new Class[]{String.class, String.class, String.class}, new Object[]{str, str2, str3});
                if (bool != null) {
                    return bool.booleanValue();
                }
                return false;
            } catch (UnsatisfiedLinkError unused) {
                f5896l = false;
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }

    public String getLogFromNative() {
        if ((!this.f5905h && !this.f5906i) || !f5896l) {
            return null;
        }
        try {
            if (this.f5906i) {
                return getNativeLog();
            }
            return (String) C2908aq.m6895a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "getNativeLog", null, null, null);
        } catch (UnsatisfiedLinkError unused) {
            f5896l = false;
            return null;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    public boolean putKeyValueToNative(String str, String str2) {
        if ((this.f5905h || this.f5906i) && f5896l && str != null && str2 != null) {
            try {
                if (this.f5906i) {
                    return putNativeKeyValue(str, str2);
                }
                Boolean bool = (Boolean) C2908aq.m6895a("com.tencent.feedback.eup.jni.NativeExceptionUpload", "putNativeKeyValue", null, new Class[]{String.class, String.class}, new Object[]{str, str2});
                if (bool != null) {
                    return bool.booleanValue();
                }
                return false;
            } catch (UnsatisfiedLinkError unused) {
                f5896l = false;
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m6707a(int i, String str) {
        if (this.f5906i && f5897m) {
            try {
                setNativeInfo(i, str);
                return true;
            } catch (UnsatisfiedLinkError unused) {
                f5897m = false;
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
                return false;
            }
        }
        return false;
    }

    public boolean filterSigabrtSysLog() {
        return m6707a(998, "true");
    }

    public boolean setNativeAppVersion(String str) {
        return m6707a(10, str);
    }

    public boolean setNativeAppChannel(String str) {
        return m6707a(12, str);
    }

    public boolean setNativeAppPackage(String str) {
        return m6707a(13, str);
    }

    public boolean setNativeUserId(String str) {
        return m6707a(11, str);
    }

    public boolean setNativeIsAppForeground(boolean z) {
        return m6707a(14, z ? "true" : "false");
    }

    public boolean setNativeLaunchTime(long j) {
        try {
            return m6707a(15, String.valueOf(j));
        } catch (NumberFormatException e) {
            if (C2903an.m6858a(e)) {
                return false;
            }
            e.printStackTrace();
            return false;
        }
    }
}
