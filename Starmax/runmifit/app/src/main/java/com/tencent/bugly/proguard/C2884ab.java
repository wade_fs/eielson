package com.tencent.bugly.proguard;

import java.io.Serializable;

/* renamed from: com.tencent.bugly.proguard.ab */
/* compiled from: BUGLY */
public class C2884ab implements Serializable, Comparable<C2884ab> {

    /* renamed from: a */
    public long f5921a;

    /* renamed from: b */
    public String f5922b;

    /* renamed from: c */
    public long f5923c;

    /* renamed from: d */
    public int f5924d;

    /* renamed from: e */
    public String f5925e;

    /* renamed from: f */
    public String f5926f;

    /* renamed from: g */
    public long f5927g;

    /* renamed from: a */
    public int compareTo(C2884ab abVar) {
        return (int) (this.f5923c - abVar.f5923c);
    }
}
