package com.tencent.bugly.crashreport.common.info;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import com.google.android.gms.common.util.AndroidUtilsLight;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.Principal;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/* compiled from: BUGLY */
public class AppInfo {

    /* renamed from: a */
    public static final String[] f5592a = "@buglyAllChannel@".split(",");

    /* renamed from: b */
    public static final String[] f5593b = "@buglyAllChannelPriority@".split(",");

    /* renamed from: c */
    private static ActivityManager f5594c;

    /* renamed from: a */
    public static String m6455a(Context context) {
        if (context == null) {
            return null;
        }
        try {
            return context.getPackageName();
        } catch (Throwable th) {
            if (C2903an.m6858a(th)) {
                return "fail";
            }
            th.printStackTrace();
            return "fail";
        }
    }

    /* renamed from: b */
    public static PackageInfo m6460b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(m6455a(context), 0);
        } catch (Throwable th) {
            if (C2903an.m6858a(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static boolean m6459a(Context context, String str) {
        if (!(context == null || str == null || str.trim().length() <= 0)) {
            try {
                String[] strArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions;
                if (strArr != null) {
                    for (String str2 : strArr) {
                        if (str.equals(str2)) {
                            return true;
                        }
                    }
                }
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0049 A[Catch:{ all -> 0x0056, all -> 0x005c }] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0052 A[SYNTHETIC, Splitter:B:26:0x0052] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m6456a(android.content.Context r5, int r6) {
        /*
            r5 = 0
            java.io.FileReader r0 = new java.io.FileReader     // Catch:{ all -> 0x0042 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0042 }
            r1.<init>()     // Catch:{ all -> 0x0042 }
            java.lang.String r2 = "/proc/"
            r1.append(r2)     // Catch:{ all -> 0x0042 }
            r1.append(r6)     // Catch:{ all -> 0x0042 }
            java.lang.String r2 = "/cmdline"
            r1.append(r2)     // Catch:{ all -> 0x0042 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0042 }
            r0.<init>(r1)     // Catch:{ all -> 0x0042 }
            r5 = 512(0x200, float:7.175E-43)
            char[] r5 = new char[r5]     // Catch:{ all -> 0x003d }
            r0.read(r5)     // Catch:{ all -> 0x003d }
            r1 = 0
            r2 = 0
        L_0x0025:
            int r3 = r5.length     // Catch:{ all -> 0x003d }
            if (r2 >= r3) goto L_0x0030
            char r3 = r5[r2]     // Catch:{ all -> 0x003d }
            if (r3 != 0) goto L_0x002d
            goto L_0x0030
        L_0x002d:
            int r2 = r2 + 1
            goto L_0x0025
        L_0x0030:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x003d }
            r3.<init>(r5)     // Catch:{ all -> 0x003d }
            java.lang.String r5 = r3.substring(r1, r2)     // Catch:{ all -> 0x003d }
            r0.close()     // Catch:{ all -> 0x003c }
        L_0x003c:
            return r5
        L_0x003d:
            r5 = move-exception
            r4 = r0
            r0 = r5
            r5 = r4
            goto L_0x0043
        L_0x0042:
            r0 = move-exception
        L_0x0043:
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x0056 }
            if (r1 != 0) goto L_0x004c
            r0.printStackTrace()     // Catch:{ all -> 0x0056 }
        L_0x004c:
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x0056 }
            if (r5 == 0) goto L_0x0055
            r5.close()     // Catch:{ all -> 0x0055 }
        L_0x0055:
            return r6
        L_0x0056:
            r6 = move-exception
            if (r5 == 0) goto L_0x005c
            r5.close()     // Catch:{ all -> 0x005c }
        L_0x005c:
            goto L_0x005e
        L_0x005d:
            throw r6
        L_0x005e:
            goto L_0x005d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.common.info.AppInfo.m6456a(android.content.Context, int):java.lang.String");
    }

    /* renamed from: c */
    public static String m6461c(Context context) {
        CharSequence applicationLabel;
        if (context == null) {
            return null;
        }
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            if (!(packageManager == null || applicationInfo == null || (applicationLabel = packageManager.getApplicationLabel(applicationInfo)) == null)) {
                return applicationLabel.toString();
            }
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        }
        return null;
    }

    /* renamed from: d */
    public static Map<String, String> m6462d(Context context) {
        if (context == null) {
            return null;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo.metaData == null) {
                return null;
            }
            HashMap hashMap = new HashMap();
            Object obj = applicationInfo.metaData.get("BUGLY_DISABLE");
            if (obj != null) {
                hashMap.put("BUGLY_DISABLE", obj.toString());
            }
            Object obj2 = applicationInfo.metaData.get("BUGLY_APPID");
            if (obj2 != null) {
                hashMap.put("BUGLY_APPID", obj2.toString());
            }
            Object obj3 = applicationInfo.metaData.get("BUGLY_APP_CHANNEL");
            if (obj3 != null) {
                hashMap.put("BUGLY_APP_CHANNEL", obj3.toString());
            }
            Object obj4 = applicationInfo.metaData.get("BUGLY_APP_VERSION");
            if (obj4 != null) {
                hashMap.put("BUGLY_APP_VERSION", obj4.toString());
            }
            Object obj5 = applicationInfo.metaData.get("BUGLY_ENABLE_DEBUG");
            if (obj5 != null) {
                hashMap.put("BUGLY_ENABLE_DEBUG", obj5.toString());
            }
            Object obj6 = applicationInfo.metaData.get("com.tencent.rdm.uuid");
            if (obj6 != null) {
                hashMap.put("com.tencent.rdm.uuid", obj6.toString());
            }
            return hashMap;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    public static List<String> m6458a(Map<String, String> map) {
        if (map == null) {
            return null;
        }
        try {
            String str = map.get("BUGLY_DISABLE");
            if (str != null) {
                if (str.length() != 0) {
                    String[] split = str.split(",");
                    for (int i = 0; i < split.length; i++) {
                        split[i] = split[i].trim();
                    }
                    return Arrays.asList(split);
                }
            }
            return null;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    public static String m6457a(byte[] bArr) {
        X509Certificate x509Certificate;
        StringBuilder sb = new StringBuilder();
        if (bArr != null && bArr.length > 0) {
            try {
                CertificateFactory instance = CertificateFactory.getInstance("X.509");
                if (instance == null || (x509Certificate = (X509Certificate) instance.generateCertificate(new ByteArrayInputStream(bArr))) == null) {
                    return null;
                }
                sb.append("Issuer|");
                Principal issuerDN = x509Certificate.getIssuerDN();
                if (issuerDN != null) {
                    sb.append(issuerDN.toString());
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("SerialNumber|");
                BigInteger serialNumber = x509Certificate.getSerialNumber();
                if (issuerDN != null) {
                    sb.append(serialNumber.toString(16));
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("NotBefore|");
                Date notBefore = x509Certificate.getNotBefore();
                if (issuerDN != null) {
                    sb.append(notBefore.toString());
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("NotAfter|");
                Date notAfter = x509Certificate.getNotAfter();
                if (issuerDN != null) {
                    sb.append(notAfter.toString());
                } else {
                    sb.append("unknown");
                }
                sb.append("\n");
                sb.append("SHA1|");
                String a = C2908aq.m6904a(MessageDigest.getInstance(AndroidUtilsLight.DIGEST_ALGORITHM_SHA1).digest(x509Certificate.getEncoded()));
                if (a == null || a.length() <= 0) {
                    sb.append("unknown");
                } else {
                    sb.append(a.toString());
                }
                sb.append("\n");
                sb.append("MD5|");
                String a2 = C2908aq.m6904a(MessageDigest.getInstance("MD5").digest(x509Certificate.getEncoded()));
                if (a2 == null || a2.length() <= 0) {
                    sb.append("unknown");
                } else {
                    sb.append(a2.toString());
                }
            } catch (CertificateException e) {
                if (!C2903an.m6858a(e)) {
                    e.printStackTrace();
                }
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
        if (sb.length() == 0) {
            return "unknown";
        }
        return sb.toString();
    }

    /* renamed from: e */
    public static String m6463e(Context context) {
        Signature[] signatureArr;
        String a = m6455a(context);
        if (a == null) {
            return null;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(a, 64);
            if (!(packageInfo == null || (signatureArr = packageInfo.signatures) == null || signatureArr.length == 0)) {
                return m6457a(packageInfo.signatures[0].toByteArray());
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return null;
    }

    /* renamed from: f */
    public static boolean m6464f(Context context) {
        if (context == null) {
            return false;
        }
        if (f5594c == null) {
            f5594c = (ActivityManager) context.getSystemService("activity");
        }
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            f5594c.getMemoryInfo(memoryInfo);
            if (!memoryInfo.lowMemory) {
                return false;
            }
            C2903an.m6863c("Memory is low.", new Object[0]);
            return true;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return false;
        }
    }

    /* renamed from: h */
    private static String m6466h(Context context) {
        String str = "";
        InputStream inputStream = null;
        try {
            String string = C2908aq.m6892a("DENGTA_META", context).getString("key_channelpath", str);
            if (C2908aq.m6915a(string)) {
                string = "channel.ini";
            }
            C2903an.m6857a("[AppInfo] Beacon channel file path: " + string, new Object[0]);
            if (!string.equals(str)) {
                inputStream = context.getAssets().open(string);
                Properties properties = new Properties();
                properties.load(inputStream);
                str = properties.getProperty("CHANNEL", str);
                C2903an.m6857a("[AppInfo] Beacon channel read from assert: " + str, new Object[0]);
                if (!C2908aq.m6915a(str)) {
                    if (inputStream != null) {
                        try {
                            inputStream.close();
                        } catch (IOException e) {
                            C2903an.m6858a(e);
                        }
                    }
                    return str;
                }
            }
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e2) {
                    C2903an.m6858a(e2);
                }
            }
        } catch (Exception unused) {
            C2903an.m6864d("[AppInfo] Failed to get get beacon channel", new Object[0]);
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e3) {
                    C2903an.m6858a(e3);
                }
            }
            throw th;
        }
        return str;
    }

    /* renamed from: i */
    private static String m6467i(Context context) {
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("CHANNEL_DENGTA");
            if (obj != null) {
                return obj.toString();
            }
            return "";
        } catch (Throwable th) {
            C2903an.m6864d("[AppInfo] Failed to read beacon channel from manifest.", new Object[0]);
            C2903an.m6858a(th);
            return "";
        }
    }

    /* renamed from: g */
    public static String m6465g(Context context) {
        if (context == null) {
            return "";
        }
        String h = m6466h(context);
        if (!C2908aq.m6915a(h)) {
            return h;
        }
        return m6467i(context);
    }
}
