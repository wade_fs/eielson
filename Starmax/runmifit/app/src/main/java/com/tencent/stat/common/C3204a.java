package com.tencent.stat.common;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import com.baidu.mobstat.Config;
import com.tencent.stat.C3226e;
import com.tencent.stat.NetworkManager;
import com.tencent.stat.StatConfig;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.common.a */
public class C3204a {

    /* renamed from: a */
    static C3206a f7225a;

    /* renamed from: d */
    private static StatLogger f7226d = StatCommonHelper.getLogger();

    /* renamed from: e */
    private static JSONObject f7227e = new JSONObject();

    /* renamed from: b */
    Integer f7228b = null;

    /* renamed from: c */
    String f7229c = null;

    /* renamed from: com.tencent.stat.common.a$a */
    static class C3206a {

        /* renamed from: a */
        String f7230a;

        /* renamed from: b */
        String f7231b;

        /* renamed from: c */
        DisplayMetrics f7232c;

        /* renamed from: d */
        int f7233d;

        /* renamed from: e */
        String f7234e;

        /* renamed from: f */
        String f7235f;

        /* renamed from: g */
        String f7236g;

        /* renamed from: h */
        String f7237h;

        /* renamed from: i */
        String f7238i;

        /* renamed from: j */
        String f7239j;

        /* renamed from: k */
        String f7240k;

        /* renamed from: l */
        int f7241l;

        /* renamed from: m */
        String f7242m;

        /* renamed from: n */
        String f7243n;

        /* renamed from: o */
        String f7244o;

        /* renamed from: p */
        Context f7245p;

        /* renamed from: q */
        private String f7246q;

        /* renamed from: r */
        private String f7247r;

        /* renamed from: s */
        private String f7248s;

        /* renamed from: t */
        private String f7249t;

        /* renamed from: u */
        private String f7250u;

        private C3206a(Context context) {
            this.f7231b = StatConstants.VERSION;
            this.f7233d = Build.VERSION.SDK_INT;
            this.f7234e = Build.MODEL;
            this.f7235f = Build.MANUFACTURER;
            this.f7236g = Locale.getDefault().getLanguage();
            this.f7241l = 0;
            this.f7243n = null;
            this.f7244o = null;
            this.f7245p = null;
            this.f7246q = null;
            this.f7247r = null;
            this.f7248s = null;
            this.f7249t = null;
            this.f7250u = null;
            this.f7245p = context.getApplicationContext();
            this.f7232c = StatCommonHelper.getDisplayMetrics(this.f7245p);
            this.f7230a = StatCommonHelper.getCurAppVersion(this.f7245p);
            this.f7237h = StatConfig.getInstallChannel(this.f7245p);
            this.f7238i = StatCommonHelper.getSimOperator(this.f7245p);
            this.f7239j = TimeZone.getDefault().getID();
            this.f7241l = StatCommonHelper.hasRootAccess(this.f7245p);
            this.f7240k = StatCommonHelper.getExternalStorageInfo(this.f7245p);
            this.f7243n = this.f7245p.getPackageName();
            int i = this.f7233d;
            this.f7247r = StatCommonHelper.getCpuInfo(this.f7245p).toString();
            this.f7248s = StatCommonHelper.getSystemMemory(this.f7245p);
            this.f7249t = StatCommonHelper.getRomMemory();
            this.f7244o = StatCommonHelper.getLauncherPackageName(this.f7245p);
            this.f7250u = StatCommonHelper.getCurAppSHA1Signature(this.f7245p);
            this.f7242m = StatCommonHelper.getDeviceIMSI(this.f7245p);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo35401a(JSONObject jSONObject, Thread thread) throws JSONException {
            if (thread == null) {
                if (this.f7232c != null) {
                    jSONObject.put("sr", this.f7232c.widthPixels + "*" + this.f7232c.heightPixels);
                    jSONObject.put("dpi", this.f7232c.xdpi + "*" + this.f7232c.ydpi);
                }
                if (NetworkManager.getInstance(this.f7245p).isWifi()) {
                    JSONObject jSONObject2 = new JSONObject();
                    Util.jsonPut(jSONObject2, "bs", Util.getWiFiBBSID(this.f7245p));
                    Util.jsonPut(jSONObject2, "ss", Util.getWiFiSSID(this.f7245p));
                    if (jSONObject2.length() > 0) {
                        Util.jsonPut(jSONObject, "wf", jSONObject2.toString());
                    }
                }
                JSONArray wifiTopN = Util.getWifiTopN(this.f7245p, 10);
                if (wifiTopN != null && wifiTopN.length() > 0) {
                    Util.jsonPut(jSONObject, "wflist", wifiTopN.toString());
                }
                Util.jsonPut(jSONObject, "sen", this.f7246q);
            } else {
                Util.jsonPut(jSONObject, "thn", thread.getName());
                Util.jsonPut(jSONObject, "qq", StatConfig.getQQ(this.f7245p));
                Util.jsonPut(jSONObject, "cui", StatConfig.getCustomUserId(this.f7245p));
                if (StatCommonHelper.isStringValid(this.f7248s) && this.f7248s.split("/").length == 2) {
                    Util.jsonPut(jSONObject, "fram", this.f7248s.split("/")[0]);
                }
                if (StatCommonHelper.isStringValid(this.f7249t) && this.f7249t.split("/").length == 2) {
                    Util.jsonPut(jSONObject, "from", this.f7249t.split("/")[0]);
                }
                if (C3226e.m8037a(this.f7245p).mo35429b(this.f7245p) != null) {
                    jSONObject.put(DeviceInfo.TAG_IMEI, C3226e.m8037a(this.f7245p).mo35429b(this.f7245p).getImei());
                }
                Util.jsonPut(jSONObject, "mid", StatConfig.getLocalMidOnly(this.f7245p));
            }
            Util.jsonPut(jSONObject, "pcn", StatCommonHelper.getCurProcessName(this.f7245p));
            Util.jsonPut(jSONObject, "osn", Build.VERSION.RELEASE);
            String appVersion = StatConfig.getAppVersion();
            if (!StatCommonHelper.isStringValid(appVersion)) {
                Util.jsonPut(jSONObject, "av", this.f7230a);
            } else {
                Util.jsonPut(jSONObject, "av", appVersion);
                Util.jsonPut(jSONObject, "appv", this.f7230a);
            }
            Util.jsonPut(jSONObject, "ch", this.f7237h);
            Util.jsonPut(jSONObject, "mf", this.f7235f);
            Util.jsonPut(jSONObject, "sv", this.f7231b);
            Util.jsonPut(jSONObject, "osd", Build.DISPLAY);
            Util.jsonPut(jSONObject, "prod", Build.PRODUCT);
            Util.jsonPut(jSONObject, "tags", Build.TAGS);
            Util.jsonPut(jSONObject, Config.FEED_LIST_ITEM_CUSTOM_ID, Build.ID);
            Util.jsonPut(jSONObject, "fng", Build.FINGERPRINT);
            Util.jsonPut(jSONObject, "lch", this.f7244o);
            Util.jsonPut(jSONObject, "ov", Integer.toString(this.f7233d));
            jSONObject.put("os", 1);
            Util.jsonPut(jSONObject, Config.OPERATOR, this.f7238i);
            Util.jsonPut(jSONObject, "lg", this.f7236g);
            Util.jsonPut(jSONObject, "md", this.f7234e);
            Util.jsonPut(jSONObject, "tz", this.f7239j);
            int i = this.f7241l;
            if (i != 0) {
                jSONObject.put("jb", i);
            }
            Util.jsonPut(jSONObject, Config.FEED_LIST_MAPPING, this.f7240k);
            Util.jsonPut(jSONObject, "apn", this.f7243n);
            Util.jsonPut(jSONObject, "cpu", this.f7247r);
            Util.jsonPut(jSONObject, "abi", Build.CPU_ABI);
            Util.jsonPut(jSONObject, "abi2", Build.CPU_ABI2);
            Util.jsonPut(jSONObject, "ram", this.f7248s);
            Util.jsonPut(jSONObject, Config.ROM, this.f7249t);
            Util.jsonPut(jSONObject, "im", this.f7242m);
            Util.jsonPut(jSONObject, "asg", this.f7250u);
        }
    }

    /* renamed from: a */
    public static void m7974a(Context context, Map<String, String> map) throws JSONException {
        if (map != null) {
            for (Map.Entry entry : new HashMap(map).entrySet()) {
                f7227e.put((String) entry.getKey(), entry.getValue());
            }
        }
    }

    /* renamed from: a */
    static synchronized C3206a m7973a(Context context) {
        C3206a aVar;
        synchronized (C3204a.class) {
            if (f7225a == null) {
                f7225a = new C3206a(context.getApplicationContext());
            }
            aVar = f7225a;
        }
        return aVar;
    }

    public C3204a(Context context) {
        try {
            m7973a(context);
            this.f7228b = StatCommonHelper.getTelephonyNetworkType(context.getApplicationContext());
            this.f7229c = NetworkManager.getInstance(context).getCurNetwrokName();
        } catch (Throwable th) {
            f7226d.mo35384e(th);
        }
    }

    /* renamed from: a */
    public void mo35400a(JSONObject jSONObject, Thread thread) throws JSONException {
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (f7225a != null) {
                f7225a.mo35401a(jSONObject2, thread);
            }
            Util.jsonPut(jSONObject2, "cn", this.f7229c);
            if (this.f7228b != null) {
                jSONObject2.put("tn", this.f7228b);
            }
            if (thread == null) {
                jSONObject.put(Config.EVENT_PART, jSONObject2);
            } else {
                jSONObject.put("errkv", jSONObject2.toString());
            }
            if (f7227e != null && f7227e.length() > 0) {
                jSONObject.put("eva", f7227e);
            }
        } catch (Throwable th) {
            f7226d.mo35384e(th);
        }
    }
}
