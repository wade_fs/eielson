package com.tencent.connect.share;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3119c;
import com.tencent.open.utils.C3131k;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/* renamed from: com.tencent.connect.share.a */
/* compiled from: ProGuard */
public class C3005a {
    /* renamed from: a */
    public static final void m7337a(Context context, final String str, final C3119c cVar) {
        C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "scaleCompressImage");
        if (TextUtils.isEmpty(str)) {
            cVar.mo34826a(1, (String) null);
        } else if (!C3131k.m7782b()) {
            cVar.mo34826a(2, (String) null);
        } else {
            final C30061 r0 = new Handler(context.getMainLooper()) {
                /* class com.tencent.connect.share.C3005a.C30061 */

                public void handleMessage(Message message) {
                    int i = message.what;
                    if (i == 101) {
                        cVar.mo34826a(0, (String) message.obj);
                    } else if (i != 102) {
                        super.handleMessage(message);
                    } else {
                        cVar.mo34826a(message.arg1, (String) null);
                    }
                }
            };
            new Thread(new Runnable() {
                /* class com.tencent.connect.share.C3005a.C30072 */

                public void run() {
                    String str;
                    Bitmap a = C3005a.m7335a(str, (int) FMParserConstants.EMPTY_DIRECTIVE_END);
                    if (a != null) {
                        String str2 = Environment.getExternalStorageDirectory() + "/tmp/";
                        String str3 = "share2qq_temp" + C3131k.m7793f(str) + ".jpg";
                        if (!C3005a.m7341b(str, (int) FMParserConstants.EMPTY_DIRECTIVE_END, (int) FMParserConstants.EMPTY_DIRECTIVE_END)) {
                            C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "not out of bound,not compress!");
                            str = str;
                        } else {
                            C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "out of bound,compress!");
                            str = C3005a.m7336a(a, str2, str3);
                        }
                        C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "-->destFilePath: " + str);
                        if (str != null) {
                            Message obtainMessage = r0.obtainMessage(101);
                            obtainMessage.obj = str;
                            r0.sendMessage(obtainMessage);
                            return;
                        }
                    }
                    Message obtainMessage2 = r0.obtainMessage(102);
                    obtainMessage2.arg1 = 3;
                    r0.sendMessage(obtainMessage2);
                }
            }).start();
        }
    }

    /* renamed from: a */
    public static final void m7338a(Context context, final ArrayList<String> arrayList, final C3119c cVar) {
        C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "batchScaleCompressImage");
        if (arrayList == null) {
            cVar.mo34826a(1, (String) null);
            return;
        }
        final C30083 r0 = new Handler(context.getMainLooper()) {
            /* class com.tencent.connect.share.C3005a.C30083 */

            public void handleMessage(Message message) {
                if (message.what != 101) {
                    super.handleMessage(message);
                    return;
                }
                cVar.mo34827a(0, message.getData().getStringArrayList("images"));
            }
        };
        new Thread(new Runnable() {
            /* class com.tencent.connect.share.C3005a.C30094 */

            public void run() {
                Bitmap a;
                for (int i = 0; i < arrayList.size(); i++) {
                    String str = (String) arrayList.get(i);
                    if (!C3131k.m7797g(str) && C3131k.m7798h(str) && (a = C3005a.m7335a(str, 10000)) != null) {
                        String str2 = Environment.getExternalStorageDirectory() + "/tmp/";
                        String str3 = "share2qzone_temp" + C3131k.m7793f(str) + ".jpg";
                        if (!C3005a.m7341b(str, 640, 10000)) {
                            C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "not out of bound,not compress!");
                        } else {
                            C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "out of bound, compress!");
                            str = C3005a.m7336a(a, str2, str3);
                        }
                        if (str != null) {
                            arrayList.set(i, str);
                        }
                    }
                }
                Message obtainMessage = r0.obtainMessage(101);
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("images", arrayList);
                obtainMessage.setData(bundle);
                r0.sendMessage(obtainMessage);
            }
        }).start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* renamed from: a */
    private static Bitmap m7334a(Bitmap bitmap, int i) {
        Matrix matrix = new Matrix();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        if (width <= height) {
            width = height;
        }
        float f = ((float) i) / ((float) width);
        matrix.postScale(f, f);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    /* renamed from: a */
    protected static final String m7336a(Bitmap bitmap, String str, String str2) {
        File file = new File(str);
        if (!file.exists()) {
            file.mkdirs();
        }
        StringBuffer stringBuffer = new StringBuffer(str);
        stringBuffer.append(str2);
        String stringBuffer2 = stringBuffer.toString();
        File file2 = new File(stringBuffer2);
        if (file2.exists()) {
            file2.delete();
        }
        if (bitmap == null) {
            return null;
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(file2);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fileOutputStream);
            fileOutputStream.flush();
            fileOutputStream.close();
            bitmap.recycle();
            return stringBuffer2;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static final boolean m7341b(String str, int i, int i2) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        int i3 = options.outWidth;
        int i4 = options.outHeight;
        if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
            return false;
        }
        int i5 = i3 > i4 ? i3 : i4;
        if (i3 >= i4) {
            i3 = i4;
        }
        C3082f.m7631b("openSDK_LOG.AsynScaleCompressImage", "longSide=" + i5 + "shortSide=" + i3);
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        if (i5 > i2 || i3 > i) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public static final Bitmap m7335a(String str, int i) {
        Bitmap bitmap;
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        if (options.mCancel || options.outWidth == -1 || options.outHeight == -1) {
            return null;
        }
        if (i2 <= i3) {
            i2 = i3;
        }
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        if (i2 > i) {
            options.inSampleSize = m7333a(options, -1, i * i);
        }
        options.inJustDecodeBounds = false;
        try {
            bitmap = BitmapFactory.decodeFile(str, options);
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            bitmap = null;
        }
        if (bitmap == null) {
            return null;
        }
        int i4 = options.outWidth;
        int i5 = options.outHeight;
        if (i4 > i5) {
            i5 = i4;
        }
        return i5 > i ? m7334a(bitmap, i) : bitmap;
    }

    /* renamed from: a */
    public static final int m7333a(BitmapFactory.Options options, int i, int i2) {
        int b = m7340b(options, i, i2);
        if (b > 8) {
            return ((b + 7) / 8) * 8;
        }
        int i3 = 1;
        while (i3 < b) {
            i3 <<= 1;
        }
        return i3;
    }

    /* renamed from: b */
    private static int m7340b(BitmapFactory.Options options, int i, int i2) {
        int i3;
        int i4;
        double d = (double) options.outWidth;
        double d2 = (double) options.outHeight;
        if (i2 == -1) {
            i3 = 1;
        } else {
            Double.isNaN(d);
            Double.isNaN(d2);
            double d3 = (double) i2;
            Double.isNaN(d3);
            i3 = (int) Math.ceil(Math.sqrt((d * d2) / d3));
        }
        if (i == -1) {
            i4 = 128;
        } else {
            double d4 = (double) i;
            Double.isNaN(d);
            Double.isNaN(d4);
            double floor = Math.floor(d / d4);
            Double.isNaN(d2);
            Double.isNaN(d4);
            i4 = (int) Math.min(floor, Math.floor(d2 / d4));
        }
        if (i4 < i3) {
            return i3;
        }
        if (i2 == -1 && i == -1) {
            return 1;
        }
        return i == -1 ? i3 : i4;
    }
}
