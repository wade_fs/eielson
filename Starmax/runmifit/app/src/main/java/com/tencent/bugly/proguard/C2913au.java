package com.tencent.bugly.proguard;

/* renamed from: com.tencent.bugly.proguard.au */
/* compiled from: BUGLY */
public class C2913au {

    /* renamed from: a */
    private static C2915aw f6066a;

    /* renamed from: b */
    private static C2916ax f6067b;

    /* renamed from: a */
    public static C2914av m6964a(int i) {
        if (i == 1) {
            return m6965b();
        }
        if (i == 2) {
            return m6963a();
        }
        return null;
    }

    /* renamed from: a */
    public static C2914av m6963a() {
        return new C2915aw();
    }

    /* renamed from: b */
    public static C2914av m6965b() {
        return new C2916ax();
    }
}
