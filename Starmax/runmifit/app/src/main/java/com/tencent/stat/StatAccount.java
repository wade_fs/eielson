package com.tencent.stat;

import com.baidu.mobstat.Config;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class StatAccount {
    public static final int CUSTOM_TYPE = 7;
    public static final int DEFAULT_TYPE = 0;
    public static final int EMAIL_TYPE = 6;
    public static final int PHONE_NUM_TYPE = 5;
    public static final int QQ_NUM_TYPE = 1;
    public static final int QQ_OPENID_TYPE = 3;
    public static final int WECHAT_ID_TYPE = 2;
    public static final int WECHAT_OPENID_TYPE = 4;

    /* renamed from: a */
    private String f6895a = "";

    /* renamed from: b */
    private int f6896b = 0;

    /* renamed from: c */
    private String f6897c = "";

    /* renamed from: d */
    private String f6898d = "";

    public StatAccount(String str) {
        this.f6895a = str;
    }

    public StatAccount(String str, int i) {
        this.f6895a = str;
        this.f6896b = i;
    }

    public String toString() {
        return "StatAccount [account=" + this.f6895a + ", accountType=" + this.f6896b + ", ext=" + this.f6897c + ", ext1=" + this.f6898d + "]";
    }

    public String toJsonString() {
        JSONObject jSONObject = new JSONObject();
        if (StatCommonHelper.isStringValid(this.f6895a)) {
            try {
                Util.jsonPut(jSONObject, Config.APP_VERSION_CODE, this.f6895a);
                jSONObject.put("t", this.f6896b);
                Util.jsonPut(jSONObject, "e", this.f6897c);
                Util.jsonPut(jSONObject, "e1", this.f6898d);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONObject.toString();
    }

    public String getAccount() {
        return this.f6895a;
    }

    public void setAccount(String str) {
        this.f6895a = str;
    }

    public int getAccountType() {
        return this.f6896b;
    }

    public void setAccountType(int i) {
        this.f6896b = i;
    }

    public String getExt() {
        return this.f6897c;
    }

    public void setExt(String str) {
        this.f6897c = str;
    }

    public String getExt1() {
        return this.f6898d;
    }

    public void setExt1(String str) {
        this.f6898d = str;
    }
}
