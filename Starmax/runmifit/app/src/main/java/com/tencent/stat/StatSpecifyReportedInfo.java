package com.tencent.stat;

public class StatSpecifyReportedInfo {

    /* renamed from: a */
    private String f7150a = null;

    /* renamed from: b */
    private String f7151b = null;

    /* renamed from: c */
    private String f7152c = null;

    /* renamed from: d */
    private boolean f7153d = false;

    /* renamed from: e */
    private boolean f7154e = false;

    /* renamed from: f */
    private int f7155f = 0;

    public int getFromH5() {
        return this.f7155f;
    }

    public void setFromH5(int i) {
        this.f7155f = i;
    }

    public boolean isSendImmediately() {
        return this.f7153d;
    }

    public void setSendImmediately(boolean z) {
        this.f7153d = z;
    }

    public String getVersion() {
        return this.f7152c;
    }

    public void setVersion(String str) {
        this.f7152c = str;
    }

    public String getAppKey() {
        return this.f7150a;
    }

    public void setAppKey(String str) {
        this.f7150a = str;
    }

    public String getInstallChannel() {
        return this.f7151b;
    }

    public void setInstallChannel(String str) {
        this.f7151b = str;
    }

    public boolean isImportant() {
        return this.f7154e;
    }

    public void setImportant(boolean z) {
        this.f7154e = z;
    }

    public String toString() {
        return "StatSpecifyReportedInfo [appKey=" + this.f7150a + ", installChannel=" + this.f7151b + ", version=" + this.f7152c + ", sendImmediately=" + this.f7153d + ", isImportant=" + this.f7154e + "]";
    }
}
