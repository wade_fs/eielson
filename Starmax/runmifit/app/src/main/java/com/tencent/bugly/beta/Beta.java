package com.tencent.bugly.beta;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.database.sqlite.SQLiteDatabase;
import android.os.Looper;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.runmifit.android.BuildConfig;
import com.tencent.bugly.BUGLY;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.beta.download.DownloadListener;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2807d;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.beta.global.C2809f;
import com.tencent.bugly.beta.global.ResBean;
import com.tencent.bugly.beta.interfaces.BetaPatchListener;
import com.tencent.bugly.beta.p050ui.C2826h;
import com.tencent.bugly.beta.p050ui.UILifecycleListener;
import com.tencent.bugly.beta.tinker.TinkerApplicationLike;
import com.tencent.bugly.beta.tinker.TinkerManager;
import com.tencent.bugly.beta.upgrade.BetaGrayStrategy;
import com.tencent.bugly.beta.upgrade.C2829c;
import com.tencent.bugly.beta.upgrade.UpgradeListener;
import com.tencent.bugly.beta.upgrade.UpgradeStateListener;
import com.tencent.bugly.beta.utils.C2836e;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.proguard.C2885ac;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import com.tencent.bugly.proguard.C2947p;
import com.tencent.bugly.proguard.C2948q;
import com.tencent.bugly.proguard.C2950r;
import com.tencent.bugly.proguard.C2952s;
import com.tencent.bugly.proguard.C2955u;
import com.tencent.bugly.proguard.C2956v;
import com.tencent.bugly.proguard.C2957w;
import com.tencent.bugly.proguard.C2959y;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/* compiled from: BUGLY */
public class Beta extends BUGLY {
    public static final String TAG_CANCEL_BUTTON = "beta_cancel_button";
    public static final String TAG_CONFIRM_BUTTON = "beta_confirm_button";
    public static final String TAG_IMG_BANNER = "beta_upgrade_banner";
    public static final String TAG_TIP_MESSAGE = "beta_tip_message";
    public static final String TAG_TITLE = "beta_title";
    public static final String TAG_UPGRADE_FEATURE = "beta_upgrade_feature";
    public static final String TAG_UPGRADE_INFO = "beta_upgrade_info";

    /* renamed from: a */
    private static DownloadTask f5306a = null;
    public static String appChannel = null;
    public static int appVersionCode = Integer.MIN_VALUE;
    public static String appVersionName = null;
    public static boolean autoCheckUpgrade = true;
    public static boolean autoDownloadOn4g = false;
    public static boolean autoDownloadOnWifi = false;
    public static boolean autoInit = true;
    public static boolean autoInstallApk = true;
    public static BetaPatchListener betaPatchListener = null;
    public static boolean canAutoDownloadPatch = true;
    public static boolean canAutoPatch = true;
    public static List<Class<? extends Activity>> canNotShowUpgradeActs = Collections.synchronizedList(new ArrayList());
    public static boolean canNotifyUserRestart = false;
    public static boolean canShowApkInfo = true;
    public static List<Class<? extends Activity>> canShowUpgradeActs = Collections.synchronizedList(new ArrayList());
    public static int defaultBannerId = 0;
    public static boolean dialogFullScreen = false;
    public static DownloadListener downloadListener = null;
    public static boolean enableHotfix = false;
    public static boolean enableNotification = true;
    public static long initDelay = 3000;
    public static String initProcessName = null;
    public static Beta instance = new Beta();
    public static int largeIconId = 0;
    public static boolean setPatchRestartOnScreenOff = true;
    public static boolean showInterruptedStrategy = true;
    public static int smallIconId = 0;
    public static List<String> soBlackList = Collections.synchronizedList(new ArrayList());
    public static File storageDir = null;
    public static String strNetworkTipsCancelBtn = "取消";
    public static String strNetworkTipsConfirmBtn = "继续下载";
    public static String strNetworkTipsMessage = "你已切换到移动网络，是否继续当前下载？";
    public static String strNetworkTipsTitle = "网络提示";
    public static String strNotificationClickToContinue = "继续下载";
    public static String strNotificationClickToInstall = "点击安装";
    public static String strNotificationClickToRetry = "点击重试";
    public static String strNotificationClickToView = "点击查看";
    public static String strNotificationDownloadError = "下载失败";
    public static String strNotificationDownloadSucc = "下载完成";
    public static String strNotificationDownloading = "正在下载";
    public static String strNotificationHaveNewVersion = "有新版本";
    public static String strToastCheckUpgradeError = "检查新版本失败，请稍后重试";
    public static String strToastCheckingUpgrade = "正在检查，请稍候...";
    public static String strToastYourAreTheLatestVersion = "你已经是最新版了";
    public static String strUpgradeDialogCancelBtn = "下次再说";
    public static String strUpgradeDialogContinueBtn = "继续";
    public static String strUpgradeDialogFeatureLabel = "更新说明";
    public static String strUpgradeDialogFileSizeLabel = "包大小";
    public static String strUpgradeDialogInstallBtn = "安装";
    public static String strUpgradeDialogRetryBtn = "重试";
    public static String strUpgradeDialogUpdateTimeLabel = "更新时间";
    public static String strUpgradeDialogUpgradeBtn = "立即更新";
    public static String strUpgradeDialogVersionLabel = "版本";
    public static int tipsDialogLayoutId;
    public static long upgradeCheckPeriod;
    public static int upgradeDialogLayoutId;
    public static UILifecycleListener<UpgradeInfo> upgradeDialogLifecycleListener;
    public static UpgradeListener upgradeListener;
    public static UpgradeStateListener upgradeStateListener;

    public void onDbDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public static Beta getInstance() {
        Beta beta = instance;
        beta.f5300id = 1002;
        beta.version = BuildConfig.VERSION_NAME;
        beta.versionKey = "G10";
        return beta;
    }

    public static void checkUpgrade() {
        checkUpgrade(true, false);
    }

    public static void checkUpgrade(boolean z, boolean z2) {
        try {
            if (TextUtils.isEmpty(C2808e.f5336E.f5388v)) {
                if (Looper.myLooper() == Looper.getMainLooper()) {
                    C2901am.m6848a().mo34547a(new C2807d(19, Boolean.valueOf(z), Boolean.valueOf(z2)));
                    return;
                }
                synchronized (C2808e.f5336E) {
                    while (TextUtils.isEmpty(C2808e.f5336E.f5388v)) {
                        try {
                            C2808e.f5336E.wait();
                        } catch (InterruptedException unused) {
                            C2903an.m6865e("wait error", new Object[0]);
                        }
                    }
                }
            }
            if (!z) {
                if (TextUtils.isEmpty(C2808e.f5336E.f5388v)) {
                    C2903an.m6865e("[beta] BetaModule is uninitialized", new Object[0]);
                } else {
                    BetaGrayStrategy betaGrayStrategy = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
                    if (!(betaGrayStrategy == null || betaGrayStrategy.f5455a == null || System.currentTimeMillis() - betaGrayStrategy.f5459e > C2808e.f5336E.f5369c)) {
                        if (betaGrayStrategy.f5455a.f6297p != 3) {
                            C2829c.f5467a.mo34191a(z, z2, 0, null, "");
                        }
                    }
                    C2829c.f5467a.mo34190a(z, z2, 0);
                }
            }
            if (!z) {
                return;
            }
            if (TextUtils.isEmpty(C2808e.f5336E.f5388v)) {
                C2903an.m6865e("[beta] BetaModule is uninitialized", new Object[0]);
                if (upgradeStateListener != null) {
                    C2836e.m6398a(new C2807d(18, upgradeStateListener, -1, Boolean.valueOf(z)));
                    return;
                }
                C2836e.m6398a(new C2807d(5, strToastCheckUpgradeError));
                return;
            }
            C2829c.f5467a.mo34190a(z, z2, 1);
            if (upgradeStateListener != null) {
                C2836e.m6398a(new C2807d(18, upgradeStateListener, 2, Boolean.valueOf(z)));
                return;
            }
            C2836e.m6398a(new C2807d(5, strToastCheckingUpgrade));
        } catch (Exception e) {
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
        }
    }

    public static UpgradeInfo getUpgradeInfo() {
        try {
            C2829c.f5467a.f5468b = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
            if (C2829c.f5467a.f5468b != null) {
                return new UpgradeInfo(C2829c.f5467a.f5468b.f5455a);
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    public static synchronized void init(Context context, boolean z) {
        synchronized (Beta.class) {
            C2903an.m6857a("Beta init start....", new Object[0]);
            C2885ac a = C2885ac.m6737a();
            int i = instance.f5300id;
            int i2 = C2808e.f5337a + 1;
            C2808e.f5337a = i2;
            a.mo34483a(i, i2);
            if (TextUtils.isEmpty(initProcessName)) {
                initProcessName = context.getPackageName();
            }
            C2903an.m6857a("Beta will init at: %s", initProcessName);
            String str = C2851a.m6471b().f5660e;
            C2903an.m6857a("current process: %s", str);
            if (TextUtils.equals(initProcessName, str)) {
                C2808e eVar = C2808e.f5336E;
                if (!TextUtils.isEmpty(eVar.f5388v)) {
                    C2903an.m6864d("Beta has been initialized [apkMD5 : %s]", eVar.f5388v);
                    return;
                }
                C2903an.m6857a("current upgrade sdk version:1.4.0", new Object[0]);
                eVar.f5341D = z;
                if (upgradeCheckPeriod < 0) {
                    C2903an.m6864d("upgradeCheckPeriod cannot be negative", new Object[0]);
                } else {
                    eVar.f5369c = upgradeCheckPeriod;
                    C2903an.m6857a("setted upgradeCheckPeriod: %d", Long.valueOf(upgradeCheckPeriod));
                }
                if (initDelay < 0) {
                    C2903an.m6864d("initDelay cannot be negative", new Object[0]);
                } else {
                    eVar.f5368b = initDelay;
                    C2903an.m6857a("setted initDelay: %d", Long.valueOf(initDelay));
                }
                if (smallIconId != 0) {
                    try {
                        if (context.getResources().getDrawable(smallIconId) != null) {
                            eVar.f5372f = smallIconId;
                            C2903an.m6857a("setted smallIconId: %d", Integer.valueOf(smallIconId));
                        }
                    } catch (Exception e) {
                        C2903an.m6865e("smallIconId is not available:\n %s", e.toString());
                    }
                }
                if (largeIconId != 0) {
                    try {
                        if (context.getResources().getDrawable(largeIconId) != null) {
                            eVar.f5373g = largeIconId;
                            C2903an.m6857a("setted largeIconId: %d", Integer.valueOf(largeIconId));
                        }
                    } catch (Exception e2) {
                        C2903an.m6865e("largeIconId is not available:\n %s", e2.toString());
                    }
                }
                if (defaultBannerId != 0) {
                    try {
                        if (context.getResources().getDrawable(defaultBannerId) != null) {
                            eVar.f5374h = defaultBannerId;
                            C2903an.m6857a("setted defaultBannerId: %d", Integer.valueOf(defaultBannerId));
                        }
                    } catch (Exception e3) {
                        C2903an.m6865e("defaultBannerId is not available:\n %s", e3.toString());
                    }
                }
                if (upgradeDialogLayoutId != 0) {
                    try {
                        XmlResourceParser layout = context.getResources().getLayout(upgradeDialogLayoutId);
                        if (layout != null) {
                            eVar.f5375i = upgradeDialogLayoutId;
                            C2903an.m6857a("setted upgradeDialogLayoutId: %d", Integer.valueOf(upgradeDialogLayoutId));
                            layout.close();
                        }
                    } catch (Exception e4) {
                        C2903an.m6865e("upgradeDialogLayoutId is not available:\n %s", e4.toString());
                    }
                }
                if (tipsDialogLayoutId != 0) {
                    try {
                        XmlResourceParser layout2 = context.getResources().getLayout(tipsDialogLayoutId);
                        if (layout2 != null) {
                            eVar.f5376j = tipsDialogLayoutId;
                            C2903an.m6857a("setted tipsDialogLayoutId: %d", Integer.valueOf(tipsDialogLayoutId));
                            layout2.close();
                        }
                    } catch (Exception e5) {
                        C2903an.m6865e("tipsDialogLayoutId is not available:\n %s", e5.toString());
                    }
                }
                if (upgradeDialogLifecycleListener != null) {
                    try {
                        eVar.f5377k = upgradeDialogLifecycleListener;
                        C2903an.m6857a("setted upgradeDialogLifecycleListener:%s" + upgradeDialogLifecycleListener, new Object[0]);
                    } catch (Exception e6) {
                        C2903an.m6865e("upgradeDialogLifecycleListener is not available:\n %", e6.toString());
                    }
                }
                if (canShowUpgradeActs != null && !canShowUpgradeActs.isEmpty()) {
                    for (Class cls : canShowUpgradeActs) {
                        if (cls != null) {
                            eVar.f5379m.add(cls);
                        }
                    }
                    C2903an.m6857a("setted canShowUpgradeActs: %s", eVar.f5379m);
                }
                if (canNotShowUpgradeActs != null && !canNotShowUpgradeActs.isEmpty()) {
                    for (Class cls2 : canNotShowUpgradeActs) {
                        if (cls2 != null) {
                            eVar.f5380n.add(cls2);
                        }
                    }
                    C2903an.m6857a("setted canNotShowUpgradeActs: %s", eVar.f5380n);
                }
                eVar.f5370d = autoCheckUpgrade;
                Object[] objArr = new Object[1];
                objArr[0] = eVar.f5370d ? "is opened" : "is closed";
                C2903an.m6857a("autoCheckUpgrade %s", objArr);
                eVar.f5366ad = autoInstallApk;
                Object[] objArr2 = new Object[1];
                objArr2[0] = eVar.f5366ad ? "is opened" : "is closed";
                C2903an.m6857a("autoInstallApk %s", objArr2);
                eVar.f5356T = autoDownloadOn4g;
                Object[] objArr3 = new Object[1];
                objArr3[0] = eVar.f5356T ? "is opened" : "is closed";
                C2903an.m6857a("autoDownloadOn4g %s", objArr3);
                eVar.f5371e = showInterruptedStrategy;
                Object[] objArr4 = new Object[1];
                objArr4[0] = eVar.f5371e ? "is opened" : "is closed";
                C2903an.m6857a("showInterruptedStrategy %s", objArr4);
                Object[] objArr5 = new Object[1];
                objArr5[0] = upgradeListener != null ? "is opened" : "is closed";
                C2903an.m6857a("isDIY %s", objArr5);
                if (storageDir != null) {
                    if (storageDir.exists() || storageDir.mkdirs()) {
                        eVar.f5378l = storageDir;
                        C2903an.m6857a("setted storageDir: %s", storageDir.getAbsolutePath());
                    } else {
                        C2903an.m6857a("storageDir is not exists: %s", storageDir.getAbsolutePath());
                    }
                }
                if (eVar.f5382p == null) {
                    eVar.f5382p = C2952s.f6238a;
                }
                if (TextUtils.isEmpty(eVar.f5387u)) {
                    eVar.f5387u = C2851a.m6471b().mo34307f();
                }
                eVar.f5354R = enableNotification;
                C2903an.m6857a("enableNotification %s", enableNotification + "");
                eVar.f5355S = autoDownloadOnWifi;
                C2903an.m6857a("autoDownloadOnWifi %s", autoDownloadOnWifi + "");
                eVar.f5357U = canShowApkInfo;
                C2903an.m6857a("canShowApkInfo %s", canShowApkInfo + "");
                eVar.f5358V = canAutoPatch;
                C2903an.m6857a("canAutoPatch %s", canAutoPatch + "");
                eVar.f5359W = betaPatchListener;
                eVar.f5390x = appVersionName;
                eVar.f5389w = appVersionCode;
                eVar.f5360X = canNotifyUserRestart;
                C2903an.m6857a("canNotifyUserRestart %s", canNotifyUserRestart + "");
                eVar.f5361Y = canAutoDownloadPatch;
                C2903an.m6857a("canAutoDownloadPatch %s", canAutoDownloadPatch + "");
                eVar.f5362Z = enableHotfix;
                C2903an.m6857a("enableHotfix %s", enableHotfix + "");
                TinkerManager.setPatchRestartOnScreenOff(setPatchRestartOnScreenOff);
                C2903an.m6857a("setPatchRestartOnScreenOff %s", setPatchRestartOnScreenOff + "");
                if (soBlackList != null && !soBlackList.isEmpty()) {
                    for (String str2 : soBlackList) {
                        if (str2 != null) {
                            eVar.f5363aa.add(str2);
                        }
                    }
                    C2903an.m6857a("setted soBlackList: %s", eVar.f5363aa);
                }
                if (appChannel != null) {
                    eVar.f5352P = appChannel;
                    C2903an.m6857a("Beta channel %s", appChannel);
                }
                eVar.mo34055a(context);
                ResBean.f5325a = (ResBean) C2804a.m6261a("rb.bch", ResBean.CREATOR);
                if (ResBean.f5325a == null) {
                    ResBean.f5325a = new ResBean();
                }
                C2829c.f5467a.f5471e = upgradeListener;
                C2829c.f5467a.f5472f = upgradeStateListener;
                C2829c.f5467a.f5470d = downloadListener;
                if (!(getStrategyTask() == null || downloadListener == null)) {
                    getStrategyTask().addListener(C2829c.f5467a.f5470d);
                }
                if (enableHotfix) {
                    C2903an.m6857a("enableHotfix %s", "1");
                    C2908aq.m6931b("D4", "1");
                    C2950r.m7139a(context);
                }
                Resources resources = context.getResources();
                DisplayMetrics displayMetrics = resources.getDisplayMetrics();
                Configuration configuration = resources.getConfiguration();
                configuration.locale = Locale.getDefault();
                if (configuration.locale.equals(Locale.US) || configuration.locale.equals(Locale.ENGLISH)) {
                    strToastYourAreTheLatestVersion = context.getResources().getString(C2798R.string.strToastYourAreTheLatestVersion);
                    strToastCheckUpgradeError = context.getResources().getString(C2798R.string.strToastCheckUpgradeError);
                    strToastCheckingUpgrade = context.getResources().getString(C2798R.string.strToastCheckingUpgrade);
                    strNotificationDownloading = context.getResources().getString(C2798R.string.strNotificationDownloading);
                    strNotificationClickToView = context.getResources().getString(C2798R.string.strNotificationClickToView);
                    strNotificationClickToInstall = context.getResources().getString(C2798R.string.strNotificationClickToInstall);
                    strNotificationClickToContinue = context.getResources().getString(C2798R.string.strNotificationClickToContinue);
                    strNotificationClickToRetry = context.getResources().getString(C2798R.string.strNotificationClickToRetry);
                    strNotificationDownloadSucc = context.getResources().getString(C2798R.string.strNotificationDownloadSucc);
                    strNotificationDownloadError = context.getResources().getString(C2798R.string.strNotificationDownloadError);
                    strNotificationHaveNewVersion = context.getResources().getString(C2798R.string.strNotificationHaveNewVersion);
                    strNetworkTipsMessage = context.getResources().getString(C2798R.string.strNetworkTipsMessage);
                    strNetworkTipsTitle = context.getResources().getString(C2798R.string.strNetworkTipsTitle);
                    strNetworkTipsConfirmBtn = context.getResources().getString(C2798R.string.strNetworkTipsConfirmBtn);
                    strNetworkTipsCancelBtn = context.getResources().getString(C2798R.string.strNetworkTipsCancelBtn);
                    strUpgradeDialogVersionLabel = context.getResources().getString(C2798R.string.strUpgradeDialogVersionLabel);
                    strUpgradeDialogFileSizeLabel = context.getResources().getString(C2798R.string.strUpgradeDialogFileSizeLabel);
                    strUpgradeDialogUpdateTimeLabel = context.getResources().getString(C2798R.string.strUpgradeDialogUpdateTimeLabel);
                    strUpgradeDialogFeatureLabel = context.getResources().getString(C2798R.string.strUpgradeDialogFeatureLabel);
                    strUpgradeDialogUpgradeBtn = context.getResources().getString(C2798R.string.strUpgradeDialogUpgradeBtn);
                    strUpgradeDialogInstallBtn = context.getResources().getString(C2798R.string.strUpgradeDialogInstallBtn);
                    strUpgradeDialogRetryBtn = context.getResources().getString(C2798R.string.strUpgradeDialogRetryBtn);
                    strUpgradeDialogContinueBtn = context.getResources().getString(C2798R.string.strUpgradeDialogContinueBtn);
                    strUpgradeDialogCancelBtn = context.getResources().getString(C2798R.string.strUpgradeDialogCancelBtn);
                }
                resources.updateConfiguration(configuration, displayMetrics);
                C2901am.m6848a().mo34548a(new C2807d(1, new Object[0]), eVar.f5368b);
                C2885ac a2 = C2885ac.m6737a();
                int i3 = instance.f5300id;
                int i4 = C2808e.f5337a - 1;
                C2808e.f5337a = i4;
                a2.mo34483a(i3, i4);
                C2903an.m6857a("Beta init finished...", new Object[0]);
            }
        }
    }

    public synchronized void init(Context context, boolean z, BuglyStrategy buglyStrategy) {
        C2851a.m6471b().mo34302c("G10", BuildConfig.VERSION_NAME);
        if (autoInit) {
            init(context, z);
        }
    }

    public String[] getTables() {
        return new String[]{"dl_1002", "ge_1002", "st_1002"};
    }

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0130 A[Catch:{ all -> 0x0141 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0138 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDbUpgrade(android.database.sqlite.SQLiteDatabase r21, int r22, int r23) {
        /*
            r20 = this;
            r9 = r21
            java.lang.String r10 = "st_1002"
            java.lang.String r11 = "_dt"
            java.lang.String r12 = "_tm"
            java.lang.String r13 = " , "
            java.lang.String r14 = "_tp"
            java.lang.String r15 = " "
            java.lang.String r8 = "_id"
            r6 = r22
            r7 = r23
        L_0x0014:
            if (r6 >= r7) goto L_0x0148
            r0 = 10
            if (r6 == r0) goto L_0x0021
            r19 = r6
            r16 = r13
            r13 = r8
            goto L_0x0138
        L_0x0021:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0097 }
            r0.<init>()     // Catch:{ all -> 0x0097 }
            r1 = 0
            r0.setLength(r1)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = " CREATE TABLE  IF NOT EXISTS "
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r10)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = " ( "
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r8)     // Catch:{ all -> 0x0097 }
            r0.append(r15)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "integer"
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r13)     // Catch:{ all -> 0x0097 }
            r0.append(r14)     // Catch:{ all -> 0x0097 }
            r0.append(r15)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "text"
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r13)     // Catch:{ all -> 0x0097 }
            r0.append(r12)     // Catch:{ all -> 0x0097 }
            r0.append(r15)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "int"
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r13)     // Catch:{ all -> 0x0097 }
            r0.append(r11)     // Catch:{ all -> 0x0097 }
            r0.append(r15)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "blob"
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = ",primary key("
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r8)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = ","
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            r0.append(r14)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = " )) "
            r0.append(r2)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "create %s"
            r3 = 1
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0097 }
            java.lang.String r4 = r0.toString()     // Catch:{ all -> 0x0097 }
            r3[r1] = r4     // Catch:{ all -> 0x0097 }
            com.tencent.bugly.proguard.C2903an.m6863c(r2, r3)     // Catch:{ all -> 0x0097 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0097 }
            r9.execSQL(r0)     // Catch:{ all -> 0x0097 }
            goto L_0x00a1
        L_0x0097:
            r0 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6861b(r0)
            if (r1 != 0) goto L_0x00a1
            r0.printStackTrace()
        L_0x00a1:
            r5 = 0
            java.lang.String r4 = "_id = 1002"
            java.lang.String r2 = "t_pf"
            r3 = 0
            r0 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r1 = r21
            r5 = r0
            r19 = r6
            r6 = r16
            r7 = r17
            r16 = r13
            r13 = r8
            r8 = r18
            android.database.Cursor r5 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x011f }
            if (r5 != 0) goto L_0x00c8
            if (r5 == 0) goto L_0x00c7
            r5.close()
        L_0x00c7:
            return
        L_0x00c8:
            boolean r0 = r5.moveToNext()     // Catch:{ all -> 0x011d }
            if (r0 == 0) goto L_0x011a
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x011d }
            r0.<init>()     // Catch:{ all -> 0x011d }
            int r1 = r5.getColumnIndex(r13)     // Catch:{ all -> 0x011d }
            long r1 = r5.getLong(r1)     // Catch:{ all -> 0x011d }
            r3 = 0
            int r6 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r6 <= 0) goto L_0x00f0
            int r1 = r5.getColumnIndex(r13)     // Catch:{ all -> 0x011d }
            long r1 = r5.getLong(r1)     // Catch:{ all -> 0x011d }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x011d }
            r0.put(r13, r1)     // Catch:{ all -> 0x011d }
        L_0x00f0:
            int r1 = r5.getColumnIndex(r12)     // Catch:{ all -> 0x011d }
            long r1 = r5.getLong(r1)     // Catch:{ all -> 0x011d }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x011d }
            r0.put(r12, r1)     // Catch:{ all -> 0x011d }
            int r1 = r5.getColumnIndex(r14)     // Catch:{ all -> 0x011d }
            java.lang.String r1 = r5.getString(r1)     // Catch:{ all -> 0x011d }
            r0.put(r14, r1)     // Catch:{ all -> 0x011d }
            int r1 = r5.getColumnIndex(r11)     // Catch:{ all -> 0x011d }
            byte[] r1 = r5.getBlob(r1)     // Catch:{ all -> 0x011d }
            r0.put(r11, r1)     // Catch:{ all -> 0x011d }
            r1 = 0
            r9.replace(r10, r1, r0)     // Catch:{ all -> 0x011d }
            goto L_0x00c8
        L_0x011a:
            if (r5 == 0) goto L_0x0138
            goto L_0x0135
        L_0x011d:
            r0 = move-exception
            goto L_0x012a
        L_0x011f:
            r0 = move-exception
            r1 = 0
            r5 = r1
            goto L_0x012a
        L_0x0123:
            r0 = move-exception
            r1 = r5
            r19 = r6
            r16 = r13
            r13 = r8
        L_0x012a:
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6861b(r0)     // Catch:{ all -> 0x0141 }
            if (r1 != 0) goto L_0x0133
            r0.printStackTrace()     // Catch:{ all -> 0x0141 }
        L_0x0133:
            if (r5 == 0) goto L_0x0138
        L_0x0135:
            r5.close()
        L_0x0138:
            int r6 = r19 + 1
            r7 = r23
            r8 = r13
            r13 = r16
            goto L_0x0014
        L_0x0141:
            r0 = move-exception
            if (r5 == 0) goto L_0x0147
            r5.close()
        L_0x0147:
            throw r0
        L_0x0148:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.Beta.onDbUpgrade(android.database.sqlite.SQLiteDatabase, int, int):void");
    }

    public static void registerDownloadListener(DownloadListener downloadListener2) {
        C2808e.f5336E.f5383q = downloadListener2;
        if (C2808e.f5336E.f5383q != null && C2829c.f5467a.f5469c != null) {
            C2829c.f5467a.f5469c.addListener(downloadListener2);
        }
    }

    public static void unregisterDownloadListener() {
        if (C2829c.f5467a.f5469c != null) {
            C2829c.f5467a.f5469c.removeListener(C2808e.f5336E.f5383q);
        }
        C2808e.f5336E.f5383q = null;
    }

    public static DownloadTask startDownload() {
        if (C2829c.f5467a.f5474h == null || C2829c.f5467a.f5474h.f5335b[0] != C2829c.f5467a.f5469c) {
            C2829c.f5467a.f5474h = new C2807d(13, C2829c.f5467a.f5469c, C2829c.f5467a.f5468b);
        }
        C2829c.f5467a.f5474h.run();
        return C2829c.f5467a.f5469c;
    }

    public static void cancelDownload() {
        if (!(C2829c.f5467a.f5475i != null && C2829c.f5467a.f5475i.f5335b[0] == C2829c.f5467a.f5469c && C2829c.f5467a.f5475i.f5335b[1] == C2829c.f5467a.f5468b && ((Boolean) C2829c.f5467a.f5475i.f5335b[2]).booleanValue() == C2829c.f5467a.f5473g)) {
            C2829c.f5467a.f5475i = new C2807d(14, C2829c.f5467a.f5469c, C2829c.f5467a.f5468b, Boolean.valueOf(C2829c.f5467a.f5473g));
        }
        C2829c.f5467a.f5475i.run();
    }

    public static DownloadTask getStrategyTask() {
        if (f5306a == null) {
            C2829c.f5467a.f5468b = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
            if (C2829c.f5467a.f5468b != null) {
                f5306a = C2808e.f5336E.f5382p.mo34042a(C2829c.f5467a.f5468b.f5455a.f6287f.f6247b, C2808e.f5336E.f5386t.getAbsolutePath(), null, C2829c.f5467a.f5468b.f5455a.f6287f.f6246a);
                C2829c.f5467a.f5469c = f5306a;
            }
        }
        return C2829c.f5467a.f5469c;
    }

    public static synchronized void showUpgradeDialog(String str, int i, String str2, long j, int i2, int i3, String str3, String str4, long j2, String str5, String str6, int i4, DownloadListener downloadListener2, Runnable runnable, Runnable runnable2, boolean z) {
        C2959y yVar;
        synchronized (Beta.class) {
            HashMap hashMap = new HashMap();
            hashMap.put("IMG_title", str6);
            hashMap.put("VAL_style", String.valueOf(i4));
            C2959y yVar2 = r2;
            C2959y yVar3 = new C2959y(str, str2, j, 0, new C2956v(C2808e.f5336E.f5387u, (byte) 1, i3, str3, i2, "", 1, "", str5, BuildConfig.VERSION_NAME, ""), new C2955u(str5, str4, "", j2, ""), (byte) i, 0, 0, null, "", hashMap, null, 1, System.currentTimeMillis(), 1);
            if (f5306a != null && !f5306a.getDownloadUrl().equals(str4)) {
                f5306a.delete(true);
                f5306a = null;
            }
            if (f5306a == null) {
                yVar = yVar2;
                f5306a = C2808e.f5336E.f5382p.mo34042a(yVar.f6287f.f6247b, C2808e.f5336E.f5386t.getAbsolutePath(), null, yVar.f6287f.f6246a);
            } else {
                yVar = yVar2;
            }
            f5306a.addListener(downloadListener2);
            C2826h.f5440v.mo34164a(yVar, f5306a);
            C2826h.f5440v.f5447r = runnable;
            C2826h.f5440v.f5448s = runnable2;
            C2809f.f5393a.mo34057a(C2808e.f5336E.f5382p, yVar.f6293l);
            boolean z2 = false;
            if (z) {
                C2809f fVar = C2809f.f5393a;
                Object[] objArr = new Object[2];
                objArr[0] = C2826h.f5440v;
                if (yVar.f6288g == 2) {
                    z2 = true;
                }
                objArr[1] = Boolean.valueOf(z2);
                fVar.mo34059a(new C2807d(2, objArr), 3000);
            } else {
                C2809f fVar2 = C2809f.f5393a;
                Object[] objArr2 = new Object[2];
                objArr2[0] = C2826h.f5440v;
                if (yVar.f6288g == 2) {
                    z2 = true;
                }
                objArr2[1] = Boolean.valueOf(z2);
                fVar2.mo34058a(new C2807d(2, objArr2));
            }
        }
    }

    public static synchronized void onUpgradeReceived(String str, int i, String str2, long j, int i2, int i3, String str3, String str4, long j2, String str5, String str6, int i4, int i5, long j3, String str7, boolean z, boolean z2, int i6, String str8, long j4) {
        synchronized (Beta.class) {
            HashMap hashMap = new HashMap();
            hashMap.put("IMG_title", str6);
            hashMap.put("VAL_style", String.valueOf(i4));
            C2829c.f5467a.mo34191a(z, z2, i6, new C2959y(str, str2, j, 0, new C2956v(C2808e.f5336E.f5387u, (byte) 1, i3, str3, i2, "", 1, "", str5, "", ""), new C2955u(str5, str4, "", j2, ""), (byte) i, i5, j3, null, "", hashMap, str7, 1, j4, 1), str8 == null ? "" : str8);
        }
    }

    public static synchronized C2959y getUpgradeStrategy() {
        synchronized (Beta.class) {
            C2829c.f5467a.f5468b = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
            try {
                if (C2829c.f5467a.f5468b != null) {
                    C2959y yVar = (C2959y) C2829c.f5467a.f5468b.f5455a.clone();
                    return yVar;
                }
            } catch (Exception unused) {
            }
            return null;
        }
    }

    public static synchronized void installApk(File file) {
        synchronized (Beta.class) {
            try {
                C2959y upgradeStrategy = getUpgradeStrategy();
                if (upgradeStrategy != null && C2804a.m6266a(C2808e.f5336E.f5385s, file, upgradeStrategy.f6287f.f6246a)) {
                    C2947p.f6229a.mo34667a(new C2957w("install", System.currentTimeMillis(), (byte) 0, 0, upgradeStrategy.f6286e, upgradeStrategy.f6294m, upgradeStrategy.f6297p, null));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public static synchronized void saveInstallEvent(boolean z) {
        synchronized (Beta.class) {
            try {
                C2959y upgradeStrategy = getUpgradeStrategy();
                if (upgradeStrategy != null && z) {
                    C2804a.m6264a("installApkMd5", upgradeStrategy.f6287f.f6246a);
                    C2947p.f6229a.mo34667a(new C2957w("install", System.currentTimeMillis(), (byte) 0, 0, upgradeStrategy.f6286e, upgradeStrategy.f6294m, upgradeStrategy.f6297p, null));
                    C2903an.m6857a("安装事件保存成功", new Object[0]);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public static void applyTinkerPatch(Context context, String str) {
        TinkerManager.getInstance().applyPatch(context, str);
    }

    public static void downloadPatch() {
        C2948q.f6230a.f6231b = C2948q.f6230a.mo34673a(null);
        try {
            if (C2948q.f6230a.f6231b != null) {
                C2948q.f6230a.mo34674a(0, C2948q.f6230a.f6231b.f5455a, true);
            }
        } catch (Exception unused) {
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.tinker.TinkerManager.applyPatch(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.tinker.TinkerManager.applyPatch(android.content.Context, java.lang.String):void
      com.tencent.bugly.beta.tinker.TinkerManager.applyPatch(java.lang.String, boolean):void */
    public static void applyDownloadedPatch() {
        if (new File(C2808e.f5336E.f5344H.getAbsolutePath()).exists()) {
            TinkerManager.getInstance().applyPatch(C2808e.f5336E.f5344H.getAbsolutePath(), true);
        } else {
            C2903an.m6862c(Beta.class, "[applyDownloadedPatch] patch file not exist", new Object[0]);
        }
    }

    public static void installTinker() {
        enableHotfix = true;
        installTinker(TinkerApplicationLike.getTinkerPatchApplicationLike());
    }

    public static void installTinker(Object obj) {
        enableHotfix = true;
        TinkerManager.installTinker(obj);
    }

    public static void installTinker(Object obj, Object obj2, Object obj3, Object obj4, TinkerManager.TinkerPatchResultListener tinkerPatchResultListener, Object obj5) {
        enableHotfix = true;
        TinkerManager.installTinker(obj, obj2, obj3, obj4, tinkerPatchResultListener, obj5);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    public static void cleanTinkerPatch(boolean z) {
        C2804a.m6265a("IS_PATCH_ROLL_BACK", false);
        TinkerManager.getInstance().cleanPatch(z);
    }

    public static void loadArmLibrary(Context context, String str) {
        TinkerManager.loadArmLibrary(context, str);
    }

    public static void loadArmV7Library(Context context, String str) {
        TinkerManager.loadArmV7Library(context, str);
    }

    public static void loadLibraryFromTinker(Context context, String str, String str2) {
        TinkerManager.loadLibraryFromTinker(context, str, str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    public static void loadLibrary(String str) {
        boolean z;
        C2808e eVar = C2808e.f5336E;
        if (str != null) {
            try {
                if (!str.isEmpty()) {
                    if (C2804a.m6273b("LoadSoFileResult", true)) {
                        C2804a.m6265a("LoadSoFileResult", false);
                        String b = C2804a.m6272b(str, "");
                        boolean b2 = C2804a.m6273b("PatchResult", false);
                        if (TextUtils.isEmpty(b) || !b2) {
                            z = false;
                        } else {
                            z = TinkerManager.loadLibraryFromTinker(eVar.f5385s, "lib/" + b, str);
                        }
                        if (!z) {
                            System.loadLibrary(str);
                        }
                        C2804a.m6265a("LoadSoFileResult", true);
                        return;
                    }
                    System.loadLibrary(str);
                    C2804a.m6265a("IS_PATCH_ROLL_BACK", true);
                    cleanTinkerPatch(true);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                C2804a.m6265a("LoadSoFileResult", false);
                return;
            }
        }
        C2903an.m6865e("libName is invalid", new Object[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    public static void unInit() {
        if (C2804a.m6273b("IS_PATCH_ROLL_BACK", false)) {
            C2804a.m6265a("IS_PATCH_ROLL_BACK", false);
            TinkerManager.getInstance().cleanPatch(true);
        }
    }
}
