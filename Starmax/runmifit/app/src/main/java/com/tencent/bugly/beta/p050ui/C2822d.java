package com.tencent.bugly.beta.p050ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import com.tamic.novate.util.FileUtil;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2806c;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.proguard.C2903an;

/* renamed from: com.tencent.bugly.beta.ui.d */
/* compiled from: BUGLY */
public class C2822d implements ViewTreeObserver.OnPreDrawListener {

    /* renamed from: a */
    final int f5429a;

    /* renamed from: b */
    final Object[] f5430b;

    /* renamed from: c */
    long f5431c;

    /* renamed from: d */
    StringBuilder f5432d;

    public C2822d(int i, Object... objArr) {
        StringBuilder sb = new StringBuilder();
        sb.append("loading");
        this.f5432d = sb;
        this.f5429a = i;
        this.f5430b = objArr;
    }

    public boolean onPreDraw() {
        int i;
        int i2;
        try {
            int i3 = this.f5429a;
            if (i3 == 1) {
                C2826h hVar = (C2826h) this.f5430b[0];
                TextView textView = (TextView) this.f5430b[1];
                Bitmap bitmap = (Bitmap) this.f5430b[2];
                int intValue = ((Integer) this.f5430b[3]).intValue();
                int measuredWidth = textView.getMeasuredWidth();
                double d = (double) measuredWidth;
                Double.isNaN(d);
                int i4 = (int) (d * 0.42857142857142855d);
                textView.setHeight(i4);
                if (hVar.f5450u == null) {
                    if (intValue == 2) {
                        hVar.f5450u = C2804a.m6260a(bitmap, measuredWidth, i4, (float) C2804a.m6257a(C2808e.f5336E.f5385s, 6.0f));
                    } else {
                        hVar.f5450u = C2804a.m6260a(bitmap, measuredWidth, i4, (float) C2804a.m6257a(C2808e.f5336E.f5385s, 0.0f));
                    }
                    if (hVar.f5450u != null) {
                        textView.setText("");
                        textView.setBackgroundDrawable(hVar.f5450u);
                        textView.getViewTreeObserver().removeOnPreDrawListener(this);
                        return true;
                    }
                }
                long currentTimeMillis = System.currentTimeMillis();
                if (currentTimeMillis - this.f5431c <= 300) {
                    return true;
                }
                this.f5431c = currentTimeMillis;
                if (this.f5432d.length() > 9) {
                    StringBuilder sb = new StringBuilder();
                    sb.append("loading");
                    this.f5432d = sb;
                } else {
                    this.f5432d.append(FileUtil.HIDDEN_PREFIX);
                }
                textView.setText(this.f5432d.toString());
                return true;
            } else if (i3 != 2) {
                if (i3 != 3) {
                    return false;
                }
                ViewGroup viewGroup = (ViewGroup) this.f5430b[0];
                if (viewGroup.getMeasuredHeight() > C2804a.m6257a((Context) this.f5430b[1], 158.0f)) {
                    ViewGroup.LayoutParams layoutParams = viewGroup.getLayoutParams();
                    layoutParams.height = C2804a.m6257a((Context) this.f5430b[1], 200.0f);
                    viewGroup.setLayoutParams(layoutParams);
                }
                return true;
            } else if (((Integer) this.f5430b[2]).intValue() <= 0) {
                return true;
            } else {
                int intValue2 = ((Integer) this.f5430b[0]).intValue();
                TextView textView2 = (TextView) this.f5430b[1];
                int measuredWidth2 = textView2.getMeasuredWidth();
                int measuredHeight = textView2.getMeasuredHeight();
                int i5 = (int) (((float) (C2808e.f5336E.f5339B.widthPixels * C2808e.f5336E.f5339B.heightPixels)) * 0.4f);
                if (measuredWidth2 == 0 || measuredHeight == 0 || measuredWidth2 * measuredHeight > i5) {
                    return true;
                }
                this.f5430b[2] = 0;
                Paint paint = new Paint();
                paint.setStyle(Paint.Style.FILL);
                paint.setAntiAlias(true);
                if (intValue2 == 2) {
                    i2 = 8;
                    i = 7;
                } else {
                    i2 = 0;
                    i = 0;
                }
                paint.setColor(-3355444);
                Bitmap createBitmap = Bitmap.createBitmap(measuredWidth2, measuredHeight, Bitmap.Config.ARGB_8888);
                Canvas canvas = new Canvas(createBitmap);
                float f = (float) measuredWidth2;
                float f2 = (float) measuredHeight;
                RectF rectF = new RectF(0.0f, 0.0f, f, f2);
                float f3 = (float) i2;
                canvas.drawRoundRect(rectF, f3, f3, paint);
                paint.setColor(-1);
                float f4 = (float) i;
                canvas.drawRoundRect(new RectF(2.0f, 2.0f, f - 2.0f, f2 - 2.0f), f4, f4, paint);
                paint.setColor(-3355444);
                Bitmap createBitmap2 = Bitmap.createBitmap(measuredWidth2, measuredHeight, Bitmap.Config.ARGB_8888);
                new Canvas(createBitmap2).drawRoundRect(rectF, f3, f3, paint);
                BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
                BitmapDrawable bitmapDrawable2 = new BitmapDrawable(createBitmap2);
                textView2.setBackgroundDrawable(bitmapDrawable);
                textView2.setOnTouchListener(new C2806c(1, bitmapDrawable2, bitmapDrawable));
                return true;
            }
        } catch (Exception e) {
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
