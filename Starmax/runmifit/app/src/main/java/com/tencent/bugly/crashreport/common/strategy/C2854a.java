package com.tencent.bugly.crashreport.common.strategy;

import android.content.Context;
import com.baidu.mobstat.Config;
import com.tencent.bugly.BUGLY;
import com.tencent.bugly.crashreport.biz.C2847b;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.proguard.C2888ad;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2892ag;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import com.tencent.bugly.proguard.C2930bk;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.common.strategy.a */
/* compiled from: BUGLY */
public class C2854a {

    /* renamed from: a */
    public static int f5712a = 1000;

    /* renamed from: b */
    public static long f5713b = 259200000;

    /* renamed from: c */
    private static C2854a f5714c;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static String f5715i;

    /* renamed from: d */
    private final List<BUGLY> f5716d;

    /* renamed from: e */
    private final C2901am f5717e;

    /* renamed from: f */
    private final StrategyBean f5718f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public StrategyBean f5719g = null;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Context f5720h;

    protected C2854a(Context context, List<BUGLY> list) {
        this.f5720h = context;
        this.f5718f = new StrategyBean();
        this.f5716d = list;
        this.f5717e = C2901am.m6848a();
    }

    /* renamed from: a */
    public static synchronized C2854a m6575a(Context context, List<BUGLY> list) {
        C2854a aVar;
        synchronized (C2854a.class) {
            if (f5714c == null) {
                f5714c = new C2854a(context, list);
            }
            aVar = f5714c;
        }
        return aVar;
    }

    /* renamed from: a */
    public void mo34336a(long j) {
        this.f5717e.mo34548a(new Thread() {
            /* class com.tencent.bugly.crashreport.common.strategy.C2854a.C28551 */

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]>
             arg types: [int, ?[OBJECT, ARRAY], int]
             candidates:
              com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
              com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, com.tencent.bugly.proguard.ad):java.util.Map
              com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
              com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]> */
            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void
             arg types: [com.tencent.bugly.crashreport.common.strategy.StrategyBean, int]
             candidates:
              com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.a, com.tencent.bugly.crashreport.common.strategy.StrategyBean):com.tencent.bugly.crashreport.common.strategy.StrategyBean
              com.tencent.bugly.crashreport.common.strategy.a.a(android.content.Context, java.util.List<com.tencent.bugly.a>):com.tencent.bugly.crashreport.common.strategy.a
              com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void */
            public void run() {
                try {
                    Map<String, byte[]> a = C2889ae.m6757a().mo34494a(C2854a.f5712a, (C2888ad) null, true);
                    if (a != null) {
                        byte[] bArr = a.get(Config.DEVICE_PART);
                        byte[] bArr2 = a.get("gateway");
                        if (bArr != null) {
                            C2851a.m6470a(C2854a.this.f5720h).mo34306e(new String(bArr));
                        }
                        if (bArr2 != null) {
                            C2851a.m6470a(C2854a.this.f5720h).mo34304d(new String(bArr2));
                        }
                    }
                    StrategyBean unused = C2854a.this.f5719g = C2854a.this.mo34341d();
                    if (C2854a.this.f5719g != null && !C2908aq.m6915a(C2854a.f5715i) && C2908aq.m6940c(C2854a.f5715i)) {
                        C2854a.this.f5719g.f5704r = C2854a.f5715i;
                        C2854a.this.f5719g.f5705s = C2854a.f5715i;
                    }
                } catch (Throwable th) {
                    if (!C2903an.m6858a(th)) {
                        th.printStackTrace();
                    }
                }
                C2854a aVar = C2854a.this;
                aVar.mo34337a(aVar.f5719g, false);
            }
        }, j);
    }

    /* renamed from: a */
    public static synchronized C2854a m6574a() {
        C2854a aVar;
        synchronized (C2854a.class) {
            aVar = f5714c;
        }
        return aVar;
    }

    /* renamed from: b */
    public synchronized boolean mo34339b() {
        return this.f5719g != null;
    }

    /* renamed from: c */
    public StrategyBean mo34340c() {
        StrategyBean strategyBean = this.f5719g;
        if (strategyBean != null) {
            return strategyBean;
        }
        return this.f5718f;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34337a(StrategyBean strategyBean, boolean z) {
        C2903an.m6863c("[Strategy] Notify %s", C2847b.class.getName());
        C2847b.m6433a(strategyBean, z);
        for (BUGLY aVar : this.f5716d) {
            try {
                C2903an.m6863c("[Strategy] Notify %s", aVar.getClass().getName());
                aVar.onServerStrategyChanged(strategyBean);
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    public static void m6576a(String str) {
        if (C2908aq.m6915a(str) || !C2908aq.m6940c(str)) {
            C2903an.m6864d("URL user set is invalid.", new Object[0]);
        } else {
            f5715i = str;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void
     arg types: [com.tencent.bugly.crashreport.common.strategy.StrategyBean, int]
     candidates:
      com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.a, com.tencent.bugly.crashreport.common.strategy.StrategyBean):com.tencent.bugly.crashreport.common.strategy.StrategyBean
      com.tencent.bugly.crashreport.common.strategy.a.a(android.content.Context, java.util.List<com.tencent.bugly.a>):com.tencent.bugly.crashreport.common.strategy.a
      com.tencent.bugly.crashreport.common.strategy.a.a(com.tencent.bugly.crashreport.common.strategy.StrategyBean, boolean):void */
    /* renamed from: a */
    public void mo34338a(C2930bk bkVar) {
        if (bkVar != null) {
            if (this.f5719g == null || bkVar.f6161h != this.f5719g.f5702p) {
                StrategyBean strategyBean = new StrategyBean();
                strategyBean.f5693g = bkVar.f6154a;
                strategyBean.f5695i = bkVar.f6156c;
                strategyBean.f5694h = bkVar.f6155b;
                if (C2908aq.m6915a(f5715i) || !C2908aq.m6940c(f5715i)) {
                    if (C2908aq.m6940c(bkVar.f6157d)) {
                        C2903an.m6863c("[Strategy] Upload url changes to %s", bkVar.f6157d);
                        strategyBean.f5704r = bkVar.f6157d;
                    }
                    if (C2908aq.m6940c(bkVar.f6158e)) {
                        C2903an.m6863c("[Strategy] Exception upload url changes to %s", bkVar.f6158e);
                        strategyBean.f5705s = bkVar.f6158e;
                    }
                }
                if (bkVar.f6159f != null && !C2908aq.m6915a(bkVar.f6159f.f6149a)) {
                    strategyBean.f5707u = bkVar.f6159f.f6149a;
                }
                if (bkVar.f6161h != 0) {
                    strategyBean.f5702p = bkVar.f6161h;
                }
                if (bkVar.f6160g != null && bkVar.f6160g.size() > 0) {
                    strategyBean.f5708v = bkVar.f6160g;
                    String str = bkVar.f6160g.get("B11");
                    if (str == null || !str.equals("1")) {
                        strategyBean.f5696j = false;
                    } else {
                        strategyBean.f5696j = true;
                    }
                    String str2 = bkVar.f6160g.get("B3");
                    if (str2 != null) {
                        strategyBean.f5711y = Long.valueOf(str2).longValue();
                    }
                    strategyBean.f5703q = (long) bkVar.f6165l;
                    strategyBean.f5710x = (long) bkVar.f6165l;
                    String str3 = bkVar.f6160g.get("B27");
                    if (str3 != null && str3.length() > 0) {
                        try {
                            int parseInt = Integer.parseInt(str3);
                            if (parseInt > 0) {
                                strategyBean.f5709w = parseInt;
                            }
                        } catch (Exception e) {
                            if (!C2903an.m6858a(e)) {
                                e.printStackTrace();
                            }
                        }
                    }
                    String str4 = bkVar.f6160g.get("B25");
                    if (str4 == null || !str4.equals("1")) {
                        strategyBean.f5698l = false;
                    } else {
                        strategyBean.f5698l = true;
                    }
                }
                C2903an.m6857a("[Strategy] enableCrashReport:%b, enableQuery:%b, enableUserInfo:%b, enableAnr:%b, enableBlock:%b, enableSession:%b, enableSessionTimer:%b, sessionOverTime:%d, enableCocos:%b, strategyLastUpdateTime:%d", Boolean.valueOf(strategyBean.f5693g), Boolean.valueOf(strategyBean.f5695i), Boolean.valueOf(strategyBean.f5694h), Boolean.valueOf(strategyBean.f5696j), Boolean.valueOf(strategyBean.f5697k), Boolean.valueOf(strategyBean.f5700n), Boolean.valueOf(strategyBean.f5701o), Long.valueOf(strategyBean.f5703q), Boolean.valueOf(strategyBean.f5698l), Long.valueOf(strategyBean.f5702p));
                this.f5719g = strategyBean;
                C2889ae.m6757a().mo34501b(2);
                C2892ag agVar = new C2892ag();
                agVar.f5965b = 2;
                agVar.f5964a = strategyBean.f5691e;
                agVar.f5968e = strategyBean.f5692f;
                agVar.f5970g = C2908aq.m6918a(strategyBean);
                C2889ae.m6757a().mo34498a(agVar);
                mo34337a(strategyBean, true);
            }
        }
    }

    /* renamed from: d */
    public StrategyBean mo34341d() {
        List<C2892ag> a = C2889ae.m6757a().mo34493a(2);
        if (a == null || a.size() <= 0) {
            return null;
        }
        C2892ag agVar = a.get(0);
        if (agVar.f5970g != null) {
            return (StrategyBean) C2908aq.m6896a(agVar.f5970g, StrategyBean.CREATOR);
        }
        return null;
    }
}
