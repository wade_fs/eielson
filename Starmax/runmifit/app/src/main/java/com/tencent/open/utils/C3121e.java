package com.tencent.open.utils;

import android.content.Context;
import java.io.File;

/* renamed from: com.tencent.open.utils.e */
/* compiled from: ProGuard */
public final class C3121e {

    /* renamed from: a */
    private static Context f6838a;

    /* renamed from: a */
    public static final Context m7727a() {
        Context context = f6838a;
        if (context == null) {
            return null;
        }
        return context;
    }

    /* renamed from: a */
    public static final void m7728a(Context context) {
        f6838a = context;
    }

    /* renamed from: b */
    public static final String m7729b() {
        if (m7727a() == null) {
            return "";
        }
        return m7727a().getPackageName();
    }

    /* renamed from: c */
    public static final File m7730c() {
        if (m7727a() == null) {
            return null;
        }
        return m7727a().getFilesDir();
    }
}
