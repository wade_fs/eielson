package com.tencent.stat.common;

import java.lang.reflect.Method;

/* renamed from: com.tencent.stat.common.c */
public class C3208c {

    /* renamed from: a */
    private static volatile int f7252a = -1;

    /* renamed from: a */
    public static String m7981a(String str) {
        try {
            Class<?> cls = Class.forName("android.os.SystemProperties");
            Method declaredMethod = cls.getDeclaredMethod("get", String.class);
            declaredMethod.setAccessible(true);
            return (String) declaredMethod.invoke(cls, str);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: a */
    public static boolean m7982a() {
        if (f7252a == -1) {
            try {
                if (!StatCommonHelper.isStringValid(m7981a("ro.miui.ui.version.code")) && !StatCommonHelper.isStringValid(m7981a(m7981a("ro.miui.ui.version.name")))) {
                    if (!StatCommonHelper.isStringValid(m7981a(m7981a("ro.miui.internal.storage")))) {
                        f7252a = 0;
                        return false;
                    }
                }
                f7252a = 1;
            } catch (Throwable unused) {
            }
            return false;
        } else if (f7252a == 1) {
            return true;
        } else {
            return false;
        }
    }
}
