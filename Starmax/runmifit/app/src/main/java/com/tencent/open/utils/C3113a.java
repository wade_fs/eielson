package com.tencent.open.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.ProtocolException;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Properties;
import java.util.zip.ZipException;

/* renamed from: com.tencent.open.utils.a */
/* compiled from: ProGuard */
public final class C3113a {

    /* renamed from: a */
    private static final C3134l f6825a = new C3134l(101010256);
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static final C3135m f6826b = new C3135m(38651);

    /* renamed from: com.tencent.open.utils.a$a */
    /* compiled from: ProGuard */
    private static class C3115a {

        /* renamed from: a */
        Properties f6827a;

        /* renamed from: b */
        byte[] f6828b;

        private C3115a() {
            this.f6827a = new Properties();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo35163a(byte[] bArr) throws IOException {
            if (bArr != null) {
                ByteBuffer wrap = ByteBuffer.wrap(bArr);
                int length = C3113a.f6826b.mo35185a().length;
                byte[] bArr2 = new byte[length];
                wrap.get(bArr2);
                if (!C3113a.f6826b.equals(new C3135m(bArr2))) {
                    throw new ProtocolException("unknow protocl [" + Arrays.toString(bArr) + "]");
                } else if (bArr.length - length > 2) {
                    byte[] bArr3 = new byte[2];
                    wrap.get(bArr3);
                    int b = new C3135m(bArr3).mo35186b();
                    if ((bArr.length - length) - 2 >= b) {
                        byte[] bArr4 = new byte[b];
                        wrap.get(bArr4);
                        this.f6827a.load(new ByteArrayInputStream(bArr4));
                        int length2 = ((bArr.length - length) - b) - 2;
                        if (length2 > 0) {
                            this.f6828b = new byte[length2];
                            wrap.get(this.f6828b);
                        }
                    }
                }
            }
        }

        public String toString() {
            return "ApkExternalInfo [p=" + this.f6827a + ", otherData=" + Arrays.toString(this.f6828b) + "]";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m7712a(java.io.File r3, java.lang.String r4) throws java.io.IOException {
        /*
            r0 = 0
            java.io.RandomAccessFile r1 = new java.io.RandomAccessFile     // Catch:{ all -> 0x0026 }
            java.lang.String r2 = "r"
            r1.<init>(r3, r2)     // Catch:{ all -> 0x0026 }
            byte[] r3 = m7713a(r1)     // Catch:{ all -> 0x0024 }
            if (r3 != 0) goto L_0x0012
            r1.close()
            return r0
        L_0x0012:
            com.tencent.open.utils.a$a r2 = new com.tencent.open.utils.a$a     // Catch:{ all -> 0x0024 }
            r2.<init>()     // Catch:{ all -> 0x0024 }
            r2.mo35163a(r3)     // Catch:{ all -> 0x0024 }
            java.util.Properties r3 = r2.f6827a     // Catch:{ all -> 0x0024 }
            java.lang.String r3 = r3.getProperty(r4)     // Catch:{ all -> 0x0024 }
            r1.close()
            return r3
        L_0x0024:
            r3 = move-exception
            goto L_0x0028
        L_0x0026:
            r3 = move-exception
            r1 = r0
        L_0x0028:
            if (r1 == 0) goto L_0x002d
            r1.close()
        L_0x002d:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3113a.m7712a(java.io.File, java.lang.String):java.lang.String");
    }

    /* renamed from: a */
    public static String m7711a(File file) throws IOException {
        return m7712a(file, "channelNo");
    }

    /* renamed from: a */
    private static byte[] m7713a(RandomAccessFile randomAccessFile) throws IOException {
        boolean z;
        long length = randomAccessFile.length() - 22;
        randomAccessFile.seek(length);
        byte[] a = f6825a.mo35181a();
        int read = randomAccessFile.read();
        while (true) {
            z = true;
            if (read != -1) {
                if (read == a[0] && randomAccessFile.read() == a[1] && randomAccessFile.read() == a[2] && randomAccessFile.read() == a[3]) {
                    break;
                }
                length--;
                randomAccessFile.seek(length);
                read = randomAccessFile.read();
            } else {
                z = false;
                break;
            }
        }
        if (z) {
            randomAccessFile.seek(length + 16 + 4);
            byte[] bArr = new byte[2];
            randomAccessFile.readFully(bArr);
            int b = new C3135m(bArr).mo35186b();
            if (b == 0) {
                return null;
            }
            byte[] bArr2 = new byte[b];
            randomAccessFile.read(bArr2);
            return bArr2;
        }
        throw new ZipException("archive is not a ZIP archive");
    }
}
