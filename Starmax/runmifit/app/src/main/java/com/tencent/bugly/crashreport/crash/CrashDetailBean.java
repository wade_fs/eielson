package com.tencent.bugly.crashreport.crash;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.crashreport.common.info.PlugInBean;
import com.tencent.bugly.proguard.C2908aq;
import java.util.Map;
import java.util.UUID;

/* compiled from: BUGLY */
public class CrashDetailBean implements Parcelable, Comparable<CrashDetailBean> {
    public static final Parcelable.Creator<CrashDetailBean> CREATOR = new Parcelable.Creator<CrashDetailBean>() {
        /* class com.tencent.bugly.crashreport.crash.CrashDetailBean.C28571 */

        /* renamed from: a */
        public CrashDetailBean createFromParcel(Parcel parcel) {
            return new CrashDetailBean(parcel);
        }

        /* renamed from: a */
        public CrashDetailBean[] newArray(int i) {
            return new CrashDetailBean[i];
        }
    };

    /* renamed from: A */
    public String f5729A = "";

    /* renamed from: B */
    public String f5730B = "";

    /* renamed from: C */
    public long f5731C = -1;

    /* renamed from: D */
    public long f5732D = -1;

    /* renamed from: E */
    public long f5733E = -1;

    /* renamed from: F */
    public long f5734F = -1;

    /* renamed from: G */
    public long f5735G = -1;

    /* renamed from: H */
    public long f5736H = -1;

    /* renamed from: I */
    public String f5737I = "";

    /* renamed from: J */
    public String f5738J = "";

    /* renamed from: K */
    public String f5739K = "";

    /* renamed from: L */
    public String f5740L = "";

    /* renamed from: M */
    public String f5741M = "";

    /* renamed from: N */
    public long f5742N = -1;

    /* renamed from: O */
    public boolean f5743O = false;

    /* renamed from: P */
    public Map<String, String> f5744P = null;

    /* renamed from: Q */
    public int f5745Q = -1;

    /* renamed from: R */
    public int f5746R = -1;

    /* renamed from: S */
    public Map<String, String> f5747S = null;

    /* renamed from: T */
    public Map<String, String> f5748T = null;

    /* renamed from: U */
    public byte[] f5749U = null;

    /* renamed from: V */
    public String f5750V = null;

    /* renamed from: W */
    public String f5751W = null;

    /* renamed from: a */
    public long f5752a = -1;

    /* renamed from: b */
    public int f5753b = 0;

    /* renamed from: c */
    public String f5754c = UUID.randomUUID().toString();

    /* renamed from: d */
    public boolean f5755d = false;

    /* renamed from: e */
    public String f5756e = "";

    /* renamed from: f */
    public String f5757f = "";

    /* renamed from: g */
    public String f5758g = "";

    /* renamed from: h */
    public Map<String, PlugInBean> f5759h = null;

    /* renamed from: i */
    public Map<String, PlugInBean> f5760i = null;

    /* renamed from: j */
    public boolean f5761j = false;

    /* renamed from: k */
    public boolean f5762k = false;

    /* renamed from: l */
    public int f5763l = 0;

    /* renamed from: m */
    public String f5764m = "";

    /* renamed from: n */
    public String f5765n = "";

    /* renamed from: o */
    public String f5766o = "";

    /* renamed from: p */
    public String f5767p = "";

    /* renamed from: q */
    public String f5768q = "";

    /* renamed from: r */
    public long f5769r = -1;

    /* renamed from: s */
    public String f5770s = null;

    /* renamed from: t */
    public int f5771t = 0;

    /* renamed from: u */
    public String f5772u = "";

    /* renamed from: v */
    public String f5773v = "";

    /* renamed from: w */
    public String f5774w = null;

    /* renamed from: x */
    public String f5775x = null;

    /* renamed from: y */
    public byte[] f5776y = null;

    /* renamed from: z */
    public Map<String, String> f5777z = null;

    public int describeContents() {
        return 0;
    }

    public CrashDetailBean() {
    }

    public CrashDetailBean(Parcel parcel) {
        this.f5753b = parcel.readInt();
        this.f5754c = parcel.readString();
        boolean z = true;
        this.f5755d = parcel.readByte() == 1;
        this.f5756e = parcel.readString();
        this.f5757f = parcel.readString();
        this.f5758g = parcel.readString();
        this.f5761j = parcel.readByte() == 1;
        this.f5762k = parcel.readByte() == 1;
        this.f5763l = parcel.readInt();
        this.f5764m = parcel.readString();
        this.f5765n = parcel.readString();
        this.f5766o = parcel.readString();
        this.f5767p = parcel.readString();
        this.f5768q = parcel.readString();
        this.f5769r = parcel.readLong();
        this.f5770s = parcel.readString();
        this.f5771t = parcel.readInt();
        this.f5772u = parcel.readString();
        this.f5773v = parcel.readString();
        this.f5774w = parcel.readString();
        this.f5777z = C2908aq.m6927b(parcel);
        this.f5729A = parcel.readString();
        this.f5730B = parcel.readString();
        this.f5731C = parcel.readLong();
        this.f5732D = parcel.readLong();
        this.f5733E = parcel.readLong();
        this.f5734F = parcel.readLong();
        this.f5735G = parcel.readLong();
        this.f5736H = parcel.readLong();
        this.f5737I = parcel.readString();
        this.f5738J = parcel.readString();
        this.f5739K = parcel.readString();
        this.f5740L = parcel.readString();
        this.f5741M = parcel.readString();
        this.f5742N = parcel.readLong();
        this.f5743O = parcel.readByte() != 1 ? false : z;
        this.f5744P = C2908aq.m6927b(parcel);
        this.f5759h = C2908aq.m6908a(parcel);
        this.f5760i = C2908aq.m6908a(parcel);
        this.f5745Q = parcel.readInt();
        this.f5746R = parcel.readInt();
        this.f5747S = C2908aq.m6927b(parcel);
        this.f5748T = C2908aq.m6927b(parcel);
        this.f5749U = parcel.createByteArray();
        this.f5776y = parcel.createByteArray();
        this.f5750V = parcel.readString();
        this.f5751W = parcel.readString();
        this.f5775x = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5753b);
        parcel.writeString(this.f5754c);
        parcel.writeByte(this.f5755d ? (byte) 1 : 0);
        parcel.writeString(this.f5756e);
        parcel.writeString(this.f5757f);
        parcel.writeString(this.f5758g);
        parcel.writeByte(this.f5761j ? (byte) 1 : 0);
        parcel.writeByte(this.f5762k ? (byte) 1 : 0);
        parcel.writeInt(this.f5763l);
        parcel.writeString(this.f5764m);
        parcel.writeString(this.f5765n);
        parcel.writeString(this.f5766o);
        parcel.writeString(this.f5767p);
        parcel.writeString(this.f5768q);
        parcel.writeLong(this.f5769r);
        parcel.writeString(this.f5770s);
        parcel.writeInt(this.f5771t);
        parcel.writeString(this.f5772u);
        parcel.writeString(this.f5773v);
        parcel.writeString(this.f5774w);
        C2908aq.m6929b(parcel, this.f5777z);
        parcel.writeString(this.f5729A);
        parcel.writeString(this.f5730B);
        parcel.writeLong(this.f5731C);
        parcel.writeLong(this.f5732D);
        parcel.writeLong(this.f5733E);
        parcel.writeLong(this.f5734F);
        parcel.writeLong(this.f5735G);
        parcel.writeLong(this.f5736H);
        parcel.writeString(this.f5737I);
        parcel.writeString(this.f5738J);
        parcel.writeString(this.f5739K);
        parcel.writeString(this.f5740L);
        parcel.writeString(this.f5741M);
        parcel.writeLong(this.f5742N);
        parcel.writeByte(this.f5743O ? (byte) 1 : 0);
        C2908aq.m6929b(parcel, this.f5744P);
        C2908aq.m6910a(parcel, this.f5759h);
        C2908aq.m6910a(parcel, this.f5760i);
        parcel.writeInt(this.f5745Q);
        parcel.writeInt(this.f5746R);
        C2908aq.m6929b(parcel, this.f5747S);
        C2908aq.m6929b(parcel, this.f5748T);
        parcel.writeByteArray(this.f5749U);
        parcel.writeByteArray(this.f5776y);
        parcel.writeString(this.f5750V);
        parcel.writeString(this.f5751W);
        parcel.writeString(this.f5775x);
    }

    /* renamed from: a */
    public int compareTo(CrashDetailBean crashDetailBean) {
        if (crashDetailBean == null) {
            return 1;
        }
        long j = this.f5769r - crashDetailBean.f5769r;
        if (j > 0) {
            return 1;
        }
        return j < 0 ? -1 : 0;
    }
}
