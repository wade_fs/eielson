package com.tencent.open.utils;

import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.util.CrashUtils;
import com.google.common.primitives.UnsignedBytes;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3088a;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URL;
import java.net.URLDecoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Enumeration;
import org.apache.commons.math3.geometry.VectorFormat;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.open.utils.k */
/* compiled from: ProGuard */
public class C3131k {

    /* renamed from: a */
    private static String f6861a = "";

    /* renamed from: b */
    private static String f6862b = "";

    /* renamed from: c */
    private static String f6863c = "";

    /* renamed from: d */
    private static String f6864d = "";

    /* renamed from: e */
    private static int f6865e = -1;

    /* renamed from: f */
    private static String f6866f = null;

    /* renamed from: g */
    private static String f6867g = "0123456789ABCDEF";

    /* renamed from: a */
    private static char m7764a(int i) {
        int i2 = i & 15;
        return (char) (i2 < 10 ? i2 + 48 : (i2 - 10) + 97);
    }

    /* renamed from: a */
    public static Bundle m7765a(String str) {
        Bundle bundle = new Bundle();
        if (str == null) {
            return bundle;
        }
        try {
            for (String str2 : str.split("&")) {
                String[] split = str2.split("=");
                if (split.length == 2) {
                    bundle.putString(URLDecoder.decode(split[0]), URLDecoder.decode(split[1]));
                }
            }
            return bundle;
        } catch (Exception unused) {
            return null;
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0034 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONObject m7774a(org.json.JSONObject r6, java.lang.String r7) {
        /*
            if (r6 != 0) goto L_0x0007
            org.json.JSONObject r6 = new org.json.JSONObject
            r6.<init>()
        L_0x0007:
            if (r7 == 0) goto L_0x0059
            java.lang.String r0 = "&"
            java.lang.String[] r7 = r7.split(r0)
            int r0 = r7.length
            r1 = 0
            r2 = 0
        L_0x0012:
            if (r2 >= r0) goto L_0x0059
            r3 = r7[r2]
            java.lang.String r4 = "="
            java.lang.String[] r3 = r3.split(r4)
            int r4 = r3.length
            r5 = 2
            if (r4 != r5) goto L_0x0056
            r4 = 1
            r5 = r3[r1]     // Catch:{ Exception -> 0x0034 }
            java.lang.String r5 = java.net.URLDecoder.decode(r5)     // Catch:{ Exception -> 0x0034 }
            r3[r1] = r5     // Catch:{ Exception -> 0x0034 }
            r5 = r3[r4]     // Catch:{ Exception -> 0x0034 }
            java.lang.String r5 = java.net.URLDecoder.decode(r5)     // Catch:{ Exception -> 0x0034 }
            r3[r4] = r5     // Catch:{ Exception -> 0x0034 }
            goto L_0x0034
        L_0x0032:
            r3 = move-exception
            goto L_0x003c
        L_0x0034:
            r5 = r3[r1]     // Catch:{ JSONException -> 0x0032 }
            r3 = r3[r4]     // Catch:{ JSONException -> 0x0032 }
            r6.put(r5, r3)     // Catch:{ JSONException -> 0x0032 }
            goto L_0x0056
        L_0x003c:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "decodeUrlToJson has exception: "
            r4.append(r5)
            java.lang.String r3 = r3.getMessage()
            r4.append(r3)
            java.lang.String r3 = r4.toString()
            java.lang.String r4 = "openSDK_LOG.Util"
            com.tencent.open.p059a.C3082f.m7636e(r4, r3)
        L_0x0056:
            int r2 = r2 + 1
            goto L_0x0012
        L_0x0059:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3131k.m7774a(org.json.JSONObject, java.lang.String):org.json.JSONObject");
    }

    /* renamed from: b */
    public static Bundle m7779b(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            Bundle a = m7765a(url.getQuery());
            a.putAll(m7765a(url.getRef()));
            return a;
        } catch (MalformedURLException unused) {
            return new Bundle();
        }
    }

    /* renamed from: c */
    public static JSONObject m7786c(String str) {
        try {
            URL url = new URL(str.replace("auth://", "http://"));
            JSONObject a = m7774a((JSONObject) null, url.getQuery());
            m7774a(a, url.getRef());
            return a;
        } catch (MalformedURLException unused) {
            return new JSONObject();
        }
    }

    /* renamed from: com.tencent.open.utils.k$a */
    /* compiled from: ProGuard */
    public static class C3133a {

        /* renamed from: a */
        public String f6870a;

        /* renamed from: b */
        public long f6871b;

        /* renamed from: c */
        public long f6872c;

        public C3133a(String str, int i) {
            this.f6870a = str;
            this.f6871b = (long) i;
            String str2 = this.f6870a;
            if (str2 != null) {
                this.f6872c = (long) str2.length();
            }
        }
    }

    /* renamed from: d */
    public static JSONObject m7788d(String str) throws JSONException {
        if (str.equals("false")) {
            str = "{value : false}";
        }
        if (str.equals("true")) {
            str = "{value : true}";
        }
        if (str.contains("allback(")) {
            str = str.replaceFirst("[\\s\\S]*allback\\(([\\s\\S]*)\\);[^\\)]*\\z", "$1").trim();
        }
        if (str.contains("online[0]=")) {
            str = "{online:" + str.charAt(str.length() - 2) + VectorFormat.DEFAULT_SUFFIX;
        }
        return new JSONObject(str);
    }

    /* renamed from: a */
    public static String m7769a() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces != null && networkInterfaces.hasMoreElements()) {
                Enumeration<InetAddress> inetAddresses = networkInterfaces.nextElement().getInetAddresses();
                while (true) {
                    if (inetAddresses.hasMoreElements()) {
                        InetAddress nextElement = inetAddresses.nextElement();
                        if (!nextElement.isLoopbackAddress()) {
                            return nextElement.getHostAddress().toString();
                        }
                    }
                }
            }
            return "";
        } catch (SocketException e) {
            C3082f.m7629a("openSDK_LOG.Util", "getUserIp SocketException ", e);
            return "";
        }
    }

    /* renamed from: e */
    public static boolean m7792e(String str) {
        return str == null || str.length() == 0;
    }

    /* renamed from: f */
    private static boolean m7794f(Context context) {
        Signature[] signatureArr;
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.tencent.mtt", 64);
            String str = packageInfo.versionName;
            if (C3125h.m7747a(str, "4.3") >= 0 && !str.startsWith("4.4") && (signatureArr = packageInfo.signatures) != null) {
                try {
                    MessageDigest instance = MessageDigest.getInstance("MD5");
                    instance.update(signatureArr[0].toByteArray());
                    String a = m7773a(instance.digest());
                    instance.reset();
                    if (a.equals("d8391a394d4a179e6fe7bdb8a301258b")) {
                        return true;
                    }
                } catch (NoSuchAlgorithmException e) {
                    C3082f.m7636e("openSDK_LOG.Util", "isQQBrowerAvailable has exception: " + e.getMessage());
                }
            }
        } catch (PackageManager.NameNotFoundException unused) {
        }
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:22|23|24|25|28) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:14|15|16|17|18|19|33) */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x002e, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0038, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        return true;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0026 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:24:0x0033 */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0022 A[SYNTHETIC, Splitter:B:14:0x0022] */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x002f A[SYNTHETIC, Splitter:B:22:0x002f] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean m7777a(android.content.Context r7, java.lang.String r8) {
        /*
            java.lang.String r0 = "com.android.browser"
            java.lang.String r1 = "com.android.browser.BrowserActivity"
            r2 = 0
            boolean r3 = m7794f(r7)     // Catch:{ Exception -> 0x0019 }
            if (r3 == 0) goto L_0x0015
            java.lang.String r4 = "com.tencent.mtt"
            java.lang.String r5 = "com.tencent.mtt.MainActivity"
            m7776a(r7, r4, r5, r8)     // Catch:{ Exception -> 0x0013 }
            goto L_0x0036
        L_0x0013:
            goto L_0x001a
        L_0x0015:
            m7776a(r7, r0, r1, r8)     // Catch:{ Exception -> 0x0013 }
            goto L_0x0036
        L_0x0019:
            r3 = 0
        L_0x001a:
            java.lang.String r4 = "com.google.android.apps.chrome.Main"
            java.lang.String r5 = "com.android.chrome"
            java.lang.String r6 = "com.google.android.browser"
            if (r3 == 0) goto L_0x002f
            m7776a(r7, r0, r1, r8)     // Catch:{ Exception -> 0x0026 }
            goto L_0x0036
        L_0x0026:
            m7776a(r7, r6, r1, r8)     // Catch:{ Exception -> 0x002a }
            goto L_0x0036
        L_0x002a:
            m7776a(r7, r5, r4, r8)     // Catch:{ Exception -> 0x002e }
            goto L_0x0036
        L_0x002e:
            return r2
        L_0x002f:
            m7776a(r7, r6, r1, r8)     // Catch:{ Exception -> 0x0033 }
            goto L_0x0036
        L_0x0033:
            m7776a(r7, r5, r4, r8)     // Catch:{ Exception -> 0x0038 }
        L_0x0036:
            r7 = 1
            return r7
        L_0x0038:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3131k.m7777a(android.content.Context, java.lang.String):boolean");
    }

    /* renamed from: a */
    private static void m7776a(Context context, String str, String str2, String str3) {
        Intent intent = new Intent();
        intent.setComponent(new ComponentName(str, str2));
        intent.setAction("android.intent.action.VIEW");
        intent.addFlags(1073741824);
        intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
        intent.setData(Uri.parse(str3));
        context.startActivity(intent);
    }

    /* renamed from: f */
    public static String m7793f(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(m7799i(str));
            byte[] digest = instance.digest();
            if (digest == null) {
                return str;
            }
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(m7764a(b >>> 4));
                sb.append(m7764a(b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            C3082f.m7636e("openSDK_LOG.Util", "encrypt has exception: " + e.getMessage());
            return str;
        }
    }

    /* renamed from: a */
    public static void m7775a(final Context context, String str, long j, String str2) {
        final Bundle bundle = new Bundle();
        bundle.putString("appid_for_getting_config", str2);
        bundle.putString("strValue", str2);
        bundle.putString("nValue", str);
        bundle.putString("qver", Constants.SDK_VERSION);
        if (j != 0) {
            bundle.putLong("elt", j);
        }
        new Thread() {
            /* class com.tencent.open.utils.C3131k.C31321 */

            public void run() {
                try {
                    HttpUtils.openUrl2(context, "http://cgi.qplus.com/report/report", "GET", bundle);
                } catch (Exception e) {
                    C3082f.m7636e("openSDK_LOG.Util", "reportBernoulli has exception: " + e.getMessage());
                }
            }
        }.start();
    }

    /* renamed from: b */
    public static boolean m7782b() {
        return (Environment.getExternalStorageState().equals("mounted") ? Environment.getExternalStorageDirectory() : null) != null;
    }

    /* renamed from: a */
    public static String m7773a(byte[] bArr) {
        if (bArr == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b : bArr) {
            String num = Integer.toString(b & UnsignedBytes.MAX_VALUE, 16);
            if (num.length() == 1) {
                num = "0" + num;
            }
            sb.append(num);
        }
        return sb.toString();
    }

    /* renamed from: a */
    public static final String m7770a(Context context) {
        CharSequence applicationLabel;
        if (context == null || (applicationLabel = context.getPackageManager().getApplicationLabel(context.getApplicationInfo())) == null) {
            return null;
        }
        return applicationLabel.toString();
    }

    /* renamed from: g */
    public static final boolean m7797g(String str) {
        if (str == null) {
            return false;
        }
        return str.startsWith("http://") || str.startsWith("https://");
    }

    /* renamed from: h */
    public static boolean m7798h(String str) {
        if (str != null && new File(str).exists()) {
            return true;
        }
        return false;
    }

    /* renamed from: a */
    public static final String m7772a(String str, int i, String str2, String str3) {
        if (TextUtils.isEmpty(str)) {
            return "";
        }
        if (TextUtils.isEmpty(str2)) {
            str2 = "UTF-8";
        }
        try {
            if (str.getBytes(str2).length <= i) {
                return str;
            }
            int i2 = 0;
            int i3 = 0;
            while (i2 < str.length()) {
                int i4 = i2 + 1;
                i3 += str.substring(i2, i4).getBytes(str2).length;
                if (i3 > i) {
                    String substring = str.substring(0, i2);
                    if (TextUtils.isEmpty(str3)) {
                        return substring;
                    }
                    return substring + str3;
                }
                i2 = i4;
            }
            return str;
        } catch (Exception e) {
            C3082f.m7636e("openSDK_LOG.Util", "Util.subString has exception: " + e.getMessage());
            return str;
        }
    }

    /* renamed from: b */
    public static boolean m7783b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null) {
            return true;
        }
        NetworkInfo[] allNetworkInfo = connectivityManager.getAllNetworkInfo();
        if (allNetworkInfo != null) {
            for (NetworkInfo networkInfo : allNetworkInfo) {
                if (networkInfo.isConnectedOrConnecting()) {
                    return true;
                }
            }
        }
        return false;
    }

    /* renamed from: a */
    public static Bundle m7766a(String str, String str2, String str3, String str4, String str5, String str6) {
        return m7768a(str, str3, str4, str2, str5, str6, "", "", "", "", "", "");
    }

    /* renamed from: a */
    public static Bundle m7768a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12) {
        Bundle bundle = new Bundle();
        bundle.putString("openid", str);
        bundle.putString("report_type", str2);
        bundle.putString("act_type", str3);
        bundle.putString("via", str4);
        bundle.putString("app_id", str5);
        bundle.putString("result", str6);
        bundle.putString(SocialConstants.PARAM_TYPE, str7);
        bundle.putString("login_status", str8);
        bundle.putString("need_user_auth", str9);
        bundle.putString("to_uin", str10);
        bundle.putString("call_source", str11);
        bundle.putString("to_type", str12);
        return bundle;
    }

    /* renamed from: a */
    public static Bundle m7767a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9) {
        Bundle bundle = new Bundle();
        bundle.putString("platform", "1");
        bundle.putString("result", str);
        bundle.putString("code", str2);
        bundle.putString("tmcost", str3);
        bundle.putString("rate", str4);
        bundle.putString("cmd", str5);
        bundle.putString("uin", str6);
        bundle.putString("appid", str7);
        bundle.putString("share_type", str8);
        bundle.putString("detail", str9);
        bundle.putString("os_ver", Build.VERSION.RELEASE);
        bundle.putString("network", C3088a.m7653a(C3121e.m7727a()));
        bundle.putString("apn", C3088a.m7654b(C3121e.m7727a()));
        bundle.putString("model_name", Build.MODEL);
        bundle.putString("sdk_ver", Constants.SDK_VERSION);
        bundle.putString("packagename", C3121e.m7729b());
        bundle.putString("app_ver", m7787d(C3121e.m7727a(), C3121e.m7729b()));
        return bundle;
    }

    /* renamed from: c */
    public static String m7784c(Context context) {
        Location lastKnownLocation;
        if (context == null) {
            return "";
        }
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            Criteria criteria = new Criteria();
            criteria.setCostAllowed(false);
            criteria.setAccuracy(2);
            String bestProvider = locationManager.getBestProvider(criteria, true);
            if (bestProvider == null || (lastKnownLocation = locationManager.getLastKnownLocation(bestProvider)) == null) {
                return "";
            }
            double latitude = lastKnownLocation.getLatitude();
            double longitude = lastKnownLocation.getLongitude();
            f6866f = latitude + "*" + longitude;
            return f6866f;
        } catch (Exception e) {
            C3082f.m7632b("openSDK_LOG.Util", "getLocation>>>", e);
        }
        return "";
    }

    /* renamed from: b */
    public static void m7781b(Context context, String str) {
        if (context != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
                f6862b = packageInfo.versionName;
                f6861a = f6862b.substring(0, f6862b.lastIndexOf(46));
                f6864d = f6862b.substring(f6862b.lastIndexOf(46) + 1, f6862b.length());
                f6865e = packageInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                C3082f.m7636e("openSDK_LOG.Util", "getPackageInfo has exception: " + e.getMessage());
            } catch (Exception e2) {
                C3082f.m7636e("openSDK_LOG.Util", "getPackageInfo has exception: " + e2.getMessage());
            }
        }
    }

    /* renamed from: c */
    public static String m7785c(Context context, String str) {
        if (context == null) {
            return "";
        }
        m7781b(context, str);
        return f6862b;
    }

    /* renamed from: d */
    public static String m7787d(Context context, String str) {
        if (context == null) {
            return "";
        }
        m7781b(context, str);
        return f6861a;
    }

    /* renamed from: e */
    public static String m7790e(Context context, String str) {
        if (context == null) {
            return "";
        }
        f6863c = m7787d(context, str);
        return f6863c;
    }

    /* renamed from: i */
    public static byte[] m7799i(String str) {
        try {
            return str.getBytes("UTF-8");
        } catch (UnsupportedEncodingException unused) {
            return null;
        }
    }

    /* renamed from: d */
    public static boolean m7789d(Context context) {
        double d;
        try {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            d = Math.sqrt(Math.pow((double) (((float) displayMetrics.widthPixels) / displayMetrics.xdpi), 2.0d) + Math.pow((double) (((float) displayMetrics.heightPixels) / displayMetrics.ydpi), 2.0d));
        } catch (Throwable unused) {
            d = 0.0d;
        }
        return d > 6.5d;
    }

    /* renamed from: f */
    public static boolean m7795f(Context context, String str) {
        boolean z = !m7789d(context) || C3125h.m7751a(context, Constants.PACKAGE_QQ_PAD) == null;
        if (z && C3125h.m7751a(context, Constants.PACKAGE_TIM) != null) {
            z = false;
        }
        if (z && C3125h.m7751a(context, Constants.PACKAGE_QQ_SPEED) != null) {
            z = false;
        }
        if (z) {
            return C3125h.m7757c(context, str) < 0;
        }
        return z;
    }

    /* renamed from: g */
    public static boolean m7796g(Context context, String str) {
        boolean z = !m7789d(context) || C3125h.m7751a(context, Constants.PACKAGE_QQ_PAD) == null;
        if (z && C3125h.m7751a(context, Constants.PACKAGE_TIM) != null) {
            z = false;
        }
        if (z && C3125h.m7751a(context, Constants.PACKAGE_QQ_SPEED) != null) {
            z = false;
        }
        if (z) {
            return C3125h.m7757c(context, str) < 0;
        }
        return z;
    }

    /* renamed from: a */
    public static boolean m7778a(Context context, boolean z) {
        if ((!m7789d(context) || C3125h.m7751a(context, Constants.PACKAGE_QQ_PAD) == null) && C3125h.m7757c(context, "4.1") < 0 && C3125h.m7751a(context, Constants.PACKAGE_TIM) == null && C3125h.m7751a(context, Constants.PACKAGE_QQ_SPEED) == null) {
            return false;
        }
        return true;
    }

    /* renamed from: e */
    public static boolean m7791e(Context context) {
        return (C3125h.m7757c(context, "5.9.5") < 0 && C3125h.m7751a(context, Constants.PACKAGE_TIM) == null && C3125h.m7751a(context, Constants.PACKAGE_QQ_SPEED) == null) ? false : true;
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0025 A[SYNTHETIC, Splitter:B:19:0x0025] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x002c A[SYNTHETIC, Splitter:B:25:0x002c] */
    /* renamed from: j */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static long m7800j(java.lang.String r4) {
        /*
            r0 = 0
            r2 = 0
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            if (r4 == 0) goto L_0x0021
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            r4.<init>(r3)     // Catch:{ Exception -> 0x0029, all -> 0x0022 }
            int r0 = r4.available()     // Catch:{ Exception -> 0x001f, all -> 0x001c }
            long r0 = (long) r0
            r4.close()     // Catch:{ IOException -> 0x001b }
        L_0x001b:
            return r0
        L_0x001c:
            r0 = move-exception
            r2 = r4
            goto L_0x0023
        L_0x001f:
            r2 = r4
            goto L_0x002a
        L_0x0021:
            return r0
        L_0x0022:
            r0 = move-exception
        L_0x0023:
            if (r2 == 0) goto L_0x0028
            r2.close()     // Catch:{ IOException -> 0x0028 }
        L_0x0028:
            throw r0
        L_0x0029:
        L_0x002a:
            if (r2 == 0) goto L_0x002f
            r2.close()     // Catch:{ IOException -> 0x002f }
        L_0x002f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3131k.m7800j(java.lang.String):long");
    }

    /* renamed from: a */
    public static String m7771a(Context context, Uri uri) {
        Uri uri2;
        if (uri == null) {
            return null;
        }
        if (!(Build.VERSION.SDK_INT >= 19) || !DocumentsContract.isDocumentUri(context, uri)) {
            String scheme = uri.getScheme();
            if ("content".equals(scheme)) {
                return m7780b(context, uri);
            }
            if ("file".equals(scheme)) {
                return uri.getPath();
            }
            return null;
        }
        String authority = uri.getAuthority();
        if ("com.android.externalstorage.documents".equals(authority)) {
            String[] split = DocumentsContract.getDocumentId(uri).split(Config.TRACE_TODAY_VISIT_SPLIT);
            String str = split[0];
            if ("primary".equals(str)) {
                return Environment.getExternalStorageDirectory().getAbsolutePath().concat("/").concat(split[1]);
            }
            return "/storage/".concat(str).concat("/").concat(split[1]);
        } else if ("com.android.providers.downloads.documents".equals(authority)) {
            String documentId = DocumentsContract.getDocumentId(uri);
            if (documentId.startsWith("raw:")) {
                return documentId.replaceFirst("raw:", "");
            }
            return m7780b(context, ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.parseLong(documentId)));
        } else {
            if ("com.android.providers.media.documents".equals(authority)) {
                String[] split2 = DocumentsContract.getDocumentId(uri).split(Config.TRACE_TODAY_VISIT_SPLIT);
                String str2 = split2[0];
                if ("image".equals(str2)) {
                    uri2 = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(str2)) {
                    uri2 = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(str2)) {
                    uri2 = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                return m7780b(context, ContentUris.withAppendedId(uri2, Long.parseLong(split2[1])));
            }
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x002d  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m7780b(android.content.Context r8, android.net.Uri r9) {
        /*
            java.lang.String r0 = "_data"
            java.lang.String[] r3 = new java.lang.String[]{r0}
            r7 = 0
            android.content.ContentResolver r1 = r8.getContentResolver()     // Catch:{ Exception -> 0x0026 }
            r4 = 0
            r5 = 0
            r6 = 0
            r2 = r9
            android.database.Cursor r8 = r1.query(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0026 }
            if (r8 == 0) goto L_0x0030
            boolean r9 = r8.moveToFirst()     // Catch:{ Exception -> 0x0024 }
            if (r9 == 0) goto L_0x0030
            int r9 = r8.getColumnIndexOrThrow(r0)     // Catch:{ Exception -> 0x0024 }
            java.lang.String r8 = r8.getString(r9)     // Catch:{ Exception -> 0x0024 }
            return r8
        L_0x0024:
            r9 = move-exception
            goto L_0x0028
        L_0x0026:
            r9 = move-exception
            r8 = r7
        L_0x0028:
            r9.printStackTrace()
            if (r8 == 0) goto L_0x0030
            r8.close()
        L_0x0030:
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3131k.m7780b(android.content.Context, android.net.Uri):java.lang.String");
    }
}
