package com.tencent.bugly.crashreport.biz;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.util.List;

/* renamed from: com.tencent.bugly.crashreport.biz.b */
/* compiled from: BUGLY */
public class C2847b {

    /* renamed from: a */
    public static boolean f5577a = false;

    /* renamed from: b */
    public static C2841a f5578b = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static int f5579c = 10;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static long f5580d = 300000;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static long f5581e = 30000;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static long f5582f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public static int f5583g = 0;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public static long f5584h = 0;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static long f5585i = 0;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static long f5586j = 0;

    /* renamed from: k */
    private static Application.ActivityLifecycleCallbacks f5587k = null;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public static Class<?> f5588l = null;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static boolean f5589m = true;

    /* renamed from: g */
    static /* synthetic */ int m6447g() {
        int i = f5583g;
        f5583g = i + 1;
        return i;
    }

    /* renamed from: m */
    private static void m6453m() {
        C2851a b = C2851a.m6471b();
        if (b != null) {
            String str = null;
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            boolean z = false;
            for (StackTraceElement stackTraceElement : stackTrace) {
                if (stackTraceElement.getMethodName().equals("onCreate")) {
                    str = stackTraceElement.getClassName();
                }
                if (stackTraceElement.getClassName().equals("android.app.Activity")) {
                    z = true;
                }
            }
            if (str == null) {
                str = "unknown";
            } else if (z) {
                b.mo34294a(true);
            } else {
                str = "background";
            }
            b.f5677v = str;
        }
    }

    /* renamed from: b */
    private static boolean m6438b(Context context) {
        C2851a a = C2851a.m6470a(context);
        List<UserInfoBean> a2 = f5578b.mo34245a(a.f5660e);
        if (a2 == null) {
            return true;
        }
        for (int i = 0; i < a2.size(); i++) {
            UserInfoBean userInfoBean = a2.get(i);
            if (userInfoBean.f5558n.equals(a.f5671p) && userInfoBean.f5546b == 1) {
                long b = C2908aq.m6923b();
                if (b <= 0) {
                    return true;
                }
                if (userInfoBean.f5549e >= b) {
                    if (userInfoBean.f5550f <= 0) {
                        f5578b.mo34250b();
                    }
                    return false;
                }
            }
        }
        return true;
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m6442c(Context context, BuglyStrategy buglyStrategy) {
        boolean z;
        boolean z2;
        if (buglyStrategy != null) {
            z2 = buglyStrategy.recordUserInfoOnceADay();
            z = buglyStrategy.isEnableUserInfo();
        } else {
            z = true;
            z2 = false;
        }
        if (z2) {
            if (m6438b(context)) {
                z = false;
            } else {
                return;
            }
        }
        m6453m();
        if (z) {
            m6441c(context);
        }
        if (f5589m) {
            m6454n();
            f5578b.mo34246a();
            f5578b.mo34251b(21600000);
        }
    }

    /* renamed from: a */
    public static void m6432a(final Context context, final BuglyStrategy buglyStrategy) {
        long j;
        if (!f5577a) {
            f5589m = C2851a.m6470a(context).f5663h;
            f5578b = new C2841a(context, f5589m);
            f5577a = true;
            if (buglyStrategy != null) {
                f5588l = buglyStrategy.getUserInfoActivity();
                j = buglyStrategy.getAppReportDelay();
            } else {
                j = 0;
            }
            if (j <= 0) {
                m6442c(context, buglyStrategy);
            } else {
                C2901am.m6848a().mo34548a(new Runnable() {
                    /* class com.tencent.bugly.crashreport.biz.C2847b.C28481 */

                    public void run() {
                        C2847b.m6442c(context, buglyStrategy);
                    }
                }, j);
            }
        }
    }

    /* renamed from: a */
    public static void m6430a(long j) {
        if (j < 0) {
            j = C2854a.m6574a().mo34340c().f5703q;
        }
        f5582f = j;
    }

    /* renamed from: a */
    public static void m6433a(StrategyBean strategyBean, boolean z) {
        C2841a aVar = f5578b;
        if (aVar != null && !z) {
            aVar.mo34250b();
        }
        if (strategyBean != null) {
            if (strategyBean.f5703q > 0) {
                f5581e = strategyBean.f5703q;
            }
            if (strategyBean.f5709w > 0) {
                f5579c = strategyBean.f5709w;
            }
            if (strategyBean.f5710x > 0) {
                f5580d = strategyBean.f5710x;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
      com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
    /* renamed from: n */
    private static void m6454n() {
        f5585i = System.currentTimeMillis();
        f5578b.mo34247a(1, false, 0L);
        C2903an.m6857a("[session] launch app, new start", new Object[0]);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
      com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
    /* renamed from: a */
    public static void m6429a() {
        C2841a aVar = f5578b;
        if (aVar != null) {
            aVar.mo34247a(2, false, 0L);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static String m6436b(String str, String str2) {
        return C2908aq.m6897a() + "  " + str + "  " + str2 + "\n";
    }

    /* renamed from: c */
    private static void m6441c(Context context) {
        if (Build.VERSION.SDK_INT >= 14) {
            Application application = context.getApplicationContext() instanceof Application ? (Application) context.getApplicationContext() : null;
            if (application != null) {
                try {
                    if (f5587k == null) {
                        f5587k = new Application.ActivityLifecycleCallbacks() {
                            /* class com.tencent.bugly.crashreport.biz.C2847b.C28492 */

                            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                            }

                            public void onActivityStarted(Activity activity) {
                            }

                            public void onActivityStopped(Activity activity) {
                            }

                            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                             method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
                             arg types: [int, int, int]
                             candidates:
                              com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
                              com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
                            public void onActivityResumed(Activity activity) {
                                String name = activity != null ? activity.getClass().getName() : "unknown";
                                if (C2847b.f5588l == null || C2847b.f5588l.getName().equals(name)) {
                                    C2903an.m6863c(">>> %s onResumed <<<", name);
                                    C2851a b = C2851a.m6471b();
                                    if (b != null) {
                                        b.f5609K.add(C2847b.m6436b(name, "onResumed"));
                                        b.mo34294a(true);
                                        b.f5677v = name;
                                        b.f5678w = System.currentTimeMillis();
                                        b.f5681z = b.f5678w - C2847b.f5585i;
                                        long d = b.f5678w - C2847b.f5584h;
                                        if (d > (C2847b.f5582f > 0 ? C2847b.f5582f : C2847b.f5581e)) {
                                            b.mo34303d();
                                            C2847b.m6447g();
                                            C2903an.m6857a("[session] launch app one times (app in background %d seconds and over %d seconds)", Long.valueOf(d / 1000), Long.valueOf(C2847b.f5581e / 1000));
                                            if (C2847b.f5583g % C2847b.f5579c == 0) {
                                                C2847b.f5578b.mo34247a(4, C2847b.f5589m, 0);
                                                return;
                                            }
                                            C2847b.f5578b.mo34247a(4, false, 0L);
                                            long currentTimeMillis = System.currentTimeMillis();
                                            if (currentTimeMillis - C2847b.f5586j > C2847b.f5580d) {
                                                long unused = C2847b.f5586j = currentTimeMillis;
                                                C2903an.m6857a("add a timer to upload hot start user info", new Object[0]);
                                                if (C2847b.f5589m) {
                                                    C2847b.f5578b.mo34248a(C2847b.f5580d);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            public void onActivityPaused(Activity activity) {
                                String name = activity != null ? activity.getClass().getName() : "unknown";
                                if (C2847b.f5588l == null || C2847b.f5588l.getName().equals(name)) {
                                    C2903an.m6863c(">>> %s onPaused <<<", name);
                                    C2851a b = C2851a.m6471b();
                                    if (b != null) {
                                        b.f5609K.add(C2847b.m6436b(name, "onPaused"));
                                        b.mo34294a(false);
                                        b.f5679x = System.currentTimeMillis();
                                        b.f5680y = b.f5679x - b.f5678w;
                                        long unused = C2847b.f5584h = b.f5679x;
                                        if (b.f5680y < 0) {
                                            b.f5680y = 0;
                                        }
                                        if (activity != null) {
                                            b.f5677v = "background";
                                        } else {
                                            b.f5677v = "unknown";
                                        }
                                    }
                                }
                            }

                            public void onActivityDestroyed(Activity activity) {
                                String name = activity != null ? activity.getClass().getName() : "unknown";
                                if (C2847b.f5588l == null || C2847b.f5588l.getName().equals(name)) {
                                    C2903an.m6863c(">>> %s onDestroyed <<<", name);
                                    C2851a b = C2851a.m6471b();
                                    if (b != null) {
                                        b.f5609K.add(C2847b.m6436b(name, "onDestroyed"));
                                    }
                                }
                            }

                            public void onActivityCreated(Activity activity, Bundle bundle) {
                                String name = activity != null ? activity.getClass().getName() : "unknown";
                                if (C2847b.f5588l == null || C2847b.f5588l.getName().equals(name)) {
                                    C2903an.m6863c(">>> %s onCreated <<<", name);
                                    C2851a b = C2851a.m6471b();
                                    if (b != null) {
                                        b.f5609K.add(C2847b.m6436b(name, "onCreated"));
                                    }
                                }
                            }
                        };
                    }
                    application.registerActivityLifecycleCallbacks(f5587k);
                } catch (Exception e) {
                    if (!C2903an.m6858a(e)) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /* renamed from: d */
    private static void m6444d(Context context) {
        if (Build.VERSION.SDK_INT >= 14) {
            Application application = context.getApplicationContext() instanceof Application ? (Application) context.getApplicationContext() : null;
            if (application != null) {
                try {
                    if (f5587k != null) {
                        application.unregisterActivityLifecycleCallbacks(f5587k);
                    }
                } catch (Exception e) {
                    if (!C2903an.m6858a(e)) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public static void m6431a(Context context) {
        if (f5577a && context != null) {
            m6444d(context);
            f5577a = false;
        }
    }
}
