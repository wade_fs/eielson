package com.tencent.bugly.beta.tinker;

import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.Log;
import com.tencent.tinker.loader.TinkerLoader;
import com.tencent.tinker.loader.app.TinkerApplication;
import com.tencent.tinker.loader.shareutil.ShareReflectUtil;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

/* compiled from: BUGLY */
public class TinkerPatchReflectApplication extends TinkerApplication {
    private static final String TAG = "Tinker.ReflectApp";
    private boolean isReflectFailure = false;
    private String rawApplicationName = null;
    private Application realApplication;

    public TinkerPatchReflectApplication() {
        super(7, "com.tencent.bugly.beta.tinker.TinkerApplicationLike", TinkerLoader.class.getName(), false);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        TinkerPatchReflectApplication.super.attachBaseContext(context);
        try {
            String rawApplicationName2 = getRawApplicationName(context);
            if (rawApplicationName2 != null) {
                this.realApplication = (Application) Class.forName(rawApplicationName2, false, getClassLoader()).getConstructor(new Class[0]).newInstance(new Object[0]);
                if (this.realApplication != null) {
                    try {
                        Method declaredMethod = ContextWrapper.class.getDeclaredMethod("attachBaseContext", Context.class);
                        declaredMethod.setAccessible(true);
                        declaredMethod.invoke(this.realApplication, context);
                    } catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                }
            } else {
                throw new RuntimeException("can get real realApplication from manifest!");
            }
        } catch (Exception e2) {
            throw new IllegalStateException(e2);
        }
    }

    /* JADX WARN: Type inference failed for: r12v0, types: [android.content.Context, com.tencent.tinker.loader.app.TinkerApplication, com.tencent.bugly.beta.tinker.TinkerPatchReflectApplication] */
    public void onCreate() {
        Class<?> cls;
        Object activityThread;
        Class<?> cls2;
        if (this.realApplication != null) {
            try {
                cls = Class.forName("android.app.ActivityThread");
                activityThread = ShareReflectUtil.getActivityThread((Context) this, cls);
                Field declaredField = cls.getDeclaredField("mInitialApplication");
                declaredField.setAccessible(true);
                Application application = (Application) declaredField.get(activityThread);
                if (this.realApplication != null && application == this) {
                    declaredField.set(activityThread, this.realApplication);
                }
                if (this.realApplication != null) {
                    Field declaredField2 = cls.getDeclaredField("mAllApplications");
                    declaredField2.setAccessible(true);
                    List list = (List) declaredField2.get(activityThread);
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i) == this) {
                            list.set(i, this.realApplication);
                        }
                    }
                }
                cls2 = Class.forName("android.app.LoadedApk");
            } catch (ClassNotFoundException unused) {
                cls2 = Class.forName("android.app.ActivityThread$PackageInfo");
            } catch (Throwable th) {
                Log.e(TAG, "Error, reflect Application fail, result:" + th);
                this.isReflectFailure = true;
            }
            Field declaredField3 = cls2.getDeclaredField("mApplication");
            declaredField3.setAccessible(true);
            Field field = null;
            try {
                field = Application.class.getDeclaredField("mLoadedApk");
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            }
            String[] strArr = {"mPackages", "mResourcePackages"};
            for (int i2 = 0; i2 < 2; i2++) {
                Field declaredField4 = cls.getDeclaredField(strArr[i2]);
                declaredField4.setAccessible(true);
                for (Map.Entry entry : ((Map) declaredField4.get(activityThread)).entrySet()) {
                    Object obj = ((WeakReference) entry.getValue()).get();
                    if (obj != null && declaredField3.get(obj) == this) {
                        if (this.realApplication != null) {
                            declaredField3.set(obj, this.realApplication);
                        }
                        if (!(this.realApplication == null || field == null)) {
                            field.set(this.realApplication, obj);
                        }
                    }
                }
            }
            if (!this.isReflectFailure) {
                try {
                    Class<?> cls3 = Class.forName("com.tencent.bugly.beta.tinker.TinkerApplicationLike", false, getClassLoader());
                    Log.e(TAG, "replaceApplicationLike delegateClass:" + cls3);
                    ShareReflectUtil.findField(cls3, "application").set(cls3.getDeclaredMethod("getTinkerPatchApplicationLike", new Class[0]).invoke(cls3, new Object[0]), this.realApplication);
                } catch (Throwable th2) {
                    Log.e(TAG, "replaceApplicationLike exception:" + th2.getMessage());
                }
            }
        }
        TinkerPatchReflectApplication.super.onCreate();
        Application application2 = this.realApplication;
        if (application2 != null) {
            application2.onCreate();
        }
    }

    public String getRawApplicationName(Context context) {
        String str = this.rawApplicationName;
        if (str != null) {
            return str;
        }
        try {
            Object obj = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.get("TINKER_PATCH_APPLICATION");
            if (obj != null) {
                this.rawApplicationName = String.valueOf(obj);
            } else {
                this.rawApplicationName = null;
            }
            Log.i(TAG, "with app realApplication from manifest applicationName:" + this.rawApplicationName);
            return this.rawApplicationName;
        } catch (Exception e) {
            Log.e(TAG, "getManifestApplication exception:" + e.getMessage());
            return null;
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.onConfigurationChanged(configuration);
        } else {
            application.onConfigurationChanged(configuration);
        }
    }

    public void onLowMemory() {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.onLowMemory();
        } else {
            application.onLowMemory();
        }
    }

    public void onTrimMemory(int i) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.onTrimMemory(i);
        } else {
            application.onTrimMemory(i);
        }
    }

    public void onTerminate() {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.onTerminate();
        } else {
            application.onTerminate();
        }
    }

    public Intent registerReceiver(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            return TinkerPatchReflectApplication.super.registerReceiver(broadcastReceiver, intentFilter);
        }
        return application.registerReceiver(broadcastReceiver, intentFilter);
    }

    public void unregisterReceiver(BroadcastReceiver broadcastReceiver) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.unregisterReceiver(broadcastReceiver);
        } else {
            application.unregisterReceiver(broadcastReceiver);
        }
    }

    public boolean bindService(Intent intent, ServiceConnection serviceConnection, int i) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            return TinkerPatchReflectApplication.super.bindService(intent, serviceConnection, i);
        }
        return application.bindService(intent, serviceConnection, i);
    }

    public void unbindService(ServiceConnection serviceConnection) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.unbindService(serviceConnection);
        } else {
            application.unbindService(serviceConnection);
        }
    }

    public void registerComponentCallbacks(ComponentCallbacks componentCallbacks) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.registerComponentCallbacks(componentCallbacks);
        } else {
            application.registerComponentCallbacks(componentCallbacks);
        }
    }

    public void unregisterComponentCallbacks(ComponentCallbacks componentCallbacks) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.unregisterComponentCallbacks(componentCallbacks);
        } else {
            application.unregisterComponentCallbacks(componentCallbacks);
        }
    }

    public void registerActivityLifecycleCallbacks(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        } else {
            application.registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
        }
    }

    public void unregisterActivityLifecycleCallbacks(Application.ActivityLifecycleCallbacks activityLifecycleCallbacks) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
        } else {
            application.unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
        }
    }

    public void registerOnProvideAssistDataListener(Application.OnProvideAssistDataListener onProvideAssistDataListener) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.registerOnProvideAssistDataListener(onProvideAssistDataListener);
        } else {
            application.registerOnProvideAssistDataListener(onProvideAssistDataListener);
        }
    }

    public void unregisterOnProvideAssistDataListener(Application.OnProvideAssistDataListener onProvideAssistDataListener) {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            TinkerPatchReflectApplication.super.unregisterOnProvideAssistDataListener(onProvideAssistDataListener);
        } else {
            application.unregisterOnProvideAssistDataListener(onProvideAssistDataListener);
        }
    }

    public Resources getResources() {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            return TinkerPatchReflectApplication.super.getResources();
        }
        return application.getResources();
    }

    public ClassLoader getClassLoader() {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            return TinkerPatchReflectApplication.super.getClassLoader();
        }
        return application.getClassLoader();
    }

    public AssetManager getAssets() {
        Application application;
        return (!this.isReflectFailure || (application = this.realApplication) == null) ? TinkerPatchReflectApplication.super.getAssets() : application.getAssets();
    }

    public ContentResolver getContentResolver() {
        Application application;
        if (!this.isReflectFailure || (application = this.realApplication) == null) {
            return TinkerPatchReflectApplication.super.getContentResolver();
        }
        return application.getContentResolver();
    }
}
