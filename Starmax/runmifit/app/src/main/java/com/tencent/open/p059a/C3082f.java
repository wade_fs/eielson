package com.tencent.open.p059a;

import android.os.Environment;
import android.text.TextUtils;
import com.tencent.connect.common.Constants;
import com.tencent.open.p059a.C3076d;
import com.tencent.open.utils.C3121e;
import java.io.File;

/* renamed from: com.tencent.open.a.f */
/* compiled from: ProGuard */
public class C3082f {

    /* renamed from: a */
    public static C3082f f6742a = null;

    /* renamed from: c */
    protected static final C3074b f6743c = new C3074b(m7633c(), C3075c.f6736m, C3075c.f6730g, C3075c.f6731h, C3075c.f6726c, (long) C3075c.f6732i, 10, C3075c.f6728e, C3075c.f6737n);

    /* renamed from: d */
    private static boolean f6744d = false;

    /* renamed from: b */
    protected C3073a f6745b = new C3073a(f6743c);

    /* renamed from: a */
    public static C3082f m7627a() {
        if (f6742a == null) {
            synchronized (C3082f.class) {
                if (f6742a == null) {
                    f6742a = new C3082f();
                    f6744d = true;
                }
            }
        }
        return f6742a;
    }

    private C3082f() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35104a(int i, String str, String str2, Throwable th) {
        C3073a aVar;
        if (f6744d) {
            String b = C3121e.m7729b();
            if (!TextUtils.isEmpty(b)) {
                String str3 = b + " SDK_VERSION:" + Constants.SDK_VERSION;
                if (this.f6745b != null) {
                    String str4 = str3;
                    C3081e.f6741a.mo35116b(32, Thread.currentThread(), System.currentTimeMillis(), "openSDK_LOG", str4, null);
                    this.f6745b.mo35116b(32, Thread.currentThread(), System.currentTimeMillis(), "openSDK_LOG", str4, null);
                    f6744d = false;
                } else {
                    return;
                }
            }
        }
        C3081e.f6741a.mo35116b(i, Thread.currentThread(), System.currentTimeMillis(), str, str2, th);
        if (C3076d.C3077a.m7615a(C3075c.f6725b, i) && (aVar = this.f6745b) != null) {
            aVar.mo35116b(i, Thread.currentThread(), System.currentTimeMillis(), str, str2, th);
        }
    }

    /* renamed from: a */
    public static final void m7628a(String str, String str2) {
        m7627a().mo35104a(1, str, str2, null);
    }

    /* renamed from: b */
    public static final void m7631b(String str, String str2) {
        m7627a().mo35104a(2, str, str2, null);
    }

    /* renamed from: a */
    public static final void m7629a(String str, String str2, Throwable th) {
        m7627a().mo35104a(2, str, str2, th);
    }

    /* renamed from: c */
    public static final void m7634c(String str, String str2) {
        m7627a().mo35104a(4, str, str2, null);
    }

    /* renamed from: d */
    public static final void m7635d(String str, String str2) {
        m7627a().mo35104a(8, str, str2, null);
    }

    /* renamed from: e */
    public static final void m7636e(String str, String str2) {
        m7627a().mo35104a(16, str, str2, null);
    }

    /* renamed from: b */
    public static final void m7632b(String str, String str2, Throwable th) {
        m7627a().mo35104a(16, str, str2, th);
    }

    /* renamed from: b */
    public static void m7630b() {
        synchronized (C3082f.class) {
            m7627a().mo35105d();
            if (f6742a != null) {
                f6742a = null;
            }
        }
    }

    /* renamed from: c */
    protected static File m7633c() {
        String str = C3075c.f6727d;
        boolean z = false;
        try {
            C3076d.C3079c b = C3076d.C3078b.m7617b();
            if (b != null && b.mo35102c() > C3075c.f6729f) {
                z = true;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        if (z) {
            return new File(Environment.getExternalStorageDirectory(), str);
        }
        return new File(C3121e.m7730c(), str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo35105d() {
        C3073a aVar = this.f6745b;
        if (aVar != null) {
            aVar.mo35075a();
            this.f6745b.mo35079b();
            this.f6745b = null;
        }
    }
}
