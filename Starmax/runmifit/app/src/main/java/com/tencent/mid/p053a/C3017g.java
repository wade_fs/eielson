package com.tencent.mid.p053a;

import android.content.Context;
import com.tencent.mid.api.MidCallback;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.p054b.C3031g;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;

/* renamed from: com.tencent.mid.a.g */
public class C3017g {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static C3038d f6521a = Util.getLogger();

    /* renamed from: c */
    private static boolean m7376c(Context context, MidCallback midCallback) {
        return true;
    }

    /* renamed from: a */
    public static MidEntity m7367a(Context context) {
        return C3031g.m7437a(context).mo34923g();
    }

    /* renamed from: a */
    public static void m7368a(Context context, final MidCallback midCallback) {
        C3038d dVar = f6521a;
        dVar.mo34950b("requestMid, callback=" + midCallback);
        m7374b(context, new MidCallback() {
            /* class com.tencent.mid.p053a.C3017g.C30181 */

            public void onSuccess(Object obj) {
                if (obj != null) {
                    MidEntity parse = MidEntity.parse(obj.toString());
                    C3038d b = C3017g.f6521a;
                    b.mo34956h("success to get mid:" + parse.getMid());
                    midCallback.onSuccess(parse.getMid());
                }
            }

            public void onFail(int i, String str) {
                C3038d b = C3017g.f6521a;
                b.mo34954f("failed to get mid, errorcode:" + i + " ,msg:" + str);
                midCallback.onFail(i, str);
            }
        });
    }

    /* renamed from: b */
    public static void m7374b(Context context, MidCallback midCallback) {
        if (m7376c(context, midCallback)) {
            MidEntity a = m7367a(context);
            if (a == null || !a.isMidValid()) {
                f6521a.mo34950b("requestMidEntity -> request new mid entity.");
                C3023i.m7381a().mo34851a(new C3020h(context, 1, midCallback));
                return;
            }
            C3038d dVar = f6521a;
            dVar.mo34950b("requestMidEntity -> get local mid entity:" + a.toString());
            midCallback.onSuccess(a.toString());
            C3023i.m7381a().mo34851a(new C3020h(context, 2, midCallback));
        }
    }

    /* renamed from: b */
    public static String m7373b(Context context) {
        if (context == null) {
            f6521a.mo34954f("context==null in getMid()");
            return null;
        }
        String f = C3031g.m7437a(context).mo34921f();
        if (!Util.isMidValid(f)) {
            C30192 r1 = new MidCallback() {
                /* class com.tencent.mid.p053a.C3017g.C30192 */

                public void onSuccess(Object obj) {
                    if (obj != null) {
                        MidEntity parse = MidEntity.parse(obj.toString());
                        C3038d b = C3017g.f6521a;
                        b.mo34956h("success to get mid:" + parse.getMid());
                    }
                }

                public void onFail(int i, String str) {
                    C3038d b = C3017g.f6521a;
                    b.mo34954f("failed to get mid, errorcode:" + i + " ,msg:" + str);
                }
            };
            f6521a.mo34956h("getMid -> request new mid entity.");
            C3023i.m7381a().mo34851a(new C3020h(context, 1, r1));
        }
        return f;
    }

    /* renamed from: c */
    public static String m7375c(Context context) {
        if (context != null) {
            return C3031g.m7437a(context).mo34921f();
        }
        f6521a.mo34954f("context==null in getMid()");
        return null;
    }

    /* renamed from: a */
    public static boolean m7371a(String str) {
        return Util.isMidValid(str);
    }

    /* renamed from: a */
    public static void m7369a(boolean z) {
        Util.getLogger().mo34947a(z);
    }

    /* renamed from: a */
    public static boolean m7370a() {
        return Util.getLogger().mo34948a();
    }
}
