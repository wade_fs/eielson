package com.tencent.bugly.proguard;

import android.content.ContentValues;
import android.text.TextUtils;
import com.tencent.bugly.beta.download.C2802b;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2808e;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;

/* renamed from: com.tencent.bugly.proguard.s */
/* compiled from: BUGLY */
public class C2952s implements C2802b {

    /* renamed from: a */
    public static C2952s f6238a = new C2952s();

    /* renamed from: b */
    public ConcurrentHashMap<String, DownloadTask> f6239b = new ConcurrentHashMap<>(3);

    /* renamed from: c */
    private ScheduledExecutorService f6240c = null;

    public C2952s() {
        try {
            this.f6240c = Executors.newScheduledThreadPool(3, new ThreadFactory() {
                /* class com.tencent.bugly.proguard.C2952s.C29531 */

                public Thread newThread(Runnable runnable) {
                    Thread thread = new Thread(runnable);
                    thread.setName("BETA_SDK_DOWNLOAD");
                    return thread;
                }
            });
            if (this.f6240c.isShutdown()) {
                throw new IllegalArgumentException("ScheduledExecutorService is not available!");
            }
        } catch (Exception e) {
            C2903an.m6858a(e);
        }
    }

    /* renamed from: a */
    public DownloadTask mo34042a(String str, String str2, String str3, String str4) {
        String str5 = str;
        C2954t tVar = null;
        if (TextUtils.isEmpty(str)) {
            C2903an.m6865e("downloadUrl is null!", new Object[0]);
            return null;
        } else if (TextUtils.isEmpty(str2)) {
            C2903an.m6865e("saveDir is null!", new Object[0]);
            return null;
        } else if (this.f6239b.get(str5) != null) {
            return this.f6239b.get(str5);
        } else {
            ContentValues a = C2947p.f6229a.mo34663a(str5);
            if (!(a == null || a.get("_dUrl") == null || a.getAsString("_sFile") == null || a.getAsLong("_sLen") == null || a.getAsLong("_tLen") == null || a.getAsString("_MD5") == null)) {
                tVar = new C2954t((String) a.get("_dUrl"), a.getAsString("_sFile"), a.getAsLong("_sLen").longValue(), a.getAsLong("_tLen").longValue(), a.getAsString("_MD5"));
                if (a.getAsLong("_DLTIME") != null) {
                    tVar.f6242k = a.getAsLong("_DLTIME").longValue();
                }
            }
            if (tVar == null) {
                tVar = new C2954t(str5, str2, str3, str4);
            }
            tVar.setDownloadType(C2808e.f5336E.f5367ae);
            return tVar;
        }
    }

    /* renamed from: a */
    public synchronized boolean mo34676a(Runnable runnable) {
        if (this.f6240c != null) {
            if (!this.f6240c.isShutdown()) {
                if (runnable == null) {
                    C2903an.m6864d("async task = null", new Object[0]);
                    return false;
                }
                C2903an.m6864d("task start %s", runnable.getClass().getName());
                this.f6240c.execute(runnable);
                return true;
            }
        }
        C2903an.m6864d("async handler was closed , should not post task!", new Object[0]);
        return false;
    }
}
