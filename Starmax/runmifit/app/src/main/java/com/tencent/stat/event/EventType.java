package com.tencent.stat.event;

import com.baidu.mapapi.UIMsg;
import com.baidu.mapapi.synchronization.SynchronizationConstants;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceConstant;

public enum EventType {
    PAGE_VIEW(1),
    SESSION_ENV(2),
    ERROR(3),
    BACKGROUND(4),
    ONCE(5),
    CUSTOM_PROPERTY(6),
    CUSTOM(1000),
    ADDITION(1001),
    MONITOR_STAT(1002),
    MTA_GAME_USER(1003),
    NETWORK_MONITOR(1004),
    NETWORK_DETECTOR(1005),
    INSTALL_CHANNEL(SynchronizationConstants.LBS_ERROR_QUERY_TRACK_ROUTE_FAILED),
    INSTALL_SOURCE(SynchronizationConstants.LBS_ERROR_QUERY_TRACK_ROUTE_SUCCESS),
    ANTI_CHEAT(SynchronizationConstants.LBS_ERROR_QUERY_TRACK_ROUTE_FAILED),
    REG_ACCOUNT(2004),
    PAY_EVENT(UIMsg.m_AppUI.MSG_APP_VERSION_FORCE),
    FBI_EVENT(10000),
    LBS_EVENT(10001),
    DATA_EVENT(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_URL_NULL);
    

    /* renamed from: a */
    private int f7359a;

    private EventType(int i) {
        this.f7359a = i;
    }

    /* renamed from: a */
    public int mo35459a() {
        return this.f7359a;
    }
}
