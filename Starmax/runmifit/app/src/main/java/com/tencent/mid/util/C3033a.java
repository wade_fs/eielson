package com.tencent.mid.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: com.tencent.mid.util.a */
public class C3033a {

    /* renamed from: a */
    public static final List<Integer> f6567a = new ArrayList(Arrays.asList(2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048));

    /* renamed from: b */
    private SecretKey f6568b = null;

    /* renamed from: c */
    private IvParameterSpec f6569c = null;

    /* renamed from: a */
    public static SecretKey m7469a() {
        try {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(128);
            return new SecretKeySpec(instance.generateKey().getEncoded(), "AES");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public byte[] mo34933b() {
        SecretKey secretKey = this.f6568b;
        if (secretKey != null) {
            return secretKey.getEncoded();
        }
        return null;
    }

    /* renamed from: c */
    public byte[] mo34935c() {
        IvParameterSpec ivParameterSpec = this.f6569c;
        if (ivParameterSpec != null) {
            return ivParameterSpec.getIV();
        }
        return null;
    }

    /* renamed from: d */
    public static IvParameterSpec m7471d() {
        byte[] bArr = new byte[16];
        new SecureRandom().nextBytes(bArr);
        return new IvParameterSpec(bArr);
    }

    /* renamed from: e */
    public void mo34936e() {
        this.f6568b = m7469a();
        this.f6569c = m7471d();
    }

    /* renamed from: a */
    private int m7468a(int i, int i2) {
        if (i < i2) {
            i = i2;
        }
        if (f6567a.contains(Integer.valueOf(i))) {
            return i;
        }
        for (Integer num : f6567a) {
            if (num.intValue() > i) {
                return num.intValue();
            }
        }
        return i;
    }

    /* renamed from: a */
    private byte[] m7470a(String str, int i) {
        byte[] bytes = str.getBytes();
        int length = bytes.length;
        if (length >= i) {
            return bytes;
        }
        byte[] bArr = new byte[i];
        Arrays.fill(bArr, (byte) 0);
        System.arraycopy(bytes, 0, bArr, 0, length);
        return bArr;
    }

    /* renamed from: a */
    public void mo34931a(String str, String str2) {
        int a = m7468a(str.length(), str2.length());
        byte[] a2 = m7470a(str, a);
        byte[] a3 = m7470a(str2, a);
        this.f6568b = new SecretKeySpec(a2, "AES");
        this.f6569c = new IvParameterSpec(a3);
    }

    /* renamed from: a */
    public byte[] mo34932a(byte[] bArr) {
        if (this.f6568b != null) {
            try {
                Cipher instance = Cipher.getInstance("AES/CFB/NoPadding");
                instance.init(1, this.f6568b, this.f6569c);
                return instance.doFinal(bArr);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            throw new Exception("密钥为空, 请设置");
        }
    }

    /* renamed from: b */
    public byte[] mo34934b(byte[] bArr) {
        if (this.f6568b != null) {
            try {
                Cipher instance = Cipher.getInstance("AES/CFB/NoPadding");
                instance.init(2, this.f6568b, this.f6569c);
                return instance.doFinal(bArr);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        } else {
            throw new Exception("密钥为空, 请设置");
        }
    }
}
