package com.tencent.open;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.webkit.ConsoleMessage;
import android.webkit.WebChromeClient;
import com.tencent.open.p059a.C3082f;

/* renamed from: com.tencent.open.b */
/* compiled from: ProGuard */
public abstract class C3086b extends Dialog {

    /* renamed from: a */
    protected C3070a f6752a;

    /* renamed from: b */
    protected final WebChromeClient f6753b = new WebChromeClient() {
        /* class com.tencent.open.C3086b.C30871 */

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            if (consoleMessage == null) {
                return false;
            }
            C3082f.m7634c("openSDK_LOG.JsDialog", "WebChromeClient onConsoleMessage" + consoleMessage.message() + " -- From  111 line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
            if (Build.VERSION.SDK_INT <= 7) {
                return true;
            }
            C3086b.this.mo35050a(consoleMessage == null ? "" : consoleMessage.message());
            return true;
        }

        public void onConsoleMessage(String str, int i, String str2) {
            C3082f.m7634c("openSDK_LOG.JsDialog", "WebChromeClient onConsoleMessage" + str + " -- From 222 line " + i + " of " + str2);
            if (Build.VERSION.SDK_INT == 7) {
                C3086b.this.mo35050a(str);
            }
        }
    };

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo35050a(String str);

    public C3086b(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f6752a = new C3070a();
    }
}
