package com.tencent.p055mm.opensdk.channel.p057a;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.tencent.p055mm.opensdk.constants.Build;
import com.tencent.p055mm.opensdk.constants.ConstantsAPI;
import com.tencent.p055mm.opensdk.p056a.C3052d;

/* renamed from: com.tencent.mm.opensdk.channel.a.a */
public final class C3053a {

    /* renamed from: com.tencent.mm.opensdk.channel.a.a$a */
    public static class C3054a {

        /* renamed from: a */
        public String f6623a;

        /* renamed from: b */
        public String f6624b;
        public Bundle bundle;

        /* renamed from: c */
        public long f6625c;
        public String content;
    }

    /* renamed from: a */
    public static boolean m7543a(Context context, C3054a aVar) {
        String str;
        if (context == null) {
            str = "send fail, invalid argument";
        } else if (C3052d.m7542a(aVar.f6624b)) {
            str = "send fail, action is null";
        } else {
            String str2 = null;
            if (!C3052d.m7542a(aVar.f6623a)) {
                str2 = aVar.f6623a + ".permission.MM_MESSAGE";
            }
            Intent intent = new Intent(aVar.f6624b);
            if (aVar.bundle != null) {
                intent.putExtras(aVar.bundle);
            }
            String packageName = context.getPackageName();
            intent.putExtra(ConstantsAPI.SDK_VERSION, (int) Build.SDK_INT);
            intent.putExtra(ConstantsAPI.APP_PACKAGE, packageName);
            intent.putExtra(ConstantsAPI.CONTENT, aVar.content);
            intent.putExtra(ConstantsAPI.APP_SUPORT_CONTENT_TYPE, aVar.f6625c);
            intent.putExtra(ConstantsAPI.CHECK_SUM, C3055b.m7544a(aVar.content, Build.SDK_INT, packageName));
            context.sendBroadcast(intent, str2);
            Log.d("MicroMsg.SDK.MMessage", "send mm message, intent=" + intent + ", perm=" + str2);
            return true;
        }
        Log.e("MicroMsg.SDK.MMessage", str);
        return false;
    }
}
