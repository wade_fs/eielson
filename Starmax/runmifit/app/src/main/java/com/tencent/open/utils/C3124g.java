package com.tencent.open.utils;

import android.content.Context;
import android.content.SharedPreferences;
import com.tencent.open.p059a.C3082f;
import java.lang.ref.WeakReference;
import java.net.URL;

/* renamed from: com.tencent.open.utils.g */
/* compiled from: ProGuard */
public class C3124g {

    /* renamed from: a */
    private static C3124g f6849a;

    /* renamed from: b */
    private volatile WeakReference<SharedPreferences> f6850b = null;

    /* renamed from: a */
    public static synchronized C3124g m7744a() {
        C3124g gVar;
        synchronized (C3124g.class) {
            if (f6849a == null) {
                f6849a = new C3124g();
            }
            gVar = f6849a;
        }
        return gVar;
    }

    /* renamed from: a */
    public String mo35172a(Context context, String str) {
        if (this.f6850b == null || this.f6850b.get() == null) {
            this.f6850b = new WeakReference<>(context.getSharedPreferences("ServerPrefs", 0));
        }
        try {
            String host = new URL(str).getHost();
            if (host == null) {
                C3082f.m7636e("openSDK_LOG.ServerSetting", "Get host error. url=" + str);
                return str;
            }
            String string = this.f6850b.get().getString(host, null);
            if (string != null) {
                if (!host.equals(string)) {
                    String replace = str.replace(host, string);
                    C3082f.m7628a("openSDK_LOG.ServerSetting", "return environment url : " + replace);
                    return replace;
                }
            }
            C3082f.m7628a("openSDK_LOG.ServerSetting", "host=" + host + ", envHost=" + string);
            return str;
        } catch (Exception e) {
            C3082f.m7636e("openSDK_LOG.ServerSetting", "getEnvUrl url=" + str + "error.: " + e.getMessage());
            return str;
        }
    }
}
