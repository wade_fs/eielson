package com.tencent.stat;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Handler;
import android.os.HandlerThread;
import com.baidu.mobstat.Config;
import com.tencent.stat.StatConfig;
import com.tencent.stat.common.DeviceInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatConstants;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.StatPreferences;
import com.tencent.stat.common.Util;
import com.tencent.stat.event.Event;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.e */
public class C3226e {

    /* renamed from: c */
    static volatile int f7295c = 0;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static StatLogger f7296i = StatCommonHelper.getLogger();

    /* renamed from: j */
    private static Context f7297j = null;

    /* renamed from: l */
    private static C3226e f7298l = null;

    /* renamed from: a */
    volatile int f7299a = 0;

    /* renamed from: b */
    DeviceInfo f7300b = null;

    /* renamed from: d */
    private C3234a f7301d = null;

    /* renamed from: e */
    private C3234a f7302e = null;

    /* renamed from: f */
    private Handler f7303f = null;

    /* renamed from: g */
    private String f7304g = "";

    /* renamed from: h */
    private String f7305h = "";

    /* renamed from: k */
    private long f7306k = 307200;

    /* renamed from: m */
    private int f7307m = 0;

    /* renamed from: n */
    private ConcurrentHashMap<Event, String> f7308n = null;

    /* renamed from: o */
    private boolean f7309o = false;

    /* renamed from: p */
    private HashMap<String, String> f7310p = new HashMap<>();

    /* renamed from: com.tencent.stat.e$b */
    static class C3235b {

        /* renamed from: a */
        long f7335a;

        /* renamed from: b */
        String f7336b;

        /* renamed from: c */
        int f7337c;

        /* renamed from: d */
        int f7338d;

        public C3235b(long j, String str, int i, int i2) {
            this.f7335a = j;
            this.f7336b = str;
            this.f7337c = i;
            this.f7338d = i2;
        }

        public String toString() {
            return this.f7336b;
        }
    }

    /* renamed from: a */
    public int mo35423a() {
        return this.f7299a;
    }

    /* renamed from: g */
    private void m8063g() {
        if (!m8051a(false)) {
            StatLogger statLogger = f7296i;
            statLogger.warn("delete " + this.f7301d.f7333a + ", and create new one");
            this.f7301d.mo35439a();
            this.f7301d = new C3234a(f7297j, this.f7304g);
        }
        if (!m8051a(true)) {
            StatLogger statLogger2 = f7296i;
            statLogger2.warn("delete " + this.f7302e.f7333a + ", and create new one");
            this.f7302e.mo35439a();
            this.f7302e = new C3234a(f7297j, this.f7305h);
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x00de */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m8051a(boolean r15) {
        /*
            r14 = this;
            java.lang.String r0 = "events"
            java.lang.String r1 = "test"
            r2 = 0
            r3 = 0
            r4 = 1
            android.database.sqlite.SQLiteDatabase r15 = r14.m8060d(r15)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r15.beginTransaction()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            android.content.ContentValues r5 = new android.content.ContentValues     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r5.<init>()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r6 = "content"
            r5.put(r6, r1)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r6 = "send_count"
            java.lang.String r7 = "100"
            r5.put(r6, r7)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r6 = "status"
            java.lang.String r7 = java.lang.Integer.toString(r4)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r5.put(r6, r7)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r6 = "timestamp"
            long r7 = java.lang.System.currentTimeMillis()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r5.put(r6, r7)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r15.insert(r0, r3, r5)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r15.setTransactionSuccessful()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r15.endTransaction()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r5 = "content = ?"
            java.lang.String[] r6 = new java.lang.String[]{r1}     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            int r0 = r15.delete(r0, r5, r6)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r6 = "events"
            r7 = 0
            java.lang.String r8 = "content=?"
            java.lang.String[] r9 = new java.lang.String[]{r1}     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r10 = 0
            r11 = 0
            r12 = 0
            java.lang.String r13 = "1"
            r5 = r15
            android.database.Cursor r3 = r5.query(r6, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            int r1 = r3.getCount()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r3.close()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            boolean r5 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            if (r5 == 0) goto L_0x0086
            com.tencent.stat.common.StatLogger r5 = com.tencent.stat.C3226e.f7296i     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r6.<init>()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r7 = "delNum="
            r6.append(r7)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r6.append(r0)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r7 = ",queryNum="
            r6.append(r7)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r6.append(r1)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r6 = r6.toString()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r5.mo35388i(r6)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
        L_0x0086:
            if (r0 == 0) goto L_0x00c2
            if (r1 > 0) goto L_0x00c2
            boolean r0 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            if (r0 == 0) goto L_0x00b7
            java.lang.String r15 = r15.getPath()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r0 = "/"
            java.lang.String[] r15 = r15.split(r0)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            int r0 = r15.length     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            if (r0 <= 0) goto L_0x00b7
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r1.<init>()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r5 = "test db passed, db name:"
            r1.append(r5)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            int r5 = r15.length     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            int r5 = r5 - r4
            r15 = r15[r5]     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r1.append(r15)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r15 = r1.toString()     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            r0.mo35388i(r15)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
        L_0x00b7:
            if (r3 == 0) goto L_0x00f2
            r3.close()     // Catch:{ all -> 0x00bd }
            goto L_0x00f2
        L_0x00bd:
            r15 = move-exception
            r15.printStackTrace()
            goto L_0x00f2
        L_0x00c2:
            android.database.SQLException r15 = new android.database.SQLException     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            java.lang.String r0 = "test delete error."
            r15.<init>(r0)     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
            throw r15     // Catch:{ SQLiteFullException -> 0x00de, Exception -> 0x00cc }
        L_0x00ca:
            r15 = move-exception
            goto L_0x00f3
        L_0x00cc:
            r15 = move-exception
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00ca }
            r0.mo35384e(r15)     // Catch:{ all -> 0x00ca }
            if (r3 == 0) goto L_0x00dc
            r3.close()     // Catch:{ all -> 0x00d8 }
            goto L_0x00dc
        L_0x00d8:
            r15 = move-exception
            r15.printStackTrace()
        L_0x00dc:
            r4 = 0
            goto L_0x00f2
        L_0x00de:
            com.tencent.stat.common.StatLogger r15 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00ca }
            java.lang.String r0 = "db is full, change to INSTANT"
            r15.warn(r0)     // Catch:{ all -> 0x00ca }
            com.tencent.stat.StatConfig.setReportEventsByOrder(r2)     // Catch:{ all -> 0x00ca }
            com.tencent.stat.StatReportStrategy r15 = com.tencent.stat.StatReportStrategy.INSTANT     // Catch:{ all -> 0x00ca }
            com.tencent.stat.StatConfig.setStatSendStrategy(r15)     // Catch:{ all -> 0x00ca }
            if (r3 == 0) goto L_0x00f2
            r3.close()     // Catch:{ all -> 0x00bd }
        L_0x00f2:
            return r4
        L_0x00f3:
            if (r3 == 0) goto L_0x00fd
            r3.close()     // Catch:{ all -> 0x00f9 }
            goto L_0x00fd
        L_0x00f9:
            r0 = move-exception
            r0.printStackTrace()
        L_0x00fd:
            throw r15
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8051a(boolean):boolean");
    }

    private C3226e(Context context) {
        try {
            HandlerThread handlerThread = new HandlerThread("StatStore");
            handlerThread.start();
            this.f7303f = new Handler(handlerThread.getLooper());
            f7297j = context.getApplicationContext();
            this.f7308n = new ConcurrentHashMap<>();
            this.f7304g = StatCommonHelper.getDatabaseName(context);
            this.f7305h = "pri_" + StatCommonHelper.getDatabaseName(context);
            this.f7301d = new C3234a(f7297j, this.f7304g);
            this.f7302e = new C3234a(f7297j, this.f7305h);
            m8063g();
            m8057b(true);
            m8057b(false);
            m8064h();
            mo35429b(f7297j);
            mo35432e();
            m8068l();
        } catch (Throwable th) {
            f7296i.mo35384e(th);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Can't wrap try/catch for region: R(4:26|27|29|30) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:18|19|21|22|37) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:10:0x0081 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0096 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00a9 */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0090 A[SYNTHETIC, Splitter:B:18:0x0090] */
    /* JADX WARNING: Removed duplicated region for block: B:35:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:10:0x0081=Splitter:B:10:0x0081, B:21:0x0096=Splitter:B:21:0x0096} */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m8057b(boolean r8) {
        /*
            r7 = this;
            android.database.sqlite.SQLiteDatabase r8 = r7.m8060d(r8)     // Catch:{ all -> 0x0087 }
            r8.beginTransaction()     // Catch:{ all -> 0x0085 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x0085 }
            r0.<init>()     // Catch:{ all -> 0x0085 }
            java.lang.String r1 = "status"
            r2 = 1
            java.lang.Integer r3 = java.lang.Integer.valueOf(r2)     // Catch:{ all -> 0x0085 }
            r0.put(r1, r3)     // Catch:{ all -> 0x0085 }
            java.lang.String r1 = "events"
            java.lang.String r3 = "status=?"
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ all -> 0x0085 }
            r4 = 0
            r5 = 2
            java.lang.String r5 = java.lang.Long.toString(r5)     // Catch:{ all -> 0x0085 }
            r2[r4] = r5     // Catch:{ all -> 0x0085 }
            int r0 = r8.update(r1, r0, r3, r2)     // Catch:{ all -> 0x0085 }
            boolean r1 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x0085 }
            if (r1 == 0) goto L_0x004a
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0085 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0085 }
            r2.<init>()     // Catch:{ all -> 0x0085 }
            java.lang.String r3 = "update "
            r2.append(r3)     // Catch:{ all -> 0x0085 }
            r2.append(r0)     // Catch:{ all -> 0x0085 }
            java.lang.String r0 = " unsent events."
            r2.append(r0)     // Catch:{ all -> 0x0085 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0085 }
            r1.mo35388i(r0)     // Catch:{ all -> 0x0085 }
        L_0x004a:
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x0085 }
            r2 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 / r2
            long r2 = com.tencent.stat.StatConfig.f6962t     // Catch:{ all -> 0x0085 }
            r4 = 24
            long r2 = r2 * r4
            r4 = 60
            long r2 = r2 * r4
            long r2 = r2 * r4
            long r0 = r0 - r2
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0085 }
            r2.<init>()     // Catch:{ all -> 0x0085 }
            java.lang.String r3 = "delete from events where timestamp<"
            r2.append(r3)     // Catch:{ all -> 0x0085 }
            r2.append(r0)     // Catch:{ all -> 0x0085 }
            java.lang.String r0 = "  or length(content) >"
            r2.append(r0)     // Catch:{ all -> 0x0085 }
            long r0 = r7.f7306k     // Catch:{ all -> 0x0085 }
            r2.append(r0)     // Catch:{ all -> 0x0085 }
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x0085 }
            r8.execSQL(r0)     // Catch:{ all -> 0x0085 }
            if (r8 == 0) goto L_0x009f
            r8.setTransactionSuccessful()     // Catch:{ Exception -> 0x0081 }
        L_0x0081:
            r8.endTransaction()     // Catch:{ all -> 0x0094 }
            goto L_0x009f
        L_0x0085:
            r0 = move-exception
            goto L_0x0089
        L_0x0087:
            r0 = move-exception
            r8 = 0
        L_0x0089:
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00a0 }
            r1.mo35384e(r0)     // Catch:{ all -> 0x00a0 }
            if (r8 == 0) goto L_0x009f
            r8.setTransactionSuccessful()     // Catch:{ Exception -> 0x0096 }
            goto L_0x0096
        L_0x0094:
            r8 = move-exception
            goto L_0x009a
        L_0x0096:
            r8.endTransaction()     // Catch:{ all -> 0x0094 }
            goto L_0x009f
        L_0x009a:
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i
            r0.mo35384e(r8)
        L_0x009f:
            return
        L_0x00a0:
            r0 = move-exception
            if (r8 == 0) goto L_0x00b2
            r8.setTransactionSuccessful()     // Catch:{ Exception -> 0x00a9 }
            goto L_0x00a9
        L_0x00a7:
            r8 = move-exception
            goto L_0x00ad
        L_0x00a9:
            r8.endTransaction()     // Catch:{ all -> 0x00a7 }
            goto L_0x00b2
        L_0x00ad:
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i
            r1.mo35384e(r8)
        L_0x00b2:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8057b(boolean):void");
    }

    /* renamed from: a */
    public static C3226e m8037a(Context context) {
        if (f7298l == null) {
            synchronized (C3226e.class) {
                if (f7298l == null) {
                    f7298l = new C3226e(context);
                }
            }
        }
        return f7298l;
    }

    /* renamed from: b */
    public static C3226e m8052b() {
        return f7298l;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c */
    public void mo35430c() {
        m8056b(new ArrayList(200), 100, false);
        C3220d.m8022b(f7297j).mo35413b();
    }

    /* JADX WARNING: Removed duplicated region for block: B:105:0x0234 A[Catch:{ all -> 0x02f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0249 A[Catch:{ all -> 0x02ef }] */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0250 A[Catch:{ all -> 0x02ef }] */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0300 A[SYNTHETIC, Splitter:B:135:0x0300] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0105  */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x01c7 A[Catch:{ all -> 0x02f1 }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x01e8 A[Catch:{ all -> 0x02f1 }] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.stat.common.DeviceInfo mo35429b(android.content.Context r29) {
        /*
            r28 = this;
            r7 = r28
            r0 = r29
            com.tencent.stat.common.DeviceInfo r1 = r7.f7300b
            if (r1 == 0) goto L_0x0009
            return r1
        L_0x0009:
            boolean r1 = r28.m8059c(r29)
            java.lang.String r8 = "ts"
            java.lang.String r9 = "app_ver"
            java.lang.String r10 = "user_type"
            java.lang.String r11 = "uid"
            r4 = 2
            java.lang.String r13 = ","
            r14 = 1000(0x3e8, double:4.94E-321)
            r5 = 0
            r6 = 1
            if (r1 == 0) goto L_0x0112
            boolean r1 = com.tencent.stat.StatConfig.isDebugEnable()
            if (r1 == 0) goto L_0x002b
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i
            java.lang.String r12 = "try to load user info from sp."
            r1.mo35388i(r12)
        L_0x002b:
            java.lang.String r1 = r7.m8038a(r11)
            java.lang.String r11 = ""
            java.lang.String r1 = com.tencent.stat.common.StatPreferences.getString(r0, r1, r11)
            java.lang.String r12 = com.tencent.stat.common.Util.decode(r1)
            java.lang.String r10 = r7.m8038a(r10)
            int r10 = com.tencent.stat.common.StatPreferences.getInt(r0, r10, r6)
            java.lang.String r9 = r7.m8038a(r9)
            java.lang.String r9 = com.tencent.stat.common.StatPreferences.getString(r0, r9, r11)
            java.lang.String r8 = r7.m8038a(r8)
            r2 = 0
            long r2 = com.tencent.stat.common.StatPreferences.getLong(r0, r8, r2)
            long r18 = java.lang.System.currentTimeMillis()
            long r18 = r18 / r14
            if (r10 == r6) goto L_0x006f
            long r2 = r2 * r14
            java.lang.String r2 = com.tencent.stat.common.StatCommonHelper.getDateFormat(r2)
            long r14 = r14 * r18
            java.lang.String r3 = com.tencent.stat.common.StatCommonHelper.getDateFormat(r14)
            boolean r2 = r2.equals(r3)
            if (r2 != 0) goto L_0x006f
            r2 = 1
            goto L_0x0070
        L_0x006f:
            r2 = r10
        L_0x0070:
            java.lang.String r3 = com.tencent.stat.common.StatCommonHelper.getAppVersion(r29)
            boolean r3 = r9.equals(r3)
            if (r3 != 0) goto L_0x007c
            r2 = r2 | 2
        L_0x007c:
            r8 = r2
            if (r12 == 0) goto L_0x0084
            java.lang.String[] r2 = r12.split(r13)
            goto L_0x0085
        L_0x0084:
            r2 = 0
        L_0x0085:
            if (r2 == 0) goto L_0x00a6
            int r3 = r2.length
            if (r3 <= 0) goto L_0x00a6
            r3 = r2[r5]
            if (r3 == 0) goto L_0x0096
            int r9 = r3.length()
            r11 = 11
            if (r9 >= r11) goto L_0x00ac
        L_0x0096:
            java.lang.String r9 = com.tencent.stat.common.Util.getDeviceID(r29)
            if (r9 == 0) goto L_0x00ac
            int r11 = r9.length()
            r14 = 10
            if (r11 <= r14) goto L_0x00ac
            r3 = r9
            goto L_0x00ab
        L_0x00a6:
            java.lang.String r12 = com.tencent.stat.common.StatCommonHelper.getUserID(r29)
            r3 = r12
        L_0x00ab:
            r5 = 1
        L_0x00ac:
            if (r2 == 0) goto L_0x00c6
            int r9 = r2.length
            if (r9 < r4) goto L_0x00c6
            r2 = r2[r6]
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            r4.append(r13)
            r4.append(r2)
            java.lang.String r12 = r4.toString()
            goto L_0x00e5
        L_0x00c6:
            java.lang.String r2 = com.tencent.stat.common.StatCommonHelper.getMacId(r29)
            if (r2 == 0) goto L_0x00e5
            int r4 = r2.length()
            if (r4 <= 0) goto L_0x00e5
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            r4.append(r13)
            r4.append(r2)
            java.lang.String r12 = r4.toString()
            r5 = 1
        L_0x00e5:
            com.tencent.stat.common.DeviceInfo r4 = new com.tencent.stat.common.DeviceInfo
            r4.<init>(r3, r2, r8)
            r7.f7300b = r4
            java.lang.String r9 = com.tencent.stat.common.Util.encode(r12)
            if (r5 == 0) goto L_0x0103
            boolean r1 = r9.equals(r1)
            if (r1 == 0) goto L_0x0103
            r1 = r28
            r2 = r29
            r3 = r9
            r4 = r8
            r5 = r18
            r1.m8041a(r2, r3, r4, r5)
        L_0x0103:
            if (r8 == r10) goto L_0x0313
            r1 = r28
            r2 = r29
            r3 = r9
            r4 = r8
            r5 = r18
            r1.m8041a(r2, r3, r4, r5)
            goto L_0x0313
        L_0x0112:
            com.tencent.stat.e$a r1 = r7.f7301d     // Catch:{ all -> 0x02f5 }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ all -> 0x02f5 }
            r1.beginTransaction()     // Catch:{ all -> 0x02f5 }
            boolean r1 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x02f5 }
            if (r1 == 0) goto L_0x012e
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0129 }
            java.lang.String r2 = "try to load user info from db."
            r1.mo35388i(r2)     // Catch:{ all -> 0x0129 }
            goto L_0x012e
        L_0x0129:
            r0 = move-exception
            r16 = 0
            goto L_0x02f9
        L_0x012e:
            com.tencent.stat.e$a r1 = r7.f7301d     // Catch:{ all -> 0x02f5 }
            android.database.sqlite.SQLiteDatabase r18 = r1.getReadableDatabase()     // Catch:{ all -> 0x02f5 }
            java.lang.String r19 = "user"
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 0
            r25 = 0
            r26 = 0
            android.database.Cursor r12 = r18.query(r19, r20, r21, r22, r23, r24, r25, r26)     // Catch:{ all -> 0x02f5 }
            boolean r1 = r12.moveToNext()     // Catch:{ all -> 0x02f1 }
            java.lang.String r3 = "user"
            if (r1 == 0) goto L_0x0268
            java.lang.String r1 = r12.getString(r5)     // Catch:{ all -> 0x02f1 }
            java.lang.String r2 = com.tencent.stat.common.Util.decode(r1)     // Catch:{ all -> 0x02f1 }
            int r5 = r12.getInt(r6)     // Catch:{ all -> 0x02f1 }
            java.lang.String r6 = r12.getString(r4)     // Catch:{ all -> 0x02f1 }
            r4 = 3
            long r21 = r12.getLong(r4)     // Catch:{ all -> 0x02f1 }
            long r23 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x02f1 }
            long r23 = r23 / r14
            r4 = 1
            if (r5 == r4) goto L_0x0182
            long r21 = r21 * r14
            java.lang.String r4 = com.tencent.stat.common.StatCommonHelper.getDateFormat(r21)     // Catch:{ all -> 0x02f1 }
            long r21 = r23 * r14
            java.lang.String r14 = com.tencent.stat.common.StatCommonHelper.getDateFormat(r21)     // Catch:{ all -> 0x02f1 }
            boolean r4 = r4.equals(r14)     // Catch:{ all -> 0x02f1 }
            if (r4 != 0) goto L_0x0182
            r4 = 1
            goto L_0x0183
        L_0x0182:
            r4 = r5
        L_0x0183:
            java.lang.String r14 = com.tencent.stat.common.StatCommonHelper.getAppVersion(r29)     // Catch:{ all -> 0x02f1 }
            boolean r6 = r6.equals(r14)     // Catch:{ all -> 0x02f1 }
            if (r6 != 0) goto L_0x018f
            r4 = r4 | 2
        L_0x018f:
            if (r2 == 0) goto L_0x0196
            java.lang.String[] r6 = r2.split(r13)     // Catch:{ all -> 0x02f1 }
            goto L_0x0197
        L_0x0196:
            r6 = 0
        L_0x0197:
            if (r6 == 0) goto L_0x01bf
            int r14 = r6.length     // Catch:{ all -> 0x02f1 }
            if (r14 <= 0) goto L_0x01bf
            r14 = 0
            r15 = r6[r14]     // Catch:{ all -> 0x02f1 }
            if (r15 == 0) goto L_0x01ac
            int r14 = r15.length()     // Catch:{ all -> 0x02f1 }
            r0 = 11
            if (r14 >= r0) goto L_0x01aa
            goto L_0x01ac
        L_0x01aa:
            r0 = 0
            goto L_0x01c5
        L_0x01ac:
            java.lang.String r0 = com.tencent.stat.common.Util.getDeviceID(r29)     // Catch:{ all -> 0x02f1 }
            if (r0 == 0) goto L_0x01aa
            int r14 = r0.length()     // Catch:{ all -> 0x02f1 }
            r17 = r0
            r0 = 10
            if (r14 <= r0) goto L_0x01aa
            r15 = r17
            goto L_0x01c4
        L_0x01bf:
            java.lang.String r2 = com.tencent.stat.common.StatCommonHelper.getUserID(r29)     // Catch:{ all -> 0x02f1 }
            r15 = r2
        L_0x01c4:
            r0 = 1
        L_0x01c5:
            if (r6 == 0) goto L_0x01e8
            int r14 = r6.length     // Catch:{ all -> 0x02f1 }
            r16 = r0
            r0 = 2
            if (r14 < r0) goto L_0x01ea
            r0 = 1
            r2 = r6[r0]     // Catch:{ all -> 0x02f1 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x02f1 }
            r0.<init>()     // Catch:{ all -> 0x02f1 }
            r0.append(r15)     // Catch:{ all -> 0x02f1 }
            r0.append(r13)     // Catch:{ all -> 0x02f1 }
            r0.append(r2)     // Catch:{ all -> 0x02f1 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x02f1 }
            r27 = r2
            r2 = r0
            r0 = r27
            goto L_0x020a
        L_0x01e8:
            r16 = r0
        L_0x01ea:
            java.lang.String r0 = com.tencent.stat.common.StatCommonHelper.getMacId(r29)     // Catch:{ all -> 0x02f1 }
            if (r0 == 0) goto L_0x020a
            int r6 = r0.length()     // Catch:{ all -> 0x02f1 }
            if (r6 <= 0) goto L_0x020a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x02f1 }
            r2.<init>()     // Catch:{ all -> 0x02f1 }
            r2.append(r15)     // Catch:{ all -> 0x02f1 }
            r2.append(r13)     // Catch:{ all -> 0x02f1 }
            r2.append(r0)     // Catch:{ all -> 0x02f1 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x02f1 }
            r16 = 1
        L_0x020a:
            com.tencent.stat.common.DeviceInfo r6 = new com.tencent.stat.common.DeviceInfo     // Catch:{ all -> 0x02f1 }
            r6.<init>(r15, r0, r4)     // Catch:{ all -> 0x02f1 }
            r7.f7300b = r6     // Catch:{ all -> 0x02f1 }
            android.content.ContentValues r0 = new android.content.ContentValues     // Catch:{ all -> 0x02f1 }
            r0.<init>()     // Catch:{ all -> 0x02f1 }
            java.lang.String r6 = com.tencent.stat.common.Util.encode(r2)     // Catch:{ all -> 0x02f1 }
            r0.put(r11, r6)     // Catch:{ all -> 0x02f1 }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x02f1 }
            r0.put(r10, r2)     // Catch:{ all -> 0x02f1 }
            java.lang.String r2 = com.tencent.stat.common.StatCommonHelper.getAppVersion(r29)     // Catch:{ all -> 0x02f1 }
            r0.put(r9, r2)     // Catch:{ all -> 0x02f1 }
            java.lang.Long r2 = java.lang.Long.valueOf(r23)     // Catch:{ all -> 0x02f1 }
            r0.put(r8, r2)     // Catch:{ all -> 0x02f1 }
            if (r16 == 0) goto L_0x0249
            com.tencent.stat.e$a r2 = r7.f7301d     // Catch:{ all -> 0x02f1 }
            android.database.sqlite.SQLiteDatabase r2 = r2.getWritableDatabase()     // Catch:{ all -> 0x02f1 }
            java.lang.String r14 = "uid=?"
            r16 = r12
            r15 = 1
            java.lang.String[] r12 = new java.lang.String[r15]     // Catch:{ all -> 0x02ef }
            r17 = 0
            r12[r17] = r1     // Catch:{ all -> 0x02ef }
            r2.update(r3, r0, r14, r12)     // Catch:{ all -> 0x02ef }
            goto L_0x024e
        L_0x0249:
            r16 = r12
            r15 = 1
            r17 = 0
        L_0x024e:
            if (r4 == r5) goto L_0x025a
            com.tencent.stat.e$a r1 = r7.f7301d     // Catch:{ all -> 0x02ef }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ all -> 0x02ef }
            r2 = 0
            r1.replace(r3, r2, r0)     // Catch:{ all -> 0x02ef }
        L_0x025a:
            r1 = r28
            r2 = r29
            r0 = r3
            r3 = r6
            r12 = 0
            r14 = 1
            r5 = r23
            r1.m8041a(r2, r3, r4, r5)     // Catch:{ all -> 0x02ef }
            goto L_0x026d
        L_0x0268:
            r0 = r3
            r16 = r12
            r12 = 0
            r14 = 0
        L_0x026d:
            if (r14 != 0) goto L_0x02d7
            java.lang.String r14 = com.tencent.stat.common.StatCommonHelper.getUserID(r29)     // Catch:{ all -> 0x02ef }
            java.lang.String r15 = com.tencent.stat.common.StatCommonHelper.getMacId(r29)     // Catch:{ all -> 0x02ef }
            if (r15 == 0) goto L_0x0292
            int r1 = r15.length()     // Catch:{ all -> 0x02ef }
            if (r1 <= 0) goto L_0x0292
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x02ef }
            r1.<init>()     // Catch:{ all -> 0x02ef }
            r1.append(r14)     // Catch:{ all -> 0x02ef }
            r1.append(r13)     // Catch:{ all -> 0x02ef }
            r1.append(r15)     // Catch:{ all -> 0x02ef }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x02ef }
            goto L_0x0293
        L_0x0292:
            r1 = r14
        L_0x0293:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x02ef }
            r4 = 1000(0x3e8, double:4.94E-321)
            long r5 = r2 / r4
            java.lang.String r2 = com.tencent.stat.common.StatCommonHelper.getAppVersion(r29)     // Catch:{ all -> 0x02ef }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x02ef }
            r3.<init>()     // Catch:{ all -> 0x02ef }
            java.lang.String r4 = com.tencent.stat.common.Util.encode(r1)     // Catch:{ all -> 0x02ef }
            r3.put(r11, r4)     // Catch:{ all -> 0x02ef }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r12)     // Catch:{ all -> 0x02ef }
            r3.put(r10, r1)     // Catch:{ all -> 0x02ef }
            r3.put(r9, r2)     // Catch:{ all -> 0x02ef }
            java.lang.Long r1 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x02ef }
            r3.put(r8, r1)     // Catch:{ all -> 0x02ef }
            com.tencent.stat.e$a r1 = r7.f7301d     // Catch:{ all -> 0x02ef }
            android.database.sqlite.SQLiteDatabase r1 = r1.getWritableDatabase()     // Catch:{ all -> 0x02ef }
            r2 = 0
            r1.insert(r0, r2, r3)     // Catch:{ all -> 0x02ef }
            r0 = 0
            r1 = r28
            r2 = r29
            r3 = r4
            r4 = r0
            r1.m8041a(r2, r3, r4, r5)     // Catch:{ all -> 0x02ef }
            com.tencent.stat.common.DeviceInfo r0 = new com.tencent.stat.common.DeviceInfo     // Catch:{ all -> 0x02ef }
            r0.<init>(r14, r15, r12)     // Catch:{ all -> 0x02ef }
            r7.f7300b = r0     // Catch:{ all -> 0x02ef }
        L_0x02d7:
            com.tencent.stat.e$a r0 = r7.f7301d     // Catch:{ all -> 0x02ef }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x02ef }
            r0.setTransactionSuccessful()     // Catch:{ all -> 0x02ef }
            if (r16 == 0) goto L_0x02e5
            r16.close()     // Catch:{ all -> 0x030d }
        L_0x02e5:
            com.tencent.stat.e$a r0 = r7.f7301d     // Catch:{ all -> 0x030d }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x030d }
            r0.endTransaction()     // Catch:{ all -> 0x030d }
            goto L_0x0313
        L_0x02ef:
            r0 = move-exception
            goto L_0x02f9
        L_0x02f1:
            r0 = move-exception
            r16 = r12
            goto L_0x02f9
        L_0x02f5:
            r0 = move-exception
            r2 = 0
            r16 = r2
        L_0x02f9:
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0316 }
            r1.mo35384e(r0)     // Catch:{ all -> 0x0316 }
            if (r16 == 0) goto L_0x0303
            r16.close()     // Catch:{ all -> 0x030d }
        L_0x0303:
            com.tencent.stat.e$a r0 = r7.f7301d     // Catch:{ all -> 0x030d }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x030d }
            r0.endTransaction()     // Catch:{ all -> 0x030d }
            goto L_0x0313
        L_0x030d:
            r0 = move-exception
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i
            r1.mo35384e(r0)
        L_0x0313:
            com.tencent.stat.common.DeviceInfo r0 = r7.f7300b
            return r0
        L_0x0316:
            r0 = move-exception
            r1 = r0
            if (r16 == 0) goto L_0x031d
            r16.close()     // Catch:{ all -> 0x0327 }
        L_0x031d:
            com.tencent.stat.e$a r0 = r7.f7301d     // Catch:{ all -> 0x0327 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x0327 }
            r0.endTransaction()     // Catch:{ all -> 0x0327 }
            goto L_0x032d
        L_0x0327:
            r0 = move-exception
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.C3226e.f7296i
            r2.mo35384e(r0)
        L_0x032d:
            goto L_0x032f
        L_0x032e:
            throw r1
        L_0x032f:
            goto L_0x032e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.mo35429b(android.content.Context):com.tencent.stat.common.DeviceInfo");
    }

    /* renamed from: a */
    private String m8038a(String str) {
        return StatConstants.MTA_DB2SP_TAG + str;
    }

    /* renamed from: c */
    private boolean m8059c(Context context) {
        return StatPreferences.contains(context, m8038a(Config.CUSTOM_USER_ID)) || StatPreferences.contains(context, m8038a("user_type")) || StatPreferences.contains(context, m8038a("app_ver")) || StatPreferences.contains(context, m8038a("ts"));
    }

    /* renamed from: a */
    private void m8041a(Context context, String str, int i, long j) {
        StatPreferences.putString(context, m8038a(Config.CUSTOM_USER_ID), str);
        StatPreferences.putInt(context, m8038a("user_type"), i);
        StatPreferences.putString(context, m8038a("app_ver"), StatCommonHelper.getAppVersion(context));
        StatPreferences.putLong(context, m8038a("ts"), j);
    }

    /* renamed from: a */
    private String m8039a(List<C3235b> list) {
        StringBuilder sb = new StringBuilder(list.size() * 3);
        sb.append("event_id in (");
        int size = list.size();
        int i = 0;
        for (C3235b bVar : list) {
            sb.append(bVar.f7335a);
            if (i != size - 1) {
                sb.append(",");
            }
            i++;
        }
        sb.append(")");
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00bf, code lost:
        r7 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r8 = com.tencent.stat.C3226e.f7296i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00dd, code lost:
        r7 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00de, code lost:
        if (r8 != null) goto L_0x00e0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r8.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00e4, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        com.tencent.stat.C3226e.f7296i.mo35384e(r8);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:27:0x00bb, B:36:0x00cc, B:48:0x00e0] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00d3 A[SYNTHETIC, Splitter:B:39:0x00d3] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void m8050a(java.util.List<com.tencent.stat.C3226e.C3235b> r7, boolean r8) {
        /*
            r6 = this;
            monitor-enter(r6)
            int r0 = r7.size()     // Catch:{ all -> 0x00eb }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r6)
            return
        L_0x0009:
            boolean r0 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x00eb }
            if (r0 == 0) goto L_0x0031
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00eb }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x00eb }
            r1.<init>()     // Catch:{ all -> 0x00eb }
            java.lang.String r2 = "Delete "
            r1.append(r2)     // Catch:{ all -> 0x00eb }
            int r2 = r7.size()     // Catch:{ all -> 0x00eb }
            r1.append(r2)     // Catch:{ all -> 0x00eb }
            java.lang.String r2 = " events, important:"
            r1.append(r2)     // Catch:{ all -> 0x00eb }
            r1.append(r8)     // Catch:{ all -> 0x00eb }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x00eb }
            r0.mo35388i(r1)     // Catch:{ all -> 0x00eb }
        L_0x0031:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00eb }
            int r1 = r7.size()     // Catch:{ all -> 0x00eb }
            int r1 = r1 * 3
            r0.<init>(r1)     // Catch:{ all -> 0x00eb }
            java.lang.String r1 = "event_id in ("
            r0.append(r1)     // Catch:{ all -> 0x00eb }
            r1 = 0
            int r2 = r7.size()     // Catch:{ all -> 0x00eb }
            java.util.Iterator r7 = r7.iterator()     // Catch:{ all -> 0x00eb }
        L_0x004a:
            boolean r3 = r7.hasNext()     // Catch:{ all -> 0x00eb }
            if (r3 == 0) goto L_0x0067
            java.lang.Object r3 = r7.next()     // Catch:{ all -> 0x00eb }
            com.tencent.stat.e$b r3 = (com.tencent.stat.C3226e.C3235b) r3     // Catch:{ all -> 0x00eb }
            long r3 = r3.f7335a     // Catch:{ all -> 0x00eb }
            r0.append(r3)     // Catch:{ all -> 0x00eb }
            int r3 = r2 + -1
            if (r1 == r3) goto L_0x0064
            java.lang.String r3 = ","
            r0.append(r3)     // Catch:{ all -> 0x00eb }
        L_0x0064:
            int r1 = r1 + 1
            goto L_0x004a
        L_0x0067:
            java.lang.String r7 = ")"
            r0.append(r7)     // Catch:{ all -> 0x00eb }
            r7 = 0
            android.database.sqlite.SQLiteDatabase r8 = r6.m8060d(r8)     // Catch:{ all -> 0x00c8 }
            r8.beginTransaction()     // Catch:{ all -> 0x00c6 }
            java.lang.String r1 = "events"
            java.lang.String r3 = r0.toString()     // Catch:{ all -> 0x00c6 }
            int r7 = r8.delete(r1, r3, r7)     // Catch:{ all -> 0x00c6 }
            boolean r1 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x00c6 }
            if (r1 == 0) goto L_0x00ae
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00c6 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c6 }
            r3.<init>()     // Catch:{ all -> 0x00c6 }
            java.lang.String r4 = "delete "
            r3.append(r4)     // Catch:{ all -> 0x00c6 }
            r3.append(r2)     // Catch:{ all -> 0x00c6 }
            java.lang.String r2 = " event "
            r3.append(r2)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00c6 }
            r3.append(r0)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = ", success delete:"
            r3.append(r0)     // Catch:{ all -> 0x00c6 }
            r3.append(r7)     // Catch:{ all -> 0x00c6 }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x00c6 }
            r1.mo35388i(r0)     // Catch:{ all -> 0x00c6 }
        L_0x00ae:
            int r0 = r6.f7299a     // Catch:{ all -> 0x00c6 }
            int r0 = r0 - r7
            r6.f7299a = r0     // Catch:{ all -> 0x00c6 }
            r8.setTransactionSuccessful()     // Catch:{ all -> 0x00c6 }
            r6.m8064h()     // Catch:{ all -> 0x00c6 }
            if (r8 == 0) goto L_0x00db
            r8.endTransaction()     // Catch:{ all -> 0x00bf }
            goto L_0x00db
        L_0x00bf:
            r7 = move-exception
            com.tencent.stat.common.StatLogger r8 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00eb }
        L_0x00c2:
            r8.mo35384e(r7)     // Catch:{ all -> 0x00eb }
            goto L_0x00db
        L_0x00c6:
            r7 = move-exception
            goto L_0x00cc
        L_0x00c8:
            r8 = move-exception
            r5 = r8
            r8 = r7
            r7 = r5
        L_0x00cc:
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00dd }
            r0.mo35384e(r7)     // Catch:{ all -> 0x00dd }
            if (r8 == 0) goto L_0x00db
            r8.endTransaction()     // Catch:{ all -> 0x00d7 }
            goto L_0x00db
        L_0x00d7:
            r7 = move-exception
            com.tencent.stat.common.StatLogger r8 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00eb }
            goto L_0x00c2
        L_0x00db:
            monitor-exit(r6)
            return
        L_0x00dd:
            r7 = move-exception
            if (r8 == 0) goto L_0x00ea
            r8.endTransaction()     // Catch:{ all -> 0x00e4 }
            goto L_0x00ea
        L_0x00e4:
            r8 = move-exception
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00eb }
            r0.mo35384e(r8)     // Catch:{ all -> 0x00eb }
        L_0x00ea:
            throw r7     // Catch:{ all -> 0x00eb }
        L_0x00eb:
            r7 = move-exception
            monitor-exit(r6)
            goto L_0x00ef
        L_0x00ee:
            throw r7
        L_0x00ef:
            goto L_0x00ee
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8050a(java.util.List, boolean):void");
    }

    /* renamed from: c */
    private int m8058c(boolean z) {
        if (!z) {
            return StatConfig.getMaxSendRetryCount();
        }
        return StatConfig.getMaxImportantDataSendRetryCount();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00b6, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        r6 = com.tencent.stat.C3226e.f7296i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d2, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00d3, code lost:
        if (r7 != null) goto L_0x00d5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r7.endTransaction();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00d9, code lost:
        r6 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        com.tencent.stat.C3226e.f7296i.mo35384e(r6);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:27:0x00b2, B:36:0x00c1, B:48:0x00d5] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00c8 A[SYNTHETIC, Splitter:B:39:0x00c8] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void m8049a(java.util.List<com.tencent.stat.C3226e.C3235b> r5, int r6, boolean r7) {
        /*
            r4 = this;
            monitor-enter(r4)
            int r0 = r5.size()     // Catch:{ all -> 0x00e0 }
            if (r0 != 0) goto L_0x0009
            monitor-exit(r4)
            return
        L_0x0009:
            int r0 = r4.m8058c(r7)     // Catch:{ all -> 0x00e0 }
            r1 = 0
            android.database.sqlite.SQLiteDatabase r7 = r4.m8060d(r7)     // Catch:{ all -> 0x00bf }
            r2 = 2
            if (r6 != r2) goto L_0x0033
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r0.<init>()     // Catch:{ all -> 0x00bd }
            java.lang.String r2 = "update events set status="
            r0.append(r2)     // Catch:{ all -> 0x00bd }
            r0.append(r6)     // Catch:{ all -> 0x00bd }
            java.lang.String r6 = ", send_count=send_count+1  where "
            r0.append(r6)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = r4.m8039a(r5)     // Catch:{ all -> 0x00bd }
            r0.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x00bd }
            goto L_0x006d
        L_0x0033:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r2.<init>()     // Catch:{ all -> 0x00bd }
            java.lang.String r3 = "update events set status="
            r2.append(r3)     // Catch:{ all -> 0x00bd }
            r2.append(r6)     // Catch:{ all -> 0x00bd }
            java.lang.String r6 = " where "
            r2.append(r6)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = r4.m8039a(r5)     // Catch:{ all -> 0x00bd }
            r2.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x00bd }
            int r6 = r4.f7307m     // Catch:{ all -> 0x00bd }
            int r6 = r6 % 3
            if (r6 != 0) goto L_0x0067
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r6.<init>()     // Catch:{ all -> 0x00bd }
            java.lang.String r1 = "delete from events where send_count>"
            r6.append(r1)     // Catch:{ all -> 0x00bd }
            r6.append(r0)     // Catch:{ all -> 0x00bd }
            java.lang.String r1 = r6.toString()     // Catch:{ all -> 0x00bd }
        L_0x0067:
            int r6 = r4.f7307m     // Catch:{ all -> 0x00bd }
            int r6 = r6 + 1
            r4.f7307m = r6     // Catch:{ all -> 0x00bd }
        L_0x006d:
            boolean r6 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x00bd }
            if (r6 == 0) goto L_0x0089
            com.tencent.stat.common.StatLogger r6 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r0.<init>()     // Catch:{ all -> 0x00bd }
            java.lang.String r2 = "update sql:"
            r0.append(r2)     // Catch:{ all -> 0x00bd }
            r0.append(r5)     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00bd }
            r6.mo35388i(r0)     // Catch:{ all -> 0x00bd }
        L_0x0089:
            r7.beginTransaction()     // Catch:{ all -> 0x00bd }
            r7.execSQL(r5)     // Catch:{ all -> 0x00bd }
            if (r1 == 0) goto L_0x00ad
            com.tencent.stat.common.StatLogger r5 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00bd }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00bd }
            r6.<init>()     // Catch:{ all -> 0x00bd }
            java.lang.String r0 = "update for delete sql:"
            r6.append(r0)     // Catch:{ all -> 0x00bd }
            r6.append(r1)     // Catch:{ all -> 0x00bd }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x00bd }
            r5.mo35388i(r6)     // Catch:{ all -> 0x00bd }
            r7.execSQL(r1)     // Catch:{ all -> 0x00bd }
            r4.m8064h()     // Catch:{ all -> 0x00bd }
        L_0x00ad:
            r7.setTransactionSuccessful()     // Catch:{ all -> 0x00bd }
            if (r7 == 0) goto L_0x00d0
            r7.endTransaction()     // Catch:{ all -> 0x00b6 }
            goto L_0x00d0
        L_0x00b6:
            r5 = move-exception
            com.tencent.stat.common.StatLogger r6 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00e0 }
        L_0x00b9:
            r6.mo35384e(r5)     // Catch:{ all -> 0x00e0 }
            goto L_0x00d0
        L_0x00bd:
            r5 = move-exception
            goto L_0x00c1
        L_0x00bf:
            r5 = move-exception
            r7 = r1
        L_0x00c1:
            com.tencent.stat.common.StatLogger r6 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00d2 }
            r6.mo35384e(r5)     // Catch:{ all -> 0x00d2 }
            if (r7 == 0) goto L_0x00d0
            r7.endTransaction()     // Catch:{ all -> 0x00cc }
            goto L_0x00d0
        L_0x00cc:
            r5 = move-exception
            com.tencent.stat.common.StatLogger r6 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00e0 }
            goto L_0x00b9
        L_0x00d0:
            monitor-exit(r4)
            return
        L_0x00d2:
            r5 = move-exception
            if (r7 == 0) goto L_0x00df
            r7.endTransaction()     // Catch:{ all -> 0x00d9 }
            goto L_0x00df
        L_0x00d9:
            r6 = move-exception
            com.tencent.stat.common.StatLogger r7 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00e0 }
            r7.mo35384e(r6)     // Catch:{ all -> 0x00e0 }
        L_0x00df:
            throw r5     // Catch:{ all -> 0x00e0 }
        L_0x00e0:
            r5 = move-exception
            monitor-exit(r4)
            goto L_0x00e4
        L_0x00e3:
            throw r5
        L_0x00e4:
            goto L_0x00e3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8049a(java.util.List, int, boolean):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35427a(List<C3235b> list, int i, boolean z, boolean z2) {
        Handler handler = this.f7303f;
        if (handler != null) {
            final List<C3235b> list2 = list;
            final int i2 = i;
            final boolean z3 = z;
            final boolean z4 = z2;
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3226e.C32271 */

                public void run() {
                    C3226e.this.m8049a(list2, i2, z3);
                    if (z4) {
                        list2.clear();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35428a(final List<C3235b> list, final boolean z, final boolean z2) {
        Handler handler = this.f7303f;
        if (handler != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3226e.C32282 */

                public void run() {
                    C3226e.this.m8050a(list, z);
                    if (z2) {
                        list.clear();
                    }
                }
            });
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:14|15|16|(2:18|49)(1:48)) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        com.tencent.stat.C3226e.f7296i.mo35396w("fetch row error, passed.");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0087, code lost:
        if (r14 < 40) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0089, code lost:
        r14 = r14 + 1;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x007e */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m8056b(java.util.List<com.tencent.stat.C3226e.C3235b> r13, int r14, boolean r15) {
        /*
            r12 = this;
            monitor-enter(r12)
            r0 = 0
            android.database.sqlite.SQLiteDatabase r1 = r12.m8061e(r15)     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "events"
            r3 = 0
            java.lang.String r4 = "status=?"
            r15 = 1
            java.lang.String[] r5 = new java.lang.String[r15]     // Catch:{ all -> 0x0097 }
            java.lang.String r6 = java.lang.Integer.toString(r15)     // Catch:{ all -> 0x0097 }
            r10 = 0
            r5[r10] = r6     // Catch:{ all -> 0x0097 }
            r6 = 0
            r7 = 0
            r8 = 0
            java.lang.String r9 = java.lang.Integer.toString(r14)     // Catch:{ all -> 0x0097 }
            android.database.Cursor r0 = r1.query(r2, r3, r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x0097 }
        L_0x0020:
            boolean r1 = r0.moveToNext()     // Catch:{ all -> 0x0097 }
            if (r1 == 0) goto L_0x008c
            long r8 = r0.getLong(r10)     // Catch:{ Exception -> 0x007e }
            java.lang.String r1 = r0.getString(r15)     // Catch:{ Exception -> 0x007e }
            boolean r2 = com.tencent.stat.StatConfig.f6949g     // Catch:{ Exception -> 0x007e }
            if (r2 != 0) goto L_0x0036
            java.lang.String r1 = com.tencent.stat.common.Util.decode(r1)     // Catch:{ Exception -> 0x007e }
        L_0x0036:
            r5 = r1
            r1 = 2
            int r6 = r0.getInt(r1)     // Catch:{ Exception -> 0x007e }
            r1 = 3
            int r1 = r0.getInt(r1)     // Catch:{ Exception -> 0x007e }
            com.tencent.stat.e$b r11 = new com.tencent.stat.e$b     // Catch:{ Exception -> 0x007e }
            r2 = r11
            r3 = r8
            r7 = r1
            r2.<init>(r3, r5, r6, r7)     // Catch:{ Exception -> 0x007e }
            boolean r2 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ Exception -> 0x007e }
            if (r2 == 0) goto L_0x007a
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.C3226e.f7296i     // Catch:{ Exception -> 0x007e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x007e }
            r3.<init>()     // Catch:{ Exception -> 0x007e }
            java.lang.String r4 = "peek event, id="
            r3.append(r4)     // Catch:{ Exception -> 0x007e }
            r3.append(r8)     // Catch:{ Exception -> 0x007e }
            java.lang.String r4 = ",send_count="
            r3.append(r4)     // Catch:{ Exception -> 0x007e }
            r3.append(r1)     // Catch:{ Exception -> 0x007e }
            java.lang.String r1 = ",timestamp="
            r3.append(r1)     // Catch:{ Exception -> 0x007e }
            r1 = 4
            long r4 = r0.getLong(r1)     // Catch:{ Exception -> 0x007e }
            r3.append(r4)     // Catch:{ Exception -> 0x007e }
            java.lang.String r1 = r3.toString()     // Catch:{ Exception -> 0x007e }
            r2.mo35388i(r1)     // Catch:{ Exception -> 0x007e }
        L_0x007a:
            r13.add(r11)     // Catch:{ Exception -> 0x007e }
            goto L_0x0020
        L_0x007e:
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0097 }
            java.lang.String r2 = "fetch row error, passed."
            r1.mo35396w(r2)     // Catch:{ all -> 0x0097 }
            r1 = 40
            if (r14 >= r1) goto L_0x0020
            int r14 = r14 + 1
            goto L_0x0020
        L_0x008c:
            if (r0 == 0) goto L_0x00a5
            r0.close()     // Catch:{ all -> 0x0092 }
            goto L_0x00a5
        L_0x0092:
            r13 = move-exception
        L_0x0093:
            r13.printStackTrace()     // Catch:{ all -> 0x00b3 }
            goto L_0x00a5
        L_0x0097:
            r13 = move-exception
            com.tencent.stat.common.StatLogger r14 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00a7 }
            r14.mo35384e(r13)     // Catch:{ all -> 0x00a7 }
            if (r0 == 0) goto L_0x00a5
            r0.close()     // Catch:{ all -> 0x00a3 }
            goto L_0x00a5
        L_0x00a3:
            r13 = move-exception
            goto L_0x0093
        L_0x00a5:
            monitor-exit(r12)
            return
        L_0x00a7:
            r13 = move-exception
            if (r0 == 0) goto L_0x00b2
            r0.close()     // Catch:{ all -> 0x00ae }
            goto L_0x00b2
        L_0x00ae:
            r14 = move-exception
            r14.printStackTrace()     // Catch:{ all -> 0x00b3 }
        L_0x00b2:
            throw r13     // Catch:{ all -> 0x00b3 }
        L_0x00b3:
            r13 = move-exception
            monitor-exit(r12)
            goto L_0x00b7
        L_0x00b6:
            throw r13
        L_0x00b7:
            goto L_0x00b6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8056b(java.util.List, int, boolean):void");
    }

    /* renamed from: h */
    private void m8064h() {
        this.f7299a = m8065i() + m8066j();
    }

    /* renamed from: i */
    private int m8065i() {
        try {
            return (int) DatabaseUtils.queryNumEntries(this.f7301d.getReadableDatabase(), "events");
        } catch (Throwable th) {
            f7296i.mo35384e(th);
            return 0;
        }
    }

    /* renamed from: j */
    private int m8066j() {
        try {
            return (int) DatabaseUtils.queryNumEntries(this.f7302e.getReadableDatabase(), "events");
        } catch (Throwable th) {
            f7296i.mo35384e(th);
            return 0;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00de, code lost:
        r1 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:?, code lost:
        r2 = com.tencent.stat.C3226e.f7296i;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0136, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0137, code lost:
        if (r3 != null) goto L_0x0139;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:?, code lost:
        r3.endTransaction();
        m8064h();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0140, code lost:
        r2 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        com.tencent.stat.C3226e.f7296i.mo35384e(r2);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:31:0x00d7, B:40:0x00e9, B:55:0x0139] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00f0 A[SYNTHETIC, Splitter:B:43:0x00f0] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0104 A[Catch:{ all -> 0x0136, all -> 0x0140, all -> 0x00de }] */
    /* renamed from: k */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m8067k() {
        /*
            r13 = this;
            boolean r0 = r13.f7309o
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            java.util.concurrent.ConcurrentHashMap<com.tencent.stat.event.Event, java.lang.String> r0 = r13.f7308n
            monitor-enter(r0)
            java.util.concurrent.ConcurrentHashMap<com.tencent.stat.event.Event, java.lang.String> r1 = r13.f7308n     // Catch:{ all -> 0x0147 }
            int r1 = r1.size()     // Catch:{ all -> 0x0147 }
            if (r1 != 0) goto L_0x0012
            monitor-exit(r0)     // Catch:{ all -> 0x0147 }
            return
        L_0x0012:
            r1 = 1
            r13.f7309o = r1     // Catch:{ all -> 0x0147 }
            boolean r2 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x0147 }
            if (r2 == 0) goto L_0x004b
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0147 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0147 }
            r3.<init>()     // Catch:{ all -> 0x0147 }
            java.lang.String r4 = "insert "
            r3.append(r4)     // Catch:{ all -> 0x0147 }
            java.util.concurrent.ConcurrentHashMap<com.tencent.stat.event.Event, java.lang.String> r4 = r13.f7308n     // Catch:{ all -> 0x0147 }
            int r4 = r4.size()     // Catch:{ all -> 0x0147 }
            r3.append(r4)     // Catch:{ all -> 0x0147 }
            java.lang.String r4 = " events ,numEventsCachedInMemory:"
            r3.append(r4)     // Catch:{ all -> 0x0147 }
            int r4 = com.tencent.stat.StatConfig.f6955m     // Catch:{ all -> 0x0147 }
            r3.append(r4)     // Catch:{ all -> 0x0147 }
            java.lang.String r4 = ",numStoredEvents:"
            r3.append(r4)     // Catch:{ all -> 0x0147 }
            int r4 = r13.f7299a     // Catch:{ all -> 0x0147 }
            r3.append(r4)     // Catch:{ all -> 0x0147 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0147 }
            r2.mo35388i(r3)     // Catch:{ all -> 0x0147 }
        L_0x004b:
            r2 = 0
            com.tencent.stat.e$a r3 = r13.f7301d     // Catch:{ all -> 0x00e7 }
            android.database.sqlite.SQLiteDatabase r3 = r3.getWritableDatabase()     // Catch:{ all -> 0x00e7 }
            r3.beginTransaction()     // Catch:{ all -> 0x00e5 }
            java.util.concurrent.ConcurrentHashMap<com.tencent.stat.event.Event, java.lang.String> r4 = r13.f7308n     // Catch:{ all -> 0x00e5 }
            java.util.Set r4 = r4.entrySet()     // Catch:{ all -> 0x00e5 }
            java.util.Iterator r4 = r4.iterator()     // Catch:{ all -> 0x00e5 }
        L_0x005f:
            boolean r5 = r4.hasNext()     // Catch:{ all -> 0x00e5 }
            if (r5 == 0) goto L_0x00d2
            java.lang.Object r5 = r4.next()     // Catch:{ all -> 0x00e5 }
            java.util.Map$Entry r5 = (java.util.Map.Entry) r5     // Catch:{ all -> 0x00e5 }
            java.lang.Object r5 = r5.getKey()     // Catch:{ all -> 0x00e5 }
            com.tencent.stat.event.Event r5 = (com.tencent.stat.event.Event) r5     // Catch:{ all -> 0x00e5 }
            android.content.ContentValues r6 = new android.content.ContentValues     // Catch:{ all -> 0x00e5 }
            r6.<init>()     // Catch:{ all -> 0x00e5 }
            java.lang.String r7 = r5.toJsonString()     // Catch:{ all -> 0x00e5 }
            boolean r8 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x00e5 }
            if (r8 == 0) goto L_0x0096
            com.tencent.stat.common.StatLogger r8 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00e5 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x00e5 }
            r9.<init>()     // Catch:{ all -> 0x00e5 }
            java.lang.String r10 = "insert content:"
            r9.append(r10)     // Catch:{ all -> 0x00e5 }
            r9.append(r7)     // Catch:{ all -> 0x00e5 }
            java.lang.String r9 = r9.toString()     // Catch:{ all -> 0x00e5 }
            r8.mo35388i(r9)     // Catch:{ all -> 0x00e5 }
        L_0x0096:
            java.lang.String r7 = com.tencent.stat.common.Util.encode(r7)     // Catch:{ all -> 0x00e5 }
            if (r7 == 0) goto L_0x00ce
            int r8 = r7.length()     // Catch:{ all -> 0x00e5 }
            long r8 = (long) r8     // Catch:{ all -> 0x00e5 }
            long r10 = r13.f7306k     // Catch:{ all -> 0x00e5 }
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 >= 0) goto L_0x00ce
            java.lang.String r8 = "content"
            r6.put(r8, r7)     // Catch:{ all -> 0x00e5 }
            java.lang.String r7 = "send_count"
            java.lang.String r8 = "0"
            r6.put(r7, r8)     // Catch:{ all -> 0x00e5 }
            java.lang.String r7 = "status"
            java.lang.String r8 = java.lang.Integer.toString(r1)     // Catch:{ all -> 0x00e5 }
            r6.put(r7, r8)     // Catch:{ all -> 0x00e5 }
            java.lang.String r7 = "timestamp"
            long r8 = r5.getTimestamp()     // Catch:{ all -> 0x00e5 }
            java.lang.Long r5 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x00e5 }
            r6.put(r7, r5)     // Catch:{ all -> 0x00e5 }
            java.lang.String r5 = "events"
            r3.insert(r5, r2, r6)     // Catch:{ all -> 0x00e5 }
        L_0x00ce:
            r4.remove()     // Catch:{ all -> 0x00e5 }
            goto L_0x005f
        L_0x00d2:
            r3.setTransactionSuccessful()     // Catch:{ all -> 0x00e5 }
            if (r3 == 0) goto L_0x00fb
            r3.endTransaction()     // Catch:{ all -> 0x00de }
            r13.m8064h()     // Catch:{ all -> 0x00de }
            goto L_0x00fb
        L_0x00de:
            r1 = move-exception
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0147 }
        L_0x00e1:
            r2.mo35384e(r1)     // Catch:{ all -> 0x0147 }
            goto L_0x00fb
        L_0x00e5:
            r1 = move-exception
            goto L_0x00e9
        L_0x00e7:
            r1 = move-exception
            r3 = r2
        L_0x00e9:
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0136 }
            r2.mo35384e(r1)     // Catch:{ all -> 0x0136 }
            if (r3 == 0) goto L_0x00fb
            r3.endTransaction()     // Catch:{ all -> 0x00f7 }
            r13.m8064h()     // Catch:{ all -> 0x00f7 }
            goto L_0x00fb
        L_0x00f7:
            r1 = move-exception
            com.tencent.stat.common.StatLogger r2 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0147 }
            goto L_0x00e1
        L_0x00fb:
            r1 = 0
            r13.f7309o = r1     // Catch:{ all -> 0x0147 }
            boolean r1 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x0147 }
            if (r1 == 0) goto L_0x0134
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0147 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0147 }
            r2.<init>()     // Catch:{ all -> 0x0147 }
            java.lang.String r3 = "after insert, cacheEventsInMemory.size():"
            r2.append(r3)     // Catch:{ all -> 0x0147 }
            java.util.concurrent.ConcurrentHashMap<com.tencent.stat.event.Event, java.lang.String> r3 = r13.f7308n     // Catch:{ all -> 0x0147 }
            int r3 = r3.size()     // Catch:{ all -> 0x0147 }
            r2.append(r3)     // Catch:{ all -> 0x0147 }
            java.lang.String r3 = ",numEventsCachedInMemory:"
            r2.append(r3)     // Catch:{ all -> 0x0147 }
            int r3 = com.tencent.stat.StatConfig.f6955m     // Catch:{ all -> 0x0147 }
            r2.append(r3)     // Catch:{ all -> 0x0147 }
            java.lang.String r3 = ",numStoredEvents:"
            r2.append(r3)     // Catch:{ all -> 0x0147 }
            int r3 = r13.f7299a     // Catch:{ all -> 0x0147 }
            r2.append(r3)     // Catch:{ all -> 0x0147 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0147 }
            r1.mo35388i(r2)     // Catch:{ all -> 0x0147 }
        L_0x0134:
            monitor-exit(r0)     // Catch:{ all -> 0x0147 }
            return
        L_0x0136:
            r1 = move-exception
            if (r3 == 0) goto L_0x0146
            r3.endTransaction()     // Catch:{ all -> 0x0140 }
            r13.m8064h()     // Catch:{ all -> 0x0140 }
            goto L_0x0146
        L_0x0140:
            r2 = move-exception
            com.tencent.stat.common.StatLogger r3 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0147 }
            r3.mo35384e(r2)     // Catch:{ all -> 0x0147 }
        L_0x0146:
            throw r1     // Catch:{ all -> 0x0147 }
        L_0x0147:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0147 }
            goto L_0x014b
        L_0x014a:
            throw r1
        L_0x014b:
            goto L_0x014a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8067k():void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public void mo35431d() {
        if (StatConfig.isEnableStatService()) {
            try {
                this.f7303f.post(new Runnable() {
                    /* class com.tencent.stat.C3226e.C32293 */

                    public void run() {
                        C3226e.this.m8067k();
                    }
                });
            } catch (Throwable th) {
                f7296i.mo35384e(th);
            }
        }
    }

    /* renamed from: d */
    private SQLiteDatabase m8060d(boolean z) {
        if (!z) {
            return this.f7301d.getWritableDatabase();
        }
        return this.f7302e.getWritableDatabase();
    }

    /* renamed from: e */
    private SQLiteDatabase m8061e(boolean z) {
        if (!z) {
            return this.f7301d.getReadableDatabase();
        }
        return this.f7302e.getReadableDatabase();
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x00aa A[SYNTHETIC, Splitter:B:33:0x00aa] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b1  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00dc  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m8048a(com.tencent.stat.event.Event r13, com.tencent.stat.StatDispatchCallback r14, boolean r15) {
        /*
            r12 = this;
            r0 = 1
            r1 = 0
            r3 = 0
            android.database.sqlite.SQLiteDatabase r4 = r12.m8060d(r15)     // Catch:{ all -> 0x009f }
            r4.beginTransaction()     // Catch:{ all -> 0x009d }
            java.lang.String r5 = "events"
            if (r15 != 0) goto L_0x002f
            int r15 = r12.f7299a     // Catch:{ all -> 0x009d }
            int r6 = com.tencent.stat.StatConfig.getMaxStoreEventCount()     // Catch:{ all -> 0x009d }
            if (r15 <= r6) goto L_0x002f
            com.tencent.stat.common.StatLogger r15 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x009d }
            java.lang.String r6 = "Too many events stored in db."
            r15.warn(r6)     // Catch:{ all -> 0x009d }
            int r15 = r12.f7299a     // Catch:{ all -> 0x009d }
            com.tencent.stat.e$a r6 = r12.f7301d     // Catch:{ all -> 0x009d }
            android.database.sqlite.SQLiteDatabase r6 = r6.getWritableDatabase()     // Catch:{ all -> 0x009d }
            java.lang.String r7 = "event_id in (select event_id from events where timestamp in (select min(timestamp) from events) limit 1)"
            int r6 = r6.delete(r5, r7, r3)     // Catch:{ all -> 0x009d }
            int r15 = r15 - r6
            r12.f7299a = r15     // Catch:{ all -> 0x009d }
        L_0x002f:
            android.content.ContentValues r15 = new android.content.ContentValues     // Catch:{ all -> 0x009d }
            r15.<init>()     // Catch:{ all -> 0x009d }
            java.lang.String r6 = r13.toJsonString()     // Catch:{ all -> 0x009d }
            boolean r7 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x009d }
            if (r7 == 0) goto L_0x0054
            com.tencent.stat.common.StatLogger r7 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x009d }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x009d }
            r8.<init>()     // Catch:{ all -> 0x009d }
            java.lang.String r9 = "insert 1 event, content:"
            r8.append(r9)     // Catch:{ all -> 0x009d }
            r8.append(r6)     // Catch:{ all -> 0x009d }
            java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x009d }
            r7.mo35388i(r8)     // Catch:{ all -> 0x009d }
        L_0x0054:
            java.lang.String r6 = com.tencent.stat.common.Util.encode(r6)     // Catch:{ all -> 0x009d }
            if (r6 == 0) goto L_0x008c
            int r7 = r6.length()     // Catch:{ all -> 0x009d }
            long r7 = (long) r7     // Catch:{ all -> 0x009d }
            long r9 = r12.f7306k     // Catch:{ all -> 0x009d }
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 >= 0) goto L_0x008c
            java.lang.String r7 = "content"
            r15.put(r7, r6)     // Catch:{ all -> 0x009d }
            java.lang.String r6 = "send_count"
            java.lang.String r7 = "0"
            r15.put(r6, r7)     // Catch:{ all -> 0x009d }
            java.lang.String r6 = "status"
            java.lang.String r7 = java.lang.Integer.toString(r0)     // Catch:{ all -> 0x009d }
            r15.put(r6, r7)     // Catch:{ all -> 0x009d }
            java.lang.String r6 = "timestamp"
            long r7 = r13.getTimestamp()     // Catch:{ all -> 0x009d }
            java.lang.Long r7 = java.lang.Long.valueOf(r7)     // Catch:{ all -> 0x009d }
            r15.put(r6, r7)     // Catch:{ all -> 0x009d }
            long r5 = r4.insert(r5, r3, r15)     // Catch:{ all -> 0x009d }
            goto L_0x008d
        L_0x008c:
            r5 = r1
        L_0x008d:
            r4.setTransactionSuccessful()     // Catch:{ all -> 0x009d }
            if (r4 == 0) goto L_0x00ad
            r4.endTransaction()     // Catch:{ all -> 0x0096 }
            goto L_0x00ad
        L_0x0096:
            r15 = move-exception
            com.tencent.stat.common.StatLogger r3 = com.tencent.stat.C3226e.f7296i
            r3.mo35384e(r15)
            goto L_0x00ad
        L_0x009d:
            r15 = move-exception
            goto L_0x00a1
        L_0x009f:
            r15 = move-exception
            r4 = r3
        L_0x00a1:
            r5 = -1
            com.tencent.stat.common.StatLogger r3 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00f7 }
            r3.mo35384e(r15)     // Catch:{ all -> 0x00f7 }
            if (r4 == 0) goto L_0x00ad
            r4.endTransaction()     // Catch:{ all -> 0x0096 }
        L_0x00ad:
            int r15 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r15 <= 0) goto L_0x00dc
            int r15 = r12.f7299a
            int r15 = r15 + r0
            r12.f7299a = r15
            boolean r15 = com.tencent.stat.StatConfig.isDebugEnable()
            if (r15 == 0) goto L_0x00d6
            com.tencent.stat.common.StatLogger r15 = com.tencent.stat.C3226e.f7296i
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "directStoreEvent insert event to db, event:"
            r0.append(r1)
            java.lang.String r13 = r13.toJsonString()
            r0.append(r13)
            java.lang.String r13 = r0.toString()
            r15.mo35381d(r13)
        L_0x00d6:
            if (r14 == 0) goto L_0x00f6
            r14.onDispatchSuccess()
            goto L_0x00f6
        L_0x00dc:
            com.tencent.stat.common.StatLogger r14 = com.tencent.stat.C3226e.f7296i
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r0 = "Failed to store event:"
            r15.append(r0)
            java.lang.String r13 = r13.toJsonString()
            r15.append(r13)
            java.lang.String r13 = r15.toString()
            r14.error(r13)
        L_0x00f6:
            return
        L_0x00f7:
            r13 = move-exception
            if (r4 == 0) goto L_0x0104
            r4.endTransaction()     // Catch:{ all -> 0x00fe }
            goto L_0x0104
        L_0x00fe:
            r14 = move-exception
            com.tencent.stat.common.StatLogger r15 = com.tencent.stat.C3226e.f7296i
            r15.mo35384e(r14)
        L_0x0104:
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8048a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public synchronized void m8055b(Event event, StatDispatchCallback statDispatchCallback, boolean z, boolean z2) {
        if (StatConfig.getMaxStoreEventCount() > 0) {
            if (StatConfig.f6955m > 0 && !z) {
                if (!z2) {
                    if (StatConfig.f6955m > 0) {
                        if (StatConfig.isDebugEnable()) {
                            StatLogger statLogger = f7296i;
                            statLogger.mo35388i("cacheEventsInMemory.size():" + this.f7308n.size() + ",numEventsCachedInMemory:" + StatConfig.f6955m + ",numStoredEvents:" + this.f7299a);
                            StatLogger statLogger2 = f7296i;
                            StringBuilder sb = new StringBuilder();
                            sb.append("cache event:");
                            sb.append(event.toJsonString());
                            statLogger2.mo35388i(sb.toString());
                        }
                        this.f7308n.put(event, "");
                        if (this.f7308n.size() >= StatConfig.f6955m) {
                            m8067k();
                        }
                        if (statDispatchCallback != null) {
                            if (this.f7308n.size() > 0) {
                                m8067k();
                            }
                            statDispatchCallback.onDispatchSuccess();
                        }
                    }
                }
            }
            m8048a(event, statDispatchCallback, z);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35426a(Event event, StatDispatchCallback statDispatchCallback, boolean z, boolean z2) {
        Handler handler = this.f7303f;
        if (handler != null) {
            final Event event2 = event;
            final StatDispatchCallback statDispatchCallback2 = statDispatchCallback;
            final boolean z3 = z;
            final boolean z4 = z2;
            handler.post(new Runnable() {
                /* class com.tencent.stat.C3226e.C32304 */

                public void run() {
                    C3226e.this.m8055b(event2, statDispatchCallback2, z3, z4);
                }
            });
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:39|(2:41|42)|43|44|46|47) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:46:0x0101 */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00e3 A[SYNTHETIC, Splitter:B:32:0x00e3] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:34:0x00e6=Splitter:B:34:0x00e6, B:23:0x00ce=Splitter:B:23:0x00ce} */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void m8054b(com.tencent.stat.StatConfig.C3142a r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            r0 = 0
            java.lang.String r1 = r13.mo35258c()     // Catch:{ all -> 0x00da }
            java.lang.String r2 = com.tencent.stat.common.StatCommonHelper.md5sum(r1)     // Catch:{ all -> 0x00da }
            android.content.ContentValues r3 = new android.content.ContentValues     // Catch:{ all -> 0x00da }
            r3.<init>()     // Catch:{ all -> 0x00da }
            java.lang.String r4 = "content"
            org.json.JSONObject r5 = r13.f6975b     // Catch:{ all -> 0x00da }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00da }
            r3.put(r4, r5)     // Catch:{ all -> 0x00da }
            java.lang.String r4 = "md5sum"
            r3.put(r4, r2)     // Catch:{ all -> 0x00da }
            r13.f6976c = r2     // Catch:{ all -> 0x00da }
            java.lang.String r2 = "version"
            int r4 = r13.f6977d     // Catch:{ all -> 0x00da }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00da }
            r3.put(r2, r4)     // Catch:{ all -> 0x00da }
            com.tencent.stat.e$a r2 = r12.f7301d     // Catch:{ all -> 0x00da }
            android.database.sqlite.SQLiteDatabase r4 = r2.getReadableDatabase()     // Catch:{ all -> 0x00da }
            java.lang.String r5 = "config"
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            android.database.Cursor r2 = r4.query(r5, r6, r7, r8, r9, r10, r11)     // Catch:{ all -> 0x00da }
        L_0x003e:
            boolean r4 = r2.moveToNext()     // Catch:{ all -> 0x00d8 }
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x0050
            int r4 = r2.getInt(r6)     // Catch:{ all -> 0x00d8 }
            int r7 = r13.f6974a     // Catch:{ all -> 0x00d8 }
            if (r4 != r7) goto L_0x003e
            r4 = 1
            goto L_0x0051
        L_0x0050:
            r4 = 0
        L_0x0051:
            com.tencent.stat.e$a r7 = r12.f7301d     // Catch:{ all -> 0x00d8 }
            android.database.sqlite.SQLiteDatabase r7 = r7.getWritableDatabase()     // Catch:{ all -> 0x00d8 }
            r7.beginTransaction()     // Catch:{ all -> 0x00d8 }
            if (r5 != r4) goto L_0x0076
            com.tencent.stat.e$a r0 = r12.f7301d     // Catch:{ all -> 0x00d8 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "config"
            java.lang.String r7 = "type=?"
            java.lang.String[] r5 = new java.lang.String[r5]     // Catch:{ all -> 0x00d8 }
            int r13 = r13.f6974a     // Catch:{ all -> 0x00d8 }
            java.lang.String r13 = java.lang.Integer.toString(r13)     // Catch:{ all -> 0x00d8 }
            r5[r6] = r13     // Catch:{ all -> 0x00d8 }
            int r13 = r0.update(r4, r3, r7, r5)     // Catch:{ all -> 0x00d8 }
            long r3 = (long) r13     // Catch:{ all -> 0x00d8 }
            goto L_0x008d
        L_0x0076:
            java.lang.String r4 = "type"
            int r13 = r13.f6974a     // Catch:{ all -> 0x00d8 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ all -> 0x00d8 }
            r3.put(r4, r13)     // Catch:{ all -> 0x00d8 }
            com.tencent.stat.e$a r13 = r12.f7301d     // Catch:{ all -> 0x00d8 }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ all -> 0x00d8 }
            java.lang.String r4 = "config"
            long r3 = r13.insert(r4, r0, r3)     // Catch:{ all -> 0x00d8 }
        L_0x008d:
            r5 = -1
            int r13 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
            if (r13 != 0) goto L_0x00aa
            com.tencent.stat.common.StatLogger r13 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00d8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r0.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = "Failed to store cfg:"
            r0.append(r3)     // Catch:{ all -> 0x00d8 }
            r0.append(r1)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d8 }
            r13.mo35383e(r0)     // Catch:{ all -> 0x00d8 }
            goto L_0x00c0
        L_0x00aa:
            com.tencent.stat.common.StatLogger r13 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00d8 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d8 }
            r0.<init>()     // Catch:{ all -> 0x00d8 }
            java.lang.String r3 = "Sucessed to store cfg:"
            r0.append(r3)     // Catch:{ all -> 0x00d8 }
            r0.append(r1)     // Catch:{ all -> 0x00d8 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d8 }
            r13.mo35381d(r0)     // Catch:{ all -> 0x00d8 }
        L_0x00c0:
            com.tencent.stat.e$a r13 = r12.f7301d     // Catch:{ all -> 0x00d8 }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ all -> 0x00d8 }
            r13.setTransactionSuccessful()     // Catch:{ all -> 0x00d8 }
            if (r2 == 0) goto L_0x00ce
            r2.close()     // Catch:{ all -> 0x00ff }
        L_0x00ce:
            com.tencent.stat.e$a r13 = r12.f7301d     // Catch:{ Exception -> 0x00ed }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ Exception -> 0x00ed }
        L_0x00d4:
            r13.endTransaction()     // Catch:{ Exception -> 0x00ed }
            goto L_0x00ed
        L_0x00d8:
            r13 = move-exception
            goto L_0x00dc
        L_0x00da:
            r13 = move-exception
            r2 = r0
        L_0x00dc:
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x00ef }
            r0.mo35384e(r13)     // Catch:{ all -> 0x00ef }
            if (r2 == 0) goto L_0x00e6
            r2.close()     // Catch:{ all -> 0x00ff }
        L_0x00e6:
            com.tencent.stat.e$a r13 = r12.f7301d     // Catch:{ Exception -> 0x00ed }
            android.database.sqlite.SQLiteDatabase r13 = r13.getWritableDatabase()     // Catch:{ Exception -> 0x00ed }
            goto L_0x00d4
        L_0x00ed:
            monitor-exit(r12)
            return
        L_0x00ef:
            r13 = move-exception
            if (r2 == 0) goto L_0x00f5
            r2.close()     // Catch:{ all -> 0x00ff }
        L_0x00f5:
            com.tencent.stat.e$a r0 = r12.f7301d     // Catch:{ Exception -> 0x0101 }
            android.database.sqlite.SQLiteDatabase r0 = r0.getWritableDatabase()     // Catch:{ Exception -> 0x0101 }
            r0.endTransaction()     // Catch:{ Exception -> 0x0101 }
            goto L_0x0101
        L_0x00ff:
            r13 = move-exception
            goto L_0x0102
        L_0x0101:
            throw r13     // Catch:{ all -> 0x00ff }
        L_0x0102:
            monitor-exit(r12)
            goto L_0x0105
        L_0x0104:
            throw r13
        L_0x0105:
            goto L_0x0104
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8054b(com.tencent.stat.StatConfig$a):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35425a(final StatConfig.C3142a aVar) {
        if (aVar != null) {
            aVar.mo35255a(f7297j);
            this.f7303f.post(new Runnable() {
                /* class com.tencent.stat.C3226e.C32315 */

                public void run() {
                    C3226e.this.m8054b(aVar);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public void mo35432e() {
        if (!StatConfig.f6944b.mo35257b(f7297j) && !StatConfig.f6932a.mo35257b(f7297j)) {
            Cursor cursor = null;
            try {
                cursor = this.f7301d.getReadableDatabase().query("config", null, null, null, null, null, null);
                while (cursor.moveToNext()) {
                    int i = cursor.getInt(0);
                    String string = cursor.getString(1);
                    String string2 = cursor.getString(2);
                    int i2 = cursor.getInt(3);
                    StatConfig.C3142a aVar = new StatConfig.C3142a(i);
                    aVar.f6974a = i;
                    aVar.f6975b = new JSONObject(string);
                    aVar.f6976c = string2;
                    aVar.f6977d = i2;
                    aVar.mo35255a(f7297j);
                    StatConfig.m7818a(f7297j, aVar);
                }
                if (cursor == null) {
                    return;
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
            cursor.close();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0078, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized void m8040a(int r4, final boolean r5) {
        /*
            r3 = this;
            monitor-enter(r3)
            int r0 = r3.f7299a     // Catch:{ all -> 0x0079 }
            if (r0 <= 0) goto L_0x0077
            if (r4 <= 0) goto L_0x0077
            boolean r0 = com.tencent.stat.StatServiceImpl.m7863a()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x000e
            goto L_0x0077
        L_0x000e:
            boolean r0 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0031
            com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            r1.<init>()     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = "Load "
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            int r2 = r3.f7299a     // Catch:{ all -> 0x0079 }
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = " unsent events"
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0079 }
            r0.mo35388i(r1)     // Catch:{ all -> 0x0079 }
        L_0x0031:
            java.util.ArrayList r0 = new java.util.ArrayList     // Catch:{ all -> 0x0079 }
            r0.<init>(r4)     // Catch:{ all -> 0x0079 }
            r3.m8056b(r0, r4, r5)     // Catch:{ all -> 0x0079 }
            int r4 = r0.size()     // Catch:{ all -> 0x0079 }
            if (r4 <= 0) goto L_0x007f
            boolean r4 = com.tencent.stat.StatConfig.isDebugEnable()     // Catch:{ all -> 0x0079 }
            if (r4 == 0) goto L_0x0064
            com.tencent.stat.common.StatLogger r4 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            r1.<init>()     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = "Peek "
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            int r2 = r0.size()     // Catch:{ all -> 0x0079 }
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r2 = " unsent events."
            r1.append(r2)     // Catch:{ all -> 0x0079 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x0079 }
            r4.mo35388i(r1)     // Catch:{ all -> 0x0079 }
        L_0x0064:
            r4 = 2
            r3.m8049a(r0, r4, r5)     // Catch:{ all -> 0x0079 }
            android.content.Context r4 = com.tencent.stat.C3226e.f7297j     // Catch:{ all -> 0x0079 }
            com.tencent.stat.d r4 = com.tencent.stat.C3220d.m8022b(r4)     // Catch:{ all -> 0x0079 }
            com.tencent.stat.e$6 r1 = new com.tencent.stat.e$6     // Catch:{ all -> 0x0079 }
            r1.<init>(r0, r5)     // Catch:{ all -> 0x0079 }
            r4.mo35416b(r0, r1)     // Catch:{ all -> 0x0079 }
            goto L_0x007f
        L_0x0077:
            monitor-exit(r3)
            return
        L_0x0079:
            r4 = move-exception
            com.tencent.stat.common.StatLogger r5 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0081 }
            r5.mo35384e(r4)     // Catch:{ all -> 0x0081 }
        L_0x007f:
            monitor-exit(r3)
            return
        L_0x0081:
            r4 = move-exception
            monitor-exit(r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.m8040a(int, boolean):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m8053b(int i, boolean z) {
        if (i == -1) {
            if (!z) {
                i = m8065i();
            } else {
                i = m8066j();
            }
        }
        if (i > 0) {
            int sendPeriodMinutes = StatConfig.getSendPeriodMinutes() * 60 * StatConfig.getNumEventsCommitPerSec();
            if (i > sendPeriodMinutes && sendPeriodMinutes > 0) {
                i = sendPeriodMinutes;
            }
            int b = StatConfig.m7826b();
            int i2 = i / b;
            int i3 = i % b;
            if (StatConfig.isDebugEnable()) {
                StatLogger statLogger = f7296i;
                statLogger.mo35388i("sentStoreEventsByDb sendNumbers=" + i + ",important=" + z + ",maxSendNumPerFor1Period=" + sendPeriodMinutes + ",maxCount=" + i2 + ",restNumbers=" + i3);
            }
            for (int i4 = 0; i4 < i2; i4++) {
                StatLogger statLogger2 = f7296i;
                statLogger2.mo35388i("round:" + f7295c + " send i:" + i4);
                m8040a(b, z);
            }
            if (i3 > 0) {
                m8040a(i3, z);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35424a(final int i) {
        this.f7303f.post(new Runnable() {
            /* class com.tencent.stat.C3226e.C32337 */

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: com.tencent.stat.e.a(com.tencent.stat.e, int, boolean):void
             arg types: [com.tencent.stat.e, int, int]
             candidates:
              com.tencent.stat.e.a(com.tencent.stat.e, java.util.List, boolean):void
              com.tencent.stat.e.a(com.tencent.stat.event.Event, com.tencent.stat.StatDispatchCallback, boolean):void
              com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, int, boolean):void
              com.tencent.stat.e.a(java.util.List<com.tencent.stat.e$b>, boolean, boolean):void
              com.tencent.stat.e.a(com.tencent.stat.e, int, boolean):void */
            public void run() {
                C3226e.this.m8053b(i, true);
                C3226e.this.m8053b(i, false);
            }
        });
    }

    /* renamed from: l */
    private void m8068l() {
        Cursor cursor = null;
        try {
            cursor = this.f7301d.getReadableDatabase().query("keyvalues", null, null, null, null, null, null);
            while (cursor.moveToNext()) {
                this.f7310p.put(cursor.getString(0), cursor.getString(1));
            }
            if (cursor == null) {
                return;
            }
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
        cursor.close();
    }

    /* renamed from: com.tencent.stat.e$a */
    static class C3234a extends SQLiteOpenHelper {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public String f7333a = "";

        /* renamed from: b */
        private Context f7334b = null;

        public C3234a(Context context, String str) {
            super(context, str, (SQLiteDatabase.CursorFactory) null, 3);
            this.f7333a = str;
            this.f7334b = context.getApplicationContext();
            if (StatConfig.isDebugEnable()) {
                StatLogger f = C3226e.f7296i;
                f.mo35388i("SQLiteOpenHelper " + this.f7333a);
            }
        }

        public synchronized void close() {
            super.close();
        }

        /* renamed from: a */
        public boolean mo35439a() {
            if (StatConfig.isDebugEnable()) {
                StatLogger f = C3226e.f7296i;
                f.mo35396w("delete " + this.f7333a);
            }
            return this.f7334b.deleteDatabase(this.f7333a);
        }

        public void onCreate(SQLiteDatabase sQLiteDatabase) {
            sQLiteDatabase.execSQL("create table if not exists events(event_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, content TEXT, status INTEGER, send_count INTEGER, timestamp LONG)");
            sQLiteDatabase.execSQL("create table if not exists user(uid TEXT PRIMARY KEY, user_type INTEGER, app_ver TEXT, ts INTEGER)");
            sQLiteDatabase.execSQL("create table if not exists config(type INTEGER PRIMARY KEY NOT NULL, content TEXT, md5sum TEXT, version INTEGER)");
            sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:? A[RETURN, SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void m8080a(android.database.sqlite.SQLiteDatabase r10) {
            /*
                r9 = this;
                r0 = 0
                java.lang.String r2 = "user"
                r3 = 0
                r4 = 0
                r5 = 0
                r6 = 0
                r7 = 0
                r8 = 0
                r1 = r10
                android.database.Cursor r1 = r1.query(r2, r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x0045 }
                android.content.ContentValues r2 = new android.content.ContentValues     // Catch:{ all -> 0x0043 }
                r2.<init>()     // Catch:{ all -> 0x0043 }
                boolean r3 = r1.moveToNext()     // Catch:{ all -> 0x0043 }
                r4 = 1
                r5 = 0
                if (r3 == 0) goto L_0x0033
                java.lang.String r0 = r1.getString(r5)     // Catch:{ all -> 0x0043 }
                r1.getInt(r4)     // Catch:{ all -> 0x0043 }
                r3 = 2
                r1.getString(r3)     // Catch:{ all -> 0x0043 }
                r3 = 3
                r1.getLong(r3)     // Catch:{ all -> 0x0043 }
                java.lang.String r3 = com.tencent.stat.common.Util.encode(r0)     // Catch:{ all -> 0x0043 }
                java.lang.String r6 = "uid"
                r2.put(r6, r3)     // Catch:{ all -> 0x0043 }
            L_0x0033:
                if (r0 == 0) goto L_0x0040
                java.lang.String r3 = "user"
                java.lang.String r6 = "uid=?"
                java.lang.String[] r4 = new java.lang.String[r4]     // Catch:{ all -> 0x0043 }
                r4[r5] = r0     // Catch:{ all -> 0x0043 }
                r10.update(r3, r2, r6, r4)     // Catch:{ all -> 0x0043 }
            L_0x0040:
                if (r1 == 0) goto L_0x0053
                goto L_0x0050
            L_0x0043:
                r10 = move-exception
                goto L_0x0047
            L_0x0045:
                r10 = move-exception
                r1 = r0
            L_0x0047:
                com.tencent.stat.common.StatLogger r0 = com.tencent.stat.C3226e.f7296i     // Catch:{ all -> 0x0054 }
                r0.mo35384e(r10)     // Catch:{ all -> 0x0054 }
                if (r1 == 0) goto L_0x0053
            L_0x0050:
                r1.close()
            L_0x0053:
                return
            L_0x0054:
                r10 = move-exception
                if (r1 == 0) goto L_0x005a
                r1.close()
            L_0x005a:
                throw r10
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.C3226e.C3234a.m8080a(android.database.sqlite.SQLiteDatabase):void");
        }

        /* renamed from: b */
        private void m8081b(SQLiteDatabase sQLiteDatabase) {
            Cursor cursor = null;
            try {
                cursor = sQLiteDatabase.query("events", null, null, null, null, null, null);
                ArrayList<C3235b> arrayList = new ArrayList<>();
                while (cursor.moveToNext()) {
                    arrayList.add(new C3235b(cursor.getLong(0), cursor.getString(1), cursor.getInt(2), cursor.getInt(3)));
                }
                ContentValues contentValues = new ContentValues();
                for (C3235b bVar : arrayList) {
                    contentValues.put("content", Util.encode(bVar.f7336b));
                    sQLiteDatabase.update("events", contentValues, "event_id=?", new String[]{Long.toString(bVar.f7335a)});
                }
                if (cursor == null) {
                    return;
                }
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
            cursor.close();
        }

        public void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
            StatLogger f = C3226e.f7296i;
            f.debug("upgrade DB from oldVersion " + i + " to newVersion " + i2);
            if (i == 1) {
                sQLiteDatabase.execSQL("create table if not exists keyvalues(key TEXT PRIMARY KEY NOT NULL, value TEXT)");
                m8080a(sQLiteDatabase);
                m8081b(sQLiteDatabase);
            }
            if (i == 2) {
                m8080a(sQLiteDatabase);
                m8081b(sQLiteDatabase);
            }
        }
    }
}
