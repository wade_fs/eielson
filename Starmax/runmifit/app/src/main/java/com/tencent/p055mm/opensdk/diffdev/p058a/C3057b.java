package com.tencent.p055mm.opensdk.diffdev.p058a;

import android.util.Log;
import com.tencent.p055mm.opensdk.diffdev.OAuthErrCode;
import com.tencent.p055mm.opensdk.diffdev.OAuthListener;
import java.util.ArrayList;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.b */
final class C3057b implements OAuthListener {

    /* renamed from: g */
    final /* synthetic */ C3056a f6629g;

    C3057b(C3056a aVar) {
        this.f6629g = aVar;
    }

    public final void onAuthFinish(OAuthErrCode oAuthErrCode, String str) {
        Log.d("MicroMsg.SDK.ListenerWrapper", String.format("onAuthFinish, errCode = %s, authCode = %s", oAuthErrCode.toString(), str));
        C3059d unused = this.f6629g.f6627e = null;
        ArrayList<OAuthListener> arrayList = new ArrayList<>();
        arrayList.addAll(this.f6629g.f6626d);
        for (OAuthListener oAuthListener : arrayList) {
            oAuthListener.onAuthFinish(oAuthErrCode, str);
        }
    }

    public final void onAuthGotQrcode(String str, byte[] bArr) {
        Log.d("MicroMsg.SDK.ListenerWrapper", "onAuthGotQrcode, qrcodeImgPath = " + str);
        ArrayList<OAuthListener> arrayList = new ArrayList<>();
        arrayList.addAll(this.f6629g.f6626d);
        for (OAuthListener oAuthListener : arrayList) {
            oAuthListener.onAuthGotQrcode(str, bArr);
        }
    }

    public final void onQrcodeScanned() {
        Log.d("MicroMsg.SDK.ListenerWrapper", "onQrcodeScanned");
        if (this.f6629g.handler != null) {
            this.f6629g.handler.post(new C3058c(this));
        }
    }
}
