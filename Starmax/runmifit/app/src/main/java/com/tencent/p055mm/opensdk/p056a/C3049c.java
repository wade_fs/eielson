package com.tencent.p055mm.opensdk.p056a;

import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

/* renamed from: com.tencent.mm.opensdk.a.c */
public final class C3049c {

    /* renamed from: com.tencent.mm.opensdk.a.c$a */
    public static final class C3050a {
        /* renamed from: a */
        public static Object m7541a(int i, String str) {
            switch (i) {
                case 1:
                    return Integer.valueOf(str);
                case 2:
                    return Long.valueOf(str);
                case 3:
                    return str;
                case 4:
                    return Boolean.valueOf(str);
                case 5:
                    return Float.valueOf(str);
                case 6:
                    try {
                        return Double.valueOf(str);
                    } catch (Exception e) {
                        Log.e("MicroMsg.SDK.PluginProvider.Resolver", "resolveObj exception:" + e.getMessage());
                        return null;
                    }
                default:
                    Log.e("MicroMsg.SDK.PluginProvider.Resolver", "unknown type");
                    return null;
            }
        }
    }

    /* renamed from: com.tencent.mm.opensdk.a.c$b */
    public static final class C3051b implements BaseColumns {
        public static final Uri CONTENT_URI = Uri.parse("content://com.tencent.mm.sdk.plugin.provider/sharedpref");
    }
}
