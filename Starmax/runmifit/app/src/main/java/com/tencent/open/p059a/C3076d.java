package com.tencent.open.p059a;

import android.os.Environment;
import android.os.StatFs;
import java.io.File;
import java.text.SimpleDateFormat;

/* renamed from: com.tencent.open.a.d */
/* compiled from: ProGuard */
public class C3076d {

    /* renamed from: com.tencent.open.a.d$a */
    /* compiled from: ProGuard */
    public static final class C3077a {
        /* renamed from: a */
        public static final boolean m7615a(int i, int i2) {
            return i2 == (i & i2);
        }
    }

    /* renamed from: com.tencent.open.a.d$d */
    /* compiled from: ProGuard */
    public static final class C3080d {
        /* renamed from: a */
        public static SimpleDateFormat m7625a(String str) {
            return new SimpleDateFormat(str);
        }
    }

    /* renamed from: com.tencent.open.a.d$b */
    /* compiled from: ProGuard */
    public static final class C3078b {
        /* renamed from: a */
        public static boolean m7616a() {
            String externalStorageState = Environment.getExternalStorageState();
            return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
        }

        /* renamed from: b */
        public static C3079c m7617b() {
            if (!m7616a()) {
                return null;
            }
            return C3079c.m7618b(Environment.getExternalStorageDirectory());
        }
    }

    /* renamed from: com.tencent.open.a.d$c */
    /* compiled from: ProGuard */
    public static class C3079c {

        /* renamed from: a */
        private File f6738a;

        /* renamed from: b */
        private long f6739b;

        /* renamed from: c */
        private long f6740c;

        /* renamed from: a */
        public File mo35097a() {
            return this.f6738a;
        }

        /* renamed from: a */
        public void mo35099a(File file) {
            this.f6738a = file;
        }

        /* renamed from: b */
        public long mo35100b() {
            return this.f6739b;
        }

        /* renamed from: a */
        public void mo35098a(long j) {
            this.f6739b = j;
        }

        /* renamed from: c */
        public long mo35102c() {
            return this.f6740c;
        }

        /* renamed from: b */
        public void mo35101b(long j) {
            this.f6740c = j;
        }

        /* renamed from: b */
        public static C3079c m7618b(File file) {
            C3079c cVar = new C3079c();
            cVar.mo35099a(file);
            StatFs statFs = new StatFs(file.getAbsolutePath());
            long blockSize = (long) statFs.getBlockSize();
            cVar.mo35098a(((long) statFs.getBlockCount()) * blockSize);
            cVar.mo35101b(((long) statFs.getAvailableBlocks()) * blockSize);
            return cVar;
        }

        public String toString() {
            return String.format("[%s : %d / %d]", mo35097a().getAbsolutePath(), Long.valueOf(mo35102c()), Long.valueOf(mo35100b()));
        }
    }
}
