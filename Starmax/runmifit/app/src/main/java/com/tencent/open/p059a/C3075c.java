package com.tencent.open.p059a;

import com.baidu.mobstat.Config;
import java.io.File;

/* renamed from: com.tencent.open.a.c */
/* compiled from: ProGuard */
public class C3075c {

    /* renamed from: a */
    public static int f6724a = 60;

    /* renamed from: b */
    public static int f6725b = 60;

    /* renamed from: c */
    public static String f6726c = "OpenSDK.Client.File.Tracer";

    /* renamed from: d */
    public static String f6727d = ("Tencent" + File.separator + "msflogs" + File.separator + "com" + File.separator + "tencent" + File.separator + "mobileqq" + File.separator);

    /* renamed from: e */
    public static String f6728e = ".log";

    /* renamed from: f */
    public static long f6729f = 8388608;

    /* renamed from: g */
    public static int f6730g = 262144;

    /* renamed from: h */
    public static int f6731h = 1024;

    /* renamed from: i */
    public static int f6732i = 10000;

    /* renamed from: j */
    public static String f6733j = "debug.file.blockcount";

    /* renamed from: k */
    public static String f6734k = "debug.file.keepperiod";

    /* renamed from: l */
    public static String f6735l = "debug.file.tracelevel";

    /* renamed from: m */
    public static int f6736m = 24;

    /* renamed from: n */
    public static long f6737n = Config.MAX_LOG_DATA_EXSIT_TIME;
}
