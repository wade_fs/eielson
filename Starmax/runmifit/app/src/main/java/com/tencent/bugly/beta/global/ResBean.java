package com.tencent.bugly.beta.global;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: BUGLY */
public class ResBean implements Parcelable, Parcelable.Creator<ResBean> {
    public static final Parcelable.Creator<ResBean> CREATOR = new ResBean();

    /* renamed from: a */
    public static ResBean f5325a;

    /* renamed from: b */
    public static final String[] f5326b = {"IMG_title", "VAL_style"};

    /* renamed from: c */
    public final String f5327c = "#273238";

    /* renamed from: d */
    public final String f5328d = "#757575";

    /* renamed from: e */
    private Map<String, String> f5329e = new ConcurrentHashMap();

    /* renamed from: a */
    public ResBean[] newArray(int i) {
        return new ResBean[0];
    }

    public int describeContents() {
        return 0;
    }

    public ResBean() {
    }

    public ResBean(Parcel parcel) {
        try {
            for (String str : f5326b) {
                this.f5329e.put(str, parcel.readString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public String mo34045a(String str) {
        return this.f5329e.get(str);
    }

    /* renamed from: a */
    public void mo34046a(String str, Object obj) {
        if (obj instanceof String) {
            this.f5329e.put(str, (String) obj);
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        for (String str : f5326b) {
            parcel.writeString(this.f5329e.get(str));
        }
    }

    /* renamed from: a */
    public ResBean createFromParcel(Parcel parcel) {
        return new ResBean(parcel);
    }
}
