package com.tencent.connect.avatar;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.tencent.connect.UserInfo;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class ImageActivity extends Activity {

    /* renamed from: a */
    RelativeLayout f6394a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public QQToken f6395b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public String f6396c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Handler f6397d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C2995c f6398e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public Button f6399f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public Button f6400g;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public C2994b f6401h;

    /* renamed from: i */
    private TextView f6402i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public ProgressBar f6403j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public int f6404k = 0;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public boolean f6405l = false;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public long f6406m = 0;

    /* renamed from: n */
    private int f6407n = 0;

    /* renamed from: o */
    private final int f6408o = 640;

    /* renamed from: p */
    private final int f6409p = 640;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public Rect f6410q = new Rect();

    /* renamed from: r */
    private String f6411r;

    /* renamed from: s */
    private Bitmap f6412s;

    /* renamed from: t */
    private final View.OnClickListener f6413t = new View.OnClickListener() {
        /* class com.tencent.connect.avatar.ImageActivity.C29852 */

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void
         arg types: [java.lang.String, int]
         candidates:
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, android.graphics.Rect):android.graphics.Rect
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, int):void
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void */
        public void onClick(View view) {
            ImageActivity.this.f6403j.setVisibility(View.VISIBLE);
            ImageActivity.this.f6400g.setEnabled(false);
            ImageActivity.this.f6400g.setTextColor(Color.rgb(21, 21, 21));
            ImageActivity.this.f6399f.setEnabled(false);
            ImageActivity.this.f6399f.setTextColor(Color.rgb(36, 94, (int) FMParserConstants.OPEN_MISPLACED_INTERPOLATION));
            new Thread(new Runnable() {
                /* class com.tencent.connect.avatar.ImageActivity.C29852.C29861 */

                public void run() {
                    ImageActivity.this.m7267c();
                }
            }).start();
            if (ImageActivity.this.f6405l) {
                ImageActivity.this.mo34764a("10657", 0L);
                return;
            }
            ImageActivity.this.mo34764a("10655", System.currentTimeMillis() - ImageActivity.this.f6406m);
            if (ImageActivity.this.f6398e.f6434b) {
                ImageActivity.this.mo34764a("10654", 0L);
            }
        }
    };

    /* renamed from: u */
    private final View.OnClickListener f6414u = new View.OnClickListener() {
        /* class com.tencent.connect.avatar.ImageActivity.C29873 */

        public void onClick(View view) {
            ImageActivity.this.mo34764a("10656", System.currentTimeMillis() - ImageActivity.this.f6406m);
            ImageActivity.this.setResult(0);
            ImageActivity.this.m7271d();
        }
    };

    /* renamed from: v */
    private final IUiListener f6415v = new IUiListener() {
        /* class com.tencent.connect.avatar.ImageActivity.C29895 */

        public void onCancel() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean
         arg types: [com.tencent.connect.avatar.ImageActivity, int]
         candidates:
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, android.graphics.Rect):android.graphics.Rect
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, int):void
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void
         arg types: [java.lang.String, int]
         candidates:
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, android.graphics.Rect):android.graphics.Rect
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, int):void
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void */
        public void onError(UiError uiError) {
            ImageActivity.this.f6400g.setEnabled(true);
            ImageActivity.this.f6400g.setTextColor(-1);
            ImageActivity.this.f6399f.setEnabled(true);
            ImageActivity.this.f6399f.setTextColor(-1);
            ImageActivity.this.f6399f.setText("重试");
            ImageActivity.this.f6403j.setVisibility(View.GONE);
            boolean unused = ImageActivity.this.f6405l = true;
            ImageActivity.this.m7258a(uiError.errorMessage, 1);
            ImageActivity.this.mo34764a("10660", 0L);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void
         arg types: [java.lang.String, int]
         candidates:
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, android.graphics.Rect):android.graphics.Rect
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, int):void
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void */
        public void onComplete(Object obj) {
            ImageActivity.this.f6400g.setEnabled(true);
            int i = -1;
            ImageActivity.this.f6400g.setTextColor(-1);
            ImageActivity.this.f6399f.setEnabled(true);
            ImageActivity.this.f6399f.setTextColor(-1);
            ImageActivity.this.f6403j.setVisibility(View.GONE);
            JSONObject jSONObject = (JSONObject) obj;
            try {
                i = jSONObject.getInt("ret");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (i == 0) {
                ImageActivity.this.m7258a("设置成功", 0);
                ImageActivity.this.mo34764a("10658", 0L);
                C3091d.m7665a().mo35122a(ImageActivity.this.f6395b.getOpenId(), ImageActivity.this.f6395b.getAppId(), Constants.VIA_SET_AVATAR_SUCCEED, Constants.VIA_REPORT_TYPE_SET_AVATAR, "3", "0");
                ImageActivity imageActivity = ImageActivity.this;
                if (imageActivity.f6396c != null && !"".equals(ImageActivity.this.f6396c)) {
                    Intent intent = new Intent();
                    intent.setClassName(imageActivity, ImageActivity.this.f6396c);
                    if (imageActivity.getPackageManager().resolveActivity(intent, 0) != null) {
                        imageActivity.startActivity(intent);
                    }
                }
                ImageActivity.this.m7254a(0, jSONObject.toString(), null, null);
                ImageActivity.this.m7271d();
                return;
            }
            ImageActivity.this.m7258a("设置出错了，请重新登录再尝试下呢：）", 1);
            C3091d.m7665a().mo35122a(ImageActivity.this.f6395b.getOpenId(), ImageActivity.this.f6395b.getAppId(), Constants.VIA_SET_AVATAR_SUCCEED, Constants.VIA_REPORT_TYPE_SET_AVATAR, Constants.VIA_ACT_TYPE_NINETEEN, "1");
        }
    };

    /* renamed from: w */
    private final IUiListener f6416w = new IUiListener() {
        /* class com.tencent.connect.avatar.ImageActivity.C29906 */

        public void onCancel() {
        }

        public void onError(UiError uiError) {
            m7285a(0);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void
         arg types: [java.lang.String, int]
         candidates:
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, android.graphics.Rect):android.graphics.Rect
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, int):void
          com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean
          com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void */
        public void onComplete(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            int i = -1;
            try {
                i = jSONObject.getInt("ret");
                if (i == 0) {
                    final String string = jSONObject.getString("nickname");
                    ImageActivity.this.f6397d.post(new Runnable() {
                        /* class com.tencent.connect.avatar.ImageActivity.C29906.C29911 */

                        public void run() {
                            ImageActivity.this.m7268c(string);
                        }
                    });
                    ImageActivity.this.mo34764a("10659", 0L);
                } else {
                    ImageActivity.this.mo34764a("10661", 0L);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (i != 0) {
                m7285a(i);
            }
        }

        /* renamed from: a */
        private void m7285a(int i) {
            if (ImageActivity.this.f6404k < 2) {
                ImageActivity.this.m7273e();
            }
        }
    };

    /* renamed from: a */
    private Bitmap m7249a(String str) throws IOException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        int i = 1;
        options.inJustDecodeBounds = true;
        Uri parse = Uri.parse(str);
        InputStream openInputStream = getContentResolver().openInputStream(parse);
        if (openInputStream == null) {
            return null;
        }
        try {
            BitmapFactory.decodeStream(openInputStream, null, options);
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
        }
        openInputStream.close();
        int i2 = options.outWidth;
        int i3 = options.outHeight;
        while (i2 * i3 > 4194304) {
            i2 /= 2;
            i3 /= 2;
            i *= 2;
        }
        options.inJustDecodeBounds = false;
        options.inSampleSize = i;
        try {
            return BitmapFactory.decodeStream(getContentResolver().openInputStream(parse), null, options);
        } catch (OutOfMemoryError e2) {
            e2.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public Drawable m7261b(String str) {
        Drawable drawable = null;
        try {
            InputStream open = getAssets().open(str);
            drawable = Drawable.createFromStream(open, str);
            open.close();
            return drawable;
        } catch (IOException e) {
            e.printStackTrace();
            return drawable;
        }
    }

    /* renamed from: a */
    private View m7252a() {
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-1, -1);
        ViewGroup.LayoutParams layoutParams2 = new ViewGroup.LayoutParams(-1, -1);
        ViewGroup.LayoutParams layoutParams3 = new ViewGroup.LayoutParams(-2, -2);
        this.f6394a = new RelativeLayout(this);
        this.f6394a.setLayoutParams(layoutParams);
        this.f6394a.setBackgroundColor(ViewCompat.MEASURED_STATE_MASK);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        relativeLayout.setLayoutParams(layoutParams3);
        this.f6394a.addView(relativeLayout);
        this.f6398e = new C2995c(this);
        this.f6398e.setLayoutParams(layoutParams2);
        this.f6398e.setScaleType(ImageView.ScaleType.MATRIX);
        relativeLayout.addView(this.f6398e);
        this.f6401h = new C2994b(this);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(layoutParams2);
        layoutParams4.addRule(14, -1);
        layoutParams4.addRule(15, -1);
        this.f6401h.setLayoutParams(layoutParams4);
        relativeLayout.addView(this.f6401h);
        LinearLayout linearLayout = new LinearLayout(this);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, C2993a.m7291a(this, 80.0f));
        layoutParams5.addRule(14, -1);
        linearLayout.setLayoutParams(layoutParams5);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        this.f6394a.addView(linearLayout);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(C2993a.m7291a(this, 24.0f), C2993a.m7291a(this, 24.0f)));
        imageView.setImageDrawable(m7261b("com.tencent.plus.logo.png"));
        linearLayout.addView(imageView);
        this.f6402i = new TextView(this);
        LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(layoutParams3);
        layoutParams6.leftMargin = C2993a.m7291a(this, 7.0f);
        this.f6402i.setLayoutParams(layoutParams6);
        this.f6402i.setEllipsize(TextUtils.TruncateAt.END);
        this.f6402i.setSingleLine();
        this.f6402i.setTextColor(-1);
        this.f6402i.setTextSize(24.0f);
        this.f6402i.setVisibility(View.GONE);
        linearLayout.addView(this.f6402i);
        RelativeLayout relativeLayout2 = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams7 = new RelativeLayout.LayoutParams(-1, C2993a.m7291a(this, 60.0f));
        layoutParams7.addRule(12, -1);
        layoutParams7.addRule(9, -1);
        relativeLayout2.setLayoutParams(layoutParams7);
        relativeLayout2.setBackgroundDrawable(m7261b("com.tencent.plus.bar.png"));
        int a = C2993a.m7291a(this, 10.0f);
        relativeLayout2.setPadding(a, a, a, 0);
        this.f6394a.addView(relativeLayout2);
        C2992a aVar = new C2992a(this);
        int a2 = C2993a.m7291a(this, 14.0f);
        int a3 = C2993a.m7291a(this, 7.0f);
        this.f6400g = new Button(this);
        this.f6400g.setLayoutParams(new RelativeLayout.LayoutParams(C2993a.m7291a(this, 78.0f), C2993a.m7291a(this, 45.0f)));
        this.f6400g.setText("取消");
        this.f6400g.setTextColor(-1);
        this.f6400g.setTextSize(18.0f);
        this.f6400g.setPadding(a2, a3, a2, a3);
        aVar.mo34776b(this.f6400g);
        relativeLayout2.addView(this.f6400g);
        this.f6399f = new Button(this);
        RelativeLayout.LayoutParams layoutParams8 = new RelativeLayout.LayoutParams(C2993a.m7291a(this, 78.0f), C2993a.m7291a(this, 45.0f));
        layoutParams8.addRule(11, -1);
        this.f6399f.setLayoutParams(layoutParams8);
        this.f6399f.setTextColor(-1);
        this.f6399f.setTextSize(18.0f);
        this.f6399f.setPadding(a2, a3, a2, a3);
        this.f6399f.setText("选取");
        aVar.mo34775a(this.f6399f);
        relativeLayout2.addView(this.f6399f);
        TextView textView = new TextView(this);
        RelativeLayout.LayoutParams layoutParams9 = new RelativeLayout.LayoutParams(layoutParams3);
        layoutParams9.addRule(13, -1);
        textView.setLayoutParams(layoutParams9);
        textView.setText("移动和缩放");
        textView.setPadding(0, C2993a.m7291a(this, 3.0f), 0, 0);
        textView.setTextSize(18.0f);
        textView.setTextColor(-1);
        relativeLayout2.addView(textView);
        this.f6403j = new ProgressBar(this);
        RelativeLayout.LayoutParams layoutParams10 = new RelativeLayout.LayoutParams(layoutParams3);
        layoutParams10.addRule(14, -1);
        layoutParams10.addRule(15, -1);
        this.f6403j.setLayoutParams(layoutParams10);
        this.f6403j.setVisibility(View.GONE);
        this.f6394a.addView(this.f6403j);
        return this.f6394a;
    }

    /* renamed from: com.tencent.connect.avatar.ImageActivity$a */
    /* compiled from: ProGuard */
    class C2992a extends View {
        public C2992a(Context context) {
            super(context);
        }

        /* renamed from: a */
        public void mo34775a(Button button) {
            StateListDrawable stateListDrawable = new StateListDrawable();
            Drawable a = ImageActivity.this.m7261b("com.tencent.plus.blue_normal.png");
            Drawable a2 = ImageActivity.this.m7261b("com.tencent.plus.blue_down.png");
            Drawable a3 = ImageActivity.this.m7261b("com.tencent.plus.blue_disable.png");
            stateListDrawable.addState(View.PRESSED_ENABLED_STATE_SET, a2);
            stateListDrawable.addState(View.ENABLED_FOCUSED_STATE_SET, a);
            stateListDrawable.addState(View.ENABLED_STATE_SET, a);
            stateListDrawable.addState(View.FOCUSED_STATE_SET, a);
            stateListDrawable.addState(View.EMPTY_STATE_SET, a3);
            button.setBackgroundDrawable(stateListDrawable);
        }

        /* renamed from: b */
        public void mo34776b(Button button) {
            StateListDrawable stateListDrawable = new StateListDrawable();
            Drawable a = ImageActivity.this.m7261b("com.tencent.plus.gray_normal.png");
            Drawable a2 = ImageActivity.this.m7261b("com.tencent.plus.gray_down.png");
            Drawable a3 = ImageActivity.this.m7261b("com.tencent.plus.gray_disable.png");
            stateListDrawable.addState(View.PRESSED_ENABLED_STATE_SET, a2);
            stateListDrawable.addState(View.ENABLED_FOCUSED_STATE_SET, a);
            stateListDrawable.addState(View.ENABLED_STATE_SET, a);
            stateListDrawable.addState(View.FOCUSED_STATE_SET, a);
            stateListDrawable.addState(View.EMPTY_STATE_SET, a3);
            button.setBackgroundDrawable(stateListDrawable);
        }
    }

    /* renamed from: b */
    private void m7262b() {
        try {
            this.f6412s = m7249a(this.f6411r);
            if (this.f6412s != null) {
                this.f6398e.setImageBitmap(this.f6412s);
                this.f6399f.setOnClickListener(this.f6413t);
                this.f6400g.setOnClickListener(this.f6414u);
                this.f6394a.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    /* class com.tencent.connect.avatar.ImageActivity.C29841 */

                    public void onGlobalLayout() {
                        ImageActivity.this.f6394a.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        ImageActivity imageActivity = ImageActivity.this;
                        Rect unused = imageActivity.f6410q = imageActivity.f6401h.mo34780a();
                        ImageActivity.this.f6398e.mo34782a(ImageActivity.this.f6410q);
                    }
                });
                return;
            }
            throw new IOException("cannot read picture: '" + this.f6411r + "'!");
        } catch (IOException e) {
            e.printStackTrace();
            m7258a(Constants.MSG_IMAGE_ERROR, 1);
            m7254a(-5, null, Constants.MSG_IMAGE_ERROR, e.getMessage());
            m7271d();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, android.graphics.Rect):android.graphics.Rect
      com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, java.lang.String):android.graphics.drawable.Drawable
      com.tencent.connect.avatar.ImageActivity.a(java.lang.String, int):void
      com.tencent.connect.avatar.ImageActivity.a(com.tencent.connect.avatar.ImageActivity, boolean):boolean
      com.tencent.connect.avatar.ImageActivity.a(java.lang.String, long):void */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        setRequestedOrientation(1);
        setContentView(m7252a());
        this.f6397d = new Handler();
        Bundle bundleExtra = getIntent().getBundleExtra(Constants.KEY_PARAMS);
        this.f6411r = bundleExtra.getString("picture");
        this.f6396c = bundleExtra.getString("return_activity");
        String string = bundleExtra.getString("appid");
        String string2 = bundleExtra.getString(Constants.PARAM_ACCESS_TOKEN);
        long j = bundleExtra.getLong(Constants.PARAM_EXPIRES_IN);
        String string3 = bundleExtra.getString("openid");
        this.f6407n = bundleExtra.getInt("exitAnim");
        this.f6395b = new QQToken(string);
        QQToken qQToken = this.f6395b;
        qQToken.setAccessToken(string2, ((j - System.currentTimeMillis()) / 1000) + "");
        this.f6395b.setOpenId(string3);
        m7262b();
        m7273e();
        this.f6406m = System.currentTimeMillis();
        mo34764a("10653", 0L);
    }

    public void onBackPressed() {
        setResult(0);
        m7271d();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.f6398e.setImageBitmap(null);
        Bitmap bitmap = this.f6412s;
        if (bitmap != null && !bitmap.isRecycled()) {
            this.f6412s.recycle();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap}
     arg types: [android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, int]
     candidates:
      ClspMth{android.graphics.Bitmap.createBitmap(android.util.DisplayMetrics, int[], int, int, int, int, android.graphics.Bitmap$Config):android.graphics.Bitmap}
      ClspMth{android.graphics.Bitmap.createBitmap(android.graphics.Bitmap, int, int, int, int, android.graphics.Matrix, boolean):android.graphics.Bitmap} */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m7267c() {
        Matrix imageMatrix = this.f6398e.getImageMatrix();
        float[] fArr = new float[9];
        imageMatrix.getValues(fArr);
        float f = fArr[2];
        float f2 = fArr[5];
        float f3 = fArr[0];
        float width = 640.0f / ((float) this.f6410q.width());
        int i = (int) ((((float) this.f6410q.left) - f) / f3);
        int i2 = i < 0 ? 0 : i;
        int i3 = (int) ((((float) this.f6410q.top) - f2) / f3);
        int i4 = i3 < 0 ? 0 : i3;
        Matrix matrix = new Matrix();
        matrix.set(imageMatrix);
        matrix.postScale(width, width);
        int i5 = (int) (650.0f / f3);
        try {
            Bitmap createBitmap = Bitmap.createBitmap(this.f6412s, i2, i4, Math.min(this.f6412s.getWidth() - i2, i5), Math.min(this.f6412s.getHeight() - i4, i5), matrix, true);
            Bitmap createBitmap2 = Bitmap.createBitmap(createBitmap, 0, 0, 640, 640);
            createBitmap.recycle();
            m7255a(createBitmap2);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            m7258a(Constants.MSG_IMAGE_ERROR, 1);
            m7254a(-5, null, Constants.MSG_IMAGE_ERROR, e.getMessage());
            m7271d();
        }
    }

    /* renamed from: a */
    private void m7255a(Bitmap bitmap) {
        new QQAvatarImp(this.f6395b).setAvator(bitmap, this.f6415v);
    }

    /* compiled from: ProGuard */
    private class QQAvatarImp extends BaseApi {
        public QQAvatarImp(QQToken qQToken) {
            super(qQToken);
        }

        public void setAvator(Bitmap bitmap, IUiListener iUiListener) {
            Bundle a = mo34798a();
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 40, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream.toByteArray();
            bitmap.recycle();
            BaseApi.TempRequestListener tempRequestListener = new BaseApi.TempRequestListener(iUiListener);
            a.putByteArray("picture", byteArray);
            HttpUtils.requestAsync(this.f6457c, C3121e.m7727a(), "user/set_user_face", a, "POST", tempRequestListener);
            C3091d.m7665a().mo35122a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_SET_AVATAR_SUCCEED, Constants.VIA_REPORT_TYPE_SET_AVATAR, Constants.VIA_ACT_TYPE_NINETEEN, "0");
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m7258a(final String str, final int i) {
        this.f6397d.post(new Runnable() {
            /* class com.tencent.connect.avatar.ImageActivity.C29884 */

            public void run() {
                ImageActivity.this.m7265b(str, i);
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m7265b(String str, int i) {
        Toast makeText = Toast.makeText(this, str, 1);
        LinearLayout linearLayout = (LinearLayout) makeText.getView();
        ((TextView) linearLayout.getChildAt(0)).setPadding(8, 0, 0, 0);
        ImageView imageView = new ImageView(this);
        imageView.setLayoutParams(new LinearLayout.LayoutParams(C2993a.m7291a(this, 16.0f), C2993a.m7291a(this, 16.0f)));
        if (i == 0) {
            imageView.setImageDrawable(m7261b("com.tencent.plus.ic_success.png"));
        } else {
            imageView.setImageDrawable(m7261b("com.tencent.plus.ic_error.png"));
        }
        linearLayout.addView(imageView, 0);
        linearLayout.setOrientation(0);
        linearLayout.setGravity(17);
        makeText.setView(linearLayout);
        makeText.setGravity(17, 0, 0);
        makeText.show();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m7254a(int i, String str, String str2, String str3) {
        Intent intent = new Intent();
        intent.putExtra(Constants.KEY_ERROR_CODE, i);
        intent.putExtra(Constants.KEY_ERROR_MSG, str2);
        intent.putExtra(Constants.KEY_ERROR_DETAIL, str3);
        intent.putExtra(Constants.KEY_RESPONSE, str);
        setResult(-1, intent);
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public void m7271d() {
        finish();
        int i = this.f6407n;
        if (i != 0) {
            overridePendingTransition(0, i);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m7273e() {
        this.f6404k++;
        new UserInfo(this, this.f6395b).getUserInfo(this.f6416w);
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m7268c(String str) {
        String d = m7270d(str);
        if (!"".equals(d)) {
            this.f6402i.setText(d);
            this.f6402i.setVisibility(View.VISIBLE);
        }
    }

    /* renamed from: d */
    private String m7270d(String str) {
        return str.replaceAll("&gt;", ">").replaceAll("&lt;", "<").replaceAll("&quot;", "\"").replaceAll("&#39;", "'").replaceAll("&amp;", "&");
    }

    /* renamed from: a */
    public void mo34764a(String str, long j) {
        C3131k.m7775a(this, str, j, this.f6395b.getAppId());
    }
}
