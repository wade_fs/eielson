package com.tencent.mid.util;

import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import com.baidu.mobstat.Config;
import java.util.Locale;
import java.util.TimeZone;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.tencent.mid.util.c */
public class C3035c {

    /* renamed from: a */
    static C3037a f6581a;

    /* renamed from: d */
    private static C3038d f6582d = Util.getLogger();

    /* renamed from: e */
    private static JSONObject f6583e = null;

    /* renamed from: b */
    Integer f6584b = null;

    /* renamed from: c */
    String f6585c = null;

    /* renamed from: com.tencent.mid.util.c$a */
    static class C3037a {

        /* renamed from: a */
        String f6586a;

        /* renamed from: b */
        String f6587b;

        /* renamed from: c */
        DisplayMetrics f6588c;

        /* renamed from: d */
        int f6589d;

        /* renamed from: e */
        String f6590e;

        /* renamed from: f */
        String f6591f;

        /* renamed from: g */
        String f6592g;

        /* renamed from: h */
        String f6593h;

        /* renamed from: i */
        String f6594i;

        /* renamed from: j */
        String f6595j;

        /* renamed from: k */
        String f6596k;

        /* renamed from: l */
        int f6597l;

        /* renamed from: m */
        String f6598m;

        /* renamed from: n */
        Context f6599n;

        /* renamed from: o */
        private String f6600o;

        /* renamed from: p */
        private String f6601p;

        /* renamed from: q */
        private String f6602q;

        /* renamed from: r */
        private String f6603r;

        /* renamed from: s */
        private String f6604s;

        private C3037a(Context context) {
            this.f6587b = String.valueOf(3.73f);
            this.f6589d = Build.VERSION.SDK_INT;
            this.f6590e = Build.MODEL;
            this.f6591f = Build.MANUFACTURER;
            this.f6592g = Locale.getDefault().getLanguage();
            this.f6597l = 0;
            this.f6598m = null;
            this.f6599n = null;
            this.f6600o = null;
            this.f6601p = null;
            this.f6602q = null;
            this.f6603r = null;
            this.f6604s = null;
            this.f6599n = context;
            this.f6588c = C3042h.m7518c(context);
            this.f6586a = C3042h.m7522e(context);
            this.f6594i = C3042h.m7521d(context);
            this.f6595j = TimeZone.getDefault().getID();
            this.f6597l = C3042h.m7526i(context);
            this.f6596k = C3042h.m7527j(context);
            this.f6598m = context.getPackageName();
            if (this.f6589d >= 14) {
                this.f6600o = C3042h.m7531n(context);
            }
            this.f6601p = C3042h.m7530m(context).toString();
            this.f6602q = C3042h.m7528k(context);
            this.f6603r = C3042h.m7514a();
            this.f6604s = C3042h.m7515a(context);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo34944a(JSONObject jSONObject) {
            jSONObject.put("sr", this.f6588c.widthPixels + "*" + this.f6588c.heightPixels);
            Util.jsonPut(jSONObject, "av", this.f6586a);
            Util.jsonPut(jSONObject, "ch", this.f6593h);
            Util.jsonPut(jSONObject, "mf", this.f6591f);
            Util.jsonPut(jSONObject, "sv", this.f6587b);
            Util.jsonPut(jSONObject, "ov", Integer.toString(this.f6589d));
            jSONObject.put("os", 1);
            Util.jsonPut(jSONObject, Config.OPERATOR, this.f6594i);
            Util.jsonPut(jSONObject, "lg", this.f6592g);
            Util.jsonPut(jSONObject, "md", this.f6590e);
            Util.jsonPut(jSONObject, "tz", this.f6595j);
            int i = this.f6597l;
            if (i != 0) {
                jSONObject.put("jb", i);
            }
            Util.jsonPut(jSONObject, Config.FEED_LIST_MAPPING, this.f6596k);
            Util.jsonPut(jSONObject, "apn", this.f6598m);
            if (Util.isNetworkAvailable(this.f6599n) && Util.isWifiNet(this.f6599n)) {
                JSONObject jSONObject2 = new JSONObject();
                Util.jsonPut(jSONObject2, "bs", Util.getWiFiBBSID(this.f6599n));
                Util.jsonPut(jSONObject2, "ss", Util.getWiFiSSID(this.f6599n));
                if (jSONObject2.length() > 0) {
                    Util.jsonPut(jSONObject, "wf", jSONObject2.toString());
                }
            }
            JSONArray wifiTopN = Util.getWifiTopN(this.f6599n, 10);
            if (wifiTopN != null && wifiTopN.length() > 0) {
                Util.jsonPut(jSONObject, "wflist", wifiTopN.toString());
            }
            JSONArray sensors = Util.getSensors(this.f6599n);
            if (sensors != null && sensors.length() > 0) {
                Util.jsonPut(jSONObject, "sslist", sensors.toString());
            }
            Util.jsonPut(jSONObject, "sen", this.f6600o);
            Util.jsonPut(jSONObject, "cpu", this.f6601p);
            Util.jsonPut(jSONObject, "ram", this.f6602q);
            Util.jsonPut(jSONObject, Config.ROM, this.f6603r);
            Util.jsonPut(jSONObject, "ciip", this.f6604s);
        }
    }

    /* renamed from: a */
    static synchronized C3037a m7485a(Context context) {
        C3037a aVar;
        synchronized (C3035c.class) {
            if (f6581a == null) {
                f6581a = new C3037a(context.getApplicationContext());
            }
            aVar = f6581a;
        }
        return aVar;
    }

    public C3035c(Context context) {
        try {
            m7485a(context);
            this.f6584b = C3042h.m7525h(context.getApplicationContext());
            this.f6585c = C3042h.m7524g(context);
        } catch (Throwable th) {
            f6582d.mo34954f(th);
        }
    }

    /* renamed from: a */
    public void mo34943a(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            if (f6581a != null) {
                f6581a.mo34944a(jSONObject2);
            }
            Util.jsonPut(jSONObject2, "cn", this.f6585c);
            if (this.f6584b != null) {
                jSONObject2.put("tn", this.f6584b);
            }
            jSONObject.put(Config.EVENT_PART, jSONObject2);
            if (f6583e != null && f6583e.length() > 0) {
                jSONObject.put("eva", f6583e);
            }
        } catch (Throwable th) {
            f6582d.mo34954f(th);
        }
    }
}
