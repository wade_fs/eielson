package com.tencent.bugly.proguard;

import android.os.Handler;
import android.os.Looper;
import com.tencent.bugly.beta.download.C2801a;
import com.tencent.bugly.beta.download.DownloadListener;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.beta.upgrade.BetaGrayStrategy;

/* renamed from: com.tencent.bugly.proguard.q */
/* compiled from: BUGLY */
public class C2948q {

    /* renamed from: a */
    public static C2948q f6230a = new C2948q();

    /* renamed from: b */
    public BetaGrayStrategy f6231b;

    /* renamed from: c */
    public DownloadTask f6232c;

    /* renamed from: d */
    public final Handler f6233d = new Handler(Looper.getMainLooper());

    /* renamed from: e */
    private DownloadListener f6234e = new C2801a(4, this, 0);

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0085, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x008c, code lost:
        r3 = r2.f6231b.f5455a.mo34696b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00b2, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00b4, code lost:
        return;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34674a(int r3, com.tencent.bugly.proguard.C2959y r4, boolean r5) {
        /*
            r2 = this;
            monitor-enter(r2)
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r4 = r2.mo34673a(r4)     // Catch:{ all -> 0x00b5 }
            r2.f6231b = r4     // Catch:{ all -> 0x00b5 }
            if (r3 != 0) goto L_0x00b3
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r3 = r2.f6231b     // Catch:{ all -> 0x00b5 }
            if (r3 != 0) goto L_0x000f
            goto L_0x00b3
        L_0x000f:
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r3 = r2.f6231b     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x00b1
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r3 = r2.f6231b     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.y r3 = r3.f5455a     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x00b1
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r3 = r2.f6231b     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.y r3 = r3.f5455a     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.u r3 = r3.f6287f     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x00b1
            com.tencent.bugly.beta.global.e r3 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            java.io.File r3 = r3.f5343G     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x0086
            boolean r4 = r3.exists()     // Catch:{ all -> 0x00b5 }
            if (r4 == 0) goto L_0x0086
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r4 = r2.f6231b     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.y r4 = r4.f5455a     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.u r4 = r4.f6287f     // Catch:{ all -> 0x00b5 }
            java.lang.String r4 = r4.f6246a     // Catch:{ all -> 0x00b5 }
            java.lang.String r0 = "SHA"
            boolean r4 = com.tencent.bugly.beta.global.C2804a.m6268a(r3, r4, r0)     // Catch:{ all -> 0x00b5 }
            if (r4 == 0) goto L_0x0086
            java.lang.String r4 = "patch has downloaded!"
            r5 = 0
            java.lang.Object[] r0 = new java.lang.Object[r5]     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.C2903an.m6857a(r4, r0)     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            boolean r4 = r4.f5350N     // Catch:{ all -> 0x00b5 }
            if (r4 != 0) goto L_0x0084
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            int r4 = r4.f5351O     // Catch:{ all -> 0x00b5 }
            r0 = 3
            if (r4 > r0) goto L_0x0084
            java.lang.String r4 = "patch has downloaded but not patched execute patch!"
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.C2903an.m6857a(r4, r5)     // Catch:{ all -> 0x00b5 }
            java.lang.String r4 = "PATCH_MAX_TIMES"
            com.tencent.bugly.beta.global.e r5 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            int r0 = r5.f5351O     // Catch:{ all -> 0x00b5 }
            int r1 = r0 + 1
            r5.f5351O = r1     // Catch:{ all -> 0x00b5 }
            java.lang.String r5 = java.lang.String.valueOf(r0)     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.global.C2804a.m6264a(r4, r5)     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            java.io.File r4 = r4.f5344H     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.global.C2804a.m6267a(r3, r4)     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.tinker.TinkerManager r3 = com.tencent.bugly.beta.tinker.TinkerManager.getInstance()     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            java.io.File r4 = r4.f5344H     // Catch:{ all -> 0x00b5 }
            java.lang.String r4 = r4.getAbsolutePath()     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.global.e r5 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            boolean r5 = r5.f5358V     // Catch:{ all -> 0x00b5 }
            r3.onDownloadSuccess(r4, r5)     // Catch:{ all -> 0x00b5 }
        L_0x0084:
            monitor-exit(r2)
            return
        L_0x0086:
            com.tencent.bugly.beta.global.e r3 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.beta.interfaces.BetaPatchListener r3 = r3.f5359W     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x00a0
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r3 = r2.f6231b     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.y r3 = r3.f5455a     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.u r3 = r3.mo34696b()     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x00a0
            android.os.Handler r4 = r2.f6233d     // Catch:{ all -> 0x00b5 }
            com.tencent.bugly.proguard.q$1 r0 = new com.tencent.bugly.proguard.q$1     // Catch:{ all -> 0x00b5 }
            r0.<init>(r3)     // Catch:{ all -> 0x00b5 }
            r4.post(r0)     // Catch:{ all -> 0x00b5 }
        L_0x00a0:
            com.tencent.bugly.beta.global.e r3 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            boolean r3 = r3.f5361Y     // Catch:{ all -> 0x00b5 }
            if (r3 != 0) goto L_0x00a8
            if (r5 == 0) goto L_0x00b1
        L_0x00a8:
            com.tencent.bugly.beta.global.e r3 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x00b5 }
            boolean r3 = r3.f5362Z     // Catch:{ all -> 0x00b5 }
            if (r3 == 0) goto L_0x00b1
            r2.m7136a()     // Catch:{ all -> 0x00b5 }
        L_0x00b1:
            monitor-exit(r2)
            return
        L_0x00b3:
            monitor-exit(r2)
            return
        L_0x00b5:
            r3 = move-exception
            monitor-exit(r2)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2948q.mo34674a(int, com.tencent.bugly.proguard.y, boolean):void");
    }

    /* JADX WARN: Type inference failed for: r1v31 */
    /* JADX WARN: Type inference failed for: r1v33 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.upgrade.BetaGrayStrategy]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=?, for r1v29, types: [int, boolean] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.tencent.bugly.beta.upgrade.BetaGrayStrategy mo34673a(com.tencent.bugly.proguard.C2959y r23) {
        /*
            r22 = this;
            r0 = r23
            android.os.Parcelable$Creator<com.tencent.bugly.beta.upgrade.BetaGrayStrategy> r1 = com.tencent.bugly.beta.upgrade.BetaGrayStrategy.CREATOR
            java.lang.String r2 = "st.bch"
            android.os.Parcelable r1 = com.tencent.bugly.beta.global.C2804a.m6261a(r2, r1)
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r1 = (com.tencent.bugly.beta.upgrade.BetaGrayStrategy) r1
            r3 = 0
            if (r1 == 0) goto L_0x0013
            com.tencent.bugly.proguard.y r4 = r1.f5455a
            if (r4 != 0) goto L_0x0017
        L_0x0013:
            com.tencent.bugly.beta.global.C2804a.m6269a(r2)
            r1 = r3
        L_0x0017:
            java.lang.String r4 = "delete patchFile"
            java.lang.String r5 = "delete tmpPatchFile"
            r7 = 1
            if (r0 == 0) goto L_0x00e7
            int r8 = r0.f6295n
            if (r8 == r7) goto L_0x00e2
            if (r1 == 0) goto L_0x00e2
            com.tencent.bugly.proguard.y r8 = r1.f5455a
            java.lang.String r8 = r8.f6294m
            boolean r8 = android.text.TextUtils.isEmpty(r8)
            if (r8 != 0) goto L_0x00e2
            java.lang.String r8 = r0.f6294m
            com.tencent.bugly.proguard.y r9 = r1.f5455a
            java.lang.String r9 = r9.f6294m
            boolean r8 = android.text.TextUtils.equals(r8, r9)
            if (r8 == 0) goto L_0x00e2
            com.tencent.bugly.proguard.p r8 = com.tencent.bugly.proguard.C2947p.f6229a
            com.tencent.bugly.proguard.w r14 = new com.tencent.bugly.proguard.w
            long r11 = java.lang.System.currentTimeMillis()
            r13 = 0
            r15 = 0
            r17 = 0
            com.tencent.bugly.proguard.y r9 = r1.f5455a
            java.lang.String r10 = r9.f6294m
            com.tencent.bugly.proguard.y r9 = r1.f5455a
            int r9 = r9.f6297p
            r19 = 0
            java.lang.String r18 = "recall"
            r20 = r9
            r9 = r14
            r21 = r10
            r10 = r18
            r6 = r14
            r14 = r15
            r16 = r17
            r17 = r21
            r18 = r20
            r9.<init>(r10, r11, r13, r14, r16, r17, r18, r19)
            r8.mo34667a(r6)
            com.tencent.bugly.beta.global.C2804a.m6269a(r2)
            com.tencent.bugly.beta.global.e r6 = com.tencent.bugly.beta.global.C2808e.f5336E
            com.tencent.bugly.beta.download.b r6 = r6.f5382p
            com.tencent.bugly.proguard.y r8 = r1.f5455a
            com.tencent.bugly.proguard.u r8 = r8.f6287f
            java.lang.String r8 = r8.f6247b
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.io.File r9 = r9.f5345I
            java.lang.String r9 = r9.getAbsolutePath()
            com.tencent.bugly.proguard.y r1 = r1.f5455a
            com.tencent.bugly.proguard.u r1 = r1.f6287f
            java.lang.String r1 = r1.f6246a
            com.tencent.bugly.beta.download.DownloadTask r1 = r6.mo34042a(r8, r9, r3, r1)
            r1.delete(r7)
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.io.File r1 = r1.f5344H
            if (r1 == 0) goto L_0x00a2
            boolean r6 = r1.exists()
            if (r6 == 0) goto L_0x00a2
            boolean r1 = r1.delete()
            if (r1 == 0) goto L_0x00a2
            r1 = 0
            java.lang.Object[] r6 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C2903an.m6857a(r5, r6)
        L_0x00a2:
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.io.File r1 = r1.f5343G
            if (r1 == 0) goto L_0x00bb
            boolean r6 = r1.exists()
            if (r6 == 0) goto L_0x00bb
            boolean r1 = r1.delete()
            if (r1 == 0) goto L_0x00bb
            r1 = 0
            java.lang.Object[] r6 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C2903an.m6857a(r4, r6)
            goto L_0x00bc
        L_0x00bb:
            r1 = 0
        L_0x00bc:
            java.lang.String r6 = "IS_PATCH_ROLL_BACK"
            com.tencent.bugly.beta.global.C2804a.m6265a(r6, r7)
            java.lang.Object[] r6 = new java.lang.Object[r1]
            java.lang.String r8 = "patch rollback"
            com.tencent.bugly.proguard.C2903an.m6857a(r8, r6)
            com.tencent.bugly.beta.global.e r6 = com.tencent.bugly.beta.global.C2808e.f5336E
            android.content.Context r6 = r6.f5385s
            boolean r6 = com.tencent.bugly.proguard.C2908aq.m6932b(r6)
            if (r6 != 0) goto L_0x00da
            com.tencent.bugly.beta.tinker.TinkerManager r6 = com.tencent.bugly.beta.tinker.TinkerManager.getInstance()
            r6.onPatchRollback(r7)
            goto L_0x00e1
        L_0x00da:
            com.tencent.bugly.beta.tinker.TinkerManager r6 = com.tencent.bugly.beta.tinker.TinkerManager.getInstance()
            r6.onPatchRollback(r1)
        L_0x00e1:
            r1 = r3
        L_0x00e2:
            int r6 = r0.f6295n
            if (r6 == r7) goto L_0x00e7
            r0 = r3
        L_0x00e7:
            r6 = 3
            if (r0 == 0) goto L_0x019b
            com.tencent.bugly.beta.upgrade.BetaGrayStrategy r8 = new com.tencent.bugly.beta.upgrade.BetaGrayStrategy
            r8.<init>()
            r8.f5455a = r0
            long r9 = java.lang.System.currentTimeMillis()
            r8.f5459e = r9
            if (r1 == 0) goto L_0x0183
            com.tencent.bugly.proguard.y r9 = r1.f5455a
            com.tencent.bugly.proguard.u r9 = r9.f6287f
            java.lang.String r9 = r9.f6247b
            com.tencent.bugly.proguard.u r10 = r0.f6287f
            java.lang.String r10 = r10.f6247b
            boolean r9 = android.text.TextUtils.equals(r9, r10)
            if (r9 == 0) goto L_0x011f
            java.util.Map<java.lang.String, java.lang.String> r9 = r0.f6293l
            if (r9 == 0) goto L_0x0183
            java.util.Map<java.lang.String, java.lang.String> r9 = r0.f6293l
            java.lang.String r10 = "H1"
            java.lang.Object r9 = r9.get(r10)
            java.lang.CharSequence r9 = (java.lang.CharSequence) r9
            java.lang.String r10 = "false"
            boolean r9 = android.text.TextUtils.equals(r9, r10)
            if (r9 == 0) goto L_0x0183
        L_0x011f:
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.util.Map<java.lang.String, java.lang.String> r10 = r0.f6293l
            java.lang.String r11 = "H2"
            java.lang.Object r10 = r10.get(r11)
            java.lang.String r10 = (java.lang.String) r10
            r9.f5349M = r10
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E
            com.tencent.bugly.beta.download.b r9 = r9.f5382p
            com.tencent.bugly.proguard.y r10 = r1.f5455a
            com.tencent.bugly.proguard.u r10 = r10.f6287f
            java.lang.String r10 = r10.f6247b
            com.tencent.bugly.beta.global.e r11 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.io.File r11 = r11.f5345I
            java.lang.String r11 = r11.getAbsolutePath()
            com.tencent.bugly.beta.download.DownloadTask r3 = r9.mo34042a(r10, r11, r3, r3)
            r3.delete(r7)
            com.tencent.bugly.proguard.y r1 = r1.f5455a
            int r1 = r1.f6297p
            if (r1 != r6) goto L_0x0183
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.io.File r1 = r1.f5344H
            if (r1 == 0) goto L_0x0164
            boolean r3 = r1.exists()
            if (r3 == 0) goto L_0x0164
            boolean r1 = r1.delete()
            if (r1 == 0) goto L_0x0164
            r1 = 0
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C2903an.m6857a(r5, r3)
        L_0x0164:
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.io.File r1 = r1.f5343G
            if (r1 == 0) goto L_0x0183
            boolean r3 = r1.exists()
            if (r3 == 0) goto L_0x0183
            boolean r1 = r1.delete()
            if (r1 == 0) goto L_0x0183
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E
            java.lang.String r3 = ""
            r1.f5348L = r3
            r1 = 0
            java.lang.Object[] r3 = new java.lang.Object[r1]
            com.tencent.bugly.proguard.C2903an.m6857a(r4, r3)
            goto L_0x0184
        L_0x0183:
            r1 = 0
        L_0x0184:
            com.tencent.bugly.beta.global.C2804a.m6270a(r2, r8)
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r1] = r0
            int r0 = r0.f6297p
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r7] = r0
            java.lang.String r0 = "onUpgradeReceived: %s [type: %d]"
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r2)
            r1 = r8
            goto L_0x01a9
        L_0x019b:
            if (r1 == 0) goto L_0x01a8
            com.tencent.bugly.proguard.y r0 = r1.f5455a
            if (r0 == 0) goto L_0x01a8
            com.tencent.bugly.proguard.y r0 = r1.f5455a
            int r0 = r0.f6297p
            if (r0 != r6) goto L_0x01a8
            goto L_0x01a9
        L_0x01a8:
            r1 = r3
        L_0x01a9:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2948q.mo34673a(com.tencent.bugly.proguard.y):com.tencent.bugly.beta.upgrade.BetaGrayStrategy");
    }

    /* renamed from: a */
    private void m7136a() {
        if (this.f6231b != null) {
            if (this.f6232c == null) {
                this.f6232c = C2808e.f5336E.f5382p.mo34042a(this.f6231b.f5455a.f6287f.f6247b, C2808e.f5336E.f5345I.getAbsolutePath(), null, this.f6231b.f5455a.f6287f.f6246a);
            }
            DownloadTask downloadTask = this.f6232c;
            if (downloadTask != null) {
                downloadTask.addListener(this.f6234e);
                this.f6232c.setNeededNotify(false);
                this.f6232c.download();
            }
        }
    }
}
