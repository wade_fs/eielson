package com.tencent.open.p059a;

import android.text.format.Time;
import android.util.Log;
import freemarker.template.Template;

/* renamed from: com.tencent.open.a.h */
/* compiled from: ProGuard */
public final class C3084h {

    /* renamed from: a */
    public static final C3084h f6748a = new C3084h();

    /* renamed from: a */
    public final String mo35111a(int i) {
        return i != 1 ? i != 2 ? i != 4 ? i != 8 ? i != 16 ? i != 32 ? "-" : "A" : "E" : "W" : "I" : Template.DEFAULT_NAMESPACE_PREFIX : "V";
    }

    /* renamed from: a */
    public String mo35112a(int i, Thread thread, long j, String str, String str2, Throwable th) {
        long j2 = j % 1000;
        Time time = new Time();
        time.set(j);
        StringBuilder sb = new StringBuilder();
        sb.append(mo35111a(i));
        sb.append('/');
        sb.append(time.format("%Y-%m-%d %H:%M:%S"));
        sb.append('.');
        if (j2 < 10) {
            sb.append("00");
        } else if (j2 < 100) {
            sb.append('0');
        }
        sb.append(j2);
        sb.append(' ');
        sb.append('[');
        if (thread == null) {
            sb.append("N/A");
        } else {
            sb.append(thread.getName());
        }
        sb.append(']');
        sb.append('[');
        sb.append(str);
        sb.append(']');
        sb.append(' ');
        sb.append(str2);
        sb.append(10);
        if (th != null) {
            sb.append("* Exception : \n");
            sb.append(Log.getStackTraceString(th));
            sb.append(10);
        }
        return sb.toString();
    }
}
