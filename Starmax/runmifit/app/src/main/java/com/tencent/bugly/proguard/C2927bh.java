package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.bh */
/* compiled from: BUGLY */
public final class C2927bh extends C2944m {

    /* renamed from: y */
    static byte[] f6113y = new byte[1];

    /* renamed from: z */
    static Map<String, String> f6114z = new HashMap();

    /* renamed from: a */
    public int f6115a = 0;

    /* renamed from: b */
    public String f6116b = "";

    /* renamed from: c */
    public String f6117c = "";

    /* renamed from: d */
    public String f6118d = "";

    /* renamed from: e */
    public String f6119e = "";

    /* renamed from: f */
    public String f6120f = "";

    /* renamed from: g */
    public int f6121g = 0;

    /* renamed from: h */
    public byte[] f6122h = null;

    /* renamed from: i */
    public String f6123i = "";

    /* renamed from: j */
    public String f6124j = "";

    /* renamed from: k */
    public Map<String, String> f6125k = null;

    /* renamed from: l */
    public String f6126l = "";

    /* renamed from: m */
    public long f6127m = 0;

    /* renamed from: n */
    public String f6128n = "";

    /* renamed from: o */
    public String f6129o = "";

    /* renamed from: p */
    public String f6130p = "";

    /* renamed from: q */
    public long f6131q = 0;

    /* renamed from: r */
    public String f6132r = "";

    /* renamed from: s */
    public String f6133s = "";

    /* renamed from: t */
    public String f6134t = "";

    /* renamed from: u */
    public String f6135u = "";

    /* renamed from: v */
    public String f6136v = "";

    /* renamed from: w */
    public String f6137w = "";

    /* renamed from: x */
    public String f6138x = "";

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34644a(this.f6115a, 0);
        lVar.mo34648a(this.f6116b, 1);
        lVar.mo34648a(this.f6117c, 2);
        lVar.mo34648a(this.f6118d, 3);
        String str = this.f6119e;
        if (str != null) {
            lVar.mo34648a(str, 4);
        }
        lVar.mo34648a(this.f6120f, 5);
        lVar.mo34644a(this.f6121g, 6);
        lVar.mo34653a(this.f6122h, 7);
        String str2 = this.f6123i;
        if (str2 != null) {
            lVar.mo34648a(str2, 8);
        }
        String str3 = this.f6124j;
        if (str3 != null) {
            lVar.mo34648a(str3, 9);
        }
        Map<String, String> map = this.f6125k;
        if (map != null) {
            lVar.mo34650a((Map) map, 10);
        }
        String str4 = this.f6126l;
        if (str4 != null) {
            lVar.mo34648a(str4, 11);
        }
        lVar.mo34645a(this.f6127m, 12);
        String str5 = this.f6128n;
        if (str5 != null) {
            lVar.mo34648a(str5, 13);
        }
        String str6 = this.f6129o;
        if (str6 != null) {
            lVar.mo34648a(str6, 14);
        }
        String str7 = this.f6130p;
        if (str7 != null) {
            lVar.mo34648a(str7, 15);
        }
        lVar.mo34645a(this.f6131q, 16);
        String str8 = this.f6132r;
        if (str8 != null) {
            lVar.mo34648a(str8, 17);
        }
        String str9 = this.f6133s;
        if (str9 != null) {
            lVar.mo34648a(str9, 18);
        }
        String str10 = this.f6134t;
        if (str10 != null) {
            lVar.mo34648a(str10, 19);
        }
        String str11 = this.f6135u;
        if (str11 != null) {
            lVar.mo34648a(str11, 20);
        }
        String str12 = this.f6136v;
        if (str12 != null) {
            lVar.mo34648a(str12, 21);
        }
        String str13 = this.f6137w;
        if (str13 != null) {
            lVar.mo34648a(str13, 22);
        }
        String str14 = this.f6138x;
        if (str14 != null) {
            lVar.mo34648a(str14, 23);
        }
    }

    static {
        f6113y[0] = 0;
        f6114z.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6115a = kVar.mo34616a(this.f6115a, 0, true);
        this.f6116b = kVar.mo34621a(1, true);
        this.f6117c = kVar.mo34621a(2, true);
        this.f6118d = kVar.mo34621a(3, true);
        this.f6119e = kVar.mo34621a(4, false);
        this.f6120f = kVar.mo34621a(5, true);
        this.f6121g = kVar.mo34616a(this.f6121g, 6, true);
        this.f6122h = kVar.mo34630a(f6113y, 7, true);
        this.f6123i = kVar.mo34621a(8, false);
        this.f6124j = kVar.mo34621a(9, false);
        this.f6125k = (Map) kVar.mo34620a((Object) f6114z, 10, false);
        this.f6126l = kVar.mo34621a(11, false);
        this.f6127m = kVar.mo34618a(this.f6127m, 12, false);
        this.f6128n = kVar.mo34621a(13, false);
        this.f6129o = kVar.mo34621a(14, false);
        this.f6130p = kVar.mo34621a(15, false);
        this.f6131q = kVar.mo34618a(this.f6131q, 16, false);
        this.f6132r = kVar.mo34621a(17, false);
        this.f6133s = kVar.mo34621a(18, false);
        this.f6134t = kVar.mo34621a(19, false);
        this.f6135u = kVar.mo34621a(20, false);
        this.f6136v = kVar.mo34621a(21, false);
        this.f6137w = kVar.mo34621a(22, false);
        this.f6138x = kVar.mo34621a(23, false);
    }
}
