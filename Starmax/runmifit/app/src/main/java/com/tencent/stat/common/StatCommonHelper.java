package com.tencent.stat.common;

import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.bluetooth.BluetoothAdapter;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.Signature;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Proxy;
import android.os.Build;
import android.os.Environment;
import android.os.Process;
import android.os.StatFs;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import android.webkit.WebSettings;
import com.baidu.mobstat.Config;
import com.google.android.gms.common.util.AndroidUtilsLight;
import com.google.common.primitives.UnsignedBytes;
import com.tamic.novate.util.FileUtil;
import com.tencent.mid.api.MidService;
import com.tencent.stat.C3226e;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatReportStrategy;
import com.tencent.stat.StatSpecifyReportedInfo;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;
import org.apache.http.HttpHost;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class StatCommonHelper {

    /* renamed from: A */
    private static long f7186A = -1;

    /* renamed from: B */
    private static long f7187B = 0;

    /* renamed from: C */
    private static Intent f7188C = null;
    public static String NEXT_REPORT = "next_al_report_time";

    /* renamed from: a */
    private static String f7189a = null;

    /* renamed from: b */
    private static String f7190b = null;

    /* renamed from: c */
    private static String f7191c = null;

    /* renamed from: d */
    private static String f7192d = null;

    /* renamed from: e */
    private static Random f7193e = null;

    /* renamed from: f */
    private static DisplayMetrics f7194f = null;

    /* renamed from: g */
    private static String f7195g = null;

    /* renamed from: h */
    private static String f7196h = "";

    /* renamed from: i */
    private static String f7197i = "";

    /* renamed from: j */
    private static String f7198j = "";

    /* renamed from: k */
    private static String f7199k = "";

    /* renamed from: l */
    private static int f7200l = -1;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static StatLogger f7201m = null;

    /* renamed from: n */
    private static String f7202n = null;

    /* renamed from: o */
    private static String f7203o = null;

    /* renamed from: p */
    private static volatile int f7204p = -1;

    /* renamed from: q */
    private static String f7205q = null;

    /* renamed from: r */
    private static String f7206r = null;

    /* renamed from: s */
    private static long f7207s = -1;

    /* renamed from: t */
    private static String f7208t = "";

    /* renamed from: u */
    private static C3201b f7209u = null;

    /* renamed from: v */
    private static String f7210v = "__MTA_FIRST_ACTIVATE__";

    /* renamed from: w */
    private static int f7211w = -1;

    /* renamed from: x */
    private static long f7212x = -1;

    /* renamed from: y */
    private static int f7213y = 0;

    /* renamed from: z */
    private static String f7214z = "";

    public static DeviceInfo getUser(Context context) {
        return C3226e.m8037a(context).mo35429b(context);
    }

    /* renamed from: b */
    private static synchronized Random m7952b() {
        Random random;
        synchronized (StatCommonHelper.class) {
            if (f7193e == null) {
                f7193e = new Random();
            }
            random = f7193e;
        }
        return random;
    }

    public static int getNextSessionID() {
        return m7952b().nextInt(Integer.MAX_VALUE);
    }

    public static byte[] deocdeGZipContent(byte[] bArr) throws IOException {
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        GZIPInputStream gZIPInputStream = new GZIPInputStream(byteArrayInputStream);
        byte[] bArr2 = new byte[4096];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length * 2);
        while (true) {
            int read = gZIPInputStream.read(bArr2);
            if (read != -1) {
                byteArrayOutputStream.write(bArr2, 0, read);
            } else {
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                byteArrayInputStream.close();
                gZIPInputStream.close();
                byteArrayOutputStream.close();
                return byteArray;
            }
        }
    }

    public static String md5sum(String str) {
        if (str == null) {
            return "0";
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            byte[] digest = instance.digest();
            StringBuffer stringBuffer = new StringBuffer();
            for (byte b : digest) {
                byte b2 = b & UnsignedBytes.MAX_VALUE;
                if (b2 < 16) {
                    stringBuffer.append("0");
                }
                stringBuffer.append(Integer.toHexString(b2));
            }
            return stringBuffer.toString();
        } catch (Throwable unused) {
            return "0";
        }
    }

    public static HttpHost getHttpProxy(Context context) {
        NetworkInfo activeNetworkInfo;
        String extraInfo;
        if (context == null) {
            return null;
        }
        try {
            if (context.getPackageManager().checkPermission("android.permission.ACCESS_NETWORK_STATE", context.getPackageName()) != 0 || (activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo()) == null) {
                return null;
            }
            if ((activeNetworkInfo.getTypeName() != null && activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) || (extraInfo = activeNetworkInfo.getExtraInfo()) == null) {
                return null;
            }
            if (!extraInfo.equals("cmwap") && !extraInfo.equals("3gwap")) {
                if (!extraInfo.equals("uniwap")) {
                    if (extraInfo.equals("ctwap")) {
                        return new HttpHost("10.0.0.200", 80);
                    }
                    String defaultHost = Proxy.getDefaultHost();
                    if (defaultHost != null && defaultHost.trim().length() > 0) {
                        return new HttpHost(defaultHost, Proxy.getDefaultPort());
                    }
                    return null;
                }
            }
            return new HttpHost("10.0.0.172", 80);
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
    }

    public static synchronized String getUserID(Context context) {
        synchronized (StatCommonHelper.class) {
            if (f7189a == null || f7189a.trim().length() == 0) {
                f7189a = Util.getDeviceID(context);
                if (f7189a == null || f7189a.trim().length() == 0) {
                    f7189a = Integer.toString(m7952b().nextInt(Integer.MAX_VALUE));
                }
                String str = f7189a;
                return str;
            }
            String str2 = f7189a;
            return str2;
        }
    }

    public static synchronized String getMacId(Context context) {
        String str;
        synchronized (StatCommonHelper.class) {
            if (f7191c == null || f7191c.trim().length() == 0) {
                f7191c = Util.getWifiMacAddress(context);
            }
            str = f7191c;
        }
        return str;
    }

    public static Location getGPSLocation(Context context) {
        try {
            if (!Util.checkPermission(context, "android.permission.ACCESS_FINE_LOCATION")) {
                return null;
            }
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            if (locationManager.isProviderEnabled("gps")) {
                return locationManager.getLastKnownLocation("gps");
            }
            return null;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
            return null;
        }
    }

    public static Location getNetworkLocation(Context context) {
        try {
            if (!Util.checkPermission(context, "android.permission.ACCESS_COARSE_LOCATION") && !Util.checkPermission(context, "android.permission.ACCESS_FINE_LOCATION")) {
                return null;
            }
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            if (locationManager.isProviderEnabled("network")) {
                return locationManager.getLastKnownLocation("network");
            }
            return null;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
            return null;
        }
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        if (f7194f == null) {
            f7194f = new DisplayMetrics();
            ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(f7194f);
        }
        return f7194f;
    }

    public static StatReportStrategy getStatSendStrategy(Context context) {
        return StatConfig.getStatSendStrategy();
    }

    public static boolean isWiFiActive(Context context) {
        NetworkInfo[] allNetworkInfo;
        try {
            if (Util.checkPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getApplicationContext().getSystemService("connectivity");
                if (!(connectivityManager == null || (allNetworkInfo = connectivityManager.getAllNetworkInfo()) == null)) {
                    for (int i = 0; i < allNetworkInfo.length; i++) {
                        if (allNetworkInfo[i].getTypeName().equalsIgnoreCase("WIFI") && allNetworkInfo[i].isConnected()) {
                            return true;
                        }
                    }
                }
                return false;
            }
            f7201m.warn("can not get the permission of android.permission.ACCESS_WIFI_STATE");
            return false;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
    }

    public static String getProperty(Context context, String str) {
        String string;
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null || (string = applicationInfo.metaData.getString(str)) == null) {
                return "";
            }
            return string;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
            return "";
        }
    }

    public static String getAppKey(Context context) {
        String str = f7190b;
        if (str != null) {
            return str;
        }
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return null;
            }
            String string = applicationInfo.metaData.getString("TA_APPKEY");
            if (string != null) {
                f7190b = string;
                return string;
            }
            f7201m.mo35388i("Could not read APPKEY meta-data from AndroidManifest.xml");
            return null;
        } catch (Throwable unused) {
            f7201m.mo35388i("Could not read APPKEY meta-data from AndroidManifest.xml");
            return null;
        }
    }

    public static String getDeviceModel(Context context) {
        if (f7192d == null) {
            f7192d = Build.MODEL;
        }
        return f7192d;
    }

    public static String getInstallChannel(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return null;
            }
            Object obj = applicationInfo.metaData.get("InstallChannel");
            if (obj != null) {
                return obj.toString();
            }
            f7201m.mo35396w("Could not read InstallChannel meta-data from AndroidManifest.xml");
            return null;
        } catch (Throwable unused) {
            f7201m.mo35396w("Could not read InstallChannel meta-data from AndroidManifest.xml");
            return null;
        }
    }

    public static String getActivityName(Context context) {
        if (context == null) {
            return null;
        }
        return context.getClass().getName();
    }

    public static String getPackageName(Context context) {
        if (Util.checkPermission(context, "android.permission.GET_TASKS")) {
            return ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1).get(0).topActivity.getPackageName();
        }
        f7201m.mo35383e("Could not get permission of android.permission.GET_TASKS");
        return null;
    }

    public static String getSimOperator(Context context) {
        TelephonyManager telephonyManager;
        String str = f7195g;
        if (str != null) {
            return str;
        }
        try {
            if (!Util.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
                f7201m.mo35383e("Could not get permission of android.permission.READ_PHONE_STATE");
            } else if (checkPhoneState(context) && (telephonyManager = (TelephonyManager) context.getSystemService("phone")) != null) {
                f7195g = telephonyManager.getSimOperator();
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return f7195g;
    }

    public static String getDeviceIMSI(Context context) {
        try {
            if (!Util.checkPermission(context, "android.permission.READ_PHONE_STATE")) {
                return "";
            }
            String subscriberId = checkPhoneState(context) ? ((TelephonyManager) context.getSystemService("phone")).getSubscriberId() : null;
            if (subscriberId != null) {
                return subscriberId;
            }
            return null;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
            return "";
        }
    }

    public static String getCurAppVersion(Context context) {
        if (isStringValid(f7196h)) {
            return f7196h;
        }
        try {
            f7196h = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (f7196h == null) {
                return "";
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return f7196h;
    }

    public static String getCurAppMd5Signature(Context context) {
        if (isStringValid(f7197i)) {
            return f7197i;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 64);
            if (packageInfo == null) {
                f7201m.mo35383e("packageInfo is null ");
                return "";
            }
            Signature[] signatureArr = packageInfo.signatures;
            if (signatureArr != null) {
                if (signatureArr.length != 0) {
                    f7197i = md5sum(signatureArr[0].toCharsString());
                    return f7197i;
                }
            }
            f7201m.mo35383e("signatures is null");
            return "";
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
    }

    public static String getCurAppSHA1Signature(Context context) {
        if (isStringValid(f7198j)) {
            return f7198j;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 64);
            if (packageInfo == null) {
                f7201m.mo35383e("packageInfo is null ");
                return "";
            }
            Signature[] signatureArr = packageInfo.signatures;
            if (signatureArr != null) {
                if (signatureArr.length != 0) {
                    byte[] digest = MessageDigest.getInstance(AndroidUtilsLight.DIGEST_ALGORITHM_SHA1).digest(signatureArr[0].toByteArray());
                    StringBuffer stringBuffer = new StringBuffer();
                    int length = digest.length;
                    for (int i = 0; i < length; i++) {
                        String upperCase = Integer.toHexString(digest[i] & UnsignedBytes.MAX_VALUE).toUpperCase(Locale.US);
                        if (upperCase.length() == 1) {
                            stringBuffer.append("0");
                        }
                        stringBuffer.append(upperCase);
                        if (i != length - 1) {
                            stringBuffer.append(Config.TRACE_TODAY_VISIT_SPLIT);
                        }
                    }
                    f7198j = stringBuffer.toString();
                    return f7198j;
                }
            }
            f7201m.mo35383e("signatures is null");
            return "";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static boolean checkPhoneState(Context context) {
        return context.getPackageManager().checkPermission("android.permission.READ_PHONE_STATE", context.getPackageName()) == 0;
    }

    public static String getLinkedWay(Context context) {
        try {
            if (!Util.checkPermission(context, "android.permission.INTERNET") || !Util.checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                f7201m.mo35383e("can not get the permission of android.permission.ACCESS_WIFI_STATE");
                return "";
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "";
            }
            if (!activeNetworkInfo.isConnected()) {
                return "";
            }
            String typeName = activeNetworkInfo.getTypeName();
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (typeName == null) {
                return "";
            }
            if (typeName.equalsIgnoreCase("WIFI")) {
                return "WIFI";
            }
            if (typeName.equalsIgnoreCase("MOBILE")) {
                if (extraInfo == null || extraInfo.trim().length() <= 0) {
                    return "MOBILE";
                }
            } else if (extraInfo == null || extraInfo.trim().length() <= 0) {
                return typeName;
            }
            return extraInfo;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
            return "";
        }
    }

    public static Integer getTelephonyNetworkType(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return Integer.valueOf(telephonyManager.getNetworkType());
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    public static boolean haveGravity(Context context) {
        return ((SensorManager) context.getSystemService("sensor")) != null;
    }

    public static String getAppVersion(Context context) {
        if (isStringValid(f7199k)) {
            return f7199k;
        }
        try {
            f7199k = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            if (f7199k == null || f7199k.length() == 0) {
                return "unknown";
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return f7199k;
    }

    public static int hasRootAccess(Context context) {
        int i = f7200l;
        if (i != -1) {
            return i;
        }
        f7200l = 0;
        try {
            if (C3202c.m7958a()) {
                f7200l = 1;
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return f7200l;
    }

    /* renamed from: com.tencent.stat.common.StatCommonHelper$c */
    static class C3202c {

        /* renamed from: a */
        private static int f7215a = -1;

        /* renamed from: a */
        public static boolean m7958a() {
            int i = f7215a;
            if (i == 1) {
                return true;
            }
            if (i == 0) {
                return false;
            }
            String[] strArr = {"/bin", "/system/bin/", "/system/xbin/", "/system/sbin/", "/sbin/", "/vendor/bin/"};
            int i2 = 0;
            while (i2 < strArr.length) {
                try {
                    if (new File(strArr[i2] + "su").exists()) {
                        f7215a = 1;
                        return true;
                    }
                    i2++;
                } catch (Exception unused) {
                }
            }
            f7215a = 0;
            return false;
        }
    }

    public static synchronized StatLogger getLogger() {
        StatLogger statLogger;
        synchronized (StatCommonHelper.class) {
            if (f7201m == null) {
                f7201m = new StatLogger(StatConstants.LOG_TAG);
                f7201m.setDebugEnable(false);
            }
            statLogger = f7201m;
        }
        return statLogger;
    }

    public static long getTomorrowStartMilliseconds() {
        try {
            Calendar instance = Calendar.getInstance();
            instance.set(11, 0);
            instance.set(12, 0);
            instance.set(13, 0);
            instance.set(14, 0);
            return instance.getTimeInMillis() + 86400000;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
            return System.currentTimeMillis() + 86400000;
        }
    }

    public static String getDateString(int i) {
        Calendar instance = Calendar.getInstance();
        instance.roll(6, i);
        return new SimpleDateFormat("yyyyMMdd").format(instance.getTime());
    }

    public static Long convertStringToLong(String str, String str2, int i, int i2, Long l) {
        if (!(str == null || str2 == null)) {
            if (str2.equalsIgnoreCase(FileUtil.HIDDEN_PREFIX) || str2.equalsIgnoreCase("|")) {
                str2 = "\\" + str2;
            }
            String[] split = str.split(str2);
            if (split.length == i2) {
                try {
                    Long l2 = 0L;
                    for (String str3 : split) {
                        l2 = Long.valueOf(((long) i) * (l2.longValue() + Long.valueOf(str3).longValue()));
                    }
                    return l2;
                } catch (NumberFormatException unused) {
                }
            }
        }
        return l;
    }

    public static long getSDKLongVersion(String str) {
        return convertStringToLong(str, FileUtil.HIDDEN_PREFIX, 100, 3, 0L).longValue();
    }

    public static boolean isStringValid(String str) {
        return (str == null || str.trim().length() == 0) ? false : true;
    }

    public static String getExternalStorageInfo(Context context) {
        String path;
        if (isStringValid(f7202n)) {
            return f7202n;
        }
        try {
            if (Util.checkPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                String externalStorageState = Environment.getExternalStorageState();
                if (!(externalStorageState == null || !externalStorageState.equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null)) {
                    StatFs statFs = new StatFs(path);
                    f7202n = String.valueOf((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1000000) + "/" + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
                    return f7202n;
                }
                return null;
            }
            f7201m.warn("can not get the permission of android.permission.WRITE_EXTERNAL_STORAGE");
            return null;
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
    }

    public static String getDateFormat(long j) {
        return new SimpleDateFormat("yyyyMMdd").format(new Date(j));
    }

    public static String getTimeFormat(long j) {
        return new SimpleDateFormat("yyyyMMddHHmmSS").format(new Date(j));
    }

    public static int getAndroidOsBuildVersion() {
        return Build.VERSION.SDK_INT;
    }

    public static int checkBluetooth() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter == null) {
            return -1;
        }
        return defaultAdapter.isEnabled() ? 1 : 0;
    }

    public static boolean isMainProcess(Context context) {
        return context.getPackageName().equals(getCurProcessName(context));
    }

    public static String getCurProcessName(Context context) {
        try {
            if (f7203o != null) {
                return f7203o;
            }
            int myPid = Process.myPid();
            Iterator<ActivityManager.RunningAppProcessInfo> it = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses().iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ActivityManager.RunningAppProcessInfo next = it.next();
                if (next.pid == myPid) {
                    f7203o = next.processName;
                    break;
                }
            }
            return f7203o;
        } catch (Throwable unused) {
        }
    }

    public static String getTagForConcurrentProcess(Context context, String str) {
        if (StatConfig.isEnableConcurrentProcess()) {
            if (f7203o == null) {
                f7203o = getCurProcessName(context);
            }
            if (f7203o != null) {
                return str + "_" + f7203o;
            }
        }
        return str;
    }

    public static void destroyProcess(Process process) {
        if (process != null) {
            try {
                process.exitValue();
            } catch (IllegalThreadStateException unused) {
            }
        }
    }

    public static String getDatabaseName(Context context) {
        return getTagForConcurrentProcess(context, StatConstants.DATABASE_NAME);
    }

    public static synchronized Integer getNextEventIndexNo(Context context) {
        Integer valueOf;
        synchronized (StatCommonHelper.class) {
            if (f7204p <= 0) {
                f7204p = StatPreferences.getInt(context, "MTA_EVENT_INDEX", 0);
                StatPreferences.putInt(context, "MTA_EVENT_INDEX", f7204p + 1000);
            } else if (f7204p % 1000 == 0) {
                try {
                    int i = f7204p + 1000;
                    if (f7204p >= 2147383647) {
                        i = 0;
                    }
                    StatPreferences.putInt(context, "MTA_EVENT_INDEX", i);
                } catch (Throwable th) {
                    f7201m.mo35396w(th);
                }
            }
            f7204p++;
            valueOf = Integer.valueOf(f7204p);
        }
        return valueOf;
    }

    public static String getCpuString() {
        if (isStringValid(f7205q)) {
            return f7205q;
        }
        if (Build.CPU_ABI.equalsIgnoreCase("x86")) {
            f7205q = "Intel";
        } else {
            try {
                byte[] bArr = new byte[1024];
                RandomAccessFile randomAccessFile = new RandomAccessFile("/proc/cpuinfo", "r");
                randomAccessFile.read(bArr);
                randomAccessFile.close();
                String str = new String(bArr);
                int indexOf = str.indexOf(0);
                if (indexOf != -1) {
                    f7205q = str.substring(0, indexOf);
                } else {
                    f7205q = str;
                }
            } catch (Throwable th) {
                f7201m.mo35384e(th);
            }
        }
        return f7205q;
    }

    public static String getCpuType() {
        String str;
        String cpuString = getCpuString();
        if (cpuString.contains("ARMv5")) {
            str = "armv5";
        } else if (cpuString.contains("ARMv6")) {
            str = "armv6";
        } else if (cpuString.contains("ARMv7")) {
            str = "armv7";
        } else if (!cpuString.contains("Intel")) {
            return "unknown";
        } else {
            str = "x86";
        }
        if (cpuString.contains("neon")) {
            return str + "_neon";
        } else if (cpuString.contains("vfpv3")) {
            return str + "_vfpv3";
        } else if (cpuString.contains(" vfp")) {
            return str + "_vfp";
        } else {
            return str + "_none";
        }
    }

    public static String getRomMemory() {
        if (isStringValid(f7206r)) {
            return f7206r;
        }
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        f7206r = String.valueOf((((long) statFs.getBlockSize()) * ((long) statFs.getAvailableBlocks())) / 1000000) + "/" + String.valueOf(getTotalInternalMemorySize() / 1000000);
        return f7206r;
    }

    public static long getTotalInternalMemorySize() {
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        return ((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize());
    }

    public static String getSystemMemory(Context context) {
        try {
            return String.valueOf(m7950a(context) / 1000000) + "/" + String.valueOf(m7953c() / 1000000);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    private static long m7950a(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        return memoryInfo.availMem;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0059 A[SYNTHETIC, Splitter:B:29:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x005e A[Catch:{ Exception -> 0x003d }] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0069 A[SYNTHETIC, Splitter:B:37:0x0069] */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0071 A[Catch:{ Exception -> 0x006d }] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long m7953c() {
        /*
            long r0 = com.tencent.stat.common.StatCommonHelper.f7207s
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 <= 0) goto L_0x0009
            return r0
        L_0x0009:
            java.lang.String r0 = "/proc/meminfo"
            r1 = 1
            r3 = 0
            java.io.FileReader r4 = new java.io.FileReader     // Catch:{ Exception -> 0x0050, all -> 0x004c }
            r4.<init>(r0)     // Catch:{ Exception -> 0x0050, all -> 0x004c }
            java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0047, all -> 0x0044 }
            r5 = 8192(0x2000, float:1.14794E-41)
            r0.<init>(r4, r5)     // Catch:{ Exception -> 0x0047, all -> 0x0044 }
            java.lang.String r3 = r0.readLine()     // Catch:{ Exception -> 0x0042 }
            if (r3 == 0) goto L_0x0036
            java.lang.String r5 = "\\s+"
            java.lang.String[] r3 = r3.split(r5)     // Catch:{ Exception -> 0x0042 }
            r5 = 1
            r3 = r3[r5]     // Catch:{ Exception -> 0x0042 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x0042 }
            int r1 = r3.intValue()     // Catch:{ Exception -> 0x0042 }
            long r1 = (long) r1
            r5 = 1024(0x400, double:5.06E-321)
            long r1 = r1 * r5
        L_0x0036:
            r4.close()     // Catch:{ Exception -> 0x003d }
            r0.close()     // Catch:{ Exception -> 0x003d }
            goto L_0x0061
        L_0x003d:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0061
        L_0x0042:
            r3 = move-exception
            goto L_0x0054
        L_0x0044:
            r1 = move-exception
            r0 = r3
            goto L_0x0067
        L_0x0047:
            r0 = move-exception
            r7 = r3
            r3 = r0
            r0 = r7
            goto L_0x0054
        L_0x004c:
            r1 = move-exception
            r0 = r3
            r4 = r0
            goto L_0x0067
        L_0x0050:
            r0 = move-exception
            r4 = r3
            r3 = r0
            r0 = r4
        L_0x0054:
            r3.printStackTrace()     // Catch:{ all -> 0x0066 }
            if (r4 == 0) goto L_0x005c
            r4.close()     // Catch:{ Exception -> 0x003d }
        L_0x005c:
            if (r0 == 0) goto L_0x0061
            r0.close()     // Catch:{ Exception -> 0x003d }
        L_0x0061:
            com.tencent.stat.common.StatCommonHelper.f7207s = r1
            long r0 = com.tencent.stat.common.StatCommonHelper.f7207s
            return r0
        L_0x0066:
            r1 = move-exception
        L_0x0067:
            if (r4 == 0) goto L_0x006f
            r4.close()     // Catch:{ Exception -> 0x006d }
            goto L_0x006f
        L_0x006d:
            r0 = move-exception
            goto L_0x0075
        L_0x006f:
            if (r0 == 0) goto L_0x0078
            r0.close()     // Catch:{ Exception -> 0x006d }
            goto L_0x0078
        L_0x0075:
            r0.printStackTrace()
        L_0x0078:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.common.StatCommonHelper.m7953c():long");
    }

    public static JSONObject getCpuInfo(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("n", C3199a.m7954a());
            String d = C3199a.m7957d();
            if (d != null && d.length() > 0) {
                jSONObject.put("na", d);
            }
            int b = C3199a.m7955b();
            if (b > 0) {
                jSONObject.put("fx", b / 1000000);
            }
            int c = C3199a.m7956c();
            if (c > 0) {
                jSONObject.put("fn", c / 1000000);
            }
        } catch (Throwable th) {
            Log.w(StatConstants.LOG_TAG, "get cpu error", th);
        }
        return jSONObject;
    }

    /* renamed from: com.tencent.stat.common.StatCommonHelper$a */
    static class C3199a {

        /* renamed from: com.tencent.stat.common.StatCommonHelper$a$a */
        class C3200a implements FileFilter {
            C3200a() {
            }

            public boolean accept(File file) {
                return Pattern.matches("cpu[0-9]", file.getName());
            }
        }

        /* renamed from: a */
        static int m7954a() {
            try {
                return new File("/sys/devices/system/cpu/").listFiles(new C3200a()).length;
            } catch (Exception e) {
                e.printStackTrace();
                return 1;
            }
        }

        /* renamed from: b */
        static int m7955b() {
            int i = 0;
            String str = "";
            try {
                InputStream inputStream = new ProcessBuilder("/system/bin/cat", "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq").start().getInputStream();
                byte[] bArr = new byte[24];
                while (inputStream.read(bArr) != -1) {
                    str = str + new String(bArr);
                }
                inputStream.close();
                String trim = str.trim();
                if (trim.length() > 0) {
                    i = Integer.valueOf(trim).intValue();
                }
            } catch (Exception e) {
                StatCommonHelper.f7201m.mo35384e((Throwable) e);
            }
            return i * 1000;
        }

        /* renamed from: c */
        static int m7956c() {
            int i = 0;
            String str = "";
            try {
                InputStream inputStream = new ProcessBuilder("/system/bin/cat", "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq").start().getInputStream();
                byte[] bArr = new byte[24];
                while (inputStream.read(bArr) != -1) {
                    str = str + new String(bArr);
                }
                inputStream.close();
                String trim = str.trim();
                if (trim.length() > 0) {
                    i = Integer.valueOf(trim).intValue();
                }
            } catch (Throwable th) {
                StatCommonHelper.f7201m.mo35384e(th);
            }
            return i * 1000;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:36:0x0066, code lost:
            if (r4 != null) goto L_0x0046;
         */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x0056 A[SYNTHETIC, Splitter:B:24:0x0056] */
        /* JADX WARNING: Removed duplicated region for block: B:27:0x005b A[Catch:{ IOException -> 0x005e }] */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x0063 A[SYNTHETIC, Splitter:B:34:0x0063] */
        /* renamed from: d */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static java.lang.String m7957d() {
            /*
                java.lang.String r0 = "/proc/cpuinfo"
                java.lang.String r1 = ""
                java.lang.String[] r1 = new java.lang.String[]{r1, r1}
                r2 = 0
                r3 = 0
                java.io.FileReader r4 = new java.io.FileReader     // Catch:{ IOException -> 0x005f, all -> 0x0051 }
                r4.<init>(r0)     // Catch:{ IOException -> 0x005f, all -> 0x0051 }
                java.io.BufferedReader r0 = new java.io.BufferedReader     // Catch:{ IOException -> 0x004f, all -> 0x004c }
                r5 = 8192(0x2000, float:1.14794E-41)
                r0.<init>(r4, r5)     // Catch:{ IOException -> 0x004f, all -> 0x004c }
                java.lang.String r2 = r0.readLine()     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                if (r2 == 0) goto L_0x0043
                java.lang.String r5 = "\\s+"
                java.lang.String[] r2 = r2.split(r5)     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r5 = 2
            L_0x0023:
                int r6 = r2.length     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                if (r5 >= r6) goto L_0x0043
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r6.<init>()     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r7 = r1[r3]     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r6.append(r7)     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r7 = r2[r5]     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r6.append(r7)     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                java.lang.String r7 = " "
                r6.append(r7)     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                java.lang.String r6 = r6.toString()     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                r1[r3] = r6     // Catch:{ IOException -> 0x0061, all -> 0x004a }
                int r5 = r5 + 1
                goto L_0x0023
            L_0x0043:
                r0.close()     // Catch:{ IOException -> 0x0069 }
            L_0x0046:
                r4.close()     // Catch:{ IOException -> 0x0069 }
                goto L_0x0069
            L_0x004a:
                r1 = move-exception
                goto L_0x0054
            L_0x004c:
                r1 = move-exception
                r0 = r2
                goto L_0x0054
            L_0x004f:
                r0 = r2
                goto L_0x0061
            L_0x0051:
                r1 = move-exception
                r0 = r2
                r4 = r0
            L_0x0054:
                if (r0 == 0) goto L_0x0059
                r0.close()     // Catch:{ IOException -> 0x005e }
            L_0x0059:
                if (r4 == 0) goto L_0x005e
                r4.close()     // Catch:{ IOException -> 0x005e }
            L_0x005e:
                throw r1
            L_0x005f:
                r0 = r2
                r4 = r0
            L_0x0061:
                if (r0 == 0) goto L_0x0066
                r0.close()     // Catch:{ IOException -> 0x0069 }
            L_0x0066:
                if (r4 == 0) goto L_0x0069
                goto L_0x0046
            L_0x0069:
                r0 = r1[r3]
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.common.StatCommonHelper.C3199a.m7957d():java.lang.String");
        }
    }

    public static String getAllSensors(Context context) {
        List<Sensor> sensorList;
        if (isStringValid(f7208t)) {
            return f7208t;
        }
        try {
            SensorManager sensorManager = (SensorManager) context.getSystemService("sensor");
            if (!(sensorManager == null || (sensorList = sensorManager.getSensorList(-1)) == null)) {
                StringBuilder sb = new StringBuilder(sensorList.size() * 10);
                for (int i = 0; i < sensorList.size(); i++) {
                    sb.append(sensorList.get(i).getType());
                    if (i != sensorList.size() - 1) {
                        sb.append(",");
                    }
                }
                f7208t = sb.toString();
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return f7208t;
    }

    public static synchronized C3201b getCamerasHelper(Context context) {
        C3201b bVar;
        synchronized (StatCommonHelper.class) {
            if (f7209u == null) {
                f7209u = new C3201b();
            }
            bVar = f7209u;
        }
        return bVar;
    }

    /* renamed from: com.tencent.stat.common.StatCommonHelper$b */
    static class C3201b {
        C3201b() {
        }
    }

    public static synchronized int isTheFirstTimeActivate(Context context) {
        synchronized (StatCommonHelper.class) {
            if (f7211w != -1) {
                int i = f7211w;
                return i;
            }
            checkFirstTimeActivate(context);
            int i2 = f7211w;
            return i2;
        }
    }

    public static void checkFirstTimeActivate(Context context) {
        f7211w = StatPreferences.getInt(context, f7210v, 1);
        if (f7211w == 1) {
            StatPreferences.putInt(context, f7210v, 0);
        }
    }

    public static boolean isSpecifyReportedValid(StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (statSpecifyReportedInfo == null) {
            return false;
        }
        return isStringValid(statSpecifyReportedInfo.getAppKey());
    }

    public static boolean needCheckTime(Context context) {
        if (f7212x < 0) {
            f7212x = StatPreferences.getLong(context, "mta.qq.com.checktime", 0);
        }
        return Math.abs(System.currentTimeMillis() - f7212x) > 86400000;
    }

    public static void updateCheckTime(Context context) {
        f7212x = System.currentTimeMillis();
        StatPreferences.putLong(context, "mta.qq.com.checktime", f7212x);
    }

    public static int getDiffTime(Context context, boolean z) {
        if (z) {
            f7213y = readDiffTimeFromServer(context);
        }
        return f7213y;
    }

    public static int readDiffTimeFromServer(Context context) {
        return StatPreferences.getInt(context, "mta.qq.com.difftime", 0);
    }

    public static void writeDiffTimeFromServer(Context context, int i) {
        f7213y = i;
        StatPreferences.putInt(context, "mta.qq.com.difftime", i);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0043, code lost:
        if (r0 != null) goto L_0x0045;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0045, code lost:
        r0.release();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0064, code lost:
        if (r0 == null) goto L_0x0067;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0069, code lost:
        return com.tencent.stat.common.StatCommonHelper.f7214z;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String getCameras(android.content.Context r4) {
        /*
            java.lang.String r0 = com.tencent.stat.common.StatCommonHelper.f7214z
            boolean r0 = isStringValid(r0)
            if (r0 == 0) goto L_0x000b
            java.lang.String r4 = com.tencent.stat.common.StatCommonHelper.f7214z
            return r4
        L_0x000b:
            r0 = 0
            java.lang.String r1 = "android.permission.CAMERA"
            boolean r4 = com.tencent.stat.common.Util.checkPermission(r4, r1)     // Catch:{ all -> 0x0049 }
            if (r4 == 0) goto L_0x0043
            android.hardware.Camera r0 = android.hardware.Camera.open()     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0043
            android.hardware.Camera$Parameters r4 = r0.getParameters()     // Catch:{ all -> 0x0049 }
            java.util.List r4 = r4.getSupportedPictureSizes()     // Catch:{ all -> 0x0049 }
            r1 = 0
            java.lang.Object r4 = r4.get(r1)     // Catch:{ all -> 0x0049 }
            android.hardware.Camera$Size r4 = (android.hardware.Camera.Size) r4     // Catch:{ all -> 0x0049 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0049 }
            r1.<init>()     // Catch:{ all -> 0x0049 }
            int r2 = r4.width     // Catch:{ all -> 0x0049 }
            r1.append(r2)     // Catch:{ all -> 0x0049 }
            java.lang.String r2 = "*"
            r1.append(r2)     // Catch:{ all -> 0x0049 }
            int r4 = r4.height     // Catch:{ all -> 0x0049 }
            r1.append(r4)     // Catch:{ all -> 0x0049 }
            java.lang.String r4 = r1.toString()     // Catch:{ all -> 0x0049 }
            com.tencent.stat.common.StatCommonHelper.f7214z = r4     // Catch:{ all -> 0x0049 }
        L_0x0043:
            if (r0 == 0) goto L_0x0067
        L_0x0045:
            r0.release()
            goto L_0x0067
        L_0x0049:
            r4 = move-exception
            com.tencent.stat.common.StatLogger r1 = com.tencent.stat.common.StatCommonHelper.f7201m     // Catch:{ all -> 0x006a }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x006a }
            r2.<init>()     // Catch:{ all -> 0x006a }
            java.lang.String r3 = "getCameras failed, "
            r2.append(r3)     // Catch:{ all -> 0x006a }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x006a }
            r2.append(r4)     // Catch:{ all -> 0x006a }
            java.lang.String r4 = r2.toString()     // Catch:{ all -> 0x006a }
            r1.mo35396w(r4)     // Catch:{ all -> 0x006a }
            if (r0 == 0) goto L_0x0067
            goto L_0x0045
        L_0x0067:
            java.lang.String r4 = com.tencent.stat.common.StatCommonHelper.f7214z
            return r4
        L_0x006a:
            r4 = move-exception
            if (r0 == 0) goto L_0x0070
            r0.release()
        L_0x0070:
            goto L_0x0072
        L_0x0071:
            throw r4
        L_0x0072:
            goto L_0x0071
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.stat.common.StatCommonHelper.getCameras(android.content.Context):java.lang.String");
    }

    public static boolean isBackgroundRunning(Context context) {
        ActivityManager activityManager;
        if (context == null || (activityManager = (ActivityManager) context.getSystemService("activity")) == null) {
            return false;
        }
        String packageName = context.getPackageName();
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : activityManager.getRunningAppProcesses()) {
            if (runningAppProcessInfo.processName.startsWith(packageName)) {
                if (runningAppProcessInfo.importance == 400) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }

    public static Map<String, Integer> getRunningApp(Context context) {
        HashMap hashMap = new HashMap();
        try {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
                hashMap.put(runningAppProcessInfo.processName, 1);
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return hashMap;
    }

    public static Map<String, Integer> getRecentTasks(Context context) {
        HashMap hashMap = new HashMap();
        try {
            for (ActivityManager.RecentTaskInfo recentTaskInfo : ((ActivityManager) context.getSystemService("activity")).getRecentTasks(64, 1)) {
                ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(recentTaskInfo.baseIntent, 0);
                if (resolveActivity != null) {
                    hashMap.put(resolveActivity.resolvePackageName, 1);
                }
            }
        } catch (Throwable th) {
            f7201m.mo35384e(th);
        }
        return hashMap;
    }

    public static JSONArray formatThrowable(Throwable th) {
        JSONArray jSONArray = new JSONArray();
        if (th != null) {
            jSONArray.put(th.toString());
            formatStackTraceElement(jSONArray, th.getStackTrace());
        }
        return jSONArray;
    }

    public static JSONArray formatStackTraceElement(JSONArray jSONArray, StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr != null) {
            for (StackTraceElement stackTraceElement : stackTraceElementArr) {
                jSONArray.put(stackTraceElement.toString());
            }
        }
        return jSONArray;
    }

    public static int getRunningCounter(Context context) {
        try {
            String str = getCurAppVersion(context) + ".run.cnt";
            int i = StatPreferences.getInt(context, str, 1);
            StatPreferences.putInt(context, str, i + 1);
            return i;
        } catch (Throwable unused) {
            return 0;
        }
    }

    public static void encodeAppPackageInfo(JSONObject jSONObject, PackageInfo packageInfo) {
        try {
            jSONObject.put(Config.PACKAGE_NAME, packageInfo.packageName);
            jSONObject.put("av", packageInfo.versionName);
            jSONObject.put("vc", packageInfo.versionCode);
            jSONObject.put("fit", packageInfo.firstInstallTime / 1000);
            jSONObject.put("lut", packageInfo.lastUpdateTime / 1000);
            jSONObject.put("fg", packageInfo.applicationInfo.flags);
        } catch (JSONException unused) {
        }
    }

    public static JSONArray getAppList(Context context) {
        JSONArray jSONArray = new JSONArray();
        try {
            List<PackageInfo> installedPackages = context.getPackageManager().getInstalledPackages(0);
            Map<String, Integer> runningApp = getRunningApp(context);
            Map<String, Integer> recentTasks = getRecentTasks(context);
            for (PackageInfo packageInfo : installedPackages) {
                JSONObject jSONObject = new JSONObject();
                String str = packageInfo.packageName;
                encodeAppPackageInfo(jSONObject, packageInfo);
                if (runningApp.containsKey(str)) {
                    jSONObject.put(Config.EVENT_VIEW_RES_NAME, 1);
                }
                if (recentTasks.containsKey(str)) {
                    jSONObject.put("rt", 1);
                }
                jSONArray.put(jSONObject);
            }
        } catch (JSONException e) {
            f7201m.mo35384e((Throwable) e);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return jSONArray;
    }

    public static String getLauncherPackageName(Context context) {
        if (context == null) {
            return null;
        }
        try {
            Intent intent = new Intent("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
            if (resolveActivity.activityInfo != null && !resolveActivity.activityInfo.packageName.equals("android")) {
                return resolveActivity.activityInfo.packageName;
            }
            return null;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    public static boolean isBackground(Context context) {
        for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses()) {
            if (runningAppProcessInfo.processName.equalsIgnoreCase(context.getPackageName())) {
                StatLogger statLogger = f7201m;
                statLogger.mo35388i("isBackground processName:" + runningAppProcessInfo.processName + ", importance:" + runningAppProcessInfo.importance);
                if ((runningAppProcessInfo.importance == 100 || runningAppProcessInfo.importance == 200) && !isLockScreenOn(context)) {
                    return isApplicationBroughtToBackground(context);
                }
                return true;
            }
        }
        return true;
    }

    public static boolean isLockScreenOn(Context context) {
        return ((KeyguardManager) context.getSystemService("keyguard")).inKeyguardRestrictedInputMode();
    }

    public static boolean isApplicationBroughtToBackground(Context context) {
        if (!Util.checkPermission(context, "android.permission.GET_TASKS")) {
            return true;
        }
        List<ActivityManager.RunningTaskInfo> runningTasks = ((ActivityManager) context.getSystemService("activity")).getRunningTasks(1);
        if (!runningTasks.isEmpty()) {
            ComponentName componentName = runningTasks.get(0).topActivity;
            StatLogger statLogger = f7201m;
            statLogger.mo35388i("isApplicationBroughtToBackground top package:" + componentName.getPackageName());
            if (!componentName.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }
        return false;
    }

    public static boolean needReportApp(Context context) {
        if (!StatConfig.isAutoTrackAppsEvent()) {
            return false;
        }
        if (f7186A == -1) {
            f7187B = Long.valueOf(StatConfig.getSDKProperty("autoAL", String.valueOf(86400L))).longValue() * 1000;
            if (f7187B > 10800000) {
                f7186A = StatPreferences.getLong(context, NEXT_REPORT, 0);
            } else {
                f7186A = Long.MAX_VALUE;
            }
            StatLogger statLogger = f7201m;
            statLogger.mo35381d("next_al_report_time:" + f7186A);
        }
        if (f7186A < System.currentTimeMillis()) {
            return true;
        }
        return false;
    }

    public static void updateNextReportTime(Context context) {
        f7186A = System.currentTimeMillis() + f7187B;
        StatPreferences.putLong(context, NEXT_REPORT, f7186A);
    }

    public static String getNewMid(Context context) {
        try {
            return (String) MidService.class.getMethod("getNewMid", Context.class).invoke(null, context);
        } catch (Throwable unused) {
            f7201m.mo35396w("MidService.getNewMid method notfound");
            return null;
        }
    }

    public static void safeClose(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static int getArgumentsLength(Object... objArr) {
        if (objArr == null) {
            return 0;
        }
        int i = 0;
        for (Object obj : objArr) {
            if (obj != null) {
                i += obj.toString().length();
            }
        }
        return i;
    }

    public static boolean checkArgumentsLength(Object... objArr) {
        return getArgumentsLength(objArr) > 61440;
    }

    public static String getUserAgent(Context context) {
        String str;
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                str = WebSettings.getDefaultUserAgent(context);
            } catch (Exception unused) {
                str = System.getProperty("http.agent");
            }
        } else {
            str = System.getProperty("http.agent");
        }
        StringBuffer stringBuffer = new StringBuffer();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char charAt = str.charAt(i);
            if (charAt <= 31 || charAt >= 127) {
                stringBuffer.append(String.format("\\u%04x", Integer.valueOf(charAt)));
            } else {
                stringBuffer.append(charAt);
            }
        }
        return stringBuffer.toString();
    }

    public static JSONObject getBatteryInfo(Context context) {
        if (context == null) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            if (f7188C == null) {
                f7188C = context.getApplicationContext().registerReceiver(null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            }
            int intExtra = f7188C.getIntExtra(NotificationCompat.CATEGORY_STATUS, 0);
            int intExtra2 = f7188C.getIntExtra("health", 1);
            boolean booleanExtra = f7188C.getBooleanExtra("present", false);
            int intExtra3 = f7188C.getIntExtra("level", 0);
            int intExtra4 = f7188C.getIntExtra("scale", 0);
            int intExtra5 = f7188C.getIntExtra("plugged", 0);
            int intExtra6 = f7188C.getIntExtra("voltage", 0);
            int intExtra7 = f7188C.getIntExtra("temperature", 0);
            String stringExtra = f7188C.getStringExtra("technology");
            jSONObject.put(NotificationCompat.CATEGORY_STATUS, intExtra);
            jSONObject.put("health", intExtra2);
            jSONObject.put("present", booleanExtra);
            jSONObject.put("level", intExtra3);
            jSONObject.put("scale", intExtra4);
            jSONObject.put("plugged", intExtra5);
            jSONObject.put("voltage", intExtra6);
            jSONObject.put("temperature", intExtra7);
            jSONObject.put("technology", stringExtra);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
