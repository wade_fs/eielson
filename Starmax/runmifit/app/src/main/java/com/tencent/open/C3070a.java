package com.tencent.open;

import android.net.Uri;
import android.webkit.WebView;
import com.tencent.open.p059a.C3082f;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.tencent.open.a */
/* compiled from: ProGuard */
public class C3070a {

    /* renamed from: a */
    protected HashMap<String, C3072b> f6699a = new HashMap<>();

    /* renamed from: com.tencent.open.a$a */
    /* compiled from: ProGuard */
    public static class C3071a {

        /* renamed from: a */
        protected WeakReference<WebView> f6700a;

        /* renamed from: b */
        protected long f6701b;

        /* renamed from: c */
        protected String f6702c;

        public C3071a(WebView webView, long j, String str) {
            this.f6700a = new WeakReference<>(webView);
            this.f6701b = j;
            this.f6702c = str;
        }

        /* renamed from: a */
        public void mo35071a(Object obj) {
            String str;
            WebView webView = this.f6700a.get();
            if (webView != null) {
                if (obj instanceof String) {
                    String replace = ((String) obj).replace("\\", "\\\\").replace("'", "\\'");
                    str = "'" + ((Object) replace) + "'";
                } else if ((obj instanceof Number) || (obj instanceof Long) || (obj instanceof Integer) || (obj instanceof Double) || (obj instanceof Float)) {
                    str = obj.toString();
                } else {
                    str = obj instanceof Boolean ? obj.toString() : "'undefined'";
                }
                webView.loadUrl("javascript:window.JsBridge&&JsBridge.callback(" + this.f6701b + ",{'r':0,'result':" + str + "});");
            }
        }

        /* renamed from: a */
        public void mo35070a() {
            WebView webView = this.f6700a.get();
            if (webView != null) {
                webView.loadUrl("javascript:window.JsBridge&&JsBridge.callback(" + this.f6701b + ",{'r':1,'result':'no such method'})");
            }
        }

        /* renamed from: a */
        public void mo35072a(String str) {
            WebView webView = this.f6700a.get();
            if (webView != null) {
                webView.loadUrl("javascript:" + str);
            }
        }
    }

    /* renamed from: com.tencent.open.a$b */
    /* compiled from: ProGuard */
    public static class C3072b {
        public boolean customCallback() {
            return false;
        }

        public void call(String str, List<String> list, C3071a aVar) {
            String str2;
            Method method;
            Object obj;
            Method[] declaredMethods = getClass().getDeclaredMethods();
            int length = declaredMethods.length;
            int i = 0;
            while (true) {
                str2 = null;
                if (i >= length) {
                    method = null;
                    break;
                }
                method = declaredMethods[i];
                if (method.getName().equals(str) && method.getParameterTypes().length == list.size()) {
                    break;
                }
                i++;
            }
            if (method != null) {
                try {
                    int size = list.size();
                    if (size == 0) {
                        obj = method.invoke(this, new Object[0]);
                    } else if (size == 1) {
                        obj = method.invoke(this, list.get(0));
                    } else if (size == 2) {
                        obj = method.invoke(this, list.get(0), list.get(1));
                    } else if (size == 3) {
                        obj = method.invoke(this, list.get(0), list.get(1), list.get(2));
                    } else if (size == 4) {
                        obj = method.invoke(this, list.get(0), list.get(1), list.get(2), list.get(3));
                    } else if (size != 5) {
                        obj = method.invoke(this, list.get(0), list.get(1), list.get(2), list.get(3), list.get(4), list.get(5));
                    } else {
                        obj = method.invoke(this, list.get(0), list.get(1), list.get(2), list.get(3), list.get(4));
                    }
                    Class<?> returnType = method.getReturnType();
                    C3082f.m7631b("openSDK_LOG.JsBridge", "-->call, result: " + obj + " | ReturnType: " + returnType.getName());
                    if (!"void".equals(returnType.getName())) {
                        if (returnType != Void.class) {
                            if (aVar != null && customCallback()) {
                                if (obj != null) {
                                    str2 = obj.toString();
                                }
                                aVar.mo35072a(str2);
                                return;
                            }
                            return;
                        }
                    }
                    if (aVar != null) {
                        aVar.mo35071a((Object) null);
                    }
                } catch (Exception e) {
                    C3082f.m7632b("openSDK_LOG.JsBridge", "-->handler call mehtod ex. targetMethod: " + method, e);
                    if (aVar != null) {
                        aVar.mo35070a();
                    }
                }
            } else if (aVar != null) {
                aVar.mo35070a();
            }
        }
    }

    /* renamed from: a */
    public void mo35067a(C3072b bVar, String str) {
        this.f6699a.put(str, bVar);
    }

    /* renamed from: a */
    public void mo35068a(String str, String str2, List<String> list, C3071a aVar) {
        C3082f.m7628a("openSDK_LOG.JsBridge", "getResult---objName = " + str + " methodName = " + str2);
        int size = list.size();
        for (int i = 0; i < size; i++) {
            try {
                list.set(i, URLDecoder.decode(list.get(i), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        C3072b bVar = this.f6699a.get(str);
        if (bVar != null) {
            C3082f.m7631b("openSDK_LOG.JsBridge", "call----");
            bVar.call(str2, list, aVar);
            return;
        }
        C3082f.m7631b("openSDK_LOG.JsBridge", "not call----objName NOT FIND");
        if (aVar != null) {
            aVar.mo35070a();
        }
    }

    /* renamed from: a */
    public boolean mo35069a(WebView webView, String str) {
        C3082f.m7628a("openSDK_LOG.JsBridge", "-->canHandleUrl---url = " + str);
        if (str == null || !Uri.parse(str).getScheme().equals("jsbridge")) {
            return false;
        }
        ArrayList arrayList = new ArrayList(Arrays.asList((str + "/#").split("/")));
        if (arrayList.size() < 6) {
            return false;
        }
        List subList = arrayList.subList(4, arrayList.size() - 1);
        C3071a aVar = new C3071a(webView, 4, str);
        webView.getUrl();
        mo35068a((String) arrayList.get(2), (String) arrayList.get(3), subList, aVar);
        return true;
    }
}
