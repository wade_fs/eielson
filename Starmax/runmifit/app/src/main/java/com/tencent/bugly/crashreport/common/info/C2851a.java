package com.tencent.bugly.crashreport.common.info;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.os.Process;
import com.baidu.mobstat.Config;
import com.tencent.bugly.C2797b;
import com.tencent.bugly.crashreport.C2839a;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/* renamed from: com.tencent.bugly.crashreport.common.info.a */
/* compiled from: BUGLY */
public class C2851a {

    /* renamed from: an */
    private static C2851a f5598an;

    /* renamed from: A */
    public boolean f5599A = false;

    /* renamed from: B */
    public String f5600B = null;

    /* renamed from: C */
    public String f5601C = null;

    /* renamed from: D */
    public String f5602D = null;

    /* renamed from: E */
    public boolean f5603E = false;

    /* renamed from: F */
    public boolean f5604F = false;

    /* renamed from: G */
    public Boolean f5605G = null;

    /* renamed from: H */
    public Boolean f5606H = null;

    /* renamed from: I */
    public HashMap<String, String> f5607I = new HashMap<>();

    /* renamed from: J */
    public boolean f5608J = true;

    /* renamed from: K */
    public List<String> f5609K = new ArrayList();

    /* renamed from: L */
    public boolean f5610L = false;

    /* renamed from: M */
    public C2839a f5611M = null;

    /* renamed from: N */
    public SharedPreferences f5612N;

    /* renamed from: O */
    private final Context f5613O;

    /* renamed from: P */
    private String f5614P;

    /* renamed from: Q */
    private String f5615Q;

    /* renamed from: R */
    private String f5616R = "unknown";

    /* renamed from: S */
    private String f5617S = "unknown";

    /* renamed from: T */
    private String f5618T = "";

    /* renamed from: U */
    private String f5619U = null;

    /* renamed from: V */
    private String f5620V = null;

    /* renamed from: W */
    private String f5621W = null;

    /* renamed from: X */
    private String f5622X = null;

    /* renamed from: Y */
    private long f5623Y = -1;

    /* renamed from: Z */
    private long f5624Z = -1;

    /* renamed from: a */
    public final long f5625a = System.currentTimeMillis();

    /* renamed from: aA */
    private final Object f5626aA = new Object();

    /* renamed from: aB */
    private final Object f5627aB = new Object();

    /* renamed from: aC */
    private final Object f5628aC = new Object();

    /* renamed from: aD */
    private final Object f5629aD = new Object();

    /* renamed from: aE */
    private final Object f5630aE = new Object();

    /* renamed from: aF */
    private final Object f5631aF = new Object();

    /* renamed from: aa */
    private long f5632aa = -1;

    /* renamed from: ab */
    private String f5633ab = null;

    /* renamed from: ac */
    private String f5634ac = null;

    /* renamed from: ad */
    private Map<String, PlugInBean> f5635ad = null;

    /* renamed from: ae */
    private boolean f5636ae = true;

    /* renamed from: af */
    private String f5637af = null;

    /* renamed from: ag */
    private String f5638ag = null;

    /* renamed from: ah */
    private Boolean f5639ah = null;

    /* renamed from: ai */
    private String f5640ai = null;

    /* renamed from: aj */
    private String f5641aj = null;

    /* renamed from: ak */
    private String f5642ak = null;

    /* renamed from: al */
    private Map<String, PlugInBean> f5643al = null;

    /* renamed from: am */
    private Map<String, PlugInBean> f5644am = null;

    /* renamed from: ao */
    private int f5645ao = -1;

    /* renamed from: ap */
    private int f5646ap = -1;

    /* renamed from: aq */
    private Map<String, String> f5647aq = new HashMap();

    /* renamed from: ar */
    private Map<String, String> f5648ar = new HashMap();

    /* renamed from: as */
    private Map<String, String> f5649as = new HashMap();

    /* renamed from: at */
    private boolean f5650at = true;

    /* renamed from: au */
    private String f5651au = null;

    /* renamed from: av */
    private String f5652av = null;

    /* renamed from: aw */
    private String f5653aw = null;

    /* renamed from: ax */
    private String f5654ax = null;

    /* renamed from: ay */
    private String f5655ay = null;

    /* renamed from: az */
    private final Object f5656az = new Object();

    /* renamed from: b */
    public final String f5657b;

    /* renamed from: c */
    public final byte f5658c;

    /* renamed from: d */
    public String f5659d;

    /* renamed from: e */
    public final String f5660e;

    /* renamed from: f */
    public String f5661f;

    /* renamed from: g */
    public String f5662g;

    /* renamed from: h */
    public boolean f5663h = true;

    /* renamed from: i */
    public final String f5664i = "com.tencent.bugly";

    /* renamed from: j */
    public final String f5665j = "2.6.5";

    /* renamed from: k */
    public final String f5666k = "";

    /* renamed from: l */
    public final String f5667l;

    /* renamed from: m */
    public final String f5668m;

    /* renamed from: n */
    public final String f5669n;

    /* renamed from: o */
    public long f5670o = 0;

    /* renamed from: p */
    public String f5671p = null;

    /* renamed from: q */
    public String f5672q = null;

    /* renamed from: r */
    public String f5673r = null;

    /* renamed from: s */
    public String f5674s = null;

    /* renamed from: t */
    public String f5675t = null;

    /* renamed from: u */
    public List<String> f5676u = null;

    /* renamed from: v */
    public String f5677v = "unknown";

    /* renamed from: w */
    public long f5678w = 0;

    /* renamed from: x */
    public long f5679x = 0;

    /* renamed from: y */
    public long f5680y = 0;

    /* renamed from: z */
    public long f5681z = 0;

    /* renamed from: c */
    public String mo34300c() {
        return "2.6.5";
    }

    private C2851a(Context context) {
        this.f5613O = C2908aq.m6891a(context);
        this.f5658c = 1;
        m6472b(context);
        this.f5659d = AppInfo.m6455a(context);
        this.f5660e = AppInfo.m6456a(context, Process.myPid());
        this.f5667l = C2852b.m6559m();
        this.f5668m = C2852b.m6533a();
        this.f5672q = AppInfo.m6461c(context);
        this.f5669n = "Android " + C2852b.m6536b() + ",level " + C2852b.m6538c();
        StringBuilder sb = new StringBuilder();
        sb.append(this.f5668m);
        sb.append(";");
        sb.append(this.f5669n);
        this.f5657b = sb.toString();
        m6473c(context);
        try {
            if (!context.getDatabasePath("bugly_db_").exists()) {
                this.f5604F = true;
                C2903an.m6863c("App is first time to be installed on the device.", new Object[0]);
            }
        } catch (Throwable th) {
            if (C2797b.f5303c) {
                th.printStackTrace();
            }
        }
        this.f5612N = C2908aq.m6892a("BUGLY_COMMON_VALUES", context);
        C2903an.m6863c("com info create end", new Object[0]);
    }

    /* renamed from: b */
    private void m6472b(Context context) {
        PackageInfo b = AppInfo.m6460b(context);
        if (b != null) {
            try {
                this.f5671p = b.versionName;
                this.f5600B = this.f5671p;
                this.f5601C = Integer.toString(b.versionCode);
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: c */
    private void m6473c(Context context) {
        Map<String, String> d = AppInfo.m6462d(context);
        if (d != null) {
            try {
                this.f5676u = AppInfo.m6458a(d);
                String str = d.get("BUGLY_APPID");
                if (str != null) {
                    this.f5638ag = str;
                    mo34302c("APP_ID", this.f5638ag);
                }
                String str2 = d.get("BUGLY_APP_VERSION");
                if (str2 != null) {
                    this.f5671p = str2;
                }
                String str3 = d.get("BUGLY_APP_CHANNEL");
                if (str3 != null) {
                    this.f5673r = str3;
                }
                String str4 = d.get("BUGLY_ENABLE_DEBUG");
                if (str4 != null) {
                    this.f5599A = str4.equalsIgnoreCase("true");
                }
                String str5 = d.get("com.tencent.rdm.uuid");
                if (str5 != null) {
                    this.f5602D = str5;
                }
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    public boolean mo34295a() {
        return this.f5650at;
    }

    /* renamed from: a */
    public void mo34294a(boolean z) {
        this.f5650at = z;
        C2839a aVar = this.f5611M;
        if (aVar != null) {
            aVar.setNativeIsAppForeground(z);
        }
    }

    /* renamed from: a */
    public static synchronized C2851a m6470a(Context context) {
        C2851a aVar;
        synchronized (C2851a.class) {
            if (f5598an == null) {
                f5598an = new C2851a(context);
            }
            aVar = f5598an;
        }
        return aVar;
    }

    /* renamed from: b */
    public static synchronized C2851a m6471b() {
        C2851a aVar;
        synchronized (C2851a.class) {
            aVar = f5598an;
        }
        return aVar;
    }

    /* renamed from: d */
    public void mo34303d() {
        synchronized (this.f5656az) {
            this.f5614P = UUID.randomUUID().toString();
        }
    }

    /* renamed from: e */
    public String mo34305e() {
        String str;
        synchronized (this.f5656az) {
            if (this.f5614P == null) {
                mo34303d();
            }
            str = this.f5614P;
        }
        return str;
    }

    /* renamed from: f */
    public String mo34307f() {
        if (!C2908aq.m6915a(this.f5661f)) {
            return this.f5661f;
        }
        return this.f5638ag;
    }

    /* renamed from: a */
    public void mo34292a(String str) {
        this.f5638ag = str;
        mo34302c("APP_ID", str);
    }

    /* renamed from: g */
    public String mo34309g() {
        String str;
        synchronized (this.f5630aE) {
            str = this.f5616R;
        }
        return str;
    }

    /* renamed from: b */
    public void mo34297b(String str) {
        synchronized (this.f5630aE) {
            if (str == null) {
                str = "10000";
            }
            this.f5616R = "" + str;
        }
    }

    /* renamed from: b */
    public void mo34299b(boolean z) {
        this.f5636ae = z;
    }

    /* renamed from: h */
    public String mo34311h() {
        String str = this.f5615Q;
        if (str != null) {
            return str;
        }
        this.f5615Q = mo34314k() + "|" + mo34316m() + "|" + mo34317n();
        return this.f5615Q;
    }

    /* renamed from: c */
    public void mo34301c(String str) {
        this.f5615Q = str;
        synchronized (this.f5631aF) {
            this.f5648ar.put("E8", str);
        }
    }

    /* renamed from: i */
    public synchronized String mo34312i() {
        return this.f5617S;
    }

    /* renamed from: d */
    public synchronized void mo34304d(String str) {
        this.f5617S = "" + str;
    }

    /* renamed from: j */
    public synchronized String mo34313j() {
        return this.f5618T;
    }

    /* renamed from: e */
    public synchronized void mo34306e(String str) {
        this.f5618T = "" + str;
    }

    /* renamed from: k */
    public String mo34314k() {
        if (!this.f5636ae) {
            return "";
        }
        if (this.f5619U == null) {
            this.f5619U = C2852b.m6534a(this.f5613O);
        }
        return this.f5619U;
    }

    /* renamed from: l */
    public String mo34315l() {
        if (!this.f5636ae) {
            return "";
        }
        String str = this.f5620V;
        if (str == null || !str.contains(Config.TRACE_TODAY_VISIT_SPLIT)) {
            this.f5620V = C2852b.m6541d(this.f5613O);
        }
        return this.f5620V;
    }

    /* renamed from: m */
    public String mo34316m() {
        if (!this.f5636ae) {
            return "";
        }
        if (this.f5621W == null) {
            this.f5621W = C2852b.m6537b(this.f5613O);
        }
        return this.f5621W;
    }

    /* renamed from: n */
    public String mo34317n() {
        if (!this.f5636ae) {
            return "";
        }
        if (this.f5622X == null) {
            this.f5622X = C2852b.m6539c(this.f5613O);
        }
        return this.f5622X;
    }

    /* renamed from: o */
    public long mo34318o() {
        if (this.f5623Y <= 0) {
            this.f5623Y = C2852b.m6544f();
        }
        return this.f5623Y;
    }

    /* renamed from: p */
    public long mo34319p() {
        if (this.f5624Z <= 0) {
            this.f5624Z = C2852b.m6548h();
        }
        return this.f5624Z;
    }

    /* renamed from: q */
    public long mo34320q() {
        if (this.f5632aa <= 0) {
            this.f5632aa = C2852b.m6552j();
        }
        return this.f5632aa;
    }

    /* renamed from: r */
    public String mo34321r() {
        if (this.f5633ab == null) {
            this.f5633ab = C2852b.m6535a(this.f5613O, true);
        }
        return this.f5633ab;
    }

    /* renamed from: s */
    public String mo34322s() {
        if (this.f5634ac == null) {
            this.f5634ac = C2852b.m6549h(this.f5613O);
        }
        return this.f5634ac;
    }

    /* renamed from: a */
    public void mo34293a(String str, String str2) {
        if (str != null && str2 != null) {
            synchronized (this.f5626aA) {
                this.f5607I.put(str, str2);
            }
        }
    }

    /* renamed from: t */
    public String mo34323t() {
        try {
            Map<String, ?> all = this.f5613O.getSharedPreferences("BuglySdkInfos", 0).getAll();
            if (!all.isEmpty()) {
                synchronized (this.f5626aA) {
                    for (Map.Entry entry : all.entrySet()) {
                        try {
                            this.f5607I.put(entry.getKey(), entry.getValue().toString());
                        } catch (Throwable th) {
                            C2903an.m6858a(th);
                        }
                    }
                }
            }
        } catch (Throwable th2) {
            C2903an.m6858a(th2);
        }
        if (this.f5607I.isEmpty()) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry2 : this.f5607I.entrySet()) {
            sb.append("[");
            sb.append((String) entry2.getKey());
            sb.append(",");
            sb.append((String) entry2.getValue());
            sb.append("] ");
        }
        mo34302c("SDK_INFO", sb.toString());
        return sb.toString();
    }

    /* renamed from: u */
    public String mo34324u() {
        if (this.f5655ay == null) {
            this.f5655ay = AppInfo.m6463e(this.f5613O);
        }
        return this.f5655ay;
    }

    /* renamed from: v */
    public synchronized Map<String, PlugInBean> mo34325v() {
        if (this.f5635ad != null) {
            if (this.f5635ad.size() > 0) {
                HashMap hashMap = new HashMap(this.f5635ad.size());
                hashMap.putAll(this.f5635ad);
                return hashMap;
            }
        }
        return null;
    }

    /* renamed from: w */
    public String mo34326w() {
        if (this.f5637af == null) {
            this.f5637af = C2852b.m6556l();
        }
        return this.f5637af;
    }

    /* renamed from: x */
    public Boolean mo34327x() {
        if (this.f5639ah == null) {
            this.f5639ah = Boolean.valueOf(C2852b.m6560n());
        }
        return this.f5639ah;
    }

    /* renamed from: y */
    public String mo34328y() {
        if (this.f5640ai == null) {
            this.f5640ai = "" + C2852b.m6547g(this.f5613O);
            C2903an.m6857a("ROM ID: %s", this.f5640ai);
        }
        return this.f5640ai;
    }

    /* renamed from: z */
    public String mo34329z() {
        if (this.f5641aj == null) {
            this.f5641aj = "" + C2852b.m6542e(this.f5613O);
            C2903an.m6857a("SIM serial number: %s", this.f5641aj);
        }
        return this.f5641aj;
    }

    /* renamed from: A */
    public String mo34271A() {
        if (this.f5642ak == null) {
            this.f5642ak = "" + C2852b.m6540d();
            C2903an.m6857a("Hardware serial number: %s", this.f5642ak);
        }
        return this.f5642ak;
    }

    /* renamed from: B */
    public Map<String, String> mo34272B() {
        synchronized (this.f5627aB) {
            if (this.f5647aq.size() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap(this.f5647aq);
            return hashMap;
        }
    }

    /* renamed from: f */
    public String mo34308f(String str) {
        String remove;
        if (C2908aq.m6915a(str)) {
            C2903an.m6864d("key should not be empty %s", "" + str);
            return null;
        }
        synchronized (this.f5627aB) {
            remove = this.f5647aq.remove(str);
        }
        return remove;
    }

    /* renamed from: C */
    public void mo34273C() {
        synchronized (this.f5627aB) {
            this.f5647aq.clear();
        }
    }

    /* renamed from: g */
    public String mo34310g(String str) {
        String str2;
        if (C2908aq.m6915a(str)) {
            C2903an.m6864d("key should not be empty %s", "" + str);
            return null;
        }
        synchronized (this.f5627aB) {
            str2 = this.f5647aq.get(str);
        }
        return str2;
    }

    /* renamed from: b */
    public void mo34298b(String str, String str2) {
        if (C2908aq.m6915a(str) || C2908aq.m6915a(str2)) {
            C2903an.m6864d("key&value should not be empty %s %s", "" + str, "" + str2);
            return;
        }
        synchronized (this.f5627aB) {
            this.f5647aq.put(str, str2);
        }
    }

    /* renamed from: D */
    public int mo34274D() {
        int size;
        synchronized (this.f5627aB) {
            size = this.f5647aq.size();
        }
        return size;
    }

    /* renamed from: E */
    public Set<String> mo34275E() {
        Set<String> keySet;
        synchronized (this.f5627aB) {
            keySet = this.f5647aq.keySet();
        }
        return keySet;
    }

    /* renamed from: F */
    public Map<String, String> mo34276F() {
        synchronized (this.f5631aF) {
            if (this.f5648ar.size() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap(this.f5648ar);
            return hashMap;
        }
    }

    /* renamed from: c */
    public void mo34302c(String str, String str2) {
        if (C2908aq.m6915a(str) || C2908aq.m6915a(str2)) {
            C2903an.m6864d("server key&value should not be empty %s %s", "" + str, "" + str2);
            return;
        }
        synchronized (this.f5628aC) {
            this.f5649as.put(str, str2);
        }
    }

    /* renamed from: G */
    public Map<String, String> mo34277G() {
        synchronized (this.f5628aC) {
            if (this.f5649as.size() <= 0) {
                return null;
            }
            HashMap hashMap = new HashMap(this.f5649as);
            return hashMap;
        }
    }

    /* renamed from: a */
    public void mo34291a(int i) {
        synchronized (this.f5629aD) {
            int i2 = this.f5645ao;
            if (i2 != i) {
                this.f5645ao = i;
                C2903an.m6857a("user scene tag %d changed to tag %d", Integer.valueOf(i2), Integer.valueOf(this.f5645ao));
            }
        }
    }

    /* renamed from: H */
    public int mo34278H() {
        int i;
        synchronized (this.f5629aD) {
            i = this.f5645ao;
        }
        return i;
    }

    /* renamed from: b */
    public void mo34296b(int i) {
        int i2 = this.f5646ap;
        if (i2 != i) {
            this.f5646ap = i;
            C2903an.m6857a("server scene tag %d changed to tag %d", Integer.valueOf(i2), Integer.valueOf(this.f5646ap));
        }
    }

    /* renamed from: I */
    public int mo34279I() {
        return this.f5646ap;
    }

    /* renamed from: J */
    public synchronized Map<String, PlugInBean> mo34280J() {
        Map<String, PlugInBean> map;
        map = this.f5643al;
        if (this.f5644am != null) {
            map.putAll(this.f5644am);
        }
        return map;
    }

    /* renamed from: K */
    public int mo34281K() {
        return C2852b.m6538c();
    }

    /* renamed from: L */
    public String mo34282L() {
        if (this.f5651au == null) {
            this.f5651au = C2852b.m6562o();
        }
        return this.f5651au;
    }

    /* renamed from: M */
    public String mo34283M() {
        if (this.f5652av == null) {
            this.f5652av = C2852b.m6551i(this.f5613O);
        }
        return this.f5652av;
    }

    /* renamed from: N */
    public String mo34284N() {
        if (this.f5653aw == null) {
            this.f5653aw = C2852b.m6553j(this.f5613O);
        }
        return this.f5653aw;
    }

    /* renamed from: O */
    public String mo34285O() {
        return C2852b.m6555k(this.f5613O);
    }

    /* renamed from: P */
    public String mo34286P() {
        if (this.f5654ax == null) {
            this.f5654ax = C2852b.m6557l(this.f5613O);
        }
        return this.f5654ax;
    }

    /* renamed from: Q */
    public long mo34287Q() {
        return C2852b.m6558m(this.f5613O);
    }

    /* renamed from: R */
    public boolean mo34288R() {
        if (this.f5605G == null) {
            this.f5605G = Boolean.valueOf(C2852b.m6561n(this.f5613O));
            C2903an.m6857a("Is it a virtual machine? " + this.f5605G, new Object[0]);
        }
        return this.f5605G.booleanValue();
    }

    /* renamed from: S */
    public boolean mo34289S() {
        if (this.f5606H == null) {
            this.f5606H = Boolean.valueOf(C2852b.m6565p(this.f5613O));
            C2903an.m6857a("Does it has hook frame? " + this.f5606H, new Object[0]);
        }
        return this.f5606H.booleanValue();
    }

    /* renamed from: T */
    public String mo34290T() {
        if (this.f5662g == null) {
            this.f5662g = AppInfo.m6465g(this.f5613O);
            C2903an.m6857a("Beacon channel " + this.f5662g, new Object[0]);
        }
        return this.f5662g;
    }
}
