package com.tencent.connect.auth;

import com.tencent.tauth.IUiListener;
import java.util.HashMap;

/* renamed from: com.tencent.connect.auth.b */
/* compiled from: ProGuard */
public class C2981b {

    /* renamed from: a */
    public static C2981b f6384a;

    /* renamed from: d */
    static final /* synthetic */ boolean f6385d = (!C2981b.class.desiredAssertionStatus());

    /* renamed from: e */
    private static int f6386e = 0;

    /* renamed from: b */
    public HashMap<String, C2982a> f6387b = new HashMap<>();

    /* renamed from: c */
    public final String f6388c = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    /* renamed from: com.tencent.connect.auth.b$a */
    /* compiled from: ProGuard */
    public static class C2982a {

        /* renamed from: a */
        public IUiListener f6389a;

        /* renamed from: b */
        public C2970a f6390b;

        /* renamed from: c */
        public String f6391c;
    }

    /* renamed from: a */
    public static C2981b m7228a() {
        if (f6384a == null) {
            f6384a = new C2981b();
        }
        return f6384a;
    }

    /* renamed from: b */
    public static int m7229b() {
        int i = f6386e + 1;
        f6386e = i;
        return i;
    }

    /* renamed from: a */
    public String mo34749a(C2982a aVar) {
        int b = m7229b();
        try {
            HashMap<String, C2982a> hashMap = this.f6387b;
            hashMap.put("" + b, aVar);
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return "" + b;
    }

    /* renamed from: c */
    public String mo34750c() {
        int ceil = (int) Math.ceil((Math.random() * 20.0d) + 3.0d);
        char[] charArray = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".toCharArray();
        int length = charArray.length;
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < ceil; i++) {
            double random = Math.random();
            double d = (double) length;
            Double.isNaN(d);
            stringBuffer.append(charArray[(int) (random * d)]);
        }
        return stringBuffer.toString();
    }
}
