package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.k */
public class C3247k extends Event {

    /* renamed from: a */
    double f7383a = 0.0d;

    /* renamed from: s */
    String f7384s;

    /* renamed from: t */
    String f7385t;

    public C3247k(Context context, String str, String str2, int i, double d, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7385t = str;
        this.f7384s = str2;
        this.f7383a = d;
    }

    public EventType getType() {
        return EventType.PAGE_VIEW;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        Util.jsonPut(jSONObject, "pi", this.f7384s);
        Util.jsonPut(jSONObject, "rf", this.f7385t);
        double d = this.f7383a;
        if (d < 0.0d) {
            return true;
        }
        jSONObject.put("du", d);
        return true;
    }
}
