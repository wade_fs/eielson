package com.tencent.open.p061c;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.widget.RelativeLayout;

/* renamed from: com.tencent.open.c.a */
/* compiled from: ProGuard */
public class C3107a extends RelativeLayout {

    /* renamed from: a */
    private static final String f6810a = C3107a.class.getName();

    /* renamed from: b */
    private Rect f6811b = null;

    /* renamed from: c */
    private boolean f6812c = false;

    /* renamed from: d */
    private C3108a f6813d = null;

    /* renamed from: com.tencent.open.c.a$a */
    /* compiled from: ProGuard */
    public interface C3108a {
        /* renamed from: a */
        void mo35147a();

        /* renamed from: a */
        void mo35148a(int i);
    }

    public C3107a(Context context) {
        super(context);
        if (this.f6811b == null) {
            this.f6811b = new Rect();
        }
    }

    /* renamed from: a */
    public void mo35155a(C3108a aVar) {
        this.f6813d = aVar;
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int size = View.MeasureSpec.getSize(i2);
        Activity activity = (Activity) getContext();
        activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(this.f6811b);
        int height = (activity.getWindowManager().getDefaultDisplay().getHeight() - this.f6811b.top) - size;
        C3108a aVar = this.f6813d;
        if (!(aVar == null || size == 0)) {
            if (height > 100) {
                aVar.mo35148a((Math.abs(this.f6811b.height()) - getPaddingBottom()) - getPaddingTop());
            } else {
                aVar.mo35147a();
            }
        }
        super.onMeasure(i, i2);
    }
}
