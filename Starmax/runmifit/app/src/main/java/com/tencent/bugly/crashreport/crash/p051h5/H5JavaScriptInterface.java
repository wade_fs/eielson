package com.tencent.bugly.crashreport.crash.p051h5;

import android.webkit.JavascriptInterface;
import com.baidu.mobstat.Config;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.bugly.crashreport.inner.InnerApi;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: com.tencent.bugly.crashreport.crash.h5.H5JavaScriptInterface */
/* compiled from: BUGLY */
public class H5JavaScriptInterface {

    /* renamed from: a */
    private static HashSet<Integer> f5877a = new HashSet<>();

    /* renamed from: b */
    private String f5878b = null;

    /* renamed from: c */
    private Thread f5879c = null;

    /* renamed from: d */
    private String f5880d = null;

    /* renamed from: e */
    private Map<String, String> f5881e = null;

    private H5JavaScriptInterface() {
    }

    public static H5JavaScriptInterface getInstance(CrashReport.WebViewInterface webViewInterface) {
        if (webViewInterface == null || f5877a.contains(Integer.valueOf(webViewInterface.hashCode()))) {
            return null;
        }
        H5JavaScriptInterface h5JavaScriptInterface = new H5JavaScriptInterface();
        f5877a.add(Integer.valueOf(webViewInterface.hashCode()));
        h5JavaScriptInterface.f5879c = Thread.currentThread();
        h5JavaScriptInterface.f5880d = m6699a(h5JavaScriptInterface.f5879c);
        h5JavaScriptInterface.f5881e = m6700a(webViewInterface);
        return h5JavaScriptInterface;
    }

    /* renamed from: a */
    private static String m6699a(Thread thread) {
        if (thread == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        for (int i = 2; i < thread.getStackTrace().length; i++) {
            StackTraceElement stackTraceElement = thread.getStackTrace()[i];
            if (!stackTraceElement.toString().contains("crashreport")) {
                sb.append(stackTraceElement.toString());
                sb.append("\n");
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    private static Map<String, String> m6700a(CrashReport.WebViewInterface webViewInterface) {
        HashMap hashMap = new HashMap();
        hashMap.put("[WebView] ContentDescription", "" + ((Object) webViewInterface.getContentDescription()));
        return hashMap;
    }

    /* renamed from: a */
    private C2877a m6698a(String str) {
        if (str != null && str.length() > 0) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                C2877a aVar = new C2877a();
                aVar.f5882a = jSONObject.getString("projectRoot");
                if (aVar.f5882a == null) {
                    return null;
                }
                aVar.f5883b = jSONObject.getString("context");
                if (aVar.f5883b == null) {
                    return null;
                }
                aVar.f5884c = jSONObject.getString("url");
                if (aVar.f5884c == null) {
                    return null;
                }
                aVar.f5885d = jSONObject.getString("userAgent");
                if (aVar.f5885d == null) {
                    return null;
                }
                aVar.f5886e = jSONObject.getString("language");
                if (aVar.f5886e == null) {
                    return null;
                }
                aVar.f5887f = jSONObject.getString(Config.FEED_LIST_NAME);
                if (aVar.f5887f != null) {
                    if (!aVar.f5887f.equals("null")) {
                        String string = jSONObject.getString("stacktrace");
                        if (string == null) {
                            return null;
                        }
                        int indexOf = string.indexOf("\n");
                        if (indexOf < 0) {
                            C2903an.m6864d("H5 crash stack's format is wrong!", new Object[0]);
                            return null;
                        }
                        aVar.f5889h = string.substring(indexOf + 1);
                        aVar.f5888g = string.substring(0, indexOf);
                        int indexOf2 = aVar.f5888g.indexOf(Config.TRACE_TODAY_VISIT_SPLIT);
                        if (indexOf2 > 0) {
                            aVar.f5888g = aVar.f5888g.substring(indexOf2 + 1);
                        }
                        aVar.f5890i = jSONObject.getString("file");
                        if (aVar.f5887f == null) {
                            return null;
                        }
                        aVar.f5891j = jSONObject.getLong("lineNumber");
                        if (aVar.f5891j < 0) {
                            return null;
                        }
                        aVar.f5892k = jSONObject.getLong("columnNumber");
                        if (aVar.f5892k < 0) {
                            return null;
                        }
                        C2903an.m6857a("H5 crash information is following: ", new Object[0]);
                        C2903an.m6857a("[projectRoot]: " + aVar.f5882a, new Object[0]);
                        C2903an.m6857a("[context]: " + aVar.f5883b, new Object[0]);
                        C2903an.m6857a("[url]: " + aVar.f5884c, new Object[0]);
                        C2903an.m6857a("[userAgent]: " + aVar.f5885d, new Object[0]);
                        C2903an.m6857a("[language]: " + aVar.f5886e, new Object[0]);
                        C2903an.m6857a("[name]: " + aVar.f5887f, new Object[0]);
                        C2903an.m6857a("[message]: " + aVar.f5888g, new Object[0]);
                        C2903an.m6857a("[stacktrace]: \n" + aVar.f5889h, new Object[0]);
                        C2903an.m6857a("[file]: " + aVar.f5890i, new Object[0]);
                        C2903an.m6857a("[lineNumber]: " + aVar.f5891j, new Object[0]);
                        C2903an.m6857a("[columnNumber]: " + aVar.f5892k, new Object[0]);
                        return aVar;
                    }
                }
                return null;
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    private static void m6701a(C2877a aVar, Thread thread, Map<String, String> map) {
        if (aVar != null) {
            InnerApi.postH5CrashAsync(thread, aVar.f5887f, aVar.f5888g, aVar.f5889h, map);
        }
    }

    @JavascriptInterface
    public void printLog(String str) {
        C2903an.m6864d("Log from js: %s", str);
    }

    @JavascriptInterface
    public void reportJSException(String str) {
        if (str == null) {
            C2903an.m6864d("Payload from JS is null.", new Object[0]);
            return;
        }
        String b = C2908aq.m6926b(str.getBytes());
        String str2 = this.f5878b;
        if (str2 == null || !str2.equals(b)) {
            this.f5878b = b;
            C2903an.m6864d("Handling JS exception ...", new Object[0]);
            C2877a a = m6698a(str);
            if (a == null) {
                C2903an.m6864d("Failed to parse payload.", new Object[0]);
                return;
            }
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            linkedHashMap.putAll(a.mo34438a());
            linkedHashMap.putAll(this.f5881e);
            linkedHashMap.put("Java Stack", this.f5880d);
            m6701a(a, this.f5879c, linkedHashMap);
            return;
        }
        C2903an.m6864d("Same payload from js. Please check whether you've injected bugly.js more than one times.", new Object[0]);
    }
}
