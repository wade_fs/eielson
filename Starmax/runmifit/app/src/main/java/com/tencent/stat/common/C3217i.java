package com.tencent.stat.common;

import android.content.Context;
import android.os.Environment;
import com.tamic.novate.util.FileUtil;
import java.io.File;

/* renamed from: com.tencent.stat.common.i */
public class C3217i {

    /* renamed from: a */
    private static long f7270a = -1;

    /* renamed from: a */
    public static long m8005a(Context context) {
        return m8006a(context, context.getPackageName());
    }

    /* renamed from: a */
    public static long m8006a(Context context, String str) {
        if (f7270a == -1) {
            File file = new File(Environment.getExternalStorageDirectory(), FileUtil.HIDDEN_PREFIX + str + ".mta_");
            if (!file.exists()) {
                C3211f.m7990a(context, file);
                file.setReadOnly();
            }
            f7270a = C3211f.m7991b(context, file);
        }
        if (f7270a == -1) {
            f7270a = 0;
        }
        return f7270a;
    }
}
