package com.tencent.stat.lifecycle;

import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;

public class MtaActivityLifeCycle {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static StatLogger f7430a = StatCommonHelper.getLogger();

    public static Boolean registerActivityLifecycleCallbacks(Application application, final MtaActivityLifecycleCallback mtaActivityLifecycleCallback) {
        if (!(application == null || mtaActivityLifecycleCallback == null || Build.VERSION.SDK_INT < 14)) {
            try {
                f7430a.mo35381d("............ start registerActivityLifecycleCallbacks.");
                application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                    /* class com.tencent.stat.lifecycle.MtaActivityLifeCycle.C32611 */

                    public void onActivityStopped(Activity activity) {
                        mtaActivityLifecycleCallback.onActivityStopped(activity);
                    }

                    public void onActivityStarted(Activity activity) {
                        mtaActivityLifecycleCallback.onActivityStarted(activity);
                    }

                    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                        mtaActivityLifecycleCallback.onActivitySaveInstanceState(activity, bundle);
                    }

                    public void onActivityResumed(Activity activity) {
                        StatLogger a = MtaActivityLifeCycle.f7430a;
                        a.mo35381d("onActivityResumed " + activity.getClass().getSimpleName());
                        mtaActivityLifecycleCallback.onActivityResumed(activity);
                    }

                    public void onActivityPaused(Activity activity) {
                        StatLogger a = MtaActivityLifeCycle.f7430a;
                        a.mo35381d("onActivityPaused " + activity.getClass().getSimpleName());
                        mtaActivityLifecycleCallback.onActivityPaused(activity);
                    }

                    public void onActivityDestroyed(Activity activity) {
                        mtaActivityLifecycleCallback.onActivityDestroyed(activity);
                    }

                    public void onActivityCreated(Activity activity, Bundle bundle) {
                        mtaActivityLifecycleCallback.onActivityCreated(activity, bundle);
                    }
                });
                f7430a.mo35381d("............ end registerActivityLifecycleCallbacks.");
                return true;
            } catch (Throwable th) {
                th.printStackTrace();
            }
        }
        return false;
    }
}
