package com.tencent.mid.api;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import com.tencent.mid.p054b.C3031g;
import com.tencent.mid.util.Util;

public class MidProvider extends ContentProvider {
    public static final int CMD_GET_PRIVATE_MID = 1;
    public static final int CMD_GET_PRIVATE_MID_ENTITY = 2;
    public static final int CMD_GET_PRIVATE_NEW_VERSION_MID_ENTITY = 3;
    public static final int CMD_INSERT_NEW_VERSION_MID_ENTITY = 10;
    public static final int CMD_INSERT_NEW_VERSION_MID_OLD_ENTITY = 11;

    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public boolean onCreate() {
        return false;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        String lastPathSegment = uri.getLastPathSegment();
        Context applicationContext = getContext().getApplicationContext();
        String packageName = applicationContext.getPackageName();
        Log.i("MID", packageName + ":MidProvider receive cmd:" + lastPathSegment);
        if (Util.isEmpty(lastPathSegment)) {
            return "-1";
        }
        try {
            int parseInt = Integer.parseInt(lastPathSegment);
            if (parseInt == 1) {
                MidEntity h = C3031g.m7437a(applicationContext).mo34925h();
                if (h != null) {
                    return h.getMid();
                }
                return null;
            } else if (parseInt == 2) {
                MidEntity h2 = C3031g.m7437a(applicationContext).mo34925h();
                if (h2 == null) {
                    return null;
                }
                h2.setImei("");
                h2.setImsi("");
                h2.setMac("");
                return h2.toString();
            } else if (parseInt != 3) {
                return "";
            } else {
                MidEntity c = C3031g.m7437a(applicationContext).mo34915c();
                if (c == null) {
                    return null;
                }
                c.setImei("");
                c.setImsi("");
                c.setMac("");
                return c.toString();
            }
        } catch (Throwable th) {
            th.printStackTrace();
            return "-2";
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.mid.b.g.a(com.tencent.mid.api.MidEntity, boolean):void
     arg types: [com.tencent.mid.api.MidEntity, int]
     candidates:
      com.tencent.mid.b.g.a(int, java.util.Map<java.lang.Integer, com.tencent.mid.b.f>):com.tencent.mid.api.MidEntity
      com.tencent.mid.b.g.a(java.util.List<java.lang.Integer>, java.util.Map<java.lang.Integer, com.tencent.mid.b.f>):com.tencent.mid.api.MidEntity
      com.tencent.mid.b.g.a(int, int):void
      com.tencent.mid.b.g.a(com.tencent.mid.api.MidEntity, boolean):void */
    public Uri insert(Uri uri, ContentValues contentValues) {
        String lastPathSegment = uri.getLastPathSegment();
        Context applicationContext = getContext().getApplicationContext();
        if (applicationContext == null) {
            return null;
        }
        String packageName = applicationContext.getPackageName();
        Log.i("MID", packageName + ":MidProvider receive cmd:" + lastPathSegment);
        if (Util.isEmpty(lastPathSegment)) {
            return null;
        }
        try {
            int parseInt = Integer.parseInt(lastPathSegment);
            if (parseInt == 10) {
                String asString = contentValues.getAsString("mid");
                Log.i("MID", packageName + ":MidProvider insert old receive cmd:" + MidService.getLocalMidOnly(getContext().getApplicationContext()));
                if (!Util.isMidValid(MidService.getLocalMidOnly(getContext().getApplicationContext()))) {
                    Log.i("MID", packageName + ":MidProvider old insert receive cmd:" + asString);
                    C3031g.m7437a(applicationContext).mo34914b(MidEntity.parse(asString), false);
                }
            } else if (parseInt == 11) {
                try {
                    String asString2 = contentValues.getAsString("mid");
                    Log.i("MID", packageName + ":MidProvider insert receive cmd:" + MidService.getLocalMidOnly(getContext().getApplicationContext()));
                    if (!Util.isMidValid(MidService.getLocalMidOnly(getContext().getApplicationContext()))) {
                        Log.i("MID", packageName + ":MidProvider insert receive cmd:" + asString2);
                        C3031g.m7437a(applicationContext).mo34909a(MidEntity.parse(asString2), false);
                    }
                } catch (Throwable unused) {
                }
            }
            return null;
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }
}
