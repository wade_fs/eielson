package com.tencent.connect.avatar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.View;

/* renamed from: com.tencent.connect.avatar.b */
/* compiled from: ProGuard */
public class C2994b extends View {

    /* renamed from: a */
    private Rect f6431a;

    /* renamed from: b */
    private Paint f6432b;

    public C2994b(Context context) {
        super(context);
        m7292b();
    }

    /* renamed from: b */
    private void m7292b() {
        this.f6432b = new Paint();
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        Rect a = mo34780a();
        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        this.f6432b.setStyle(Paint.Style.FILL);
        this.f6432b.setColor(Color.argb(100, 0, 0, 0));
        float f = (float) measuredWidth;
        Canvas canvas2 = canvas;
        float f2 = f;
        canvas2.drawRect(0.0f, 0.0f, f2, (float) a.top, this.f6432b);
        canvas2.drawRect(0.0f, (float) a.bottom, f2, (float) measuredHeight, this.f6432b);
        canvas.drawRect(0.0f, (float) a.top, (float) a.left, (float) a.bottom, this.f6432b);
        canvas.drawRect((float) a.right, (float) a.top, f, (float) a.bottom, this.f6432b);
        canvas.drawColor(Color.argb(100, 0, 0, 0));
        this.f6432b.setStyle(Paint.Style.STROKE);
        this.f6432b.setColor(-1);
        canvas.drawRect((float) a.left, (float) a.top, (float) (a.right - 1), (float) a.bottom, this.f6432b);
    }

    /* renamed from: a */
    public Rect mo34780a() {
        if (this.f6431a == null) {
            this.f6431a = new Rect();
            int measuredWidth = getMeasuredWidth();
            int measuredHeight = getMeasuredHeight();
            int min = Math.min(Math.min((measuredHeight - 60) - 80, measuredWidth), 640);
            int i = (measuredWidth - min) / 2;
            int i2 = (measuredHeight - min) / 2;
            this.f6431a.set(i, i2, i + min, min + i2);
        }
        return this.f6431a;
    }
}
