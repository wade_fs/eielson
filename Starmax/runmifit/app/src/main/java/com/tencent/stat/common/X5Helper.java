package com.tencent.stat.common;

import android.content.Context;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import org.json.JSONObject;

public class X5Helper {

    /* renamed from: a */
    private static StatLogger f7222a = StatCommonHelper.getLogger();

    /* renamed from: b */
    private static Bundle f7223b = null;

    /* renamed from: c */
    private static String f7224c = null;

    /* renamed from: a */
    private static Class<?> m7968a() {
        try {
            return Class.forName("com.tencent.smtt.sdk.WebView");
        } catch (ClassNotFoundException unused) {
            return null;
        }
    }

    public static String getX5UserAgent() {
        return f7224c;
    }

    public static Bundle getBundle() {
        return f7223b;
    }

    public static String getX5Guid() {
        Bundle bundle = f7223b;
        if (bundle != null) {
            return bundle.getString("guid");
        }
        return null;
    }

    public static String getQua2() {
        Bundle bundle = f7223b;
        if (bundle != null) {
            return bundle.getString("qua2");
        }
        return null;
    }

    public static String getLc() {
        Bundle bundle = f7223b;
        if (bundle != null) {
            return bundle.getString("lc");
        }
        return null;
    }

    public static void encodeX5(Context context, JSONObject jSONObject) {
        try {
            if (f7223b != null) {
                Util.jsonPut(jSONObject, "x5qua2", getQua2());
                Util.jsonPut(jSONObject, "x5ua", getX5UserAgent());
                encodeQGuid(context, jSONObject);
            }
        } catch (Throwable th) {
            f7222a.mo35396w(th);
        }
    }

    public static void encodeQGuid(Context context, JSONObject jSONObject) {
        try {
            if (f7223b != null) {
                Util.jsonPut(jSONObject, "x5guid", getX5Guid());
                Util.jsonPut(jSONObject, "x5lc", getLc());
                jSONObject.put("x5vc", getX5Version(context));
            }
        } catch (Throwable th) {
            f7222a.mo35396w(th);
        }
    }

    public static void initOnUiThread(Context context) {
        m7970a(context);
    }

    /* renamed from: a */
    private static void m7970a(Context context) {
        Class<?> a;
        try {
            if (Looper.myLooper() != Looper.getMainLooper()) {
                f7222a.warn("init X5 sdk failed, should init on UI thread, such as Application.onCreate() or MainActivity.onCreate()");
            } else if (f7223b == null && (a = m7968a()) != null) {
                m7971a(context, a);
            }
        } catch (Throwable th) {
            f7222a.mo35384e(th);
        }
    }

    public static int getX5Version(Context context) {
        try {
            Class<?> cls = Class.forName("com.tencent.smtt.sdk.QbSdk");
            return ((Integer) cls.getDeclaredMethod("getTbsVersion", Context.class).invoke(cls, context)).intValue();
        } catch (Throwable th) {
            f7222a.mo35396w(th);
            return 0;
        }
    }

    /* renamed from: a */
    private static void m7971a(Context context, Class<?> cls) {
        try {
            Object newInstance = cls.getConstructor(Context.class).newInstance(context);
            m7972a(newInstance);
            Field a = m7969a(newInstance, "com.tencent.smtt.export.external.interfaces.IX5WebViewBase");
            Object obj = a.get(newInstance);
            Log.e(StatConstants.LOG_TAG, "iX5WebViewBase:" + obj + ",webviewObject:" + newInstance + ",base:" + a);
            Method method = obj.getClass().getMethod("getX5WebViewExtension", new Class[0]);
            StringBuilder sb = new StringBuilder();
            sb.append("Method getX5WebViewExtension:");
            sb.append(method);
            Log.e(StatConstants.LOG_TAG, sb.toString());
            Object invoke = method.invoke(obj, new Object[0]);
            Log.e(StatConstants.LOG_TAG, "iX5WebViewExtension:" + invoke);
            f7223b = (Bundle) invoke.getClass().getMethod("getSdkQBStatisticsInfo", new Class[0]).invoke(invoke, new Object[0]);
            Log.e(StatConstants.LOG_TAG, "x5statisticsInfoBundle:" + f7223b);
        } catch (Throwable th) {
            f7222a.mo35396w(th);
        }
    }

    /* renamed from: a */
    private static void m7972a(Object obj) {
        try {
            Object invoke = obj.getClass().getDeclaredMethod("getSettings", new Class[0]).invoke(obj, new Object[0]);
            f7224c = (String) invoke.getClass().getMethod("getUserAgentString", new Class[0]).invoke(invoke, new Object[0]);
            Log.e(StatConstants.LOG_TAG, "x5UserAgent:" + f7224c);
        } catch (Throwable th) {
            f7222a.mo35396w(th);
        }
    }

    /* renamed from: a */
    private static Field m7969a(Object obj, String str) {
        for (Class<?> cls = obj.getClass(); cls != Object.class; cls = cls.getSuperclass()) {
            try {
                Field[] declaredFields = cls.getDeclaredFields();
                for (Field field : declaredFields) {
                    if (field.getType().getName().equals(str)) {
                        field.setAccessible(true);
                        return field;
                    }
                }
                continue;
            } catch (Exception e) {
                f7222a.mo35396w(e);
            }
        }
        return null;
    }
}
