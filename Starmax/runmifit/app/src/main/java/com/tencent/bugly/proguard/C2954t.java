package com.tencent.bugly.proguard;

import android.text.TextUtils;
import com.tencent.bugly.beta.download.BetaReceiver;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2807d;
import com.tencent.bugly.beta.p050ui.C2821c;
import com.tencent.bugly.beta.utils.C2836e;
import java.io.File;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: com.tencent.bugly.proguard.t */
/* compiled from: BUGLY */
public class C2954t extends DownloadTask implements Runnable {

    /* renamed from: k */
    public long f6242k = 0;

    /* renamed from: l */
    private File f6243l;

    /* renamed from: m */
    private long f6244m = 0;

    public C2954t(String str, String str2, long j, long j2, String str3) {
        super(str, "", "", str3);
        this.f6243l = new File(str2);
        this.f5312b = this.f6243l.getParent();
        this.f5313c = this.f6243l.getName();
        this.f5315e = j;
        this.f5316f = j2;
        getStatus();
    }

    public C2954t(String str, String str2, String str3, String str4) {
        super(str, str2, str3, str4);
        getStatus();
    }

    public File getSaveFile() {
        return this.f6243l;
    }

    public void download() {
        if (getStatus() == 1) {
            mo34680b();
        } else if (getStatus() != 2) {
            if (getSaveFile() == null || !getSaveFile().exists()) {
                this.f5315e = 0;
                this.f5316f = 0;
                this.f6242k = 0;
            } else {
                this.f5315e = getSaveFile().length();
            }
            if (this.f5317g) {
                C2821c.f5418a.mo34160a(super);
            }
            this.f6244m = System.currentTimeMillis();
            this.f5319i = 2;
            C2952s.f6238a.f6239b.put(getDownloadUrl(), this);
            C2952s.f6238a.mo34676a(this);
        }
    }

    public void delete(boolean z) {
        stop();
        if (z) {
            if (getSaveFile() != null && getSaveFile().exists() && !getSaveFile().isDirectory()) {
                getSaveFile().delete();
            }
            C2947p.f6229a.mo34669b(super);
        }
        BetaReceiver.netListeners.remove(getDownloadUrl());
        this.f5313c = null;
        this.f5315e = 0;
        this.f5316f = 0;
        this.f5319i = 4;
    }

    public void stop() {
        if (this.f5319i != 5) {
            this.f5319i = 3;
        }
    }

    public int getStatus() {
        if (getSaveFile() != null && getSaveFile().exists() && getSaveFile().length() == this.f5316f && !C2952s.f6238a.f6239b.contains(this)) {
            this.f5315e = this.f5316f;
            this.f5319i = 1;
        }
        if (getSaveFile() != null && getSaveFile().exists() && getSaveFile().length() > 0 && getSaveFile().length() < this.f5316f && !C2952s.f6238a.f6239b.contains(this)) {
            this.f5315e = getSaveFile().length();
            this.f5319i = 3;
        }
        if ((getSaveFile() == null || !getSaveFile().exists()) && !C2952s.f6238a.f6239b.contains(this)) {
            this.f5319i = 0;
        }
        return this.f5319i;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:107:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:108:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:109:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:110:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:111:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:113:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0094, code lost:
        mo34679a(2000, "tLen <= 0 ");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0099, code lost:
        if (r5 == null) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:?, code lost:
        r5.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x009e, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x00a4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00a5, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00dc, code lost:
        mo34680b();
        com.tencent.bugly.proguard.C2903an.m6865e("mSavedLength > mTotalLength,重新下载!", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00e9, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00ef, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f0, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0129, code lost:
        if (r6 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x012f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x0130, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:?, code lost:
        mo34680b();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x013b, code lost:
        r0 = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0178, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0179, code lost:
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x017b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:?, code lost:
        mo34679a(2000, r0.getMessage());
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0186, code lost:
        if (r6 != null) goto L_0x0188;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:88:0x018c, code lost:
        if (r6 != null) goto L_0x018e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0192, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:92:0x0193, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0197, code lost:
        throw r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x019e, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:98:0x019f, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Removed duplicated region for block: B:106:0x015e A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x00ce A[Catch:{ IOException -> 0x0140, all -> 0x013d }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x0135 A[EDGE_INSN: B:58:0x0135->B:59:? ?: BREAK  , SYNTHETIC, Splitter:B:58:0x0135] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x015b A[SYNTHETIC, Splitter:B:72:0x015b] */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x0163 A[Catch:{ Exception -> 0x017b, all -> 0x0178 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r18 = this;
            r1 = r18
            java.lang.Class<com.tencent.bugly.proguard.t> r2 = com.tencent.bugly.proguard.C2954t.class
            r3 = 0
            java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x01a4 }
            java.lang.String r0 = r18.getDownloadUrl()     // Catch:{ MalformedURLException -> 0x01a4 }
            r4.<init>(r0)     // Catch:{ MalformedURLException -> 0x01a4 }
            r0 = 0
            r5 = r0
            r6 = r5
            r0 = 0
        L_0x0012:
            r7 = 2000(0x7d0, float:2.803E-42)
            r8 = 3
            if (r0 >= r8) goto L_0x0167
            int r9 = r0 + 1
            java.net.URLConnection r0 = r4.openConnection()     // Catch:{ IOException -> 0x0145 }
            javax.net.ssl.HttpsURLConnection r0 = (javax.net.ssl.HttpsURLConnection) r0     // Catch:{ IOException -> 0x0145 }
            r10 = 5000(0x1388, float:7.006E-42)
            r0.setConnectTimeout(r10)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r10 = "GET"
            r0.setRequestMethod(r10)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r10 = "Referer"
            java.lang.String r11 = r4.toString()     // Catch:{ IOException -> 0x0145 }
            r0.setRequestProperty(r10, r11)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r10 = "Charset"
            java.lang.String r11 = "UTF-8"
            r0.setRequestProperty(r10, r11)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r10 = "Range"
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0145 }
            r11.<init>()     // Catch:{ IOException -> 0x0145 }
            java.lang.String r12 = "bytes="
            r11.append(r12)     // Catch:{ IOException -> 0x0145 }
            long r12 = r1.f5315e     // Catch:{ IOException -> 0x0145 }
            r11.append(r12)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r12 = "-"
            r11.append(r12)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r11 = r11.toString()     // Catch:{ IOException -> 0x0145 }
            r0.setRequestProperty(r10, r11)     // Catch:{ IOException -> 0x0145 }
            java.lang.String r10 = "Connection"
            java.lang.String r11 = "Keep-Alive"
            r0.setRequestProperty(r10, r11)     // Catch:{ IOException -> 0x0145 }
            r0.connect()     // Catch:{ IOException -> 0x0145 }
            java.lang.String r10 = r1.m7142a(r0)     // Catch:{ IOException -> 0x0145 }
            r1.f5313c = r10     // Catch:{ IOException -> 0x0145 }
            java.io.File r10 = new java.io.File     // Catch:{ IOException -> 0x0145 }
            java.lang.String r11 = r1.f5312b     // Catch:{ IOException -> 0x0145 }
            r10.<init>(r11)     // Catch:{ IOException -> 0x0145 }
            boolean r11 = r10.exists()     // Catch:{ IOException -> 0x0145 }
            if (r11 != 0) goto L_0x0076
            r10.mkdirs()     // Catch:{ IOException -> 0x0145 }
        L_0x0076:
            java.io.File r11 = new java.io.File     // Catch:{ IOException -> 0x0145 }
            java.lang.String r12 = r1.f5313c     // Catch:{ IOException -> 0x0145 }
            r11.<init>(r10, r12)     // Catch:{ IOException -> 0x0145 }
            r1.f6243l = r11     // Catch:{ IOException -> 0x0145 }
            long r10 = r1.f5316f     // Catch:{ IOException -> 0x0145 }
            r12 = 0
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 != 0) goto L_0x00aa
            int r10 = r0.getContentLength()     // Catch:{ IOException -> 0x0145 }
            long r10 = (long) r10     // Catch:{ IOException -> 0x0145 }
            r1.f5316f = r10     // Catch:{ IOException -> 0x0145 }
            long r10 = r1.f5316f     // Catch:{ IOException -> 0x0145 }
            int r14 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r14 > 0) goto L_0x00aa
            java.lang.String r0 = "tLen <= 0 "
            r1.mo34679a(r7, r0)     // Catch:{ IOException -> 0x0145 }
            if (r5 == 0) goto L_0x009e
            r5.close()     // Catch:{ Exception -> 0x017b }
        L_0x009e:
            if (r6 == 0) goto L_0x00a9
            r6.close()     // Catch:{ Exception -> 0x00a4 }
            goto L_0x00a9
        L_0x00a4:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
        L_0x00a9:
            return
        L_0x00aa:
            com.tencent.bugly.proguard.p r10 = com.tencent.bugly.proguard.C2947p.f6229a     // Catch:{ IOException -> 0x0145 }
            r10.mo34666a(r1)     // Catch:{ IOException -> 0x0145 }
            java.io.InputStream r6 = r0.getInputStream()     // Catch:{ IOException -> 0x0145 }
            r0 = 307200(0x4b000, float:4.30479E-40)
            byte[] r0 = new byte[r0]     // Catch:{ IOException -> 0x0145 }
            java.io.RandomAccessFile r10 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x0145 }
            java.io.File r11 = r1.f6243l     // Catch:{ IOException -> 0x0145 }
            java.lang.String r12 = "rwd"
            r10.<init>(r11, r12)     // Catch:{ IOException -> 0x0145 }
            long r11 = r1.f5315e     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r10.seek(r11)     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r5 = 0
        L_0x00c7:
            int r11 = r6.read(r0)     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r12 = -1
            if (r11 == r12) goto L_0x0135
            long r12 = r1.f5315e     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            long r14 = (long) r11     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            long r12 = r12 + r14
            r1.f5315e = r12     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            long r12 = r1.f5315e     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            long r14 = r1.f5316f     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            int r16 = (r12 > r14 ? 1 : (r12 == r14 ? 0 : -1))
            if (r16 <= 0) goto L_0x00f5
            r18.mo34680b()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            java.lang.String r0 = "mSavedLength > mTotalLength,重新下载!"
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            com.tencent.bugly.proguard.C2903an.m6865e(r0, r5)     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r10.close()     // Catch:{ Exception -> 0x017b }
            if (r6 == 0) goto L_0x00f4
            r6.close()     // Catch:{ Exception -> 0x00ef }
            goto L_0x00f4
        L_0x00ef:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
        L_0x00f4:
            return
        L_0x00f5:
            r12 = 1120403456(0x42c80000, float:100.0)
            long r13 = r1.f5315e     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            float r13 = (float) r13     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            long r14 = r1.f5316f     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            float r14 = (float) r14     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            float r13 = r13 / r14
            float r13 = r13 * r12
            float r12 = r13 - r5
            double r14 = (double) r12     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r16 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            int r12 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r12 < 0) goto L_0x010d
            r18.mo34678a()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r5 = r13
        L_0x010d:
            r10.write(r0, r3, r11)     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            java.io.File r11 = r18.getSaveFile()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            if (r11 == 0) goto L_0x0126
            java.io.File r11 = r18.getSaveFile()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            boolean r11 = r11.exists()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            if (r11 == 0) goto L_0x0126
            int r11 = r18.getStatus()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            if (r11 != r8) goto L_0x00c7
        L_0x0126:
            r10.close()     // Catch:{ Exception -> 0x017b }
            if (r6 == 0) goto L_0x0134
            r6.close()     // Catch:{ Exception -> 0x012f }
            goto L_0x0134
        L_0x012f:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
        L_0x0134:
            return
        L_0x0135:
            r18.mo34680b()     // Catch:{ IOException -> 0x0140, all -> 0x013d }
            r10.close()     // Catch:{ Exception -> 0x017b }
            r0 = r9
            goto L_0x0167
        L_0x013d:
            r0 = move-exception
            r5 = r10
            goto L_0x0161
        L_0x0140:
            r0 = move-exception
            r5 = r10
            goto L_0x0146
        L_0x0143:
            r0 = move-exception
            goto L_0x0161
        L_0x0145:
            r0 = move-exception
        L_0x0146:
            r0.printStackTrace()     // Catch:{ all -> 0x0143 }
            r8 = 2020(0x7e4, float:2.83E-42)
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x0143 }
            r1.mo34679a(r8, r0)     // Catch:{ all -> 0x0143 }
            java.lang.String r0 = "IOException,stop download!"
            java.lang.Object[] r8 = new java.lang.Object[r3]     // Catch:{ all -> 0x0143 }
            com.tencent.bugly.proguard.C2903an.m6859b(r2, r0, r8)     // Catch:{ all -> 0x0143 }
            if (r5 == 0) goto L_0x015e
            r5.close()     // Catch:{ Exception -> 0x017b }
        L_0x015e:
            r0 = r9
            goto L_0x0012
        L_0x0161:
            if (r5 == 0) goto L_0x0166
            r5.close()     // Catch:{ Exception -> 0x017b }
        L_0x0166:
            throw r0     // Catch:{ Exception -> 0x017b }
        L_0x0167:
            if (r0 < r8) goto L_0x0198
            java.lang.String r0 = "have retry %d times"
            r4 = 1
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x017b }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r8)     // Catch:{ Exception -> 0x017b }
            r4[r3] = r5     // Catch:{ Exception -> 0x017b }
            com.tencent.bugly.proguard.C2903an.m6859b(r2, r0, r4)     // Catch:{ Exception -> 0x017b }
            goto L_0x0198
        L_0x0178:
            r0 = move-exception
            r2 = r0
            goto L_0x018c
        L_0x017b:
            r0 = move-exception
            java.lang.String r2 = r0.getMessage()     // Catch:{ all -> 0x0178 }
            r1.mo34679a(r7, r2)     // Catch:{ all -> 0x0178 }
            r0.printStackTrace()     // Catch:{ all -> 0x0178 }
            if (r6 == 0) goto L_0x01a3
            r6.close()     // Catch:{ Exception -> 0x019e }
            goto L_0x01a3
        L_0x018c:
            if (r6 == 0) goto L_0x0197
            r6.close()     // Catch:{ Exception -> 0x0192 }
            goto L_0x0197
        L_0x0192:
            r0 = move-exception
            r3 = r0
            r3.printStackTrace()
        L_0x0197:
            throw r2
        L_0x0198:
            if (r6 == 0) goto L_0x01a3
            r6.close()     // Catch:{ Exception -> 0x019e }
            goto L_0x01a3
        L_0x019e:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
        L_0x01a3:
            return
        L_0x01a4:
            r0 = move-exception
            java.lang.String r2 = r0.getMessage()
            java.lang.Object[] r3 = new java.lang.Object[r3]
            com.tencent.bugly.proguard.C2903an.m6857a(r2, r3)
            r2 = 2010(0x7da, float:2.817E-42)
            java.lang.String r0 = r0.getMessage()
            r1.mo34679a(r2, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2954t.run():void");
    }

    /* renamed from: a */
    private String m7142a(HttpURLConnection httpURLConnection) {
        try {
            if (!TextUtils.isEmpty(this.f5313c)) {
                return this.f5313c;
            }
            Map<String, List<String>> headerFields = httpURLConnection.getHeaderFields();
            if (headerFields != null) {
                for (String str : headerFields.keySet()) {
                    if (str != null) {
                        List<String> list = headerFields.get(str);
                        if (list != null) {
                            for (String str2 : list) {
                                if (str2 != null) {
                                    if ("content-disposition".equals(str.toLowerCase())) {
                                        Matcher matcher = Pattern.compile(".*filename=(.*)").matcher(str2.toLowerCase());
                                        if (matcher.find()) {
                                            return matcher.group(1);
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                            }
                            continue;
                        }
                    }
                }
            }
            String substring = getDownloadUrl().substring(getDownloadUrl().lastIndexOf(47) + 1);
            if (!TextUtils.isEmpty(substring)) {
                return substring;
            }
            return UUID.randomUUID() + ".apk";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34679a(int i, String str) {
        this.f5319i = 5;
        C2821c.f5418a.mo34159a();
        C2952s.f6238a.f6239b.remove(getDownloadUrl());
        C2836e.m6398a(new C2807d(10, this.f5314d, this, Integer.valueOf(i), str));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34678a() {
        this.f6242k += System.currentTimeMillis() - this.f6244m;
        C2947p.f6229a.mo34666a(super);
        this.f6244m = System.currentTimeMillis();
        C2821c.f5418a.mo34159a();
        C2836e.m6398a(new C2807d(9, this.f5314d, this));
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo34680b() {
        this.f5319i = 1;
        mo34678a();
        C2952s.f6238a.f6239b.remove(getDownloadUrl());
        BetaReceiver.netListeners.remove(getDownloadUrl());
        C2836e.m6398a(new C2807d(8, this.f5314d, this));
    }

    public long getCostTime() {
        return this.f6242k;
    }
}
