package com.tencent.bugly.beta.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

/* renamed from: com.tencent.bugly.beta.utils.f */
/* compiled from: BUGLY */
public class C2837f {

    /* renamed from: a */
    private static Toast f5541a;

    /* renamed from: a */
    public static void m6401a(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            Toast toast = f5541a;
            if (toast == null) {
                f5541a = Toast.makeText(context, str, 0);
            } else {
                toast.setText(str);
            }
            f5541a.show();
        }
    }
}
