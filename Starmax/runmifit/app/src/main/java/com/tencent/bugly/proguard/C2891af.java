package com.tencent.bugly.proguard;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.tencent.bugly.BUGLY;
import com.tencent.bugly.crashreport.common.info.C2852b;
import java.io.File;
import java.util.List;

/* renamed from: com.tencent.bugly.proguard.af */
/* compiled from: BUGLY */
public class C2891af extends SQLiteOpenHelper {

    /* renamed from: a */
    public static String f5960a = "bugly_db";

    /* renamed from: b */
    public static int f5961b = 15;

    /* renamed from: c */
    protected Context f5962c;

    /* renamed from: d */
    private List<BUGLY> f5963d;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public C2891af(android.content.Context r4, java.util.List<com.tencent.bugly.BUGLY> r5) {
        /*
            r3 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = com.tencent.bugly.proguard.C2891af.f5960a
            r0.append(r1)
            java.lang.String r1 = "_"
            r0.append(r1)
            com.tencent.bugly.crashreport.common.info.a r1 = com.tencent.bugly.crashreport.common.info.C2851a.m6470a(r4)
            r1.getClass()
            java.lang.String r1 = ""
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            int r1 = com.tencent.bugly.proguard.C2891af.f5961b
            r2 = 0
            r3.<init>(r4, r0, r2, r1)
            r3.f5962c = r4
            r3.f5963d = r5
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2891af.<init>(android.content.Context, java.util.List):void");
    }

    public synchronized void onCreate(SQLiteDatabase sQLiteDatabase) {
        try {
            StringBuilder sb = new StringBuilder();
            sb.setLength(0);
            sb.append(" CREATE TABLE IF NOT EXISTS ");
            sb.append("t_ui");
            sb.append(" ( ");
            sb.append("_id");
            sb.append(" ");
            sb.append("INTEGER PRIMARY KEY");
            sb.append(" , ");
            sb.append("_tm");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_ut");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_tp");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_dt");
            sb.append(" ");
            sb.append("blob");
            sb.append(" , ");
            sb.append("_pc");
            sb.append(" ");
            sb.append("text");
            sb.append(" ) ");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
            sb.setLength(0);
            sb.append(" CREATE TABLE IF NOT EXISTS ");
            sb.append("t_lr");
            sb.append(" ( ");
            sb.append("_id");
            sb.append(" ");
            sb.append("INTEGER PRIMARY KEY");
            sb.append(" , ");
            sb.append("_tp");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_tm");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_pc");
            sb.append(" ");
            sb.append("text");
            sb.append(" , ");
            sb.append("_th");
            sb.append(" ");
            sb.append("text");
            sb.append(" , ");
            sb.append("_dt");
            sb.append(" ");
            sb.append("blob");
            sb.append(" ) ");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
            sb.setLength(0);
            sb.append(" CREATE TABLE IF NOT EXISTS ");
            sb.append("t_pf");
            sb.append(" ( ");
            sb.append("_id");
            sb.append(" ");
            sb.append("integer");
            sb.append(" , ");
            sb.append("_tp");
            sb.append(" ");
            sb.append("text");
            sb.append(" , ");
            sb.append("_tm");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_dt");
            sb.append(" ");
            sb.append("blob");
            sb.append(",primary key(");
            sb.append("_id");
            sb.append(",");
            sb.append("_tp");
            sb.append(" )) ");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
            sb.setLength(0);
            sb.append(" CREATE TABLE IF NOT EXISTS ");
            sb.append("t_cr");
            sb.append(" ( ");
            sb.append("_id");
            sb.append(" ");
            sb.append("INTEGER PRIMARY KEY");
            sb.append(" , ");
            sb.append("_tm");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_s1");
            sb.append(" ");
            sb.append("text");
            sb.append(" , ");
            sb.append("_up");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_me");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_uc");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_dt");
            sb.append(" ");
            sb.append("blob");
            sb.append(" ) ");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
            sb.setLength(0);
            sb.append(" CREATE TABLE IF NOT EXISTS ");
            sb.append("dl_1002");
            sb.append(" (");
            sb.append("_id");
            sb.append(" integer primary key autoincrement, ");
            sb.append("_dUrl");
            sb.append(" varchar(100), ");
            sb.append("_sFile");
            sb.append(" varchar(100), ");
            sb.append("_sLen");
            sb.append(" INTEGER, ");
            sb.append("_tLen");
            sb.append(" INTEGER, ");
            sb.append("_MD5");
            sb.append(" varchar(100), ");
            sb.append("_DLTIME");
            sb.append(" INTEGER)");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
            sb.setLength(0);
            sb.append("CREATE TABLE IF NOT EXISTS ");
            sb.append("ge_1002");
            sb.append(" (");
            sb.append("_id");
            sb.append(" integer primary key autoincrement, ");
            sb.append("_time");
            sb.append(" INTEGER, ");
            sb.append("_datas");
            sb.append(" blob)");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
            sb.setLength(0);
            sb.append(" CREATE TABLE IF NOT EXISTS ");
            sb.append("st_1002");
            sb.append(" ( ");
            sb.append("_id");
            sb.append(" ");
            sb.append("integer");
            sb.append(" , ");
            sb.append("_tp");
            sb.append(" ");
            sb.append("text");
            sb.append(" , ");
            sb.append("_tm");
            sb.append(" ");
            sb.append("int");
            sb.append(" , ");
            sb.append("_dt");
            sb.append(" ");
            sb.append("blob");
            sb.append(",primary key(");
            sb.append("_id");
            sb.append(",");
            sb.append("_tp");
            sb.append(" )) ");
            C2903an.m6863c(sb.toString(), new Object[0]);
            sQLiteDatabase.execSQL(sb.toString(), new String[0]);
        } catch (Throwable th) {
            if (!C2903an.m6861b(th)) {
                th.printStackTrace();
            }
        }
        if (this.f5963d != null) {
            for (BUGLY aVar : this.f5963d) {
                try {
                    aVar.onDbCreate(sQLiteDatabase);
                } catch (Throwable th2) {
                    if (!C2903an.m6861b(th2)) {
                        th2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized boolean mo34510a(SQLiteDatabase sQLiteDatabase) {
        try {
            String[] strArr = {"t_lr", "t_ui", "t_pf"};
            for (String str : strArr) {
                sQLiteDatabase.execSQL("DROP TABLE IF EXISTS " + str, new String[0]);
            }
        } catch (Throwable th) {
            if (!C2903an.m6861b(th)) {
                th.printStackTrace();
            }
            return false;
        }
        return true;
    }

    public synchronized void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        C2903an.m6864d("[Database] Upgrade %d to %d , drop tables!", Integer.valueOf(i), Integer.valueOf(i2));
        if (this.f5963d != null) {
            for (BUGLY aVar : this.f5963d) {
                try {
                    aVar.onDbUpgrade(sQLiteDatabase, i, i2);
                } catch (Throwable th) {
                    if (!C2903an.m6861b(th)) {
                        th.printStackTrace();
                    }
                }
            }
        }
        if (mo34510a(sQLiteDatabase)) {
            onCreate(sQLiteDatabase);
        } else {
            C2903an.m6864d("[Database] Failed to drop, delete db.", new Object[0]);
            File databasePath = this.f5962c.getDatabasePath(f5960a);
            if (databasePath != null && databasePath.canWrite()) {
                databasePath.delete();
            }
        }
    }

    public synchronized void onDowngrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
        if (C2852b.m6538c() >= 11) {
            C2903an.m6864d("[Database] Downgrade %d to %d drop tables.", Integer.valueOf(i), Integer.valueOf(i2));
            if (this.f5963d != null) {
                for (BUGLY aVar : this.f5963d) {
                    try {
                        aVar.onDbDowngrade(sQLiteDatabase, i, i2);
                    } catch (Throwable th) {
                        if (!C2903an.m6861b(th)) {
                            th.printStackTrace();
                        }
                    }
                }
            }
            if (mo34510a(sQLiteDatabase)) {
                onCreate(sQLiteDatabase);
            } else {
                C2903an.m6864d("[Database] Failed to drop, delete db.", new Object[0]);
                File databasePath = this.f5962c.getDatabasePath(f5960a);
                if (databasePath != null && databasePath.canWrite()) {
                    databasePath.delete();
                }
            }
        }
    }

    public synchronized SQLiteDatabase getReadableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        sQLiteDatabase = null;
        int i = 0;
        while (sQLiteDatabase == null && i < 5) {
            i++;
            try {
                sQLiteDatabase = super.getReadableDatabase();
            } catch (Throwable unused) {
                C2903an.m6864d("[Database] Try to get db(count: %d).", Integer.valueOf(i));
                if (i == 5) {
                    C2903an.m6865e("[Database] Failed to get db.", new Object[0]);
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        return sQLiteDatabase;
    }

    public synchronized SQLiteDatabase getWritableDatabase() {
        SQLiteDatabase sQLiteDatabase;
        sQLiteDatabase = null;
        int i = 0;
        while (sQLiteDatabase == null && i < 5) {
            i++;
            try {
                sQLiteDatabase = super.getWritableDatabase();
            } catch (Throwable unused) {
                C2903an.m6864d("[Database] Try to get db(count: %d).", Integer.valueOf(i));
                if (i == 5) {
                    C2903an.m6865e("[Database] Failed to get db.", new Object[0]);
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        if (sQLiteDatabase == null) {
            C2903an.m6864d("[Database] db error delay error record 1min.", new Object[0]);
        }
        return sQLiteDatabase;
    }
}
