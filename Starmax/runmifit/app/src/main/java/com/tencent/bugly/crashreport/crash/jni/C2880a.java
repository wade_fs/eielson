package com.tencent.bugly.crashreport.crash.jni;

import android.content.Context;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.info.C2852b;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.crash.C2867b;
import com.tencent.bugly.crashreport.crash.C2869c;
import com.tencent.bugly.crashreport.crash.CrashDetailBean;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2905ap;
import com.tencent.bugly.proguard.C2908aq;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.crash.jni.a */
/* compiled from: BUGLY */
public class C2880a implements NativeExceptionHandler {

    /* renamed from: a */
    private final Context f5911a;

    /* renamed from: b */
    private final C2867b f5912b;

    /* renamed from: c */
    private final C2851a f5913c;

    /* renamed from: d */
    private final C2854a f5914d;

    public C2880a(Context context, C2851a aVar, C2867b bVar, C2854a aVar2) {
        this.f5911a = context;
        this.f5912b = bVar;
        this.f5913c = aVar;
        this.f5914d = aVar2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.aq.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.aq.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.aq.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.aq.a(java.io.File, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.aq.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.aq.a(android.content.Context, java.lang.String):java.util.ArrayList<java.lang.String>
      com.tencent.bugly.proguard.aq.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.aq.a(byte[], int):byte[]
      com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    public CrashDetailBean packageCrashDatas(String str, String str2, long j, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, byte[] bArr, Map<String, String> map, boolean z, boolean z2) {
        int i;
        String str12;
        int indexOf;
        String str13 = str;
        String str14 = str8;
        byte[] bArr2 = bArr;
        boolean l = C2869c.m6653a().mo34417l();
        if (l) {
            C2903an.m6865e("This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful!", new Object[0]);
        }
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        crashDetailBean.f5753b = 1;
        crashDetailBean.f5756e = this.f5913c.mo34311h();
        crashDetailBean.f5757f = this.f5913c.f5671p;
        crashDetailBean.f5758g = this.f5913c.mo34326w();
        crashDetailBean.f5764m = this.f5913c.mo34309g();
        crashDetailBean.f5765n = str3;
        String str15 = "";
        crashDetailBean.f5766o = l ? " This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful![Bugly]" : str15;
        crashDetailBean.f5767p = str4;
        if (str5 != null) {
            str15 = str5;
        }
        crashDetailBean.f5768q = str15;
        crashDetailBean.f5769r = j;
        crashDetailBean.f5772u = C2908aq.m6926b(crashDetailBean.f5768q.getBytes());
        crashDetailBean.f5729A = str13;
        crashDetailBean.f5730B = str2;
        crashDetailBean.f5737I = this.f5913c.mo34328y();
        crashDetailBean.f5759h = this.f5913c.mo34325v();
        crashDetailBean.f5760i = this.f5913c.mo34280J();
        crashDetailBean.f5773v = str14;
        NativeCrashHandler instance = NativeCrashHandler.getInstance();
        String dumpFilePath = instance != null ? instance.getDumpFilePath() : null;
        String a = C2881b.m6723a(dumpFilePath, str14);
        if (!C2908aq.m6915a(a)) {
            crashDetailBean.f5750V = a;
        }
        crashDetailBean.f5751W = C2881b.m6728c(dumpFilePath);
        crashDetailBean.f5774w = C2881b.m6722a(str9, C2869c.f5825e, C2869c.f5828h, C2869c.f5833m);
        crashDetailBean.f5775x = C2881b.m6722a(str10, C2869c.f5825e, null, true);
        crashDetailBean.f5739K = str7;
        crashDetailBean.f5740L = str6;
        crashDetailBean.f5741M = str11;
        crashDetailBean.f5734F = this.f5913c.mo34319p();
        crashDetailBean.f5735G = this.f5913c.mo34318o();
        crashDetailBean.f5736H = this.f5913c.mo34320q();
        if (z) {
            crashDetailBean.f5731C = C2852b.m6550i();
            crashDetailBean.f5732D = C2852b.m6546g();
            crashDetailBean.f5733E = C2852b.m6554k();
            if (crashDetailBean.f5774w == null) {
                crashDetailBean.f5774w = C2908aq.m6899a(this.f5911a, C2869c.f5825e, C2869c.f5828h);
            }
            crashDetailBean.f5776y = C2905ap.m6877a();
            crashDetailBean.f5742N = this.f5913c.f5625a;
            crashDetailBean.f5743O = this.f5913c.mo34295a();
            crashDetailBean.f5745Q = this.f5913c.mo34278H();
            crashDetailBean.f5746R = this.f5913c.mo34279I();
            crashDetailBean.f5747S = this.f5913c.mo34272B();
            crashDetailBean.f5748T = this.f5913c.mo34277G();
            crashDetailBean.f5777z = C2908aq.m6907a(C2869c.f5826f, false);
            int indexOf2 = crashDetailBean.f5768q.indexOf("java:\n");
            if (indexOf2 > 0 && (i = indexOf2 + 6) < crashDetailBean.f5768q.length()) {
                String substring = crashDetailBean.f5768q.substring(i, crashDetailBean.f5768q.length() - 1);
                if (substring.length() > 0 && crashDetailBean.f5777z.containsKey(crashDetailBean.f5730B) && (indexOf = (str12 = crashDetailBean.f5777z.get(crashDetailBean.f5730B)).indexOf(substring)) > 0) {
                    String substring2 = str12.substring(indexOf);
                    crashDetailBean.f5777z.put(crashDetailBean.f5730B, substring2);
                    crashDetailBean.f5768q = crashDetailBean.f5768q.substring(0, i);
                    crashDetailBean.f5768q += substring2;
                }
            }
            if (str13 == null) {
                crashDetailBean.f5729A = this.f5913c.f5660e;
            }
            this.f5912b.mo34397c(crashDetailBean);
        } else {
            crashDetailBean.f5731C = -1;
            crashDetailBean.f5732D = -1;
            crashDetailBean.f5733E = -1;
            if (crashDetailBean.f5774w == null) {
                crashDetailBean.f5774w = "this crash is occurred at last process! Log is miss, when get an terrible ABRT Native Exception etc.";
            }
            crashDetailBean.f5742N = -1;
            crashDetailBean.f5745Q = -1;
            crashDetailBean.f5746R = -1;
            crashDetailBean.f5747S = map;
            crashDetailBean.f5748T = this.f5913c.mo34277G();
            crashDetailBean.f5777z = null;
            if (str13 == null) {
                crashDetailBean.f5729A = "unknown(record)";
            }
            if (bArr2 != null) {
                crashDetailBean.f5776y = bArr2;
            }
        }
        return crashDetailBean;
    }

    public void handleNativeException(int i, int i2, long j, long j2, String str, String str2, String str3, String str4, int i3, String str5, int i4, int i5, int i6, String str6, String str7) {
        C2903an.m6857a("Native Crash Happen v1", new Object[0]);
        handleNativeException2(i, i2, j, j2, str, str2, str3, str4, i3, str5, i4, i5, i6, str6, str7, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void
     arg types: [int, java.lang.String]
     candidates:
      com.tencent.bugly.crashreport.crash.jni.b.a(java.lang.String, java.lang.String):java.lang.String
      com.tencent.bugly.crashreport.crash.jni.b.a(boolean, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
     arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
     candidates:
      com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.be
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bf
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean>, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bg
      com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:103:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0101 A[Catch:{ all -> 0x029f }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x01b6 A[Catch:{ all -> 0x029f }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x01c9 A[SYNTHETIC, Splitter:B:64:0x01c9] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0231 A[Catch:{ all -> 0x029b }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x023a A[Catch:{ all -> 0x029b }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x02a7  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void handleNativeException2(int r28, int r29, long r30, long r32, java.lang.String r34, java.lang.String r35, java.lang.String r36, java.lang.String r37, int r38, java.lang.String r39, int r40, int r41, int r42, java.lang.String r43, java.lang.String r44, java.lang.String[] r45) {
        /*
            r27 = this;
            r14 = r27
            r0 = r35
            r13 = r38
            r1 = r40
            r2 = r45
            r12 = 0
            java.lang.Object[] r3 = new java.lang.Object[r12]
            java.lang.String r4 = "Native Crash Happen v2"
            com.tencent.bugly.proguard.C2903an.m6857a(r4, r3)
            java.lang.String r11 = com.tencent.bugly.crashreport.crash.jni.C2881b.m6726b(r36)     // Catch:{ all -> 0x029f }
            java.lang.String r3 = "UNKNOWN"
            java.lang.String r4 = ")"
            java.lang.String r5 = "("
            if (r13 <= 0) goto L_0x003e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x029f }
            r1.<init>()     // Catch:{ all -> 0x029f }
            r6 = r34
            r1.append(r6)     // Catch:{ all -> 0x029f }
            r1.append(r5)     // Catch:{ all -> 0x029f }
            r7 = r39
            r1.append(r7)     // Catch:{ all -> 0x029f }
            r1.append(r4)     // Catch:{ all -> 0x029f }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x029f }
            java.lang.String r6 = "KERNEL"
            r10 = r1
            r18 = r3
            r9 = r6
            goto L_0x0070
        L_0x003e:
            r6 = r34
            r7 = r39
            if (r1 <= 0) goto L_0x004a
            android.content.Context r3 = r14.f5911a     // Catch:{ all -> 0x029f }
            java.lang.String r3 = com.tencent.bugly.crashreport.common.info.AppInfo.m6456a(r3, r1)     // Catch:{ all -> 0x029f }
        L_0x004a:
            java.lang.String r8 = java.lang.String.valueOf(r40)     // Catch:{ all -> 0x029f }
            boolean r8 = r3.equals(r8)     // Catch:{ all -> 0x029f }
            if (r8 != 0) goto L_0x006c
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x029f }
            r8.<init>()     // Catch:{ all -> 0x029f }
            r8.append(r3)     // Catch:{ all -> 0x029f }
            r8.append(r5)     // Catch:{ all -> 0x029f }
            r8.append(r1)     // Catch:{ all -> 0x029f }
            r8.append(r4)     // Catch:{ all -> 0x029f }
            java.lang.String r1 = r8.toString()     // Catch:{ all -> 0x029f }
            r18 = r1
            goto L_0x006e
        L_0x006c:
            r18 = r3
        L_0x006e:
            r10 = r6
            r9 = r7
        L_0x0070:
            java.util.HashMap r1 = new java.util.HashMap     // Catch:{ all -> 0x029f }
            r1.<init>()     // Catch:{ all -> 0x029f }
            if (r2 == 0) goto L_0x00b0
            r3 = 0
        L_0x0078:
            int r6 = r2.length     // Catch:{ all -> 0x029f }
            if (r3 >= r6) goto L_0x00b7
            r6 = r2[r3]     // Catch:{ all -> 0x029f }
            if (r6 == 0) goto L_0x00ad
            java.lang.String r7 = "Extra message[%d]: %s"
            r15 = 2
            java.lang.Object[] r8 = new java.lang.Object[r15]     // Catch:{ all -> 0x029f }
            java.lang.Integer r16 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x029f }
            r8[r12] = r16     // Catch:{ all -> 0x029f }
            r16 = 1
            r8[r16] = r6     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6857a(r7, r8)     // Catch:{ all -> 0x029f }
            java.lang.String r7 = "="
            java.lang.String[] r7 = r6.split(r7)     // Catch:{ all -> 0x029f }
            int r8 = r7.length     // Catch:{ all -> 0x029f }
            if (r8 != r15) goto L_0x00a3
            r6 = r7[r12]     // Catch:{ all -> 0x029f }
            r8 = 1
            r7 = r7[r8]     // Catch:{ all -> 0x029f }
            r1.put(r6, r7)     // Catch:{ all -> 0x029f }
            goto L_0x00ad
        L_0x00a3:
            java.lang.String r7 = "bad extraMsg %s"
            r8 = 1
            java.lang.Object[] r15 = new java.lang.Object[r8]     // Catch:{ all -> 0x029f }
            r15[r12] = r6     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6864d(r7, r15)     // Catch:{ all -> 0x029f }
        L_0x00ad:
            int r3 = r3 + 1
            goto L_0x0078
        L_0x00b0:
            java.lang.String r2 = "not found extraMsg"
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6863c(r2, r3)     // Catch:{ all -> 0x029f }
        L_0x00b7:
            java.lang.String r2 = "HasPendingException"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029f }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x029f }
            if (r2 == 0) goto L_0x00d3
            java.lang.String r3 = "true"
            boolean r2 = r2.equals(r3)     // Catch:{ all -> 0x029f }
            if (r2 == 0) goto L_0x00d3
            java.lang.String r2 = "Native crash happened with a Java pending exception."
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6857a(r2, r3)     // Catch:{ all -> 0x029f }
            r19 = 1
            goto L_0x00d5
        L_0x00d3:
            r19 = 0
        L_0x00d5:
            java.lang.String r2 = "ExceptionProcessName"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029f }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x029f }
            if (r2 == 0) goto L_0x00f1
            int r3 = r2.length()     // Catch:{ all -> 0x029f }
            if (r3 != 0) goto L_0x00e6
            goto L_0x00f1
        L_0x00e6:
            java.lang.String r3 = "Name of crash process: %s"
            r6 = 1
            java.lang.Object[] r7 = new java.lang.Object[r6]     // Catch:{ all -> 0x029f }
            r7[r12] = r2     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6863c(r3, r7)     // Catch:{ all -> 0x029f }
            goto L_0x00f5
        L_0x00f1:
            com.tencent.bugly.crashreport.common.info.a r2 = r14.f5913c     // Catch:{ all -> 0x029f }
            java.lang.String r2 = r2.f5660e     // Catch:{ all -> 0x029f }
        L_0x00f5:
            r20 = r2
            java.lang.String r2 = "ExceptionThreadName"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029f }
            java.lang.String r2 = (java.lang.String) r2     // Catch:{ all -> 0x029f }
            if (r2 == 0) goto L_0x016f
            int r3 = r2.length()     // Catch:{ all -> 0x029f }
            if (r3 != 0) goto L_0x0108
            goto L_0x016f
        L_0x0108:
            java.lang.String r3 = "Name of crash thread: %s"
            r8 = 1
            java.lang.Object[] r6 = new java.lang.Object[r8]     // Catch:{ all -> 0x029f }
            r6[r12] = r2     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6863c(r3, r6)     // Catch:{ all -> 0x029f }
            java.util.Map r3 = java.lang.Thread.getAllStackTraces()     // Catch:{ all -> 0x029f }
            java.util.Set r3 = r3.keySet()     // Catch:{ all -> 0x029f }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ all -> 0x029f }
        L_0x011e:
            boolean r6 = r3.hasNext()     // Catch:{ all -> 0x029f }
            if (r6 == 0) goto L_0x0150
            java.lang.Object r6 = r3.next()     // Catch:{ all -> 0x029f }
            java.lang.Thread r6 = (java.lang.Thread) r6     // Catch:{ all -> 0x029f }
            java.lang.String r7 = r6.getName()     // Catch:{ all -> 0x029f }
            boolean r7 = r7.equals(r2)     // Catch:{ all -> 0x029f }
            if (r7 == 0) goto L_0x011e
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x029f }
            r3.<init>()     // Catch:{ all -> 0x029f }
            r3.append(r2)     // Catch:{ all -> 0x029f }
            r3.append(r5)     // Catch:{ all -> 0x029f }
            long r6 = r6.getId()     // Catch:{ all -> 0x029f }
            r3.append(r6)     // Catch:{ all -> 0x029f }
            r3.append(r4)     // Catch:{ all -> 0x029f }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x029f }
            r3 = r2
            r2 = 1
            goto L_0x0152
        L_0x0150:
            r3 = r2
            r2 = 0
        L_0x0152:
            if (r2 != 0) goto L_0x016c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x029f }
            r2.<init>()     // Catch:{ all -> 0x029f }
            r2.append(r3)     // Catch:{ all -> 0x029f }
            r2.append(r5)     // Catch:{ all -> 0x029f }
            r3 = r29
            r2.append(r3)     // Catch:{ all -> 0x029f }
            r2.append(r4)     // Catch:{ all -> 0x029f }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x029f }
            goto L_0x0191
        L_0x016c:
            r21 = r3
            goto L_0x0193
        L_0x016f:
            r8 = 1
            java.lang.Thread r2 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x029f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x029f }
            r3.<init>()     // Catch:{ all -> 0x029f }
            java.lang.String r6 = r2.getName()     // Catch:{ all -> 0x029f }
            r3.append(r6)     // Catch:{ all -> 0x029f }
            r3.append(r5)     // Catch:{ all -> 0x029f }
            long r5 = r2.getId()     // Catch:{ all -> 0x029f }
            r3.append(r5)     // Catch:{ all -> 0x029f }
            r3.append(r4)     // Catch:{ all -> 0x029f }
            java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x029f }
        L_0x0191:
            r21 = r2
        L_0x0193:
            r2 = 1000(0x3e8, double:4.94E-321)
            long r4 = r30 * r2
            long r2 = r32 / r2
            long r4 = r4 + r2
            java.lang.String r2 = "SysLogPath"
            java.lang.Object r2 = r1.get(r2)     // Catch:{ all -> 0x029f }
            r22 = r2
            java.lang.String r22 = (java.lang.String) r22     // Catch:{ all -> 0x029f }
            java.lang.String r2 = "JniLogPath"
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x029f }
            r23 = r1
            java.lang.String r23 = (java.lang.String) r23     // Catch:{ all -> 0x029f }
            com.tencent.bugly.crashreport.common.strategy.a r1 = r14.f5914d     // Catch:{ all -> 0x029f }
            boolean r1 = r1.mo34339b()     // Catch:{ all -> 0x029f }
            if (r1 != 0) goto L_0x01bd
            java.lang.String r1 = "no remote but still store!"
            java.lang.Object[] r2 = new java.lang.Object[r12]     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6864d(r1, r2)     // Catch:{ all -> 0x029f }
        L_0x01bd:
            com.tencent.bugly.crashreport.common.strategy.a r1 = r14.f5914d     // Catch:{ all -> 0x029f }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r1 = r1.mo34340c()     // Catch:{ all -> 0x029f }
            boolean r1 = r1.f5693g     // Catch:{ all -> 0x029f }
            java.lang.String r7 = "\n"
            if (r1 != 0) goto L_0x020a
            com.tencent.bugly.crashreport.common.strategy.a r1 = r14.f5914d     // Catch:{ all -> 0x029f }
            boolean r1 = r1.mo34339b()     // Catch:{ all -> 0x029f }
            if (r1 == 0) goto L_0x020a
            java.lang.String r1 = "crash report was closed by remote , will not upload to Bugly , print local for helpful!"
            java.lang.Object[] r2 = new java.lang.Object[r12]     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2903an.m6865e(r1, r2)     // Catch:{ all -> 0x029f }
            java.lang.String r1 = "NATIVE_CRASH"
            java.lang.String r2 = com.tencent.bugly.proguard.C2908aq.m6897a()     // Catch:{ all -> 0x029f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x029f }
            r3.<init>()     // Catch:{ all -> 0x029f }
            r3.append(r10)     // Catch:{ all -> 0x029f }
            r3.append(r7)     // Catch:{ all -> 0x029f }
            r3.append(r0)     // Catch:{ all -> 0x029f }
            r3.append(r7)     // Catch:{ all -> 0x029f }
            r3.append(r11)     // Catch:{ all -> 0x029f }
            java.lang.String r0 = r3.toString()     // Catch:{ all -> 0x029f }
            r3 = 0
            r29 = r1
            r30 = r2
            r31 = r20
            r32 = r21
            r33 = r0
            r34 = r3
            com.tencent.bugly.crashreport.crash.C2867b.m6631a(r29, r30, r31, r32, r33, r34)     // Catch:{ all -> 0x029f }
            com.tencent.bugly.proguard.C2908aq.m6930b(r37)     // Catch:{ all -> 0x029f }
            return
        L_0x020a:
            r15 = 0
            r16 = 0
            r17 = 1
            r1 = r27
            r2 = r20
            r3 = r21
            r6 = r10
            r24 = r7
            r7 = r35
            r8 = r11
            r25 = r10
            r10 = r18
            r26 = r11
            r11 = r37
            r12 = r22
            r13 = r23
            r14 = r44
            r18 = r19
            com.tencent.bugly.crashreport.crash.CrashDetailBean r1 = r1.packageCrashDatas(r2, r3, r4, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r18)     // Catch:{ all -> 0x029b }
            if (r1 != 0) goto L_0x023a
            java.lang.String r0 = "pkg crash datas fail!"
            r2 = 0
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ all -> 0x029b }
            com.tencent.bugly.proguard.C2903an.m6865e(r0, r1)     // Catch:{ all -> 0x029b }
            return
        L_0x023a:
            r2 = 0
            java.lang.String r3 = "NATIVE_CRASH"
            java.lang.String r4 = com.tencent.bugly.proguard.C2908aq.m6897a()     // Catch:{ all -> 0x029b }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x029b }
            r5.<init>()     // Catch:{ all -> 0x029b }
            r6 = r25
            r5.append(r6)     // Catch:{ all -> 0x029b }
            r6 = r24
            r5.append(r6)     // Catch:{ all -> 0x029b }
            r5.append(r0)     // Catch:{ all -> 0x029b }
            r5.append(r6)     // Catch:{ all -> 0x029b }
            r0 = r26
            r5.append(r0)     // Catch:{ all -> 0x029b }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x029b }
            r29 = r3
            r30 = r4
            r31 = r20
            r32 = r21
            r33 = r0
            r34 = r1
            com.tencent.bugly.crashreport.crash.C2867b.m6631a(r29, r30, r31, r32, r33, r34)     // Catch:{ all -> 0x029b }
            r3 = r27
            com.tencent.bugly.crashreport.crash.b r0 = r3.f5912b     // Catch:{ all -> 0x0299 }
            r4 = r38
            boolean r0 = r0.mo34392a(r1, r4)     // Catch:{ all -> 0x0299 }
            if (r0 != 0) goto L_0x027b
            r2 = 1
        L_0x027b:
            r0 = 0
            com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler r4 = com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler.getInstance()     // Catch:{ all -> 0x0299 }
            if (r4 == 0) goto L_0x0286
            java.lang.String r0 = r4.getDumpFilePath()     // Catch:{ all -> 0x0299 }
        L_0x0286:
            r4 = 1
            com.tencent.bugly.crashreport.crash.jni.C2881b.m6725a(r4, r0)     // Catch:{ all -> 0x0299 }
            if (r2 == 0) goto L_0x0293
            com.tencent.bugly.crashreport.crash.b r0 = r3.f5912b     // Catch:{ all -> 0x0299 }
            r5 = 3000(0xbb8, double:1.482E-320)
            r0.mo34388a(r1, r5, r4)     // Catch:{ all -> 0x0299 }
        L_0x0293:
            com.tencent.bugly.crashreport.crash.b r0 = r3.f5912b     // Catch:{ all -> 0x0299 }
            r0.mo34396b(r1)     // Catch:{ all -> 0x0299 }
            goto L_0x02aa
        L_0x0299:
            r0 = move-exception
            goto L_0x02a1
        L_0x029b:
            r0 = move-exception
            r3 = r27
            goto L_0x02a1
        L_0x029f:
            r0 = move-exception
            r3 = r14
        L_0x02a1:
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r0)
            if (r1 != 0) goto L_0x02aa
            r0.printStackTrace()
        L_0x02aa:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.jni.C2880a.handleNativeException2(int, int, long, long, java.lang.String, java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, int, int, int, java.lang.String, java.lang.String, java.lang.String[]):void");
    }
}
