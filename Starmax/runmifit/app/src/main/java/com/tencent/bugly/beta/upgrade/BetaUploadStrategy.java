package com.tencent.bugly.beta.upgrade;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C2893ah;
import com.tencent.bugly.proguard.C2930bk;
import com.tencent.bugly.proguard.C2944m;

/* compiled from: BUGLY */
public class BetaUploadStrategy implements Parcelable, Parcelable.Creator<BetaUploadStrategy> {
    public static final Parcelable.Creator<BetaUploadStrategy> CREATOR = new BetaUploadStrategy();

    /* renamed from: a */
    public C2930bk f5460a;

    /* renamed from: b */
    public long f5461b;

    public int describeContents() {
        return 0;
    }

    public BetaUploadStrategy() {
        this.f5460a = new C2930bk();
        C2930bk bkVar = this.f5460a;
        bkVar.f6155b = true;
        bkVar.f6156c = true;
        if (C2808e.f5336E.f5353Q) {
            this.f5460a.f6157d = StrategyBean.f5688b;
            this.f5460a.f6158e = StrategyBean.f5688b;
        } else {
            C2930bk bkVar2 = this.f5460a;
            bkVar2.f6157d = "http://android.bugly.qq.com/rqd/async";
            bkVar2.f6158e = "http://android.bugly.qq.com/rqd/async";
        }
        long currentTimeMillis = System.currentTimeMillis();
        this.f5460a.f6161h = currentTimeMillis;
        this.f5461b = currentTimeMillis;
    }

    public BetaUploadStrategy(Parcel parcel) {
        this.f5460a = (C2930bk) C2893ah.m6793a(parcel.createByteArray(), C2930bk.class);
        this.f5461b = parcel.readLong();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(C2893ah.m6794a((C2944m) this.f5460a));
        parcel.writeLong(this.f5461b);
    }

    /* renamed from: a */
    public BetaUploadStrategy createFromParcel(Parcel parcel) {
        return new BetaUploadStrategy(parcel);
    }

    /* renamed from: a */
    public BetaUploadStrategy[] newArray(int i) {
        return new BetaUploadStrategy[i];
    }
}
