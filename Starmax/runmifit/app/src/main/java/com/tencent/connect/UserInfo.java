package com.tencent.connect;

import android.content.Context;
import com.tencent.connect.auth.C2983c;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;

/* compiled from: ProGuard */
public class UserInfo extends BaseApi {
    public static final String GRAPH_OPEN_ID = "oauth2.0/m_me";

    public UserInfo(Context context, QQToken qQToken) {
        super(qQToken);
    }

    public UserInfo(Context context, C2983c cVar, QQToken qQToken) {
        super(cVar, qQToken);
    }

    public void getUserInfo(IUiListener iUiListener) {
        HttpUtils.requestAsync(this.f6457c, C3121e.m7727a(), "user/get_simple_userinfo", mo34798a(), "GET", new BaseApi.TempRequestListener(iUiListener));
    }

    public void getOpenId(IUiListener iUiListener) {
        HttpUtils.requestAsync(this.f6457c, C3121e.m7727a(), GRAPH_OPEN_ID, mo34798a(), "GET", new BaseApi.TempRequestListener(iUiListener));
    }
}
