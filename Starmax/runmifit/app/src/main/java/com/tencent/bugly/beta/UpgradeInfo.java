package com.tencent.bugly.beta;

import com.tencent.bugly.proguard.C2959y;

/* compiled from: BUGLY */
public class UpgradeInfo {
    public String apkMd5;
    public String apkUrl;
    public long fileSize;

    /* renamed from: id */
    public String f5307id = "";
    public String imageUrl;
    public String newFeature = "";
    public long popInterval = 0;
    public int popTimes = 0;
    public long publishTime = 0;
    public int publishType = 0;
    public String title = "";
    public int updateType;
    public int upgradeType = 1;
    public int versionCode;
    public String versionName = "";

    public UpgradeInfo(C2959y yVar) {
        if (yVar != null) {
            this.f5307id = yVar.f6294m;
            this.title = yVar.f6282a;
            this.newFeature = yVar.f6283b;
            this.publishTime = yVar.f6284c;
            this.publishType = yVar.f6285d;
            this.upgradeType = yVar.f6288g;
            this.popTimes = yVar.f6289h;
            this.popInterval = yVar.f6290i;
            this.versionCode = yVar.f6286e.f6254c;
            this.versionName = yVar.f6286e.f6255d;
            this.apkMd5 = yVar.f6286e.f6260i;
            this.apkUrl = yVar.f6287f.f6247b;
            this.fileSize = yVar.f6287f.f6249d;
            this.imageUrl = yVar.f6293l.get("IMG_title");
            this.updateType = yVar.f6297p;
        }
    }
}
