package com.tencent.bugly.proguard;

import com.tamic.novate.util.FileUtil;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.e */
/* compiled from: BUGLY */
public class C2935e extends C2934d {

    /* renamed from: h */
    static HashMap<String, byte[]> f6190h;

    /* renamed from: i */
    static HashMap<String, HashMap<String, byte[]>> f6191i;

    /* renamed from: g */
    protected C2937g f6192g = new C2937g();

    /* renamed from: j */
    private int f6193j = 0;

    public C2935e() {
        this.f6192g.f6199a = 2;
    }

    /* renamed from: a */
    public <T> void mo34581a(String str, Object obj) {
        if (!str.startsWith(FileUtil.HIDDEN_PREFIX)) {
            super.mo34581a(str, obj);
            return;
        }
        throw new IllegalArgumentException("put name can not startwith . , now is " + str);
    }

    /* renamed from: b */
    public void mo34585b() {
        super.mo34585b();
        this.f6192g.f6199a = 3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.HashMap, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public byte[] mo34583a() {
        if (this.f6192g.f6199a != 2) {
            if (this.f6192g.f6203e == null) {
                this.f6192g.f6203e = "";
            }
            if (this.f6192g.f6204f == null) {
                this.f6192g.f6204f = "";
            }
        } else if (this.f6192g.f6203e.equals("")) {
            throw new IllegalArgumentException("servantName can not is null");
        } else if (this.f6192g.f6204f.equals("")) {
            throw new IllegalArgumentException("funcName can not is null");
        }
        C2943l lVar = new C2943l(0);
        lVar.mo34638a(this.f6184c);
        if (this.f6192g.f6199a == 2) {
            lVar.mo34650a((Map) this.f6182a, 0);
        } else {
            lVar.mo34650a((Map) this.f6187e, 0);
        }
        this.f6192g.f6205g = C2945n.m7125a(lVar.mo34639a());
        C2943l lVar2 = new C2943l(0);
        lVar2.mo34638a(this.f6184c);
        mo34588a(lVar2);
        byte[] a = C2945n.m7125a(lVar2.mo34639a());
        int length = a.length + 4;
        ByteBuffer allocate = ByteBuffer.allocate(length);
        allocate.putInt(length).put(a).flip();
        return allocate.array();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, byte[]>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
     arg types: [java.util.HashMap<java.lang.String, java.util.HashMap<java.lang.String, byte[]>>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V> */
    /* renamed from: a */
    public void mo34582a(byte[] bArr) {
        if (bArr.length >= 4) {
            try {
                C2941k kVar = new C2941k(bArr, 4);
                kVar.mo34617a(this.f6184c);
                mo34587a(kVar);
                if (this.f6192g.f6199a == 3) {
                    C2941k kVar2 = new C2941k(this.f6192g.f6205g);
                    kVar2.mo34617a(this.f6184c);
                    if (f6190h == null) {
                        f6190h = new HashMap<>();
                        f6190h.put("", new byte[0]);
                    }
                    this.f6187e = kVar2.mo34622a((Map) f6190h, 0, false);
                    return;
                }
                C2941k kVar3 = new C2941k(this.f6192g.f6205g);
                kVar3.mo34617a(this.f6184c);
                if (f6191i == null) {
                    f6191i = new HashMap<>();
                    HashMap hashMap = new HashMap();
                    hashMap.put("", new byte[0]);
                    f6191i.put("", hashMap);
                }
                this.f6182a = kVar3.mo34622a((Map) f6191i, 0, false);
                this.f6183b = new HashMap();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new IllegalArgumentException("decode package must include size head");
        }
    }

    /* renamed from: b */
    public void mo34589b(String str) {
        this.f6192g.f6203e = str;
    }

    /* renamed from: c */
    public void mo34590c(String str) {
        this.f6192g.f6204f = str;
    }

    /* renamed from: a */
    public void mo34586a(int i) {
        this.f6192g.f6202d = i;
    }

    /* renamed from: a */
    public void mo34588a(C2943l lVar) {
        this.f6192g.mo34476a(lVar);
    }

    /* renamed from: a */
    public void mo34587a(C2941k kVar) {
        this.f6192g.mo34475a(kVar);
    }
}
