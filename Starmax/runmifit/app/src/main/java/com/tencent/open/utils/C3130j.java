package com.tencent.open.utils;

import android.net.SSLCertificateSocketFactory;
import android.os.Build;
import com.tencent.open.p059a.C3082f;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSocket;
import org.apache.http.conn.scheme.LayeredSocketFactory;
import org.apache.http.conn.ssl.StrictHostnameVerifier;
import org.apache.http.params.HttpParams;

/* renamed from: com.tencent.open.utils.j */
/* compiled from: ProGuard */
public class C3130j implements LayeredSocketFactory {

    /* renamed from: a */
    static final HostnameVerifier f6859a = new StrictHostnameVerifier();

    /* renamed from: b */
    SSLCertificateSocketFactory f6860b = ((SSLCertificateSocketFactory) SSLCertificateSocketFactory.getInsecure(0, null));

    public Socket connectSocket(Socket socket, String str, int i, InetAddress inetAddress, int i2, HttpParams httpParams) throws IOException {
        socket.connect(new InetSocketAddress(str, i));
        return socket;
    }

    public Socket createSocket() {
        return new Socket();
    }

    public boolean isSecure(Socket socket) throws IllegalArgumentException {
        if (socket instanceof SSLSocket) {
            return ((SSLSocket) socket).isConnected();
        }
        return false;
    }

    public Socket createSocket(Socket socket, String str, int i, boolean z) throws IOException {
        C3082f.m7628a("SNISocketFactory", "createSocket " + socket.toString() + " host:" + str + " port:" + i + " autoClose:" + z);
        SSLSocket sSLSocket = (SSLSocket) this.f6860b.createSocket(socket, str, i, z);
        sSLSocket.setEnabledProtocols(sSLSocket.getSupportedProtocols());
        if (Build.VERSION.SDK_INT >= 17) {
            C3082f.m7628a("SNISocketFactory", "Setting SNI hostname");
            this.f6860b.setHostname(sSLSocket, str);
        } else {
            C3082f.m7628a("SNISocketFactory", "No documented SNI support on Android <4.2, trying with reflection");
            try {
                sSLSocket.getClass().getMethod("setHostname", String.class).invoke(sSLSocket, str);
            } catch (Exception unused) {
                C3082f.m7628a("SNISocketFactory", "SNI not useable");
            }
        }
        if (f6859a.verify(str, sSLSocket.getSession())) {
            return sSLSocket;
        }
        throw new SSLPeerUnverifiedException("Cannot verify hostname: " + str);
    }
}
