package com.tencent.bugly.proguard;

import java.security.NoSuchAlgorithmException;

/* renamed from: com.tencent.bugly.proguard.bb */
/* compiled from: BUGLY */
public interface C2921bb {
    /* renamed from: a */
    void mo34574a(String str);

    /* renamed from: a */
    byte[] mo34575a(byte[] bArr) throws Exception;

    /* renamed from: b */
    byte[] mo34576b(byte[] bArr) throws Exception, NoSuchAlgorithmException;
}
