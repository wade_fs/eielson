package com.tencent.bugly.beta.utils;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

/* renamed from: com.tencent.bugly.beta.utils.a */
/* compiled from: BUGLY */
public class C2830a {

    /* renamed from: a */
    public static long f5483a = -1;

    /* renamed from: b */
    public static long f5484b = 0;

    /* renamed from: c */
    public static long f5485c = 1;

    /* renamed from: d */
    private byte[] f5486d = new byte[2];

    /* renamed from: e */
    private byte[] f5487e = new byte[4];

    /* renamed from: f */
    private byte[] f5488f = new byte[8];

    /* renamed from: g */
    private long f5489g = f5485c;

    /* renamed from: h */
    private String f5490h = null;

    /* renamed from: i */
    private BufferedInputStream f5491i = null;

    /* renamed from: j */
    private long f5492j = 0;

    /* renamed from: k */
    private long f5493k = 0;

    /* renamed from: a */
    public synchronized void mo34192a(long j) {
        this.f5489g = j;
    }

    public C2830a(String str) throws Exception {
        this.f5490h = str;
        this.f5491i = new BufferedInputStream(new FileInputStream(this.f5490h));
        this.f5492j = 0;
        this.f5493k = 0;
    }

    public C2830a(String str, long j) throws Exception {
        this.f5490h = str;
        this.f5489g = j;
        this.f5491i = new BufferedInputStream(new FileInputStream(this.f5490h));
        this.f5492j = 0;
        this.f5493k = 0;
    }

    /* renamed from: a */
    public synchronized boolean mo34193a() {
        try {
            if (this.f5491i != null) {
                this.f5491i.close();
            }
            this.f5491i = null;
            this.f5490h = null;
            this.f5492j = 0;
            this.f5493k = 0;
        } catch (IOException e) {
            Log.e("BinaryFileReader", e.getMessage());
            return false;
        }
        return true;
    }

    /* renamed from: b */
    public synchronized boolean mo34196b(long j) {
        if (this.f5491i == null) {
            Log.e("BinaryFileReader", "Please open file first！");
            return false;
        } else if (j == 0) {
            return true;
        } else {
            long j2 = j;
            while (j2 > 0) {
                try {
                    j2 -= this.f5491i.skip(j2);
                } catch (IOException unused) {
                    Log.e("BinaryFileReader", "Failed to skip file pointer！");
                    return false;
                }
            }
            this.f5492j += j;
            return true;
        }
    }

    /* renamed from: a */
    public synchronized boolean mo34194a(byte[] bArr) {
        try {
            this.f5491i.read(bArr);
            this.f5492j += (long) bArr.length;
            this.f5493k += (long) bArr.length;
        } catch (IOException e) {
            Log.e("BinaryFileReader", e.getMessage());
            return false;
        }
        return true;
    }

    /* renamed from: b */
    public synchronized byte mo34195b() throws IOException {
        if (this.f5491i == null) {
            Log.e("BinaryFileReader", "Failed to skip file pointer！");
            return 0;
        }
        byte read = (byte) this.f5491i.read();
        this.f5492j++;
        this.f5493k++;
        return read;
    }

    /* renamed from: c */
    public synchronized short mo34197c() throws IOException {
        if (this.f5491i == null) {
            Log.e("BinaryFileReader", "Failed to skip file pointer！");
            return 0;
        }
        this.f5491i.read(this.f5486d);
        short a = m6321a(this.f5486d, this.f5489g);
        this.f5492j += 2;
        this.f5493k += 2;
        return a;
    }

    /* renamed from: d */
    public synchronized int mo34198d() throws IOException {
        if (this.f5491i == null) {
            Log.e("BinaryFileReader", "Failed to skip file pointer！");
            return 0;
        }
        this.f5491i.read(this.f5487e);
        int b = m6322b(this.f5487e, this.f5489g);
        this.f5492j += 4;
        this.f5493k += 4;
        return b;
    }

    /* renamed from: e */
    public synchronized long mo34199e() throws IOException {
        if (this.f5491i == null) {
            Log.e("BinaryFileReader", "Failed to skip file pointer！");
            return 0;
        }
        this.f5491i.read(this.f5488f);
        long c = m6325c(this.f5488f, this.f5489g);
        this.f5492j += 8;
        this.f5493k += 8;
        return c;
    }

    /* renamed from: f */
    public synchronized long mo34200f() throws IOException {
        return ((long) mo34195b()) & 255;
    }

    /* renamed from: g */
    public synchronized long mo34201g() throws IOException {
        return ((long) mo34197c()) & 65535;
    }

    /* renamed from: h */
    public synchronized long mo34202h() throws IOException {
        return ((long) mo34198d()) & 4294967295L;
    }

    /* renamed from: i */
    public synchronized long mo34203i() throws IOException {
        return mo34199e();
    }

    /* renamed from: b */
    private static short m6323b(byte[] bArr) {
        if (bArr == null || bArr.length > 2) {
            return -1;
        }
        return (short) m6324c(bArr);
    }

    /* renamed from: c */
    private static int m6324c(byte[] bArr) {
        if (bArr == null || bArr.length > 4) {
            return -1;
        }
        return (int) m6326d(bArr);
    }

    /* renamed from: d */
    private static long m6326d(byte[] bArr) {
        if (bArr == null || bArr.length > 8) {
            return -1;
        }
        long j = 0;
        for (int length = bArr.length - 1; length >= 0; length--) {
            j = (j << 8) | (((long) bArr[length]) & 255);
        }
        return j;
    }

    /* renamed from: e */
    private static short m6327e(byte[] bArr) {
        if (bArr == null || bArr.length > 2) {
            return -1;
        }
        return (short) m6328f(bArr);
    }

    /* renamed from: f */
    private static int m6328f(byte[] bArr) {
        if (bArr == null || bArr.length > 4) {
            return -1;
        }
        return (int) m6329g(bArr);
    }

    /* renamed from: g */
    private static long m6329g(byte[] bArr) {
        if (bArr == null || bArr.length > 8) {
            return -1;
        }
        long j = 0;
        for (byte b : bArr) {
            j = (j << 8) | (((long) b) & 255);
        }
        return j;
    }

    /* renamed from: a */
    public static short m6321a(byte[] bArr, long j) {
        if (j == f5485c) {
            return m6323b(bArr);
        }
        return m6327e(bArr);
    }

    /* renamed from: b */
    public static int m6322b(byte[] bArr, long j) {
        if (j == f5485c) {
            return m6324c(bArr);
        }
        return m6328f(bArr);
    }

    /* renamed from: c */
    public static long m6325c(byte[] bArr, long j) {
        if (j == f5485c) {
            return m6326d(bArr);
        }
        return m6329g(bArr);
    }
}
