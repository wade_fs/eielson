package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatServiceImpl;
import com.tencent.stat.StatSpecifyReportedInfo;
import java.util.Map;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.c */
public class C3238c extends Event {

    /* renamed from: a */
    protected C3239a f7362a = new C3239a();

    /* renamed from: s */
    private double f7363s = 0.0d;

    /* renamed from: com.tencent.stat.event.c$a */
    public static class C3239a {

        /* renamed from: a */
        public String f7364a;

        /* renamed from: b */
        public JSONArray f7365b;

        /* renamed from: c */
        public JSONObject f7366c = null;

        public C3239a(String str, String[] strArr, Properties properties) {
            this.f7364a = str;
            if (properties != null) {
                this.f7366c = new JSONObject(properties);
            } else if (strArr != null) {
                this.f7365b = new JSONArray();
                for (String str2 : strArr) {
                    this.f7365b.put(str2);
                }
            } else {
                this.f7366c = new JSONObject();
            }
        }

        public C3239a() {
        }

        public String toString() {
            StringBuilder sb = new StringBuilder(32);
            sb.append(this.f7364a);
            sb.append(",");
            JSONArray jSONArray = this.f7365b;
            if (jSONArray != null) {
                sb.append(jSONArray.toString());
            }
            JSONObject jSONObject = this.f7366c;
            if (jSONObject != null) {
                sb.append(jSONObject.toString());
            }
            return sb.toString();
        }

        public int hashCode() {
            return toString().hashCode();
        }

        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (this == obj) {
                return true;
            }
            if (obj instanceof C3239a) {
                return toString().equals(((C3239a) obj).toString());
            }
            return false;
        }
    }

    /* renamed from: a */
    public C3239a mo35462a() {
        return this.f7362a;
    }

    /* renamed from: a */
    public void mo35463a(double d) {
        this.f7363s = d;
    }

    public C3238c(Context context, int i, String str, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7362a.f7364a = str;
    }

    public EventType getType() {
        return EventType.CUSTOM;
    }

    /* renamed from: b */
    private void m8086b() {
        Properties commonKeyValueForKVEvent;
        if (this.f7362a.f7364a != null && (commonKeyValueForKVEvent = StatServiceImpl.getCommonKeyValueForKVEvent(this.f7362a.f7364a)) != null && commonKeyValueForKVEvent.size() > 0) {
            if (this.f7362a.f7366c == null || this.f7362a.f7366c.length() == 0) {
                this.f7362a.f7366c = new JSONObject(commonKeyValueForKVEvent);
                return;
            }
            for (Map.Entry entry : commonKeyValueForKVEvent.entrySet()) {
                try {
                    this.f7362a.f7366c.put(entry.getKey().toString(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        jSONObject.put("ei", this.f7362a.f7364a);
        double d = this.f7363s;
        if (d > 0.0d) {
            jSONObject.put("du", d);
        }
        if (this.f7362a.f7365b == null) {
            m8086b();
            jSONObject.put("kv", this.f7362a.f7366c);
            return true;
        }
        jSONObject.put("ar", this.f7362a.f7365b);
        return true;
    }
}
