package com.tencent.bugly.proguard;

import com.google.common.base.Ascii;

/* renamed from: com.tencent.bugly.proguard.f */
/* compiled from: BUGLY */
public class C2936f {

    /* renamed from: a */
    public static final byte[] f6194a = new byte[0];

    /* renamed from: b */
    private static final char[] f6195b = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: a */
    public static String m7035a(byte[] bArr) {
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i];
            int i2 = i * 2;
            char[] cArr2 = f6195b;
            cArr[i2 + 1] = cArr2[b & Ascii.f4512SI];
            cArr[i2 + 0] = cArr2[((byte) (b >>> 4)) & Ascii.f4512SI];
        }
        return new String(cArr);
    }
}
