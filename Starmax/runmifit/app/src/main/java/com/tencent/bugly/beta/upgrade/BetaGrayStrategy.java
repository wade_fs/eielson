package com.tencent.bugly.beta.upgrade;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.proguard.C2893ah;
import com.tencent.bugly.proguard.C2944m;
import com.tencent.bugly.proguard.C2959y;

/* compiled from: BUGLY */
public class BetaGrayStrategy implements Parcelable, Parcelable.Creator<BetaGrayStrategy> {
    public static final Parcelable.Creator<BetaGrayStrategy> CREATOR = new BetaGrayStrategy();

    /* renamed from: a */
    public C2959y f5455a;

    /* renamed from: b */
    public int f5456b = 0;

    /* renamed from: c */
    public long f5457c = -1;

    /* renamed from: d */
    public boolean f5458d = false;

    /* renamed from: e */
    public long f5459e = -1;

    public int describeContents() {
        return 0;
    }

    public BetaGrayStrategy(Parcel parcel) {
        boolean z = false;
        this.f5455a = (C2959y) C2893ah.m6793a(parcel.createByteArray(), C2959y.class);
        this.f5456b = parcel.readInt();
        this.f5457c = parcel.readLong();
        this.f5458d = 1 == parcel.readByte() ? true : z;
        this.f5459e = parcel.readLong();
    }

    public BetaGrayStrategy() {
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByteArray(C2893ah.m6794a((C2944m) this.f5455a));
        parcel.writeInt(this.f5456b);
        parcel.writeLong(this.f5457c);
        parcel.writeByte(this.f5458d ? (byte) 1 : 0);
        parcel.writeLong(this.f5459e);
    }

    /* renamed from: a */
    public BetaGrayStrategy createFromParcel(Parcel parcel) {
        return new BetaGrayStrategy(parcel);
    }

    /* renamed from: a */
    public BetaGrayStrategy[] newArray(int i) {
        return new BetaGrayStrategy[i];
    }
}
