package com.tencent.bugly.crashreport.crash;

import android.content.Context;
import android.os.Build;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.crashreport.common.info.AppInfo;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.crashreport.crash.anr.C2864b;
import com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2892ag;
import com.tencent.bugly.proguard.C2896ak;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* renamed from: com.tencent.bugly.crashreport.crash.c */
/* compiled from: BUGLY */
public class C2869c {

    /* renamed from: a */
    public static int f5821a = 0;

    /* renamed from: b */
    public static boolean f5822b = false;

    /* renamed from: c */
    public static int f5823c = 2;

    /* renamed from: d */
    public static boolean f5824d = true;

    /* renamed from: e */
    public static int f5825e = 20480;

    /* renamed from: f */
    public static int f5826f = 20480;

    /* renamed from: g */
    public static long f5827g = 604800000;

    /* renamed from: h */
    public static String f5828h = null;

    /* renamed from: i */
    public static boolean f5829i = false;

    /* renamed from: j */
    public static String f5830j = null;

    /* renamed from: k */
    public static int f5831k = 5000;

    /* renamed from: l */
    public static boolean f5832l = true;

    /* renamed from: m */
    public static boolean f5833m = false;

    /* renamed from: n */
    public static String f5834n;

    /* renamed from: o */
    public static String f5835o;

    /* renamed from: v */
    private static C2869c f5836v;

    /* renamed from: p */
    public final C2867b f5837p;

    /* renamed from: q */
    public final C2854a f5838q = C2854a.m6574a();

    /* renamed from: r */
    public final C2901am f5839r;

    /* renamed from: s */
    public BuglyStrategy.C2796a f5840s;

    /* renamed from: t */
    public C2876f f5841t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public final Context f5842u;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public final C2875e f5843w;

    /* renamed from: x */
    private final NativeCrashHandler f5844x;

    /* renamed from: y */
    private final C2864b f5845y;

    /* renamed from: z */
    private Boolean f5846z;

    protected C2869c(int i, Context context, C2901am amVar, boolean z, BuglyStrategy.C2796a aVar, C2876f fVar, String str) {
        f5821a = i;
        Context a = C2908aq.m6891a(context);
        this.f5842u = a;
        this.f5839r = amVar;
        this.f5840s = aVar;
        C2876f fVar2 = fVar;
        this.f5841t = fVar2;
        C2896ak a2 = C2896ak.m6805a();
        C2889ae a3 = C2889ae.m6757a();
        this.f5837p = new C2867b(i, a, a2, a3, this.f5838q, aVar, fVar2);
        C2851a a4 = C2851a.m6470a(a);
        this.f5843w = new C2875e(a, this.f5837p, this.f5838q, a4);
        C2901am amVar2 = amVar;
        this.f5844x = NativeCrashHandler.getInstance(a, a4, this.f5837p, this.f5838q, amVar2, z, str);
        a4.f5611M = this.f5844x;
        this.f5845y = new C2864b(a, this.f5838q, a4, amVar2, a3, this.f5837p, aVar);
    }

    /* renamed from: a */
    public static synchronized C2869c m6654a(int i, Context context, boolean z, BuglyStrategy.C2796a aVar, C2876f fVar, String str) {
        C2869c cVar;
        synchronized (C2869c.class) {
            if (f5836v == null) {
                f5836v = new C2869c(i, context, C2901am.m6848a(), z, aVar, fVar, str);
            }
            cVar = f5836v;
        }
        return cVar;
    }

    /* renamed from: a */
    public static synchronized C2869c m6653a() {
        C2869c cVar;
        synchronized (C2869c.class) {
            cVar = f5836v;
        }
        return cVar;
    }

    /* renamed from: a */
    public void mo34403a(StrategyBean strategyBean) {
        this.f5843w.mo34426a(strategyBean);
        this.f5844x.onStrategyChanged(strategyBean);
        this.f5845y.mo34366a(strategyBean);
        mo34402a(3000);
    }

    /* renamed from: b */
    public boolean mo34407b() {
        Boolean bool = this.f5846z;
        if (bool != null) {
            return bool.booleanValue();
        }
        String str = C2851a.m6471b().f5660e;
        List<C2892ag> a = C2889ae.m6757a().mo34493a(1);
        ArrayList arrayList = new ArrayList();
        if (a == null || a.size() <= 0) {
            this.f5846z = false;
            return false;
        }
        for (C2892ag agVar : a) {
            if (str.equals(agVar.f5966c)) {
                this.f5846z = true;
                arrayList.add(agVar);
            }
        }
        if (arrayList.size() > 0) {
            C2889ae.m6757a().mo34495a(arrayList);
        }
        return true;
    }

    /* renamed from: c */
    public synchronized void mo34408c() {
        mo34411f();
        mo34413h();
        mo34414i();
    }

    /* renamed from: d */
    public synchronized void mo34409d() {
        mo34410e();
        mo34412g();
        mo34415j();
    }

    /* renamed from: e */
    public void mo34410e() {
        this.f5843w.mo34428b();
    }

    /* renamed from: f */
    public void mo34411f() {
        this.f5843w.mo34425a();
    }

    /* renamed from: g */
    public void mo34412g() {
        this.f5844x.setUserOpened(false);
    }

    /* renamed from: h */
    public void mo34413h() {
        this.f5844x.setUserOpened(true);
    }

    /* renamed from: i */
    public void mo34414i() {
        if (Build.VERSION.SDK_INT <= 19) {
            this.f5845y.mo34374b(true);
        } else {
            this.f5845y.mo34380h();
        }
    }

    /* renamed from: j */
    public void mo34415j() {
        if (Build.VERSION.SDK_INT < 19) {
            this.f5845y.mo34374b(false);
        } else {
            this.f5845y.mo34381i();
        }
    }

    /* renamed from: a */
    public synchronized void mo34406a(boolean z, boolean z2, boolean z3) {
        this.f5844x.testNativeCrash(z, z2, z3);
    }

    /* renamed from: k */
    public synchronized void mo34416k() {
        this.f5845y.mo34379g();
    }

    /* renamed from: l */
    public boolean mo34417l() {
        return this.f5845y.mo34369a();
    }

    /* renamed from: a */
    public void mo34405a(Thread thread, Throwable th, boolean z, String str, byte[] bArr, boolean z2) {
        final boolean z3 = z;
        final Thread thread2 = thread;
        final Throwable th2 = th;
        final String str2 = str;
        final byte[] bArr2 = bArr;
        final boolean z4 = z2;
        this.f5839r.mo34547a(new Runnable() {
            /* class com.tencent.bugly.crashreport.crash.C2869c.C28701 */

            public void run() {
                try {
                    C2903an.m6863c("post a throwable %b", Boolean.valueOf(z3));
                    C2869c.this.f5843w.mo34429b(thread2, th2, false, str2, bArr2);
                    if (z4) {
                        C2903an.m6857a("clear user datas", new Object[0]);
                        C2851a.m6470a(C2869c.this.f5842u).mo34273C();
                    }
                } catch (Throwable th) {
                    if (!C2903an.m6861b(th)) {
                        th.printStackTrace();
                    }
                    C2903an.m6865e("java catch error: %s", th2.toString());
                }
            }
        });
    }

    /* renamed from: a */
    public void mo34404a(CrashDetailBean crashDetailBean) {
        this.f5837p.mo34401e(crashDetailBean);
    }

    /* renamed from: a */
    public void mo34402a(long j) {
        C2901am.m6848a().mo34548a(new Thread() {
            /* class com.tencent.bugly.crashreport.crash.C2869c.C28712 */

            public void run() {
                ArrayList arrayList;
                if (C2908aq.m6912a(C2869c.this.f5842u, "local_crash_lock", 10000)) {
                    List<CrashDetailBean> a = C2869c.this.f5837p.mo34386a();
                    if (a != null && a.size() > 0) {
                        C2903an.m6863c("Size of crash list: %s", Integer.valueOf(a.size()));
                        int size = a.size();
                        if (((long) size) > 100) {
                            ArrayList arrayList2 = new ArrayList();
                            Collections.sort(a);
                            for (int i = 0; ((long) i) < 100; i++) {
                                arrayList2.add(a.get((size - 1) - i));
                            }
                            arrayList = arrayList2;
                        } else {
                            arrayList = a;
                        }
                        C2869c.this.f5837p.mo34389a(arrayList, 0, false, false, false);
                    }
                    C2908aq.m6939c(C2869c.this.f5842u, "local_crash_lock");
                }
            }
        }, j);
    }

    /* renamed from: m */
    public void mo34418m() {
        this.f5844x.checkUploadRecordCrash();
    }

    /* renamed from: n */
    public void mo34419n() {
        if (C2851a.m6471b().f5660e.equals(AppInfo.m6455a(this.f5842u))) {
            this.f5844x.removeEmptyNativeRecordFiles();
        }
    }
}
