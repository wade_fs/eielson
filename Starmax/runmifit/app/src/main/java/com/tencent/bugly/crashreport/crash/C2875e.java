package com.tencent.bugly.crashreport.crash;

import android.content.Context;
import android.os.Process;
import com.baidu.mobstat.Config;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.info.C2852b;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2905ap;
import com.tencent.bugly.proguard.C2908aq;
import java.lang.Thread;
import java.util.HashMap;

/* renamed from: com.tencent.bugly.crashreport.crash.e */
/* compiled from: BUGLY */
public class C2875e implements Thread.UncaughtExceptionHandler {

    /* renamed from: h */
    private static String f5867h;

    /* renamed from: i */
    private static final Object f5868i = new Object();

    /* renamed from: a */
    protected final Context f5869a;

    /* renamed from: b */
    protected final C2867b f5870b;

    /* renamed from: c */
    protected final C2854a f5871c;

    /* renamed from: d */
    protected final C2851a f5872d;

    /* renamed from: e */
    protected Thread.UncaughtExceptionHandler f5873e;

    /* renamed from: f */
    protected Thread.UncaughtExceptionHandler f5874f;

    /* renamed from: g */
    protected boolean f5875g = false;

    /* renamed from: j */
    private int f5876j;

    public C2875e(Context context, C2867b bVar, C2854a aVar, C2851a aVar2) {
        this.f5869a = context;
        this.f5870b = bVar;
        this.f5871c = aVar;
        this.f5872d = aVar2;
    }

    /* renamed from: a */
    public synchronized void mo34425a() {
        if (this.f5876j >= 10) {
            C2903an.m6857a("java crash handler over %d, no need set.", 10);
            return;
        }
        this.f5875g = true;
        Thread.UncaughtExceptionHandler defaultUncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();
        if (defaultUncaughtExceptionHandler != null) {
            if (!getClass().getName().equals(defaultUncaughtExceptionHandler.getClass().getName())) {
                if ("com.android.internal.os.RuntimeInit$UncaughtHandler".equals(defaultUncaughtExceptionHandler.getClass().getName())) {
                    C2903an.m6857a("backup system java handler: %s", defaultUncaughtExceptionHandler.toString());
                    this.f5874f = defaultUncaughtExceptionHandler;
                    this.f5873e = defaultUncaughtExceptionHandler;
                } else {
                    C2903an.m6857a("backup java handler: %s", defaultUncaughtExceptionHandler.toString());
                    this.f5873e = defaultUncaughtExceptionHandler;
                }
            } else {
                return;
            }
        }
        Thread.setDefaultUncaughtExceptionHandler(this);
        this.f5876j++;
        C2903an.m6857a("registered java monitor: %s", toString());
    }

    /* renamed from: b */
    public synchronized void mo34428b() {
        this.f5875g = false;
        C2903an.m6857a("close java monitor!", new Object[0]);
        if (Thread.getDefaultUncaughtExceptionHandler().getClass().getName().contains("bugly")) {
            C2903an.m6857a("Java monitor to unregister: %s", toString());
            Thread.setDefaultUncaughtExceptionHandler(this.f5873e);
            this.f5876j--;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34427a(Thread thread, Throwable th) {
        C2903an.m6865e("current process die", new Object[0]);
        Process.killProcess(Process.myPid());
        System.exit(1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.aq.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.aq.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.aq.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.aq.a(java.io.File, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.aq.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.aq.a(android.content.Context, java.lang.String):java.util.ArrayList<java.lang.String>
      com.tencent.bugly.proguard.aq.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.aq.a(byte[], int):byte[]
      com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* renamed from: a */
    public CrashDetailBean mo34424a(Thread thread, Throwable th, boolean z, String str, byte[] bArr) {
        String str2;
        String str3;
        Throwable th2 = th;
        String str4 = str;
        byte[] bArr2 = bArr;
        boolean z2 = false;
        if (th2 == null) {
            C2903an.m6864d("We can do nothing with a null throwable.", new Object[0]);
            return null;
        }
        boolean l = C2869c.m6653a().mo34417l();
        String str5 = (!l || !z) ? "" : " This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful![Bugly]";
        if (l && z) {
            C2903an.m6865e("This Crash Caused By ANR , PLS To Fix ANR , This Trace May Be Not Useful!", new Object[0]);
        }
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        crashDetailBean.f5731C = C2852b.m6550i();
        crashDetailBean.f5732D = C2852b.m6546g();
        crashDetailBean.f5733E = C2852b.m6554k();
        crashDetailBean.f5734F = this.f5872d.mo34319p();
        crashDetailBean.f5735G = this.f5872d.mo34318o();
        crashDetailBean.f5736H = this.f5872d.mo34320q();
        crashDetailBean.f5774w = C2908aq.m6899a(this.f5869a, C2869c.f5825e, C2869c.f5828h);
        crashDetailBean.f5776y = C2905ap.m6877a();
        Object[] objArr = new Object[1];
        objArr[0] = Integer.valueOf(crashDetailBean.f5776y == null ? 0 : crashDetailBean.f5776y.length);
        C2903an.m6857a("user log size:%d", objArr);
        crashDetailBean.f5753b = z ? 0 : 2;
        crashDetailBean.f5756e = this.f5872d.mo34311h();
        crashDetailBean.f5757f = this.f5872d.f5671p;
        crashDetailBean.f5758g = this.f5872d.mo34326w();
        crashDetailBean.f5764m = this.f5872d.mo34309g();
        String name = th.getClass().getName();
        String b = m6686b(th2, 1000);
        if (b == null) {
            b = "";
        }
        Object[] objArr2 = new Object[2];
        objArr2[0] = Integer.valueOf(th.getStackTrace().length);
        objArr2[1] = Boolean.valueOf(th.getCause() != null);
        C2903an.m6865e("stack frame :%d, has cause %b", objArr2);
        if (th.getStackTrace().length > 0) {
            str2 = th.getStackTrace()[0].toString();
        } else {
            str2 = "";
        }
        Throwable th3 = th2;
        while (th3 != null && th3.getCause() != null) {
            th3 = th3.getCause();
        }
        if (th3 == null || th3 == th2) {
            crashDetailBean.f5765n = name;
            crashDetailBean.f5766o = b + "" + str5;
            if (crashDetailBean.f5766o == null) {
                crashDetailBean.f5766o = "";
            }
            crashDetailBean.f5767p = str2;
            str3 = m6683a(th2, C2869c.f5826f);
            crashDetailBean.f5768q = str3;
        } else {
            crashDetailBean.f5765n = th3.getClass().getName();
            crashDetailBean.f5766o = m6686b(th3, 1000);
            if (crashDetailBean.f5766o == null) {
                crashDetailBean.f5766o = "";
            }
            if (th3.getStackTrace().length > 0) {
                crashDetailBean.f5767p = th3.getStackTrace()[0].toString();
            }
            StringBuilder sb = new StringBuilder();
            sb.append(name);
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(b);
            sb.append("\n");
            sb.append(str2);
            sb.append("\n......");
            sb.append("\nCaused by:\n");
            sb.append(crashDetailBean.f5765n);
            sb.append(Config.TRACE_TODAY_VISIT_SPLIT);
            sb.append(crashDetailBean.f5766o);
            sb.append("\n");
            str3 = m6683a(th3, C2869c.f5826f);
            sb.append(str3);
            crashDetailBean.f5768q = sb.toString();
        }
        crashDetailBean.f5769r = System.currentTimeMillis();
        crashDetailBean.f5772u = C2908aq.m6926b(crashDetailBean.f5768q.getBytes());
        try {
            crashDetailBean.f5777z = C2908aq.m6907a(C2869c.f5826f, false);
            crashDetailBean.f5729A = this.f5872d.f5660e;
            crashDetailBean.f5730B = thread.getName() + "(" + thread.getId() + ")";
            crashDetailBean.f5777z.put(crashDetailBean.f5730B, str3);
            crashDetailBean.f5737I = this.f5872d.mo34328y();
            crashDetailBean.f5759h = this.f5872d.mo34325v();
            crashDetailBean.f5760i = this.f5872d.mo34280J();
            crashDetailBean.f5742N = this.f5872d.f5625a;
            crashDetailBean.f5743O = this.f5872d.mo34295a();
            crashDetailBean.f5745Q = this.f5872d.mo34278H();
            crashDetailBean.f5746R = this.f5872d.mo34279I();
            crashDetailBean.f5747S = this.f5872d.mo34272B();
            crashDetailBean.f5748T = this.f5872d.mo34277G();
        } catch (Throwable th4) {
            C2903an.m6865e("handle crash error %s", th4.toString());
        }
        if (z) {
            this.f5870b.mo34397c(crashDetailBean);
        } else {
            boolean z3 = str4 != null && str.length() > 0;
            if (bArr2 != null && bArr2.length > 0) {
                z2 = true;
            }
            if (z3) {
                crashDetailBean.f5744P = new HashMap(1);
                crashDetailBean.f5744P.put("UserData", str4);
            }
            if (z2) {
                crashDetailBean.f5749U = bArr2;
            }
        }
        return crashDetailBean;
    }

    /* renamed from: a */
    private boolean m6685a(Thread thread) {
        synchronized (f5868i) {
            if (f5867h != null) {
                if (thread.getName().equals(f5867h)) {
                    return true;
                }
            }
            f5867h = thread.getName();
            return false;
        }
    }

    /* renamed from: b */
    public void mo34429b(Thread thread, Throwable th, boolean z, String str, byte[] bArr) {
        String str2;
        Thread thread2 = thread;
        Throwable th2 = th;
        boolean z2 = z;
        if (z2) {
            C2903an.m6865e("Java Crash Happen cause by %s(%d)", thread.getName(), Long.valueOf(thread.getId()));
            if (m6685a(thread)) {
                C2903an.m6857a("this class has handled this exception", new Object[0]);
                if (this.f5874f != null) {
                    C2903an.m6857a("call system handler", new Object[0]);
                    this.f5874f.uncaughtException(thread2, th2);
                } else {
                    mo34427a(thread, th);
                }
            }
        } else {
            C2903an.m6865e("Java Catch Happen", new Object[0]);
        }
        try {
            if (!this.f5875g) {
                C2903an.m6863c("Java crash handler is disable. Just return.", new Object[0]);
                if (z2) {
                    Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.f5873e;
                    if (uncaughtExceptionHandler != null && m6684a(uncaughtExceptionHandler)) {
                        C2903an.m6865e("sys default last handle start!", new Object[0]);
                        this.f5873e.uncaughtException(thread2, th2);
                        C2903an.m6865e("sys default last handle end!", new Object[0]);
                    } else if (this.f5874f != null) {
                        C2903an.m6865e("system handle start!", new Object[0]);
                        this.f5874f.uncaughtException(thread2, th2);
                        C2903an.m6865e("system handle end!", new Object[0]);
                    } else {
                        C2903an.m6865e("crashreport last handle start!", new Object[0]);
                        mo34427a(thread, th);
                        C2903an.m6865e("crashreport last handle end!", new Object[0]);
                    }
                }
            } else {
                if (!this.f5871c.mo34339b()) {
                    C2903an.m6864d("no remote but still store!", new Object[0]);
                }
                String str3 = "JAVA_CRASH";
                if (!this.f5871c.mo34340c().f5693g) {
                    if (this.f5871c.mo34339b()) {
                        C2903an.m6865e("crash report was closed by remote , will not upload to Bugly , print local for helpful!", new Object[0]);
                        if (z2) {
                            str2 = str3;
                        } else {
                            str2 = "JAVA_CATCH";
                        }
                        C2867b.m6631a(str2, C2908aq.m6897a(), this.f5872d.f5660e, thread.getName(), C2908aq.m6902a(th), null);
                        if (z2) {
                            Thread.UncaughtExceptionHandler uncaughtExceptionHandler2 = this.f5873e;
                            if (uncaughtExceptionHandler2 != null && m6684a(uncaughtExceptionHandler2)) {
                                C2903an.m6865e("sys default last handle start!", new Object[0]);
                                this.f5873e.uncaughtException(thread2, th2);
                                C2903an.m6865e("sys default last handle end!", new Object[0]);
                                return;
                            } else if (this.f5874f != null) {
                                C2903an.m6865e("system handle start!", new Object[0]);
                                this.f5874f.uncaughtException(thread2, th2);
                                C2903an.m6865e("system handle end!", new Object[0]);
                                return;
                            } else {
                                C2903an.m6865e("crashreport last handle start!", new Object[0]);
                                mo34427a(thread, th);
                                C2903an.m6865e("crashreport last handle end!", new Object[0]);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                }
                CrashDetailBean a = mo34424a(thread, th, z, str, bArr);
                if (a == null) {
                    C2903an.m6865e("pkg crash datas fail!", new Object[0]);
                    if (z2) {
                        Thread.UncaughtExceptionHandler uncaughtExceptionHandler3 = this.f5873e;
                        if (uncaughtExceptionHandler3 != null && m6684a(uncaughtExceptionHandler3)) {
                            C2903an.m6865e("sys default last handle start!", new Object[0]);
                            this.f5873e.uncaughtException(thread2, th2);
                            C2903an.m6865e("sys default last handle end!", new Object[0]);
                        } else if (this.f5874f != null) {
                            C2903an.m6865e("system handle start!", new Object[0]);
                            this.f5874f.uncaughtException(thread2, th2);
                            C2903an.m6865e("system handle end!", new Object[0]);
                        } else {
                            C2903an.m6865e("crashreport last handle start!", new Object[0]);
                            mo34427a(thread, th);
                            C2903an.m6865e("crashreport last handle end!", new Object[0]);
                        }
                    }
                } else {
                    if (!z2) {
                        str3 = "JAVA_CATCH";
                    }
                    C2867b.m6631a(str3, C2908aq.m6897a(), this.f5872d.f5660e, thread.getName(), C2908aq.m6902a(th), a);
                    if (!this.f5870b.mo34391a(a)) {
                        this.f5870b.mo34388a(a, 3000, z2);
                    }
                    if (z2) {
                        this.f5870b.mo34396b(a);
                    }
                    if (z2) {
                        Thread.UncaughtExceptionHandler uncaughtExceptionHandler4 = this.f5873e;
                        if (uncaughtExceptionHandler4 != null && m6684a(uncaughtExceptionHandler4)) {
                            C2903an.m6865e("sys default last handle start!", new Object[0]);
                            this.f5873e.uncaughtException(thread2, th2);
                            C2903an.m6865e("sys default last handle end!", new Object[0]);
                        } else if (this.f5874f != null) {
                            C2903an.m6865e("system handle start!", new Object[0]);
                            this.f5874f.uncaughtException(thread2, th2);
                            C2903an.m6865e("system handle end!", new Object[0]);
                        } else {
                            C2903an.m6865e("crashreport last handle start!", new Object[0]);
                            mo34427a(thread, th);
                            C2903an.m6865e("crashreport last handle end!", new Object[0]);
                        }
                    }
                }
            }
        } catch (Throwable th3) {
            if (z2) {
                Thread.UncaughtExceptionHandler uncaughtExceptionHandler5 = this.f5873e;
                if (uncaughtExceptionHandler5 != null && m6684a(uncaughtExceptionHandler5)) {
                    C2903an.m6865e("sys default last handle start!", new Object[0]);
                    this.f5873e.uncaughtException(thread2, th2);
                    C2903an.m6865e("sys default last handle end!", new Object[0]);
                } else if (this.f5874f != null) {
                    C2903an.m6865e("system handle start!", new Object[0]);
                    this.f5874f.uncaughtException(thread2, th2);
                    C2903an.m6865e("system handle end!", new Object[0]);
                } else {
                    C2903an.m6865e("crashreport last handle start!", new Object[0]);
                    mo34427a(thread, th);
                    C2903an.m6865e("crashreport last handle end!", new Object[0]);
                }
            }
            throw th3;
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        synchronized (f5868i) {
            mo34429b(thread, th, true, null, null);
        }
    }

    /* renamed from: a */
    private boolean m6684a(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        if (uncaughtExceptionHandler == null) {
            return true;
        }
        String name = uncaughtExceptionHandler.getClass().getName();
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        for (StackTraceElement stackTraceElement : stackTrace) {
            String className = stackTraceElement.getClassName();
            String methodName = stackTraceElement.getMethodName();
            if (name.equals(className) && "uncaughtException".equals(methodName)) {
                return false;
            }
        }
        return true;
    }

    /* renamed from: a */
    public synchronized void mo34426a(StrategyBean strategyBean) {
        if (strategyBean != null) {
            if (strategyBean.f5693g != this.f5875g) {
                C2903an.m6857a("java changed to %b", Boolean.valueOf(strategyBean.f5693g));
                if (strategyBean.f5693g) {
                    mo34425a();
                } else {
                    mo34428b();
                }
            }
        }
    }

    /* renamed from: a */
    public static String m6683a(Throwable th, int i) {
        if (th == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        try {
            if (th.getStackTrace() != null) {
                StackTraceElement[] stackTrace = th.getStackTrace();
                int length = stackTrace.length;
                int i2 = 0;
                while (i2 < length) {
                    StackTraceElement stackTraceElement = stackTrace[i2];
                    if (i <= 0 || sb.length() < i) {
                        sb.append(stackTraceElement.toString());
                        sb.append("\n");
                        i2++;
                    } else {
                        sb.append("\n[Stack over limit size :" + i + " , has been cutted !]");
                        return sb.toString();
                    }
                }
            }
        } catch (Throwable th2) {
            C2903an.m6865e("gen stack error %s", th2.toString());
        }
        return sb.toString();
    }

    /* renamed from: b */
    public static String m6686b(Throwable th, int i) {
        if (th.getMessage() == null) {
            return "";
        }
        if (i < 0 || th.getMessage().length() <= i) {
            return th.getMessage();
        }
        return th.getMessage().substring(0, i) + "\n[Message over limit size:" + i + ", has been cutted!]";
    }
}
