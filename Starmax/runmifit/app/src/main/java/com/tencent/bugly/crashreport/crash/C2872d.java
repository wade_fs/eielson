package com.tencent.bugly.crashreport.crash;

import android.content.Context;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.info.C2852b;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2905ap;
import com.tencent.bugly.proguard.C2908aq;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.crash.d */
/* compiled from: BUGLY */
public class C2872d {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static C2872d f5855a;

    /* renamed from: b */
    private C2854a f5856b;

    /* renamed from: c */
    private C2851a f5857c;

    /* renamed from: d */
    private C2867b f5858d;

    /* renamed from: e */
    private Context f5859e;

    private C2872d(Context context) {
        C2869c a = C2869c.m6653a();
        if (a != null) {
            this.f5856b = C2854a.m6574a();
            this.f5857c = C2851a.m6470a(context);
            this.f5858d = a.f5837p;
            this.f5859e = context;
            C2901am.m6848a().mo34547a(new Runnable() {
                /* class com.tencent.bugly.crashreport.crash.C2872d.C28731 */

                public void run() {
                    C2872d.this.m6681b();
                }
            });
        }
    }

    /* renamed from: a */
    public static C2872d m6676a(Context context) {
        if (f5855a == null) {
            f5855a = new C2872d(context);
        }
        return f5855a;
    }

    /* renamed from: a */
    public static void m6679a(Thread thread, int i, String str, String str2, String str3, Map<String, String> map) {
        final Thread thread2 = thread;
        final int i2 = i;
        final String str4 = str;
        final String str5 = str2;
        final String str6 = str3;
        final Map<String, String> map2 = map;
        C2901am.m6848a().mo34547a(new Runnable() {
            /* class com.tencent.bugly.crashreport.crash.C2872d.C28742 */

            public void run() {
                try {
                    if (C2872d.f5855a == null) {
                        C2903an.m6865e("[ExtraCrashManager] Extra crash manager has not been initialized.", new Object[0]);
                    } else {
                        C2872d.f5855a.m6682c(thread2, i2, str4, str5, str6, map2);
                    }
                } catch (Throwable th) {
                    if (!C2903an.m6861b(th)) {
                        th.printStackTrace();
                    }
                    C2903an.m6865e("[ExtraCrashManager] Crash error %s %s %s", str4, str5, str6);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m6681b() {
        C2903an.m6863c("[ExtraCrashManager] Trying to notify Bugly agents.", new Object[0]);
        try {
            Class<?> cls = Class.forName("com.tencent.bugly.proguard.o");
            this.f5857c.getClass();
            C2908aq.m6911a(cls, "sdkPackageName", "com.tencent.bugly", (Object) null);
            C2903an.m6863c("[ExtraCrashManager] Bugly game agent has been notified.", new Object[0]);
        } catch (Throwable unused) {
            C2903an.m6857a("[ExtraCrashManager] no game agent", new Object[0]);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String>
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.aq.a(java.lang.String, android.content.Context):android.content.SharedPreferences
      com.tencent.bugly.proguard.aq.a(java.lang.String, java.lang.String):java.io.BufferedReader
      com.tencent.bugly.proguard.aq.a(byte[], android.os.Parcelable$Creator):T
      com.tencent.bugly.proguard.aq.a(java.io.File, java.lang.String):java.lang.String
      com.tencent.bugly.proguard.aq.a(java.lang.Runnable, java.lang.String):java.lang.Thread
      com.tencent.bugly.proguard.aq.a(android.content.Context, java.lang.String):java.util.ArrayList<java.lang.String>
      com.tencent.bugly.proguard.aq.a(android.os.Parcel, java.util.Map<java.lang.String, com.tencent.bugly.crashreport.common.info.PlugInBean>):void
      com.tencent.bugly.proguard.aq.a(byte[], int):byte[]
      com.tencent.bugly.proguard.aq.a(int, boolean):java.util.Map<java.lang.String, java.lang.String> */
    /* renamed from: b */
    private CrashDetailBean m6680b(Thread thread, int i, String str, String str2, String str3, Map<String, String> map) {
        CrashDetailBean crashDetailBean = new CrashDetailBean();
        crashDetailBean.f5731C = C2852b.m6550i();
        crashDetailBean.f5732D = C2852b.m6546g();
        crashDetailBean.f5733E = C2852b.m6554k();
        crashDetailBean.f5734F = this.f5857c.mo34319p();
        crashDetailBean.f5735G = this.f5857c.mo34318o();
        crashDetailBean.f5736H = this.f5857c.mo34320q();
        crashDetailBean.f5774w = C2908aq.m6899a(this.f5859e, C2869c.f5825e, C2869c.f5828h);
        crashDetailBean.f5753b = i;
        crashDetailBean.f5756e = this.f5857c.mo34311h();
        crashDetailBean.f5757f = this.f5857c.f5671p;
        crashDetailBean.f5758g = this.f5857c.mo34326w();
        crashDetailBean.f5764m = this.f5857c.mo34309g();
        StringBuilder sb = new StringBuilder();
        String str4 = "";
        sb.append(str4);
        sb.append(str);
        crashDetailBean.f5765n = sb.toString();
        crashDetailBean.f5766o = str4 + str2;
        if (str3 != null) {
            String[] split = str3.split("\n");
            if (split.length > 0) {
                str4 = split[0];
            }
        } else {
            str3 = str4;
        }
        crashDetailBean.f5767p = str4;
        crashDetailBean.f5768q = str3;
        crashDetailBean.f5769r = System.currentTimeMillis();
        crashDetailBean.f5772u = C2908aq.m6926b(crashDetailBean.f5768q.getBytes());
        crashDetailBean.f5777z = C2908aq.m6907a(C2869c.f5826f, false);
        crashDetailBean.f5729A = this.f5857c.f5660e;
        crashDetailBean.f5730B = thread.getName() + "(" + thread.getId() + ")";
        crashDetailBean.f5737I = this.f5857c.mo34328y();
        crashDetailBean.f5759h = this.f5857c.mo34325v();
        crashDetailBean.f5742N = this.f5857c.f5625a;
        crashDetailBean.f5743O = this.f5857c.mo34295a();
        crashDetailBean.f5745Q = this.f5857c.mo34278H();
        crashDetailBean.f5746R = this.f5857c.mo34279I();
        crashDetailBean.f5747S = this.f5857c.mo34272B();
        crashDetailBean.f5748T = this.f5857c.mo34277G();
        this.f5858d.mo34397c(crashDetailBean);
        crashDetailBean.f5776y = C2905ap.m6877a();
        if (crashDetailBean.f5744P == null) {
            crashDetailBean.f5744P = new LinkedHashMap();
        }
        if (map != null) {
            crashDetailBean.f5744P.putAll(map);
        }
        return crashDetailBean;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void
     arg types: [com.tencent.bugly.crashreport.crash.CrashDetailBean, int, int]
     candidates:
      com.tencent.bugly.crashreport.crash.b.a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.be
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, com.tencent.bugly.crashreport.crash.CrashDetailBean, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bf
      com.tencent.bugly.crashreport.crash.b.a(android.content.Context, java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean>, com.tencent.bugly.crashreport.common.info.a):com.tencent.bugly.proguard.bg
      com.tencent.bugly.crashreport.crash.b.a(com.tencent.bugly.crashreport.crash.CrashDetailBean, long, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m6682c(Thread thread, int i, String str, String str2, String str3, Map<String, String> map) {
        Thread thread2;
        String str4;
        int i2 = i;
        String str5 = str;
        String str6 = str2;
        String str7 = str3;
        if (thread == null) {
            thread2 = Thread.currentThread();
        } else {
            thread2 = thread;
        }
        if (i2 == 4) {
            str4 = "Unity";
        } else if (i2 == 5 || i2 == 6) {
            str4 = "Cocos";
        } else if (i2 != 8) {
            C2903an.m6864d("[ExtraCrashManager] Unknown extra crash type: %d", Integer.valueOf(i));
            return;
        } else {
            str4 = "H5";
        }
        String str8 = str4;
        C2903an.m6865e("[ExtraCrashManager] %s Crash Happen", str8);
        try {
            if (!this.f5856b.mo34339b()) {
                C2903an.m6864d("[ExtraCrashManager] There is no remote strategy, but still store it.", new Object[0]);
            }
            StrategyBean c = this.f5856b.mo34340c();
            if (!c.f5693g) {
                if (this.f5856b.mo34339b()) {
                    C2903an.m6865e("[ExtraCrashManager] Crash report was closed by remote , will not upload to Bugly , print local for helpful!", new Object[0]);
                    String a = C2908aq.m6897a();
                    String str9 = this.f5857c.f5660e;
                    String name = thread2.getName();
                    C2867b.m6631a(str8, a, str9, name, str5 + "\n" + str6 + "\n" + str7, null);
                    return;
                }
            }
            if (i2 != 4) {
                if (i2 == 5 || i2 == 6) {
                    if (!c.f5698l) {
                        C2903an.m6865e("[ExtraCrashManager] %s report is disabled.", str8);
                        C2903an.m6865e("[ExtraCrashManager] Successfully handled.", new Object[0]);
                        return;
                    }
                } else if (i2 == 8) {
                    if (!c.f5699m) {
                        C2903an.m6865e("[ExtraCrashManager] %s report is disabled.", str8);
                        C2903an.m6865e("[ExtraCrashManager] Successfully handled.", new Object[0]);
                        return;
                    }
                }
            }
            int i3 = i2 == 8 ? 5 : i2;
            String str10 = "\n";
            CrashDetailBean b = m6680b(thread2, i3, str, str2, str3, map);
            if (b == null) {
                C2903an.m6865e("[ExtraCrashManager] Failed to package crash data.", new Object[0]);
                C2903an.m6865e("[ExtraCrashManager] Successfully handled.", new Object[0]);
                return;
            }
            String a2 = C2908aq.m6897a();
            String str11 = this.f5857c.f5660e;
            String name2 = thread2.getName();
            C2867b.m6631a(str8, a2, str11, name2, str5 + str10 + str6 + str10 + str7, b);
            if (!this.f5858d.mo34391a(b)) {
                this.f5858d.mo34388a(b, 3000L, false);
            }
            C2903an.m6865e("[ExtraCrashManager] Successfully handled.", new Object[0]);
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        } finally {
            C2903an.m6865e("[ExtraCrashManager] Successfully handled.", new Object[0]);
        }
    }
}
