package com.tencent.bugly.proguard;

import java.security.NoSuchAlgorithmException;
import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;

/* renamed from: com.tencent.bugly.proguard.az */
/* compiled from: BUGLY */
public class C2918az implements C2921bb {

    /* renamed from: a */
    private String f6069a = null;

    /* renamed from: a */
    public byte[] mo34575a(byte[] bArr) throws Exception {
        if (this.f6069a == null || bArr == null) {
            return null;
        }
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(2, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f6069a.getBytes("UTF-8"))), new IvParameterSpec(this.f6069a.getBytes("UTF-8")));
        return instance.doFinal(bArr);
    }

    /* renamed from: b */
    public byte[] mo34576b(byte[] bArr) throws Exception, NoSuchAlgorithmException {
        if (this.f6069a == null || bArr == null) {
            return null;
        }
        Cipher instance = Cipher.getInstance("DES/CBC/PKCS5Padding");
        instance.init(1, SecretKeyFactory.getInstance("DES").generateSecret(new DESKeySpec(this.f6069a.getBytes("UTF-8"))), new IvParameterSpec(this.f6069a.getBytes("UTF-8")));
        return instance.doFinal(bArr);
    }

    /* renamed from: a */
    public void mo34574a(String str) {
        if (str != null) {
            this.f6069a = str;
        }
    }
}
