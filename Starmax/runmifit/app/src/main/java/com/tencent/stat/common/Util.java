package com.tencent.stat.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.WindowManager;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceConstant;
import com.tencent.mid.api.MidService;
import com.tencent.stat.StatConfig;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class Util {

    /* renamed from: a */
    private static String f7221a = "";

    public static boolean checkPermission(Context context, String str) {
        try {
            if (context.getPackageManager().checkPermission(str, context.getPackageName()) == 0) {
                return true;
            }
            return false;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "checkPermission error", th);
            return false;
        }
    }

    public static String getSimOperator(Context context) {
        try {
            if (checkPermission(context, "android.permission.READ_PHONE_STATE")) {
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
                if (telephonyManager != null) {
                    return telephonyManager.getSimOperator();
                }
                return null;
            }
            Log.e(StatConstants.LOG_TAG, "Could not get permission of android.permission.READ_PHONE_STATE");
            return null;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "", th);
            return null;
        }
    }

    public static String getDeviceModel() {
        return Build.MODEL;
    }

    public static Integer getTelephonyNetworkType(Context context) {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager != null) {
                return Integer.valueOf(telephonyManager.getNetworkType());
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String getExternalStorageInfo(Context context) {
        String path;
        try {
            if (checkPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE")) {
                String externalStorageState = Environment.getExternalStorageState();
                if (!(externalStorageState == null || !externalStorageState.equals("mounted") || (path = Environment.getExternalStorageDirectory().getPath()) == null)) {
                    StatFs statFs = new StatFs(path);
                    return String.valueOf((((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / 1000000) + "/" + String.valueOf((((long) statFs.getBlockCount()) * ((long) statFs.getBlockSize())) / 1000000);
                }
                return null;
            }
            Log.e(StatConstants.LOG_TAG, "can not get the permission of android.permission.WRITE_EXTERNAL_STORAGE");
            return null;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "", th);
        }
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    public static String getLinkedWay(Context context) {
        try {
            if (!checkPermission(context, "android.permission.INTERNET") || !checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                Log.e(StatConstants.LOG_TAG, "can not get the permission of android.permission.ACCESS_WIFI_STATE");
                return null;
            }
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return null;
            }
            if (!activeNetworkInfo.isConnected()) {
                return null;
            }
            String typeName = activeNetworkInfo.getTypeName();
            String extraInfo = activeNetworkInfo.getExtraInfo();
            if (typeName == null) {
                return null;
            }
            if (typeName.equalsIgnoreCase("WIFI")) {
                return "WIFI";
            }
            if (typeName.equalsIgnoreCase("MOBILE")) {
                if (extraInfo == null || extraInfo.trim().length() <= 0) {
                    return "MOBILE";
                }
            } else if (extraInfo == null || extraInfo.trim().length() <= 0) {
                return typeName;
            }
            return extraInfo;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "", th);
            return null;
        }
    }

    public static String getDeviceID(Context context) {
        try {
            if (checkPermission(context, "android.permission.READ_PHONE_STATE")) {
                String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
                if (deviceId != null) {
                    return deviceId;
                }
                return null;
            }
            Log.e(StatConstants.LOG_TAG, "Could not get permission of android.permission.READ_PHONE_STATE");
            return null;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "get device id error", th);
            return null;
        }
    }

    public static String getWifiMacAddress(Context context) {
        if (checkPermission(context, "android.permission.ACCESS_WIFI_STATE")) {
            try {
                WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
                if (wifiManager == null) {
                    return "";
                }
                return wifiManager.getConnectionInfo().getMacAddress();
            } catch (Exception e) {
                Log.e(StatConstants.LOG_TAG, "get wifi address error", e);
                return "";
            }
        } else {
            Log.e(StatConstants.LOG_TAG, "Could not get permission of android.permission.ACCESS_WIFI_STATE");
            return "";
        }
    }

    public static String decode(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(C3210e.m7987b(C3213h.m7999a(str.getBytes("UTF-8"), 0)), "UTF-8");
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "decode error", th);
            return str;
        }
    }

    public static String encode(String str) {
        if (str == null) {
            return null;
        }
        if (Build.VERSION.SDK_INT < 8) {
            return str;
        }
        try {
            return new String(C3213h.m8001b(C3210e.m7985a(str.getBytes("UTF-8")), 0), "UTF-8");
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "encode error", th);
            return str;
        }
    }

    public static void jsonPut(JSONObject jSONObject, String str, String str2) {
        if (str2 != null) {
            try {
                if (str2.length() > 0) {
                    jSONObject.put(str, str2);
                }
            } catch (Throwable th) {
                Log.e(StatConstants.LOG_TAG, "jsonPut error", th);
            }
        }
    }

    public static void safeJsonPut(JSONObject jSONObject, String str, Object obj) {
        try {
            jSONObject.put(str, obj);
        } catch (Exception e) {
            Log.e(StatConstants.LOG_TAG, "safeJsonPut error", e);
        }
    }

    public static WifiInfo getWifiInfo(Context context) {
        WifiManager wifiManager;
        if (!checkPermission(context, "android.permission.ACCESS_WIFI_STATE") || (wifiManager = (WifiManager) context.getApplicationContext().getSystemService("wifi")) == null) {
            return null;
        }
        return wifiManager.getConnectionInfo();
    }

    public static JSONObject getConnecetedWifiInfo(Context context) {
        WifiInfo wifiInfo = getWifiInfo(context);
        if (wifiInfo == null) {
            return null;
        }
        try {
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("bs", wifiInfo.getBSSID());
            jSONObject.put("ss", wifiInfo.getSSID());
            jSONObject.put("dBm", wifiInfo.getRssi());
            return jSONObject;
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String getWiFiBBSID(Context context) {
        try {
            WifiInfo wifiInfo = getWifiInfo(context);
            if (wifiInfo != null) {
                return wifiInfo.getBSSID();
            }
            return null;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "encode error", th);
            return null;
        }
    }

    public static String getWiFiSSID(Context context) {
        try {
            WifiInfo wifiInfo = getWifiInfo(context);
            if (wifiInfo != null) {
                return wifiInfo.getSSID();
            }
            return null;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "encode error", th);
            return null;
        }
    }

    public static boolean isNetworkAvailable(Context context) {
        try {
            if (!checkPermission(context, "android.permission.INTERNET") || !checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                Log.e(StatConstants.LOG_TAG, "can not get the permisson of android.permission.INTERNET");
                return false;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager != null) {
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isAvailable()) {
                    return true;
                }
                Log.w(StatConstants.LOG_TAG, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_NETWORK_ERROR);
                return false;
            }
            return false;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "isNetworkAvailable error", th);
        }
    }

    public static boolean isWifiNet(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            if (!checkPermission(context, "android.permission.INTERNET") || !checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                Log.e(StatConstants.LOG_TAG, "can not get the permisson of android.permission.INTERNET");
                return false;
            }
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable() || !activeNetworkInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "isWifiNet error", th);
        }
    }

    public static JSONArray getWifiTopN(Context context, int i) {
        List<ScanResult> scanResults;
        try {
            if (!StatConfig.isEnableReportWifiList()) {
                return null;
            }
            if (!checkPermission(context, "android.permission.INTERNET") || !checkPermission(context, "android.permission.ACCESS_NETWORK_STATE")) {
                Log.e(StatConstants.LOG_TAG, "can not get the permisson of android.permission.INTERNET");
                return null;
            }
            WifiManager wifiManager = (WifiManager) context.getSystemService("wifi");
            if (!(wifiManager == null || (scanResults = wifiManager.getScanResults()) == null || scanResults.size() <= 0)) {
                Collections.sort(scanResults, new Comparator<ScanResult>() {
                    /* class com.tencent.stat.common.Util.C32031 */

                    /* renamed from: a */
                    public int compare(ScanResult scanResult, ScanResult scanResult2) {
                        int abs = Math.abs(scanResult.level);
                        int abs2 = Math.abs(scanResult2.level);
                        if (abs > abs2) {
                            return 1;
                        }
                        return abs == abs2 ? 0 : -1;
                    }
                });
                JSONArray jSONArray = new JSONArray();
                int i2 = 0;
                while (true) {
                    if (i2 >= scanResults.size()) {
                        break;
                    } else if (i2 >= i) {
                        break;
                    } else {
                        ScanResult scanResult = scanResults.get(i2);
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("bs", scanResult.BSSID);
                        jSONObject.put("ss", scanResult.SSID);
                        jSONObject.put("dBm", scanResult.level);
                        jSONArray.put(jSONObject);
                        i2++;
                    }
                }
                return jSONArray;
            }
            return null;
        } catch (Throwable th) {
            Log.e(StatConstants.LOG_TAG, "isWifiNet error", th);
        }
    }

    public static String getMid(Context context) {
        if (com.tencent.mid.util.Util.isMidValid(f7221a)) {
            return f7221a;
        }
        return MidService.getMid(context);
    }
}
