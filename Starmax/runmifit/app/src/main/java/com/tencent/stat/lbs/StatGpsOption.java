package com.tencent.stat.lbs;

public class StatGpsOption {

    /* renamed from: a */
    private long f7427a = 1800000;

    /* renamed from: b */
    private float f7428b = 100.0f;

    public long getMinTime() {
        return this.f7427a;
    }

    public void setMinTime(long j) {
        this.f7427a = j;
    }

    public float getMinDistance() {
        return this.f7428b;
    }

    public void setMinDistance(float f) {
        this.f7428b = f;
    }
}
