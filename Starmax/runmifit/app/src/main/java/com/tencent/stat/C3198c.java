package com.tencent.stat;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.tencent.mid.util.Util;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatConstants;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.StatPreferences;
import freemarker.ext.servlet.FreemarkerServlet;
import java.io.InputStream;
import java.lang.reflect.Method;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.c */
public class C3198c {

    /* renamed from: a */
    private static volatile boolean f7177a = false;

    /* renamed from: b */
    private static StatLogger f7178b = StatCommonHelper.getLogger();

    /* renamed from: a */
    public static void m7941a(Context context) {
        if (m7945c(context)) {
            Log.w(StatConstants.LOG_TAG, "MtaSmartStat is self kill!");
            StatConfig.addCustomReportKeyValue("SSS", "-1");
            return;
        }
        StatPreferences.putLong(context, "mta.smart.start.ts", System.currentTimeMillis());
        try {
            String customProperty = StatConfig.getCustomProperty(context, "MtaSmartStat", "1");
            if (!Util.isEmpty(customProperty) && !"0".equals(customProperty)) {
                m7947d(context);
                StatConfig.addCustomReportKeyValue("SSS", "1");
                Log.i(StatConstants.LOG_TAG, "Smart Provider is in use.");
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        StatPreferences.putLong(context, "mta.smart.end.ts", System.currentTimeMillis());
    }

    /* renamed from: b */
    public static Application m7942b(Context context) {
        try {
            Method declaredMethod = Class.forName("android.app.ActivityThread").getDeclaredMethod("currentApplication", new Class[0]);
            declaredMethod.setAccessible(true);
            return (Application) declaredMethod.invoke(null, new Object[0]);
        } catch (Throwable th) {
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static void m7938a(Application application) {
        if (application != null) {
            m7943b(application);
            m7946d(application);
        }
    }

    /* renamed from: d */
    private static void m7946d(Application application) {
        StatService.registerActivityLifecycleCallbacks(application);
    }

    /* renamed from: b */
    public static void m7943b(Application application) {
        m7939a(application, m7948e(application));
        m7939a(application, m7944c(application));
    }

    /* renamed from: a */
    private static void m7939a(Application application, JSONArray jSONArray) {
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    StatLogger statLogger = f7178b;
                    statLogger.mo35381d("try to invoke module:" + jSONObject);
                    m7940a(application, jSONObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* renamed from: c */
    public static JSONArray m7944c(Application application) {
        try {
            return new JSONArray(StatConfig.getCustomProperty(application, "SmartStartModules", ""));
        } catch (Throwable unused) {
            return null;
        }
    }

    /* renamed from: c */
    public static boolean m7945c(Context context) {
        return StatPreferences.contains(context, "mta.smart.start.ts") && !StatPreferences.contains(context, "mta.smart.end.ts");
    }

    /* renamed from: e */
    private static JSONArray m7948e(Application application) {
        JSONArray jSONArray;
        JSONArray jSONArray2 = new JSONArray();
        try {
            InputStream open = application.getAssets().open("MTA_SMART_MODULE");
            byte[] bArr = new byte[open.available()];
            open.read(bArr);
            open.close();
            String str = new String(bArr);
            jSONArray = new JSONArray(str);
            try {
                StatLogger statLogger = f7178b;
                statLogger.mo35381d("readModuleConfigFile:" + str);
            } catch (Exception e) {
                e = e;
            }
        } catch (Exception e2) {
            e = e2;
            jSONArray = jSONArray2;
            e.printStackTrace();
            return jSONArray;
        }
        return jSONArray;
    }

    /* renamed from: a */
    private static void m7940a(Application application, JSONObject jSONObject) {
        Method method;
        char c;
        try {
            Class<?> cls = Class.forName(jSONObject.getString("class"));
            String string = jSONObject.getString("method");
            JSONArray jSONArray = new JSONArray(jSONObject.getString("args"));
            int length = jSONArray.length();
            Object[] objArr = new Object[length];
            if (length > 0) {
                Class[] clsArr = new Class[length];
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject2 = jSONArray.getJSONObject(i);
                    String string2 = jSONObject2.getString("cname");
                    String string3 = jSONObject2.getString("cvalue");
                    switch (string2.hashCode()) {
                        case -1808118735:
                            if (string2.equals("String")) {
                                c = 8;
                                break;
                            }
                            c = 65535;
                            break;
                        case -1678783089:
                            if (string2.equals("Context")) {
                                c = 9;
                                break;
                            }
                            c = 65535;
                            break;
                        case -1072845520:
                            if (string2.equals(FreemarkerServlet.KEY_APPLICATION)) {
                                c = 10;
                                break;
                            }
                            c = 65535;
                            break;
                        case -672261858:
                            if (string2.equals("Integer")) {
                                c = 3;
                                break;
                            }
                            c = 65535;
                            break;
                        case 104431:
                            if (string2.equals("int")) {
                                c = 2;
                                break;
                            }
                            c = 65535;
                            break;
                        case 2374300:
                            if (string2.equals("Long")) {
                                c = 7;
                                break;
                            }
                            c = 65535;
                            break;
                        case 3327612:
                            if (string2.equals("long")) {
                                c = 6;
                                break;
                            }
                            c = 65535;
                            break;
                        case 64711720:
                            if (string2.equals("boolean")) {
                                c = 1;
                                break;
                            }
                            c = 65535;
                            break;
                        case 67973692:
                            if (string2.equals("Float")) {
                                c = 5;
                                break;
                            }
                            c = 65535;
                            break;
                        case 97526364:
                            if (string2.equals("float")) {
                                c = 4;
                                break;
                            }
                            c = 65535;
                            break;
                        case 1729365000:
                            if (string2.equals("Boolean")) {
                                c = 0;
                                break;
                            }
                            c = 65535;
                            break;
                        default:
                            c = 65535;
                            break;
                    }
                    switch (c) {
                        case 0:
                        case 1:
                            clsArr[i] = Boolean.TYPE;
                            objArr[i] = Boolean.valueOf(string3);
                            break;
                        case 2:
                        case 3:
                            clsArr[i] = Integer.class;
                            objArr[i] = Integer.valueOf(string3);
                            break;
                        case 4:
                        case 5:
                            clsArr[i] = Float.class;
                            objArr[i] = Float.valueOf(string3);
                            break;
                        case 6:
                        case 7:
                            clsArr[i] = Long.class;
                            objArr[i] = Long.valueOf(string3);
                            break;
                        case 8:
                            clsArr[i] = String.class;
                            objArr[i] = string3;
                            break;
                        case 9:
                            clsArr[i] = Context.class;
                            objArr[i] = application.getApplicationContext();
                            break;
                        case 10:
                            clsArr[i] = Application.class;
                            objArr[i] = application;
                            break;
                    }
                }
                method = cls.getDeclaredMethod(string, clsArr);
            } else {
                method = cls.getDeclaredMethod(string, new Class[0]);
            }
            method.setAccessible(true);
            if (jSONObject.getInt("static") != 1) {
                Object newInstance = cls.newInstance();
                if (length > 0) {
                    method.invoke(newInstance, objArr);
                } else {
                    method.invoke(newInstance, new Object[0]);
                }
            } else if (length > 0) {
                method.invoke(null, objArr);
            } else {
                method.invoke(null, new Object[0]);
            }
            StatLogger statLogger = f7178b;
            statLogger.mo35381d("invokeSingleModule:" + jSONObject);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* renamed from: d */
    public static void m7947d(Context context) {
        if (!f7177a && StatCommonHelper.isMainProcess(context)) {
            m7938a(m7942b(context));
            f7177a = true;
        }
    }
}
