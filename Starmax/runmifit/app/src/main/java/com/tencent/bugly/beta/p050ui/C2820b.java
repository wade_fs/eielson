package com.tencent.bugly.beta.p050ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;

/* renamed from: com.tencent.bugly.beta.ui.b */
/* compiled from: BUGLY */
public abstract class C2820b extends Fragment {

    /* renamed from: m */
    protected boolean f5417m = false;

    /* renamed from: a */
    public abstract boolean mo34157a(int i, KeyEvent keyEvent);

    /* renamed from: b */
    public synchronized boolean mo34158b() {
        return this.f5417m;
    }

    /* renamed from: a */
    public synchronized void mo34150a() {
        FragmentActivity activity = getActivity();
        if (activity != null) {
            activity.finish();
        }
    }

    public void onResume() {
        super.onResume();
        synchronized (this) {
            this.f5417m = true;
        }
    }

    public void onPause() {
        super.onPause();
        synchronized (this) {
            this.f5417m = false;
        }
    }
}
