package com.tencent.bugly.beta.p050ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.UpgradeInfo;
import com.tencent.bugly.beta.download.C2801a;
import com.tencent.bugly.beta.download.DownloadListener;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2805b;
import com.tencent.bugly.beta.global.C2807d;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.beta.global.C2809f;
import com.tencent.bugly.beta.global.ResBean;
import com.tencent.bugly.beta.utils.C2836e;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2959y;

/* renamed from: com.tencent.bugly.beta.ui.h */
/* compiled from: BUGLY */
public class C2826h extends C2816a {

    /* renamed from: v */
    public static C2826h f5440v = new C2826h();

    /* renamed from: A */
    DownloadListener f5441A = new C2801a(2, this);

    /* renamed from: B */
    private C2822d f5442B = null;

    /* renamed from: n */
    protected TextView f5443n;

    /* renamed from: o */
    protected TextView f5444o;

    /* renamed from: p */
    public C2959y f5445p;

    /* renamed from: q */
    public DownloadTask f5446q;

    /* renamed from: r */
    public Runnable f5447r;

    /* renamed from: s */
    public Runnable f5448s;

    /* renamed from: t */
    protected Bitmap f5449t = null;

    /* renamed from: u */
    public BitmapDrawable f5450u;

    /* renamed from: w */
    public UILifecycleListener f5451w;

    /* renamed from: x */
    View.OnClickListener f5452x = new C2805b(4, this);

    /* renamed from: y */
    View.OnClickListener f5453y = new C2805b(5, this);

    /* renamed from: z */
    View.OnClickListener f5454z = new C2805b(6, this);

    public void onStart() {
        super.onStart();
        UILifecycleListener uILifecycleListener = this.f5451w;
        if (uILifecycleListener != null) {
            Context context = this.f5397a;
            View view = this.f5398b;
            C2959y yVar = this.f5445p;
            uILifecycleListener.onStart(context, view, yVar != null ? new UpgradeInfo(yVar) : null);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        if (this.f5408l != 0) {
            this.f5443n = (TextView) onCreateView.findViewWithTag(Beta.TAG_UPGRADE_INFO);
            this.f5444o = (TextView) onCreateView.findViewWithTag(Beta.TAG_UPGRADE_FEATURE);
        } else {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -1);
            LinearLayout linearLayout = new LinearLayout(this.f5397a);
            linearLayout.setLayoutParams(layoutParams);
            linearLayout.setOrientation(1);
            LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
            ResBean resBean = ResBean.f5325a;
            if (C2808e.f5336E.f5357U) {
                this.f5443n = new TextView(this.f5397a);
                this.f5443n.setLayoutParams(layoutParams2);
                TextView textView = this.f5443n;
                resBean.getClass();
                textView.setTextColor(Color.parseColor("#757575"));
                this.f5443n.setTextSize((float) 14);
                this.f5443n.setTag(Beta.TAG_UPGRADE_INFO);
                this.f5443n.setLineSpacing(15.0f, 1.0f);
                linearLayout.addView(this.f5443n);
            }
            TextView textView2 = new TextView(this.f5397a);
            textView2.setLayoutParams(layoutParams2);
            resBean.getClass();
            textView2.setTextColor(Color.parseColor("#273238"));
            float f = (float) 14;
            textView2.setTextSize(f);
            textView2.setSingleLine();
            textView2.setEllipsize(TextUtils.TruncateAt.END);
            textView2.setText(String.valueOf(Beta.strUpgradeDialogFeatureLabel + ": "));
            textView2.setPadding(0, C2804a.m6257a(this.f5397a, 8.0f), 0, 0);
            linearLayout.addView(textView2);
            this.f5444o = new TextView(this.f5397a);
            this.f5444o.setLayoutParams(layoutParams2);
            TextView textView3 = this.f5444o;
            resBean.getClass();
            textView3.setTextColor(Color.parseColor("#273238"));
            this.f5444o.setTextSize(f);
            this.f5444o.setTag(Beta.TAG_UPGRADE_FEATURE);
            this.f5444o.setMaxHeight(C2804a.m6257a(this.f5397a, 200.0f));
            this.f5444o.setLineSpacing(15.0f, 1.0f);
            linearLayout.addView(this.f5444o);
            this.f5405i.addView(linearLayout);
        }
        UILifecycleListener uILifecycleListener = this.f5451w;
        if (uILifecycleListener != null) {
            FragmentActivity activity = getActivity();
            C2959y yVar = this.f5445p;
            uILifecycleListener.onCreate(activity, onCreateView, yVar != null ? new UpgradeInfo(yVar) : null);
        }
        return onCreateView;
    }

    public void onDestroyView() {
        try {
            super.onDestroyView();
            UpgradeInfo upgradeInfo = null;
            this.f5443n = null;
            this.f5444o = null;
            synchronized (this) {
                this.f5442B = null;
            }
            if (this.f5450u != null) {
                this.f5450u.setCallback(null);
            }
            if (this.f5451w != null) {
                UILifecycleListener uILifecycleListener = this.f5451w;
                Context context = this.f5397a;
                View view = this.f5398b;
                if (this.f5445p != null) {
                    upgradeInfo = new UpgradeInfo(this.f5445p);
                }
                uILifecycleListener.onDestroy(context, view, upgradeInfo);
            }
        } catch (Exception unused) {
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.f5408l = C2808e.f5336E.f5375i;
        this.f5451w = C2808e.f5336E.f5377k;
        try {
            this.f5407k = Integer.parseInt(ResBean.f5325a.mo34045a("VAL_style"));
        } catch (Exception e) {
            C2903an.m6857a(e.getMessage(), new Object[0]);
            this.f5407k = 0;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:19:0x0058  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0060  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo34163a(com.tencent.bugly.beta.download.DownloadTask r9) {
        /*
            r8 = this;
            int r0 = r9.getStatus()
            r1 = 2
            r2 = 0
            if (r0 == 0) goto L_0x004e
            r3 = 1
            if (r0 == r3) goto L_0x0049
            if (r0 == r1) goto L_0x0024
            r9 = 3
            if (r0 == r9) goto L_0x001f
            r9 = 4
            if (r0 == r9) goto L_0x004e
            r9 = 5
            if (r0 == r9) goto L_0x001a
            java.lang.String r9 = ""
            r0 = r2
            goto L_0x0052
        L_0x001a:
            java.lang.String r9 = com.tencent.bugly.beta.Beta.strUpgradeDialogRetryBtn
            android.view.View$OnClickListener r0 = r8.f5452x
            goto L_0x0052
        L_0x001f:
            java.lang.String r9 = com.tencent.bugly.beta.Beta.strUpgradeDialogContinueBtn
            android.view.View$OnClickListener r0 = r8.f5452x
            goto L_0x0052
        L_0x0024:
            java.util.Locale r0 = java.util.Locale.getDefault()
            java.lang.Object[] r3 = new java.lang.Object[r3]
            r4 = 0
            long r5 = r9.getSavedLength()
            float r5 = (float) r5
            long r6 = r9.getTotalLength()
            float r9 = (float) r6
            float r5 = r5 / r9
            r9 = 1120403456(0x42c80000, float:100.0)
            float r5 = r5 * r9
            java.lang.Float r9 = java.lang.Float.valueOf(r5)
            r3[r4] = r9
            java.lang.String r9 = "%.1f%%"
            java.lang.String r9 = java.lang.String.format(r0, r9, r3)
            android.view.View$OnClickListener r0 = r8.f5453y
            goto L_0x0052
        L_0x0049:
            java.lang.String r9 = com.tencent.bugly.beta.Beta.strUpgradeDialogInstallBtn
            android.view.View$OnClickListener r0 = r8.f5452x
            goto L_0x0052
        L_0x004e:
            java.lang.String r9 = com.tencent.bugly.beta.Beta.strUpgradeDialogUpgradeBtn
            android.view.View$OnClickListener r0 = r8.f5452x
        L_0x0052:
            com.tencent.bugly.proguard.y r3 = r8.f5445p
            byte r3 = r3.f6288g
            if (r3 == r1) goto L_0x0060
            java.lang.String r1 = com.tencent.bugly.beta.Beta.strUpgradeDialogCancelBtn
            android.view.View$OnClickListener r2 = r8.f5454z
            r8.mo34151a(r1, r2, r9, r0)
            goto L_0x0063
        L_0x0060:
            r8.mo34151a(r2, r2, r9, r0)
        L_0x0063:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.p050ui.C2826h.mo34163a(com.tencent.bugly.beta.download.DownloadTask):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:49:0x01cd, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x01e6, code lost:
        return;
     */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34165c() {
        /*
            r7 = this;
            monitor-enter(r7)
            r0 = 0
            android.view.View r1 = r7.f5398b     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x01cc
            com.tencent.bugly.proguard.y r1 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x01cc
            com.tencent.bugly.beta.download.DownloadTask r1 = r7.f5446q     // Catch:{ Exception -> 0x01d0 }
            if (r1 != 0) goto L_0x0010
            goto L_0x01cc
        L_0x0010:
            int r1 = r7.f5408l     // Catch:{ Exception -> 0x01d0 }
            r2 = 0
            r3 = 1
            if (r1 != 0) goto L_0x0099
            int r1 = r7.f5407k     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x0082
            android.content.Context r1 = r7.f5397a     // Catch:{ Exception -> 0x01d0 }
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.global.ResBean r5 = r7.f5406j     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r6 = "IMG_title"
            java.lang.String r5 = r5.mo34045a(r6)     // Catch:{ Exception -> 0x01d0 }
            r4[r0] = r5     // Catch:{ Exception -> 0x01d0 }
            android.graphics.Bitmap r1 = com.tencent.bugly.beta.global.C2804a.m6258a(r1, r0, r4)     // Catch:{ Exception -> 0x01d0 }
            r7.f5449t = r1     // Catch:{ Exception -> 0x01d0 }
            r7.f5450u = r2     // Catch:{ Exception -> 0x01d0 }
            android.graphics.Bitmap r1 = r7.f5449t     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x0037
            android.graphics.Bitmap r2 = r7.f5449t     // Catch:{ Exception -> 0x01d0 }
            goto L_0x004f
        L_0x0037:
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x01d0 }
            int r1 = r1.f5374h     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x004f
            android.content.Context r1 = r7.f5397a     // Catch:{ Exception -> 0x01d0 }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x01d0 }
            int r4 = r4.f5374h     // Catch:{ Exception -> 0x01d0 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x01d0 }
            r2[r0] = r4     // Catch:{ Exception -> 0x01d0 }
            android.graphics.Bitmap r2 = com.tencent.bugly.beta.global.C2804a.m6258a(r1, r3, r2)     // Catch:{ Exception -> 0x01d0 }
        L_0x004f:
            android.widget.TextView r1 = r7.f5402f     // Catch:{ Exception -> 0x01d0 }
            android.view.ViewTreeObserver r1 = r1.getViewTreeObserver()     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.ui.d r4 = r7.f5442B     // Catch:{ Exception -> 0x01d0 }
            r1.removeOnPreDrawListener(r4)     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.ui.d r1 = new com.tencent.bugly.beta.ui.d     // Catch:{ Exception -> 0x01d0 }
            r4 = 4
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01d0 }
            r4[r0] = r7     // Catch:{ Exception -> 0x01d0 }
            android.widget.TextView r5 = r7.f5402f     // Catch:{ Exception -> 0x01d0 }
            r4[r3] = r5     // Catch:{ Exception -> 0x01d0 }
            r5 = 2
            r4[r5] = r2     // Catch:{ Exception -> 0x01d0 }
            r2 = 3
            int r5 = r7.f5407k     // Catch:{ Exception -> 0x01d0 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x01d0 }
            r4[r2] = r5     // Catch:{ Exception -> 0x01d0 }
            r1.<init>(r3, r4)     // Catch:{ Exception -> 0x01d0 }
            r7.f5442B = r1     // Catch:{ Exception -> 0x01d0 }
            android.widget.TextView r1 = r7.f5402f     // Catch:{ Exception -> 0x01d0 }
            android.view.ViewTreeObserver r1 = r1.getViewTreeObserver()     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.ui.d r2 = r7.f5442B     // Catch:{ Exception -> 0x01d0 }
            r1.addOnPreDrawListener(r2)     // Catch:{ Exception -> 0x01d0 }
            goto L_0x00e9
        L_0x0082:
            android.widget.TextView r1 = r7.f5402f     // Catch:{ Exception -> 0x01d0 }
            android.content.Context r2 = r7.f5397a     // Catch:{ Exception -> 0x01d0 }
            r4 = 1109917696(0x42280000, float:42.0)
            int r2 = com.tencent.bugly.beta.global.C2804a.m6257a(r2, r4)     // Catch:{ Exception -> 0x01d0 }
            r1.setHeight(r2)     // Catch:{ Exception -> 0x01d0 }
            android.widget.TextView r1 = r7.f5402f     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.y r2 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.f6282a     // Catch:{ Exception -> 0x01d0 }
            r1.setText(r2)     // Catch:{ Exception -> 0x01d0 }
            goto L_0x00e9
        L_0x0099:
            android.widget.TextView r1 = r7.f5402f     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.y r4 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r4 = r4.f6282a     // Catch:{ Exception -> 0x01d0 }
            r1.setText(r4)     // Catch:{ Exception -> 0x01d0 }
            android.widget.ImageView r1 = r7.f5401e     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x00e9
            android.widget.ImageView r1 = r7.f5401e     // Catch:{ Exception -> 0x01d0 }
            r1.setAdjustViewBounds(r3)     // Catch:{ Exception -> 0x01d0 }
            int r1 = r7.f5407k     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x00e9
            android.content.Context r1 = r7.f5397a     // Catch:{ Exception -> 0x01d0 }
            java.lang.Object[] r4 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.global.ResBean r5 = r7.f5406j     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r6 = "IMG_title"
            java.lang.String r5 = r5.mo34045a(r6)     // Catch:{ Exception -> 0x01d0 }
            r4[r0] = r5     // Catch:{ Exception -> 0x01d0 }
            android.graphics.Bitmap r1 = com.tencent.bugly.beta.global.C2804a.m6258a(r1, r0, r4)     // Catch:{ Exception -> 0x01d0 }
            r7.f5449t = r1     // Catch:{ Exception -> 0x01d0 }
            r7.f5450u = r2     // Catch:{ Exception -> 0x01d0 }
            android.graphics.Bitmap r1 = r7.f5449t     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x00cc
            android.graphics.Bitmap r2 = r7.f5449t     // Catch:{ Exception -> 0x01d0 }
            goto L_0x00e4
        L_0x00cc:
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x01d0 }
            int r1 = r1.f5374h     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x00e4
            android.content.Context r1 = r7.f5397a     // Catch:{ Exception -> 0x01d0 }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x01d0 }
            int r4 = r4.f5374h     // Catch:{ Exception -> 0x01d0 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x01d0 }
            r2[r0] = r4     // Catch:{ Exception -> 0x01d0 }
            android.graphics.Bitmap r2 = com.tencent.bugly.beta.global.C2804a.m6258a(r1, r3, r2)     // Catch:{ Exception -> 0x01d0 }
        L_0x00e4:
            android.widget.ImageView r1 = r7.f5401e     // Catch:{ Exception -> 0x01d0 }
            r1.setImageBitmap(r2)     // Catch:{ Exception -> 0x01d0 }
        L_0x00e9:
            android.widget.TextView r1 = r7.f5444o     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.y r2 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.f6283b     // Catch:{ Exception -> 0x01d0 }
            int r2 = r2.length()     // Catch:{ Exception -> 0x01d0 }
            r4 = 500(0x1f4, float:7.0E-43)
            if (r2 <= r4) goto L_0x0100
            com.tencent.bugly.proguard.y r2 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.f6283b     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.substring(r0, r4)     // Catch:{ Exception -> 0x01d0 }
            goto L_0x0104
        L_0x0100:
            com.tencent.bugly.proguard.y r2 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.f6283b     // Catch:{ Exception -> 0x01d0 }
        L_0x0104:
            r1.setText(r2)     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ Exception -> 0x01d0 }
            boolean r1 = r1.f5357U     // Catch:{ Exception -> 0x01d0 }
            if (r1 == 0) goto L_0x01c6
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d0 }
            r1.<init>()     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = com.tencent.bugly.beta.Beta.strUpgradeDialogVersionLabel     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = ": "
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.y r2 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.v r2 = r2.f6286e     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.f6255d     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = "\n"
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = com.tencent.bugly.beta.Beta.strUpgradeDialogFileSizeLabel     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = ": "
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.y r2 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.u r2 = r2.f6287f     // Catch:{ Exception -> 0x01d0 }
            long r4 = r2.f6249d     // Catch:{ Exception -> 0x01d0 }
            float r2 = (float) r4     // Catch:{ Exception -> 0x01d0 }
            r4 = 1233125376(0x49800000, float:1048576.0)
            int r5 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r5 < 0) goto L_0x015d
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r6 = "%.1f"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            float r2 = r2 / r4
            java.lang.Float r2 = java.lang.Float.valueOf(r2)     // Catch:{ Exception -> 0x01d0 }
            r3[r0] = r2     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = java.lang.String.format(r5, r6, r3)     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = "M"
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            goto L_0x0199
        L_0x015d:
            r4 = 1149239296(0x44800000, float:1024.0)
            int r5 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r5 < 0) goto L_0x017f
            java.util.Locale r5 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r6 = "%.1f"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            float r2 = r2 / r4
            java.lang.Float r2 = java.lang.Float.valueOf(r2)     // Catch:{ Exception -> 0x01d0 }
            r3[r0] = r2     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = java.lang.String.format(r5, r6, r3)     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = "K"
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            goto L_0x0199
        L_0x017f:
            java.util.Locale r4 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r5 = "%.1f"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x01d0 }
            java.lang.Float r2 = java.lang.Float.valueOf(r2)     // Catch:{ Exception -> 0x01d0 }
            r3[r0] = r2     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = java.lang.String.format(r4, r5, r3)     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = "B"
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
        L_0x0199:
            java.lang.String r2 = "\n"
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r3 = "yyyy-MM-dd HH:mm"
            java.util.Locale r4 = java.util.Locale.CHINA     // Catch:{ Exception -> 0x01d0 }
            r2.<init>(r3, r4)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r3 = com.tencent.bugly.beta.Beta.strUpgradeDialogUpdateTimeLabel     // Catch:{ Exception -> 0x01d0 }
            r1.append(r3)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r3 = ": "
            r1.append(r3)     // Catch:{ Exception -> 0x01d0 }
            java.util.Date r3 = new java.util.Date     // Catch:{ Exception -> 0x01d0 }
            com.tencent.bugly.proguard.y r4 = r7.f5445p     // Catch:{ Exception -> 0x01d0 }
            long r4 = r4.f6296o     // Catch:{ Exception -> 0x01d0 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x01d0 }
            java.lang.String r2 = r2.format(r3)     // Catch:{ Exception -> 0x01d0 }
            r1.append(r2)     // Catch:{ Exception -> 0x01d0 }
            android.widget.TextView r2 = r7.f5443n     // Catch:{ Exception -> 0x01d0 }
            r2.setText(r1)     // Catch:{ Exception -> 0x01d0 }
        L_0x01c6:
            com.tencent.bugly.beta.download.DownloadTask r1 = r7.f5446q     // Catch:{ Exception -> 0x01d0 }
            r7.mo34163a(r1)     // Catch:{ Exception -> 0x01d0 }
            goto L_0x01e5
        L_0x01cc:
            monitor-exit(r7)
            return
        L_0x01ce:
            r0 = move-exception
            goto L_0x01e7
        L_0x01d0:
            r1 = move-exception
            int r2 = r7.f5408l     // Catch:{ all -> 0x01ce }
            if (r2 == 0) goto L_0x01dc
            java.lang.String r2 = "please confirm your argument: [Beta.upgradeDialogLayoutId] is correct"
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x01ce }
            com.tencent.bugly.proguard.C2903an.m6865e(r2, r0)     // Catch:{ all -> 0x01ce }
        L_0x01dc:
            boolean r0 = com.tencent.bugly.proguard.C2903an.m6861b(r1)     // Catch:{ all -> 0x01ce }
            if (r0 != 0) goto L_0x01e5
            r1.printStackTrace()     // Catch:{ all -> 0x01ce }
        L_0x01e5:
            monitor-exit(r7)
            return
        L_0x01e7:
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.p050ui.C2826h.mo34165c():void");
    }

    /* renamed from: a */
    public synchronized void mo34164a(C2959y yVar, DownloadTask downloadTask) {
        this.f5445p = yVar;
        this.f5446q = downloadTask;
        this.f5446q.addListener(this.f5441A);
        C2836e.m6398a(new C2807d(7, this));
    }

    /* renamed from: a */
    public boolean mo34157a(int i, KeyEvent keyEvent) {
        if (i != 4) {
            return false;
        }
        if (this.f5445p.f6288g == 2) {
            return true;
        }
        Runnable runnable = this.f5448s;
        if (runnable != null) {
            runnable.run();
        }
        mo34150a();
        return true;
    }

    public void onPause() {
        DownloadTask downloadTask;
        super.onPause();
        DownloadListener downloadListener = this.f5441A;
        if (!(downloadListener == null || (downloadTask = this.f5446q) == null)) {
            downloadTask.removeListener(downloadListener);
        }
        UILifecycleListener uILifecycleListener = this.f5451w;
        if (uILifecycleListener != null) {
            Context context = this.f5397a;
            View view = this.f5398b;
            C2959y yVar = this.f5445p;
            uILifecycleListener.onPause(context, view, yVar != null ? new UpgradeInfo(yVar) : null);
        }
    }

    public void onResume() {
        DownloadTask downloadTask;
        super.onResume();
        DownloadListener downloadListener = this.f5441A;
        if (!(downloadListener == null || (downloadTask = this.f5446q) == null)) {
            downloadTask.addListener(downloadListener);
        }
        mo34165c();
        if (this.f5407k != 0 && this.f5449t == null) {
            C2809f.f5393a.mo34058a(new C2807d(7, this));
        }
        UILifecycleListener uILifecycleListener = this.f5451w;
        if (uILifecycleListener != null) {
            Context context = this.f5397a;
            View view = this.f5398b;
            C2959y yVar = this.f5445p;
            uILifecycleListener.onResume(context, view, yVar != null ? new UpgradeInfo(yVar) : null);
        }
    }

    public void onStop() {
        super.onStop();
        UILifecycleListener uILifecycleListener = this.f5451w;
        if (uILifecycleListener != null) {
            Context context = this.f5397a;
            View view = this.f5398b;
            C2959y yVar = this.f5445p;
            uILifecycleListener.onStop(context, view, yVar != null ? new UpgradeInfo(yVar) : null);
        }
    }
}
