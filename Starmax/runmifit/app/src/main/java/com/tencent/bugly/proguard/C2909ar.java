package com.tencent.bugly.proguard;

import android.os.Handler;
import android.os.SystemClock;

/* renamed from: com.tencent.bugly.proguard.ar */
/* compiled from: BUGLY */
public class C2909ar implements Runnable {

    /* renamed from: a */
    private final Handler f6055a;

    /* renamed from: b */
    private final String f6056b;

    /* renamed from: c */
    private long f6057c;

    /* renamed from: d */
    private final long f6058d;

    /* renamed from: e */
    private boolean f6059e = true;

    /* renamed from: f */
    private long f6060f;

    C2909ar(Handler handler, String str, long j) {
        this.f6055a = handler;
        this.f6056b = str;
        this.f6057c = j;
        this.f6058d = j;
    }

    /* renamed from: a */
    public void mo34554a() {
        if (!this.f6059e) {
            C2903an.m6864d("scheduleCheckBlock fail as %s thread is blocked.", mo34559e());
            return;
        }
        this.f6059e = false;
        this.f6060f = SystemClock.uptimeMillis();
        this.f6055a.postAtFrontOfQueue(this);
    }

    /* renamed from: b */
    public boolean mo34556b() {
        C2903an.m6863c("%s thread waitTime:%d", mo34559e(), Long.valueOf(this.f6057c));
        if (this.f6059e || SystemClock.uptimeMillis() <= this.f6060f + this.f6057c) {
            return false;
        }
        return true;
    }

    /* renamed from: c */
    public int mo34557c() {
        if (this.f6059e) {
            return 0;
        }
        return SystemClock.uptimeMillis() - this.f6060f < this.f6057c ? 1 : 3;
    }

    /* renamed from: d */
    public Thread mo34558d() {
        return this.f6055a.getLooper().getThread();
    }

    /* renamed from: e */
    public String mo34559e() {
        return this.f6056b;
    }

    public void run() {
        this.f6059e = true;
        mo34560f();
    }

    /* renamed from: a */
    public void mo34555a(long j) {
        this.f6057c = j;
    }

    /* renamed from: f */
    public void mo34560f() {
        this.f6057c = this.f6058d;
    }
}
