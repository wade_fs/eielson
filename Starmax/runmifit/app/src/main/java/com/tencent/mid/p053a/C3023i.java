package com.tencent.mid.p053a;

import android.os.Handler;
import android.os.HandlerThread;

/* renamed from: com.tencent.mid.a.i */
public class C3023i {

    /* renamed from: a */
    private static C3023i f6529a;

    /* renamed from: b */
    private Handler f6530b = null;

    private C3023i() {
        HandlerThread handlerThread = new HandlerThread("MidWorkThread");
        handlerThread.start();
        this.f6530b = new Handler(handlerThread.getLooper());
    }

    /* renamed from: a */
    public static C3023i m7381a() {
        if (f6529a == null) {
            synchronized (C3023i.class) {
                if (f6529a == null) {
                    f6529a = new C3023i();
                }
            }
        }
        return f6529a;
    }

    /* renamed from: a */
    public void mo34851a(Runnable runnable) {
        Handler handler = this.f6530b;
        if (handler != null) {
            handler.post(runnable);
        }
    }
}
