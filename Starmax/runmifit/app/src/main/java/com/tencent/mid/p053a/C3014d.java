package com.tencent.mid.p053a;

import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.mid.a.d */
public class C3014d {

    /* renamed from: a */
    private int f6518a = -1;

    /* renamed from: b */
    private JSONObject f6519b = null;

    public C3014d(int i, String str) {
        this.f6518a = i;
        try {
            this.f6519b = new JSONObject(str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public int mo34843a() {
        return this.f6518a;
    }

    /* renamed from: b */
    public JSONObject mo34844b() {
        return this.f6519b;
    }
}
