package com.tencent.p055mm.opensdk.p056a;

import android.os.Bundle;
import android.util.Log;

/* renamed from: com.tencent.mm.opensdk.a.a */
public final class C3047a {
    /* renamed from: a */
    public static int m7538a(Bundle bundle, String str) {
        if (bundle == null) {
            return -1;
        }
        try {
            return bundle.getInt(str, -1);
        } catch (Exception e) {
            Log.e("MicroMsg.IntentUtil", "getIntExtra exception:" + e.getMessage());
            return -1;
        }
    }

    /* renamed from: b */
    public static String m7539b(Bundle bundle, String str) {
        if (bundle == null) {
            return null;
        }
        try {
            return bundle.getString(str);
        } catch (Exception e) {
            Log.e("MicroMsg.IntentUtil", "getStringExtra exception:" + e.getMessage());
            return null;
        }
    }
}
