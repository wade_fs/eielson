package com.tencent.bugly.beta.p050ui;

import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2805b;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.proguard.C2903an;

/* renamed from: com.tencent.bugly.beta.ui.f */
/* compiled from: BUGLY */
public class C2824f extends C2816a {

    /* renamed from: n */
    public DownloadTask f5434n;

    /* renamed from: o */
    protected TextView f5435o;

    /* renamed from: a */
    public boolean mo34157a(int i, KeyEvent keyEvent) {
        return false;
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        this.f5408l = C2808e.f5336E.f5376j;
        View onCreateView = super.onCreateView(layoutInflater, viewGroup, bundle);
        if (this.f5408l == 0) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
            this.f5435o = new TextView(this.f5397a);
            this.f5435o.setLayoutParams(layoutParams);
            TextView textView = this.f5435o;
            this.f5406j.getClass();
            textView.setTextColor(Color.parseColor("#757575"));
            this.f5435o.setTextSize(16.0f);
            this.f5435o.setTag(Beta.TAG_TIP_MESSAGE);
            this.f5405i.addView(this.f5435o);
        } else if (onCreateView != null) {
            this.f5435o = (TextView) onCreateView.findViewWithTag(Beta.TAG_TIP_MESSAGE);
        }
        try {
            this.f5435o.setText(Beta.strNetworkTipsMessage);
            this.f5402f.setText(Beta.strNetworkTipsTitle);
            mo34151a(Beta.strNetworkTipsCancelBtn, new C2805b(2, this), Beta.strNetworkTipsConfirmBtn, new C2805b(3, this, this.f5434n));
        } catch (Exception e) {
            if (this.f5408l != 0) {
                C2903an.m6865e("please confirm your argument: [Beta.tipsDialogLayoutId] is correct", new Object[0]);
            }
            if (!C2903an.m6861b(e)) {
                e.printStackTrace();
            }
        }
        return onCreateView;
    }

    public void onDestroyView() {
        super.onDestroyView();
        this.f5435o = null;
    }
}
