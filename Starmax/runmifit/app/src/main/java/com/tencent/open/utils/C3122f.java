package com.tencent.open.utils;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import com.baidu.mobstat.Config;
import com.tamic.novate.util.FileUtil;
import com.tencent.connect.common.Constants;
import com.tencent.open.p059a.C3082f;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.open.utils.f */
/* compiled from: ProGuard */
public class C3122f {

    /* renamed from: a */
    private static Map<String, C3122f> f6839a = Collections.synchronizedMap(new HashMap());

    /* renamed from: b */
    private static String f6840b = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Context f6841c = null;

    /* renamed from: d */
    private String f6842d = null;

    /* renamed from: e */
    private JSONObject f6843e = null;

    /* renamed from: f */
    private long f6844f = 0;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public int f6845g = 0;

    /* renamed from: h */
    private boolean f6846h = true;

    /* renamed from: a */
    public static C3122f m7733a(Context context, String str) {
        C3122f fVar;
        synchronized (f6839a) {
            C3082f.m7628a("openSDK_LOG.OpenConfig", "getInstance begin");
            if (str != null) {
                f6840b = str;
            }
            if (str == null) {
                str = f6840b != null ? f6840b : "0";
            }
            fVar = f6839a.get(str);
            if (fVar == null) {
                fVar = new C3122f(context, str);
                f6839a.put(str, fVar);
            }
            C3082f.m7628a("openSDK_LOG.OpenConfig", "getInstance end");
        }
        return fVar;
    }

    private C3122f(Context context, String str) {
        this.f6841c = context.getApplicationContext();
        this.f6842d = str;
        m7734a();
        m7738b();
    }

    /* renamed from: a */
    private void m7734a() {
        try {
            this.f6843e = new JSONObject(m7739c("com.tencent.open.config.json"));
        } catch (JSONException unused) {
            this.f6843e = new JSONObject();
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:7|8) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0077, code lost:
        r5 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0078, code lost:
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007b, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:?, code lost:
        r5 = r4.f6841c.getAssets().open(r5);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0025 */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m7739c(java.lang.String r5) {
        /*
            r4 = this;
            java.lang.String r0 = ""
            java.lang.String r1 = r4.f6842d     // Catch:{ FileNotFoundException -> 0x0025 }
            if (r1 == 0) goto L_0x001d
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0025 }
            r1.<init>()     // Catch:{ FileNotFoundException -> 0x0025 }
            r1.append(r5)     // Catch:{ FileNotFoundException -> 0x0025 }
            java.lang.String r2 = "."
            r1.append(r2)     // Catch:{ FileNotFoundException -> 0x0025 }
            java.lang.String r2 = r4.f6842d     // Catch:{ FileNotFoundException -> 0x0025 }
            r1.append(r2)     // Catch:{ FileNotFoundException -> 0x0025 }
            java.lang.String r1 = r1.toString()     // Catch:{ FileNotFoundException -> 0x0025 }
            goto L_0x001e
        L_0x001d:
            r1 = r5
        L_0x001e:
            android.content.Context r2 = r4.f6841c     // Catch:{ FileNotFoundException -> 0x0025 }
            java.io.FileInputStream r5 = r2.openFileInput(r1)     // Catch:{ FileNotFoundException -> 0x0025 }
            goto L_0x002f
        L_0x0025:
            android.content.Context r1 = r4.f6841c     // Catch:{ IOException -> 0x0077 }
            android.content.res.AssetManager r1 = r1.getAssets()     // Catch:{ IOException -> 0x0077 }
            java.io.InputStream r5 = r1.open(r5)     // Catch:{ IOException -> 0x0077 }
        L_0x002f:
            java.io.BufferedReader r1 = new java.io.BufferedReader
            java.io.InputStreamReader r2 = new java.io.InputStreamReader
            java.lang.String r3 = "UTF-8"
            java.nio.charset.Charset r3 = java.nio.charset.Charset.forName(r3)
            r2.<init>(r5, r3)
            r1.<init>(r2)
            java.lang.StringBuffer r2 = new java.lang.StringBuffer
            r2.<init>()
        L_0x0044:
            java.lang.String r3 = r1.readLine()     // Catch:{ IOException -> 0x0060 }
            if (r3 == 0) goto L_0x004e
            r2.append(r3)     // Catch:{ IOException -> 0x0060 }
            goto L_0x0044
        L_0x004e:
            java.lang.String r0 = r2.toString()     // Catch:{ IOException -> 0x0060 }
            r5.close()     // Catch:{ IOException -> 0x0059 }
            r1.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x006a
        L_0x0059:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x006a
        L_0x005e:
            r0 = move-exception
            goto L_0x006b
        L_0x0060:
            r2 = move-exception
            r2.printStackTrace()     // Catch:{ all -> 0x005e }
            r5.close()     // Catch:{ IOException -> 0x0059 }
            r1.close()     // Catch:{ IOException -> 0x0059 }
        L_0x006a:
            return r0
        L_0x006b:
            r5.close()     // Catch:{ IOException -> 0x0072 }
            r1.close()     // Catch:{ IOException -> 0x0072 }
            goto L_0x0076
        L_0x0072:
            r5 = move-exception
            r5.printStackTrace()
        L_0x0076:
            throw r0
        L_0x0077:
            r5 = move-exception
            r5.printStackTrace()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3122f.m7739c(java.lang.String):java.lang.String");
    }

    /* renamed from: a */
    private void m7736a(String str, String str2) {
        try {
            if (this.f6842d != null) {
                str = str + FileUtil.HIDDEN_PREFIX + this.f6842d;
            }
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.f6841c.openFileOutput(str, 0), Charset.forName("UTF-8"));
            outputStreamWriter.write(str2);
            outputStreamWriter.flush();
            outputStreamWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: b */
    private void m7738b() {
        if (this.f6845g != 0) {
            m7741d("update thread is running, return");
            return;
        }
        this.f6845g = 1;
        final Bundle bundle = new Bundle();
        bundle.putString("appid", this.f6842d);
        bundle.putString("appid_for_getting_config", this.f6842d);
        bundle.putString("status_os", Build.VERSION.RELEASE);
        bundle.putString("status_machine", Build.MODEL);
        bundle.putString("status_version", Build.VERSION.SDK);
        bundle.putString("sdkv", Constants.SDK_VERSION);
        bundle.putString("sdkp", Config.APP_VERSION_CODE);
        new Thread() {
            /* class com.tencent.open.utils.C3122f.C31231 */

            public void run() {
                try {
                    C3122f.this.m7737a(C3131k.m7788d(HttpUtils.openUrl2(C3122f.this.f6841c, "http://cgi.connect.qq.com/qqconnectopen/openapi/policy_conf", "GET", bundle).f6870a));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                int unused = C3122f.this.f6845g = 0;
            }
        }.start();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m7737a(JSONObject jSONObject) {
        m7741d("cgi back, do update");
        this.f6843e = jSONObject;
        m7736a("com.tencent.open.config.json", jSONObject.toString());
        this.f6844f = SystemClock.elapsedRealtime();
    }

    /* renamed from: c */
    private void m7740c() {
        int optInt = this.f6843e.optInt("Common_frequency");
        if (optInt == 0) {
            optInt = 1;
        }
        if (SystemClock.elapsedRealtime() - this.f6844f >= ((long) (optInt * 3600000))) {
            m7738b();
        }
    }

    /* renamed from: a */
    public int mo35169a(String str) {
        m7741d("get " + str);
        m7740c();
        return this.f6843e.optInt(str);
    }

    /* renamed from: b */
    public boolean mo35170b(String str) {
        m7741d("get " + str);
        m7740c();
        Object opt = this.f6843e.opt(str);
        if (opt == null) {
            return false;
        }
        if (opt instanceof Integer) {
            return !opt.equals(0);
        }
        if (opt instanceof Boolean) {
            return ((Boolean) opt).booleanValue();
        }
        return false;
    }

    /* renamed from: d */
    private void m7741d(String str) {
        if (this.f6846h) {
            C3082f.m7628a("openSDK_LOG.OpenConfig", str + "; appid: " + this.f6842d);
        }
    }
}
