package com.tencent.open.web.security;

import android.view.KeyEvent;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputConnectionWrapper;
import com.tencent.open.p059a.C3082f;

/* renamed from: com.tencent.open.web.security.a */
/* compiled from: ProGuard */
public class C3136a extends InputConnectionWrapper {

    /* renamed from: a */
    public static String f6876a = null;

    /* renamed from: b */
    public static boolean f6877b = false;

    /* renamed from: c */
    public static boolean f6878c = false;

    public C3136a(InputConnection inputConnection, boolean z) {
        super(inputConnection, z);
    }

    public boolean setComposingText(CharSequence charSequence, int i) {
        f6878c = true;
        f6876a = charSequence.toString();
        C3082f.m7628a("openSDK_LOG.CaptureInputConnection", "-->setComposingText: " + charSequence.toString());
        return super.setComposingText(charSequence, i);
    }

    public boolean commitText(CharSequence charSequence, int i) {
        f6878c = true;
        f6876a = charSequence.toString();
        C3082f.m7628a("openSDK_LOG.CaptureInputConnection", "-->commitText: " + charSequence.toString());
        return super.commitText(charSequence, i);
    }

    public boolean sendKeyEvent(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 0) {
            C3082f.m7634c("openSDK_LOG.CaptureInputConnection", "sendKeyEvent");
            f6876a = String.valueOf((char) keyEvent.getUnicodeChar());
            f6878c = true;
            C3082f.m7631b("openSDK_LOG.CaptureInputConnection", "s: " + f6876a);
        }
        C3082f.m7631b("openSDK_LOG.CaptureInputConnection", "-->sendKeyEvent: " + f6876a);
        return super.sendKeyEvent(keyEvent);
    }
}
