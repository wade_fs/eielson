package com.tencent.p055mm.opensdk.diffdev.p058a;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.tencent.p055mm.opensdk.diffdev.IDiffDevOAuth;
import com.tencent.p055mm.opensdk.diffdev.OAuthListener;
import java.util.ArrayList;
import java.util.List;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.a */
public final class C3056a implements IDiffDevOAuth {
    /* access modifiers changed from: private */

    /* renamed from: d */
    public List<OAuthListener> f6626d = new ArrayList();
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C3059d f6627e;

    /* renamed from: f */
    private OAuthListener f6628f = new C3057b(this);
    /* access modifiers changed from: private */
    public Handler handler = null;

    public final void addListener(OAuthListener oAuthListener) {
        if (!this.f6626d.contains(oAuthListener)) {
            this.f6626d.add(oAuthListener);
        }
    }

    public final boolean auth(String str, String str2, String str3, String str4, String str5, OAuthListener oAuthListener) {
        String str6 = str;
        Log.i("MicroMsg.SDK.DiffDevOAuth", "start auth, appId = " + str);
        if (str6 == null || str.length() <= 0 || str2 == null || str2.length() <= 0) {
            Log.d("MicroMsg.SDK.DiffDevOAuth", String.format("auth fail, invalid argument, appId = %s, scope = %s", str6, str2));
            return false;
        }
        if (this.handler == null) {
            this.handler = new Handler(Looper.getMainLooper());
        }
        addListener(oAuthListener);
        if (this.f6627e != null) {
            Log.d("MicroMsg.SDK.DiffDevOAuth", "auth, already running, no need to start auth again");
            return true;
        }
        this.f6627e = new C3059d(str, str2, str3, str4, str5, this.f6628f);
        C3059d dVar = this.f6627e;
        if (Build.VERSION.SDK_INT >= 11) {
            dVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        } else {
            dVar.execute(new Void[0]);
        }
        return true;
    }

    public final void detach() {
        Log.i("MicroMsg.SDK.DiffDevOAuth", "detach");
        this.f6626d.clear();
        stopAuth();
    }

    public final void removeAllListeners() {
        this.f6626d.clear();
    }

    public final void removeListener(OAuthListener oAuthListener) {
        this.f6626d.remove(oAuthListener);
    }

    public final boolean stopAuth() {
        boolean z;
        Log.i("MicroMsg.SDK.DiffDevOAuth", "stopAuth");
        try {
            z = this.f6627e == null ? true : this.f6627e.mo34975a();
        } catch (Exception e) {
            Log.w("MicroMsg.SDK.DiffDevOAuth", "stopAuth fail, ex = " + e.getMessage());
            z = false;
        }
        this.f6627e = null;
        return z;
    }
}
