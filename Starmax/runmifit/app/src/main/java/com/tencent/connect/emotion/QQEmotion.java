package com.tencent.connect.emotion;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.media.session.PlaybackStateCompat;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.Toast;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.connect.common.UIListenerManager;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3131k;
import com.tencent.tauth.IUiListener;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class QQEmotion extends BaseApi {

    /* renamed from: a */
    private IUiListener f6466a;

    public QQEmotion(QQToken qQToken) {
        super(qQToken);
    }

    public void setEmotions(Activity activity, ArrayList<Uri> arrayList, IUiListener iUiListener) {
        IUiListener iUiListener2 = this.f6466a;
        if (iUiListener2 != null) {
            iUiListener2.onCancel();
        }
        this.f6466a = iUiListener;
        if (!C3125h.m7756b(activity)) {
            Toast.makeText(activity.getApplicationContext(), "当前手机未安装QQ，请安装最新版QQ后再试。", 1).show();
        } else if (C3125h.m7757c(activity, "8.0.0") < 0) {
            Toast.makeText(activity.getApplicationContext(), "当前手机QQ版本过低，不支持设置表情功能。", 1).show();
        } else if (!m7317a(activity.getApplicationContext(), arrayList)) {
            Toast.makeText(activity.getApplicationContext(), "图片不符合要求，不支持设置表情功能。", 1).show();
        } else {
            String a = C3131k.m7770a(activity);
            StringBuffer stringBuffer = new StringBuffer("mqqapi://profile/sdk_face_collection?");
            if (!TextUtils.isEmpty(a)) {
                if (a.length() > 20) {
                    a = a.substring(0, 20) + "...";
                }
                stringBuffer.append("&app_name=" + Base64.encodeToString(C3131k.m7799i(a), 2));
            }
            String appId = this.f6457c.getAppId();
            String openId = this.f6457c.getOpenId();
            if (!TextUtils.isEmpty(appId)) {
                stringBuffer.append("&share_id=" + appId);
            }
            if (!TextUtils.isEmpty(openId)) {
                stringBuffer.append("&open_id=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
            }
            stringBuffer.append("&sdk_version=" + Base64.encodeToString(C3131k.m7799i(Constants.SDK_VERSION), 2));
            String a2 = m7316a(arrayList);
            if (!TextUtils.isEmpty(a2)) {
                stringBuffer.append("&set_uri_list=" + a2);
            }
            C3082f.m7628a("QQEMOTION", "-->set avatar, url: " + stringBuffer.toString());
            Intent intent = new Intent("android.intent.action.VIEW");
            intent.setData(Uri.parse(stringBuffer.toString()));
            intent.putParcelableArrayListExtra("android.intent.extra.STREAM", arrayList);
            if (mo34804a(intent)) {
                UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_EDIT_EMOTION, iUiListener);
                mo34800a(activity, Constants.REQUEST_EDIT_EMOTION, intent, false);
            }
        }
    }

    /* renamed from: a */
    private boolean m7317a(Context context, ArrayList<Uri> arrayList) {
        if (arrayList == null || arrayList.size() == 0) {
            return false;
        }
        if (arrayList.size() > 9) {
            C3082f.m7634c("QQEMOTION", "isLegality -->illegal, file count > 9, count = " + arrayList.size());
            return false;
        }
        long j = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            String a = C3131k.m7771a(context, arrayList.get(i));
            long j2 = C3131k.m7800j(a);
            if (j2 > PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
                C3082f.m7634c("QQEMOTION", "isLegality -->illegal, fileSize: " + j2 + "， path =" + a);
                return false;
            }
            j += j2;
        }
        if (j > 3145728) {
            C3082f.m7634c("QQEMOTION", "isLegality -->illegal, totalSize: " + j);
            return false;
        }
        C3082f.m7634c("QQEMOTION", "isLegality -->legal, totalSize: " + j);
        return true;
    }

    /* renamed from: a */
    private String m7316a(ArrayList<Uri> arrayList) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arrayList.size(); i++) {
            sb.append(arrayList.get(i));
            sb.append(";");
        }
        String sb2 = sb.toString();
        C3082f.m7634c("QQEMOTION", "-->getFilePathListJson listStr : " + sb2);
        return Base64.encodeToString(C3131k.m7799i(sb2), 2);
    }
}
