package com.tencent.open.p060b;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.baidu.mobstat.Config;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3121e;
import java.util.Locale;

/* renamed from: com.tencent.open.b.c */
/* compiled from: ProGuard */
public class C3090c {

    /* renamed from: a */
    static String f6757a;

    /* renamed from: b */
    static String f6758b;

    /* renamed from: c */
    static String f6759c;

    /* renamed from: d */
    private static String f6760d;

    /* renamed from: e */
    private static String f6761e;

    /* renamed from: a */
    public static String m7658a() {
        WifiManager wifiManager;
        WifiInfo connectionInfo;
        try {
            Context a = C3121e.m7727a();
            if (a == null || (wifiManager = (WifiManager) a.getSystemService("wifi")) == null || (connectionInfo = wifiManager.getConnectionInfo()) == null) {
                return "";
            }
            return connectionInfo.getMacAddress();
        } catch (SecurityException e) {
            C3082f.m7632b("openSDK_LOG.MobileInfoUtil", "getLocalMacAddress>>>", e);
            return "";
        }
    }

    /* renamed from: a */
    public static String m7659a(Context context) {
        if (!TextUtils.isEmpty(f6760d)) {
            return f6760d;
        }
        if (context == null) {
            return "";
        }
        f6760d = "";
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        if (windowManager != null) {
            int width = windowManager.getDefaultDisplay().getWidth();
            int height = windowManager.getDefaultDisplay().getHeight();
            f6760d = width + Config.EVENT_HEAT_X + height;
        }
        return f6760d;
    }

    /* renamed from: b */
    public static String m7660b() {
        return Locale.getDefault().getLanguage();
    }

    /* renamed from: b */
    public static String m7661b(Context context) {
        String str = f6757a;
        if (str != null && str.length() > 0) {
            return f6757a;
        }
        if (context == null) {
            return "";
        }
        try {
            f6757a = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
            return f6757a;
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: c */
    public static String m7662c(Context context) {
        String str = f6758b;
        if (str != null && str.length() > 0) {
            return f6758b;
        }
        if (context == null) {
            return "";
        }
        try {
            f6758b = ((TelephonyManager) context.getSystemService("phone")).getSimSerialNumber();
            return f6758b;
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: d */
    public static String m7663d(Context context) {
        String str = f6759c;
        if (str != null && str.length() > 0) {
            return f6759c;
        }
        if (context == null) {
            return "";
        }
        try {
            f6759c = Settings.Secure.getString(context.getContentResolver(), "android_id");
            return f6759c;
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: e */
    public static String m7664e(Context context) {
        try {
            if (f6761e == null) {
                DisplayMetrics displayMetrics = new DisplayMetrics();
                ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
                StringBuilder sb = new StringBuilder();
                sb.append("imei=");
                sb.append(m7661b(context));
                sb.append('&');
                sb.append("model=");
                sb.append(Build.MODEL);
                sb.append('&');
                sb.append("os=");
                sb.append(Build.VERSION.RELEASE);
                sb.append('&');
                sb.append("apilevel=");
                sb.append(Build.VERSION.SDK_INT);
                sb.append('&');
                String b = C3088a.m7654b(context);
                if (b == null) {
                    b = "";
                }
                sb.append("network=");
                sb.append(b);
                sb.append('&');
                sb.append("sdcard=");
                sb.append(Environment.getExternalStorageState().equals("mounted") ? 1 : 0);
                sb.append('&');
                sb.append("display=");
                sb.append(displayMetrics.widthPixels);
                sb.append('*');
                sb.append(displayMetrics.heightPixels);
                sb.append('&');
                sb.append("manu=");
                sb.append(Build.MANUFACTURER);
                sb.append("&");
                sb.append("wifi=");
                sb.append(C3088a.m7657e(context));
                f6761e = sb.toString();
            }
            return f6761e;
        } catch (Exception unused) {
            return null;
        }
    }
}
