package com.tencent.stat;

public enum StatReportStrategy {
    INSTANT(1),
    ONLY_WIFI(2),
    BATCH(3),
    APP_LAUNCH(4),
    DEVELOPER(5),
    PERIOD(6),
    ONLY_WIFI_NO_CACHE(7);
    

    /* renamed from: a */
    int f6998a;

    private StatReportStrategy(int i) {
        this.f6998a = i;
    }

    /* renamed from: a */
    public int mo35302a() {
        return this.f6998a;
    }

    public static StatReportStrategy getStatReportStrategy(int i) {
        StatReportStrategy[] values = values();
        for (StatReportStrategy statReportStrategy : values) {
            if (i == statReportStrategy.mo35302a()) {
                return statReportStrategy;
            }
        }
        return null;
    }
}
