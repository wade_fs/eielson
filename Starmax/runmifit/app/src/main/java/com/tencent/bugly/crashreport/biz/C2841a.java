package com.tencent.bugly.crashreport.biz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.proguard.C2888ad;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.util.List;

/* renamed from: com.tencent.bugly.crashreport.biz.a */
/* compiled from: BUGLY */
public class C2841a {

    /* renamed from: a */
    private Context f5564a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public long f5565b;

    /* renamed from: c */
    private int f5566c;

    /* renamed from: d */
    private boolean f5567d = true;

    public C2841a(Context context, boolean z) {
        this.f5564a = context;
        this.f5567d = z;
    }

    /* renamed from: a */
    private static UserInfoBean m6410a(Context context, int i) {
        C2851a a = C2851a.m6470a(context);
        UserInfoBean userInfoBean = new UserInfoBean();
        userInfoBean.f5546b = i;
        userInfoBean.f5547c = a.f5660e;
        userInfoBean.f5548d = a.mo34309g();
        userInfoBean.f5549e = System.currentTimeMillis();
        userInfoBean.f5550f = -1;
        userInfoBean.f5558n = a.f5671p;
        int i2 = 1;
        if (i != 1) {
            i2 = 0;
        }
        userInfoBean.f5559o = i2;
        userInfoBean.f5556l = a.mo34295a();
        userInfoBean.f5557m = a.f5677v;
        userInfoBean.f5551g = a.f5678w;
        userInfoBean.f5552h = a.f5679x;
        userInfoBean.f5553i = a.f5680y;
        userInfoBean.f5555k = a.f5681z;
        userInfoBean.f5562r = a.mo34272B();
        userInfoBean.f5563s = a.mo34277G();
        userInfoBean.f5560p = a.mo34278H();
        userInfoBean.f5561q = a.mo34279I();
        return userInfoBean;
    }

    /* renamed from: a */
    public void mo34247a(int i, boolean z, long j) {
        C2854a a = C2854a.m6574a();
        if (a == null || a.mo34340c().f5694h || i == 1 || i == 3) {
            if (i == 1 || i == 3) {
                this.f5566c++;
            }
            C2901am.m6848a().mo34548a(new C2844a(m6410a(this.f5564a, i), z), j);
            return;
        }
        C2903an.m6865e("UserInfo is disable", new Object[0]);
    }

    /* renamed from: a */
    public void mo34248a(long j) {
        C2901am.m6848a().mo34548a(new C2844a(null, true), j);
    }

    /* renamed from: b */
    public void mo34251b(long j) {
        C2901am.m6848a().mo34548a(new C2846c(j), j);
    }

    /* renamed from: a */
    public void mo34246a() {
        this.f5565b = C2908aq.m6923b() + 86400000;
        C2901am.m6848a().mo34548a(new C2845b(), (this.f5565b - System.currentTimeMillis()) + 5000);
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$a */
    /* compiled from: BUGLY */
    class C2844a implements Runnable {

        /* renamed from: b */
        private boolean f5572b;

        /* renamed from: c */
        private UserInfoBean f5573c;

        public C2844a(UserInfoBean userInfoBean, boolean z) {
            this.f5573c = userInfoBean;
            this.f5572b = z;
        }

        /* renamed from: a */
        private void m6427a(UserInfoBean userInfoBean) {
            C2851a b;
            if (userInfoBean != null && (b = C2851a.m6471b()) != null) {
                userInfoBean.f5554j = b.mo34305e();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
         arg types: [com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, int]
         candidates:
          com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
          com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void */
        public void run() {
            try {
                if (this.f5573c != null) {
                    m6427a(this.f5573c);
                    C2903an.m6863c("[UserInfo] Record user info.", new Object[0]);
                    C2841a.this.m6411a(this.f5573c, false);
                }
                if (this.f5572b) {
                    C2841a.this.mo34250b();
                }
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:68:0x00f7  */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void m6415c() {
        /*
            r14 = this;
            monitor-enter(r14)
            boolean r0 = r14.f5567d     // Catch:{ all -> 0x018b }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r14)
            return
        L_0x0007:
            com.tencent.bugly.proguard.ak r0 = com.tencent.bugly.proguard.C2896ak.m6805a()     // Catch:{ all -> 0x018b }
            if (r0 != 0) goto L_0x000f
            monitor-exit(r14)
            return
        L_0x000f:
            com.tencent.bugly.crashreport.common.strategy.a r1 = com.tencent.bugly.crashreport.common.strategy.C2854a.m6574a()     // Catch:{ all -> 0x018b }
            if (r1 != 0) goto L_0x0017
            monitor-exit(r14)
            return
        L_0x0017:
            boolean r1 = r1.mo34339b()     // Catch:{ all -> 0x018b }
            if (r1 == 0) goto L_0x0027
            r1 = 1001(0x3e9, float:1.403E-42)
            boolean r1 = r0.mo34535b(r1)     // Catch:{ all -> 0x018b }
            if (r1 != 0) goto L_0x0027
            monitor-exit(r14)
            return
        L_0x0027:
            android.content.Context r1 = r14.f5564a     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.common.info.a r1 = com.tencent.bugly.crashreport.common.info.C2851a.m6470a(r1)     // Catch:{ all -> 0x018b }
            java.lang.String r1 = r1.f5660e     // Catch:{ all -> 0x018b }
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ all -> 0x018b }
            r2.<init>()     // Catch:{ all -> 0x018b }
            java.util.List r1 = r14.mo34245a(r1)     // Catch:{ all -> 0x018b }
            r3 = 0
            r4 = 1
            if (r1 == 0) goto L_0x00e6
            int r5 = r1.size()     // Catch:{ all -> 0x018b }
            int r5 = r5 + -20
            if (r5 <= 0) goto L_0x008b
            r6 = 0
        L_0x0045:
            int r7 = r1.size()     // Catch:{ all -> 0x018b }
            int r7 = r7 - r4
            if (r6 >= r7) goto L_0x007e
            int r7 = r6 + 1
            r8 = r7
        L_0x004f:
            int r9 = r1.size()     // Catch:{ all -> 0x018b }
            if (r8 >= r9) goto L_0x007c
            java.lang.Object r9 = r1.get(r6)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r9 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r9     // Catch:{ all -> 0x018b }
            long r9 = r9.f5549e     // Catch:{ all -> 0x018b }
            java.lang.Object r11 = r1.get(r8)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r11 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r11     // Catch:{ all -> 0x018b }
            long r11 = r11.f5549e     // Catch:{ all -> 0x018b }
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 <= 0) goto L_0x0079
            java.lang.Object r9 = r1.get(r6)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r9 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r9     // Catch:{ all -> 0x018b }
            java.lang.Object r10 = r1.get(r8)     // Catch:{ all -> 0x018b }
            r1.set(r6, r10)     // Catch:{ all -> 0x018b }
            r1.set(r8, r9)     // Catch:{ all -> 0x018b }
        L_0x0079:
            int r8 = r8 + 1
            goto L_0x004f
        L_0x007c:
            r6 = r7
            goto L_0x0045
        L_0x007e:
            r6 = 0
        L_0x007f:
            if (r6 >= r5) goto L_0x008b
            java.lang.Object r7 = r1.get(r6)     // Catch:{ all -> 0x018b }
            r2.add(r7)     // Catch:{ all -> 0x018b }
            int r6 = r6 + 1
            goto L_0x007f
        L_0x008b:
            java.util.Iterator r5 = r1.iterator()     // Catch:{ all -> 0x018b }
            r6 = 0
        L_0x0090:
            boolean r7 = r5.hasNext()     // Catch:{ all -> 0x018b }
            if (r7 == 0) goto L_0x00d3
            java.lang.Object r7 = r5.next()     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.biz.UserInfoBean r7 = (com.tencent.bugly.crashreport.biz.UserInfoBean) r7     // Catch:{ all -> 0x018b }
            long r8 = r7.f5550f     // Catch:{ all -> 0x018b }
            r10 = -1
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 == 0) goto L_0x00b4
            r5.remove()     // Catch:{ all -> 0x018b }
            long r8 = r7.f5549e     // Catch:{ all -> 0x018b }
            long r10 = com.tencent.bugly.proguard.C2908aq.m6923b()     // Catch:{ all -> 0x018b }
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 >= 0) goto L_0x00b4
            r2.add(r7)     // Catch:{ all -> 0x018b }
        L_0x00b4:
            long r8 = r7.f5549e     // Catch:{ all -> 0x018b }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x018b }
            r12 = 600000(0x927c0, double:2.964394E-318)
            long r10 = r10 - r12
            int r12 = (r8 > r10 ? 1 : (r8 == r10 ? 0 : -1))
            if (r12 <= 0) goto L_0x0090
            int r8 = r7.f5546b     // Catch:{ all -> 0x018b }
            if (r8 == r4) goto L_0x00d0
            int r8 = r7.f5546b     // Catch:{ all -> 0x018b }
            r9 = 4
            if (r8 == r9) goto L_0x00d0
            int r7 = r7.f5546b     // Catch:{ all -> 0x018b }
            r8 = 3
            if (r7 != r8) goto L_0x0090
        L_0x00d0:
            int r6 = r6 + 1
            goto L_0x0090
        L_0x00d3:
            r5 = 15
            if (r6 <= r5) goto L_0x00eb
            java.lang.String r5 = "[UserInfo] Upload user info too many times in 10 min: %d"
            java.lang.Object[] r7 = new java.lang.Object[r4]     // Catch:{ all -> 0x018b }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x018b }
            r7[r3] = r6     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C2903an.m6864d(r5, r7)     // Catch:{ all -> 0x018b }
            r5 = 0
            goto L_0x00ec
        L_0x00e6:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x018b }
            r1.<init>()     // Catch:{ all -> 0x018b }
        L_0x00eb:
            r5 = 1
        L_0x00ec:
            int r6 = r2.size()     // Catch:{ all -> 0x018b }
            if (r6 <= 0) goto L_0x00f5
            r14.mo34249a(r2)     // Catch:{ all -> 0x018b }
        L_0x00f5:
            if (r5 == 0) goto L_0x0182
            int r2 = r1.size()     // Catch:{ all -> 0x018b }
            if (r2 != 0) goto L_0x00ff
            goto L_0x0182
        L_0x00ff:
            java.lang.String r2 = "[UserInfo] Upload user info(size: %d)"
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ all -> 0x018b }
            int r6 = r1.size()     // Catch:{ all -> 0x018b }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x018b }
            r5[r3] = r6     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C2903an.m6863c(r2, r5)     // Catch:{ all -> 0x018b }
            int r2 = r14.f5566c     // Catch:{ all -> 0x018b }
            if (r2 != r4) goto L_0x0116
            r2 = 1
            goto L_0x0117
        L_0x0116:
            r2 = 2
        L_0x0117:
            com.tencent.bugly.proguard.bm r2 = com.tencent.bugly.proguard.C2893ah.m6792a(r1, r2)     // Catch:{ all -> 0x018b }
            if (r2 != 0) goto L_0x0126
            java.lang.String r0 = "[UserInfo] Failed to create UserInfoPackage."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x0126:
            byte[] r2 = com.tencent.bugly.proguard.C2893ah.m6794a(r2)     // Catch:{ all -> 0x018b }
            if (r2 != 0) goto L_0x0135
            java.lang.String r0 = "[UserInfo] Failed to encode data."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x0135:
            boolean r5 = r0.f5976b     // Catch:{ all -> 0x018b }
            if (r5 == 0) goto L_0x013c
            r5 = 840(0x348, float:1.177E-42)
            goto L_0x013e
        L_0x013c:
            r5 = 640(0x280, float:8.97E-43)
        L_0x013e:
            android.content.Context r6 = r14.f5564a     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.bh r9 = com.tencent.bugly.proguard.C2893ah.m6789a(r6, r5, r2)     // Catch:{ all -> 0x018b }
            if (r9 != 0) goto L_0x014f
            java.lang.String r0 = "[UserInfo] Request package is null."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C2903an.m6864d(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x014f:
            com.tencent.bugly.crashreport.biz.a$1 r12 = new com.tencent.bugly.crashreport.biz.a$1     // Catch:{ all -> 0x018b }
            r12.<init>(r1)     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.common.strategy.a r1 = com.tencent.bugly.crashreport.common.strategy.C2854a.m6574a()     // Catch:{ all -> 0x018b }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r1 = r1.mo34340c()     // Catch:{ all -> 0x018b }
            boolean r2 = r0.f5976b     // Catch:{ all -> 0x018b }
            if (r2 == 0) goto L_0x0163
            java.lang.String r1 = r1.f5704r     // Catch:{ all -> 0x018b }
            goto L_0x0165
        L_0x0163:
            java.lang.String r1 = r1.f5706t     // Catch:{ all -> 0x018b }
        L_0x0165:
            r10 = r1
            boolean r0 = r0.f5976b     // Catch:{ all -> 0x018b }
            if (r0 == 0) goto L_0x016d
            java.lang.String r0 = com.tencent.bugly.crashreport.common.strategy.StrategyBean.f5688b     // Catch:{ all -> 0x018b }
            goto L_0x016f
        L_0x016d:
            java.lang.String r0 = com.tencent.bugly.crashreport.common.strategy.StrategyBean.f5687a     // Catch:{ all -> 0x018b }
        L_0x016f:
            r11 = r0
            com.tencent.bugly.proguard.ak r7 = com.tencent.bugly.proguard.C2896ak.m6805a()     // Catch:{ all -> 0x018b }
            r8 = 1001(0x3e9, float:1.403E-42)
            int r0 = r14.f5566c     // Catch:{ all -> 0x018b }
            if (r0 != r4) goto L_0x017c
            r13 = 1
            goto L_0x017d
        L_0x017c:
            r13 = 0
        L_0x017d:
            r7.mo34528a(r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x0182:
            java.lang.String r0 = "[UserInfo] There is no user info in local database."
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ all -> 0x018b }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r1)     // Catch:{ all -> 0x018b }
            monitor-exit(r14)
            return
        L_0x018b:
            r0 = move-exception
            monitor-exit(r14)
            goto L_0x018f
        L_0x018e:
            throw r0
        L_0x018f:
            goto L_0x018e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.biz.C2841a.m6415c():void");
    }

    /* renamed from: b */
    public void mo34250b() {
        C2901am a = C2901am.m6848a();
        if (a != null) {
            a.mo34547a(new Runnable() {
                /* class com.tencent.bugly.crashreport.biz.C2841a.C28432 */

                public void run() {
                    try {
                        C2841a.this.m6415c();
                    } catch (Throwable th) {
                        C2903an.m6858a(th);
                    }
                }
            });
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$b */
    /* compiled from: BUGLY */
    class C2845b implements Runnable {
        C2845b() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void
         arg types: [int, int, int]
         candidates:
          com.tencent.bugly.crashreport.biz.a.a(com.tencent.bugly.crashreport.biz.a, com.tencent.bugly.crashreport.biz.UserInfoBean, boolean):void
          com.tencent.bugly.crashreport.biz.a.a(int, boolean, long):void */
        public void run() {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis < C2841a.this.f5565b) {
                C2901am.m6848a().mo34548a(new C2845b(), (C2841a.this.f5565b - currentTimeMillis) + 5000);
                return;
            }
            C2841a.this.mo34247a(3, false, 0L);
            C2841a.this.mo34246a();
        }
    }

    /* renamed from: com.tencent.bugly.crashreport.biz.a$c */
    /* compiled from: BUGLY */
    class C2846c implements Runnable {

        /* renamed from: b */
        private long f5576b = 21600000;

        public C2846c(long j) {
            this.f5576b = j;
        }

        public void run() {
            C2841a.this.mo34250b();
            C2841a.this.mo34251b(this.f5576b);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad, boolean):long
     arg types: [java.lang.String, android.content.ContentValues, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad, boolean):long */
    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m6411a(UserInfoBean userInfoBean, boolean z) {
        List<UserInfoBean> a;
        if (userInfoBean != null) {
            if (z || userInfoBean.f5546b == 1 || (a = mo34245a(C2851a.m6470a(this.f5564a).f5660e)) == null || a.size() < 20) {
                long a2 = C2889ae.m6757a().mo34489a("t_ui", mo34243a(userInfoBean), (C2888ad) null, true);
                if (a2 >= 0) {
                    C2903an.m6863c("[Database] insert %s success with ID: %d", "t_ui", Long.valueOf(a2));
                    userInfoBean.f5545a = a2;
                    return;
                }
                return;
            }
            C2903an.m6857a("[UserInfo] There are too many user info in local: %d", Integer.valueOf(a.size()));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00b3 A[Catch:{ all -> 0x00bc }] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00b8 A[DONT_GENERATE] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.bugly.crashreport.biz.UserInfoBean> mo34245a(java.lang.String r13) {
        /*
            r12 = this;
            java.lang.String r0 = "_id"
            r1 = 0
            boolean r2 = com.tencent.bugly.proguard.C2908aq.m6915a(r13)     // Catch:{ all -> 0x00ab }
            if (r2 == 0) goto L_0x000b
            r5 = r1
            goto L_0x0022
        L_0x000b:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ab }
            r2.<init>()     // Catch:{ all -> 0x00ab }
            java.lang.String r3 = "_pc = '"
            r2.append(r3)     // Catch:{ all -> 0x00ab }
            r2.append(r13)     // Catch:{ all -> 0x00ab }
            java.lang.String r13 = "'"
            r2.append(r13)     // Catch:{ all -> 0x00ab }
            java.lang.String r13 = r2.toString()     // Catch:{ all -> 0x00ab }
            r5 = r13
        L_0x0022:
            com.tencent.bugly.proguard.ae r2 = com.tencent.bugly.proguard.C2889ae.m6757a()     // Catch:{ all -> 0x00ab }
            java.lang.String r3 = "t_ui"
            r4 = 0
            r6 = 0
            r7 = 0
            r8 = 1
            android.database.Cursor r13 = r2.mo34490a(r3, r4, r5, r6, r7, r8)     // Catch:{ all -> 0x00ab }
            if (r13 != 0) goto L_0x0038
            if (r13 == 0) goto L_0x0037
            r13.close()
        L_0x0037:
            return r1
        L_0x0038:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00a9 }
            r2.<init>()     // Catch:{ all -> 0x00a9 }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ all -> 0x00a9 }
            r3.<init>()     // Catch:{ all -> 0x00a9 }
        L_0x0042:
            boolean r4 = r13.moveToNext()     // Catch:{ all -> 0x00a9 }
            r5 = 0
            if (r4 == 0) goto L_0x0074
            com.tencent.bugly.crashreport.biz.UserInfoBean r4 = r12.mo34244a(r13)     // Catch:{ all -> 0x00a9 }
            if (r4 == 0) goto L_0x0053
            r3.add(r4)     // Catch:{ all -> 0x00a9 }
            goto L_0x0042
        L_0x0053:
            int r4 = r13.getColumnIndex(r0)     // Catch:{ all -> 0x006c }
            long r6 = r13.getLong(r4)     // Catch:{ all -> 0x006c }
            java.lang.String r4 = " or "
            r2.append(r4)     // Catch:{ all -> 0x006c }
            r2.append(r0)     // Catch:{ all -> 0x006c }
            java.lang.String r4 = " = "
            r2.append(r4)     // Catch:{ all -> 0x006c }
            r2.append(r6)     // Catch:{ all -> 0x006c }
            goto L_0x0042
        L_0x006c:
            java.lang.String r4 = "[Database] unknown id."
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.proguard.C2903an.m6864d(r4, r5)     // Catch:{ all -> 0x00a9 }
            goto L_0x0042
        L_0x0074:
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00a9 }
            int r2 = r0.length()     // Catch:{ all -> 0x00a9 }
            if (r2 <= 0) goto L_0x00a3
            r2 = 4
            java.lang.String r8 = r0.substring(r2)     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.proguard.ae r6 = com.tencent.bugly.proguard.C2889ae.m6757a()     // Catch:{ all -> 0x00a9 }
            java.lang.String r7 = "t_ui"
            r9 = 0
            r10 = 0
            r11 = 1
            int r0 = r6.mo34488a(r7, r8, r9, r10, r11)     // Catch:{ all -> 0x00a9 }
            java.lang.String r2 = "[Database] deleted %s error data %d"
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00a9 }
            java.lang.String r6 = "t_ui"
            r4[r5] = r6     // Catch:{ all -> 0x00a9 }
            r5 = 1
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00a9 }
            r4[r5] = r0     // Catch:{ all -> 0x00a9 }
            com.tencent.bugly.proguard.C2903an.m6864d(r2, r4)     // Catch:{ all -> 0x00a9 }
        L_0x00a3:
            if (r13 == 0) goto L_0x00a8
            r13.close()
        L_0x00a8:
            return r3
        L_0x00a9:
            r0 = move-exception
            goto L_0x00ad
        L_0x00ab:
            r0 = move-exception
            r13 = r1
        L_0x00ad:
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x00bc }
            if (r2 != 0) goto L_0x00b6
            r0.printStackTrace()     // Catch:{ all -> 0x00bc }
        L_0x00b6:
            if (r13 == 0) goto L_0x00bb
            r13.close()
        L_0x00bb:
            return r1
        L_0x00bc:
            r0 = move-exception
            if (r13 == 0) goto L_0x00c2
            r13.close()
        L_0x00c2:
            goto L_0x00c4
        L_0x00c3:
            throw r0
        L_0x00c4:
            goto L_0x00c3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.biz.C2841a.mo34245a(java.lang.String):java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int */
    /* renamed from: a */
    public void mo34249a(List<UserInfoBean> list) {
        if (list != null && list.size() != 0) {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            while (i < list.size() && i < 50) {
                sb.append(" or ");
                sb.append("_id");
                sb.append(" = ");
                sb.append(list.get(i).f5545a);
                i++;
            }
            String sb2 = sb.toString();
            if (sb2.length() > 0) {
                sb2 = sb2.substring(4);
            }
            String str = sb2;
            sb.setLength(0);
            try {
                C2903an.m6863c("[Database] deleted %s data %d", "t_ui", Integer.valueOf(C2889ae.m6757a().mo34488a("t_ui", str, (String[]) null, (C2888ad) null, true)));
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public ContentValues mo34243a(UserInfoBean userInfoBean) {
        if (userInfoBean == null) {
            return null;
        }
        try {
            ContentValues contentValues = new ContentValues();
            if (userInfoBean.f5545a > 0) {
                contentValues.put("_id", Long.valueOf(userInfoBean.f5545a));
            }
            contentValues.put("_tm", Long.valueOf(userInfoBean.f5549e));
            contentValues.put("_ut", Long.valueOf(userInfoBean.f5550f));
            contentValues.put("_tp", Integer.valueOf(userInfoBean.f5546b));
            contentValues.put("_pc", userInfoBean.f5547c);
            contentValues.put("_dt", C2908aq.m6918a(userInfoBean));
            return contentValues;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public UserInfoBean mo34244a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            byte[] blob = cursor.getBlob(cursor.getColumnIndex("_dt"));
            if (blob == null) {
                return null;
            }
            long j = cursor.getLong(cursor.getColumnIndex("_id"));
            UserInfoBean userInfoBean = (UserInfoBean) C2908aq.m6896a(blob, UserInfoBean.CREATOR);
            if (userInfoBean != null) {
                userInfoBean.f5545a = j;
            }
            return userInfoBean;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }
}
