package com.tencent.stat;

import com.baidu.mobstat.Config;
import org.json.JSONException;
import org.json.JSONObject;

public class NetworkMonitor {

    /* renamed from: a */
    private long f6890a = 0;

    /* renamed from: b */
    private int f6891b = 0;

    /* renamed from: c */
    private String f6892c = "";

    /* renamed from: d */
    private int f6893d = 0;

    /* renamed from: e */
    private String f6894e = "";

    public long getMillisecondsConsume() {
        return this.f6890a;
    }

    public void setMillisecondsConsume(long j) {
        this.f6890a = j;
    }

    public int getStatusCode() {
        return this.f6891b;
    }

    public void setStatusCode(int i) {
        this.f6891b = i;
    }

    public String getDomain() {
        return this.f6892c;
    }

    public void setDomain(String str) {
        this.f6892c = str;
    }

    public int getPort() {
        return this.f6893d;
    }

    public void setPort(int i) {
        this.f6893d = i;
    }

    public String getRemoteIp() {
        return this.f6894e;
    }

    public void setRemoteIp(String str) {
        this.f6894e = str;
    }

    public JSONObject toJSONObject() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("tm", this.f6890a);
            jSONObject.put(Config.STAT_SDK_TYPE, this.f6891b);
            if (this.f6892c != null) {
                jSONObject.put("dm", this.f6892c);
            }
            jSONObject.put(Config.PLATFORM_TYPE, this.f6893d);
            if (this.f6894e != null) {
                jSONObject.put("rip", this.f6894e);
            }
            jSONObject.put("ts", System.currentTimeMillis() / 1000);
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
