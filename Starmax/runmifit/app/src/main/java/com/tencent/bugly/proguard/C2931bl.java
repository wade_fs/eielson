package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.bl */
/* compiled from: BUGLY */
public final class C2931bl extends C2944m {

    /* renamed from: i */
    static Map<String, String> f6166i = new HashMap();

    /* renamed from: a */
    public long f6167a = 0;

    /* renamed from: b */
    public byte f6168b = 0;

    /* renamed from: c */
    public String f6169c = "";

    /* renamed from: d */
    public String f6170d = "";

    /* renamed from: e */
    public String f6171e = "";

    /* renamed from: f */
    public Map<String, String> f6172f = null;

    /* renamed from: g */
    public String f6173g = "";

    /* renamed from: h */
    public boolean f6174h = true;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34645a(this.f6167a, 0);
        lVar.mo34660b(this.f6168b, 1);
        String str = this.f6169c;
        if (str != null) {
            lVar.mo34648a(str, 2);
        }
        String str2 = this.f6170d;
        if (str2 != null) {
            lVar.mo34648a(str2, 3);
        }
        String str3 = this.f6171e;
        if (str3 != null) {
            lVar.mo34648a(str3, 4);
        }
        Map<String, String> map = this.f6172f;
        if (map != null) {
            lVar.mo34650a((Map) map, 5);
        }
        String str4 = this.f6173g;
        if (str4 != null) {
            lVar.mo34648a(str4, 6);
        }
        lVar.mo34652a(this.f6174h, 7);
    }

    static {
        f6166i.put("", "");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
     arg types: [boolean, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6167a = kVar.mo34618a(this.f6167a, 0, true);
        this.f6168b = kVar.mo34613a(this.f6168b, 1, true);
        this.f6169c = kVar.mo34621a(2, false);
        this.f6170d = kVar.mo34621a(3, false);
        this.f6171e = kVar.mo34621a(4, false);
        this.f6172f = (Map) kVar.mo34620a((Object) f6166i, 5, false);
        this.f6173g = kVar.mo34621a(6, false);
        this.f6174h = kVar.mo34629a(this.f6174h, 7, false);
    }
}
