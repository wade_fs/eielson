package com.tencent.bugly.beta.global;

import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.text.TextUtils;
import com.tencent.bugly.beta.download.C2801a;
import com.tencent.bugly.beta.download.C2802b;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2947p;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.beta.global.f */
/* compiled from: BUGLY */
public class C2809f {

    /* renamed from: a */
    public static C2809f f5393a = new C2809f();

    /* renamed from: b */
    final Map<String, DownloadTask> f5394b = Collections.synchronizedMap(new HashMap());

    /* renamed from: c */
    Handler f5395c = new Handler(Looper.getMainLooper());

    /* renamed from: d */
    private List<Runnable> f5396d = new ArrayList();

    /* renamed from: a */
    public synchronized void mo34059a(Runnable runnable, int i) {
        if (this.f5394b.size() == 0) {
            runnable.run();
        } else {
            C2807d dVar = new C2807d(6, false, runnable);
            this.f5395c.postDelayed(dVar, (long) i);
            mo34058a(dVar);
        }
    }

    /* renamed from: a */
    public synchronized void mo34058a(Runnable runnable) {
        if (this.f5394b.size() == 0) {
            runnable.run();
        } else {
            this.f5396d.add(runnable);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
     arg types: [java.lang.String, com.tencent.bugly.beta.global.ResBean]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean */
    /* renamed from: a */
    public void mo34057a(C2802b bVar, Map<String, String> map) {
        DownloadTask downloadTask;
        if (bVar != null) {
            if (map == null || map.isEmpty()) {
                this.f5394b.clear();
                ResBean.f5325a = new ResBean();
                C2804a.m6270a("rb.bch", (Parcelable) ResBean.f5325a);
                return;
            }
            for (DownloadTask downloadTask2 : this.f5394b.values()) {
                downloadTask2.delete(true);
            }
            this.f5394b.clear();
            C2801a aVar = new C2801a(1, this, this.f5394b);
            String[] strArr = ResBean.f5326b;
            for (String str : strArr) {
                if (!map.containsKey(str)) {
                    this.f5394b.clear();
                    ResBean.f5325a = new ResBean();
                    C2804a.m6270a("rb.bch", (Parcelable) ResBean.f5325a);
                    return;
                }
                String str2 = map.get(str);
                if (!str.startsWith("IMG_") || TextUtils.isEmpty(str2)) {
                    ResBean.f5325a.mo34046a(str, str2);
                } else {
                    ResBean.f5325a.mo34046a(str, "");
                    Iterator<DownloadTask> it = this.f5394b.values().iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            downloadTask = null;
                            break;
                        }
                        downloadTask = it.next();
                        if (downloadTask.getDownloadUrl().equals(str2)) {
                            break;
                        }
                    }
                    if (downloadTask == null) {
                        downloadTask = bVar.mo34042a(str2, C2808e.f5336E.f5384r.getAbsolutePath(), null, null);
                    }
                    if (downloadTask != null) {
                        downloadTask.addListener(aVar);
                        downloadTask.setNeededNotify(false);
                        this.f5394b.put(str, downloadTask);
                    }
                }
            }
            C2804a.m6270a("rb.bch", (Parcelable) ResBean.f5325a);
            if (!this.f5394b.isEmpty()) {
                for (DownloadTask downloadTask3 : this.f5394b.values()) {
                    downloadTask3.download();
                }
            }
        }
    }

    /* renamed from: a */
    public void mo34056a() {
        ArrayList<String> arrayList = new ArrayList<>();
        String[] strArr = ResBean.f5326b;
        for (String str : strArr) {
            String a = ResBean.f5325a.mo34045a(str);
            if (str.startsWith("IMG_") && !TextUtils.isEmpty(a)) {
                arrayList.add(a);
            }
        }
        File[] listFiles = C2808e.f5336E.f5384r.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            for (File file : listFiles) {
                boolean z = true;
                for (String str2 : arrayList) {
                    if (file.getAbsolutePath().equals(str2)) {
                        z = false;
                    }
                }
                if (z) {
                    C2947p.f6229a.mo34670b(file.getAbsolutePath());
                    if (!file.delete()) {
                        C2903an.m6865e("cannot deleteCache file:%s", file.getAbsolutePath());
                    }
                }
            }
        }
    }

    /* renamed from: b */
    public void mo34060b() {
        synchronized (this) {
            for (Runnable runnable : this.f5396d) {
                runnable.run();
            }
            for (DownloadTask downloadTask : this.f5394b.values()) {
                downloadTask.delete(false);
            }
            this.f5396d.clear();
            this.f5394b.clear();
        }
    }
}
