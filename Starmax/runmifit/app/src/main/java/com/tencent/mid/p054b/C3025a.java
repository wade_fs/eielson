package com.tencent.mid.p054b;

import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.mid.b.a */
public class C3025a {

    /* renamed from: a */
    public static String f6543a = "ts";

    /* renamed from: b */
    public static String f6544b = "times";

    /* renamed from: c */
    public static String f6545c = "mfreq";

    /* renamed from: d */
    public static String f6546d = "mdays";

    /* renamed from: i */
    private static C3038d f6547i = Util.getLogger();

    /* renamed from: e */
    private long f6548e = 0;

    /* renamed from: f */
    private int f6549f = 1;

    /* renamed from: g */
    private int f6550g = 1024;

    /* renamed from: h */
    private int f6551h = 3;

    public C3025a() {
    }

    public C3025a(String str) {
        if (Util.isStringValid(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull(f6543a)) {
                    this.f6548e = jSONObject.getLong(f6543a);
                }
                if (!jSONObject.isNull(f6545c)) {
                    this.f6550g = jSONObject.getInt(f6545c);
                }
                if (!jSONObject.isNull(f6544b)) {
                    this.f6549f = jSONObject.getInt(f6544b);
                }
                if (!jSONObject.isNull(f6546d)) {
                    this.f6551h = jSONObject.getInt(f6546d);
                }
            } catch (JSONException e) {
                f6547i.mo34952d(e.toString());
            }
        }
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(f6543a, this.f6548e);
            jSONObject.put(f6544b, this.f6549f);
            jSONObject.put(f6545c, this.f6550g);
            jSONObject.put(f6546d, this.f6551h);
        } catch (JSONException e) {
            f6547i.mo34952d(e.toString());
        }
        return jSONObject.toString();
    }

    /* renamed from: a */
    public int mo34877a() {
        return this.f6551h;
    }

    /* renamed from: a */
    public void mo34878a(int i) {
        this.f6551h = i;
    }

    /* renamed from: b */
    public long mo34880b() {
        return this.f6548e;
    }

    /* renamed from: a */
    public void mo34879a(long j) {
        this.f6548e = j;
    }

    /* renamed from: c */
    public int mo34882c() {
        return this.f6549f;
    }

    /* renamed from: b */
    public void mo34881b(int i) {
        this.f6549f = i;
    }

    /* renamed from: d */
    public int mo34884d() {
        return this.f6550g;
    }

    /* renamed from: c */
    public void mo34883c(int i) {
        this.f6550g = i;
    }
}
