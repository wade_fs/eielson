package com.tencent.p055mm.opensdk.diffdev.p058a;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;
import com.tencent.p055mm.opensdk.diffdev.OAuthErrCode;
import com.tencent.p055mm.opensdk.diffdev.OAuthListener;
import java.io.File;
import org.json.JSONObject;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.d */
public final class C3059d extends AsyncTask<Void, Void, C3060a> {
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static final boolean f6631i = (Environment.getExternalStorageState().equals("mounted") && new File(Environment.getExternalStorageDirectory().getAbsolutePath()).canWrite());
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static final String f6632j = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/tencent/MicroMsg/oauth_qrcode.png");

    /* renamed from: k */
    private static String f6633k;
    private String appId;

    /* renamed from: l */
    private String f6634l;

    /* renamed from: m */
    private String f6635m;

    /* renamed from: n */
    private OAuthListener f6636n;

    /* renamed from: o */
    private C3062f f6637o;
    private String scope;
    private String signature;

    /* renamed from: com.tencent.mm.opensdk.diffdev.a.d$a */
    static class C3060a {

        /* renamed from: p */
        public OAuthErrCode f6638p;

        /* renamed from: q */
        public String f6639q;

        /* renamed from: r */
        public String f6640r;

        /* renamed from: s */
        public String f6641s;

        /* renamed from: t */
        public int f6642t;

        /* renamed from: u */
        public String f6643u;

        /* renamed from: v */
        public byte[] f6644v;

        private C3060a() {
        }

        /* renamed from: a */
        public static C3060a m7551a(byte[] bArr) {
            OAuthErrCode oAuthErrCode;
            String format;
            C3060a aVar = new C3060a();
            if (bArr == null || bArr.length == 0) {
                Log.e("MicroMsg.SDK.GetQRCodeResult", "parse fail, buf is null");
                oAuthErrCode = OAuthErrCode.WechatAuth_Err_NetworkErr;
            } else {
                try {
                    try {
                        JSONObject jSONObject = new JSONObject(new String(bArr, "utf-8"));
                        int i = jSONObject.getInt("errcode");
                        if (i != 0) {
                            Log.e("MicroMsg.SDK.GetQRCodeResult", String.format("resp errcode = %d", Integer.valueOf(i)));
                            aVar.f6638p = OAuthErrCode.WechatAuth_Err_NormalErr;
                            aVar.f6642t = i;
                            aVar.f6643u = jSONObject.optString("errmsg");
                            return aVar;
                        }
                        String string = jSONObject.getJSONObject("qrcode").getString("qrcodebase64");
                        if (string != null) {
                            if (string.length() != 0) {
                                byte[] decode = Base64.decode(string, 0);
                                if (decode != null) {
                                    if (decode.length != 0) {
                                        if (C3059d.f6631i) {
                                            File file = new File(C3059d.f6632j);
                                            file.mkdirs();
                                            if (file.exists()) {
                                                file.delete();
                                            }
                                            if (!m7552a(C3059d.f6632j, decode)) {
                                                Log.e("MicroMsg.SDK.GetQRCodeResult", String.format("writeToFile fail, qrcodeBuf length = %d", Integer.valueOf(decode.length)));
                                                aVar.f6638p = OAuthErrCode.WechatAuth_Err_NormalErr;
                                                return aVar;
                                            }
                                            aVar.f6638p = OAuthErrCode.WechatAuth_Err_OK;
                                            aVar.f6641s = C3059d.f6632j;
                                            aVar.f6639q = jSONObject.getString("uuid");
                                            aVar.f6640r = jSONObject.getString("appname");
                                            Log.d("MicroMsg.SDK.GetQRCodeResult", String.format("parse succ, save in external storage, uuid = %s, appname = %s, imgPath = %s", aVar.f6639q, aVar.f6640r, aVar.f6641s));
                                            return aVar;
                                        }
                                        aVar.f6638p = OAuthErrCode.WechatAuth_Err_OK;
                                        aVar.f6644v = decode;
                                        aVar.f6639q = jSONObject.getString("uuid");
                                        aVar.f6640r = jSONObject.getString("appname");
                                        Log.d("MicroMsg.SDK.GetQRCodeResult", String.format("parse succ, save in memory, uuid = %s, appname = %s, imgBufLength = %d", aVar.f6639q, aVar.f6640r, Integer.valueOf(aVar.f6644v.length)));
                                        return aVar;
                                    }
                                }
                                Log.e("MicroMsg.SDK.GetQRCodeResult", "parse fail, qrcodeBuf is null");
                                aVar.f6638p = OAuthErrCode.WechatAuth_Err_JsonDecodeErr;
                                return aVar;
                            }
                        }
                        Log.e("MicroMsg.SDK.GetQRCodeResult", "parse fail, qrcodeBase64 is null");
                        aVar.f6638p = OAuthErrCode.WechatAuth_Err_JsonDecodeErr;
                        return aVar;
                    } catch (Exception e) {
                        format = String.format("parse json fail, ex = %s", e.getMessage());
                        Log.e("MicroMsg.SDK.GetQRCodeResult", format);
                        oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                        aVar.f6638p = oAuthErrCode;
                        return aVar;
                    }
                } catch (Exception e2) {
                    format = String.format("parse fail, build String fail, ex = %s", e2.getMessage());
                    Log.e("MicroMsg.SDK.GetQRCodeResult", format);
                    oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                    aVar.f6638p = oAuthErrCode;
                    return aVar;
                }
            }
            aVar.f6638p = oAuthErrCode;
            return aVar;
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x004f A[SYNTHETIC, Splitter:B:20:0x004f] */
        /* JADX WARNING: Removed duplicated region for block: B:25:0x006b A[SYNTHETIC, Splitter:B:25:0x006b] */
        /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static boolean m7552a(java.lang.String r4, byte[] r5) {
            /*
                java.lang.String r0 = "fout.close() exception:"
                java.lang.String r1 = "MicroMsg.SDK.GetQRCodeResult"
                r2 = 0
                java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0037 }
                r3.<init>(r4)     // Catch:{ Exception -> 0x0037 }
                r3.write(r5)     // Catch:{ Exception -> 0x0032, all -> 0x002f }
                r3.flush()     // Catch:{ Exception -> 0x0032, all -> 0x002f }
                r3.close()     // Catch:{ IOException -> 0x0014 }
                goto L_0x0028
            L_0x0014:
                r4 = move-exception
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>(r0)
                java.lang.String r4 = r4.getMessage()
                r5.append(r4)
                java.lang.String r4 = r5.toString()
                android.util.Log.e(r1, r4)
            L_0x0028:
                java.lang.String r4 = "writeToFile ok!"
                android.util.Log.d(r1, r4)
                r4 = 1
                return r4
            L_0x002f:
                r4 = move-exception
                r2 = r3
                goto L_0x0069
            L_0x0032:
                r4 = move-exception
                r2 = r3
                goto L_0x0038
            L_0x0035:
                r4 = move-exception
                goto L_0x0069
            L_0x0037:
                r4 = move-exception
            L_0x0038:
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0035 }
                java.lang.String r3 = "write to file error, exception:"
                r5.<init>(r3)     // Catch:{ all -> 0x0035 }
                java.lang.String r4 = r4.getMessage()     // Catch:{ all -> 0x0035 }
                r5.append(r4)     // Catch:{ all -> 0x0035 }
                java.lang.String r4 = r5.toString()     // Catch:{ all -> 0x0035 }
                android.util.Log.w(r1, r4)     // Catch:{ all -> 0x0035 }
                if (r2 == 0) goto L_0x0067
                r2.close()     // Catch:{ IOException -> 0x0053 }
                goto L_0x0067
            L_0x0053:
                r4 = move-exception
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r5.<init>(r0)
                java.lang.String r4 = r4.getMessage()
                r5.append(r4)
                java.lang.String r4 = r5.toString()
                android.util.Log.e(r1, r4)
            L_0x0067:
                r4 = 0
                return r4
            L_0x0069:
                if (r2 == 0) goto L_0x0083
                r2.close()     // Catch:{ IOException -> 0x006f }
                goto L_0x0083
            L_0x006f:
                r5 = move-exception
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>(r0)
                java.lang.String r5 = r5.getMessage()
                r2.append(r5)
                java.lang.String r5 = r2.toString()
                android.util.Log.e(r1, r5)
            L_0x0083:
                throw r4
            */
            throw new UnsupportedOperationException("Method not decompiled: com.tencent.p055mm.opensdk.diffdev.p058a.C3059d.C3060a.m7552a(java.lang.String, byte[]):boolean");
        }
    }

    static {
        f6633k = null;
        f6633k = "http://open.weixin.qq.com/connect/sdk/qrconnect?appid=%s&noncestr=%s&timestamp=%s&scope=%s&signature=%s";
    }

    public C3059d(String str, String str2, String str3, String str4, String str5, OAuthListener oAuthListener) {
        this.appId = str;
        this.scope = str2;
        this.f6634l = str3;
        this.f6635m = str4;
        this.signature = str5;
        this.f6636n = oAuthListener;
    }

    /* renamed from: a */
    public final boolean mo34975a() {
        Log.i("MicroMsg.SDK.GetQRCodeTask", "cancelTask");
        C3062f fVar = this.f6637o;
        return fVar == null ? cancel(true) : fVar.cancel(true);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        Log.i("MicroMsg.SDK.GetQRCodeTask", "external storage available = " + f6631i);
        String format = String.format(f6633k, this.appId, this.f6634l, this.f6635m, this.scope, this.signature);
        long currentTimeMillis = System.currentTimeMillis();
        byte[] a = C3061e.m7553a(format, -1);
        Log.d("MicroMsg.SDK.GetQRCodeTask", String.format("doInBackground, url = %s, time consumed = %d(ms)", format, Long.valueOf(System.currentTimeMillis() - currentTimeMillis)));
        return C3060a.m7551a(a);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        C3060a aVar = (C3060a) obj;
        if (aVar.f6638p == OAuthErrCode.WechatAuth_Err_OK) {
            Log.d("MicroMsg.SDK.GetQRCodeTask", "onPostExecute, get qrcode success");
            this.f6636n.onAuthGotQrcode(aVar.f6641s, aVar.f6644v);
            this.f6637o = new C3062f(aVar.f6639q, this.f6636n);
            C3062f fVar = this.f6637o;
            if (Build.VERSION.SDK_INT >= 11) {
                fVar.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
            } else {
                fVar.execute(new Void[0]);
            }
        } else {
            Log.e("MicroMsg.SDK.GetQRCodeTask", String.format("onPostExecute, get qrcode fail, OAuthErrCode = %s", aVar.f6638p));
            this.f6636n.onAuthFinish(aVar.f6638p, null);
        }
    }
}
