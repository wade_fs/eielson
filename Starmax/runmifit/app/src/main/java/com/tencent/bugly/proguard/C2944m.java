package com.tencent.bugly.proguard;

import java.io.Serializable;

/* renamed from: com.tencent.bugly.proguard.m */
/* compiled from: BUGLY */
public abstract class C2944m implements Serializable {
    /* renamed from: a */
    public abstract void mo34475a(C2941k kVar);

    /* renamed from: a */
    public abstract void mo34476a(C2943l lVar);

    /* renamed from: a */
    public abstract void mo34477a(StringBuilder sb, int i);

    public String toString() {
        StringBuilder sb = new StringBuilder();
        mo34477a(sb, 0);
        return sb.toString();
    }
}
