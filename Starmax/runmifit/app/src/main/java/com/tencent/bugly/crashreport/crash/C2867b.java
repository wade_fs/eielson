package com.tencent.bugly.crashreport.crash;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.text.TextUtils;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.C2797b;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.info.PlugInBean;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.proguard.C2888ad;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2892ag;
import com.tencent.bugly.proguard.C2893ah;
import com.tencent.bugly.proguard.C2895aj;
import com.tencent.bugly.proguard.C2896ak;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import com.tencent.bugly.proguard.C2922bc;
import com.tencent.bugly.proguard.C2924be;
import com.tencent.bugly.proguard.C2925bf;
import com.tencent.bugly.proguard.C2926bg;
import com.tencent.bugly.proguard.C2927bh;
import com.tencent.bugly.proguard.C2928bi;
import com.tencent.bugly.proguard.C2944m;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

/* renamed from: com.tencent.bugly.crashreport.crash.b */
/* compiled from: BUGLY */
public class C2867b {

    /* renamed from: a */
    public static int f5812a;

    /* renamed from: b */
    protected final Context f5813b;

    /* renamed from: c */
    protected final C2896ak f5814c;

    /* renamed from: d */
    protected final C2889ae f5815d;

    /* renamed from: e */
    protected final C2854a f5816e;

    /* renamed from: f */
    protected C2876f f5817f;

    /* renamed from: g */
    protected BuglyStrategy.C2796a f5818g;

    public C2867b(int i, Context context, C2896ak akVar, C2889ae aeVar, C2854a aVar, BuglyStrategy.C2796a aVar2, C2876f fVar) {
        f5812a = i;
        this.f5813b = context;
        this.f5814c = akVar;
        this.f5815d = aeVar;
        this.f5816e = aVar;
        this.f5818g = aVar2;
        this.f5817f = fVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public List<C2858a> mo34387a(List<C2858a> list) {
        if (list == null || list.size() == 0) {
            return null;
        }
        long currentTimeMillis = System.currentTimeMillis();
        ArrayList arrayList = new ArrayList();
        for (C2858a aVar : list) {
            if (aVar.f5781d && aVar.f5779b <= currentTimeMillis - 86400000) {
                arrayList.add(aVar);
            }
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public CrashDetailBean mo34385a(List<C2858a> list, CrashDetailBean crashDetailBean) {
        List<CrashDetailBean> b;
        String[] split;
        if (list == null || list.size() == 0) {
            return crashDetailBean;
        }
        CrashDetailBean crashDetailBean2 = null;
        ArrayList arrayList = new ArrayList(10);
        for (C2858a aVar : list) {
            if (aVar.f5782e) {
                arrayList.add(aVar);
            }
        }
        if (arrayList.size() > 0 && (b = mo34395b(arrayList)) != null && b.size() > 0) {
            Collections.sort(b);
            CrashDetailBean crashDetailBean3 = null;
            for (int i = 0; i < b.size(); i++) {
                CrashDetailBean crashDetailBean4 = b.get(i);
                if (i == 0) {
                    crashDetailBean3 = crashDetailBean4;
                } else if (!(crashDetailBean4.f5770s == null || (split = crashDetailBean4.f5770s.split("\n")) == null)) {
                    for (String str : split) {
                        if (!crashDetailBean3.f5770s.contains("" + str)) {
                            crashDetailBean3.f5771t++;
                            crashDetailBean3.f5770s += str + "\n";
                        }
                    }
                }
            }
            crashDetailBean2 = crashDetailBean3;
        }
        if (crashDetailBean2 == null) {
            crashDetailBean.f5761j = true;
            crashDetailBean.f5771t = 0;
            crashDetailBean.f5770s = "";
            crashDetailBean2 = crashDetailBean;
        }
        for (C2858a aVar2 : list) {
            if (!aVar2.f5782e && !aVar2.f5781d) {
                if (!crashDetailBean2.f5770s.contains("" + aVar2.f5779b)) {
                    crashDetailBean2.f5771t++;
                    crashDetailBean2.f5770s += aVar2.f5779b + "\n";
                }
            }
        }
        if (crashDetailBean2.f5769r != crashDetailBean.f5769r) {
            if (!crashDetailBean2.f5770s.contains("" + crashDetailBean.f5769r)) {
                crashDetailBean2.f5771t++;
                crashDetailBean2.f5770s += crashDetailBean.f5769r + "\n";
            }
        }
        return crashDetailBean2;
    }

    /* renamed from: a */
    public boolean mo34391a(CrashDetailBean crashDetailBean) {
        return mo34392a(crashDetailBean, -123456789);
    }

    /* renamed from: a */
    public boolean mo34392a(CrashDetailBean crashDetailBean, int i) {
        CrashDetailBean crashDetailBean2 = crashDetailBean;
        if (crashDetailBean2 == null) {
            return true;
        }
        if (C2869c.f5834n != null && !C2869c.f5834n.isEmpty()) {
            C2903an.m6863c("Crash filter for crash stack is: %s", C2869c.f5834n);
            if (crashDetailBean2.f5768q.contains(C2869c.f5834n)) {
                C2903an.m6864d("This crash contains the filter string set. It will not be record and upload.", new Object[0]);
                return true;
            }
        }
        if (C2869c.f5835o != null && !C2869c.f5835o.isEmpty()) {
            C2903an.m6863c("Crash regular filter for crash stack is: %s", C2869c.f5835o);
            if (Pattern.compile(C2869c.f5835o).matcher(crashDetailBean2.f5768q).find()) {
                C2903an.m6864d("This crash matches the regular filter string set. It will not be record and upload.", new Object[0]);
                return true;
            }
        }
        boolean z = crashDetailBean2.f5753b == 1;
        String str = crashDetailBean2.f5765n;
        String str2 = crashDetailBean2.f5766o;
        String str3 = crashDetailBean2.f5767p;
        String str4 = crashDetailBean2.f5768q;
        long j = crashDetailBean2.f5769r;
        String str5 = crashDetailBean2.f5764m;
        String str6 = crashDetailBean2.f5756e;
        String str7 = crashDetailBean2.f5754c;
        String str8 = crashDetailBean2.f5729A;
        String str9 = crashDetailBean2.f5730B;
        if (this.f5817f != null) {
            C2903an.m6863c("Calling 'onCrashSaving' of RQD crash listener.", new Object[0]);
            if (!this.f5817f.mo34432a(z, str, str2, str3, str4, i, j, str5, str6, str7, str8, str9)) {
                C2903an.m6864d("Crash listener 'onCrashSaving' return 'false' thus will not handle this crash.", new Object[0]);
                return true;
            }
        }
        if (crashDetailBean2.f5753b != 2) {
            C2892ag agVar = new C2892ag();
            agVar.f5965b = 1;
            agVar.f5966c = crashDetailBean2.f5729A;
            agVar.f5967d = crashDetailBean2.f5730B;
            agVar.f5968e = crashDetailBean2.f5769r;
            this.f5815d.mo34501b(1);
            this.f5815d.mo34498a(agVar);
            C2903an.m6860b("[crash] a crash occur, handling...", new Object[0]);
        } else {
            C2903an.m6860b("[crash] a caught exception occur, handling...", new Object[0]);
        }
        List<C2858a> b = mo34394b();
        ArrayList arrayList = null;
        if (b != null && b.size() > 0) {
            arrayList = new ArrayList(10);
            ArrayList<C2858a> arrayList2 = new ArrayList<>(10);
            arrayList.addAll(mo34387a(b));
            b.removeAll(arrayList);
            if (!C2797b.f5303c && C2869c.f5824d) {
                boolean z2 = false;
                for (C2858a aVar : b) {
                    if (crashDetailBean2.f5772u.equals(aVar.f5780c)) {
                        if (aVar.f5782e) {
                            z2 = true;
                        }
                        arrayList2.add(aVar);
                    }
                }
                if (z2 || arrayList2.size() >= C2869c.f5823c) {
                    C2903an.m6857a("same crash occur too much do merged!", new Object[0]);
                    CrashDetailBean a = mo34385a(arrayList2, crashDetailBean2);
                    for (C2858a aVar2 : arrayList2) {
                        if (aVar2.f5778a != a.f5752a) {
                            arrayList.add(aVar2);
                        }
                    }
                    mo34401e(a);
                    mo34398c(arrayList);
                    C2903an.m6860b("[crash] save crash success. For this device crash many times, it will not upload crashes immediately", new Object[0]);
                    return true;
                }
            }
        }
        mo34401e(crashDetailBean);
        if (arrayList != null && !arrayList.isEmpty()) {
            mo34398c(arrayList);
        }
        C2903an.m6860b("[crash] save crash success", new Object[0]);
        return false;
    }

    /* renamed from: a */
    public List<CrashDetailBean> mo34386a() {
        StrategyBean c = C2854a.m6574a().mo34340c();
        if (c == null) {
            C2903an.m6864d("have not synced remote!", new Object[0]);
            return null;
        } else if (!c.f5693g) {
            C2903an.m6864d("Crashreport remote closed, please check your APP ID correct and Version available, then uninstall and reinstall your app.", new Object[0]);
            C2903an.m6860b("[init] WARNING! Crashreport closed by server, please check your APP ID correct and Version available, then uninstall and reinstall your app.", new Object[0]);
            return null;
        } else {
            long currentTimeMillis = System.currentTimeMillis();
            long b = C2908aq.m6923b();
            List<C2858a> b2 = mo34394b();
            C2903an.m6863c("Size of crash list loaded from DB: %s", Integer.valueOf(b2.size()));
            if (b2 == null || b2.size() <= 0) {
                return null;
            }
            ArrayList arrayList = new ArrayList();
            arrayList.addAll(mo34387a(b2));
            b2.removeAll(arrayList);
            Iterator<C2858a> it = b2.iterator();
            while (it.hasNext()) {
                C2858a next = it.next();
                if (next.f5779b < b - C2869c.f5827g) {
                    it.remove();
                    arrayList.add(next);
                } else if (next.f5781d) {
                    if (next.f5779b >= currentTimeMillis - 86400000) {
                        it.remove();
                    } else if (!next.f5782e) {
                        it.remove();
                        arrayList.add(next);
                    }
                } else if (((long) next.f5783f) >= 3 && next.f5779b < currentTimeMillis - 86400000) {
                    it.remove();
                    arrayList.add(next);
                }
            }
            if (arrayList.size() > 0) {
                mo34398c(arrayList);
            }
            ArrayList arrayList2 = new ArrayList();
            List<CrashDetailBean> b3 = mo34395b(b2);
            if (b3 != null && b3.size() > 0) {
                String str = C2851a.m6471b().f5671p;
                Iterator<CrashDetailBean> it2 = b3.iterator();
                while (it2.hasNext()) {
                    CrashDetailBean next2 = it2.next();
                    if (!str.equals(next2.f5757f)) {
                        it2.remove();
                        arrayList2.add(next2);
                    }
                }
            }
            if (arrayList2.size() > 0) {
                mo34400d(arrayList2);
            }
            return b3;
        }
    }

    /* renamed from: b */
    public void mo34396b(CrashDetailBean crashDetailBean) {
        if (this.f5817f != null) {
            boolean z = false;
            C2903an.m6863c("Calling 'onCrashHandleEnd' of RQD crash listener.", new Object[0]);
            C2876f fVar = this.f5817f;
            if (crashDetailBean.f5753b == 1) {
                z = true;
            }
            fVar.mo34435b(z);
        }
    }

    /* renamed from: a */
    public void mo34388a(CrashDetailBean crashDetailBean, long j, boolean z) {
        if (C2869c.f5832l) {
            C2903an.m6857a("try to upload right now", new Object[0]);
            ArrayList arrayList = new ArrayList();
            arrayList.add(crashDetailBean);
            mo34389a(arrayList, j, z, crashDetailBean.f5753b == 7, z);
        }
    }

    /* renamed from: a */
    public void mo34389a(final List<CrashDetailBean> list, long j, boolean z, boolean z2, boolean z3) {
        C2896ak akVar;
        List<CrashDetailBean> list2 = list;
        if (!C2851a.m6470a(this.f5813b).f5663h || (akVar = this.f5814c) == null) {
            return;
        }
        if (z3 || akVar.mo34535b(C2869c.f5821a)) {
            StrategyBean c = this.f5816e.mo34340c();
            if (!c.f5693g) {
                C2903an.m6864d("remote report is disable!", new Object[0]);
                C2903an.m6860b("[crash] server closed bugly in this app. please check your appid if is correct, and re-install it", new Object[0]);
            } else if (list2 != null && list.size() != 0) {
                try {
                    String str = this.f5814c.f5976b ? c.f5705s : c.f5706t;
                    String str2 = this.f5814c.f5976b ? StrategyBean.f5689c : StrategyBean.f5687a;
                    int i = this.f5814c.f5976b ? 830 : 630;
                    C2926bg a = m6630a(this.f5813b, list, C2851a.m6471b());
                    if (a == null) {
                        C2903an.m6864d("create eupPkg fail!", new Object[0]);
                        return;
                    }
                    byte[] a2 = C2893ah.m6794a((C2944m) a);
                    if (a2 == null) {
                        C2903an.m6864d("send encode fail!", new Object[0]);
                        return;
                    }
                    C2927bh a3 = C2893ah.m6789a(this.f5813b, i, a2);
                    if (a3 == null) {
                        C2903an.m6864d("request package is null.", new Object[0]);
                        return;
                    }
                    C28681 r10 = new C2895aj() {
                        /* class com.tencent.bugly.crashreport.crash.C2867b.C28681 */

                        /* renamed from: a */
                        public void mo34184a(int i) {
                        }

                        /* renamed from: a */
                        public void mo34185a(int i, C2928bi biVar, long j, long j2, boolean z, String str) {
                            C2867b.this.mo34390a(z, list);
                        }
                    };
                    if (z) {
                        this.f5814c.mo34527a(f5812a, a3, str, str2, r10, j, z2);
                    } else {
                        this.f5814c.mo34528a(f5812a, a3, str, str2, r10, false);
                    }
                } catch (Throwable th) {
                    C2903an.m6865e("req cr error %s", th.toString());
                    if (!C2903an.m6861b(th)) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }

    /* renamed from: a */
    public void mo34390a(boolean z, List<CrashDetailBean> list) {
        if (list != null && list.size() > 0) {
            C2903an.m6863c("up finish update state %b", Boolean.valueOf(z));
            for (CrashDetailBean crashDetailBean : list) {
                C2903an.m6863c("pre uid:%s uc:%d re:%b me:%b", crashDetailBean.f5754c, Integer.valueOf(crashDetailBean.f5763l), Boolean.valueOf(crashDetailBean.f5755d), Boolean.valueOf(crashDetailBean.f5761j));
                crashDetailBean.f5763l++;
                crashDetailBean.f5755d = z;
                C2903an.m6863c("set uid:%s uc:%d re:%b me:%b", crashDetailBean.f5754c, Integer.valueOf(crashDetailBean.f5763l), Boolean.valueOf(crashDetailBean.f5755d), Boolean.valueOf(crashDetailBean.f5761j));
            }
            for (CrashDetailBean crashDetailBean2 : list) {
                C2869c.m6653a().mo34404a(crashDetailBean2);
            }
            C2903an.m6863c("update state size %d", Integer.valueOf(list.size()));
        }
        if (!z) {
            C2903an.m6860b("[crash] upload fail.", new Object[0]);
        }
    }

    /* renamed from: c */
    public void mo34397c(CrashDetailBean crashDetailBean) {
        int i;
        String str;
        long j;
        String str2;
        Map<String, String> map;
        int i2;
        String str3;
        HashMap hashMap;
        CrashDetailBean crashDetailBean2 = crashDetailBean;
        if (crashDetailBean2 != null) {
            if (this.f5818g != null || this.f5817f != null) {
                try {
                    C2903an.m6857a("[crash callback] start user's callback:onCrashHandleStart()", new Object[0]);
                    switch (crashDetailBean2.f5753b) {
                        case 0:
                            i = 0;
                            break;
                        case 1:
                            i = 2;
                            break;
                        case 2:
                            i = 1;
                            break;
                        case 3:
                            i = 4;
                            break;
                        case 4:
                            i = 3;
                            break;
                        case 5:
                            i = 5;
                            break;
                        case 6:
                            i = 6;
                            break;
                        case 7:
                            i = 7;
                            break;
                        default:
                            return;
                    }
                    boolean z = crashDetailBean2.f5753b == 1;
                    String str4 = crashDetailBean2.f5765n;
                    String str5 = crashDetailBean2.f5767p;
                    String str6 = crashDetailBean2.f5768q;
                    long j2 = crashDetailBean2.f5769r;
                    byte[] bArr = null;
                    if (this.f5817f != null) {
                        C2903an.m6863c("Calling 'onCrashHandleStart' of RQD crash listener.", new Object[0]);
                        this.f5817f.mo34431a(z);
                        C2903an.m6863c("Calling 'getCrashExtraMessage' of RQD crash listener.", new Object[0]);
                        j = j2;
                        str2 = str5;
                        str = str6;
                        String b = this.f5817f.mo34434b(z, str4, str5, str6, -1234567890, j);
                        if (b != null) {
                            hashMap = new HashMap(1);
                            hashMap.put("userData", b);
                        } else {
                            hashMap = null;
                        }
                        map = hashMap;
                    } else {
                        j = j2;
                        str2 = str5;
                        str = str6;
                        if (this.f5818g != null) {
                            C2903an.m6863c("Calling 'onCrashHandleStart' of Bugly crash listener.", new Object[0]);
                            map = this.f5818g.onCrashHandleStart(i, crashDetailBean2.f5765n, crashDetailBean2.f5766o, crashDetailBean2.f5768q);
                        } else {
                            map = null;
                        }
                    }
                    if (map != null && map.size() > 0) {
                        crashDetailBean2.f5744P = new LinkedHashMap(map.size());
                        for (Map.Entry entry : map.entrySet()) {
                            if (!C2908aq.m6915a((String) entry.getKey())) {
                                String str7 = (String) entry.getKey();
                                if (str7.length() > 100) {
                                    str7 = str7.substring(0, 100);
                                    C2903an.m6864d("setted key length is over limit %d substring to %s", 100, str7);
                                }
                                if (C2908aq.m6915a((String) entry.getValue()) || ((String) entry.getValue()).length() <= 30000) {
                                    str3 = "" + ((String) entry.getValue());
                                } else {
                                    str3 = ((String) entry.getValue()).substring(((String) entry.getValue()).length() - 30000);
                                    C2903an.m6864d("setted %s value length is over limit %d substring", str7, 30000);
                                }
                                crashDetailBean2.f5744P.put(str7, str3);
                                C2903an.m6857a("add setted key %s value size:%d", str7, Integer.valueOf(str3.length()));
                            }
                        }
                    }
                    C2903an.m6857a("[crash callback] start user's callback:onCrashHandleStart2GetExtraDatas()", new Object[0]);
                    if (this.f5817f != null) {
                        C2903an.m6863c("Calling 'getCrashExtraData' of RQD crash listener.", new Object[0]);
                        i2 = 30000;
                        bArr = this.f5817f.mo34433a(z, str4, str2, str, -1234567890, j);
                    } else {
                        i2 = 30000;
                        if (this.f5818g != null) {
                            C2903an.m6863c("Calling 'onCrashHandleStart2GetExtraDatas' of Bugly crash listener.", new Object[0]);
                            bArr = this.f5818g.onCrashHandleStart2GetExtraDatas(i, crashDetailBean2.f5765n, crashDetailBean2.f5766o, crashDetailBean2.f5768q);
                        }
                    }
                    byte[] bArr2 = bArr;
                    crashDetailBean2.f5749U = bArr2;
                    if (bArr2 != null) {
                        if (bArr2.length > i2) {
                            C2903an.m6864d("extra bytes size %d is over limit %d will drop over part", Integer.valueOf(bArr2.length), Integer.valueOf(i2));
                            crashDetailBean2.f5749U = Arrays.copyOf(bArr2, i2);
                        }
                        C2903an.m6857a("add extra bytes %d ", Integer.valueOf(bArr2.length));
                    }
                } catch (Throwable th) {
                    C2903an.m6864d("crash handle callback something wrong! %s", th.getClass().getName());
                    if (!C2903an.m6858a(th)) {
                        th.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public ContentValues mo34399d(CrashDetailBean crashDetailBean) {
        if (crashDetailBean == null) {
            return null;
        }
        try {
            ContentValues contentValues = new ContentValues();
            if (crashDetailBean.f5752a > 0) {
                contentValues.put("_id", Long.valueOf(crashDetailBean.f5752a));
            }
            contentValues.put("_tm", Long.valueOf(crashDetailBean.f5769r));
            contentValues.put("_s1", crashDetailBean.f5772u);
            int i = 1;
            contentValues.put("_up", Integer.valueOf(crashDetailBean.f5755d ? 1 : 0));
            if (!crashDetailBean.f5761j) {
                i = 0;
            }
            contentValues.put("_me", Integer.valueOf(i));
            contentValues.put("_uc", Integer.valueOf(crashDetailBean.f5763l));
            contentValues.put("_dt", C2908aq.m6918a(crashDetailBean));
            return contentValues;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public CrashDetailBean mo34384a(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            byte[] blob = cursor.getBlob(cursor.getColumnIndex("_dt"));
            if (blob == null) {
                return null;
            }
            long j = cursor.getLong(cursor.getColumnIndex("_id"));
            CrashDetailBean crashDetailBean = (CrashDetailBean) C2908aq.m6896a(blob, CrashDetailBean.CREATOR);
            if (crashDetailBean != null) {
                crashDetailBean.f5752a = j;
            }
            return crashDetailBean;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad, boolean):long
     arg types: [java.lang.String, android.content.ContentValues, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad, boolean):long */
    /* renamed from: e */
    public void mo34401e(CrashDetailBean crashDetailBean) {
        if (crashDetailBean != null) {
            ContentValues d = mo34399d(crashDetailBean);
            if (d != null) {
                long a = C2889ae.m6757a().mo34489a("t_cr", d, (C2888ad) null, true);
                if (a >= 0) {
                    C2903an.m6863c("insert %s success!", "t_cr");
                    crashDetailBean.f5752a = a;
                }
            }
            if (C2869c.f5829i) {
                m6632f(crashDetailBean);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x0108 A[Catch:{ all -> 0x0111 }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x010d A[DONT_GENERATE] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.bugly.crashreport.crash.CrashDetailBean> mo34395b(java.util.List<com.tencent.bugly.crashreport.crash.C2858a> r17) {
        /*
            r16 = this;
            r1 = 0
            if (r17 == 0) goto L_0x0118
            int r0 = r17.size()
            if (r0 != 0) goto L_0x000b
            goto L_0x0118
        L_0x000b:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "_id"
            r0.append(r2)
            java.lang.String r3 = " in "
            r0.append(r3)
            java.lang.String r4 = "("
            r0.append(r4)
            java.util.Iterator r5 = r17.iterator()
        L_0x0023:
            boolean r6 = r5.hasNext()
            java.lang.String r7 = ","
            if (r6 == 0) goto L_0x003a
            java.lang.Object r6 = r5.next()
            com.tencent.bugly.crashreport.crash.a r6 = (com.tencent.bugly.crashreport.crash.C2858a) r6
            long r8 = r6.f5778a
            r0.append(r8)
            r0.append(r7)
            goto L_0x0023
        L_0x003a:
            java.lang.String r5 = r0.toString()
            boolean r5 = r5.contains(r7)
            r6 = 0
            if (r5 == 0) goto L_0x0053
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            int r8 = r0.lastIndexOf(r7)
            java.lang.String r0 = r0.substring(r6, r8)
            r5.<init>(r0)
            r0 = r5
        L_0x0053:
            java.lang.String r5 = ")"
            r0.append(r5)
            java.lang.String r11 = r0.toString()
            r0.setLength(r6)
            com.tencent.bugly.proguard.ae r8 = com.tencent.bugly.proguard.C2889ae.m6757a()     // Catch:{ all -> 0x00fe }
            java.lang.String r9 = "t_cr"
            r10 = 0
            r12 = 0
            r13 = 0
            r14 = 1
            android.database.Cursor r8 = r8.mo34490a(r9, r10, r11, r12, r13, r14)     // Catch:{ all -> 0x00fe }
            if (r8 != 0) goto L_0x0075
            if (r8 == 0) goto L_0x0074
            r8.close()
        L_0x0074:
            return r1
        L_0x0075:
            java.util.ArrayList r9 = new java.util.ArrayList     // Catch:{ all -> 0x00fa }
            r9.<init>()     // Catch:{ all -> 0x00fa }
            r0.append(r2)     // Catch:{ all -> 0x00fa }
            r0.append(r3)     // Catch:{ all -> 0x00fa }
            r0.append(r4)     // Catch:{ all -> 0x00fa }
            r3 = 0
        L_0x0084:
            boolean r4 = r8.moveToNext()     // Catch:{ all -> 0x00fa }
            if (r4 == 0) goto L_0x00af
            r4 = r16
            com.tencent.bugly.crashreport.crash.CrashDetailBean r10 = r4.mo34384a(r8)     // Catch:{ all -> 0x00f8 }
            if (r10 == 0) goto L_0x0096
            r9.add(r10)     // Catch:{ all -> 0x00f8 }
            goto L_0x0084
        L_0x0096:
            int r10 = r8.getColumnIndex(r2)     // Catch:{ all -> 0x00a7 }
            long r10 = r8.getLong(r10)     // Catch:{ all -> 0x00a7 }
            r0.append(r10)     // Catch:{ all -> 0x00a7 }
            r0.append(r7)     // Catch:{ all -> 0x00a7 }
            int r3 = r3 + 1
            goto L_0x0084
        L_0x00a7:
            java.lang.String r10 = "unknown id!"
            java.lang.Object[] r11 = new java.lang.Object[r6]     // Catch:{ all -> 0x00f8 }
            com.tencent.bugly.proguard.C2903an.m6864d(r10, r11)     // Catch:{ all -> 0x00f8 }
            goto L_0x0084
        L_0x00af:
            r4 = r16
            java.lang.String r2 = r0.toString()     // Catch:{ all -> 0x00f8 }
            boolean r2 = r2.contains(r7)     // Catch:{ all -> 0x00f8 }
            if (r2 == 0) goto L_0x00c9
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f8 }
            int r7 = r0.lastIndexOf(r7)     // Catch:{ all -> 0x00f8 }
            java.lang.String r0 = r0.substring(r6, r7)     // Catch:{ all -> 0x00f8 }
            r2.<init>(r0)     // Catch:{ all -> 0x00f8 }
            r0 = r2
        L_0x00c9:
            r0.append(r5)     // Catch:{ all -> 0x00f8 }
            java.lang.String r12 = r0.toString()     // Catch:{ all -> 0x00f8 }
            if (r3 <= 0) goto L_0x00f2
            com.tencent.bugly.proguard.ae r10 = com.tencent.bugly.proguard.C2889ae.m6757a()     // Catch:{ all -> 0x00f8 }
            java.lang.String r11 = "t_cr"
            r13 = 0
            r14 = 0
            r15 = 1
            int r0 = r10.mo34488a(r11, r12, r13, r14, r15)     // Catch:{ all -> 0x00f8 }
            java.lang.String r2 = "deleted %s illegal data %d"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00f8 }
            java.lang.String r5 = "t_cr"
            r3[r6] = r5     // Catch:{ all -> 0x00f8 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00f8 }
            r5 = 1
            r3[r5] = r0     // Catch:{ all -> 0x00f8 }
            com.tencent.bugly.proguard.C2903an.m6864d(r2, r3)     // Catch:{ all -> 0x00f8 }
        L_0x00f2:
            if (r8 == 0) goto L_0x00f7
            r8.close()
        L_0x00f7:
            return r9
        L_0x00f8:
            r0 = move-exception
            goto L_0x0102
        L_0x00fa:
            r0 = move-exception
            r4 = r16
            goto L_0x0102
        L_0x00fe:
            r0 = move-exception
            r4 = r16
            r8 = r1
        L_0x0102:
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x0111 }
            if (r2 != 0) goto L_0x010b
            r0.printStackTrace()     // Catch:{ all -> 0x0111 }
        L_0x010b:
            if (r8 == 0) goto L_0x0110
            r8.close()
        L_0x0110:
            return r1
        L_0x0111:
            r0 = move-exception
            if (r8 == 0) goto L_0x0117
            r8.close()
        L_0x0117:
            throw r0
        L_0x0118:
            r4 = r16
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C2867b.mo34395b(java.util.List):java.util.List");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public C2858a mo34393b(Cursor cursor) {
        if (cursor == null) {
            return null;
        }
        try {
            C2858a aVar = new C2858a();
            aVar.f5778a = cursor.getLong(cursor.getColumnIndex("_id"));
            aVar.f5779b = cursor.getLong(cursor.getColumnIndex("_tm"));
            aVar.f5780c = cursor.getString(cursor.getColumnIndex("_s1"));
            boolean z = false;
            aVar.f5781d = cursor.getInt(cursor.getColumnIndex("_up")) == 1;
            if (cursor.getInt(cursor.getColumnIndex("_me")) == 1) {
                z = true;
            }
            aVar.f5782e = z;
            aVar.f5783f = cursor.getInt(cursor.getColumnIndex("_uc"));
            return aVar;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00ce A[Catch:{ all -> 0x00d7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d3 A[DONT_GENERATE] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<com.tencent.bugly.crashreport.crash.C2858a> mo34394b() {
        /*
            r16 = this;
            java.lang.String r0 = "_id"
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r2 = 0
            java.lang.String r3 = "_id"
            java.lang.String r4 = "_tm"
            java.lang.String r5 = "_s1"
            java.lang.String r6 = "_up"
            java.lang.String r7 = "_me"
            java.lang.String r8 = "_uc"
            java.lang.String[] r11 = new java.lang.String[]{r3, r4, r5, r6, r7, r8}     // Catch:{ all -> 0x00c4 }
            com.tencent.bugly.proguard.ae r9 = com.tencent.bugly.proguard.C2889ae.m6757a()     // Catch:{ all -> 0x00c4 }
            java.lang.String r10 = "t_cr"
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 1
            android.database.Cursor r3 = r9.mo34490a(r10, r11, r12, r13, r14, r15)     // Catch:{ all -> 0x00c4 }
            if (r3 != 0) goto L_0x002e
            if (r3 == 0) goto L_0x002d
            r3.close()
        L_0x002d:
            return r2
        L_0x002e:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00c0 }
            r2.<init>()     // Catch:{ all -> 0x00c0 }
            r2.append(r0)     // Catch:{ all -> 0x00c0 }
            java.lang.String r4 = " in "
            r2.append(r4)     // Catch:{ all -> 0x00c0 }
            java.lang.String r4 = "("
            r2.append(r4)     // Catch:{ all -> 0x00c0 }
            r4 = 0
            r5 = 0
        L_0x0042:
            boolean r6 = r3.moveToNext()     // Catch:{ all -> 0x00c0 }
            java.lang.String r7 = ","
            if (r6 == 0) goto L_0x006f
            r6 = r16
            com.tencent.bugly.crashreport.crash.a r8 = r6.mo34393b(r3)     // Catch:{ all -> 0x00be }
            if (r8 == 0) goto L_0x0056
            r1.add(r8)     // Catch:{ all -> 0x00be }
            goto L_0x0042
        L_0x0056:
            int r8 = r3.getColumnIndex(r0)     // Catch:{ all -> 0x0067 }
            long r8 = r3.getLong(r8)     // Catch:{ all -> 0x0067 }
            r2.append(r8)     // Catch:{ all -> 0x0067 }
            r2.append(r7)     // Catch:{ all -> 0x0067 }
            int r5 = r5 + 1
            goto L_0x0042
        L_0x0067:
            java.lang.String r7 = "unknown id!"
            java.lang.Object[] r8 = new java.lang.Object[r4]     // Catch:{ all -> 0x00be }
            com.tencent.bugly.proguard.C2903an.m6864d(r7, r8)     // Catch:{ all -> 0x00be }
            goto L_0x0042
        L_0x006f:
            r6 = r16
            java.lang.String r0 = r2.toString()     // Catch:{ all -> 0x00be }
            boolean r0 = r0.contains(r7)     // Catch:{ all -> 0x00be }
            if (r0 == 0) goto L_0x0089
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00be }
            int r7 = r2.lastIndexOf(r7)     // Catch:{ all -> 0x00be }
            java.lang.String r2 = r2.substring(r4, r7)     // Catch:{ all -> 0x00be }
            r0.<init>(r2)     // Catch:{ all -> 0x00be }
            goto L_0x008a
        L_0x0089:
            r0 = r2
        L_0x008a:
            java.lang.String r2 = ")"
            r0.append(r2)     // Catch:{ all -> 0x00be }
            java.lang.String r9 = r0.toString()     // Catch:{ all -> 0x00be }
            r0.setLength(r4)     // Catch:{ all -> 0x00be }
            if (r5 <= 0) goto L_0x00b8
            com.tencent.bugly.proguard.ae r7 = com.tencent.bugly.proguard.C2889ae.m6757a()     // Catch:{ all -> 0x00be }
            java.lang.String r8 = "t_cr"
            r10 = 0
            r11 = 0
            r12 = 1
            int r0 = r7.mo34488a(r8, r9, r10, r11, r12)     // Catch:{ all -> 0x00be }
            java.lang.String r2 = "deleted %s illegal data %d"
            r5 = 2
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x00be }
            java.lang.String r7 = "t_cr"
            r5[r4] = r7     // Catch:{ all -> 0x00be }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00be }
            r4 = 1
            r5[r4] = r0     // Catch:{ all -> 0x00be }
            com.tencent.bugly.proguard.C2903an.m6864d(r2, r5)     // Catch:{ all -> 0x00be }
        L_0x00b8:
            if (r3 == 0) goto L_0x00bd
            r3.close()
        L_0x00bd:
            return r1
        L_0x00be:
            r0 = move-exception
            goto L_0x00c8
        L_0x00c0:
            r0 = move-exception
            r6 = r16
            goto L_0x00c8
        L_0x00c4:
            r0 = move-exception
            r6 = r16
            r3 = r2
        L_0x00c8:
            boolean r2 = com.tencent.bugly.proguard.C2903an.m6858a(r0)     // Catch:{ all -> 0x00d7 }
            if (r2 != 0) goto L_0x00d1
            r0.printStackTrace()     // Catch:{ all -> 0x00d7 }
        L_0x00d1:
            if (r3 == 0) goto L_0x00d6
            r3.close()
        L_0x00d6:
            return r1
        L_0x00d7:
            r0 = move-exception
            if (r3 == 0) goto L_0x00dd
            r3.close()
        L_0x00dd:
            goto L_0x00df
        L_0x00de:
            throw r0
        L_0x00df:
            goto L_0x00de
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C2867b.mo34394b():java.util.List");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int */
    /* renamed from: c */
    public void mo34398c(List<C2858a> list) {
        if (list != null && list.size() != 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("_id");
            sb.append(" in ");
            sb.append("(");
            for (C2858a aVar : list) {
                sb.append(aVar.f5778a);
                sb.append(",");
            }
            StringBuilder sb2 = new StringBuilder(sb.substring(0, sb.lastIndexOf(",")));
            sb2.append(")");
            String sb3 = sb2.toString();
            sb2.setLength(0);
            try {
                C2903an.m6863c("deleted %s data %d", "t_cr", Integer.valueOf(C2889ae.m6757a().mo34488a("t_cr", sb3, (String[]) null, (C2888ad) null, true)));
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
     arg types: [java.lang.String, java.lang.String, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int */
    /* renamed from: d */
    public void mo34400d(List<CrashDetailBean> list) {
        if (list != null) {
            try {
                if (list.size() != 0) {
                    StringBuilder sb = new StringBuilder();
                    for (CrashDetailBean crashDetailBean : list) {
                        sb.append(" or ");
                        sb.append("_id");
                        sb.append(" = ");
                        sb.append(crashDetailBean.f5752a);
                    }
                    String sb2 = sb.toString();
                    if (sb2.length() > 0) {
                        sb2 = sb2.substring(4);
                    }
                    sb.setLength(0);
                    C2903an.m6863c("deleted %s data %d", "t_cr", Integer.valueOf(C2889ae.m6757a().mo34488a("t_cr", sb2, (String[]) null, (C2888ad) null, true)));
                }
            } catch (Throwable th) {
                if (!C2903an.m6858a(th)) {
                    th.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a */
    public static C2925bf m6629a(Context context, CrashDetailBean crashDetailBean, C2851a aVar) {
        C2924be a;
        C2924be a2;
        C2924be beVar;
        boolean z = false;
        if (context == null || crashDetailBean == null || aVar == null) {
            C2903an.m6864d("enExp args == null", new Object[0]);
            return null;
        }
        C2925bf bfVar = new C2925bf();
        switch (crashDetailBean.f5753b) {
            case 0:
                bfVar.f6090a = crashDetailBean.f5761j ? "200" : "100";
                break;
            case 1:
                bfVar.f6090a = crashDetailBean.f5761j ? "201" : "101";
                break;
            case 2:
                bfVar.f6090a = crashDetailBean.f5761j ? "202" : "102";
                break;
            case 3:
                bfVar.f6090a = crashDetailBean.f5761j ? "203" : "103";
                break;
            case 4:
                bfVar.f6090a = crashDetailBean.f5761j ? "204" : "104";
                break;
            case 5:
                bfVar.f6090a = crashDetailBean.f5761j ? "207" : "107";
                break;
            case 6:
                bfVar.f6090a = crashDetailBean.f5761j ? "206" : "106";
                break;
            case 7:
                bfVar.f6090a = crashDetailBean.f5761j ? "208" : "108";
                break;
            default:
                C2903an.m6865e("crash type error! %d", Integer.valueOf(crashDetailBean.f5753b));
                break;
        }
        bfVar.f6091b = crashDetailBean.f5769r;
        bfVar.f6092c = crashDetailBean.f5765n;
        bfVar.f6093d = crashDetailBean.f5766o;
        bfVar.f6094e = crashDetailBean.f5767p;
        bfVar.f6096g = crashDetailBean.f5768q;
        bfVar.f6097h = crashDetailBean.f5777z;
        bfVar.f6098i = crashDetailBean.f5754c;
        bfVar.f6099j = null;
        bfVar.f6101l = crashDetailBean.f5764m;
        bfVar.f6102m = crashDetailBean.f5756e;
        bfVar.f6095f = crashDetailBean.f5730B;
        bfVar.f6109t = C2851a.m6471b().mo34312i();
        bfVar.f6103n = null;
        if (crashDetailBean.f5760i != null && crashDetailBean.f5760i.size() > 0) {
            bfVar.f6104o = new ArrayList<>();
            for (Map.Entry entry : crashDetailBean.f5760i.entrySet()) {
                C2922bc bcVar = new C2922bc();
                bcVar.f6070a = ((PlugInBean) entry.getValue()).f5595a;
                bcVar.f6072c = ((PlugInBean) entry.getValue()).f5597c;
                bcVar.f6074e = ((PlugInBean) entry.getValue()).f5596b;
                bcVar.f6071b = aVar.mo34321r();
                bfVar.f6104o.add(bcVar);
            }
        }
        if (crashDetailBean.f5759h != null && crashDetailBean.f5759h.size() > 0) {
            bfVar.f6105p = new ArrayList<>();
            for (Map.Entry entry2 : crashDetailBean.f5759h.entrySet()) {
                C2922bc bcVar2 = new C2922bc();
                bcVar2.f6070a = ((PlugInBean) entry2.getValue()).f5595a;
                bcVar2.f6072c = ((PlugInBean) entry2.getValue()).f5597c;
                bcVar2.f6074e = ((PlugInBean) entry2.getValue()).f5596b;
                bfVar.f6105p.add(bcVar2);
            }
        }
        if (crashDetailBean.f5761j) {
            bfVar.f6100k = crashDetailBean.f5771t;
            if (crashDetailBean.f5770s != null && crashDetailBean.f5770s.length() > 0) {
                if (bfVar.f6106q == null) {
                    bfVar.f6106q = new ArrayList<>();
                }
                try {
                    bfVar.f6106q.add(new C2924be((byte) 1, "alltimes.txt", crashDetailBean.f5770s.getBytes("utf-8")));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    bfVar.f6106q = null;
                }
            }
            Object[] objArr = new Object[2];
            objArr[0] = Integer.valueOf(bfVar.f6100k);
            objArr[1] = Integer.valueOf(bfVar.f6106q != null ? bfVar.f6106q.size() : 0);
            C2903an.m6863c("crashcount:%d sz:%d", objArr);
        }
        if (crashDetailBean.f5774w != null) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            try {
                bfVar.f6106q.add(new C2924be((byte) 1, "log.txt", crashDetailBean.f5774w.getBytes("utf-8")));
            } catch (UnsupportedEncodingException e2) {
                e2.printStackTrace();
                bfVar.f6106q = null;
            }
        }
        if (crashDetailBean.f5775x != null) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            try {
                bfVar.f6106q.add(new C2924be((byte) 1, "jniLog.txt", crashDetailBean.f5775x.getBytes("utf-8")));
            } catch (UnsupportedEncodingException e3) {
                e3.printStackTrace();
                bfVar.f6106q = null;
            }
        }
        if (!C2908aq.m6915a(crashDetailBean.f5750V)) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            try {
                beVar = new C2924be((byte) 1, "crashInfos.txt", crashDetailBean.f5750V.getBytes("utf-8"));
            } catch (UnsupportedEncodingException e4) {
                e4.printStackTrace();
                beVar = null;
            }
            if (beVar != null) {
                C2903an.m6863c("attach crash infos", new Object[0]);
                bfVar.f6106q.add(beVar);
            }
        }
        if (crashDetailBean.f5751W != null) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            C2924be a3 = m6628a("backupRecord.zip", context, crashDetailBean.f5751W);
            if (a3 != null) {
                C2903an.m6863c("attach backup record", new Object[0]);
                bfVar.f6106q.add(a3);
            }
        }
        if (crashDetailBean.f5776y != null && crashDetailBean.f5776y.length > 0) {
            C2924be beVar2 = new C2924be((byte) 2, "buglylog.zip", crashDetailBean.f5776y);
            C2903an.m6863c("attach user log", new Object[0]);
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            bfVar.f6106q.add(beVar2);
        }
        if (crashDetailBean.f5753b == 3) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            C2903an.m6863c("crashBean.userDatas:%s", crashDetailBean.f5744P);
            if (crashDetailBean.f5744P != null && crashDetailBean.f5744P.containsKey("BUGLY_CR_01")) {
                try {
                    if (!TextUtils.isEmpty(crashDetailBean.f5744P.get("BUGLY_CR_01"))) {
                        bfVar.f6106q.add(new C2924be((byte) 1, "anrMessage.txt", crashDetailBean.f5744P.get("BUGLY_CR_01").getBytes("utf-8")));
                        C2903an.m6863c("attach anr message", new Object[0]);
                    }
                } catch (UnsupportedEncodingException e5) {
                    e5.printStackTrace();
                    bfVar.f6106q = null;
                }
                crashDetailBean.f5744P.remove("BUGLY_CR_01");
            }
            if (!(crashDetailBean.f5773v == null || (a2 = m6628a("trace.zip", context, crashDetailBean.f5773v)) == null)) {
                C2903an.m6863c("attach traces", new Object[0]);
                bfVar.f6106q.add(a2);
            }
        }
        if (crashDetailBean.f5753b == 1) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            if (!(crashDetailBean.f5773v == null || (a = m6628a("tomb.zip", context, crashDetailBean.f5773v)) == null)) {
                C2903an.m6863c("attach tombs", new Object[0]);
                bfVar.f6106q.add(a);
            }
        }
        if (aVar.f5609K != null && !aVar.f5609K.isEmpty()) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            StringBuilder sb = new StringBuilder();
            for (String str : aVar.f5609K) {
                sb.append(str);
            }
            try {
                bfVar.f6106q.add(new C2924be((byte) 1, "martianlog.txt", sb.toString().getBytes("utf-8")));
                C2903an.m6863c("attach pageTracingList", new Object[0]);
            } catch (UnsupportedEncodingException e6) {
                e6.printStackTrace();
            }
        }
        if (crashDetailBean.f5749U != null && crashDetailBean.f5749U.length > 0) {
            if (bfVar.f6106q == null) {
                bfVar.f6106q = new ArrayList<>();
            }
            bfVar.f6106q.add(new C2924be((byte) 1, "userExtraByteData", crashDetailBean.f5749U));
            C2903an.m6863c("attach extraData", new Object[0]);
        }
        bfVar.f6107r = new HashMap();
        bfVar.f6107r.put("A9", "" + crashDetailBean.f5731C);
        bfVar.f6107r.put("A11", "" + crashDetailBean.f5732D);
        bfVar.f6107r.put("A10", "" + crashDetailBean.f5733E);
        bfVar.f6107r.put("A23", "" + crashDetailBean.f5757f);
        bfVar.f6107r.put("A7", "" + aVar.f5667l);
        bfVar.f6107r.put("A6", "" + aVar.mo34322s());
        bfVar.f6107r.put("A5", "" + aVar.mo34321r());
        bfVar.f6107r.put("A22", "" + aVar.mo34311h());
        bfVar.f6107r.put("A2", "" + crashDetailBean.f5735G);
        bfVar.f6107r.put("A1", "" + crashDetailBean.f5734F);
        bfVar.f6107r.put("A24", "" + aVar.f5669n);
        bfVar.f6107r.put("A17", "" + crashDetailBean.f5736H);
        bfVar.f6107r.put("A3", "" + aVar.mo34314k());
        bfVar.f6107r.put("A16", "" + aVar.mo34316m());
        bfVar.f6107r.put("A25", "" + aVar.mo34317n());
        bfVar.f6107r.put("A14", "" + aVar.mo34315l());
        bfVar.f6107r.put("A15", "" + aVar.mo34326w());
        bfVar.f6107r.put("A13", "" + aVar.mo34327x());
        bfVar.f6107r.put("A34", "" + crashDetailBean.f5729A);
        if (aVar.f5602D != null) {
            bfVar.f6107r.put("productIdentify", "" + aVar.f5602D);
        }
        try {
            bfVar.f6107r.put("A26", "" + URLEncoder.encode(crashDetailBean.f5737I, "utf-8"));
        } catch (UnsupportedEncodingException e7) {
            e7.printStackTrace();
        }
        if (crashDetailBean.f5753b == 1) {
            bfVar.f6107r.put("A27", "" + crashDetailBean.f5740L);
            bfVar.f6107r.put("A28", "" + crashDetailBean.f5739K);
            bfVar.f6107r.put("A29", "" + crashDetailBean.f5762k);
        }
        bfVar.f6107r.put("A30", "" + crashDetailBean.f5741M);
        bfVar.f6107r.put("A18", "" + crashDetailBean.f5742N);
        Map<String, String> map = bfVar.f6107r;
        StringBuilder sb2 = new StringBuilder();
        sb2.append("");
        sb2.append(!crashDetailBean.f5743O);
        map.put("A36", sb2.toString());
        bfVar.f6107r.put("F02", "" + aVar.f5678w);
        bfVar.f6107r.put("F03", "" + aVar.f5679x);
        bfVar.f6107r.put("F04", "" + aVar.mo34305e());
        bfVar.f6107r.put("F05", "" + aVar.f5680y);
        bfVar.f6107r.put("F06", "" + aVar.f5677v);
        bfVar.f6107r.put("F08", "" + aVar.f5600B);
        bfVar.f6107r.put("F09", "" + aVar.f5601C);
        bfVar.f6107r.put("F10", "" + aVar.f5681z);
        if (crashDetailBean.f5745Q >= 0) {
            bfVar.f6107r.put("C01", "" + crashDetailBean.f5745Q);
        }
        if (crashDetailBean.f5746R >= 0) {
            bfVar.f6107r.put("C02", "" + crashDetailBean.f5746R);
        }
        if (crashDetailBean.f5747S != null && crashDetailBean.f5747S.size() > 0) {
            for (Map.Entry entry3 : crashDetailBean.f5747S.entrySet()) {
                bfVar.f6107r.put("C03_" + ((String) entry3.getKey()), entry3.getValue());
            }
        }
        if (crashDetailBean.f5748T != null && crashDetailBean.f5748T.size() > 0) {
            for (Map.Entry entry4 : crashDetailBean.f5748T.entrySet()) {
                bfVar.f6107r.put("C04_" + ((String) entry4.getKey()), entry4.getValue());
            }
        }
        bfVar.f6108s = null;
        if (crashDetailBean.f5744P != null && crashDetailBean.f5744P.size() > 0) {
            bfVar.f6108s = crashDetailBean.f5744P;
            C2903an.m6857a("setted message size %d", Integer.valueOf(bfVar.f6108s.size()));
        }
        Object[] objArr2 = new Object[12];
        objArr2[0] = crashDetailBean.f5765n;
        objArr2[1] = crashDetailBean.f5754c;
        objArr2[2] = aVar.mo34305e();
        objArr2[3] = Long.valueOf((crashDetailBean.f5769r - crashDetailBean.f5742N) / 1000);
        objArr2[4] = Boolean.valueOf(crashDetailBean.f5762k);
        objArr2[5] = Boolean.valueOf(crashDetailBean.f5743O);
        objArr2[6] = Boolean.valueOf(crashDetailBean.f5761j);
        if (crashDetailBean.f5753b == 1) {
            z = true;
        }
        objArr2[7] = Boolean.valueOf(z);
        objArr2[8] = Integer.valueOf(crashDetailBean.f5771t);
        objArr2[9] = crashDetailBean.f5770s;
        objArr2[10] = Boolean.valueOf(crashDetailBean.f5755d);
        objArr2[11] = Integer.valueOf(bfVar.f6107r.size());
        C2903an.m6863c("%s rid:%s sess:%s ls:%ds isR:%b isF:%b isM:%b isN:%b mc:%d ,%s ,isUp:%b ,vm:%d", objArr2);
        return bfVar;
    }

    /* renamed from: a */
    public static C2926bg m6630a(Context context, List<CrashDetailBean> list, C2851a aVar) {
        if (context == null || list == null || list.size() == 0 || aVar == null) {
            C2903an.m6864d("enEXPPkg args == null!", new Object[0]);
            return null;
        }
        C2926bg bgVar = new C2926bg();
        bgVar.f6112a = new ArrayList<>();
        for (CrashDetailBean crashDetailBean : list) {
            bgVar.f6112a.add(m6629a(context, crashDetailBean, aVar));
        }
        return bgVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x0090 A[Catch:{ all -> 0x00b2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0095 A[SYNTHETIC, Splitter:B:35:0x0095] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00a9 A[DONT_GENERATE] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.tencent.bugly.proguard.C2924be m6628a(java.lang.String r6, android.content.Context r7, java.lang.String r8) {
        /*
            java.lang.String r0 = "del tmp"
            r1 = 0
            r2 = 0
            if (r8 == 0) goto L_0x00d2
            if (r7 != 0) goto L_0x000a
            goto L_0x00d2
        L_0x000a:
            r3 = 1
            java.lang.Object[] r4 = new java.lang.Object[r3]
            r4[r2] = r8
            java.lang.String r5 = "zip %s"
            com.tencent.bugly.proguard.C2903an.m6863c(r5, r4)
            java.io.File r4 = new java.io.File
            r4.<init>(r8)
            java.io.File r8 = new java.io.File
            java.io.File r7 = r7.getCacheDir()
            r8.<init>(r7, r6)
            r6 = 5000(0x1388, float:7.006E-42)
            boolean r6 = com.tencent.bugly.proguard.C2908aq.m6913a(r4, r8, r6)
            if (r6 != 0) goto L_0x0032
            java.lang.Object[] r6 = new java.lang.Object[r2]
            java.lang.String r7 = "zip fail!"
            com.tencent.bugly.proguard.C2903an.m6864d(r7, r6)
            return r1
        L_0x0032:
            java.io.ByteArrayOutputStream r6 = new java.io.ByteArrayOutputStream
            r6.<init>()
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ all -> 0x0088 }
            r7.<init>(r8)     // Catch:{ all -> 0x0088 }
            r4 = 4096(0x1000, float:5.74E-42)
            byte[] r4 = new byte[r4]     // Catch:{ all -> 0x0086 }
        L_0x0040:
            int r5 = r7.read(r4)     // Catch:{ all -> 0x0086 }
            if (r5 <= 0) goto L_0x004d
            r6.write(r4, r2, r5)     // Catch:{ all -> 0x0086 }
            r6.flush()     // Catch:{ all -> 0x0086 }
            goto L_0x0040
        L_0x004d:
            byte[] r6 = r6.toByteArray()     // Catch:{ all -> 0x0086 }
            java.lang.String r4 = "read bytes :%d"
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x0086 }
            int r5 = r6.length     // Catch:{ all -> 0x0086 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0086 }
            r3[r2] = r5     // Catch:{ all -> 0x0086 }
            com.tencent.bugly.proguard.C2903an.m6863c(r4, r3)     // Catch:{ all -> 0x0086 }
            com.tencent.bugly.proguard.be r3 = new com.tencent.bugly.proguard.be     // Catch:{ all -> 0x0086 }
            r4 = 2
            java.lang.String r5 = r8.getName()     // Catch:{ all -> 0x0086 }
            r3.<init>(r4, r5, r6)     // Catch:{ all -> 0x0086 }
            r7.close()     // Catch:{ IOException -> 0x006d }
            goto L_0x0077
        L_0x006d:
            r6 = move-exception
            boolean r7 = com.tencent.bugly.proguard.C2903an.m6858a(r6)
            if (r7 != 0) goto L_0x0077
            r6.printStackTrace()
        L_0x0077:
            boolean r6 = r8.exists()
            if (r6 == 0) goto L_0x0085
            java.lang.Object[] r6 = new java.lang.Object[r2]
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r6)
            r8.delete()
        L_0x0085:
            return r3
        L_0x0086:
            r6 = move-exception
            goto L_0x008a
        L_0x0088:
            r6 = move-exception
            r7 = r1
        L_0x008a:
            boolean r3 = com.tencent.bugly.proguard.C2903an.m6858a(r6)     // Catch:{ all -> 0x00b2 }
            if (r3 != 0) goto L_0x0093
            r6.printStackTrace()     // Catch:{ all -> 0x00b2 }
        L_0x0093:
            if (r7 == 0) goto L_0x00a3
            r7.close()     // Catch:{ IOException -> 0x0099 }
            goto L_0x00a3
        L_0x0099:
            r6 = move-exception
            boolean r7 = com.tencent.bugly.proguard.C2903an.m6858a(r6)
            if (r7 != 0) goto L_0x00a3
            r6.printStackTrace()
        L_0x00a3:
            boolean r6 = r8.exists()
            if (r6 == 0) goto L_0x00b1
            java.lang.Object[] r6 = new java.lang.Object[r2]
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r6)
            r8.delete()
        L_0x00b1:
            return r1
        L_0x00b2:
            r6 = move-exception
            if (r7 == 0) goto L_0x00c3
            r7.close()     // Catch:{ IOException -> 0x00b9 }
            goto L_0x00c3
        L_0x00b9:
            r7 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r7)
            if (r1 != 0) goto L_0x00c3
            r7.printStackTrace()
        L_0x00c3:
            boolean r7 = r8.exists()
            if (r7 == 0) goto L_0x00d1
            java.lang.Object[] r7 = new java.lang.Object[r2]
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r7)
            r8.delete()
        L_0x00d1:
            throw r6
        L_0x00d2:
            java.lang.Object[] r6 = new java.lang.Object[r2]
            java.lang.String r7 = "rqdp{  createZipAttachment sourcePath == null || context == null ,pls check}"
            com.tencent.bugly.proguard.C2903an.m6864d(r7, r6)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.C2867b.m6628a(java.lang.String, android.content.Context, java.lang.String):com.tencent.bugly.proguard.be");
    }

    /* renamed from: f */
    private boolean m6632f(CrashDetailBean crashDetailBean) {
        try {
            C2903an.m6863c("save eup logs", new Object[0]);
            C2851a b = C2851a.m6471b();
            String f = b.mo34307f();
            String str = b.f5671p;
            String str2 = crashDetailBean.f5729A;
            Locale locale = Locale.US;
            b.getClass();
            String format = String.format(locale, "#--------\npackage:%s\nversion:%s\nsdk:%s\nprocess:%s\ndate:%s\ntype:%s\nmessage:%s\nstack:\n%s\neupID:%s\n", f, str, "2.6.5", str2, C2908aq.m6903a(new Date(crashDetailBean.f5769r)), crashDetailBean.f5765n, crashDetailBean.f5766o, crashDetailBean.f5768q, crashDetailBean.f5754c);
            String str3 = null;
            if (C2869c.f5830j != null) {
                File file = new File(C2869c.f5830j);
                if (file.isFile()) {
                    file = file.getParentFile();
                }
                str3 = file.getAbsolutePath();
            } else if (Environment.getExternalStorageState().equals("mounted")) {
                str3 = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Tencent/" + this.f5813b.getPackageName();
            }
            C2908aq.m6909a(this.f5813b, str3 + "/euplog.txt", format, C2869c.f5831k);
            return true;
        } catch (Throwable th) {
            C2903an.m6864d("rqdp{  save error} %s", th.toString());
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
            return false;
        }
    }

    /* renamed from: a */
    public static void m6631a(String str, String str2, String str3, String str4, String str5, CrashDetailBean crashDetailBean) {
        String str6;
        C2851a b = C2851a.m6471b();
        if (b != null) {
            C2903an.m6865e("#++++++++++Record By Bugly++++++++++#", new Object[0]);
            C2903an.m6865e("# You can use Bugly(http:\\\\bugly.qq.com) to get more Crash Detail!", new Object[0]);
            C2903an.m6865e("# PKG NAME: %s", b.f5659d);
            C2903an.m6865e("# APP VER: %s", b.f5671p);
            C2903an.m6865e("# LAUNCH TIME: %s", C2908aq.m6903a(new Date(C2851a.m6471b().f5625a)));
            C2903an.m6865e("# CRASH TYPE: %s", str);
            C2903an.m6865e("# CRASH TIME: %s", str2);
            C2903an.m6865e("# CRASH PROCESS: %s", str3);
            C2903an.m6865e("# CRASH THREAD: %s", str4);
            if (crashDetailBean != null) {
                C2903an.m6865e("# REPORT ID: %s", crashDetailBean.f5754c);
                Object[] objArr = new Object[2];
                objArr[0] = b.f5668m;
                objArr[1] = b.mo34327x().booleanValue() ? "ROOTED" : "UNROOT";
                C2903an.m6865e("# CRASH DEVICE: %s %s", objArr);
                C2903an.m6865e("# RUNTIME AVAIL RAM:%d ROM:%d SD:%d", Long.valueOf(crashDetailBean.f5731C), Long.valueOf(crashDetailBean.f5732D), Long.valueOf(crashDetailBean.f5733E));
                C2903an.m6865e("# RUNTIME TOTAL RAM:%d ROM:%d SD:%d", Long.valueOf(crashDetailBean.f5734F), Long.valueOf(crashDetailBean.f5735G), Long.valueOf(crashDetailBean.f5736H));
                if (!C2908aq.m6915a(crashDetailBean.f5740L)) {
                    C2903an.m6865e("# EXCEPTION FIRED BY %s %s", crashDetailBean.f5740L, crashDetailBean.f5739K);
                } else if (crashDetailBean.f5753b == 3) {
                    Object[] objArr2 = new Object[1];
                    if (crashDetailBean.f5744P == null) {
                        str6 = "null";
                    } else {
                        str6 = "" + crashDetailBean.f5744P.get("BUGLY_CR_01");
                    }
                    objArr2[0] = str6;
                    C2903an.m6865e("# EXCEPTION ANR MESSAGE:\n %s", objArr2);
                }
            }
            if (!C2908aq.m6915a(str5)) {
                C2903an.m6865e("# CRASH STACK: ", new Object[0]);
                C2903an.m6865e(str5, new Object[0]);
            }
            C2903an.m6865e("#++++++++++++++++++++++++++++++++++++++++++#", new Object[0]);
        }
    }
}
