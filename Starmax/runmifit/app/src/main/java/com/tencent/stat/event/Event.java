package com.tencent.stat.event;

import android.content.Context;
import android.os.Build;
import android.os.SystemClock;
import com.tencent.mid.util.Util;
import com.tencent.stat.C3226e;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatServiceImpl;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.DeviceInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatConstants;
import com.tencent.stat.common.X5Helper;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class Event {

    /* renamed from: a */
    private static final long f7339a = (System.currentTimeMillis() - SystemClock.elapsedRealtime());

    /* renamed from: m */
    protected static String f7340m;

    /* renamed from: p */
    protected static int f7341p = -1;

    /* renamed from: b */
    protected int f7342b = 0;

    /* renamed from: c */
    protected boolean f7343c = false;

    /* renamed from: d */
    protected String f7344d = null;

    /* renamed from: e */
    protected long f7345e;

    /* renamed from: f */
    protected long f7346f;

    /* renamed from: g */
    protected int f7347g;

    /* renamed from: h */
    protected DeviceInfo f7348h = null;

    /* renamed from: i */
    protected int f7349i;

    /* renamed from: j */
    protected String f7350j = null;

    /* renamed from: k */
    protected String f7351k = null;

    /* renamed from: l */
    protected String f7352l = null;

    /* renamed from: n */
    protected boolean f7353n = false;

    /* renamed from: o */
    protected Map<String, Object> f7354o = new HashMap();

    /* renamed from: q */
    protected Context f7355q;

    /* renamed from: r */
    protected StatSpecifyReportedInfo f7356r = null;

    /* renamed from: s */
    private boolean f7357s = false;

    public boolean decode(JSONObject jSONObject) {
        return true;
    }

    public abstract EventType getType();

    public abstract boolean onEncode(JSONObject jSONObject) throws JSONException;

    public void addCommonProperty(String str, Object obj) {
        this.f7354o.put(str, obj);
    }

    public Map<String, Object> getCommonProperty() {
        return this.f7354o;
    }

    public int getFromH5() {
        return this.f7342b;
    }

    public void setFromH5(int i) {
        this.f7342b = i;
    }

    public void encodeCommonProperty(JSONObject jSONObject) {
        Map<String, Object> map = this.f7354o;
        if (map != null && map.size() > 0) {
            for (Map.Entry entry : this.f7354o.entrySet()) {
                try {
                    jSONObject.put((String) entry.getKey(), entry.getValue());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        Map<String, Object> customReportMap = StatConfig.getCustomReportMap();
        if (customReportMap != null && customReportMap.size() > 0) {
            for (Map.Entry entry2 : customReportMap.entrySet()) {
                try {
                    jSONObject.put((String) entry2.getKey(), entry2.getValue());
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
            }
        }
    }

    public void setFilter(boolean z) {
        this.f7357s = z;
    }

    public String getReportedAppkey() {
        return this.f7344d;
    }

    public long getTimestamp() {
        return this.f7345e;
    }

    public StatSpecifyReportedInfo getStatSpecifyReportedInfo() {
        return this.f7356r;
    }

    Event() {
    }

    public Event(Context context, int i, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        if (context != null) {
            init(context, i, statSpecifyReportedInfo);
        }
    }

    public void init(Context context, int i, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        this.f7355q = context.getApplicationContext();
        this.f7346f = System.currentTimeMillis();
        this.f7345e = this.f7346f / 1000;
        this.f7347g = i;
        this.f7352l = StatCommonHelper.getCurAppVersion(context);
        if (statSpecifyReportedInfo != null) {
            this.f7356r = statSpecifyReportedInfo;
            if (StatCommonHelper.isStringValid(statSpecifyReportedInfo.getAppKey())) {
                this.f7344d = statSpecifyReportedInfo.getAppKey();
            }
            if (StatCommonHelper.isStringValid(statSpecifyReportedInfo.getInstallChannel())) {
                this.f7351k = statSpecifyReportedInfo.getInstallChannel();
            }
            if (StatCommonHelper.isStringValid(statSpecifyReportedInfo.getVersion())) {
                this.f7352l = statSpecifyReportedInfo.getVersion();
            }
            this.f7353n = statSpecifyReportedInfo.isImportant();
        } else {
            this.f7344d = StatConfig.getAppKey(context);
            this.f7351k = StatConfig.getInstallChannel(context);
        }
        this.f7350j = StatConfig.getCustomUserId(context);
        this.f7348h = C3226e.m8037a(context).mo35429b(context);
        if (getType() != EventType.NETWORK_DETECTOR) {
            this.f7349i = StatCommonHelper.getNextEventIndexNo(context).intValue();
        } else {
            this.f7349i = -EventType.NETWORK_DETECTOR.mo35459a();
        }
        if (!Util.isMidValid(f7340m)) {
            f7340m = StatConfig.getLocalMidOnly(context);
            if (!StatCommonHelper.isStringValid(f7340m)) {
                f7340m = "0";
            }
        }
        if (f7341p == -1) {
            f7341p = StatCommonHelper.hasRootAccess(context);
        }
        if (statSpecifyReportedInfo != null) {
            this.f7342b = statSpecifyReportedInfo.getFromH5();
        }
    }

    public Context getContext() {
        return this.f7355q;
    }

    public boolean isImportant() {
        return this.f7353n;
    }

    public boolean encode(JSONObject jSONObject) {
        try {
            com.tencent.stat.common.Util.jsonPut(jSONObject, "ky", this.f7344d);
            jSONObject.put("et", getType().mo35459a());
            int i = 1;
            if (this.f7348h != null) {
                jSONObject.put(DeviceInfo.TAG_IMEI, this.f7348h.getImei());
                com.tencent.stat.common.Util.jsonPut(jSONObject, "mc", this.f7348h.getMac());
                int userType = this.f7348h.getUserType();
                jSONObject.put("ut", userType);
                if (userType == 0 && StatCommonHelper.isTheFirstTimeActivate(this.f7355q) == 1) {
                    jSONObject.put("ia", 1);
                }
            }
            com.tencent.stat.common.Util.jsonPut(jSONObject, "cui", this.f7350j);
            String appVersion = StatConfig.getAppVersion();
            if (!StatCommonHelper.isStringValid(appVersion)) {
                com.tencent.stat.common.Util.jsonPut(jSONObject, "av", this.f7352l);
            } else {
                com.tencent.stat.common.Util.jsonPut(jSONObject, "av", appVersion);
                com.tencent.stat.common.Util.jsonPut(jSONObject, "appv", this.f7352l);
            }
            com.tencent.stat.common.Util.jsonPut(jSONObject, "osn", Build.VERSION.RELEASE);
            jSONObject.put("midver", String.valueOf(3.73f));
            com.tencent.stat.common.Util.jsonPut(jSONObject, "ch", this.f7351k);
            if (this.f7353n) {
                jSONObject.put("impt", 1);
            }
            if (this.f7357s) {
                jSONObject.put("ft", 1);
            }
            com.tencent.stat.common.Util.jsonPut(jSONObject, "cch", "");
            com.tencent.stat.common.Util.jsonPut(jSONObject, "mid", f7340m);
            jSONObject.put("idx", this.f7349i);
            jSONObject.put("si", this.f7347g);
            jSONObject.put("ts", this.f7345e);
            jSONObject.put("lts", this.f7346f);
            jSONObject.put("dts", StatCommonHelper.getDiffTime(this.f7355q, false));
            jSONObject.put("os", 1);
            jSONObject.put("osst", f7339a);
            jSONObject.put("sut", f7339a);
            com.tencent.stat.common.Util.jsonPut(jSONObject, "pcn", StatCommonHelper.getCurProcessName(this.f7355q));
            com.tencent.stat.common.Util.jsonPut(jSONObject, "new_mid", StatCommonHelper.getNewMid(this.f7355q));
            com.tencent.stat.common.Util.jsonPut(jSONObject, "ov", Integer.toString(Build.VERSION.SDK_INT));
            com.tencent.stat.common.Util.jsonPut(jSONObject, "md", Build.MODEL);
            jSONObject.put("jb", f7341p);
            com.tencent.stat.common.Util.jsonPut(jSONObject, "mf", Build.MANUFACTURER);
            JSONObject customGlobalReportContent = StatConfig.getCustomGlobalReportContent();
            if (customGlobalReportContent != null && customGlobalReportContent.length() > 0) {
                jSONObject.put("cc", customGlobalReportContent.toString());
            }
            if (StatServiceImpl.isEnableAutoMonitorActivityCycle()) {
                if (!StatServiceImpl.isForeground()) {
                    i = 0;
                }
                jSONObject.put("ifg", i);
            }
            com.tencent.stat.common.Util.jsonPut(jSONObject, "sv", StatConstants.VERSION);
            jSONObject.put("ot", StatCommonHelper.getRunningCounter(getContext()));
            encodeCommonProperty(jSONObject);
            jSONObject.put("h5", this.f7342b);
            m8084b(jSONObject);
            mo35467a(jSONObject);
            return onEncode(jSONObject);
        } catch (Throwable unused) {
            return false;
        }
    }

    /* renamed from: a */
    private void mo35467a(JSONObject jSONObject) {
        Map<String, Object> customReportMap = StatConfig.getCustomReportMap();
        if (customReportMap != null && customReportMap.size() > 0) {
            for (Map.Entry entry : customReportMap.entrySet()) {
                try {
                    jSONObject.put((String) entry.getKey(), entry.getValue());
                } catch (Exception unused) {
                }
            }
        }
    }

    /* renamed from: b */
    private void m8084b(JSONObject jSONObject) {
        if (this.f7343c) {
            com.tencent.stat.common.Util.jsonPut(jSONObject, "ua", StatCommonHelper.getUserAgent(this.f7355q));
            X5Helper.encodeX5(getContext(), jSONObject);
        }
    }

    public String toJsonString() {
        try {
            JSONObject jSONObject = new JSONObject();
            encode(jSONObject);
            return jSONObject.toString();
        } catch (Throwable unused) {
            return "";
        }
    }
}
