package com.tencent.bugly.proguard;

import com.tencent.p055mm.opensdk.constants.ConstantsAPI;

/* renamed from: com.tencent.bugly.proguard.v */
/* compiled from: BUGLY */
public final class C2956v extends C2944m implements Cloneable {

    /* renamed from: l */
    static final /* synthetic */ boolean f6251l = (!C2956v.class.desiredAssertionStatus());

    /* renamed from: a */
    public String f6252a = "";

    /* renamed from: b */
    public byte f6253b = 0;

    /* renamed from: c */
    public int f6254c = 0;

    /* renamed from: d */
    public String f6255d = "";

    /* renamed from: e */
    public int f6256e = 0;

    /* renamed from: f */
    public String f6257f = "";

    /* renamed from: g */
    public long f6258g = 0;

    /* renamed from: h */
    public String f6259h = "";

    /* renamed from: i */
    public String f6260i = "";

    /* renamed from: j */
    public String f6261j = "";

    /* renamed from: k */
    public String f6262k = "";

    public C2956v() {
    }

    public C2956v(String str, byte b, int i, String str2, int i2, String str3, long j, String str4, String str5, String str6, String str7) {
        this.f6252a = str;
        this.f6253b = b;
        this.f6254c = i;
        this.f6255d = str2;
        this.f6256e = i2;
        this.f6257f = str3;
        this.f6258g = j;
        this.f6259h = str4;
        this.f6260i = str5;
        this.f6261j = str6;
        this.f6262k = str7;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C2956v vVar = (C2956v) obj;
        if (!C2945n.m7123a(this.f6252a, vVar.f6252a) || !C2945n.m7120a(this.f6253b, vVar.f6253b) || !C2945n.m7121a(this.f6254c, vVar.f6254c) || !C2945n.m7123a(this.f6255d, vVar.f6255d) || !C2945n.m7121a(this.f6256e, vVar.f6256e) || !C2945n.m7123a(this.f6257f, vVar.f6257f) || !C2945n.m7122a(this.f6258g, vVar.f6258g) || !C2945n.m7123a(this.f6259h, vVar.f6259h) || !C2945n.m7123a(this.f6260i, vVar.f6260i) || !C2945n.m7123a(this.f6261j, vVar.f6261j) || !C2945n.m7123a(this.f6262k, vVar.f6262k)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f6251l) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34648a(this.f6252a, 0);
        lVar.mo34660b(this.f6253b, 1);
        lVar.mo34644a(this.f6254c, 2);
        String str = this.f6255d;
        if (str != null) {
            lVar.mo34648a(str, 3);
        }
        lVar.mo34644a(this.f6256e, 4);
        String str2 = this.f6257f;
        if (str2 != null) {
            lVar.mo34648a(str2, 5);
        }
        lVar.mo34645a(this.f6258g, 6);
        String str3 = this.f6259h;
        if (str3 != null) {
            lVar.mo34648a(str3, 7);
        }
        String str4 = this.f6260i;
        if (str4 != null) {
            lVar.mo34648a(str4, 8);
        }
        String str5 = this.f6261j;
        if (str5 != null) {
            lVar.mo34648a(str5, 9);
        }
        String str6 = this.f6262k;
        if (str6 != null) {
            lVar.mo34648a(str6, 10);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6252a = kVar.mo34621a(0, true);
        this.f6253b = kVar.mo34613a(this.f6253b, 1, true);
        this.f6254c = kVar.mo34616a(this.f6254c, 2, true);
        this.f6255d = kVar.mo34621a(3, false);
        this.f6256e = kVar.mo34616a(this.f6256e, 4, false);
        this.f6257f = kVar.mo34621a(5, false);
        this.f6258g = kVar.mo34618a(this.f6258g, 6, false);
        this.f6259h = kVar.mo34621a(7, false);
        this.f6260i = kVar.mo34621a(8, false);
        this.f6261j = kVar.mo34621a(9, false);
        this.f6262k = kVar.mo34621a(10, false);
    }

    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34601a(this.f6252a, "appId");
        iVar.mo34593a(this.f6253b, ConstantsAPI.Token.WX_TOKEN_PLATFORMID_KEY);
        iVar.mo34597a(this.f6254c, "versionCode");
        iVar.mo34601a(this.f6255d, "versionName");
        iVar.mo34597a(this.f6256e, "buildNo");
        iVar.mo34601a(this.f6257f, "iconUrl");
        iVar.mo34598a(this.f6258g, "apkId");
        iVar.mo34601a(this.f6259h, "channelId");
        iVar.mo34601a(this.f6260i, "md5");
        iVar.mo34601a(this.f6261j, "sdkVer");
        iVar.mo34601a(this.f6262k, "bundleId");
    }
}
