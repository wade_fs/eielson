package com.tencent.connect.auth;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.NinePatch;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.NinePatchDrawable;
import android.graphics.drawable.PaintDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.WindowManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.tencent.bugly.beta.tinker.TinkerReport;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.connect.common.UIListenerManager;
import com.tencent.connect.p052a.ProGuard;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3122f;
import com.tencent.open.utils.C3124g;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3126i;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.URLDecoder;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class AuthAgent extends BaseApi {
    public static final String KEY_FORCE_QR_LOGIN = "KEY_FORCE_QR_LOGIN";
    public static final String SECURE_LIB_ARM64_FILE_NAME = "libwbsafeedit_64";
    public static final String SECURE_LIB_ARM_FILE_NAME = "libwbsafeedit";
    public static String SECURE_LIB_FILE_NAME = "libwbsafeedit";
    public static String SECURE_LIB_NAME = null;
    public static final String SECURE_LIB_X86_64_FILE_NAME = "libwbsafeedit_x86_64";
    public static final String SECURE_LIB_X86_FILE_NAME = "libwbsafeedit_x86";

    /* renamed from: a */
    private IUiListener f6311a;

    /* renamed from: d */
    private String f6312d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public WeakReference<Activity> f6313e;

    static {
        SECURE_LIB_NAME = SECURE_LIB_FILE_NAME + ".so";
        String str = Build.CPU_ABI;
        if (str == null || str.equals("")) {
            SECURE_LIB_FILE_NAME = SECURE_LIB_ARM_FILE_NAME;
            SECURE_LIB_NAME = SECURE_LIB_FILE_NAME + ".so";
            C3082f.m7634c("openSDK_LOG.AuthAgent", "is arm(default) architecture");
        } else if (str.equalsIgnoreCase("arm64-v8a")) {
            SECURE_LIB_FILE_NAME = SECURE_LIB_ARM64_FILE_NAME;
            SECURE_LIB_NAME = SECURE_LIB_FILE_NAME + ".so";
            C3082f.m7634c("openSDK_LOG.AuthAgent", "is arm64-v8a architecture");
        } else if (str.equalsIgnoreCase("x86")) {
            SECURE_LIB_FILE_NAME = SECURE_LIB_X86_FILE_NAME;
            SECURE_LIB_NAME = SECURE_LIB_FILE_NAME + ".so";
            C3082f.m7634c("openSDK_LOG.AuthAgent", "is x86 architecture");
        } else if (str.equalsIgnoreCase("x86_64")) {
            SECURE_LIB_FILE_NAME = SECURE_LIB_X86_64_FILE_NAME;
            SECURE_LIB_NAME = SECURE_LIB_FILE_NAME + ".so";
            C3082f.m7634c("openSDK_LOG.AuthAgent", "is x86_64 architecture");
        } else {
            SECURE_LIB_FILE_NAME = SECURE_LIB_ARM_FILE_NAME;
            SECURE_LIB_NAME = SECURE_LIB_FILE_NAME + ".so";
            C3082f.m7634c("openSDK_LOG.AuthAgent", "is arm(default) architecture");
        }
    }

    public AuthAgent(QQToken qQToken) {
        super(qQToken);
    }

    /* renamed from: com.tencent.connect.auth.AuthAgent$c */
    /* compiled from: ProGuard */
    private class C2969c implements IUiListener {

        /* renamed from: b */
        private final IUiListener f6338b;

        /* renamed from: c */
        private final boolean f6339c;

        /* renamed from: d */
        private final Context f6340d;

        public C2969c(Context context, IUiListener iUiListener, boolean z, boolean z2) {
            this.f6340d = context;
            this.f6338b = iUiListener;
            this.f6339c = z;
            C3082f.m7631b("openSDK_LOG.AuthAgent", "OpenUi, TokenListener()");
        }

        public void onComplete(Object obj) {
            C3082f.m7631b("openSDK_LOG.AuthAgent", "OpenUi, TokenListener() onComplete");
            JSONObject jSONObject = (JSONObject) obj;
            try {
                String string = jSONObject.getString(Constants.PARAM_ACCESS_TOKEN);
                String string2 = jSONObject.getString(Constants.PARAM_EXPIRES_IN);
                String string3 = jSONObject.getString("openid");
                if (!(string == null || AuthAgent.this.f6457c == null || string3 == null)) {
                    AuthAgent.this.f6457c.setAccessToken(string, string2);
                    AuthAgent.this.f6457c.setOpenId(string3);
                    ProGuard.m7171d(this.f6340d, AuthAgent.this.f6457c);
                }
                String string4 = jSONObject.getString(Constants.PARAM_PLATFORM_ID);
                if (string4 != null) {
                    try {
                        this.f6340d.getSharedPreferences(Constants.PREFERENCE_PF, 0).edit().putString(Constants.PARAM_PLATFORM_ID, string4).commit();
                    } catch (Exception e) {
                        e.printStackTrace();
                        C3082f.m7632b("openSDK_LOG.AuthAgent", "OpenUi, TokenListener() onComplete error", e);
                    }
                }
                if (this.f6339c) {
                    CookieSyncManager.getInstance().sync();
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
                C3082f.m7632b("openSDK_LOG.AuthAgent", "OpenUi, TokenListener() onComplete error", e2);
            }
            this.f6338b.onComplete(jSONObject);
            AuthAgent.this.releaseResource();
            C3082f.m7630b();
        }

        public void onError(UiError uiError) {
            C3082f.m7631b("openSDK_LOG.AuthAgent", "OpenUi, TokenListener() onError");
            this.f6338b.onError(uiError);
            C3082f.m7630b();
        }

        public void onCancel() {
            C3082f.m7631b("openSDK_LOG.AuthAgent", "OpenUi, TokenListener() onCancel");
            this.f6338b.onCancel();
            C3082f.m7630b();
        }
    }

    public int doLogin(Activity activity, String str, IUiListener iUiListener) {
        return doLogin(activity, str, iUiListener, false, null);
    }

    public int doLogin(Activity activity, String str, IUiListener iUiListener, boolean z, Fragment fragment) {
        return doLogin(activity, str, iUiListener, z, fragment, false);
    }

    public int doLogin(Activity activity, String str, IUiListener iUiListener, boolean z, Fragment fragment, boolean z2) {
        this.f6312d = str;
        this.f6313e = new WeakReference<>(activity);
        this.f6311a = iUiListener;
        if (activity.getIntent().getBooleanExtra(KEY_FORCE_QR_LOGIN, false) || C3122f.m7733a(activity, this.f6457c.getAppId()).mo35170b("C_LoginWeb") || !m7175a(activity, fragment, z)) {
            C3091d.m7665a().mo35123a(this.f6457c.getOpenId(), this.f6457c.getAppId(), "2", "1", "5", "1", "0", "0");
            C3082f.m7635d("openSDK_LOG.AuthAgent", "doLogin startActivity fail show dialog.");
            this.f6311a = new C2964b(this.f6311a);
            return m7172a(z, this.f6311a, z2);
        }
        C3082f.m7634c("openSDK_LOG.AuthAgent", "OpenUi, showUi, return Constants.UI_ACTIVITY");
        C3091d.m7665a().mo35123a(this.f6457c.getOpenId(), this.f6457c.getAppId(), "2", "1", "5", "0", "0", "0");
        return 1;
    }

    public void releaseResource() {
        this.f6311a = null;
    }

    /* renamed from: a */
    private int m7172a(boolean z, IUiListener iUiListener, boolean z2) {
        C3082f.m7634c("openSDK_LOG.AuthAgent", "OpenUi, showDialog -- start");
        CookieSyncManager.createInstance(C3121e.m7727a());
        Bundle a = mo34798a();
        if (z) {
            a.putString("isadd", "1");
        }
        a.putString(Constants.PARAM_SCOPE, this.f6312d);
        a.putString(Constants.PARAM_CLIENT_ID, this.f6457c.getAppId());
        if (isOEM) {
            a.putString(Constants.PARAM_PLATFORM_ID, "desktop_m_qq-" + installChannel + "-" + "android" + "-" + registerChannel + "-" + businessId);
        } else {
            a.putString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF);
        }
        String str = (System.currentTimeMillis() / 1000) + "";
        a.putString("sign", C3125h.m7755b(C3121e.m7727a(), str));
        a.putString("time", str);
        a.putString("display", "mobile");
        a.putString("response_type", "token");
        a.putString("redirect_uri", "auth://tauth.qq.com/");
        a.putString("cancel_display", "1");
        a.putString("switch", "1");
        a.putString("status_userip", C3131k.m7769a());
        if (z2) {
            a.putString("style", "qr");
        }
        final String str2 = C3124g.m7744a().mo35172a(C3121e.m7727a(), "https://openmobile.qq.com/oauth2.0/m_authorize?") + HttpUtils.encodeUrl(a);
        final C2969c cVar = new C2969c(C3121e.m7727a(), iUiListener, true, false);
        C3082f.m7631b("openSDK_LOG.AuthAgent", "OpenUi, showDialog TDialog");
        C3126i.m7760a(new Runnable() {
            /* class com.tencent.connect.auth.AuthAgent.C29611 */

            /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
                r0 = (android.app.Activity) com.tencent.connect.auth.AuthAgent.m7179e(r3.f6316c).get();
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r3 = this;
                    java.lang.String r0 = com.tencent.connect.auth.AuthAgent.SECURE_LIB_FILE_NAME
                    java.lang.String r1 = com.tencent.connect.auth.AuthAgent.SECURE_LIB_NAME
                    r2 = 3
                    com.tencent.open.utils.C3125h.m7754a(r0, r1, r2)
                    com.tencent.open.web.security.JniInterface.loadSo()
                    com.tencent.connect.auth.AuthAgent r0 = com.tencent.connect.auth.AuthAgent.this
                    java.lang.ref.WeakReference r0 = r0.f6313e
                    if (r0 != 0) goto L_0x0014
                    return
                L_0x0014:
                    com.tencent.connect.auth.AuthAgent r0 = com.tencent.connect.auth.AuthAgent.this
                    java.lang.ref.WeakReference r0 = r0.f6313e
                    java.lang.Object r0 = r0.get()
                    android.app.Activity r0 = (android.app.Activity) r0
                    if (r0 == 0) goto L_0x002a
                    com.tencent.connect.auth.AuthAgent$1$1 r1 = new com.tencent.connect.auth.AuthAgent$1$1
                    r1.<init>(r0)
                    r0.runOnUiThread(r1)
                L_0x002a:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.tencent.connect.auth.AuthAgent.C29611.run():void");
            }
        });
        C3082f.m7634c("openSDK_LOG.AuthAgent", "OpenUi, showDialog -- end");
        return 2;
    }

    /* renamed from: a */
    private boolean m7175a(Activity activity, Fragment fragment, boolean z) {
        C3082f.m7634c("openSDK_LOG.AuthAgent", "startActionActivity() -- start");
        Intent b = mo34805b("com.tencent.open.agent.AgentActivity");
        if (b != null) {
            Bundle a = mo34798a();
            if (z) {
                a.putString("isadd", "1");
            }
            a.putString(Constants.PARAM_SCOPE, this.f6312d);
            a.putString(Constants.PARAM_CLIENT_ID, this.f6457c.getAppId());
            if (isOEM) {
                a.putString(Constants.PARAM_PLATFORM_ID, "desktop_m_qq-" + installChannel + "-" + "android" + "-" + registerChannel + "-" + businessId);
            } else {
                a.putString(Constants.PARAM_PLATFORM_ID, Constants.DEFAULT_PF);
            }
            a.putString("need_pay", "1");
            a.putString(Constants.KEY_APP_NAME, C3125h.m7750a(C3121e.m7727a()));
            b.putExtra(Constants.KEY_ACTION, "action_login");
            b.putExtra(Constants.KEY_PARAMS, a);
            b.putExtra("appid", this.f6457c.getAppId());
            if (mo34804a(b)) {
                this.f6311a = new C2964b(this.f6311a);
                UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_LOGIN, this.f6311a);
                if (fragment != null) {
                    C3082f.m7631b("openSDK_LOG.AuthAgent", "startAssitActivity fragment");
                    mo34803a(fragment, b, (int) Constants.REQUEST_LOGIN);
                } else {
                    C3082f.m7631b("openSDK_LOG.AuthAgent", "startAssitActivity activity");
                    mo34801a(activity, b, (int) Constants.REQUEST_LOGIN);
                }
                C3082f.m7634c("openSDK_LOG.AuthAgent", "startActionActivity() -- end, found activity for loginIntent");
                C3091d.m7665a().mo35121a(0, "LOGIN_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), "", Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "");
                return true;
            }
        }
        C3091d.m7665a().mo35121a(1, "LOGIN_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), "", Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "startActionActivity fail");
        C3082f.m7634c("openSDK_LOG.AuthAgent", "startActionActivity() -- end, no target activity for loginIntent");
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34706a(IUiListener iUiListener) {
        String str;
        C3082f.m7634c("openSDK_LOG.AuthAgent", "reportDAU() -- start");
        String accessToken = this.f6457c.getAccessToken();
        String openId = this.f6457c.getOpenId();
        String appId = this.f6457c.getAppId();
        if (TextUtils.isEmpty(accessToken) || TextUtils.isEmpty(openId) || TextUtils.isEmpty(appId)) {
            str = "";
        } else {
            str = C3131k.m7793f("tencent&sdk&qazxc***14969%%" + accessToken + appId + openId + "qzone3.4");
        }
        if (TextUtils.isEmpty(str)) {
            C3082f.m7636e("openSDK_LOG.AuthAgent", "reportDAU -- encrytoken is null");
            return;
        }
        Bundle a = mo34798a();
        a.putString("encrytoken", str);
        HttpUtils.requestAsync(this.f6457c, C3121e.m7727a(), "https://openmobile.qq.com/user/user_login_statis", a, "POST", null);
        C3082f.m7634c("openSDK_LOG.AuthAgent", "reportDAU() -- end");
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo34707b(IUiListener iUiListener) {
        Bundle a = mo34798a();
        a.putString("reqType", "checkLogin");
        HttpUtils.requestAsync(this.f6457c, C3121e.m7727a(), "https://openmobile.qq.com/v3/user/get_info", a, "GET", new BaseApi.TempRequestListener(new C2963a(iUiListener)));
    }

    /* renamed from: com.tencent.connect.auth.AuthAgent$a */
    /* compiled from: ProGuard */
    private class C2963a implements IUiListener {

        /* renamed from: a */
        IUiListener f6319a;

        public C2963a(IUiListener iUiListener) {
            this.f6319a = iUiListener;
        }

        public void onComplete(Object obj) {
            String str;
            if (obj == null) {
                C3082f.m7636e("openSDK_LOG.AuthAgent", "CheckLoginListener response data is null");
                return;
            }
            JSONObject jSONObject = (JSONObject) obj;
            try {
                int i = jSONObject.getInt("ret");
                if (i == 0) {
                    str = "success";
                } else {
                    str = jSONObject.getString("msg");
                }
                if (this.f6319a != null) {
                    this.f6319a.onComplete(new JSONObject().put("ret", i).put("msg", str));
                }
            } catch (JSONException e) {
                e.printStackTrace();
                C3082f.m7636e("openSDK_LOG.AuthAgent", "CheckLoginListener response data format error");
            }
        }

        public void onError(UiError uiError) {
            IUiListener iUiListener = this.f6319a;
            if (iUiListener != null) {
                iUiListener.onError(uiError);
            }
        }

        public void onCancel() {
            IUiListener iUiListener = this.f6319a;
            if (iUiListener != null) {
                iUiListener.onCancel();
            }
        }
    }

    /* renamed from: com.tencent.connect.auth.AuthAgent$b */
    /* compiled from: ProGuard */
    private class C2964b implements IUiListener {

        /* renamed from: a */
        WeakReference<IUiListener> f6321a;

        /* renamed from: c */
        private final String f6323c = "sendinstall";

        /* renamed from: d */
        private final String f6324d = "installwording";

        /* renamed from: e */
        private final String f6325e = "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi";

        public C2964b(IUiListener iUiListener) {
            this.f6321a = new WeakReference<>(iUiListener);
        }

        public void onComplete(Object obj) {
            JSONObject jSONObject;
            String str;
            if (obj != null && (jSONObject = (JSONObject) obj) != null) {
                boolean z = false;
                try {
                    if (jSONObject.getInt("sendinstall") == 1) {
                        z = true;
                    }
                    str = jSONObject.getString("installwording");
                } catch (JSONException unused) {
                    C3082f.m7635d("openSDK_LOG.AuthAgent", "FeedConfirmListener onComplete There is no value for sendinstall.");
                    str = "";
                }
                String decode = URLDecoder.decode(str);
                C3082f.m7628a("openSDK_LOG.AuthAgent", " WORDING = " + decode + "xx");
                if (z && !TextUtils.isEmpty(decode)) {
                    m7190a(decode, this.f6321a.get(), obj);
                } else if (this.f6321a.get() != null) {
                    if (AuthAgent.this.f6457c != null) {
                        AuthAgent.this.f6457c.saveSession(jSONObject);
                    }
                    this.f6321a.get().onComplete(obj);
                }
            }
        }

        /* renamed from: com.tencent.connect.auth.AuthAgent$b$a */
        /* compiled from: ProGuard */
        private abstract class C2968a implements View.OnClickListener {

            /* renamed from: d */
            Dialog f6335d;

            C2968a(Dialog dialog) {
                this.f6335d = dialog;
            }
        }

        /* renamed from: a */
        private void m7190a(String str, final IUiListener iUiListener, final Object obj) {
            Activity activity;
            PackageInfo packageInfo;
            if (AuthAgent.this.f6313e != null && (activity = (Activity) AuthAgent.this.f6313e.get()) != null) {
                Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(1);
                PackageManager packageManager = activity.getPackageManager();
                Drawable drawable = null;
                try {
                    packageInfo = packageManager.getPackageInfo(activity.getPackageName(), 0);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                    packageInfo = null;
                }
                if (packageInfo != null) {
                    drawable = packageInfo.applicationInfo.loadIcon(packageManager);
                }
                C29651 r5 = new C2968a(dialog) {
                    /* class com.tencent.connect.auth.AuthAgent.C2964b.C29651 */

                    public void onClick(View view) {
                        C2964b.this.mo34714a();
                        if (this.f6335d != null && this.f6335d.isShowing()) {
                            this.f6335d.dismiss();
                        }
                        IUiListener iUiListener = iUiListener;
                        if (iUiListener != null) {
                            iUiListener.onComplete(obj);
                        }
                    }
                };
                C29662 r6 = new C2968a(dialog) {
                    /* class com.tencent.connect.auth.AuthAgent.C2964b.C29662 */

                    public void onClick(View view) {
                        if (this.f6335d != null && this.f6335d.isShowing()) {
                            this.f6335d.dismiss();
                        }
                        IUiListener iUiListener = iUiListener;
                        if (iUiListener != null) {
                            iUiListener.onComplete(obj);
                        }
                    }
                };
                ColorDrawable colorDrawable = new ColorDrawable();
                colorDrawable.setAlpha(0);
                dialog.getWindow().setBackgroundDrawable(colorDrawable);
                dialog.setContentView(m7189a(activity, drawable, str, r5, r6));
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    /* class com.tencent.connect.auth.AuthAgent.C2964b.C29673 */

                    public void onCancel(DialogInterface dialogInterface) {
                        IUiListener iUiListener = iUiListener;
                        if (iUiListener != null) {
                            iUiListener.onComplete(obj);
                        }
                    }
                });
                if (activity != null && !activity.isFinishing()) {
                    dialog.show();
                }
            }
        }

        /* renamed from: a */
        private Drawable m7188a(String str, Context context) {
            Bitmap bitmap;
            try {
                InputStream open = context.getApplicationContext().getAssets().open(str);
                if (open == null) {
                    return null;
                }
                if (str.endsWith(".9.png")) {
                    try {
                        bitmap = BitmapFactory.decodeStream(open);
                    } catch (OutOfMemoryError e) {
                        e.printStackTrace();
                        bitmap = null;
                    }
                    if (bitmap == null) {
                        return null;
                    }
                    byte[] ninePatchChunk = bitmap.getNinePatchChunk();
                    NinePatch.isNinePatchChunk(ninePatchChunk);
                    return new NinePatchDrawable(bitmap, ninePatchChunk, new Rect(), null);
                }
                Drawable createFromStream = Drawable.createFromStream(open, str);
                open.close();
                return createFromStream;
            } catch (IOException e2) {
                e2.printStackTrace();
                return null;
            }
        }

        /* renamed from: a */
        private View m7189a(Context context, Drawable drawable, String str, View.OnClickListener onClickListener, View.OnClickListener onClickListener2) {
            Context context2 = context;
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((WindowManager) context2.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
            float f = displayMetrics.density;
            RelativeLayout relativeLayout = new RelativeLayout(context2);
            ImageView imageView = new ImageView(context2);
            imageView.setImageDrawable(drawable);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setId(1);
            int i = (int) (60.0f * f);
            int i2 = (int) (f * 14.0f);
            int i3 = (int) (18.0f * f);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i);
            layoutParams.addRule(9);
            layoutParams.setMargins(0, i3, (int) (6.0f * f), i3);
            relativeLayout.addView(imageView, layoutParams);
            TextView textView = new TextView(context2);
            textView.setText(str);
            textView.setTextSize(14.0f);
            textView.setGravity(3);
            textView.setIncludeFontPadding(false);
            textView.setPadding(0, 0, 0, 0);
            textView.setLines(2);
            textView.setId(5);
            textView.setMinWidth((int) (185.0f * f));
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams2.addRule(1, 1);
            layoutParams2.addRule(6, 1);
            float f2 = 5.0f * f;
            layoutParams2.setMargins(0, 0, (int) f2, 0);
            relativeLayout.addView(textView, layoutParams2);
            View view = new View(context2);
            view.setBackgroundColor(Color.rgb(214, 214, 214));
            view.setId(3);
            RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, 2);
            layoutParams3.addRule(3, 1);
            layoutParams3.addRule(5, 1);
            layoutParams3.addRule(7, 5);
            int i4 = (int) (12.0f * f);
            layoutParams3.setMargins(0, 0, 0, i4);
            relativeLayout.addView(view, layoutParams3);
            LinearLayout linearLayout = new LinearLayout(context2);
            RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams4.addRule(5, 1);
            layoutParams4.addRule(7, 5);
            layoutParams4.addRule(3, 3);
            Button button = new Button(context2);
            button.setText("跳过");
            button.setBackgroundDrawable(m7188a("buttonNegt.png", context2));
            button.setTextColor(Color.rgb(36, 97, (int) FMParserConstants.f7456AS));
            button.setTextSize(20.0f);
            button.setOnClickListener(onClickListener2);
            button.setId(4);
            int i5 = (int) (45.0f * f);
            LinearLayout.LayoutParams layoutParams5 = new LinearLayout.LayoutParams(0, i5);
            layoutParams5.rightMargin = i2;
            int i6 = (int) (4.0f * f);
            layoutParams5.leftMargin = i6;
            layoutParams5.weight = 1.0f;
            linearLayout.addView(button, layoutParams5);
            Button button2 = new Button(context2);
            button2.setText("确定");
            button2.setTextSize(20.0f);
            button2.setTextColor(Color.rgb(255, 255, 255));
            button2.setBackgroundDrawable(m7188a("buttonPost.png", context2));
            button2.setOnClickListener(onClickListener);
            LinearLayout.LayoutParams layoutParams6 = new LinearLayout.LayoutParams(0, i5);
            layoutParams6.weight = 1.0f;
            layoutParams6.rightMargin = i6;
            linearLayout.addView(button2, layoutParams6);
            relativeLayout.addView(linearLayout, layoutParams4);
            FrameLayout.LayoutParams layoutParams7 = new FrameLayout.LayoutParams((int) (279.0f * f), (int) (f * 163.0f));
            relativeLayout.setPadding(i2, 0, i4, i4);
            relativeLayout.setLayoutParams(layoutParams7);
            relativeLayout.setBackgroundColor(Color.rgb(247, (int) TinkerReport.KEY_LOADED_UNCAUGHT_EXCEPTION, 247));
            PaintDrawable paintDrawable = new PaintDrawable(Color.rgb(247, (int) TinkerReport.KEY_LOADED_UNCAUGHT_EXCEPTION, 247));
            paintDrawable.setCornerRadius(f2);
            relativeLayout.setBackgroundDrawable(paintDrawable);
            return relativeLayout;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void mo34714a() {
            Activity activity;
            Bundle j = AuthAgent.this.mo34806b();
            if (AuthAgent.this.f6313e != null && (activity = (Activity) AuthAgent.this.f6313e.get()) != null) {
                HttpUtils.requestAsync(AuthAgent.this.f6457c, activity, "http://appsupport.qq.com/cgi-bin/qzapps/mapp_addapp.cgi", j, "POST", null);
            }
        }

        public void onError(UiError uiError) {
            if (this.f6321a.get() != null) {
                this.f6321a.get().onError(uiError);
            }
        }

        public void onCancel() {
            if (this.f6321a.get() != null) {
                this.f6321a.get().onCancel();
            }
        }
    }
}
