package com.tencent.bugly.proguard;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import com.baidu.mobstat.Config;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import java.util.Map;
import java.util.UUID;

/* renamed from: com.tencent.bugly.proguard.al */
/* compiled from: BUGLY */
public class C2900al implements Runnable {

    /* renamed from: a */
    public int f6005a;

    /* renamed from: b */
    public int f6006b;

    /* renamed from: c */
    protected int f6007c;

    /* renamed from: d */
    protected long f6008d;

    /* renamed from: e */
    protected long f6009e;

    /* renamed from: f */
    protected boolean f6010f;

    /* renamed from: g */
    private final Context f6011g;

    /* renamed from: h */
    private final int f6012h;

    /* renamed from: i */
    private final byte[] f6013i;

    /* renamed from: j */
    private final C2851a f6014j;

    /* renamed from: k */
    private final C2854a f6015k;

    /* renamed from: l */
    private final C2894ai f6016l;

    /* renamed from: m */
    private final C2896ak f6017m;

    /* renamed from: n */
    private final int f6018n;

    /* renamed from: o */
    private final C2895aj f6019o;

    /* renamed from: p */
    private final C2895aj f6020p;

    /* renamed from: q */
    private String f6021q;

    /* renamed from: r */
    private final String f6022r;

    /* renamed from: s */
    private final Map<String, String> f6023s;

    /* renamed from: t */
    private boolean f6024t;

    public C2900al(Context context, int i, int i2, byte[] bArr, String str, String str2, C2895aj ajVar, boolean z, boolean z2) {
        this(context, i, i2, bArr, str, str2, ajVar, z, 2, 30000, z2, null);
    }

    public C2900al(Context context, int i, int i2, byte[] bArr, String str, String str2, C2895aj ajVar, boolean z, int i3, int i4, boolean z2, Map<String, String> map) {
        this.f6005a = 2;
        this.f6006b = 30000;
        this.f6021q = null;
        this.f6007c = 0;
        this.f6008d = 0;
        this.f6009e = 0;
        this.f6010f = true;
        this.f6024t = false;
        this.f6011g = context;
        this.f6014j = C2851a.m6470a(context);
        this.f6013i = bArr;
        this.f6015k = C2854a.m6574a();
        this.f6016l = C2894ai.m6796a(context);
        this.f6017m = C2896ak.m6805a();
        this.f6018n = i;
        this.f6021q = str;
        this.f6022r = str2;
        this.f6019o = ajVar;
        this.f6020p = this.f6017m.f5975a;
        this.f6010f = z;
        this.f6012h = i2;
        if (i3 > 0) {
            this.f6005a = i3;
        }
        if (i4 > 0) {
            this.f6006b = i4;
        }
        this.f6024t = z2;
        this.f6023s = map;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34540a() {
        this.f6017m.mo34526a(this.f6018n, System.currentTimeMillis());
        C2895aj ajVar = this.f6019o;
        if (ajVar != null) {
            ajVar.mo34184a(this.f6012h);
        }
        C2895aj ajVar2 = this.f6020p;
        if (ajVar2 != null) {
            ajVar2.mo34184a(this.f6012h);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34541a(int i, String str) {
        C2903an.m6865e("[Upload] Failed to upload(%d): %s", Integer.valueOf(i), str);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x002b  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0055  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x007f  */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo34543a(com.tencent.bugly.proguard.C2928bi r14, boolean r15, int r16, java.lang.String r17, int r18) {
        /*
            r13 = this;
            r0 = r13
            int r1 = r0.f6012h
            r2 = 630(0x276, float:8.83E-43)
            if (r1 == r2) goto L_0x001b
            r2 = 640(0x280, float:8.97E-43)
            if (r1 == r2) goto L_0x0018
            r2 = 830(0x33e, float:1.163E-42)
            if (r1 == r2) goto L_0x001b
            r2 = 840(0x348, float:1.177E-42)
            if (r1 == r2) goto L_0x0018
            java.lang.String r1 = java.lang.String.valueOf(r1)
            goto L_0x001d
        L_0x0018:
            java.lang.String r1 = "userinfo"
            goto L_0x001d
        L_0x001b:
            java.lang.String r1 = "crash"
        L_0x001d:
            r2 = 1
            r3 = 0
            if (r15 == 0) goto L_0x002b
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r3] = r1
            java.lang.String r1 = "[Upload] Success: %s"
            com.tencent.bugly.proguard.C2903an.m6857a(r1, r2)
            goto L_0x004a
        L_0x002b:
            r4 = 3
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.Integer r5 = java.lang.Integer.valueOf(r16)
            r4[r3] = r5
            r4[r2] = r1
            r1 = 2
            r4[r1] = r17
            java.lang.String r1 = "[Upload] Failed to upload(%d) %s: %s"
            com.tencent.bugly.proguard.C2903an.m6865e(r1, r4)
            boolean r1 = r0.f6010f
            if (r1 == 0) goto L_0x004a
            com.tencent.bugly.proguard.ak r1 = r0.f6017m
            r2 = 0
            r3 = r18
            r1.mo34529a(r3, r2)
        L_0x004a:
            long r1 = r0.f6008d
            long r3 = r0.f6009e
            long r1 = r1 + r3
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x006a
            com.tencent.bugly.proguard.ak r1 = r0.f6017m
            boolean r2 = r0.f6024t
            long r1 = r1.mo34522a(r2)
            long r3 = r0.f6008d
            long r1 = r1 + r3
            long r3 = r0.f6009e
            long r1 = r1 + r3
            com.tencent.bugly.proguard.ak r3 = r0.f6017m
            boolean r4 = r0.f6024t
            r3.mo34530a(r1, r4)
        L_0x006a:
            com.tencent.bugly.proguard.aj r4 = r0.f6019o
            if (r4 == 0) goto L_0x007b
            int r5 = r0.f6012h
            long r7 = r0.f6008d
            long r9 = r0.f6009e
            r6 = r14
            r11 = r15
            r12 = r17
            r4.mo34185a(r5, r6, r7, r9, r11, r12)
        L_0x007b:
            com.tencent.bugly.proguard.aj r4 = r0.f6020p
            if (r4 == 0) goto L_0x008c
            int r5 = r0.f6012h
            long r7 = r0.f6008d
            long r9 = r0.f6009e
            r6 = r14
            r11 = r15
            r12 = r17
            r4.mo34185a(r5, r6, r7, r9, r11, r12)
        L_0x008c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2900al.mo34543a(com.tencent.bugly.proguard.bi, boolean, int, java.lang.String, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
     arg types: [int, java.lang.String, byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo34545a(C2928bi biVar, C2851a aVar, C2854a aVar2) {
        if (biVar == null) {
            C2903an.m6864d("resp == null!", new Object[0]);
            return false;
        } else if (biVar.f6141a != 0) {
            C2903an.m6865e("resp result error %d", Byte.valueOf(biVar.f6141a));
            return false;
        } else {
            try {
                if (!C2908aq.m6915a(biVar.f6144d)) {
                    if (!C2851a.m6471b().mo34312i().equals(biVar.f6144d)) {
                        C2889ae.m6757a().mo34497a(C2854a.f5712a, "gateway", biVar.f6144d.getBytes("UTF-8"), (C2888ad) null, true);
                        aVar.mo34304d(biVar.f6144d);
                    }
                }
                if (!C2908aq.m6915a(biVar.f6147g) && !C2851a.m6471b().mo34313j().equals(biVar.f6147g)) {
                    C2889ae.m6757a().mo34497a(C2854a.f5712a, Config.DEVICE_PART, biVar.f6147g.getBytes("UTF-8"), (C2888ad) null, true);
                    aVar.mo34306e(biVar.f6147g);
                }
            } catch (Throwable th) {
                C2903an.m6858a(th);
            }
            aVar.f5670o = biVar.f6145e;
            if (biVar.f6142b == 510) {
                if (biVar.f6143c == null) {
                    C2903an.m6865e("[Upload] Strategy data is null. Response cmd: %d", Integer.valueOf(biVar.f6142b));
                    return false;
                }
                C2930bk bkVar = (C2930bk) C2893ah.m6793a(biVar.f6143c, C2930bk.class);
                if (bkVar == null) {
                    C2903an.m6865e("[Upload] Failed to decode strategy from server. Response cmd: %d", Integer.valueOf(biVar.f6142b));
                    return false;
                }
                aVar2.mo34338a(bkVar);
            }
            return true;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:141:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x0251, code lost:
        if (r9 == 0) goto L_0x02fe;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0253, code lost:
        if (r9 != 2) goto L_0x02c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x025e, code lost:
        if ((r7.f6008d + r7.f6009e) <= 0) goto L_0x0275;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0260, code lost:
        r7.f6017m.mo34530a((r7.f6017m.mo34522a(r7.f6024t) + r7.f6008d) + r7.f6009e, r7.f6024t);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0275, code lost:
        r7.f6017m.mo34529a(r9, (com.tencent.bugly.proguard.C2928bi) null);
        com.tencent.bugly.proguard.C2903an.m6857a("[Upload] Session ID is invalid, will try again immediately (pid=%d | tid=%d).", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
        r7.f6017m.mo34523a(r7.f6018n, r7.f6012h, r7.f6013i, r7.f6021q, r7.f6022r, r7.f6019o, r7.f6005a, r7.f6006b, true, r7.f6023s);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x02c0, code lost:
        mo34543a(null, false, 1, "status of server is " + r9, r9);
     */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r25 = this;
            r7 = r25
            java.lang.String r0 = "[Upload] Failed to upload for no status header."
            r1 = 0
            r7.f6007c = r1     // Catch:{ all -> 0x03eb }
            r2 = 0
            r7.f6008d = r2     // Catch:{ all -> 0x03eb }
            r7.f6009e = r2     // Catch:{ all -> 0x03eb }
            byte[] r4 = r7.f6013i     // Catch:{ all -> 0x03eb }
            android.content.Context r5 = r7.f6011g     // Catch:{ all -> 0x03eb }
            java.lang.String r5 = com.tencent.bugly.crashreport.common.info.C2852b.m6545f(r5)     // Catch:{ all -> 0x03eb }
            if (r5 != 0) goto L_0x0023
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "network is not available"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x0023:
            if (r4 == 0) goto L_0x03df
            int r5 = r4.length     // Catch:{ all -> 0x03eb }
            if (r5 != 0) goto L_0x002a
            goto L_0x03df
        L_0x002a:
            com.tencent.bugly.proguard.ak r5 = r7.f6017m     // Catch:{ all -> 0x03eb }
            boolean r6 = r7.f6024t     // Catch:{ all -> 0x03eb }
            long r5 = r5.mo34522a(r6)     // Catch:{ all -> 0x03eb }
            r8 = 2097152(0x200000, double:1.0361308E-317)
            int r10 = r4.length     // Catch:{ all -> 0x03eb }
            long r10 = (long) r10     // Catch:{ all -> 0x03eb }
            long r10 = r10 + r5
            r12 = 2
            r13 = 1
            int r14 = (r10 > r8 ? 1 : (r10 == r8 ? 0 : -1))
            if (r14 < 0) goto L_0x0073
            java.lang.String r0 = "[Upload] Upload too much data, try next time: %d/%d"
            java.lang.Object[] r2 = new java.lang.Object[r12]     // Catch:{ all -> 0x03eb }
            java.lang.Long r3 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x03eb }
            r2[r1] = r3     // Catch:{ all -> 0x03eb }
            java.lang.Long r1 = java.lang.Long.valueOf(r8)     // Catch:{ all -> 0x03eb }
            r2[r13] = r1     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6865e(r0, r2)     // Catch:{ all -> 0x03eb }
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x03eb }
            r0.<init>()     // Catch:{ all -> 0x03eb }
            java.lang.String r1 = "over net consume: "
            r0.append(r1)     // Catch:{ all -> 0x03eb }
            r5 = 2048(0x800, double:1.0118E-320)
            r0.append(r5)     // Catch:{ all -> 0x03eb }
            java.lang.String r1 = "K"
            r0.append(r1)     // Catch:{ all -> 0x03eb }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x03eb }
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x0073:
            java.lang.String r5 = "[Upload] Run upload task with cmd: %d"
            java.lang.Object[] r6 = new java.lang.Object[r13]     // Catch:{ all -> 0x03eb }
            int r8 = r7.f6012h     // Catch:{ all -> 0x03eb }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x03eb }
            r6[r1] = r8     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r5, r6)     // Catch:{ all -> 0x03eb }
            android.content.Context r5 = r7.f6011g     // Catch:{ all -> 0x03eb }
            if (r5 == 0) goto L_0x03d3
            com.tencent.bugly.crashreport.common.info.a r5 = r7.f6014j     // Catch:{ all -> 0x03eb }
            if (r5 == 0) goto L_0x03d3
            com.tencent.bugly.crashreport.common.strategy.a r5 = r7.f6015k     // Catch:{ all -> 0x03eb }
            if (r5 == 0) goto L_0x03d3
            com.tencent.bugly.proguard.ai r5 = r7.f6016l     // Catch:{ all -> 0x03eb }
            if (r5 != 0) goto L_0x0094
            goto L_0x03d3
        L_0x0094:
            com.tencent.bugly.crashreport.common.strategy.a r5 = r7.f6015k     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.crashreport.common.strategy.StrategyBean r5 = r5.mo34340c()     // Catch:{ all -> 0x03eb }
            if (r5 != 0) goto L_0x00a8
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "illegal local strategy"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x00a8:
            java.util.HashMap r6 = new java.util.HashMap     // Catch:{ all -> 0x03eb }
            r6.<init>()     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "prodId"
            com.tencent.bugly.crashreport.common.info.a r9 = r7.f6014j     // Catch:{ all -> 0x03eb }
            java.lang.String r9 = r9.mo34307f()     // Catch:{ all -> 0x03eb }
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "bundleId"
            com.tencent.bugly.crashreport.common.info.a r9 = r7.f6014j     // Catch:{ all -> 0x03eb }
            java.lang.String r9 = r9.f5659d     // Catch:{ all -> 0x03eb }
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "appVer"
            com.tencent.bugly.crashreport.common.info.a r9 = r7.f6014j     // Catch:{ all -> 0x03eb }
            java.lang.String r9 = r9.f5671p     // Catch:{ all -> 0x03eb }
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            java.util.Map<java.lang.String, java.lang.String> r8 = r7.f6023s     // Catch:{ all -> 0x03eb }
            if (r8 == 0) goto L_0x00d3
            java.util.Map<java.lang.String, java.lang.String> r8 = r7.f6023s     // Catch:{ all -> 0x03eb }
            r6.putAll(r8)     // Catch:{ all -> 0x03eb }
        L_0x00d3:
            boolean r8 = r7.f6010f     // Catch:{ all -> 0x03eb }
            if (r8 == 0) goto L_0x013c
            java.lang.String r8 = "cmd"
            int r9 = r7.f6012h     // Catch:{ all -> 0x03eb }
            java.lang.String r9 = java.lang.Integer.toString(r9)     // Catch:{ all -> 0x03eb }
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "platformId"
            java.lang.String r9 = java.lang.Byte.toString(r13)     // Catch:{ all -> 0x03eb }
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "sdkVer"
            com.tencent.bugly.crashreport.common.info.a r9 = r7.f6014j     // Catch:{ all -> 0x03eb }
            r9.getClass()     // Catch:{ all -> 0x03eb }
            java.lang.String r9 = "2.6.5"
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "strategylastUpdateTime"
            long r9 = r5.f5702p     // Catch:{ all -> 0x03eb }
            java.lang.String r9 = java.lang.Long.toString(r9)     // Catch:{ all -> 0x03eb }
            r6.put(r8, r9)     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.ak r8 = r7.f6017m     // Catch:{ all -> 0x03eb }
            boolean r8 = r8.mo34531a(r6)     // Catch:{ all -> 0x03eb }
            if (r8 != 0) goto L_0x0116
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "failed to add security info to HTTP headers"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x0116:
            byte[] r4 = com.tencent.bugly.proguard.C2908aq.m6920a(r4, r12)     // Catch:{ all -> 0x03eb }
            if (r4 != 0) goto L_0x0128
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "failed to zip request body"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x0128:
            com.tencent.bugly.proguard.ak r8 = r7.f6017m     // Catch:{ all -> 0x03eb }
            byte[] r4 = r8.mo34532a(r4)     // Catch:{ all -> 0x03eb }
            if (r4 != 0) goto L_0x013c
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "failed to encrypt request body"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x013c:
            r25.mo34540a()     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = r7.f6021q     // Catch:{ all -> 0x03eb }
            r9 = -1
            r10 = r8
            r8 = 0
            r9 = 0
            r11 = -1
        L_0x0146:
            int r14 = r8 + 1
            int r15 = r7.f6005a     // Catch:{ all -> 0x03eb }
            if (r8 >= r15) goto L_0x03c7
            if (r14 <= r13) goto L_0x0172
            java.lang.String r8 = "[Upload] Failed to upload last time, wait and try(%d) again."
            java.lang.Object[] r9 = new java.lang.Object[r13]     // Catch:{ all -> 0x03eb }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r14)     // Catch:{ all -> 0x03eb }
            r9[r1] = r15     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6864d(r8, r9)     // Catch:{ all -> 0x03eb }
            int r8 = r7.f6006b     // Catch:{ all -> 0x03eb }
            long r8 = (long) r8     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2908aq.m6928b(r8)     // Catch:{ all -> 0x03eb }
            int r8 = r7.f6005a     // Catch:{ all -> 0x03eb }
            if (r14 != r8) goto L_0x0172
            java.lang.String r8 = "[Upload] Use the back-up url at the last time: %s"
            java.lang.Object[] r9 = new java.lang.Object[r13]     // Catch:{ all -> 0x03eb }
            java.lang.String r10 = r7.f6022r     // Catch:{ all -> 0x03eb }
            r9[r1] = r10     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6864d(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r10 = r7.f6022r     // Catch:{ all -> 0x03eb }
        L_0x0172:
            java.lang.String r8 = "[Upload] Send %d bytes"
            java.lang.Object[] r9 = new java.lang.Object[r13]     // Catch:{ all -> 0x03eb }
            int r15 = r4.length     // Catch:{ all -> 0x03eb }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x03eb }
            r9[r1] = r15     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r8, r9)     // Catch:{ all -> 0x03eb }
            boolean r8 = r7.f6010f     // Catch:{ all -> 0x03eb }
            if (r8 == 0) goto L_0x0189
            java.lang.String r8 = m6840a(r10)     // Catch:{ all -> 0x03eb }
            r10 = r8
        L_0x0189:
            java.lang.String r8 = "[Upload] Upload to %s with cmd %d (pid=%d | tid=%d)."
            r9 = 4
            java.lang.Object[] r9 = new java.lang.Object[r9]     // Catch:{ all -> 0x03eb }
            r9[r1] = r10     // Catch:{ all -> 0x03eb }
            int r15 = r7.f6012h     // Catch:{ all -> 0x03eb }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x03eb }
            r9[r13] = r15     // Catch:{ all -> 0x03eb }
            int r15 = android.os.Process.myPid()     // Catch:{ all -> 0x03eb }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x03eb }
            r9[r12] = r15     // Catch:{ all -> 0x03eb }
            int r15 = android.os.Process.myTid()     // Catch:{ all -> 0x03eb }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x03eb }
            r2 = 3
            r9[r2] = r15     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r8, r9)     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.ai r3 = r7.f6016l     // Catch:{ all -> 0x03eb }
            byte[] r3 = r3.mo34519a(r10, r4, r7, r6)     // Catch:{ all -> 0x03eb }
            if (r3 != 0) goto L_0x01c2
            java.lang.String r2 = "Failed to upload for no response!"
            r7.mo34541a(r13, r2)     // Catch:{ all -> 0x03eb }
        L_0x01bd:
            r8 = r14
            r2 = 0
        L_0x01c0:
            r9 = 1
            goto L_0x0146
        L_0x01c2:
            com.tencent.bugly.proguard.ai r8 = r7.f6016l     // Catch:{ all -> 0x03eb }
            java.util.Map<java.lang.String, java.lang.String> r8 = r8.f5973b     // Catch:{ all -> 0x03eb }
            boolean r9 = r7.f6010f     // Catch:{ all -> 0x03eb }
            if (r9 == 0) goto L_0x02fd
            boolean r9 = m6841a(r8)     // Catch:{ all -> 0x03eb }
            if (r9 != 0) goto L_0x0224
            java.lang.String r2 = "[Upload] Headers from server is not valid, just try again (pid=%d | tid=%d)."
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ all -> 0x03eb }
            int r9 = android.os.Process.myPid()     // Catch:{ all -> 0x03eb }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x03eb }
            r3[r1] = r9     // Catch:{ all -> 0x03eb }
            int r9 = android.os.Process.myTid()     // Catch:{ all -> 0x03eb }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x03eb }
            r3[r13] = r9     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r2, r3)     // Catch:{ all -> 0x03eb }
            r7.mo34541a(r13, r0)     // Catch:{ all -> 0x03eb }
            if (r8 == 0) goto L_0x021e
            java.util.Set r2 = r8.entrySet()     // Catch:{ all -> 0x03eb }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x03eb }
        L_0x01f8:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x03eb }
            if (r3 == 0) goto L_0x021e
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x03eb }
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3     // Catch:{ all -> 0x03eb }
            java.lang.String r8 = "[key]: %s, [value]: %s"
            java.lang.Object[] r9 = new java.lang.Object[r12]     // Catch:{ all -> 0x03eb }
            java.lang.Object r15 = r3.getKey()     // Catch:{ all -> 0x03eb }
            r9[r1] = r15     // Catch:{ all -> 0x03eb }
            java.lang.Object r3 = r3.getValue()     // Catch:{ all -> 0x03eb }
            r9[r13] = r3     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = java.lang.String.format(r8, r9)     // Catch:{ all -> 0x03eb }
            java.lang.Object[] r8 = new java.lang.Object[r1]     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r3, r8)     // Catch:{ all -> 0x03eb }
            goto L_0x01f8
        L_0x021e:
            java.lang.Object[] r2 = new java.lang.Object[r1]     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r2)     // Catch:{ all -> 0x03eb }
            goto L_0x01bd
        L_0x0224:
            java.lang.String r9 = "status"
            java.lang.Object r9 = r8.get(r9)     // Catch:{ all -> 0x02df }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x02df }
            int r9 = java.lang.Integer.parseInt(r9)     // Catch:{ all -> 0x02df }
            java.lang.String r11 = "[Upload] Status from server is %d (pid=%d | tid=%d)."
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x02db }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x02db }
            r2[r1] = r15     // Catch:{ all -> 0x02db }
            int r15 = android.os.Process.myPid()     // Catch:{ all -> 0x02db }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x02db }
            r2[r13] = r15     // Catch:{ all -> 0x02db }
            int r15 = android.os.Process.myTid()     // Catch:{ all -> 0x02db }
            java.lang.Integer r15 = java.lang.Integer.valueOf(r15)     // Catch:{ all -> 0x02db }
            r2[r12] = r15     // Catch:{ all -> 0x02db }
            com.tencent.bugly.proguard.C2903an.m6863c(r11, r2)     // Catch:{ all -> 0x02db }
            if (r9 == 0) goto L_0x02fe
            if (r9 != r12) goto L_0x02c0
            long r2 = r7.f6008d     // Catch:{ all -> 0x03eb }
            long r4 = r7.f6009e     // Catch:{ all -> 0x03eb }
            long r2 = r2 + r4
            r15 = 0
            int r0 = (r2 > r15 ? 1 : (r2 == r15 ? 0 : -1))
            if (r0 <= 0) goto L_0x0275
            com.tencent.bugly.proguard.ak r0 = r7.f6017m     // Catch:{ all -> 0x03eb }
            boolean r2 = r7.f6024t     // Catch:{ all -> 0x03eb }
            long r2 = r0.mo34522a(r2)     // Catch:{ all -> 0x03eb }
            long r4 = r7.f6008d     // Catch:{ all -> 0x03eb }
            long r2 = r2 + r4
            long r4 = r7.f6009e     // Catch:{ all -> 0x03eb }
            long r2 = r2 + r4
            com.tencent.bugly.proguard.ak r0 = r7.f6017m     // Catch:{ all -> 0x03eb }
            boolean r4 = r7.f6024t     // Catch:{ all -> 0x03eb }
            r0.mo34530a(r2, r4)     // Catch:{ all -> 0x03eb }
        L_0x0275:
            com.tencent.bugly.proguard.ak r0 = r7.f6017m     // Catch:{ all -> 0x03eb }
            r2 = 0
            r0.mo34529a(r9, r2)     // Catch:{ all -> 0x03eb }
            java.lang.String r0 = "[Upload] Session ID is invalid, will try again immediately (pid=%d | tid=%d)."
            java.lang.Object[] r2 = new java.lang.Object[r12]     // Catch:{ all -> 0x03eb }
            int r3 = android.os.Process.myPid()     // Catch:{ all -> 0x03eb }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ all -> 0x03eb }
            r2[r1] = r3     // Catch:{ all -> 0x03eb }
            int r1 = android.os.Process.myTid()     // Catch:{ all -> 0x03eb }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x03eb }
            r2[r13] = r1     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r2)     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.ak r14 = r7.f6017m     // Catch:{ all -> 0x03eb }
            int r15 = r7.f6018n     // Catch:{ all -> 0x03eb }
            int r0 = r7.f6012h     // Catch:{ all -> 0x03eb }
            byte[] r1 = r7.f6013i     // Catch:{ all -> 0x03eb }
            java.lang.String r2 = r7.f6021q     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = r7.f6022r     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.aj r4 = r7.f6019o     // Catch:{ all -> 0x03eb }
            int r5 = r7.f6005a     // Catch:{ all -> 0x03eb }
            int r6 = r7.f6006b     // Catch:{ all -> 0x03eb }
            r23 = 1
            java.util.Map<java.lang.String, java.lang.String> r8 = r7.f6023s     // Catch:{ all -> 0x03eb }
            r16 = r0
            r17 = r1
            r18 = r2
            r19 = r3
            r20 = r4
            r21 = r5
            r22 = r6
            r24 = r8
            r14.mo34523a(r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)     // Catch:{ all -> 0x03eb }
            goto L_0x02da
        L_0x02c0:
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x03eb }
            r0.<init>()     // Catch:{ all -> 0x03eb }
            java.lang.String r1 = "status of server is "
            r0.append(r1)     // Catch:{ all -> 0x03eb }
            r0.append(r9)     // Catch:{ all -> 0x03eb }
            java.lang.String r5 = r0.toString()     // Catch:{ all -> 0x03eb }
            r1 = r25
            r6 = r9
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
        L_0x02da:
            return
        L_0x02db:
            r15 = 0
            r11 = r9
            goto L_0x02e1
        L_0x02df:
            r15 = 0
        L_0x02e1:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x03eb }
            r2.<init>()     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = "[Upload] Failed to upload for format of status header is invalid: "
            r2.append(r3)     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = java.lang.Integer.toString(r11)     // Catch:{ all -> 0x03eb }
            r2.append(r3)     // Catch:{ all -> 0x03eb }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x03eb }
            r7.mo34541a(r13, r2)     // Catch:{ all -> 0x03eb }
            r8 = r14
            r2 = r15
            goto L_0x01c0
        L_0x02fd:
            r9 = r11
        L_0x02fe:
            java.lang.String r0 = "[Upload] Received %d bytes"
            java.lang.Object[] r2 = new java.lang.Object[r13]     // Catch:{ all -> 0x03eb }
            int r4 = r3.length     // Catch:{ all -> 0x03eb }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x03eb }
            r2[r1] = r4     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r2)     // Catch:{ all -> 0x03eb }
            boolean r0 = r7.f6010f     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x036d
            int r0 = r3.length     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x0347
            java.util.Set r0 = r8.entrySet()     // Catch:{ all -> 0x03eb }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x03eb }
        L_0x031b:
            boolean r2 = r0.hasNext()     // Catch:{ all -> 0x03eb }
            if (r2 == 0) goto L_0x033b
            java.lang.Object r2 = r0.next()     // Catch:{ all -> 0x03eb }
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2     // Catch:{ all -> 0x03eb }
            java.lang.String r3 = "[Upload] HTTP headers from server: key = %s, value = %s"
            java.lang.Object[] r4 = new java.lang.Object[r12]     // Catch:{ all -> 0x03eb }
            java.lang.Object r5 = r2.getKey()     // Catch:{ all -> 0x03eb }
            r4[r1] = r5     // Catch:{ all -> 0x03eb }
            java.lang.Object r2 = r2.getValue()     // Catch:{ all -> 0x03eb }
            r4[r13] = r2     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r3, r4)     // Catch:{ all -> 0x03eb }
            goto L_0x031b
        L_0x033b:
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "response data from server is empty"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x0347:
            com.tencent.bugly.proguard.ak r0 = r7.f6017m     // Catch:{ all -> 0x03eb }
            byte[] r0 = r0.mo34536b(r3)     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x035b
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "failed to decrypt response from server"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x035b:
            byte[] r3 = com.tencent.bugly.proguard.C2908aq.m6934b(r0, r12)     // Catch:{ all -> 0x03eb }
            if (r3 != 0) goto L_0x036d
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "failed unzip(Gzip) response from server"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x036d:
            boolean r0 = r7.f6010f     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.bi r2 = com.tencent.bugly.proguard.C2893ah.m6790a(r3, r5, r0)     // Catch:{ all -> 0x03eb }
            if (r2 != 0) goto L_0x0381
            r2 = 0
            r3 = 0
            r4 = 1
            java.lang.String r5 = "failed to decode response package"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x0381:
            boolean r0 = r7.f6010f     // Catch:{ all -> 0x03eb }
            if (r0 == 0) goto L_0x038a
            com.tencent.bugly.proguard.ak r0 = r7.f6017m     // Catch:{ all -> 0x03eb }
            r0.mo34529a(r9, r2)     // Catch:{ all -> 0x03eb }
        L_0x038a:
            java.lang.String r0 = "[Upload] Response cmd is: %d, length of sBuffer is: %d"
            java.lang.Object[] r3 = new java.lang.Object[r12]     // Catch:{ all -> 0x03eb }
            int r4 = r2.f6142b     // Catch:{ all -> 0x03eb }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x03eb }
            r3[r1] = r4     // Catch:{ all -> 0x03eb }
            byte[] r4 = r2.f6143c     // Catch:{ all -> 0x03eb }
            if (r4 != 0) goto L_0x039b
            goto L_0x039e
        L_0x039b:
            byte[] r1 = r2.f6143c     // Catch:{ all -> 0x03eb }
            int r1 = r1.length     // Catch:{ all -> 0x03eb }
        L_0x039e:
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x03eb }
            r3[r13] = r1     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r3)     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.crashreport.common.info.a r0 = r7.f6014j     // Catch:{ all -> 0x03eb }
            com.tencent.bugly.crashreport.common.strategy.a r1 = r7.f6015k     // Catch:{ all -> 0x03eb }
            boolean r0 = r7.mo34545a(r2, r0, r1)     // Catch:{ all -> 0x03eb }
            if (r0 != 0) goto L_0x03bc
            r3 = 0
            r4 = 2
            java.lang.String r5 = "failed to process response package"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x03bc:
            r3 = 1
            r4 = 2
            java.lang.String r5 = "successfully uploaded"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x03c7:
            r2 = 0
            r3 = 0
            java.lang.String r5 = "failed after many attempts"
            r6 = 0
            r1 = r25
            r4 = r9
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            goto L_0x03f5
        L_0x03d3:
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "illegal access error"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x03df:
            r2 = 0
            r3 = 0
            r4 = 0
            java.lang.String r5 = "request package is empty!"
            r6 = 0
            r1 = r25
            r1.mo34543a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x03eb }
            return
        L_0x03eb:
            r0 = move-exception
            boolean r1 = com.tencent.bugly.proguard.C2903an.m6858a(r0)
            if (r1 != 0) goto L_0x03f5
            r0.printStackTrace()
        L_0x03f5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2900al.run():void");
    }

    /* renamed from: a */
    public void mo34544a(String str, long j, String str2) {
        this.f6007c++;
        this.f6008d += j;
    }

    /* renamed from: a */
    public void mo34542a(long j) {
        this.f6009e += j;
    }

    /* renamed from: a */
    private static String m6840a(String str) {
        if (C2908aq.m6915a(str)) {
            return str;
        }
        try {
            return String.format("%s?aid=%s", str, UUID.randomUUID().toString());
        } catch (Throwable th) {
            C2903an.m6858a(th);
            return str;
        }
    }

    /* renamed from: a */
    private static boolean m6841a(Map<String, String> map) {
        if (map == null || map.size() == 0) {
            C2903an.m6864d("[Upload] Headers is empty.", new Object[0]);
            return false;
        } else if (!map.containsKey(NotificationCompat.CATEGORY_STATUS)) {
            C2903an.m6864d("[Upload] Headers does not contain %s", NotificationCompat.CATEGORY_STATUS);
            return false;
        } else if (!map.containsKey("Bugly-Version")) {
            C2903an.m6864d("[Upload] Headers does not contain %s", "Bugly-Version");
            return false;
        } else {
            String str = map.get("Bugly-Version");
            if (!str.contains("bugly")) {
                C2903an.m6864d("[Upload] Bugly version is not valid: %s", str);
                return false;
            }
            C2903an.m6863c("[Upload] Bugly version from headers is: %s", str);
            return true;
        }
    }
}
