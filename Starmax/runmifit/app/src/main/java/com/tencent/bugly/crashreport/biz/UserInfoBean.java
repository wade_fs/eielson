package com.tencent.bugly.crashreport.biz;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.proguard.C2908aq;
import java.util.Map;

/* compiled from: BUGLY */
public class UserInfoBean implements Parcelable {
    public static final Parcelable.Creator<UserInfoBean> CREATOR = new Parcelable.Creator<UserInfoBean>() {
        /* class com.tencent.bugly.crashreport.biz.UserInfoBean.C28401 */

        /* renamed from: a */
        public UserInfoBean createFromParcel(Parcel parcel) {
            return new UserInfoBean(parcel);
        }

        /* renamed from: a */
        public UserInfoBean[] newArray(int i) {
            return new UserInfoBean[i];
        }
    };

    /* renamed from: a */
    public long f5545a;

    /* renamed from: b */
    public int f5546b;

    /* renamed from: c */
    public String f5547c;

    /* renamed from: d */
    public String f5548d;

    /* renamed from: e */
    public long f5549e;

    /* renamed from: f */
    public long f5550f;

    /* renamed from: g */
    public long f5551g;

    /* renamed from: h */
    public long f5552h;

    /* renamed from: i */
    public long f5553i;

    /* renamed from: j */
    public String f5554j;

    /* renamed from: k */
    public long f5555k;

    /* renamed from: l */
    public boolean f5556l;

    /* renamed from: m */
    public String f5557m;

    /* renamed from: n */
    public String f5558n;

    /* renamed from: o */
    public int f5559o;

    /* renamed from: p */
    public int f5560p;

    /* renamed from: q */
    public int f5561q;

    /* renamed from: r */
    public Map<String, String> f5562r;

    /* renamed from: s */
    public Map<String, String> f5563s;

    public int describeContents() {
        return 0;
    }

    public UserInfoBean() {
        this.f5555k = 0;
        this.f5556l = false;
        this.f5557m = "unknown";
        this.f5560p = -1;
        this.f5561q = -1;
        this.f5562r = null;
        this.f5563s = null;
    }

    public UserInfoBean(Parcel parcel) {
        this.f5555k = 0;
        boolean z = false;
        this.f5556l = false;
        this.f5557m = "unknown";
        this.f5560p = -1;
        this.f5561q = -1;
        this.f5562r = null;
        this.f5563s = null;
        this.f5546b = parcel.readInt();
        this.f5547c = parcel.readString();
        this.f5548d = parcel.readString();
        this.f5549e = parcel.readLong();
        this.f5550f = parcel.readLong();
        this.f5551g = parcel.readLong();
        this.f5552h = parcel.readLong();
        this.f5553i = parcel.readLong();
        this.f5554j = parcel.readString();
        this.f5555k = parcel.readLong();
        this.f5556l = parcel.readByte() == 1 ? true : z;
        this.f5557m = parcel.readString();
        this.f5560p = parcel.readInt();
        this.f5561q = parcel.readInt();
        this.f5562r = C2908aq.m6927b(parcel);
        this.f5563s = C2908aq.m6927b(parcel);
        this.f5558n = parcel.readString();
        this.f5559o = parcel.readInt();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f5546b);
        parcel.writeString(this.f5547c);
        parcel.writeString(this.f5548d);
        parcel.writeLong(this.f5549e);
        parcel.writeLong(this.f5550f);
        parcel.writeLong(this.f5551g);
        parcel.writeLong(this.f5552h);
        parcel.writeLong(this.f5553i);
        parcel.writeString(this.f5554j);
        parcel.writeLong(this.f5555k);
        parcel.writeByte(this.f5556l ? (byte) 1 : 0);
        parcel.writeString(this.f5557m);
        parcel.writeInt(this.f5560p);
        parcel.writeInt(this.f5561q);
        C2908aq.m6929b(parcel, this.f5562r);
        C2908aq.m6929b(parcel, this.f5563s);
        parcel.writeString(this.f5558n);
        parcel.writeInt(this.f5559o);
    }
}
