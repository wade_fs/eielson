package com.tencent.bugly.proguard;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.tencent.bugly.crashreport.common.info.C2851a;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.ac */
/* compiled from: BUGLY */
public class C2885ac {

    /* renamed from: a */
    public static final long f5928a = System.currentTimeMillis();

    /* renamed from: b */
    private static C2885ac f5929b;

    /* renamed from: c */
    private Context f5930c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public String f5931d = C2851a.m6471b().f5660e;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public Map<Integer, Map<String, C2884ab>> f5932e = new HashMap();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public SharedPreferences f5933f;

    public C2885ac(Context context) {
        this.f5930c = context;
        this.f5933f = context.getSharedPreferences("crashrecord", 0);
    }

    /* renamed from: a */
    public static synchronized C2885ac m6738a(Context context) {
        C2885ac acVar;
        synchronized (C2885ac.class) {
            if (f5929b == null) {
                f5929b = new C2885ac(context);
            }
            acVar = f5929b;
        }
        return acVar;
    }

    /* renamed from: a */
    public static synchronized C2885ac m6737a() {
        C2885ac acVar;
        synchronized (C2885ac.class) {
            acVar = f5929b;
        }
        return acVar;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0077, code lost:
        return true;
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean m6744b(int r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            r0 = 0
            java.util.List r1 = r12.m6747c(r13)     // Catch:{ Exception -> 0x0082 }
            if (r1 != 0) goto L_0x000a
            monitor-exit(r12)
            return r0
        L_0x000a:
            long r2 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0082 }
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0082 }
            r4.<init>()     // Catch:{ Exception -> 0x0082 }
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ Exception -> 0x0082 }
            r5.<init>()     // Catch:{ Exception -> 0x0082 }
            java.util.Iterator r6 = r1.iterator()     // Catch:{ Exception -> 0x0082 }
        L_0x001c:
            boolean r7 = r6.hasNext()     // Catch:{ Exception -> 0x0082 }
            r8 = 86400000(0x5265c00, double:4.2687272E-316)
            if (r7 == 0) goto L_0x004b
            java.lang.Object r7 = r6.next()     // Catch:{ Exception -> 0x0082 }
            com.tencent.bugly.proguard.ab r7 = (com.tencent.bugly.proguard.C2884ab) r7     // Catch:{ Exception -> 0x0082 }
            java.lang.String r10 = r7.f5922b     // Catch:{ Exception -> 0x0082 }
            if (r10 == 0) goto L_0x0040
            java.lang.String r10 = r7.f5922b     // Catch:{ Exception -> 0x0082 }
            java.lang.String r11 = r12.f5931d     // Catch:{ Exception -> 0x0082 }
            boolean r10 = r10.equalsIgnoreCase(r11)     // Catch:{ Exception -> 0x0082 }
            if (r10 == 0) goto L_0x0040
            int r10 = r7.f5924d     // Catch:{ Exception -> 0x0082 }
            if (r10 <= 0) goto L_0x0040
            r4.add(r7)     // Catch:{ Exception -> 0x0082 }
        L_0x0040:
            long r10 = r7.f5923c     // Catch:{ Exception -> 0x0082 }
            long r10 = r10 + r8
            int r8 = (r10 > r2 ? 1 : (r10 == r2 ? 0 : -1))
            if (r8 >= 0) goto L_0x001c
            r5.add(r7)     // Catch:{ Exception -> 0x0082 }
            goto L_0x001c
        L_0x004b:
            java.util.Collections.sort(r4)     // Catch:{ Exception -> 0x0082 }
            int r6 = r4.size()     // Catch:{ Exception -> 0x0082 }
            r7 = 2
            if (r6 < r7) goto L_0x0078
            int r5 = r4.size()     // Catch:{ Exception -> 0x0082 }
            r6 = 1
            if (r5 <= 0) goto L_0x0076
            int r5 = r4.size()     // Catch:{ Exception -> 0x0082 }
            int r5 = r5 - r6
            java.lang.Object r4 = r4.get(r5)     // Catch:{ Exception -> 0x0082 }
            com.tencent.bugly.proguard.ab r4 = (com.tencent.bugly.proguard.C2884ab) r4     // Catch:{ Exception -> 0x0082 }
            long r4 = r4.f5923c     // Catch:{ Exception -> 0x0082 }
            long r4 = r4 + r8
            int r7 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r7 >= 0) goto L_0x0076
            r1.clear()     // Catch:{ Exception -> 0x0082 }
            r12.m6741a(r13, r1)     // Catch:{ Exception -> 0x0082 }
            monitor-exit(r12)
            return r0
        L_0x0076:
            monitor-exit(r12)
            return r6
        L_0x0078:
            r1.removeAll(r5)     // Catch:{ Exception -> 0x0082 }
            r12.m6741a(r13, r1)     // Catch:{ Exception -> 0x0082 }
            monitor-exit(r12)
            return r0
        L_0x0080:
            r13 = move-exception
            goto L_0x008b
        L_0x0082:
            java.lang.String r13 = "isFrequentCrash failed"
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch:{ all -> 0x0080 }
            com.tencent.bugly.proguard.C2903an.m6865e(r13, r1)     // Catch:{ all -> 0x0080 }
            monitor-exit(r12)
            return r0
        L_0x008b:
            monitor-exit(r12)
            goto L_0x008e
        L_0x008d:
            throw r13
        L_0x008e:
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2885ac.m6744b(int):boolean");
    }

    /* renamed from: a */
    public void mo34483a(final int i, final int i2) {
        C2901am.m6848a().mo34547a(new Runnable() {
            /* class com.tencent.bugly.proguard.C2885ac.C28861 */

            public void run() {
                C2884ab abVar;
                try {
                    if (!TextUtils.isEmpty(C2885ac.this.f5931d)) {
                        List<C2884ab> a = C2885ac.this.m6747c(i);
                        if (a == null) {
                            a = new ArrayList<>();
                        }
                        if (C2885ac.this.f5932e.get(Integer.valueOf(i)) == null) {
                            C2885ac.this.f5932e.put(Integer.valueOf(i), new HashMap());
                        }
                        if (((Map) C2885ac.this.f5932e.get(Integer.valueOf(i))).get(C2885ac.this.f5931d) == null) {
                            abVar = new C2884ab();
                            abVar.f5921a = (long) i;
                            abVar.f5927g = C2885ac.f5928a;
                            abVar.f5922b = C2885ac.this.f5931d;
                            abVar.f5926f = C2851a.m6471b().f5671p;
                            C2851a.m6471b().getClass();
                            abVar.f5925e = "2.6.5";
                            abVar.f5923c = System.currentTimeMillis();
                            abVar.f5924d = i2;
                            ((Map) C2885ac.this.f5932e.get(Integer.valueOf(i))).put(C2885ac.this.f5931d, abVar);
                        } else {
                            abVar = (C2884ab) ((Map) C2885ac.this.f5932e.get(Integer.valueOf(i))).get(C2885ac.this.f5931d);
                            abVar.f5924d = i2;
                        }
                        ArrayList arrayList = new ArrayList();
                        boolean z = false;
                        for (C2884ab abVar2 : a) {
                            if (abVar2.f5927g == abVar.f5927g && abVar2.f5922b != null && abVar2.f5922b.equalsIgnoreCase(abVar.f5922b)) {
                                z = true;
                                abVar2.f5924d = abVar.f5924d;
                            }
                            if ((abVar2.f5925e != null && !abVar2.f5925e.equalsIgnoreCase(abVar.f5925e)) || ((abVar2.f5926f != null && !abVar2.f5926f.equalsIgnoreCase(abVar.f5926f)) || abVar2.f5924d <= 0)) {
                                arrayList.add(abVar2);
                            }
                        }
                        a.removeAll(arrayList);
                        if (!z) {
                            a.add(abVar);
                        }
                        C2885ac.this.m6741a(i, a);
                    }
                } catch (Exception unused) {
                    C2903an.m6865e("saveCrashRecord failed", new Object[0]);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0049, code lost:
        if (r6 != null) goto L_0x004b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:?, code lost:
        r6.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0057, code lost:
        if (r6 != null) goto L_0x004b;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x005d A[SYNTHETIC, Splitter:B:35:0x005d] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T extends java.util.List<?>> T m6747c(int r6) {
        /*
            r5 = this;
            monitor-enter(r5)
            r0 = 0
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x0063 }
            android.content.Context r3 = r5.f5930c     // Catch:{ Exception -> 0x0063 }
            java.lang.String r4 = "crashrecord"
            java.io.File r3 = r3.getDir(r4, r1)     // Catch:{ Exception -> 0x0063 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0063 }
            r4.<init>()     // Catch:{ Exception -> 0x0063 }
            r4.append(r6)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r6 = ""
            r4.append(r6)     // Catch:{ Exception -> 0x0063 }
            java.lang.String r6 = r4.toString()     // Catch:{ Exception -> 0x0063 }
            r2.<init>(r3, r6)     // Catch:{ Exception -> 0x0063 }
            boolean r6 = r2.exists()     // Catch:{ Exception -> 0x0063 }
            if (r6 != 0) goto L_0x0029
            monitor-exit(r5)
            return r0
        L_0x0029:
            java.io.ObjectInputStream r6 = new java.io.ObjectInputStream     // Catch:{ IOException -> 0x004f, ClassNotFoundException -> 0x0041, all -> 0x003e }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ IOException -> 0x004f, ClassNotFoundException -> 0x0041, all -> 0x003e }
            r3.<init>(r2)     // Catch:{ IOException -> 0x004f, ClassNotFoundException -> 0x0041, all -> 0x003e }
            r6.<init>(r3)     // Catch:{ IOException -> 0x004f, ClassNotFoundException -> 0x0041, all -> 0x003e }
            java.lang.Object r2 = r6.readObject()     // Catch:{ IOException -> 0x0050, ClassNotFoundException -> 0x0042 }
            java.util.List r2 = (java.util.List) r2     // Catch:{ IOException -> 0x0050, ClassNotFoundException -> 0x0042 }
            r6.close()     // Catch:{ Exception -> 0x0063 }
            monitor-exit(r5)
            return r2
        L_0x003e:
            r2 = move-exception
            r6 = r0
            goto L_0x005b
        L_0x0041:
            r6 = r0
        L_0x0042:
            java.lang.String r2 = "get object error"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x005a }
            com.tencent.bugly.proguard.C2903an.m6857a(r2, r3)     // Catch:{ all -> 0x005a }
            if (r6 == 0) goto L_0x006a
        L_0x004b:
            r6.close()     // Catch:{ Exception -> 0x0063 }
            goto L_0x006a
        L_0x004f:
            r6 = r0
        L_0x0050:
            java.lang.String r2 = "open record file error"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x005a }
            com.tencent.bugly.proguard.C2903an.m6857a(r2, r3)     // Catch:{ all -> 0x005a }
            if (r6 == 0) goto L_0x006a
            goto L_0x004b
        L_0x005a:
            r2 = move-exception
        L_0x005b:
            if (r6 == 0) goto L_0x0060
            r6.close()     // Catch:{ Exception -> 0x0063 }
        L_0x0060:
            throw r2     // Catch:{ Exception -> 0x0063 }
        L_0x0061:
            r6 = move-exception
            goto L_0x006c
        L_0x0063:
            java.lang.String r6 = "readCrashRecord error"
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0061 }
            com.tencent.bugly.proguard.C2903an.m6865e(r6, r1)     // Catch:{ all -> 0x0061 }
        L_0x006a:
            monitor-exit(r5)
            return r0
        L_0x006c:
            monitor-exit(r5)
            goto L_0x006f
        L_0x006e:
            throw r6
        L_0x006f:
            goto L_0x006e
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2885ac.m6747c(int):java.util.List");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x004d A[SYNTHETIC, Splitter:B:23:0x004d] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0053 A[Catch:{ Exception -> 0x0059 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized <T extends java.util.List<?>> void m6741a(int r5, java.util.List r6) {
        /*
            r4 = this;
            monitor-enter(r4)
            if (r6 != 0) goto L_0x0005
            monitor-exit(r4)
            return
        L_0x0005:
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0059 }
            android.content.Context r2 = r4.f5930c     // Catch:{ Exception -> 0x0059 }
            java.lang.String r3 = "crashrecord"
            java.io.File r2 = r2.getDir(r3, r0)     // Catch:{ Exception -> 0x0059 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0059 }
            r3.<init>()     // Catch:{ Exception -> 0x0059 }
            r3.append(r5)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r5 = ""
            r3.append(r5)     // Catch:{ Exception -> 0x0059 }
            java.lang.String r5 = r3.toString()     // Catch:{ Exception -> 0x0059 }
            r1.<init>(r2, r5)     // Catch:{ Exception -> 0x0059 }
            r5 = 0
            java.io.ObjectOutputStream r2 = new java.io.ObjectOutputStream     // Catch:{ IOException -> 0x0040 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0040 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0040 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0040 }
            r2.writeObject(r6)     // Catch:{ IOException -> 0x003a, all -> 0x0036 }
            r2.close()     // Catch:{ Exception -> 0x0059 }
            goto L_0x0060
        L_0x0036:
            r5 = move-exception
            r6 = r5
            r5 = r2
            goto L_0x0051
        L_0x003a:
            r5 = move-exception
            r6 = r5
            r5 = r2
            goto L_0x0041
        L_0x003e:
            r6 = move-exception
            goto L_0x0051
        L_0x0040:
            r6 = move-exception
        L_0x0041:
            r6.printStackTrace()     // Catch:{ all -> 0x003e }
            java.lang.String r6 = "open record file error"
            java.lang.Object[] r1 = new java.lang.Object[r0]     // Catch:{ all -> 0x003e }
            com.tencent.bugly.proguard.C2903an.m6857a(r6, r1)     // Catch:{ all -> 0x003e }
            if (r5 == 0) goto L_0x0060
            r5.close()     // Catch:{ Exception -> 0x0059 }
            goto L_0x0060
        L_0x0051:
            if (r5 == 0) goto L_0x0056
            r5.close()     // Catch:{ Exception -> 0x0059 }
        L_0x0056:
            throw r6     // Catch:{ Exception -> 0x0059 }
        L_0x0057:
            r5 = move-exception
            goto L_0x0062
        L_0x0059:
            java.lang.String r5 = "writeCrashRecord error"
            java.lang.Object[] r6 = new java.lang.Object[r0]     // Catch:{ all -> 0x0057 }
            com.tencent.bugly.proguard.C2903an.m6865e(r5, r6)     // Catch:{ all -> 0x0057 }
        L_0x0060:
            monitor-exit(r4)
            return
        L_0x0062:
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2885ac.m6741a(int, java.util.List):void");
    }

    /* renamed from: a */
    public synchronized boolean mo34484a(final int i) {
        boolean z;
        z = true;
        try {
            SharedPreferences sharedPreferences = this.f5933f;
            z = sharedPreferences.getBoolean(i + "_" + this.f5931d, true);
            C2901am.m6848a().mo34547a(new Runnable() {
                /* class com.tencent.bugly.proguard.C2885ac.C28872 */

                public void run() {
                    boolean b = C2885ac.this.m6744b(i);
                    SharedPreferences.Editor edit = C2885ac.this.f5933f.edit();
                    edit.putBoolean(i + "_" + C2885ac.this.f5931d, !b).commit();
                }
            });
        } catch (Exception unused) {
            C2903an.m6865e("canInit error", new Object[0]);
            return z;
        }
        return z;
    }
}
