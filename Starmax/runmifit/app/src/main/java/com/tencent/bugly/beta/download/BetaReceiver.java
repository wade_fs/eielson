package com.tencent.bugly.beta.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.beta.p050ui.C2821c;
import com.tencent.bugly.beta.p050ui.C2824f;
import com.tencent.bugly.beta.p050ui.C2825g;
import com.tencent.bugly.beta.p050ui.C2826h;
import com.tencent.bugly.beta.upgrade.C2829c;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.open.SocialConstants;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: BUGLY */
public class BetaReceiver extends BroadcastReceiver {
    public static String CONNECTIVITY_CHANGE = "android.net.conn.CONNECTIVITY_CHANGE";
    public static ConcurrentHashMap<String, C2803c> netListeners = new ConcurrentHashMap<>(3);
    public static int netStatus = 0;

    public static synchronized void addTask(DownloadTask downloadTask) {
        synchronized (BetaReceiver.class) {
            netStatus = C2804a.m6256a(C2808e.f5336E.f5385s);
            C2824f fVar = new C2824f();
            fVar.f5434n = downloadTask;
            netListeners.put(downloadTask.getDownloadUrl(), new C2803c(2, downloadTask, false, fVar));
        }
    }

    public synchronized void onReceive(final Context context, final Intent intent) {
        C2901am.m6848a().mo34547a(new Runnable() {
            /* class com.tencent.bugly.beta.download.BetaReceiver.C28001 */

            public void run() {
                try {
                    if (intent.getAction().equals(BetaReceiver.CONNECTIVITY_CHANGE)) {
                        int i = BetaReceiver.netStatus;
                        BetaReceiver.netStatus = C2804a.m6256a(context);
                        for (C2803c cVar : BetaReceiver.netListeners.values()) {
                            cVar.mo34043a(i, BetaReceiver.netStatus);
                        }
                    } else if (intent.getAction().equals(C2821c.f5418a.f5420c)) {
                        int intExtra = intent.getIntExtra(SocialConstants.TYPE_REQUEST, -1);
                        if (intExtra == 1) {
                            DownloadTask downloadTask = C2821c.f5418a.f5419b;
                            if (downloadTask != null) {
                                int status = downloadTask.getStatus();
                                if (status != 0) {
                                    if (status == 1) {
                                        C2804a.m6266a(C2808e.f5336E.f5385s, downloadTask.getSaveFile(), downloadTask.getMD5());
                                    } else if (status == 2) {
                                        BetaReceiver.netListeners.remove(downloadTask.getDownloadUrl());
                                        downloadTask.stop();
                                    } else if (!(status == 3 || status == 4 || status == 5)) {
                                    }
                                    if (C2829c.f5467a.f5471e == null && C2826h.f5440v != null) {
                                        C2826h.f5440v.mo34163a(downloadTask);
                                        return;
                                    }
                                }
                                BetaReceiver.addTask(downloadTask);
                                downloadTask.download();
                                if (C2829c.f5467a.f5471e == null) {
                                }
                            }
                        } else if (intExtra != 2) {
                            Log.v("", "do nothing");
                        } else {
                            C2825g.m6299a(C2821c.f5418a.f5422e, true, true, 0);
                        }
                    }
                } catch (Exception e) {
                    if (!C2903an.m6861b(e)) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
