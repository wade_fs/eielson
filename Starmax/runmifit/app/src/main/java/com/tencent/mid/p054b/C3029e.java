package com.tencent.mid.p054b;

import android.content.Context;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.C3041g;
import com.tencent.mid.util.Util;

/* renamed from: com.tencent.mid.b.e */
public class C3029e extends C3030f {

    /* renamed from: a */
    protected static C3038d f6552a = Util.getLogger();

    /* renamed from: a */
    public int mo34886a() {
        return 1;
    }

    public C3029e(Context context, int i) {
        super(context, i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo34889b() {
        return Util.checkPermission(this.f6554c, "android.permission.WRITE_SETTINGS");
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public String mo34890c() {
        String a;
        synchronized (this) {
            f6552a.mo34950b("read mid from Settings.System");
            a = C3041g.m7509a(this.f6554c).mo34957a(mo34901h());
        }
        return a;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34888a(String str) {
        synchronized (this) {
            f6552a.mo34950b("write mid to Settings.System");
            C3041g.m7509a(this.f6554c).mo34959a(mo34901h(), str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C3025a mo34891d() {
        C3025a aVar;
        synchronized (this) {
            aVar = new C3025a(C3041g.m7509a(this.f6554c).mo34957a(mo34900g()));
            C3038d dVar = f6552a;
            dVar.mo34950b("read readCheckEntity from Settings.System:" + aVar.toString());
        }
        return aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo34887a(C3025a aVar) {
        synchronized (this) {
            C3038d dVar = f6552a;
            dVar.mo34950b("write CheckEntity to Settings.System:" + aVar.toString());
            C3041g.m7509a(this.f6554c).mo34959a(mo34900g(), aVar.toString());
        }
    }
}
