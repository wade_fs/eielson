package com.tencent.tauth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import com.tencent.connect.auth.C2983c;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.avatar.QQAvatar;
import com.tencent.connect.common.Constants;
import com.tencent.connect.common.UIListenerManager;
import com.tencent.connect.emotion.QQEmotion;
import com.tencent.connect.share.QQShare;
import com.tencent.connect.share.QzonePublish;
import com.tencent.connect.share.QzoneShare;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3122f;
import com.tencent.open.utils.C3125h;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import java.io.IOException;
import java.util.ArrayList;
import org.json.JSONException;
import org.json.JSONObject;

/* compiled from: ProGuard */
public class Tencent {
    public static final int REQUEST_LOGIN = 10001;

    /* renamed from: b */
    private static Tencent f7433b;

    /* renamed from: a */
    private final C2983c f7434a;

    public void releaseResource() {
    }

    private Tencent(String str, Context context) {
        this.f7434a = C2983c.m7234a(str, context);
    }

    public static synchronized Tencent createInstance(String str, Context context) {
        synchronized (Tencent.class) {
            C3121e.m7728a(context.getApplicationContext());
            C3082f.m7634c("openSDK_LOG.Tencent", "createInstance()  -- start, appId = " + str);
            if (f7433b == null) {
                f7433b = new Tencent(str, context);
            } else if (!str.equals(f7433b.getAppId())) {
                f7433b.logout(context);
                f7433b = new Tencent(str, context);
            }
            if (!m8137a(context, str)) {
                return null;
            }
            C3122f.m7733a(context, str);
            C3082f.m7634c("openSDK_LOG.Tencent", "createInstance()  -- end");
            Tencent tencent = f7433b;
            return tencent;
        }
    }

    /* renamed from: a */
    private static boolean m8137a(Context context, String str) {
        try {
            context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity"), 0);
            try {
                context.getPackageManager().getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.connect.common.AssistActivity"), 0);
                return true;
            } catch (PackageManager.NameNotFoundException unused) {
                C3082f.m7636e("openSDK_LOG.Tencent", "AndroidManifest.xml 没有检测到com.tencent.connect.common.AssistActivity\n" + ("没有在AndroidManifest.xml中检测到com.tencent.connect.common.AssistActivity,请加上com.tencent.connect.common.AssistActivity,详细信息请查看官网文档." + "\n配置示例如下: \n<activity\n     android:name=\"com.tencent.connect.common.AssistActivity\"\n     android:screenOrientation=\"behind\"\n     android:theme=\"@android:style/Theme.Translucent.NoTitleBar\"\n     android:configChanges=\"orientation|keyboardHidden\">\n</activity>"));
                return false;
            }
        } catch (PackageManager.NameNotFoundException unused2) {
            C3082f.m7636e("openSDK_LOG.Tencent", "AndroidManifest.xml 没有检测到com.tencent.tauth.AuthActivity" + (("没有在AndroidManifest.xml中检测到com.tencent.tauth.AuthActivity,请加上com.tencent.tauth.AuthActivity,并配置<data android:scheme=\"tencent" + str + "\" />,详细信息请查看官网文档.") + "\n配置示例如下: \n<activity\n     android:name=\"com.tencent.tauth.AuthActivity\"\n     android:noHistory=\"true\"\n     android:launchMode=\"singleTask\">\n<intent-filter>\n    <action android:name=\"android.intent.action.VIEW\" />\n    <category android:name=\"android.intent.category.DEFAULT\" />\n    <category android:name=\"android.intent.category.BROWSABLE\" />\n    <data android:scheme=\"tencent" + str + "\" />\n" + "</intent-filter>\n" + "</activity>"));
            return false;
        }
    }

    public int login(Activity activity, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "login() with activity, scope is " + str);
        return this.f7434a.mo34751a(activity, str, iUiListener);
    }

    public int login(Activity activity, String str, IUiListener iUiListener, boolean z) {
        C3082f.m7634c("openSDK_LOG.Tencent", "login() with activity, scope is " + str);
        return this.f7434a.mo34753a(activity, str, iUiListener, z);
    }

    public int login(Fragment fragment, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "login() with fragment, scope is " + str);
        return this.f7434a.mo34755a(fragment, str, iUiListener, "");
    }

    public int login(Fragment fragment, String str, IUiListener iUiListener, boolean z) {
        C3082f.m7634c("openSDK_LOG.Tencent", "login() with fragment, scope is " + str);
        return this.f7434a.mo34756a(fragment, str, iUiListener, "", z);
    }

    public int loginServerSide(Activity activity, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "loginServerSide() with activity, scope = " + str + ",server_side");
        C2983c cVar = this.f7434a;
        return cVar.mo34751a(activity, str + ",server_side", iUiListener);
    }

    public int loginServerSide(Fragment fragment, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "loginServerSide() with fragment, scope = " + str + ",server_side");
        C2983c cVar = this.f7434a;
        return cVar.mo34755a(fragment, str + ",server_side", iUiListener, "");
    }

    public int loginWithOEM(Activity activity, String str, IUiListener iUiListener, boolean z, String str2, String str3, String str4) {
        C3082f.m7634c("openSDK_LOG.Tencent", "loginWithOEM() with activity, scope = " + str);
        return this.f7434a.mo34754a(activity, str, iUiListener, z, str2, str3, str4);
    }

    public void logout(Context context) {
        C3082f.m7634c("openSDK_LOG.Tencent", "logout()");
        this.f7434a.mo34761b().setAccessToken(null, "0");
        this.f7434a.mo34761b().setOpenId(null);
        this.f7434a.mo34761b().removeSession(this.f7434a.mo34761b().getAppId());
    }

    public int reAuth(Activity activity, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "reAuth() with activity, scope = " + str);
        return this.f7434a.mo34760b(activity, str, iUiListener);
    }

    public void reportDAU() {
        C3082f.m7634c("openSDK_LOG.Tencent", "reportDAU() ");
        this.f7434a.mo34757a();
    }

    public void checkLogin(IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "checkLogin()");
        this.f7434a.mo34758a(iUiListener);
    }

    public void requestAsync(String str, Bundle bundle, String str2, IRequestListener iRequestListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "requestAsync()");
        HttpUtils.requestAsync(this.f7434a.mo34761b(), C3121e.m7727a(), str, bundle, str2, iRequestListener);
    }

    public JSONObject request(String str, Bundle bundle, String str2) throws IOException, JSONException, HttpUtils.NetworkUnavailableException, HttpUtils.HttpStatusException {
        C3082f.m7634c("openSDK_LOG.Tencent", "request()");
        return HttpUtils.request(this.f7434a.mo34761b(), C3121e.m7727a(), str, bundle, str2);
    }

    public void shareToQQ(Activity activity, Bundle bundle, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "shareToQQ()");
        new QQShare(activity, this.f7434a.mo34761b()).shareToQQ(activity, bundle, iUiListener);
    }

    public void shareToQzone(Activity activity, Bundle bundle, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "shareToQzone()");
        new QzoneShare(activity, this.f7434a.mo34761b()).shareToQzone(activity, bundle, iUiListener);
    }

    public void publishToQzone(Activity activity, Bundle bundle, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "publishToQzone()");
        new QzonePublish(activity, this.f7434a.mo34761b()).publishToQzone(activity, bundle, iUiListener);
    }

    public void setAvatar(Activity activity, Bundle bundle, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "setAvatar()");
        String string = bundle.getString("picture");
        new QQAvatar(this.f7434a.mo34761b()).setAvatar(activity, Uri.parse(string), iUiListener, bundle.getInt("exitAnim"));
    }

    public void setAvatar(Activity activity, Bundle bundle, IUiListener iUiListener, int i, int i2) {
        C3082f.m7634c("openSDK_LOG.Tencent", "setAvatar()");
        bundle.putInt("exitAnim", i2);
        activity.overridePendingTransition(i, 0);
        setAvatar(activity, bundle, iUiListener);
    }

    public void setAvatarByQQ(Activity activity, Uri uri, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "setAvatarByQQ()");
        new QQAvatar(this.f7434a.mo34761b()).setAvatarByQQ(activity, uri, iUiListener);
    }

    public void setDynamicAvatar(Activity activity, Uri uri, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "setDynamicAvatar()");
        new QQAvatar(this.f7434a.mo34761b()).setDynamicAvatar(activity, uri, iUiListener);
    }

    public void setEmotions(Activity activity, ArrayList<Uri> arrayList, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.Tencent", "saveQQEmotions()");
        new QQEmotion(this.f7434a.mo34761b()).setEmotions(activity, arrayList, iUiListener);
    }

    public boolean onActivityResult(int i, int i2, Intent intent) {
        C3082f.m7634c("openSDK_LOG.Tencent", "onActivityResult() deprecated, will do nothing");
        return false;
    }

    public static boolean onActivityResultData(int i, int i2, Intent intent, IUiListener iUiListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("onActivityResultData() reqcode = ");
        sb.append(i);
        sb.append(", resultcode = ");
        sb.append(i2);
        sb.append(", data = null ? ");
        boolean z = true;
        sb.append(intent == null);
        sb.append(", listener = null ? ");
        if (iUiListener != null) {
            z = false;
        }
        sb.append(z);
        C3082f.m7634c("openSDK_LOG.Tencent", sb.toString());
        return UIListenerManager.getInstance().onActivityResult(i, i2, intent, iUiListener);
    }

    public boolean isSessionValid() {
        boolean c = this.f7434a.mo34763c();
        C3082f.m7634c("openSDK_LOG.Tencent", "isSessionValid() isvalid =" + c);
        return c;
    }

    public String getAppId() {
        String appId = this.f7434a.mo34761b().getAppId();
        C3082f.m7634c("openSDK_LOG.Tencent", "getAppId() appid =" + appId);
        return appId;
    }

    public String getAccessToken() {
        String accessToken = this.f7434a.mo34761b().getAccessToken();
        C3082f.m7634c("openSDK_LOG.Tencent", "getAccessToken() accessToken = " + accessToken);
        return accessToken;
    }

    public long getExpiresIn() {
        long expireTimeInSecond = this.f7434a.mo34761b().getExpireTimeInSecond();
        C3082f.m7634c("openSDK_LOG.Tencent", "getExpiresIn() expiresin= " + expireTimeInSecond);
        return expireTimeInSecond;
    }

    public String getOpenId() {
        String openId = this.f7434a.mo34761b().getOpenId();
        C3082f.m7634c("openSDK_LOG.Tencent", "getOpenId() openid= " + openId);
        return openId;
    }

    @Deprecated
    public void handleLoginData(Intent intent, IUiListener iUiListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("handleLoginData() data = null ? ");
        boolean z = true;
        sb.append(intent == null);
        sb.append(", listener = null ? ");
        if (iUiListener != null) {
            z = false;
        }
        sb.append(z);
        C3082f.m7634c("openSDK_LOG.Tencent", sb.toString());
        UIListenerManager.getInstance().handleDataToListener(intent, iUiListener);
    }

    public static void handleResultData(Intent intent, IUiListener iUiListener) {
        StringBuilder sb = new StringBuilder();
        sb.append("handleResultData() data = null ? ");
        boolean z = true;
        sb.append(intent == null);
        sb.append(", listener = null ? ");
        if (iUiListener != null) {
            z = false;
        }
        sb.append(z);
        C3082f.m7634c("openSDK_LOG.Tencent", sb.toString());
        UIListenerManager.getInstance().handleDataToListener(intent, iUiListener);
    }

    public void setAccessToken(String str, String str2) {
        C3082f.m7634c("openSDK_LOG.Tencent", "setAccessToken(), expiresIn = " + str2 + "");
        this.f7434a.mo34759a(str, str2);
    }

    public void setOpenId(String str) {
        C3082f.m7634c("openSDK_LOG.Tencent", "setOpenId() --start");
        this.f7434a.mo34762b(C3121e.m7727a(), str);
        C3082f.m7634c("openSDK_LOG.Tencent", "setOpenId() --end");
    }

    public boolean isReady() {
        boolean z = isSessionValid() && getOpenId() != null;
        C3082f.m7634c("openSDK_LOG.Tencent", "isReady() --ready=" + z);
        return z;
    }

    public QQToken getQQToken() {
        C3082f.m7634c("openSDK_LOG.Tencent", "getQQToken()");
        return this.f7434a.mo34761b();
    }

    public boolean isSupportSSOLogin(Activity activity) {
        C3082f.m7634c("openSDK_LOG.Tencent", "isSupportSSOLogin()");
        boolean z = true;
        if (C3131k.m7789d(activity) && C3125h.m7751a(activity, Constants.PACKAGE_QQ_PAD) != null) {
            return true;
        }
        if (C3125h.m7757c(activity, "4.1") < 0 && C3125h.m7758d(activity, "1.1") < 0) {
            z = false;
        }
        C3082f.m7634c("openSDK_LOG.Tencent", "isSupportSSOLogin() support=" + z);
        return z;
    }

    public static boolean isSupportShareToQQ(Context context) {
        C3082f.m7634c("openSDK_LOG.Tencent", "isSupportShareToQQ()");
        boolean z = true;
        if (C3131k.m7789d(context) && C3125h.m7751a(context, Constants.PACKAGE_QQ_PAD) != null) {
            return true;
        }
        if (C3125h.m7757c(context, "4.1") < 0 && C3125h.m7751a(context, Constants.PACKAGE_TIM) == null && C3125h.m7751a(context, Constants.PACKAGE_QQ_SPEED) == null) {
            z = false;
        }
        C3082f.m7634c("openSDK_LOG.Tencent", "isSupportShareToQQ() support=" + z);
        return z;
    }

    public static boolean isSupportPushToQZone(Context context) {
        boolean z = (C3125h.m7757c(context, "5.9.5") < 0 && C3125h.m7751a(context, Constants.PACKAGE_TIM) == null && C3125h.m7751a(context, Constants.PACKAGE_QQ_SPEED) == null) ? false : true;
        C3082f.m7634c("openSDK_LOG.Tencent", "isSupportPushToQZone() support=" + z);
        return z;
    }

    public boolean isQQInstalled(Context context) {
        boolean b = C3125h.m7756b(context);
        C3082f.m7634c("openSDK_LOG.Tencent", "isQQInstalled() installed=" + b);
        return b;
    }

    public void saveSession(JSONObject jSONObject) {
        StringBuilder sb = new StringBuilder();
        sb.append("saveSession() length=");
        sb.append(jSONObject != null ? jSONObject.length() : 0);
        C3082f.m7634c("openSDK_LOG.Tencent", sb.toString());
        this.f7434a.mo34761b().saveSession(jSONObject);
    }

    public boolean checkSessionValid(String str) {
        JSONObject loadSession = this.f7434a.mo34761b().loadSession(str);
        if (!(loadSession == null || loadSession.length() == 0)) {
            try {
                String string = loadSession.getString(Constants.PARAM_ACCESS_TOKEN);
                String string2 = loadSession.getString(Constants.PARAM_EXPIRES_IN);
                String string3 = loadSession.getString("openid");
                String string4 = loadSession.getString(Constants.PARAM_EXPIRES_TIME);
                if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3) && !TextUtils.isEmpty(string4)) {
                    if (System.currentTimeMillis() < Long.parseLong(string4)) {
                        C3082f.m7634c("openSDK_LOG.Tencent", "checkSessionValid() true");
                        return true;
                    }
                }
            } catch (Exception e) {
                C3082f.m7634c("QQToken", "checkSessionValid " + e.toString());
                return false;
            }
        }
        C3082f.m7634c("openSDK_LOG.Tencent", "checkSessionValid() false");
        return false;
    }

    public JSONObject loadSession(String str) {
        JSONObject loadSession = this.f7434a.mo34761b().loadSession(str);
        StringBuilder sb = new StringBuilder();
        sb.append("loadSession() appid ");
        sb.append(str);
        sb.append(", length=");
        sb.append(loadSession != null ? loadSession.length() : 0);
        C3082f.m7634c("openSDK_LOG.Tencent", sb.toString());
        return loadSession;
    }

    public void initSessionCache(JSONObject jSONObject) {
        try {
            String string = jSONObject.getString(Constants.PARAM_ACCESS_TOKEN);
            String string2 = jSONObject.getString(Constants.PARAM_EXPIRES_IN);
            String string3 = jSONObject.getString("openid");
            if (!TextUtils.isEmpty(string) && !TextUtils.isEmpty(string2) && !TextUtils.isEmpty(string3)) {
                setAccessToken(string, string2);
                setOpenId(string3);
            }
            C3082f.m7634c("openSDK_LOG.Tencent", "initSessionCache()");
        } catch (Exception e) {
            C3082f.m7634c("QQToken", "initSessionCache " + e.toString());
        }
    }
}
