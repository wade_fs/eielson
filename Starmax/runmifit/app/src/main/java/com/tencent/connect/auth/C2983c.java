package com.tencent.connect.auth;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.widget.Toast;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.connect.p052a.ProGuard;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.utils.C3113a;
import com.tencent.open.utils.C3121e;
import com.tencent.tauth.IUiListener;
import java.io.File;
import java.util.Iterator;

/* renamed from: com.tencent.connect.auth.c */
/* compiled from: ProGuard */
public class C2983c {

    /* renamed from: a */
    private AuthAgent f6392a = new AuthAgent(this.f6393b);

    /* renamed from: b */
    private QQToken f6393b;

    private C2983c(String str, Context context) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "new QQAuth() --start");
        this.f6393b = new QQToken(str);
        ProGuard.m7170c(context, this.f6393b);
        m7235a(context, Constants.SDK_VERSION);
        C3082f.m7634c("openSDK_LOG.QQAuth", "new QQAuth() --end");
    }

    /* renamed from: a */
    public static void m7235a(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("BuglySdkInfos", 0).edit();
        edit.putString("bcb3903995", str);
        edit.apply();
    }

    /* renamed from: a */
    public static C2983c m7234a(String str, Context context) {
        C3121e.m7728a(context.getApplicationContext());
        C3082f.m7634c("openSDK_LOG.QQAuth", "QQAuth -- createInstance() --start");
        try {
            PackageManager packageManager = context.getPackageManager();
            packageManager.getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.tauth.AuthActivity"), 0);
            packageManager.getActivityInfo(new ComponentName(context.getPackageName(), "com.tencent.connect.common.AssistActivity"), 0);
            C2983c cVar = new C2983c(str, context);
            C3082f.m7634c("openSDK_LOG.QQAuth", "QQAuth -- createInstance()  --end");
            return cVar;
        } catch (PackageManager.NameNotFoundException e) {
            C3082f.m7632b("openSDK_LOG.QQAuth", "createInstance() error --end", e);
            Toast.makeText(context.getApplicationContext(), "请参照文档在Androidmanifest.xml加上AuthActivity和AssitActivity的定义 ", 1).show();
            return null;
        }
    }

    /* renamed from: a */
    public int mo34751a(Activity activity, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "login()");
        return mo34752a(activity, str, iUiListener, "");
    }

    /* renamed from: a */
    public int mo34753a(Activity activity, String str, IUiListener iUiListener, boolean z) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "login()");
        return m7233a(activity, null, str, iUiListener, "", z);
    }

    /* renamed from: a */
    public int mo34752a(Activity activity, String str, IUiListener iUiListener, String str2) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "-->login activity: " + activity);
        return m7232a(activity, (Fragment) null, str, iUiListener, str2);
    }

    /* renamed from: a */
    public int mo34755a(Fragment fragment, String str, IUiListener iUiListener, String str2) {
        FragmentActivity activity = fragment.getActivity();
        C3082f.m7634c("openSDK_LOG.QQAuth", "-->login activity: " + activity);
        return m7232a(activity, fragment, str, iUiListener, str2);
    }

    /* renamed from: a */
    public int mo34756a(Fragment fragment, String str, IUiListener iUiListener, String str2, boolean z) {
        FragmentActivity activity = fragment.getActivity();
        C3082f.m7634c("openSDK_LOG.QQAuth", "-->login activity: " + activity);
        return m7233a(activity, fragment, str, iUiListener, str2, z);
    }

    /* renamed from: a */
    private int m7232a(Activity activity, Fragment fragment, String str, IUiListener iUiListener, String str2) {
        return m7233a(activity, fragment, str, iUiListener, str2, false);
    }

    /* renamed from: a */
    private int m7233a(Activity activity, Fragment fragment, String str, IUiListener iUiListener, String str2, boolean z) {
        String packageName = activity.getApplicationContext().getPackageName();
        String str3 = null;
        try {
            Iterator<ApplicationInfo> it = activity.getPackageManager().getInstalledApplications(128).iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                ApplicationInfo next = it.next();
                if (packageName.equals(next.packageName)) {
                    str3 = next.sourceDir;
                    break;
                }
            }
            if (str3 != null) {
                String a = C3113a.m7711a(new File(str3));
                if (!TextUtils.isEmpty(a)) {
                    C3082f.m7628a("openSDK_LOG.QQAuth", "-->login channelId: " + a);
                    return mo34754a(activity, str, iUiListener, z, a, a, "");
                }
            }
        } catch (Throwable th) {
            C3082f.m7632b("openSDK_LOG.QQAuth", "-->login get channel id exception.", th);
            th.printStackTrace();
        }
        C3082f.m7631b("openSDK_LOG.QQAuth", "-->login channelId is null ");
        BaseApi.isOEM = false;
        return this.f6392a.doLogin(activity, str, iUiListener, false, fragment, z);
    }

    @Deprecated
    /* renamed from: a */
    public int mo34754a(Activity activity, String str, IUiListener iUiListener, boolean z, String str2, String str3, String str4) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "loginWithOEM");
        BaseApi.isOEM = true;
        if (str2.equals("")) {
            str2 = "null";
        }
        if (str3.equals("")) {
            str3 = "null";
        }
        if (str4.equals("")) {
            str4 = "null";
        }
        BaseApi.installChannel = str3;
        BaseApi.registerChannel = str2;
        BaseApi.businessId = str4;
        return this.f6392a.doLogin(activity, str, iUiListener, false, null, z);
    }

    /* renamed from: b */
    public int mo34760b(Activity activity, String str, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "reAuth()");
        return this.f6392a.doLogin(activity, str, iUiListener, true, null);
    }

    /* renamed from: a */
    public void mo34757a() {
        this.f6392a.mo34706a((IUiListener) null);
    }

    /* renamed from: a */
    public void mo34758a(IUiListener iUiListener) {
        this.f6392a.mo34707b(iUiListener);
    }

    /* renamed from: b */
    public QQToken mo34761b() {
        return this.f6393b;
    }

    /* renamed from: a */
    public void mo34759a(String str, String str2) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "setAccessToken(), validTimeInSecond = " + str2 + "");
        this.f6393b.setAccessToken(str, str2);
    }

    /* renamed from: c */
    public boolean mo34763c() {
        StringBuilder sb = new StringBuilder();
        sb.append("isSessionValid(), result = ");
        sb.append(this.f6393b.isSessionValid() ? "true" : "false");
        C3082f.m7634c("openSDK_LOG.QQAuth", sb.toString());
        return this.f6393b.isSessionValid();
    }

    /* renamed from: b */
    public void mo34762b(Context context, String str) {
        C3082f.m7634c("openSDK_LOG.QQAuth", "setOpenId() --start");
        this.f6393b.setOpenId(str);
        ProGuard.m7171d(context, this.f6393b);
        C3082f.m7634c("openSDK_LOG.QQAuth", "setOpenId() --end");
    }
}
