package com.tencent.stat;

import android.content.Context;
import android.os.Build;
import android.support.v4.media.session.PlaybackStateCompat;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.StatPreferences;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashSet;
import org.json.JSONArray;

public class StatNativeCrashReport {
    public static final String PRE_TAG_TOMBSTONE_FNAME = "tombstone_";
    public static final String SO_FULL_NAME = "libMtaNativeCrash_v2.so";
    public static final String SO_LOAD_NAME = "MtaNativeCrash_v2";

    /* renamed from: a */
    static StatNativeCrashReport f6988a = new StatNativeCrashReport();

    /* renamed from: b */
    private static StatLogger f6989b = StatCommonHelper.getLogger();

    /* renamed from: d */
    private static JSONArray f6990d;

    /* renamed from: e */
    private static Thread f6991e = null;

    /* renamed from: f */
    private static boolean f6992f;

    /* renamed from: g */
    private static boolean f6993g = false;

    /* renamed from: h */
    private static String f6994h = null;

    /* renamed from: i */
    private static boolean f6995i;

    /* renamed from: c */
    private volatile boolean f6996c = false;

    public native void enableNativeCrash(boolean z);

    public native void enableNativeCrashDebug(boolean z);

    public native boolean initJNICrash(String str, int i);

    public native String makeJniCrash();

    public native String stringFromJNI();

    static {
        f6992f = false;
        f6995i = false;
        try {
            System.loadLibrary(SO_LOAD_NAME);
            f6995i = true;
        } catch (Throwable unused) {
            f6992f = false;
            f6989b.mo35396w("can't find libMtaNativeCrash_v2.so, NativeCrash report disable.");
        }
    }

    /* renamed from: a */
    static JSONArray m7847a() {
        JSONArray jSONArray = new JSONArray();
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace != null) {
            boolean z = false;
            for (StackTraceElement stackTraceElement : stackTrace) {
                if (stackTraceElement.getClassName().equals(StatNativeCrashReport.class.getName())) {
                    z = true;
                } else if (z) {
                    jSONArray.put(stackTraceElement.toString());
                }
            }
        }
        return jSONArray;
    }

    /* renamed from: b */
    private static void m7848b() {
        f6990d = m7847a();
        f6991e = Thread.currentThread();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.stat.StatServiceImpl.a(android.content.Context, com.tencent.stat.StatSpecifyReportedInfo, boolean):void
     arg types: [android.content.Context, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.stat.StatServiceImpl.a(android.content.Context, com.tencent.stat.StatAccount, com.tencent.stat.StatSpecifyReportedInfo):void
      com.tencent.stat.StatServiceImpl.a(android.content.Context, java.lang.String, com.tencent.stat.StatSpecifyReportedInfo):void
      com.tencent.stat.StatServiceImpl.a(org.json.JSONArray, java.lang.Thread, java.lang.String):void
      com.tencent.stat.StatServiceImpl.a(android.content.Context, com.tencent.stat.StatSpecifyReportedInfo, boolean):void */
    public static void createNativeCrashEvent(String str) {
        StatLogger statLogger = f6989b;
        statLogger.mo35381d("createNativeCrashEvent:" + str);
        StatServiceImpl.m7856a(StatServiceImpl.getContext(null), (StatSpecifyReportedInfo) null, true);
        m7848b();
        StatServiceImpl.m7862a(f6990d, f6991e, str);
    }

    public static void doNativeCrashTest() {
        if (!f6995i) {
            f6989b.mo35396w("libMtaNativeCrash_v2.so not loaded.");
        } else {
            f6988a.makeJniCrash();
        }
    }

    public static void setNativeCrashEnable(boolean z) {
        if (!f6995i) {
            f6989b.mo35396w("libMtaNativeCrash_v2.so not loaded.");
            return;
        }
        try {
            f6988a.enableNativeCrash(z);
            f6992f = z;
        } catch (Throwable th) {
            f6989b.mo35396w(th);
        }
    }

    public static boolean isNativeCrashEnable() {
        return f6992f;
    }

    public static void setNativeCrashDebugEnable(boolean z) {
        if (!f6995i) {
            f6989b.mo35396w("libMtaNativeCrash_v2.so not loaded.");
            return;
        }
        try {
            f6988a.enableNativeCrashDebug(z);
            f6993g = z;
        } catch (Throwable th) {
            f6989b.mo35396w(th);
        }
    }

    public static boolean isNativeCrashDebugEnable() {
        return f6993g;
    }

    public static void initNativeCrash(Context context, String str) {
        if (!f6995i) {
            f6989b.mo35396w("libMtaNativeCrash_v2.so not loaded.");
        } else if (!f6988a.f6996c) {
            if (str == null) {
                try {
                    str = context.getDir("tombstones", 0).getAbsolutePath();
                } catch (Throwable th) {
                    f6989b.mo35396w(th);
                    return;
                }
            }
            f6994h = str;
            StatPreferences.putString(context, "__mta_tombstone__", str);
            setNativeCrashEnable(true);
            f6988a.initJNICrash(str, Build.VERSION.SDK_INT);
            f6988a.f6996c = true;
            if (StatConfig.isDebugEnable()) {
                f6989b.mo35381d("initNativeCrash success.");
            }
        }
    }

    public static String getTombstonesDir(Context context) {
        if (f6994h == null) {
            f6994h = StatPreferences.getString(context, "__mta_tombstone__", "");
        }
        return f6994h;
    }

    public static String readFile(File file) {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
                sb.append(10);
            }
            bufferedReader.close();
        } catch (IOException e) {
            f6989b.mo35384e((Throwable) e);
        }
        return sb.toString();
    }

    public static LinkedHashSet<File> getCrashEventsFilesList(Context context) {
        File[] listFiles;
        LinkedHashSet<File> linkedHashSet = new LinkedHashSet<>();
        String absolutePath = context.getDir("mtajcrash", 0).getAbsolutePath();
        if (absolutePath != null) {
            File file = new File(absolutePath);
            if (file.isDirectory() && (listFiles = file.listFiles()) != null) {
                for (File file2 : listFiles) {
                    if (file2.exists() && file2.length() > PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
                        try {
                            file2.delete();
                        } catch (Throwable unused) {
                        }
                    } else if (file2.getName().endsWith(".v1.crash") && file2.isFile()) {
                        if (StatConfig.isDebugEnable()) {
                            f6989b.mo35381d("get tombstone file:" + file2.getAbsolutePath().toString());
                        }
                        linkedHashSet.add(file2.getAbsoluteFile());
                    }
                }
            }
        }
        return linkedHashSet;
    }
}
