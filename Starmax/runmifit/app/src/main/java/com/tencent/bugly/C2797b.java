package com.tencent.bugly;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.bugly.crashreport.biz.C2847b;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.proguard.C2885ac;
import com.tencent.bugly.proguard.C2888ad;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2896ak;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2905ap;
import com.tencent.bugly.proguard.C2908aq;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.b */
/* compiled from: BUGLY */
public class C2797b {

    /* renamed from: a */
    public static boolean f5301a = true;

    /* renamed from: b */
    public static List<BUGLY> f5302b = new ArrayList();

    /* renamed from: c */
    public static boolean f5303c;

    /* renamed from: d */
    private static C2889ae f5304d;

    /* renamed from: e */
    private static boolean f5305e;

    /* renamed from: a */
    private static boolean m6249a(C2851a aVar) {
        List<String> list = aVar.f5676u;
        aVar.getClass();
        return list != null && list.contains("bugly");
    }

    /* renamed from: a */
    public static synchronized void m6244a(Context context) {
        synchronized (C2797b.class) {
            m6245a(context, (BuglyStrategy) null);
        }
    }

    /* renamed from: a */
    public static synchronized void m6245a(Context context, BuglyStrategy buglyStrategy) {
        synchronized (C2797b.class) {
            if (f5305e) {
                C2903an.m6864d("[init] initial Multi-times, ignore this.", new Object[0]);
            } else if (context == null) {
                Log.w(C2903an.f6030b, "[init] context of init() is null, check it.");
            } else {
                C2851a a = C2851a.m6470a(context);
                if (m6249a(a)) {
                    f5301a = false;
                    return;
                }
                String f = a.mo34307f();
                if (f == null) {
                    Log.e(C2903an.f6030b, "[init] meta data of BUGLY_APPID in AndroidManifest.xml should be set.");
                } else {
                    m6246a(context, f, a.f5599A, buglyStrategy);
                }
            }
        }
    }

    /* renamed from: a */
    public static synchronized void m6246a(Context context, String str, boolean z, BuglyStrategy buglyStrategy) {
        synchronized (C2797b.class) {
            if (f5305e) {
                C2903an.m6864d("[init] initial Multi-times, ignore this.", new Object[0]);
            } else if (context == null) {
                Log.w(C2903an.f6030b, "[init] context is null, check it.");
            } else if (str == null) {
                Log.e(C2903an.f6030b, "init arg 'crashReportAppID' should not be null!");
            } else {
                f5305e = true;
                if (z) {
                    f5303c = true;
                    C2903an.f6031c = true;
                    C2903an.m6864d("Bugly debug模式开启，请在发布时把isDebug关闭。 -- Running in debug model for 'isDebug' is enabled. Please disable it when you release.", new Object[0]);
                    C2903an.m6865e("--------------------------------------------------------------------------------------------", new Object[0]);
                    C2903an.m6864d("Bugly debug模式将有以下行为特性 -- The following list shows the behaviour of debug model: ", new Object[0]);
                    C2903an.m6864d("[1] 输出详细的Bugly SDK的Log -- More detailed log of Bugly SDK will be output to logcat;", new Object[0]);
                    C2903an.m6864d("[2] 每一条Crash都会被立即上报 -- Every crash caught by Bugly will be uploaded immediately.", new Object[0]);
                    C2903an.m6864d("[3] 自定义日志将会在Logcat中输出 -- Custom log will be output to logcat.", new Object[0]);
                    C2903an.m6865e("--------------------------------------------------------------------------------------------", new Object[0]);
                    C2903an.m6860b("[init] Open debug mode of Bugly.", new Object[0]);
                }
                C2903an.m6857a("[init] Bugly version: v%s", "2.6.5");
                C2903an.m6857a(" crash report start initializing...", new Object[0]);
                C2903an.m6860b("[init] Bugly start initializing...", new Object[0]);
                C2903an.m6857a("[init] Bugly complete version: v%s", "2.6.5(1.4.0)");
                Context a = C2908aq.m6891a(context);
                C2851a a2 = C2851a.m6470a(a);
                a2.mo34323t();
                C2905ap.m6873a(a);
                f5304d = C2889ae.m6758a(a, f5302b);
                C2896ak.m6806a(a);
                C2854a a3 = C2854a.m6575a(a, f5302b);
                C2885ac a4 = C2885ac.m6738a(a);
                if (m6249a(a2)) {
                    f5301a = false;
                    return;
                }
                a2.mo34292a(str);
                C2903an.m6857a("[param] Set APP ID:%s", str);
                m6247a(buglyStrategy, a2);
                C2847b.m6432a(a, buglyStrategy);
                for (int i = 0; i < f5302b.size(); i++) {
                    try {
                        if (a4.mo34484a(f5302b.get(i).f5300id)) {
                            f5302b.get(i).init(a, z, buglyStrategy);
                        }
                    } catch (Throwable th) {
                        if (!C2903an.m6858a(th)) {
                            th.printStackTrace();
                        }
                    }
                }
                a3.mo34336a(buglyStrategy != null ? buglyStrategy.getAppReportDelay() : 0);
                C2903an.m6860b("[init] Bugly initialization finished.", new Object[0]);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
     arg types: [int, java.lang.String, byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]>
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, com.tencent.bugly.proguard.ad):java.util.Map
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]> */
    /* renamed from: a */
    private static void m6247a(BuglyStrategy buglyStrategy, C2851a aVar) {
        byte[] bArr;
        if (buglyStrategy != null) {
            String appVersion = buglyStrategy.getAppVersion();
            if (!TextUtils.isEmpty(appVersion)) {
                if (appVersion.length() > 100) {
                    String substring = appVersion.substring(0, 100);
                    C2903an.m6864d("appVersion %s length is over limit %d substring to %s", appVersion, 100, substring);
                    appVersion = substring;
                }
                aVar.f5671p = appVersion;
                C2903an.m6857a("[param] Set App version: %s", buglyStrategy.getAppVersion());
            }
            try {
                if (buglyStrategy.isReplaceOldChannel()) {
                    String appChannel = buglyStrategy.getAppChannel();
                    if (!TextUtils.isEmpty(appChannel)) {
                        if (appChannel.length() > 100) {
                            String substring2 = appChannel.substring(0, 100);
                            C2903an.m6864d("appChannel %s length is over limit %d substring to %s", appChannel, 100, substring2);
                            appChannel = substring2;
                        }
                        f5304d.mo34497a(556, "app_channel", appChannel.getBytes(), (C2888ad) null, false);
                        aVar.f5673r = appChannel;
                    }
                } else {
                    Map<String, byte[]> a = f5304d.mo34494a(556, (C2888ad) null, true);
                    if (!(a == null || (bArr = a.get("app_channel")) == null)) {
                        aVar.f5673r = new String(bArr);
                    }
                }
                C2903an.m6857a("[param] Set App channel: %s", aVar.f5673r);
            } catch (Exception e) {
                if (f5303c) {
                    e.printStackTrace();
                }
            }
            String appPackageName = buglyStrategy.getAppPackageName();
            if (!TextUtils.isEmpty(appPackageName)) {
                if (appPackageName.length() > 100) {
                    String substring3 = appPackageName.substring(0, 100);
                    C2903an.m6864d("appPackageName %s length is over limit %d substring to %s", appPackageName, 100, substring3);
                    appPackageName = substring3;
                }
                aVar.f5659d = appPackageName;
                C2903an.m6857a("[param] Set App package: %s", buglyStrategy.getAppPackageName());
            }
            String deviceID = buglyStrategy.getDeviceID();
            if (deviceID != null) {
                if (deviceID.length() > 100) {
                    String substring4 = deviceID.substring(0, 100);
                    C2903an.m6864d("deviceId %s length is over limit %d substring to %s", deviceID, 100, substring4);
                    deviceID = substring4;
                }
                aVar.mo34301c(deviceID);
                C2903an.m6857a("[param] Set device ID: %s", deviceID);
            }
            aVar.f5663h = buglyStrategy.isUploadProcess();
            C2905ap.f6033a = buglyStrategy.isBuglyLogUpload();
        }
    }

    /* renamed from: a */
    public static synchronized void m6248a(BUGLY aVar) {
        synchronized (C2797b.class) {
            if (!f5302b.contains(aVar)) {
                f5302b.add(aVar);
            }
        }
    }
}
