package com.tencent.p055mm.opensdk.diffdev.p058a;

import android.os.AsyncTask;
import android.util.Log;
import com.tencent.p055mm.opensdk.diffdev.OAuthErrCode;
import com.tencent.p055mm.opensdk.diffdev.OAuthListener;
import org.json.JSONObject;

/* renamed from: com.tencent.mm.opensdk.diffdev.a.f */
final class C3062f extends AsyncTask<Void, Void, C3063a> {

    /* renamed from: n */
    private OAuthListener f6645n;

    /* renamed from: q */
    private String f6646q;
    private String url;

    /* renamed from: w */
    private int f6647w;

    /* renamed from: com.tencent.mm.opensdk.diffdev.a.f$a */
    static class C3063a {

        /* renamed from: p */
        public OAuthErrCode f6648p;

        /* renamed from: x */
        public String f6649x;

        /* renamed from: y */
        public int f6650y;

        C3063a() {
        }

        /* renamed from: b */
        public static C3063a m7554b(byte[] bArr) {
            OAuthErrCode oAuthErrCode;
            String format;
            OAuthErrCode oAuthErrCode2;
            C3063a aVar = new C3063a();
            if (bArr == null || bArr.length == 0) {
                Log.e("MicroMsg.SDK.NoopingResult", "parse fail, buf is null");
                oAuthErrCode = OAuthErrCode.WechatAuth_Err_NetworkErr;
            } else {
                try {
                    try {
                        JSONObject jSONObject = new JSONObject(new String(bArr, "utf-8"));
                        aVar.f6650y = jSONObject.getInt("wx_errcode");
                        Log.d("MicroMsg.SDK.NoopingResult", String.format("nooping uuidStatusCode = %d", Integer.valueOf(aVar.f6650y)));
                        int i = aVar.f6650y;
                        if (i == 408) {
                            oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_OK;
                        } else if (i != 500) {
                            switch (i) {
                                case 402:
                                    oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_Timeout;
                                    break;
                                case 403:
                                    oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_Cancel;
                                    break;
                                case 404:
                                    oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_OK;
                                    break;
                                case 405:
                                    aVar.f6648p = OAuthErrCode.WechatAuth_Err_OK;
                                    aVar.f6649x = jSONObject.getString("wx_code");
                                    break;
                                default:
                                    oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_NormalErr;
                                    break;
                            }
                            return aVar;
                        } else {
                            oAuthErrCode2 = OAuthErrCode.WechatAuth_Err_NormalErr;
                        }
                        aVar.f6648p = oAuthErrCode2;
                        return aVar;
                    } catch (Exception e) {
                        format = String.format("parse json fail, ex = %s", e.getMessage());
                        Log.e("MicroMsg.SDK.NoopingResult", format);
                        oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                        aVar.f6648p = oAuthErrCode;
                        return aVar;
                    }
                } catch (Exception e2) {
                    format = String.format("parse fail, build String fail, ex = %s", e2.getMessage());
                    Log.e("MicroMsg.SDK.NoopingResult", format);
                    oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
                    aVar.f6648p = oAuthErrCode;
                    return aVar;
                }
            }
            aVar.f6648p = oAuthErrCode;
            return aVar;
        }
    }

    public C3062f(String str, OAuthListener oAuthListener) {
        this.f6646q = str;
        this.f6645n = oAuthListener;
        this.url = String.format("https://long.open.weixin.qq.com/connect/l/qrconnect?f=json&uuid=%s", str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        C3063a aVar;
        OAuthErrCode oAuthErrCode;
        String str;
        String str2 = this.f6646q;
        if (str2 == null || str2.length() == 0) {
            Log.e("MicroMsg.SDK.NoopingTask", "run fail, uuid is null");
            aVar = new C3063a();
            oAuthErrCode = OAuthErrCode.WechatAuth_Err_NormalErr;
        } else {
            while (!isCancelled()) {
                StringBuilder sb = new StringBuilder();
                sb.append(this.url);
                if (this.f6647w == 0) {
                    str = "";
                } else {
                    str = "&last=" + this.f6647w;
                }
                sb.append(str);
                String sb2 = sb.toString();
                long currentTimeMillis = System.currentTimeMillis();
                byte[] a = C3061e.m7553a(sb2, 60000);
                long currentTimeMillis2 = System.currentTimeMillis();
                C3063a b = C3063a.m7554b(a);
                Log.d("MicroMsg.SDK.NoopingTask", String.format("nooping, url = %s, errCode = %s, uuidStatusCode = %d, time consumed = %d(ms)", sb2, b.f6648p.toString(), Integer.valueOf(b.f6650y), Long.valueOf(currentTimeMillis2 - currentTimeMillis)));
                if (b.f6648p == OAuthErrCode.WechatAuth_Err_OK) {
                    this.f6647w = b.f6650y;
                    if (b.f6650y == C3064g.UUID_SCANED.getCode()) {
                        this.f6645n.onQrcodeScanned();
                    } else if (b.f6650y != C3064g.UUID_KEEP_CONNECT.getCode() && b.f6650y == C3064g.UUID_CONFIRM.getCode()) {
                        if (b.f6649x == null || b.f6649x.length() == 0) {
                            Log.e("MicroMsg.SDK.NoopingTask", "nooping fail, confirm with an empty code!!!");
                            b.f6648p = OAuthErrCode.WechatAuth_Err_NormalErr;
                        }
                        return b;
                    }
                } else {
                    Log.e("MicroMsg.SDK.NoopingTask", String.format("nooping fail, errCode = %s, uuidStatusCode = %d", b.f6648p.toString(), Integer.valueOf(b.f6650y)));
                    return b;
                }
            }
            Log.i("MicroMsg.SDK.NoopingTask", "IDiffDevOAuth.stopAuth / detach invoked");
            aVar = new C3063a();
            oAuthErrCode = OAuthErrCode.WechatAuth_Err_Auth_Stopped;
        }
        aVar.f6648p = oAuthErrCode;
        return aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ void onPostExecute(Object obj) {
        C3063a aVar = (C3063a) obj;
        this.f6645n.onAuthFinish(aVar.f6648p, aVar.f6649x);
    }
}
