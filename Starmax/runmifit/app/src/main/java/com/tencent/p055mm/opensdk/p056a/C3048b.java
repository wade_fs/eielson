package com.tencent.p055mm.opensdk.p056a;

import com.google.common.base.Ascii;
import java.security.MessageDigest;

/* renamed from: com.tencent.mm.opensdk.a.b */
public final class C3048b {
    /* renamed from: c */
    public static final String m7540c(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(bArr);
            byte[] digest = instance.digest();
            int length = digest.length;
            char[] cArr2 = new char[(length * 2)];
            int i = 0;
            for (byte b : digest) {
                int i2 = i + 1;
                cArr2[i] = cArr[(b >>> 4) & 15];
                i = i2 + 1;
                cArr2[i2] = cArr[b & Ascii.f4512SI];
            }
            return new String(cArr2);
        } catch (Exception unused) {
            return null;
        }
    }
}
