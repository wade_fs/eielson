package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.w */
/* compiled from: BUGLY */
public final class C2957w extends C2944m implements Cloneable {

    /* renamed from: i */
    static C2956v f6263i;

    /* renamed from: j */
    static Map<String, String> f6264j;

    /* renamed from: k */
    static final /* synthetic */ boolean f6265k = (!C2957w.class.desiredAssertionStatus());

    /* renamed from: a */
    public String f6266a = "";

    /* renamed from: b */
    public long f6267b = 0;

    /* renamed from: c */
    public byte f6268c = 0;

    /* renamed from: d */
    public long f6269d = 0;

    /* renamed from: e */
    public C2956v f6270e = null;

    /* renamed from: f */
    public String f6271f = "";

    /* renamed from: g */
    public int f6272g = 0;

    /* renamed from: h */
    public Map<String, String> f6273h = null;

    public C2957w() {
    }

    public C2957w(String str, long j, byte b, long j2, C2956v vVar, String str2, int i, Map<String, String> map) {
        this.f6266a = str;
        this.f6267b = j;
        this.f6268c = b;
        this.f6269d = j2;
        this.f6270e = vVar;
        this.f6271f = str2;
        this.f6272g = i;
        this.f6273h = map;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C2957w wVar = (C2957w) obj;
        if (!C2945n.m7123a(this.f6266a, wVar.f6266a) || !C2945n.m7122a(this.f6267b, wVar.f6267b) || !C2945n.m7120a(this.f6268c, wVar.f6268c) || !C2945n.m7122a(this.f6269d, wVar.f6269d) || !C2945n.m7123a(this.f6270e, wVar.f6270e) || !C2945n.m7123a(this.f6271f, wVar.f6271f) || !C2945n.m7121a(this.f6272g, wVar.f6272g) || !C2945n.m7123a(this.f6273h, wVar.f6273h)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f6265k) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
     arg types: [com.tencent.bugly.proguard.v, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34648a(this.f6266a, 0);
        lVar.mo34645a(this.f6267b, 1);
        lVar.mo34660b(this.f6268c, 2);
        lVar.mo34645a(this.f6269d, 3);
        C2956v vVar = this.f6270e;
        if (vVar != null) {
            lVar.mo34646a((C2944m) super, 4);
        }
        String str = this.f6271f;
        if (str != null) {
            lVar.mo34648a(str, 5);
        }
        lVar.mo34644a(this.f6272g, 6);
        Map<String, String> map = this.f6273h;
        if (map != null) {
            lVar.mo34650a((Map) map, 7);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
     arg types: [com.tencent.bugly.proguard.v, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6266a = kVar.mo34621a(0, true);
        this.f6267b = kVar.mo34618a(this.f6267b, 1, true);
        this.f6268c = kVar.mo34613a(this.f6268c, 2, true);
        this.f6269d = kVar.mo34618a(this.f6269d, 3, false);
        if (f6263i == null) {
            f6263i = new C2956v();
        }
        this.f6270e = (C2956v) kVar.mo34619a((C2944m) f6263i, 4, false);
        this.f6271f = kVar.mo34621a(5, false);
        this.f6272g = kVar.mo34616a(this.f6272g, 6, false);
        if (f6264j == null) {
            f6264j = new HashMap();
            f6264j.put("", "");
        }
        this.f6273h = (Map) kVar.mo34620a((Object) f6264j, 7, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [com.tencent.bugly.proguard.v, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i */
    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34601a(this.f6266a, "eventType");
        iVar.mo34598a(this.f6267b, "eventTime");
        iVar.mo34593a(this.f6268c, "eventResult");
        iVar.mo34598a(this.f6269d, "eventElapse");
        iVar.mo34599a((C2944m) this.f6270e, "destAppInfo");
        iVar.mo34601a(this.f6271f, "strategyId");
        iVar.mo34597a(this.f6272g, "updateType");
        iVar.mo34603a((Map) this.f6273h, "reserved");
    }
}
