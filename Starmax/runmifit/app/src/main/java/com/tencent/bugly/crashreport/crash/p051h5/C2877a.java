package com.tencent.bugly.crashreport.crash.p051h5;

import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.crashreport.crash.h5.a */
/* compiled from: BUGLY */
public class C2877a {

    /* renamed from: a */
    public String f5882a = null;

    /* renamed from: b */
    public String f5883b = null;

    /* renamed from: c */
    public String f5884c = null;

    /* renamed from: d */
    public String f5885d = null;

    /* renamed from: e */
    public String f5886e = null;

    /* renamed from: f */
    public String f5887f = null;

    /* renamed from: g */
    public String f5888g = null;

    /* renamed from: h */
    public String f5889h = null;

    /* renamed from: i */
    public String f5890i = null;

    /* renamed from: j */
    public long f5891j = 0;

    /* renamed from: k */
    public long f5892k = 0;

    /* renamed from: a */
    public Map<String, String> mo34438a() {
        LinkedHashMap linkedHashMap = new LinkedHashMap();
        String str = this.f5882a;
        if (str != null) {
            linkedHashMap.put("[JS] projectRoot", str);
        }
        String str2 = this.f5883b;
        if (str2 != null) {
            linkedHashMap.put("[JS] context", str2);
        }
        String str3 = this.f5884c;
        if (str3 != null) {
            linkedHashMap.put("[JS] url", str3);
        }
        String str4 = this.f5885d;
        if (str4 != null) {
            linkedHashMap.put("[JS] userAgent", str4);
        }
        String str5 = this.f5890i;
        if (str5 != null) {
            linkedHashMap.put("[JS] file", str5);
        }
        long j = this.f5891j;
        if (j != 0) {
            linkedHashMap.put("[JS] lineNumber", Long.toString(j));
        }
        return linkedHashMap;
    }
}
