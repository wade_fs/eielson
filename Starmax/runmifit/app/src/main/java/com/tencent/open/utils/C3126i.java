package com.tencent.open.utils;

import android.os.Handler;
import android.os.HandlerThread;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;

/* renamed from: com.tencent.open.utils.i */
/* compiled from: ProGuard */
public final class C3126i {

    /* renamed from: a */
    public static final Executor f6851a = m7762c();

    /* renamed from: b */
    private static Object f6852b = new Object();

    /* renamed from: c */
    private static Handler f6853c;

    /* renamed from: d */
    private static HandlerThread f6854d;

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.util.concurrent.Executor] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.concurrent.Executor m7762c() {
        /*
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 11
            if (r0 < r1) goto L_0x0018
            java.util.concurrent.ThreadPoolExecutor r0 = new java.util.concurrent.ThreadPoolExecutor
            r3 = 1
            r4 = 1
            r5 = 0
            java.util.concurrent.TimeUnit r7 = java.util.concurrent.TimeUnit.SECONDS
            java.util.concurrent.LinkedBlockingQueue r8 = new java.util.concurrent.LinkedBlockingQueue
            r8.<init>()
            r2 = r0
            r2.<init>(r3, r4, r5, r7, r8)
            goto L_0x003d
        L_0x0018:
            java.lang.Class<android.os.AsyncTask> r0 = android.os.AsyncTask.class
            java.lang.String r1 = "sExecutor"
            java.lang.reflect.Field r0 = r0.getDeclaredField(r1)     // Catch:{ Exception -> 0x002c }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ Exception -> 0x002c }
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x002c }
            java.util.concurrent.Executor r0 = (java.util.concurrent.Executor) r0     // Catch:{ Exception -> 0x002c }
            goto L_0x003d
        L_0x002c:
            java.util.concurrent.ThreadPoolExecutor r0 = new java.util.concurrent.ThreadPoolExecutor
            r2 = 1
            r3 = 1
            r4 = 0
            java.util.concurrent.TimeUnit r6 = java.util.concurrent.TimeUnit.SECONDS
            java.util.concurrent.LinkedBlockingQueue r7 = new java.util.concurrent.LinkedBlockingQueue
            r7.<init>()
            r1 = r0
            r1.<init>(r2, r3, r4, r6, r7)
        L_0x003d:
            boolean r1 = r0 instanceof java.util.concurrent.ThreadPoolExecutor
            if (r1 == 0) goto L_0x0048
            r1 = r0
            java.util.concurrent.ThreadPoolExecutor r1 = (java.util.concurrent.ThreadPoolExecutor) r1
            r2 = 3
            r1.setCorePoolSize(r2)
        L_0x0048:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3126i.m7762c():java.util.concurrent.Executor");
    }

    /* renamed from: a */
    public static Handler m7759a() {
        if (f6853c == null) {
            synchronized (C3126i.class) {
                f6854d = new HandlerThread("SDK_SUB");
                f6854d.start();
                f6853c = new Handler(f6854d.getLooper());
            }
        }
        return f6853c;
    }

    /* renamed from: a */
    public static void m7760a(Runnable runnable) {
        m7759a().post(runnable);
    }

    /* renamed from: b */
    public static Executor m7761b() {
        return new C3128a();
    }

    /* renamed from: com.tencent.open.utils.i$a */
    /* compiled from: ProGuard */
    private static class C3128a implements Executor {

        /* renamed from: a */
        final Queue<Runnable> f6855a;

        /* renamed from: b */
        Runnable f6856b;

        private C3128a() {
            this.f6855a = new LinkedList();
        }

        public synchronized void execute(final Runnable runnable) {
            this.f6855a.offer(new Runnable() {
                /* class com.tencent.open.utils.C3126i.C3128a.C31291 */

                public void run() {
                    try {
                        runnable.run();
                    } finally {
                        C3128a.this.mo35173a();
                    }
                }
            });
            if (this.f6856b == null) {
                mo35173a();
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public synchronized void mo35173a() {
            Runnable poll = this.f6855a.poll();
            this.f6856b = poll;
            if (poll != null) {
                C3126i.f6851a.execute(this.f6856b);
            }
        }
    }
}
