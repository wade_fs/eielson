package com.tencent.stat;

import android.content.Context;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;
import java.util.Timer;
import java.util.TimerTask;

/* renamed from: com.tencent.stat.b */
public class C3196b {

    /* renamed from: b */
    private static volatile C3196b f7173b;

    /* renamed from: a */
    private Timer f7174a = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Context f7175c = null;

    private C3196b(Context context) {
        this.f7175c = context.getApplicationContext();
        this.f7174a = new Timer(false);
    }

    /* renamed from: a */
    public void mo35370a() {
        if (StatConfig.getStatSendStrategy() == StatReportStrategy.PERIOD) {
            long sendPeriodMinutes = (long) (StatConfig.getSendPeriodMinutes() * 60 * 1000);
            if (StatConfig.isDebugEnable()) {
                StatLogger logger = StatCommonHelper.getLogger();
                logger.mo35388i("setupPeriodTimer delay:" + sendPeriodMinutes);
            }
            mo35371a(new TimerTask() {
                /* class com.tencent.stat.C3196b.C31971 */

                public void run() {
                    if (StatConfig.isDebugEnable()) {
                        StatCommonHelper.getLogger().mo35388i("TimerTask run");
                    }
                    StatServiceImpl.m7887e(C3196b.this.f7175c);
                    cancel();
                    C3196b.this.mo35370a();
                }
            }, sendPeriodMinutes);
        }
    }

    /* renamed from: a */
    public static C3196b m7935a(Context context) {
        if (f7173b == null) {
            synchronized (C3196b.class) {
                if (f7173b == null) {
                    f7173b = new C3196b(context);
                }
            }
        }
        return f7173b;
    }

    /* renamed from: a */
    public void mo35371a(TimerTask timerTask, long j) {
        if (this.f7174a != null) {
            if (StatConfig.isDebugEnable()) {
                StatLogger logger = StatCommonHelper.getLogger();
                logger.mo35388i("setupPeriodTimer schedule delay:" + j);
            }
            this.f7174a.schedule(timerTask, j);
        } else if (StatConfig.isDebugEnable()) {
            StatCommonHelper.getLogger().mo35396w("setupPeriodTimer schedule timer == null");
        }
    }
}
