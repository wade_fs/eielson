package com.tencent.stat.event;

import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.C3217i;
import com.tencent.stat.common.StatCommonHelper;
import org.json.JSONException;
import org.json.JSONObject;

public class FBIEvent extends Event {
    public FBIEvent() {
        this.f7343c = true;
    }

    public void setSessionId(int i) {
        this.f7347g = i;
    }

    public void setStatSpecifyReportedInfo(StatSpecifyReportedInfo statSpecifyReportedInfo) {
        this.f7356r = statSpecifyReportedInfo;
    }

    public EventType getType() {
        return EventType.FBI_EVENT;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        JSONObject batteryInfo = StatCommonHelper.getBatteryInfo(getContext());
        if (batteryInfo != null) {
            jSONObject.put("bttr", batteryInfo);
        }
        jSONObject.put("lstFile", C3217i.m8005a(getContext()));
        return false;
    }
}
