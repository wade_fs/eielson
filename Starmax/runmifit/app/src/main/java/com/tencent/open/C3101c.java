package com.tencent.open;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.Constants;
import com.tencent.open.C3070a;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3094g;
import com.tencent.open.p061c.C3107a;
import com.tencent.open.p061c.C3109b;
import com.tencent.open.utils.C3124g;
import com.tencent.open.utils.C3131k;
import com.tencent.tauth.AuthActivity;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.lang.ref.WeakReference;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.open.c */
/* compiled from: ProGuard */
public class C3101c extends C3086b implements C3107a.C3108a {

    /* renamed from: c */
    static Toast f6792c;

    /* renamed from: d */
    private String f6793d;

    /* renamed from: e */
    private IUiListener f6794e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public C3105c f6795f;

    /* renamed from: g */
    private Handler f6796g;

    /* renamed from: h */
    private C3107a f6797h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public C3109b f6798i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public WeakReference<Context> f6799j;

    /* renamed from: k */
    private int f6800k;

    public C3101c(Context context, String str, String str2, IUiListener iUiListener, QQToken qQToken) {
        super(context, 16973840);
        this.f6799j = new WeakReference<>(context);
        this.f6793d = str2;
        this.f6795f = new C3105c(context, str, str2, qQToken.getAppId(), iUiListener);
        this.f6796g = new C3106d(this.f6795f, context.getMainLooper());
        this.f6794e = iUiListener;
        this.f6800k = Math.round(context.getResources().getDisplayMetrics().density * 185.0f);
        C3082f.m7636e("openSDK_LOG.PKDialog", "density=" + context.getResources().getDisplayMetrics().density + "; webviewHeight=" + this.f6800k);
    }

    public void onBackPressed() {
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        getWindow().setSoftInputMode(16);
        getWindow().setSoftInputMode(1);
        m7691b();
        m7694c();
    }

    /* renamed from: b */
    private void m7691b() {
        this.f6797h = new C3107a(this.f6799j.get());
        this.f6797h.setBackgroundColor(1711276032);
        this.f6797h.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.f6798i = new C3109b(this.f6799j.get());
        this.f6798i.setBackgroundColor(0);
        this.f6798i.setBackgroundDrawable(null);
        if (Build.VERSION.SDK_INT >= 11) {
            Class<View> cls = View.class;
            try {
                cls.getMethod("setLayerType", Integer.TYPE, Paint.class).invoke(this.f6798i, 1, new Paint());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, this.f6800k);
        layoutParams.addRule(13, -1);
        this.f6798i.setLayoutParams(layoutParams);
        this.f6797h.addView(this.f6798i);
        this.f6797h.mo35155a(this);
        setContentView(this.f6797h);
    }

    /* renamed from: c */
    private void m7694c() {
        this.f6798i.setVerticalScrollBarEnabled(false);
        this.f6798i.setHorizontalScrollBarEnabled(false);
        this.f6798i.setWebViewClient(new C3103a());
        this.f6798i.setWebChromeClient(this.f6753b);
        this.f6798i.clearFormData();
        WebSettings settings = this.f6798i.getSettings();
        if (settings != null) {
            settings.setSavePassword(false);
            settings.setSaveFormData(false);
            settings.setCacheMode(-1);
            settings.setNeedInitialFocus(false);
            settings.setBuiltInZoomControls(true);
            settings.setSupportZoom(true);
            settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
            settings.setJavaScriptEnabled(true);
            WeakReference<Context> weakReference = this.f6799j;
            if (!(weakReference == null || weakReference.get() == null)) {
                settings.setDatabaseEnabled(true);
                settings.setDatabasePath(this.f6799j.get().getApplicationContext().getDir("databases", 0).getPath());
            }
            settings.setDomStorageEnabled(true);
            this.f6752a.mo35067a(new C3104b(), "sdk_js_if");
            this.f6798i.clearView();
            this.f6798i.loadUrl(this.f6793d);
            this.f6798i.getSettings().setSavePassword(false);
        }
    }

    /* renamed from: com.tencent.open.c$b */
    /* compiled from: ProGuard */
    private class C3104b extends C3070a.C3072b {
        private C3104b() {
        }
    }

    /* renamed from: com.tencent.open.c$a */
    /* compiled from: ProGuard */
    private class C3103a extends WebViewClient {
        private C3103a() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            C3082f.m7628a("openSDK_LOG.PKDialog", "Redirect URL: " + str);
            if (str.startsWith(C3124g.m7744a().mo35172a((Context) C3101c.this.f6799j.get(), "auth://tauth.qq.com/"))) {
                C3101c.this.f6795f.onComplete(C3131k.m7786c(str));
                C3101c.this.dismiss();
                return true;
            } else if (str.startsWith(Constants.CANCEL_URI)) {
                C3101c.this.f6795f.onCancel();
                C3101c.this.dismiss();
                return true;
            } else if (!str.startsWith(Constants.CLOSE_URI)) {
                return false;
            } else {
                C3101c.this.dismiss();
                return true;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            C3101c.this.f6795f.onError(new UiError(i, str, str2));
            if (!(C3101c.this.f6799j == null || C3101c.this.f6799j.get() == null)) {
                Toast.makeText((Context) C3101c.this.f6799j.get(), "网络连接异常或系统错误", 0).show();
            }
            C3101c.this.dismiss();
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            C3082f.m7628a("openSDK_LOG.PKDialog", "Webview loading URL: " + str);
            super.onPageStarted(webView, str, bitmap);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            C3101c.this.f6798i.setVisibility(View.VISIBLE);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: c */
    public static void m7695c(Context context, String str) {
        try {
            JSONObject d = C3131k.m7788d(str);
            int i = d.getInt(SocialConstants.PARAM_TYPE);
            String string = d.getString("msg");
            if (i == 0) {
                if (f6792c == null) {
                    f6792c = Toast.makeText(context, string, 0);
                } else {
                    f6792c.setView(f6792c.getView());
                    f6792c.setText(string);
                    f6792c.setDuration(0);
                }
                f6792c.show();
            } else if (i == 1) {
                if (f6792c == null) {
                    f6792c = Toast.makeText(context, string, 1);
                } else {
                    f6792c.setView(f6792c.getView());
                    f6792c.setText(string);
                    f6792c.setDuration(1);
                }
                f6792c.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static void m7696d(Context context, String str) {
        if (context != null && str != null) {
            try {
                JSONObject d = C3131k.m7788d(str);
                d.getInt(AuthActivity.ACTION_KEY);
                d.getString("msg");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: com.tencent.open.c$d */
    /* compiled from: ProGuard */
    private class C3106d extends Handler {

        /* renamed from: b */
        private C3105c f6809b;

        public C3106d(C3105c cVar, Looper looper) {
            super(looper);
            this.f6809b = cVar;
        }

        public void handleMessage(Message message) {
            C3082f.m7631b("openSDK_LOG.PKDialog", "msg = " + message.what);
            int i = message.what;
            if (i == 1) {
                this.f6809b.m7701a((String) message.obj);
            } else if (i == 2) {
                this.f6809b.onCancel();
            } else if (i != 3) {
                if (i != 4 && i == 5 && C3101c.this.f6799j != null && C3101c.this.f6799j.get() != null) {
                    C3101c.m7696d((Context) C3101c.this.f6799j.get(), (String) message.obj);
                }
            } else if (C3101c.this.f6799j != null && C3101c.this.f6799j.get() != null) {
                C3101c.m7695c((Context) C3101c.this.f6799j.get(), (String) message.obj);
            }
        }
    }

    /* renamed from: com.tencent.open.c$c */
    /* compiled from: ProGuard */
    private static class C3105c implements IUiListener {

        /* renamed from: a */
        String f6803a;

        /* renamed from: b */
        String f6804b;

        /* renamed from: c */
        private WeakReference<Context> f6805c;

        /* renamed from: d */
        private String f6806d;

        /* renamed from: e */
        private IUiListener f6807e;

        public C3105c(Context context, String str, String str2, String str3, IUiListener iUiListener) {
            this.f6805c = new WeakReference<>(context);
            this.f6806d = str;
            this.f6803a = str2;
            this.f6804b = str3;
            this.f6807e = iUiListener;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m7701a(String str) {
            try {
                onComplete(C3131k.m7788d(str));
            } catch (JSONException e) {
                e.printStackTrace();
                onError(new UiError(-4, Constants.MSG_JSON_ERROR, str));
            }
        }

        public void onComplete(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            C3094g a = C3094g.m7676a();
            a.mo35133a(this.f6806d + "_H5", SystemClock.elapsedRealtime(), 0, 0, jSONObject.optInt("ret", -6), this.f6803a, false);
            IUiListener iUiListener = this.f6807e;
            if (iUiListener != null) {
                iUiListener.onComplete(jSONObject);
                this.f6807e = null;
            }
        }

        public void onError(UiError uiError) {
            String str;
            if (uiError.errorMessage != null) {
                str = uiError.errorMessage + this.f6803a;
            } else {
                str = this.f6803a;
            }
            C3094g a = C3094g.m7676a();
            a.mo35133a(this.f6806d + "_H5", SystemClock.elapsedRealtime(), 0, 0, uiError.errorCode, str, false);
            IUiListener iUiListener = this.f6807e;
            if (iUiListener != null) {
                iUiListener.onError(uiError);
                this.f6807e = null;
            }
        }

        public void onCancel() {
            IUiListener iUiListener = this.f6807e;
            if (iUiListener != null) {
                iUiListener.onCancel();
                this.f6807e = null;
            }
        }
    }

    /* renamed from: a */
    public void mo35148a(int i) {
        WeakReference<Context> weakReference = this.f6799j;
        if (!(weakReference == null || weakReference.get() == null)) {
            if (i >= this.f6800k || 2 != this.f6799j.get().getResources().getConfiguration().orientation) {
                this.f6798i.getLayoutParams().height = this.f6800k;
            } else {
                this.f6798i.getLayoutParams().height = i;
            }
        }
        C3082f.m7636e("openSDK_LOG.PKDialog", "onKeyboardShown keyboard show");
    }

    /* renamed from: a */
    public void mo35147a() {
        this.f6798i.getLayoutParams().height = this.f6800k;
        C3082f.m7636e("openSDK_LOG.PKDialog", "onKeyboardHidden keyboard hide");
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35050a(String str) {
        C3082f.m7631b("openSDK_LOG.PKDialog", "--onConsoleMessage--");
        try {
            this.f6752a.mo35069a(this.f6798i, str);
        } catch (Exception unused) {
        }
    }
}
