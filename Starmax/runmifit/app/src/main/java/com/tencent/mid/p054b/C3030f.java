package com.tencent.mid.p054b;

import android.content.Context;
import com.tencent.mid.api.C3024a;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;

/* renamed from: com.tencent.mid.b.f */
public abstract class C3030f {

    /* renamed from: b */
    protected static C3038d f6553b = Util.getLogger();

    /* renamed from: c */
    protected Context f6554c = null;

    /* renamed from: d */
    protected int f6555d = 0;

    /* renamed from: a */
    public abstract int mo34886a();

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo34887a(C3025a aVar);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo34888a(String str);

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract boolean mo34889b();

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract String mo34890c();

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public abstract C3025a mo34891d();

    /* renamed from: e */
    public String mo34898e() {
        if (this.f6555d == 0) {
            return Util.decode("6X8Y4XdM2Vhvn0I=");
        }
        return Util.decode("6X8Y4XdM2Vhvn0I=") + this.f6555d;
    }

    /* renamed from: f */
    public String mo34899f() {
        if (this.f6555d == 0) {
            return Util.decode("6X8Y4XdM2Vhvn0KfzcEatGnWaNU=");
        }
        return Util.decode("6X8Y4XdM2Vhvn0KfzcEatGnWaNU=") + this.f6555d;
    }

    /* renamed from: g */
    public String mo34900g() {
        if (this.f6555d == 0) {
            return Util.decode("4kU71lN96TJUomD1vOU9lgj9U+kKmxDPLVM+zzjst5U=");
        }
        return Util.decode("4kU71lN96TJUomD1vOU9lgj9U+kKmxDPLVM+zzjst5U=") + this.f6555d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public String mo34901h() {
        if (this.f6555d == 0) {
            return Util.decode("4kU71lN96TJUomD1vOU9lgj9Tw==");
        }
        return Util.decode("4kU71lN96TJUomD1vOU9lgj9Tw==") + this.f6555d;
    }

    protected C3030f(Context context, int i) {
        this.f6554c = context;
        this.f6555d = i;
    }

    /* renamed from: k */
    private String m7419k() {
        if (mo34889b()) {
            return mo34897d(mo34890c());
        }
        return null;
    }

    /* renamed from: i */
    public MidEntity mo34902i() {
        String k = m7419k();
        if (k != null) {
            return MidEntity.parse(k);
        }
        return null;
    }

    /* renamed from: b */
    private void mo34893b(String str) {
        if (mo34889b()) {
            mo34888a(mo34896c(str));
        }
    }

    /* renamed from: a */
    public void mo34894a(MidEntity midEntity) {
        if (midEntity != null) {
            if (mo34886a() == 4) {
                C3024a.m7384a(this.f6554c).mo34875a(midEntity.getMid());
            }
            mo34893b(midEntity.toString());
        }
    }

    /* renamed from: j */
    public C3025a mo34903j() {
        if (mo34889b()) {
            return mo34891d();
        }
        return null;
    }

    /* renamed from: b */
    public void mo34895b(C3025a aVar) {
        if (aVar != null && mo34889b()) {
            mo34887a(aVar);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public String mo34896c(String str) {
        return Util.encode(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public String mo34897d(String str) {
        return Util.decode(str);
    }
}
