package com.tencent.connect.share;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.Base64;
import com.tencent.connect.auth.QQToken;
import com.tencent.connect.common.BaseApi;
import com.tencent.connect.common.Constants;
import com.tencent.connect.common.UIListenerManager;
import com.tencent.connect.p052a.ProGuard;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3091d;
import com.tencent.open.utils.C3116b;
import com.tencent.open.utils.C3119c;
import com.tencent.open.utils.C3121e;
import com.tencent.open.utils.C3131k;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.util.ArrayList;

/* compiled from: ProGuard */
public class QQShare extends BaseApi {
    public static final int QQ_SHARE_SUMMARY_MAX_LENGTH = 512;
    public static final int QQ_SHARE_TITLE_MAX_LENGTH = 128;
    public static final String SHARE_TO_QQ_APP_NAME = "appName";
    public static final String SHARE_TO_QQ_ARK_INFO = "share_to_qq_ark_info";
    public static final String SHARE_TO_QQ_AUDIO_URL = "audio_url";
    public static final String SHARE_TO_QQ_EXT_INT = "cflag";
    public static final String SHARE_TO_QQ_EXT_STR = "share_qq_ext_str";
    public static final int SHARE_TO_QQ_FLAG_QZONE_AUTO_OPEN = 1;
    public static final int SHARE_TO_QQ_FLAG_QZONE_ITEM_HIDE = 2;
    public static final String SHARE_TO_QQ_IMAGE_LOCAL_URL = "imageLocalUrl";
    public static final String SHARE_TO_QQ_IMAGE_URL = "imageUrl";
    public static final String SHARE_TO_QQ_KEY_TYPE = "req_type";
    public static final int SHARE_TO_QQ_MINI_PROGRAM = 7;
    public static final String SHARE_TO_QQ_MINI_PROGRAM_APPID = "mini_program_appid";
    public static final String SHARE_TO_QQ_MINI_PROGRAM_PATH = "mini_program_path";
    public static final String SHARE_TO_QQ_MINI_PROGRAM_TYPE = "mini_program_type";
    public static final String SHARE_TO_QQ_SITE = "site";
    public static final String SHARE_TO_QQ_SUMMARY = "summary";
    public static final String SHARE_TO_QQ_TARGET_URL = "targetUrl";
    public static final String SHARE_TO_QQ_TITLE = "title";
    public static final int SHARE_TO_QQ_TYPE_APP = 6;
    public static final int SHARE_TO_QQ_TYPE_AUDIO = 2;
    public static final int SHARE_TO_QQ_TYPE_DEFAULT = 1;
    public static final int SHARE_TO_QQ_TYPE_IMAGE = 5;
    public String mViaShareQQType = "";

    public void releaseResource() {
    }

    public QQShare(Context context, QQToken qQToken) {
        super(qQToken);
    }

    /* JADX WARNING: Removed duplicated region for block: B:56:0x023c  */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x031f  */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x0328  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void shareToQQ(android.app.Activity r22, android.os.Bundle r23, com.tencent.tauth.IUiListener r24) {
        /*
            r21 = this;
            r1 = r21
            r0 = r22
            r2 = r23
            r8 = r24
            java.lang.String r9 = "openSDK_LOG.QQShare"
            java.lang.String r3 = "shareToQQ() -- start."
            com.tencent.open.p059a.C3082f.m7634c(r9, r3)
            java.lang.String r3 = "imageUrl"
            java.lang.String r3 = r2.getString(r3)
            java.lang.String r4 = "title"
            java.lang.String r5 = r2.getString(r4)
            java.lang.String r6 = "summary"
            java.lang.String r7 = r2.getString(r6)
            java.lang.String r10 = "targetUrl"
            java.lang.String r11 = r2.getString(r10)
            java.lang.String r12 = "imageLocalUrl"
            java.lang.String r12 = r2.getString(r12)
            java.lang.String r13 = "mini_program_appid"
            java.lang.String r13 = r2.getString(r13)
            java.lang.String r14 = "mini_program_path"
            java.lang.String r14 = r2.getString(r14)
            r15 = 1
            r16 = r11
            java.lang.String r11 = "req_type"
            int r11 = r2.getInt(r11, r15)
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r18 = r6
            java.lang.String r6 = "shareToQQ -- type: "
            r15.append(r6)
            r15.append(r11)
            java.lang.String r6 = r15.toString()
            com.tencent.open.p059a.C3082f.m7634c(r9, r6)
            r15 = 2
            r6 = 1
            if (r11 == r6) goto L_0x007c
            if (r11 == r15) goto L_0x0077
            r6 = 5
            if (r11 == r6) goto L_0x0072
            r6 = 6
            if (r11 == r6) goto L_0x006d
            r6 = 7
            if (r11 == r6) goto L_0x0068
            goto L_0x0080
        L_0x0068:
            java.lang.String r6 = "9"
            r1.mViaShareQQType = r6
            goto L_0x0080
        L_0x006d:
            java.lang.String r6 = "4"
            r1.mViaShareQQType = r6
            goto L_0x0080
        L_0x0072:
            java.lang.String r6 = "2"
            r1.mViaShareQQType = r6
            goto L_0x0080
        L_0x0077:
            java.lang.String r6 = "3"
            r1.mViaShareQQType = r6
            goto L_0x0080
        L_0x007c:
            java.lang.String r6 = "1"
            r1.mViaShareQQType = r6
        L_0x0080:
            r15 = 0
            r6 = 6
            if (r11 != r6) goto L_0x00e0
            java.lang.String r6 = "5.0.0"
            boolean r6 = com.tencent.open.utils.C3131k.m7795f(r0, r6)
            if (r6 == 0) goto L_0x00c3
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            r2 = -15
            java.lang.String r3 = "手Q版本过低，应用分享只支持手Q5.0及其以上版本"
            r0.<init>(r2, r3, r15)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ, app share is not support below qq5.0."
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "shareToQQ, app share is not support below qq5.0."
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x00c3:
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]
            com.tencent.connect.auth.QQToken r15 = r1.f6457c
            java.lang.String r15 = r15.getAppId()
            r16 = 0
            r6[r16] = r15
            java.lang.String r15 = "mqq"
            r16 = 1
            r6[r16] = r15
            java.lang.String r15 = "http://fusion.qq.com/cgi-bin/qzapps/unified_jump?appid=%1$s&from=%2$s&isOpenAppID=1"
            java.lang.String r6 = java.lang.String.format(r15, r6)
            r2.putString(r10, r6)
            goto L_0x00e2
        L_0x00e0:
            r6 = r16
        L_0x00e2:
            boolean r10 = com.tencent.open.utils.C3131k.m7782b()
            r15 = -6
            if (r10 != 0) goto L_0x0127
            java.lang.String r10 = "4.5.0"
            boolean r10 = com.tencent.open.utils.C3131k.m7795f(r0, r10)
            if (r10 == 0) goto L_0x0127
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            java.lang.String r2 = "分享图片失败，检测不到SD卡!"
            r3 = 0
            r0.<init>(r15, r2, r3)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ sdcard is null--end"
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "shareToQQ sdcard is null"
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x0127:
            java.lang.String r10 = "非法的图片地址!"
            java.lang.String r15 = "低版本手Q不支持该项功能!"
            r20 = r7
            r7 = 5
            if (r11 != r7) goto L_0x01a8
            java.lang.String r7 = "4.3.0"
            boolean r7 = com.tencent.open.utils.C3131k.m7795f(r0, r7)
            if (r7 == 0) goto L_0x016d
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            r2 = -6
            r3 = 0
            r0.<init>(r2, r15, r3)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ, version below 4.3 is not support."
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "shareToQQ, version below 4.3 is not support."
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x016d:
            boolean r7 = com.tencent.open.utils.C3131k.m7798h(r12)
            if (r7 != 0) goto L_0x01a8
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            r2 = -6
            r3 = 0
            r0.<init>(r2, r10, r3)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ -- error: 非法的图片地址!"
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "非法的图片地址!"
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x01a8:
            java.lang.String r7 = "http://"
            r12 = 5
            if (r11 == r12) goto L_0x0239
            r12 = 7
            if (r11 == r12) goto L_0x023a
            boolean r12 = android.text.TextUtils.isEmpty(r6)
            if (r12 != 0) goto L_0x0202
            boolean r12 = r6.startsWith(r7)
            if (r12 != 0) goto L_0x01c5
            java.lang.String r12 = "https://"
            boolean r12 = r6.startsWith(r12)
            if (r12 != 0) goto L_0x01c5
            goto L_0x0202
        L_0x01c5:
            boolean r12 = android.text.TextUtils.isEmpty(r5)
            if (r12 == 0) goto L_0x0239
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            java.lang.String r2 = "title不能为空!"
            r3 = -6
            r4 = 0
            r0.<init>(r3, r2, r4)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ, title is empty."
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "shareToQQ, title is empty."
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x0202:
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            java.lang.String r2 = "传入参数有误!"
            r3 = -6
            r4 = 0
            r0.<init>(r3, r2, r4)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ, targetUrl is empty or illegal.."
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "shareToQQ, targetUrl is empty or illegal.."
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x0239:
            r12 = 7
        L_0x023a:
            if (r11 != r12) goto L_0x0285
            boolean r11 = android.text.TextUtils.isEmpty(r13)
            if (r11 != 0) goto L_0x0277
            boolean r11 = android.text.TextUtils.isEmpty(r14)
            if (r11 != 0) goto L_0x0277
            boolean r6 = android.text.TextUtils.isEmpty(r6)
            if (r6 != 0) goto L_0x0277
            com.tencent.connect.auth.QQToken r6 = r1.f6457c
            java.lang.String r6 = r6.getAppId()
            boolean r6 = android.text.TextUtils.isEmpty(r6)
            if (r6 == 0) goto L_0x025b
            goto L_0x0277
        L_0x025b:
            java.lang.String r6 = "com.tencent.qqlite"
            java.lang.String r6 = com.tencent.open.utils.C3125h.m7751a(r0, r6)
            if (r6 != 0) goto L_0x0285
            java.lang.String r6 = "8.0.8"
            int r6 = com.tencent.open.utils.C3125h.m7757c(r0, r6)
            if (r6 >= 0) goto L_0x0285
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            r2 = -5
            java.lang.String r3 = "808以下不支持分享小程序"
            r0.<init>(r2, r15, r3)
            r8.onError(r0)
            return
        L_0x0277:
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            r2 = -5
            java.lang.String r3 = "传入参数有误!"
            java.lang.String r4 = "appid || path || url empty."
            r0.<init>(r2, r3, r4)
            r8.onError(r0)
            return
        L_0x0285:
            boolean r6 = android.text.TextUtils.isEmpty(r3)
            if (r6 != 0) goto L_0x02d9
            boolean r6 = r3.startsWith(r7)
            if (r6 != 0) goto L_0x02d9
            java.lang.String r6 = "https://"
            boolean r6 = r3.startsWith(r6)
            if (r6 != 0) goto L_0x02d9
            java.io.File r6 = new java.io.File
            r6.<init>(r3)
            boolean r3 = r6.exists()
            if (r3 != 0) goto L_0x02d9
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            r2 = -6
            r3 = 0
            r0.<init>(r2, r10, r3)
            r8.onError(r0)
            java.lang.String r0 = "shareToQQ, image url is emprty or illegal."
            com.tencent.open.p059a.C3082f.m7636e(r9, r0)
            com.tencent.open.b.d r10 = com.tencent.open.p060b.C3091d.m7665a()
            r11 = 1
            com.tencent.connect.auth.QQToken r0 = r1.f6457c
            java.lang.String r14 = r0.getAppId()
            r0 = 0
            java.lang.String r15 = java.lang.String.valueOf(r0)
            long r2 = android.os.SystemClock.elapsedRealtime()
            java.lang.Long r16 = java.lang.Long.valueOf(r2)
            r17 = 0
            r18 = 1
            java.lang.String r12 = "SHARE_CHECK_SDK"
            java.lang.String r13 = "1000"
            java.lang.String r19 = "shareToQQ, image url is emprty or illegal."
            r10.mo35121a(r11, r12, r13, r14, r15, r16, r17, r18, r19)
            return
        L_0x02d9:
            boolean r3 = android.text.TextUtils.isEmpty(r5)
            if (r3 != 0) goto L_0x02f2
            int r3 = r5.length()
            r6 = 128(0x80, float:1.794E-43)
            if (r3 <= r6) goto L_0x02f2
            r3 = 128(0x80, float:1.794E-43)
            r6 = 0
            java.lang.String r3 = com.tencent.open.utils.C3131k.m7772a(r5, r3, r6, r6)
            r2.putString(r4, r3)
            goto L_0x02f3
        L_0x02f2:
            r6 = 0
        L_0x02f3:
            boolean r3 = android.text.TextUtils.isEmpty(r20)
            if (r3 != 0) goto L_0x030e
            int r3 = r20.length()
            r4 = 512(0x200, float:7.175E-43)
            if (r3 <= r4) goto L_0x030e
            r3 = 512(0x200, float:7.175E-43)
            r4 = r20
            java.lang.String r3 = com.tencent.open.utils.C3131k.m7772a(r4, r3, r6, r6)
            r4 = r18
            r2.putString(r4, r3)
        L_0x030e:
            java.lang.String r3 = "cflag"
            r4 = 0
            int r3 = r2.getInt(r3, r4)
            r6 = 1
            if (r3 != r6) goto L_0x0319
            r4 = 1
        L_0x0319:
            boolean r3 = com.tencent.open.utils.C3131k.m7778a(r0, r4)
            if (r3 == 0) goto L_0x0328
            java.lang.String r3 = "shareToQQ, support share"
            com.tencent.open.p059a.C3082f.m7634c(r9, r3)
            r21.m7321b(r22, r23, r24)
            goto L_0x0359
        L_0x0328:
            java.lang.String r2 = "shareToQQ, don't support share, will show download dialog"
            com.tencent.open.p059a.C3082f.m7635d(r9, r2)     // Catch:{ RuntimeException -> 0x0344 }
            com.tencent.open.TDialog r10 = new com.tencent.open.TDialog     // Catch:{ RuntimeException -> 0x0344 }
            java.lang.String r4 = ""
            java.lang.String r2 = ""
            java.lang.String r5 = r1.mo34799a(r2)     // Catch:{ RuntimeException -> 0x0344 }
            r6 = 0
            com.tencent.connect.auth.QQToken r7 = r1.f6457c     // Catch:{ RuntimeException -> 0x0344 }
            r2 = r10
            r3 = r22
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ RuntimeException -> 0x0344 }
            r10.show()     // Catch:{ RuntimeException -> 0x0344 }
            goto L_0x0359
        L_0x0344:
            r0 = move-exception
            java.lang.String r2 = " shareToQQ, TDialog.show not in main thread"
            com.tencent.open.p059a.C3082f.m7632b(r9, r2, r0)
            r0.printStackTrace()
            com.tencent.tauth.UiError r0 = new com.tencent.tauth.UiError
            java.lang.String r2 = "没有在主线程调用！"
            r3 = -6
            r4 = 0
            r0.<init>(r3, r2, r4)
            r8.onError(r0)
        L_0x0359:
            java.lang.String r0 = "shareToQQ() -- end."
            com.tencent.open.p059a.C3082f.m7634c(r9, r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.connect.share.QQShare.shareToQQ(android.app.Activity, android.os.Bundle, com.tencent.tauth.IUiListener):void");
    }

    /* renamed from: b */
    private void m7321b(Activity activity, Bundle bundle, IUiListener iUiListener) {
        C3082f.m7634c("openSDK_LOG.QQShare", "shareToMobileQQ() -- start.");
        String string = bundle.getString("imageUrl");
        final String string2 = bundle.getString("title");
        final String string3 = bundle.getString("summary");
        C3082f.m7628a("openSDK_LOG.QQShare", "shareToMobileQQ -- imageUrl: " + string);
        if (TextUtils.isEmpty(string)) {
            m7322c(activity, bundle, iUiListener);
        } else if (!C3131k.m7797g(string)) {
            bundle.putString("imageUrl", null);
            if (C3131k.m7795f(activity, "4.3.0")) {
                C3082f.m7631b("openSDK_LOG.QQShare", "shareToMobileQQ -- QQ Version is < 4.3.0 ");
                m7322c(activity, bundle, iUiListener);
            } else {
                C3082f.m7631b("openSDK_LOG.QQShare", "shareToMobileQQ -- QQ Version is > 4.3.0 ");
                final Bundle bundle2 = bundle;
                final IUiListener iUiListener2 = iUiListener;
                final Activity activity2 = activity;
                C3005a.m7337a(activity, string, new C3119c() {
                    /* class com.tencent.connect.share.QQShare.C30012 */

                    /* renamed from: a */
                    public void mo34827a(int i, ArrayList<String> arrayList) {
                    }

                    /* renamed from: a */
                    public void mo34826a(int i, String str) {
                        if (i == 0) {
                            bundle2.putString("imageLocalUrl", str);
                        } else if (TextUtils.isEmpty(string2) && TextUtils.isEmpty(string3)) {
                            IUiListener iUiListener = iUiListener2;
                            if (iUiListener != null) {
                                iUiListener.onError(new UiError(-6, Constants.MSG_SHARE_GETIMG_ERROR, null));
                                C3082f.m7636e("openSDK_LOG.QQShare", "shareToMobileQQ -- error: 获取分享图片失败!");
                            }
                            C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, QQShare.this.f6457c.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.MSG_SHARE_GETIMG_ERROR);
                            return;
                        }
                        QQShare.this.m7322c(activity2, bundle2, iUiListener2);
                    }
                });
            }
        } else if (!C3131k.m7795f(activity, "4.3.0")) {
            m7322c(activity, bundle, iUiListener);
        } else {
            final Bundle bundle3 = bundle;
            final IUiListener iUiListener3 = iUiListener;
            final Activity activity3 = activity;
            new C3116b(activity).mo35165a(string, new C3119c() {
                /* class com.tencent.connect.share.QQShare.C30001 */

                /* renamed from: a */
                public void mo34827a(int i, ArrayList<String> arrayList) {
                }

                /* renamed from: a */
                public void mo34826a(int i, String str) {
                    if (i == 0) {
                        bundle3.putString("imageLocalUrl", str);
                    } else if (TextUtils.isEmpty(string2) && TextUtils.isEmpty(string3)) {
                        IUiListener iUiListener = iUiListener3;
                        if (iUiListener != null) {
                            iUiListener.onError(new UiError(-6, Constants.MSG_SHARE_GETIMG_ERROR, null));
                            C3082f.m7636e("openSDK_LOG.QQShare", "shareToMobileQQ -- error: 获取分享图片失败!");
                        }
                        C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, QQShare.this.f6457c.getAppId(), String.valueOf(0), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, Constants.MSG_SHARE_GETIMG_ERROR);
                        return;
                    }
                    QQShare.this.m7322c(activity3, bundle3, iUiListener3);
                }
            });
        }
        C3082f.m7634c("openSDK_LOG.QQShare", "shareToMobileQQ() -- end");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.connect.common.BaseApi.a(android.app.Activity, int, android.content.Intent, boolean):void
     arg types: [android.app.Activity, ?, android.content.Intent, int]
     candidates:
      com.tencent.connect.share.QQShare.a(com.tencent.connect.share.QQShare, android.app.Activity, android.os.Bundle, com.tencent.tauth.IUiListener):void
      com.tencent.connect.common.BaseApi.a(android.app.Activity, int, android.content.Intent, boolean):void */
    /* access modifiers changed from: private */
    /* renamed from: c */
    public void m7322c(Activity activity, Bundle bundle, IUiListener iUiListener) {
        int i;
        int i2;
        int i3;
        Bundle bundle2 = bundle;
        C3082f.m7634c("openSDK_LOG.QQShare", "doShareToQQ() -- start");
        StringBuffer stringBuffer = new StringBuffer("mqqapi://share/to_fri?src_type=app&version=1&file_type=news");
        String string = bundle2.getString("imageUrl");
        String string2 = bundle2.getString("title");
        String string3 = bundle2.getString("summary");
        String string4 = bundle2.getString("targetUrl");
        String string5 = bundle2.getString("audio_url");
        int i4 = bundle2.getInt("req_type", 1);
        String string6 = bundle2.getString(SHARE_TO_QQ_ARK_INFO);
        String string7 = bundle2.getString(SHARE_TO_QQ_MINI_PROGRAM_APPID);
        String string8 = bundle2.getString(SHARE_TO_QQ_MINI_PROGRAM_PATH);
        String string9 = bundle2.getString(SHARE_TO_QQ_MINI_PROGRAM_TYPE);
        int i5 = bundle2.getInt("cflag", 0);
        String string10 = bundle2.getString("share_qq_ext_str");
        String a = C3131k.m7770a(activity);
        int i6 = i5;
        if (a == null) {
            a = bundle2.getString("appName");
        }
        String str = a;
        String str2 = string10;
        String string11 = bundle2.getString("imageLocalUrl");
        String appId = this.f6457c.getAppId();
        String str3 = string6;
        String openId = this.f6457c.getOpenId();
        StringBuilder sb = new StringBuilder();
        String str4 = string9;
        sb.append("doShareToQQ -- openid: ");
        sb.append(openId);
        C3082f.m7628a("openSDK_LOG.QQShare", sb.toString());
        if (!TextUtils.isEmpty(string)) {
            stringBuffer.append("&image_url=" + Base64.encodeToString(C3131k.m7799i(string), 2));
        }
        if (!TextUtils.isEmpty(string11)) {
            stringBuffer.append("&file_data=" + Base64.encodeToString(C3131k.m7799i(string11), 2));
        }
        if (!TextUtils.isEmpty(string2)) {
            stringBuffer.append("&title=" + Base64.encodeToString(C3131k.m7799i(string2), 2));
        }
        if (!TextUtils.isEmpty(string3)) {
            stringBuffer.append("&description=" + Base64.encodeToString(C3131k.m7799i(string3), 2));
        }
        if (!TextUtils.isEmpty(appId)) {
            stringBuffer.append("&share_id=" + appId);
        }
        if (!TextUtils.isEmpty(string4)) {
            stringBuffer.append("&url=" + Base64.encodeToString(C3131k.m7799i(string4), 2));
        }
        if (!TextUtils.isEmpty(str)) {
            if (str.length() > 20) {
                str = str.substring(0, 20) + "...";
            }
            stringBuffer.append("&app_name=" + Base64.encodeToString(C3131k.m7799i(str), 2));
        }
        if (!TextUtils.isEmpty(openId)) {
            stringBuffer.append("&open_id=" + Base64.encodeToString(C3131k.m7799i(openId), 2));
        }
        if (!TextUtils.isEmpty(string5)) {
            stringBuffer.append("&audioUrl=" + Base64.encodeToString(C3131k.m7799i(string5), 2));
        }
        stringBuffer.append("&req_type=" + Base64.encodeToString(C3131k.m7799i(String.valueOf(i4)), 2));
        if (!TextUtils.isEmpty(string7)) {
            stringBuffer.append("&mini_program_appid=" + Base64.encodeToString(C3131k.m7799i(String.valueOf(string7)), 2));
        }
        if (!TextUtils.isEmpty(string8)) {
            stringBuffer.append("&mini_program_path=" + Base64.encodeToString(C3131k.m7799i(String.valueOf(string8)), 2));
        }
        if (!TextUtils.isEmpty(str4)) {
            stringBuffer.append("&mini_program_type=" + Base64.encodeToString(C3131k.m7799i(String.valueOf(str4)), 2));
        }
        if (!TextUtils.isEmpty(str3)) {
            stringBuffer.append("&share_to_qq_ark_info=" + Base64.encodeToString(C3131k.m7799i(str3), 2));
        }
        if (!TextUtils.isEmpty(str2)) {
            stringBuffer.append("&share_qq_ext_str=" + Base64.encodeToString(C3131k.m7799i(str2), 2));
        }
        stringBuffer.append("&cflag=" + Base64.encodeToString(C3131k.m7799i(String.valueOf(i6)), 2));
        C3082f.m7628a("openSDK_LOG.QQShare", "doShareToQQ -- url: " + stringBuffer.toString());
        ProGuard.m7167a(C3121e.m7727a(), this.f6457c, "requireApi", "shareToNativeQQ");
        Intent intent = new Intent("android.intent.action.VIEW");
        intent.setData(Uri.parse(stringBuffer.toString()));
        intent.putExtra("pkg_name", activity.getPackageName());
        Activity activity2 = activity;
        if (C3131k.m7795f(activity2, "4.6.0")) {
            C3082f.m7634c("openSDK_LOG.QQShare", "doShareToQQ, qqver below 4.6.");
            if (mo34804a(intent)) {
                i = 0;
                UIListenerManager.getInstance().setListenerWithRequestcode(Constants.REQUEST_OLD_SHARE, iUiListener);
                mo34801a(activity2, intent, (int) Constants.REQUEST_OLD_SHARE);
            } else {
                i = 0;
            }
            i3 = i6;
            i2 = 1;
        } else {
            i = 0;
            C3082f.m7634c("openSDK_LOG.QQShare", "doShareToQQ, qqver greater than 4.6.");
            if (UIListenerManager.getInstance().setListnerWithAction("shareToQQ", iUiListener) != null) {
                C3082f.m7634c("openSDK_LOG.QQShare", "doShareToQQ, last listener is not null, cancel it.");
            }
            if (mo34804a(intent)) {
                i2 = 1;
                mo34800a(activity2, (int) Constants.REQUEST_QQ_SHARE, intent, true);
            } else {
                i2 = 1;
            }
            i3 = i6;
        }
        String str5 = i3 == i2 ? Constants.VIA_REPORT_TYPE_SHARE_TO_QZONE : Constants.VIA_REPORT_TYPE_SHARE_TO_QQ;
        if (mo34804a(intent)) {
            C3091d.m7665a().mo35124a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_SHARE_TO_QQ, str5, "3", "0", this.mViaShareQQType, "0", "1", "0");
            C3091d.m7665a().mo35121a(0, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(i), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "");
        } else {
            C3091d.m7665a().mo35124a(this.f6457c.getOpenId(), this.f6457c.getAppId(), Constants.VIA_SHARE_TO_QQ, str5, "3", "1", this.mViaShareQQType, "0", "1", "0");
            C3091d.m7665a().mo35121a(1, "SHARE_CHECK_SDK", Constants.DEFAULT_UIN, this.f6457c.getAppId(), String.valueOf(i), Long.valueOf(SystemClock.elapsedRealtime()), 0, 1, "hasActivityForIntent fail");
        }
        C3082f.m7634c("openSDK_LOG.QQShare", "doShareToQQ() --end");
    }
}
