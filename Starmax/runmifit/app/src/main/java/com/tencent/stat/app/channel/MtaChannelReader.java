package com.tencent.stat.app.channel;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.text.TextUtils;
import com.tencent.stat.apkreader.ChannelInfo;
import com.tencent.stat.apkreader.ChannelReader;
import java.io.File;
import java.util.Map;

public final class MtaChannelReader {
    private MtaChannelReader() {
    }

    public static String getChannel(Context context) {
        return getChannel(context, null);
    }

    public static String getChannel(Context context, String str) {
        ChannelInfo channelInfo = getChannelInfo(context);
        if (channelInfo == null) {
            return str;
        }
        return channelInfo.getChannel();
    }

    public static ChannelInfo getChannelInfo(Context context) {
        String a = m7933a(context);
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        return ChannelReader.get(new File(a));
    }

    public static String get(Context context, String str) {
        Map<String, String> channelInfoMap = getChannelInfoMap(context);
        if (channelInfoMap == null) {
            return null;
        }
        return channelInfoMap.get(str);
    }

    public static Map<String, String> getChannelInfoMap(Context context) {
        String a = m7933a(context);
        if (TextUtils.isEmpty(a)) {
            return null;
        }
        return ChannelReader.getMap(new File(a));
    }

    /* renamed from: a */
    private static String m7933a(Context context) {
        try {
            ApplicationInfo applicationInfo = context.getApplicationInfo();
            if (applicationInfo == null) {
                return null;
            }
            return applicationInfo.sourceDir;
        } catch (Throwable unused) {
            return null;
        }
    }
}
