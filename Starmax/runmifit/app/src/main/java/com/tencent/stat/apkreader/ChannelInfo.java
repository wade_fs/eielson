package com.tencent.stat.apkreader;

import java.util.Map;

public class ChannelInfo {

    /* renamed from: a */
    private final String f7164a;

    /* renamed from: b */
    private final Map<String, String> f7165b;

    public ChannelInfo(String str, Map<String, String> map) {
        this.f7164a = str;
        this.f7165b = map;
    }

    public String getChannel() {
        return this.f7164a;
    }

    public Map<String, String> getExtraInfo() {
        return this.f7165b;
    }
}
