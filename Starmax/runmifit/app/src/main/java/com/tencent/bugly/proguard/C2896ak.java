package com.tencent.bugly.proguard;

import android.content.Context;
import android.os.Process;
import android.util.Base64;
import com.tencent.bugly.C2797b;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: com.tencent.bugly.proguard.ak */
/* compiled from: BUGLY */
public class C2896ak {

    /* renamed from: c */
    private static C2896ak f5974c;

    /* renamed from: a */
    public C2895aj f5975a;

    /* renamed from: b */
    public boolean f5976b = true;

    /* renamed from: d */
    private final C2889ae f5977d;

    /* renamed from: e */
    private final Context f5978e;

    /* renamed from: f */
    private Map<Integer, Long> f5979f = new HashMap();

    /* renamed from: g */
    private long f5980g;

    /* renamed from: h */
    private long f5981h;

    /* renamed from: i */
    private LinkedBlockingQueue<Runnable> f5982i = new LinkedBlockingQueue<>();

    /* renamed from: j */
    private LinkedBlockingQueue<Runnable> f5983j = new LinkedBlockingQueue<>();
    /* access modifiers changed from: private */

    /* renamed from: k */
    public final Object f5984k = new Object();

    /* renamed from: l */
    private String f5985l = null;

    /* renamed from: m */
    private byte[] f5986m = null;

    /* renamed from: n */
    private long f5987n = 0;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public byte[] f5988o = null;

    /* renamed from: p */
    private long f5989p = 0;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public String f5990q = null;

    /* renamed from: r */
    private long f5991r = 0;

    /* renamed from: s */
    private final Object f5992s = new Object();
    /* access modifiers changed from: private */

    /* renamed from: t */
    public boolean f5993t = false;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public final Object f5994u = new Object();

    /* renamed from: v */
    private int f5995v = 0;

    /* renamed from: b */
    static /* synthetic */ int m6815b(C2896ak akVar) {
        int i = akVar.f5995v - 1;
        akVar.f5995v = i;
        return i;
    }

    protected C2896ak(Context context) {
        this.f5978e = context;
        this.f5977d = C2889ae.m6757a();
        try {
            Class.forName("android.util.Base64");
        } catch (ClassNotFoundException unused) {
            C2903an.m6857a("[UploadManager] Error: Can not find Base64 class, will not use stronger security way to upload", new Object[0]);
            this.f5976b = false;
        }
        if (this.f5976b) {
            this.f5985l = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDP9x32s5pPtZBXzJBz2GWM/" + "sbTvVO2+RvW0PH01IdaBxc/" + "fB6fbHZocC9T3nl1+" + "J5eAFjIRVuV8vHDky7Qo82Mnh0PVvcZIEQvMMVKU8dsMQopxgsOs2gkSHJwgWdinKNS8CmWobo6pFwPUW11lMv714jAUZRq2GBOqiO2vQI6iwIDAQAB";
        }
    }

    /* renamed from: a */
    public static synchronized C2896ak m6806a(Context context) {
        C2896ak akVar;
        synchronized (C2896ak.class) {
            if (f5974c == null) {
                f5974c = new C2896ak(context);
            }
            akVar = f5974c;
        }
        return akVar;
    }

    /* renamed from: a */
    public static synchronized C2896ak m6805a() {
        C2896ak akVar;
        synchronized (C2896ak.class) {
            akVar = f5974c;
        }
        return akVar;
    }

    /* renamed from: a */
    public void mo34524a(int i, int i2, byte[] bArr, String str, String str2, C2895aj ajVar, long j, boolean z) {
        try {
            m6816b(new C2900al(this.f5978e, i, i2, bArr, str, str2, ajVar, this.f5976b, z), true, true, j);
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo34527a(int i, C2927bh bhVar, String str, String str2, C2895aj ajVar, long j, boolean z) {
        mo34524a(i, bhVar.f6121g, C2893ah.m6795a((Object) bhVar), str, str2, ajVar, j, z);
    }

    /* renamed from: a */
    public void mo34523a(int i, int i2, byte[] bArr, String str, String str2, C2895aj ajVar, int i3, int i4, boolean z, Map<String, String> map) {
        try {
            m6816b(new C2900al(this.f5978e, i, i2, bArr, str, str2, ajVar, this.f5976b, i3, i4, false, map), z, false, 0);
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public void mo34525a(int i, int i2, byte[] bArr, String str, String str2, C2895aj ajVar, boolean z, Map<String, String> map) {
        mo34523a(i, i2, bArr, str, str2, ajVar, 0, 0, z, map);
    }

    /* renamed from: a */
    public void mo34528a(int i, C2927bh bhVar, String str, String str2, C2895aj ajVar, boolean z) {
        mo34523a(i, bhVar.f6121g, C2893ah.m6795a((Object) bhVar), str, str2, ajVar, 0, 0, z, null);
    }

    /* renamed from: a */
    public long mo34522a(boolean z) {
        long j;
        long b = C2908aq.m6923b();
        int i = z ? 5 : 3;
        List<C2892ag> a = this.f5977d.mo34493a(i);
        if (a == null || a.size() <= 0) {
            j = z ? this.f5981h : this.f5980g;
        } else {
            j = 0;
            try {
                C2892ag agVar = a.get(0);
                if (agVar.f5968e >= b) {
                    j = C2908aq.m6937c(agVar.f5970g);
                    if (i == 3) {
                        this.f5980g = j;
                    } else {
                        this.f5981h = j;
                    }
                    a.remove(agVar);
                }
            } catch (Throwable th) {
                C2903an.m6858a(th);
            }
            if (a.size() > 0) {
                this.f5977d.mo34495a(a);
            }
        }
        C2903an.m6863c("[UploadManager] Local network consume: %d KB", Long.valueOf(j / 1024));
        return j;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public synchronized void mo34530a(long j, boolean z) {
        int i = z ? 5 : 3;
        C2892ag agVar = new C2892ag();
        agVar.f5965b = i;
        agVar.f5968e = C2908aq.m6923b();
        agVar.f5966c = "";
        agVar.f5967d = "";
        agVar.f5970g = C2908aq.m6941c(j);
        this.f5977d.mo34501b(i);
        this.f5977d.mo34498a(agVar);
        if (z) {
            this.f5981h = j;
        } else {
            this.f5980g = j;
        }
        C2903an.m6863c("[UploadManager] Network total consume: %d KB", Long.valueOf(j / 1024));
    }

    /* renamed from: a */
    public synchronized void mo34526a(int i, long j) {
        if (i >= 0) {
            this.f5979f.put(Integer.valueOf(i), Long.valueOf(j));
            C2892ag agVar = new C2892ag();
            agVar.f5965b = i;
            agVar.f5968e = j;
            agVar.f5966c = "";
            agVar.f5967d = "";
            agVar.f5970g = new byte[0];
            this.f5977d.mo34501b(i);
            this.f5977d.mo34498a(agVar);
            C2903an.m6863c("[UploadManager] Uploading(ID:%d) time: %s", Integer.valueOf(i), C2908aq.m6898a(j));
        } else {
            C2903an.m6865e("[UploadManager] Unknown uploading ID: %d", Integer.valueOf(i));
        }
    }

    /* renamed from: a */
    public synchronized long mo34521a(int i) {
        long j;
        j = 0;
        if (i >= 0) {
            Long l = this.f5979f.get(Integer.valueOf(i));
            if (l != null) {
                return l.longValue();
            }
            List<C2892ag> a = this.f5977d.mo34493a(i);
            if (a != null && a.size() > 0) {
                if (a.size() > 1) {
                    for (C2892ag agVar : a) {
                        if (agVar.f5968e > j) {
                            j = agVar.f5968e;
                        }
                    }
                    this.f5977d.mo34501b(i);
                } else {
                    try {
                        j = a.get(0).f5968e;
                    } catch (Throwable th) {
                        C2903an.m6858a(th);
                    }
                }
            }
        } else {
            C2903an.m6865e("[UploadManager] Unknown upload ID: %d", Integer.valueOf(i));
        }
        return j;
    }

    /* renamed from: b */
    public boolean mo34535b(int i) {
        if (C2797b.f5303c) {
            C2903an.m6863c("Uploading frequency will not be checked if SDK is in debug mode.", new Object[0]);
            return true;
        }
        long currentTimeMillis = System.currentTimeMillis() - mo34521a(i);
        C2903an.m6863c("[UploadManager] Time interval is %d seconds since last uploading(ID: %d).", Long.valueOf(currentTimeMillis / 1000), Integer.valueOf(i));
        if (currentTimeMillis >= 30000) {
            return true;
        }
        C2903an.m6857a("[UploadManager] Data only be uploaded once in %d seconds.", 30L);
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad, boolean):boolean
     arg types: [int, java.lang.String, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad, boolean):long
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad, boolean):boolean */
    /* renamed from: c */
    private boolean m6818c() {
        C2903an.m6863c("[UploadManager] Drop security info of database (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            C2889ae a = C2889ae.m6757a();
            if (a != null) {
                return a.mo34496a(555, "security_info", (C2888ad) null, true);
            }
            C2903an.m6864d("[UploadManager] Failed to get Database", new Object[0]);
            return false;
        } catch (Throwable th) {
            C2903an.m6858a(th);
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean
     arg types: [int, java.lang.String, byte[], ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad):int
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, java.lang.String, byte[], com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(java.lang.String, java.lang.String, java.lang.String[], com.tencent.bugly.proguard.ad, boolean):int
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, byte[], com.tencent.bugly.proguard.ad, boolean):boolean */
    /* renamed from: d */
    private boolean m6821d() {
        C2903an.m6863c("[UploadManager] Record security info to database (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            C2889ae a = C2889ae.m6757a();
            if (a == null) {
                C2903an.m6864d("[UploadManager] Failed to get database", new Object[0]);
                return false;
            }
            StringBuilder sb = new StringBuilder();
            if (this.f5988o != null) {
                sb.append(Base64.encodeToString(this.f5988o, 0));
                sb.append("#");
                if (this.f5989p != 0) {
                    sb.append(Long.toString(this.f5989p));
                } else {
                    sb.append("null");
                }
                sb.append("#");
                if (this.f5990q != null) {
                    sb.append(this.f5990q);
                } else {
                    sb.append("null");
                }
                sb.append("#");
                if (this.f5991r != 0) {
                    sb.append(Long.toString(this.f5991r));
                } else {
                    sb.append("null");
                }
                a.mo34497a(555, "security_info", sb.toString().getBytes(), (C2888ad) null, true);
                return true;
            }
            C2903an.m6863c("[UploadManager] AES key is null, will not record", new Object[0]);
            return false;
        } catch (Throwable th) {
            C2903an.m6858a(th);
            m6818c();
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]>
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, com.tencent.bugly.proguard.ad):java.util.Map
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]> */
    /* access modifiers changed from: private */
    /* renamed from: e */
    public boolean m6823e() {
        boolean z;
        C2903an.m6863c("[UploadManager] Load security info from database (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        try {
            C2889ae a = C2889ae.m6757a();
            if (a == null) {
                C2903an.m6864d("[UploadManager] Failed to get database", new Object[0]);
                return false;
            }
            Map<String, byte[]> a2 = a.mo34494a(555, (C2888ad) null, true);
            if (a2 != null && a2.containsKey("security_info")) {
                String str = new String(a2.get("security_info"));
                String[] split = str.split("#");
                if (split.length == 4) {
                    if (!split[0].isEmpty()) {
                        if (!split[0].equals("null")) {
                            this.f5988o = Base64.decode(split[0], 0);
                        }
                    }
                    z = false;
                    if (!z && !split[1].isEmpty() && !split[1].equals("null")) {
                        try {
                            this.f5989p = Long.parseLong(split[1]);
                        } catch (Throwable th) {
                            C2903an.m6858a(th);
                            z = true;
                        }
                    }
                    if (!z && !split[2].isEmpty() && !split[2].equals("null")) {
                        this.f5990q = split[2];
                    }
                    if (!z && !split[3].isEmpty() && !split[3].equals("null")) {
                        try {
                            this.f5991r = Long.parseLong(split[3]);
                        } catch (Throwable th2) {
                            C2903an.m6858a(th2);
                        }
                    }
                } else {
                    C2903an.m6857a("SecurityInfo = %s, Strings.length = %d", str, Integer.valueOf(split.length));
                    z = true;
                }
                if (z) {
                    m6818c();
                }
            }
            return true;
        } catch (Throwable th3) {
            C2903an.m6858a(th3);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo34534b() {
        if (this.f5990q == null || this.f5991r == 0) {
            return false;
        }
        long currentTimeMillis = System.currentTimeMillis() + this.f5987n;
        long j = this.f5991r;
        if (j >= currentTimeMillis) {
            return true;
        }
        C2903an.m6863c("[UploadManager] Session ID expired time from server is: %d(%s), but now is: %d(%s)", Long.valueOf(j), new Date(this.f5991r).toString(), Long.valueOf(currentTimeMillis), new Date(currentTimeMillis).toString());
        return false;
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo34533b(boolean z) {
        synchronized (this.f5992s) {
            C2903an.m6863c("[UploadManager] Clear security context (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            this.f5988o = null;
            this.f5990q = null;
            this.f5991r = 0;
        }
        if (z) {
            m6818c();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ak.a(java.lang.Runnable, boolean):boolean
     arg types: [java.lang.Runnable, int]
     candidates:
      com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, int):void
      com.tencent.bugly.proguard.ak.a(java.lang.Runnable, long):void
      com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, boolean):boolean
      com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, byte[]):byte[]
      com.tencent.bugly.proguard.ak.a(int, long):void
      com.tencent.bugly.proguard.ak.a(int, com.tencent.bugly.proguard.bi):void
      com.tencent.bugly.proguard.ak.a(long, boolean):void
      com.tencent.bugly.proguard.ak.a(java.lang.Runnable, boolean):boolean */
    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:111:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:112:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x00bc, code lost:
        if (r5 <= 0) goto L_0x00df;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00be, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Execute urgent upload tasks of queue which has %d tasks (pid=%d | tid=%d)", java.lang.Integer.valueOf(r5), java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x00df, code lost:
        r7 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00e0, code lost:
        if (r7 >= r5) goto L_0x012f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00e2, code lost:
        r8 = (java.lang.Runnable) r2.poll();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x00e8, code lost:
        if (r8 != null) goto L_0x00eb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00eb, code lost:
        r10 = r12.f5984k;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00ed, code lost:
        monitor-enter(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00f0, code lost:
        if (r12.f5995v < 2) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00f2, code lost:
        if (r1 == null) goto L_0x00f9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00f4, code lost:
        r1.mo34547a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00f7, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00f9, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x00fa, code lost:
        com.tencent.bugly.proguard.C2903an.m6857a("[UploadManager] Create and start a new thread to execute a upload task: %s", "BUGLY_ASYNC_UPLOAD");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:67:0x0110, code lost:
        if (com.tencent.bugly.proguard.C2908aq.m6905a(new com.tencent.bugly.proguard.C2896ak.C28971(r12), "BUGLY_ASYNC_UPLOAD") == null) goto L_0x011f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x0112, code lost:
        r10 = r12.f5984k;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x0114, code lost:
        monitor-enter(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:?, code lost:
        r12.f5995v++;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:72:0x011a, code lost:
        monitor-exit(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x011f, code lost:
        com.tencent.bugly.proguard.C2903an.m6864d("[UploadManager] Failed to start a thread to execute asynchronous upload task, will try again next time.", new java.lang.Object[0]);
        m6813a(r8, true);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0129, code lost:
        r7 = r7 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x012f, code lost:
        if (r13 <= 0) goto L_0x0152;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0131, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Execute upload tasks of queue which has %d tasks (pid=%d | tid=%d)", java.lang.Integer.valueOf(r13), java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x0152, code lost:
        if (r1 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x0154, code lost:
        r1.mo34547a(new com.tencent.bugly.proguard.C2896ak.C28982(r12));
     */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006d A[Catch:{ all -> 0x0081 }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0095 A[Catch:{ all -> 0x0081 }] */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m6817c(int r13) {
        /*
            r12 = this;
            r0 = 0
            if (r13 >= 0) goto L_0x000b
            java.lang.Object[] r13 = new java.lang.Object[r0]
            java.lang.String r0 = "[UploadManager] Number of task to execute should >= 0"
            com.tencent.bugly.proguard.C2903an.m6857a(r0, r13)
            return
        L_0x000b:
            com.tencent.bugly.proguard.am r1 = com.tencent.bugly.proguard.C2901am.m6848a()
            java.util.concurrent.LinkedBlockingQueue r2 = new java.util.concurrent.LinkedBlockingQueue
            r2.<init>()
            java.util.concurrent.LinkedBlockingQueue r3 = new java.util.concurrent.LinkedBlockingQueue
            r3.<init>()
            java.lang.Object r4 = r12.f5984k
            monitor-enter(r4)
            java.lang.String r5 = "[UploadManager] Try to poll all upload task need and put them into temp queue (pid=%d | tid=%d)"
            r6 = 2
            java.lang.Object[] r7 = new java.lang.Object[r6]     // Catch:{ all -> 0x015d }
            int r8 = android.os.Process.myPid()     // Catch:{ all -> 0x015d }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x015d }
            r7[r0] = r8     // Catch:{ all -> 0x015d }
            int r8 = android.os.Process.myTid()     // Catch:{ all -> 0x015d }
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)     // Catch:{ all -> 0x015d }
            r9 = 1
            r7[r9] = r8     // Catch:{ all -> 0x015d }
            com.tencent.bugly.proguard.C2903an.m6863c(r5, r7)     // Catch:{ all -> 0x015d }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r5 = r12.f5982i     // Catch:{ all -> 0x015d }
            int r5 = r5.size()     // Catch:{ all -> 0x015d }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r7 = r12.f5983j     // Catch:{ all -> 0x015d }
            int r7 = r7.size()     // Catch:{ all -> 0x015d }
            if (r5 != 0) goto L_0x0052
            if (r7 != 0) goto L_0x0052
            java.lang.String r13 = "[UploadManager] There is no upload task in queue."
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x015d }
            com.tencent.bugly.proguard.C2903an.m6863c(r13, r0)     // Catch:{ all -> 0x015d }
            monitor-exit(r4)     // Catch:{ all -> 0x015d }
            return
        L_0x0052:
            if (r13 != 0) goto L_0x0055
            goto L_0x0060
        L_0x0055:
            if (r13 >= r5) goto L_0x005a
            r5 = r13
            r13 = 0
            goto L_0x0061
        L_0x005a:
            int r8 = r5 + r7
            if (r13 >= r8) goto L_0x0060
            int r13 = r13 - r5
            goto L_0x0061
        L_0x0060:
            r13 = r7
        L_0x0061:
            if (r1 == 0) goto L_0x0069
            boolean r7 = r1.mo34550c()     // Catch:{ all -> 0x015d }
            if (r7 != 0) goto L_0x006a
        L_0x0069:
            r13 = 0
        L_0x006a:
            r7 = 0
        L_0x006b:
            if (r7 >= r5) goto L_0x0092
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f5982i     // Catch:{ all -> 0x015d }
            java.lang.Object r8 = r8.peek()     // Catch:{ all -> 0x015d }
            java.lang.Runnable r8 = (java.lang.Runnable) r8     // Catch:{ all -> 0x015d }
            if (r8 != 0) goto L_0x0078
            goto L_0x0092
        L_0x0078:
            r2.put(r8)     // Catch:{ all -> 0x0081 }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f5982i     // Catch:{ all -> 0x0081 }
            r8.poll()     // Catch:{ all -> 0x0081 }
            goto L_0x008f
        L_0x0081:
            r8 = move-exception
            java.lang.String r10 = "[UploadManager] Failed to add upload task to temp urgent queue: %s"
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x015d }
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x015d }
            r11[r0] = r8     // Catch:{ all -> 0x015d }
            com.tencent.bugly.proguard.C2903an.m6865e(r10, r11)     // Catch:{ all -> 0x015d }
        L_0x008f:
            int r7 = r7 + 1
            goto L_0x006b
        L_0x0092:
            r7 = 0
        L_0x0093:
            if (r7 >= r13) goto L_0x00ba
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f5983j     // Catch:{ all -> 0x015d }
            java.lang.Object r8 = r8.peek()     // Catch:{ all -> 0x015d }
            java.lang.Runnable r8 = (java.lang.Runnable) r8     // Catch:{ all -> 0x015d }
            if (r8 != 0) goto L_0x00a0
            goto L_0x00ba
        L_0x00a0:
            r3.put(r8)     // Catch:{ all -> 0x00a9 }
            java.util.concurrent.LinkedBlockingQueue<java.lang.Runnable> r8 = r12.f5983j     // Catch:{ all -> 0x00a9 }
            r8.poll()     // Catch:{ all -> 0x00a9 }
            goto L_0x00b7
        L_0x00a9:
            r8 = move-exception
            java.lang.String r10 = "[UploadManager] Failed to add upload task to temp urgent queue: %s"
            java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ all -> 0x015d }
            java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x015d }
            r11[r0] = r8     // Catch:{ all -> 0x015d }
            com.tencent.bugly.proguard.C2903an.m6865e(r10, r11)     // Catch:{ all -> 0x015d }
        L_0x00b7:
            int r7 = r7 + 1
            goto L_0x0093
        L_0x00ba:
            monitor-exit(r4)     // Catch:{ all -> 0x015d }
            r4 = 3
            if (r5 <= 0) goto L_0x00df
            java.lang.Object[] r7 = new java.lang.Object[r4]
            java.lang.Integer r8 = java.lang.Integer.valueOf(r5)
            r7[r0] = r8
            int r8 = android.os.Process.myPid()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r9] = r8
            int r8 = android.os.Process.myTid()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            r7[r6] = r8
            java.lang.String r8 = "[UploadManager] Execute urgent upload tasks of queue which has %d tasks (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C2903an.m6863c(r8, r7)
        L_0x00df:
            r7 = 0
        L_0x00e0:
            if (r7 >= r5) goto L_0x012f
            java.lang.Object r8 = r2.poll()
            java.lang.Runnable r8 = (java.lang.Runnable) r8
            if (r8 != 0) goto L_0x00eb
            goto L_0x012f
        L_0x00eb:
            java.lang.Object r10 = r12.f5984k
            monitor-enter(r10)
            int r11 = r12.f5995v     // Catch:{ all -> 0x012c }
            if (r11 < r6) goto L_0x00f9
            if (r1 == 0) goto L_0x00f9
            r1.mo34547a(r8)     // Catch:{ all -> 0x012c }
            monitor-exit(r10)     // Catch:{ all -> 0x012c }
            goto L_0x0129
        L_0x00f9:
            monitor-exit(r10)     // Catch:{ all -> 0x012c }
            java.lang.Object[] r10 = new java.lang.Object[r9]
            java.lang.String r11 = "BUGLY_ASYNC_UPLOAD"
            r10[r0] = r11
            java.lang.String r11 = "[UploadManager] Create and start a new thread to execute a upload task: %s"
            com.tencent.bugly.proguard.C2903an.m6857a(r11, r10)
            com.tencent.bugly.proguard.ak$1 r10 = new com.tencent.bugly.proguard.ak$1
            r10.<init>(r8)
            java.lang.String r11 = "BUGLY_ASYNC_UPLOAD"
            java.lang.Thread r10 = com.tencent.bugly.proguard.C2908aq.m6905a(r10, r11)
            if (r10 == 0) goto L_0x011f
            java.lang.Object r10 = r12.f5984k
            monitor-enter(r10)
            int r8 = r12.f5995v     // Catch:{ all -> 0x011c }
            int r8 = r8 + r9
            r12.f5995v = r8     // Catch:{ all -> 0x011c }
            monitor-exit(r10)     // Catch:{ all -> 0x011c }
            goto L_0x0129
        L_0x011c:
            r13 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x011c }
            throw r13
        L_0x011f:
            java.lang.Object[] r10 = new java.lang.Object[r0]
            java.lang.String r11 = "[UploadManager] Failed to start a thread to execute asynchronous upload task, will try again next time."
            com.tencent.bugly.proguard.C2903an.m6864d(r11, r10)
            r12.m6813a(r8, r9)
        L_0x0129:
            int r7 = r7 + 1
            goto L_0x00e0
        L_0x012c:
            r13 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x012c }
            throw r13
        L_0x012f:
            if (r13 <= 0) goto L_0x0152
            java.lang.Object[] r2 = new java.lang.Object[r4]
            java.lang.Integer r4 = java.lang.Integer.valueOf(r13)
            r2[r0] = r4
            int r0 = android.os.Process.myPid()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r9] = r0
            int r0 = android.os.Process.myTid()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            r2[r6] = r0
            java.lang.String r0 = "[UploadManager] Execute upload tasks of queue which has %d tasks (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C2903an.m6863c(r0, r2)
        L_0x0152:
            if (r1 == 0) goto L_0x015c
            com.tencent.bugly.proguard.ak$2 r0 = new com.tencent.bugly.proguard.ak$2
            r0.<init>(r13, r3)
            r1.mo34547a(r0)
        L_0x015c:
            return
        L_0x015d:
            r13 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x015d }
            goto L_0x0161
        L_0x0160:
            throw r13
        L_0x0161:
            goto L_0x0160
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2896ak.m6817c(int):void");
    }

    /* renamed from: a */
    private boolean m6813a(Runnable runnable, boolean z) {
        if (runnable == null) {
            C2903an.m6857a("[UploadManager] Upload task should not be null", new Object[0]);
            return false;
        }
        try {
            C2903an.m6863c("[UploadManager] Add upload task to queue (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            synchronized (this.f5984k) {
                if (z) {
                    this.f5982i.put(runnable);
                } else {
                    this.f5983j.put(runnable);
                }
            }
            return true;
        } catch (Throwable th) {
            C2903an.m6865e("[UploadManager] Failed to add upload task to queue: %s", th.getMessage());
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ak.a(java.lang.Runnable, boolean):boolean
     arg types: [java.lang.Runnable, int]
     candidates:
      com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, int):void
      com.tencent.bugly.proguard.ak.a(java.lang.Runnable, long):void
      com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, boolean):boolean
      com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, byte[]):byte[]
      com.tencent.bugly.proguard.ak.a(int, long):void
      com.tencent.bugly.proguard.ak.a(int, com.tencent.bugly.proguard.bi):void
      com.tencent.bugly.proguard.ak.a(long, boolean):void
      com.tencent.bugly.proguard.ak.a(java.lang.Runnable, boolean):boolean */
    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m6810a(Runnable runnable, long j) {
        if (runnable == null) {
            C2903an.m6864d("[UploadManager] Upload task should not be null", new Object[0]);
            return;
        }
        C2903an.m6863c("[UploadManager] Execute synchronized upload task (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        Thread a = C2908aq.m6905a(runnable, "BUGLY_SYNC_UPLOAD");
        if (a == null) {
            C2903an.m6865e("[UploadManager] Failed to start a thread to execute synchronized upload task, add it to queue.", new Object[0]);
            m6813a(runnable, true);
            return;
        }
        try {
            a.join(j);
        } catch (Throwable th) {
            C2903an.m6865e("[UploadManager] Failed to join upload synchronized task with message: %s. Add it to queue.", th.getMessage());
            m6813a(runnable, true);
            m6817c(0);
        }
    }

    /* renamed from: a */
    private void m6811a(Runnable runnable, boolean z, boolean z2, long j) {
        C2903an.m6863c("[UploadManager] Initialize security context now (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        if (z2) {
            m6810a(new C2899a(this.f5978e, runnable, j), 0);
            return;
        }
        m6813a(runnable, z);
        C2899a aVar = new C2899a(this.f5978e);
        C2903an.m6857a("[UploadManager] Create and start a new thread to execute a task of initializing security context: %s", "BUGLY_ASYNC_UPLOAD");
        if (C2908aq.m6905a(aVar, "BUGLY_ASYNC_UPLOAD") == null) {
            C2903an.m6864d("[UploadManager] Failed to start a thread to execute task of initializing security context, try to post it into thread pool.", new Object[0]);
            C2901am a = C2901am.m6848a();
            if (a != null) {
                a.mo34547a(aVar);
                return;
            }
            C2903an.m6865e("[UploadManager] Asynchronous thread pool is unavailable now, try next time.", new Object[0]);
            synchronized (this.f5994u) {
                this.f5993t = false;
            }
        }
    }

    /* renamed from: b */
    private void m6816b(Runnable runnable, boolean z, boolean z2, long j) {
        if (runnable == null) {
            C2903an.m6864d("[UploadManager] Upload task should not be null", new Object[0]);
        }
        C2903an.m6863c("[UploadManager] Add upload task (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        if (this.f5990q != null) {
            if (mo34534b()) {
                C2903an.m6863c("[UploadManager] Sucessfully got session ID, try to execute upload task now (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                if (z2) {
                    m6810a(runnable, j);
                    return;
                }
                m6813a(runnable, z);
                m6817c(0);
                return;
            }
            C2903an.m6857a("[UploadManager] Session ID is expired, drop it (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
            mo34533b(false);
        }
        synchronized (this.f5994u) {
            if (this.f5993t) {
                m6813a(runnable, z);
                return;
            }
            this.f5993t = true;
            m6811a(runnable, z, z2, j);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0034, code lost:
        if (r9 == null) goto L_0x0107;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Record security context (pid=%d | tid=%d)", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:?, code lost:
        r8 = r9.f6148h;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0053, code lost:
        if (r8 == null) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x005b, code lost:
        if (r8.containsKey("S1") == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0063, code lost:
        if (r8.containsKey("S2") == false) goto L_0x0101;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0065, code lost:
        r7.f5987n = r9.f6145e - java.lang.System.currentTimeMillis();
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Time lag of server is: %d", java.lang.Long.valueOf(r7.f5987n));
        r7.f5990q = r8.get("S1");
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Session ID from server is: %s", r7.f5990q);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0098, code lost:
        if (r7.f5990q.length() <= 0) goto L_0x00f5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
        r7.f5991r = java.lang.Long.parseLong(r8.get("S2"));
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Session expired time from server is: %d(%s)", java.lang.Long.valueOf(r7.f5991r), new java.util.Date(r7.f5991r).toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x00cd, code lost:
        if (r7.f5991r >= 1000) goto L_0x00e2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00cf, code lost:
        com.tencent.bugly.proguard.C2903an.m6864d("[UploadManager] Session expired time from server is less than 1 second, will set to default value", new java.lang.Object[0]);
        r7.f5991r = 259200000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        com.tencent.bugly.proguard.C2903an.m6864d("[UploadManager] Session expired time is invalid, will set to default value", new java.lang.Object[0]);
        r7.f5991r = 259200000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00f5, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Session ID from server is invalid, try next time", new java.lang.Object[0]);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00fd, code lost:
        r8 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00fe, code lost:
        com.tencent.bugly.proguard.C2903an.m6858a(r8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0107, code lost:
        com.tencent.bugly.proguard.C2903an.m6863c("[UploadManager] Fail to init security context and clear local info (pid=%d | tid=%d)", java.lang.Integer.valueOf(android.os.Process.myPid()), java.lang.Integer.valueOf(android.os.Process.myTid()));
        mo34533b(false);
     */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0103  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0128 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo34529a(int r8, com.tencent.bugly.proguard.C2928bi r9) {
        /*
            r7 = this;
            boolean r0 = r7.f5976b
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            r0 = 2
            r1 = 1
            r2 = 0
            if (r8 != r0) goto L_0x002a
            java.lang.Object[] r8 = new java.lang.Object[r0]
            int r9 = android.os.Process.myPid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r2] = r9
            int r9 = android.os.Process.myTid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r1] = r9
            java.lang.String r9 = "[UploadManager] Session ID is invalid, will clear security context (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C2903an.m6863c(r9, r8)
            r7.mo34533b(r1)
            goto L_0x0125
        L_0x002a:
            java.lang.Object r8 = r7.f5994u
            monitor-enter(r8)
            boolean r3 = r7.f5993t     // Catch:{ all -> 0x013a }
            if (r3 != 0) goto L_0x0033
            monitor-exit(r8)     // Catch:{ all -> 0x013a }
            return
        L_0x0033:
            monitor-exit(r8)     // Catch:{ all -> 0x013a }
            if (r9 == 0) goto L_0x0107
            java.lang.Object[] r8 = new java.lang.Object[r0]
            int r3 = android.os.Process.myPid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r8[r2] = r3
            int r3 = android.os.Process.myTid()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            r8[r1] = r3
            java.lang.String r3 = "[UploadManager] Record security context (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C2903an.m6863c(r3, r8)
            java.util.Map<java.lang.String, java.lang.String> r8 = r9.f6148h     // Catch:{ all -> 0x00fd }
            if (r8 == 0) goto L_0x0101
            java.lang.String r3 = "S1"
            boolean r3 = r8.containsKey(r3)     // Catch:{ all -> 0x00fd }
            if (r3 == 0) goto L_0x0101
            java.lang.String r3 = "S2"
            boolean r3 = r8.containsKey(r3)     // Catch:{ all -> 0x00fd }
            if (r3 == 0) goto L_0x0101
            long r3 = r9.f6145e     // Catch:{ all -> 0x00fd }
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00fd }
            long r3 = r3 - r5
            r7.f5987n = r3     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = "[UploadManager] Time lag of server is: %d"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00fd }
            long r4 = r7.f5987n     // Catch:{ all -> 0x00fd }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x00fd }
            r3[r2] = r4     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C2903an.m6863c(r9, r3)     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = "S1"
            java.lang.Object r9 = r8.get(r9)     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ all -> 0x00fd }
            r7.f5990q = r9     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = "[UploadManager] Session ID from server is: %s"
            java.lang.Object[] r3 = new java.lang.Object[r1]     // Catch:{ all -> 0x00fd }
            java.lang.String r4 = r7.f5990q     // Catch:{ all -> 0x00fd }
            r3[r2] = r4     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C2903an.m6863c(r9, r3)     // Catch:{ all -> 0x00fd }
            java.lang.String r9 = r7.f5990q     // Catch:{ all -> 0x00fd }
            int r9 = r9.length()     // Catch:{ all -> 0x00fd }
            if (r9 <= 0) goto L_0x00f5
            r3 = 259200000(0xf731400, double:1.280618154E-315)
            java.lang.String r9 = "S2"
            java.lang.Object r8 = r8.get(r9)     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ NumberFormatException -> 0x00d9 }
            long r8 = java.lang.Long.parseLong(r8)     // Catch:{ NumberFormatException -> 0x00d9 }
            r7.f5991r = r8     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.String r8 = "[UploadManager] Session expired time from server is: %d(%s)"
            java.lang.Object[] r9 = new java.lang.Object[r0]     // Catch:{ NumberFormatException -> 0x00d9 }
            long r5 = r7.f5991r     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.Long r0 = java.lang.Long.valueOf(r5)     // Catch:{ NumberFormatException -> 0x00d9 }
            r9[r2] = r0     // Catch:{ NumberFormatException -> 0x00d9 }
            java.util.Date r0 = new java.util.Date     // Catch:{ NumberFormatException -> 0x00d9 }
            long r5 = r7.f5991r     // Catch:{ NumberFormatException -> 0x00d9 }
            r0.<init>(r5)     // Catch:{ NumberFormatException -> 0x00d9 }
            java.lang.String r0 = r0.toString()     // Catch:{ NumberFormatException -> 0x00d9 }
            r9[r1] = r0     // Catch:{ NumberFormatException -> 0x00d9 }
            com.tencent.bugly.proguard.C2903an.m6863c(r8, r9)     // Catch:{ NumberFormatException -> 0x00d9 }
            long r8 = r7.f5991r     // Catch:{ NumberFormatException -> 0x00d9 }
            r5 = 1000(0x3e8, double:4.94E-321)
            int r0 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r0 >= 0) goto L_0x00e2
            java.lang.String r8 = "[UploadManager] Session expired time from server is less than 1 second, will set to default value"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ NumberFormatException -> 0x00d9 }
            com.tencent.bugly.proguard.C2903an.m6864d(r8, r9)     // Catch:{ NumberFormatException -> 0x00d9 }
            r7.f5991r = r3     // Catch:{ NumberFormatException -> 0x00d9 }
            goto L_0x00e2
        L_0x00d9:
            java.lang.String r8 = "[UploadManager] Session expired time is invalid, will set to default value"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C2903an.m6864d(r8, r9)     // Catch:{ all -> 0x00fd }
            r7.f5991r = r3     // Catch:{ all -> 0x00fd }
        L_0x00e2:
            boolean r8 = r7.m6821d()     // Catch:{ all -> 0x00fd }
            if (r8 == 0) goto L_0x00ea
            r1 = 0
            goto L_0x00f1
        L_0x00ea:
            java.lang.String r8 = "[UploadManager] Failed to record database"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C2903an.m6863c(r8, r9)     // Catch:{ all -> 0x00fd }
        L_0x00f1:
            r7.m6817c(r2)     // Catch:{ all -> 0x00fd }
            goto L_0x0101
        L_0x00f5:
            java.lang.String r8 = "[UploadManager] Session ID from server is invalid, try next time"
            java.lang.Object[] r9 = new java.lang.Object[r2]     // Catch:{ all -> 0x00fd }
            com.tencent.bugly.proguard.C2903an.m6863c(r8, r9)     // Catch:{ all -> 0x00fd }
            goto L_0x0101
        L_0x00fd:
            r8 = move-exception
            com.tencent.bugly.proguard.C2903an.m6858a(r8)
        L_0x0101:
            if (r1 == 0) goto L_0x0125
            r7.mo34533b(r2)
            goto L_0x0125
        L_0x0107:
            java.lang.Object[] r8 = new java.lang.Object[r0]
            int r9 = android.os.Process.myPid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r2] = r9
            int r9 = android.os.Process.myTid()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
            r8[r1] = r9
            java.lang.String r9 = "[UploadManager] Fail to init security context and clear local info (pid=%d | tid=%d)"
            com.tencent.bugly.proguard.C2903an.m6863c(r9, r8)
            r7.mo34533b(r2)
        L_0x0125:
            java.lang.Object r9 = r7.f5994u
            monitor-enter(r9)
            boolean r8 = r7.f5993t     // Catch:{ all -> 0x0137 }
            if (r8 == 0) goto L_0x0135
            r7.f5993t = r2     // Catch:{ all -> 0x0137 }
            android.content.Context r8 = r7.f5978e     // Catch:{ all -> 0x0137 }
            java.lang.String r0 = "security_info"
            com.tencent.bugly.proguard.C2908aq.m6939c(r8, r0)     // Catch:{ all -> 0x0137 }
        L_0x0135:
            monitor-exit(r9)     // Catch:{ all -> 0x0137 }
            return
        L_0x0137:
            r8 = move-exception
            monitor-exit(r9)     // Catch:{ all -> 0x0137 }
            throw r8
        L_0x013a:
            r9 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x013a }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.proguard.C2896ak.mo34529a(int, com.tencent.bugly.proguard.bi):void");
    }

    /* renamed from: com.tencent.bugly.proguard.ak$a */
    /* compiled from: BUGLY */
    class C2899a implements Runnable {

        /* renamed from: b */
        private final Context f6002b;

        /* renamed from: c */
        private final Runnable f6003c;

        /* renamed from: d */
        private final long f6004d;

        public C2899a(Context context) {
            this.f6002b = context;
            this.f6003c = null;
            this.f6004d = 0;
        }

        public C2899a(Context context, Runnable runnable, long j) {
            this.f6002b = context;
            this.f6003c = runnable;
            this.f6004d = j;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, boolean):boolean
         arg types: [com.tencent.bugly.proguard.ak, int]
         candidates:
          com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, int):void
          com.tencent.bugly.proguard.ak.a(java.lang.Runnable, long):void
          com.tencent.bugly.proguard.ak.a(java.lang.Runnable, boolean):boolean
          com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, byte[]):byte[]
          com.tencent.bugly.proguard.ak.a(int, long):void
          com.tencent.bugly.proguard.ak.a(int, com.tencent.bugly.proguard.bi):void
          com.tencent.bugly.proguard.ak.a(long, boolean):void
          com.tencent.bugly.proguard.ak.a(com.tencent.bugly.proguard.ak, boolean):boolean */
        public void run() {
            if (!C2908aq.m6912a(this.f6002b, "security_info", 30000)) {
                C2903an.m6863c("[UploadManager] Sleep %d try to lock security file again (pid=%d | tid=%d)", 5000, Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                C2908aq.m6928b(5000);
                if (C2908aq.m6905a(this, "BUGLY_ASYNC_UPLOAD") == null) {
                    C2903an.m6864d("[UploadManager] Failed to start a thread to execute task of initializing security context, try to post it into thread pool.", new Object[0]);
                    C2901am a = C2901am.m6848a();
                    if (a != null) {
                        a.mo34547a(this);
                    } else {
                        C2903an.m6865e("[UploadManager] Asynchronous thread pool is unavailable now, try next time.", new Object[0]);
                    }
                }
            } else {
                if (!C2896ak.this.m6823e()) {
                    C2903an.m6864d("[UploadManager] Failed to load security info from database", new Object[0]);
                    C2896ak.this.mo34533b(false);
                }
                if (C2896ak.this.f5990q != null) {
                    if (C2896ak.this.mo34534b()) {
                        C2903an.m6863c("[UploadManager] Sucessfully got session ID, try to execute upload tasks now (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                        Runnable runnable = this.f6003c;
                        if (runnable != null) {
                            C2896ak.this.m6810a(runnable, this.f6004d);
                        }
                        C2896ak.this.m6817c(0);
                        C2908aq.m6939c(this.f6002b, "security_info");
                        synchronized (C2896ak.this.f5994u) {
                            boolean unused = C2896ak.this.f5993t = false;
                        }
                        return;
                    }
                    C2903an.m6857a("[UploadManager] Session ID is expired, drop it.", new Object[0]);
                    C2896ak.this.mo34533b(true);
                }
                byte[] a2 = C2908aq.m6916a(128);
                if (a2 == null || a2.length * 8 != 128) {
                    C2903an.m6864d("[UploadManager] Failed to create AES key (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                    C2896ak.this.mo34533b(false);
                    C2908aq.m6939c(this.f6002b, "security_info");
                    synchronized (C2896ak.this.f5994u) {
                        boolean unused2 = C2896ak.this.f5993t = false;
                    }
                    return;
                }
                byte[] unused3 = C2896ak.this.f5988o = a2;
                C2903an.m6863c("[UploadManager] Execute one upload task for requesting session ID (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
                Runnable runnable2 = this.f6003c;
                if (runnable2 != null) {
                    C2896ak.this.m6810a(runnable2, this.f6004d);
                } else {
                    C2896ak.this.m6817c(1);
                }
            }
        }
    }

    /* renamed from: a */
    public byte[] mo34532a(byte[] bArr) {
        byte[] bArr2 = this.f5988o;
        if (bArr2 != null && bArr2.length * 8 == 128) {
            return C2908aq.m6917a(1, bArr, bArr2);
        }
        C2903an.m6864d("[UploadManager] AES key is invalid (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        return null;
    }

    /* renamed from: b */
    public byte[] mo34536b(byte[] bArr) {
        byte[] bArr2 = this.f5988o;
        if (bArr2 != null && bArr2.length * 8 == 128) {
            return C2908aq.m6917a(2, bArr, bArr2);
        }
        C2903an.m6864d("[UploadManager] AES key is invalid (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        return null;
    }

    /* renamed from: a */
    public boolean mo34531a(Map<String, String> map) {
        if (map == null) {
            return false;
        }
        C2903an.m6863c("[UploadManager] Integrate security to HTTP headers (pid=%d | tid=%d)", Integer.valueOf(Process.myPid()), Integer.valueOf(Process.myTid()));
        String str = this.f5990q;
        if (str != null) {
            map.put("secureSessionId", str);
            return true;
        }
        byte[] bArr = this.f5988o;
        if (bArr == null || bArr.length * 8 != 128) {
            C2903an.m6864d("[UploadManager] AES key is invalid", new Object[0]);
            return false;
        }
        if (this.f5986m == null) {
            this.f5986m = Base64.decode(this.f5985l, 0);
            if (this.f5986m == null) {
                C2903an.m6864d("[UploadManager] Failed to decode RSA public key", new Object[0]);
                return false;
            }
        }
        byte[] b = C2908aq.m6933b(1, this.f5988o, this.f5986m);
        if (b == null) {
            C2903an.m6864d("[UploadManager] Failed to encrypt AES key", new Object[0]);
            return false;
        }
        String encodeToString = Base64.encodeToString(b, 0);
        if (encodeToString == null) {
            C2903an.m6864d("[UploadManager] Failed to encode AES key", new Object[0]);
            return false;
        }
        map.put("raKey", encodeToString);
        return true;
    }
}
