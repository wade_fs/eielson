package com.tencent.stat.lbs;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.v4.app.NotificationCompat;
import android.telephony.CellLocation;
import android.telephony.NeighboringCellInfo;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.baidu.mobstat.Config;
import com.tencent.open.SocialConstants;
import com.tencent.stat.StatActionListener;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatServiceImpl;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.Util;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class StatGpsMonitor {

    /* renamed from: a */
    private static volatile StatGpsMonitor f7402a;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public static StatLogger f7403i = StatCommonHelper.getLogger();

    /* renamed from: o */
    private static long f7404o = 1800000;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public LocationManager f7405b = null;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public volatile Location f7406c = null;

    /* renamed from: d */
    private volatile long f7407d = 0;

    /* renamed from: e */
    private volatile Location f7408e = null;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public LocationListener f7409f = null;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public LocationListener f7410g = null;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public Handler f7411h = null;

    /* renamed from: j */
    private Context f7412j = StatServiceImpl.getContext(null);

    /* renamed from: k */
    private boolean f7413k = false;

    /* renamed from: l */
    private PendingIntent f7414l = null;

    /* renamed from: m */
    private BroadcastReceiver f7415m = null;

    /* renamed from: n */
    private AlarmManager f7416n = null;

    /* renamed from: p */
    private StatActionListener f7417p = new StatActionListener() {
        /* class com.tencent.stat.lbs.StatGpsMonitor.C32552 */

        public void onBecameForeground() {
            StatGpsMonitor.this.m8123f();
        }

        public void onBecameBackground() {
            StatGpsMonitor.this.m8125g();
        }
    };

    public static StatGpsMonitor getInstance() {
        if (f7402a == null) {
            synchronized (StatGpsMonitor.class) {
                if (f7402a == null) {
                    f7402a = new StatGpsMonitor();
                }
            }
        }
        return f7402a;
    }

    public void init(StatGpsOption statGpsOption) {
        f7403i.mo35381d("init StatGpsMonitor");
        HandlerThread handlerThread = new HandlerThread("fbilbs");
        handlerThread.start();
        this.f7411h = new Handler(handlerThread.getLooper());
        this.f7416n = (AlarmManager) this.f7412j.getSystemService(NotificationCompat.CATEGORY_ALARM);
        this.f7405b = (LocationManager) this.f7412j.getSystemService("location");
        m8108a(statGpsOption);
    }

    public void startMonitor() {
        if (!this.f7413k) {
            f7403i.mo35381d("startMonitor");
            StatServiceImpl.addActionListener(this.f7417p);
            m8123f();
            this.f7413k = true;
        }
    }

    public void stopMonitor() {
        if (this.f7413k) {
            f7403i.mo35381d("stopMonitor");
            StatServiceImpl.removeActionListener(this.f7417p);
            m8125g();
            m8129i();
            this.f7413k = false;
        }
    }

    public Location getLastLocation() {
        return this.f7406c;
    }

    /* renamed from: d */
    private void m8118d() {
        StatLogger statLogger = f7403i;
        statLogger.mo35381d("gps reportCurLocation:" + this.f7406c);
        Context context = this.f7412j;
        StatServiceImpl.reportEvent(this.f7412j, new C3259a(context, StatServiceImpl.getSessionID(context, false, null), null), null);
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m8121e() {
        try {
            m8118d();
            m8123f();
        } catch (Throwable th) {
            f7403i.mo35396w(th);
        }
    }

    public static long getLbsReportDuration(Context context) {
        long longValue = Long.valueOf(StatConfig.getCustomProperty(context, "_lbs_du_", "0")).longValue();
        if (longValue > 30000) {
            f7404o = longValue;
        }
        return f7404o;
    }

    public static void setLbsReportDuration(Context context, long j) {
        f7404o = j;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m8123f() {
        try {
            long lbsReportDuration = getLbsReportDuration(this.f7412j);
            long currentTimeMillis = System.currentTimeMillis();
            if (this.f7414l == null) {
                synchronized (StatGpsMonitor.class) {
                    if (this.f7414l == null) {
                        this.f7414l = PendingIntent.getBroadcast(this.f7412j, 0, new Intent("com.tencent.fbi.lbs.report.action"), 134217728);
                        m8118d();
                        this.f7415m = new BroadcastReceiver() {
                            /* class com.tencent.stat.lbs.StatGpsMonitor.C32531 */

                            public void onReceive(Context context, Intent intent) {
                                StatLogger c = StatGpsMonitor.f7403i;
                                c.mo35388i("hearbeat onReceive:" + intent);
                                StatGpsMonitor.this.f7411h.post(new Runnable() {
                                    /* class com.tencent.stat.lbs.StatGpsMonitor.C32531.C32541 */

                                    public void run() {
                                        StatGpsMonitor.this.m8121e();
                                    }
                                });
                            }
                        };
                        try {
                            StatLogger statLogger = f7403i;
                            statLogger.mo35388i("registerReceiver:" + this.f7415m);
                            this.f7412j.registerReceiver(this.f7415m, new IntentFilter("com.tencent.fbi.lbs.report.action"));
                        } catch (Throwable th) {
                            th.printStackTrace();
                        }
                    }
                }
            }
            StatLogger statLogger2 = f7403i;
            statLogger2.mo35388i("AlarmManager set reportInterval:" + lbsReportDuration + ",reportPendingIntent:" + this.f7414l);
            this.f7416n.set(0, currentTimeMillis + lbsReportDuration, this.f7414l);
        } catch (Throwable th2) {
            f7403i.mo35396w(th2);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m8125g() {
        try {
            StatLogger statLogger = f7403i;
            statLogger.mo35388i("stopReporting, reportPendingIntent=" + this.f7414l + ",reportBroadcastReceiver=" + this.f7415m);
            if (this.f7414l != null) {
                this.f7416n.cancel(this.f7414l);
                this.f7414l = null;
            }
            if (this.f7415m != null) {
                this.f7412j.unregisterReceiver(this.f7415m);
                this.f7415m = null;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public JSONObject mo35482a() {
        try {
            JSONObject jSONObject = new JSONObject();
            try {
                TelephonyManager telephonyManager = (TelephonyManager) this.f7412j.getSystemService("phone");
                String networkOperator = telephonyManager.getNetworkOperator();
                CellLocation cellLocation = telephonyManager.getCellLocation();
                if (cellLocation instanceof GsmCellLocation) {
                    StatLogger statLogger = f7403i;
                    statLogger.mo35388i("CellLocation type:GsmCellLocation:" + cellLocation);
                    jSONObject.put(SocialConstants.PARAM_TYPE, "gsm");
                    jSONObject.put("lac", ((GsmCellLocation) cellLocation).getLac());
                    jSONObject.put("cid", ((GsmCellLocation) cellLocation).getCid());
                    if (networkOperator != null && networkOperator.length() >= 6) {
                        jSONObject.put(Config.OPERATOR, networkOperator);
                        jSONObject.put("mcc", networkOperator.substring(0, 3));
                        jSONObject.put("mnc", networkOperator.substring(3, 5));
                    }
                } else if (cellLocation instanceof CdmaCellLocation) {
                    StatLogger statLogger2 = f7403i;
                    statLogger2.mo35388i("CellLocation type:CdmaCellLocation:" + cellLocation);
                    CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                    jSONObject.put(SocialConstants.PARAM_TYPE, "cdma");
                    jSONObject.put("lac", cdmaCellLocation.getNetworkId());
                    jSONObject.put("cid", cdmaCellLocation.getBaseStationId());
                    jSONObject.put("lat", cdmaCellLocation.getBaseStationLatitude());
                    jSONObject.put("lng", cdmaCellLocation.getBaseStationLongitude());
                    if (networkOperator != null && networkOperator.length() >= 6) {
                        jSONObject.put(Config.OPERATOR, networkOperator);
                        jSONObject.put("mcc", networkOperator.substring(0, 3));
                        jSONObject.put("mnc", cdmaCellLocation.getSystemId() + "");
                    }
                } else {
                    f7403i.mo35396w("error CellLocation type");
                    return null;
                }
                jSONObject.put("nt", telephonyManager.getNetworkType());
                StatLogger statLogger3 = f7403i;
                statLogger3.mo35388i("cellLoc:" + jSONObject);
                JSONArray jSONArray = new JSONArray();
                List<NeighboringCellInfo> neighboringCellInfo = telephonyManager.getNeighboringCellInfo();
                if (neighboringCellInfo != null && neighboringCellInfo.size() > 0) {
                    StatLogger statLogger4 = f7403i;
                    statLogger4.mo35388i("infoLists:" + neighboringCellInfo + "     size:" + neighboringCellInfo.size());
                    for (NeighboringCellInfo neighboringCellInfo2 : neighboringCellInfo) {
                        StatLogger statLogger5 = f7403i;
                        statLogger5.mo35388i("get neighboringCellInfo:" + neighboringCellInfo2);
                        if (!(neighboringCellInfo2.getRssi() == 99 || neighboringCellInfo2.getCid() == -1)) {
                            JSONObject jSONObject2 = new JSONObject();
                            jSONObject2.put("dBm", (neighboringCellInfo2.getRssi() * 2) - 133);
                            jSONObject2.put("rssi", neighboringCellInfo2.getRssi());
                            jSONObject2.put("lac", neighboringCellInfo2.getLac());
                            jSONObject2.put("cid", neighboringCellInfo2.getCid());
                            jSONArray.put(jSONObject2);
                        }
                    }
                }
                if (jSONArray.length() > 0) {
                    jSONObject.put("nb", jSONArray);
                }
                return jSONObject;
            } catch (Throwable th) {
                th = th;
                f7403i.mo35384e(th);
                return null;
            }
        } catch (Throwable th2) {
            th = th2;
            f7403i.mo35384e(th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public JSONObject mo35483b() {
        JSONObject jSONObject = new JSONObject();
        if (this.f7406c != null) {
            Util.safeJsonPut(jSONObject, "lat", Double.valueOf(this.f7406c.getLatitude()));
            Util.safeJsonPut(jSONObject, "lng", Double.valueOf(this.f7406c.getLongitude()));
            Util.safeJsonPut(jSONObject, "alt", Double.valueOf(this.f7406c.getAltitude()));
            Util.safeJsonPut(jSONObject, "bear", Float.valueOf(this.f7406c.getBearing()));
            Util.safeJsonPut(jSONObject, "acc", Float.valueOf(this.f7406c.getAccuracy()));
            Util.safeJsonPut(jSONObject, "time", Long.valueOf(this.f7406c.getTime()));
            Util.safeJsonPut(jSONObject, "sp", Float.valueOf(this.f7406c.getSpeed()));
            Util.safeJsonPut(jSONObject, "pvd", this.f7406c.getProvider());
        }
        return jSONObject;
    }

    /* access modifiers changed from: private */
    /* renamed from: h */
    public String m8127h() {
        Criteria criteria = new Criteria();
        criteria.setAccuracy(1);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setCostAllowed(false);
        criteria.setPowerRequirement(1);
        return this.f7405b.getBestProvider(criteria, true);
    }

    /* renamed from: a */
    private void m8108a(StatGpsOption statGpsOption) {
        f7403i.mo35381d("registerGps");
        if (statGpsOption == null) {
            statGpsOption = new StatGpsOption();
        }
        m8114b(statGpsOption);
        m8117c(statGpsOption);
    }

    /* renamed from: b */
    private void m8114b(final StatGpsOption statGpsOption) {
        Handler handler;
        if (this.f7409f == null && (handler = this.f7411h) != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.lbs.StatGpsMonitor.C32563 */

                public void run() {
                    try {
                        StatGpsMonitor.f7403i.mo35381d("registerGpsLocationListener");
                        LocationListener unused = StatGpsMonitor.this.f7409f = new C3258a("gps");
                        Location lastKnownLocation = StatGpsMonitor.this.f7405b.getLastKnownLocation(StatGpsMonitor.this.m8127h());
                        if (StatGpsMonitor.this.m8109a(StatGpsMonitor.this.f7406c, lastKnownLocation)) {
                            Location unused2 = StatGpsMonitor.this.f7406c = lastKnownLocation;
                        }
                        StatGpsMonitor.this.f7405b.requestLocationUpdates("gps", statGpsOption.getMinTime(), statGpsOption.getMinDistance(), StatGpsMonitor.this.f7409f);
                    } catch (Throwable th) {
                        StatGpsMonitor.f7403i.mo35384e(th);
                    }
                }
            });
        }
    }

    /* renamed from: c */
    private void m8117c(final StatGpsOption statGpsOption) {
        Handler handler;
        if (this.f7410g == null && (handler = this.f7411h) != null) {
            handler.post(new Runnable() {
                /* class com.tencent.stat.lbs.StatGpsMonitor.C32574 */

                public void run() {
                    try {
                        StatGpsMonitor.f7403i.mo35381d("registerNetworkLocationListener");
                        LocationListener unused = StatGpsMonitor.this.f7410g = new C3258a("network");
                        Location lastKnownLocation = StatGpsMonitor.this.f7405b.getLastKnownLocation(StatGpsMonitor.this.m8127h());
                        if (StatGpsMonitor.this.m8109a(StatGpsMonitor.this.f7406c, lastKnownLocation)) {
                            Location unused2 = StatGpsMonitor.this.f7406c = lastKnownLocation;
                        }
                        StatGpsMonitor.this.f7405b.requestLocationUpdates("gps", statGpsOption.getMinTime(), statGpsOption.getMinDistance(), StatGpsMonitor.this.f7410g);
                    } catch (Throwable th) {
                        StatGpsMonitor.f7403i.mo35384e(th);
                    }
                }
            });
        }
    }

    /* renamed from: i */
    private void m8129i() {
        m8130j();
        m8131k();
    }

    /* renamed from: j */
    private void m8130j() {
        LocationListener locationListener = this.f7409f;
        if (locationListener != null) {
            this.f7405b.removeUpdates(locationListener);
            this.f7409f = null;
        }
    }

    /* renamed from: k */
    private void m8131k() {
        LocationListener locationListener = this.f7410g;
        if (locationListener != null) {
            this.f7405b.removeUpdates(locationListener);
            this.f7409f = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public boolean m8109a(Location location, Location location2) {
        if (location2 == null) {
            return true;
        }
        if (location == null) {
            return false;
        }
        long time = location.getTime() - location2.getTime();
        boolean z = time > 120000;
        boolean z2 = time < -120000;
        boolean z3 = time > 0;
        if (z) {
            return true;
        }
        if (z2) {
            return false;
        }
        int accuracy = (int) (location.getAccuracy() - location2.getAccuracy());
        boolean z4 = accuracy > 0;
        boolean z5 = accuracy < 0;
        boolean z6 = accuracy > 200;
        boolean a = m8111a(location.getProvider(), location2.getProvider());
        if (z5) {
            return true;
        }
        if (!z3 || z4) {
            return z3 && !z6 && a;
        }
        return true;
    }

    /* renamed from: a */
    private boolean m8111a(String str, String str2) {
        if (str == null) {
            return str2 == null;
        }
        return str.equals(str2);
    }

    /* renamed from: com.tencent.stat.lbs.StatGpsMonitor$a */
    private class C3258a implements LocationListener {

        /* renamed from: b */
        private String f7426b = "";

        public C3258a(String str) {
            this.f7426b = str;
        }

        public void onStatusChanged(String str, int i, Bundle bundle) {
            StatLogger c = StatGpsMonitor.f7403i;
            c.mo35388i(this.f7426b + " gps onStatusChanged status:" + i + ",extras:" + bundle + ", provider:" + str);
            if (i == 0) {
                StatLogger c2 = StatGpsMonitor.f7403i;
                c2.mo35388i(str + " OUT_OF_SERVICE gps");
            } else if (i == 1) {
                StatLogger c3 = StatGpsMonitor.f7403i;
                c3.mo35388i(str + " TEMPORARILY_UNAVAILABLE gps");
            } else if (i == 2) {
                StatLogger c4 = StatGpsMonitor.f7403i;
                c4.mo35388i(str + " AVAILABLE gps");
            }
        }

        public void onProviderEnabled(String str) {
            StatLogger c = StatGpsMonitor.f7403i;
            c.mo35381d(this.f7426b + " gps onProviderEnabled provider:" + str);
            if (StatGpsMonitor.this.f7406c == null) {
                try {
                    Location unused = StatGpsMonitor.this.f7406c = StatGpsMonitor.this.f7405b.getLastKnownLocation(StatGpsMonitor.this.m8127h());
                } catch (Exception e) {
                    StatGpsMonitor.f7403i.mo35384e((Throwable) e);
                }
            }
        }

        public void onProviderDisabled(String str) {
            StatLogger c = StatGpsMonitor.f7403i;
            c.mo35381d(this.f7426b + " gps onProviderDisabled provider:" + str);
        }

        public void onLocationChanged(Location location) {
            if (location != null) {
                StatLogger c = StatGpsMonitor.f7403i;
                c.mo35388i(this.f7426b + " gps onLocationChanged location:" + location.getLatitude() + "," + location.getLongitude() + "," + location.getTime() + "==?" + System.currentTimeMillis());
                StatGpsMonitor statGpsMonitor = StatGpsMonitor.this;
                if (statGpsMonitor.m8109a(location, statGpsMonitor.f7406c)) {
                    Location unused = StatGpsMonitor.this.f7406c = location;
                }
            }
        }
    }
}
