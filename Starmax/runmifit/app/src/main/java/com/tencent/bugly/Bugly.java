package com.tencent.bugly;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.crashreport.CrashReport;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.proguard.C2888ad;
import com.tencent.bugly.proguard.C2889ae;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;
import java.util.Map;

/* compiled from: BUGLY */
public class Bugly {

    /* renamed from: a */
    private static boolean f4545a = false;
    public static Context applicationContext = null;
    public static boolean enable = true;

    public static void init(Context context, String str, boolean z) {
        init(context, str, z, null);
    }

    public static synchronized void init(Context context, String str, boolean z, BuglyStrategy buglyStrategy) {
        synchronized (Bugly.class) {
            if (!f4545a) {
                f4545a = true;
                applicationContext = C2908aq.m6891a(context);
                if (applicationContext == null) {
                    Log.e(C2903an.f6030b, "init arg 'context' should not be null!");
                    return;
                }
                C2797b.m6248a(CrashModule.getInstance());
                C2797b.m6248a(Beta.getInstance());
                C2797b.f5301a = enable;
                C2797b.m6246a(applicationContext, str, z, buglyStrategy);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]>
     arg types: [int, ?[OBJECT, ARRAY], int]
     candidates:
      com.tencent.bugly.proguard.ae.a(java.lang.String, android.content.ContentValues, com.tencent.bugly.proguard.ad):long
      com.tencent.bugly.proguard.ae.a(com.tencent.bugly.proguard.ae, int, com.tencent.bugly.proguard.ad):java.util.Map
      com.tencent.bugly.proguard.ae.a(int, java.lang.String, com.tencent.bugly.proguard.ad):boolean
      com.tencent.bugly.proguard.ae.a(int, com.tencent.bugly.proguard.ad, boolean):java.util.Map<java.lang.String, byte[]> */
    public static synchronized String getAppChannel() {
        byte[] bArr;
        synchronized (Bugly.class) {
            C2851a b = C2851a.m6471b();
            if (b == null) {
                return null;
            }
            if (TextUtils.isEmpty(b.f5673r)) {
                C2889ae a = C2889ae.m6757a();
                if (a == null) {
                    String str = b.f5673r;
                    return str;
                }
                Map<String, byte[]> a2 = a.mo34494a(556, (C2888ad) null, true);
                if (!(a2 == null || (bArr = a2.get("app_channel")) == null)) {
                    String str2 = new String(bArr);
                    return str2;
                }
            }
            String str3 = b.f5673r;
            return str3;
        }
    }

    public static void setIsDevelopmentDevice(Context context, boolean z) {
        CrashReport.setIsDevelopmentDevice(context, z);
    }

    public static void setAppChannel(Context context, String str) {
        CrashReport.setAppChannel(context, str);
    }

    public static void setUserId(Context context, String str) {
        CrashReport.setUserId(context, str);
    }

    public static void setUserTag(Context context, int i) {
        CrashReport.setUserSceneTag(context, i);
    }

    public static void putUserData(Context context, String str, String str2) {
        CrashReport.putUserData(context, str, str2);
    }
}
