package com.tencent.bugly.proguard;

import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.z */
/* compiled from: BUGLY */
public final class C2960z extends C2944m implements Cloneable {

    /* renamed from: e */
    static Map<String, String> f6298e;

    /* renamed from: f */
    static final /* synthetic */ boolean f6299f = (!C2960z.class.desiredAssertionStatus());

    /* renamed from: a */
    public int f6300a = 0;

    /* renamed from: b */
    public String f6301b = "";

    /* renamed from: c */
    public long f6302c = 0;

    /* renamed from: d */
    public Map<String, String> f6303d = null;

    public C2960z() {
    }

    public C2960z(int i, String str, long j, Map<String, String> map) {
        this.f6300a = i;
        this.f6301b = str;
        this.f6302c = j;
        this.f6303d = map;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C2960z zVar = (C2960z) obj;
        if (!C2945n.m7121a(this.f6300a, zVar.f6300a) || !C2945n.m7123a(this.f6301b, zVar.f6301b) || !C2945n.m7122a(this.f6302c, zVar.f6302c) || !C2945n.m7123a(this.f6303d, zVar.f6303d)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f6299f) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34644a(this.f6300a, 0);
        String str = this.f6301b;
        if (str != null) {
            lVar.mo34648a(str, 1);
        }
        lVar.mo34645a(this.f6302c, 2);
        Map<String, String> map = this.f6303d;
        if (map != null) {
            lVar.mo34650a((Map) map, 3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6300a = kVar.mo34616a(this.f6300a, 0, false);
        this.f6301b = kVar.mo34621a(1, false);
        this.f6302c = kVar.mo34618a(this.f6302c, 2, false);
        if (f6298e == null) {
            f6298e = new HashMap();
            f6298e.put("", "");
        }
        this.f6303d = (Map) kVar.mo34620a((Object) f6298e, 3, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i */
    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34597a(this.f6300a, "flag");
        iVar.mo34601a(this.f6301b, "localStrategyId");
        iVar.mo34598a(this.f6302c, "localStrategyTime");
        iVar.mo34603a((Map) this.f6303d, "reserved");
    }
}
