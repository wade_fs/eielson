package com.tencent.open.utils;

import android.support.v4.view.MotionEventCompat;
import com.google.common.primitives.UnsignedBytes;

/* renamed from: com.tencent.open.utils.m */
/* compiled from: ProGuard */
public final class C3135m implements Cloneable {

    /* renamed from: a */
    private int f6874a;

    public C3135m(byte[] bArr) {
        this(bArr, 0);
    }

    public C3135m(byte[] bArr, int i) {
        this.f6874a = (bArr[i + 1] << 8) & MotionEventCompat.ACTION_POINTER_INDEX_MASK;
        this.f6874a += bArr[i] & UnsignedBytes.MAX_VALUE;
    }

    public C3135m(int i) {
        this.f6874a = i;
    }

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof C3135m) || this.f6874a != ((C3135m) obj).mo35186b()) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public byte[] mo35185a() {
        int i = this.f6874a;
        return new byte[]{(byte) (i & 255), (byte) ((i & MotionEventCompat.ACTION_POINTER_INDEX_MASK) >> 8)};
    }

    /* renamed from: b */
    public int mo35186b() {
        return this.f6874a;
    }

    public int hashCode() {
        return this.f6874a;
    }
}
