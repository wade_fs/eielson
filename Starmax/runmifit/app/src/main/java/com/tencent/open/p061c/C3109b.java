package com.tencent.open.p061c;

import android.content.Context;
import android.os.Build;
import android.webkit.WebSettings;
import android.webkit.WebView;
import java.lang.reflect.Method;

/* renamed from: com.tencent.open.c.b */
/* compiled from: ProGuard */
public class C3109b extends WebView {
    public C3109b(Context context) {
        super(context);
        mo35157a();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35157a() {
        if (Build.VERSION.SDK_INT >= 11) {
            removeJavascriptInterface("searchBoxJavaBridge_");
            removeJavascriptInterface("accessibility");
            removeJavascriptInterface("accessibilityTraversal");
        }
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        WebSettings settings = getSettings();
        if (settings != null) {
            try {
                Method method = settings.getClass().getMethod("removeJavascriptInterface", String.class);
                if (method != null) {
                    method.invoke(this, "searchBoxJavaBridge_");
                }
            } catch (Exception unused) {
            }
        }
    }
}
