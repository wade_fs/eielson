package com.tencent.open.p061c;

import android.content.Context;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.web.security.C3136a;
import com.tencent.open.web.security.SecureJsInterface;

/* renamed from: com.tencent.open.c.c */
/* compiled from: ProGuard */
public class C3110c extends C3109b {

    /* renamed from: a */
    public static boolean f6814a;

    /* renamed from: b */
    private KeyEvent f6815b;

    /* renamed from: c */
    private C3136a f6816c;

    public C3110c(Context context) {
        super(context);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int unicodeChar;
        C3082f.m7631b("openSDK_LOG.SecureWebView", "-->dispatchKeyEvent, is device support: " + f6814a);
        if (!f6814a) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyEvent.getAction() != 0) {
            return super.dispatchKeyEvent(keyEvent);
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 4) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyCode == 66) {
            return super.dispatchKeyEvent(keyEvent);
        }
        if (keyCode == 67) {
            C3136a.f6877b = true;
            return super.dispatchKeyEvent(keyEvent);
        } else if (keyEvent.getUnicodeChar() == 0) {
            return super.dispatchKeyEvent(keyEvent);
        } else {
            if (!SecureJsInterface.isPWDEdit || (((unicodeChar = keyEvent.getUnicodeChar()) < 33 || unicodeChar > 95) && (unicodeChar < 97 || unicodeChar > 125))) {
                return super.dispatchKeyEvent(keyEvent);
            }
            this.f6815b = new KeyEvent(0, 17);
            return super.dispatchKeyEvent(this.f6815b);
        }
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        int unicodeChar;
        C3082f.m7631b("openSDK_LOG.SecureWebView", "-->onKeyDown, is device support: " + f6814a);
        if (!f6814a) {
            return super.onKeyDown(i, keyEvent);
        }
        if (keyEvent.getAction() != 0) {
            return super.onKeyDown(i, keyEvent);
        }
        int keyCode = keyEvent.getKeyCode();
        if (keyCode == 4) {
            return super.onKeyDown(i, keyEvent);
        }
        if (keyCode == 66) {
            return super.onKeyDown(i, keyEvent);
        }
        if (keyCode == 67) {
            C3136a.f6877b = true;
            return super.onKeyDown(i, keyEvent);
        } else if (keyEvent.getUnicodeChar() == 0) {
            return super.onKeyDown(i, keyEvent);
        } else {
            if (!SecureJsInterface.isPWDEdit || (((unicodeChar = keyEvent.getUnicodeChar()) < 33 || unicodeChar > 95) && (unicodeChar < 97 || unicodeChar > 125))) {
                return super.onKeyDown(i, keyEvent);
            }
            this.f6815b = new KeyEvent(0, 17);
            return super.onKeyDown(this.f6815b.getKeyCode(), this.f6815b);
        }
    }

    public InputConnection onCreateInputConnection(EditorInfo editorInfo) {
        C3082f.m7634c("openSDK_LOG.SecureWebView", "-->create input connection, is edit: " + SecureJsInterface.isPWDEdit);
        InputConnection onCreateInputConnection = super.onCreateInputConnection(editorInfo);
        C3082f.m7628a("openSDK_LOG.SecureWebView", "-->onCreateInputConnection, inputConn is " + onCreateInputConnection);
        if (onCreateInputConnection != null) {
            f6814a = true;
            this.f6816c = new C3136a(super.onCreateInputConnection(editorInfo), false);
            return this.f6816c;
        }
        f6814a = false;
        return onCreateInputConnection;
    }
}
