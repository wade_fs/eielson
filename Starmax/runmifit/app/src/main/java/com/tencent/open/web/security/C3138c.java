package com.tencent.open.web.security;

import android.webkit.WebView;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import com.tencent.open.C3070a;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p061c.C3110c;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.open.web.security.c */
/* compiled from: ProGuard */
public class C3138c extends C3070a.C3071a {

    /* renamed from: d */
    private String f6879d;

    public C3138c(WebView webView, long j, String str, String str2) {
        super(webView, j, str);
        this.f6879d = str2;
    }

    /* renamed from: a */
    public void mo35071a(Object obj) {
        C3082f.m7628a("openSDK_LOG.SecureJsListener", "-->onComplete, result: " + obj);
    }

    /* renamed from: a */
    public void mo35070a() {
        C3082f.m7631b("openSDK_LOG.SecureJsListener", "-->onNoMatchMethod...");
    }

    /* renamed from: a */
    public void mo35072a(String str) {
        C3082f.m7628a("openSDK_LOG.SecureJsListener", "-->onCustomCallback, js: " + str);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("result", !C3110c.f6814a ? -4 : 0);
            jSONObject.put("sn", this.f6701b);
            jSONObject.put("data", str);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        m7807b(jSONObject.toString());
    }

    /* renamed from: b */
    private void m7807b(String str) {
        WebView webView = (WebView) this.f6700a.get();
        if (webView != null) {
            StringBuffer stringBuffer = new StringBuffer("javascript:");
            stringBuffer.append("if(!!");
            stringBuffer.append(this.f6879d);
            stringBuffer.append("){");
            stringBuffer.append(this.f6879d);
            stringBuffer.append("(");
            stringBuffer.append(str);
            stringBuffer.append(")}");
            String stringBuffer2 = stringBuffer.toString();
            C3082f.m7628a("openSDK_LOG.SecureJsListener", "-->callback, callback: " + stringBuffer2);
            webView.loadUrl(stringBuffer2);
        }
    }
}
