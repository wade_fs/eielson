package com.tencent.stat.common;

import android.content.Context;
import android.os.Environment;
import java.io.File;

/* renamed from: com.tencent.stat.common.f */
public class C3211f {
    /* renamed from: a */
    public static boolean m7989a(Context context) {
        return Util.checkPermission(context, "android.permission.WRITE_EXTERNAL_STORAGE") && "mounted".equals(Environment.getExternalStorageState());
    }

    /* renamed from: b */
    public static boolean m7992b(Context context) {
        return Util.checkPermission(context, "android.permission.READ_EXTERNAL_STORAGE") && "mounted".equals(Environment.getExternalStorageState());
    }

    /* renamed from: a */
    public static boolean m7990a(Context context, File file) {
        File parentFile;
        try {
            if (!m7989a(context) || (parentFile = file.getParentFile()) == null || parentFile.exists()) {
                return false;
            }
            parentFile.mkdirs();
            return file.createNewFile();
        } catch (Throwable th) {
            th.printStackTrace();
            return false;
        }
    }

    /* renamed from: b */
    public static long m7991b(Context context, File file) {
        try {
            if (!m7992b(context) || !file.exists()) {
                return 0;
            }
            return file.lastModified();
        } catch (Throwable th) {
            th.printStackTrace();
            return 0;
        }
    }
}
