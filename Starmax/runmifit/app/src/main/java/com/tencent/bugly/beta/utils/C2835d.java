package com.tencent.bugly.beta.utils;

/* renamed from: com.tencent.bugly.beta.utils.d */
/* compiled from: BUGLY */
public class C2835d {

    /* renamed from: a */
    C2830a f5536a = null;

    /* renamed from: b */
    private String f5537b = null;

    /* renamed from: c */
    private long f5538c = 0;

    /* renamed from: d */
    private long f5539d = 0;

    public C2835d(String str, long j, long j2) {
        this.f5537b = str;
        this.f5538c = j;
        this.f5539d = j2;
    }

    /* renamed from: b */
    private boolean m6393b() {
        return (this.f5538c == 0 || this.f5539d == 0) ? false : true;
    }

    /* renamed from: a */
    public synchronized void mo34227a() {
        if (this.f5536a != null) {
            this.f5536a.mo34193a();
            this.f5536a = null;
        }
    }

    /* renamed from: c */
    private synchronized boolean m6394c() {
        if (!m6393b()) {
            return false;
        }
        if (this.f5536a == null) {
            try {
                this.f5536a = new C2830a(this.f5537b);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        return true;
    }

    /* renamed from: b */
    private synchronized String m6392b(long j) {
        if (this.f5536a == null) {
            return null;
        }
        StringBuffer stringBuffer = new StringBuffer();
        try {
            this.f5536a.mo34196b(this.f5538c);
            this.f5536a.mo34196b(j);
            while (true) {
                byte b = this.f5536a.mo34195b();
                if (b == 0) {
                    break;
                }
                stringBuffer.append((char) b);
            }
            this.f5536a.mo34193a();
            this.f5536a = new C2830a(this.f5537b);
        } catch (Exception e) {
            this.f5536a = null;
            e.printStackTrace();
        }
        return stringBuffer.toString();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0025, code lost:
        return null;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized java.lang.String mo34226a(long r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            r0 = 0
            r2 = 0
            int r3 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r3 < 0) goto L_0x0024
            long r0 = r4.f5539d     // Catch:{ all -> 0x0021 }
            int r3 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r3 < 0) goto L_0x000f
            goto L_0x0024
        L_0x000f:
            com.tencent.bugly.beta.utils.a r0 = r4.f5536a     // Catch:{ all -> 0x0021 }
            if (r0 != 0) goto L_0x001b
            boolean r0 = r4.m6394c()     // Catch:{ all -> 0x0021 }
            if (r0 != 0) goto L_0x001b
            monitor-exit(r4)
            return r2
        L_0x001b:
            java.lang.String r5 = r4.m6392b(r5)     // Catch:{ all -> 0x0021 }
            monitor-exit(r4)
            return r5
        L_0x0021:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        L_0x0024:
            monitor-exit(r4)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.utils.C2835d.mo34226a(long):java.lang.String");
    }
}
