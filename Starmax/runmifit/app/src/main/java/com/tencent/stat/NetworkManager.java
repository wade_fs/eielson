package com.tencent.stat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.HandlerThread;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.StatLogger;
import com.tencent.stat.common.Util;
import org.apache.http.HttpHost;

public class NetworkManager {
    public static final int TYPE_NOT_WIFI = 2;
    public static final int TYPE_NO_NETWORK = 0;
    public static final int TYPE_WIFI = 1;

    /* renamed from: f */
    private static NetworkManager f6880f;

    /* renamed from: a */
    private volatile int f6881a = 2;

    /* renamed from: b */
    private volatile String f6882b = "";

    /* renamed from: c */
    private volatile HttpHost f6883c = null;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Handler f6884d = null;

    /* renamed from: e */
    private int f6885e = 0;

    /* renamed from: g */
    private Context f6886g = null;

    /* renamed from: h */
    private StatLogger f6887h = null;

    public void onDispatchFailed() {
    }

    public HttpHost getHttpProxy() {
        return this.f6883c;
    }

    public String getCurNetwrokName() {
        return this.f6882b;
    }

    public int getNetworkType() {
        return this.f6881a;
    }

    private NetworkManager(Context context) {
        if (context != null) {
            this.f6886g = context.getApplicationContext();
        } else {
            this.f6886g = StatServiceImpl.getContext(null);
        }
        HandlerThread handlerThread = new HandlerThread("nt");
        handlerThread.start();
        this.f6884d = new Handler(handlerThread.getLooper());
        C3220d.m8020a(context);
        this.f6887h = StatCommonHelper.getLogger();
        m7812b();
        mo35200a();
    }

    public boolean isWifi() {
        return this.f6881a == 1;
    }

    public boolean isNetworkAvailable() {
        return this.f6881a != 0;
    }

    public static NetworkManager getInstance(Context context) {
        if (f6880f == null) {
            synchronized (NetworkManager.class) {
                if (f6880f == null) {
                    f6880f = new NetworkManager(context);
                }
            }
        }
        return f6880f;
    }

    /* renamed from: b */
    private void m7812b() {
        this.f6881a = 0;
        this.f6883c = null;
        this.f6882b = null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35200a() {
        if (Util.isNetworkAvailable(this.f6886g)) {
            this.f6882b = StatCommonHelper.getLinkedWay(this.f6886g);
            if (StatConfig.isDebugEnable()) {
                StatLogger statLogger = this.f6887h;
                statLogger.mo35388i("NETWORK name:" + this.f6882b);
            }
            if (StatCommonHelper.isStringValid(this.f6882b)) {
                if ("WIFI".equalsIgnoreCase(this.f6882b)) {
                    this.f6881a = 1;
                } else {
                    this.f6881a = 2;
                }
                this.f6883c = StatCommonHelper.getHttpProxy(this.f6886g);
            }
            if (StatServiceImpl.m7863a()) {
                StatServiceImpl.m7884d(this.f6886g);
                return;
            }
            return;
        }
        if (StatConfig.isDebugEnable()) {
            this.f6887h.mo35388i("NETWORK TYPE: network is close.");
        }
        m7812b();
    }

    public void registerBroadcast() {
        try {
            this.f6886g.getApplicationContext().registerReceiver(new BroadcastReceiver() {
                /* class com.tencent.stat.NetworkManager.C31391 */

                public void onReceive(Context context, Intent intent) {
                    if (NetworkManager.this.f6884d != null) {
                        NetworkManager.this.f6884d.post(new Runnable() {
                            /* class com.tencent.stat.NetworkManager.C31391.C31401 */

                            public void run() {
                                NetworkManager.this.mo35200a();
                            }
                        });
                    }
                }
            }, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
