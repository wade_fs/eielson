package com.tencent.open.p059a;

import com.tencent.open.p059a.C3076d;

/* renamed from: com.tencent.open.a.i */
/* compiled from: ProGuard */
public abstract class C3085i {

    /* renamed from: a */
    private volatile int f6749a;

    /* renamed from: b */
    private volatile boolean f6750b;

    /* renamed from: c */
    private C3084h f6751c;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo35076a(int i, Thread thread, long j, String str, String str2, Throwable th);

    public C3085i() {
        this(C3075c.f6724a, true, C3084h.f6748a);
    }

    public C3085i(int i, boolean z, C3084h hVar) {
        this.f6749a = C3075c.f6724a;
        this.f6750b = true;
        this.f6751c = C3084h.f6748a;
        mo35113a(i);
        mo35115a(z);
        mo35114a(hVar);
    }

    /* renamed from: b */
    public void mo35116b(int i, Thread thread, long j, String str, String str2, Throwable th) {
        if (mo35117d() && C3076d.C3077a.m7615a(this.f6749a, i)) {
            mo35076a(i, thread, j, str, str2, th);
        }
    }

    /* renamed from: a */
    public void mo35113a(int i) {
        this.f6749a = i;
    }

    /* renamed from: d */
    public boolean mo35117d() {
        return this.f6750b;
    }

    /* renamed from: a */
    public void mo35115a(boolean z) {
        this.f6750b = z;
    }

    /* renamed from: e */
    public C3084h mo35118e() {
        return this.f6751c;
    }

    /* renamed from: a */
    public void mo35114a(C3084h hVar) {
        this.f6751c = hVar;
    }
}
