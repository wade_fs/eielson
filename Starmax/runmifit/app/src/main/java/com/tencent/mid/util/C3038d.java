package com.tencent.mid.util;

import android.util.Log;
import com.baidu.mobstat.Config;
import org.greenrobot.greendao.generator.Schema;

/* renamed from: com.tencent.mid.util.d */
public final class C3038d {

    /* renamed from: a */
    private String f6605a = Schema.DEFAULT_NAME;

    /* renamed from: b */
    private boolean f6606b = true;

    /* renamed from: c */
    private int f6607c = 2;

    /* renamed from: a */
    public void mo34947a(boolean z) {
    }

    /* renamed from: a */
    public boolean mo34948a() {
        return this.f6606b;
    }

    public C3038d() {
    }

    public C3038d(String str) {
        this.f6605a = str;
    }

    /* renamed from: b */
    private String m7488b() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!stackTraceElement.isNativeMethod() && !stackTraceElement.getClassName().equals(Thread.class.getName()) && !stackTraceElement.getClassName().equals(getClass().getName())) {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + Config.TRACE_TODAY_VISIT_SPLIT + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo34946a(Object obj) {
        String str;
        if (this.f6607c <= 4) {
            String b = m7488b();
            if (b == null) {
                str = obj.toString();
            } else {
                str = b + " - " + obj;
            }
            Log.i(this.f6605a, str);
        }
    }

    /* renamed from: b */
    public void mo34950b(Object obj) {
        if (mo34948a()) {
            mo34946a(obj);
        }
    }

    /* renamed from: c */
    public void mo34951c(Object obj) {
        String str;
        if (this.f6607c <= 5) {
            String b = m7488b();
            if (b == null) {
                str = obj.toString();
            } else {
                str = b + " - " + obj;
            }
            Log.w(this.f6605a, str);
        }
    }

    /* renamed from: d */
    public void mo34952d(Object obj) {
        if (mo34948a()) {
            mo34951c(obj);
        }
    }

    /* renamed from: e */
    public void mo34953e(Object obj) {
        String str;
        if (this.f6607c <= 6) {
            String b = m7488b();
            if (b == null) {
                str = obj.toString();
            } else {
                str = b + " - " + obj;
            }
            Log.e(this.f6605a, str);
        }
    }

    /* renamed from: a */
    public void mo34945a(Exception exc) {
        if (this.f6607c <= 6) {
            StringBuffer stringBuffer = new StringBuffer();
            String b = m7488b();
            StackTraceElement[] stackTrace = exc.getStackTrace();
            if (b != null) {
                stringBuffer.append(b + " - " + exc + "\r\n");
            } else {
                stringBuffer.append(exc + "\r\n");
            }
            if (stackTrace != null && stackTrace.length > 0) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    if (stackTraceElement != null) {
                        stringBuffer.append("[ " + stackTraceElement.getFileName() + Config.TRACE_TODAY_VISIT_SPLIT + stackTraceElement.getLineNumber() + " ]\r\n");
                    }
                }
            }
            Log.e(this.f6605a, stringBuffer.toString());
        }
    }

    /* renamed from: f */
    public void mo34954f(Object obj) {
        if (mo34948a()) {
            mo34953e(obj);
        }
    }

    /* renamed from: b */
    public void mo34949b(Exception exc) {
        if (mo34948a()) {
            mo34945a(exc);
        }
    }

    /* renamed from: g */
    public void mo34955g(Object obj) {
        String str;
        if (this.f6607c <= 3) {
            String b = m7488b();
            if (b == null) {
                str = obj.toString();
            } else {
                str = b + " - " + obj;
            }
            Log.d(this.f6605a, str);
        }
    }

    /* renamed from: h */
    public void mo34956h(Object obj) {
        if (mo34948a()) {
            mo34955g(obj);
        }
    }
}
