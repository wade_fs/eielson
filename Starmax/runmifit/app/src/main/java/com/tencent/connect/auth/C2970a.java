package com.tencent.connect.auth;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.mapapi.UIMsg;
import com.google.android.gms.common.util.CrashUtils;
import com.tencent.connect.auth.C2981b;
import com.tencent.connect.common.Constants;
import com.tencent.open.SocialConstants;
import com.tencent.open.p059a.C3082f;
import com.tencent.open.p060b.C3094g;
import com.tencent.open.p061c.C3110c;
import com.tencent.open.utils.C3131k;
import com.tencent.open.utils.HttpUtils;
import com.tencent.open.web.security.C3137b;
import com.tencent.open.web.security.JniInterface;
import com.tencent.open.web.security.SecureJsInterface;
import com.tencent.tauth.IUiListener;
import com.tencent.tauth.UiError;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.connect.auth.a */
/* compiled from: ProGuard */
public class C2970a extends Dialog {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f6347a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C2978b f6348b;

    /* renamed from: c */
    private IUiListener f6349c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public Handler f6350d;

    /* renamed from: e */
    private FrameLayout f6351e;

    /* renamed from: f */
    private LinearLayout f6352f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public FrameLayout f6353g;

    /* renamed from: h */
    private ProgressBar f6354h;

    /* renamed from: i */
    private String f6355i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public C3110c f6356j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public Context f6357k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public C3137b f6358l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public boolean f6359m = false;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public int f6360n;
    /* access modifiers changed from: private */

    /* renamed from: o */
    public String f6361o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public String f6362p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public long f6363q = 0;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public long f6364r = 30000;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public HashMap<String, Runnable> f6365s;

    /* renamed from: a */
    static /* synthetic */ String m7198a(C2970a aVar, Object obj) {
        String str = aVar.f6347a + obj;
        aVar.f6347a = str;
        return str;
    }

    /* renamed from: m */
    static /* synthetic */ int m7221m(C2970a aVar) {
        int i = aVar.f6360n;
        aVar.f6360n = i + 1;
        return i;
    }

    public C2970a(Context context, String str, String str2, IUiListener iUiListener, QQToken qQToken) {
        super(context, 16973840);
        this.f6357k = context;
        this.f6347a = str2;
        this.f6348b = new C2978b(str, str2, qQToken.getAppId(), iUiListener);
        this.f6350d = new C2979c(this.f6348b, context.getMainLooper());
        this.f6349c = iUiListener;
        this.f6355i = str;
        this.f6358l = new C3137b();
        getWindow().setSoftInputMode(32);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        m7204b();
        m7211d();
        this.f6365s = new HashMap<>();
    }

    public void onBackPressed() {
        if (!this.f6359m) {
            this.f6348b.onCancel();
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
    }

    /* renamed from: com.tencent.connect.auth.a$c */
    /* compiled from: ProGuard */
    private class C2979c extends Handler {

        /* renamed from: b */
        private C2978b f6381b;

        public C2979c(C2978b bVar, Looper looper) {
            super(looper);
            this.f6381b = bVar;
        }

        public void handleMessage(Message message) {
            int i = message.what;
            if (i == 1) {
                this.f6381b.m7227a((String) message.obj);
            } else if (i == 2) {
                this.f6381b.onCancel();
            } else if (i == 3) {
                C2970a.m7205b(C2970a.this.f6357k, (String) message.obj);
            }
        }
    }

    /* renamed from: com.tencent.connect.auth.a$b */
    /* compiled from: ProGuard */
    private class C2978b implements IUiListener {

        /* renamed from: a */
        String f6375a;

        /* renamed from: b */
        String f6376b;

        /* renamed from: d */
        private String f6378d;

        /* renamed from: e */
        private IUiListener f6379e;

        public C2978b(String str, String str2, String str3, IUiListener iUiListener) {
            this.f6378d = str;
            this.f6375a = str2;
            this.f6376b = str3;
            this.f6379e = iUiListener;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m7227a(String str) {
            try {
                onComplete(C3131k.m7788d(str));
            } catch (JSONException e) {
                e.printStackTrace();
                onError(new UiError(-4, Constants.MSG_JSON_ERROR, str));
            }
        }

        public void onComplete(Object obj) {
            JSONObject jSONObject = (JSONObject) obj;
            C3094g a = C3094g.m7676a();
            a.mo35133a(this.f6378d + "_H5", SystemClock.elapsedRealtime(), 0, 0, jSONObject.optInt("ret", -6), this.f6375a, false);
            IUiListener iUiListener = this.f6379e;
            if (iUiListener != null) {
                iUiListener.onComplete(jSONObject);
                this.f6379e = null;
            }
        }

        public void onError(UiError uiError) {
            String str;
            if (uiError.errorMessage != null) {
                str = uiError.errorMessage + this.f6375a;
            } else {
                str = this.f6375a;
            }
            C3094g.m7676a().mo35133a(this.f6378d + "_H5", SystemClock.elapsedRealtime(), 0, 0, uiError.errorCode, str, false);
            String unused = C2970a.this.m7200a(str);
            IUiListener iUiListener = this.f6379e;
            if (iUiListener != null) {
                iUiListener.onError(uiError);
                this.f6379e = null;
            }
        }

        public void onCancel() {
            IUiListener iUiListener = this.f6379e;
            if (iUiListener != null) {
                iUiListener.onCancel();
                this.f6379e = null;
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m7200a(String str) {
        StringBuilder sb = new StringBuilder(str);
        if (!TextUtils.isEmpty(this.f6362p) && this.f6362p.length() >= 4) {
            String str2 = this.f6362p;
            String substring = str2.substring(str2.length() - 4);
            sb.append("_u_");
            sb.append(substring);
        }
        return sb.toString();
    }

    /* renamed from: com.tencent.connect.auth.a$a */
    /* compiled from: ProGuard */
    private class C2974a extends WebViewClient {
        private C2974a() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.tencent.connect.auth.a.a(com.tencent.connect.auth.a, java.lang.Object):java.lang.String
         arg types: [com.tencent.connect.auth.a, java.lang.String]
         candidates:
          com.tencent.connect.auth.a.a(com.tencent.connect.auth.a, long):long
          com.tencent.connect.auth.a.a(com.tencent.connect.auth.a, java.lang.String):java.lang.String
          com.tencent.connect.auth.a.a(android.content.Context, java.lang.String):void
          com.tencent.connect.auth.a.a(com.tencent.connect.auth.a, boolean):boolean
          com.tencent.connect.auth.a.a(java.lang.String, java.lang.String):void
          com.tencent.connect.auth.a.a(com.tencent.connect.auth.a, java.lang.Object):java.lang.String */
        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Uri uri;
            C3082f.m7628a("openSDK_LOG.AuthDialog", "-->Redirect URL: " + str);
            if (str.startsWith("auth://browser")) {
                JSONObject c = C3131k.m7786c(str);
                C2970a aVar = C2970a.this;
                boolean unused = aVar.f6359m = aVar.m7213e();
                if (!C2970a.this.f6359m) {
                    if (c.optString("fail_cb", null) != null) {
                        C2970a.this.mo34731a(c.optString("fail_cb"), "");
                    } else if (c.optInt("fall_to_wv") == 1) {
                        C2970a aVar2 = C2970a.this;
                        String str2 = "?";
                        if (aVar2.f6347a.indexOf(str2) > -1) {
                            str2 = "&";
                        }
                        C2970a.m7198a(aVar2, (Object) str2);
                        C2970a.m7198a(C2970a.this, (Object) "browser_error=1");
                        C2970a.this.f6356j.loadUrl(C2970a.this.f6347a);
                    } else {
                        String optString = c.optString("redir", null);
                        if (optString != null) {
                            C2970a.this.f6356j.loadUrl(optString);
                        }
                    }
                }
                return true;
            } else if (str.startsWith("auth://tauth.qq.com/")) {
                C2970a.this.f6348b.onComplete(C3131k.m7786c(str));
                C2970a.this.dismiss();
                return true;
            } else if (str.startsWith(Constants.CANCEL_URI)) {
                C2970a.this.f6348b.onCancel();
                C2970a.this.dismiss();
                return true;
            } else if (str.startsWith(Constants.CLOSE_URI)) {
                C2970a.this.dismiss();
                return true;
            } else if (str.startsWith(Constants.DOWNLOAD_URI) || str.endsWith(".apk")) {
                try {
                    if (str.startsWith(Constants.DOWNLOAD_URI)) {
                        uri = Uri.parse(Uri.decode(str.substring(11)));
                    } else {
                        uri = Uri.parse(Uri.decode(str));
                    }
                    Intent intent = new Intent("android.intent.action.VIEW", uri);
                    intent.addFlags(CrashUtils.ErrorDialogData.BINDER_CRASH);
                    C2970a.this.f6357k.startActivity(intent);
                } catch (Exception e) {
                    C3082f.m7632b("openSDK_LOG.AuthDialog", "-->start download activity exception, e: ", e);
                }
                return true;
            } else if (str.startsWith("auth://progress")) {
                try {
                    List<String> pathSegments = Uri.parse(str).getPathSegments();
                    if (pathSegments.isEmpty()) {
                        return true;
                    }
                    int intValue = Integer.valueOf(pathSegments.get(0)).intValue();
                    if (intValue == 0) {
                        C2970a.this.f6353g.setVisibility(View.GONE);
                        C2970a.this.f6356j.setVisibility(View.VISIBLE);
                    } else if (intValue == 1) {
                        C2970a.this.f6353g.setVisibility(View.VISIBLE);
                    }
                    return true;
                } catch (Exception unused2) {
                }
            } else if (str.startsWith("auth://onLoginSubmit")) {
                try {
                    List<String> pathSegments2 = Uri.parse(str).getPathSegments();
                    if (!pathSegments2.isEmpty()) {
                        String unused3 = C2970a.this.f6362p = pathSegments2.get(0);
                    }
                } catch (Exception unused4) {
                }
                return true;
            } else if (C2970a.this.f6358l.mo35069a(C2970a.this.f6356j, str)) {
                return true;
            } else {
                C3082f.m7634c("openSDK_LOG.AuthDialog", "-->Redirect URL: return false");
                return false;
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
            C3082f.m7634c("openSDK_LOG.AuthDialog", "-->onReceivedError, errorCode: " + i + " | description: " + str);
            if (!C3131k.m7783b(C2970a.this.f6357k)) {
                C2970a.this.f6348b.onError(new UiError(UIMsg.m_AppUI.MSG_CLICK_ITEM, "当前网络不可用，请稍后重试！", str2));
                C2970a.this.dismiss();
            } else if (!C2970a.this.f6361o.startsWith("http://qzs.qq.com/open/mobile/login/qzsjump.html?")) {
                long elapsedRealtime = SystemClock.elapsedRealtime() - C2970a.this.f6363q;
                if (C2970a.this.f6360n >= 1 || elapsedRealtime >= C2970a.this.f6364r) {
                    C2970a.this.f6356j.loadUrl(C2970a.this.m7197a());
                    return;
                }
                C2970a.m7221m(C2970a.this);
                C2970a.this.f6350d.postDelayed(new Runnable() {
                    /* class com.tencent.connect.auth.C2970a.C2974a.C29751 */

                    public void run() {
                        C2970a.this.f6356j.loadUrl(C2970a.this.f6361o);
                    }
                }, 500);
            } else {
                C2970a.this.f6348b.onError(new UiError(i, str, str2));
                C2970a.this.dismiss();
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            C3082f.m7628a("openSDK_LOG.AuthDialog", "-->onPageStarted, url: " + str);
            super.onPageStarted(webView, str, bitmap);
            C2970a.this.f6353g.setVisibility(View.VISIBLE);
            long unused = C2970a.this.f6363q = SystemClock.elapsedRealtime();
            if (!TextUtils.isEmpty(C2970a.this.f6361o)) {
                C2970a.this.f6350d.removeCallbacks((Runnable) C2970a.this.f6365s.remove(C2970a.this.f6361o));
            }
            String unused2 = C2970a.this.f6361o = str;
            C2970a aVar = C2970a.this;
            C2980d dVar = new C2980d(aVar.f6361o);
            C2970a.this.f6365s.put(str, dVar);
            C2970a.this.f6350d.postDelayed(dVar, 120000);
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            C3082f.m7628a("openSDK_LOG.AuthDialog", "-->onPageFinished, url: " + str);
            C2970a.this.f6353g.setVisibility(View.GONE);
            if (C2970a.this.f6356j != null) {
                C2970a.this.f6356j.setVisibility(View.VISIBLE);
            }
            if (!TextUtils.isEmpty(str)) {
                C2970a.this.f6350d.removeCallbacks((Runnable) C2970a.this.f6365s.remove(str));
            }
        }

        public void onReceivedSslError(WebView webView, final SslErrorHandler sslErrorHandler, SslError sslError) {
            String str;
            String str2;
            String str3;
            C3082f.m7636e("openSDK_LOG.AuthDialog", "-->onReceivedSslError " + sslError.getPrimaryError() + "请求不合法，请检查手机安全设置，如系统时间、代理等");
            if (Locale.getDefault().getLanguage().equals("zh")) {
                str2 = "ssl证书无效，是否继续访问？";
                str = "是";
                str3 = "否";
            } else {
                str2 = "The SSL certificate is invalid,do you countinue?";
                str = "yes";
                str3 = "no";
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(C2970a.this.f6357k);
            builder.setMessage(str2);
            builder.setPositiveButton(str, new DialogInterface.OnClickListener() {
                /* class com.tencent.connect.auth.C2970a.C2974a.C29762 */

                public void onClick(DialogInterface dialogInterface, int i) {
                    sslErrorHandler.proceed();
                }
            });
            builder.setNegativeButton(str3, new DialogInterface.OnClickListener() {
                /* class com.tencent.connect.auth.C2970a.C2974a.C29773 */

                public void onClick(DialogInterface dialogInterface, int i) {
                    sslErrorHandler.cancel();
                    C2970a.this.dismiss();
                }
            });
            builder.create().show();
        }
    }

    /* renamed from: com.tencent.connect.auth.a$d */
    /* compiled from: ProGuard */
    class C2980d implements Runnable {

        /* renamed from: a */
        String f6382a = "";

        public C2980d(String str) {
            this.f6382a = str;
        }

        public void run() {
            C3082f.m7628a("openSDK_LOG.AuthDialog", "-->timeoutUrl: " + this.f6382a + " | mRetryUrl: " + C2970a.this.f6361o);
            if (this.f6382a.equals(C2970a.this.f6361o)) {
                C2970a.this.f6348b.onError(new UiError(9002, "请求页面超时，请稍后重试！", C2970a.this.f6361o));
                C2970a.this.dismiss();
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m7197a() {
        String str = this.f6347a;
        String str2 = "http://qzs.qq.com/open/mobile/login/qzsjump.html?" + str.substring(str.indexOf("?") + 1);
        C3082f.m7634c("openSDK_LOG.AuthDialog", "-->generateDownloadUrl, url: http://qzs.qq.com/open/mobile/login/qzsjump.html?");
        return str2;
    }

    /* renamed from: b */
    private void m7204b() {
        m7208c();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.f6356j = new C3110c(this.f6357k);
        if (Build.VERSION.SDK_INT >= 11) {
            this.f6356j.setLayerType(1, null);
        }
        this.f6356j.setLayoutParams(layoutParams);
        this.f6351e = new FrameLayout(this.f6357k);
        layoutParams.gravity = 17;
        this.f6351e.setLayoutParams(layoutParams);
        this.f6351e.addView(this.f6356j);
        this.f6351e.addView(this.f6353g);
        setContentView(this.f6351e);
    }

    /* renamed from: c */
    private void m7208c() {
        TextView textView;
        this.f6354h = new ProgressBar(this.f6357k);
        this.f6354h.setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.f6352f = new LinearLayout(this.f6357k);
        if (this.f6355i.equals("action_login")) {
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.gravity = 16;
            layoutParams.leftMargin = 5;
            textView = new TextView(this.f6357k);
            if (Locale.getDefault().getLanguage().equals("zh")) {
                textView.setText("登录中...");
            } else {
                textView.setText("Logging in...");
            }
            textView.setTextColor(Color.rgb(255, 255, 255));
            textView.setTextSize(18.0f);
            textView.setLayoutParams(layoutParams);
        } else {
            textView = null;
        }
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-2, -2);
        layoutParams2.gravity = 17;
        this.f6352f.setLayoutParams(layoutParams2);
        this.f6352f.addView(this.f6354h);
        if (textView != null) {
            this.f6352f.addView(textView);
        }
        this.f6353g = new FrameLayout(this.f6357k);
        FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(-1, -2);
        layoutParams3.leftMargin = 80;
        layoutParams3.rightMargin = 80;
        layoutParams3.topMargin = 40;
        layoutParams3.bottomMargin = 40;
        layoutParams3.gravity = 17;
        this.f6353g.setLayoutParams(layoutParams3);
        this.f6353g.setBackgroundResource(17301504);
        this.f6353g.addView(this.f6352f);
    }

    /* renamed from: d */
    private void m7211d() {
        this.f6356j.setVerticalScrollBarEnabled(false);
        this.f6356j.setHorizontalScrollBarEnabled(false);
        this.f6356j.setWebViewClient(new C2974a());
        this.f6356j.setWebChromeClient(new WebChromeClient());
        this.f6356j.clearFormData();
        this.f6356j.clearSslPreferences();
        this.f6356j.setOnLongClickListener(new View.OnLongClickListener() {
            /* class com.tencent.connect.auth.C2970a.C29711 */

            public boolean onLongClick(View view) {
                return true;
            }
        });
        this.f6356j.setOnTouchListener(new View.OnTouchListener() {
            /* class com.tencent.connect.auth.C2970a.C29722 */

            public boolean onTouch(View view, MotionEvent motionEvent) {
                int action = motionEvent.getAction();
                if ((action != 0 && action != 1) || view.hasFocus()) {
                    return false;
                }
                view.requestFocus();
                return false;
            }
        });
        WebSettings settings = this.f6356j.getSettings();
        settings.setSavePassword(false);
        settings.setSaveFormData(false);
        settings.setCacheMode(-1);
        settings.setNeedInitialFocus(false);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setRenderPriority(WebSettings.RenderPriority.HIGH);
        settings.setJavaScriptEnabled(true);
        settings.setDatabaseEnabled(true);
        settings.setDatabasePath(this.f6357k.getDir("databases", 0).getPath());
        settings.setDomStorageEnabled(true);
        C3082f.m7628a("openSDK_LOG.AuthDialog", "-->mUrl : " + this.f6347a);
        String str = this.f6347a;
        this.f6361o = str;
        this.f6356j.loadUrl(str);
        this.f6356j.setVisibility(View.INVISIBLE);
        this.f6356j.getSettings().setSavePassword(false);
        this.f6358l.mo35067a(new SecureJsInterface(), "SecureJsInterface");
        SecureJsInterface.isPWDEdit = false;
        super.setOnDismissListener(new DialogInterface.OnDismissListener() {
            /* class com.tencent.connect.auth.C2970a.C29733 */

            public void onDismiss(DialogInterface dialogInterface) {
                try {
                    if (JniInterface.isJniOk) {
                        JniInterface.clearAllPWD();
                    }
                } catch (Exception unused) {
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public boolean m7213e() {
        C2981b a = C2981b.m7228a();
        String c = a.mo34750c();
        C2981b.C2982a aVar = new C2981b.C2982a();
        aVar.f6389a = this.f6349c;
        aVar.f6390b = this;
        aVar.f6391c = c;
        String a2 = a.mo34749a(aVar);
        String str = this.f6347a;
        String substring = str.substring(0, str.indexOf("?"));
        Bundle b = C3131k.m7779b(this.f6347a);
        b.putString("token_key", c);
        b.putString("serial", a2);
        b.putString("browser", "1");
        this.f6347a = substring + "?" + HttpUtils.encodeUrl(b);
        return C3131k.m7777a(this.f6357k, this.f6347a);
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public static void m7205b(Context context, String str) {
        try {
            JSONObject d = C3131k.m7788d(str);
            int i = d.getInt(SocialConstants.PARAM_TYPE);
            Toast.makeText(context.getApplicationContext(), d.getString("msg"), i).show();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public void mo34731a(String str, String str2) {
        this.f6356j.loadUrl("javascript:" + str + "(" + str2 + ");void(" + System.currentTimeMillis() + ");");
    }

    public void dismiss() {
        this.f6365s.clear();
        this.f6350d.removeCallbacksAndMessages(null);
        if (isShowing()) {
            super.dismiss();
        }
        C3110c cVar = this.f6356j;
        if (cVar != null) {
            cVar.destroy();
            this.f6356j = null;
        }
    }
}
