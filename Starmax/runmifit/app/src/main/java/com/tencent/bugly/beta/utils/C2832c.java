package com.tencent.bugly.beta.utils;

import android.util.Log;
import com.tencent.bugly.proguard.C2903an;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

/* renamed from: com.tencent.bugly.beta.utils.c */
/* compiled from: BUGLY */
public class C2832c {

    /* renamed from: u */
    private static HashMap<Long, String> f5500u = new HashMap<>();

    /* renamed from: a */
    private C2830a f5501a = null;

    /* renamed from: b */
    private String f5502b = null;

    /* renamed from: c */
    private long f5503c = 0;

    /* renamed from: d */
    private long f5504d = 0;

    /* renamed from: e */
    private HashMap<String, C2834b> f5505e = null;

    /* renamed from: f */
    private byte[] f5506f = new byte[16];

    /* renamed from: g */
    private long f5507g = 0;

    /* renamed from: h */
    private long f5508h = 0;

    /* renamed from: i */
    private String f5509i = null;

    /* renamed from: j */
    private long f5510j = 0;

    /* renamed from: k */
    private long f5511k = 0;

    /* renamed from: l */
    private long f5512l = 0;

    /* renamed from: m */
    private long f5513m = 0;

    /* renamed from: n */
    private long f5514n = 0;

    /* renamed from: o */
    private long f5515o = 0;

    /* renamed from: p */
    private long f5516p = 0;

    /* renamed from: q */
    private long f5517q = 0;

    /* renamed from: r */
    private long f5518r = 0;

    /* renamed from: s */
    private long f5519s = 0;

    /* renamed from: t */
    private long f5520t = 0;

    /* renamed from: a */
    private long m6352a(byte b) {
        if (1 == b) {
            return 32;
        }
        return 2 == b ? 64 : 0;
    }

    public C2832c(String str) {
        this.f5502b = str;
        f5500u.put(3L, "x86");
        f5500u.put(7L, "x86");
        f5500u.put(8L, "mips");
        f5500u.put(10L, "mips");
        f5500u.put(40L, "armeabi");
        f5500u.put(62L, "x86_64");
        f5500u.put(183L, "arm64-v8a");
    }

    /* renamed from: b */
    private String m6357b() {
        return this.f5509i;
    }

    /* renamed from: c */
    private long m6359c() {
        return this.f5513m;
    }

    /* renamed from: d */
    private long m6360d() {
        return this.f5519s;
    }

    /* renamed from: e */
    private long m6361e() {
        return this.f5520t;
    }

    /* renamed from: a */
    public synchronized String mo34204a() {
        if (!m6362f()) {
            Log.e("ElfParser", "Failed to parseElfHeader elf header");
            return null;
        }
        String b = m6357b();
        if (!b.equals("armeabi")) {
            return b;
        }
        if (!m6365i()) {
            Log.e("ElfParser", "Failed to parseElfHeader section table");
            return b;
        }
        C2834b bVar = this.f5505e.get(".ARM.attributes");
        if (bVar == null) {
            Log.e("ElfParser", "No .ARM.attributes section in the elf file");
            return b;
        }
        return C2831b.m6343a(this.f5502b, this.f5504d, bVar.mo34220a());
    }

    /* renamed from: a */
    private static String m6353a(long j, long j2) {
        String str = f5500u.get(Long.valueOf(j));
        return (64 != j2 || !str.equals("mips")) ? str : "mips64";
    }

    /* renamed from: f */
    private boolean m6362f() {
        if (!m6368l()) {
            return false;
        }
        if (!m6363g()) {
            m6366j();
            return false;
        }
        m6366j();
        return true;
    }

    /* renamed from: g */
    private synchronized boolean m6363g() {
        if (!m6364h()) {
            Log.e("ElfParser", "Faile to parseElfHeader header indent of elf");
            return false;
        }
        try {
            this.f5507g = this.f5501a.mo34201g();
            this.f5508h = this.f5501a.mo34201g();
            this.f5509i = m6353a(this.f5508h, this.f5503c);
            this.f5510j = this.f5501a.mo34202h();
            if (32 == this.f5503c) {
                long h = this.f5501a.mo34202h();
                this.f5510j = h;
                this.f5511k = h;
                this.f5512l = this.f5501a.mo34202h();
                this.f5513m = this.f5501a.mo34202h();
            } else if (64 == this.f5503c) {
                long i = this.f5501a.mo34203i();
                this.f5510j = i;
                this.f5511k = i;
                this.f5512l = this.f5501a.mo34203i();
                this.f5513m = this.f5501a.mo34203i();
            } else {
                Log.e("ElfParser", "File format error");
                return false;
            }
            this.f5514n = this.f5501a.mo34202h();
            this.f5515o = this.f5501a.mo34201g();
            this.f5516p = this.f5501a.mo34201g();
            this.f5517q = this.f5501a.mo34201g();
            this.f5518r = this.f5501a.mo34201g();
            this.f5519s = this.f5501a.mo34201g();
            this.f5520t = this.f5501a.mo34201g();
            return true;
        } catch (IOException e) {
            Log.e("ElfParser", e.getMessage());
            return false;
        }
    }

    /* renamed from: h */
    private synchronized boolean m6364h() {
        if (!this.f5501a.mo34194a(this.f5506f)) {
            Log.e("ElfParser", "Fail to parseElfHeader elf indentification");
            return false;
        } else if (!m6355a(this.f5506f)) {
            Log.e("ElfParser", "Not a elf file: " + this.f5502b);
            return false;
        } else {
            this.f5503c = m6352a(this.f5506f[4]);
            if (0 == this.f5503c) {
                Log.e("ElfParser", "File format error: " + ((int) this.f5506f[4]));
                return false;
            }
            this.f5504d = m6356b(this.f5506f[5]);
            if (C2830a.f5483a == this.f5504d) {
                Log.e("ElfParser", "Endian error: " + ((int) this.f5506f[5]));
                return false;
            }
            this.f5501a.mo34192a(this.f5504d);
            return true;
        }
    }

    /* renamed from: a */
    private static boolean m6355a(byte[] bArr) {
        if (bArr.length >= 3 && Byte.MAX_VALUE == bArr[0] && 69 == bArr[1] && 76 == bArr[2] && 70 == bArr[3]) {
            return true;
        }
        return false;
    }

    /* renamed from: b */
    private long m6356b(byte b) {
        if (1 == b) {
            return C2830a.f5485c;
        }
        if (2 == b) {
            return C2830a.f5484b;
        }
        return C2830a.f5483a;
    }

    /* renamed from: i */
    private synchronized boolean m6365i() {
        this.f5505e = m6354a(m6359c(), m6360d(), m6361e());
        if (this.f5505e == null) {
            return false;
        }
        return true;
    }

    /* renamed from: j */
    private synchronized void m6366j() {
        if (this.f5501a != null) {
            if (this.f5501a.mo34193a()) {
                this.f5501a = null;
            }
        }
    }

    /* renamed from: k */
    private synchronized boolean m6367k() {
        if (this.f5501a != null) {
            m6366j();
        }
        try {
            this.f5501a = new C2830a(this.f5502b, this.f5504d);
        } catch (Exception e) {
            Log.e("ElfParser", e.getMessage());
            return false;
        }
        return true;
    }

    /* renamed from: l */
    private synchronized boolean m6368l() {
        if (this.f5501a != null) {
            m6366j();
        }
        try {
            this.f5501a = new C2830a(this.f5502b);
        } catch (Exception e) {
            Log.e("ElfParser", e.getMessage());
            return false;
        }
        return true;
    }

    /* renamed from: a */
    private synchronized HashMap<String, C2834b> m6354a(long j, long j2, long j3) {
        if (!m6367k()) {
            m6366j();
            return null;
        } else if (!this.f5501a.mo34196b(j)) {
            m6366j();
            return null;
        } else {
            HashMap<String, C2834b> b = m6358b(j2, j3);
            m6366j();
            return b;
        }
    }

    /* renamed from: b */
    private synchronized HashMap<String, C2834b> m6358b(long j, long j2) {
        if (j <= 0 || j2 <= 0) {
            C2903an.m6864d("The SO file is invalid or has a shell.", new Object[0]);
            return null;
        }
        Vector vector = new Vector();
        for (int i = 0; ((long) i) < j; i++) {
            vector.add(m6369m());
        }
        C2833a aVar = (C2833a) vector.get((int) j2);
        long length = new File(this.f5502b).length();
        C2903an.m6863c("File length = %d", Long.valueOf(length));
        if (aVar.mo34209c() >= length) {
            C2903an.m6864d("The SO file is invalid or has a shell.", new Object[0]);
            return null;
        }
        C2835d dVar = new C2835d(this.f5502b, aVar.mo34209c(), aVar.mo34211d());
        HashMap<String, C2834b> hashMap = new HashMap<>();
        Iterator it = vector.iterator();
        while (it.hasNext()) {
            C2833a aVar2 = (C2833a) it.next();
            String a = dVar.mo34226a(aVar2.mo34205a());
            C2834b bVar = new C2834b();
            bVar.mo34222a(a);
            bVar.mo34221a(aVar2.mo34207b());
            bVar.mo34223b(aVar2.mo34209c());
            bVar.mo34224c(aVar2.mo34211d());
            bVar.mo34225d(aVar2.mo34213e());
            hashMap.put(a, bVar);
        }
        dVar.mo34227a();
        return hashMap;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x00b6, code lost:
        return r0;
     */
    /* renamed from: m */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized com.tencent.bugly.beta.utils.C2832c.C2833a m6369m() {
        /*
            r9 = this;
            monitor-enter(r9)
            com.tencent.bugly.beta.utils.c$a r0 = new com.tencent.bugly.beta.utils.c$a     // Catch:{ all -> 0x00d5 }
            r0.<init>()     // Catch:{ all -> 0x00d5 }
            r1 = 0
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34206a(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34208b(r2)     // Catch:{ IOException -> 0x00c9 }
            long r2 = r9.f5503c     // Catch:{ IOException -> 0x00c9 }
            r4 = 64
            r6 = 32
            int r8 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r8 != 0) goto L_0x0048
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34210c(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34214e(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34212d(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34215f(r2)     // Catch:{ IOException -> 0x00c9 }
            goto L_0x0072
        L_0x0048:
            long r2 = r9.f5503c     // Catch:{ IOException -> 0x00c9 }
            int r8 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r8 != 0) goto L_0x00c0
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34203i()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34210c(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34203i()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34214e(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34203i()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34212d(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34203i()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34215f(r2)     // Catch:{ IOException -> 0x00c9 }
        L_0x0072:
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34216g(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34217h(r2)     // Catch:{ IOException -> 0x00c9 }
            long r2 = r9.f5503c     // Catch:{ IOException -> 0x00c9 }
            int r8 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r8 != 0) goto L_0x009d
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34218i(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34202h()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34219j(r2)     // Catch:{ IOException -> 0x00c9 }
            goto L_0x00b5
        L_0x009d:
            long r2 = r9.f5503c     // Catch:{ IOException -> 0x00c9 }
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x00b7
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34203i()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34218i(r2)     // Catch:{ IOException -> 0x00c9 }
            com.tencent.bugly.beta.utils.a r2 = r9.f5501a     // Catch:{ IOException -> 0x00c9 }
            long r2 = r2.mo34203i()     // Catch:{ IOException -> 0x00c9 }
            r0.mo34219j(r2)     // Catch:{ IOException -> 0x00c9 }
        L_0x00b5:
            monitor-exit(r9)
            return r0
        L_0x00b7:
            java.lang.String r0 = "ElfParser"
            java.lang.String r2 = "File format error"
            android.util.Log.e(r0, r2)     // Catch:{ IOException -> 0x00c9 }
            monitor-exit(r9)
            return r1
        L_0x00c0:
            java.lang.String r0 = "ElfParser"
            java.lang.String r2 = "File format error"
            android.util.Log.e(r0, r2)     // Catch:{ IOException -> 0x00c9 }
            monitor-exit(r9)
            return r1
        L_0x00c9:
            r0 = move-exception
            java.lang.String r2 = "ElfParser"
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x00d5 }
            android.util.Log.e(r2, r0)     // Catch:{ all -> 0x00d5 }
            monitor-exit(r9)
            return r1
        L_0x00d5:
            r0 = move-exception
            monitor-exit(r9)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.utils.C2832c.m6369m():com.tencent.bugly.beta.utils.c$a");
    }

    /* renamed from: com.tencent.bugly.beta.utils.c$a */
    /* compiled from: BUGLY */
    public static class C2833a {

        /* renamed from: a */
        private long f5521a = -1;

        /* renamed from: b */
        private long f5522b = 0;

        /* renamed from: c */
        private long f5523c = 0;

        /* renamed from: d */
        private long f5524d = -1;

        /* renamed from: e */
        private long f5525e = -1;

        /* renamed from: f */
        private long f5526f = 0;

        /* renamed from: g */
        private long f5527g = 0;

        /* renamed from: h */
        private long f5528h = 0;

        /* renamed from: i */
        private long f5529i = 0;

        /* renamed from: j */
        private long f5530j = 0;

        /* renamed from: a */
        public long mo34205a() {
            return this.f5521a;
        }

        /* renamed from: b */
        public long mo34207b() {
            return this.f5524d;
        }

        /* renamed from: c */
        public long mo34209c() {
            return this.f5525e;
        }

        /* renamed from: d */
        public long mo34211d() {
            return this.f5526f;
        }

        /* renamed from: e */
        public long mo34213e() {
            return this.f5530j;
        }

        /* renamed from: a */
        public synchronized void mo34206a(long j) {
            this.f5521a = j;
        }

        /* renamed from: b */
        public synchronized void mo34208b(long j) {
            this.f5522b = j;
        }

        /* renamed from: c */
        public synchronized void mo34210c(long j) {
            this.f5523c = j;
        }

        /* renamed from: d */
        public synchronized void mo34212d(long j) {
            this.f5525e = j;
        }

        /* renamed from: e */
        public synchronized void mo34214e(long j) {
            this.f5524d = j;
        }

        /* renamed from: f */
        public synchronized void mo34215f(long j) {
            this.f5526f = j;
        }

        /* renamed from: g */
        public synchronized void mo34216g(long j) {
            this.f5527g = j;
        }

        /* renamed from: h */
        public synchronized void mo34217h(long j) {
            this.f5528h = j;
        }

        /* renamed from: i */
        public synchronized void mo34218i(long j) {
            this.f5529i = j;
        }

        /* renamed from: j */
        public synchronized void mo34219j(long j) {
            this.f5530j = j;
        }
    }

    /* renamed from: com.tencent.bugly.beta.utils.c$b */
    /* compiled from: BUGLY */
    public static class C2834b {

        /* renamed from: a */
        private String f5531a = null;

        /* renamed from: b */
        private long f5532b = -1;

        /* renamed from: c */
        private long f5533c = -1;

        /* renamed from: d */
        private long f5534d = 0;

        /* renamed from: e */
        private long f5535e = 0;

        /* renamed from: a */
        public long mo34220a() {
            return this.f5533c;
        }

        /* renamed from: a */
        public synchronized void mo34222a(String str) {
            this.f5531a = str;
        }

        /* renamed from: a */
        public synchronized void mo34221a(long j) {
            this.f5532b = j;
        }

        /* renamed from: b */
        public synchronized void mo34223b(long j) {
            this.f5533c = j;
        }

        /* renamed from: c */
        public synchronized void mo34224c(long j) {
            this.f5534d = j;
        }

        /* renamed from: d */
        public synchronized void mo34225d(long j) {
            this.f5535e = j;
        }
    }
}
