package com.tencent.stat.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import com.tencent.stat.StatConfig;
import java.util.Map;

public class StatPreferences {

    /* renamed from: a */
    private static SharedPreferences f7220a;

    /* renamed from: a */
    static synchronized SharedPreferences m7966a(Context context) {
        SharedPreferences sharedPreferences;
        synchronized (StatPreferences.class) {
            if (f7220a == null) {
                try {
                    if (StatConfig.getMTAPreferencesFileName() != null) {
                        if (StatConfig.getMTAPreferencesFileName().trim().length() != 0) {
                            f7220a = context.getSharedPreferences(StatConfig.getMTAPreferencesFileName(), 0);
                        }
                    }
                    f7220a = PreferenceManager.getDefaultSharedPreferences(context);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            sharedPreferences = f7220a;
        }
        return sharedPreferences;
    }

    public static boolean contains(Context context, String str) {
        return m7966a(context).contains(StatCommonHelper.getTagForConcurrentProcess(context, "" + str));
    }

    public static long getLong(Context context, String str, long j) {
        return m7966a(context).getLong(StatCommonHelper.getTagForConcurrentProcess(context, "" + str), j);
    }

    public static void putLong(Context context, String str, long j) {
        String tagForConcurrentProcess = StatCommonHelper.getTagForConcurrentProcess(context, "" + str);
        SharedPreferences.Editor edit = m7966a(context).edit();
        edit.putLong(tagForConcurrentProcess, j);
        edit.commit();
    }

    public static int getInt(Context context, String str, int i) {
        return m7966a(context).getInt(StatCommonHelper.getTagForConcurrentProcess(context, "" + str), i);
    }

    public static void putInt(Context context, String str, int i) {
        String tagForConcurrentProcess = StatCommonHelper.getTagForConcurrentProcess(context, "" + str);
        SharedPreferences.Editor edit = m7966a(context).edit();
        edit.putInt(tagForConcurrentProcess, i);
        edit.commit();
    }

    public static String getString(Context context, String str, String str2) {
        return m7966a(context).getString(StatCommonHelper.getTagForConcurrentProcess(context, "" + str), str2);
    }

    public static void putString(Context context, String str, String str2) {
        String tagForConcurrentProcess = StatCommonHelper.getTagForConcurrentProcess(context, "" + str);
        SharedPreferences.Editor edit = m7966a(context).edit();
        edit.putString(tagForConcurrentProcess, str2);
        edit.commit();
    }

    public static float getFloat(Context context, String str, float f) {
        return m7966a(context).getFloat(StatCommonHelper.getTagForConcurrentProcess(context, "" + str), f);
    }

    public static void putFloat(Context context, String str, float f) {
        String tagForConcurrentProcess = StatCommonHelper.getTagForConcurrentProcess(context, "" + str);
        SharedPreferences.Editor edit = m7966a(context).edit();
        edit.putFloat(tagForConcurrentProcess, f);
        edit.commit();
    }

    public static boolean getBoolean(Context context, String str, boolean z) {
        return m7966a(context).getBoolean(StatCommonHelper.getTagForConcurrentProcess(context, "" + str), z);
    }

    public static void putBoolean(Context context, String str, boolean z) {
        String tagForConcurrentProcess = StatCommonHelper.getTagForConcurrentProcess(context, "" + str);
        SharedPreferences.Editor edit = m7966a(context).edit();
        edit.putBoolean(tagForConcurrentProcess, z);
        edit.commit();
    }

    public static Map<String, ?> getAll(Context context) {
        return m7966a(context).getAll();
    }

    public static void remove(Context context, String str) {
        String tagForConcurrentProcess = StatCommonHelper.getTagForConcurrentProcess(context, "" + str);
        if (m7966a(context).contains(tagForConcurrentProcess)) {
            SharedPreferences.Editor edit = m7966a(context).edit();
            edit.remove(tagForConcurrentProcess);
            edit.commit();
        }
    }
}
