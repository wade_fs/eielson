package com.tencent.bugly.proguard;

import android.content.Context;
import com.tencent.bugly.BUGLY;
import com.tencent.bugly.C2797b;
import com.tencent.bugly.crashreport.biz.UserInfoBean;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.info.C2852b;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.ah */
/* compiled from: BUGLY */
public class C2893ah {
    /* renamed from: a */
    public static C2931bl m6791a(UserInfoBean userInfoBean, C2851a aVar) {
        if (userInfoBean == null) {
            return null;
        }
        C2931bl blVar = new C2931bl();
        blVar.f6167a = userInfoBean.f5549e;
        blVar.f6171e = userInfoBean.f5554j;
        blVar.f6170d = userInfoBean.f5547c;
        blVar.f6169c = userInfoBean.f5548d;
        blVar.f6173g = C2851a.m6471b().mo34312i();
        blVar.f6174h = userInfoBean.f5559o == 1;
        int i = userInfoBean.f5546b;
        if (i == 1) {
            blVar.f6168b = 1;
        } else if (i == 2) {
            blVar.f6168b = 4;
        } else if (i == 3) {
            blVar.f6168b = 2;
        } else if (i == 4) {
            blVar.f6168b = 3;
        } else if (userInfoBean.f5546b < 10 || userInfoBean.f5546b >= 20) {
            C2903an.m6865e("unknown uinfo type %d ", Integer.valueOf(userInfoBean.f5546b));
            return null;
        } else {
            blVar.f6168b = (byte) userInfoBean.f5546b;
        }
        blVar.f6172f = new HashMap();
        if (userInfoBean.f5560p >= 0) {
            Map<String, String> map = blVar.f6172f;
            map.put("C01", "" + userInfoBean.f5560p);
        }
        if (userInfoBean.f5561q >= 0) {
            Map<String, String> map2 = blVar.f6172f;
            map2.put("C02", "" + userInfoBean.f5561q);
        }
        if (userInfoBean.f5562r != null && userInfoBean.f5562r.size() > 0) {
            for (Map.Entry entry : userInfoBean.f5562r.entrySet()) {
                Map<String, String> map3 = blVar.f6172f;
                map3.put("C03_" + ((String) entry.getKey()), entry.getValue());
            }
        }
        if (userInfoBean.f5563s != null && userInfoBean.f5563s.size() > 0) {
            for (Map.Entry entry2 : userInfoBean.f5563s.entrySet()) {
                Map<String, String> map4 = blVar.f6172f;
                map4.put("C04_" + ((String) entry2.getKey()), entry2.getValue());
            }
        }
        Map<String, String> map5 = blVar.f6172f;
        StringBuilder sb = new StringBuilder();
        sb.append("");
        sb.append(!userInfoBean.f5556l);
        map5.put("A36", sb.toString());
        Map<String, String> map6 = blVar.f6172f;
        map6.put("F02", "" + userInfoBean.f5551g);
        Map<String, String> map7 = blVar.f6172f;
        map7.put("F03", "" + userInfoBean.f5552h);
        Map<String, String> map8 = blVar.f6172f;
        map8.put("F04", "" + userInfoBean.f5554j);
        Map<String, String> map9 = blVar.f6172f;
        map9.put("F05", "" + userInfoBean.f5553i);
        Map<String, String> map10 = blVar.f6172f;
        map10.put("F06", "" + userInfoBean.f5557m);
        Map<String, String> map11 = blVar.f6172f;
        map11.put("F10", "" + userInfoBean.f5555k);
        C2903an.m6863c("summary type %d vm:%d", Byte.valueOf(blVar.f6168b), Integer.valueOf(blVar.f6172f.size()));
        return blVar;
    }

    /* renamed from: a */
    public static C2932bm m6792a(List<UserInfoBean> list, int i) {
        C2851a b;
        if (list == null || list.size() == 0 || (b = C2851a.m6471b()) == null) {
            return null;
        }
        b.mo34323t();
        C2932bm bmVar = new C2932bm();
        bmVar.f6178b = b.f5660e;
        bmVar.f6179c = b.mo34311h();
        ArrayList<C2931bl> arrayList = new ArrayList<>();
        for (UserInfoBean userInfoBean : list) {
            C2931bl a = m6791a(userInfoBean, b);
            if (a != null) {
                arrayList.add(a);
            }
        }
        bmVar.f6180d = arrayList;
        bmVar.f6181e = new HashMap();
        Map<String, String> map = bmVar.f6181e;
        map.put("A7", "" + b.f5667l);
        Map<String, String> map2 = bmVar.f6181e;
        map2.put("A6", "" + b.mo34322s());
        Map<String, String> map3 = bmVar.f6181e;
        map3.put("A5", "" + b.mo34321r());
        Map<String, String> map4 = bmVar.f6181e;
        map4.put("A2", "" + b.mo34319p());
        Map<String, String> map5 = bmVar.f6181e;
        map5.put("A1", "" + b.mo34319p());
        Map<String, String> map6 = bmVar.f6181e;
        map6.put("A24", "" + b.f5669n);
        Map<String, String> map7 = bmVar.f6181e;
        map7.put("A17", "" + b.mo34320q());
        Map<String, String> map8 = bmVar.f6181e;
        map8.put("A15", "" + b.mo34326w());
        Map<String, String> map9 = bmVar.f6181e;
        map9.put("A13", "" + b.mo34327x());
        Map<String, String> map10 = bmVar.f6181e;
        map10.put("F08", "" + b.f5600B);
        Map<String, String> map11 = bmVar.f6181e;
        map11.put("F09", "" + b.f5601C);
        Map<String, String> G = b.mo34277G();
        if (G != null && G.size() > 0) {
            for (Map.Entry entry : G.entrySet()) {
                Map<String, String> map12 = bmVar.f6181e;
                map12.put("C04_" + ((String) entry.getKey()), entry.getValue());
            }
        }
        if (i == 1) {
            bmVar.f6177a = 1;
        } else if (i != 2) {
            C2903an.m6865e("unknown up type %d ", Integer.valueOf(i));
            return null;
        } else {
            bmVar.f6177a = 2;
        }
        return bmVar;
    }

    /* renamed from: a */
    public static <T extends C2944m> T m6793a(byte[] bArr, Class cls) {
        if (bArr != null && bArr.length > 0) {
            try {
                T t = (C2944m) cls.newInstance();
                C2941k kVar = new C2941k(bArr);
                kVar.mo34617a("utf-8");
                t.mo34475a(kVar);
                return t;
            } catch (Throwable th) {
                if (!C2903an.m6861b(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    public static C2927bh m6789a(Context context, int i, byte[] bArr) {
        C2851a b = C2851a.m6471b();
        StrategyBean c = C2854a.m6574a().mo34340c();
        if (b == null || c == null) {
            C2903an.m6865e("Can not create request pkg for parameters is invalid.", new Object[0]);
            return null;
        }
        try {
            C2927bh bhVar = new C2927bh();
            synchronized (b) {
                bhVar.f6115a = b.f5658c;
                bhVar.f6116b = b.mo34307f();
                bhVar.f6117c = b.f5659d;
                bhVar.f6118d = b.f5671p;
                bhVar.f6119e = b.f5673r;
                b.getClass();
                bhVar.f6120f = "2.6.5";
                bhVar.f6121g = i;
                bhVar.f6122h = bArr == null ? "".getBytes() : bArr;
                bhVar.f6123i = b.f5668m;
                bhVar.f6124j = b.f5669n;
                bhVar.f6125k = new HashMap();
                bhVar.f6126l = b.mo34305e();
                bhVar.f6127m = c.f5702p;
                bhVar.f6129o = b.mo34311h();
                bhVar.f6130p = C2852b.m6545f(context);
                bhVar.f6131q = System.currentTimeMillis();
                bhVar.f6132r = "" + b.mo34314k();
                bhVar.f6133s = b.mo34313j();
                bhVar.f6134t = "" + b.mo34316m();
                bhVar.f6135u = b.mo34315l();
                bhVar.f6136v = "" + b.mo34317n();
                bhVar.f6137w = bhVar.f6130p;
                b.getClass();
                bhVar.f6128n = "com.tencent.bugly";
                Map<String, String> map = bhVar.f6125k;
                map.put("A26", "" + b.mo34328y());
                Map<String, String> map2 = bhVar.f6125k;
                map2.put("A60", "" + b.mo34329z());
                Map<String, String> map3 = bhVar.f6125k;
                map3.put("A61", "" + b.mo34271A());
                Map<String, String> map4 = bhVar.f6125k;
                map4.put("A62", "" + b.mo34288R());
                Map<String, String> map5 = bhVar.f6125k;
                map5.put("A63", "" + b.mo34289S());
                Map<String, String> map6 = bhVar.f6125k;
                map6.put("F11", "" + b.f5604F);
                Map<String, String> map7 = bhVar.f6125k;
                map7.put("F12", "" + b.f5603E);
                Map<String, String> map8 = bhVar.f6125k;
                map8.put("G1", "" + b.mo34324u());
                Map<String, String> map9 = bhVar.f6125k;
                map9.put("A64", "" + b.mo34290T());
                if (b.f5608J) {
                    Map<String, String> map10 = bhVar.f6125k;
                    map10.put("G2", "" + b.mo34282L());
                    Map<String, String> map11 = bhVar.f6125k;
                    map11.put("G3", "" + b.mo34283M());
                    Map<String, String> map12 = bhVar.f6125k;
                    map12.put("G4", "" + b.mo34284N());
                    Map<String, String> map13 = bhVar.f6125k;
                    map13.put("G5", "" + b.mo34285O());
                    Map<String, String> map14 = bhVar.f6125k;
                    map14.put("G6", "" + b.mo34286P());
                    Map<String, String> map15 = bhVar.f6125k;
                    map15.put("G7", "" + Long.toString(b.mo34287Q()));
                }
                Map<String, String> map16 = bhVar.f6125k;
                map16.put("D3", "" + b.f5672q);
                if (C2797b.f5302b != null) {
                    for (BUGLY aVar : C2797b.f5302b) {
                        if (!(aVar.versionKey == null || aVar.version == null)) {
                            bhVar.f6125k.put(aVar.versionKey, aVar.version);
                        }
                    }
                }
                bhVar.f6125k.put("G15", C2908aq.m6938c("G15", ""));
                bhVar.f6125k.put("D4", C2908aq.m6938c("D4", "0"));
            }
            C2896ak a = C2896ak.m6805a();
            if (!(a == null || a.f5976b || bArr == null)) {
                bhVar.f6122h = C2908aq.m6921a(bhVar.f6122h, 2, 1, c.f5707u);
                if (bhVar.f6122h == null) {
                    C2903an.m6865e("reqPkg sbuffer error!", new Object[0]);
                    return null;
                }
            }
            Map<String, String> F = b.mo34276F();
            if (F != null) {
                for (Map.Entry entry : F.entrySet()) {
                    bhVar.f6125k.put(entry.getKey(), entry.getValue());
                }
            }
            return bhVar;
        } catch (Throwable th) {
            if (!C2903an.m6861b(th)) {
                th.printStackTrace();
            }
            return null;
        }
    }

    /* renamed from: a */
    public static byte[] m6795a(Object obj) {
        try {
            C2935e eVar = new C2935e();
            eVar.mo34585b();
            eVar.mo34580a("utf-8");
            eVar.mo34586a(1);
            eVar.mo34589b("RqdServer");
            eVar.mo34590c("sync");
            eVar.mo34581a("detail", obj);
            return eVar.mo34583a();
        } catch (Throwable th) {
            if (C2903an.m6861b(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }

    /* renamed from: a */
    public static C2928bi m6790a(byte[] bArr, StrategyBean strategyBean, boolean z) {
        if (bArr != null) {
            try {
                C2935e eVar = new C2935e();
                eVar.mo34585b();
                eVar.mo34580a("utf-8");
                eVar.mo34582a(bArr);
                Object b = eVar.mo34584b("detail", new C2928bi());
                C2928bi cast = C2928bi.class.isInstance(b) ? C2928bi.class.cast(b) : null;
                if (!z && cast != null && cast.f6143c != null && cast.f6143c.length > 0) {
                    C2903an.m6863c("resp buf %d", Integer.valueOf(cast.f6143c.length));
                    cast.f6143c = C2908aq.m6935b(cast.f6143c, 2, 1, StrategyBean.f5690d);
                    if (cast.f6143c == null) {
                        C2903an.m6865e("resp sbuffer error!", new Object[0]);
                        return null;
                    }
                }
                return cast;
            } catch (Throwable th) {
                if (!C2903an.m6861b(th)) {
                    th.printStackTrace();
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    public static byte[] m6794a(C2944m mVar) {
        try {
            C2943l lVar = new C2943l();
            lVar.mo34638a("utf-8");
            mVar.mo34476a(lVar);
            return lVar.mo34661b();
        } catch (Throwable th) {
            if (C2903an.m6861b(th)) {
                return null;
            }
            th.printStackTrace();
            return null;
        }
    }
}
