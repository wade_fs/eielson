package com.tencent.mid.util;

import android.content.Context;
import com.tencent.mid.p054b.C3028d;

/* renamed from: com.tencent.mid.util.b */
public class C3034b {

    /* renamed from: c */
    private static C3038d f6570c = Util.getLogger();

    /* renamed from: d */
    private static C3034b f6571d = null;

    /* renamed from: e */
    private static int f6572e = 0;

    /* renamed from: f */
    private static String f6573f = null;

    /* renamed from: g */
    private static String f6574g = null;

    /* renamed from: h */
    private static String f6575h = null;

    /* renamed from: i */
    private static String f6576i = null;

    /* renamed from: j */
    private static String f6577j = null;

    /* renamed from: k */
    private static String f6578k = null;

    /* renamed from: a */
    private C3028d f6579a = null;

    /* renamed from: b */
    private Context f6580b = null;

    /* renamed from: d */
    public String mo34940d() {
        return "/request";
    }

    /* renamed from: e */
    public String mo34941e() {
        return "/request_new";
    }

    /* renamed from: f */
    public String mo34942f() {
        return "/verify";
    }

    private C3034b(Context context) {
        this.f6580b = context;
        this.f6579a = new C3028d(context, 0);
    }

    /* renamed from: a */
    public static synchronized C3034b m7478a(Context context) {
        C3034b bVar;
        synchronized (C3034b.class) {
            if (f6571d == null) {
                f6571d = new C3034b(context);
            }
            bVar = f6571d;
        }
        return bVar;
    }

    /* renamed from: a */
    public int mo34937a() {
        try {
            if (f6572e == 0) {
                f6572e = Integer.parseInt(this.f6579a.mo34893b("teg_mid_key_version"));
            }
            return f6572e;
        } catch (Exception unused) {
            return 2;
        }
    }

    /* renamed from: b */
    public String mo34938b() {
        try {
            if (Util.isEmpty(f6573f)) {
                f6573f = this.f6579a.mo34893b("teg_mid_rsa_pk");
            }
            if (Util.isEmpty(f6573f)) {
                return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5zQz+I9s/DqreFr8dkd6wSdYDngK9T36rtlDPM6VJHjWQHv6FK83xbDoX6hgcZMPYIIawcwRCVPZNetRlsAnztAt7b71z9NvPaF24+fhHe8Sy3Z/Z2JxvGXsjDnejZzdiuHTS+FGUSjcX+CzyqB30yX0AV+LgxXtQe9aRpT5yo5W6jc2UpEhBYCjpGlmW1mksAwWbyvWSEUTkUD7n9uP7C8eFEh5DHnaTwzxAQtzSxQVC1ZopnC3ly/LhMRl8GFXsFlRzg4VTkSdwS/amyWtkKjfHXp7qh4ySBqY9HEGaoZIHrXGv3VtpXoTgGraj+5HPArW0wqQroUOYVx48xRs6QIDAQAB";
            }
            return f6573f;
        } catch (Exception unused) {
            return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA5zQz+I9s/DqreFr8dkd6wSdYDngK9T36rtlDPM6VJHjWQHv6FK83xbDoX6hgcZMPYIIawcwRCVPZNetRlsAnztAt7b71z9NvPaF24+fhHe8Sy3Z/Z2JxvGXsjDnejZzdiuHTS+FGUSjcX+CzyqB30yX0AV+LgxXtQe9aRpT5yo5W6jc2UpEhBYCjpGlmW1mksAwWbyvWSEUTkUD7n9uP7C8eFEh5DHnaTwzxAQtzSxQVC1ZopnC3ly/LhMRl8GFXsFlRzg4VTkSdwS/amyWtkKjfHXp7qh4ySBqY9HEGaoZIHrXGv3VtpXoTgGraj+5HPArW0wqQroUOYVx48xRs6QIDAQAB";
        }
    }

    /* renamed from: c */
    public String mo34939c() {
        try {
            if (Util.isEmpty(f6574g)) {
                f6574g = this.f6579a.mo34893b("teg_mid_http_service");
            }
            if (Util.isEmpty(f6574g)) {
                return "pingmid.qq.com:80";
            }
            return f6574g;
        } catch (Exception unused) {
            return "pingmid.qq.com:80";
        }
    }
}
