package com.tencent.stat.common;

import android.util.Log;
import com.baidu.mobstat.Config;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatCustomLogger;
import org.greenrobot.greendao.generator.Schema;

public final class StatLogger {

    /* renamed from: a */
    private String f7217a = Schema.DEFAULT_NAME;

    /* renamed from: b */
    private boolean f7218b = true;

    /* renamed from: c */
    private int f7219c = 2;

    public boolean isDebugEnable() {
        return this.f7218b;
    }

    public void setDebugEnable(boolean z) {
        this.f7218b = z;
    }

    public int getLogLevel() {
        return this.f7219c;
    }

    public void setLogLevel(int i) {
        this.f7219c = i;
    }

    public StatLogger() {
    }

    public StatLogger(String str) {
        this.f7217a = str;
    }

    public void setTag(String str) {
        this.f7217a = str;
    }

    /* renamed from: a */
    private String m7959a() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (StackTraceElement stackTraceElement : stackTrace) {
            if (!stackTraceElement.isNativeMethod() && !stackTraceElement.getClassName().equals(Thread.class.getName()) && !stackTraceElement.getClassName().equals(getClass().getName())) {
                return "[" + Thread.currentThread().getName() + "(" + Thread.currentThread().getId() + "): " + stackTraceElement.getFileName() + Config.TRACE_TODAY_VISIT_SPLIT + stackTraceElement.getLineNumber() + "]";
            }
        }
        return null;
    }

    public void info(Object obj) {
        String str;
        if (obj != null && this.f7219c <= 4) {
            String a = m7959a();
            if (a == null) {
                str = obj.toString();
            } else {
                str = a + " - " + obj;
            }
            Log.i(this.f7217a, str);
            StatCustomLogger customLogger = StatConfig.getCustomLogger();
            if (customLogger != null) {
                customLogger.info(str);
            }
        }
    }

    /* renamed from: i */
    public void mo35388i(Object obj) {
        if (isDebugEnable()) {
            info(obj);
        }
    }

    public void verbose(Object obj) {
        String str;
        if (this.f7219c <= 2) {
            String a = m7959a();
            if (a == null) {
                str = obj.toString();
            } else {
                str = a + " - " + obj;
            }
            Log.v(this.f7217a, str);
            StatCustomLogger customLogger = StatConfig.getCustomLogger();
            if (customLogger != null) {
                customLogger.verbose(str);
            }
        }
    }

    /* renamed from: v */
    public void mo35394v(Object obj) {
        if (isDebugEnable()) {
            verbose(obj);
        }
    }

    public void warn(Object obj) {
        String str;
        if (this.f7219c <= 5) {
            String a = m7959a();
            if (a == null) {
                str = obj.toString();
            } else {
                str = a + " - " + obj;
            }
            Log.w(this.f7217a, str);
            StatCustomLogger customLogger = StatConfig.getCustomLogger();
            if (customLogger != null) {
                customLogger.warn(str);
            }
        }
    }

    /* renamed from: w */
    public void mo35396w(Object obj) {
        if (isDebugEnable()) {
            warn(obj);
        }
    }

    public void error(Object obj) {
        String str;
        if (this.f7219c <= 6) {
            String a = m7959a();
            if (a == null) {
                str = obj.toString();
            } else {
                str = a + " - " + obj;
            }
            Log.e(this.f7217a, str);
            StatCustomLogger customLogger = StatConfig.getCustomLogger();
            if (customLogger != null) {
                customLogger.error(str);
            }
        }
    }

    public void error(Throwable th) {
        if (this.f7219c <= 6) {
            Log.e(this.f7217a, "", th);
            StatCustomLogger customLogger = StatConfig.getCustomLogger();
            if (customLogger != null) {
                customLogger.error(th);
            }
        }
    }

    /* renamed from: e */
    public void mo35383e(Object obj) {
        if (isDebugEnable()) {
            error(obj);
        }
    }

    /* renamed from: e */
    public void mo35384e(Throwable th) {
        if (isDebugEnable()) {
            error(th);
        }
    }

    public void debug(Object obj) {
        String str;
        if (this.f7219c <= 3) {
            String a = m7959a();
            if (a == null) {
                str = obj.toString();
            } else {
                str = a + " - " + obj;
            }
            Log.d(this.f7217a, str);
            StatCustomLogger customLogger = StatConfig.getCustomLogger();
            if (customLogger != null) {
                customLogger.debug(str);
            }
        }
    }

    /* renamed from: d */
    public void mo35381d(Object obj) {
        if (isDebugEnable()) {
            debug(obj);
        }
    }
}
