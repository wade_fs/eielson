package com.tencent.mid.api;

import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;
import org.json.JSONException;
import org.json.JSONObject;

public class MidEntity {
    public static final String TAG_IMEI = "imei";
    public static final String TAG_IMSI = "imsi";
    public static final String TAG_MAC = "mac";
    public static final String TAG_MID = "mid";
    public static final String TAG_TIMESTAMPS = "ts";
    public static final String TAG_VER = "ver";
    public static final int TYPE_DEFAULT = 1;
    public static final int TYPE_NEW = 2;

    /* renamed from: a */
    private static C3038d f6531a = Util.getLogger();

    /* renamed from: b */
    private String f6532b = null;

    /* renamed from: c */
    private String f6533c = null;

    /* renamed from: d */
    private String f6534d = null;

    /* renamed from: e */
    private String f6535e = "0";

    /* renamed from: f */
    private long f6536f = 0;

    /* renamed from: g */
    private int f6537g = 0;

    public int getVersion() {
        return this.f6537g;
    }

    public void setVersion(int i) {
        this.f6537g = i;
    }

    public long getTimestamps() {
        return this.f6536f;
    }

    public void setTimestamps(long j) {
        this.f6536f = j;
    }

    public boolean isMidValid() {
        return Util.isMidValid(this.f6535e);
    }

    public static MidEntity parse(String str) {
        MidEntity midEntity = new MidEntity();
        if (Util.isStringValid(str)) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (!jSONObject.isNull(TAG_IMEI)) {
                    midEntity.setImei(jSONObject.getString(TAG_IMEI));
                }
                if (!jSONObject.isNull(TAG_IMSI)) {
                    midEntity.setImsi(jSONObject.getString(TAG_IMSI));
                }
                if (!jSONObject.isNull(TAG_MAC)) {
                    midEntity.setMac(jSONObject.getString(TAG_MAC));
                }
                if (!jSONObject.isNull("mid")) {
                    midEntity.setMid(jSONObject.getString("mid"));
                }
                if (!jSONObject.isNull("ts")) {
                    midEntity.setTimestamps(jSONObject.getLong("ts"));
                }
                if (!jSONObject.isNull("ver")) {
                    midEntity.f6537g = jSONObject.optInt("ver", 0);
                }
            } catch (JSONException e) {
                f6531a.mo34952d(e.toString());
            }
        }
        return midEntity;
    }

    public int compairTo(MidEntity midEntity) {
        if (midEntity == null) {
            return 1;
        }
        if (!isMidValid() || !midEntity.isMidValid()) {
            if (isMidValid()) {
                return 1;
            }
            return -1;
        } else if (this.f6535e.equals(midEntity.f6535e)) {
            return 0;
        } else {
            if (this.f6536f >= midEntity.f6536f) {
                return 1;
            }
            return -1;
        }
    }

    public String toString() {
        return mo34852a().toString();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public JSONObject mo34852a() {
        JSONObject jSONObject = new JSONObject();
        try {
            Util.jsonPut(jSONObject, TAG_IMEI, this.f6532b);
            Util.jsonPut(jSONObject, TAG_IMSI, this.f6533c);
            Util.jsonPut(jSONObject, TAG_MAC, this.f6534d);
            Util.jsonPut(jSONObject, "mid", this.f6535e);
            jSONObject.put("ts", this.f6536f);
        } catch (JSONException e) {
            f6531a.mo34952d(e.toString());
        }
        return jSONObject;
    }

    public String getMid() {
        return this.f6535e;
    }

    public void setMid(String str) {
        this.f6535e = str;
    }

    public String getImei() {
        return this.f6532b;
    }

    public void setImei(String str) {
        this.f6532b = str;
    }

    public String getImsi() {
        return this.f6533c;
    }

    public void setImsi(String str) {
        this.f6533c = str;
    }

    public String getMac() {
        return this.f6534d;
    }

    public void setMac(String str) {
        this.f6534d = str;
    }
}
