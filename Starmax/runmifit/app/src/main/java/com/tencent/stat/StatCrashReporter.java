package com.tencent.stat;

import android.content.Context;
import java.util.ArrayList;
import java.util.List;

public class StatCrashReporter {

    /* renamed from: a */
    private static volatile StatCrashReporter f6979a;

    /* renamed from: b */
    private Context f6980b = null;

    /* renamed from: c */
    private List<StatCrashCallback> f6981c = null;

    /* renamed from: d */
    private boolean f6982d = true;

    /* renamed from: e */
    private int f6983e = 3;

    /* renamed from: f */
    private int f6984f = 3;

    public int getMaxNumOfRetries() {
        return this.f6984f;
    }

    public void setMaxNumOfRetries(int i) {
        if (i > 0 && i <= 100) {
            this.f6984f = i;
        }
    }

    public int getReportDelaySecOnStart() {
        return this.f6983e;
    }

    public void setReportDelaySecOnStart(int i) {
        if (i >= 0 && i <= 600) {
            this.f6983e = i;
        }
    }

    public boolean isEnableInstantReporting() {
        return this.f6982d;
    }

    public void setEnableInstantReporting(boolean z) {
        this.f6982d = z;
    }

    private StatCrashReporter(Context context) {
        this.f6980b = context.getApplicationContext();
        if (StatServiceImpl.getContext(null) == null) {
            StatServiceImpl.setContext(context);
        }
        this.f6981c = new ArrayList(1);
    }

    public static StatCrashReporter getStatCrashReporter(Context context) {
        if (f6979a == null) {
            synchronized (StatCrashReporter.class) {
                if (f6979a == null) {
                    f6979a = new StatCrashReporter(context);
                }
            }
        }
        return f6979a;
    }

    public void setJavaCrashHandlerStatus(boolean z) {
        StatConfig.setAutoExceptionCaught(z);
        if (z) {
            C3190a.m7914a(this.f6980b).mo35358a();
        }
    }

    public boolean getJavaCrashHandlerStatus() {
        return StatConfig.isAutoExceptionCaught();
    }

    public void setJniNativeCrashStatus(boolean z) {
        StatNativeCrashReport.setNativeCrashEnable(z);
        if (z) {
            StatNativeCrashReport.initNativeCrash(this.f6980b, null);
        }
    }

    public boolean getJniNativeCrashStatus() {
        return StatNativeCrashReport.isNativeCrashEnable();
    }

    public void setJniNativeCrashLogcatOutputStatus(boolean z) {
        StatNativeCrashReport.setNativeCrashDebugEnable(z);
    }

    public boolean isJniNativeCrashLogcatOutputEnable() {
        return StatNativeCrashReport.isNativeCrashDebugEnable();
    }

    public void addCrashCallback(StatCrashCallback statCrashCallback) {
        if (statCrashCallback != null && !this.f6981c.contains(statCrashCallback)) {
            this.f6981c.add(statCrashCallback);
        }
    }

    public void removeCrashCallback(StatCrashCallback statCrashCallback) {
        if (statCrashCallback != null) {
            this.f6981c.remove(statCrashCallback);
        }
    }

    public void clearCrashCallback() {
        this.f6981c.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35262a(Thread thread, Throwable th) {
        List<StatCrashCallback> list = this.f6981c;
        if (list != null && list.size() > 0) {
            for (StatCrashCallback statCrashCallback : this.f6981c) {
                statCrashCallback.onJavaCrash(thread, th);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo35261a(String str) {
        List<StatCrashCallback> list = this.f6981c;
        if (list != null && list.size() > 0) {
            for (StatCrashCallback statCrashCallback : this.f6981c) {
                statCrashCallback.onJniNativeCrash(str);
            }
        }
    }
}
