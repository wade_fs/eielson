package com.tencent.open.p059a;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/* renamed from: com.tencent.open.a.a */
/* compiled from: ProGuard */
public class C3073a extends C3085i implements Handler.Callback {

    /* renamed from: a */
    private C3074b f6703a;

    /* renamed from: b */
    private FileWriter f6704b;

    /* renamed from: c */
    private File f6705c;

    /* renamed from: d */
    private char[] f6706d;

    /* renamed from: e */
    private volatile C3083g f6707e;

    /* renamed from: f */
    private volatile C3083g f6708f;

    /* renamed from: g */
    private volatile C3083g f6709g;

    /* renamed from: h */
    private volatile C3083g f6710h;

    /* renamed from: i */
    private volatile boolean f6711i;

    /* renamed from: j */
    private HandlerThread f6712j;

    /* renamed from: k */
    private Handler f6713k;

    public C3073a(C3074b bVar) {
        this(C3075c.f6725b, true, C3084h.f6748a, bVar);
    }

    public C3073a(int i, boolean z, C3084h hVar, C3074b bVar) {
        super(i, z, hVar);
        this.f6711i = false;
        mo35077a(bVar);
        this.f6707e = new C3083g();
        this.f6708f = new C3083g();
        this.f6709g = this.f6707e;
        this.f6710h = this.f6708f;
        this.f6706d = new char[bVar.mo35093d()];
        m7588g();
        this.f6712j = new HandlerThread(bVar.mo35091c(), bVar.mo35096f());
        HandlerThread handlerThread = this.f6712j;
        if (handlerThread != null) {
            handlerThread.start();
        }
        if (this.f6712j.isAlive() && this.f6712j.getLooper() != null) {
            this.f6713k = new Handler(this.f6712j.getLooper(), this);
        }
    }

    /* renamed from: a */
    public void mo35075a() {
        if (this.f6713k.hasMessages(1024)) {
            this.f6713k.removeMessages(1024);
        }
        this.f6713k.sendEmptyMessage(1024);
    }

    /* renamed from: b */
    public void mo35079b() {
        m7589h();
        this.f6712j.quit();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35076a(int i, Thread thread, long j, String str, String str2, Throwable th) {
        mo35078a(mo35118e().mo35112a(i, thread, j, str, str2, th));
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo35078a(String str) {
        this.f6709g.mo35107a(str);
        if (this.f6709g.mo35106a() >= mo35080c().mo35093d()) {
            mo35075a();
        }
    }

    public boolean handleMessage(Message message) {
        if (message.what != 1024) {
            return true;
        }
        m7587f();
        return true;
    }

    /* renamed from: f */
    private void m7587f() {
        if (Thread.currentThread() == this.f6712j && !this.f6711i) {
            this.f6711i = true;
            m7590i();
            try {
                this.f6710h.mo35108a(m7588g(), this.f6706d);
            } catch (IOException unused) {
            } catch (Throwable th) {
                this.f6710h.mo35109b();
                throw th;
            }
            this.f6710h.mo35109b();
            this.f6711i = false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException}
     arg types: [java.io.File, int]
     candidates:
      ClspMth{java.io.FileWriter.<init>(java.lang.String, boolean):void throws java.io.IOException}
      ClspMth{java.io.FileWriter.<init>(java.io.File, boolean):void throws java.io.IOException} */
    /* renamed from: g */
    private Writer m7588g() {
        File a = mo35080c().mo35082a();
        if ((a != null && !a.equals(this.f6705c)) || (this.f6704b == null && a != null)) {
            this.f6705c = a;
            m7589h();
            try {
                this.f6704b = new FileWriter(this.f6705c, true);
            } catch (IOException unused) {
                return null;
            }
        }
        return this.f6704b;
    }

    /* renamed from: h */
    private void m7589h() {
        try {
            if (this.f6704b != null) {
                this.f6704b.flush();
                this.f6704b.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: i */
    private void m7590i() {
        synchronized (this) {
            if (this.f6709g == this.f6707e) {
                this.f6709g = this.f6708f;
                this.f6710h = this.f6707e;
            } else {
                this.f6709g = this.f6707e;
                this.f6710h = this.f6708f;
            }
        }
    }

    /* renamed from: c */
    public C3074b mo35080c() {
        return this.f6703a;
    }

    /* renamed from: a */
    public void mo35077a(C3074b bVar) {
        this.f6703a = bVar;
    }
}
