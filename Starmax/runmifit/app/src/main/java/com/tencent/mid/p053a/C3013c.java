package com.tencent.mid.p053a;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.LocalServerSocket;
import com.tencent.mid.api.C3024a;
import com.tencent.mid.api.MidCallback;
import com.tencent.mid.api.MidConstants;
import com.tencent.mid.api.MidEntity;
import com.tencent.mid.p054b.C3031g;
import com.tencent.mid.util.C3033a;
import com.tencent.mid.util.C3038d;
import com.tencent.mid.util.Util;
import java.io.IOException;

/* renamed from: com.tencent.mid.a.c */
public class C3013c {

    /* renamed from: c */
    private static C3038d f6508c = Util.getLogger();

    /* renamed from: i */
    private static C3013c f6509i = null;

    /* renamed from: j */
    private static Context f6510j = null;

    /* renamed from: a */
    int f6511a = -1;

    /* renamed from: b */
    LocalServerSocket f6512b = null;

    /* renamed from: d */
    private C3033a f6513d = null;

    /* renamed from: e */
    private C3033a f6514e = null;

    /* renamed from: f */
    private long f6515f = 0;

    /* renamed from: g */
    private int f6516g = 0;

    /* renamed from: h */
    private int f6517h = -1;

    /* renamed from: c */
    private void m7351c() {
        this.f6515f = 0;
        this.f6516g = 0;
    }

    /* renamed from: d */
    private void m7352d() {
        this.f6516g++;
        this.f6515f = System.currentTimeMillis();
    }

    /* renamed from: e */
    private boolean m7353e() {
        if (this.f6516g <= 3) {
            return false;
        }
        if (System.currentTimeMillis() - this.f6515f < 1800000) {
            return true;
        }
        m7351c();
        return false;
    }

    private C3013c(Context context) {
        try {
            f6510j = context.getApplicationContext();
        } catch (Throwable th) {
            f6508c.mo34954f(th);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public C3033a mo34840a(int i) {
        if (i == 1) {
            if (this.f6513d == null) {
                this.f6513d = new C3033a();
                this.f6513d.mo34936e();
            }
            return this.f6513d;
        }
        if (this.f6514e == null) {
            this.f6514e = new C3033a();
            this.f6514e.mo34931a("key-/.*$!xx", "vec-;*5@)&%(");
        }
        return this.f6514e;
    }

    /* renamed from: a */
    public static synchronized C3013c m7347a(Context context) {
        C3013c cVar;
        synchronized (C3013c.class) {
            if (f6509i == null) {
                f6509i = new C3013c(context);
            }
            cVar = f6509i;
        }
        return cVar;
    }

    /* renamed from: a */
    static Context m7346a() {
        return f6510j;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo34842b() {
        int i = this.f6511a;
        this.f6511a = i + 1;
        if (i > 1000) {
            f6508c.mo34954f("send count limit " + this.f6511a);
            return false;
        }
        SharedPreferences a = C3024a.m7384a(f6510j).mo34874a();
        if (a == null) {
            return true;
        }
        String str = "SEND_LIMIT_" + Util.getDateString(0);
        if (this.f6511a == 0) {
            this.f6511a = a.getInt(str, 0);
        }
        a.edit().putInt(str, this.f6511a);
        return true;
    }

    /* renamed from: f */
    private boolean m7354f() {
        try {
            this.f6512b = new LocalServerSocket("com.tencent.teg.mid.sock.lock");
            C3038d dVar = f6508c;
            dVar.mo34956h("open socket mLocalServerSocket:" + this.f6512b);
            return true;
        } catch (IOException unused) {
            C3038d dVar2 = f6508c;
            dVar2.mo34952d("socket Name:" + "com.tencent.teg.mid.sock.lock" + " is in use.");
            return false;
        } catch (Throwable unused2) {
            f6508c.mo34952d("something wrong while create LocalServerSocket.");
            return false;
        }
    }

    /* renamed from: g */
    private void m7355g() {
        LocalServerSocket localServerSocket = this.f6512b;
        if (localServerSocket != null) {
            try {
                localServerSocket.close();
                C3038d dVar = f6508c;
                dVar.mo34950b("close socket  mLocalServerSocket:" + this.f6512b);
                this.f6512b = null;
            } catch (Throwable unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo34841a(int i, C3015e eVar, MidCallback midCallback) {
        if (eVar == null || midCallback == null) {
            if (midCallback != null) {
                midCallback.onFail(MidConstants.ERROR_ARGUMENT, "packet == null || handler == null");
            }
            f6508c.mo34954f("packet == null || handler == null || cb == null");
        } else if (!Util.isNetworkAvailable(f6510j)) {
            midCallback.onFail(MidConstants.ERROR_NETWORK, "network not available.");
        } else {
            int i2 = 0;
            while (!m7354f()) {
                int i3 = i2 + 1;
                if (i2 >= 10) {
                    break;
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException unused) {
                }
                i2 = i3;
            }
            if (i == 1) {
                MidEntity a = C3017g.m7367a(f6510j);
                if (Util.isMidValid(a)) {
                    midCallback.onSuccess(a);
                    m7355g();
                    return;
                }
            }
            if (i == 3) {
                MidEntity a2 = C3031g.m7437a(f6510j).mo34904a();
                if (Util.isMidValid(a2)) {
                    midCallback.onSuccess(a2);
                    m7355g();
                    return;
                }
            }
            if (!mo34842b()) {
                m7355g();
                return;
            }
            m7350b(i, eVar, midCallback);
            m7355g();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0165, code lost:
        if (r12.has("ret_msg") != false) goto L_0x0167;
     */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x0203 A[SYNTHETIC, Splitter:B:62:0x0203] */
    /* JADX WARNING: Removed duplicated region for block: B:80:? A[RETURN, SYNTHETIC] */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m7350b(int r12, com.tencent.mid.p053a.C3015e r13, com.tencent.mid.api.MidCallback r14) {
        /*
            r11 = this;
            java.lang.String r0 = "mid"
            java.lang.String r1 = "ret_code"
            com.tencent.mid.util.d r2 = com.tencent.mid.p053a.C3013c.f6508c
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = " enter http request, type:"
            r3.append(r4)
            r3.append(r12)
            java.lang.String r3 = r3.toString()
            r2.mo34950b(r3)
            r2 = 0
            boolean r3 = r11.m7353e()     // Catch:{ all -> 0x01e4 }
            if (r3 == 0) goto L_0x0030
            java.lang.String r12 = "Http request failed too much, please check the network."
            com.tencent.mid.util.d r13 = com.tencent.mid.p053a.C3013c.f6508c     // Catch:{ all -> 0x01e4 }
            r13.mo34954f(r12)     // Catch:{ all -> 0x01e4 }
            if (r14 == 0) goto L_0x002f
            r13 = -10050(0xffffffffffffd8be, float:NaN)
            r14.onFail(r13, r12)     // Catch:{ all -> 0x01e4 }
        L_0x002f:
            return
        L_0x0030:
            android.content.Context r3 = com.tencent.mid.p053a.C3013c.f6510j     // Catch:{ all -> 0x01e4 }
            com.tencent.mid.util.b r3 = com.tencent.mid.util.C3034b.m7478a(r3)     // Catch:{ all -> 0x01e4 }
            com.tencent.mid.a.b r4 = new com.tencent.mid.a.b     // Catch:{ all -> 0x01e4 }
            android.content.Context r5 = com.tencent.mid.p053a.C3013c.f6510j     // Catch:{ all -> 0x01e4 }
            java.lang.String r5 = com.tencent.mid.util.Util.getHttpAddr(r5)     // Catch:{ all -> 0x01e4 }
            r4.<init>(r5, r2)     // Catch:{ all -> 0x01e4 }
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ all -> 0x01e2 }
            r2.<init>()     // Catch:{ all -> 0x01e2 }
            r13.mo34846a(r2)     // Catch:{ all -> 0x01e2 }
            java.lang.String r13 = "rty"
            r2.put(r13, r12)     // Catch:{ all -> 0x01e2 }
            int r13 = r11.f6517h     // Catch:{ all -> 0x01e2 }
            java.lang.String r5 = "seq"
            if (r13 <= 0) goto L_0x0059
            int r13 = r11.f6517h     // Catch:{ all -> 0x01e2 }
            r2.put(r5, r13)     // Catch:{ all -> 0x01e2 }
        L_0x0059:
            org.json.JSONObject r13 = new org.json.JSONObject     // Catch:{ all -> 0x01e2 }
            r13.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r6 = "android"
            r13.put(r6, r2)     // Catch:{ all -> 0x01e2 }
            java.lang.String r2 = "mid_list"
            android.content.Context r6 = com.tencent.mid.p053a.C3013c.f6510j     // Catch:{ all -> 0x01e2 }
            r7 = 1
            org.json.JSONArray r6 = com.tencent.mid.util.Util.queryMids(r6, r7)     // Catch:{ all -> 0x01e2 }
            r13.put(r2, r6)     // Catch:{ all -> 0x01e2 }
            java.lang.String r2 = "mid_list_new"
            android.content.Context r6 = com.tencent.mid.p053a.C3013c.f6510j     // Catch:{ all -> 0x01e2 }
            r8 = 2
            org.json.JSONArray r6 = com.tencent.mid.util.Util.queryMids(r6, r8)     // Catch:{ all -> 0x01e2 }
            r13.put(r2, r6)     // Catch:{ all -> 0x01e2 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01e2 }
            com.tencent.mid.util.d r2 = com.tencent.mid.p053a.C3013c.f6508c     // Catch:{ all -> 0x01e2 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e2 }
            r6.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r8 = "jsonBodyStr:"
            r6.append(r8)     // Catch:{ all -> 0x01e2 }
            r6.append(r13)     // Catch:{ all -> 0x01e2 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x01e2 }
            r2.mo34950b(r6)     // Catch:{ all -> 0x01e2 }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x01e2 }
            int r6 = r13.length()     // Catch:{ all -> 0x01e2 }
            r2.<init>(r6)     // Catch:{ all -> 0x01e2 }
            java.util.zip.GZIPOutputStream r6 = new java.util.zip.GZIPOutputStream     // Catch:{ all -> 0x01e2 }
            r6.<init>(r2)     // Catch:{ all -> 0x01e2 }
            java.lang.String r8 = "UTF-8"
            byte[] r13 = r13.getBytes(r8)     // Catch:{ all -> 0x01e2 }
            r6.write(r13)     // Catch:{ all -> 0x01e2 }
            r6.close()     // Catch:{ all -> 0x01e2 }
            r2.flush()     // Catch:{ all -> 0x01e2 }
            byte[] r13 = r2.toByteArray()     // Catch:{ all -> 0x01e2 }
            com.tencent.mid.util.a r6 = r11.mo34840a(r12)     // Catch:{ all -> 0x01e2 }
            r2.reset()     // Catch:{ all -> 0x01e2 }
            java.io.DataOutputStream r8 = new java.io.DataOutputStream     // Catch:{ all -> 0x01e2 }
            r8.<init>(r2)     // Catch:{ all -> 0x01e2 }
            java.lang.String r9 = r3.mo34942f()     // Catch:{ all -> 0x01e2 }
            if (r12 == r7) goto L_0x00cb
            r10 = 3
            if (r12 != r10) goto L_0x010c
        L_0x00cb:
            if (r12 != r7) goto L_0x00d2
            java.lang.String r7 = r3.mo34940d()     // Catch:{ all -> 0x01e2 }
            goto L_0x00d6
        L_0x00d2:
            java.lang.String r7 = r3.mo34941e()     // Catch:{ all -> 0x01e2 }
        L_0x00d6:
            r9 = r7
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x01e2 }
            r10 = 64
            r7.<init>(r10)     // Catch:{ all -> 0x01e2 }
            byte[] r10 = r6.mo34933b()     // Catch:{ all -> 0x01e2 }
            r7.write(r10)     // Catch:{ all -> 0x01e2 }
            byte[] r10 = r6.mo34935c()     // Catch:{ all -> 0x01e2 }
            r7.write(r10)     // Catch:{ all -> 0x01e2 }
            r7.close()     // Catch:{ all -> 0x01e2 }
            byte[] r7 = r7.toByteArray()     // Catch:{ all -> 0x01e2 }
            java.lang.String r10 = r3.mo34938b()     // Catch:{ all -> 0x01e2 }
            com.tencent.mid.util.C3040f.m7505a(r10)     // Catch:{ all -> 0x01e2 }
            byte[] r7 = com.tencent.mid.util.C3040f.m7506a(r7)     // Catch:{ all -> 0x01e2 }
            int r3 = r3.mo34937a()     // Catch:{ all -> 0x01e2 }
            r8.writeShort(r3)     // Catch:{ all -> 0x01e2 }
            int r3 = r7.length     // Catch:{ all -> 0x01e2 }
            r8.writeShort(r3)     // Catch:{ all -> 0x01e2 }
            r8.write(r7)     // Catch:{ all -> 0x01e2 }
        L_0x010c:
            byte[] r13 = r6.mo34932a(r13)     // Catch:{ all -> 0x01e2 }
            r8.write(r13)     // Catch:{ all -> 0x01e2 }
            r8.close()     // Catch:{ all -> 0x01e2 }
            r2.close()     // Catch:{ all -> 0x01e2 }
            byte[] r13 = r2.toByteArray()     // Catch:{ all -> 0x01e2 }
            java.lang.String r2 = "gzip"
            com.tencent.mid.a.d r12 = r4.mo34836a(r9, r13, r2, r12)     // Catch:{ all -> 0x01e2 }
            int r13 = r12.mo34843a()     // Catch:{ all -> 0x01e2 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r13 == r2) goto L_0x0155
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e2 }
            r13.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = "response code invalid:"
            r13.append(r0)     // Catch:{ all -> 0x01e2 }
            int r0 = r12.mo34843a()     // Catch:{ all -> 0x01e2 }
            r13.append(r0)     // Catch:{ all -> 0x01e2 }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x01e2 }
            com.tencent.mid.util.d r0 = com.tencent.mid.p053a.C3013c.f6508c     // Catch:{ all -> 0x01e2 }
            r0.mo34952d(r13)     // Catch:{ all -> 0x01e2 }
            int r12 = r12.mo34843a()     // Catch:{ all -> 0x01e2 }
            r14.onFail(r12, r13)     // Catch:{ all -> 0x01e2 }
            r4.mo34838a()     // Catch:{ all -> 0x0150 }
            goto L_0x0154
        L_0x0150:
            r12 = move-exception
            r12.printStackTrace()
        L_0x0154:
            return
        L_0x0155:
            org.json.JSONObject r12 = r12.mo34844b()     // Catch:{ all -> 0x01e2 }
            boolean r13 = r12.has(r1)     // Catch:{ all -> 0x01e2 }
            java.lang.String r2 = "ret_msg"
            if (r13 != 0) goto L_0x0167
            boolean r13 = r12.has(r2)     // Catch:{ all -> 0x01e2 }
            if (r13 == 0) goto L_0x019b
        L_0x0167:
            int r13 = r12.getInt(r1)     // Catch:{ all -> 0x01e2 }
            java.lang.String r1 = r12.getString(r2)     // Catch:{ all -> 0x01e2 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01e2 }
            r2.<init>()     // Catch:{ all -> 0x01e2 }
            java.lang.String r3 = "response code:"
            r2.append(r3)     // Catch:{ all -> 0x01e2 }
            r2.append(r13)     // Catch:{ all -> 0x01e2 }
            java.lang.String r3 = ",msg:"
            r2.append(r3)     // Catch:{ all -> 0x01e2 }
            r2.append(r1)     // Catch:{ all -> 0x01e2 }
            java.lang.String r1 = r2.toString()     // Catch:{ all -> 0x01e2 }
            com.tencent.mid.util.d r2 = com.tencent.mid.p053a.C3013c.f6508c     // Catch:{ all -> 0x01e2 }
            r2.mo34952d(r1)     // Catch:{ all -> 0x01e2 }
            if (r13 == 0) goto L_0x019b
            r14.onFail(r13, r1)     // Catch:{ all -> 0x01e2 }
            r4.mo34838a()     // Catch:{ all -> 0x0196 }
            goto L_0x019a
        L_0x0196:
            r12 = move-exception
            r12.printStackTrace()
        L_0x019a:
            return
        L_0x019b:
            boolean r13 = r12.isNull(r5)     // Catch:{ all -> 0x01e2 }
            if (r13 != 0) goto L_0x01a7
            int r13 = r12.getInt(r5)     // Catch:{ all -> 0x01e2 }
            r11.f6517h = r13     // Catch:{ all -> 0x01e2 }
        L_0x01a7:
            boolean r13 = r12.isNull(r0)     // Catch:{ all -> 0x01e2 }
            r1 = 0
            if (r13 != 0) goto L_0x01bb
            java.lang.String r13 = r12.getString(r0)     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = "reset"
            int r0 = r12.optInt(r0, r1)     // Catch:{ all -> 0x01e2 }
            m7349a(r13, r0, r14)     // Catch:{ all -> 0x01e2 }
        L_0x01bb:
            java.lang.String r13 = "locW"
            r0 = -1
            int r13 = r12.optInt(r13, r0)     // Catch:{ all -> 0x01e2 }
            if (r13 <= r0) goto L_0x01cf
            android.content.Context r0 = com.tencent.mid.p053a.C3013c.f6510j     // Catch:{ all -> 0x01e2 }
            com.tencent.mid.util.g r0 = com.tencent.mid.util.C3041g.m7509a(r0)     // Catch:{ all -> 0x01e2 }
            java.lang.String r2 = "ten.mid.allowCheckAndRewriteLocal.bool"
            r0.mo34958a(r2, r13)     // Catch:{ all -> 0x01e2 }
        L_0x01cf:
            java.lang.String r13 = "mid_new"
            java.lang.String r13 = r12.optString(r13)     // Catch:{ all -> 0x01e2 }
            java.lang.String r0 = "reset_new"
            int r12 = r12.optInt(r0, r1)     // Catch:{ all -> 0x01e2 }
            m7348a(r13, r12)     // Catch:{ all -> 0x01e2 }
            r4.mo34838a()     // Catch:{ all -> 0x0207 }
            goto L_0x020b
        L_0x01e2:
            r12 = move-exception
            goto L_0x01e6
        L_0x01e4:
            r12 = move-exception
            r4 = r2
        L_0x01e6:
            r11.m7352d()     // Catch:{ all -> 0x020c }
            r12.printStackTrace()     // Catch:{ all -> 0x020c }
            r13 = -10030(0xffffffffffffd8d2, float:NaN)
            java.lang.String r0 = r12.toString()     // Catch:{ all -> 0x020c }
            r14.onFail(r13, r0)     // Catch:{ all -> 0x020c }
            com.tencent.mid.util.d r13 = com.tencent.mid.p053a.C3013c.f6508c     // Catch:{ all -> 0x020c }
            java.lang.String r14 = r12.toString()     // Catch:{ all -> 0x020c }
            r13.mo34952d(r14)     // Catch:{ all -> 0x020c }
            r12.printStackTrace()     // Catch:{ all -> 0x020c }
            if (r4 == 0) goto L_0x020b
            r4.mo34838a()     // Catch:{ all -> 0x0207 }
            goto L_0x020b
        L_0x0207:
            r12 = move-exception
            r12.printStackTrace()
        L_0x020b:
            return
        L_0x020c:
            r12 = move-exception
            if (r4 == 0) goto L_0x0217
            r4.mo34838a()     // Catch:{ all -> 0x0213 }
            goto L_0x0217
        L_0x0213:
            r13 = move-exception
            r13.printStackTrace()
        L_0x0217:
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.mid.p053a.C3013c.m7350b(int, com.tencent.mid.a.e, com.tencent.mid.api.MidCallback):void");
    }

    /* renamed from: a */
    private static void m7348a(String str, int i) {
        if (Util.isMidValid(str)) {
            if (!Util.isMidValid(C3031g.m7437a(f6510j).mo34912b())) {
                i = 3;
            }
            C3038d dVar = f6508c;
            dVar.mo34950b("updateNewVersionMidEntity reset:" + i);
            if (i > 0) {
                MidEntity midEntity = new MidEntity();
                midEntity.setMid(str);
                midEntity.setMac(Util.getWifiMacAddress(f6510j));
                midEntity.setImei(Util.getImei(f6510j));
                midEntity.setImsi(Util.getImsi(f6510j));
                midEntity.setTimestamps(System.currentTimeMillis());
                midEntity.setVersion(3);
                C3038d dVar2 = f6508c;
                dVar2.mo34950b("server return new version mid midEntity:" + midEntity.toString());
                if (i == 1) {
                    C3031g.m7437a(f6510j).mo34913b(midEntity);
                } else if (i == 2) {
                    C3031g.m7437a(f6510j).mo34916c(midEntity);
                } else if (i == 3) {
                    C3031g.m7437a(f6510j).mo34908a(midEntity);
                } else if (i == 4) {
                    C3031g.m7437a(f6510j).mo34922f(midEntity);
                    C3031g.m7437a(f6510j).mo34908a(midEntity);
                } else if (i == 8) {
                    C3031g.m7437a(f6510j).mo34922f(midEntity);
                    C3031g.m7437a(f6510j).mo34908a(midEntity);
                    C3031g.m7437a(f6510j).mo34924g(midEntity);
                }
                C3031g.m7437a(f6510j).mo34907a(-1, -1);
            }
        }
    }

    /* renamed from: a */
    private static void m7349a(String str, int i, MidCallback midCallback) {
        if (Util.isMidValid(str)) {
            if (!Util.isMidValid(C3017g.m7375c(f6510j))) {
                i = 4;
            }
            C3038d dVar = f6508c;
            dVar.mo34950b("updateMidEntity reset:" + i);
            if (i > 0) {
                MidEntity midEntity = new MidEntity();
                midEntity.setMid(str);
                midEntity.setMac(Util.getWifiMacAddress(f6510j));
                midEntity.setImei(Util.getImei(f6510j));
                midEntity.setImsi(Util.getImsi(f6510j));
                midEntity.setTimestamps(System.currentTimeMillis());
                midEntity.setVersion(3);
                C3038d dVar2 = f6508c;
                dVar2.mo34950b("server return new mid midEntity:" + midEntity.toString());
                midCallback.onSuccess(midEntity.toString());
                if (i == 1) {
                    C3031g.m7437a(f6510j).mo34918d(midEntity);
                } else if (i == 2) {
                    C3031g.m7437a(f6510j).mo34920e(midEntity);
                } else if (i == 3) {
                    C3031g.m7437a(f6510j).mo34922f(midEntity);
                } else if (i == 4) {
                    C3031g.m7437a(f6510j).mo34922f(midEntity);
                    C3031g.m7437a(f6510j).mo34924g(midEntity);
                }
                C3031g.m7437a(f6510j).mo34907a(-1, -1);
            }
        }
    }
}
