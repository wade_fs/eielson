package com.tencent.stat.event;

import android.content.Context;
import com.baidu.mobstat.Config;
import com.tencent.stat.NetworkManager;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.i */
public class C3245i extends Event {

    /* renamed from: a */
    private static String f7380a;

    /* renamed from: s */
    private String f7381s = null;

    /* renamed from: t */
    private String f7382t = null;

    /* renamed from: a */
    public void mo35472a(String str) {
        this.f7382t = str;
    }

    public C3245i(Context context, int i, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7381s = NetworkManager.getInstance(context).getCurNetwrokName();
        if (f7380a == null) {
            f7380a = StatCommonHelper.getSimOperator(context);
        }
    }

    public EventType getType() {
        return EventType.NETWORK_MONITOR;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        Util.jsonPut(jSONObject, Config.OPERATOR, f7380a);
        Util.jsonPut(jSONObject, "cn", this.f7381s);
        jSONObject.put("sp", this.f7382t);
        return true;
    }
}
