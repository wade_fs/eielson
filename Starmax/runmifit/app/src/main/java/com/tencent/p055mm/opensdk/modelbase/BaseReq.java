package com.tencent.p055mm.opensdk.modelbase;

import android.os.Bundle;
import com.tencent.p055mm.opensdk.p056a.C3047a;

/* renamed from: com.tencent.mm.opensdk.modelbase.BaseReq */
public abstract class BaseReq {
    public String openId;
    public String transaction;

    public abstract boolean checkArgs();

    public void fromBundle(Bundle bundle) {
        this.transaction = C3047a.m7539b(bundle, "_wxapi_basereq_transaction");
        this.openId = C3047a.m7539b(bundle, "_wxapi_basereq_openid");
    }

    public abstract int getType();

    public void toBundle(Bundle bundle) {
        bundle.putInt("_wxapi_command_type", getType());
        bundle.putString("_wxapi_basereq_transaction", this.transaction);
        bundle.putString("_wxapi_basereq_openid", this.openId);
    }
}
