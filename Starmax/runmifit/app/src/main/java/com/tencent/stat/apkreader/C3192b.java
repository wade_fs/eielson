package com.tencent.stat.apkreader;

/* renamed from: com.tencent.stat.apkreader.b */
final class C3192b<A, B> {

    /* renamed from: a */
    private final A f7166a;

    /* renamed from: b */
    private final B f7167b;

    private C3192b(A a, B b) {
        this.f7166a = a;
        this.f7167b = b;
    }

    /* renamed from: a */
    public static <A, B> C3192b<A, B> m7925a(A a, B b) {
        return new C3192b<>(a, b);
    }

    /* renamed from: a */
    public A mo35362a() {
        return this.f7166a;
    }

    public int hashCode() {
        A a = this.f7166a;
        int i = 0;
        int hashCode = ((a == null ? 0 : a.hashCode()) + 31) * 31;
        B b = this.f7167b;
        if (b != null) {
            i = b.hashCode();
        }
        return hashCode + i;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        C3192b bVar = (C3192b) obj;
        A a = this.f7166a;
        if (a == null) {
            if (bVar.f7166a != null) {
                return false;
            }
        } else if (!a.equals(bVar.f7166a)) {
            return false;
        }
        B b = this.f7167b;
        if (b == null) {
            if (bVar.f7167b != null) {
                return false;
            }
        } else if (!b.equals(bVar.f7167b)) {
            return false;
        }
        return true;
    }
}
