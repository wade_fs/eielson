package com.tencent.bugly.beta.utils;

import android.util.Log;
import com.google.common.primitives.UnsignedBytes;
import java.io.IOException;
import java.util.HashMap;

/* renamed from: com.tencent.bugly.beta.utils.b */
/* compiled from: BUGLY */
public class C2831b {

    /* renamed from: a */
    private String f5494a = null;

    /* renamed from: b */
    private C2830a f5495b = null;

    /* renamed from: c */
    private long f5496c = 0;

    /* renamed from: d */
    private long f5497d = 0;

    /* renamed from: e */
    private String f5498e = null;

    /* renamed from: f */
    private final HashMap<Long, String> f5499f = new HashMap<>();

    public C2831b(String str, long j, long j2) {
        this.f5499f.put(1L, "armeabi-v4");
        this.f5499f.put(2L, "armeabi-v4t");
        this.f5499f.put(3L, "armeabi-v5t");
        this.f5499f.put(4L, "armeabi-v5te");
        this.f5499f.put(5L, "armeabi-v5tej");
        this.f5499f.put(6L, "armeabi-v6");
        this.f5499f.put(7L, "armeabi-v6kz");
        this.f5499f.put(8L, "armeabi-v6t2");
        this.f5499f.put(9L, "armeabi-v6k");
        this.f5499f.put(10L, "armeabi-v7a");
        this.f5499f.put(11L, "armeabi-v6-m");
        this.f5499f.put(12L, "armeabi-v6s-m");
        this.f5499f.put(13L, "armeabi-v7e-m");
        this.f5499f.put(14L, "armeabi-v8a");
        this.f5494a = str;
        this.f5496c = j;
        this.f5497d = j2;
    }

    /* renamed from: a */
    private boolean m6344a() {
        return 0 != this.f5497d;
    }

    /* renamed from: b */
    private synchronized void m6346b() {
        if (this.f5495b != null) {
            if (this.f5495b.mo34193a()) {
                this.f5495b = null;
            }
        }
    }

    /* renamed from: c */
    private synchronized boolean m6347c() {
        if (!m6344a()) {
            return false;
        }
        if (this.f5495b != null) {
            m6346b();
        }
        try {
            this.f5495b = new C2830a(this.f5494a, this.f5496c);
            if (!this.f5495b.mo34196b(this.f5497d)) {
                return false;
            }
            return true;
        } catch (Exception e) {
            Log.e("ElfArmAttrParser", e.getMessage());
            return false;
        }
    }

    /* renamed from: d */
    private synchronized String m6348d() {
        StringBuilder sb;
        sb = new StringBuilder();
        while (true) {
            try {
                char b = (char) this.f5495b.mo34195b();
                if (b != 0) {
                    sb.append(b);
                }
            } catch (IOException e) {
                Log.e("ElfArmAttrParser", e.getMessage());
                return null;
            }
        }
        return sb.toString();
    }

    /* renamed from: e */
    private String m6349e() {
        return m6348d();
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x004d A[SYNTHETIC, Splitter:B:26:0x004d] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean m6345a(long r5) {
        /*
            r4 = this;
            monitor-enter(r4)
        L_0x0001:
            r0 = 0
            r2 = 0
            int r3 = (r5 > r0 ? 1 : (r5 == r0 ? 0 : -1))
            if (r3 <= 0) goto L_0x0070
            com.tencent.bugly.beta.utils.a r0 = r4.f5495b     // Catch:{ IOException -> 0x0062 }
            long r0 = m6342a(r0)     // Catch:{ IOException -> 0x0062 }
            int r1 = (int) r0     // Catch:{ IOException -> 0x0062 }
            r0 = 34
            if (r1 == r0) goto L_0x005a
            r0 = 36
            if (r1 == r0) goto L_0x005a
            r0 = 38
            if (r1 == r0) goto L_0x005a
            r0 = 42
            if (r1 == r0) goto L_0x005a
            r0 = 44
            if (r1 == r0) goto L_0x005a
            r0 = 70
            if (r1 == r0) goto L_0x005a
            switch(r1) {
                case 4: goto L_0x004d;
                case 5: goto L_0x004d;
                case 6: goto L_0x0036;
                case 7: goto L_0x005a;
                case 8: goto L_0x005a;
                case 9: goto L_0x005a;
                case 10: goto L_0x005a;
                case 11: goto L_0x005a;
                case 12: goto L_0x005a;
                case 13: goto L_0x005a;
                case 14: goto L_0x005a;
                case 15: goto L_0x005a;
                case 16: goto L_0x005a;
                case 17: goto L_0x005a;
                case 18: goto L_0x005a;
                case 19: goto L_0x005a;
                case 20: goto L_0x005a;
                case 21: goto L_0x005a;
                case 22: goto L_0x005a;
                case 23: goto L_0x005a;
                case 24: goto L_0x005a;
                case 25: goto L_0x005a;
                case 26: goto L_0x005a;
                case 27: goto L_0x005a;
                case 28: goto L_0x005a;
                case 29: goto L_0x005a;
                case 30: goto L_0x005a;
                case 31: goto L_0x005a;
                case 32: goto L_0x004d;
                default: goto L_0x002a;
            }     // Catch:{ IOException -> 0x0062 }
        L_0x002a:
            switch(r1) {
                case 64: goto L_0x005a;
                case 65: goto L_0x004d;
                case 66: goto L_0x005a;
                case 67: goto L_0x004d;
                case 68: goto L_0x005a;
                default: goto L_0x002d;
            }     // Catch:{ IOException -> 0x0062 }
        L_0x002d:
            java.lang.String r5 = "ElfArmAttrParser"
            java.lang.String r6 = "Unimplemented tag."
            android.util.Log.e(r5, r6)     // Catch:{ IOException -> 0x0062 }
            monitor-exit(r4)
            return r2
        L_0x0036:
            com.tencent.bugly.beta.utils.a r5 = r4.f5495b     // Catch:{ IOException -> 0x0062 }
            long r5 = m6342a(r5)     // Catch:{ IOException -> 0x0062 }
            java.util.HashMap<java.lang.Long, java.lang.String> r0 = r4.f5499f     // Catch:{ IOException -> 0x0062 }
            java.lang.Long r5 = java.lang.Long.valueOf(r5)     // Catch:{ IOException -> 0x0062 }
            java.lang.Object r5 = r0.get(r5)     // Catch:{ IOException -> 0x0062 }
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ IOException -> 0x0062 }
            r4.f5498e = r5     // Catch:{ IOException -> 0x0062 }
            r5 = 1
            monitor-exit(r4)
            return r5
        L_0x004d:
            java.lang.String r0 = r4.m6348d()     // Catch:{ IOException -> 0x0062 }
            if (r0 == 0) goto L_0x0001
            int r0 = r0.length()     // Catch:{ IOException -> 0x0062 }
            long r0 = (long) r0     // Catch:{ IOException -> 0x0062 }
            long r5 = r5 - r0
            goto L_0x0001
        L_0x005a:
            com.tencent.bugly.beta.utils.a r0 = r4.f5495b     // Catch:{ IOException -> 0x0062 }
            m6342a(r0)     // Catch:{ IOException -> 0x0062 }
            goto L_0x0001
        L_0x0060:
            r5 = move-exception
            goto L_0x006e
        L_0x0062:
            r5 = move-exception
            java.lang.String r6 = "ElfArmAttrParser"
            java.lang.String r5 = r5.getMessage()     // Catch:{ all -> 0x0060 }
            android.util.Log.e(r6, r5)     // Catch:{ all -> 0x0060 }
            monitor-exit(r4)
            return r2
        L_0x006e:
            monitor-exit(r4)
            throw r5
        L_0x0070:
            monitor-exit(r4)
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.utils.C2831b.m6345a(long):boolean");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        return false;
     */
    /* renamed from: f */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean m6350f() {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 0
            com.tencent.bugly.beta.utils.a r1 = r10.f5495b     // Catch:{ IOException -> 0x0059 }
            long r1 = r1.mo34200f()     // Catch:{ IOException -> 0x0059 }
            r3 = 65
            int r5 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r5 == 0) goto L_0x0010
            monitor-exit(r10)
            return r0
        L_0x0010:
            com.tencent.bugly.beta.utils.a r1 = r10.f5495b     // Catch:{ IOException -> 0x0059 }
            long r1 = r1.mo34202h()     // Catch:{ IOException -> 0x0059 }
            java.lang.String r3 = r10.m6349e()     // Catch:{ IOException -> 0x0059 }
            if (r3 == 0) goto L_0x0055
            java.lang.String r4 = "aeabi"
            boolean r4 = r3.equals(r4)     // Catch:{ IOException -> 0x0059 }
            if (r4 != 0) goto L_0x0025
            goto L_0x0055
        L_0x0025:
            int r3 = r3.length()     // Catch:{ IOException -> 0x0059 }
            long r3 = (long) r3     // Catch:{ IOException -> 0x0059 }
            long r1 = r1 - r3
        L_0x002b:
            r3 = 0
            int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
            if (r5 <= 0) goto L_0x0052
            com.tencent.bugly.beta.utils.a r3 = r10.f5495b     // Catch:{ IOException -> 0x0059 }
            long r3 = r3.mo34200f()     // Catch:{ IOException -> 0x0059 }
            com.tencent.bugly.beta.utils.a r5 = r10.f5495b     // Catch:{ IOException -> 0x0059 }
            long r5 = r5.mo34202h()     // Catch:{ IOException -> 0x0059 }
            r7 = 5
            long r5 = r5 - r7
            r7 = 1
            int r9 = (r7 > r3 ? 1 : (r7 == r3 ? 0 : -1))
            if (r9 != 0) goto L_0x004c
            boolean r0 = r10.m6345a(r5)     // Catch:{ IOException -> 0x0059 }
            monitor-exit(r10)
            return r0
        L_0x004c:
            com.tencent.bugly.beta.utils.a r3 = r10.f5495b     // Catch:{ IOException -> 0x0059 }
            r3.mo34196b(r5)     // Catch:{ IOException -> 0x0059 }
            goto L_0x002b
        L_0x0052:
            r0 = 1
            monitor-exit(r10)
            return r0
        L_0x0055:
            monitor-exit(r10)
            return r0
        L_0x0057:
            r0 = move-exception
            goto L_0x0065
        L_0x0059:
            r1 = move-exception
            java.lang.String r2 = "ElfArmAttrParser"
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0057 }
            android.util.Log.e(r2, r1)     // Catch:{ all -> 0x0057 }
            monitor-exit(r10)
            return r0
        L_0x0065:
            monitor-exit(r10)
            goto L_0x0068
        L_0x0067:
            throw r0
        L_0x0068:
            goto L_0x0067
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.utils.C2831b.m6350f():boolean");
    }

    /* renamed from: g */
    private boolean m6351g() {
        if (!m6347c()) {
            m6346b();
            return false;
        }
        if (!m6350f()) {
            Log.e("ElfArmAttrParser", "Failed to parse elf header");
        }
        m6346b();
        return true;
    }

    /* renamed from: a */
    public static String m6343a(String str, long j, long j2) {
        C2831b bVar = new C2831b(str, j, j2);
        if (bVar.m6351g()) {
            return bVar.f5498e;
        }
        Log.e("ElfArmAttrParser", "Failed to parse the arch.");
        return null;
    }

    /* renamed from: a */
    public static synchronized long m6342a(C2830a aVar) throws IOException {
        long j;
        byte b;
        synchronized (C2831b.class) {
            j = 0;
            long j2 = 0;
            do {
                b = aVar.mo34195b();
                j |= (((long) b) & 127) << ((int) j2);
                j2 += 7;
            } while ((b & UnsignedBytes.MAX_POWER_OF_TWO) != 0);
        }
        return j;
    }
}
