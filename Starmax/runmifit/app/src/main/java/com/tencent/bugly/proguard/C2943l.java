package com.tencent.bugly.proguard;

import com.google.common.base.Ascii;
import com.runmifit.android.util.ChangeCharset;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.l */
/* compiled from: BUGLY */
public class C2943l {

    /* renamed from: a */
    protected String f6215a;

    /* renamed from: b */
    private ByteBuffer f6216b;

    public C2943l(int i) {
        this.f6215a = ChangeCharset.GBK;
        this.f6216b = ByteBuffer.allocate(i);
    }

    public C2943l() {
        this(128);
    }

    /* renamed from: a */
    public ByteBuffer mo34639a() {
        return this.f6216b;
    }

    /* renamed from: b */
    public byte[] mo34661b() {
        byte[] bArr = new byte[this.f6216b.position()];
        System.arraycopy(this.f6216b.array(), 0, bArr, 0, this.f6216b.position());
        return bArr;
    }

    /* renamed from: a */
    public void mo34643a(int i) {
        if (this.f6216b.remaining() < i) {
            ByteBuffer allocate = ByteBuffer.allocate((this.f6216b.capacity() + i) * 2);
            allocate.put(this.f6216b.array(), 0, this.f6216b.position());
            this.f6216b = allocate;
        }
    }

    /* renamed from: a */
    public void mo34640a(byte b, int i) {
        if (i < 15) {
            this.f6216b.put((byte) (b | (i << 4)));
        } else if (i < 256) {
            this.f6216b.put((byte) (b | 240));
            this.f6216b.put((byte) i);
        } else {
            throw new C2940j("tag is too large: " + i);
        }
    }

    /* renamed from: a */
    public void mo34652a(boolean z, int i) {
        mo34660b(z ? (byte) 1 : 0, i);
    }

    /* renamed from: b */
    public void mo34660b(byte b, int i) {
        mo34643a(3);
        if (b == 0) {
            mo34640a((byte) Ascii.f4505FF, i);
            return;
        }
        mo34640a((byte) 0, i);
        this.f6216b.put(b);
    }

    /* renamed from: a */
    public void mo34651a(short s, int i) {
        mo34643a(4);
        if (s < -128 || s > 127) {
            mo34640a((byte) 1, i);
            this.f6216b.putShort(s);
            return;
        }
        mo34660b((byte) s, i);
    }

    /* renamed from: a */
    public void mo34644a(int i, int i2) {
        mo34643a(6);
        if (i < -32768 || i > 32767) {
            mo34640a((byte) 2, i2);
            this.f6216b.putInt(i);
            return;
        }
        mo34651a((short) i, i2);
    }

    /* renamed from: a */
    public void mo34645a(long j, int i) {
        mo34643a(10);
        if (j < -2147483648L || j > 2147483647L) {
            mo34640a((byte) 3, i);
            this.f6216b.putLong(j);
            return;
        }
        mo34644a((int) j, i);
    }

    /* renamed from: a */
    public void mo34642a(float f, int i) {
        mo34643a(6);
        mo34640a((byte) 4, i);
        this.f6216b.putFloat(f);
    }

    /* renamed from: a */
    public void mo34641a(double d, int i) {
        mo34643a(10);
        mo34640a((byte) 5, i);
        this.f6216b.putDouble(d);
    }

    /* renamed from: a */
    public void mo34648a(String str, int i) {
        byte[] bArr;
        try {
            bArr = str.getBytes(this.f6215a);
        } catch (UnsupportedEncodingException unused) {
            bArr = str.getBytes();
        }
        mo34643a(bArr.length + 10);
        if (bArr.length > 255) {
            mo34640a((byte) 7, i);
            this.f6216b.putInt(bArr.length);
            this.f6216b.put(bArr);
            return;
        }
        mo34640a((byte) 6, i);
        this.f6216b.put((byte) bArr.length);
        this.f6216b.put(bArr);
    }

    /* renamed from: a */
    public <K, V> void mo34650a(Map map, int i) {
        int i2;
        mo34643a(8);
        mo34640a((byte) 8, i);
        if (map == null) {
            i2 = 0;
        } else {
            i2 = map.size();
        }
        mo34644a(i2, 0);
        if (map != null) {
            for (Map.Entry entry : map.entrySet()) {
                mo34647a(entry.getKey(), 0);
                mo34647a(entry.getValue(), 1);
            }
        }
    }

    /* renamed from: a */
    public void mo34659a(boolean[] zArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(zArr.length, 0);
        for (boolean z : zArr) {
            mo34652a(z, 0);
        }
    }

    /* renamed from: a */
    public void mo34653a(byte[] bArr, int i) {
        mo34643a(bArr.length + 8);
        mo34640a((byte) Ascii.f4503CR, i);
        mo34640a((byte) 0, 0);
        mo34644a(bArr.length, 0);
        this.f6216b.put(bArr);
    }

    /* renamed from: a */
    public void mo34658a(short[] sArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(sArr.length, 0);
        for (short s : sArr) {
            mo34651a(s, 0);
        }
    }

    /* renamed from: a */
    public void mo34656a(int[] iArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(iArr.length, 0);
        for (int i2 : iArr) {
            mo34644a(i2, 0);
        }
    }

    /* renamed from: a */
    public void mo34657a(long[] jArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(jArr.length, 0);
        for (long j : jArr) {
            mo34645a(j, 0);
        }
    }

    /* renamed from: a */
    public void mo34655a(float[] fArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(fArr.length, 0);
        for (float f : fArr) {
            mo34642a(f, 0);
        }
    }

    /* renamed from: a */
    public void mo34654a(double[] dArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(dArr.length, 0);
        for (double d : dArr) {
            mo34641a(d, 0);
        }
    }

    /* renamed from: a */
    private void m7092a(Object[] objArr, int i) {
        mo34643a(8);
        mo34640a((byte) 9, i);
        mo34644a(objArr.length, 0);
        for (Object obj : objArr) {
            mo34647a(obj, 0);
        }
    }

    /* renamed from: a */
    public <T> void mo34649a(Collection collection, int i) {
        int i2;
        mo34643a(8);
        mo34640a((byte) 9, i);
        if (collection == null) {
            i2 = 0;
        } else {
            i2 = collection.size();
        }
        mo34644a(i2, 0);
        if (collection != null) {
            for (Object obj : collection) {
                mo34647a(obj, 0);
            }
        }
    }

    /* renamed from: a */
    public void mo34646a(C2944m mVar, int i) {
        mo34643a(2);
        mo34640a((byte) 10, i);
        mVar.mo34476a(this);
        mo34643a(2);
        mo34640a((byte) 11, 0);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
     arg types: [java.util.List, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void */
    /* renamed from: a */
    public void mo34647a(Object obj, int i) {
        if (obj instanceof Byte) {
            mo34660b(((Byte) obj).byteValue(), i);
        } else if (obj instanceof Boolean) {
            mo34652a(((Boolean) obj).booleanValue(), i);
        } else if (obj instanceof Short) {
            mo34651a(((Short) obj).shortValue(), i);
        } else if (obj instanceof Integer) {
            mo34644a(((Integer) obj).intValue(), i);
        } else if (obj instanceof Long) {
            mo34645a(((Long) obj).longValue(), i);
        } else if (obj instanceof Float) {
            mo34642a(((Float) obj).floatValue(), i);
        } else if (obj instanceof Double) {
            mo34641a(((Double) obj).doubleValue(), i);
        } else if (obj instanceof String) {
            mo34648a((String) obj, i);
        } else if (obj instanceof Map) {
            mo34650a((Map) obj, i);
        } else if (obj instanceof List) {
            mo34649a((Collection) ((List) obj), i);
        } else if (obj instanceof C2944m) {
            mo34646a((C2944m) obj, i);
        } else if (obj instanceof byte[]) {
            mo34653a((byte[]) obj, i);
        } else if (obj instanceof boolean[]) {
            mo34659a((boolean[]) obj, i);
        } else if (obj instanceof short[]) {
            mo34658a((short[]) obj, i);
        } else if (obj instanceof int[]) {
            mo34656a((int[]) obj, i);
        } else if (obj instanceof long[]) {
            mo34657a((long[]) obj, i);
        } else if (obj instanceof float[]) {
            mo34655a((float[]) obj, i);
        } else if (obj instanceof double[]) {
            mo34654a((double[]) obj, i);
        } else if (obj.getClass().isArray()) {
            m7092a((Object[]) obj, i);
        } else if (obj instanceof Collection) {
            mo34649a((Collection) obj, i);
        } else {
            throw new C2940j("write object error: unsupport type. " + obj.getClass());
        }
    }

    /* renamed from: a */
    public int mo34638a(String str) {
        this.f6215a = str;
        return 0;
    }
}
