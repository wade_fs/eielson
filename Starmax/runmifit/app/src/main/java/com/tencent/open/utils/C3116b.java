package com.tencent.open.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import com.tencent.open.p059a.C3082f;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/* renamed from: com.tencent.open.utils.b */
/* compiled from: ProGuard */
public class C3116b {
    /* access modifiers changed from: private */

    /* renamed from: c */
    public static String f6829c;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public String f6830a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C3119c f6831b;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public long f6832d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public Handler f6833e;

    /* renamed from: f */
    private Runnable f6834f = new Runnable() {
        /* class com.tencent.open.utils.C3116b.C31182 */

        public void run() {
            boolean z;
            C3082f.m7628a("AsynLoadImg", "saveFileRunnable:");
            String str = "share_qq_" + C3131k.m7793f(C3116b.this.f6830a) + ".jpg";
            String str2 = C3116b.f6829c + str;
            File file = new File(str2);
            Message obtainMessage = C3116b.this.f6833e.obtainMessage();
            if (file.exists()) {
                obtainMessage.arg1 = 0;
                obtainMessage.obj = str2;
                C3082f.m7628a("AsynLoadImg", "file exists: time:" + (System.currentTimeMillis() - C3116b.this.f6832d));
            } else {
                Bitmap a = C3116b.m7715a(C3116b.this.f6830a);
                if (a != null) {
                    z = C3116b.this.mo35166a(a, str);
                } else {
                    C3082f.m7628a("AsynLoadImg", "saveFileRunnable:get bmp fail---");
                    z = false;
                }
                if (z) {
                    obtainMessage.arg1 = 0;
                    obtainMessage.obj = str2;
                } else {
                    obtainMessage.arg1 = 1;
                }
                C3082f.m7628a("AsynLoadImg", "file not exists: download time:" + (System.currentTimeMillis() - C3116b.this.f6832d));
            }
            C3116b.this.f6833e.sendMessage(obtainMessage);
        }
    };

    public C3116b(Activity activity) {
        this.f6833e = new Handler(activity.getMainLooper()) {
            /* class com.tencent.open.utils.C3116b.C31171 */

            public void handleMessage(Message message) {
                C3082f.m7628a("AsynLoadImg", "handleMessage:" + message.arg1);
                if (message.arg1 == 0) {
                    C3116b.this.f6831b.mo34826a(message.arg1, (String) message.obj);
                } else {
                    C3116b.this.f6831b.mo34826a(message.arg1, (String) null);
                }
            }
        };
    }

    /* renamed from: a */
    public void mo35165a(String str, C3119c cVar) {
        C3082f.m7628a("AsynLoadImg", "--save---");
        if (str == null || str.equals("")) {
            cVar.mo34826a(1, (String) null);
        } else if (!C3131k.m7782b()) {
            cVar.mo34826a(2, (String) null);
        } else {
            f6829c = Environment.getExternalStorageDirectory() + "/tmp/";
            this.f6832d = System.currentTimeMillis();
            this.f6830a = str;
            this.f6831b = cVar;
            new Thread(this.f6834f).start();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x006d A[SYNTHETIC, Splitter:B:22:0x006d] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0078 A[SYNTHETIC, Splitter:B:28:0x0078] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo35166a(android.graphics.Bitmap r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.String r0 = "AsynLoadImg"
            java.lang.String r1 = com.tencent.open.utils.C3116b.f6829c
            r2 = 0
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x0061 }
            r3.<init>(r1)     // Catch:{ IOException -> 0x0061 }
            boolean r4 = r3.exists()     // Catch:{ IOException -> 0x0061 }
            if (r4 != 0) goto L_0x0013
            r3.mkdir()     // Catch:{ IOException -> 0x0061 }
        L_0x0013:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0061 }
            r3.<init>()     // Catch:{ IOException -> 0x0061 }
            r3.append(r1)     // Catch:{ IOException -> 0x0061 }
            r3.append(r7)     // Catch:{ IOException -> 0x0061 }
            java.lang.String r1 = r3.toString()     // Catch:{ IOException -> 0x0061 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0061 }
            r3.<init>()     // Catch:{ IOException -> 0x0061 }
            java.lang.String r4 = "saveFile:"
            r3.append(r4)     // Catch:{ IOException -> 0x0061 }
            r3.append(r7)     // Catch:{ IOException -> 0x0061 }
            java.lang.String r7 = r3.toString()     // Catch:{ IOException -> 0x0061 }
            com.tencent.open.p059a.C3082f.m7628a(r0, r7)     // Catch:{ IOException -> 0x0061 }
            java.io.File r7 = new java.io.File     // Catch:{ IOException -> 0x0061 }
            r7.<init>(r1)     // Catch:{ IOException -> 0x0061 }
            java.io.BufferedOutputStream r1 = new java.io.BufferedOutputStream     // Catch:{ IOException -> 0x0061 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0061 }
            r3.<init>(r7)     // Catch:{ IOException -> 0x0061 }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0061 }
            android.graphics.Bitmap$CompressFormat r7 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ IOException -> 0x005c, all -> 0x0059 }
            r2 = 80
            r6.compress(r7, r2, r1)     // Catch:{ IOException -> 0x005c, all -> 0x0059 }
            r1.flush()     // Catch:{ IOException -> 0x005c, all -> 0x0059 }
            r1.close()     // Catch:{ IOException -> 0x0053 }
            goto L_0x0057
        L_0x0053:
            r6 = move-exception
            r6.printStackTrace()
        L_0x0057:
            r6 = 1
            return r6
        L_0x0059:
            r6 = move-exception
            r2 = r1
            goto L_0x0076
        L_0x005c:
            r6 = move-exception
            r2 = r1
            goto L_0x0062
        L_0x005f:
            r6 = move-exception
            goto L_0x0076
        L_0x0061:
            r6 = move-exception
        L_0x0062:
            r6.printStackTrace()     // Catch:{ all -> 0x005f }
            java.lang.String r7 = "saveFile bmp fail---"
            com.tencent.open.p059a.C3082f.m7632b(r0, r7, r6)     // Catch:{ all -> 0x005f }
            r6 = 0
            if (r2 == 0) goto L_0x0075
            r2.close()     // Catch:{ IOException -> 0x0071 }
            goto L_0x0075
        L_0x0071:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0075:
            return r6
        L_0x0076:
            if (r2 == 0) goto L_0x0080
            r2.close()     // Catch:{ IOException -> 0x007c }
            goto L_0x0080
        L_0x007c:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0080:
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.open.utils.C3116b.mo35166a(android.graphics.Bitmap, java.lang.String):boolean");
    }

    /* renamed from: a */
    public static Bitmap m7715a(String str) {
        C3082f.m7628a("AsynLoadImg", "getbitmap:" + str);
        try {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setDoInput(true);
            httpURLConnection.connect();
            InputStream inputStream = httpURLConnection.getInputStream();
            Bitmap decodeStream = BitmapFactory.decodeStream(inputStream);
            inputStream.close();
            C3082f.m7628a("AsynLoadImg", "image download finished." + str);
            return decodeStream;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            C3082f.m7628a("AsynLoadImg", "getbitmap bmp fail---");
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            C3082f.m7628a("AsynLoadImg", "getbitmap bmp fail---");
            return null;
        }
    }
}
