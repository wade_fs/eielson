package com.tencent.stat;

public class StatAppMonitor implements Cloneable {
    public static final int FAILURE_RESULT_TYPE = 1;
    public static final int LOGIC_FAILURE_RESULT_TYPE = 2;
    public static final int SUCCESS_RESULT_TYPE = 0;

    /* renamed from: a */
    private String f6899a = null;

    /* renamed from: b */
    private long f6900b = 0;

    /* renamed from: c */
    private long f6901c = 0;

    /* renamed from: d */
    private int f6902d = 0;

    /* renamed from: e */
    private long f6903e = 0;

    /* renamed from: f */
    private int f6904f = 0;

    /* renamed from: g */
    private int f6905g = 1;

    public StatAppMonitor(String str) {
        this.f6899a = str;
    }

    public StatAppMonitor(String str, int i, int i2, long j, long j2, long j3, int i3) {
        this.f6899a = str;
        this.f6900b = j;
        this.f6901c = j2;
        this.f6902d = i;
        this.f6903e = j3;
        this.f6904f = i2;
        this.f6905g = i3;
    }

    public String getInterfaceName() {
        return this.f6899a;
    }

    public void setInterfaceName(String str) {
        this.f6899a = str;
    }

    public long getReqSize() {
        return this.f6900b;
    }

    public void setReqSize(long j) {
        this.f6900b = j;
    }

    public long getRespSize() {
        return this.f6901c;
    }

    public void setRespSize(long j) {
        this.f6901c = j;
    }

    public int getResultType() {
        return this.f6902d;
    }

    public void setResultType(int i) {
        this.f6902d = i;
    }

    public long getMillisecondsConsume() {
        return this.f6903e;
    }

    public void setMillisecondsConsume(long j) {
        this.f6903e = j;
    }

    public int getReturnCode() {
        return this.f6904f;
    }

    public void setReturnCode(int i) {
        this.f6904f = i;
    }

    public int getSampling() {
        return this.f6905g;
    }

    public void setSampling(int i) {
        if (i <= 0) {
            i = 1;
        }
        this.f6905g = i;
    }

    public StatAppMonitor clone() {
        try {
            return (StatAppMonitor) super.clone();
        } catch (CloneNotSupportedException unused) {
            return null;
        }
    }
}
