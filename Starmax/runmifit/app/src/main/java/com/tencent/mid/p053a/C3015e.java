package com.tencent.mid.p053a;

import android.content.Context;
import org.json.JSONObject;

/* renamed from: com.tencent.mid.a.e */
public abstract class C3015e {

    /* renamed from: a */
    protected Context f6520a = null;

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract int mo34845a();

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public abstract void mo34847b(JSONObject jSONObject);

    protected C3015e(Context context) {
        this.f6520a = context.getApplicationContext();
    }

    /* renamed from: a */
    public JSONObject mo34846a(JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = new JSONObject();
        }
        jSONObject.put("et", mo34845a());
        mo34847b(jSONObject);
        return jSONObject;
    }
}
