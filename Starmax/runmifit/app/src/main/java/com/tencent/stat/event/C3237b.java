package com.tencent.stat.event;

import android.content.Context;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.StatPreferences;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.b */
public class C3237b extends C3247k {

    /* renamed from: u */
    private static int f7361u;

    public EventType getType() {
        return EventType.BACKGROUND;
    }

    public C3237b(Context context, int i, double d, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, null, null, i, d, statSpecifyReportedInfo);
        if (f7361u == 0) {
            f7361u = StatPreferences.getInt(context, "back_ev_index", 0);
            if (f7361u > 2147383647) {
                f7361u = 0;
            }
        }
        f7361u++;
        StatPreferences.putInt(context, "back_ev_index", f7361u);
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        jSONObject.put("bc", f7361u);
        jSONObject.put("ft", 1);
        return super.onEncode(jSONObject);
    }
}
