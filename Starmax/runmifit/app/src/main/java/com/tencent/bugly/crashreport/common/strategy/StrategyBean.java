package com.tencent.bugly.crashreport.common.strategy;

import android.os.Parcel;
import android.os.Parcelable;
import com.tencent.bugly.proguard.C2908aq;
import java.util.Map;

/* compiled from: BUGLY */
public class StrategyBean implements Parcelable {
    public static final Parcelable.Creator<StrategyBean> CREATOR = new Parcelable.Creator<StrategyBean>() {
        /* class com.tencent.bugly.crashreport.common.strategy.StrategyBean.C28531 */

        /* renamed from: a */
        public StrategyBean createFromParcel(Parcel parcel) {
            return new StrategyBean(parcel);
        }

        /* renamed from: a */
        public StrategyBean[] newArray(int i) {
            return new StrategyBean[i];
        }
    };

    /* renamed from: a */
    public static String f5687a = "http://rqd.uu.qq.com/rqd/sync";

    /* renamed from: b */
    public static String f5688b = "http://android.bugly.qq.com/rqd/async";

    /* renamed from: c */
    public static String f5689c = "http://android.bugly.qq.com/rqd/async";

    /* renamed from: d */
    public static String f5690d;

    /* renamed from: e */
    public long f5691e;

    /* renamed from: f */
    public long f5692f;

    /* renamed from: g */
    public boolean f5693g;

    /* renamed from: h */
    public boolean f5694h;

    /* renamed from: i */
    public boolean f5695i;

    /* renamed from: j */
    public boolean f5696j;

    /* renamed from: k */
    public boolean f5697k;

    /* renamed from: l */
    public boolean f5698l;

    /* renamed from: m */
    public boolean f5699m;

    /* renamed from: n */
    public boolean f5700n;

    /* renamed from: o */
    public boolean f5701o;

    /* renamed from: p */
    public long f5702p;

    /* renamed from: q */
    public long f5703q;

    /* renamed from: r */
    public String f5704r;

    /* renamed from: s */
    public String f5705s;

    /* renamed from: t */
    public String f5706t;

    /* renamed from: u */
    public String f5707u;

    /* renamed from: v */
    public Map<String, String> f5708v;

    /* renamed from: w */
    public int f5709w;

    /* renamed from: x */
    public long f5710x;

    /* renamed from: y */
    public long f5711y;

    public int describeContents() {
        return 0;
    }

    public StrategyBean() {
        this.f5691e = -1;
        this.f5692f = -1;
        this.f5693g = true;
        this.f5694h = true;
        this.f5695i = true;
        this.f5696j = true;
        this.f5697k = false;
        this.f5698l = true;
        this.f5699m = true;
        this.f5700n = true;
        this.f5701o = true;
        this.f5703q = 30000;
        this.f5704r = f5688b;
        this.f5705s = f5689c;
        this.f5706t = f5687a;
        this.f5709w = 10;
        this.f5710x = 300000;
        this.f5711y = -1;
        this.f5692f = System.currentTimeMillis();
        StringBuilder sb = new StringBuilder();
        sb.append("S(");
        sb.append("@L@L");
        sb.append("@)");
        f5690d = sb.toString();
        sb.setLength(0);
        sb.append("*^");
        sb.append("@K#K");
        sb.append("@!");
        this.f5707u = sb.toString();
    }

    public StrategyBean(Parcel parcel) {
        this.f5691e = -1;
        this.f5692f = -1;
        boolean z = true;
        this.f5693g = true;
        this.f5694h = true;
        this.f5695i = true;
        this.f5696j = true;
        this.f5697k = false;
        this.f5698l = true;
        this.f5699m = true;
        this.f5700n = true;
        this.f5701o = true;
        this.f5703q = 30000;
        this.f5704r = f5688b;
        this.f5705s = f5689c;
        this.f5706t = f5687a;
        this.f5709w = 10;
        this.f5710x = 300000;
        this.f5711y = -1;
        try {
            f5690d = "S(" + "@L@L" + "@)";
            this.f5692f = parcel.readLong();
            this.f5693g = parcel.readByte() == 1;
            this.f5694h = parcel.readByte() == 1;
            this.f5695i = parcel.readByte() == 1;
            this.f5704r = parcel.readString();
            this.f5705s = parcel.readString();
            this.f5707u = parcel.readString();
            this.f5708v = C2908aq.m6927b(parcel);
            this.f5696j = parcel.readByte() == 1;
            this.f5697k = parcel.readByte() == 1;
            this.f5700n = parcel.readByte() == 1;
            this.f5701o = parcel.readByte() == 1;
            this.f5703q = parcel.readLong();
            this.f5698l = parcel.readByte() == 1;
            if (parcel.readByte() != 1) {
                z = false;
            }
            this.f5699m = z;
            this.f5702p = parcel.readLong();
            this.f5709w = parcel.readInt();
            this.f5710x = parcel.readLong();
            this.f5711y = parcel.readLong();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(this.f5692f);
        parcel.writeByte(this.f5693g ? (byte) 1 : 0);
        parcel.writeByte(this.f5694h ? (byte) 1 : 0);
        parcel.writeByte(this.f5695i ? (byte) 1 : 0);
        parcel.writeString(this.f5704r);
        parcel.writeString(this.f5705s);
        parcel.writeString(this.f5707u);
        C2908aq.m6929b(parcel, this.f5708v);
        parcel.writeByte(this.f5696j ? (byte) 1 : 0);
        parcel.writeByte(this.f5697k ? (byte) 1 : 0);
        parcel.writeByte(this.f5700n ? (byte) 1 : 0);
        parcel.writeByte(this.f5701o ? (byte) 1 : 0);
        parcel.writeLong(this.f5703q);
        parcel.writeByte(this.f5698l ? (byte) 1 : 0);
        parcel.writeByte(this.f5699m ? (byte) 1 : 0);
        parcel.writeLong(this.f5702p);
        parcel.writeInt(this.f5709w);
        parcel.writeLong(this.f5710x);
        parcel.writeLong(this.f5711y);
    }
}
