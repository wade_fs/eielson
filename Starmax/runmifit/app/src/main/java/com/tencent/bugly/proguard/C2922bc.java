package com.tencent.bugly.proguard;

/* renamed from: com.tencent.bugly.proguard.bc */
/* compiled from: BUGLY */
public final class C2922bc extends C2944m implements Cloneable {

    /* renamed from: a */
    public String f6070a = "";

    /* renamed from: b */
    public String f6071b = "";

    /* renamed from: c */
    public String f6072c = "";

    /* renamed from: d */
    public String f6073d = "";

    /* renamed from: e */
    public String f6074e = "";

    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
    }

    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34648a(this.f6070a, 0);
        String str = this.f6071b;
        if (str != null) {
            lVar.mo34648a(str, 1);
        }
        String str2 = this.f6072c;
        if (str2 != null) {
            lVar.mo34648a(str2, 2);
        }
        String str3 = this.f6073d;
        if (str3 != null) {
            lVar.mo34648a(str3, 3);
        }
        String str4 = this.f6074e;
        if (str4 != null) {
            lVar.mo34648a(str4, 4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6070a = kVar.mo34621a(0, true);
        this.f6071b = kVar.mo34621a(1, false);
        this.f6072c = kVar.mo34621a(2, false);
        this.f6073d = kVar.mo34621a(3, false);
        this.f6074e = kVar.mo34621a(4, false);
    }
}
