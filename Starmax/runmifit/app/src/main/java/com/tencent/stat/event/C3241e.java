package com.tencent.stat.event;

import android.content.Context;
import android.os.Process;
import com.baidu.mobstat.Config;
import com.tencent.stat.StatConfig;
import com.tencent.stat.StatNativeCrashReport;
import com.tencent.stat.StatServiceImpl;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.StatTrackLog;
import com.tencent.stat.common.C3204a;
import com.tencent.stat.common.C3209d;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.Util;
import java.io.File;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.e */
public class C3241e extends Event {

    /* renamed from: a */
    private String f7368a;

    /* renamed from: s */
    private JSONArray f7369s;

    /* renamed from: t */
    private int f7370t;

    /* renamed from: u */
    private Thread f7371u = null;

    /* renamed from: v */
    private String f7372v = null;

    /* renamed from: w */
    private long f7373w = -1;

    /* renamed from: x */
    private String f7374x = null;

    /* renamed from: a */
    public void mo35469a(long j) {
        this.f7373w = j;
    }

    /* renamed from: a */
    public void mo35470a(String str) {
        this.f7372v = str;
    }

    public C3241e(Context context, int i, int i2, Throwable th, Thread thread, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7370t = i2;
        m8091a(i2, th);
        this.f7371u = thread;
    }

    public C3241e(Context context, int i, int i2, JSONArray jSONArray, Thread thread, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7370t = i2;
        this.f7369s = jSONArray;
        this.f7371u = thread;
    }

    /* renamed from: b */
    public void mo35471b(String str) {
        try {
            this.f7374x = StatNativeCrashReport.readFile(new File(str));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: a */
    public String mo35468a() {
        return this.f7374x;
    }

    /* renamed from: a */
    private void m8091a(int i, Throwable th) {
        if (th != null) {
            this.f7370t = i;
            this.f7369s = StatCommonHelper.formatThrowable(th);
        }
    }

    public C3241e(Context context, int i, String str, int i2, int i3, Thread thread, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        if (str != null) {
            i3 = i3 <= 0 ? StatConfig.getMaxReportEventLength() : i3;
            if (str.length() <= i3) {
                this.f7368a = str;
            } else {
                this.f7368a = str.substring(0, i3);
            }
        }
        this.f7371u = thread;
        this.f7370t = i2;
    }

    public EventType getType() {
        return EventType.ERROR;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        jSONObject.put("cmode", 2);
        jSONObject.put("ea", this.f7370t);
        jSONObject.put("prcp", Process.myPid());
        jSONObject.put("prct", Process.myTid());
        long currentTimeMillis = System.currentTimeMillis();
        jSONObject.put("prts", (currentTimeMillis - StatServiceImpl.getAppStartupTime()) / 1000);
        jSONObject.put("fgts", (currentTimeMillis - StatServiceImpl.getFrontgroundStartupTime()) / 1000);
        jSONObject.put("tpg", StatServiceImpl.fetchPageFlows());
        jSONObject.put("ckv", StatConfig.getCrashKeyValue());
        new C3204a(this.f7355q).mo35400a(jSONObject, this.f7371u);
        m8094b(jSONObject);
        m8092a(jSONObject);
        m8095c(jSONObject);
        m8096d(jSONObject);
        return true;
    }

    /* renamed from: a */
    private void m8092a(JSONObject jSONObject) throws JSONException {
        jSONObject.put("md5", StatCommonHelper.md5sum(this.f7368a));
        jSONObject.put(Config.EXCEPTION_CRASH_TYPE, this.f7370t);
        jSONObject.put("bid", this.f7355q.getPackageName());
        jSONObject.put("dt", System.currentTimeMillis() / 1000);
    }

    /* renamed from: b */
    private void m8094b(JSONObject jSONObject) throws JSONException {
        JSONObject a = m8090a(this.f7371u);
        JSONArray jSONArray = this.f7369s;
        if (jSONArray != null) {
            a.put("fra", jSONArray);
            if (this.f7373w > -1) {
                a.put("gfra", this.f7369s);
            }
        } else {
            a.put("fra", this.f7368a);
            if (this.f7373w > -1) {
                a.put("gfra", this.f7368a);
            }
        }
        int i = this.f7370t;
        if (i >= 4 && i <= 10) {
            a.put("fra", this.f7368a);
            if (this.f7373w > -1) {
                a.put("gfra", this.f7368a);
            }
        }
        Util.jsonPut(a, "des", this.f7372v);
        jSONObject.put("cth", a);
        if (this.f7370t == 3) {
            a.put("nfra", this.f7374x);
        }
    }

    /* renamed from: a */
    private JSONObject m8090a(Thread thread) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("thn", thread.getId());
        jSONObject.put("na", thread.getName());
        jSONObject.put(Config.PRINCIPAL_PART, thread.getPriority());
        long j = this.f7373w;
        if (j > -1) {
            jSONObject.put("gthn", j);
        }
        return jSONObject;
    }

    /* renamed from: c */
    private void m8095c(JSONObject jSONObject) {
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        if (allStackTraces != null && allStackTraces.size() != 0) {
            JSONArray jSONArray = new JSONArray();
            for (Map.Entry entry : allStackTraces.entrySet()) {
                try {
                    Thread thread = (Thread) entry.getKey();
                    if (thread.getId() != this.f7371u.getId()) {
                        JSONObject a = m8090a(thread);
                        JSONArray jSONArray2 = new JSONArray();
                        StatCommonHelper.formatStackTraceElement(jSONArray2, (StackTraceElement[]) entry.getValue());
                        if (jSONArray2.length() != 0) {
                            a.put("fra", jSONArray2);
                            jSONArray.put(a);
                        }
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
            if (jSONArray.length() > 0) {
                try {
                    jSONObject.put("oth", jSONArray);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* renamed from: b */
    private String m8093b() {
        return C3209d.m7983a(50);
    }

    /* renamed from: d */
    private void m8096d(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        Util.jsonPut(jSONObject2, "clog", StatTrackLog.fetchLog());
        Util.jsonPut(jSONObject2, "llog", m8093b());
        try {
            jSONObject.put("ext", jSONObject2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
