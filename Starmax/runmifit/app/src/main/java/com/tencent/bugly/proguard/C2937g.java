package com.tencent.bugly.proguard;

import android.support.v4.app.NotificationCompat;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.tencent.bugly.proguard.g */
/* compiled from: BUGLY */
public final class C2937g extends C2944m {

    /* renamed from: k */
    static byte[] f6196k = null;

    /* renamed from: l */
    static Map<String, String> f6197l = null;

    /* renamed from: m */
    static final /* synthetic */ boolean f6198m = (!C2937g.class.desiredAssertionStatus());

    /* renamed from: a */
    public short f6199a = 0;

    /* renamed from: b */
    public byte f6200b = 0;

    /* renamed from: c */
    public int f6201c = 0;

    /* renamed from: d */
    public int f6202d = 0;

    /* renamed from: e */
    public String f6203e = null;

    /* renamed from: f */
    public String f6204f = null;

    /* renamed from: g */
    public byte[] f6205g;

    /* renamed from: h */
    public int f6206h = 0;

    /* renamed from: i */
    public Map<String, String> f6207i;

    /* renamed from: j */
    public Map<String, String> f6208j;

    public boolean equals(Object obj) {
        C2937g gVar = (C2937g) obj;
        return C2945n.m7121a(1, gVar.f6199a) && C2945n.m7121a(1, gVar.f6200b) && C2945n.m7121a(1, gVar.f6201c) && C2945n.m7121a(1, gVar.f6202d) && C2945n.m7123a(1, gVar.f6203e) && C2945n.m7123a(1, gVar.f6204f) && C2945n.m7123a(1, gVar.f6205g) && C2945n.m7121a(1, gVar.f6206h) && C2945n.m7123a(1, gVar.f6207i) && C2945n.m7123a(1, gVar.f6208j);
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f6198m) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.l.a(java.util.Map, int):void
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int]
     candidates:
      com.tencent.bugly.proguard.l.a(java.lang.Object[], int):void
      com.tencent.bugly.proguard.l.a(byte, int):void
      com.tencent.bugly.proguard.l.a(double, int):void
      com.tencent.bugly.proguard.l.a(float, int):void
      com.tencent.bugly.proguard.l.a(int, int):void
      com.tencent.bugly.proguard.l.a(long, int):void
      com.tencent.bugly.proguard.l.a(com.tencent.bugly.proguard.m, int):void
      com.tencent.bugly.proguard.l.a(java.lang.Object, int):void
      com.tencent.bugly.proguard.l.a(java.lang.String, int):void
      com.tencent.bugly.proguard.l.a(java.util.Collection, int):void
      com.tencent.bugly.proguard.l.a(short, int):void
      com.tencent.bugly.proguard.l.a(boolean, int):void
      com.tencent.bugly.proguard.l.a(byte[], int):void
      com.tencent.bugly.proguard.l.a(double[], int):void
      com.tencent.bugly.proguard.l.a(float[], int):void
      com.tencent.bugly.proguard.l.a(int[], int):void
      com.tencent.bugly.proguard.l.a(long[], int):void
      com.tencent.bugly.proguard.l.a(short[], int):void
      com.tencent.bugly.proguard.l.a(boolean[], int):void
      com.tencent.bugly.proguard.l.a(java.util.Map, int):void */
    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34651a(this.f6199a, 1);
        lVar.mo34660b(this.f6200b, 2);
        lVar.mo34644a(this.f6201c, 3);
        lVar.mo34644a(this.f6202d, 4);
        lVar.mo34648a(this.f6203e, 5);
        lVar.mo34648a(this.f6204f, 6);
        lVar.mo34653a(this.f6205g, 7);
        lVar.mo34644a(this.f6206h, 8);
        lVar.mo34650a((Map) this.f6207i, 9);
        lVar.mo34650a((Map) this.f6208j, 10);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(short, int, boolean):short
     arg types: [short, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(short, int, boolean):short */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
     arg types: [byte, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, int, boolean):int
     arg types: [int, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
     arg types: [byte[], int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
     arg types: [java.util.Map<java.lang.String, java.lang.String>, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(long, int, boolean):long
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        try {
            this.f6199a = kVar.mo34624a(this.f6199a, 1, true);
            this.f6200b = kVar.mo34613a(this.f6200b, 2, true);
            this.f6201c = kVar.mo34616a(this.f6201c, 3, true);
            this.f6202d = kVar.mo34616a(this.f6202d, 4, true);
            this.f6203e = kVar.mo34621a(5, true);
            this.f6204f = kVar.mo34621a(6, true);
            if (f6196k == null) {
                f6196k = new byte[]{0};
            }
            this.f6205g = kVar.mo34630a(f6196k, 7, true);
            this.f6206h = kVar.mo34616a(this.f6206h, 8, true);
            if (f6197l == null) {
                f6197l = new HashMap();
                f6197l.put("", "");
            }
            this.f6207i = (Map) kVar.mo34620a((Object) f6197l, 9, true);
            if (f6197l == null) {
                f6197l = new HashMap();
                f6197l.put("", "");
            }
            this.f6208j = (Map) kVar.mo34620a((Object) f6197l, 10, true);
        } catch (Exception e) {
            e.printStackTrace();
            PrintStream printStream = System.out;
            printStream.println("RequestPacket decode error " + C2936f.m7035a(this.f6205g));
            throw new RuntimeException(e);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i
     arg types: [java.util.Map<java.lang.String, java.lang.String>, java.lang.String]
     candidates:
      com.tencent.bugly.proguard.i.a(byte, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(char, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(com.tencent.bugly.proguard.m, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.String, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Collection, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(boolean, java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(byte[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(double[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(float[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(int[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(long[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.lang.Object[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(short[], java.lang.String):com.tencent.bugly.proguard.i
      com.tencent.bugly.proguard.i.a(java.util.Map, java.lang.String):com.tencent.bugly.proguard.i */
    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34604a(this.f6199a, "iVersion");
        iVar.mo34593a(this.f6200b, "cPacketType");
        iVar.mo34597a(this.f6201c, "iMessageType");
        iVar.mo34597a(this.f6202d, "iRequestId");
        iVar.mo34601a(this.f6203e, "sServantName");
        iVar.mo34601a(this.f6204f, "sFuncName");
        iVar.mo34606a(this.f6205g, "sBuffer");
        iVar.mo34597a(this.f6206h, "iTimeout");
        iVar.mo34603a((Map) this.f6207i, "context");
        iVar.mo34603a((Map) this.f6208j, NotificationCompat.CATEGORY_STATUS);
    }
}
