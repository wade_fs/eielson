package com.tencent.stat.event;

import android.content.Context;
import com.baidu.mobstat.Config;
import com.tencent.stat.NetworkManager;
import com.tencent.stat.StatAppMonitor;
import com.tencent.stat.StatSpecifyReportedInfo;
import com.tencent.stat.common.StatCommonHelper;
import com.tencent.stat.common.Util;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.tencent.stat.event.g */
public class C3243g extends Event {

    /* renamed from: s */
    private static String f7376s;

    /* renamed from: t */
    private static String f7377t;

    /* renamed from: a */
    private StatAppMonitor f7378a = null;

    public C3243g(Context context, int i, StatAppMonitor statAppMonitor, StatSpecifyReportedInfo statSpecifyReportedInfo) {
        super(context, i, statSpecifyReportedInfo);
        this.f7378a = statAppMonitor.clone();
    }

    public EventType getType() {
        return EventType.MONITOR_STAT;
    }

    public boolean onEncode(JSONObject jSONObject) throws JSONException {
        StatAppMonitor statAppMonitor = this.f7378a;
        if (statAppMonitor == null) {
            return false;
        }
        jSONObject.put("na", statAppMonitor.getInterfaceName());
        jSONObject.put("rq", this.f7378a.getReqSize());
        jSONObject.put("rp", this.f7378a.getRespSize());
        jSONObject.put("rt", this.f7378a.getResultType());
        jSONObject.put("tm", this.f7378a.getMillisecondsConsume());
        jSONObject.put("rc", this.f7378a.getReturnCode());
        jSONObject.put("sp", this.f7378a.getSampling());
        if (f7377t == null) {
            f7377t = StatCommonHelper.getAppVersion(this.f7355q);
        }
        Util.jsonPut(jSONObject, "av", f7377t);
        if (f7376s == null) {
            f7376s = StatCommonHelper.getSimOperator(this.f7355q);
        }
        Util.jsonPut(jSONObject, Config.OPERATOR, f7376s);
        jSONObject.put("cn", NetworkManager.getInstance(this.f7355q).getCurNetwrokName());
        return true;
    }
}
