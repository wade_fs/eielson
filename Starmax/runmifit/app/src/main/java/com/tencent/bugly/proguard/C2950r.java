package com.tencent.bugly.proguard;

import android.content.Context;
import android.text.TextUtils;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.beta.p050ui.C2823e;
import com.tencent.bugly.beta.p050ui.C2825g;
import com.tencent.bugly.beta.tinker.TinkerManager;
import com.tencent.bugly.beta.upgrade.BetaGrayStrategy;
import java.io.File;
import java.util.Iterator;

/* renamed from: com.tencent.bugly.proguard.r */
/* compiled from: BUGLY */
public class C2950r {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.b(java.lang.String, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.b(java.lang.String, boolean):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
     arg types: [java.lang.String, int]
     candidates:
      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
    /* renamed from: a */
    public static synchronized void m7139a(Context context) {
        String str;
        File[] listFiles;
        String a;
        Context context2 = context;
        synchronized (C2950r.class) {
            final C2808e eVar = C2808e.f5336E;
            String absolutePath = context2.getDir(TinkerManager.PATCH_DIR, 0).getAbsolutePath();
            String absolutePath2 = context2.getDir("tmpPatch", 32768).getAbsolutePath();
            eVar.f5343G = new File(absolutePath, TinkerManager.PATCH_NAME);
            if (TextUtils.isEmpty(C2804a.m6272b("PatchFile", ""))) {
                C2804a.m6264a("PatchFile", eVar.f5343G.getAbsolutePath());
            }
            eVar.f5344H = new File(absolutePath2, "tmpPatch.apk");
            if (eVar.f5343G != null && TextUtils.isEmpty(C2804a.m6272b("PatchFile", ""))) {
                C2804a.m6264a("PatchFile", eVar.f5343G.getAbsolutePath());
            }
            eVar.f5345I = context2.getDir("tmpPatch", 0);
            if (eVar.f5345I != null && TextUtils.isEmpty(C2804a.m6272b("PatchTmpDir", ""))) {
                C2804a.m6264a("PatchTmpDir", eVar.f5343G.getAbsolutePath());
            }
            BetaGrayStrategy betaGrayStrategy = (BetaGrayStrategy) C2804a.m6261a("st.bch", BetaGrayStrategy.CREATOR);
            if (betaGrayStrategy == null || betaGrayStrategy.f5455a == null || betaGrayStrategy.f5455a.f6297p != 3) {
                if (TinkerManager.getInstance().getPatchDirectory(eVar.f5385s) != null && !TinkerManager.getInstance().getPatchDirectory(eVar.f5385s).exists()) {
                    C2804a.m6265a("IS_PATCH_ROLL_BACK", false);
                }
                if (C2804a.m6273b("IS_PATCH_ROLL_BACK", false)) {
                    TinkerManager.getInstance().onPatchRollback(false);
                }
            } else {
                if (betaGrayStrategy.f5455a.f6293l != null) {
                    eVar.f5349M = betaGrayStrategy.f5455a.f6293l.get("H2");
                }
                eVar.f5350N = C2804a.m6273b("PatchResult", false);
                eVar.f5351O = Integer.valueOf(C2804a.m6272b("PATCH_MAX_TIMES", "0")).intValue();
                if (!eVar.f5350N) {
                    C2903an.m6857a("[patch] inject failure", new Object[0]);
                    if (!C2804a.m6273b("UPLOAD_PATCH_RESULT", false)) {
                        C2804a.m6265a("UPLOAD_PATCH_RESULT", true);
                        C2947p pVar = C2947p.f6229a;
                        C2957w wVar = r7;
                        C2957w wVar2 = new C2957w("active", System.currentTimeMillis(), (byte) 1, 0, null, betaGrayStrategy.f5455a.f6294m, betaGrayStrategy.f5455a.f6297p, null);
                        if (pVar.mo34667a(wVar)) {
                            C2903an.m6857a("save patch failed event success!", new Object[0]);
                        } else {
                            C2903an.m6865e("save patch failed event failed!", new Object[0]);
                        }
                        if (C2808e.f5336E.f5343G != null && C2808e.f5336E.f5343G.exists() && C2808e.f5336E.f5343G.delete()) {
                            C2903an.m6857a("[patch] delete patch.apk success", new Object[0]);
                        }
                    }
                } else {
                    C2903an.m6857a("[patch] inject success", new Object[0]);
                    if (!C2804a.m6273b("UPLOAD_PATCH_RESULT", false)) {
                        C2804a.m6265a("UPLOAD_PATCH_RESULT", true);
                        C2947p pVar2 = C2947p.f6229a;
                        C2957w wVar3 = r6;
                        C2957w wVar4 = new C2957w("active", System.currentTimeMillis(), (byte) 0, 0, null, betaGrayStrategy.f5455a.f6294m, betaGrayStrategy.f5455a.f6297p, null);
                        if (pVar2.mo34667a(wVar3)) {
                            C2903an.m6857a("save patch success event success!", new Object[0]);
                        } else {
                            C2903an.m6865e("save patch success event failed!", new Object[0]);
                        }
                    }
                }
            }
            if (TextUtils.isEmpty(TinkerManager.getNewTinkerId())) {
                C2903an.m6864d("[patch] tinker new id is null ,so patch version is invalid", new Object[0]);
                eVar.f5349M = "";
            }
            C2908aq.m6931b("G15", eVar.f5349M);
            File file = C2808e.f5336E.f5344H;
            if (file != null && file.exists() && file.delete()) {
                C2903an.m6857a("[patch] delete tmpPatch.apk success", new Object[0]);
            }
            File file2 = C2808e.f5336E.f5343G;
            if (!(file2 == null || !file2.exists() || (a = C2908aq.m6901a(file2, "SHA")) == null)) {
                C2808e.f5336E.f5348L = a;
            }
            if (TinkerManager.isTinkerManagerInstalled()) {
                if (TextUtils.isEmpty(eVar.f5346J)) {
                    eVar.f5346J = TinkerManager.getTinkerId();
                }
                C2903an.m6857a("TINKER_ID:" + eVar.f5346J, new Object[0]);
                eVar.f5347K = TinkerManager.getNewTinkerId();
                C2903an.m6857a("NEW_TINKER_ID:" + eVar.f5347K, new Object[0]);
                TinkerManager.getInstance().setTinkerListener(new TinkerManager.TinkerListener() {
                    /* class com.tencent.bugly.proguard.C2950r.C29511 */

                    public void onDownloadSuccess(String str) {
                        if (eVar.f5359W != null) {
                            eVar.f5359W.onDownloadSuccess(str);
                        }
                    }

                    public void onDownloadFailure(String str) {
                        if (eVar.f5359W != null) {
                            eVar.f5359W.onDownloadFailure(str);
                        }
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
                     arg types: [java.lang.String, int]
                     candidates:
                      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
                      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
                      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
                      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
                    public void onPatchStart() {
                        eVar.f5365ac = true;
                        C2804a.m6265a("IS_PATCHING", true);
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
                     arg types: [java.lang.String, int]
                     candidates:
                      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
                      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
                      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
                      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
                    public void onApplySuccess(String str) {
                        C2808e eVar = eVar;
                        eVar.f5365ac = false;
                        eVar.f5350N = true;
                        C2804a.m6265a("IS_PATCHING", false);
                        C2804a.m6265a("PatchResult", true);
                        C2903an.m6857a("Tinker patch success, result: " + str, new Object[0]);
                        if (eVar.f5360X) {
                            C2825g.m6298a(new C2823e(), true);
                        }
                        if (eVar.f5359W != null) {
                            eVar.f5359W.onApplySuccess(str);
                        }
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
                     arg types: [java.lang.String, int]
                     candidates:
                      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
                      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
                      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
                      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
                    public void onApplyFailure(String str) {
                        eVar.f5350N = false;
                        C2804a.m6265a("PatchResult", false);
                        C2804a.m6265a("IS_PATCHING", false);
                        C2903an.m6857a("Tinker patch failure, result: " + str, new Object[0]);
                        if (eVar.f5359W != null) {
                            eVar.f5359W.onApplyFailure(str);
                        }
                    }

                    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
                     method: com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void
                     arg types: [java.lang.String, int]
                     candidates:
                      com.tencent.bugly.beta.global.a.a(android.content.Context, float):int
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable$Creator):T
                      com.tencent.bugly.beta.global.a.a(android.content.Context, java.lang.String):java.lang.String
                      com.tencent.bugly.beta.global.a.a(java.lang.String, java.lang.String):void
                      com.tencent.bugly.beta.global.a.a(java.io.File, java.io.File):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, android.os.Parcelable):boolean
                      com.tencent.bugly.beta.global.a.a(java.lang.String, boolean):void */
                    public void onPatchRollback() {
                        C2903an.m6857a("patch rollback callback.", new Object[0]);
                        if (eVar.f5359W != null) {
                            eVar.f5359W.onPatchRollback();
                        }
                        if (TinkerManager.getInstance().getPatchDirectory(eVar.f5385s) != null && !TinkerManager.getInstance().getPatchDirectory(eVar.f5385s).exists()) {
                            C2804a.m6265a("IS_PATCH_ROLL_BACK", false);
                        }
                    }
                });
            }
            if (TextUtils.isEmpty(C2804a.m6272b("BaseArchName", "")) && (str = eVar.f5385s.getApplicationInfo().nativeLibraryDir) != null && (listFiles = new File(str).listFiles()) != null && listFiles.length > 0) {
                boolean z = false;
                for (File file3 : listFiles) {
                    if (file3.getName().endsWith(".so")) {
                        String replace = file3.getName().replace(".so", "");
                        if (replace.startsWith("lib")) {
                            replace = replace.substring(replace.indexOf("lib") + 3);
                        }
                        C2903an.m6857a("libName:" + replace, new Object[0]);
                        String absolutePath3 = file3.getAbsolutePath();
                        C2903an.m6857a("soFilePath:" + absolutePath3, new Object[0]);
                        Iterator<String> it = eVar.f5363aa.iterator();
                        while (true) {
                            if (it.hasNext()) {
                                if (replace.equals(it.next())) {
                                    z = true;
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                        if (!z) {
                            String b = C2804a.m6271b(absolutePath3);
                            C2903an.m6857a("archName:" + b, new Object[0]);
                            if (b != null && b.equals("armeabi-v5te")) {
                                b = "armeabi";
                            }
                            C2804a.m6264a(replace, b);
                            if (TextUtils.isEmpty(C2804a.m6272b("BaseArchName", ""))) {
                                C2804a.m6264a("BaseArchName", b);
                            }
                            z = false;
                        }
                    }
                }
            }
        }
    }
}
