package com.tencent.open.web.security;

import com.tencent.open.C3070a;
import com.tencent.open.p059a.C3082f;

/* compiled from: ProGuard */
public class SecureJsInterface extends C3070a.C3072b {
    public static boolean isPWDEdit = false;

    /* renamed from: a */
    private String f6875a;

    public boolean customCallback() {
        return true;
    }

    public void curPosFromJS(String str) {
        int i;
        C3082f.m7631b("openSDK_LOG.SecureJsInterface", "-->curPosFromJS: " + str);
        try {
            i = Integer.parseInt(str);
        } catch (NumberFormatException e) {
            C3082f.m7632b("openSDK_LOG.SecureJsInterface", "-->curPosFromJS number format exception.", e);
            i = -1;
        }
        if (i >= 0) {
            boolean z = C3136a.f6878c;
            if (!C3136a.f6877b) {
                this.f6875a = C3136a.f6876a;
                String str2 = this.f6875a;
                JniInterface.insetTextToArray(i, str2, str2.length());
                C3082f.m7628a("openSDK_LOG.SecureJsInterface", "curPosFromJS mKey: " + this.f6875a);
            } else if (Boolean.valueOf(JniInterface.BackSpaceChar(C3136a.f6877b, i)).booleanValue()) {
                C3136a.f6877b = false;
            }
        } else {
            throw new RuntimeException("position is illegal.");
        }
    }

    public void isPasswordEdit(String str) {
        int i;
        C3082f.m7634c("openSDK_LOG.SecureJsInterface", "-->is pswd edit, flag: " + str);
        try {
            i = Integer.parseInt(str);
        } catch (Exception e) {
            C3082f.m7636e("openSDK_LOG.SecureJsInterface", "-->is pswd edit exception: " + e.getMessage());
            i = -1;
        }
        if (i != 0 && i != 1) {
            throw new RuntimeException("is pswd edit flag is illegal.");
        } else if (i == 0) {
            isPWDEdit = false;
        } else if (i == 1) {
            isPWDEdit = true;
        }
    }

    public void clearAllEdit() {
        C3082f.m7634c("openSDK_LOG.SecureJsInterface", "-->clear all edit.");
        try {
            JniInterface.clearAllPWD();
        } catch (Exception e) {
            C3082f.m7636e("openSDK_LOG.SecureJsInterface", "-->clear all edit exception: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    public String getMD5FromNative() {
        C3082f.m7634c("openSDK_LOG.SecureJsInterface", "-->get md5 form native");
        try {
            String pWDKeyToMD5 = JniInterface.getPWDKeyToMD5(null);
            C3082f.m7628a("openSDK_LOG.SecureJsInterface", "-->getMD5FromNative, MD5= " + pWDKeyToMD5);
            return pWDKeyToMD5;
        } catch (Exception e) {
            C3082f.m7636e("openSDK_LOG.SecureJsInterface", "-->get md5 form native exception: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
