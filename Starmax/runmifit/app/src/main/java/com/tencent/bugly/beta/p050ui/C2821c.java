package com.tencent.bugly.beta.p050ui;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import com.google.android.gms.common.util.CrashUtils;
import com.tencent.bugly.beta.Beta;
import com.tencent.bugly.beta.download.BetaReceiver;
import com.tencent.bugly.beta.download.DownloadTask;
import com.tencent.bugly.beta.global.C2804a;
import com.tencent.bugly.beta.global.C2808e;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2959y;
import com.tencent.open.SocialConstants;
import java.util.Locale;

/* renamed from: com.tencent.bugly.beta.ui.c */
/* compiled from: BUGLY */
public class C2821c {

    /* renamed from: a */
    public static C2821c f5418a = new C2821c();

    /* renamed from: b */
    public DownloadTask f5419b;

    /* renamed from: c */
    public String f5420c = (this.f5425h.getPackageName() + ".beta.DOWNLOAD_NOTIFY");

    /* renamed from: d */
    public C2959y f5421d;

    /* renamed from: e */
    public C2820b f5422e;

    /* renamed from: f */
    private NotificationManager f5423f = ((NotificationManager) this.f5425h.getSystemService("notification"));

    /* renamed from: g */
    private Notification f5424g;

    /* renamed from: h */
    private Context f5425h = C2808e.f5336E.f5385s;

    /* renamed from: i */
    private boolean f5426i = true;

    /* renamed from: j */
    private long f5427j;

    /* renamed from: k */
    private NotificationCompat.Builder f5428k;

    private C2821c() {
        this.f5425h.registerReceiver(new BetaReceiver(), new IntentFilter(this.f5420c));
        m6291b();
    }

    /* renamed from: b */
    private void m6291b() {
        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel notificationChannel = new NotificationChannel("001", "bugly_upgrade", 2);
            notificationChannel.setDescription("bugly upgrade");
            notificationChannel.enableVibration(false);
            notificationChannel.setVibrationPattern(new long[]{0});
            this.f5423f.createNotificationChannel(notificationChannel);
        }
    }

    /* renamed from: a */
    public void mo34160a(DownloadTask downloadTask) {
        this.f5419b = downloadTask;
        this.f5427j = this.f5419b.getSavedLength();
        this.f5426i = downloadTask.isNeededNotify();
        if (this.f5426i && C2808e.f5336E.f5354R) {
            this.f5423f.cancel(1000);
            Intent intent = new Intent(this.f5420c);
            intent.putExtra(SocialConstants.TYPE_REQUEST, 1);
            if (this.f5428k == null) {
                if (Build.VERSION.SDK_INT >= 26) {
                    try {
                        this.f5428k = new NotificationCompat.Builder(this.f5425h, "001");
                    } catch (Throwable unused) {
                        this.f5428k = new NotificationCompat.Builder(this.f5425h);
                    }
                } else {
                    this.f5428k = new NotificationCompat.Builder(this.f5425h);
                }
            }
            NotificationCompat.Builder builder = this.f5428k;
            NotificationCompat.Builder contentTitle = builder.setTicker(Beta.strNotificationDownloading + C2808e.f5336E.f5391y).setContentTitle(C2808e.f5336E.f5391y);
            Locale locale = Locale.getDefault();
            Object[] objArr = new Object[2];
            objArr[0] = Beta.strNotificationDownloading;
            long j = 0;
            if (this.f5419b.getTotalLength() != 0) {
                j = (this.f5419b.getSavedLength() * 100) / this.f5419b.getTotalLength();
            }
            objArr[1] = Integer.valueOf((int) j);
            contentTitle.setContentText(String.format(locale, "%s %d%%", objArr)).setContentIntent(PendingIntent.getBroadcast(this.f5425h, 1, intent, CrashUtils.ErrorDialogData.BINDER_CRASH)).setAutoCancel(false);
            if (C2808e.f5336E.f5372f > 0) {
                this.f5428k.setSmallIcon(C2808e.f5336E.f5372f);
            } else if (!(C2808e.f5336E.f5392z == null || C2808e.f5336E.f5392z.applicationInfo == null)) {
                this.f5428k.setSmallIcon(C2808e.f5336E.f5392z.applicationInfo.icon);
            }
            try {
                if (C2808e.f5336E.f5373g > 0 && this.f5425h.getResources().getDrawable(C2808e.f5336E.f5373g) != null) {
                    this.f5428k.setLargeIcon(C2804a.m6259a(this.f5425h.getResources().getDrawable(C2808e.f5336E.f5373g)));
                }
            } catch (Resources.NotFoundException e) {
                C2903an.m6862c(C2821c.class, "[initNotify] " + e.getMessage(), new Object[0]);
            }
            this.f5424g = this.f5428k.build();
            this.f5423f.notify(1000, this.f5424g);
        }
    }

    /* renamed from: a */
    public void mo34159a() {
        if (this.f5426i && this.f5419b != null && C2808e.f5336E.f5354R) {
            if (this.f5419b.getSavedLength() - this.f5427j > 307200 || this.f5419b.getStatus() == 1 || this.f5419b.getStatus() == 5 || this.f5419b.getStatus() == 3) {
                this.f5427j = this.f5419b.getSavedLength();
                if (this.f5419b.getStatus() == 1) {
                    this.f5428k.setAutoCancel(true).setContentText(Beta.strNotificationClickToInstall).setContentTitle(String.format("%s %s", C2808e.f5336E.f5391y, Beta.strNotificationDownloadSucc));
                } else if (this.f5419b.getStatus() == 5) {
                    this.f5428k.setAutoCancel(false).setContentText(Beta.strNotificationClickToRetry).setContentTitle(String.format("%s %s", C2808e.f5336E.f5391y, Beta.strNotificationDownloadError));
                } else {
                    long j = 0;
                    if (this.f5419b.getStatus() == 2) {
                        NotificationCompat.Builder contentTitle = this.f5428k.setContentTitle(C2808e.f5336E.f5391y);
                        Locale locale = Locale.getDefault();
                        Object[] objArr = new Object[2];
                        objArr[0] = Beta.strNotificationDownloading;
                        if (this.f5419b.getTotalLength() != 0) {
                            j = (this.f5419b.getSavedLength() * 100) / this.f5419b.getTotalLength();
                        }
                        objArr[1] = Integer.valueOf((int) j);
                        contentTitle.setContentText(String.format(locale, "%s %d%%", objArr)).setAutoCancel(false);
                    } else if (this.f5419b.getStatus() == 3) {
                        NotificationCompat.Builder contentTitle2 = this.f5428k.setContentTitle(C2808e.f5336E.f5391y);
                        Locale locale2 = Locale.getDefault();
                        Object[] objArr2 = new Object[2];
                        objArr2[0] = Beta.strNotificationClickToContinue;
                        if (this.f5419b.getTotalLength() != 0) {
                            j = (this.f5419b.getSavedLength() * 100) / this.f5419b.getTotalLength();
                        }
                        objArr2[1] = Integer.valueOf((int) j);
                        contentTitle2.setContentText(String.format(locale2, "%s %d%%", objArr2)).setAutoCancel(false);
                    }
                }
                this.f5424g = this.f5428k.build();
                this.f5423f.notify(1000, this.f5424g);
            }
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:7|8|9|10) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x002f */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void mo34161a(com.tencent.bugly.proguard.C2959y r9, com.tencent.bugly.beta.p050ui.C2820b r10) {
        /*
            r8 = this;
            monitor-enter(r8)
            r8.f5421d = r9     // Catch:{ all -> 0x0109 }
            r8.f5422e = r10     // Catch:{ all -> 0x0109 }
            android.app.NotificationManager r10 = r8.f5423f     // Catch:{ all -> 0x0109 }
            r0 = 1001(0x3e9, float:1.403E-42)
            r10.cancel(r0)     // Catch:{ all -> 0x0109 }
            android.content.Intent r10 = new android.content.Intent     // Catch:{ all -> 0x0109 }
            java.lang.String r1 = r8.f5420c     // Catch:{ all -> 0x0109 }
            r10.<init>(r1)     // Catch:{ all -> 0x0109 }
            java.lang.String r1 = "request"
            r2 = 2
            r10.putExtra(r1, r2)     // Catch:{ all -> 0x0109 }
            android.support.v4.app.NotificationCompat$Builder r1 = r8.f5428k     // Catch:{ all -> 0x0109 }
            if (r1 != 0) goto L_0x0042
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0109 }
            r3 = 26
            if (r1 < r3) goto L_0x0039
            android.support.v4.app.NotificationCompat$Builder r1 = new android.support.v4.app.NotificationCompat$Builder     // Catch:{ all -> 0x002f }
            android.content.Context r3 = r8.f5425h     // Catch:{ all -> 0x002f }
            java.lang.String r4 = "001"
            r1.<init>(r3, r4)     // Catch:{ all -> 0x002f }
            r8.f5428k = r1     // Catch:{ all -> 0x002f }
            goto L_0x0042
        L_0x002f:
            android.support.v4.app.NotificationCompat$Builder r1 = new android.support.v4.app.NotificationCompat$Builder     // Catch:{ all -> 0x0109 }
            android.content.Context r3 = r8.f5425h     // Catch:{ all -> 0x0109 }
            r1.<init>(r3)     // Catch:{ all -> 0x0109 }
            r8.f5428k = r1     // Catch:{ all -> 0x0109 }
            goto L_0x0042
        L_0x0039:
            android.support.v4.app.NotificationCompat$Builder r1 = new android.support.v4.app.NotificationCompat$Builder     // Catch:{ all -> 0x0109 }
            android.content.Context r3 = r8.f5425h     // Catch:{ all -> 0x0109 }
            r1.<init>(r3)     // Catch:{ all -> 0x0109 }
            r8.f5428k = r1     // Catch:{ all -> 0x0109 }
        L_0x0042:
            android.support.v4.app.NotificationCompat$Builder r1 = r8.f5428k     // Catch:{ all -> 0x0109 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0109 }
            r3.<init>()     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r4 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            java.lang.String r4 = r4.f5391y     // Catch:{ all -> 0x0109 }
            r3.append(r4)     // Catch:{ all -> 0x0109 }
            java.lang.String r4 = com.tencent.bugly.beta.Beta.strNotificationHaveNewVersion     // Catch:{ all -> 0x0109 }
            r3.append(r4)     // Catch:{ all -> 0x0109 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0109 }
            android.support.v4.app.NotificationCompat$Builder r1 = r1.setTicker(r3)     // Catch:{ all -> 0x0109 }
            java.lang.String r3 = "%s %s"
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r5 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            java.lang.String r5 = r5.f5391y     // Catch:{ all -> 0x0109 }
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x0109 }
            java.lang.String r5 = com.tencent.bugly.beta.Beta.strNotificationHaveNewVersion     // Catch:{ all -> 0x0109 }
            r7 = 1
            r4[r7] = r5     // Catch:{ all -> 0x0109 }
            java.lang.String r3 = java.lang.String.format(r3, r4)     // Catch:{ all -> 0x0109 }
            android.support.v4.app.NotificationCompat$Builder r1 = r1.setContentTitle(r3)     // Catch:{ all -> 0x0109 }
            android.content.Context r3 = r8.f5425h     // Catch:{ all -> 0x0109 }
            r4 = 268435456(0x10000000, float:2.5243549E-29)
            android.app.PendingIntent r10 = android.app.PendingIntent.getBroadcast(r3, r2, r10, r4)     // Catch:{ all -> 0x0109 }
            android.support.v4.app.NotificationCompat$Builder r10 = r1.setContentIntent(r10)     // Catch:{ all -> 0x0109 }
            android.support.v4.app.NotificationCompat$Builder r10 = r10.setAutoCancel(r7)     // Catch:{ all -> 0x0109 }
            java.lang.String r1 = "%s.%s"
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.proguard.v r3 = r9.f6286e     // Catch:{ all -> 0x0109 }
            java.lang.String r3 = r3.f6255d     // Catch:{ all -> 0x0109 }
            r2[r6] = r3     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.proguard.v r9 = r9.f6286e     // Catch:{ all -> 0x0109 }
            int r9 = r9.f6254c     // Catch:{ all -> 0x0109 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ all -> 0x0109 }
            r2[r7] = r9     // Catch:{ all -> 0x0109 }
            java.lang.String r9 = java.lang.String.format(r1, r2)     // Catch:{ all -> 0x0109 }
            r10.setContentText(r9)     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            int r9 = r9.f5372f     // Catch:{ all -> 0x0109 }
            if (r9 <= 0) goto L_0x00b0
            android.support.v4.app.NotificationCompat$Builder r9 = r8.f5428k     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r10 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            int r10 = r10.f5372f     // Catch:{ all -> 0x0109 }
            r9.setSmallIcon(r10)     // Catch:{ all -> 0x0109 }
            goto L_0x00cb
        L_0x00b0:
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            android.content.pm.PackageInfo r9 = r9.f5392z     // Catch:{ all -> 0x0109 }
            if (r9 == 0) goto L_0x00cb
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            android.content.pm.PackageInfo r9 = r9.f5392z     // Catch:{ all -> 0x0109 }
            android.content.pm.ApplicationInfo r9 = r9.applicationInfo     // Catch:{ all -> 0x0109 }
            if (r9 == 0) goto L_0x00cb
            android.support.v4.app.NotificationCompat$Builder r9 = r8.f5428k     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r10 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            android.content.pm.PackageInfo r10 = r10.f5392z     // Catch:{ all -> 0x0109 }
            android.content.pm.ApplicationInfo r10 = r10.applicationInfo     // Catch:{ all -> 0x0109 }
            int r10 = r10.icon     // Catch:{ all -> 0x0109 }
            r9.setSmallIcon(r10)     // Catch:{ all -> 0x0109 }
        L_0x00cb:
            com.tencent.bugly.beta.global.e r9 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            int r9 = r9.f5373g     // Catch:{ all -> 0x0109 }
            if (r9 <= 0) goto L_0x00f8
            android.content.Context r9 = r8.f5425h     // Catch:{ all -> 0x0109 }
            android.content.res.Resources r9 = r9.getResources()     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r10 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            int r10 = r10.f5373g     // Catch:{ all -> 0x0109 }
            android.graphics.drawable.Drawable r9 = r9.getDrawable(r10)     // Catch:{ all -> 0x0109 }
            if (r9 == 0) goto L_0x00f8
            android.support.v4.app.NotificationCompat$Builder r9 = r8.f5428k     // Catch:{ all -> 0x0109 }
            android.content.Context r10 = r8.f5425h     // Catch:{ all -> 0x0109 }
            android.content.res.Resources r10 = r10.getResources()     // Catch:{ all -> 0x0109 }
            com.tencent.bugly.beta.global.e r1 = com.tencent.bugly.beta.global.C2808e.f5336E     // Catch:{ all -> 0x0109 }
            int r1 = r1.f5373g     // Catch:{ all -> 0x0109 }
            android.graphics.drawable.Drawable r10 = r10.getDrawable(r1)     // Catch:{ all -> 0x0109 }
            android.graphics.Bitmap r10 = com.tencent.bugly.beta.global.C2804a.m6259a(r10)     // Catch:{ all -> 0x0109 }
            r9.setLargeIcon(r10)     // Catch:{ all -> 0x0109 }
        L_0x00f8:
            android.support.v4.app.NotificationCompat$Builder r9 = r8.f5428k     // Catch:{ all -> 0x0109 }
            android.app.Notification r9 = r9.build()     // Catch:{ all -> 0x0109 }
            r8.f5424g = r9     // Catch:{ all -> 0x0109 }
            android.app.NotificationManager r9 = r8.f5423f     // Catch:{ all -> 0x0109 }
            android.app.Notification r10 = r8.f5424g     // Catch:{ all -> 0x0109 }
            r9.notify(r0, r10)     // Catch:{ all -> 0x0109 }
            monitor-exit(r8)
            return
        L_0x0109:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.beta.p050ui.C2821c.mo34161a(com.tencent.bugly.proguard.y, com.tencent.bugly.beta.ui.b):void");
    }
}
