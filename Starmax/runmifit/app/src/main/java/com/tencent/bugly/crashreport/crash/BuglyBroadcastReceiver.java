package com.tencent.bugly.crashreport.crash;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2908aq;

/* compiled from: BUGLY */
public class BuglyBroadcastReceiver extends BroadcastReceiver {
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static BuglyBroadcastReceiver f5722d;
    /* access modifiers changed from: private */

    /* renamed from: a */
    public IntentFilter f5723a = new IntentFilter();
    /* access modifiers changed from: private */

    /* renamed from: b */
    public Context f5724b;

    /* renamed from: c */
    private String f5725c;

    /* renamed from: e */
    private boolean f5726e = true;

    public static synchronized BuglyBroadcastReceiver getInstance() {
        BuglyBroadcastReceiver buglyBroadcastReceiver;
        synchronized (BuglyBroadcastReceiver.class) {
            if (f5722d == null) {
                f5722d = new BuglyBroadcastReceiver();
            }
            buglyBroadcastReceiver = f5722d;
        }
        return buglyBroadcastReceiver;
    }

    public synchronized void addFilter(String str) {
        if (!this.f5723a.hasAction(str)) {
            this.f5723a.addAction(str);
        }
        C2903an.m6863c("add action %s", str);
    }

    public synchronized void register(Context context) {
        this.f5724b = context;
        C2908aq.m6914a(new Runnable() {
            /* class com.tencent.bugly.crashreport.crash.BuglyBroadcastReceiver.C28561 */

            public void run() {
                try {
                    C2903an.m6856a(BuglyBroadcastReceiver.f5722d.getClass(), "Register broadcast receiver of Bugly.", new Object[0]);
                    synchronized (this) {
                        BuglyBroadcastReceiver.this.f5724b.registerReceiver(BuglyBroadcastReceiver.f5722d, BuglyBroadcastReceiver.this.f5723a);
                    }
                } catch (Throwable th) {
                    th.printStackTrace();
                }
            }
        });
    }

    public synchronized void unregister(Context context) {
        try {
            C2903an.m6856a(getClass(), "Unregister broadcast receiver of Bugly.", new Object[0]);
            context.unregisterReceiver(super);
            this.f5724b = context;
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        }
    }

    public final void onReceive(Context context, Intent intent) {
        try {
            mo34343a(context, intent);
        } catch (Throwable th) {
            if (!C2903an.m6858a(th)) {
                th.printStackTrace();
            }
        }
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00c9, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00d7, code lost:
        return false;
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized boolean mo34343a(android.content.Context r12, android.content.Intent r13) {
        /*
            r11 = this;
            monitor-enter(r11)
            r0 = 0
            if (r12 == 0) goto L_0x00d6
            if (r13 == 0) goto L_0x00d6
            java.lang.String r13 = r13.getAction()     // Catch:{ all -> 0x00d3 }
            java.lang.String r1 = "android.net.conn.CONNECTIVITY_CHANGE"
            boolean r13 = r13.equals(r1)     // Catch:{ all -> 0x00d3 }
            if (r13 != 0) goto L_0x0014
            goto L_0x00d6
        L_0x0014:
            boolean r13 = r11.f5726e     // Catch:{ all -> 0x00d3 }
            r1 = 1
            if (r13 == 0) goto L_0x001d
            r11.f5726e = r0     // Catch:{ all -> 0x00d3 }
            monitor-exit(r11)
            return r1
        L_0x001d:
            android.content.Context r13 = r11.f5724b     // Catch:{ all -> 0x00d3 }
            java.lang.String r13 = com.tencent.bugly.crashreport.common.info.C2852b.m6545f(r13)     // Catch:{ all -> 0x00d3 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r2.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r3 = "is Connect BC "
            r2.append(r3)     // Catch:{ all -> 0x00d3 }
            r2.append(r13)     // Catch:{ all -> 0x00d3 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d3 }
            java.lang.Object[] r3 = new java.lang.Object[r0]     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.proguard.C2903an.m6863c(r2, r3)     // Catch:{ all -> 0x00d3 }
            java.lang.String r2 = "network %s changed to %s"
            r3 = 2
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x00d3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r4.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r5 = ""
            r4.append(r5)     // Catch:{ all -> 0x00d3 }
            java.lang.String r5 = r11.f5725c     // Catch:{ all -> 0x00d3 }
            r4.append(r5)     // Catch:{ all -> 0x00d3 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00d3 }
            r3[r0] = r4     // Catch:{ all -> 0x00d3 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d3 }
            r4.<init>()     // Catch:{ all -> 0x00d3 }
            java.lang.String r5 = ""
            r4.append(r5)     // Catch:{ all -> 0x00d3 }
            r4.append(r13)     // Catch:{ all -> 0x00d3 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00d3 }
            r3[r1] = r4     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.proguard.C2903an.m6857a(r2, r3)     // Catch:{ all -> 0x00d3 }
            if (r13 != 0) goto L_0x0070
            r12 = 0
            r11.f5725c = r12     // Catch:{ all -> 0x00d3 }
            monitor-exit(r11)
            return r1
        L_0x0070:
            java.lang.String r2 = r11.f5725c     // Catch:{ all -> 0x00d3 }
            r11.f5725c = r13     // Catch:{ all -> 0x00d3 }
            long r3 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.crashreport.common.strategy.a r5 = com.tencent.bugly.crashreport.common.strategy.C2854a.m6574a()     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.proguard.ak r6 = com.tencent.bugly.proguard.C2896ak.m6805a()     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.crashreport.common.info.a r12 = com.tencent.bugly.crashreport.common.info.C2851a.m6470a(r12)     // Catch:{ all -> 0x00d3 }
            if (r5 == 0) goto L_0x00ca
            if (r6 == 0) goto L_0x00ca
            if (r12 != 0) goto L_0x008b
            goto L_0x00ca
        L_0x008b:
            boolean r12 = r13.equals(r2)     // Catch:{ all -> 0x00d3 }
            if (r12 != 0) goto L_0x00c8
            int r12 = com.tencent.bugly.crashreport.crash.C2869c.f5821a     // Catch:{ all -> 0x00d3 }
            long r12 = r6.mo34521a(r12)     // Catch:{ all -> 0x00d3 }
            long r12 = r3 - r12
            r7 = 30000(0x7530, double:1.4822E-319)
            int r2 = (r12 > r7 ? 1 : (r12 == r7 ? 0 : -1))
            if (r2 <= 0) goto L_0x00b1
            java.lang.String r12 = "try to upload crash on network changed."
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.proguard.C2903an.m6857a(r12, r13)     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.crashreport.crash.c r12 = com.tencent.bugly.crashreport.crash.C2869c.m6653a()     // Catch:{ all -> 0x00d3 }
            if (r12 == 0) goto L_0x00b1
            r9 = 0
            r12.mo34402a(r9)     // Catch:{ all -> 0x00d3 }
        L_0x00b1:
            r12 = 1001(0x3e9, float:1.403E-42)
            long r12 = r6.mo34521a(r12)     // Catch:{ all -> 0x00d3 }
            long r3 = r3 - r12
            int r12 = (r3 > r7 ? 1 : (r3 == r7 ? 0 : -1))
            if (r12 <= 0) goto L_0x00c8
            java.lang.String r12 = "try to upload userinfo on network changed."
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.proguard.C2903an.m6857a(r12, r13)     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.crashreport.biz.a r12 = com.tencent.bugly.crashreport.biz.C2847b.f5578b     // Catch:{ all -> 0x00d3 }
            r12.mo34250b()     // Catch:{ all -> 0x00d3 }
        L_0x00c8:
            monitor-exit(r11)
            return r1
        L_0x00ca:
            java.lang.String r12 = "not inited BC not work"
            java.lang.Object[] r13 = new java.lang.Object[r0]     // Catch:{ all -> 0x00d3 }
            com.tencent.bugly.proguard.C2903an.m6864d(r12, r13)     // Catch:{ all -> 0x00d3 }
            monitor-exit(r11)
            return r1
        L_0x00d3:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        L_0x00d6:
            monitor-exit(r11)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.tencent.bugly.crashreport.crash.BuglyBroadcastReceiver.mo34343a(android.content.Context, android.content.Intent):boolean");
    }
}
