package com.tencent.bugly.proguard;

/* renamed from: com.tencent.bugly.proguard.u */
/* compiled from: BUGLY */
public final class C2955u extends C2944m implements Cloneable {

    /* renamed from: f */
    static final /* synthetic */ boolean f6245f = (!C2955u.class.desiredAssertionStatus());

    /* renamed from: a */
    public String f6246a = "";

    /* renamed from: b */
    public String f6247b = "";

    /* renamed from: c */
    public String f6248c = "";

    /* renamed from: d */
    public long f6249d = 0;

    /* renamed from: e */
    public String f6250e = "";

    /* renamed from: a */
    public String mo34682a() {
        return this.f6247b;
    }

    public C2955u() {
    }

    public C2955u(String str, String str2, String str3, long j, String str4) {
        this.f6246a = str;
        this.f6247b = str2;
        this.f6248c = str3;
        this.f6249d = j;
        this.f6250e = str4;
    }

    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        C2955u uVar = (C2955u) obj;
        if (!C2945n.m7123a(this.f6246a, uVar.f6246a) || !C2945n.m7123a(this.f6247b, uVar.f6247b) || !C2945n.m7123a(this.f6248c, uVar.f6248c) || !C2945n.m7122a(this.f6249d, uVar.f6249d) || !C2945n.m7123a(this.f6250e, uVar.f6250e)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        try {
            throw new Exception("Need define key first!");
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException unused) {
            if (f6245f) {
                return null;
            }
            throw new AssertionError();
        }
    }

    /* renamed from: a */
    public void mo34476a(C2943l lVar) {
        lVar.mo34648a(this.f6246a, 0);
        lVar.mo34648a(this.f6247b, 1);
        String str = this.f6248c;
        if (str != null) {
            lVar.mo34648a(str, 2);
        }
        lVar.mo34645a(this.f6249d, 3);
        String str2 = this.f6250e;
        if (str2 != null) {
            lVar.mo34648a(str2, 4);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String
     arg types: [int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.k$a, java.nio.ByteBuffer):int
      com.tencent.bugly.proguard.k.a(int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.proguard.k.a(long, int, boolean):long
     arg types: [long, int, int]
     candidates:
      com.tencent.bugly.proguard.k.a(byte, int, boolean):byte
      com.tencent.bugly.proguard.k.a(double, int, boolean):double
      com.tencent.bugly.proguard.k.a(float, int, boolean):float
      com.tencent.bugly.proguard.k.a(int, int, boolean):int
      com.tencent.bugly.proguard.k.a(com.tencent.bugly.proguard.m, int, boolean):com.tencent.bugly.proguard.m
      com.tencent.bugly.proguard.k.a(java.lang.Object, int, boolean):java.lang.Object
      com.tencent.bugly.proguard.k.a(java.util.Map, int, boolean):java.util.HashMap<K, V>
      com.tencent.bugly.proguard.k.a(java.util.List, int, boolean):java.util.List<T>
      com.tencent.bugly.proguard.k.a(short, int, boolean):short
      com.tencent.bugly.proguard.k.a(boolean, int, boolean):boolean
      com.tencent.bugly.proguard.k.a(byte[], int, boolean):byte[]
      com.tencent.bugly.proguard.k.a(double[], int, boolean):double[]
      com.tencent.bugly.proguard.k.a(float[], int, boolean):float[]
      com.tencent.bugly.proguard.k.a(int[], int, boolean):int[]
      com.tencent.bugly.proguard.k.a(long[], int, boolean):long[]
      com.tencent.bugly.proguard.k.a(java.lang.Object[], int, boolean):T[]
      com.tencent.bugly.proguard.k.a(short[], int, boolean):short[]
      com.tencent.bugly.proguard.k.a(boolean[], int, boolean):boolean[]
      com.tencent.bugly.proguard.k.a(long, int, boolean):long */
    /* renamed from: a */
    public void mo34475a(C2941k kVar) {
        this.f6246a = kVar.mo34621a(0, true);
        this.f6247b = kVar.mo34621a(1, true);
        this.f6248c = kVar.mo34621a(2, false);
        this.f6249d = kVar.mo34618a(this.f6249d, 3, true);
        this.f6250e = kVar.mo34621a(4, false);
    }

    /* renamed from: a */
    public void mo34477a(StringBuilder sb, int i) {
        C2939i iVar = new C2939i(sb, i);
        iVar.mo34601a(this.f6246a, "apkMd5");
        iVar.mo34601a(this.f6247b, "apkUrl");
        iVar.mo34601a(this.f6248c, "manifestMd5");
        iVar.mo34598a(this.f6249d, "fileSize");
        iVar.mo34601a(this.f6250e, "signatureMd5");
    }
}
