package com.tencent.bugly.crashreport;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.tencent.bugly.BuglyStrategy;
import com.tencent.bugly.C2797b;
import com.tencent.bugly.CrashModule;
import com.tencent.bugly.crashreport.biz.C2847b;
import com.tencent.bugly.crashreport.common.info.C2851a;
import com.tencent.bugly.crashreport.common.strategy.C2854a;
import com.tencent.bugly.crashreport.common.strategy.StrategyBean;
import com.tencent.bugly.crashreport.crash.BuglyBroadcastReceiver;
import com.tencent.bugly.crashreport.crash.C2869c;
import com.tencent.bugly.crashreport.crash.C2872d;
import com.tencent.bugly.crashreport.crash.jni.NativeCrashHandler;
import com.tencent.bugly.crashreport.crash.p051h5.C2878b;
import com.tencent.bugly.crashreport.crash.p051h5.H5JavaScriptInterface;
import com.tencent.bugly.proguard.C2891af;
import com.tencent.bugly.proguard.C2901am;
import com.tencent.bugly.proguard.C2903an;
import com.tencent.bugly.proguard.C2904ao;
import com.tencent.bugly.proguard.C2908aq;
import java.net.InetAddress;
import java.net.Proxy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import org.apache.commons.math3.geometry.VectorFormat;

/* compiled from: BUGLY */
public class CrashReport {

    /* renamed from: a */
    private static Context f5542a;

    /* compiled from: BUGLY */
    public static class CrashHandleCallback extends BuglyStrategy.C2796a {
    }

    /* compiled from: BUGLY */
    public interface WebViewInterface {
        void addJavascriptInterface(H5JavaScriptInterface h5JavaScriptInterface, String str);

        CharSequence getContentDescription();

        String getUrl();

        void loadUrl(String str);

        void setJavaScriptEnabled(boolean z);
    }

    public static void enableBugly(boolean z) {
        C2797b.f5301a = z;
    }

    public static void initCrashReport(Context context) {
        f5542a = context;
        C2797b.m6248a(CrashModule.getInstance());
        C2797b.m6244a(context);
    }

    public static void initCrashReport(Context context, UserStrategy userStrategy) {
        f5542a = context;
        C2797b.m6248a(CrashModule.getInstance());
        C2797b.m6245a(context, userStrategy);
    }

    public static void initCrashReport(Context context, String str, boolean z) {
        initCrashReport(context, str, z, null);
    }

    public static void initCrashReport(Context context, String str, boolean z, UserStrategy userStrategy) {
        if (context != null) {
            f5542a = context;
            C2797b.m6248a(CrashModule.getInstance());
            C2797b.m6246a(context, str, z, userStrategy);
        }
    }

    public static String getBuglyVersion(Context context) {
        if (context != null) {
            return C2851a.m6470a(context).mo34300c();
        }
        C2903an.m6864d("Please call with context.", new Object[0]);
        return "unknown";
    }

    public static void testJavaCrash() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not test Java crash because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2851a b = C2851a.m6471b();
            if (b != null) {
                b.mo34296b(24096);
            }
            throw new RuntimeException("This Crash create for Test! You can go to Bugly see more detail!");
        }
    }

    public static void testNativeCrash() {
        testNativeCrash(false, false, false);
    }

    public static void testNativeCrash(boolean z, boolean z2, boolean z3) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not test native crash because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2903an.m6857a("start to create a native crash for test!", new Object[0]);
            C2869c.m6653a().mo34406a(z, z2, z3);
        }
    }

    public static void testANRCrash() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not test ANR crash because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2903an.m6857a("start to create a anr crash for test!", new Object[0]);
            C2869c.m6653a().mo34416k();
        }
    }

    public static void postException(Thread thread, int i, String str, String str2, String str3, Map<String, String> map) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not post crash caught because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2872d.m6679a(thread, i, str, str2, str3, map);
        }
    }

    public static void postException(int i, String str, String str2, String str3, Map<String, String> map) {
        postException(Thread.currentThread(), i, str, str2, str3, map);
    }

    public static void postCatchedException(Throwable th) {
        postCatchedException(th, Thread.currentThread());
    }

    public static void postCatchedException(Throwable th, Thread thread) {
        postCatchedException(th, thread, false);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.crash.c.a(java.lang.Thread, java.lang.Throwable, boolean, java.lang.String, byte[], boolean):void
     arg types: [java.lang.Thread, java.lang.Throwable, int, ?[OBJECT, ARRAY], ?[OBJECT, ARRAY], boolean]
     candidates:
      com.tencent.bugly.crashreport.crash.c.a(int, android.content.Context, boolean, com.tencent.bugly.BuglyStrategy$a, com.tencent.bugly.crashreport.crash.f, java.lang.String):com.tencent.bugly.crashreport.crash.c
      com.tencent.bugly.crashreport.crash.c.a(java.lang.Thread, java.lang.Throwable, boolean, java.lang.String, byte[], boolean):void */
    public static void postCatchedException(Throwable th, Thread thread, boolean z) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not post crash caught because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else if (th == null) {
            C2903an.m6864d("throwable is null, just return", new Object[0]);
        } else {
            if (thread == null) {
                thread = Thread.currentThread();
            }
            C2869c.m6653a().mo34405a(thread, th, false, (String) null, (byte[]) null, z);
        }
    }

    public static void closeNativeReport() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not close native report because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2869c.m6653a().mo34412g();
        }
    }

    public static void startCrashReport() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not start crash report because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.w(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2869c.m6653a().mo34408c();
        }
    }

    public static void closeCrashReport() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not close crash report because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.w(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            C2869c.m6653a().mo34409d();
        }
    }

    public static void closeBugly() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not close bugly because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.w(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else if (f5542a != null) {
            BuglyBroadcastReceiver instance = BuglyBroadcastReceiver.getInstance();
            if (instance != null) {
                instance.unregister(f5542a);
            }
            closeCrashReport();
            C2847b.m6431a(f5542a);
            C2901am a = C2901am.m6848a();
            if (a != null) {
                a.mo34549b();
            }
        }
    }

    public static void setUserSceneTag(Context context, int i) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set tag caught because bugly is disable.");
        } else if (context == null) {
            Log.e(C2903an.f6030b, "setTag args context should not be null");
        } else {
            if (i <= 0) {
                C2903an.m6864d("setTag args tagId should > 0", new Object[0]);
            }
            C2851a.m6470a(context).mo34291a(i);
            C2903an.m6860b("[param] set user scene tag: %d", Integer.valueOf(i));
        }
    }

    public static int getUserSceneTagId(Context context) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get user scene tag because bugly is disable.");
            return -1;
        } else if (context != null) {
            return C2851a.m6470a(context).mo34278H();
        } else {
            Log.e(C2903an.f6030b, "getUserSceneTagId args context should not be null");
            return -1;
        }
    }

    public static String getUserData(Context context, String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get user data because bugly is disable.");
            return "unknown";
        } else if (context == null) {
            Log.e(C2903an.f6030b, "getUserDataValue args context should not be null");
            return "unknown";
        } else if (C2908aq.m6915a(str)) {
            return null;
        } else {
            return C2851a.m6470a(context).mo34310g(str);
        }
    }

    public static void putUserData(Context context, String str, String str2) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not put user data because bugly is disable.");
        } else if (context == null) {
            Log.w(C2903an.f6030b, "putUserData args context should not be null");
        } else if (str == null) {
            "" + str;
            C2903an.m6864d("putUserData args key should not be null or empty", new Object[0]);
        } else if (str2 == null) {
            "" + str2;
            C2903an.m6864d("putUserData args value should not be null", new Object[0]);
        } else if (!str.matches("[a-zA-Z[0-9]]+")) {
            C2903an.m6864d("putUserData args key should match [a-zA-Z[0-9]]+  {" + str + VectorFormat.DEFAULT_SUFFIX, new Object[0]);
        } else {
            if (str2.length() > 200) {
                C2903an.m6864d("user data value length over limit %d, it will be cutted!", 200);
                str2 = str2.substring(0, 200);
            }
            C2851a a = C2851a.m6470a(context);
            if (a.mo34275E().contains(str)) {
                NativeCrashHandler instance = NativeCrashHandler.getInstance();
                if (instance != null) {
                    instance.putKeyValueToNative(str, str2);
                }
                C2851a.m6470a(context).mo34298b(str, str2);
                C2903an.m6863c("replace KV %s %s", str, str2);
            } else if (a.mo34274D() >= 10) {
                C2903an.m6864d("user data size is over limit %d, it will be cutted!", 10);
            } else {
                if (str.length() > 50) {
                    C2903an.m6864d("user data key length over limit %d , will drop this new key %s", 50, str);
                    str = str.substring(0, 50);
                }
                NativeCrashHandler instance2 = NativeCrashHandler.getInstance();
                if (instance2 != null) {
                    instance2.putKeyValueToNative(str, str2);
                }
                C2851a.m6470a(context).mo34298b(str, str2);
                C2903an.m6860b("[param] set user data: %s - %s", str, str2);
            }
        }
    }

    public static String removeUserData(Context context, String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not remove user data because bugly is disable.");
            return "unknown";
        } else if (context == null) {
            Log.e(C2903an.f6030b, "removeUserData args context should not be null");
            return "unknown";
        } else if (C2908aq.m6915a(str)) {
            return null;
        } else {
            C2903an.m6860b("[param] remove user data: %s", str);
            return C2851a.m6470a(context).mo34308f(str);
        }
    }

    public static Set<String> getAllUserDataKeys(Context context) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get all keys of user data because bugly is disable.");
            return new HashSet();
        } else if (context != null) {
            return C2851a.m6470a(context).mo34275E();
        } else {
            Log.e(C2903an.f6030b, "getAllUserDataKeys args context should not be null");
            return new HashSet();
        }
    }

    public static int getUserDatasSize(Context context) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get size of user data because bugly is disable.");
            return -1;
        } else if (context != null) {
            return C2851a.m6470a(context).mo34274D();
        } else {
            Log.e(C2903an.f6030b, "getUserDatasSize args context should not be null");
            return -1;
        }
    }

    public static String getAppID() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get App ID because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C2851a.m6470a(f5542a).mo34307f();
        } else {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static void setUserId(String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set user ID because bugly is disable.");
        } else if (!CrashModule.getInstance().hasInitialized()) {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
        } else {
            setUserId(f5542a, str);
        }
    }

    public static void setUserId(Context context, String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set user ID because bugly is disable.");
        } else if (context == null) {
            Log.e(C2903an.f6030b, "Context should not be null when bugly has not been initialed!");
        } else if (str == null) {
            C2903an.m6864d("userId should not be null", new Object[0]);
        } else {
            if (str.length() > 100) {
                String substring = str.substring(0, 100);
                C2903an.m6864d("userId %s length is over limit %d substring to %s", str, 100, substring);
                str = substring;
            }
            if (!str.equals(C2851a.m6470a(context).mo34309g())) {
                C2851a.m6470a(context).mo34297b(str);
                C2903an.m6860b("[user] set userId : %s", str);
                NativeCrashHandler instance = NativeCrashHandler.getInstance();
                if (instance != null) {
                    instance.setNativeUserId(str);
                }
                if (CrashModule.getInstance().hasInitialized()) {
                    C2847b.m6429a();
                }
            }
        }
    }

    public static String getUserId() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get user ID because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C2851a.m6470a(f5542a).mo34309g();
        } else {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static String getAppVer() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get app version because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C2851a.m6470a(f5542a).f5671p;
        } else {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static String getAppChannel() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get App channel because bugly is disable.");
            return "unknown";
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C2851a.m6470a(f5542a).f5673r;
        } else {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return "unknown";
        }
    }

    public static void setContext(Context context) {
        f5542a = context;
    }

    public static boolean isLastSessionCrash() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "The info 'isLastSessionCrash' is not accurate because bugly is disable.");
            return false;
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C2869c.m6653a().mo34407b();
        } else {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return false;
        }
    }

    public static void setSdkExtraData(Context context, String str, String str2) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not put SDK extra data because bugly is disable.");
        } else if (context != null && !C2908aq.m6915a(str) && !C2908aq.m6915a(str2)) {
            C2851a.m6470a(context).mo34293a(str, str2);
        }
    }

    public static Map<String, String> getSdkExtraData() {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get SDK extra data because bugly is disable.");
            return new HashMap();
        } else if (CrashModule.getInstance().hasInitialized()) {
            return C2851a.m6470a(f5542a).f5607I;
        } else {
            Log.e(C2903an.f6030b, "CrashReport has not been initialed! pls to call method 'initCrashReport' first!");
            return null;
        }
    }

    public static Map<String, String> getSdkExtraData(Context context) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not get SDK extra data because bugly is disable.");
            return new HashMap();
        } else if (context != null) {
            return C2851a.m6470a(context).f5607I;
        } else {
            C2903an.m6864d("Context should not be null.", new Object[0]);
            return null;
        }
    }

    private static void putSdkData(Context context, String str, String str2) {
        if (context != null && !C2908aq.m6915a(str) && !C2908aq.m6915a(str2)) {
            String replace = str.replace("[a-zA-Z[0-9]]+", "");
            if (replace.length() > 100) {
                Log.w(C2903an.f6030b, String.format("putSdkData key length over limit %d, will be cutted.", 50));
                replace = replace.substring(0, 50);
            }
            if (str2.length() > 500) {
                Log.w(C2903an.f6030b, String.format("putSdkData value length over limit %d, will be cutted!", 200));
                str2 = str2.substring(0, 200);
            }
            C2851a.m6470a(context).mo34302c(replace, str2);
            C2903an.m6860b(String.format("[param] putSdkData data: %s - %s", replace, str2), new Object[0]);
        }
    }

    public static void setIsAppForeground(Context context, boolean z) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set 'isAppForeground' because bugly is disable.");
        } else if (context == null) {
            C2903an.m6864d("Context should not be null.", new Object[0]);
        } else {
            if (z) {
                C2903an.m6863c("App is in foreground.", new Object[0]);
            } else {
                C2903an.m6863c("App is in background.", new Object[0]);
            }
            C2851a.m6470a(context).mo34294a(z);
        }
    }

    public static void setIsDevelopmentDevice(Context context, boolean z) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set 'isDevelopmentDevice' because bugly is disable.");
        } else if (context == null) {
            C2903an.m6864d("Context should not be null.", new Object[0]);
        } else {
            if (z) {
                C2903an.m6863c("This is a development device.", new Object[0]);
            } else {
                C2903an.m6863c("This is not a development device.", new Object[0]);
            }
            C2851a.m6470a(context).f5603E = z;
        }
    }

    public static void setSessionIntervalMills(long j) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set 'SessionIntervalMills' because bugly is disable.");
        } else {
            C2847b.m6430a(j);
        }
    }

    public static void setAppVersion(Context context, String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App version because bugly is disable.");
        } else if (context == null) {
            Log.w(C2903an.f6030b, "setAppVersion args context should not be null");
        } else if (str == null) {
            Log.w(C2903an.f6030b, "App version is null, will not set");
        } else {
            C2851a.m6470a(context).f5671p = str;
            NativeCrashHandler instance = NativeCrashHandler.getInstance();
            if (instance != null) {
                instance.setNativeAppVersion(str);
            }
        }
    }

    public static void setAppChannel(Context context, String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App channel because Bugly is disable.");
        } else if (context == null) {
            Log.w(C2903an.f6030b, "setAppChannel args context should not be null");
        } else if (str == null) {
            Log.w(C2903an.f6030b, "App channel is null, will not set");
        } else {
            C2851a.m6470a(context).f5673r = str;
            NativeCrashHandler instance = NativeCrashHandler.getInstance();
            if (instance != null) {
                instance.setNativeAppChannel(str);
            }
        }
    }

    public static void setAppPackage(Context context, String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App package because bugly is disable.");
        } else if (context == null) {
            Log.w(C2903an.f6030b, "setAppPackage args context should not be null");
        } else if (str == null) {
            Log.w(C2903an.f6030b, "App package is null, will not set");
        } else {
            C2851a.m6470a(context).f5659d = str;
            NativeCrashHandler instance = NativeCrashHandler.getInstance();
            if (instance != null) {
                instance.setNativeAppPackage(str);
            }
        }
    }

    public static void setCrashFilter(String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App package because bugly is disable.");
            return;
        }
        String str2 = C2903an.f6030b;
        Log.i(str2, "Set crash stack filter: " + str);
        C2869c.f5834n = str;
    }

    public static void setCrashRegularFilter(String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App package because bugly is disable.");
            return;
        }
        String str2 = C2903an.f6030b;
        Log.i(str2, "Set crash stack filter: " + str);
        C2869c.f5835o = str;
    }

    public static void setHandleNativeCrashInJava(boolean z) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App package because bugly is disable.");
            return;
        }
        String str = C2903an.f6030b;
        Log.i(str, "Should handle native crash in Java profile after handled in native profile: " + z);
        NativeCrashHandler.setShouldHandleInJava(z);
    }

    public static void setBuglyDbName(String str) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set DB name because bugly is disable.");
            return;
        }
        String str2 = C2903an.f6030b;
        Log.i(str2, "Set Bugly DB name: " + str);
        C2891af.f5960a = str;
    }

    public static void enableObtainId(Context context, boolean z) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set DB name because bugly is disable.");
        } else if (context == null) {
            Log.w(C2903an.f6030b, "enableObtainId args context should not be null");
        } else {
            String str = C2903an.f6030b;
            Log.i(str, "Enable identification obtaining? " + z);
            C2851a.m6470a(context).mo34299b(z);
        }
    }

    public static void setAuditEnable(Context context, boolean z) {
        if (!C2797b.f5301a) {
            Log.w(C2903an.f6030b, "Can not set App package because bugly is disable.");
        } else if (context == null) {
            Log.w(C2903an.f6030b, "setAppPackage args context should not be null");
        } else {
            String str = C2903an.f6030b;
            Log.i(str, "Set audit enable: " + z);
            C2851a.m6470a(context).f5608J = z;
        }
    }

    public static void setServerUrl(String str) {
        if (C2908aq.m6915a(str) || !C2908aq.m6940c(str)) {
            Log.i(C2903an.f6030b, "URL is invalid.");
            return;
        }
        C2854a.m6576a(str);
        StrategyBean.f5688b = str;
        StrategyBean.f5689c = str;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(android.webkit.WebView, boolean, boolean):boolean
     arg types: [android.webkit.WebView, boolean, int]
     candidates:
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, boolean):boolean
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(android.webkit.WebView, boolean, boolean):boolean */
    public static boolean setJavascriptMonitor(WebView webView, boolean z) {
        return setJavascriptMonitor(webView, z, false);
    }

    public static boolean setJavascriptMonitor(final WebView webView, boolean z, boolean z2) {
        if (webView != null) {
            return setJavascriptMonitor(new WebViewInterface() {
                /* class com.tencent.bugly.crashreport.CrashReport.C28381 */

                public String getUrl() {
                    return webView.getUrl();
                }

                public void setJavaScriptEnabled(boolean z) {
                    WebSettings settings = webView.getSettings();
                    if (!settings.getJavaScriptEnabled()) {
                        settings.setJavaScriptEnabled(true);
                    }
                }

                public void loadUrl(String str) {
                    webView.loadUrl(str);
                }

                public void addJavascriptInterface(H5JavaScriptInterface h5JavaScriptInterface, String str) {
                    webView.addJavascriptInterface(h5JavaScriptInterface, str);
                }

                public CharSequence getContentDescription() {
                    return webView.getContentDescription();
                }
            }, z, z2);
        }
        Log.w(C2903an.f6030b, "WebView is null.");
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, boolean):boolean
     arg types: [com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, int]
     candidates:
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(android.webkit.WebView, boolean, boolean):boolean
      com.tencent.bugly.crashreport.CrashReport.setJavascriptMonitor(com.tencent.bugly.crashreport.CrashReport$WebViewInterface, boolean, boolean):boolean */
    public static boolean setJavascriptMonitor(WebViewInterface webViewInterface, boolean z) {
        return setJavascriptMonitor(webViewInterface, z, false);
    }

    public static boolean setJavascriptMonitor(WebViewInterface webViewInterface, boolean z, boolean z2) {
        if (webViewInterface == null) {
            Log.w(C2903an.f6030b, "WebViewInterface is null.");
            return false;
        } else if (!CrashModule.getInstance().hasInitialized()) {
            C2903an.m6865e("CrashReport has not been initialed! please to call method 'initCrashReport' first!", new Object[0]);
            return false;
        } else {
            C2903an.m6857a("Set Javascript exception monitor of webview.", new Object[0]);
            if (!C2797b.f5301a) {
                Log.w(C2903an.f6030b, "Can not set JavaScript monitor because bugly is disable.");
                return false;
            }
            C2903an.m6863c("URL of webview is %s", webViewInterface.getUrl());
            if (z2 || Build.VERSION.SDK_INT >= 19) {
                C2903an.m6857a("Enable the javascript needed by webview monitor.", new Object[0]);
                webViewInterface.setJavaScriptEnabled(true);
                H5JavaScriptInterface instance = H5JavaScriptInterface.getInstance(webViewInterface);
                if (instance != null) {
                    C2903an.m6857a("Add a secure javascript interface to the webview.", new Object[0]);
                    webViewInterface.addJavascriptInterface(instance, "exceptionUploader");
                }
                if (z) {
                    C2903an.m6857a("Inject bugly.js(v%s) to the webview.", C2878b.m6704b());
                    String a = C2878b.m6703a();
                    if (a == null) {
                        C2903an.m6865e("Failed to inject Bugly.js.", C2878b.m6704b());
                        return false;
                    }
                    webViewInterface.loadUrl("javascript:" + a);
                }
                return true;
            }
            C2903an.m6865e("This interface is only available for Android 4.4 or later.", new Object[0]);
            return false;
        }
    }

    public static void setHttpProxy(String str, int i) {
        C2904ao.m6868a(str, i);
    }

    public static void setHttpProxy(InetAddress inetAddress, int i) {
        C2904ao.m6869a(inetAddress, i);
    }

    public static Proxy getHttpProxy() {
        return C2904ao.m6867a();
    }

    /* compiled from: BUGLY */
    public static class UserStrategy extends BuglyStrategy {

        /* renamed from: a */
        CrashHandleCallback f5544a;

        public UserStrategy(Context context) {
        }

        public synchronized CrashHandleCallback getCrashHandleCallback() {
            return this.f5544a;
        }

        public synchronized void setCrashHandleCallback(CrashHandleCallback crashHandleCallback) {
            this.f5544a = crashHandleCallback;
        }
    }
}
