package com.tencent.bugly.crashreport.common.info;

import android.os.Parcel;
import android.os.Parcelable;

/* compiled from: BUGLY */
public class PlugInBean implements Parcelable {
    public static final Parcelable.Creator<PlugInBean> CREATOR = new Parcelable.Creator<PlugInBean>() {
        /* class com.tencent.bugly.crashreport.common.info.PlugInBean.C28501 */

        /* renamed from: a */
        public PlugInBean createFromParcel(Parcel parcel) {
            return new PlugInBean(parcel);
        }

        /* renamed from: a */
        public PlugInBean[] newArray(int i) {
            return new PlugInBean[i];
        }
    };

    /* renamed from: a */
    public final String f5595a;

    /* renamed from: b */
    public final String f5596b;

    /* renamed from: c */
    public final String f5597c;

    public int describeContents() {
        return 0;
    }

    public PlugInBean(String str, String str2, String str3) {
        this.f5595a = str;
        this.f5596b = str2;
        this.f5597c = str3;
    }

    public String toString() {
        return "plid:" + this.f5595a + " plV:" + this.f5596b + " plUUID:" + this.f5597c;
    }

    public PlugInBean(Parcel parcel) {
        this.f5595a = parcel.readString();
        this.f5596b = parcel.readString();
        this.f5597c = parcel.readString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f5595a);
        parcel.writeString(this.f5596b);
        parcel.writeString(this.f5597c);
    }
}
