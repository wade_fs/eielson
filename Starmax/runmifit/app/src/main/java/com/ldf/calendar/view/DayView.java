package com.ldf.calendar.view;

import android.content.Context;
import android.graphics.Canvas;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import com.ldf.calendar.Utils;
import com.ldf.calendar.interf.IDayRenderer;

public abstract class DayView extends RelativeLayout implements IDayRenderer {
    protected Context context;
    protected Day day;
    protected int layoutResource;

    public DayView(Context context2, int i) {
        super(context2);
        setupLayoutResource(i);
        this.context = context2;
        this.layoutResource = i;
    }

    private void setupLayoutResource(int i) {
        View inflate = LayoutInflater.from(getContext()).inflate(i, this);
        inflate.measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        inflate.layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
    }

    public void refreshContent() {
        measure(View.MeasureSpec.makeMeasureSpec(0, 0), View.MeasureSpec.makeMeasureSpec(0, 0));
        layout(0, 0, getMeasuredWidth(), getMeasuredHeight());
    }

    public void drawDay(Canvas canvas, Day day2) {
        this.day = day2;
        refreshContent();
        int save = canvas.save();
        canvas.translate((float) getTranslateX(canvas, day2), (float) (day2.getPosRow() * getMeasuredHeight()));
        draw(canvas);
        canvas.restoreToCount(save);
    }

    private int getTranslateX(Canvas canvas, Day day2) {
        int width = canvas.getWidth() / 7;
        return (day2.getPosCol() * width) + ((width - getMeasuredWidth()) / 2);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        Utils.cleanMarkData();
    }
}
