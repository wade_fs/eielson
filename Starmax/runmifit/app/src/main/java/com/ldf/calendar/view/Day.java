package com.ldf.calendar.view;

import android.os.Parcel;
import android.os.Parcelable;
import com.ldf.calendar.component.State;
import com.ldf.calendar.model.CalendarDate;

public class Day implements Parcelable {
    public static final Parcelable.Creator<Day> CREATOR = new Parcelable.Creator<Day>() {
        /* class com.ldf.calendar.view.Day.C21561 */

        public Day createFromParcel(Parcel parcel) {
            return new Day(parcel);
        }

        public Day[] newArray(int i) {
            return new Day[i];
        }
    };
    private CalendarDate date;
    private int posCol;
    private int posRow;
    private State state;

    public int describeContents() {
        return 0;
    }

    public Day(State state2, CalendarDate calendarDate, int i, int i2) {
        this.state = state2;
        this.date = calendarDate;
        this.posRow = i;
        this.posCol = i2;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state2) {
        this.state = state2;
    }

    public CalendarDate getDate() {
        return this.date;
    }

    public void setDate(CalendarDate calendarDate) {
        this.date = calendarDate;
    }

    public int getPosRow() {
        return this.posRow;
    }

    public void setPosRow(int i) {
        this.posRow = i;
    }

    public int getPosCol() {
        return this.posCol;
    }

    public void setPosCol(int i) {
        this.posCol = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        State state2 = this.state;
        parcel.writeInt(state2 == null ? -1 : state2.ordinal());
        parcel.writeSerializable(this.date);
        parcel.writeInt(this.posRow);
        parcel.writeInt(this.posCol);
    }

    protected Day(Parcel parcel) {
        State state2;
        int readInt = parcel.readInt();
        if (readInt == -1) {
            state2 = null;
        } else {
            state2 = State.values()[readInt];
        }
        this.state = state2;
        this.date = (CalendarDate) parcel.readSerializable();
        this.posRow = parcel.readInt();
        this.posCol = parcel.readInt();
    }
}
