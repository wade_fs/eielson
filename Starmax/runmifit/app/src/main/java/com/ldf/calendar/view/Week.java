package com.ldf.calendar.view;

public class Week {
    public Day[] days = new Day[7];
    public int row;

    public Week(int i) {
        this.row = i;
    }

    public int getRow() {
        return this.row;
    }

    public void setRow(int i) {
        this.row = i;
    }

    public Day[] getDays() {
        return this.days;
    }

    public void setDays(Day[] dayArr) {
        this.days = dayArr;
    }
}
