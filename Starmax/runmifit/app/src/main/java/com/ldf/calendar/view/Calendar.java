package com.ldf.calendar.view;

import android.content.Context;
import android.graphics.Canvas;
import android.view.MotionEvent;
import android.view.View;
import com.ldf.calendar.Utils;
import com.ldf.calendar.component.CalendarAttr;
import com.ldf.calendar.component.CalendarRenderer;
import com.ldf.calendar.interf.IDayRenderer;
import com.ldf.calendar.interf.OnAdapterSelectListener;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;

public class Calendar extends View {
    private CalendarAttr calendarAttr;
    private CalendarAttr.CalendarType calendarType;
    private int cellHeight;
    private int cellWidth;
    private Context context;
    private OnAdapterSelectListener onAdapterSelectListener;
    private OnSelectDateListener onSelectDateListener;
    private float posX = 0.0f;
    private float posY = 0.0f;
    private CalendarRenderer renderer;
    private float touchSlop;

    public Calendar(Context context2, OnSelectDateListener onSelectDateListener2, CalendarAttr calendarAttr2) {
        super(context2);
        this.onSelectDateListener = onSelectDateListener2;
        this.calendarAttr = calendarAttr2;
        init(context2);
    }

    private void init(Context context2) {
        this.context = context2;
        this.touchSlop = (float) Utils.getTouchSlop(context2);
        initAttrAndRenderer();
    }

    private void initAttrAndRenderer() {
        this.renderer = new CalendarRenderer(this, this.calendarAttr, this.context);
        this.renderer.setOnSelectDateListener(this.onSelectDateListener);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.renderer.draw(canvas);
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i, int i2, int i3, int i4) {
        super.onSizeChanged(i, i2, i3, i4);
        this.cellHeight = i2 / 5;
        this.cellWidth = i / 7;
        this.calendarAttr.setCellHeight(this.cellHeight);
        this.calendarAttr.setCellWidth(this.cellWidth);
        this.renderer.setAttr(this.calendarAttr);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            this.posX = motionEvent.getX();
            this.posY = motionEvent.getY();
        } else if (action == 1) {
            float x = motionEvent.getX() - this.posX;
            float y = motionEvent.getY() - this.posY;
            if (Math.abs(x) < this.touchSlop && Math.abs(y) < this.touchSlop) {
                this.onAdapterSelectListener.cancelSelectState();
                this.renderer.onClickDate((int) (this.posX / ((float) this.cellWidth)), (int) (this.posY / ((float) this.cellHeight)));
                this.onAdapterSelectListener.updateSelectState();
                invalidate();
            }
        }
        return true;
    }

    public CalendarAttr.CalendarType getCalendarType() {
        return this.calendarAttr.getCalendarType();
    }

    public void switchCalendarType(CalendarAttr.CalendarType calendarType2) {
        this.calendarAttr.setCalendarType(calendarType2);
        this.renderer.setAttr(this.calendarAttr);
    }

    public int getCellHeight() {
        return this.cellHeight;
    }

    public void resetSelectedRowIndex() {
        this.renderer.resetSelectedRowIndex();
    }

    public int getSelectedRowIndex() {
        return this.renderer.getSelectedRowIndex();
    }

    public void setSelectedRowIndex(int i) {
        this.renderer.setSelectedRowIndex(i);
    }

    public void setOnAdapterSelectListener(OnAdapterSelectListener onAdapterSelectListener2) {
        this.onAdapterSelectListener = onAdapterSelectListener2;
    }

    public void showDate(CalendarDate calendarDate) {
        this.renderer.showDate(calendarDate);
    }

    public void updateWeek(int i) {
        this.renderer.updateWeek(i);
        invalidate();
    }

    public void update() {
        this.renderer.update();
    }

    public void cancelSelectState() {
        this.renderer.cancelSelectState();
    }

    public CalendarDate getSeedDate() {
        return this.renderer.getSeedDate();
    }

    public CalendarDate getFirstDate() {
        return this.renderer.getFirstDate();
    }

    public CalendarDate getLastDate() {
        return this.renderer.getLastDate();
    }

    public void setDayRenderer(IDayRenderer iDayRenderer) {
        this.renderer.setDayRenderer(iDayRenderer);
    }
}
