package com.ldf.calendar.component;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import com.ldf.calendar.Utils;
import com.ldf.calendar.component.CalendarAttr;
import com.ldf.calendar.interf.IDayRenderer;
import com.ldf.calendar.interf.OnAdapterSelectListener;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.ldf.calendar.view.Calendar;
import com.ldf.calendar.view.MonthPager;
import java.util.ArrayList;
import java.util.HashMap;

public class CalendarViewAdapter extends PagerAdapter {
    private static CalendarDate date = new CalendarDate();
    private CalendarAttr.CalendarType calendarType = CalendarAttr.CalendarType.MONTH;
    private ArrayList<Calendar> calendars = new ArrayList<>();
    private int currentPosition = MonthPager.CURRENT_DAY_INDEX;
    private OnCalendarTypeChanged onCalendarTypeChangedListener;
    private int rowCount = 0;
    private CalendarDate seedDate;
    private CalendarAttr.WeekArrayType weekArrayType = CalendarAttr.WeekArrayType.Monday;

    public interface OnCalendarTypeChanged {
        void onCalendarTypeChanged(CalendarAttr.CalendarType calendarType);
    }

    public int getCount() {
        return Integer.MAX_VALUE;
    }

    public int getItemPosition(Object obj) {
        return -2;
    }

    public CalendarViewAdapter(Context context, OnSelectDateListener onSelectDateListener, CalendarAttr.WeekArrayType weekArrayType2, IDayRenderer iDayRenderer) {
        this.weekArrayType = weekArrayType2;
        init(context, onSelectDateListener);
        setCustomDayRenderer(iDayRenderer);
    }

    public static void saveSelectedDate(CalendarDate calendarDate) {
        date = calendarDate;
    }

    public static CalendarDate loadSelectedDate() {
        return date;
    }

    private void init(Context context, OnSelectDateListener onSelectDateListener) {
        saveSelectedDate(new CalendarDate());
        this.seedDate = new CalendarDate();
        for (int i = 0; i < 3; i++) {
            CalendarAttr calendarAttr = new CalendarAttr();
            calendarAttr.setCalendarType(CalendarAttr.CalendarType.MONTH);
            calendarAttr.setWeekArrayType(this.weekArrayType);
            Calendar calendar = new Calendar(context, onSelectDateListener, calendarAttr);
            calendar.setOnAdapterSelectListener(new OnAdapterSelectListener() {
                /* class com.ldf.calendar.component.CalendarViewAdapter.C21551 */

                public void cancelSelectState() {
                    CalendarViewAdapter.this.cancelOtherSelectState();
                }

                public void updateSelectState() {
                    CalendarViewAdapter.this.invalidateCurrentCalendar();
                }
            });
            this.calendars.add(calendar);
        }
    }

    public void setPrimaryItem(ViewGroup viewGroup, int i, Object obj) {
        Log.e("ldf", "setPrimaryItem");
        super.setPrimaryItem(viewGroup, i, obj);
        this.currentPosition = i;
    }

    public Object instantiateItem(ViewGroup viewGroup, int i) {
        Log.e("ldf", "instantiateItem");
        if (i < 2) {
            return null;
        }
        ArrayList<Calendar> arrayList = this.calendars;
        Calendar calendar = arrayList.get(i % arrayList.size());
        if (this.calendarType == CalendarAttr.CalendarType.MONTH) {
            CalendarDate modifyMonth = this.seedDate.modifyMonth(i - MonthPager.CURRENT_DAY_INDEX);
            modifyMonth.setDay(1);
            calendar.showDate(modifyMonth);
        } else {
            CalendarDate modifyWeek = this.seedDate.modifyWeek(i - MonthPager.CURRENT_DAY_INDEX);
            if (this.weekArrayType == CalendarAttr.WeekArrayType.Sunday) {
                calendar.showDate(Utils.getSaturday(modifyWeek));
            } else {
                calendar.showDate(Utils.getSunday(modifyWeek));
            }
            calendar.updateWeek(this.rowCount);
        }
        if (viewGroup.getChildCount() == this.calendars.size()) {
            viewGroup.removeView(this.calendars.get(i % 3));
        }
        if (viewGroup.getChildCount() < this.calendars.size()) {
            viewGroup.addView(calendar, 0);
        } else {
            viewGroup.addView(calendar, i % 3);
        }
        return calendar;
    }

    public boolean isViewFromObject(View view, Object obj) {
        return view == ((View) obj);
    }

    public void destroyItem(ViewGroup viewGroup, int i, Object obj) {
        viewGroup.removeView(viewGroup);
    }

    public ArrayList<Calendar> getPagers() {
        return this.calendars;
    }

    public CalendarDate getFirstVisibleDate() {
        return this.calendars.get(this.currentPosition % 3).getFirstDate();
    }

    public CalendarDate getLastVisibleDate() {
        return this.calendars.get(this.currentPosition % 3).getLastDate();
    }

    public void cancelOtherSelectState() {
        for (int i = 0; i < this.calendars.size(); i++) {
            this.calendars.get(i).cancelSelectState();
        }
    }

    public void invalidateCurrentCalendar() {
        for (int i = 0; i < this.calendars.size(); i++) {
            Calendar calendar = this.calendars.get(i);
            calendar.update();
            if (calendar.getCalendarType() == CalendarAttr.CalendarType.WEEK) {
                calendar.updateWeek(this.rowCount);
            }
        }
    }

    public void setMarkData(HashMap<String, String> hashMap) {
        Utils.setMarkData(hashMap);
        notifyDataChanged();
    }

    public void switchToMonth() {
        ArrayList<Calendar> arrayList = this.calendars;
        if (arrayList != null && arrayList.size() > 0 && this.calendarType != CalendarAttr.CalendarType.MONTH) {
            OnCalendarTypeChanged onCalendarTypeChanged = this.onCalendarTypeChangedListener;
            if (onCalendarTypeChanged != null) {
                onCalendarTypeChanged.onCalendarTypeChanged(CalendarAttr.CalendarType.MONTH);
            }
            this.calendarType = CalendarAttr.CalendarType.MONTH;
            int i = this.currentPosition;
            MonthPager.CURRENT_DAY_INDEX = i;
            this.seedDate = this.calendars.get(i % 3).getSeedDate();
            Calendar calendar = this.calendars.get(this.currentPosition % 3);
            calendar.switchCalendarType(CalendarAttr.CalendarType.MONTH);
            calendar.showDate(this.seedDate);
            Calendar calendar2 = this.calendars.get((this.currentPosition - 1) % 3);
            calendar2.switchCalendarType(CalendarAttr.CalendarType.MONTH);
            CalendarDate modifyMonth = this.seedDate.modifyMonth(-1);
            modifyMonth.setDay(1);
            calendar2.showDate(modifyMonth);
            Calendar calendar3 = this.calendars.get((this.currentPosition + 1) % 3);
            calendar3.switchCalendarType(CalendarAttr.CalendarType.MONTH);
            CalendarDate modifyMonth2 = this.seedDate.modifyMonth(1);
            modifyMonth2.setDay(1);
            calendar3.showDate(modifyMonth2);
        }
    }

    public void switchToWeek(int i) {
        this.rowCount = i;
        ArrayList<Calendar> arrayList = this.calendars;
        if (arrayList != null && arrayList.size() > 0 && this.calendarType != CalendarAttr.CalendarType.WEEK) {
            OnCalendarTypeChanged onCalendarTypeChanged = this.onCalendarTypeChangedListener;
            if (onCalendarTypeChanged != null) {
                onCalendarTypeChanged.onCalendarTypeChanged(CalendarAttr.CalendarType.WEEK);
            }
            this.calendarType = CalendarAttr.CalendarType.WEEK;
            int i2 = this.currentPosition;
            MonthPager.CURRENT_DAY_INDEX = i2;
            Calendar calendar = this.calendars.get(i2 % 3);
            this.seedDate = calendar.getSeedDate();
            this.rowCount = calendar.getSelectedRowIndex();
            Calendar calendar2 = this.calendars.get(this.currentPosition % 3);
            calendar2.switchCalendarType(CalendarAttr.CalendarType.WEEK);
            calendar2.showDate(this.seedDate);
            calendar2.updateWeek(i);
            Calendar calendar3 = this.calendars.get((this.currentPosition - 1) % 3);
            calendar3.switchCalendarType(CalendarAttr.CalendarType.WEEK);
            CalendarDate modifyWeek = this.seedDate.modifyWeek(-1);
            if (this.weekArrayType == CalendarAttr.WeekArrayType.Sunday) {
                calendar3.showDate(Utils.getSaturday(modifyWeek));
            } else {
                calendar3.showDate(Utils.getSunday(modifyWeek));
            }
            calendar3.updateWeek(i);
            Calendar calendar4 = this.calendars.get((this.currentPosition + 1) % 3);
            calendar4.switchCalendarType(CalendarAttr.CalendarType.WEEK);
            CalendarDate modifyWeek2 = this.seedDate.modifyWeek(1);
            if (this.weekArrayType == CalendarAttr.WeekArrayType.Sunday) {
                calendar4.showDate(Utils.getSaturday(modifyWeek2));
            } else {
                calendar4.showDate(Utils.getSunday(modifyWeek2));
            }
            calendar4.updateWeek(i);
        }
    }

    public void notifyMonthDataChanged(CalendarDate calendarDate) {
        this.seedDate = calendarDate;
        refreshCalendar();
    }

    public void notifyDataChanged(CalendarDate calendarDate) {
        this.seedDate = calendarDate;
        saveSelectedDate(calendarDate);
        refreshCalendar();
    }

    public void notifyDataChanged() {
        refreshCalendar();
    }

    private void refreshCalendar() {
        if (this.calendarType == CalendarAttr.CalendarType.WEEK) {
            int i = this.currentPosition;
            MonthPager.CURRENT_DAY_INDEX = i;
            Calendar calendar = this.calendars.get(i % 3);
            calendar.showDate(this.seedDate);
            calendar.updateWeek(this.rowCount);
            Calendar calendar2 = this.calendars.get((this.currentPosition - 1) % 3);
            CalendarDate modifyWeek = this.seedDate.modifyWeek(-1);
            if (this.weekArrayType == CalendarAttr.WeekArrayType.Sunday) {
                calendar2.showDate(Utils.getSaturday(modifyWeek));
            } else {
                calendar2.showDate(Utils.getSunday(modifyWeek));
            }
            calendar2.updateWeek(this.rowCount);
            Calendar calendar3 = this.calendars.get((this.currentPosition + 1) % 3);
            CalendarDate modifyWeek2 = this.seedDate.modifyWeek(1);
            if (this.weekArrayType == CalendarAttr.WeekArrayType.Sunday) {
                calendar3.showDate(Utils.getSaturday(modifyWeek2));
            } else {
                calendar3.showDate(Utils.getSunday(modifyWeek2));
            }
            calendar3.updateWeek(this.rowCount);
            return;
        }
        int i2 = this.currentPosition;
        MonthPager.CURRENT_DAY_INDEX = i2;
        this.calendars.get(i2 % 3).showDate(this.seedDate);
        CalendarDate modifyMonth = this.seedDate.modifyMonth(-1);
        modifyMonth.setDay(1);
        this.calendars.get((this.currentPosition - 1) % 3).showDate(modifyMonth);
        CalendarDate modifyMonth2 = this.seedDate.modifyMonth(1);
        modifyMonth2.setDay(1);
        this.calendars.get((this.currentPosition + 1) % 3).showDate(modifyMonth2);
    }

    public CalendarAttr.CalendarType getCalendarType() {
        return this.calendarType;
    }

    public void setCustomDayRenderer(IDayRenderer iDayRenderer) {
        this.calendars.get(0).setDayRenderer(iDayRenderer);
        this.calendars.get(1).setDayRenderer(iDayRenderer.copy());
        this.calendars.get(2).setDayRenderer(iDayRenderer.copy());
    }

    public void setOnCalendarTypeChangedListener(OnCalendarTypeChanged onCalendarTypeChanged) {
        this.onCalendarTypeChangedListener = onCalendarTypeChanged;
    }

    public CalendarAttr.WeekArrayType getWeekArrayType() {
        return this.weekArrayType;
    }
}
