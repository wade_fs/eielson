package com.ldf.calendar.behavior;

import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.ldf.calendar.Utils;
import com.ldf.calendar.component.CalendarViewAdapter;
import com.ldf.calendar.view.MonthPager;

public class MonthPagerBehavior extends CoordinatorLayout.Behavior<MonthPager> {
    private int dependentViewTop = -1;
    private boolean directionUpa;
    private float downX;
    private float downY;
    private boolean isVerticalScroll;
    private float lastTop;
    private float lastY;
    private int offsetY = 0;
    private int top = 0;
    private int touchSlop = 1;

    public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, MonthPager monthPager, View view) {
        return view instanceof RecyclerView;
    }

    public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, MonthPager monthPager, int i) {
        coordinatorLayout.onLayoutChild(monthPager, i);
        monthPager.offsetTopAndBottom(this.top);
        return true;
    }

    public boolean onTouchEvent(CoordinatorLayout coordinatorLayout, MonthPager monthPager, MotionEvent motionEvent) {
        int action;
        if (this.downY <= this.lastTop && (action = motionEvent.getAction()) != 0) {
            if (action != 1) {
                if (action == 2 && this.isVerticalScroll) {
                    if (motionEvent.getY() > this.lastY) {
                        Utils.setScrollToBottom(true);
                        this.directionUpa = false;
                    } else {
                        Utils.setScrollToBottom(false);
                        this.directionUpa = true;
                    }
                    if (this.lastTop < ((float) ((monthPager.getViewHeight() / 2) + (monthPager.getCellHeight() / 2)))) {
                        if (motionEvent.getY() - this.downY <= 0.0f || Utils.loadTop() >= monthPager.getViewHeight()) {
                            this.lastY = motionEvent.getY();
                            return true;
                        } else if ((motionEvent.getY() - this.downY) + ((float) monthPager.getCellHeight()) >= ((float) monthPager.getViewHeight())) {
                            saveTop(monthPager.getViewHeight());
                            Utils.scrollTo(coordinatorLayout, (RecyclerView) coordinatorLayout.getChildAt(1), monthPager.getViewHeight(), 10);
                            this.isVerticalScroll = false;
                        } else {
                            saveTop((int) (((float) monthPager.getCellHeight()) + (motionEvent.getY() - this.downY)));
                            Utils.scroll(coordinatorLayout.getChildAt(1), (int) (this.lastY - motionEvent.getY()), monthPager.getCellHeight(), monthPager.getViewHeight());
                        }
                    } else if (motionEvent.getY() - this.downY >= 0.0f || Utils.loadTop() <= monthPager.getCellHeight()) {
                        this.lastY = motionEvent.getY();
                        return true;
                    } else if ((motionEvent.getY() - this.downY) + ((float) monthPager.getViewHeight()) <= ((float) monthPager.getCellHeight())) {
                        saveTop(monthPager.getCellHeight());
                        Utils.scrollTo(coordinatorLayout, (RecyclerView) coordinatorLayout.getChildAt(1), monthPager.getCellHeight(), 10);
                        this.isVerticalScroll = false;
                    } else {
                        saveTop((int) (((float) monthPager.getViewHeight()) + (motionEvent.getY() - this.downY)));
                        Utils.scroll(coordinatorLayout.getChildAt(1), (int) (this.lastY - motionEvent.getY()), monthPager.getCellHeight(), monthPager.getViewHeight());
                    }
                    this.lastY = motionEvent.getY();
                    return true;
                }
            } else if (this.isVerticalScroll) {
                monthPager.setScrollable(true);
                CalendarViewAdapter calendarViewAdapter = (CalendarViewAdapter) monthPager.getAdapter();
                if (calendarViewAdapter != null) {
                    if (this.directionUpa) {
                        Utils.setScrollToBottom(true);
                        calendarViewAdapter.switchToWeek(monthPager.getRowIndex());
                        Utils.scrollTo(coordinatorLayout, (RecyclerView) coordinatorLayout.getChildAt(1), monthPager.getCellHeight(), 300);
                    } else {
                        Utils.setScrollToBottom(false);
                        calendarViewAdapter.switchToMonth();
                        Utils.scrollTo(coordinatorLayout, (RecyclerView) coordinatorLayout.getChildAt(1), monthPager.getViewHeight(), 300);
                    }
                }
                this.isVerticalScroll = false;
                return true;
            }
        }
        return false;
    }

    private void saveTop(int i) {
        Utils.saveTop(i);
    }

    public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, MonthPager monthPager, MotionEvent motionEvent) {
        int action = motionEvent.getAction();
        if (action == 0) {
            this.downX = motionEvent.getX();
            this.downY = motionEvent.getY();
            this.lastTop = (float) Utils.loadTop();
            this.lastY = this.downY;
        } else if (action != 1) {
            if (action == 2) {
                if (this.downY > this.lastTop) {
                    return false;
                }
                if (Math.abs(motionEvent.getY() - this.downY) > 25.0f && Math.abs(motionEvent.getX() - this.downX) <= 25.0f && !this.isVerticalScroll) {
                    this.isVerticalScroll = true;
                    return true;
                }
            }
        } else if (this.isVerticalScroll) {
            this.isVerticalScroll = false;
            return true;
        }
        return this.isVerticalScroll;
    }

    public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, MonthPager monthPager, View view) {
        int i;
        int i2;
        CalendarViewAdapter calendarViewAdapter = (CalendarViewAdapter) monthPager.getAdapter();
        if (this.dependentViewTop != -1) {
            int top2 = view.getTop() - this.dependentViewTop;
            int top3 = monthPager.getTop();
            int i3 = this.touchSlop;
            if (top2 > i3) {
                calendarViewAdapter.switchToMonth();
            } else if (top2 < (-i3)) {
                calendarViewAdapter.switchToWeek(monthPager.getRowIndex());
            }
            int i4 = -top3;
            if (top2 > i4) {
                top2 = i4;
            }
            if (top2 < i4 - monthPager.getTopMovableDistance()) {
                top2 = i4 - monthPager.getTopMovableDistance();
            }
            monthPager.offsetTopAndBottom(top2);
            Log.e("ldf", "onDependentViewChanged = " + top2);
        }
        this.dependentViewTop = view.getTop();
        this.top = monthPager.getTop();
        if (this.offsetY > monthPager.getCellHeight()) {
            calendarViewAdapter.switchToMonth();
        }
        if (this.offsetY < (-monthPager.getCellHeight())) {
            calendarViewAdapter.switchToWeek(monthPager.getRowIndex());
        }
        if (this.dependentViewTop > monthPager.getCellHeight() - 24 && this.dependentViewTop < monthPager.getCellHeight() + 24 && this.top > (-this.touchSlop) - monthPager.getTopMovableDistance() && this.top < this.touchSlop - monthPager.getTopMovableDistance()) {
            Utils.setScrollToBottom(true);
            calendarViewAdapter.switchToWeek(monthPager.getRowIndex());
            this.offsetY = 0;
        }
        if (this.dependentViewTop > monthPager.getViewHeight() - 24 && this.dependentViewTop < monthPager.getViewHeight() + 24 && (i = this.top) < (i2 = this.touchSlop) && i > (-i2)) {
            Utils.setScrollToBottom(false);
            calendarViewAdapter.switchToMonth();
            this.offsetY = 0;
        }
        return true;
    }
}
