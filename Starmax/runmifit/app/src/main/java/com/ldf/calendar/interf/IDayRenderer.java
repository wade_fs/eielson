package com.ldf.calendar.interf;

import android.graphics.Canvas;
import com.ldf.calendar.view.Day;

public interface IDayRenderer {
    IDayRenderer copy();

    void drawDay(Canvas canvas, Day day);

    void refreshContent();
}
