package com.ldf.calendar.interf;

import com.ldf.calendar.model.CalendarDate;

public interface OnSelectDateListener {
    void onSelectDate(CalendarDate calendarDate);

    void onSelectOtherMonth(int i);
}
