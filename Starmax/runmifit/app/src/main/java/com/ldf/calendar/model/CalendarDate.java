package com.ldf.calendar.model;

import android.util.Log;
import com.ldf.calendar.Utils;
import java.io.Serializable;
import java.util.Calendar;

public class CalendarDate implements Serializable {
    private static final long serialVersionUID = 1;
    public int day;
    public int month;
    public int year;

    public CalendarDate(int i, int i2, int i3) {
        if (i2 > 12) {
            i++;
            i2 = 1;
        } else if (i2 < 1) {
            i--;
            i2 = 12;
        }
        this.year = i;
        this.month = i2;
        this.day = i3;
    }

    public CalendarDate() {
        this.year = Utils.getYear();
        this.month = Utils.getMonth();
        this.day = Utils.getDay();
    }

    public CalendarDate modifyDay(int i) {
        int monthDays = Utils.getMonthDays(this.year, this.month - 1);
        if (i > Utils.getMonthDays(this.year, this.month)) {
            CalendarDate calendarDate = new CalendarDate(this.year, this.month, this.day);
            Log.e("ldf", "移动天数过大");
            return calendarDate;
        } else if (i > 0) {
            return new CalendarDate(this.year, this.month, i);
        } else {
            if (i > 0 - monthDays) {
                return new CalendarDate(this.year, this.month - 1, monthDays + i);
            }
            CalendarDate calendarDate2 = new CalendarDate(this.year, this.month, this.day);
            Log.e("ldf", "移动天数过大");
            return calendarDate2;
        }
    }

    public CalendarDate modifyWeek(int i) {
        CalendarDate calendarDate = new CalendarDate();
        Calendar instance = Calendar.getInstance();
        instance.set(1, this.year);
        instance.set(2, this.month - 1);
        instance.set(5, this.day);
        instance.add(5, i * 7);
        calendarDate.setYear(instance.get(1));
        calendarDate.setMonth(instance.get(2) + 1);
        calendarDate.setDay(instance.get(5));
        return calendarDate;
    }

    public CalendarDate modifyMonth(int i) {
        CalendarDate calendarDate = new CalendarDate();
        int i2 = this.month + i;
        if (i > 0) {
            if (i2 > 12) {
                calendarDate.setYear(this.year + ((i2 - 1) / 12));
                int i3 = i2 % 12;
                if (i3 == 0) {
                    i3 = 12;
                }
                calendarDate.setMonth(i3);
            } else {
                calendarDate.setYear(this.year);
                calendarDate.setMonth(i2);
            }
        } else if (i2 == 0) {
            calendarDate.setYear(this.year - 1);
            calendarDate.setMonth(12);
        } else if (i2 < 0) {
            calendarDate.setYear((this.year + (i2 / 12)) - 1);
            int abs = 12 - (Math.abs(i2) % 12);
            if (abs == 0) {
                abs = 12;
            }
            calendarDate.setMonth(abs);
        } else {
            calendarDate.setYear(this.year);
            if (i2 == 0) {
                i2 = 12;
            }
            calendarDate.setMonth(i2);
        }
        return calendarDate;
    }

    public String toString() {
        return this.year + "-" + this.month + "-" + this.day;
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int i) {
        this.year = i;
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int i) {
        this.month = i;
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int i) {
        this.day = i;
    }

    public boolean equals(CalendarDate calendarDate) {
        if (calendarDate != null && getYear() == calendarDate.getYear() && getMonth() == calendarDate.getMonth() && getDay() == calendarDate.getDay()) {
            return true;
        }
        return false;
    }

    public CalendarDate cloneSelf() {
        return new CalendarDate(this.year, this.month, this.day);
    }
}
