package com.ldf.calendar;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.Scroller;
import com.ldf.calendar.component.CalendarAttr;
import com.ldf.calendar.model.CalendarDate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public final class Utils {
    private static boolean customScrollToBottom = false;
    private static HashMap<String, String> markData = new HashMap<>();
    private static int top;

    private static int calcOffset(int i, int i2, int i3) {
        return i > i3 ? i3 : i < i2 ? i2 : i;
    }

    private Utils() {
    }

    public static int getMonthDays(int i, int i2) {
        if (i2 > 12) {
            i++;
            i2 = 1;
        } else if (i2 < 1) {
            i--;
            i2 = 12;
        }
        int[] iArr = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if ((i % 4 == 0 && i % 100 != 0) || i % 400 == 0) {
            iArr[1] = 29;
        }
        try {
            return iArr[i2 - 1];
        } catch (Exception e) {
            e.getStackTrace();
            return 0;
        }
    }

    public static int getYear() {
        return Calendar.getInstance().get(1);
    }

    public static int getMonth() {
        return Calendar.getInstance().get(2) + 1;
    }

    public static int getDay() {
        return Calendar.getInstance().get(5);
    }

    public static int getFirstDayWeekPosition(int i, int i2, CalendarAttr.WeekArrayType weekArrayType) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(getDateFromString(i, i2));
        int i3 = instance.get(7) - 1;
        if (weekArrayType == CalendarAttr.WeekArrayType.Sunday) {
            return i3;
        }
        int i4 = instance.get(7) + 5;
        return i4 >= 7 ? i4 - 7 : i4;
    }

    public static Date getDateFromString(int i, int i2) {
        Object obj;
        StringBuilder sb = new StringBuilder();
        sb.append(i);
        sb.append("-");
        if (i2 > 9) {
            obj = Integer.valueOf(i2);
        } else {
            obj = "0" + i2;
        }
        sb.append(obj);
        sb.append("-01");
        String sb2 = sb.toString();
        Date date = new Date();
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(sb2);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            return date;
        }
    }

    public static int calculateMonthOffset(int i, int i2, CalendarDate calendarDate) {
        return ((i - calendarDate.getYear()) * 12) + (i2 - calendarDate.getMonth());
    }

    public static int dpi2px(Context context, float f) {
        return (int) ((context.getResources().getDisplayMetrics().density * f) + 0.5f);
    }

    public static HashMap<String, String> loadMarkData() {
        return markData;
    }

    public static void setMarkData(HashMap<String, String> hashMap) {
        markData = hashMap;
    }

    public static void cleanMarkData() {
        markData.clear();
    }

    public static int scroll(View view, int i, int i2, int i3) {
        int top2 = view.getTop();
        int calcOffset = calcOffset(top2 - i, i2, i3) - top2;
        view.offsetTopAndBottom(calcOffset);
        return -calcOffset;
    }

    public static int getTouchSlop(Context context) {
        return ViewConfiguration.get(context).getScaledTouchSlop();
    }

    public static CalendarDate getSunday(CalendarDate calendarDate) {
        Calendar instance = Calendar.getInstance();
        String calendarDate2 = calendarDate.toString();
        Date date = new Date();
        try {
            date = new SimpleDateFormat("yyyy-M-d").parse(calendarDate2);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
        }
        instance.setTime(date);
        if (instance.get(7) != 1) {
            instance.add(5, (7 - instance.get(7)) + 1);
        }
        return new CalendarDate(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    public static CalendarDate getSaturday(CalendarDate calendarDate) {
        Date date;
        Calendar instance = Calendar.getInstance();
        try {
            date = new SimpleDateFormat("yyyy-M-d").parse(calendarDate.toString());
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            date = null;
        }
        instance.setTime(date);
        instance.add(5, 7 - instance.get(7));
        return new CalendarDate(instance.get(1), instance.get(2) + 1, instance.get(5));
    }

    public static boolean isScrollToBottom() {
        return customScrollToBottom;
    }

    public static void setScrollToBottom(boolean z) {
        customScrollToBottom = z;
    }

    public static void scrollTo(final CoordinatorLayout coordinatorLayout, final RecyclerView recyclerView, int i, int i2) {
        final Scroller scroller = new Scroller(coordinatorLayout.getContext());
        int i3 = top;
        scroller.startScroll(0, i3, 0, i - i3, i2);
        ViewCompat.postOnAnimation(recyclerView, new Runnable() {
            /* class com.ldf.calendar.Utils.C21541 */

            public void run() {
                if (scroller.computeScrollOffset()) {
                    recyclerView.offsetTopAndBottom(scroller.getCurrY() - recyclerView.getTop());
                    Utils.saveTop(recyclerView.getTop());
                    coordinatorLayout.dispatchDependentViewsChanged(recyclerView);
                    ViewCompat.postOnAnimation(recyclerView, this);
                }
            }
        });
    }

    public static void saveTop(int i) {
        top = i;
    }

    public static int loadTop() {
        return top;
    }
}
