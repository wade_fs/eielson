package com.ldf.calendar.behavior;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.ldf.calendar.Utils;
import com.ldf.calendar.view.MonthPager;
import com.tencent.bugly.beta.tinker.TinkerReport;

public class RecyclerViewBehavior extends CoordinatorLayout.Behavior<RecyclerView> {
    private Context context;
    boolean hidingTop = false;
    private int initOffset = -1;
    private boolean initiated = false;
    private int minOffset = -1;
    boolean showingTop = false;

    public RecyclerViewBehavior(Context context2, AttributeSet attributeSet) {
        super(context2, attributeSet);
        this.context = context2;
    }

    public boolean onLayoutChild(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, int i) {
        coordinatorLayout.onLayoutChild(recyclerView, i);
        initMinOffsetAndInitOffset(coordinatorLayout, recyclerView, getMonthPager(coordinatorLayout));
        return true;
    }

    private void initMinOffsetAndInitOffset(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, MonthPager monthPager) {
        if (monthPager.getBottom() > 0 && this.initOffset == -1) {
            this.initOffset = monthPager.getViewHeight();
            saveTop(this.initOffset);
        }
        if (!this.initiated) {
            this.initOffset = monthPager.getViewHeight();
            saveTop(this.initOffset);
            this.initiated = true;
        }
        recyclerView.offsetTopAndBottom(Utils.loadTop());
        this.minOffset = getMonthPager(coordinatorLayout).getCellHeight();
    }

    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, View view, View view2, int i) {
        Log.e("ldf", "onStartNestedScroll");
        ((MonthPager) coordinatorLayout.getChildAt(0)).setScrollable(false);
        if ((i & 2) != 0) {
            return true;
        }
        return false;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.Behavior.onNestedPreScroll(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int, int, int[]):void
     arg types: [android.support.design.widget.CoordinatorLayout, android.support.v7.widget.RecyclerView, android.view.View, int, int, int[]]
     candidates:
      com.ldf.calendar.behavior.RecyclerViewBehavior.onNestedPreScroll(android.support.design.widget.CoordinatorLayout, android.support.v7.widget.RecyclerView, android.view.View, int, int, int[]):void
      android.support.design.widget.CoordinatorLayout.Behavior.onNestedPreScroll(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, int, int, int[]):void */
    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, View view, int i, int i2, int[] iArr) {
        Log.e("ldf", "onNestedPreScroll");
        super.onNestedPreScroll(coordinatorLayout, (View) recyclerView, view, i, i2, iArr);
        recyclerView.setVerticalScrollBarEnabled(true);
        boolean z = false;
        if (((MonthPager) coordinatorLayout.getChildAt(0)).getPageScrollState() != 0) {
            iArr[1] = i2;
            Log.w("ldf", "onNestedPreScroll: MonthPager dragging");
            Toast.makeText(this.context, "loading month data", 0).show();
            return;
        }
        this.hidingTop = i2 > 0 && recyclerView.getTop() <= this.initOffset && recyclerView.getTop() > getMonthPager(coordinatorLayout).getCellHeight();
        if (i2 < 0 && !ViewCompat.canScrollVertically(view, -1)) {
            z = true;
        }
        this.showingTop = z;
        if (this.hidingTop || this.showingTop) {
            iArr[1] = Utils.scroll(recyclerView, i2, getMonthPager(coordinatorLayout).getCellHeight(), getMonthPager(coordinatorLayout).getViewHeight());
            saveTop(recyclerView.getTop());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.Behavior.onStopNestedScroll(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):void
     arg types: [android.support.design.widget.CoordinatorLayout, android.support.v7.widget.RecyclerView, android.view.View]
     candidates:
      com.ldf.calendar.behavior.RecyclerViewBehavior.onStopNestedScroll(android.support.design.widget.CoordinatorLayout, android.support.v7.widget.RecyclerView, android.view.View):void
      android.support.design.widget.CoordinatorLayout.Behavior.onStopNestedScroll(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View):void */
    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, View view) {
        Log.e("ldf", "onStopNestedScroll");
        super.onStopNestedScroll(coordinatorLayout, (View) recyclerView, view);
        ((MonthPager) coordinatorLayout.getChildAt(0)).setScrollable(true);
        if (!Utils.isScrollToBottom()) {
            if (this.initOffset - Utils.loadTop() <= Utils.getTouchSlop(this.context) || !this.hidingTop) {
                Utils.scrollTo(coordinatorLayout, recyclerView, getMonthPager(coordinatorLayout).getViewHeight(), TinkerReport.KEY_APPLIED_PACKAGE_CHECK_SIGNATURE);
            } else {
                Utils.scrollTo(coordinatorLayout, recyclerView, getMonthPager(coordinatorLayout).getCellHeight(), 500);
            }
        } else if (Utils.loadTop() - this.minOffset <= Utils.getTouchSlop(this.context) || !this.showingTop) {
            Utils.scrollTo(coordinatorLayout, recyclerView, getMonthPager(coordinatorLayout).getCellHeight(), TinkerReport.KEY_APPLIED_PACKAGE_CHECK_SIGNATURE);
        } else {
            Utils.scrollTo(coordinatorLayout, recyclerView, getMonthPager(coordinatorLayout).getViewHeight(), 500);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: android.support.design.widget.CoordinatorLayout.Behavior.onNestedFling(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, float, float, boolean):boolean
     arg types: [android.support.design.widget.CoordinatorLayout, android.support.v7.widget.RecyclerView, android.view.View, float, float, boolean]
     candidates:
      com.ldf.calendar.behavior.RecyclerViewBehavior.onNestedFling(android.support.design.widget.CoordinatorLayout, android.support.v7.widget.RecyclerView, android.view.View, float, float, boolean):boolean
      android.support.design.widget.CoordinatorLayout.Behavior.onNestedFling(android.support.design.widget.CoordinatorLayout, android.view.View, android.view.View, float, float, boolean):boolean */
    public boolean onNestedFling(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, View view, float f, float f2, boolean z) {
        Log.d("ldf", "onNestedFling: velocityY: " + f2);
        return super.onNestedFling(coordinatorLayout, (View) recyclerView, view, f, f2, z);
    }

    public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, RecyclerView recyclerView, View view, float f, float f2) {
        return this.hidingTop || this.showingTop;
    }

    private MonthPager getMonthPager(CoordinatorLayout coordinatorLayout) {
        return (MonthPager) coordinatorLayout.getChildAt(0);
    }

    private void saveTop(int i) {
        Utils.saveTop(i);
        if (Utils.loadTop() == this.initOffset) {
            Utils.setScrollToBottom(false);
        } else if (Utils.loadTop() == this.minOffset) {
            Utils.setScrollToBottom(true);
        }
    }
}
