package com.ldf.calendar.view;

import android.content.Context;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import com.ldf.calendar.behavior.MonthPagerBehavior;
import com.ldf.calendar.component.CalendarViewAdapter;

@CoordinatorLayout.DefaultBehavior(MonthPagerBehavior.class)
public class MonthPager extends ViewPager {
    public static int CURRENT_DAY_INDEX = 1000;
    private int cellHeight;
    /* access modifiers changed from: private */
    public int currentPosition;
    private boolean hasPageChangeListener;
    /* access modifiers changed from: private */
    public OnPageChangeListener monthPageChangeListener;
    /* access modifiers changed from: private */
    public boolean pageChangeByGesture;
    /* access modifiers changed from: private */
    public int pageScrollState;
    private int rowIndex;
    private boolean scrollable;
    private int viewHeight;

    public interface OnPageChangeListener {
        void onPageScrollStateChanged(int i);

        void onPageScrolled(int i, float f, int i2);

        void onPageSelected(int i);
    }

    public MonthPager(Context context) {
        this(context, null);
    }

    public MonthPager(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.currentPosition = CURRENT_DAY_INDEX;
        this.rowIndex = 6;
        this.pageChangeByGesture = false;
        this.hasPageChangeListener = false;
        this.scrollable = true;
        this.pageScrollState = 0;
        init();
    }

    private void init() {
        addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            /* class com.ldf.calendar.view.MonthPager.C21571 */

            public void onPageScrolled(int i, float f, int i2) {
                if (MonthPager.this.monthPageChangeListener != null) {
                    MonthPager.this.monthPageChangeListener.onPageScrolled(i, f, i2);
                }
            }

            public void onPageSelected(int i) {
                int unused = MonthPager.this.currentPosition = i;
                if (MonthPager.this.pageChangeByGesture) {
                    if (MonthPager.this.monthPageChangeListener != null) {
                        MonthPager.this.monthPageChangeListener.onPageSelected(i);
                    }
                    boolean unused2 = MonthPager.this.pageChangeByGesture = false;
                }
            }

            public void onPageScrollStateChanged(int i) {
                int unused = MonthPager.this.pageScrollState = i;
                if (MonthPager.this.monthPageChangeListener != null) {
                    MonthPager.this.monthPageChangeListener.onPageScrollStateChanged(i);
                }
                boolean unused2 = MonthPager.this.pageChangeByGesture = true;
            }
        });
        this.hasPageChangeListener = true;
    }

    public void addOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        if (this.hasPageChangeListener) {
            Log.e("ldf", "MonthPager Just Can Use Own OnPageChangeListener");
        } else {
            super.addOnPageChangeListener(onPageChangeListener);
        }
    }

    public void addOnPageChangeListener(OnPageChangeListener onPageChangeListener) {
        this.monthPageChangeListener = onPageChangeListener;
        Log.e("ldf", "MonthPager Just Can Use Own OnPageChangeListener");
    }

    public void setScrollable(boolean z) {
        this.scrollable = z;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        if (!this.scrollable) {
            return false;
        }
        return super.onTouchEvent(motionEvent);
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (!this.scrollable) {
            return false;
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void selectOtherMonth(int i) {
        setCurrentItem(this.currentPosition + i);
        ((CalendarViewAdapter) getAdapter()).notifyDataChanged(CalendarViewAdapter.loadSelectedDate());
    }

    public int getPageScrollState() {
        return this.pageScrollState;
    }

    public int getTopMovableDistance() {
        CalendarViewAdapter calendarViewAdapter = (CalendarViewAdapter) getAdapter();
        if (calendarViewAdapter == null) {
            return this.cellHeight;
        }
        this.rowIndex = calendarViewAdapter.getPagers().get(this.currentPosition % 3).getSelectedRowIndex();
        return this.cellHeight * this.rowIndex;
    }

    public int getCellHeight() {
        return this.cellHeight;
    }

    public void setViewHeight(int i) {
        this.cellHeight = i / 6;
        this.viewHeight = i;
    }

    public int getViewHeight() {
        return this.viewHeight;
    }

    public int getCurrentPosition() {
        return this.currentPosition;
    }

    public void setCurrentPosition(int i) {
        this.currentPosition = i;
    }

    public int getRowIndex() {
        this.rowIndex = ((CalendarViewAdapter) getAdapter()).getPagers().get(this.currentPosition % 3).getSelectedRowIndex();
        Log.e("ldf", "getRowIndex = " + this.rowIndex);
        return this.rowIndex;
    }

    public void setRowIndex(int i) {
        this.rowIndex = i;
    }
}
