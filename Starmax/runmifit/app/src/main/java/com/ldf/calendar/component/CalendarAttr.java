package com.ldf.calendar.component;

public class CalendarAttr {
    private CalendarType calendarType;
    private int cellHeight;
    private int cellWidth;
    private WeekArrayType weekArrayType;

    public enum CalendarType {
        WEEK,
        MONTH
    }

    public enum WeekArrayType {
        Sunday,
        Monday
    }

    public WeekArrayType getWeekArrayType() {
        return this.weekArrayType;
    }

    public void setWeekArrayType(WeekArrayType weekArrayType2) {
        this.weekArrayType = weekArrayType2;
    }

    public CalendarType getCalendarType() {
        return this.calendarType;
    }

    public void setCalendarType(CalendarType calendarType2) {
        this.calendarType = calendarType2;
    }

    public int getCellHeight() {
        return this.cellHeight;
    }

    public void setCellHeight(int i) {
        this.cellHeight = i;
    }

    public int getCellWidth() {
        return this.cellWidth;
    }

    public void setCellWidth(int i) {
        this.cellWidth = i;
    }
}
