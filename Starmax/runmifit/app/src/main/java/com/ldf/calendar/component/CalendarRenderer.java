package com.ldf.calendar.component;

import android.content.Context;
import android.graphics.Canvas;
import android.util.Log;
import com.ldf.calendar.Utils;
import com.ldf.calendar.component.CalendarAttr;
import com.ldf.calendar.interf.IDayRenderer;
import com.ldf.calendar.interf.OnSelectDateListener;
import com.ldf.calendar.model.CalendarDate;
import com.ldf.calendar.view.Calendar;
import com.ldf.calendar.view.Day;
import com.ldf.calendar.view.Week;

public class CalendarRenderer {
    private CalendarAttr attr;
    private Calendar calendar;
    private Context context;
    private IDayRenderer dayRenderer;
    private OnSelectDateListener onSelectDateListener;
    private CalendarDate seedDate;
    private CalendarDate selectedDate;
    private int selectedRowIndex = 0;
    private Week[] weeks = new Week[5];

    public CalendarRenderer(Calendar calendar2, CalendarAttr calendarAttr, Context context2) {
        this.calendar = calendar2;
        this.attr = calendarAttr;
        this.context = context2;
    }

    public void draw(Canvas canvas) {
        for (int i = 0; i < 5; i++) {
            if (this.weeks[i] != null) {
                for (int i2 = 0; i2 < 7; i2++) {
                    if (this.weeks[i].days[i2] != null) {
                        this.dayRenderer.drawDay(canvas, this.weeks[i].days[i2]);
                    }
                }
            }
        }
    }

    public void onClickDate(int i, int i2) {
        if (i < 7 && i2 < 5 && this.weeks[i2] != null) {
            if (this.attr.getCalendarType() != CalendarAttr.CalendarType.MONTH) {
                this.weeks[i2].days[i].setState(State.SELECT);
                this.selectedDate = this.weeks[i2].days[i].getDate();
                CalendarViewAdapter.saveSelectedDate(this.selectedDate);
                this.onSelectDateListener.onSelectDate(this.selectedDate);
                this.seedDate = this.selectedDate;
            } else if (this.weeks[i2].days[i].getState() == State.CURRENT_MONTH) {
                this.weeks[i2].days[i].setState(State.SELECT);
                this.selectedDate = this.weeks[i2].days[i].getDate();
                CalendarViewAdapter.saveSelectedDate(this.selectedDate);
                this.onSelectDateListener.onSelectDate(this.selectedDate);
                this.seedDate = this.selectedDate;
            } else if (this.weeks[i2].days[i].getState() == State.PAST_MONTH) {
                this.selectedDate = this.weeks[i2].days[i].getDate();
                CalendarViewAdapter.saveSelectedDate(this.selectedDate);
                this.onSelectDateListener.onSelectOtherMonth(-1);
                this.onSelectDateListener.onSelectDate(this.selectedDate);
            } else if (this.weeks[i2].days[i].getState() == State.NEXT_MONTH) {
                this.selectedDate = this.weeks[i2].days[i].getDate();
                CalendarViewAdapter.saveSelectedDate(this.selectedDate);
                this.onSelectDateListener.onSelectOtherMonth(1);
                this.onSelectDateListener.onSelectDate(this.selectedDate);
            }
        }
    }

    public void updateWeek(int i) {
        CalendarDate calendarDate;
        if (this.attr.getWeekArrayType() == CalendarAttr.WeekArrayType.Sunday) {
            calendarDate = Utils.getSaturday(this.seedDate);
        } else {
            calendarDate = Utils.getSunday(this.seedDate);
        }
        int i2 = calendarDate.day;
        for (int i3 = 6; i3 >= 0; i3--) {
            CalendarDate modifyDay = calendarDate.modifyDay(i2);
            Week[] weekArr = this.weeks;
            if (weekArr[i] == null) {
                weekArr[i] = new Week(i);
            }
            if (this.weeks[i].days[i3] != null) {
                if (modifyDay.equals(CalendarViewAdapter.loadSelectedDate())) {
                    this.weeks[i].days[i3].setState(State.SELECT);
                    this.weeks[i].days[i3].setDate(modifyDay);
                } else {
                    this.weeks[i].days[i3].setState(State.CURRENT_MONTH);
                    this.weeks[i].days[i3].setDate(modifyDay);
                }
            } else if (modifyDay.equals(CalendarViewAdapter.loadSelectedDate())) {
                this.weeks[i].days[i3] = new Day(State.SELECT, modifyDay, i, i3);
            } else {
                this.weeks[i].days[i3] = new Day(State.CURRENT_MONTH, modifyDay, i, i3);
            }
            i2--;
        }
    }

    private void instantiateMonth() {
        int monthDays = Utils.getMonthDays(this.seedDate.year, this.seedDate.month - 1);
        int monthDays2 = Utils.getMonthDays(this.seedDate.year, this.seedDate.month);
        int firstDayWeekPosition = Utils.getFirstDayWeekPosition(this.seedDate.year, this.seedDate.month, this.attr.getWeekArrayType());
        Log.e("ldf", "firstDayPosition = " + firstDayWeekPosition);
        int i = 0;
        for (int i2 = 0; i2 < 5; i2++) {
            i = fillWeek(monthDays, monthDays2, firstDayWeekPosition, i, i2);
        }
    }

    public CalendarDate getFirstDate() {
        return this.weeks[0].days[0].getDate();
    }

    public CalendarDate getLastDate() {
        Week[] weekArr = this.weeks;
        Week week = weekArr[weekArr.length - 1];
        return week.days[week.days.length - 1].getDate();
    }

    private int fillWeek(int i, int i2, int i3, int i4, int i5) {
        for (int i6 = 0; i6 < 7; i6++) {
            int i7 = i6 + (i5 * 7);
            if (i7 >= i3 && i7 < i3 + i2) {
                i4++;
                fillCurrentMonthDate(i4, i5, i6);
            } else if (i7 < i3) {
                instantiateLastMonth(i, i3, i5, i6, i7);
            } else if (i7 >= i3 + i2) {
                instantiateNextMonth(i2, i3, i5, i6, i7);
            }
        }
        return i4;
    }

    private void fillCurrentMonthDate(int i, int i2, int i3) {
        CalendarDate modifyDay = this.seedDate.modifyDay(i);
        Week[] weekArr = this.weeks;
        if (weekArr[i2] == null) {
            weekArr[i2] = new Week(i2);
        }
        if (this.weeks[i2].days[i3] != null) {
            if (modifyDay.equals(CalendarViewAdapter.loadSelectedDate())) {
                this.weeks[i2].days[i3].setDate(modifyDay);
                this.weeks[i2].days[i3].setState(State.SELECT);
            } else {
                this.weeks[i2].days[i3].setDate(modifyDay);
                this.weeks[i2].days[i3].setState(State.CURRENT_MONTH);
            }
        } else if (modifyDay.equals(CalendarViewAdapter.loadSelectedDate())) {
            this.weeks[i2].days[i3] = new Day(State.SELECT, modifyDay, i2, i3);
        } else {
            this.weeks[i2].days[i3] = new Day(State.CURRENT_MONTH, modifyDay, i2, i3);
        }
        if (modifyDay.equals(this.seedDate)) {
            this.selectedRowIndex = i2;
        }
    }

    private void instantiateNextMonth(int i, int i2, int i3, int i4, int i5) {
        CalendarDate calendarDate = new CalendarDate(this.seedDate.year, this.seedDate.month + 1, ((i5 - i2) - i) + 1);
        Week[] weekArr = this.weeks;
        if (weekArr[i3] == null) {
            weekArr[i3] = new Week(i3);
        }
        if (this.weeks[i3].days[i4] != null) {
            this.weeks[i3].days[i4].setDate(calendarDate);
            this.weeks[i3].days[i4].setState(State.NEXT_MONTH);
            return;
        }
        this.weeks[i3].days[i4] = new Day(State.NEXT_MONTH, calendarDate, i3, i4);
    }

    private void instantiateLastMonth(int i, int i2, int i3, int i4, int i5) {
        CalendarDate calendarDate = new CalendarDate(this.seedDate.year, this.seedDate.month - 1, i - ((i2 - i5) - 1));
        Week[] weekArr = this.weeks;
        if (weekArr[i3] == null) {
            weekArr[i3] = new Week(i3);
        }
        if (this.weeks[i3].days[i4] != null) {
            this.weeks[i3].days[i4].setDate(calendarDate);
            this.weeks[i3].days[i4].setState(State.PAST_MONTH);
            return;
        }
        this.weeks[i3].days[i4] = new Day(State.PAST_MONTH, calendarDate, i3, i4);
    }

    public void showDate(CalendarDate calendarDate) {
        if (calendarDate != null) {
            this.seedDate = calendarDate;
        } else {
            this.seedDate = new CalendarDate();
        }
        update();
    }

    public void update() {
        instantiateMonth();
        this.calendar.invalidate();
    }

    public CalendarDate getSeedDate() {
        return this.seedDate;
    }

    public void cancelSelectState() {
        for (int i = 0; i < 5; i++) {
            if (this.weeks[i] != null) {
                int i2 = 0;
                while (true) {
                    if (i2 >= 7) {
                        break;
                    } else if (this.weeks[i].days[i2].getState() == State.SELECT) {
                        this.weeks[i].days[i2].setState(State.CURRENT_MONTH);
                        resetSelectedRowIndex();
                        break;
                    } else {
                        i2++;
                    }
                }
            }
        }
    }

    public void resetSelectedRowIndex() {
        this.selectedRowIndex = 0;
    }

    public int getSelectedRowIndex() {
        return this.selectedRowIndex;
    }

    public void setSelectedRowIndex(int i) {
        this.selectedRowIndex = i;
    }

    public Calendar getCalendar() {
        return this.calendar;
    }

    public void setCalendar(Calendar calendar2) {
        this.calendar = calendar2;
    }

    public CalendarAttr getAttr() {
        return this.attr;
    }

    public void setAttr(CalendarAttr calendarAttr) {
        this.attr = calendarAttr;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public void setOnSelectDateListener(OnSelectDateListener onSelectDateListener2) {
        this.onSelectDateListener = onSelectDateListener2;
    }

    public void setDayRenderer(IDayRenderer iDayRenderer) {
        this.dayRenderer = iDayRenderer;
    }
}
