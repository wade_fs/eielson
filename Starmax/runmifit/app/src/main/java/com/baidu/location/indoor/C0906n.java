package com.baidu.location.indoor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import com.baidu.location.indoor.mapversion.C0883a;

/* renamed from: com.baidu.location.indoor.n */
class C0906n implements SensorEventListener {

    /* renamed from: a */
    final /* synthetic */ C0881m f2298a;

    C0906n(C0881m mVar) {
        this.f2298a = mVar;
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        int type = sensorEvent.sensor.getType();
        if (type == 1) {
            float[] fArr = (float[]) sensorEvent.values.clone();
            float[] unused = this.f2298a.f2193q = (float[]) fArr.clone();
            if (this.f2298a.f2187k && C0883a.m2674b()) {
                C0883a.m2673a(1, fArr, sensorEvent.timestamp);
            }
            float[] a = this.f2298a.m2638a(fArr[0], fArr[1], fArr[2]);
            if (C0881m.m2641b(this.f2298a) >= 20) {
                double d = (double) ((a[0] * a[0]) + (a[1] * a[1]) + (a[2] * a[2]));
                if (this.f2298a.f2190n == 0) {
                    if (d > 4.0d) {
                        int unused2 = this.f2298a.f2190n = 1;
                    }
                } else if (d < 0.009999999776482582d) {
                    int unused3 = this.f2298a.f2190n = 0;
                }
            }
        } else if (type == 3) {
            float[] fArr2 = (float[]) sensorEvent.values.clone();
            if (this.f2298a.f2187k && C0883a.m2674b()) {
                C0883a.m2673a(5, fArr2, sensorEvent.timestamp);
            }
            this.f2298a.f2173P[this.f2298a.f2172O] = (double) fArr2[0];
            C0881m.m2647f(this.f2298a);
            if (this.f2298a.f2172O == this.f2298a.f2171N) {
                int unused4 = this.f2298a.f2172O = 0;
            }
            if (C0881m.m2649h(this.f2298a) >= 20) {
                C0881m mVar = this.f2298a;
                boolean unused5 = mVar.f2174Q = mVar.m2650i();
                if (!this.f2298a.f2174Q) {
                    this.f2298a.f2180d.unregisterListener(this.f2298a.f2178b, this.f2298a.f2184h);
                }
                double[] m = this.f2298a.f2194r;
                C0881m mVar2 = this.f2298a;
                m[0] = mVar2.m2630a(mVar2.f2194r[0], (double) fArr2[0], 0.7d);
                this.f2298a.f2194r[1] = (double) fArr2[1];
                this.f2298a.f2194r[2] = (double) fArr2[2];
            }
        } else if (type == 4) {
            float[] fArr3 = (float[]) sensorEvent.values.clone();
            if (this.f2298a.f2187k && C0883a.m2674b()) {
                C0883a.m2673a(4, fArr3, sensorEvent.timestamp);
            }
        }
    }
}
