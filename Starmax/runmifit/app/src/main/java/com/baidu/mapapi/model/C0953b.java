package com.baidu.mapapi.model;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.model.b */
final class C0953b implements Parcelable.Creator<LatLngBounds> {
    C0953b() {
    }

    /* renamed from: a */
    public LatLngBounds createFromParcel(Parcel parcel) {
        return new LatLngBounds(parcel);
    }

    /* renamed from: a */
    public LatLngBounds[] newArray(int i) {
        return new LatLngBounds[i];
    }
}
