package com.baidu.mapapi.search.geocode;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.geocode.ReverseGeoCodeResult;

/* renamed from: com.baidu.mapapi.search.geocode.d */
final class C0978d implements Parcelable.Creator<ReverseGeoCodeResult.PoiRegionsInfo> {
    C0978d() {
    }

    /* renamed from: a */
    public ReverseGeoCodeResult.PoiRegionsInfo createFromParcel(Parcel parcel) {
        return new ReverseGeoCodeResult.PoiRegionsInfo(parcel);
    }

    /* renamed from: a */
    public ReverseGeoCodeResult.PoiRegionsInfo[] newArray(int i) {
        return new ReverseGeoCodeResult.PoiRegionsInfo[i];
    }
}
