package com.baidu.mapsdkplatform.comapi.synchronization.p028c;

import android.os.Build;
import com.baidu.mapapi.UIMsg;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.c.a */
public class C1112a {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f3675a = UIMsg.m_AppUI.MSG_APP_SAVESCREEN;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f3676b = UIMsg.m_AppUI.MSG_APP_SAVESCREEN;

    /* renamed from: c */
    private ExecutorService f3677c = Executors.newCachedThreadPool();

    /* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.c.a$a */
    private static abstract class C1113a implements Runnable {
        private C1113a() {
        }

        /* synthetic */ C1113a(C1114b bVar) {
            this();
        }

        /* renamed from: a */
        public abstract void mo13194a();

        public void run() {
            mo13194a();
        }
    }

    static {
        if (Build.VERSION.SDK_INT <= 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    /* renamed from: a */
    public void mo13193a(String str, C1118e eVar) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Request URL cannot be null");
        }
        this.f3677c.submit(new C1114b(this, eVar, str));
    }
}
