package com.baidu.mapsdkplatform.comapi;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import com.baidu.mapapi.JNIInitializer;
import com.baidu.mapapi.common.EnvironmentUtilities;
import com.baidu.mapsdkplatform.comapi.p024b.p025a.C1048c;
import java.io.File;
import java.io.IOException;

/* renamed from: com.baidu.mapsdkplatform.comapi.c */
public class C1052c {

    /* renamed from: a */
    private static boolean f3367a;

    /* renamed from: a */
    public static void m3462a(Context context, boolean z, String str, String str2, String str3) {
        if (!f3367a) {
            if (context == null) {
                throw new IllegalArgumentException("BDMapSDKException: context can not be null");
            } else if (context instanceof Application) {
                NativeLoader.setContext(context);
                NativeLoader.m3329a(z, str);
                C1028a.m3342a().mo12866a(context);
                C1028a.m3342a().mo12870c();
                C1028a.m3342a().mo12868a(str3);
                JNIInitializer.setContext((Application) context);
                if (m3463a(str2)) {
                    EnvironmentUtilities.setSDCardPath(str2);
                }
                EnvironmentUtilities.initAppDirectory(context);
                C1048c.m3446a().mo12923a(context);
                f3367a = true;
            } else {
                throw new RuntimeException("BDMapSDKException: context must be an ApplicationContext");
            }
        }
    }

    /* renamed from: a */
    private static boolean m3463a(String str) {
        if (str == null || str.isEmpty()) {
            return false;
        }
        try {
            File file = new File(str + "/check.0");
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            if (!file.exists()) {
                return true;
            }
            file.delete();
            return true;
        } catch (IOException e) {
            Log.e("SDKInitializer", "SDCard cache path invalid", e);
            throw new IllegalArgumentException("BDMapSDKException: Provided sdcard cache path invalid can not used.");
        }
    }
}
