package com.baidu.mapsdkplatform.comapi.synchronization.p027b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.SparseArray;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceConstant;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceData;
import com.baidu.mapapi.synchronization.histroytrace.HistoryTraceQueryOptions;
import com.baidu.mapapi.synchronization.histroytrace.OnHistoryTraceListener;
import com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1105b;
import com.baidu.mapsdkplatform.comapi.synchronization.p029d.C1120a;
import com.baidu.mobstat.Config;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.a */
class C1104a extends Handler {

    /* renamed from: a */
    private static final String f3636a = C1104a.class.getSimpleName();

    /* renamed from: b */
    private static OnHistoryTraceListener f3637b;

    /* renamed from: h */
    private static boolean f3638h = false;

    /* renamed from: c */
    private HistoryTraceData f3639c = new HistoryTraceData();

    /* renamed from: d */
    private List<HistoryTraceData.HistoryTracePoint> f3640d = new ArrayList();

    /* renamed from: e */
    private C1105b.C1106a f3641e;

    /* renamed from: f */
    private HistoryTraceQueryOptions f3642f;

    /* renamed from: g */
    private boolean f3643g = false;

    /* renamed from: i */
    private int f3644i = 0;

    /* renamed from: j */
    private String f3645j = HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_SUCCESS;

    /* renamed from: k */
    private SparseArray<List<HistoryTraceData.HistoryTracePoint>> f3646k = new SparseArray<>();

    C1104a(Looper looper) {
        super(looper);
    }

    /* renamed from: a */
    private LatLng m3767a(String str) {
        if (TextUtils.isEmpty(str)) {
            C1120a.m3855b(f3636a, "Coord string is null");
            return null;
        }
        String[] split = str.split(",");
        if (split.length == 0 || 2 != split.length) {
            C1120a.m3855b(f3636a, "Coord result is error");
            return null;
        }
        try {
            try {
                return new LatLng(Double.parseDouble(split[1]), Double.parseDouble(split[0]));
            } catch (NumberFormatException e) {
                C1120a.m3852a(f3636a, "Parser coord latitude failed", e);
                return null;
            }
        } catch (NumberFormatException e2) {
            C1120a.m3852a(f3636a, "Parser coord longitude failed", e2);
            return null;
        }
    }

    /* renamed from: a */
    private List<HistoryTraceData.HistoryTracePoint> m3768a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            C1120a.m3855b(f3636a, "Request result not contain points info");
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                HistoryTraceData.HistoryTracePoint historyTracePoint = new HistoryTraceData.HistoryTracePoint();
                historyTracePoint.setPoint(new LatLng(optJSONObject.optDouble("latitude"), optJSONObject.optDouble("longitude")));
                historyTracePoint.setLocationTime(optJSONObject.optLong("loc_time"));
                historyTracePoint.setCreateTime(optJSONObject.optString("create_time"));
                arrayList.add(historyTracePoint);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private void m3769a(int i) {
        if (this.f3641e == null) {
            this.f3641e = new C1105b.C1106a();
        }
        Message obtainMessage = this.f3641e.obtainMessage();
        obtainMessage.what = i;
        obtainMessage.obj = this.f3642f;
        this.f3641e.sendMessage(obtainMessage);
    }

    /* renamed from: a */
    private void m3770a(int i, String str, int i2, HistoryTraceData historyTraceData) {
        OnHistoryTraceListener onHistoryTraceListener = f3637b;
        if (onHistoryTraceListener == null) {
            C1120a.m3855b(f3636a, "OnHistoryTraceListener is null");
        } else if (1 == i2) {
            onHistoryTraceListener.onQueryHistroyTraceData(i, str, historyTraceData);
        } else {
            C1120a.m3855b(f3636a, "Undefined message type to notify");
        }
    }

    /* renamed from: a */
    private boolean m3771a(String str, HistoryTraceData historyTraceData, int i) {
        if (TextUtils.isEmpty(str) || historyTraceData == null) {
            C1120a.m3855b(f3636a, "Parameter error when parser");
            m3770a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_PARSER_FAILED, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_RESULT_PARSER_FAILED, i, null);
            return false;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!m3772a(jSONObject, i)) {
                C1120a.m3855b(f3636a, "Request result contain error");
                return false;
            }
            this.f3643g = false;
            int optInt = jSONObject.optInt(Config.EXCEPTION_MEMORY_TOTAL);
            historyTraceData.setTotalPoints(optInt);
            int optInt2 = jSONObject.optInt("size");
            int optInt3 = jSONObject.optInt("req_page_index");
            if (optInt2 * optInt3 < optInt) {
                f3638h = true;
                optInt3++;
                C1110f.m3819a(optInt3);
            }
            historyTraceData.setCurrentPageIndex(optInt3);
            historyTraceData.setDistance(jSONObject.optDouble("distance"));
            historyTraceData.setTollDiatance(jSONObject.optDouble("toll_distance"));
            historyTraceData.setCurrentOrderState(jSONObject.optInt("o_status"));
            historyTraceData.setOrderStartPosition(m3767a(jSONObject.optString("o_start_point")));
            historyTraceData.setOrderEndPosition(m3767a(jSONObject.optString("o_end_point")));
            List<HistoryTraceData.HistoryTracePoint> list = this.f3640d;
            if (list != null && !list.isEmpty()) {
                this.f3640d.clear();
            }
            List<HistoryTraceData.HistoryTracePoint> a = m3768a(jSONObject.optJSONArray("points"));
            if (a != null && !a.isEmpty()) {
                this.f3640d.addAll(a);
            }
            return true;
        } catch (JSONException e) {
            C1120a.m3852a(f3636a, "JSONException happened when parser request result", e);
            return false;
        }
    }

    /* renamed from: a */
    private boolean m3772a(JSONObject jSONObject, int i) {
        if (jSONObject == null || !jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
            m3770a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_PARSER_FAILED, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_RESULT_PARSER_FAILED, i, null);
            C1120a.m3855b(f3636a, "Request result no status");
            return false;
        }
        this.f3644i = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
        this.f3645j = jSONObject.optString("message");
        int i2 = this.f3644i;
        if (i2 == 0) {
            this.f3643g = false;
            return true;
        } else if (1 != i2 || this.f3643g) {
            return true;
        } else {
            m3769a(i);
            this.f3643g = true;
            return false;
        }
    }

    /* renamed from: a */
    public void mo13174a() {
        if (f3637b != null) {
            f3637b = null;
        }
        List<HistoryTraceData.HistoryTracePoint> list = this.f3640d;
        if (list != null) {
            list.clear();
            this.f3640d = null;
        }
        this.f3639c = null;
    }

    /* renamed from: a */
    public void mo13175a(HistoryTraceQueryOptions historyTraceQueryOptions) {
        this.f3642f = historyTraceQueryOptions;
    }

    /* renamed from: a */
    public void mo13176a(OnHistoryTraceListener onHistoryTraceListener) {
        f3637b = onHistoryTraceListener;
    }

    /* renamed from: a */
    public void mo13177a(C1105b.C1106a aVar) {
        this.f3641e = aVar;
    }

    public void handleMessage(Message message) {
        String str = f3636a;
        C1120a.m3856c(str, "Message type = " + message.what);
        if (message.what != 3) {
            C1120a.m3855b(f3636a, "Undefined message type");
            return;
        }
        int i = message.arg1;
        String str2 = (String) message.obj;
        if (str2 == null) {
            m3770a(HistoryTraceConstant.LBS_HISTORY_TRACE_CODE_QUERY_RESULT_NULL, HistoryTraceConstant.LBS_HISTORY_TRACE_MESSAGE_QUERY_RESULT_NULL, i, null);
            return;
        }
        boolean a = m3771a(str2, this.f3639c, i);
        if (a) {
            this.f3646k.put(this.f3639c.getCurrentPageIndex() - 1, this.f3640d);
            if (f3638h) {
                m3769a(i);
                return;
            }
        }
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.f3646k.size(); i2++) {
            List list = this.f3646k.get(i2);
            if (list != null && !list.isEmpty()) {
                arrayList.addAll(list);
            }
        }
        this.f3639c.setPointsList(arrayList);
        HistoryTraceData historyTraceData = this.f3639c;
        if (a && !f3638h) {
            m3770a(this.f3644i, this.f3645j, i, historyTraceData);
            this.f3639c = null;
            this.f3639c = new HistoryTraceData();
            this.f3640d.clear();
            this.f3640d = null;
            this.f3640d = new ArrayList();
            this.f3646k.clear();
        }
        f3638h = false;
    }
}
