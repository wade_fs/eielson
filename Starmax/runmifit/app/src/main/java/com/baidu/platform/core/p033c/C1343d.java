package com.baidu.platform.core.p033c;

import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.PoiDetailInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiDetailResult;
import com.baidu.mapapi.search.poi.PoiDetailSearchResult;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1319d;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.core.c.d */
public class C1343d extends C1319d {

    /* renamed from: b */
    private static final String f4457b = C1343d.class.getSimpleName();

    /* renamed from: c */
    private boolean f4458c = false;

    /* renamed from: a */
    private LatLng m4996a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        double optDouble = jSONObject.optDouble("lat");
        double optDouble2 = jSONObject.optDouble("lng");
        return SDKInitializer.getCoordType() == CoordType.GCJ02 ? CoordTrans.baiduToGcj(new LatLng(optDouble, optDouble2)) : new LatLng(optDouble, optDouble2);
    }

    /* renamed from: a */
    private boolean m4997a(String str, SearchResult searchResult) {
        JSONArray optJSONArray;
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (jSONObject.length() == 0 || jSONObject.optInt(NotificationCompat.CATEGORY_STATUS) != 0 || (optJSONArray = jSONObject.optJSONArray("result")) == null || optJSONArray.length() == 0) {
                return false;
            }
            return this.f4458c ? m4999a(optJSONArray, (PoiDetailSearchResult) searchResult) : m4998a(optJSONArray, (PoiDetailResult) searchResult);
        } catch (JSONException e) {
            Log.e(f4457b, "Parse detail search result error", e);
            return false;
        }
    }

    /* renamed from: a */
    private boolean m4998a(JSONArray jSONArray, PoiDetailResult poiDetailResult) {
        JSONObject jSONObject = (JSONObject) jSONArray.opt(0);
        if (jSONObject == null || jSONObject.length() == 0) {
            return false;
        }
        poiDetailResult.setName(jSONObject.optString(Config.FEED_LIST_NAME));
        poiDetailResult.setLocation(m4996a(jSONObject.optJSONObject("location")));
        poiDetailResult.setAddress(jSONObject.optString("address"));
        poiDetailResult.setTelephone(jSONObject.optString("telephone"));
        poiDetailResult.setUid(jSONObject.optString(Config.CUSTOM_USER_ID));
        JSONObject optJSONObject = jSONObject.optJSONObject("detail_info");
        if (!(optJSONObject == null || optJSONObject.length() == 0)) {
            poiDetailResult.setTag(optJSONObject.optString("tag"));
            poiDetailResult.setDetailUrl(optJSONObject.optString("detail_url"));
            poiDetailResult.setType(optJSONObject.optString(SocialConstants.PARAM_TYPE));
            poiDetailResult.setPrice(optJSONObject.optDouble("price", 0.0d));
            poiDetailResult.setOverallRating(optJSONObject.optDouble("overall_rating", 0.0d));
            poiDetailResult.setTasteRating(optJSONObject.optDouble("taste_rating", 0.0d));
            poiDetailResult.setServiceRating(optJSONObject.optDouble("service_rating", 0.0d));
            poiDetailResult.setEnvironmentRating(optJSONObject.optDouble("environment_rating", 0.0d));
            poiDetailResult.setFacilityRating(optJSONObject.optDouble("facility_rating", 0.0d));
            poiDetailResult.setHygieneRating(optJSONObject.optDouble("hygiene_rating", 0.0d));
            poiDetailResult.setTechnologyRating(optJSONObject.optDouble("technology_rating", 0.0d));
            poiDetailResult.setImageNum(optJSONObject.optInt("image_num"));
            poiDetailResult.setGrouponNum(optJSONObject.optInt("groupon_num", 0));
            poiDetailResult.setCommentNum(optJSONObject.optInt("comment_num", 0));
            poiDetailResult.setDiscountNum(optJSONObject.optInt("discount_num", 0));
            poiDetailResult.setFavoriteNum(optJSONObject.optInt("favorite_num", 0));
            poiDetailResult.setCheckinNum(optJSONObject.optInt("checkin_num", 0));
            poiDetailResult.setShopHours(optJSONObject.optString("shop_hours"));
        }
        poiDetailResult.error = SearchResult.ERRORNO.NO_ERROR;
        return true;
    }

    /* renamed from: a */
    private boolean m4999a(JSONArray jSONArray, PoiDetailSearchResult poiDetailSearchResult) {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = (JSONObject) jSONArray.opt(i);
            if (!(jSONObject == null || jSONObject.length() == 0)) {
                PoiDetailInfo poiDetailInfo = new PoiDetailInfo();
                poiDetailInfo.setName(jSONObject.optString(Config.FEED_LIST_NAME));
                poiDetailInfo.setLocation(m4996a(jSONObject.optJSONObject("location")));
                poiDetailInfo.setAddress(jSONObject.optString("address"));
                poiDetailInfo.setProvince(jSONObject.optString("province"));
                poiDetailInfo.setCity(jSONObject.optString("city"));
                poiDetailInfo.setArea(jSONObject.optString("area"));
                poiDetailInfo.setTelephone(jSONObject.optString("telephone"));
                poiDetailInfo.setUid(jSONObject.optString(Config.CUSTOM_USER_ID));
                poiDetailInfo.setStreetId(jSONObject.optString("setStreetId"));
                poiDetailInfo.setDetail(jSONObject.optString("detail"));
                JSONObject optJSONObject = jSONObject.optJSONObject("detail_info");
                if (!(optJSONObject == null || optJSONObject.length() == 0)) {
                    poiDetailInfo.setDistance(optJSONObject.optInt("distance", 0));
                    poiDetailInfo.setType(optJSONObject.optString(SocialConstants.PARAM_TYPE));
                    poiDetailInfo.setTag(optJSONObject.optString("tag"));
                    poiDetailInfo.setDetailUrl(optJSONObject.optString("detail_url"));
                    poiDetailInfo.setPrice(optJSONObject.optDouble("price", 0.0d));
                    poiDetailInfo.setShopHours(optJSONObject.optString("shop_hours"));
                    poiDetailInfo.setOverallRating(optJSONObject.optDouble("overall_rating", 0.0d));
                    poiDetailInfo.setTasteRating(optJSONObject.optDouble("taste_rating", 0.0d));
                    poiDetailInfo.setServiceRating(optJSONObject.optDouble("service_rating", 0.0d));
                    poiDetailInfo.setEnvironmentRating(optJSONObject.optDouble("environment_rating", 0.0d));
                    poiDetailInfo.setFacilityRating(optJSONObject.optDouble("facility_rating", 0.0d));
                    poiDetailInfo.setHygieneRating(optJSONObject.optDouble("hygiene_rating", 0.0d));
                    poiDetailInfo.setTechnologyRating(optJSONObject.optDouble("technology_rating", 0.0d));
                    poiDetailInfo.setImageNum(optJSONObject.optInt("image_num"));
                    poiDetailInfo.setGrouponNum(optJSONObject.optInt("groupon_num", 0));
                    poiDetailInfo.setCommentNum(optJSONObject.optInt("comment_num", 0));
                    poiDetailInfo.setDiscountNum(optJSONObject.optInt("discount_num", 0));
                    poiDetailInfo.setFavoriteNum(optJSONObject.optInt("favorite_num", 0));
                    poiDetailInfo.setCheckinNum(optJSONObject.optInt("checkin_num", 0));
                }
                arrayList.add(poiDetailInfo);
            }
        }
        poiDetailSearchResult.setPoiDetailInfoList(arrayList);
        poiDetailSearchResult.error = SearchResult.ERRORNO.NO_ERROR;
        return true;
    }

    /* renamed from: a */
    public SearchResult mo14018a(String str) {
        SearchResult.ERRORNO errorno;
        SearchResult poiDetailSearchResult = this.f4458c ? new PoiDetailSearchResult() : new PoiDetailResult();
        if (str != null && !str.isEmpty()) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.length() == 0) {
                    poiDetailSearchResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                    return poiDetailSearchResult;
                } else if (!jSONObject.has("SDK_InnerError")) {
                    if (!m4997a(str, poiDetailSearchResult)) {
                        poiDetailSearchResult.error = SearchResult.ERRORNO.RESULT_NOT_FOUND;
                    }
                    return poiDetailSearchResult;
                } else {
                    JSONObject optJSONObject = jSONObject.optJSONObject("SDK_InnerError");
                    if (!(optJSONObject == null || optJSONObject.length() == 0)) {
                        if (optJSONObject.has("PermissionCheckError")) {
                            errorno = SearchResult.ERRORNO.PERMISSION_UNFINISHED;
                            poiDetailSearchResult.error = errorno;
                            return poiDetailSearchResult;
                        }
                        if (optJSONObject.has("httpStateError")) {
                            String optString = optJSONObject.optString("httpStateError");
                            char c = 65535;
                            int hashCode = optString.hashCode();
                            if (hashCode != -879828873) {
                                if (hashCode == 1470557208 && optString.equals("REQUEST_ERROR")) {
                                    c = 1;
                                }
                            } else if (optString.equals("NETWORK_ERROR")) {
                                c = 0;
                            }
                            poiDetailSearchResult.error = c != 0 ? c != 1 ? SearchResult.ERRORNO.SEARCH_SERVER_INTERNAL_ERROR : SearchResult.ERRORNO.REQUEST_ERROR : SearchResult.ERRORNO.NETWORK_ERROR;
                        }
                        return poiDetailSearchResult;
                    }
                }
            } catch (JSONException e) {
                Log.e(f4457b, "Parse detail search result failed", e);
            }
        }
        errorno = SearchResult.ERRORNO.RESULT_NOT_FOUND;
        poiDetailSearchResult.error = errorno;
        return poiDetailSearchResult;
    }

    /* renamed from: a */
    public void mo14020a(SearchResult searchResult, Object obj) {
        if (obj != null && (obj instanceof OnGetPoiSearchResultListener)) {
            OnGetPoiSearchResultListener onGetPoiSearchResultListener = (OnGetPoiSearchResultListener) obj;
            if (this.f4458c) {
                onGetPoiSearchResultListener.onGetPoiDetailResult((PoiDetailSearchResult) searchResult);
            } else {
                onGetPoiSearchResultListener.onGetPoiDetailResult((PoiDetailResult) searchResult);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo14054a(boolean z) {
        this.f4458c = z;
    }
}
