package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.BusInfo;
import com.baidu.mapapi.search.core.CoachInfo;
import com.baidu.mapapi.search.core.PlaneInfo;
import com.baidu.mapapi.search.core.PriceInfo;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteStep;
import com.baidu.mapapi.search.core.TrainInfo;
import com.baidu.mapsdkplatform.comapi.util.CoordTrans;
import java.util.ArrayList;
import java.util.List;

public final class MassTransitRouteLine extends RouteLine<TransitStep> implements Parcelable {
    public static final Parcelable.Creator<MassTransitRouteLine> CREATOR = new C0993i();

    /* renamed from: b */
    private String f3065b;

    /* renamed from: c */
    private double f3066c;

    /* renamed from: d */
    private List<PriceInfo> f3067d;

    /* renamed from: e */
    private List<List<TransitStep>> f3068e = null;

    public static class TransitStep extends RouteStep implements Parcelable {
        public static final Parcelable.Creator<TransitStep> CREATOR = new C0994j();

        /* renamed from: d */
        private List<TrafficCondition> f3069d;

        /* renamed from: e */
        private LatLng f3070e;

        /* renamed from: f */
        private LatLng f3071f;

        /* renamed from: g */
        private TrainInfo f3072g;

        /* renamed from: h */
        private PlaneInfo f3073h;

        /* renamed from: i */
        private CoachInfo f3074i;

        /* renamed from: j */
        private BusInfo f3075j;

        /* renamed from: k */
        private StepVehicleInfoType f3076k;

        /* renamed from: l */
        private String f3077l;

        /* renamed from: m */
        private String f3078m;

        public enum StepVehicleInfoType {
            ESTEP_TRAIN(1),
            ESTEP_PLANE(2),
            ESTEP_BUS(3),
            ESTEP_DRIVING(4),
            ESTEP_WALK(5),
            ESTEP_COACH(6);
            

            /* renamed from: a */
            private int f3079a = 0;

            private StepVehicleInfoType(int i) {
                this.f3079a = i;
            }

            public int getInt() {
                return this.f3079a;
            }
        }

        public static class TrafficCondition implements Parcelable {
            public static final Parcelable.Creator<TrafficCondition> CREATOR = new C0995k();

            /* renamed from: a */
            private int f3080a;

            /* renamed from: b */
            private int f3081b;

            public TrafficCondition() {
            }

            protected TrafficCondition(Parcel parcel) {
                this.f3080a = parcel.readInt();
                this.f3081b = parcel.readInt();
            }

            public int describeContents() {
                return 0;
            }

            public int getTrafficGeoCnt() {
                return this.f3081b;
            }

            public int getTrafficStatus() {
                return this.f3080a;
            }

            public void setTrafficGeoCnt(int i) {
                this.f3081b = i;
            }

            public void setTrafficStatus(int i) {
                this.f3080a = i;
            }

            public void writeToParcel(Parcel parcel, int i) {
                parcel.writeInt(this.f3080a);
                parcel.writeInt(this.f3081b);
            }
        }

        public TransitStep() {
        }

        protected TransitStep(Parcel parcel) {
            super(parcel);
            StepVehicleInfoType stepVehicleInfoType;
            this.f3069d = parcel.createTypedArrayList(TrafficCondition.CREATOR);
            this.f3070e = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
            this.f3071f = (LatLng) parcel.readParcelable(LatLng.class.getClassLoader());
            this.f3072g = (TrainInfo) parcel.readParcelable(TrainInfo.class.getClassLoader());
            this.f3073h = (PlaneInfo) parcel.readParcelable(PlaneInfo.class.getClassLoader());
            this.f3074i = (CoachInfo) parcel.readParcelable(CoachInfo.class.getClassLoader());
            this.f3075j = (BusInfo) parcel.readParcelable(BusInfo.class.getClassLoader());
            switch (parcel.readInt()) {
                case 1:
                    stepVehicleInfoType = StepVehicleInfoType.ESTEP_TRAIN;
                    this.f3076k = stepVehicleInfoType;
                    break;
                case 2:
                    stepVehicleInfoType = StepVehicleInfoType.ESTEP_PLANE;
                    this.f3076k = stepVehicleInfoType;
                    break;
                case 3:
                    stepVehicleInfoType = StepVehicleInfoType.ESTEP_BUS;
                    this.f3076k = stepVehicleInfoType;
                    break;
                case 4:
                    stepVehicleInfoType = StepVehicleInfoType.ESTEP_DRIVING;
                    this.f3076k = stepVehicleInfoType;
                    break;
                case 5:
                    stepVehicleInfoType = StepVehicleInfoType.ESTEP_WALK;
                    this.f3076k = stepVehicleInfoType;
                    break;
                case 6:
                    stepVehicleInfoType = StepVehicleInfoType.ESTEP_COACH;
                    this.f3076k = stepVehicleInfoType;
                    break;
            }
            this.f3077l = parcel.readString();
            this.f3078m = parcel.readString();
        }

        /* renamed from: a */
        private List<LatLng> m3199a(String str) {
            String[] split;
            ArrayList arrayList = new ArrayList();
            String[] split2 = str.split(";");
            if (split2 != null) {
                for (int i = 0; i < split2.length; i++) {
                    if (!(split2[i] == null || split2[i] == "" || (split = split2[i].split(",")) == null || split[1] == "" || split[0] == "")) {
                        LatLng latLng = new LatLng(Double.parseDouble(split[1]), Double.parseDouble(split[0]));
                        if (SDKInitializer.getCoordType() == CoordType.GCJ02) {
                            latLng = CoordTrans.baiduToGcj(latLng);
                        }
                        arrayList.add(latLng);
                    }
                }
            }
            return arrayList;
        }

        public int describeContents() {
            return 0;
        }

        public BusInfo getBusInfo() {
            return this.f3075j;
        }

        public CoachInfo getCoachInfo() {
            return this.f3074i;
        }

        public LatLng getEndLocation() {
            return this.f3071f;
        }

        public String getInstructions() {
            return this.f3077l;
        }

        public PlaneInfo getPlaneInfo() {
            return this.f3073h;
        }

        public LatLng getStartLocation() {
            return this.f3070e;
        }

        public List<TrafficCondition> getTrafficConditions() {
            return this.f3069d;
        }

        public TrainInfo getTrainInfo() {
            return this.f3072g;
        }

        public StepVehicleInfoType getVehileType() {
            return this.f3076k;
        }

        public List<LatLng> getWayPoints() {
            if (this.mWayPoints == null) {
                this.mWayPoints = m3199a(this.f3078m);
            }
            return this.mWayPoints;
        }

        public void setBusInfo(BusInfo busInfo) {
            this.f3075j = busInfo;
        }

        public void setCoachInfo(CoachInfo coachInfo) {
            this.f3074i = coachInfo;
        }

        public void setEndLocation(LatLng latLng) {
            this.f3071f = latLng;
        }

        public void setInstructions(String str) {
            this.f3077l = str;
        }

        public void setPathString(String str) {
            this.f3078m = str;
        }

        public void setPlaneInfo(PlaneInfo planeInfo) {
            this.f3073h = planeInfo;
        }

        public void setStartLocation(LatLng latLng) {
            this.f3070e = latLng;
        }

        public void setTrafficConditions(List<TrafficCondition> list) {
            this.f3069d = list;
        }

        public void setTrainInfo(TrainInfo trainInfo) {
            this.f3072g = trainInfo;
        }

        public void setVehileType(StepVehicleInfoType stepVehicleInfoType) {
            this.f3076k = stepVehicleInfoType;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeTypedList(this.f3069d);
            parcel.writeParcelable(this.f3070e, i);
            parcel.writeParcelable(this.f3071f, i);
            parcel.writeParcelable(this.f3072g, i);
            parcel.writeParcelable(this.f3073h, i);
            parcel.writeParcelable(this.f3074i, i);
            parcel.writeParcelable(this.f3075j, i);
            parcel.writeInt(this.f3076k.getInt());
            parcel.writeString(this.f3077l);
            parcel.writeString(this.f3078m);
        }
    }

    public MassTransitRouteLine() {
    }

    protected MassTransitRouteLine(Parcel parcel) {
        super(parcel);
        int readInt = parcel.readInt();
        this.f3065b = parcel.readString();
        this.f3066c = parcel.readDouble();
        this.f3067d = parcel.createTypedArrayList(PriceInfo.CREATOR);
        if (readInt > 0) {
            this.f3068e = new ArrayList();
            for (int i = 0; i < readInt; i++) {
                this.f3068e.add(parcel.createTypedArrayList(TransitStep.CREATOR));
            }
        }
    }

    public int describeContents() {
        return 0;
    }

    public String getArriveTime() {
        return this.f3065b;
    }

    public List<List<TransitStep>> getNewSteps() {
        return this.f3068e;
    }

    public double getPrice() {
        return this.f3066c;
    }

    public List<PriceInfo> getPriceInfo() {
        return this.f3067d;
    }

    public void setArriveTime(String str) {
        this.f3065b = str;
    }

    public void setNewSteps(List<List<TransitStep>> list) {
        this.f3068e = list;
    }

    public void setPrice(double d) {
        this.f3066c = d;
    }

    public void setPriceInfo(List<PriceInfo> list) {
        this.f3067d = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        List<List<TransitStep>> list = this.f3068e;
        parcel.writeInt(list == null ? 0 : list.size());
        parcel.writeString(this.f3065b);
        parcel.writeDouble(this.f3066c);
        parcel.writeTypedList(this.f3067d);
        for (List<TransitStep> list2 : this.f3068e) {
            parcel.writeTypedList(list2);
        }
    }
}
