package com.baidu.platform.comapi.p030a;

import android.content.Context;

/* renamed from: com.baidu.platform.comapi.a.a */
public class C1321a {

    /* renamed from: a */
    private static int f4438a = 621133959;

    /* renamed from: a */
    public static boolean m4916a(Context context) {
        return m4918c(context);
    }

    /* renamed from: b */
    private static int m4917b(Context context) {
        try {
            return context.getPackageManager().getPackageInfo("com.baidu.BaiduMap", 64).signatures[0].hashCode();
        } catch (Exception unused) {
            return 0;
        }
    }

    /* renamed from: c */
    private static boolean m4918c(Context context) {
        return m4917b(context) == f4438a;
    }
}
