package com.baidu.location.p013a;

import android.app.ActivityManager;
import android.content.Context;
import android.location.Location;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.p014b.C0775d;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0825d;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0854j;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import com.google.common.base.Ascii;
import com.google.common.net.HttpHeaders;
import com.google.common.primitives.UnsignedBytes;
import com.tamic.novate.util.Utils;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import javax.net.ssl.HttpsURLConnection;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.location.a.d */
public class C0731d {

    /* renamed from: f */
    public static String f1251f = "0";

    /* renamed from: j */
    private static C0731d f1252j;

    /* renamed from: A */
    private C0732a f1253A;

    /* renamed from: B */
    private boolean f1254B;

    /* renamed from: C */
    private boolean f1255C;

    /* renamed from: D */
    private int f1256D;

    /* renamed from: E */
    private float f1257E;

    /* renamed from: F */
    private float f1258F;

    /* renamed from: G */
    private long f1259G;

    /* renamed from: H */
    private int f1260H;

    /* renamed from: I */
    private Handler f1261I;

    /* renamed from: J */
    private byte[] f1262J;

    /* renamed from: K */
    private byte[] f1263K;

    /* renamed from: L */
    private int f1264L;

    /* renamed from: M */
    private List<Byte> f1265M;

    /* renamed from: N */
    private boolean f1266N;

    /* renamed from: a */
    long f1267a;

    /* renamed from: b */
    Location f1268b;

    /* renamed from: c */
    Location f1269c;

    /* renamed from: d */
    StringBuilder f1270d;

    /* renamed from: e */
    long f1271e;

    /* renamed from: g */
    int f1272g;

    /* renamed from: h */
    double f1273h;

    /* renamed from: i */
    double f1274i;

    /* renamed from: k */
    private int f1275k;

    /* renamed from: l */
    private double f1276l;

    /* renamed from: m */
    private String f1277m;

    /* renamed from: n */
    private int f1278n;

    /* renamed from: o */
    private int f1279o;

    /* renamed from: p */
    private int f1280p;

    /* renamed from: q */
    private int f1281q;

    /* renamed from: r */
    private double f1282r;

    /* renamed from: s */
    private double f1283s;

    /* renamed from: t */
    private double f1284t;

    /* renamed from: u */
    private int f1285u;

    /* renamed from: v */
    private int f1286v;

    /* renamed from: w */
    private int f1287w;

    /* renamed from: x */
    private int f1288x;

    /* renamed from: y */
    private int f1289y;

    /* renamed from: z */
    private long f1290z;

    /* renamed from: com.baidu.location.a.d$a */
    class C0732a extends C0849e {

        /* renamed from: a */
        String f1291a = null;

        public C0732a() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1882h = "http://loc.map.baidu.com/cc.php";
            String encode = Jni.encode(this.f1291a);
            this.f1291a = null;
            this.f1885k.put("q", encode);
        }

        /* renamed from: a */
        public void mo10451a(String str) {
            this.f1291a = str;
            mo10733b(C0762v.m1943a().mo10505c());
        }

        /* renamed from: a */
        public void mo10446a(boolean z) {
            if (z && this.f1884j != null) {
                try {
                    JSONObject jSONObject = new JSONObject(this.f1884j);
                    jSONObject.put("prod", C0844b.f1848e);
                    jSONObject.put(Config.DEVICE_UPTIME, System.currentTimeMillis());
                    C0731d.this.m1801e(jSONObject.toString());
                } catch (Exception unused) {
                }
            }
            if (this.f1885k != null) {
                this.f1885k.clear();
            }
        }
    }

    private C0731d() {
        this.f1275k = 1;
        this.f1276l = 0.699999988079071d;
        this.f1277m = "3G|4G";
        this.f1278n = 1;
        this.f1279o = 307200;
        this.f1280p = 15;
        this.f1281q = 1;
        this.f1282r = 3.5d;
        this.f1283s = 3.0d;
        this.f1284t = 0.5d;
        this.f1285u = 300;
        this.f1286v = 60;
        this.f1287w = 0;
        this.f1288x = 60;
        this.f1289y = 0;
        this.f1290z = 0;
        this.f1253A = null;
        this.f1254B = false;
        this.f1255C = false;
        this.f1256D = 0;
        this.f1257E = 0.0f;
        this.f1258F = 0.0f;
        this.f1259G = 0;
        this.f1260H = 500;
        this.f1267a = 0;
        this.f1268b = null;
        this.f1269c = null;
        this.f1270d = null;
        this.f1271e = 0;
        this.f1261I = null;
        this.f1262J = new byte[4];
        this.f1263K = null;
        this.f1264L = 0;
        this.f1265M = null;
        this.f1266N = false;
        this.f1272g = 0;
        this.f1273h = 116.22345545d;
        this.f1274i = 40.245667323d;
        this.f1261I = new Handler();
    }

    /* renamed from: a */
    public static C0731d m1782a() {
        if (f1252j == null) {
            f1252j = new C0731d();
        }
        return f1252j;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m1784a(File file, String str) {
        String uuid = UUID.randomUUID().toString();
        try {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) new URL(str).openConnection();
            httpsURLConnection.setReadTimeout(10000);
            httpsURLConnection.setConnectTimeout(10000);
            httpsURLConnection.setDoInput(true);
            httpsURLConnection.setDoOutput(true);
            httpsURLConnection.setUseCaches(false);
            httpsURLConnection.setRequestMethod("POST");
            httpsURLConnection.setRequestProperty("Charset", "utf-8");
            httpsURLConnection.setRequestProperty("connection", "close");
            httpsURLConnection.setRequestProperty(HttpHeaders.CONTENT_TYPE, Utils.MULTIPART_FORM_DATA + ";boundary=" + uuid);
            if (file == null || !file.exists()) {
                return "0";
            }
            OutputStream outputStream = httpsURLConnection.getOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append("--");
            stringBuffer.append(uuid);
            stringBuffer.append("\r\n");
            stringBuffer.append("Content-Disposition: form-data; name=\"location_dat\"; filename=\"" + file.getName() + "\"" + "\r\n");
            StringBuilder sb = new StringBuilder();
            sb.append("Content-Type: application/octet-stream; charset=utf-8");
            sb.append("\r\n");
            stringBuffer.append(sb.toString());
            stringBuffer.append("\r\n");
            dataOutputStream.write(stringBuffer.toString().getBytes());
            FileInputStream fileInputStream = new FileInputStream(file);
            byte[] bArr = new byte[1024];
            while (true) {
                int read = fileInputStream.read(bArr);
                if (read == -1) {
                    break;
                }
                dataOutputStream.write(bArr, 0, read);
            }
            fileInputStream.close();
            dataOutputStream.write("\r\n".getBytes());
            dataOutputStream.write(("--" + uuid + "--" + "\r\n").getBytes());
            dataOutputStream.flush();
            dataOutputStream.close();
            int responseCode = httpsURLConnection.getResponseCode();
            outputStream.close();
            httpsURLConnection.disconnect();
            this.f1289y += 400;
            m1794c(this.f1289y);
            return responseCode == 200 ? "1" : "0";
        } catch (IOException | MalformedURLException unused) {
            return "0";
        }
    }

    /* renamed from: a */
    private boolean m1787a(String str, Context context) {
        boolean z = false;
        try {
            List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
            if (runningAppProcesses != null) {
                for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                    if (runningAppProcessInfo.processName.equals(str)) {
                        int i = runningAppProcessInfo.importance;
                        if (i == 200 || i == 100) {
                            z = true;
                        }
                    }
                }
            }
        } catch (Exception unused) {
        }
        return z;
    }

    /* renamed from: a */
    private byte[] m1788a(int i) {
        return new byte[]{(byte) (i & 255), (byte) ((65280 & i) >> 8), (byte) ((16711680 & i) >> 16), (byte) ((i & ViewCompat.MEASURED_STATE_MASK) >> 24)};
    }

    /* renamed from: a */
    private byte[] m1789a(String str) {
        if (str == null) {
            return null;
        }
        byte[] bytes = str.getBytes();
        byte nextInt = (byte) new Random().nextInt(255);
        byte nextInt2 = (byte) new Random().nextInt(255);
        byte[] bArr = new byte[(bytes.length + 2)];
        int length = bytes.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            bArr[i2] = (byte) (bytes[i] ^ nextInt);
            i++;
            i2++;
        }
        bArr[i2] = nextInt;
        bArr[i2 + 1] = nextInt2;
        return bArr;
    }

    /* renamed from: b */
    private String m1790b(String str) {
        Calendar instance = Calendar.getInstance();
        return String.format(str, Integer.valueOf(instance.get(1)), Integer.valueOf(instance.get(2) + 1), Integer.valueOf(instance.get(5)));
    }

    /* renamed from: b */
    private void m1791b(int i) {
        byte[] a = m1788a(i);
        for (int i2 = 0; i2 < 4; i2++) {
            this.f1265M.add(Byte.valueOf(a[i2]));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m1792b(Location location) {
        m1795c(location);
        m1805h();
    }

    /* renamed from: c */
    private void m1793c() {
        if (!this.f1266N) {
            this.f1266N = true;
            m1799d(C0844b.f1848e);
            m1807j();
            m1797d();
        }
    }

    /* renamed from: c */
    private void m1794c(int i) {
        if (i != 0) {
            try {
                File file = new File(C0854j.f1898a + "/grtcf.dat");
                if (!file.exists()) {
                    File file2 = new File(C0854j.f1898a);
                    if (!file2.exists()) {
                        file2.mkdirs();
                    }
                    if (file.createNewFile()) {
                        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                        randomAccessFile.seek(2);
                        randomAccessFile.writeInt(0);
                        randomAccessFile.seek(8);
                        byte[] bytes = "1980_01_01:0".getBytes();
                        randomAccessFile.writeInt(bytes.length);
                        randomAccessFile.write(bytes);
                        randomAccessFile.seek(200);
                        randomAccessFile.writeBoolean(false);
                        randomAccessFile.seek(800);
                        randomAccessFile.writeBoolean(false);
                        randomAccessFile.close();
                    } else {
                        return;
                    }
                }
                RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
                randomAccessFile2.seek(8);
                byte[] bytes2 = (m1790b("%d_%02d_%02d") + Config.TRACE_TODAY_VISIT_SPLIT + i).getBytes();
                randomAccessFile2.writeInt(bytes2.length);
                randomAccessFile2.write(bytes2);
                randomAccessFile2.close();
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: c */
    private void m1795c(Location location) {
        if (System.currentTimeMillis() - this.f1267a >= ((long) this.f1260H) && location != null) {
            if (location != null && location.hasSpeed() && location.getSpeed() > this.f1257E) {
                this.f1257E = location.getSpeed();
            }
            try {
                if (this.f1265M == null) {
                    this.f1265M = new ArrayList();
                    m1806i();
                    m1798d(location);
                } else {
                    m1800e(location);
                }
            } catch (Exception unused) {
            }
            this.f1264L++;
        }
    }

    /* renamed from: c */
    private void m1796c(String str) {
        String str2 = str;
        String str3 = Config.DEVICE_UPTIME;
        if (str2 != null) {
            String str4 = "nondrper";
            try {
                JSONObject jSONObject = new JSONObject(str2);
                if (jSONObject.has("on")) {
                    this.f1275k = jSONObject.getInt("on");
                }
                if (jSONObject.has("bash")) {
                    this.f1276l = jSONObject.getDouble("bash");
                }
                if (jSONObject.has("net")) {
                    this.f1277m = jSONObject.getString("net");
                }
                if (jSONObject.has("tcon")) {
                    this.f1278n = jSONObject.getInt("tcon");
                }
                if (jSONObject.has("tcsh")) {
                    this.f1279o = jSONObject.getInt("tcsh");
                }
                if (jSONObject.has("per")) {
                    this.f1280p = jSONObject.getInt("per");
                }
                if (jSONObject.has("chdron")) {
                    this.f1281q = jSONObject.getInt("chdron");
                }
                if (jSONObject.has("spsh")) {
                    this.f1282r = jSONObject.getDouble("spsh");
                }
                if (jSONObject.has("acsh")) {
                    this.f1283s = jSONObject.getDouble("acsh");
                }
                if (jSONObject.has("stspsh")) {
                    this.f1284t = jSONObject.getDouble("stspsh");
                }
                if (jSONObject.has("drstsh")) {
                    this.f1285u = jSONObject.getInt("drstsh");
                }
                if (jSONObject.has("stper")) {
                    this.f1286v = jSONObject.getInt("stper");
                }
                if (jSONObject.has("nondron")) {
                    this.f1287w = jSONObject.getInt("nondron");
                }
                String str5 = str4;
                if (jSONObject.has(str5)) {
                    this.f1288x = jSONObject.getInt(str5);
                }
                String str6 = str3;
                if (jSONObject.has(str6)) {
                    this.f1290z = jSONObject.getLong(str6);
                }
                m1808k();
            } catch (JSONException unused) {
            }
        }
    }

    /* renamed from: d */
    private void m1797d() {
        String[] split = "7.8.2".split("\\.");
        int length = split.length;
        byte[] bArr = this.f1262J;
        int i = 0;
        bArr[0] = 0;
        bArr[1] = 0;
        bArr[2] = 0;
        bArr[3] = 0;
        if (length >= 4) {
            length = 4;
        }
        while (i < length) {
            try {
                this.f1262J[i] = (byte) (Integer.valueOf(split[i]).intValue() & 255);
                i++;
            } catch (Exception unused) {
            }
        }
        this.f1263K = m1789a(C0844b.f1848e + Config.TRACE_TODAY_VISIT_SPLIT + C0844b.m2417a().f1856c);
    }

    /* renamed from: d */
    private void m1798d(Location location) {
        byte b;
        this.f1271e = System.currentTimeMillis();
        m1791b((int) (location.getTime() / 1000));
        m1791b((int) (location.getLongitude() * 1000000.0d));
        m1791b((int) (location.getLatitude() * 1000000.0d));
        char c = !location.hasBearing();
        char c2 = !location.hasSpeed();
        this.f1265M.add(Byte.valueOf(c > 0 ? 32 : (byte) (((byte) (((int) (location.getBearing() / 15.0f)) & 255)) & -33)));
        if (c2 > 0) {
            b = UnsignedBytes.MAX_POWER_OF_TWO;
        } else {
            double speed = (double) location.getSpeed();
            Double.isNaN(speed);
            b = (byte) (((byte) (((int) ((speed * 3.6d) / 4.0d)) & 255)) & Ascii.DEL);
        }
        this.f1265M.add(Byte.valueOf(b));
        this.f1268b = location;
    }

    /* renamed from: d */
    private void m1799d(String str) {
        try {
            File file = new File(C0854j.f1898a + "/grtcf.dat");
            if (file.exists()) {
                RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                randomAccessFile.seek(2);
                int readInt = randomAccessFile.readInt();
                randomAccessFile.seek(8);
                int readInt2 = randomAccessFile.readInt();
                byte[] bArr = new byte[readInt2];
                randomAccessFile.read(bArr, 0, readInt2);
                String str2 = new String(bArr);
                int i = 1;
                if (str2.contains(m1790b("%d_%02d_%02d")) && str2.contains(Config.TRACE_TODAY_VISIT_SPLIT)) {
                    try {
                        String[] split = str2.split(Config.TRACE_TODAY_VISIT_SPLIT);
                        if (split.length > 1) {
                            this.f1289y = Integer.valueOf(split[1]).intValue();
                        }
                    } catch (Exception unused) {
                    }
                }
                while (true) {
                    if (i > readInt) {
                        break;
                    }
                    randomAccessFile.seek((long) (i * 2048));
                    int readInt3 = randomAccessFile.readInt();
                    byte[] bArr2 = new byte[readInt3];
                    randomAccessFile.read(bArr2, 0, readInt3);
                    String str3 = new String(bArr2);
                    if (str != null && str3.contains(str)) {
                        m1796c(str3);
                        break;
                    }
                    i++;
                }
                randomAccessFile.close();
            }
        } catch (Exception unused2) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:30:0x0106, code lost:
        if (r8 > 0) goto L_0x011e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x011c, code lost:
        if (r8 > 0) goto L_0x011e;
     */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1800e(android.location.Location r22) {
        /*
            r21 = this;
            r0 = r21
            r1 = r22
            if (r1 != 0) goto L_0x0007
            return
        L_0x0007:
            double r2 = r22.getLongitude()
            android.location.Location r4 = r0.f1268b
            double r4 = r4.getLongitude()
            double r2 = r2 - r4
            r4 = 4696837146684686336(0x412e848000000000, double:1000000.0)
            double r2 = r2 * r4
            int r2 = (int) r2
            double r6 = r22.getLatitude()
            android.location.Location r3 = r0.f1268b
            double r8 = r3.getLatitude()
            double r6 = r6 - r8
            double r6 = r6 * r4
            int r3 = (int) r6
            boolean r4 = r22.hasBearing()
            r5 = 1
            r4 = r4 ^ r5
            boolean r6 = r22.hasSpeed()
            r6 = r6 ^ r5
            if (r2 <= 0) goto L_0x0037
            r8 = 0
            goto L_0x0038
        L_0x0037:
            r8 = 1
        L_0x0038:
            int r2 = java.lang.Math.abs(r2)
            if (r3 <= 0) goto L_0x0040
            r9 = 0
            goto L_0x0041
        L_0x0040:
            r9 = 1
        L_0x0041:
            int r3 = java.lang.Math.abs(r3)
            int r10 = r0.f1264L
            if (r10 <= r5) goto L_0x0050
            r5 = 0
            r0.f1269c = r5
            android.location.Location r5 = r0.f1268b
            r0.f1269c = r5
        L_0x0050:
            r0.f1268b = r1
            android.location.Location r5 = r0.f1268b
            if (r5 == 0) goto L_0x00c8
            android.location.Location r10 = r0.f1269c
            if (r10 == 0) goto L_0x00c8
            long r10 = r5.getTime()
            android.location.Location r5 = r0.f1269c
            long r12 = r5.getTime()
            int r5 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r5 <= 0) goto L_0x00c8
            android.location.Location r5 = r0.f1268b
            long r10 = r5.getTime()
            android.location.Location r5 = r0.f1269c
            long r12 = r5.getTime()
            long r10 = r10 - r12
            r12 = 5000(0x1388, double:2.4703E-320)
            int r5 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r5 >= 0) goto L_0x00c8
            android.location.Location r5 = r0.f1268b
            long r10 = r5.getTime()
            android.location.Location r5 = r0.f1269c
            long r12 = r5.getTime()
            long r10 = r10 - r12
            r5 = 2
            float[] r5 = new float[r5]
            android.location.Location r12 = r0.f1268b
            double r12 = r12.getAltitude()
            android.location.Location r14 = r0.f1268b
            double r14 = r14.getLongitude()
            android.location.Location r7 = r0.f1269c
            double r16 = r7.getLatitude()
            android.location.Location r7 = r0.f1269c
            double r18 = r7.getLongitude()
            r20 = r5
            android.location.Location.distanceBetween(r12, r14, r16, r18, r20)
            r7 = 1073741824(0x40000000, float:2.0)
            r12 = 0
            r5 = r5[r12]
            android.location.Location r12 = r0.f1269c
            float r12 = r12.getSpeed()
            float r13 = (float) r10
            float r12 = r12 * r13
            float r5 = r5 - r12
            float r5 = r5 * r7
            long r10 = r10 * r10
            float r7 = (float) r10
            float r5 = r5 / r7
            double r10 = (double) r5
            float r5 = r0.f1258F
            double r12 = (double) r5
            int r5 = (r10 > r12 ? 1 : (r10 == r12 ? 0 : -1))
            if (r5 <= 0) goto L_0x00c8
            float r5 = (float) r10
            r0.f1258F = r5
        L_0x00c8:
            java.util.List<java.lang.Byte> r5 = r0.f1265M
            r7 = r2 & 255(0xff, float:3.57E-43)
            byte r7 = (byte) r7
            java.lang.Byte r7 = java.lang.Byte.valueOf(r7)
            r5.add(r7)
            java.util.List<java.lang.Byte> r5 = r0.f1265M
            r7 = 65280(0xff00, float:9.1477E-41)
            r2 = r2 & r7
            int r2 = r2 >> 8
            byte r2 = (byte) r2
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)
            r5.add(r2)
            java.util.List<java.lang.Byte> r2 = r0.f1265M
            r5 = r3 & 255(0xff, float:3.57E-43)
            byte r5 = (byte) r5
            java.lang.Byte r5 = java.lang.Byte.valueOf(r5)
            r2.add(r5)
            java.util.List<java.lang.Byte> r2 = r0.f1265M
            r3 = r3 & r7
            int r3 = r3 >> 8
            byte r3 = (byte) r3
            java.lang.Byte r3 = java.lang.Byte.valueOf(r3)
            r2.add(r3)
            if (r4 <= 0) goto L_0x0109
            r2 = 32
            if (r9 <= 0) goto L_0x0106
            r2 = 96
            byte r2 = (byte) r2
        L_0x0106:
            if (r8 <= 0) goto L_0x0121
            goto L_0x011e
        L_0x0109:
            float r2 = r22.getBearing()
            r3 = 1097859072(0x41700000, float:15.0)
            float r2 = r2 / r3
            int r2 = (int) r2
            r2 = r2 & 255(0xff, float:3.57E-43)
            byte r2 = (byte) r2
            r2 = r2 & 31
            byte r2 = (byte) r2
            if (r9 <= 0) goto L_0x011c
            r2 = r2 | 64
            byte r2 = (byte) r2
        L_0x011c:
            if (r8 <= 0) goto L_0x0121
        L_0x011e:
            r2 = r2 | -128(0xffffffffffffff80, float:NaN)
            byte r2 = (byte) r2
        L_0x0121:
            java.util.List<java.lang.Byte> r3 = r0.f1265M
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)
            r3.add(r2)
            if (r6 <= 0) goto L_0x0138
            java.util.List<java.lang.Byte> r1 = r0.f1265M
            r2 = -128(0xffffffffffffff80, float:NaN)
            java.lang.Byte r2 = java.lang.Byte.valueOf(r2)
            r1.add(r2)
            goto L_0x015a
        L_0x0138:
            float r1 = r22.getSpeed()
            double r1 = (double) r1
            r3 = 4615288898129284301(0x400ccccccccccccd, double:3.6)
            java.lang.Double.isNaN(r1)
            double r1 = r1 * r3
            r3 = 4616189618054758400(0x4010000000000000, double:4.0)
            double r1 = r1 / r3
            int r1 = (int) r1
            r1 = r1 & 255(0xff, float:3.57E-43)
            byte r1 = (byte) r1
            r1 = r1 & 127(0x7f, float:1.78E-43)
            byte r1 = (byte) r1
            java.util.List<java.lang.Byte> r2 = r0.f1265M
            java.lang.Byte r1 = java.lang.Byte.valueOf(r1)
            r2.add(r1)
        L_0x015a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0731d.m1800e(android.location.Location):void");
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m1801e(String str) {
        try {
            File file = new File(C0854j.f1898a + "/grtcf.dat");
            if (!file.exists()) {
                File file2 = new File(C0854j.f1898a);
                if (!file2.exists()) {
                    file2.mkdirs();
                }
                if (file.createNewFile()) {
                    RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
                    randomAccessFile.seek(2);
                    randomAccessFile.writeInt(0);
                    randomAccessFile.seek(8);
                    byte[] bytes = "1980_01_01:0".getBytes();
                    randomAccessFile.writeInt(bytes.length);
                    randomAccessFile.write(bytes);
                    randomAccessFile.seek(200);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.seek(800);
                    randomAccessFile.writeBoolean(false);
                    randomAccessFile.close();
                } else {
                    return;
                }
            }
            RandomAccessFile randomAccessFile2 = new RandomAccessFile(file, "rw");
            randomAccessFile2.seek(2);
            int readInt = randomAccessFile2.readInt();
            int i = 1;
            while (true) {
                if (i > readInt) {
                    break;
                }
                randomAccessFile2.seek((long) (i * 2048));
                int readInt2 = randomAccessFile2.readInt();
                byte[] bArr = new byte[readInt2];
                randomAccessFile2.read(bArr, 0, readInt2);
                if (new String(bArr).contains(C0844b.f1848e)) {
                    break;
                }
                i++;
            }
            if (i >= readInt) {
                randomAccessFile2.seek(2);
                randomAccessFile2.writeInt(i);
            }
            randomAccessFile2.seek((long) (i * 2048));
            byte[] bytes2 = str.getBytes();
            randomAccessFile2.writeInt(bytes2.length);
            randomAccessFile2.write(bytes2);
            randomAccessFile2.close();
        } catch (Exception unused) {
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v1, resolved type: java.io.RandomAccessFile} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.nio.channels.FileChannel} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: java.nio.channels.FileLock} */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0056 A[SYNTHETIC, Splitter:B:27:0x0056] */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005b A[Catch:{ Exception -> 0x005e }] */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0061 A[SYNTHETIC, Splitter:B:35:0x0061] */
    /* renamed from: e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m1802e() {
        /*
            r5 = this;
            r0 = 0
            r1 = 0
            java.io.File r2 = new java.io.File     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            r3.<init>()     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.lang.String r4 = com.baidu.location.p019g.C0855k.m2471i()     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            r3.append(r4)     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.lang.String r4 = java.io.File.separator     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            r3.append(r4)     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.lang.String r4 = "gflk.dat"
            r3.append(r4)     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            if (r3 != 0) goto L_0x002a
            r2.createNewFile()     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
        L_0x002a:
            java.io.RandomAccessFile r3 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.lang.String r4 = "rw"
            r3.<init>(r2, r4)     // Catch:{ Exception -> 0x005f, all -> 0x0052 }
            java.nio.channels.FileChannel r2 = r3.getChannel()     // Catch:{ Exception -> 0x0050, all -> 0x004e }
            java.nio.channels.FileLock r0 = r2.tryLock()     // Catch:{ Exception -> 0x003d, all -> 0x003a }
            goto L_0x003e
        L_0x003a:
            r1 = move-exception
            r0 = r2
            goto L_0x0054
        L_0x003d:
            r1 = 1
        L_0x003e:
            if (r0 == 0) goto L_0x0043
            r0.release()     // Catch:{ Exception -> 0x0064 }
        L_0x0043:
            if (r2 == 0) goto L_0x0048
            r2.close()     // Catch:{ Exception -> 0x0064 }
        L_0x0048:
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ Exception -> 0x0064 }
            goto L_0x0064
        L_0x004e:
            r1 = move-exception
            goto L_0x0054
        L_0x0050:
            r0 = r3
            goto L_0x005f
        L_0x0052:
            r1 = move-exception
            r3 = r0
        L_0x0054:
            if (r0 == 0) goto L_0x0059
            r0.close()     // Catch:{ Exception -> 0x005e }
        L_0x0059:
            if (r3 == 0) goto L_0x005e
            r3.close()     // Catch:{ Exception -> 0x005e }
        L_0x005e:
            throw r1
        L_0x005f:
            if (r0 == 0) goto L_0x0064
            r0.close()     // Catch:{ Exception -> 0x0064 }
        L_0x0064:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p013a.C0731d.m1802e():boolean");
    }

    /* renamed from: f */
    private boolean m1803f() {
        if (this.f1254B) {
            if (this.f1255C) {
                if (((double) this.f1257E) < this.f1284t) {
                    this.f1256D += this.f1280p;
                    if (this.f1256D <= this.f1285u || System.currentTimeMillis() - this.f1259G > ((long) (this.f1286v * 1000))) {
                        return true;
                    }
                } else {
                    this.f1256D = 0;
                    this.f1255C = false;
                    return true;
                }
            } else if (((double) this.f1257E) >= this.f1284t) {
                return true;
            } else {
                this.f1255C = true;
                this.f1256D = 0;
                this.f1256D += this.f1280p;
                return true;
            }
        } else if (((double) this.f1257E) < this.f1282r && ((double) this.f1258F) < this.f1283s) {
            return this.f1287w == 1 && System.currentTimeMillis() - this.f1259G > ((long) (this.f1288x * 1000));
        } else {
            this.f1254B = true;
            return true;
        }
    }

    /* renamed from: g */
    private void m1804g() {
        this.f1265M = null;
        this.f1271e = 0;
        this.f1264L = 0;
        this.f1268b = null;
        this.f1269c = null;
        this.f1257E = 0.0f;
        this.f1258F = 0.0f;
    }

    /* renamed from: h */
    private void m1805h() {
        if (this.f1271e != 0 && System.currentTimeMillis() - this.f1271e >= ((long) (this.f1280p * 1000))) {
            if (C0839f.getServiceContext().getSharedPreferences("loc_navi_mode", 4).getBoolean("is_navi_on", false)) {
                m1804g();
            } else if (this.f1278n != 1 || m1803f()) {
                if (!C0844b.f1848e.equals("com.ubercab.driver")) {
                    if (!m1787a(C0844b.f1848e, C0839f.getServiceContext())) {
                        m1804g();
                        return;
                    }
                } else if (m1802e()) {
                    m1804g();
                    return;
                }
                List<Byte> list = this.f1265M;
                if (list != null) {
                    int size = list.size();
                    this.f1265M.set(0, Byte.valueOf((byte) (size & 255)));
                    this.f1265M.set(1, Byte.valueOf((byte) ((65280 & size) >> 8)));
                    this.f1265M.set(3, Byte.valueOf((byte) (this.f1264L & 255)));
                    byte[] bArr = new byte[size];
                    for (int i = 0; i < size; i++) {
                        bArr[i] = this.f1265M.get(i).byteValue();
                    }
                    File file = new File(C0855k.m2473k(), "baidu/tempdata");
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    if (file.exists()) {
                        File file2 = new File(file, "intime.dat");
                        if (file2.exists()) {
                            file2.delete();
                        }
                        try {
                            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file2));
                            bufferedOutputStream.write(bArr);
                            bufferedOutputStream.flush();
                            bufferedOutputStream.close();
                            new C0734f(this).start();
                        } catch (Exception unused) {
                        }
                    }
                    m1804g();
                    this.f1259G = System.currentTimeMillis();
                }
            } else {
                m1804g();
            }
        }
    }

    /* renamed from: i */
    private void m1806i() {
        byte b;
        List<Byte> list;
        this.f1265M.add((byte) 0);
        this.f1265M.add((byte) 0);
        if (f1251f.equals("0")) {
            list = this.f1265M;
            b = -82;
        } else {
            list = this.f1265M;
            b = -66;
        }
        list.add(Byte.valueOf(b));
        this.f1265M.add((byte) 0);
        this.f1265M.add(Byte.valueOf(this.f1262J[0]));
        this.f1265M.add(Byte.valueOf(this.f1262J[1]));
        this.f1265M.add(Byte.valueOf(this.f1262J[2]));
        this.f1265M.add(Byte.valueOf(this.f1262J[3]));
        int length = this.f1263K.length;
        this.f1265M.add(Byte.valueOf((byte) ((length + 1) & 255)));
        for (int i = 0; i < length; i++) {
            this.f1265M.add(Byte.valueOf(this.f1263K[i]));
        }
    }

    /* renamed from: j */
    private void m1807j() {
        if (System.currentTimeMillis() - this.f1290z > 86400000) {
            if (this.f1253A == null) {
                this.f1253A = new C0732a();
            }
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer.append(C0844b.m2417a().mo10713a(false));
            stringBuffer.append(C0723a.m1729a().mo10432d());
            this.f1253A.mo10451a(stringBuffer.toString());
        }
        m1808k();
    }

    /* renamed from: k */
    private void m1808k() {
    }

    /* renamed from: a */
    public void mo10449a(Location location) {
        if (!this.f1266N) {
            m1793c();
        }
        boolean z = ((double) C0775d.m2005a().mo10533f()) < this.f1276l * 100.0d;
        if (this.f1275k == 1 && z && this.f1277m.contains(C0825d.m2298a(C0822b.m2279a().mo10630e()))) {
            if (this.f1278n != 1 || this.f1289y <= this.f1279o) {
                this.f1261I.post(new C0733e(this, location));
            }
        }
    }

    /* renamed from: b */
    public void mo10450b() {
        if (this.f1266N) {
            this.f1266N = false;
            m1804g();
        }
    }
}
