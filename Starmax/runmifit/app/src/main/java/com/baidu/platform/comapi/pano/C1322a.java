package com.baidu.platform.comapi.pano;

import android.net.Uri;
import android.text.TextUtils;
import com.baidu.mapapi.http.AsyncHttpClient;
import com.baidu.mapapi.http.HttpClient;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import com.baidu.mobstat.Config;
import com.tencent.tauth.AuthActivity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.comapi.pano.a */
public class C1322a {

    /* renamed from: a */
    AsyncHttpClient f4444a = new AsyncHttpClient();

    /* renamed from: com.baidu.platform.comapi.pano.a$a */
    public interface C1323a<T> {
        /* renamed from: a */
        void mo12839a(HttpClient.HttpStateError httpStateError);

        /* renamed from: a */
        void mo12841a(Object obj);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public C1325c m4920a(String str) {
        if (str == null || str.equals("")) {
            return new C1325c(PanoStateError.PANO_NOT_FOUND);
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject optJSONObject = jSONObject.optJSONObject("result");
            if (optJSONObject == null) {
                return new C1325c(PanoStateError.PANO_NOT_FOUND);
            }
            if (optJSONObject.optInt("error") != 0) {
                return new C1325c(PanoStateError.PANO_UID_ERROR);
            }
            JSONArray optJSONArray = jSONObject.optJSONArray("content");
            if (optJSONArray == null) {
                return new C1325c(PanoStateError.PANO_NOT_FOUND);
            }
            C1325c cVar = null;
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject optJSONObject2 = optJSONArray.optJSONObject(i).optJSONObject("poiinfo");
                if (optJSONObject2 != null) {
                    cVar = new C1325c(PanoStateError.PANO_NO_ERROR);
                    cVar.mo14031a(optJSONObject2.optString("PID"));
                    cVar.mo14030a(optJSONObject2.optInt("hasstreet"));
                }
            }
            return cVar;
        } catch (JSONException e) {
            e.printStackTrace();
            return new C1325c(PanoStateError.PANO_NOT_FOUND);
        }
    }

    /* renamed from: a */
    private String m4921a(Uri.Builder builder) {
        String uri = builder.build().toString();
        Uri.Builder buildUpon = Uri.parse(uri + HttpClient.getPhoneInfo()).buildUpon();
        buildUpon.appendQueryParameter("sign", AppMD5.getSignMD5String(buildUpon.build().getEncodedQuery()));
        return buildUpon.build().toString();
    }

    /* renamed from: a */
    private void m4922a(Uri.Builder builder, String str, String str2) {
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
            builder.appendQueryParameter(str, str2);
        }
    }

    /* renamed from: a */
    public void mo14028a(String str, C1323a<C1325c> aVar) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(HttpClient.isHttpsEnable ? "https" : "http");
        builder.encodedAuthority("api.map.baidu.com");
        builder.path("/sdkproxy/lbs_androidsdk/pano/v1/");
        m4922a(builder, "qt", "poi");
        m4922a(builder, Config.CUSTOM_USER_ID, str);
        m4922a(builder, AuthActivity.ACTION_KEY, "0");
        String authToken = HttpClient.getAuthToken();
        if (authToken == null) {
            aVar.mo12841a(new C1325c(PanoStateError.PANO_NO_TOKEN));
            return;
        }
        m4922a(builder, "token", authToken);
        this.f4444a.get(m4921a(builder), new C1324b(this, aVar));
    }
}
