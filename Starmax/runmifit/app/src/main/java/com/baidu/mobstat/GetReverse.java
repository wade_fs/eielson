package com.baidu.mobstat;

import android.content.Context;

public class GetReverse {

    /* renamed from: a */
    private static ICooperService f4145a;

    private GetReverse() {
    }

    public static ICooperService getCooperService(Context context) {
        if (f4145a == null) {
            f4145a = CooperService.instance();
        }
        return f4145a;
    }
}
