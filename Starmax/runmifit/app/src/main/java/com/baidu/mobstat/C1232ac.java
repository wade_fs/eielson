package com.baidu.mobstat;

import android.content.Context;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.ac */
class C1232ac implements C1288h {

    /* renamed from: a */
    private C1237ag f4241a = C1237ag.f4266a;

    C1232ac() {
    }

    /* renamed from: a */
    public void mo13884a(Context context, JSONObject jSONObject) {
        this.f4241a.mo13909a(context, jSONObject);
    }

    /* renamed from: a */
    public void mo13883a(Context context, String str) {
        this.f4241a.mo13908a(context, str);
    }

    /* renamed from: b */
    public void mo13886b(Context context, String str) {
        this.f4241a.mo13911b(context, str);
    }

    /* renamed from: a */
    public void mo13882a(Context context, long j) {
        this.f4241a.mo13907a(context, j);
    }

    /* renamed from: a */
    public boolean mo13885a(Context context) {
        return this.f4241a.mo13910a(context);
    }

    /* renamed from: b */
    public boolean mo13887b(Context context) {
        return this.f4241a.mo13912b(context);
    }
}
