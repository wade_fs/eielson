package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Bundle;
import android.view.View;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.ParcelItem;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;
import com.google.android.gms.common.internal.ImagesContract;
import com.google.common.primitives.UnsignedBytes;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Iterator;

public final class Marker extends Overlay {

    /* renamed from: a */
    LatLng f2608a;

    /* renamed from: b */
    BitmapDescriptor f2609b;

    /* renamed from: c */
    float f2610c;

    /* renamed from: d */
    float f2611d;

    /* renamed from: e */
    boolean f2612e;

    /* renamed from: f */
    boolean f2613f;

    /* renamed from: g */
    float f2614g;

    /* renamed from: h */
    String f2615h;

    /* renamed from: i */
    int f2616i;

    /* renamed from: j */
    boolean f2617j = false;

    /* renamed from: k */
    boolean f2618k = false;

    /* renamed from: l */
    float f2619l;

    /* renamed from: m */
    int f2620m;

    /* renamed from: n */
    boolean f2621n = false;

    /* renamed from: o */
    ArrayList<BitmapDescriptor> f2622o;

    /* renamed from: p */
    int f2623p = 20;

    /* renamed from: q */
    Animation f2624q;

    /* renamed from: r */
    float f2625r = 1.0f;

    /* renamed from: s */
    float f2626s = 1.0f;

    /* renamed from: t */
    float f2627t = 1.0f;

    /* renamed from: u */
    Point f2628u;

    /* renamed from: v */
    InfoWindow f2629v;

    /* renamed from: w */
    InfoWindow.C0920a f2630w;

    /* renamed from: x */
    boolean f2631x = false;

    Marker() {
        this.type = C1082h.marker;
    }

    /* renamed from: a */
    private void m2935a(ArrayList<BitmapDescriptor> arrayList, Bundle bundle) {
        int i;
        ArrayList arrayList2 = new ArrayList();
        Iterator<BitmapDescriptor> it = arrayList.iterator();
        while (true) {
            i = 0;
            if (!it.hasNext()) {
                break;
            }
            ParcelItem parcelItem = new ParcelItem();
            Bundle bundle2 = new Bundle();
            Bitmap bitmap = it.next().f2437a;
            ByteBuffer allocate = ByteBuffer.allocate(bitmap.getWidth() * bitmap.getHeight() * 4);
            bitmap.copyPixelsToBuffer(allocate);
            byte[] array = allocate.array();
            bundle2.putByteArray(ImagesContract.IMAGE_DATA, array);
            bundle2.putInt("image_width", bitmap.getWidth());
            bundle2.putInt("image_height", bitmap.getHeight());
            MessageDigest messageDigest = null;
            try {
                messageDigest = MessageDigest.getInstance("MD5");
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            if (messageDigest != null) {
                messageDigest.update(array, 0, array.length);
                byte[] digest = messageDigest.digest();
                StringBuilder sb = new StringBuilder("");
                while (i < digest.length) {
                    sb.append(Integer.toString((digest[i] & UnsignedBytes.MAX_VALUE) + 256, 16).substring(1));
                    i++;
                }
                bundle2.putString("image_hashcode", sb.toString());
            }
            parcelItem.setBundle(bundle2);
            arrayList2.add(parcelItem);
        }
        if (arrayList2.size() > 0) {
            ParcelItem[] parcelItemArr = new ParcelItem[arrayList2.size()];
            while (i < arrayList2.size()) {
                parcelItemArr[i] = (ParcelItem) arrayList2.get(i);
                i++;
            }
            bundle.putParcelableArray("icons", parcelItemArr);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        Bundle bundle2 = new Bundle();
        BitmapDescriptor bitmapDescriptor = this.f2609b;
        if (bitmapDescriptor != null) {
            bundle.putBundle("image_info", bitmapDescriptor.mo11047b());
        }
        GeoPoint ll2mc = CoordUtil.ll2mc(this.f2608a);
        bundle.putInt("animatetype", this.f2620m);
        bundle.putDouble("location_x", ll2mc.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc.getLatitudeE6());
        bundle.putInt("perspective", this.f2612e ? 1 : 0);
        bundle.putFloat("anchor_x", this.f2610c);
        bundle.putFloat("anchor_y", this.f2611d);
        bundle.putFloat("rotate", this.f2614g);
        bundle.putInt("y_offset", this.f2616i);
        bundle.putInt("isflat", this.f2617j ? 1 : 0);
        bundle.putInt("istop", this.f2618k ? 1 : 0);
        bundle.putInt("period", this.f2623p);
        bundle.putFloat("alpha", this.f2619l);
        bundle.putFloat("scaleX", this.f2625r);
        bundle.putFloat("scaleY", this.f2626s);
        Point point = this.f2628u;
        if (point != null) {
            bundle.putInt("fix_x", point.x);
            bundle.putInt("fix_y", this.f2628u.y);
        }
        bundle.putInt("isfixed", this.f2621n ? 1 : 0);
        ArrayList<BitmapDescriptor> arrayList = this.f2622o;
        if (arrayList != null && arrayList.size() > 0) {
            m2935a(this.f2622o, bundle);
        }
        bundle2.putBundle("param", bundle);
        return bundle;
    }

    public void cancelAnimation() {
        Animation animation = this.f2624q;
        if (animation != null) {
            animation.bdAnimation.mo12881b();
        }
    }

    public float getAlpha() {
        return this.f2619l;
    }

    public float getAnchorX() {
        return this.f2610c;
    }

    public float getAnchorY() {
        return this.f2611d;
    }

    public Point getFixedPosition() {
        return this.f2628u;
    }

    public BitmapDescriptor getIcon() {
        return this.f2609b;
    }

    public ArrayList<BitmapDescriptor> getIcons() {
        return this.f2622o;
    }

    public String getId() {
        return this.f2664y;
    }

    public InfoWindow getInfoWindow() {
        return this.f2629v;
    }

    public int getPeriod() {
        return this.f2623p;
    }

    public LatLng getPosition() {
        return this.f2608a;
    }

    public float getRotate() {
        return this.f2614g;
    }

    public float getScale() {
        return this.f2627t;
    }

    public float getScaleX() {
        return this.f2625r;
    }

    public float getScaleY() {
        return this.f2626s;
    }

    public String getTitle() {
        return this.f2615h;
    }

    public int getYOffset() {
        return this.f2616i;
    }

    public void hideInfoWindow() {
        InfoWindow.C0920a aVar = this.f2630w;
        if (aVar != null) {
            aVar.mo11150a(this.f2629v);
            this.f2631x = false;
        }
    }

    public boolean isDraggable() {
        return this.f2613f;
    }

    public boolean isFixed() {
        return this.f2621n;
    }

    public boolean isFlat() {
        return this.f2617j;
    }

    public boolean isInfoWindowEnabled() {
        return this.f2631x;
    }

    public boolean isPerspective() {
        return this.f2612e;
    }

    public void setAlpha(float f) {
        if (f < 0.0f || ((double) f) > 1.0d) {
            this.f2619l = 1.0f;
            return;
        }
        this.f2619l = f;
        this.listener.mo11330b(super);
    }

    public void setAnchor(float f, float f2) {
        if (f >= 0.0f && f <= 1.0f && f2 >= 0.0f && f2 <= 1.0f) {
            this.f2610c = f;
            this.f2611d = f2;
            this.listener.mo11330b(super);
        }
    }

    public void setAnimateType(int i) {
        this.f2620m = i;
        this.listener.mo11330b(super);
    }

    public void setAnimation(Animation animation) {
        if (animation != null) {
            this.f2624q = animation;
            this.f2624q.bdAnimation.mo12880a(this, animation);
        }
    }

    public void setDraggable(boolean z) {
        this.f2613f = z;
        this.listener.mo11330b(super);
    }

    public void setFixedScreenPosition(Point point) {
        if (point != null) {
            this.f2628u = point;
            this.f2621n = true;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: the screenPosition can not be null");
    }

    public void setFlat(boolean z) {
        this.f2617j = z;
        this.listener.mo11330b(super);
    }

    public void setIcon(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f2609b = bitmapDescriptor;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's icon can not be null");
    }

    public void setIcons(ArrayList<BitmapDescriptor> arrayList) {
        BitmapDescriptor bitmapDescriptor;
        if (arrayList == null) {
            throw new IllegalArgumentException("BDMapSDKException: marker's icons can not be null");
        } else if (arrayList.size() != 0) {
            int i = 0;
            if (arrayList.size() == 1) {
                bitmapDescriptor = arrayList.get(0);
            } else {
                while (i < arrayList.size()) {
                    if (arrayList.get(i) != null && arrayList.get(i).f2437a != null) {
                        i++;
                    } else {
                        return;
                    }
                }
                this.f2622o = (ArrayList) arrayList.clone();
                bitmapDescriptor = null;
            }
            this.f2609b = bitmapDescriptor;
            this.listener.mo11330b(super);
        }
    }

    public void setPeriod(int i) {
        if (i > 0) {
            this.f2623p = i;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's period must be greater than zero ");
    }

    public void setPerspective(boolean z) {
        this.f2612e = z;
        this.listener.mo11330b(super);
    }

    public void setPosition(LatLng latLng) {
        if (latLng != null) {
            this.f2608a = latLng;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's position can not be null");
    }

    public void setPositionWithInfoWindow(LatLng latLng) {
        if (latLng != null) {
            this.f2608a = latLng;
            this.listener.mo11330b(super);
            InfoWindow infoWindow = this.f2629v;
            if (infoWindow != null) {
                infoWindow.setPosition(latLng);
                return;
            }
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: marker's position can not be null");
    }

    public void setRotate(float f) {
        while (f < 0.0f) {
            f += 360.0f;
        }
        this.f2614g = f % 360.0f;
        this.listener.mo11330b(super);
    }

    public void setScale(float f) {
        if (f < 0.0f) {
            f = 0.0f;
        }
        this.f2625r = f;
        this.f2626s = f;
        this.listener.mo11330b(super);
    }

    public void setScaleX(float f) {
        if (f < 0.0f) {
            f = 0.0f;
        }
        this.f2625r = f;
        this.listener.mo11330b(super);
    }

    public void setScaleY(float f) {
        if (f < 0.0f) {
            f = 0.0f;
        }
        this.f2626s = f;
        this.listener.mo11330b(super);
    }

    public void setTitle(String str) {
        this.f2615h = str;
    }

    public void setToTop() {
        this.f2618k = true;
        this.listener.mo11330b(super);
    }

    public void setYOffset(int i) {
        this.f2616i = i;
        this.listener.mo11330b(super);
    }

    public void showInfoWindow(InfoWindow infoWindow) {
        if (infoWindow != null) {
            this.f2629v = infoWindow;
            InfoWindow.C0920a aVar = this.f2630w;
            if (aVar != null) {
                aVar.mo11151b(infoWindow);
                this.f2631x = true;
                return;
            }
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: the InfoWindow can not be null");
    }

    public void showSmoothMoveInfoWindow(InfoWindow infoWindow) {
        if (infoWindow != null) {
            if (!infoWindow.f2521j) {
                throw new IllegalArgumentException("BDMapSDKException: the SmoothMoveInfoWindow must build with View");
            } else if (infoWindow.f2513b != null) {
                this.f2629v = infoWindow;
                this.f2629v.f2520i = true;
                InfoWindow.C0920a aVar = this.f2630w;
                if (aVar != null) {
                    aVar.mo11151b(infoWindow);
                    this.f2631x = true;
                }
            } else {
                throw new IllegalArgumentException("BDMapSDKException: the SmoothMoveInfoWindow's View can not be null");
            }
        }
    }

    public void startAnimation() {
        Animation animation = this.f2624q;
        if (animation != null) {
            animation.bdAnimation.mo12874a();
        }
    }

    public void updateInfoWindowBitmapDescriptor(BitmapDescriptor bitmapDescriptor) {
        InfoWindow infoWindow = this.f2629v;
        if (infoWindow != null && !infoWindow.f2522k) {
            this.f2629v.setBitmapDescriptor(bitmapDescriptor);
        }
    }

    public void updateInfoWindowPosition(LatLng latLng) {
        InfoWindow infoWindow = this.f2629v;
        if (infoWindow != null) {
            infoWindow.setPosition(latLng);
        }
    }

    public void updateInfoWindowView(View view) {
        InfoWindow infoWindow = this.f2629v;
        if (infoWindow != null && infoWindow.f2521j) {
            this.f2629v.setView(view);
        }
    }

    public void updateInfoWindowYOffset(int i) {
        InfoWindow infoWindow = this.f2629v;
        if (infoWindow != null) {
            infoWindow.setYOffset(i);
        }
    }
}
