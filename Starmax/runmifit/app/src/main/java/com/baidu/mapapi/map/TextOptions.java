package com.baidu.mapapi.map;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import com.baidu.mapapi.model.LatLng;

public final class TextOptions extends OverlayOptions {
    public static final int ALIGN_BOTTOM = 16;
    public static final int ALIGN_CENTER_HORIZONTAL = 4;
    public static final int ALIGN_CENTER_VERTICAL = 32;
    public static final int ALIGN_LEFT = 1;
    public static final int ALIGN_RIGHT = 2;
    public static final int ALIGN_TOP = 8;

    /* renamed from: a */
    int f2730a;

    /* renamed from: b */
    boolean f2731b = true;

    /* renamed from: c */
    Bundle f2732c;

    /* renamed from: d */
    private String f2733d;

    /* renamed from: e */
    private LatLng f2734e;

    /* renamed from: f */
    private int f2735f;

    /* renamed from: g */
    private int f2736g = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: h */
    private int f2737h = 12;

    /* renamed from: i */
    private Typeface f2738i;

    /* renamed from: j */
    private int f2739j = 4;

    /* renamed from: k */
    private int f2740k = 32;

    /* renamed from: l */
    private float f2741l;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Text text = new Text();
        text.f2662A = this.f2731b;
        text.f2665z = this.f2730a;
        text.f2663B = this.f2732c;
        text.f2720a = this.f2733d;
        text.f2721b = this.f2734e;
        text.f2722c = this.f2735f;
        text.f2723d = this.f2736g;
        text.f2724e = this.f2737h;
        text.f2725f = this.f2738i;
        text.f2726g = this.f2739j;
        text.f2727h = this.f2740k;
        text.f2728i = this.f2741l;
        return text;
    }

    public TextOptions align(int i, int i2) {
        this.f2739j = i;
        this.f2740k = i2;
        return this;
    }

    public TextOptions bgColor(int i) {
        this.f2735f = i;
        return this;
    }

    public TextOptions extraInfo(Bundle bundle) {
        this.f2732c = bundle;
        return this;
    }

    public TextOptions fontColor(int i) {
        this.f2736g = i;
        return this;
    }

    public TextOptions fontSize(int i) {
        this.f2737h = i;
        return this;
    }

    public float getAlignX() {
        return (float) this.f2739j;
    }

    public float getAlignY() {
        return (float) this.f2740k;
    }

    public int getBgColor() {
        return this.f2735f;
    }

    public Bundle getExtraInfo() {
        return this.f2732c;
    }

    public int getFontColor() {
        return this.f2736g;
    }

    public int getFontSize() {
        return this.f2737h;
    }

    public LatLng getPosition() {
        return this.f2734e;
    }

    public float getRotate() {
        return this.f2741l;
    }

    public String getText() {
        return this.f2733d;
    }

    public Typeface getTypeface() {
        return this.f2738i;
    }

    public int getZIndex() {
        return this.f2730a;
    }

    public boolean isVisible() {
        return this.f2731b;
    }

    public TextOptions position(LatLng latLng) {
        if (latLng != null) {
            this.f2734e = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: position can not be null");
    }

    public TextOptions rotate(float f) {
        this.f2741l = f;
        return this;
    }

    public TextOptions text(String str) {
        if (str == null || str.equals("")) {
            throw new IllegalArgumentException("BDMapSDKException: text can not be null or empty");
        }
        this.f2733d = str;
        return this;
    }

    public TextOptions typeface(Typeface typeface) {
        this.f2738i = typeface;
        return this;
    }

    public TextOptions visible(boolean z) {
        this.f2731b = z;
        return this;
    }

    public TextOptions zIndex(int i) {
        this.f2730a = i;
        return this;
    }
}
