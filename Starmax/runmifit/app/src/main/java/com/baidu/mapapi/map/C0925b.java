package com.baidu.mapapi.map;

import com.baidu.mapapi.map.InfoWindow;

/* renamed from: com.baidu.mapapi.map.b */
class C0925b implements InfoWindow.C0920a {

    /* renamed from: a */
    final /* synthetic */ BaiduMap f2836a;

    C0925b(BaiduMap baiduMap) {
        this.f2836a = baiduMap;
    }

    /* renamed from: a */
    public void mo11150a(InfoWindow infoWindow) {
        this.f2836a.hideInfoWindow(infoWindow);
    }

    /* renamed from: b */
    public void mo11151b(InfoWindow infoWindow) {
        this.f2836a.m2828a(infoWindow);
    }
}
