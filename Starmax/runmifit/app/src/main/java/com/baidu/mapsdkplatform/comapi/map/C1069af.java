package com.baidu.mapsdkplatform.comapi.map;

import android.graphics.Point;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comjni.map.basemap.C1177a;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.af */
public class C1069af {

    /* renamed from: a */
    private C1177a f3452a;

    public C1069af(C1177a aVar) {
        this.f3452a = aVar;
    }

    /* renamed from: a */
    public Point mo12972a(GeoPoint geoPoint) {
        if (geoPoint == null) {
            return null;
        }
        Point point = new Point(0, 0);
        String b = this.f3452a.mo13488b((int) geoPoint.getLongitudeE6(), (int) geoPoint.getLatitudeE6());
        if (b != null && !b.isEmpty()) {
            try {
                JSONObject jSONObject = new JSONObject(b);
                point.x = jSONObject.getInt("scrx");
                point.y = jSONObject.getInt("scry");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return point;
    }

    /* renamed from: a */
    public GeoPoint mo12973a(int i, int i2) {
        GeoPoint geoPoint = new GeoPoint(0.0d, 0.0d);
        String a = this.f3452a.mo13470a(i, i2);
        if (a != null && !a.isEmpty()) {
            try {
                JSONObject jSONObject = new JSONObject(a);
                geoPoint.setLongitudeE6((double) jSONObject.getInt("geox"));
                geoPoint.setLatitudeE6((double) jSONObject.getInt("geoy"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return geoPoint;
    }
}
