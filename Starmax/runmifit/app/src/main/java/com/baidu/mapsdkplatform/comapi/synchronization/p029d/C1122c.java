package com.baidu.mapsdkplatform.comapi.synchronization.p029d;

import android.text.TextUtils;
import com.google.common.base.Ascii;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.d.c */
public final class C1122c {

    /* renamed from: a */
    private static final String f3698a = C1122c.class.getSimpleName();

    /* renamed from: a */
    public static String m3858a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            return m3859a(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            C1120a.m3852a(f3698a, "NoSuchAlgorithmException happened when get MD5 string", e);
            return null;
        }
    }

    /* renamed from: a */
    private static String m3859a(byte[] bArr) {
        char[] cArr = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] cArr2 = new char[(bArr.length * 2)];
        int i = 0;
        for (byte b : bArr) {
            int i2 = i + 1;
            cArr2[i] = cArr[(b >>> 4) & 15];
            i = i2 + 1;
            cArr2[i2] = cArr[b & Ascii.f4512SI];
        }
        return new String(cArr2).toLowerCase();
    }
}
