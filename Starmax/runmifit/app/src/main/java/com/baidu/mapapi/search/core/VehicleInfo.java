package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

public class VehicleInfo implements Parcelable {
    public static final Parcelable.Creator<VehicleInfo> CREATOR = new C0973q();

    /* renamed from: a */
    private String f2959a;

    /* renamed from: b */
    private int f2960b;

    /* renamed from: c */
    private String f2961c;

    /* renamed from: d */
    private int f2962d;

    /* renamed from: e */
    private int f2963e;

    public VehicleInfo() {
    }

    protected VehicleInfo(Parcel parcel) {
        this.f2959a = parcel.readString();
        this.f2960b = parcel.readInt();
        this.f2961c = parcel.readString();
        this.f2962d = parcel.readInt();
        this.f2963e = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public int getPassStationNum() {
        return this.f2960b;
    }

    public String getTitle() {
        return this.f2961c;
    }

    public int getTotalPrice() {
        return this.f2963e;
    }

    public String getUid() {
        return this.f2959a;
    }

    public int getZonePrice() {
        return this.f2962d;
    }

    public void setPassStationNum(int i) {
        this.f2960b = i;
    }

    public void setTitle(String str) {
        this.f2961c = str;
    }

    public void setTotalPrice(int i) {
        this.f2963e = i;
    }

    public void setUid(String str) {
        this.f2959a = str;
    }

    public void setZonePrice(int i) {
        this.f2962d = i;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2959a);
        parcel.writeInt(this.f2960b);
        parcel.writeString(this.f2961c);
        parcel.writeInt(this.f2962d);
        parcel.writeInt(this.f2963e);
    }
}
