package com.baidu.mobstat;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.ClientCertRequest;
import android.webkit.ConsoleMessage;
import android.webkit.GeolocationPermissions;
import android.webkit.HttpAuthHandler;
import android.webkit.JsPromptResult;
import android.webkit.JsResult;
import android.webkit.PermissionRequest;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebStorage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.tencent.tauth.AuthActivity;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class BaiduStatJSInterface {

    public interface IWebviewPageLoadCallback {
        void onPageFinished(WebView webView, String str, C1261ax axVar);

        void onPageStarted(WebView webView, String str, C1261ax axVar);
    }

    public static class CustomWebViewClient extends WebViewClient {

        /* renamed from: a */
        private WeakReference<Context> f4105a;

        /* renamed from: b */
        private WebViewClient f4106b;

        /* renamed from: c */
        private IWebviewPageLoadCallback f4107c;

        /* renamed from: d */
        private C1261ax f4108d;

        public CustomWebViewClient(Context context, WebViewClient webViewClient, IWebviewPageLoadCallback iWebviewPageLoadCallback, C1261ax axVar) {
            this.f4105a = new WeakReference<>(context);
            this.f4106b = super;
            this.f4107c = iWebviewPageLoadCallback;
            this.f4108d = axVar;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            try {
                str = URLDecoder.decode(str, "UTF-8");
                if (!TextUtils.isEmpty(str) && str.startsWith("bmtj:")) {
                    m4401a(str.substring(5));
                    return true;
                }
            } catch (UnsupportedEncodingException | JSONException unused) {
            }
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                return super.shouldOverrideUrlLoading(webView, str);
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                return super.shouldOverrideUrlLoading(webView, webResourceRequest);
            }
            return super.shouldOverrideUrlLoading(webView, webResourceRequest);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.mobstat.BDStatCore.onEvent(android.content.Context, java.lang.String, java.lang.String, int, com.baidu.mobstat.ExtraInfo, java.util.Map<java.lang.String, java.lang.String>, boolean, boolean):void
         arg types: [android.content.Context, java.lang.String, java.lang.String, int, ?[OBJECT, ARRAY], java.util.HashMap<java.lang.String, java.lang.String>, int, int]
         candidates:
          com.baidu.mobstat.BDStatCore.onEvent(android.content.Context, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, int, boolean):void
          com.baidu.mobstat.BDStatCore.onEvent(android.content.Context, java.lang.String, java.lang.String, int, com.baidu.mobstat.ExtraInfo, java.util.Map<java.lang.String, java.lang.String>, boolean, boolean):void */
        /* renamed from: a */
        private void m4401a(String str) throws JSONException {
            JSONObject jSONObject;
            JSONObject jSONObject2;
            JSONObject jSONObject3;
            JSONObject jSONObject4 = new JSONObject(str);
            String string = jSONObject4.getString(AuthActivity.ACTION_KEY);
            JSONObject jSONObject5 = jSONObject4.getJSONObject("obj");
            Context context = this.f4105a.get();
            if (context != null) {
                if ("onPageStart".equals(string)) {
                    String string2 = jSONObject5.getString("page");
                    if (!TextUtils.isEmpty(string2)) {
                        BDStatCore.instance().onPageStart(context, string2);
                    }
                } else if ("onPageEnd".equals(string)) {
                    String string3 = jSONObject5.getString("page");
                    if (!TextUtils.isEmpty(string3)) {
                        BDStatCore.instance().onPageEnd(context, string3, null, true);
                    }
                } else if ("onEvent".equals(string)) {
                    String string4 = jSONObject5.getString("event_id");
                    String string5 = jSONObject5.getString("label");
                    int i = jSONObject5.getInt("acc");
                    if (!TextUtils.isEmpty(string4)) {
                        try {
                            jSONObject3 = (JSONObject) jSONObject5.get("attributes");
                        } catch (Exception unused) {
                            jSONObject3 = null;
                        }
                        BDStatCore.instance().onEvent(context, string4, string5, i, (ExtraInfo) null, (Map<String, String>) m4400a(jSONObject3), false, true);
                    }
                } else if ("onEventStart".equals(string)) {
                    String string6 = jSONObject5.getString("event_id");
                    String string7 = jSONObject5.getString("label");
                    if (!TextUtils.isEmpty(string6)) {
                        BDStatCore.instance().onEventStart(context, string6, string7, false);
                    }
                } else if ("onEventEnd".equals(string)) {
                    String string8 = jSONObject5.getString("event_id");
                    String string9 = jSONObject5.getString("label");
                    if (!TextUtils.isEmpty(string8)) {
                        try {
                            jSONObject2 = (JSONObject) jSONObject5.get("attributes");
                        } catch (Exception unused2) {
                            jSONObject2 = null;
                        }
                        BDStatCore.instance().onEventEnd(context, string8, string9, null, m4400a(jSONObject2), true);
                    }
                } else if ("onEventDuration".equals(string)) {
                    String string10 = jSONObject5.getString("event_id");
                    String string11 = jSONObject5.getString("label");
                    long j = jSONObject5.getLong("duration");
                    if (!TextUtils.isEmpty(string10)) {
                        try {
                            jSONObject = (JSONObject) jSONObject5.get("attributes");
                        } catch (Exception unused3) {
                            jSONObject = null;
                        }
                        BDStatCore.instance().onEventDuration(context, string10, string11, j, null, m4400a(jSONObject), false, true);
                    }
                }
            }
        }

        /* renamed from: a */
        private HashMap<String, String> m4400a(JSONObject jSONObject) {
            HashMap<String, String> hashMap = null;
            if (jSONObject == null) {
                return null;
            }
            if (jSONObject.length() != 0) {
                hashMap = new HashMap<>();
            }
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    hashMap.put(next, jSONObject.getString(next));
                } catch (Exception unused) {
                }
            }
            return hashMap;
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onPageStarted(webView, str, bitmap);
            }
        }

        public void onPageFinished(WebView webView, String str) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onPageFinished(webView, str);
            }
        }

        public void onLoadResource(WebView webView, String str) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onLoadResource(webView, str);
            }
        }

        @Deprecated
        public void onTooManyRedirects(WebView webView, Message message, Message message2) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onTooManyRedirects(webView, message, message2);
            }
        }

        public void onFormResubmission(WebView webView, Message message, Message message2) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onFormResubmission(webView, message, message2);
            }
        }

        public void doUpdateVisitedHistory(WebView webView, String str, boolean z) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.doUpdateVisitedHistory(webView, str, z);
            }
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedSslError(webView, sslErrorHandler, sslError);
            }
        }

        public void onReceivedHttpAuthRequest(WebView webView, HttpAuthHandler httpAuthHandler, String str, String str2) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedHttpAuthRequest(webView, httpAuthHandler, str, str2);
            }
        }

        public boolean shouldOverrideKeyEvent(WebView webView, KeyEvent keyEvent) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                return super.shouldOverrideKeyEvent(webView, keyEvent);
            }
            return super.shouldOverrideKeyEvent(webView, keyEvent);
        }

        public void onUnhandledKeyEvent(WebView webView, KeyEvent keyEvent) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onUnhandledKeyEvent(webView, keyEvent);
            }
        }

        public void onScaleChanged(WebView webView, float f, float f2) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onScaleChanged(webView, f, f2);
            }
        }

        public void onReceivedLoginRequest(WebView webView, String str, String str2, String str3) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedLoginRequest(webView, str, str2, str3);
            }
        }

        public void onReceivedClientCertRequest(WebView webView, ClientCertRequest clientCertRequest) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedClientCertRequest(webView, clientCertRequest);
            }
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                return super.shouldInterceptRequest(webView, str);
            }
            return super.shouldInterceptRequest(webView, str);
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                return super.shouldInterceptRequest(webView, webResourceRequest);
            }
            return super.shouldInterceptRequest(webView, webResourceRequest);
        }

        public void onPageCommitVisible(WebView webView, String str) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onPageCommitVisible(webView, str);
            }
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedError(webView, i, str, str2);
            }
        }

        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedError(webView, webResourceRequest, webResourceError);
            }
        }

        public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
            }
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            WebViewClient webViewClient = this.f4106b;
            if (webViewClient != null) {
                return super.onRenderProcessGone(webView, renderProcessGoneDetail);
            }
            return super.onRenderProcessGone(webView, renderProcessGoneDetail);
        }
    }

    public static class CustomWebChromeViewClient extends WebChromeClient {

        /* renamed from: a */
        private WeakReference<Context> f4100a;

        /* renamed from: b */
        private WebChromeClient f4101b;

        /* renamed from: c */
        private ArrayList<IWebviewPageLoadCallback> f4102c;

        /* renamed from: d */
        private C1261ax f4103d;

        /* renamed from: e */
        private int f4104e = 0;

        public CustomWebChromeViewClient(Context context, WebChromeClient webChromeClient, ArrayList<IWebviewPageLoadCallback> arrayList, C1261ax axVar) {
            this.f4100a = new WeakReference<>(context);
            this.f4101b = super;
            this.f4102c = arrayList;
            this.f4103d = axVar;
        }

        public void onProgressChanged(WebView webView, int i) {
            ArrayList<IWebviewPageLoadCallback> arrayList = this.f4102c;
            if (arrayList != null) {
                if (this.f4104e == 0) {
                    Iterator<IWebviewPageLoadCallback> it = arrayList.iterator();
                    while (it.hasNext()) {
                        IWebviewPageLoadCallback next = it.next();
                        if (next != null) {
                            next.onPageStarted(webView, webView.getUrl(), this.f4103d);
                        }
                    }
                }
                this.f4104e = i;
                if (i == 100) {
                    Iterator<IWebviewPageLoadCallback> it2 = this.f4102c.iterator();
                    while (it2.hasNext()) {
                        IWebviewPageLoadCallback next2 = it2.next();
                        if (next2 != null) {
                            next2.onPageFinished(webView, webView.getUrl(), this.f4103d);
                        }
                    }
                }
            }
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onProgressChanged(webView, i);
            }
        }

        public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onCreateWindow(webView, z, z2, message);
            }
            return super.onCreateWindow(webView, z, z2, message);
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onJsAlert(webView, str, str2, jsResult);
            }
            return super.onJsAlert(webView, str, str2, jsResult);
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onConsoleMessage(consoleMessage);
            }
            return super.onConsoleMessage(consoleMessage);
        }

        @Deprecated
        public void onConsoleMessage(String str, int i, String str2) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onConsoleMessage(str, i, str2);
            }
        }

        public void onCloseWindow(WebView webView) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onCloseWindow(webView);
            }
        }

        @Deprecated
        public void onExceededDatabaseQuota(String str, String str2, long j, long j2, long j3, WebStorage.QuotaUpdater quotaUpdater) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onExceededDatabaseQuota(str, str2, j, j2, j3, quotaUpdater);
            }
        }

        public void onGeolocationPermissionsHidePrompt() {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onGeolocationPermissionsHidePrompt();
            }
        }

        public void onGeolocationPermissionsShowPrompt(String str, GeolocationPermissions.Callback callback) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onGeolocationPermissionsShowPrompt(str, callback);
            }
        }

        public void onHideCustomView() {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onHideCustomView();
            }
        }

        public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onJsBeforeUnload(webView, str, str2, jsResult);
            }
            return super.onJsBeforeUnload(webView, str, str2, jsResult);
        }

        public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onJsConfirm(webView, str, str2, jsResult);
            }
            return super.onJsConfirm(webView, str, str2, jsResult);
        }

        public boolean onJsPrompt(WebView webView, String str, String str2, String str3, JsPromptResult jsPromptResult) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
            }
            return super.onJsPrompt(webView, str, str2, str3, jsPromptResult);
        }

        @Deprecated
        public boolean onJsTimeout() {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onJsTimeout();
            }
            return super.onJsTimeout();
        }

        public void onPermissionRequest(PermissionRequest permissionRequest) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onPermissionRequest(permissionRequest);
            }
        }

        public void onPermissionRequestCanceled(PermissionRequest permissionRequest) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onPermissionRequestCanceled(permissionRequest);
            }
        }

        @Deprecated
        public void onReachedMaxAppCacheSize(long j, long j2, WebStorage.QuotaUpdater quotaUpdater) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onReachedMaxAppCacheSize(j, j2, quotaUpdater);
            }
        }

        public void onReceivedIcon(WebView webView, Bitmap bitmap) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onReceivedIcon(webView, bitmap);
            }
        }

        public void onReceivedTitle(WebView webView, String str) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onReceivedTitle(webView, str);
            }
        }

        public void onReceivedTouchIconUrl(WebView webView, String str, boolean z) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onReceivedTouchIconUrl(webView, str, z);
            }
        }

        public void onRequestFocus(WebView webView) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onRequestFocus(webView);
            }
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onShowCustomView(view, customViewCallback);
            }
        }

        @Deprecated
        public void onShowCustomView(View view, int i, WebChromeClient.CustomViewCallback customViewCallback) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                super.onShowCustomView(view, i, customViewCallback);
            }
        }

        public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> valueCallback, WebChromeClient.FileChooserParams fileChooserParams) {
            WebChromeClient webChromeClient = this.f4101b;
            if (webChromeClient != null) {
                return super.onShowFileChooser(webView, valueCallback, fileChooserParams);
            }
            return super.onShowFileChooser(webView, valueCallback, fileChooserParams);
        }
    }
}
