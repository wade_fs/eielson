package com.baidu.location.p016d;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.net.wifi.ScanResult;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import com.baidu.location.BDLocation;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.p013a.C0723a;
import com.baidu.location.p013a.C0750l;
import com.baidu.location.p017e.C0821a;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0855k;
import com.baidu.mobstat.Config;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

/* renamed from: com.baidu.location.d.a */
public final class C0790a {

    /* renamed from: b */
    private static C0790a f1564b;

    /* renamed from: l */
    private static final String f1565l = (Environment.getExternalStorageDirectory().getPath() + "/baidu/tempdata/");
    /* access modifiers changed from: private */

    /* renamed from: m */
    public static final String f1566m = (Environment.getExternalStorageDirectory().getPath() + "/baidu/tempdata" + "/ls.db");

    /* renamed from: a */
    public boolean f1567a = false;

    /* renamed from: c */
    private String f1568c = null;

    /* renamed from: d */
    private boolean f1569d = false;

    /* renamed from: e */
    private boolean f1570e = false;

    /* renamed from: f */
    private double f1571f = 0.0d;

    /* renamed from: g */
    private double f1572g = 0.0d;

    /* renamed from: h */
    private double f1573h = 0.0d;

    /* renamed from: i */
    private double f1574i = 0.0d;

    /* renamed from: j */
    private double f1575j = 0.0d;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public volatile boolean f1576k = false;

    /* renamed from: n */
    private Handler f1577n = null;

    /* renamed from: com.baidu.location.d.a$a */
    private class C0791a extends AsyncTask<Boolean, Void, Boolean> {
        private C0791a() {
        }

        /* synthetic */ C0791a(C0790a aVar, C0793b bVar) {
            this();
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0042 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x0060 */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x004a A[Catch:{ Exception -> 0x0069 }] */
        /* renamed from: a */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public java.lang.Boolean doInBackground(java.lang.Boolean... r6) {
            /*
                r5 = this;
                int r0 = r6.length
                r1 = 0
                java.lang.Boolean r2 = java.lang.Boolean.valueOf(r1)
                r3 = 4
                if (r0 == r3) goto L_0x000a
                return r2
            L_0x000a:
                r0 = 0
                java.lang.String r3 = com.baidu.location.p016d.C0790a.f1566m     // Catch:{ Exception -> 0x0014 }
                android.database.sqlite.SQLiteDatabase r0 = android.database.sqlite.SQLiteDatabase.openOrCreateDatabase(r3, r0)     // Catch:{ Exception -> 0x0014 }
                goto L_0x0015
            L_0x0014:
            L_0x0015:
                if (r0 != 0) goto L_0x0018
                return r2
            L_0x0018:
                long r2 = java.lang.System.currentTimeMillis()
                r4 = 28
                long r2 = r2 >> r4
                int r3 = (int) r2
                r2 = 1
                r0.beginTransaction()     // Catch:{ Exception -> 0x0069 }
                r1 = r6[r1]     // Catch:{ Exception -> 0x0069 }
                boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x0069 }
                if (r1 == 0) goto L_0x0042
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0069 }
                r1.<init>()     // Catch:{ Exception -> 0x0069 }
                java.lang.String r4 = "delete from wof where ac < "
                r1.append(r4)     // Catch:{ Exception -> 0x0069 }
                int r4 = r3 + -35
                r1.append(r4)     // Catch:{ Exception -> 0x0069 }
                java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0069 }
                r0.execSQL(r1)     // Catch:{ Exception -> 0x0042 }
            L_0x0042:
                r6 = r6[r2]     // Catch:{ Exception -> 0x0069 }
                boolean r6 = r6.booleanValue()     // Catch:{ Exception -> 0x0069 }
                if (r6 == 0) goto L_0x0060
                java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0069 }
                r6.<init>()     // Catch:{ Exception -> 0x0069 }
                java.lang.String r1 = "delete from bdcltb09 where ac is NULL or ac < "
                r6.append(r1)     // Catch:{ Exception -> 0x0069 }
                int r3 = r3 + -130
                r6.append(r3)     // Catch:{ Exception -> 0x0069 }
                java.lang.String r6 = r6.toString()     // Catch:{ Exception -> 0x0069 }
                r0.execSQL(r6)     // Catch:{ Exception -> 0x0060 }
            L_0x0060:
                r0.setTransactionSuccessful()     // Catch:{ Exception -> 0x0069 }
                r0.endTransaction()     // Catch:{ Exception -> 0x0069 }
                r0.close()     // Catch:{ Exception -> 0x0069 }
            L_0x0069:
                java.lang.Boolean r6 = java.lang.Boolean.valueOf(r2)
                return r6
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0790a.C0791a.doInBackground(java.lang.Boolean[]):java.lang.Boolean");
        }
    }

    /* renamed from: com.baidu.location.d.a$b */
    private class C0792b extends AsyncTask<Object, Void, Boolean> {
        private C0792b() {
        }

        /* synthetic */ C0792b(C0790a aVar, C0793b bVar) {
            this();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.d.a.a(com.baidu.location.d.a, boolean):boolean
         arg types: [com.baidu.location.d.a, int]
         candidates:
          com.baidu.location.d.a.a(java.lang.String, android.database.sqlite.SQLiteDatabase):void
          com.baidu.location.d.a.a(java.lang.String, java.util.List<android.net.wifi.ScanResult>):void
          com.baidu.location.d.a.a(java.util.List<android.net.wifi.ScanResult>, android.database.sqlite.SQLiteDatabase):void
          com.baidu.location.d.a.a(com.baidu.location.d.a, boolean):boolean */
        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(Object... objArr) {
            if (objArr.length == 4) {
                SQLiteDatabase sQLiteDatabase = null;
                try {
                    sQLiteDatabase = SQLiteDatabase.openOrCreateDatabase(C0790a.f1566m, (SQLiteDatabase.CursorFactory) null);
                } catch (Exception unused) {
                }
                if (sQLiteDatabase != null) {
                    try {
                        sQLiteDatabase.beginTransaction();
                        C0790a.this.m2069a((String) objArr[0], (C0821a) objArr[1], sQLiteDatabase);
                        C0790a.this.m2067a((C0834h) objArr[2], (BDLocation) objArr[3], sQLiteDatabase);
                        sQLiteDatabase.setTransactionSuccessful();
                        sQLiteDatabase.endTransaction();
                        sQLiteDatabase.close();
                    } catch (Exception unused2) {
                    }
                    boolean unused3 = C0790a.this.f1576k = false;
                    return true;
                }
            }
            boolean unused4 = C0790a.this.f1576k = false;
            return false;
        }
    }

    private C0790a() {
        mo10557b();
    }

    /* renamed from: a */
    public static synchronized C0790a m2063a() {
        C0790a aVar;
        synchronized (C0790a.class) {
            if (f1564b == null) {
                f1564b = new C0790a();
            }
            aVar = f1564b;
        }
        return aVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2067a(C0834h hVar, BDLocation bDLocation, SQLiteDatabase sQLiteDatabase) {
        Iterator<ScanResult> it;
        int i;
        int i2;
        int i3;
        double d;
        int i4;
        boolean z;
        String str;
        C0834h hVar2 = hVar;
        SQLiteDatabase sQLiteDatabase2 = sQLiteDatabase;
        if (bDLocation != null && bDLocation.getLocType() == 161) {
            if (("wf".equals(bDLocation.getNetworkLocationType()) || bDLocation.getRadius() < 300.0f) && hVar2.f1789a != null) {
                int currentTimeMillis = (int) (System.currentTimeMillis() >> 28);
                System.currentTimeMillis();
                Iterator<ScanResult> it2 = hVar2.f1789a.iterator();
                int i5 = 0;
                while (it2.hasNext()) {
                    ScanResult next = it2.next();
                    if (next.level != 0) {
                        int i6 = i5 + 1;
                        if (i6 <= 6) {
                            ContentValues contentValues = new ContentValues();
                            String encode2 = Jni.encode2(next.BSSID.replace(Config.TRACE_TODAY_VISIT_SPLIT, ""));
                            try {
                                Cursor rawQuery = sQLiteDatabase2.rawQuery("select * from wof where id = \"" + encode2 + "\";", null);
                                double d2 = 0.0d;
                                if (rawQuery == null || !rawQuery.moveToFirst()) {
                                    d = 0.0d;
                                    z = false;
                                    i4 = 0;
                                    i3 = 0;
                                } else {
                                    d2 = rawQuery.getDouble(1) - 113.2349d;
                                    d = rawQuery.getDouble(2) - 432.1238d;
                                    int i7 = rawQuery.getInt(4);
                                    i4 = rawQuery.getInt(5);
                                    i3 = i7;
                                    z = true;
                                }
                                if (rawQuery != null) {
                                    rawQuery.close();
                                }
                                if (!z) {
                                    contentValues.put("mktime", Double.valueOf(bDLocation.getLongitude() + 113.2349d));
                                    contentValues.put("time", Double.valueOf(bDLocation.getLatitude() + 432.1238d));
                                    contentValues.put("bc", (Integer) 1);
                                    contentValues.put("cc", (Integer) 1);
                                    contentValues.put("ac", Integer.valueOf(currentTimeMillis));
                                    contentValues.put(Config.FEED_LIST_ITEM_CUSTOM_ID, encode2);
                                    sQLiteDatabase2.insert("wof", null, contentValues);
                                } else if (i4 != 0) {
                                    it = it2;
                                    try {
                                        float[] fArr = new float[1];
                                        Location.distanceBetween(d, d2, bDLocation.getLatitude(), bDLocation.getLongitude(), fArr);
                                        if (fArr[0] > 1500.0f) {
                                            int i8 = i4 + 1;
                                            if (i8 <= 10 || i8 <= i3 * 3) {
                                                contentValues.put("cc", Integer.valueOf(i8));
                                            } else {
                                                contentValues.put("mktime", Double.valueOf(bDLocation.getLongitude() + 113.2349d));
                                                contentValues.put("time", Double.valueOf(bDLocation.getLatitude() + 432.1238d));
                                                contentValues.put("bc", (Integer) 1);
                                                contentValues.put("cc", (Integer) 1);
                                                contentValues.put("ac", Integer.valueOf(currentTimeMillis));
                                            }
                                            i2 = currentTimeMillis;
                                            i = i6;
                                            str = encode2;
                                        } else {
                                            i2 = currentTimeMillis;
                                            int i9 = i3;
                                            double d3 = (double) i9;
                                            Double.isNaN(d3);
                                            try {
                                                double longitude = (d2 * d3) + bDLocation.getLongitude();
                                                int i10 = i9 + 1;
                                                i = i6;
                                                str = encode2;
                                                double d4 = (double) i10;
                                                Double.isNaN(d4);
                                                double d5 = longitude / d4;
                                                Double.isNaN(d3);
                                                try {
                                                    double latitude = (d * d3) + bDLocation.getLatitude();
                                                    Double.isNaN(d4);
                                                    double d6 = latitude / d4;
                                                    contentValues.put("mktime", Double.valueOf(d5 + 113.2349d));
                                                    contentValues.put("time", Double.valueOf(d6 + 432.1238d));
                                                    contentValues.put("bc", Integer.valueOf(i10));
                                                    contentValues.put("ac", Integer.valueOf(i2));
                                                } catch (Exception unused) {
                                                }
                                            } catch (Exception unused2) {
                                            }
                                        }
                                        sQLiteDatabase2.update("wof", contentValues, "id = \"" + str + "\"", null);
                                    } catch (Exception unused3) {
                                    }
                                    currentTimeMillis = i2;
                                    i5 = i;
                                    it2 = it;
                                }
                            } catch (Exception unused4) {
                            }
                            it = it2;
                            i2 = currentTimeMillis;
                            i = i6;
                            currentTimeMillis = i2;
                            i5 = i;
                            it2 = it;
                        } else {
                            return;
                        }
                    }
                }
            }
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:8:0x005a */
    /* JADX WARN: Type inference failed for: r0v3, types: [java.lang.String[], android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v4, types: [android.database.Cursor] */
    /* JADX WARN: Type inference failed for: r0v5, types: [android.database.Cursor] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2068a(java.lang.String r6, android.database.sqlite.SQLiteDatabase r7) {
        /*
            r5 = this;
            if (r6 == 0) goto L_0x006b
            java.lang.String r0 = r5.f1568c
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x000b
            goto L_0x006b
        L_0x000b:
            r0 = 0
            r5.f1569d = r0
            r0 = 0
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r1.<init>()     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            java.lang.String r2 = "select * from bdcltb09 where id = \""
            r1.append(r2)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r1.append(r6)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            java.lang.String r2 = "\";"
            r1.append(r2)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            android.database.Cursor r0 = r7.rawQuery(r1, r0)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r5.f1568c = r6     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            boolean r6 = r0.moveToFirst()     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            if (r6 == 0) goto L_0x005a
            r6 = 1
            double r1 = r0.getDouble(r6)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r3 = 4653148304163072062(0x40934dbaacd9e83e, double:1235.4323)
            double r1 = r1 - r3
            r5.f1572g = r1     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r7 = 2
            double r1 = r0.getDouble(r7)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r3 = 4661478502002851840(0x40b0e60000000000, double:4326.0)
            double r1 = r1 - r3
            r5.f1571f = r1     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r7 = 3
            double r1 = r0.getDouble(r7)     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r3 = 4657424210545395263(0x40a27ea4b5dcc63f, double:2367.3217)
            double r1 = r1 - r3
            r5.f1573h = r1     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
            r5.f1569d = r6     // Catch:{ Exception -> 0x0067, all -> 0x0060 }
        L_0x005a:
            if (r0 == 0) goto L_0x006b
        L_0x005c:
            r0.close()     // Catch:{ Exception -> 0x006b }
            goto L_0x006b
        L_0x0060:
            r6 = move-exception
            if (r0 == 0) goto L_0x0066
            r0.close()     // Catch:{ Exception -> 0x0066 }
        L_0x0066:
            throw r6
        L_0x0067:
            if (r0 == 0) goto L_0x006b
            goto L_0x005c
        L_0x006b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0790a.m2068a(java.lang.String, android.database.sqlite.SQLiteDatabase):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x00cb A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x00cc  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m2069a(java.lang.String r20, com.baidu.location.p017e.C0821a r21, android.database.sqlite.SQLiteDatabase r22) {
        /*
            r19 = this;
            r0 = r22
            java.lang.String r1 = "clf"
            boolean r2 = r21.mo10617b()
            if (r2 == 0) goto L_0x0125
            com.baidu.location.a.l r2 = com.baidu.location.p013a.C0750l.m1883c()
            boolean r2 = r2.mo10485h()
            if (r2 != 0) goto L_0x0016
            goto L_0x0125
        L_0x0016:
            java.lang.System.currentTimeMillis()
            r2 = 0
            long r3 = java.lang.System.currentTimeMillis()
            r5 = 28
            long r3 = r3 >> r5
            int r4 = (int) r3
            java.lang.String r3 = r21.mo10622g()
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ Exception -> 0x0125 }
            r6 = r20
            r5.<init>(r6)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r6 = "result"
            org.json.JSONObject r6 = r5.getJSONObject(r6)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r7 = "error"
            java.lang.String r6 = r6.getString(r7)     // Catch:{ Exception -> 0x0125 }
            int r6 = java.lang.Integer.parseInt(r6)     // Catch:{ Exception -> 0x0125 }
            r7 = 161(0xa1, float:2.26E-43)
            java.lang.String r8 = "\""
            java.lang.String r9 = "id = \""
            r10 = 1
            r11 = 0
            r13 = 0
            java.lang.String r14 = "bdcltb09"
            if (r6 != r7) goto L_0x00af
            java.lang.String r6 = "content"
            org.json.JSONObject r5 = r5.getJSONObject(r6)     // Catch:{ Exception -> 0x0125 }
            boolean r6 = r5.has(r1)     // Catch:{ Exception -> 0x0125 }
            r7 = 0
            if (r6 == 0) goto L_0x00ad
            java.lang.String r1 = r5.getString(r1)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r2 = "0"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x0125 }
            if (r2 == 0) goto L_0x0090
            java.lang.String r1 = "point"
            org.json.JSONObject r1 = r5.getJSONObject(r1)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r2 = "x"
            java.lang.String r2 = r1.getString(r2)     // Catch:{ Exception -> 0x0125 }
            double r10 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r2 = "y"
            java.lang.String r1 = r1.getString(r2)     // Catch:{ Exception -> 0x0125 }
            double r1 = java.lang.Double.parseDouble(r1)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r6 = "radius"
            java.lang.String r5 = r5.getString(r6)     // Catch:{ Exception -> 0x0125 }
            float r5 = java.lang.Float.parseFloat(r5)     // Catch:{ Exception -> 0x0125 }
            r17 = r1
            r2 = r5
            r5 = r10
            r10 = 0
            r11 = r17
            goto L_0x00c9
        L_0x0090:
            java.lang.String r2 = "\\|"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0125 }
            r2 = r1[r7]     // Catch:{ Exception -> 0x0125 }
            double r5 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x0125 }
            r2 = r1[r10]     // Catch:{ Exception -> 0x0125 }
            double r10 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x0125 }
            r2 = 2
            r1 = r1[r2]     // Catch:{ Exception -> 0x0125 }
            float r1 = java.lang.Float.parseFloat(r1)     // Catch:{ Exception -> 0x0125 }
            r2 = r1
            r11 = r10
            r10 = 0
            goto L_0x00c9
        L_0x00ad:
            r5 = r11
            goto L_0x00c9
        L_0x00af:
            r1 = 167(0xa7, float:2.34E-43)
            if (r6 != r1) goto L_0x00ad
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0125 }
            r1.<init>()     // Catch:{ Exception -> 0x0125 }
            r1.append(r9)     // Catch:{ Exception -> 0x0125 }
            r1.append(r3)     // Catch:{ Exception -> 0x0125 }
            r1.append(r8)     // Catch:{ Exception -> 0x0125 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0125 }
            r0.delete(r14, r1, r13)     // Catch:{ Exception -> 0x0125 }
            return
        L_0x00c9:
            if (r10 == 0) goto L_0x00cc
            return
        L_0x00cc:
            r15 = 4653148304163072062(0x40934dbaacd9e83e, double:1235.4323)
            double r5 = r5 + r15
            r15 = 4657424210545395263(0x40a27ea4b5dcc63f, double:2367.3217)
            double r11 = r11 + r15
            r1 = 1166487552(0x45873000, float:4326.0)
            float r2 = r2 + r1
            android.content.ContentValues r1 = new android.content.ContentValues
            r1.<init>()
            java.lang.Double r5 = java.lang.Double.valueOf(r5)
            java.lang.String r6 = "time"
            r1.put(r6, r5)
            java.lang.Float r2 = java.lang.Float.valueOf(r2)
            java.lang.String r5 = "tag"
            r1.put(r5, r2)
            java.lang.Double r2 = java.lang.Double.valueOf(r11)
            java.lang.String r5 = "type"
            r1.put(r5, r2)
            java.lang.Integer r2 = java.lang.Integer.valueOf(r4)
            java.lang.String r4 = "ac"
            r1.put(r4, r2)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{  }
            r2.<init>()     // Catch:{  }
            r2.append(r9)     // Catch:{  }
            r2.append(r3)     // Catch:{  }
            r2.append(r8)     // Catch:{  }
            java.lang.String r2 = r2.toString()     // Catch:{  }
            int r2 = r0.update(r14, r1, r2, r13)     // Catch:{  }
            if (r2 > 0) goto L_0x0125
            java.lang.String r2 = "id"
            r1.put(r2, r3)     // Catch:{  }
            r0.insert(r14, r13, r1)     // Catch:{  }
        L_0x0125:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0790a.m2069a(java.lang.String, com.baidu.location.e.a, android.database.sqlite.SQLiteDatabase):void");
    }

    /* renamed from: a */
    private void m2070a(String str, List<ScanResult> list) {
        this.f1569d = false;
        this.f1570e = false;
        SQLiteDatabase sQLiteDatabase = null;
        try {
            sQLiteDatabase = SQLiteDatabase.openOrCreateDatabase(f1566m, (SQLiteDatabase.CursorFactory) null);
        } catch (Throwable unused) {
        }
        if (!(str == null || sQLiteDatabase == null)) {
            m2068a(str, sQLiteDatabase);
        }
        if (!(list == null || sQLiteDatabase == null)) {
            m2071a(list, sQLiteDatabase);
        }
        if (sQLiteDatabase != null && sQLiteDatabase.isOpen()) {
            sQLiteDatabase.close();
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:59:0x017e A[Catch:{ Exception -> 0x01a2, all -> 0x019b }] */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x017d A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m2071a(java.util.List<android.net.wifi.ScanResult> r30, android.database.sqlite.SQLiteDatabase r31) {
        /*
            r29 = this;
            r1 = r29
            r0 = r31
            java.lang.System.currentTimeMillis()
            r2 = 0
            r1.f1570e = r2
            if (r30 == 0) goto L_0x01a6
            int r3 = r30.size()
            if (r3 != 0) goto L_0x0014
            goto L_0x01a6
        L_0x0014:
            if (r0 == 0) goto L_0x01a6
            if (r30 != 0) goto L_0x001a
            goto L_0x01a6
        L_0x001a:
            r3 = 8
            double[] r4 = new double[r3]
            java.lang.StringBuffer r5 = new java.lang.StringBuffer
            r5.<init>()
            java.util.Iterator r6 = r30.iterator()
            r7 = 0
        L_0x0028:
            boolean r8 = r6.hasNext()
            if (r8 == 0) goto L_0x005c
            java.lang.Object r8 = r6.next()
            android.net.wifi.ScanResult r8 = (android.net.wifi.ScanResult) r8
            r9 = 10
            if (r7 <= r9) goto L_0x0039
            goto L_0x005c
        L_0x0039:
            if (r7 <= 0) goto L_0x0040
            java.lang.String r9 = ","
            r5.append(r9)
        L_0x0040:
            int r7 = r7 + 1
            java.lang.String r8 = r8.BSSID
            java.lang.String r9 = ":"
            java.lang.String r10 = ""
            java.lang.String r8 = r8.replace(r9, r10)
            java.lang.String r8 = com.baidu.location.Jni.encode2(r8)
            java.lang.String r9 = "\""
            r5.append(r9)
            r5.append(r8)
            r5.append(r9)
            goto L_0x0028
        L_0x005c:
            r6 = 0
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r7.<init>()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            java.lang.String r8 = "select * from wof where id in ("
            r7.append(r8)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            java.lang.String r5 = r5.toString()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r7.append(r5)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            java.lang.String r5 = ");"
            r7.append(r5)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            java.lang.String r5 = r7.toString()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            android.database.Cursor r6 = r0.rawQuery(r5, r6)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            boolean r0 = r6.moveToFirst()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            if (r0 == 0) goto L_0x0195
            r7 = 0
            r11 = r7
            r0 = 0
            r5 = 0
            r10 = 0
            r8 = r11
            r7 = 0
        L_0x0089:
            boolean r13 = r6.isAfterLast()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r14 = 1
            if (r13 != 0) goto L_0x0183
            double r15 = r6.getDouble(r14)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r17 = 4637668614646953253(0x405c4f089a027525, double:113.2349)
            double r15 = r15 - r17
            r13 = 2
            double r17 = r6.getDouble(r13)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r19 = 4646309618475430891(0x407b01fb15b573eb, double:432.1238)
            double r17 = r17 - r19
            r13 = 4
            int r2 = r6.getInt(r13)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r13 = 5
            int r13 = r6.getInt(r13)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            if (r13 <= r3) goto L_0x00bb
            if (r13 <= r2) goto L_0x00bb
            r6.moveToNext()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r28 = r4
            goto L_0x0118
        L_0x00bb:
            boolean r2 = r1.f1569d     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            if (r2 == 0) goto L_0x00f3
            float[] r2 = new float[r14]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r28 = r4
            double r3 = r1.f1573h     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r13 = r1.f1572g     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r19 = r17
            r21 = r15
            r23 = r3
            r25 = r13
            r27 = r2
            android.location.Location.distanceBetween(r19, r21, r23, r25, r27)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r3 = 0
            r2 = r2[r3]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r2 = (double) r2     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r13 = r1.f1571f     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r19 = 4656510908468559872(0x409f400000000000, double:2000.0)
            double r13 = r13 + r19
            int r4 = (r2 > r13 ? 1 : (r2 == r13 ? 0 : -1))
            if (r4 <= 0) goto L_0x00e9
        L_0x00e5:
            r6.moveToNext()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            goto L_0x0118
        L_0x00e9:
            double r11 = r11 + r15
            double r8 = r8 + r17
            int r10 = r10 + 1
            r0 = 8
            r2 = 4
            r3 = 1
            goto L_0x0123
        L_0x00f3:
            r28 = r4
            r2 = 1148846080(0x447a0000, float:1000.0)
            if (r0 == 0) goto L_0x0125
            r3 = 1
            float[] r4 = new float[r3]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r13 = (double) r10
            java.lang.Double.isNaN(r13)
            double r23 = r8 / r13
            java.lang.Double.isNaN(r13)
            double r25 = r11 / r13
            r19 = r17
            r21 = r15
            r27 = r4
            android.location.Location.distanceBetween(r19, r21, r23, r25, r27)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r3 = 0
            r4 = r4[r3]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            int r2 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r2 <= 0) goto L_0x011f
            goto L_0x00e5
        L_0x0118:
            r4 = r28
            r2 = 0
            r3 = 8
            goto L_0x0089
        L_0x011f:
            r3 = r0
            r0 = 8
            r2 = 4
        L_0x0123:
            r14 = 0
            goto L_0x017b
        L_0x0125:
            if (r5 != 0) goto L_0x0137
            int r2 = r7 + 1
            r28[r7] = r15     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            int r3 = r2 + 1
            r28[r2] = r17     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r7 = r3
            r2 = 4
            r5 = 1
            r14 = 0
            r3 = r0
            r0 = 8
            goto L_0x017b
        L_0x0137:
            r3 = r0
            r0 = 0
        L_0x0139:
            if (r0 >= r7) goto L_0x0161
            r4 = 1
            float[] r13 = new float[r4]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            int r4 = r0 + 1
            r23 = r28[r4]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r25 = r28[r0]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r19 = r17
            r21 = r15
            r27 = r13
            android.location.Location.distanceBetween(r19, r21, r23, r25, r27)     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r14 = 0
            r13 = r13[r14]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            int r13 = (r13 > r2 ? 1 : (r13 == r2 ? 0 : -1))
            if (r13 >= 0) goto L_0x015e
            r19 = r28[r0]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r11 = r11 + r19
            r3 = r28[r4]     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r8 = r8 + r3
            int r10 = r10 + 1
            r3 = 1
        L_0x015e:
            int r0 = r0 + 2
            goto L_0x0139
        L_0x0161:
            r14 = 0
            if (r3 == 0) goto L_0x016d
            double r11 = r11 + r15
            double r8 = r8 + r17
            int r10 = r10 + 1
            r0 = 8
        L_0x016b:
            r2 = 4
            goto L_0x017b
        L_0x016d:
            r0 = 8
            if (r7 >= r0) goto L_0x0183
            int r2 = r7 + 1
            r28[r7] = r15     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            int r4 = r2 + 1
            r28[r2] = r17     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r7 = r4
            goto L_0x016b
        L_0x017b:
            if (r10 <= r2) goto L_0x017e
            goto L_0x0183
        L_0x017e:
            r6.moveToNext()     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            r0 = r3
            goto L_0x0118
        L_0x0183:
            if (r10 <= 0) goto L_0x0195
            r0 = 1
            r1.f1570e = r0     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            double r2 = (double) r10
            java.lang.Double.isNaN(r2)
            double r11 = r11 / r2
            r1.f1574i = r11     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
            java.lang.Double.isNaN(r2)
            double r8 = r8 / r2
            r1.f1575j = r8     // Catch:{ Exception -> 0x01a2, all -> 0x019b }
        L_0x0195:
            if (r6 == 0) goto L_0x01a6
        L_0x0197:
            r6.close()     // Catch:{ Exception -> 0x01a6 }
            goto L_0x01a6
        L_0x019b:
            r0 = move-exception
            if (r6 == 0) goto L_0x01a1
            r6.close()     // Catch:{ Exception -> 0x01a1 }
        L_0x01a1:
            throw r0
        L_0x01a2:
            if (r6 == 0) goto L_0x01a6
            goto L_0x0197
        L_0x01a6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0790a.m2071a(java.util.List, android.database.sqlite.SQLiteDatabase):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x00a7  */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m2073b(boolean r17) {
        /*
            r16 = this;
            r0 = r16
            boolean r1 = r0.f1570e
            r2 = 0
            r4 = 0
            r5 = 1
            if (r1 == 0) goto L_0x0016
            double r2 = r0.f1574i
            double r6 = r0.f1575j
            r8 = 4642873445846928589(0x406ecccccccccccd, double:246.4)
        L_0x0013:
            r1 = 1
            r10 = 1
            goto L_0x0025
        L_0x0016:
            boolean r1 = r0.f1569d
            if (r1 == 0) goto L_0x0021
            double r2 = r0.f1572g
            double r6 = r0.f1573h
            double r8 = r0.f1571f
            goto L_0x0013
        L_0x0021:
            r6 = r2
            r8 = r6
            r1 = 0
            r10 = 0
        L_0x0025:
            java.lang.String r11 = "{\"result\":{\"time\":\""
            if (r1 == 0) goto L_0x00a7
            r1 = 3
            r12 = 2
            r13 = 4
            java.lang.String r14 = "\"%f\",\"y\":\"%f\"},\"radius\":\"%f\",\"isCellChanged\":\"%b\"}}"
            java.lang.String r15 = "\",\"error\":\"66\"},\"content\":{\"point\":{\"x\":"
            if (r17 == 0) goto L_0x006c
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r10.append(r11)
            java.lang.String r11 = com.baidu.location.p019g.C0855k.m2452a()
            r10.append(r11)
            r10.append(r15)
            r10.append(r14)
            java.lang.String r10 = r10.toString()
            java.util.Locale r11 = java.util.Locale.CHINA
            java.lang.Object[] r13 = new java.lang.Object[r13]
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            r13[r4] = r2
            java.lang.Double r2 = java.lang.Double.valueOf(r6)
            r13[r5] = r2
            java.lang.Double r2 = java.lang.Double.valueOf(r8)
            r13[r12] = r2
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r5)
            r13[r1] = r2
            java.lang.String r1 = java.lang.String.format(r11, r10, r13)
            goto L_0x00d1
        L_0x006c:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r11)
            java.lang.String r11 = com.baidu.location.p019g.C0855k.m2452a()
            r1.append(r11)
            r1.append(r15)
            r1.append(r14)
            java.lang.String r1 = r1.toString()
            java.util.Locale r11 = java.util.Locale.CHINA
            java.lang.Object[] r13 = new java.lang.Object[r13]
            java.lang.Double r2 = java.lang.Double.valueOf(r2)
            r13[r4] = r2
            java.lang.Double r2 = java.lang.Double.valueOf(r6)
            r13[r5] = r2
            java.lang.Double r2 = java.lang.Double.valueOf(r8)
            r13[r12] = r2
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r10)
            r3 = 3
            r13[r3] = r2
            java.lang.String r1 = java.lang.String.format(r11, r1, r13)
            goto L_0x00d1
        L_0x00a7:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            if (r17 == 0) goto L_0x00bb
            r1.<init>()
            r1.append(r11)
            java.lang.String r2 = com.baidu.location.p019g.C0855k.m2452a()
            r1.append(r2)
            java.lang.String r2 = "\",\"error\":\"67\"}}"
            goto L_0x00ca
        L_0x00bb:
            r1.<init>()
            r1.append(r11)
            java.lang.String r2 = com.baidu.location.p019g.C0855k.m2452a()
            r1.append(r2)
            java.lang.String r2 = "\",\"error\":\"63\"}}"
        L_0x00ca:
            r1.append(r2)
            java.lang.String r1 = r1.toString()
        L_0x00d1:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0790a.m2073b(boolean):java.lang.String");
    }

    /* access modifiers changed from: private */
    /* renamed from: e */
    public void m2075e() {
        SQLiteDatabase sQLiteDatabase;
        try {
            sQLiteDatabase = SQLiteDatabase.openOrCreateDatabase(f1566m, (SQLiteDatabase.CursorFactory) null);
        } catch (Exception unused) {
            sQLiteDatabase = null;
        }
        if (sQLiteDatabase != null) {
            try {
                long queryNumEntries = DatabaseUtils.queryNumEntries(sQLiteDatabase, "wof");
                long queryNumEntries2 = DatabaseUtils.queryNumEntries(sQLiteDatabase, "bdcltb09");
                boolean z = queryNumEntries > 10000;
                boolean z2 = queryNumEntries2 > 10000;
                sQLiteDatabase.close();
                if (z || z2) {
                    new C0791a(this, null).execute(Boolean.valueOf(z), Boolean.valueOf(z2));
                }
            } catch (Exception unused2) {
            }
        }
    }

    /* renamed from: a */
    public BDLocation mo10554a(String str, List<ScanResult> list, boolean z) {
        if (!this.f1567a) {
            return new BDLocation("{\"result\":{\"time\":\"" + C0855k.m2452a() + "\",\"error\":\"67\"}}");
        }
        String str2 = "{\"result\":{\"time\":\"" + C0855k.m2452a() + "\",\"error\":\"67\"}}";
        try {
            m2070a(str, list);
            String b = m2073b(true);
            if (b != null) {
                str2 = b;
            }
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return new BDLocation(str2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.location.d.a.a(java.lang.String, java.util.List<android.net.wifi.ScanResult>, boolean):com.baidu.location.BDLocation
     arg types: [java.lang.String, java.util.List<android.net.wifi.ScanResult>, int]
     candidates:
      com.baidu.location.d.a.a(com.baidu.location.e.h, com.baidu.location.BDLocation, android.database.sqlite.SQLiteDatabase):void
      com.baidu.location.d.a.a(java.lang.String, com.baidu.location.e.a, android.database.sqlite.SQLiteDatabase):void
      com.baidu.location.d.a.a(java.lang.String, java.util.List<android.net.wifi.ScanResult>, boolean):com.baidu.location.BDLocation */
    /* renamed from: a */
    public BDLocation mo10555a(boolean z) {
        if (!this.f1567a) {
            return new BDLocation("{\"result\":{\"time\":\"" + C0855k.m2452a() + "\",\"error\":\"67\"}}");
        }
        C0821a f = C0822b.m2279a().mo10631f();
        BDLocation bDLocation = null;
        String g = (f == null || !f.mo10620e()) ? null : f.mo10622g();
        C0834h o = C0835i.m2376a().mo10698o();
        if (o != null) {
            bDLocation = mo10554a(g, o.f1789a, true);
        }
        if (bDLocation != null && bDLocation.getLocType() == 66) {
            StringBuffer stringBuffer = new StringBuffer(1024);
            stringBuffer.append(String.format(Locale.CHINA, "&ofl=%f|%f|%f", Double.valueOf(bDLocation.getLatitude()), Double.valueOf(bDLocation.getLongitude()), Float.valueOf(bDLocation.getRadius())));
            if (o != null && o.mo10664a() > 0) {
                stringBuffer.append("&wf=");
                stringBuffer.append(o.mo10673c(15));
            }
            if (f != null) {
                stringBuffer.append(f.mo10623h());
            }
            stringBuffer.append("&uptype=oldoff");
            stringBuffer.append(C0855k.m2467e(C0839f.getServiceContext()));
            stringBuffer.append(C0844b.m2417a().mo10713a(false));
            stringBuffer.append(C0723a.m1729a().mo10432d());
            stringBuffer.toString();
        }
        return bDLocation;
    }

    /* renamed from: a */
    public void mo10556a(String str, C0821a aVar, C0834h hVar, BDLocation bDLocation) {
        if (this.f1567a) {
            boolean z = !aVar.mo10617b() || !C0750l.m1883c().mo10485h();
            boolean z2 = bDLocation == null || bDLocation.getLocType() != 161 || (!"wf".equals(bDLocation.getNetworkLocationType()) && bDLocation.getRadius() >= 300.0f);
            if (hVar.f1789a == null) {
                z2 = true;
            }
            if ((!z || !z2) && !this.f1576k) {
                this.f1576k = true;
                new C0792b(this, null).execute(str, aVar, hVar, bDLocation);
            }
        }
    }

    /* renamed from: b */
    public void mo10557b() {
        try {
            File file = new File(f1565l);
            File file2 = new File(f1566m);
            if (!file.exists()) {
                file.mkdirs();
            }
            if (!file2.exists()) {
                file2.createNewFile();
            }
            if (file2.exists()) {
                SQLiteDatabase openOrCreateDatabase = SQLiteDatabase.openOrCreateDatabase(file2, (SQLiteDatabase.CursorFactory) null);
                openOrCreateDatabase.execSQL("CREATE TABLE IF NOT EXISTS bdcltb09(id CHAR(40) PRIMARY KEY,time DOUBLE,tag DOUBLE, type DOUBLE , ac INT);");
                openOrCreateDatabase.execSQL("CREATE TABLE IF NOT EXISTS wof(id CHAR(15) PRIMARY KEY,mktime DOUBLE,time DOUBLE, ac INT, bc INT, cc INT);");
                openOrCreateDatabase.setVersion(1);
                openOrCreateDatabase.close();
            }
            this.f1567a = true;
        } catch (Throwable unused) {
            this.f1567a = false;
        }
    }

    /* renamed from: c */
    public void mo10558c() {
        if (this.f1577n == null) {
            this.f1577n = new Handler();
        }
        this.f1577n.postDelayed(new C0793b(this), 3000);
    }
}
