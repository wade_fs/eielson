package com.baidu.location.indoor;

import android.location.Location;
import android.os.Handler;
import com.baidu.location.BDLocation;

/* renamed from: com.baidu.location.indoor.p */
public class C0908p {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public C0909a f2300a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public long f2301b = 450;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public BDLocation f2302c;

    /* renamed from: d */
    private C0910b f2303d = null;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public C0910b f2304e = null;

    /* renamed from: f */
    private C0910b f2305f = new C0910b();

    /* renamed from: g */
    private C0910b f2306g = new C0910b();

    /* renamed from: h */
    private C0910b f2307h = new C0910b();

    /* renamed from: i */
    private C0910b f2308i = new C0910b();
    /* access modifiers changed from: private */

    /* renamed from: j */
    public BDLocation f2309j = null;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public long f2310k = -1;

    /* renamed from: l */
    private boolean f2311l = false;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public Handler f2312m = new Handler();

    /* renamed from: n */
    private Runnable f2313n = new C0911q(this);
    /* access modifiers changed from: private */

    /* renamed from: o */
    public Runnable f2314o = new C0912r(this);

    /* renamed from: com.baidu.location.indoor.p$a */
    public interface C0909a {
        /* renamed from: a */
        void mo10779a(BDLocation bDLocation);
    }

    /* renamed from: com.baidu.location.indoor.p$b */
    private class C0910b {

        /* renamed from: a */
        public double f2315a;

        /* renamed from: b */
        public double f2316b;

        public C0910b() {
            this.f2315a = 0.0d;
            this.f2316b = 0.0d;
        }

        public C0910b(double d, double d2) {
            this.f2315a = d;
            this.f2316b = d2;
        }

        public C0910b(C0910b bVar) {
            this.f2315a = bVar.f2315a;
            this.f2316b = bVar.f2316b;
        }

        /* renamed from: a */
        public C0910b mo10842a(double d) {
            return new C0910b(this.f2315a * d, this.f2316b * d);
        }

        /* renamed from: a */
        public C0910b mo10843a(C0910b bVar) {
            return new C0910b(this.f2315a - bVar.f2315a, this.f2316b - bVar.f2316b);
        }

        /* renamed from: b */
        public C0910b mo10844b(C0910b bVar) {
            return new C0910b(this.f2315a + bVar.f2315a, this.f2316b + bVar.f2316b);
        }

        /* renamed from: b */
        public boolean mo10845b(double d) {
            double abs = Math.abs(this.f2315a);
            double abs2 = Math.abs(this.f2316b);
            return abs > 0.0d && abs < d && abs2 > 0.0d && abs2 < d;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public C0910b m2785a(C0910b bVar) {
        C0910b bVar2 = this.f2303d;
        if (bVar2 == null || bVar == null) {
            return null;
        }
        C0910b a = bVar2.mo10843a(bVar);
        this.f2308i = this.f2308i.mo10844b(a);
        C0910b a2 = this.f2307h.mo10843a(this.f2305f);
        this.f2305f = new C0910b(this.f2307h);
        this.f2307h = new C0910b(a);
        C0910b a3 = a.mo10842a(0.2d);
        C0910b a4 = this.f2308i.mo10842a(0.01d);
        return a3.mo10844b(a4).mo10844b(a2.mo10842a(-0.02d));
    }

    /* renamed from: a */
    public void mo10837a() {
        if (this.f2311l) {
            this.f2311l = false;
            this.f2312m.removeCallbacks(this.f2314o);
            mo10840b();
        }
    }

    /* renamed from: a */
    public void mo10838a(long j) {
        this.f2301b = j;
    }

    /* renamed from: a */
    public synchronized void mo10839a(BDLocation bDLocation) {
        BDLocation bDLocation2 = bDLocation;
        synchronized (this) {
            double latitude = bDLocation.getLatitude();
            double longitude = bDLocation.getLongitude();
            this.f2302c = bDLocation2;
            this.f2303d = new C0910b(latitude, longitude);
            if (this.f2304e == null) {
                this.f2304e = new C0910b(latitude, longitude);
            }
            if (this.f2309j == null) {
                this.f2309j = new BDLocation(bDLocation2);
            } else {
                double latitude2 = this.f2309j.getLatitude();
                double longitude2 = this.f2309j.getLongitude();
                double latitude3 = bDLocation.getLatitude();
                double longitude3 = bDLocation.getLongitude();
                float[] fArr = new float[2];
                double d = longitude3;
                Location.distanceBetween(latitude2, longitude2, latitude3, longitude3, fArr);
                if (fArr[0] > 10.0f) {
                    this.f2309j.setLatitude(latitude3);
                    this.f2309j.setLongitude(d);
                } else {
                    this.f2309j.setLatitude((latitude2 + latitude3) / 2.0d);
                    this.f2309j.setLongitude((longitude2 + d) / 2.0d);
                }
            }
        }
    }

    /* renamed from: b */
    public void mo10840b() {
        this.f2310k = -1;
        this.f2304e = null;
        this.f2303d = null;
        this.f2305f = new C0910b();
        this.f2306g = new C0910b();
        this.f2307h = new C0910b();
        this.f2308i = new C0910b();
        this.f2309j = null;
    }

    /* renamed from: c */
    public boolean mo10841c() {
        return this.f2311l;
    }
}
