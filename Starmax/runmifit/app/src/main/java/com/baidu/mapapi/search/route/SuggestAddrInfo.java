package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import java.util.List;

public class SuggestAddrInfo implements Parcelable {
    public static final Parcelable.Creator<SuggestAddrInfo> CREATOR = new C0998n();

    /* renamed from: a */
    private List<PoiInfo> f3099a;

    /* renamed from: b */
    private List<PoiInfo> f3100b;

    /* renamed from: c */
    private List<List<PoiInfo>> f3101c;

    /* renamed from: d */
    private List<CityInfo> f3102d;

    /* renamed from: e */
    private List<CityInfo> f3103e;

    /* renamed from: f */
    private List<List<CityInfo>> f3104f;

    public SuggestAddrInfo() {
    }

    SuggestAddrInfo(Parcel parcel) {
        this.f3099a = parcel.readArrayList(PoiInfo.class.getClassLoader());
        this.f3100b = parcel.readArrayList(PoiInfo.class.getClassLoader());
        this.f3101c = parcel.readArrayList(PoiInfo.class.getClassLoader());
        this.f3102d = parcel.readArrayList(CityInfo.class.getClassLoader());
        this.f3103e = parcel.readArrayList(CityInfo.class.getClassLoader());
        this.f3104f = parcel.readArrayList(CityInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public List<CityInfo> getSuggestEndCity() {
        return this.f3103e;
    }

    public List<PoiInfo> getSuggestEndNode() {
        return this.f3100b;
    }

    public List<CityInfo> getSuggestStartCity() {
        return this.f3102d;
    }

    public List<PoiInfo> getSuggestStartNode() {
        return this.f3099a;
    }

    public List<List<CityInfo>> getSuggestWpCity() {
        return this.f3104f;
    }

    public List<List<PoiInfo>> getSuggestWpNode() {
        return this.f3101c;
    }

    public void setSuggestEndCity(List<CityInfo> list) {
        this.f3103e = list;
    }

    public void setSuggestEndNode(List<PoiInfo> list) {
        this.f3100b = list;
    }

    public void setSuggestStartCity(List<CityInfo> list) {
        this.f3102d = list;
    }

    public void setSuggestStartNode(List<PoiInfo> list) {
        this.f3099a = list;
    }

    public void setSuggestWpCity(List<List<CityInfo>> list) {
        this.f3104f = list;
    }

    public void setSuggestWpNode(List<List<PoiInfo>> list) {
        this.f3101c = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.f3099a);
        parcel.writeList(this.f3100b);
        parcel.writeList(this.f3101c);
        parcel.writeList(this.f3102d);
        parcel.writeList(this.f3103e);
        parcel.writeList(this.f3104f);
    }
}
