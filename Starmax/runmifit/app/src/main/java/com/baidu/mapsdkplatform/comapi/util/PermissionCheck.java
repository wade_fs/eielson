package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.baidu.lbsapi.auth.LBSAuthManager;
import com.baidu.lbsapi.auth.LBSAuthManagerListener;
import com.baidu.mobstat.Config;
import com.tencent.bugly.beta.tinker.TinkerReport;
import java.util.Hashtable;
import org.json.JSONException;
import org.json.JSONObject;

public class PermissionCheck {

    /* renamed from: a */
    public static int f3866a = 200;

    /* renamed from: b */
    public static int f3867b = 202;

    /* renamed from: c */
    public static int f3868c = TinkerReport.KEY_LOADED_EXCEPTION_DEX;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static final String f3869d = PermissionCheck.class.getSimpleName();
    /* access modifiers changed from: private */

    /* renamed from: e */
    public static Context f3870e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static String f3871f;

    /* renamed from: g */
    private static Hashtable<String, String> f3872g;

    /* renamed from: h */
    private static LBSAuthManager f3873h = null;

    /* renamed from: i */
    private static LBSAuthManagerListener f3874i = null;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public static C1163c f3875j = null;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public static int f3876k = LBSAuthManager.CODE_UNAUTHENTICATE;

    /* renamed from: com.baidu.mapsdkplatform.comapi.util.PermissionCheck$a */
    private static class C1161a implements LBSAuthManagerListener {
        private C1161a() {
        }

        public void onAuthResult(int i, String str) {
            if (str == null) {
                Log.e(PermissionCheck.f3869d, "The result is null");
                int permissionCheck = PermissionCheck.permissionCheck();
                String a = PermissionCheck.f3869d;
                Log.d(a, "onAuthResult try permissionCheck result is: " + permissionCheck);
                return;
            }
            C1162b bVar = new C1162b();
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                    bVar.f3877a = jSONObject.optInt(NotificationCompat.CATEGORY_STATUS);
                }
                if (jSONObject.has("appid")) {
                    bVar.f3879c = jSONObject.optString("appid");
                }
                if (jSONObject.has(Config.CUSTOM_USER_ID)) {
                    bVar.f3878b = jSONObject.optString(Config.CUSTOM_USER_ID);
                }
                if (jSONObject.has("message")) {
                    bVar.f3880d = jSONObject.optString("message");
                }
                if (jSONObject.has("token")) {
                    bVar.f3881e = jSONObject.optString("token");
                }
                if (jSONObject.has("ak_permission")) {
                    bVar.f3882f = jSONObject.optInt("ak_permission");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            int unused = PermissionCheck.f3876k = bVar.f3877a;
            if (PermissionCheck.f3875j != null) {
                PermissionCheck.f3875j.mo12867a(bVar);
            }
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.util.PermissionCheck$b */
    public static class C1162b {

        /* renamed from: a */
        public int f3877a = 0;

        /* renamed from: b */
        public String f3878b = "-1";

        /* renamed from: c */
        public String f3879c = "-1";

        /* renamed from: d */
        public String f3880d = "";

        /* renamed from: e */
        public String f3881e;

        /* renamed from: f */
        public int f3882f;

        public String toString() {
            return String.format("=============================================\n----------------- 鉴权错误信息 ------------\nsha1;package:%s\nkey:%s\nerrorcode: %d uid: %s appid %s msg: %s\n请仔细核查 SHA1、package与key申请信息是否对应，key是否删除，平台是否匹配\nerrorcode为230时，请参考论坛链接：\nhttp://bbs.lbsyun.baidu.com/forum.php?mod=viewthread&tid=106461\n=============================================\n", C1164a.m4230a(PermissionCheck.f3870e), PermissionCheck.f3871f, Integer.valueOf(this.f3877a), this.f3878b, this.f3879c, this.f3880d);
        }
    }

    /* renamed from: com.baidu.mapsdkplatform.comapi.util.PermissionCheck$c */
    public interface C1163c {
        /* renamed from: a */
        void mo12867a(C1162b bVar);
    }

    public static void destory() {
        f3875j = null;
        f3870e = null;
        f3874i = null;
    }

    public static int getPermissionResult() {
        return f3876k;
    }

    public static void init(Context context) {
        ApplicationInfo applicationInfo;
        String str;
        f3870e = context;
        try {
            applicationInfo = f3870e.getPackageManager().getApplicationInfo(f3870e.getPackageName(), 128);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            applicationInfo = null;
        }
        if (applicationInfo != null) {
            f3871f = applicationInfo.metaData.getString("com.baidu.lbsapi.API_KEY");
        }
        if (f3872g == null) {
            f3872g = new Hashtable<>();
        }
        if (f3873h == null) {
            f3873h = LBSAuthManager.getInstance(f3870e);
        }
        if (f3874i == null) {
            f3874i = new C1161a();
        }
        try {
            str = context.getPackageManager().getPackageInfo(f3870e.getPackageName(), 0).applicationInfo.loadLabel(f3870e.getPackageManager()).toString();
        } catch (Exception e2) {
            e2.printStackTrace();
            str = "";
        }
        Bundle b = C1175i.m4268b();
        if (b != null) {
            f3872g.put("mb", b.getString("mb"));
            f3872g.put("os", b.getString("os"));
            f3872g.put("sv", b.getString("sv"));
            f3872g.put("imt", "1");
            f3872g.put("net", b.getString("net"));
            f3872g.put("cpu", b.getString("cpu"));
            f3872g.put("glr", b.getString("glr"));
            f3872g.put("glv", b.getString("glv"));
            f3872g.put("resid", b.getString("resid"));
            f3872g.put("appid", "-1");
            f3872g.put("ver", "1");
            f3872g.put("screen", String.format("(%d,%d)", Integer.valueOf(b.getInt("screen_x")), Integer.valueOf(b.getInt("screen_y"))));
            f3872g.put("dpi", String.format("(%d,%d)", Integer.valueOf(b.getInt("dpi_x")), Integer.valueOf(b.getInt("dpi_y"))));
            f3872g.put("pcn", b.getString("pcn"));
            f3872g.put("cuid", b.getString("cuid"));
            f3872g.put(Config.FEED_LIST_NAME, str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0036, code lost:
        return r1;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized int permissionCheck() {
        /*
            java.lang.Class<com.baidu.mapsdkplatform.comapi.util.PermissionCheck> r0 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.class
            monitor-enter(r0)
            com.baidu.lbsapi.auth.LBSAuthManager r1 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3873h     // Catch:{ all -> 0x0065 }
            r2 = 0
            if (r1 == 0) goto L_0x0037
            com.baidu.lbsapi.auth.LBSAuthManagerListener r1 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3874i     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x0037
            android.content.Context r1 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3870e     // Catch:{ all -> 0x0065 }
            if (r1 != 0) goto L_0x0011
            goto L_0x0037
        L_0x0011:
            com.baidu.lbsapi.auth.LBSAuthManager r1 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3873h     // Catch:{ all -> 0x0065 }
            java.lang.String r3 = "lbs_androidmapsdk"
            java.util.Hashtable<java.lang.String, java.lang.String> r4 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3872g     // Catch:{ all -> 0x0065 }
            com.baidu.lbsapi.auth.LBSAuthManagerListener r5 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3874i     // Catch:{ all -> 0x0065 }
            int r1 = r1.authenticate(r2, r3, r4, r5)     // Catch:{ all -> 0x0065 }
            if (r1 == 0) goto L_0x0035
            java.lang.String r2 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3869d     // Catch:{ all -> 0x0065 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0065 }
            r3.<init>()     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = "permission check result is: "
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            r3.append(r1)     // Catch:{ all -> 0x0065 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0065 }
            android.util.Log.e(r2, r3)     // Catch:{ all -> 0x0065 }
        L_0x0035:
            monitor-exit(r0)
            return r1
        L_0x0037:
            java.lang.String r1 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3869d     // Catch:{ all -> 0x0065 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0065 }
            r3.<init>()     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = "The authManager is: "
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            com.baidu.lbsapi.auth.LBSAuthManager r4 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3873h     // Catch:{ all -> 0x0065 }
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = "; the authCallback is: "
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            com.baidu.lbsapi.auth.LBSAuthManagerListener r4 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3874i     // Catch:{ all -> 0x0065 }
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            java.lang.String r4 = "; the mContext is: "
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            android.content.Context r4 = com.baidu.mapsdkplatform.comapi.util.PermissionCheck.f3870e     // Catch:{ all -> 0x0065 }
            r3.append(r4)     // Catch:{ all -> 0x0065 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0065 }
            android.util.Log.e(r1, r3)     // Catch:{ all -> 0x0065 }
            monitor-exit(r0)
            return r2
        L_0x0065:
            r1 = move-exception
            monitor-exit(r0)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.util.PermissionCheck.permissionCheck():int");
    }

    public static void setPermissionCheckResultListener(C1163c cVar) {
        f3875j = cVar;
    }
}
