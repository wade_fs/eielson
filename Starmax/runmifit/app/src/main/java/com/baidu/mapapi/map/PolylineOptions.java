package com.baidu.mapapi.map;

import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.util.Log;
import com.baidu.mapapi.model.LatLng;
import java.util.ArrayList;
import java.util.List;

public final class PolylineOptions extends OverlayOptions {

    /* renamed from: a */
    int f2685a;

    /* renamed from: b */
    boolean f2686b = true;

    /* renamed from: c */
    Bundle f2687c;

    /* renamed from: d */
    private int f2688d = ViewCompat.MEASURED_STATE_MASK;

    /* renamed from: e */
    private List<LatLng> f2689e;

    /* renamed from: f */
    private List<Integer> f2690f;

    /* renamed from: g */
    private List<Integer> f2691g;

    /* renamed from: h */
    private int f2692h = 5;

    /* renamed from: i */
    private BitmapDescriptor f2693i;

    /* renamed from: j */
    private List<BitmapDescriptor> f2694j;

    /* renamed from: k */
    private boolean f2695k = true;

    /* renamed from: l */
    private boolean f2696l = false;

    /* renamed from: m */
    private boolean f2697m = false;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        Polyline polyline = new Polyline();
        polyline.f2662A = this.f2686b;
        polyline.f2680f = this.f2697m;
        polyline.f2665z = this.f2685a;
        polyline.f2663B = this.f2687c;
        List<LatLng> list = this.f2689e;
        if (list == null || list.size() < 2) {
            throw new IllegalStateException("BDMapSDKException: when you add polyline, you must at least supply 2 points");
        }
        polyline.f2676b = this.f2689e;
        polyline.f2675a = this.f2688d;
        polyline.f2679e = this.f2692h;
        polyline.f2683i = this.f2693i;
        polyline.f2684j = this.f2694j;
        polyline.f2681g = this.f2695k;
        polyline.f2682h = this.f2696l;
        List<Integer> list2 = this.f2690f;
        if (list2 != null && list2.size() < this.f2689e.size() - 1) {
            ArrayList arrayList = new ArrayList((this.f2689e.size() - 1) - this.f2690f.size());
            List<Integer> list3 = this.f2690f;
            list3.addAll(list3.size(), arrayList);
        }
        List<Integer> list4 = this.f2690f;
        int i = 0;
        if (list4 != null && list4.size() > 0) {
            int[] iArr = new int[this.f2690f.size()];
            int i2 = 0;
            for (Integer num : this.f2690f) {
                iArr[i2] = num.intValue();
                i2++;
            }
            polyline.f2677c = iArr;
        }
        List<Integer> list5 = this.f2691g;
        if (list5 != null && list5.size() < this.f2689e.size() - 1) {
            ArrayList arrayList2 = new ArrayList((this.f2689e.size() - 1) - this.f2691g.size());
            List<Integer> list6 = this.f2691g;
            list6.addAll(list6.size(), arrayList2);
        }
        List<Integer> list7 = this.f2691g;
        if (list7 != null && list7.size() > 0) {
            int[] iArr2 = new int[this.f2691g.size()];
            for (Integer num2 : this.f2691g) {
                iArr2[i] = num2.intValue();
                i++;
            }
            polyline.f2678d = iArr2;
        }
        return polyline;
    }

    public PolylineOptions color(int i) {
        this.f2688d = i;
        return this;
    }

    public PolylineOptions colorsValues(List<Integer> list) {
        if (list == null) {
            throw new IllegalArgumentException("BDMapSDKException: colors list can not be null");
        } else if (!list.contains(null)) {
            this.f2691g = list;
            return this;
        } else {
            throw new IllegalArgumentException("BDMapSDKException: colors list can not contains null");
        }
    }

    public PolylineOptions customTexture(BitmapDescriptor bitmapDescriptor) {
        this.f2693i = bitmapDescriptor;
        return this;
    }

    public PolylineOptions customTextureList(List<BitmapDescriptor> list) {
        if (list != null) {
            if (list.size() == 0) {
                Log.e("baidumapsdk", "custom texture list is empty,the texture will not work");
            }
            for (BitmapDescriptor bitmapDescriptor : list) {
                if (bitmapDescriptor == null) {
                    Log.e("baidumapsdk", "the custom texture item is null,it will be discard");
                }
            }
            this.f2694j = list;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: customTexture list can not be null");
    }

    public PolylineOptions dottedLine(boolean z) {
        this.f2697m = z;
        return this;
    }

    public PolylineOptions extraInfo(Bundle bundle) {
        this.f2687c = bundle;
        return this;
    }

    public PolylineOptions focus(boolean z) {
        this.f2695k = z;
        return this;
    }

    public int getColor() {
        return this.f2688d;
    }

    public BitmapDescriptor getCustomTexture() {
        return this.f2693i;
    }

    public List<BitmapDescriptor> getCustomTextureList() {
        return this.f2694j;
    }

    public Bundle getExtraInfo() {
        return this.f2687c;
    }

    public List<LatLng> getPoints() {
        return this.f2689e;
    }

    public List<Integer> getTextureIndexs() {
        return this.f2690f;
    }

    public int getWidth() {
        return this.f2692h;
    }

    public int getZIndex() {
        return this.f2685a;
    }

    public boolean isDottedLine() {
        return this.f2697m;
    }

    public boolean isFocus() {
        return this.f2695k;
    }

    public boolean isVisible() {
        return this.f2686b;
    }

    public PolylineOptions keepScale(boolean z) {
        this.f2696l = z;
        return this;
    }

    public PolylineOptions points(List<LatLng> list) {
        if (list == null) {
            throw new IllegalArgumentException("BDMapSDKException: points list can not be null");
        } else if (list.size() < 2) {
            throw new IllegalArgumentException("BDMapSDKException: points count can not less than 2");
        } else if (!list.contains(null)) {
            this.f2689e = list;
            return this;
        } else {
            throw new IllegalArgumentException("BDMapSDKException: points list can not contains null");
        }
    }

    public PolylineOptions textureIndex(List<Integer> list) {
        if (list == null) {
            throw new IllegalArgumentException("BDMapSDKException: indexs list can not be null");
        } else if (!list.contains(null)) {
            this.f2690f = list;
            return this;
        } else {
            throw new IllegalArgumentException("BDMapSDKException: index list can not contains null");
        }
    }

    public PolylineOptions visible(boolean z) {
        this.f2686b = z;
        return this;
    }

    public PolylineOptions width(int i) {
        if (i > 0) {
            this.f2692h = i;
        }
        return this;
    }

    public PolylineOptions zIndex(int i) {
        this.f2685a = i;
        return this;
    }
}
