package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import java.util.ArrayList;

/* renamed from: com.baidu.mapsdkplatform.comapi.util.d */
public class C1169d {

    /* renamed from: a */
    private ArrayList<Integer> f3892a;

    /* renamed from: b */
    private Context f3893b;

    /* renamed from: com.baidu.mapsdkplatform.comapi.util.d$a */
    private static class C1170a {
        /* access modifiers changed from: private */

        /* renamed from: a */
        public static final C1169d f3894a = new C1169d();
    }

    private C1169d() {
        this.f3892a = new ArrayList<>();
    }

    /* renamed from: a */
    private int m4245a(String str) {
        Context context = this.f3893b;
        if (context == null) {
            return -101;
        }
        return context.getSharedPreferences("ad_auth", 0).getInt(str, 0);
    }

    /* renamed from: a */
    public static C1169d m4246a() {
        return C1170a.f3894a;
    }

    /* renamed from: a */
    private void m4247a(String str, int i) {
        Context context = this.f3893b;
        if (context != null) {
            context.getSharedPreferences("ad_auth", 0).edit().putInt(str, i).apply();
        }
    }

    /* renamed from: a */
    public void mo13393a(int i) {
        if (i != -1 || (i = m4245a("ad_key")) != -101) {
            for (int i2 = i; i2 != 0; i2 /= 2) {
                this.f3892a.add(Integer.valueOf(i2 % 2));
            }
            m4247a("ad_key", i);
        }
    }

    /* renamed from: a */
    public void mo13394a(Context context) {
        this.f3893b = context;
    }

    /* renamed from: b */
    public boolean mo13395b() {
        ArrayList<Integer> arrayList = this.f3892a;
        return arrayList != null && arrayList.size() > 0 && this.f3892a.get(0).intValue() == 1;
    }
}
