package com.baidu.mobstat;

import android.content.Context;
import java.io.File;

public final class NativeCrashHandler {

    /* renamed from: a */
    private static boolean f4202a = true;

    /* renamed from: b */
    private static Context f4203b;

    private static native void nativeException();

    private static native void nativeInit(String str);

    private static native void nativeProcess(String str);

    private static native void nativeUnint();

    static {
        try {
            System.loadLibrary("crash_analysis");
        } catch (Throwable unused) {
        }
    }

    private NativeCrashHandler() {
    }

    public static void doNativeCrash() {
        if (f4202a) {
            try {
                nativeException();
            } catch (Throwable unused) {
            }
        }
    }

    public static void init(Context context) {
        if (context != null) {
            f4203b = context;
            if (f4202a) {
                File cacheDir = context.getCacheDir();
                if (cacheDir.exists() && cacheDir.isDirectory()) {
                    try {
                        nativeInit(cacheDir.getAbsolutePath());
                    } catch (Throwable unused) {
                    }
                }
            }
        }
    }

    public static void uninit() {
        if (f4202a) {
            try {
                nativeUnint();
            } catch (Throwable unused) {
            }
        }
    }

    public static void process(String str) {
        if (str != null && str.length() != 0 && f4202a) {
            File file = new File(str);
            if (file.exists() && file.isFile()) {
                try {
                    nativeProcess(str);
                } catch (Throwable unused) {
                }
            }
        }
    }

    public static void onCrashCallbackFromNative(String str) {
        ExceptionAnalysis.getInstance().saveCrashInfo(f4203b, System.currentTimeMillis(), str, "NativeException", 1, 0);
    }
}
