package com.baidu.mapapi.search.poi;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.HashMap;
import java.util.Map;
import org.greenrobot.greendao.generator.Schema;

public final class PoiFilter implements Parcelable {
    public static final Parcelable.Creator<PoiFilter> CREATOR = new C0981c();
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static Map<SortName, String> f2990f = new HashMap();

    /* renamed from: a */
    private String f2991a = "";

    /* renamed from: b */
    private String f2992b = "";

    /* renamed from: c */
    private String f2993c = "";

    /* renamed from: d */
    private String f2994d = "";

    /* renamed from: e */
    private String f2995e = "";

    public static final class Builder {

        /* renamed from: a */
        private String f2996a;

        /* renamed from: b */
        private String f2997b;

        /* renamed from: c */
        private String f2998c;

        /* renamed from: d */
        private String f2999d;

        /* renamed from: e */
        private String f3000e;

        public Builder() {
            PoiFilter.f2990f.put(SortName.HotelSortName.DEFAULT, Schema.DEFAULT_NAME);
            PoiFilter.f2990f.put(SortName.HotelSortName.HOTEL_LEVEL, "level");
            PoiFilter.f2990f.put(SortName.HotelSortName.HOTEL_PRICE, "price");
            PoiFilter.f2990f.put(SortName.HotelSortName.HOTEL_DISTANCE, "distance");
            PoiFilter.f2990f.put(SortName.HotelSortName.HOTEL_HEALTH_SCORE, "health_score");
            PoiFilter.f2990f.put(SortName.HotelSortName.HOTEL_TOTAL_SCORE, "total_score");
            PoiFilter.f2990f.put(SortName.CaterSortName.DEFAULT, Schema.DEFAULT_NAME);
            PoiFilter.f2990f.put(SortName.CaterSortName.CATER_DISTANCE, "distance");
            PoiFilter.f2990f.put(SortName.CaterSortName.CATER_PRICE, "price");
            PoiFilter.f2990f.put(SortName.CaterSortName.CATER_OVERALL_RATING, "overall_rating");
            PoiFilter.f2990f.put(SortName.CaterSortName.CATER_SERVICE_RATING, "service_rating");
            PoiFilter.f2990f.put(SortName.CaterSortName.CATER_TASTE_RATING, "taste_rating");
            PoiFilter.f2990f.put(SortName.LifeSortName.DEFAULT, Schema.DEFAULT_NAME);
            PoiFilter.f2990f.put(SortName.LifeSortName.PRICE, "price");
            PoiFilter.f2990f.put(SortName.LifeSortName.LIFE_COMMENT_RATING, "comment_num");
            PoiFilter.f2990f.put(SortName.LifeSortName.LIFE_OVERALL_RATING, "overall_rating");
            PoiFilter.f2990f.put(SortName.LifeSortName.DISTANCE, "distance");
        }

        public PoiFilter build() {
            return new PoiFilter(this.f2996a, this.f2997b, this.f2998c, this.f3000e, this.f2999d);
        }

        public Builder industryType(IndustryType industryType) {
            int i = C0982d.f3018a[industryType.ordinal()];
            this.f2996a = i != 1 ? i != 2 ? i != 3 ? "" : "life" : "cater" : "hotel";
            return this;
        }

        public Builder isDiscount(boolean z) {
            this.f3000e = z ? "1" : "0";
            return this;
        }

        public Builder isGroupon(boolean z) {
            this.f2999d = z ? "1" : "0";
            return this;
        }

        public Builder sortName(SortName sortName) {
            if (!TextUtils.isEmpty(this.f2996a) && sortName != null) {
                this.f2997b = (String) PoiFilter.f2990f.get(sortName);
            }
            return this;
        }

        public Builder sortRule(int i) {
            this.f2998c = i + "";
            return this;
        }
    }

    public enum IndustryType {
        HOTEL,
        CATER,
        LIFE
    }

    public interface SortName {

        public enum CaterSortName implements SortName {
            DEFAULT,
            CATER_PRICE,
            CATER_DISTANCE,
            CATER_TASTE_RATING,
            CATER_OVERALL_RATING,
            CATER_SERVICE_RATING
        }

        public enum HotelSortName implements SortName {
            DEFAULT,
            HOTEL_PRICE,
            HOTEL_DISTANCE,
            HOTEL_TOTAL_SCORE,
            HOTEL_LEVEL,
            HOTEL_HEALTH_SCORE
        }

        public enum LifeSortName implements SortName {
            DEFAULT,
            PRICE,
            DISTANCE,
            LIFE_OVERALL_RATING,
            LIFE_COMMENT_RATING
        }
    }

    protected PoiFilter(Parcel parcel) {
        this.f2991a = parcel.readString();
        this.f2992b = parcel.readString();
        this.f2993c = parcel.readString();
        this.f2995e = parcel.readString();
        this.f2994d = parcel.readString();
    }

    PoiFilter(String str, String str2, String str3, String str4, String str5) {
        this.f2991a = str;
        this.f2992b = str2;
        this.f2993c = str3;
        this.f2995e = str4;
        this.f2994d = str5;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (!TextUtils.isEmpty(this.f2991a)) {
            sb.append("industry_type:");
            sb.append(this.f2991a);
            sb.append("|");
        }
        if (!TextUtils.isEmpty(this.f2992b)) {
            sb.append("sort_name:");
            sb.append(this.f2992b);
            sb.append("|");
        }
        if (!TextUtils.isEmpty(this.f2993c)) {
            sb.append("sort_rule:");
            sb.append(this.f2993c);
            sb.append("|");
        }
        if (!TextUtils.isEmpty(this.f2995e)) {
            sb.append("discount:");
            sb.append(this.f2995e);
            sb.append("|");
        }
        if (!TextUtils.isEmpty(this.f2994d)) {
            sb.append("groupon:");
            sb.append(this.f2994d);
            sb.append("|");
        }
        if (!TextUtils.isEmpty(sb.toString())) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2991a);
        parcel.writeString(this.f2992b);
        parcel.writeString(this.f2993c);
        parcel.writeString(this.f2995e);
        parcel.writeString(this.f2994d);
    }
}
