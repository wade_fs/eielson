package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.os.Message;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c;
import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1118e;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.h */
class C1138h extends C1118e {

    /* renamed from: a */
    final /* synthetic */ C1135g f3772a;

    C1138h(C1135g gVar) {
        this.f3772a = gVar;
    }

    /* renamed from: a */
    public void mo13186a(C1115c.C1116a aVar) {
        Message obtainMessage = this.f3772a.f3763j.obtainMessage();
        obtainMessage.what = 100001;
        obtainMessage.obj = aVar;
        if (this.f3772a.f3763j != null) {
            this.f3772a.f3763j.sendMessage(obtainMessage);
        }
        if (C1115c.C1116a.SERVER_ERROR == aVar || C1115c.C1116a.NETWORK_ERROR == aVar || C1115c.C1116a.INNER_ERROR == aVar) {
            C1135g.m3953b(this.f3772a);
        }
    }

    /* renamed from: a */
    public void mo13187a(String str) {
        Message obtainMessage = this.f3772a.f3763j.obtainMessage();
        obtainMessage.what = 100000;
        obtainMessage.obj = str;
        if (this.f3772a.f3763j != null) {
            this.f3772a.f3763j.sendMessage(obtainMessage);
        }
        int unused = this.f3772a.f3764k = 0;
    }
}
