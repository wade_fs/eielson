package com.baidu.mobstat;

import android.app.ActivityManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Process;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import com.baidu.mobstat.C1262ay;
import com.baidu.mobstat.C1271be;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.bh */
public class C1276bh {

    /* renamed from: a */
    private static String f4341a;

    /* renamed from: b */
    private static String f4342b;

    /* renamed from: c */
    private static String f4343c;

    /* renamed from: d */
    private static final Pattern f4344d = Pattern.compile("\\s*|\t|\r|\n");

    /* renamed from: a */
    public static String m4716a(Context context, String str) {
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128);
            if (applicationInfo == null) {
                return "";
            }
            Object obj = null;
            if (applicationInfo.metaData != null) {
                obj = applicationInfo.metaData.get(str);
            }
            if (obj != null) {
                return obj.toString();
            }
            C1256at c = C1256at.m4629c();
            c.mo13941a("can't find information in AndroidManifest.xml for key " + str);
            return "";
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: a */
    public static String m4713a(int i, Context context) {
        try {
            return C1262ay.C1264b.m4672c(i, m4714a(context).getBytes());
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: a */
    public static String m4714a(Context context) {
        return f4344d.matcher(C1278bi.m4753a(context)).replaceAll("");
    }

    /* renamed from: b */
    public static String m4720b(Context context) {
        return C1271be.C1272a.m4701a(m4714a(context).getBytes()).toUpperCase(Locale.US);
    }

    /* renamed from: c */
    public static int m4722c(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            displayMetrics = m4728e(context);
        } catch (Exception unused) {
        }
        return displayMetrics.widthPixels;
    }

    /* renamed from: d */
    public static int m4726d(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        try {
            displayMetrics = m4728e(context);
        } catch (Exception unused) {
        }
        return displayMetrics.heightPixels;
    }

    /* renamed from: e */
    public static DisplayMetrics m4728e(Context context) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((WindowManager) context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics;
    }

    /* renamed from: f */
    public static int m4730f(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (Exception unused) {
            return 1;
        }
    }

    /* renamed from: g */
    public static String m4733g(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: h */
    public static String m4735h(Context context) {
        CellLocation cellLocation;
        String format = String.format("%s_%s_%s", 0, 0, 0);
        try {
            if ((!C1267ba.m4689e(context, "android.permission.ACCESS_FINE_LOCATION") && !C1267ba.m4689e(context, "android.permission.ACCESS_COARSE_LOCATION")) || (cellLocation = ((TelephonyManager) context.getSystemService("phone")).getCellLocation()) == null) {
                return format;
            }
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                return String.format("%s_%s_%s", String.format("%d", Integer.valueOf(gsmCellLocation.getCid())), String.format("%d", Integer.valueOf(gsmCellLocation.getLac())), 0);
            }
            String[] split = cellLocation.toString().replace("[", "").replace("]", "").split(",");
            return String.format("%s_%s_%s", split[0], split[3], split[4]);
        } catch (Exception unused) {
        }
        return format;
    }

    /* renamed from: i */
    public static String m4736i(Context context) {
        Location lastKnownLocation;
        try {
            return (!C1267ba.m4689e(context, "android.permission.ACCESS_FINE_LOCATION") || (lastKnownLocation = ((LocationManager) context.getSystemService("location")).getLastKnownLocation("gps")) == null) ? "" : String.format("%s_%s_%s", Long.valueOf(lastKnownLocation.getTime()), Double.valueOf(lastKnownLocation.getLongitude()), Double.valueOf(lastKnownLocation.getLatitude()));
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: b */
    public static String m4719b(int i, Context context) {
        String k = m4738k(context);
        return TextUtils.isEmpty(k) ? "" : C1262ay.C1264b.m4672c(i, k.getBytes());
    }

    /* renamed from: j */
    public static String m4737j(Context context) {
        if (Build.VERSION.SDK_INT < 23) {
            return m4738k(context);
        }
        return m4723c();
    }

    /* renamed from: k */
    public static String m4738k(Context context) {
        WifiInfo connectionInfo;
        try {
            if (!C1267ba.m4689e(context, "android.permission.ACCESS_WIFI_STATE") || (connectionInfo = ((WifiManager) context.getSystemService("wifi")).getConnectionInfo()) == null) {
                return "";
            }
            String macAddress = connectionInfo.getMacAddress();
            return !TextUtils.isEmpty(macAddress) ? macAddress : "";
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: c */
    private static String m4723c() {
        if (Build.VERSION.SDK_INT < 9) {
            return "";
        }
        try {
            for (NetworkInterface networkInterface : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (networkInterface.getName().equalsIgnoreCase("wlan0")) {
                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress == null) {
                        return "";
                    }
                    StringBuilder sb = new StringBuilder();
                    int length = hardwareAddress.length;
                    for (int i = 0; i < length; i++) {
                        sb.append(String.format("%02x:", Byte.valueOf(hardwareAddress[i])));
                    }
                    if (sb.length() > 0) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    return sb.toString();
                }
            }
        } catch (Throwable unused) {
        }
        return "";
    }

    /* renamed from: a */
    private static String m4712a(byte b) {
        String str = "00" + Integer.toHexString(b) + Config.TRACE_TODAY_VISIT_SPLIT;
        return str.substring(str.length() - 3);
    }

    /* renamed from: c */
    public static String m4724c(int i, Context context) {
        String d = m4727d(i, context);
        String c = !TextUtils.isEmpty(d) ? C1262ay.C1264b.m4672c(i, d.getBytes()) : null;
        return TextUtils.isEmpty(c) ? "" : c;
    }

    /* renamed from: d */
    public static String m4727d(int i, Context context) {
        String a = m4711a();
        if (TextUtils.isEmpty(a)) {
            a = m4729e(i, context);
        }
        return TextUtils.isEmpty(a) ? "" : a;
    }

    /* renamed from: e */
    public static String m4729e(int i, Context context) {
        StringBuffer stringBuffer = new StringBuffer();
        byte[] bArr = null;
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface nextElement = networkInterfaces.nextElement();
                Enumeration<InetAddress> inetAddresses = nextElement.getInetAddresses();
                while (true) {
                    if (!inetAddresses.hasMoreElements()) {
                        break;
                    }
                    InetAddress nextElement2 = inetAddresses.nextElement();
                    if (!nextElement2.isAnyLocalAddress() && (nextElement2 instanceof Inet4Address)) {
                        if (!nextElement2.isLoopbackAddress()) {
                            if (nextElement2.isSiteLocalAddress()) {
                                bArr = nextElement.getHardwareAddress();
                            } else if (!nextElement2.isLinkLocalAddress()) {
                                bArr = nextElement.getHardwareAddress();
                                break;
                            }
                        }
                    }
                }
            }
        } catch (Exception unused) {
        }
        if (bArr != null) {
            for (byte b : bArr) {
                stringBuffer.append(m4712a(b));
            }
            return stringBuffer.substring(0, stringBuffer.length() - 1).replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, "");
        }
        String b2 = m4719b(i, context);
        return b2 != null ? b2.replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, "") : b2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x005a A[SYNTHETIC, Splitter:B:27:0x005a] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0061 A[SYNTHETIC, Splitter:B:34:0x0061] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m4711a() {
        /*
            java.lang.StringBuffer r0 = new java.lang.StringBuffer
            r0.<init>()
            r1 = 20
            r2 = 0
            char[] r1 = new char[r1]     // Catch:{ Exception -> 0x005e, all -> 0x0056 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x005e, all -> 0x0056 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005e, all -> 0x0056 }
            java.lang.String r5 = "/sys/class/net/eth0/address"
            r4.<init>(r5)     // Catch:{ Exception -> 0x005e, all -> 0x0056 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x005e, all -> 0x0056 }
        L_0x0016:
            int r4 = r3.read(r1)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r5 = -1
            if (r4 == r5) goto L_0x003e
            int r5 = r1.length     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r6 = 13
            if (r4 != r5) goto L_0x002f
            int r5 = r1.length     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            int r5 = r5 + -1
            char r5 = r1[r5]     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            if (r5 == r6) goto L_0x002f
            java.io.PrintStream r4 = java.lang.System.out     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r4.print(r1)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            goto L_0x0016
        L_0x002f:
            r5 = 0
        L_0x0030:
            if (r5 >= r4) goto L_0x0016
            char r7 = r1[r5]     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            if (r7 == r6) goto L_0x003b
            char r7 = r1[r5]     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r0.append(r7)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
        L_0x003b:
            int r5 = r5 + 1
            goto L_0x0030
        L_0x003e:
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            java.lang.String r1 = ":"
            java.lang.String r4 = ""
            java.lang.String r0 = r0.replaceAll(r1, r4)     // Catch:{ Exception -> 0x0054, all -> 0x0052 }
            r3.close()     // Catch:{ Exception -> 0x0051 }
        L_0x0051:
            return r0
        L_0x0052:
            r0 = move-exception
            goto L_0x0058
        L_0x0054:
            goto L_0x005f
        L_0x0056:
            r0 = move-exception
            r3 = r2
        L_0x0058:
            if (r3 == 0) goto L_0x005d
            r3.close()     // Catch:{ Exception -> 0x005d }
        L_0x005d:
            throw r0
        L_0x005e:
            r3 = r2
        L_0x005f:
            if (r3 == 0) goto L_0x0064
            r3.close()     // Catch:{ Exception -> 0x0064 }
        L_0x0064:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1276bh.m4711a():java.lang.String");
    }

    /* renamed from: a */
    public static String m4715a(Context context, int i) {
        String l = m4739l(context);
        return TextUtils.isEmpty(l) ? "" : C1262ay.C1264b.m4672c(i, l.getBytes());
    }

    /* renamed from: l */
    public static String m4739l(Context context) {
        String name;
        try {
            BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
            return (defaultAdapter == null || (name = defaultAdapter.getName()) == null) ? "" : name;
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: f */
    public static String m4731f(int i, Context context) {
        String m = m4740m(context);
        return TextUtils.isEmpty(m) ? "" : C1262ay.C1264b.m4672c(i, m.getBytes());
    }

    /* renamed from: m */
    public static String m4740m(Context context) {
        BluetoothAdapter defaultAdapter;
        String address;
        String str = Build.BRAND;
        if ("4.1.1".equals(Build.VERSION.RELEASE) && "TCT".equals(str)) {
            return "";
        }
        try {
            if (!C1267ba.m4689e(context, "android.permission.BLUETOOTH") || (defaultAdapter = BluetoothAdapter.getDefaultAdapter()) == null || (address = defaultAdapter.getAddress()) == null) {
                return "";
            }
            return address;
        } catch (Exception unused) {
        }
    }

    /* renamed from: n */
    public static String m4741n(Context context) {
        String o = m4742o(context);
        if (TextUtils.isEmpty(o)) {
            return "";
        }
        return C1262ay.C1263a.m4665a(o.getBytes());
    }

    /* renamed from: g */
    public static String m4732g(int i, Context context) {
        String o = m4742o(context);
        if (TextUtils.isEmpty(o)) {
            return "";
        }
        return C1262ay.C1264b.m4673d(i, o.getBytes());
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0056  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00b6 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00b0 A[ADDED_TO_REGION, EDGE_INSN: B:58:0x00b0->B:46:0x00b0 ?: BREAK  , SYNTHETIC] */
    /* renamed from: o */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String m4742o(android.content.Context r15) {
        /*
            java.lang.String r0 = ""
            if (r15 != 0) goto L_0x0005
            return r0
        L_0x0005:
            java.lang.String r1 = "android.permission.ACCESS_WIFI_STATE"
            boolean r1 = com.baidu.mobstat.C1267ba.m4689e(r15, r1)
            if (r1 != 0) goto L_0x000e
            return r0
        L_0x000e:
            r1 = 0
            java.lang.String r2 = "android.permission.ACCESS_FINE_LOCATION"
            boolean r2 = com.baidu.mobstat.C1267ba.m4689e(r15, r2)     // Catch:{ Exception -> 0x0026 }
            if (r2 == 0) goto L_0x0026
            java.lang.String r2 = "location"
            java.lang.Object r2 = r15.getSystemService(r2)     // Catch:{ Exception -> 0x0026 }
            android.location.LocationManager r2 = (android.location.LocationManager) r2     // Catch:{ Exception -> 0x0026 }
            java.lang.String r3 = "gps"
            boolean r2 = r2.isProviderEnabled(r3)     // Catch:{ Exception -> 0x0026 }
            goto L_0x0027
        L_0x0026:
            r2 = 0
        L_0x0027:
            r3 = 0
            java.lang.String r4 = "wifi"
            java.lang.Object r4 = r15.getSystemService(r4)     // Catch:{ all -> 0x0039 }
            android.net.wifi.WifiManager r4 = (android.net.wifi.WifiManager) r4     // Catch:{ all -> 0x0039 }
            android.net.wifi.WifiInfo r5 = r4.getConnectionInfo()     // Catch:{ all -> 0x0039 }
            java.util.List r4 = r4.getScanResults()     // Catch:{ all -> 0x003a }
            goto L_0x003b
        L_0x0039:
            r5 = r3
        L_0x003a:
            r4 = r3
        L_0x003b:
            if (r4 == 0) goto L_0x004b
            int r6 = r4.size()
            if (r6 == 0) goto L_0x004b
            com.baidu.mobstat.bh$1 r6 = new com.baidu.mobstat.bh$1
            r6.<init>()
            java.util.Collections.sort(r4, r6)
        L_0x004b:
            org.json.JSONArray r6 = new org.json.JSONArray
            r6.<init>()
            r7 = 0
        L_0x0051:
            r8 = 1
            java.lang.String r9 = "|"
            if (r4 == 0) goto L_0x00b0
            int r10 = r4.size()
            if (r7 >= r10) goto L_0x00b0
            r10 = 30
            if (r7 >= r10) goto L_0x00b0
            java.lang.Object r11 = r4.get(r7)     // Catch:{ Exception -> 0x00ad }
            android.net.wifi.ScanResult r11 = (android.net.wifi.ScanResult) r11     // Catch:{ Exception -> 0x00ad }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ad }
            r12.<init>()     // Catch:{ Exception -> 0x00ad }
            java.lang.String r13 = r11.BSSID     // Catch:{ Exception -> 0x00ad }
            r12.append(r13)     // Catch:{ Exception -> 0x00ad }
            r12.append(r9)     // Catch:{ Exception -> 0x00ad }
            java.lang.String r13 = r11.SSID     // Catch:{ Exception -> 0x00ad }
            java.lang.String r14 = "\\|"
            java.lang.String r13 = r13.replaceAll(r14, r0)     // Catch:{ Exception -> 0x00ad }
            int r14 = r13.length()     // Catch:{ Exception -> 0x00ad }
            if (r14 <= r10) goto L_0x0085
            java.lang.String r13 = r13.substring(r1, r10)     // Catch:{ Exception -> 0x00ad }
        L_0x0085:
            r12.append(r13)     // Catch:{ Exception -> 0x00ad }
            r12.append(r9)     // Catch:{ Exception -> 0x00ad }
            int r10 = r11.level     // Catch:{ Exception -> 0x00ad }
            r12.append(r10)     // Catch:{ Exception -> 0x00ad }
            r12.append(r9)     // Catch:{ Exception -> 0x00ad }
            if (r5 == 0) goto L_0x00a2
            java.lang.String r9 = r11.BSSID     // Catch:{ Exception -> 0x00ad }
            java.lang.String r10 = r5.getBSSID()     // Catch:{ Exception -> 0x00ad }
            boolean r9 = r9.equals(r10)     // Catch:{ Exception -> 0x00ad }
            if (r9 == 0) goto L_0x00a2
            goto L_0x00a3
        L_0x00a2:
            r8 = 0
        L_0x00a3:
            r12.append(r8)     // Catch:{ Exception -> 0x00ad }
            java.lang.String r8 = r12.toString()     // Catch:{ Exception -> 0x00ad }
            r6.put(r8)     // Catch:{ Exception -> 0x00ad }
        L_0x00ad:
            int r7 = r7 + 1
            goto L_0x0051
        L_0x00b0:
            int r4 = r6.length()
            if (r4 != 0) goto L_0x00b7
            return r3
        L_0x00b7:
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ee }
            r4.<init>()     // Catch:{ Exception -> 0x00ee }
            long r10 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x00ee }
            r4.append(r10)     // Catch:{ Exception -> 0x00ee }
            r4.append(r9)     // Catch:{ Exception -> 0x00ee }
            if (r2 == 0) goto L_0x00ce
            r1 = 1
        L_0x00ce:
            r4.append(r1)     // Catch:{ Exception -> 0x00ee }
            r4.append(r9)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r15 = m4736i(r15)     // Catch:{ Exception -> 0x00ee }
            r4.append(r15)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r15 = "ap-list"
            r3.put(r15, r6)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r15 = "meta-data"
            java.lang.String r1 = r4.toString()     // Catch:{ Exception -> 0x00ee }
            r3.put(r15, r1)     // Catch:{ Exception -> 0x00ee }
            java.lang.String r15 = r3.toString()     // Catch:{ Exception -> 0x00ee }
            return r15
        L_0x00ee:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1276bh.m4742o(android.content.Context):java.lang.String");
    }

    /* renamed from: p */
    public static boolean m4743p(Context context) {
        if (context == null) {
            return false;
        }
        try {
            NetworkInfo networkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getNetworkInfo(1);
            if (networkInfo == null || !networkInfo.isAvailable() || !networkInfo.isConnected()) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: q */
    public static String m4744q(Context context) {
        try {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
            if (activeNetworkInfo == null) {
                return "";
            }
            String typeName = activeNetworkInfo.getTypeName();
            return (typeName.equals("WIFI") || activeNetworkInfo.getSubtypeName() == null) ? typeName : activeNetworkInfo.getSubtypeName();
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: r */
    public static String m4745r(Context context) {
        return context != null ? context.getPackageName() : "";
    }

    /* renamed from: h */
    public static String m4734h(int i, Context context) {
        String r = m4745r(context);
        if (TextUtils.isEmpty(r)) {
            return "";
        }
        try {
            return C1262ay.C1264b.m4672c(i, r.getBytes());
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: w */
    private static String m4750w(Context context) {
        String str = f4341a;
        if (str == null) {
            try {
                List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
                int i = 0;
                while (true) {
                    if (runningAppProcesses == null || i >= runningAppProcesses.size()) {
                        break;
                    }
                    ActivityManager.RunningAppProcessInfo runningAppProcessInfo = runningAppProcesses.get(i);
                    if (runningAppProcessInfo != null && runningAppProcessInfo.pid == Process.myPid()) {
                        str = runningAppProcessInfo.processName;
                        break;
                    }
                    i++;
                }
            } catch (Exception unused) {
            }
            if (str == null) {
                str = "";
            }
            f4341a = str;
        }
        return str;
    }

    /* renamed from: b */
    private static String m4721b(Context context, String str) {
        int lastIndexOf;
        int i;
        if (str != null && (lastIndexOf = str.lastIndexOf(58)) > 0 && (i = lastIndexOf + 1) < str.length()) {
            return str.substring(i);
        }
        return null;
    }

    /* renamed from: c */
    private static String m4725c(Context context, String str) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null) {
            return null;
        }
        String str2 = applicationInfo.processName;
        if (str2 == null || str2.equals(str)) {
            return null;
        }
        return str;
    }

    /* renamed from: s */
    public static String m4746s(Context context) {
        String str = f4342b;
        if (str == null) {
            String w = m4750w(context);
            String b = m4721b(context, w);
            if (TextUtils.isEmpty(b)) {
                b = m4725c(context, w);
            }
            str = b == null ? "" : b;
            f4342b = str;
        }
        return str;
    }

    /* renamed from: t */
    public static String m4747t(Context context) {
        ServiceInfo[] serviceInfoArr;
        String str;
        String w = m4750w(context);
        if (w == null) {
            return "";
        }
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4);
        } catch (Exception unused) {
        }
        if (packageInfo == null || (serviceInfoArr = packageInfo.services) == null) {
            return "";
        }
        int length = serviceInfoArr.length;
        int i = 0;
        while (true) {
            if (i >= length) {
                str = "";
                break;
            }
            ServiceInfo serviceInfo = serviceInfoArr[i];
            if (w.equals(serviceInfo.processName)) {
                str = serviceInfo.name;
                break;
            }
            i++;
        }
        if (str == null) {
            return "";
        }
        return str;
    }

    /* renamed from: u */
    public static boolean m4748u(Context context) {
        if (context == null) {
            return false;
        }
        try {
            return context.getPackageManager().hasSystemFeature("android.hardware.type.watch");
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: v */
    public static String m4749v(Context context) {
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
            JSONObject jSONObject = new JSONObject();
            jSONObject.put(Config.MODEL, memoryInfo.availMem);
            jSONObject.put("l", memoryInfo.lowMemory);
            jSONObject.put("t", memoryInfo.threshold);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put(jSONObject);
            StringBuilder sb = new StringBuilder();
            sb.append(System.currentTimeMillis());
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("app_mem", jSONArray);
            jSONObject2.put("meta-data", sb.toString());
            return C1262ay.C1263a.m4665a(jSONObject2.toString().getBytes());
        } catch (Exception unused) {
            return "";
        }
    }

    /* renamed from: b */
    public static String m4718b() {
        String str;
        String str2 = f4343c;
        if (str2 != null) {
            return str2;
        }
        if (!TextUtils.isEmpty(m4717a("ro.miui.ui.version.name"))) {
            str = "miui";
        } else if (!TextUtils.isEmpty(m4717a("ro.build.version.opporom"))) {
            str = "coloros";
        } else if (!TextUtils.isEmpty(m4717a("ro.build.version.emui"))) {
            str = "emui";
        } else if (!TextUtils.isEmpty(m4717a("ro.vivo.os.version"))) {
            str = "funtouch";
        } else {
            str = !TextUtils.isEmpty(m4717a("ro.smartisan.version")) ? "smartisan" : "";
        }
        if (TextUtils.isEmpty(str)) {
            String a = m4717a("ro.build.display.id");
            if (!TextUtils.isEmpty(a) && a.contains("Flyme")) {
                str = "flyme";
            }
        }
        f4343c = str;
        return f4343c;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:10:0x0033 */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r0v6, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0033, code lost:
        if (r5 == null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0035, code lost:
        r5.destroy();
        r0 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0060, code lost:
        if (r5 == null) goto L_0x0063;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0063, code lost:
        return r0;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x004c A[SYNTHETIC, Splitter:B:22:0x004c] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:31:0x005b A[SYNTHETIC, Splitter:B:31:0x005b] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String m4717a(java.lang.String r5) {
        /*
            r0 = 0
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            r2.<init>()     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            java.lang.String r3 = "getprop "
            r2.append(r3)     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            r2.append(r5)     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            java.lang.String r5 = r2.toString()     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            java.lang.Process r5 = r1.exec(r5)     // Catch:{ Exception -> 0x0057, all -> 0x0048 }
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            java.io.InputStream r3 = r5.getInputStream()     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            r3 = 1024(0x400, float:1.435E-42)
            r1.<init>(r2, r3)     // Catch:{ Exception -> 0x0046, all -> 0x0041 }
            java.lang.String r0 = r1.readLine()     // Catch:{ Exception -> 0x003f, all -> 0x0039 }
            r1.close()     // Catch:{ Exception -> 0x0032 }
            goto L_0x0033
        L_0x0032:
        L_0x0033:
            if (r5 == 0) goto L_0x0063
        L_0x0035:
            r5.destroy()
            goto L_0x0063
        L_0x0039:
            r0 = move-exception
            r4 = r1
            r1 = r5
            r5 = r0
            r0 = r4
            goto L_0x004a
        L_0x003f:
            goto L_0x0059
        L_0x0041:
            r1 = move-exception
            r4 = r1
            r1 = r5
            r5 = r4
            goto L_0x004a
        L_0x0046:
            r1 = r0
            goto L_0x0059
        L_0x0048:
            r5 = move-exception
            r1 = r0
        L_0x004a:
            if (r0 == 0) goto L_0x0051
            r0.close()     // Catch:{ Exception -> 0x0050 }
            goto L_0x0051
        L_0x0050:
        L_0x0051:
            if (r1 == 0) goto L_0x0056
            r1.destroy()
        L_0x0056:
            throw r5
        L_0x0057:
            r5 = r0
            r1 = r5
        L_0x0059:
            if (r1 == 0) goto L_0x0060
            r1.close()     // Catch:{ Exception -> 0x005f }
            goto L_0x0060
        L_0x005f:
        L_0x0060:
            if (r5 == 0) goto L_0x0063
            goto L_0x0035
        L_0x0063:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1276bh.m4717a(java.lang.String):java.lang.String");
    }
}
