package com.baidu.location.indoor.mapversion.p022c;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: com.baidu.location.indoor.mapversion.c.b */
class C0901b implements FilenameFilter {

    /* renamed from: a */
    final /* synthetic */ String f2280a;

    /* renamed from: b */
    final /* synthetic */ C0896a f2281b;

    C0901b(C0896a aVar, String str) {
        this.f2281b = aVar;
        this.f2280a = str;
    }

    public boolean accept(File file, String str) {
        StringBuilder sb = new StringBuilder();
        sb.append(this.f2281b.m2753d(this.f2280a));
        sb.append("_");
        return str.startsWith(sb.toString());
    }
}
