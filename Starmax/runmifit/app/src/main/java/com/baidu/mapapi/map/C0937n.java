package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;

/* renamed from: com.baidu.mapapi.map.n */
class C0937n implements View.OnClickListener {

    /* renamed from: a */
    final /* synthetic */ MapView f2854a;

    C0937n(MapView mapView) {
        this.f2854a = mapView;
    }

    public void onClick(View view) {
        float f = this.f2854a.f2573e.mo13086a().f3505a;
        C1064ab E = this.f2854a.f2573e.mo13086a().mo12989E();
        E.f3414a += 1.0f;
        if (E.f3414a <= f) {
            f = E.f3414a;
        }
        E.f3414a = f;
        BaiduMap.mapStatusReason |= 16;
        this.f2854a.f2573e.mo13086a().mo13016a(E, 300);
    }
}
