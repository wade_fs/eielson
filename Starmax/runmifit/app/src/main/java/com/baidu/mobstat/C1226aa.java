package com.baidu.mobstat;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import java.util.List;

/* renamed from: com.baidu.mobstat.aa */
public enum C1226aa {
    SERVICE(1) {
        /* renamed from: a */
        public void mo13880a(Context context) {
            if (C1226aa.m4500d(context) && C1231ab.m4506a(context).mo13887b(context)) {
                try {
                    Intent intent = new Intent(context, Class.forName("com.baidu.bottom.service.BottomService"));
                    intent.putExtra("SDK_PRODUCT_LY", "MS");
                    context.startService(intent);
                } catch (Throwable th) {
                    C1255as.m4626c().mo13944b(th);
                }
            }
        }
    },
    NO_SERVICE(2) {
        /* renamed from: a */
        public void mo13880a(Context context) {
            if (C1226aa.m4500d(context)) {
                Context applicationContext = context.getApplicationContext();
                C1288h a = C1231ab.m4506a(context);
                C1240aj ajVar = new C1240aj();
                ajVar.f4275a = false;
                ajVar.f4276b = "M";
                ajVar.f4277c = false;
                a.mo13884a(applicationContext, ajVar.mo13913a());
            }
        }
    },
    RECEIVER(3) {
        /* renamed from: a */
        public void mo13880a(Context context) {
            if (C1226aa.m4500d(context)) {
                Context applicationContext = context.getApplicationContext();
                C1288h a = C1231ab.m4506a(context);
                C1240aj ajVar = new C1240aj();
                ajVar.f4275a = false;
                ajVar.f4276b = "R";
                ajVar.f4277c = false;
                a.mo13884a(applicationContext, ajVar.mo13913a());
            }
        }
    },
    ERISED(4) {
        /* renamed from: a */
        public void mo13880a(Context context) {
            if (C1226aa.m4500d(context)) {
                Context applicationContext = context.getApplicationContext();
                C1288h a = C1231ab.m4506a(context);
                C1240aj ajVar = new C1240aj();
                ajVar.f4275a = false;
                ajVar.f4276b = "E";
                ajVar.f4277c = false;
                a.mo13884a(applicationContext, ajVar.mo13913a());
            }
        }
    };
    

    /* renamed from: e */
    private int f4239e;

    /* renamed from: a */
    public abstract void mo13880a(Context context);

    private C1226aa(int i) {
        this.f4239e = i;
    }

    public String toString() {
        return String.valueOf(this.f4239e);
    }

    /* renamed from: a */
    public static C1226aa m4497a(int i) {
        C1226aa[] values = values();
        for (C1226aa aaVar : values) {
            if (aaVar.f4239e == i) {
                return aaVar;
            }
        }
        return NO_SERVICE;
    }

    /* renamed from: b */
    public static boolean m4498b(Context context) {
        ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
        if (activityManager != null) {
            try {
                List<ActivityManager.RunningServiceInfo> runningServices = activityManager.getRunningServices(Integer.MAX_VALUE);
                int i = 0;
                while (runningServices != null && i < runningServices.size()) {
                    if ("com.baidu.bottom.service.BottomService".equals(runningServices.get(i).service.getClassName())) {
                        return true;
                    }
                    i++;
                }
            } catch (Exception e) {
                C1255as.m4626c().mo13942a(e);
            }
        }
        return false;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public static boolean m4500d(Context context) {
        return C1267ba.m4689e(context, "android.permission.WRITE_EXTERNAL_STORAGE");
    }
}
