package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.view.PointerIconCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapBaseIndoorMapInfo;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MyLocationConfiguration;
import com.baidu.mapapi.map.Overlay;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapsdkplatform.comapi.map.C1063aa;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;
import com.baidu.mapsdkplatform.comapi.map.C1066ac;
import com.baidu.mapsdkplatform.comapi.map.C1078e;
import com.baidu.mapsdkplatform.comapi.map.C1084j;
import com.baidu.mobstat.Config;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.microedition.khronos.opengles.GL10;

public class BaiduMap {
    public static final int MAP_TYPE_NONE = 3;
    public static final int MAP_TYPE_NORMAL = 1;
    public static final int MAP_TYPE_SATELLITE = 2;

    /* renamed from: e */
    private static final String f2379e = BaiduMap.class.getSimpleName();
    public static int mapStatusReason;
    /* access modifiers changed from: private */

    /* renamed from: A */
    public OnMyLocationClickListener f2380A;
    /* access modifiers changed from: private */

    /* renamed from: B */
    public SnapshotReadyCallback f2381B;
    /* access modifiers changed from: private */

    /* renamed from: C */
    public OnMapDrawFrameCallback f2382C;
    /* access modifiers changed from: private */

    /* renamed from: D */
    public OnBaseIndoorMapListener f2383D;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public OnMapRenderValidDataListener f2384E;
    /* access modifiers changed from: private */

    /* renamed from: F */
    public OnSynchronizationListener f2385F;
    /* access modifiers changed from: private */

    /* renamed from: G */
    public TileOverlay f2386G;
    /* access modifiers changed from: private */

    /* renamed from: H */
    public HeatMap f2387H;
    /* access modifiers changed from: private */

    /* renamed from: I */
    public Lock f2388I = new ReentrantLock();
    /* access modifiers changed from: private */

    /* renamed from: J */
    public Lock f2389J = new ReentrantLock();
    /* access modifiers changed from: private */

    /* renamed from: K */
    public Map<String, InfoWindow> f2390K;

    /* renamed from: L */
    private Map<InfoWindow, Marker> f2391L;
    /* access modifiers changed from: private */

    /* renamed from: M */
    public Marker f2392M;

    /* renamed from: N */
    private MyLocationData f2393N;

    /* renamed from: O */
    private MyLocationConfiguration f2394O;

    /* renamed from: P */
    private boolean f2395P;

    /* renamed from: Q */
    private boolean f2396Q;

    /* renamed from: R */
    private boolean f2397R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public boolean f2398S;

    /* renamed from: T */
    private Point f2399T;

    /* renamed from: a */
    MapView f2400a;

    /* renamed from: b */
    TextureMapView f2401b;

    /* renamed from: c */
    WearMapView f2402c;

    /* renamed from: d */
    C1063aa f2403d;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public Projection f2404f;

    /* renamed from: g */
    private UiSettings f2405g;

    /* renamed from: h */
    private C1084j f2406h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public C1078e f2407i;

    /* renamed from: j */
    private C1066ac f2408j;
    /* access modifiers changed from: private */

    /* renamed from: k */
    public List<Overlay> f2409k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public List<Marker> f2410l;
    /* access modifiers changed from: private */

    /* renamed from: m */
    public List<Marker> f2411m;

    /* renamed from: n */
    private List<InfoWindow> f2412n;

    /* renamed from: o */
    private Overlay.C0921a f2413o;

    /* renamed from: p */
    private InfoWindow.C0920a f2414p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public OnMapStatusChangeListener f2415q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public OnMapTouchListener f2416r;
    /* access modifiers changed from: private */

    /* renamed from: s */
    public OnMapClickListener f2417s;
    /* access modifiers changed from: private */

    /* renamed from: t */
    public OnMapLoadedCallback f2418t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public OnMapRenderCallback f2419u;
    /* access modifiers changed from: private */

    /* renamed from: v */
    public OnMapDoubleClickListener f2420v;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public OnMapLongClickListener f2421w;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public CopyOnWriteArrayList<OnMarkerClickListener> f2422x = new CopyOnWriteArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: y */
    public CopyOnWriteArrayList<OnPolylineClickListener> f2423y = new CopyOnWriteArrayList<>();
    /* access modifiers changed from: private */

    /* renamed from: z */
    public OnMarkerDragListener f2424z;

    public interface OnBaseIndoorMapListener {
        void onBaseIndoorMapMode(boolean z, MapBaseIndoorMapInfo mapBaseIndoorMapInfo);
    }

    public interface OnMapClickListener {
        void onMapClick(LatLng latLng);

        boolean onMapPoiClick(MapPoi mapPoi);
    }

    public interface OnMapDoubleClickListener {
        void onMapDoubleClick(LatLng latLng);
    }

    public interface OnMapDrawFrameCallback {
        void onMapDrawFrame(MapStatus mapStatus);

        @Deprecated
        void onMapDrawFrame(GL10 gl10, MapStatus mapStatus);
    }

    public interface OnMapLoadedCallback {
        void onMapLoaded();
    }

    public interface OnMapLongClickListener {
        void onMapLongClick(LatLng latLng);
    }

    public interface OnMapRenderCallback {
        void onMapRenderFinished();
    }

    public interface OnMapRenderValidDataListener {
        void onMapRenderValidData(boolean z, int i, String str);
    }

    public interface OnMapStatusChangeListener {
        public static final int REASON_API_ANIMATION = 2;
        public static final int REASON_DEVELOPER_ANIMATION = 3;
        public static final int REASON_GESTURE = 1;

        void onMapStatusChange(MapStatus mapStatus);

        void onMapStatusChangeFinish(MapStatus mapStatus);

        void onMapStatusChangeStart(MapStatus mapStatus);

        void onMapStatusChangeStart(MapStatus mapStatus, int i);
    }

    public interface OnMapTouchListener {
        void onTouch(MotionEvent motionEvent);
    }

    public interface OnMarkerClickListener {
        boolean onMarkerClick(Marker marker);
    }

    public interface OnMarkerDragListener {
        void onMarkerDrag(Marker marker);

        void onMarkerDragEnd(Marker marker);

        void onMarkerDragStart(Marker marker);
    }

    public interface OnMyLocationClickListener {
        boolean onMyLocationClick();
    }

    public interface OnPolylineClickListener {
        boolean onPolylineClick(Polyline polyline);
    }

    public interface OnSynchronizationListener {
        void onMapStatusChangeReason(int i);
    }

    public interface SnapshotReadyCallback {
        void onSnapshotReady(Bitmap bitmap);
    }

    BaiduMap(C1066ac acVar) {
        this.f2408j = acVar;
        this.f2407i = this.f2408j.mo12954b();
        this.f2403d = C1063aa.TextureView;
        m2834c();
    }

    BaiduMap(C1084j jVar) {
        this.f2406h = jVar;
        this.f2407i = this.f2406h.mo13086a();
        this.f2403d = C1063aa.GLSurfaceView;
        m2834c();
    }

    /* renamed from: a */
    private Point m2820a(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        int i = 0;
        int i2 = 0;
        for (String str2 : str.replaceAll("^\\{", "").replaceAll("\\}$", "").split(",")) {
            String[] split = str2.replaceAll("\"", "").split(Config.TRACE_TODAY_VISIT_SPLIT);
            if (Config.EVENT_HEAT_X.equals(split[0])) {
                i = Integer.valueOf(split[1]).intValue();
            }
            if ("y".equals(split[0])) {
                i2 = Integer.valueOf(split[1]).intValue();
            }
        }
        return new Point(i, i2);
    }

    /* renamed from: a */
    private C1064ab m2823a(MapStatusUpdate mapStatusUpdate) {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return null;
        }
        C1064ab E = eVar.mo12989E();
        MapStatus a = mapStatusUpdate.mo11191a(this.f2407i, getMapStatus());
        if (a == null) {
            return null;
        }
        return a.mo11180b(E);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m2824a(int i) {
        if (i == 0) {
            return "数据请求成功";
        }
        switch (i) {
            case 1004:
                return "网络连接错误";
            case 1005:
                return "请求发送错误";
            case 1006:
                return "响应数据读取失败";
            case 1007:
                return "返回响应数据过大，数据溢出";
            case 1008:
                return "当前网络类型有问题";
            case PointerIconCompat.TYPE_VERTICAL_TEXT:
                return "数据不一致";
            case PointerIconCompat.TYPE_ALIAS:
                return "请求取消";
            case PointerIconCompat.TYPE_COPY:
                return "网络超时错误";
            case PointerIconCompat.TYPE_NO_DROP:
                return "网络连接超时";
            case PointerIconCompat.TYPE_ALL_SCROLL:
                return "网络发送超时";
            case PointerIconCompat.TYPE_HORIZONTAL_DOUBLE_ARROW:
                return "网络接收超时";
            case PointerIconCompat.TYPE_VERTICAL_DOUBLE_ARROW:
                return "DNS解析错误";
            case PointerIconCompat.TYPE_TOP_RIGHT_DIAGONAL_DOUBLE_ARROW:
                return "DNS解析超时";
            case PointerIconCompat.TYPE_TOP_LEFT_DIAGONAL_DOUBLE_ARROW:
                return "网络写错误";
            case PointerIconCompat.TYPE_ZOOM_IN:
                return "SSL握手错误";
            case PointerIconCompat.TYPE_ZOOM_OUT:
                return "SSL握手超时";
            default:
                return "";
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void m2828a(com.baidu.mapapi.map.InfoWindow r8) {
        /*
            r7 = this;
            if (r8 != 0) goto L_0x0003
            return
        L_0x0003:
            java.util.Map<com.baidu.mapapi.map.InfoWindow, com.baidu.mapapi.map.Marker> r0 = r7.f2391L
            java.util.Set r0 = r0.keySet()
            boolean r1 = r0.isEmpty()
            r2 = 0
            if (r1 != 0) goto L_0x00aa
            boolean r0 = r0.contains(r8)
            if (r0 != 0) goto L_0x0018
            goto L_0x00aa
        L_0x0018:
            android.view.View r0 = r8.f2513b
            r1 = 1
            if (r0 == 0) goto L_0x006e
            boolean r3 = r8.f2521j
            if (r3 == 0) goto L_0x006e
            r0.destroyDrawingCache()
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r3 = new com.baidu.mapapi.map.MapViewLayoutParams$Builder
            r3.<init>()
            com.baidu.mapapi.map.MapViewLayoutParams$ELayoutMode r4 = com.baidu.mapapi.map.MapViewLayoutParams.ELayoutMode.mapMode
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r3 = r3.layoutMode(r4)
            com.baidu.mapapi.model.LatLng r4 = r8.f2514c
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r3 = r3.position(r4)
            int r4 = r8.f2517f
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r3 = r3.yOffset(r4)
            com.baidu.mapapi.map.MapViewLayoutParams r3 = r3.build()
            int[] r4 = com.baidu.mapapi.map.C0929f.f2841b
            com.baidu.mapsdkplatform.comapi.map.aa r5 = r7.f2403d
            int r5 = r5.ordinal()
            r4 = r4[r5]
            if (r4 == r1) goto L_0x005c
            r5 = 2
            if (r4 == r5) goto L_0x004f
            goto L_0x0068
        L_0x004f:
            com.baidu.mapapi.map.MapView r4 = r7.f2400a
            if (r4 == 0) goto L_0x0068
            r4.removeView(r0)
            com.baidu.mapapi.map.MapView r4 = r7.f2400a
            r4.addView(r0, r3)
            goto L_0x0068
        L_0x005c:
            com.baidu.mapapi.map.TextureMapView r4 = r7.f2401b
            if (r4 == 0) goto L_0x0068
            r4.removeView(r0)
            com.baidu.mapapi.map.TextureMapView r4 = r7.f2401b
            r4.addView(r0, r3)
        L_0x0068:
            boolean r0 = r8.f2520i
            if (r0 == 0) goto L_0x006e
            r0 = 0
            goto L_0x006f
        L_0x006e:
            r0 = 1
        L_0x006f:
            com.baidu.mapapi.map.BitmapDescriptor r3 = r7.m2831b(r8)
            java.util.Map<com.baidu.mapapi.map.InfoWindow, com.baidu.mapapi.map.Marker> r4 = r7.f2391L
            java.lang.Object r4 = r4.get(r8)
            com.baidu.mapapi.map.Marker r4 = (com.baidu.mapapi.map.Marker) r4
            if (r4 == 0) goto L_0x00a9
            android.os.Bundle r5 = new android.os.Bundle
            r5.<init>()
            com.baidu.mapapi.map.BitmapDescriptor r6 = r8.f2512a
            if (r6 == 0) goto L_0x0099
            com.baidu.mapsdkplatform.comapi.map.h r6 = com.baidu.mapsdkplatform.comapi.map.C1082h.popup
            r4.type = r6
            r4.f2609b = r3
            android.view.View r3 = r8.f2513b
            java.lang.String r6 = "draw_with_view"
            if (r3 == 0) goto L_0x0096
            r5.putInt(r6, r1)
            goto L_0x0099
        L_0x0096:
            r5.putInt(r6, r2)
        L_0x0099:
            com.baidu.mapapi.model.LatLng r8 = r8.f2514c
            r4.f2608a = r8
            r4.mo10900a(r5)
            com.baidu.mapsdkplatform.comapi.map.e r8 = r7.f2407i
            if (r8 == 0) goto L_0x00a9
            if (r0 == 0) goto L_0x00a9
            r8.mo13038c(r5)
        L_0x00a9:
            return
        L_0x00aa:
            r7.showInfoWindow(r8, r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.BaiduMap.m2828a(com.baidu.mapapi.map.InfoWindow):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x00ff  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x019b  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01af  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01f2  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void m2829a(com.baidu.mapapi.map.MyLocationData r21, com.baidu.mapapi.map.MyLocationConfiguration r22) {
        /*
            r20 = this;
            r1 = r20
            r2 = r21
            r3 = r22
            java.lang.String r0 = "direction_wheel"
            java.lang.String r4 = "iconarrowfocid"
            java.lang.String r5 = "iconarrowfoc"
            java.lang.String r6 = "iconarrownorid"
            java.lang.String r7 = "iconarrownor"
            java.lang.String r8 = "direction"
            java.lang.String r9 = "radius"
            java.lang.String r10 = "pty"
            java.lang.String r11 = "ptx"
            if (r2 == 0) goto L_0x0231
            if (r3 == 0) goto L_0x0231
            boolean r12 = r20.isMyLocationEnabled()
            if (r12 != 0) goto L_0x0024
            goto L_0x0231
        L_0x0024:
            org.json.JSONObject r12 = new org.json.JSONObject
            r12.<init>()
            org.json.JSONArray r13 = new org.json.JSONArray
            r13.<init>()
            org.json.JSONObject r14 = new org.json.JSONObject
            r14.<init>()
            org.json.JSONObject r15 = new org.json.JSONObject
            r15.<init>()
            com.baidu.mapapi.model.LatLng r1 = new com.baidu.mapapi.model.LatLng
            r16 = r4
            r17 = r5
            double r4 = r2.latitude
            r18 = r6
            r19 = r7
            double r6 = r2.longitude
            r1.<init>(r4, r6)
            com.baidu.mapapi.model.inner.GeoPoint r4 = com.baidu.mapapi.model.CoordUtil.ll2mc(r1)
            r5 = 0
            java.lang.String r6 = "type"
            r12.put(r6, r5)     // Catch:{ JSONException -> 0x00f6 }
            double r6 = r4.getLongitudeE6()     // Catch:{ JSONException -> 0x00f6 }
            r14.put(r11, r6)     // Catch:{ JSONException -> 0x00f6 }
            double r6 = r4.getLatitudeE6()     // Catch:{ JSONException -> 0x00f6 }
            r14.put(r10, r6)     // Catch:{ JSONException -> 0x00f6 }
            float r6 = r2.accuracy     // Catch:{ JSONException -> 0x00f6 }
            int r6 = (int) r6     // Catch:{ JSONException -> 0x00f6 }
            int r1 = com.baidu.mapapi.model.CoordUtil.getMCDistanceByOneLatLngAndRadius(r1, r6)     // Catch:{ JSONException -> 0x00f6 }
            float r1 = (float) r1     // Catch:{ JSONException -> 0x00f6 }
            double r6 = (double) r1     // Catch:{ JSONException -> 0x00f6 }
            r14.put(r9, r6)     // Catch:{ JSONException -> 0x00f6 }
            float r1 = r2.direction     // Catch:{ JSONException -> 0x00f6 }
            boolean r1 = r3.enableDirection     // Catch:{ JSONException -> 0x00f6 }
            if (r1 == 0) goto L_0x0088
            float r1 = r2.direction     // Catch:{ JSONException -> 0x00f6 }
            r6 = 1135869952(0x43b40000, float:360.0)
            float r1 = r1 % r6
            r7 = 1127481344(0x43340000, float:180.0)
            int r7 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r7 <= 0) goto L_0x0080
            float r1 = r1 - r6
            goto L_0x008b
        L_0x0080:
            r7 = -1020002304(0xffffffffc3340000, float:-180.0)
            int r7 = (r1 > r7 ? 1 : (r1 == r7 ? 0 : -1))
            if (r7 >= 0) goto L_0x008b
            float r1 = r1 + r6
            goto L_0x008b
        L_0x0088:
            r1 = -998621184(0xffffffffc47a4000, float:-1001.0)
        L_0x008b:
            double r6 = (double) r1     // Catch:{ JSONException -> 0x00f6 }
            r14.put(r8, r6)     // Catch:{ JSONException -> 0x00f6 }
            java.lang.String r1 = "NormalLocArrow"
            r6 = r19
            r14.put(r6, r1)     // Catch:{ JSONException -> 0x00f6 }
            r1 = 28
            r7 = r18
            r14.put(r7, r1)     // Catch:{ JSONException -> 0x00f6 }
            java.lang.String r1 = "FocusLocArrow"
            r5 = r17
            r14.put(r5, r1)     // Catch:{ JSONException -> 0x00f6 }
            r1 = 29
            r2 = r16
            r14.put(r2, r1)     // Catch:{ JSONException -> 0x00f6 }
            java.lang.String r1 = "lineid"
            r16 = r2
            int r2 = r3.accuracyCircleStrokeColor     // Catch:{ JSONException -> 0x00f6 }
            r14.put(r1, r2)     // Catch:{ JSONException -> 0x00f6 }
            java.lang.String r1 = "areaid"
            int r2 = r3.accuracyCircleFillColor     // Catch:{ JSONException -> 0x00f6 }
            r14.put(r1, r2)     // Catch:{ JSONException -> 0x00f6 }
            r13.put(r14)     // Catch:{ JSONException -> 0x00f6 }
            java.lang.String r1 = "data"
            r12.put(r1, r13)     // Catch:{ JSONException -> 0x00f6 }
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode r1 = r3.locationMode     // Catch:{ JSONException -> 0x00f6 }
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode r2 = com.baidu.mapapi.map.MyLocationConfiguration.LocationMode.COMPASS     // Catch:{ JSONException -> 0x00f6 }
            if (r1 != r2) goto L_0x00f4
            double r1 = r4.getLongitudeE6()     // Catch:{ JSONException -> 0x00f6 }
            r15.put(r11, r1)     // Catch:{ JSONException -> 0x00f6 }
            double r1 = r4.getLatitudeE6()     // Catch:{ JSONException -> 0x00f6 }
            r15.put(r10, r1)     // Catch:{ JSONException -> 0x00f6 }
            r1 = 0
            r15.put(r9, r1)     // Catch:{ JSONException -> 0x00f2 }
            r15.put(r8, r1)     // Catch:{ JSONException -> 0x00f2 }
            r15.put(r6, r0)     // Catch:{ JSONException -> 0x00f2 }
            r2 = 54
            r15.put(r7, r2)     // Catch:{ JSONException -> 0x00f2 }
            r15.put(r5, r0)     // Catch:{ JSONException -> 0x00f2 }
            r0 = r16
            r15.put(r0, r2)     // Catch:{ JSONException -> 0x00f2 }
            r13.put(r15)     // Catch:{ JSONException -> 0x00f2 }
            goto L_0x00fb
        L_0x00f2:
            r0 = move-exception
            goto L_0x00f8
        L_0x00f4:
            r1 = 0
            goto L_0x00fb
        L_0x00f6:
            r0 = move-exception
            r1 = 0
        L_0x00f8:
            r0.printStackTrace()
        L_0x00fb:
            com.baidu.mapapi.map.BitmapDescriptor r0 = r3.customMarker
            if (r0 != 0) goto L_0x0104
            r0 = 0
            r1 = r20
            goto L_0x0197
        L_0x0104:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            com.baidu.mapapi.map.BitmapDescriptor r2 = r3.customMarker
            r0.add(r2)
            android.os.Bundle r2 = new android.os.Bundle
            r2.<init>()
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x011c:
            boolean r5 = r0.hasNext()
            if (r5 == 0) goto L_0x0172
            java.lang.Object r5 = r0.next()
            com.baidu.mapapi.map.BitmapDescriptor r5 = (com.baidu.mapapi.map.BitmapDescriptor) r5
            com.baidu.mapapi.model.ParcelItem r6 = new com.baidu.mapapi.model.ParcelItem
            r6.<init>()
            android.os.Bundle r7 = new android.os.Bundle
            r7.<init>()
            android.graphics.Bitmap r8 = r5.f2437a
            int r9 = r8.getWidth()
            int r10 = r8.getHeight()
            int r9 = r9 * r10
            int r9 = r9 * 4
            java.nio.ByteBuffer r9 = java.nio.ByteBuffer.allocate(r9)
            r8.copyPixelsToBuffer(r9)
            byte[] r9 = r9.array()
            java.lang.String r10 = "imgdata"
            r7.putByteArray(r10, r9)
            int r5 = r5.hashCode()
            java.lang.String r9 = "imgindex"
            r7.putInt(r9, r5)
            int r5 = r8.getHeight()
            java.lang.String r9 = "imgH"
            r7.putInt(r9, r5)
            int r5 = r8.getWidth()
            java.lang.String r8 = "imgW"
            r7.putInt(r8, r5)
            r6.setBundle(r7)
            r4.add(r6)
            goto L_0x011c
        L_0x0172:
            int r0 = r4.size()
            if (r0 <= 0) goto L_0x0194
            int r0 = r4.size()
            com.baidu.mapapi.model.ParcelItem[] r0 = new com.baidu.mapapi.model.ParcelItem[r0]
        L_0x017e:
            int r5 = r4.size()
            if (r1 >= r5) goto L_0x018f
            java.lang.Object r5 = r4.get(r1)
            com.baidu.mapapi.model.ParcelItem r5 = (com.baidu.mapapi.model.ParcelItem) r5
            r0[r1] = r5
            int r1 = r1 + 1
            goto L_0x017e
        L_0x018f:
            java.lang.String r1 = "icondata"
            r2.putParcelableArray(r1, r0)
        L_0x0194:
            r1 = r20
            r0 = r2
        L_0x0197:
            com.baidu.mapsdkplatform.comapi.map.e r2 = r1.f2407i
            if (r2 == 0) goto L_0x01a2
            java.lang.String r4 = r12.toString()
            r2.mo13021a(r4, r0)
        L_0x01a2:
            int[] r0 = com.baidu.mapapi.map.C0929f.f2840a
            com.baidu.mapapi.map.MyLocationConfiguration$LocationMode r2 = r3.locationMode
            int r2 = r2.ordinal()
            r0 = r0[r2]
            r2 = 1
            if (r0 == r2) goto L_0x01f2
            r2 = 2
            if (r0 == r2) goto L_0x01b5
            r2 = 3
            goto L_0x0231
        L_0x01b5:
            com.baidu.mapapi.map.MapStatus$Builder r0 = new com.baidu.mapapi.map.MapStatus$Builder
            r0.<init>()
            com.baidu.mapapi.model.LatLng r2 = new com.baidu.mapapi.model.LatLng
            r3 = r21
            double r4 = r3.latitude
            double r6 = r3.longitude
            r2.<init>(r4, r6)
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.target(r2)
            com.baidu.mapapi.map.MapStatus r2 = r20.getMapStatus()
            float r2 = r2.zoom
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.zoom(r2)
            com.baidu.mapapi.map.MapStatus r2 = r20.getMapStatus()
            float r2 = r2.rotate
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.rotate(r2)
            com.baidu.mapapi.map.MapStatus r2 = r20.getMapStatus()
            float r2 = r2.overlook
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.overlook(r2)
            com.baidu.mapapi.map.MapStatus r2 = r20.getMapStatus()
            android.graphics.Point r2 = r2.targetScreen
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.targetScreen(r2)
            goto L_0x0226
        L_0x01f2:
            r3 = r21
            com.baidu.mapapi.map.MapStatus$Builder r0 = new com.baidu.mapapi.map.MapStatus$Builder
            r0.<init>()
            float r2 = r3.direction
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.rotate(r2)
            r2 = -1036779520(0xffffffffc2340000, float:-45.0)
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.overlook(r2)
            com.baidu.mapapi.model.LatLng r2 = new com.baidu.mapapi.model.LatLng
            double r4 = r3.latitude
            double r6 = r3.longitude
            r2.<init>(r4, r6)
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.target(r2)
            com.baidu.mapapi.map.MapStatus r2 = r20.getMapStatus()
            android.graphics.Point r2 = r2.targetScreen
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.targetScreen(r2)
            com.baidu.mapapi.map.MapStatus r2 = r20.getMapStatus()
            float r2 = r2.zoom
            com.baidu.mapapi.map.MapStatus$Builder r0 = r0.zoom(r2)
        L_0x0226:
            com.baidu.mapapi.map.MapStatus r0 = r0.build()
            com.baidu.mapapi.map.MapStatusUpdate r0 = com.baidu.mapapi.map.MapStatusUpdateFactory.newMapStatus(r0)
            r1.animateMapStatus(r0)
        L_0x0231:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.BaiduMap.m2829a(com.baidu.mapapi.map.MyLocationData, com.baidu.mapapi.map.MyLocationConfiguration):void");
    }

    /* renamed from: b */
    private BitmapDescriptor m2831b(InfoWindow infoWindow) {
        BitmapDescriptor bitmapDescriptor;
        if (infoWindow.f2513b == null || !infoWindow.f2521j) {
            return infoWindow.f2512a;
        }
        if (infoWindow.f2518g) {
            if (infoWindow.f2519h <= 0) {
                infoWindow.f2519h = SysOSUtil.getDensityDpi();
            }
            bitmapDescriptor = BitmapDescriptorFactory.fromViewWithDpi(infoWindow.f2513b, infoWindow.f2519h);
        } else {
            bitmapDescriptor = BitmapDescriptorFactory.fromView(infoWindow.f2513b);
        }
        infoWindow.f2512a = bitmapDescriptor;
        return bitmapDescriptor;
    }

    /* renamed from: c */
    private void m2834c() {
        this.f2409k = new CopyOnWriteArrayList();
        this.f2410l = new CopyOnWriteArrayList();
        this.f2411m = new CopyOnWriteArrayList();
        this.f2390K = new ConcurrentHashMap();
        this.f2391L = new ConcurrentHashMap();
        this.f2412n = new CopyOnWriteArrayList();
        this.f2399T = new Point((int) (SysOSUtil.getDensity() * 40.0f), (int) (SysOSUtil.getDensity() * 40.0f));
        this.f2405g = new UiSettings(this.f2407i);
        this.f2413o = new C0923a(this);
        this.f2414p = new C0925b(this);
        this.f2407i.mo13018a(new C0926c(this));
        this.f2407i.mo13019a(new C0927d(this));
        this.f2407i.mo13017a(new C0928e(this));
        this.f2395P = this.f2407i.mo12987C();
        this.f2396Q = this.f2407i.mo12988D();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10924a() {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13075t();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10925a(HeatMap heatMap) {
        this.f2388I.lock();
        try {
            if (!(this.f2387H == null || this.f2407i == null || heatMap != this.f2387H)) {
                this.f2387H.mo11130b();
                this.f2387H.mo11131c();
                this.f2387H.f2495a = null;
                this.f2407i.mo13065o();
                this.f2387H = null;
                this.f2407i.mo13066o(false);
            }
        } finally {
            this.f2388I.unlock();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public void mo10926a(TileOverlay tileOverlay) {
        this.f2389J.lock();
        if (tileOverlay != null) {
            try {
                if (this.f2386G == tileOverlay) {
                    tileOverlay.mo11485b();
                    tileOverlay.f2778a = null;
                    if (this.f2407i != null) {
                        this.f2407i.mo13049f(false);
                    }
                }
            } catch (Throwable th) {
                this.f2386G = null;
                this.f2389J.unlock();
                throw th;
            }
        }
        this.f2386G = null;
        this.f2389J.unlock();
    }

    public void addHeatMap(HeatMap heatMap) {
        if (heatMap != null) {
            this.f2388I.lock();
            try {
                if (heatMap != this.f2387H) {
                    if (this.f2387H != null) {
                        this.f2387H.mo11130b();
                        this.f2387H.mo11131c();
                        this.f2387H.f2495a = null;
                        this.f2407i.mo13065o();
                    }
                    this.f2387H = heatMap;
                    this.f2387H.f2495a = this;
                    this.f2407i.mo13066o(true);
                    this.f2388I.unlock();
                }
            } finally {
                this.f2388I.unlock();
            }
        }
    }

    public final Overlay addOverlay(OverlayOptions overlayOptions) {
        if (overlayOptions == null) {
            return null;
        }
        Overlay a = overlayOptions.mo10909a();
        a.listener = this.f2413o;
        if (a instanceof Marker) {
            Marker marker = (Marker) a;
            marker.f2630w = this.f2414p;
            if (!(marker.f2622o == null || marker.f2622o.size() == 0)) {
                this.f2410l.add(marker);
                C1078e eVar = this.f2407i;
                if (eVar != null) {
                    eVar.mo13035b(true);
                }
            }
            this.f2411m.add(marker);
            if (marker.f2629v != null) {
                showInfoWindow(marker.f2629v, false);
            }
        }
        Bundle bundle = new Bundle();
        a.mo10900a(bundle);
        C1078e eVar2 = this.f2407i;
        if (eVar2 != null) {
            eVar2.mo13033b(bundle);
        }
        this.f2409k.add(a);
        return a;
    }

    public final List<Overlay> addOverlays(List<OverlayOptions> list) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        Bundle[] bundleArr = new Bundle[list.size()];
        int i = 0;
        for (OverlayOptions overlayOptions : list) {
            if (overlayOptions != null) {
                Bundle bundle = new Bundle();
                Overlay a = overlayOptions.mo10909a();
                a.listener = this.f2413o;
                if (a instanceof Marker) {
                    Marker marker = (Marker) a;
                    marker.f2630w = this.f2414p;
                    if (!(marker.f2622o == null || marker.f2622o.size() == 0)) {
                        this.f2410l.add(marker);
                        C1078e eVar = this.f2407i;
                        if (eVar != null) {
                            eVar.mo13035b(true);
                        }
                    }
                    this.f2411m.add(marker);
                }
                this.f2409k.add(a);
                arrayList.add(a);
                a.mo10900a(bundle);
                bundleArr[i] = bundle;
                i++;
            }
        }
        int length = bundleArr.length / 400;
        for (int i2 = 0; i2 < length + 1; i2++) {
            ArrayList arrayList2 = new ArrayList();
            for (int i3 = 0; i3 < 400; i3++) {
                int i4 = (i2 * 400) + i3;
                if (i4 >= bundleArr.length) {
                    break;
                }
                if (bundleArr[i4] != null) {
                    arrayList2.add(bundleArr[i4]);
                }
            }
            C1078e eVar2 = this.f2407i;
            if (eVar2 != null) {
                eVar2.mo13022a(arrayList2);
            }
        }
        return arrayList;
    }

    public TileOverlay addTileLayer(TileOverlayOptions tileOverlayOptions) {
        if (tileOverlayOptions == null) {
            return null;
        }
        TileOverlay tileOverlay = this.f2386G;
        if (tileOverlay != null) {
            tileOverlay.mo11485b();
            this.f2386G.f2778a = null;
        }
        C1078e eVar = this.f2407i;
        if (eVar == null || !eVar.mo13027a(tileOverlayOptions.mo11488a())) {
            return null;
        }
        TileOverlay a = tileOverlayOptions.mo11489a(this);
        this.f2386G = a;
        return a;
    }

    public final void animateMapStatus(MapStatusUpdate mapStatusUpdate) {
        animateMapStatus(mapStatusUpdate, 300);
    }

    public final void animateMapStatus(MapStatusUpdate mapStatusUpdate, int i) {
        if (mapStatusUpdate != null && i > 0) {
            C1064ab a = m2823a(mapStatusUpdate);
            C1078e eVar = this.f2407i;
            if (eVar != null) {
                mapStatusReason |= 256;
                if (!this.f2398S) {
                    eVar.mo13015a(a);
                } else {
                    eVar.mo13016a(a, i);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public boolean mo10933b() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return false;
        }
        return eVar.mo13047e();
    }

    public void changeLocationLayerOrder(boolean z) {
        this.f2407i.mo13044d(z);
    }

    public final void clear() {
        this.f2409k.clear();
        this.f2410l.clear();
        this.f2411m.clear();
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13035b(false);
            this.f2407i.mo13063n();
        }
        hideInfoWindow();
    }

    public List<InfoWindow> getAllInfoWindows() {
        return this.f2412n;
    }

    public final Point getCompassPosition() {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            return m2820a(eVar.mo13052h());
        }
        return null;
    }

    public MapBaseIndoorMapInfo getFocusedBaseIndoorMapInfo() {
        return this.f2407i.mo13067p();
    }

    public final MyLocationConfiguration getLocationConfigeration() {
        return getLocationConfiguration();
    }

    public final MyLocationConfiguration getLocationConfiguration() {
        return this.f2394O;
    }

    public final MyLocationData getLocationData() {
        return this.f2393N;
    }

    public final MapStatus getMapStatus() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return null;
        }
        return MapStatus.m2907a(eVar.mo12989E());
    }

    public final LatLngBounds getMapStatusLimit() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return null;
        }
        return eVar.mo12990F();
    }

    public final int getMapType() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return 1;
        }
        if (!eVar.mo13061l()) {
            return 3;
        }
        return this.f2407i.mo13059k() ? 2 : 1;
    }

    public List<Marker> getMarkersInBounds(LatLngBounds latLngBounds) {
        if (getMapStatus() == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (this.f2411m.size() == 0) {
            return null;
        }
        for (Marker marker : this.f2411m) {
            if (latLngBounds.contains(marker.getPosition())) {
                arrayList.add(marker);
            }
        }
        return arrayList;
    }

    public final float getMaxZoomLevel() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return 0.0f;
        }
        return eVar.f3505a;
    }

    public final float getMinZoomLevel() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return 0.0f;
        }
        return eVar.f3527b;
    }

    public final Projection getProjection() {
        return this.f2404f;
    }

    public float[] getProjectionMatrix() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return null;
        }
        return eVar.mo12998N();
    }

    public final UiSettings getUiSettings() {
        return this.f2405g;
    }

    public float[] getViewMatrix() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return null;
        }
        return eVar.mo12999O();
    }

    public float getZoomToBound(int i, int i2, int i3, int i4, int i5, int i6) {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return 0.0f;
        }
        return eVar.mo13002a(i, i2, i3, i4, i5, i6);
    }

    public C1084j getmGLMapView() {
        return this.f2406h;
    }

    public void hideInfoWindow() {
        View view;
        MapView mapView;
        Collection<InfoWindow> values = this.f2390K.values();
        if (!values.isEmpty()) {
            for (InfoWindow infoWindow : values) {
                if (!(infoWindow == null || (view = infoWindow.f2513b) == null)) {
                    int i = C0929f.f2841b[this.f2403d.ordinal()];
                    if (i == 1) {
                        TextureMapView textureMapView = this.f2401b;
                        if (textureMapView != null) {
                            textureMapView.removeView(view);
                        }
                    } else if (i == 2 && (mapView = this.f2400a) != null) {
                        mapView.removeView(view);
                    }
                }
            }
        }
        for (Overlay overlay : this.f2409k) {
            Set<String> keySet = this.f2390K.keySet();
            String str = overlay.f2664y;
            if ((overlay instanceof Marker) && !keySet.isEmpty() && keySet.contains(str)) {
                overlay.remove();
            }
        }
        this.f2390K.clear();
        this.f2391L.clear();
        this.f2412n.clear();
    }

    public void hideInfoWindow(InfoWindow infoWindow) {
        MapView mapView;
        Set<InfoWindow> keySet = this.f2391L.keySet();
        if (infoWindow != null && !keySet.isEmpty() && keySet.contains(infoWindow)) {
            View view = infoWindow.f2513b;
            if (view != null) {
                int i = C0929f.f2841b[this.f2403d.ordinal()];
                if (i == 1) {
                    TextureMapView textureMapView = this.f2401b;
                    if (textureMapView != null) {
                        textureMapView.removeView(view);
                    }
                } else if (i == 2 && (mapView = this.f2400a) != null) {
                    mapView.removeView(view);
                }
            }
            Marker marker = this.f2391L.get(infoWindow);
            if (marker != null) {
                marker.remove();
                this.f2390K.remove(marker.f2664y);
            }
            this.f2391L.remove(infoWindow);
            this.f2412n.remove(infoWindow);
        }
    }

    public void hideSDKLayer() {
        this.f2407i.mo13037c();
    }

    public final boolean isBaiduHeatMapEnabled() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return false;
        }
        return eVar.mo13055i();
    }

    public boolean isBaseIndoorMapMode() {
        return this.f2407i.mo13070q();
    }

    public final boolean isBuildingsEnabled() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return false;
        }
        return eVar.mo13062m();
    }

    public final boolean isMyLocationEnabled() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return false;
        }
        return eVar.mo13074s();
    }

    public final boolean isSupportBaiduHeatMap() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return false;
        }
        return eVar.mo13057j();
    }

    public final boolean isTrafficEnabled() {
        C1078e eVar = this.f2407i;
        if (eVar == null) {
            return false;
        }
        return eVar.mo13051g();
    }

    public final void removeMarkerClickListener(OnMarkerClickListener onMarkerClickListener) {
        if (this.f2422x.contains(onMarkerClickListener)) {
            this.f2422x.remove(onMarkerClickListener);
        }
    }

    public final void setBaiduHeatMapEnabled(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13053h(z);
        }
    }

    public final void setBuildingsEnabled(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13056j(z);
        }
    }

    public void setCompassEnable(boolean z) {
        this.f2407i.mo13046e(z);
    }

    public void setCompassIcon(Bitmap bitmap) {
        if (bitmap != null) {
            this.f2407i.mo13011a(bitmap);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: compass's icon can not be null");
    }

    public void setCompassPosition(Point point) {
        if (this.f2407i.mo13026a(point)) {
            this.f2399T = point;
        }
    }

    public boolean setCustomTrafficColor(String str, String str2, String str3, String str4) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
            if (!TextUtils.isEmpty(str) || !TextUtils.isEmpty(str2) || !TextUtils.isEmpty(str3) || !TextUtils.isEmpty(str4)) {
                return true;
            }
            this.f2407i.mo13010a((long) Color.parseColor("#ffffffff"), (long) Color.parseColor("#ffffffff"), (long) Color.parseColor("#ffffffff"), (long) Color.parseColor("#ffffffff"), false);
            return true;
        } else if (!str.matches("^#[0-9a-fA-F]{8}$") || !str2.matches("^#[0-9a-fA-F]{8}$") || !str3.matches("^#[0-9a-fA-F]{8}$") || !str4.matches("^#[0-9a-fA-F]{8}$")) {
            Log.e(f2379e, "the string of the input customTrafficColor is error");
            return false;
        } else {
            this.f2407i.mo13010a((long) Color.parseColor(str), (long) Color.parseColor(str2), (long) Color.parseColor(str3), (long) Color.parseColor(str4), true);
            return true;
        }
    }

    public final void setIndoorEnable(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            this.f2397R = z;
            eVar.mo13060l(z);
        }
        OnBaseIndoorMapListener onBaseIndoorMapListener = this.f2383D;
        if (onBaseIndoorMapListener != null && !z) {
            onBaseIndoorMapListener.onBaseIndoorMapMode(false, null);
        }
    }

    public final void setMapStatus(MapStatusUpdate mapStatusUpdate) {
        if (mapStatusUpdate != null) {
            C1064ab a = m2823a(mapStatusUpdate);
            C1078e eVar = this.f2407i;
            if (eVar != null) {
                eVar.mo13015a(a);
                OnMapStatusChangeListener onMapStatusChangeListener = this.f2415q;
                if (onMapStatusChangeListener != null) {
                    onMapStatusChangeListener.onMapStatusChange(getMapStatus());
                }
            }
        }
    }

    public final void setMapStatusLimits(LatLngBounds latLngBounds) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13014a(latLngBounds);
            setMapStatus(MapStatusUpdateFactory.newLatLngBounds(latLngBounds));
        }
    }

    public final void setMapType(int i) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            if (i == 1) {
                eVar.mo13023a(false);
                this.f2407i.mo13078u(this.f2395P);
                this.f2407i.mo13080v(this.f2396Q);
                this.f2407i.mo13050g(true);
                this.f2407i.mo13060l(this.f2397R);
            } else if (i == 2) {
                eVar.mo13023a(true);
                this.f2407i.mo13078u(this.f2395P);
                this.f2407i.mo13080v(this.f2396Q);
                this.f2407i.mo13050g(true);
            } else if (i == 3) {
                if (eVar.mo12987C()) {
                    this.f2407i.mo13078u(false);
                }
                if (this.f2407i.mo12988D()) {
                    this.f2407i.mo13080v(false);
                }
                this.f2407i.mo13050g(false);
                this.f2407i.mo13060l(false);
            }
            C1084j jVar = this.f2406h;
            if (jVar != null) {
                jVar.mo13088a(i);
            }
        }
    }

    public final void setMaxAndMinZoomLevel(float f, float f2) {
        C1078e eVar;
        if (f <= 21.0f && f2 >= 4.0f && f >= f2 && (eVar = this.f2407i) != null) {
            eVar.mo13007a(f, f2);
        }
    }

    public final void setMyLocationConfigeration(MyLocationConfiguration myLocationConfiguration) {
        setMyLocationConfiguration(myLocationConfiguration);
    }

    public final void setMyLocationConfiguration(MyLocationConfiguration myLocationConfiguration) {
        this.f2394O = myLocationConfiguration;
        m2829a(this.f2393N, this.f2394O);
    }

    public final void setMyLocationData(MyLocationData myLocationData) {
        this.f2393N = myLocationData;
        if (this.f2394O == null) {
            this.f2394O = new MyLocationConfiguration(MyLocationConfiguration.LocationMode.NORMAL, false, null);
        }
        m2829a(myLocationData, this.f2394O);
    }

    public final void setMyLocationEnabled(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13064n(z);
        }
    }

    public final void setOnBaseIndoorMapListener(OnBaseIndoorMapListener onBaseIndoorMapListener) {
        this.f2383D = onBaseIndoorMapListener;
    }

    public final void setOnMapClickListener(OnMapClickListener onMapClickListener) {
        this.f2417s = onMapClickListener;
    }

    public final void setOnMapDoubleClickListener(OnMapDoubleClickListener onMapDoubleClickListener) {
        this.f2420v = onMapDoubleClickListener;
    }

    public final void setOnMapDrawFrameCallback(OnMapDrawFrameCallback onMapDrawFrameCallback) {
        this.f2382C = onMapDrawFrameCallback;
    }

    public void setOnMapLoadedCallback(OnMapLoadedCallback onMapLoadedCallback) {
        this.f2418t = onMapLoadedCallback;
    }

    public final void setOnMapLongClickListener(OnMapLongClickListener onMapLongClickListener) {
        this.f2421w = onMapLongClickListener;
    }

    public void setOnMapRenderCallbadk(OnMapRenderCallback onMapRenderCallback) {
        this.f2419u = onMapRenderCallback;
    }

    public final void setOnMapRenderValidDataListener(OnMapRenderValidDataListener onMapRenderValidDataListener) {
        this.f2384E = onMapRenderValidDataListener;
    }

    public final void setOnMapStatusChangeListener(OnMapStatusChangeListener onMapStatusChangeListener) {
        this.f2415q = onMapStatusChangeListener;
    }

    public final void setOnMapTouchListener(OnMapTouchListener onMapTouchListener) {
        this.f2416r = onMapTouchListener;
    }

    public final void setOnMarkerClickListener(OnMarkerClickListener onMarkerClickListener) {
        if (onMarkerClickListener != null && !this.f2422x.contains(onMarkerClickListener)) {
            this.f2422x.add(onMarkerClickListener);
        }
    }

    public final void setOnMarkerDragListener(OnMarkerDragListener onMarkerDragListener) {
        this.f2424z = onMarkerDragListener;
    }

    public final void setOnMyLocationClickListener(OnMyLocationClickListener onMyLocationClickListener) {
        this.f2380A = onMyLocationClickListener;
    }

    public final void setOnPolylineClickListener(OnPolylineClickListener onPolylineClickListener) {
        if (onPolylineClickListener != null) {
            this.f2423y.add(onPolylineClickListener);
        }
    }

    public final void setOnSynchronizationListener(OnSynchronizationListener onSynchronizationListener) {
        this.f2385F = onSynchronizationListener;
    }

    public void setOverlayUnderPoi(boolean z) {
        this.f2407i.mo13039c(z);
    }

    @Deprecated
    public final void setPadding(int i, int i2, int i3, int i4) {
        C1078e eVar;
        MapView mapView;
        if (i >= 0 && i2 >= 0 && i3 >= 0 && i4 >= 0 && (eVar = this.f2407i) != null) {
            eVar.mo12989E();
            int i5 = C0929f.f2841b[this.f2403d.ordinal()];
            if (i5 == 1) {
                TextureMapView textureMapView = this.f2401b;
                if (textureMapView != null) {
                    float width = ((float) ((textureMapView.getWidth() - i) - i3)) / ((float) this.f2401b.getWidth());
                    float height = ((float) ((this.f2401b.getHeight() - i2) - i4)) / ((float) this.f2401b.getHeight());
                    MapStatusUpdate newMapStatus = MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().targetScreen(new Point(((this.f2401b.getWidth() + i) - i3) / 2, ((this.f2401b.getHeight() + i2) - i4) / 2)).build());
                    this.f2407i.mo13026a(new Point((int) (((float) i) + (((float) this.f2399T.x) * width)), (int) (((float) i2) + (((float) this.f2399T.y) * height))));
                    setMapStatus(newMapStatus);
                    this.f2401b.setPadding(i, i2, i3, i4);
                    this.f2401b.invalidate();
                }
            } else if (i5 == 2 && (mapView = this.f2400a) != null) {
                float width2 = ((float) ((mapView.getWidth() - i) - i3)) / ((float) this.f2400a.getWidth());
                float height2 = ((float) ((this.f2400a.getHeight() - i2) - i4)) / ((float) this.f2400a.getHeight());
                MapStatusUpdate newMapStatus2 = MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().targetScreen(new Point(((this.f2400a.getWidth() + i) - i3) / 2, ((this.f2400a.getHeight() + i2) - i4) / 2)).build());
                this.f2407i.mo13026a(new Point((int) (((float) i) + (((float) this.f2399T.x) * width2)), (int) (((float) i2) + (((float) this.f2399T.y) * height2))));
                setMapStatus(newMapStatus2);
                this.f2400a.setPadding(i, i2, i3, i4);
                this.f2400a.invalidate();
            }
        }
    }

    public void setPixelFormatTransparent(boolean z) {
        C1084j jVar = this.f2406h;
        if (jVar != null) {
            if (z) {
                jVar.mo13096d();
            } else {
                jVar.mo13098e();
            }
        }
    }

    public final void setTrafficEnabled(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13054i(z);
        }
    }

    public final void setViewPadding(int i, int i2, int i3, int i4) {
        MapView mapView;
        if (i >= 0 && i2 >= 0 && i3 >= 0 && i4 >= 0 && this.f2407i != null) {
            int i5 = C0929f.f2841b[this.f2403d.ordinal()];
            if (i5 == 1) {
                TextureMapView textureMapView = this.f2401b;
                if (textureMapView != null) {
                    this.f2407i.mo13026a(new Point((int) (((float) i) + (((float) this.f2399T.x) * (((float) ((textureMapView.getWidth() - i) - i3)) / ((float) this.f2401b.getWidth())))), (int) (((float) i2) + (((float) this.f2399T.y) * (((float) ((this.f2401b.getHeight() - i2) - i4)) / ((float) this.f2401b.getHeight()))))));
                    this.f2401b.setPadding(i, i2, i3, i4);
                    this.f2401b.invalidate();
                }
            } else if (i5 == 2 && (mapView = this.f2400a) != null) {
                this.f2407i.mo13026a(new Point((int) (((float) i) + (((float) this.f2399T.x) * (((float) ((mapView.getWidth() - i) - i3)) / ((float) this.f2400a.getWidth())))), (int) (((float) i2) + (((float) this.f2399T.y) * (((float) ((this.f2400a.getHeight() - i2) - i4)) / ((float) this.f2400a.getHeight()))))));
                this.f2400a.setPadding(i, i2, i3, i4);
                this.f2400a.invalidate();
            }
        }
    }

    public void showInfoWindow(InfoWindow infoWindow) {
        showInfoWindow(infoWindow, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:26:0x00ad  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void showInfoWindow(com.baidu.mapapi.map.InfoWindow r7, boolean r8) {
        /*
            r6 = this;
            java.util.Map<com.baidu.mapapi.map.InfoWindow, com.baidu.mapapi.map.Marker> r0 = r6.f2391L
            java.util.Set r0 = r0.keySet()
            if (r7 == 0) goto L_0x00d9
            boolean r0 = r0.contains(r7)
            if (r0 == 0) goto L_0x0010
            goto L_0x00d9
        L_0x0010:
            if (r8 == 0) goto L_0x0015
            r6.hideInfoWindow()
        L_0x0015:
            com.baidu.mapapi.map.InfoWindow$a r8 = r6.f2414p
            r7.f2516e = r8
            android.view.View r8 = r7.f2513b
            r0 = 0
            r1 = 1
            if (r8 == 0) goto L_0x006a
            boolean r8 = r7.f2521j
            if (r8 == 0) goto L_0x006a
            android.view.View r8 = r7.f2513b
            r8.destroyDrawingCache()
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r2 = new com.baidu.mapapi.map.MapViewLayoutParams$Builder
            r2.<init>()
            com.baidu.mapapi.map.MapViewLayoutParams$ELayoutMode r3 = com.baidu.mapapi.map.MapViewLayoutParams.ELayoutMode.mapMode
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r2 = r2.layoutMode(r3)
            com.baidu.mapapi.model.LatLng r3 = r7.f2514c
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r2 = r2.position(r3)
            int r3 = r7.f2517f
            com.baidu.mapapi.map.MapViewLayoutParams$Builder r2 = r2.yOffset(r3)
            com.baidu.mapapi.map.MapViewLayoutParams r2 = r2.build()
            int[] r3 = com.baidu.mapapi.map.C0929f.f2841b
            com.baidu.mapsdkplatform.comapi.map.aa r4 = r6.f2403d
            int r4 = r4.ordinal()
            r3 = r3[r4]
            if (r3 == r1) goto L_0x005d
            r4 = 2
            if (r3 == r4) goto L_0x0053
            goto L_0x0064
        L_0x0053:
            com.baidu.mapsdkplatform.comapi.map.j r3 = r6.f2406h
            if (r3 == 0) goto L_0x0064
            com.baidu.mapapi.map.MapView r3 = r6.f2400a
            r3.addView(r8, r2)
            goto L_0x0064
        L_0x005d:
            com.baidu.mapapi.map.TextureMapView r3 = r6.f2401b
            if (r3 == 0) goto L_0x0064
            r3.addView(r8, r2)
        L_0x0064:
            boolean r8 = r7.f2520i
            if (r8 == 0) goto L_0x006a
            r8 = 0
            goto L_0x006b
        L_0x006a:
            r8 = 1
        L_0x006b:
            com.baidu.mapapi.map.BitmapDescriptor r2 = r6.m2831b(r7)
            com.baidu.mapapi.map.MarkerOptions r3 = new com.baidu.mapapi.map.MarkerOptions
            r3.<init>()
            com.baidu.mapapi.map.MarkerOptions r3 = r3.perspective(r0)
            com.baidu.mapapi.map.MarkerOptions r2 = r3.icon(r2)
            com.baidu.mapapi.model.LatLng r3 = r7.f2514c
            com.baidu.mapapi.map.MarkerOptions r2 = r2.position(r3)
            r3 = 2147483647(0x7fffffff, float:NaN)
            com.baidu.mapapi.map.MarkerOptions r2 = r2.zIndex(r3)
            int r3 = r7.f2517f
            com.baidu.mapapi.map.MarkerOptions r2 = r2.yOffset(r3)
            com.baidu.mapapi.map.MarkerOptions r2 = r2.infoWindow(r7)
            com.baidu.mapapi.map.Overlay r2 = r2.mo10909a()
            com.baidu.mapapi.map.Overlay$a r3 = r6.f2413o
            r2.listener = r3
            com.baidu.mapsdkplatform.comapi.map.h r3 = com.baidu.mapsdkplatform.comapi.map.C1082h.popup
            r2.type = r3
            android.os.Bundle r3 = new android.os.Bundle
            r3.<init>()
            r2.mo10900a(r3)
            android.view.View r4 = r7.f2513b
            java.lang.String r5 = "draw_with_view"
            if (r4 == 0) goto L_0x00b1
            r3.putInt(r5, r1)
            goto L_0x00b4
        L_0x00b1:
            r3.putInt(r5, r0)
        L_0x00b4:
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f2407i
            if (r0 == 0) goto L_0x00c2
            if (r8 == 0) goto L_0x00c2
            r0.mo13033b(r3)
            java.util.List<com.baidu.mapapi.map.Overlay> r8 = r6.f2409k
            r8.add(r2)
        L_0x00c2:
            com.baidu.mapapi.map.Marker r2 = (com.baidu.mapapi.map.Marker) r2
            com.baidu.mapapi.map.InfoWindow$a r8 = r6.f2414p
            r2.f2630w = r8
            java.util.Map<java.lang.String, com.baidu.mapapi.map.InfoWindow> r8 = r6.f2390K
            java.lang.String r0 = r2.f2664y
            r8.put(r0, r7)
            java.util.Map<com.baidu.mapapi.map.InfoWindow, com.baidu.mapapi.map.Marker> r8 = r6.f2391L
            r8.put(r7, r2)
            java.util.List<com.baidu.mapapi.map.InfoWindow> r8 = r6.f2412n
            r8.add(r7)
        L_0x00d9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.BaiduMap.showInfoWindow(com.baidu.mapapi.map.InfoWindow, boolean):void");
    }

    public void showInfoWindows(List<InfoWindow> list) {
        if (list != null && !list.isEmpty()) {
            for (InfoWindow infoWindow : list) {
                showInfoWindow(infoWindow, false);
            }
        }
    }

    public final void showMapIndoorPoi(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13080v(z);
            this.f2396Q = z;
        }
    }

    public final void showMapPoi(boolean z) {
        C1078e eVar = this.f2407i;
        if (eVar != null) {
            eVar.mo13078u(z);
            this.f2395P = z;
        }
    }

    public void showSDKLayer() {
        this.f2407i.mo13042d();
    }

    public final void snapshot(SnapshotReadyCallback snapshotReadyCallback) {
        C1084j jVar;
        this.f2381B = snapshotReadyCallback;
        int i = C0929f.f2841b[this.f2403d.ordinal()];
        if (i == 1) {
            C1066ac acVar = this.f2408j;
            if (acVar != null) {
                acVar.mo12953a("anything", null);
            }
        } else if (i == 2 && (jVar = this.f2406h) != null) {
            jVar.mo13089a("anything", (Rect) null);
        }
    }

    public final void snapshotScope(Rect rect, SnapshotReadyCallback snapshotReadyCallback) {
        C1084j jVar;
        this.f2381B = snapshotReadyCallback;
        int i = C0929f.f2841b[this.f2403d.ordinal()];
        if (i == 1) {
            C1066ac acVar = this.f2408j;
            if (acVar != null) {
                acVar.mo12953a("anything", rect);
            }
        } else if (i == 2 && (jVar = this.f2406h) != null) {
            jVar.mo13089a("anything", rect);
        }
    }

    public MapBaseIndoorMapInfo.SwitchFloorError switchBaseIndoorMapFloor(String str, String str2) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2)) {
            return MapBaseIndoorMapInfo.SwitchFloorError.FLOOR_INFO_ERROR;
        }
        MapBaseIndoorMapInfo focusedBaseIndoorMapInfo = getFocusedBaseIndoorMapInfo();
        if (focusedBaseIndoorMapInfo == null) {
            return MapBaseIndoorMapInfo.SwitchFloorError.SWITCH_ERROR;
        }
        if (!str2.equals(focusedBaseIndoorMapInfo.f2527a)) {
            return MapBaseIndoorMapInfo.SwitchFloorError.FOCUSED_ID_ERROR;
        }
        ArrayList<String> floors = focusedBaseIndoorMapInfo.getFloors();
        return (floors == null || !floors.contains(str)) ? MapBaseIndoorMapInfo.SwitchFloorError.FLOOR_OVERLFLOW : this.f2407i.mo13029a(str, str2) ? MapBaseIndoorMapInfo.SwitchFloorError.SWITCH_OK : MapBaseIndoorMapInfo.SwitchFloorError.SWITCH_ERROR;
    }
}
