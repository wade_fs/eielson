package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.m */
class C1043m implements Animator.AnimatorListener {

    /* renamed from: a */
    final /* synthetic */ C1040l f3355a;

    C1043m(C1040l lVar) {
        this.f3355a = lVar;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.f3355a.f3349d != null) {
            this.f3355a.f3349d.onAnimationCancel();
        }
    }

    public void onAnimationEnd(Animator animator) {
        if (this.f3355a.f3349d != null) {
            this.f3355a.f3349d.onAnimationEnd();
        }
    }

    public void onAnimationRepeat(Animator animator) {
        if (this.f3355a.f3349d != null) {
            this.f3355a.f3349d.onAnimationRepeat();
        }
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3355a.f3349d != null) {
            this.f3355a.f3349d.onAnimationStart();
        }
    }
}
