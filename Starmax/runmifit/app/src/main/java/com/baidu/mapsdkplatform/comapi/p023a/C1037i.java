package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.i */
class C1037i implements Animator.AnimatorListener {

    /* renamed from: a */
    final /* synthetic */ C1036h f3336a;

    C1037i(C1036h hVar) {
        this.f3336a = hVar;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.f3336a.f3332d != null) {
            this.f3336a.f3332d.onAnimationCancel();
        }
    }

    public void onAnimationEnd(Animator animator) {
        if (this.f3336a.f3332d != null) {
            this.f3336a.f3332d.onAnimationEnd();
        }
    }

    public void onAnimationRepeat(Animator animator) {
        if (this.f3336a.f3332d != null) {
            this.f3336a.f3332d.onAnimationRepeat();
        }
    }

    public void onAnimationStart(Animator animator) {
        if (this.f3336a.f3332d != null) {
            this.f3336a.f3332d.onAnimationStart();
        }
    }
}
