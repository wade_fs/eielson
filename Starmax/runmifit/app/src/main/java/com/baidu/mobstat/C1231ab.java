package com.baidu.mobstat;

/* renamed from: com.baidu.mobstat.ab */
public class C1231ab {

    /* renamed from: a */
    private static C1288h f4240a;

    /* JADX WARNING: Removed duplicated region for block: B:19:0x003a  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized com.baidu.mobstat.C1288h m4506a(android.content.Context r4) {
        /*
            java.lang.Class<com.baidu.mobstat.ab> r0 = com.baidu.mobstat.C1231ab.class
            monitor-enter(r0)
            com.baidu.mobstat.as r1 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0058 }
            java.lang.String r2 = "getBPStretegyController begin"
            r1.mo13941a(r2)     // Catch:{ all -> 0x0058 }
            com.baidu.mobstat.h r1 = com.baidu.mobstat.C1231ab.f4240a     // Catch:{ all -> 0x0058 }
            if (r1 != 0) goto L_0x0038
            java.lang.String r2 = "com.baidu.bottom.remote.BPStretegyController2"
            java.lang.Class r2 = com.baidu.mobstat.C1234ae.m4523a(r4, r2)     // Catch:{ Exception -> 0x0030 }
            if (r2 == 0) goto L_0x0038
            java.lang.Object r2 = r2.newInstance()     // Catch:{ Exception -> 0x0030 }
            com.baidu.mobstat.ad r3 = new com.baidu.mobstat.ad     // Catch:{ Exception -> 0x0030 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0030 }
            com.baidu.mobstat.as r1 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ Exception -> 0x002c }
            java.lang.String r2 = "Get BPStretegyController load remote class v2"
            r1.mo13941a(r2)     // Catch:{ Exception -> 0x002c }
            r1 = r3
            goto L_0x0038
        L_0x002c:
            r1 = move-exception
            r2 = r1
            r1 = r3
            goto L_0x0031
        L_0x0030:
            r2 = move-exception
        L_0x0031:
            com.baidu.mobstat.as r3 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0058 }
            r3.mo13942a(r2)     // Catch:{ all -> 0x0058 }
        L_0x0038:
            if (r1 != 0) goto L_0x0048
            com.baidu.mobstat.ac r1 = new com.baidu.mobstat.ac     // Catch:{ all -> 0x0058 }
            r1.<init>()     // Catch:{ all -> 0x0058 }
            com.baidu.mobstat.as r2 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0058 }
            java.lang.String r3 = "Get BPStretegyController load local class"
            r2.mo13941a(r3)     // Catch:{ all -> 0x0058 }
        L_0x0048:
            com.baidu.mobstat.C1231ab.f4240a = r1     // Catch:{ all -> 0x0058 }
            com.baidu.mobstat.C1234ae.m4525a(r4, r1)     // Catch:{ all -> 0x0058 }
            com.baidu.mobstat.as r4 = com.baidu.mobstat.C1255as.m4626c()     // Catch:{ all -> 0x0058 }
            java.lang.String r2 = "getBPStretegyController end"
            r4.mo13941a(r2)     // Catch:{ all -> 0x0058 }
            monitor-exit(r0)
            return r1
        L_0x0058:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1231ab.m4506a(android.content.Context):com.baidu.mobstat.h");
    }

    /* renamed from: a */
    public static synchronized void m4507a() {
        synchronized (C1231ab.class) {
            f4240a = null;
        }
    }
}
