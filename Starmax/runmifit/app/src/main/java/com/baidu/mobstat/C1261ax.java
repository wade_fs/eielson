package com.baidu.mobstat;

import android.app.Activity;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import com.google.android.gms.common.data.DataBufferSafeParcelable;
import java.lang.ref.WeakReference;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.ax */
public class C1261ax {

    /* renamed from: a */
    private static String f4331a;

    /* renamed from: b */
    private WeakReference<WebView> f4332b;

    /* renamed from: c */
    private WeakReference<Activity> f4333c;

    /* renamed from: d */
    private JSONObject f4334d;

    /* renamed from: e */
    private boolean f4335e;

    @JavascriptInterface
    public void setViewportTreeToNative(String str) {
        if (C1257au.m4633c().mo13945b()) {
            C1257au c = C1257au.m4633c();
            c.mo13941a("setViewportTreeToNative " + str);
        }
        f4331a = str;
    }

    @JavascriptInterface
    public void setEventToNative(String str) {
        Activity activity;
        WeakReference<WebView> weakReference;
        WebView webView;
        if (C1257au.m4633c().mo13945b() && this.f4335e) {
            C1257au c = C1257au.m4633c();
            c.mo13941a("setEventToNative: " + str);
        }
        if (C1258av.m4636c().mo13945b()) {
            C1258av c2 = C1258av.m4636c();
            c2.mo13941a("setEventToNative: " + str);
        }
        WeakReference<Activity> weakReference2 = this.f4333c;
        if (weakReference2 != null && (activity = weakReference2.get()) != null && (weakReference = this.f4332b) != null && (webView = weakReference.get()) != null) {
            m4663a(str, activity, webView);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0034 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0035  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m4663a(java.lang.String r34, android.app.Activity r35, android.webkit.WebView r36) {
        /*
            r33 = this;
            r6 = r33
            r0 = 0
            org.json.JSONObject r1 = new org.json.JSONObject     // Catch:{ Exception -> 0x0028 }
            r2 = r34
            r1.<init>(r2)     // Catch:{ Exception -> 0x0028 }
            java.lang.String r2 = "h3"
            org.json.JSONArray r2 = r1.optJSONArray(r2)     // Catch:{ Exception -> 0x0028 }
            java.lang.String r3 = "p2"
            java.lang.String r3 = r1.optString(r3)     // Catch:{ Exception -> 0x0026 }
            java.lang.String r4 = "l"
            java.lang.String r4 = r1.optString(r4)     // Catch:{ Exception -> 0x0024 }
            java.lang.String r5 = "point"
            org.json.JSONObject r0 = r1.optJSONObject(r5)     // Catch:{ Exception -> 0x002b }
            r1 = 1
            goto L_0x002c
        L_0x0024:
            r4 = r0
            goto L_0x002b
        L_0x0026:
            r3 = r0
            goto L_0x002a
        L_0x0028:
            r2 = r0
            r3 = r2
        L_0x002a:
            r4 = r3
        L_0x002b:
            r1 = 0
        L_0x002c:
            r23 = r0
            r21 = r2
            r22 = r4
            if (r1 != 0) goto L_0x0035
            return
        L_0x0035:
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 == 0) goto L_0x0040
            java.lang.String r0 = "/"
            r24 = r0
            goto L_0x0042
        L_0x0040:
            r24 = r3
        L_0x0042:
            org.json.JSONArray r25 = com.baidu.mobstat.C1259aw.m4647a(r35, r36)
            java.lang.Class r0 = r35.getClass()
            java.lang.String r26 = r0.getName()
            java.lang.String r4 = com.baidu.mobstat.C1259aw.m4646a(r25)
            java.lang.String r5 = com.baidu.mobstat.C1259aw.m4652b(r21)
            java.lang.String r27 = com.baidu.mobstat.C1259aw.m4656d(r36)
            java.util.Map r28 = com.baidu.mobstat.C1259aw.m4659e(r36)
            android.content.Context r29 = r35.getApplicationContext()
            java.lang.String r30 = ""
            long r31 = java.lang.System.currentTimeMillis()
            org.json.JSONObject r1 = r6.f4334d
            java.lang.Class r0 = r35.getClass()
            java.lang.String r2 = r0.getName()
            r0 = r33
            r3 = r24
            boolean r0 = r0.m4664a(r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x009d
            com.baidu.mobstat.BDStatCore r7 = com.baidu.mobstat.BDStatCore.instance()
            r11 = 1
            long r12 = java.lang.System.currentTimeMillis()
            r20 = 1
            r8 = r29
            r9 = r30
            r10 = r22
            r14 = r25
            r15 = r21
            r16 = r26
            r17 = r24
            r18 = r27
            r19 = r28
            r7.onEvent(r8, r9, r10, r11, r12, r14, r15, r16, r17, r18, r19, r20)
            goto L_0x00b4
        L_0x009d:
            com.baidu.mobstat.au r0 = com.baidu.mobstat.C1257au.m4633c()
            boolean r0 = r0.mo13945b()
            if (r0 == 0) goto L_0x00b4
            boolean r0 = r6.f4335e
            if (r0 == 0) goto L_0x00b4
            com.baidu.mobstat.au r0 = com.baidu.mobstat.C1257au.m4633c()
            java.lang.String r1 = "setEventToNative: not circle event, will not take effect"
            r0.mo13941a(r1)
        L_0x00b4:
            com.baidu.mobstat.ap r7 = com.baidu.mobstat.C1250ap.m4592a()
            r11 = 1
            r20 = 1
            java.lang.String r0 = ""
            r8 = r29
            r9 = r30
            r10 = r22
            r12 = r31
            r14 = r26
            r15 = r25
            r16 = r24
            r17 = r21
            r18 = r27
            r19 = r28
            r21 = r23
            r22 = r0
            r7.mo13933a(r8, r9, r10, r11, r12, r14, r15, r16, r17, r18, r19, r20, r21, r22)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mobstat.C1261ax.m4663a(java.lang.String, android.app.Activity, android.webkit.WebView):void");
    }

    /* renamed from: a */
    private boolean m4664a(JSONObject jSONObject, String str, String str2, String str3, String str4) {
        int i = 0;
        if (jSONObject == null || jSONObject.toString().equals(new JSONObject().toString()) || TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || TextUtils.isEmpty(str3) || TextUtils.isEmpty(str4)) {
            return false;
        }
        try {
            if (((JSONObject) jSONObject.get("meta")).getInt("matchAll") != 0) {
                return true;
            }
        } catch (Exception unused) {
        }
        try {
            JSONArray jSONArray = (JSONArray) jSONObject.get("data");
            boolean z = false;
            while (i < jSONArray.length()) {
                try {
                    JSONObject jSONObject2 = (JSONObject) jSONArray.get(i);
                    String optString = jSONObject2.optString("page");
                    String optString2 = jSONObject2.optString("layout");
                    String str5 = (String) jSONObject2.opt("url");
                    String str6 = (String) jSONObject2.opt("webLayout");
                    if (str.equals(optString) && str2.equals(str5) && str3.equals(optString2) && str4.equals(str6)) {
                        z = true;
                    }
                    i++;
                } catch (Exception unused2) {
                    return z;
                }
            }
            return z;
        } catch (Exception unused3) {
            return false;
        }
    }
}
