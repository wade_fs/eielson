package com.baidu.lbsapi.auth;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/* renamed from: com.baidu.lbsapi.auth.i */
class C0713i extends Handler {

    /* renamed from: a */
    final /* synthetic */ LBSAuthManager f1092a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C0713i(LBSAuthManager lBSAuthManager, Looper looper) {
        super(looper);
        this.f1092a = lBSAuthManager;
    }

    public void handleMessage(Message message) {
        C0702a.m1616a("handleMessage !!");
        LBSAuthManagerListener lBSAuthManagerListener = (LBSAuthManagerListener) LBSAuthManager.f1071f.get(message.getData().getString("listenerKey"));
        C0702a.m1616a("handleMessage listener = " + lBSAuthManagerListener);
        if (lBSAuthManagerListener != null) {
            lBSAuthManagerListener.onAuthResult(message.what, message.obj.toString());
        }
    }
}
