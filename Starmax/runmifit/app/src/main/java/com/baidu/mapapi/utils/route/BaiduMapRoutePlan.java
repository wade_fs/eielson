package com.baidu.mapapi.utils.route;

import android.content.Context;
import android.util.Log;
import com.baidu.mapapi.navi.IllegalNaviArgumentException;
import com.baidu.mapapi.utils.C1012b;
import com.baidu.mapapi.utils.OpenClientUtil;
import com.baidu.mapapi.utils.poi.IllegalPoiSearchArgumentException;
import com.baidu.mapapi.utils.route.RouteParaOption;

public class BaiduMapRoutePlan {

    /* renamed from: a */
    private static boolean f3272a = true;

    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c4  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x00d2  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00e7  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m3313a(com.baidu.mapapi.utils.route.RouteParaOption r10, android.content.Context r11, int r12) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "http://api.map.baidu.com/direction?"
            r0.append(r1)
            java.lang.String r1 = "origin="
            r0.append(r1)
            com.baidu.mapapi.model.LatLng r1 = r10.f3273a
            com.baidu.mapapi.CoordType r2 = com.baidu.mapapi.SDKInitializer.getCoordType()
            com.baidu.mapapi.CoordType r3 = com.baidu.mapapi.CoordType.GCJ02
            if (r2 != r3) goto L_0x001f
            if (r1 == 0) goto L_0x001f
            com.baidu.mapapi.model.LatLng r1 = com.baidu.mapsdkplatform.comapi.util.CoordTrans.gcjToBaidu(r1)
        L_0x001f:
            com.baidu.mapapi.model.LatLng r2 = r10.f3273a
            java.lang.String r3 = "name:"
            java.lang.String r4 = "|"
            java.lang.String r5 = "latlng:"
            java.lang.String r6 = ","
            java.lang.String r7 = ""
            if (r2 == 0) goto L_0x0052
            java.lang.String r2 = r10.f3275c
            if (r2 == 0) goto L_0x0052
            java.lang.String r2 = r10.f3275c
            boolean r2 = r2.equals(r7)
            if (r2 != 0) goto L_0x0052
            if (r1 == 0) goto L_0x0052
            r0.append(r5)
            double r8 = r1.latitude
            r0.append(r8)
            r0.append(r6)
            double r1 = r1.longitude
            r0.append(r1)
            r0.append(r4)
            r0.append(r3)
            goto L_0x0066
        L_0x0052:
            com.baidu.mapapi.model.LatLng r2 = r10.f3273a
            if (r2 == 0) goto L_0x0066
            if (r1 == 0) goto L_0x0066
            double r8 = r1.latitude
            r0.append(r8)
            r0.append(r6)
            double r1 = r1.longitude
            r0.append(r1)
            goto L_0x006b
        L_0x0066:
            java.lang.String r1 = r10.f3275c
            r0.append(r1)
        L_0x006b:
            com.baidu.mapapi.model.LatLng r1 = r10.f3274b
            com.baidu.mapapi.CoordType r2 = com.baidu.mapapi.SDKInitializer.getCoordType()
            com.baidu.mapapi.CoordType r8 = com.baidu.mapapi.CoordType.GCJ02
            if (r2 != r8) goto L_0x007b
            if (r1 == 0) goto L_0x007b
            com.baidu.mapapi.model.LatLng r1 = com.baidu.mapsdkplatform.comapi.util.CoordTrans.gcjToBaidu(r1)
        L_0x007b:
            java.lang.String r2 = "&destination="
            r0.append(r2)
            com.baidu.mapapi.model.LatLng r2 = r10.f3274b
            if (r2 == 0) goto L_0x00a9
            java.lang.String r2 = r10.f3276d
            if (r2 == 0) goto L_0x00a9
            java.lang.String r2 = r10.f3276d
            boolean r2 = r2.equals(r7)
            if (r2 != 0) goto L_0x00a9
            if (r1 == 0) goto L_0x00a9
            r0.append(r5)
            double r8 = r1.latitude
            r0.append(r8)
            r0.append(r6)
            double r1 = r1.longitude
            r0.append(r1)
            r0.append(r4)
            r0.append(r3)
            goto L_0x00bd
        L_0x00a9:
            com.baidu.mapapi.model.LatLng r2 = r10.f3274b
            if (r2 == 0) goto L_0x00bd
            if (r1 == 0) goto L_0x00bd
            double r2 = r1.latitude
            r0.append(r2)
            r0.append(r6)
            double r1 = r1.longitude
            r0.append(r1)
            goto L_0x00c2
        L_0x00bd:
            java.lang.String r1 = r10.f3276d
            r0.append(r1)
        L_0x00c2:
            if (r12 == 0) goto L_0x00d2
            r1 = 1
            if (r12 == r1) goto L_0x00cf
            r1 = 2
            if (r12 == r1) goto L_0x00cc
            r12 = r7
            goto L_0x00d4
        L_0x00cc:
            java.lang.String r12 = "walking"
            goto L_0x00d4
        L_0x00cf:
            java.lang.String r12 = "transit"
            goto L_0x00d4
        L_0x00d2:
            java.lang.String r12 = "driving"
        L_0x00d4:
            java.lang.String r1 = "&mode="
            r0.append(r1)
            r0.append(r12)
            java.lang.String r12 = "&region="
            r0.append(r12)
            java.lang.String r12 = r10.getCityName()
            if (r12 == 0) goto L_0x00f7
            java.lang.String r12 = r10.getCityName()
            boolean r12 = r12.equals(r7)
            if (r12 == 0) goto L_0x00f2
            goto L_0x00f7
        L_0x00f2:
            java.lang.String r10 = r10.getCityName()
            goto L_0x00f9
        L_0x00f7:
            java.lang.String r10 = "全国"
        L_0x00f9:
            r0.append(r10)
            java.lang.String r10 = "&output=html"
            r0.append(r10)
            java.lang.String r10 = "&src="
            r0.append(r10)
            java.lang.String r10 = r11.getPackageName()
            r0.append(r10)
            java.lang.String r10 = r0.toString()
            android.net.Uri r10 = android.net.Uri.parse(r10)
            android.content.Intent r12 = new android.content.Intent
            r12.<init>()
            java.lang.String r0 = "android.intent.action.VIEW"
            r12.setAction(r0)
            r0 = 268435456(0x10000000, float:2.5243549E-29)
            r12.setFlags(r0)
            r12.setData(r10)
            r11.startActivity(r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.utils.route.BaiduMapRoutePlan.m3313a(com.baidu.mapapi.utils.route.RouteParaOption, android.content.Context, int):void");
    }

    public static void finish(Context context) {
        if (context != null) {
            C1012b.m3270a(context);
        }
    }

    public static boolean openBaiduMapDrivingRoute(RouteParaOption routeParaOption, Context context) {
        if (routeParaOption == null || context == null) {
            throw new IllegalPoiSearchArgumentException("BDMapSDKException: para or context can not be null.");
        } else if (routeParaOption.f3274b == null && routeParaOption.f3273a == null && routeParaOption.f3276d == null && routeParaOption.f3275c == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: startPoint and endPoint and endName and startName not all null.");
        } else if (routeParaOption.f3275c == null && routeParaOption.f3273a == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: startPoint and startName not all null.");
        } else if (routeParaOption.f3276d == null && routeParaOption.f3274b == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: endPoint and endName not all null.");
        } else if (((routeParaOption.f3275c == null || routeParaOption.f3275c.equals("")) && routeParaOption.f3273a == null) || ((routeParaOption.f3276d == null || routeParaOption.f3276d.equals("")) && routeParaOption.f3274b == null)) {
            Log.e(BaiduMapRoutePlan.class.getName(), "poi startName or endName can not be empty string while pt is null");
            return false;
        } else {
            if (routeParaOption.f3278f == null) {
                routeParaOption.f3278f = RouteParaOption.EBusStrategyType.bus_recommend_way;
            }
            int baiduMapVersion = OpenClientUtil.getBaiduMapVersion(context);
            if (baiduMapVersion == 0) {
                Log.e("baidumapsdk", "BaiduMap app is not installed.");
                if (f3272a) {
                    m3313a(routeParaOption, context, 0);
                    return true;
                }
                throw new IllegalPoiSearchArgumentException("BDMapSDKException: BaiduMap app is not installed.");
            } else if (baiduMapVersion >= 810) {
                return C1012b.m3276a(routeParaOption, context, 0);
            } else {
                Log.e("baidumapsdk", "Baidumap app version is too lowl.Version is greater than 8.1");
                if (f3272a) {
                    m3313a(routeParaOption, context, 0);
                    return true;
                }
                throw new IllegalPoiSearchArgumentException("BDMapSDKException: Baidumap app version is too lowl.Version is greater than 8.1");
            }
        }
    }

    public static boolean openBaiduMapTransitRoute(RouteParaOption routeParaOption, Context context) {
        if (routeParaOption == null || context == null) {
            throw new IllegalPoiSearchArgumentException("BDMapSDKException: para or context can not be null.");
        } else if (routeParaOption.f3274b == null && routeParaOption.f3273a == null && routeParaOption.f3276d == null && routeParaOption.f3275c == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: startPoint and endPoint and endName and startName not all null.");
        } else if (routeParaOption.f3275c == null && routeParaOption.f3273a == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: startPoint and startName not all null.");
        } else if (routeParaOption.f3276d == null && routeParaOption.f3274b == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: endPoint and endName not all null.");
        } else if (((routeParaOption.f3275c == null || routeParaOption.f3275c.equals("")) && routeParaOption.f3273a == null) || ((routeParaOption.f3276d == null || routeParaOption.f3276d.equals("")) && routeParaOption.f3274b == null)) {
            Log.e(BaiduMapRoutePlan.class.getName(), "poi startName or endName can not be empty string while pt is null");
            return false;
        } else {
            if (routeParaOption.f3278f == null) {
                routeParaOption.f3278f = RouteParaOption.EBusStrategyType.bus_recommend_way;
            }
            int baiduMapVersion = OpenClientUtil.getBaiduMapVersion(context);
            if (baiduMapVersion == 0) {
                Log.e("baidumapsdk", "BaiduMap app is not installed.");
                if (f3272a) {
                    m3313a(routeParaOption, context, 1);
                    return true;
                }
                throw new IllegalPoiSearchArgumentException("BDMapSDKException: BaiduMap app is not installed.");
            } else if (baiduMapVersion >= 810) {
                return C1012b.m3276a(routeParaOption, context, 1);
            } else {
                Log.e("baidumapsdk", "Baidumap app version is too lowl.Version is greater than 8.1");
                if (f3272a) {
                    m3313a(routeParaOption, context, 1);
                    return true;
                }
                throw new IllegalPoiSearchArgumentException("BDMapSDKException: Baidumap app version is too lowl.Version is greater than 8.1");
            }
        }
    }

    public static boolean openBaiduMapWalkingRoute(RouteParaOption routeParaOption, Context context) {
        if (routeParaOption == null || context == null) {
            throw new IllegalPoiSearchArgumentException("BDMapSDKException: para or context can not be null.");
        } else if (routeParaOption.f3274b == null && routeParaOption.f3273a == null && routeParaOption.f3276d == null && routeParaOption.f3275c == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: startPoint and endPoint and endName and startName not all null.");
        } else if (routeParaOption.f3275c == null && routeParaOption.f3273a == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: startPoint and startName not all null.");
        } else if (routeParaOption.f3276d == null && routeParaOption.f3274b == null) {
            throw new IllegalNaviArgumentException("BDMapSDKException: endPoint and endName not all null.");
        } else if (((routeParaOption.f3275c == null || routeParaOption.f3275c.equals("")) && routeParaOption.f3273a == null) || ((routeParaOption.f3276d == null || routeParaOption.f3276d.equals("")) && routeParaOption.f3274b == null)) {
            Log.e(BaiduMapRoutePlan.class.getName(), "poi startName or endName can not be empty string while pt is null");
            return false;
        } else {
            if (routeParaOption.f3278f == null) {
                routeParaOption.f3278f = RouteParaOption.EBusStrategyType.bus_recommend_way;
            }
            int baiduMapVersion = OpenClientUtil.getBaiduMapVersion(context);
            if (baiduMapVersion == 0) {
                Log.e("baidumapsdk", "BaiduMap app is not installed.");
                if (f3272a) {
                    m3313a(routeParaOption, context, 2);
                    return true;
                }
                throw new IllegalPoiSearchArgumentException("BDMapSDKException: BaiduMap app is not installed.");
            } else if (baiduMapVersion >= 810) {
                return C1012b.m3276a(routeParaOption, context, 2);
            } else {
                Log.e("baidumapsdk", "Baidumap app version is too lowl.Version is greater than 8.1");
                if (f3272a) {
                    m3313a(routeParaOption, context, 2);
                    return true;
                }
                throw new IllegalPoiSearchArgumentException("BDMapSDKException: Baidumap app version is too lowl.Version is greater than 8.1");
            }
        }
    }

    public static void setSupportWebRoute(boolean z) {
        f3272a = z;
    }
}
