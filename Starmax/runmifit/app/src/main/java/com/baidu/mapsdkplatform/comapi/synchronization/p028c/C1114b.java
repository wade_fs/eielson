package com.baidu.mapsdkplatform.comapi.synchronization.p028c;

import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1112a;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.c.b */
class C1114b extends C1112a.C1113a {

    /* renamed from: a */
    final /* synthetic */ C1118e f3678a;

    /* renamed from: b */
    final /* synthetic */ String f3679b;

    /* renamed from: c */
    final /* synthetic */ C1112a f3680c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    C1114b(C1112a aVar, C1118e eVar, String str) {
        super(null);
        this.f3680c = aVar;
        this.f3678a = eVar;
        this.f3679b = str;
    }

    /* renamed from: a */
    public void mo13194a() {
        C1115c cVar = new C1115c("GET", this.f3678a);
        cVar.mo13198b(this.f3680c.f3675a);
        cVar.mo13196a(this.f3680c.f3676b);
        cVar.mo13197a(this.f3679b);
    }
}
