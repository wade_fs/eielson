package com.baidu.mapapi.map;

import android.graphics.Point;
import android.util.Log;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1078e;

public final class MapStatusUpdate {

    /* renamed from: o */
    private static final String f2550o = MapStatusUpdate.class.getSimpleName();

    /* renamed from: a */
    MapStatus f2551a;

    /* renamed from: b */
    LatLng f2552b;

    /* renamed from: c */
    LatLngBounds f2553c;

    /* renamed from: d */
    int f2554d;

    /* renamed from: e */
    int f2555e;

    /* renamed from: f */
    float f2556f;

    /* renamed from: g */
    int f2557g;

    /* renamed from: h */
    int f2558h;

    /* renamed from: i */
    float f2559i;

    /* renamed from: j */
    Point f2560j;

    /* renamed from: k */
    int f2561k = 0;

    /* renamed from: l */
    int f2562l = 0;

    /* renamed from: m */
    int f2563m = 0;

    /* renamed from: n */
    int f2564n = 0;

    /* renamed from: p */
    private int f2565p;

    private MapStatusUpdate() {
    }

    MapStatusUpdate(int i) {
        this.f2565p = i;
    }

    /* renamed from: a */
    private float m2913a(float f) {
        double pow = Math.pow(2.0d, (double) (18.0f - f));
        double densityDpi = (double) (((float) SysOSUtil.getDensityDpi()) / 310.0f);
        Double.isNaN(densityDpi);
        return (float) (pow / densityDpi);
    }

    /* renamed from: a */
    private float m2914a(LatLngBounds latLngBounds, C1078e eVar, int i, int i2) {
        GeoPoint ll2mc = CoordUtil.ll2mc(latLngBounds.southwest);
        GeoPoint ll2mc2 = CoordUtil.ll2mc(latLngBounds.northeast);
        int latitudeE6 = (int) ll2mc.getLatitudeE6();
        return eVar.mo13002a((int) ll2mc.getLongitudeE6(), (int) ll2mc2.getLatitudeE6(), (int) ll2mc2.getLongitudeE6(), latitudeE6, i, i2);
    }

    /* renamed from: a */
    private MapStatusUpdate m2915a(MapStatus mapStatus) {
        MapStatusUpdate mapStatusUpdate = new MapStatusUpdate();
        synchronized (this) {
            mapStatusUpdate.f2551a = mapStatus;
            mapStatusUpdate.f2553c = this.f2553c;
            mapStatusUpdate.f2561k = this.f2561k;
            mapStatusUpdate.f2562l = this.f2562l;
            mapStatusUpdate.f2563m = this.f2563m;
            mapStatusUpdate.f2564n = this.f2564n;
        }
        return mapStatusUpdate;
    }

    /* renamed from: a */
    private LatLng m2916a(LatLngBounds latLngBounds, C1078e eVar, float f) {
        double d;
        double d2;
        double latitudeE6;
        if (latLngBounds == null || eVar == null) {
            return null;
        }
        GeoPoint ll2mc = CoordUtil.ll2mc(latLngBounds.getCenter());
        int i = this.f2561k;
        double d3 = (double) (((float) i) * f);
        int i2 = this.f2563m;
        double d4 = (double) (((float) i2) * f);
        double d5 = (double) (((float) this.f2562l) * f);
        double d6 = (double) (((float) this.f2564n) * f);
        if (i > i2) {
            double longitudeE6 = ll2mc.getLongitudeE6();
            Double.isNaN(d3);
            Double.isNaN(d4);
            d = longitudeE6 - ((d3 - d4) / 2.0d);
        } else if (i < i2) {
            double longitudeE62 = ll2mc.getLongitudeE6();
            Double.isNaN(d4);
            Double.isNaN(d3);
            d = longitudeE62 + ((d4 - d3) / 2.0d);
        } else {
            d = ll2mc.getLongitudeE6();
        }
        int i3 = this.f2562l;
        int i4 = this.f2564n;
        if (i3 < i4) {
            double latitudeE62 = ll2mc.getLatitudeE6();
            Double.isNaN(d6);
            Double.isNaN(d5);
            latitudeE6 = latitudeE62 - ((d6 - d5) / 2.0d);
            Double.isNaN(d5);
        } else if (i3 > i4) {
            latitudeE6 = ll2mc.getLatitudeE6();
            Double.isNaN(d5);
            Double.isNaN(d6);
            d5 -= d6;
        } else {
            d2 = ll2mc.getLatitudeE6();
            return CoordUtil.mc2ll(new GeoPoint(d2, d));
        }
        d2 = latitudeE6 + (d5 / 2.0d);
        return CoordUtil.mc2ll(new GeoPoint(d2, d));
    }

    /* renamed from: a */
    private boolean m2917a(int i, int i2, int i3, int i4, C1078e eVar) {
        MapStatusUpdate G = eVar.mo12991G();
        return (G != null && i == G.f2561k && i2 == G.f2562l && i3 == G.f2563m && i4 == G.f2564n) ? false : true;
    }

    /* renamed from: a */
    private boolean m2918a(LatLngBounds latLngBounds, C1078e eVar) {
        LatLngBounds latLngBounds2 = latLngBounds;
        MapStatusUpdate G = eVar.mo12991G();
        if (G == null) {
            return true;
        }
        return (latLngBounds2.southwest.latitude == G.f2553c.southwest.latitude && latLngBounds2.southwest.longitude == G.f2553c.southwest.longitude && latLngBounds2.northeast.latitude == G.f2553c.northeast.latitude && latLngBounds2.northeast.longitude == G.f2553c.northeast.longitude) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public MapStatus mo11191a(C1078e eVar, MapStatus mapStatus) {
        C1078e eVar2 = eVar;
        MapStatus mapStatus2 = mapStatus;
        if (eVar2 == null || mapStatus2 == null) {
            return null;
        }
        switch (this.f2565p) {
            case 1:
                return this.f2551a;
            case 2:
                return new MapStatus(mapStatus2.rotate, this.f2552b, mapStatus2.overlook, mapStatus2.zoom, mapStatus2.targetScreen, null);
            case 3:
                LatLngBounds latLngBounds = this.f2553c;
                if (latLngBounds == null) {
                    return null;
                }
                GeoPoint ll2mc = CoordUtil.ll2mc(latLngBounds.southwest);
                GeoPoint ll2mc2 = CoordUtil.ll2mc(this.f2553c.northeast);
                float a = eVar.mo13002a((int) ll2mc.getLongitudeE6(), (int) ll2mc2.getLatitudeE6(), (int) ll2mc2.getLongitudeE6(), (int) ll2mc.getLatitudeE6(), mapStatus2.f2538a.f3423j.right - mapStatus2.f2538a.f3423j.left, mapStatus2.f2538a.f3423j.bottom - mapStatus2.f2538a.f3423j.top);
                return new MapStatus(mapStatus2.rotate, this.f2553c.getCenter(), mapStatus2.overlook, a, mapStatus2.targetScreen, null);
            case 4:
                return new MapStatus(mapStatus2.rotate, this.f2552b, mapStatus2.overlook, this.f2556f, mapStatus2.targetScreen, null);
            case 5:
                GeoPoint b = eVar2.mo13030b((eVar.mo12992H() / 2) + this.f2557g, (eVar.mo12993I() / 2) + this.f2558h);
                return new MapStatus(mapStatus2.rotate, CoordUtil.mc2ll(b), mapStatus2.overlook, mapStatus2.zoom, mapStatus2.targetScreen, b.getLongitudeE6(), b.getLatitudeE6(), null);
            case 6:
                return new MapStatus(mapStatus2.rotate, mapStatus2.target, mapStatus2.overlook, mapStatus2.zoom + this.f2559i, mapStatus2.targetScreen, mapStatus.mo11178a(), mapStatus.mo11179b(), null);
            case 7:
                return new MapStatus(mapStatus2.rotate, CoordUtil.mc2ll(eVar2.mo13030b(this.f2560j.x, this.f2560j.y)), mapStatus2.overlook, mapStatus2.zoom + this.f2559i, this.f2560j, null);
            case 8:
                return new MapStatus(mapStatus2.rotate, mapStatus2.target, mapStatus2.overlook, this.f2556f, mapStatus2.targetScreen, mapStatus.mo11178a(), mapStatus.mo11179b(), null);
            case 9:
                LatLngBounds latLngBounds2 = this.f2553c;
                if (latLngBounds2 == null) {
                    return null;
                }
                GeoPoint ll2mc3 = CoordUtil.ll2mc(latLngBounds2.southwest);
                GeoPoint ll2mc4 = CoordUtil.ll2mc(this.f2553c.northeast);
                float a2 = eVar.mo13002a((int) ll2mc3.getLongitudeE6(), (int) ll2mc4.getLatitudeE6(), (int) ll2mc4.getLongitudeE6(), (int) ll2mc3.getLatitudeE6(), this.f2554d, this.f2555e);
                return new MapStatus(mapStatus2.rotate, this.f2553c.getCenter(), mapStatus2.overlook, a2, mapStatus2.targetScreen, null);
            case 10:
                if (this.f2553c == null) {
                    return null;
                }
                int H = (eVar.mo12992H() - this.f2561k) - this.f2563m;
                if (H < 0) {
                    H = eVar.mo12992H();
                    Log.e(f2550o, "Bound paddingLeft or paddingRight too larger, please check");
                }
                int I = (eVar.mo12993I() - this.f2562l) - this.f2564n;
                if (I < 0) {
                    I = eVar.mo12993I();
                    Log.e(f2550o, "Bound paddingTop or paddingBottom too larger, please check");
                }
                float a3 = m2914a(this.f2553c, eVar2, H, I);
                LatLng a4 = m2916a(this.f2553c, eVar2, m2913a(a3));
                if (a4 == null) {
                    Log.e(f2550o, "Bound center error");
                    return null;
                }
                boolean a5 = m2918a(this.f2553c, eVar2);
                boolean a6 = m2917a(this.f2561k, this.f2562l, this.f2563m, this.f2564n, eVar);
                if (a5 || a6) {
                    MapStatus mapStatus3 = new MapStatus(mapStatus2.rotate, a4, mapStatus2.overlook, a3, null, null);
                    eVar2.mo13013a(m2915a(mapStatus3));
                    return mapStatus3;
                } else if (eVar.mo12991G() != null) {
                    return eVar.mo12991G().f2551a;
                } else {
                    return null;
                }
            case 11:
                if (this.f2553c == null) {
                    return null;
                }
                WinRound winRound = mapStatus2.winRound;
                int abs = Math.abs(winRound.right - winRound.left);
                int abs2 = Math.abs(winRound.bottom - winRound.top);
                GeoPoint ll2mc5 = CoordUtil.ll2mc(this.f2553c.southwest);
                GeoPoint ll2mc6 = CoordUtil.ll2mc(this.f2553c.northeast);
                double longitudeE6 = ll2mc5.getLongitudeE6();
                double latitudeE6 = ll2mc6.getLatitudeE6();
                double longitudeE62 = ll2mc6.getLongitudeE6();
                double latitudeE62 = ll2mc5.getLatitudeE6();
                double d = longitudeE62;
                C1078e eVar3 = eVar;
                double d2 = latitudeE6;
                float a7 = eVar3.mo13002a((int) longitudeE6, (int) latitudeE6, (int) longitudeE62, (int) latitudeE62, (abs - this.f2561k) - this.f2563m, (abs2 - this.f2562l) - Math.abs(this.f2564n));
                if (a7 == 0.0f) {
                    return null;
                }
                Point a8 = eVar2.mo13005a(CoordUtil.ll2mc(this.f2553c.northeast));
                Point a9 = eVar2.mo13005a(CoordUtil.ll2mc(this.f2553c.southwest));
                if (Math.abs(a8.y) < Math.abs(this.f2564n)) {
                    a7 -= ((float) this.f2564n) / ((float) abs2);
                }
                if (longitudeE6 == d && d2 == latitudeE62) {
                    a7 = mapStatus2.zoom;
                }
                int i = abs / 2;
                int i2 = abs2 - a9.y;
                int i3 = this.f2564n;
                return new MapStatus(mapStatus2.rotate, CoordUtil.mc2ll(eVar2.mo13030b(i, i2 > i3 ? ((((abs2 / 2) + 0) + (i3 / 2)) - (this.f2562l / 2)) + 50 : (((((abs2 / 2) + 0) + ((i3 - (abs2 - a9.y)) / 2)) + (this.f2564n / 2)) - (this.f2562l / 2)) + 50)), mapStatus2.overlook, a7, mapStatus2.targetScreen, null);
            default:
                return null;
        }
    }
}
