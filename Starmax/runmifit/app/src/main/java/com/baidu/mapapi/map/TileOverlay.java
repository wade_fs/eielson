package com.baidu.mapapi.map;

import android.util.Log;
import com.baidu.mapapi.common.Logger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;

public final class TileOverlay {
    /* access modifiers changed from: private */

    /* renamed from: b */
    public static final String f2776b = TileOverlay.class.getSimpleName();

    /* renamed from: f */
    private static int f2777f = 0;

    /* renamed from: a */
    BaiduMap f2778a;

    /* renamed from: c */
    private ExecutorService f2779c = Executors.newFixedThreadPool(1);

    /* renamed from: d */
    private HashMap<String, Tile> f2780d = new HashMap<>();
    /* access modifiers changed from: private */

    /* renamed from: e */
    public HashSet<String> f2781e = new HashSet<>();
    /* access modifiers changed from: private */

    /* renamed from: g */
    public TileProvider f2782g;

    public TileOverlay(BaiduMap baiduMap, TileProvider tileProvider) {
        this.f2778a = baiduMap;
        this.f2782g = tileProvider;
    }

    /* renamed from: a */
    private synchronized Tile m2979a(String str) {
        if (!this.f2780d.containsKey(str)) {
            return null;
        }
        Tile tile = this.f2780d.get(str);
        this.f2780d.remove(str);
        return tile;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void m2982a(String str, Tile tile) {
        this.f2780d.put(str, tile);
    }

    /* renamed from: b */
    private synchronized boolean m2984b(String str) {
        return this.f2781e.contains(str);
    }

    /* renamed from: c */
    private synchronized void m2986c(String str) {
        this.f2781e.add(str);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Tile mo11483a(int i, int i2, int i3) {
        String str;
        String str2;
        String str3 = i + "_" + i2 + "_" + i3;
        Tile a = m2979a(str3);
        if (a != null) {
            return a;
        }
        BaiduMap baiduMap = this.f2778a;
        if (baiduMap != null && f2777f == 0) {
            MapStatus mapStatus = baiduMap.getMapStatus();
            f2777f = (((mapStatus.f2538a.f3423j.right - mapStatus.f2538a.f3423j.left) / 256) + 2) * (((mapStatus.f2538a.f3423j.bottom - mapStatus.f2538a.f3423j.top) / 256) + 2);
        }
        if (this.f2780d.size() > f2777f) {
            mo11484a();
        }
        if (m2984b(str3) || this.f2779c.isShutdown()) {
            return null;
        }
        try {
            m2986c(str3);
            this.f2779c.execute(new C0948w(this, i, i2, i3, str3));
            return null;
        } catch (RejectedExecutionException unused) {
            str2 = f2776b;
            str = "ThreadPool excepiton";
        } catch (Exception unused2) {
            str2 = f2776b;
            str = "fileDir is not legal";
        }
        Log.e(str2, str);
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public synchronized void mo11484a() {
        Logger.logE(f2776b, "clearTaskSet");
        this.f2781e.clear();
        this.f2780d.clear();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo11485b() {
        this.f2779c.shutdownNow();
    }

    public boolean clearTileCache() {
        BaiduMap baiduMap = this.f2778a;
        if (baiduMap == null) {
            return false;
        }
        return baiduMap.mo10933b();
    }

    public void removeTileOverlay() {
        BaiduMap baiduMap = this.f2778a;
        if (baiduMap != null) {
            baiduMap.mo10926a(this);
        }
    }
}
