package com.baidu.platform.core.p034d;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.search.core.CityInfo;
import com.baidu.mapapi.search.core.PoiInfo;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.route.DrivingRouteResult;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.SuggestAddrInfo;
import com.baidu.mapapi.search.route.TransitRouteResult;
import com.baidu.mapapi.search.route.WalkingRouteResult;
import com.baidu.mobstat.Config;
import com.baidu.platform.base.C1319d;
import com.baidu.platform.base.SearchType;
import com.tencent.open.SocialConstants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.platform.core.d.k */
public class C1359k extends C1319d {

    /* renamed from: b */
    SuggestAddrInfo f4465b = null;

    /* renamed from: c */
    protected boolean f4466c;

    /* renamed from: a */
    private SuggestAddrInfo m5080a(JSONObject jSONObject) {
        JSONObject optJSONObject;
        JSONObject jSONObject2;
        if (jSONObject == null || (optJSONObject = jSONObject.optJSONObject("traffic_pois")) == null) {
            return null;
        }
        JSONObject optJSONObject2 = optJSONObject.optJSONObject("option");
        JSONObject optJSONObject3 = optJSONObject.optJSONObject("content");
        if (!(optJSONObject2 == null || optJSONObject3 == null)) {
            JSONObject optJSONObject4 = optJSONObject2.optJSONObject("start_city");
            String optString = optJSONObject4 != null ? optJSONObject4.optString("cname") : null;
            JSONArray optJSONArray = optJSONObject2.optJSONArray("end_city");
            String optString2 = (optJSONArray == null || (jSONObject2 = (JSONObject) optJSONArray.opt(0)) == null) ? null : jSONObject2.optString("cname");
            JSONArray optJSONArray2 = optJSONObject2.optJSONArray("city_list");
            JSONArray optJSONArray3 = optJSONObject2.optJSONArray("prio_flag");
            if (!(optJSONArray2 == null || optJSONArray3 == null)) {
                int length = optJSONArray2.length();
                boolean[] zArr = new boolean[length];
                boolean[] zArr2 = new boolean[length];
                for (int i = 0; i < length; i++) {
                    int parseInt = Integer.parseInt(optJSONArray2.optString(i));
                    int parseInt2 = Integer.parseInt(optJSONArray3.optString(i));
                    boolean z = true;
                    zArr[i] = parseInt == 1;
                    if (parseInt2 != 1) {
                        z = false;
                    }
                    zArr2[i] = z;
                }
                SuggestAddrInfo suggestAddrInfo = new SuggestAddrInfo();
                for (int i2 = 0; i2 < length; i2++) {
                    if (!zArr2[i2]) {
                        if (zArr[i2]) {
                            if (i2 == 0) {
                                suggestAddrInfo.setSuggestStartCity(m5081a(optJSONObject3.optJSONArray("start")));
                            } else if (i2 != length - 1 || i2 <= 0) {
                                suggestAddrInfo.setSuggestWpCity(m5083a(optJSONObject3, "multi_waypoints"));
                            } else {
                                suggestAddrInfo.setSuggestEndCity(m5081a(optJSONObject3.optJSONArray("end")));
                            }
                        } else if (i2 == 0) {
                            suggestAddrInfo.setSuggestStartNode(m5082a(optJSONObject3.optJSONArray("start"), optString));
                        } else if (i2 != length - 1 || i2 <= 0) {
                            suggestAddrInfo.setSuggestWpNode(m5084b(optJSONObject3, "multi_waypoints"));
                        } else {
                            suggestAddrInfo.setSuggestEndNode(m5082a(optJSONObject3.optJSONArray("end"), optString2));
                        }
                    }
                }
                return suggestAddrInfo;
            }
        }
        return null;
    }

    /* renamed from: a */
    private List<CityInfo> m5081a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() <= 0) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = (JSONObject) jSONArray.opt(i);
            if (jSONObject != null) {
                CityInfo cityInfo = new CityInfo();
                cityInfo.num = jSONObject.optInt("num");
                cityInfo.city = jSONObject.optString(Config.FEED_LIST_NAME);
                arrayList.add(cityInfo);
            }
        }
        arrayList.trimToSize();
        return arrayList;
    }

    /* renamed from: a */
    private List<PoiInfo> m5082a(JSONArray jSONArray, String str) {
        if (jSONArray == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject jSONObject = (JSONObject) jSONArray.opt(i);
            if (jSONObject != null) {
                PoiInfo poiInfo = new PoiInfo();
                poiInfo.address = jSONObject.optString("addr");
                poiInfo.uid = jSONObject.optString(Config.CUSTOM_USER_ID);
                poiInfo.name = jSONObject.optString(Config.FEED_LIST_NAME);
                poiInfo.location = CoordUtil.decodeLocation(jSONObject.optString("geo"));
                poiInfo.city = str;
                arrayList.add(poiInfo);
            }
        }
        if (arrayList.size() > 0) {
            return arrayList;
        }
        return null;
    }

    /* renamed from: a */
    private List<List<CityInfo>> m5083a(JSONObject jSONObject, String str) {
        JSONArray optJSONArray;
        ArrayList arrayList = new ArrayList();
        if (jSONObject == null || (optJSONArray = jSONObject.optJSONArray(str)) == null) {
            return null;
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            List<CityInfo> a = m5081a((JSONArray) optJSONArray.opt(i));
            if (a != null) {
                arrayList.add(a);
            }
        }
        return arrayList;
    }

    /* renamed from: b */
    private List<List<PoiInfo>> m5084b(JSONObject jSONObject, String str) {
        JSONArray optJSONArray;
        ArrayList arrayList = new ArrayList();
        if (jSONObject == null || (optJSONArray = jSONObject.optJSONArray(str)) == null) {
            return null;
        }
        for (int i = 0; i < optJSONArray.length(); i++) {
            List<PoiInfo> a = m5082a(((JSONObject) optJSONArray.opt(i)).optJSONArray("way_ponits"), "");
            if (a != null) {
                arrayList.add(a);
            }
        }
        return arrayList;
    }

    /* renamed from: b */
    private boolean m5085b(String str) {
        if (str != null && str.length() > 0) {
            try {
                JSONObject jSONObject = new JSONObject(str);
                JSONObject optJSONObject = jSONObject.optJSONObject("result");
                if (optJSONObject == null || optJSONObject.optInt(SocialConstants.PARAM_TYPE) != 23 || optJSONObject.optInt("error") != 0) {
                    return false;
                }
                this.f4465b = m5080a(jSONObject);
                return this.f4465b != null;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /* renamed from: a */
    public SearchResult mo14018a(String str) {
        SearchType a = mo14019a();
        if (m5085b(str)) {
            this.f4466c = true;
        } else {
            this.f4466c = false;
        }
        int i = C1360l.f4467a[a.ordinal()];
        if (i == 1) {
            TransitRouteResult transitRouteResult = new TransitRouteResult();
            if (!this.f4466c) {
                ((C1361m) this).mo14066a(str, transitRouteResult);
                return transitRouteResult;
            }
            transitRouteResult.setSuggestAddrInfo(this.f4465b);
            transitRouteResult.error = SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR;
            return transitRouteResult;
        } else if (i == 2) {
            DrivingRouteResult drivingRouteResult = new DrivingRouteResult();
            if (!this.f4466c) {
                ((C1351c) this).mo14056a(str, drivingRouteResult);
                return drivingRouteResult;
            }
            drivingRouteResult.setSuggestAddrInfo(this.f4465b);
            drivingRouteResult.error = SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR;
            return drivingRouteResult;
        } else if (i != 3) {
            return null;
        } else {
            WalkingRouteResult walkingRouteResult = new WalkingRouteResult();
            if (!this.f4466c) {
                ((C1363o) this).mo14067a(str, walkingRouteResult);
                return walkingRouteResult;
            }
            walkingRouteResult.setSuggestAddrInfo(this.f4465b);
            walkingRouteResult.error = SearchResult.ERRORNO.AMBIGUOUS_ROURE_ADDR;
            return walkingRouteResult;
        }
    }

    /* renamed from: a */
    public void mo14020a(SearchResult searchResult, Object obj) {
        if (obj != null && (obj instanceof OnGetRoutePlanResultListener)) {
            OnGetRoutePlanResultListener onGetRoutePlanResultListener = (OnGetRoutePlanResultListener) obj;
            int i = C1360l.f4467a[mo14019a().ordinal()];
            if (i == 1) {
                onGetRoutePlanResultListener.onGetTransitRouteResult((TransitRouteResult) searchResult);
            } else if (i == 2) {
                onGetRoutePlanResultListener.onGetDrivingRouteResult((DrivingRouteResult) searchResult);
            } else if (i == 3) {
                onGetRoutePlanResultListener.onGetWalkingRouteResult((WalkingRouteResult) searchResult);
            }
        }
    }
}
