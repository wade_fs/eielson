package com.baidu.platform.core.p034d;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapapi.search.route.IndoorRoutePlanOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tamic.novate.download.MimeType;

/* renamed from: com.baidu.platform.core.d.g */
public class C1355g extends C1320e {
    C1355g(IndoorRoutePlanOption indoorRoutePlanOption) {
        m5057a(indoorRoutePlanOption);
    }

    /* renamed from: a */
    private void m5057a(IndoorRoutePlanOption indoorRoutePlanOption) {
        this.f4435a.mo14094a("qt", "indoornavi");
        this.f4435a.mo14094a("rp_format", MimeType.JSON);
        this.f4435a.mo14094a(ProviderConstants.API_COLNAME_FEATURE_VERSION, "1");
        GeoPoint ll2mc = CoordUtil.ll2mc(indoorRoutePlanOption.mFrom.getLocation());
        if (ll2mc != null) {
            String format = String.format("%f,%f", Double.valueOf(ll2mc.getLongitudeE6()), Double.valueOf(ll2mc.getLatitudeE6()));
            this.f4435a.mo14094a("sn", (format + "|" + indoorRoutePlanOption.mFrom.getFloor()).replaceAll(" ", ""));
        }
        GeoPoint ll2mc2 = CoordUtil.ll2mc(indoorRoutePlanOption.mTo.getLocation());
        if (ll2mc2 != null) {
            String format2 = String.format("%f,%f", Double.valueOf(ll2mc2.getLongitudeE6()), Double.valueOf(ll2mc2.getLatitudeE6()));
            this.f4435a.mo14094a("en", (format2 + "|" + indoorRoutePlanOption.mTo.getFloor()).replaceAll(" ", ""));
        }
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14087l();
    }
}
