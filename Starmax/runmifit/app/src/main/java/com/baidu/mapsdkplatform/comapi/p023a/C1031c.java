package com.baidu.mapsdkplatform.comapi.p023a;

import android.animation.Animator;
import android.view.animation.Interpolator;
import com.baidu.mapapi.animation.Animation;
import com.baidu.mapapi.map.Marker;

/* renamed from: com.baidu.mapsdkplatform.comapi.a.c */
public abstract class C1031c {
    /* renamed from: a */
    public abstract void mo12874a();

    /* renamed from: a */
    public abstract void mo12875a(int i);

    /* renamed from: a */
    public abstract void mo12876a(long j);

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public abstract void mo12877a(Animator animator);

    /* renamed from: a */
    public abstract void mo12878a(Interpolator interpolator);

    /* renamed from: a */
    public abstract void mo12879a(Animation.AnimationListener animationListener);

    /* renamed from: a */
    public abstract void mo12880a(Marker marker, Animation animation);

    /* renamed from: b */
    public abstract void mo12881b();

    /* renamed from: b */
    public abstract void mo12882b(int i);

    /* renamed from: c */
    public abstract void mo12884c(int i);
}
