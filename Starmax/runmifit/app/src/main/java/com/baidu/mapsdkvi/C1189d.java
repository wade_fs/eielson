package com.baidu.mapsdkvi;

import android.net.NetworkInfo;

/* renamed from: com.baidu.mapsdkvi.d */
/* synthetic */ class C1189d {

    /* renamed from: a */
    static final /* synthetic */ int[] f3962a = new int[NetworkInfo.State.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
    static {
        /*
            android.net.NetworkInfo$State[] r0 = android.net.NetworkInfo.State.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.mapsdkvi.C1189d.f3962a = r0
            int[] r0 = com.baidu.mapsdkvi.C1189d.f3962a     // Catch:{ NoSuchFieldError -> 0x0014 }
            android.net.NetworkInfo$State r1 = android.net.NetworkInfo.State.CONNECTED     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.baidu.mapsdkvi.C1189d.f3962a     // Catch:{ NoSuchFieldError -> 0x001f }
            android.net.NetworkInfo$State r1 = android.net.NetworkInfo.State.CONNECTING     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.baidu.mapsdkvi.C1189d.f3962a     // Catch:{ NoSuchFieldError -> 0x002a }
            android.net.NetworkInfo$State r1 = android.net.NetworkInfo.State.DISCONNECTED     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            int[] r0 = com.baidu.mapsdkvi.C1189d.f3962a     // Catch:{ NoSuchFieldError -> 0x0035 }
            android.net.NetworkInfo$State r1 = android.net.NetworkInfo.State.DISCONNECTING     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            int[] r0 = com.baidu.mapsdkvi.C1189d.f3962a     // Catch:{ NoSuchFieldError -> 0x0040 }
            android.net.NetworkInfo$State r1 = android.net.NetworkInfo.State.SUSPENDED     // Catch:{ NoSuchFieldError -> 0x0040 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
        L_0x0040:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkvi.C1189d.<clinit>():void");
    }
}
