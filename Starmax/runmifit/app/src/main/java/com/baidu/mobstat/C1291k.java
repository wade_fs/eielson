package com.baidu.mobstat;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.text.TextUtils;
import com.baidu.mobstat.C1262ay;
import com.baidu.mobstat.C1271be;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.k */
public class C1291k {

    /* renamed from: a */
    static C1291k f4360a = new C1291k();

    /* renamed from: a */
    public synchronized void mo13971a(Context context) {
        m4805b(context);
    }

    /* renamed from: b */
    private void m4805b(Context context) {
        m4804a(context, m4806c(context));
    }

    /* renamed from: c */
    private ArrayList<C1292a> m4806c(Context context) {
        ArrayList<C1292a> arrayList = new ArrayList<>();
        Iterator<PackageInfo> it = m4807d(context).iterator();
        while (it.hasNext()) {
            PackageInfo next = it.next();
            ApplicationInfo applicationInfo = next.applicationInfo;
            if (applicationInfo != null) {
                String str = next.packageName;
                String str2 = next.versionName;
                Signature[] signatureArr = next.signatures;
                String str3 = "";
                String a = C1271be.C1273b.m4703a(((signatureArr == null || signatureArr.length == 0) ? str3 : signatureArr[0].toChars().toString()).getBytes());
                String str4 = applicationInfo.sourceDir;
                if (!TextUtils.isEmpty(str4)) {
                    str3 = C1271be.C1273b.m4702a(new File(str4));
                }
                arrayList.add(new C1292a(str, str2, a, str3));
            }
        }
        return arrayList;
    }

    /* renamed from: d */
    private ArrayList<PackageInfo> m4807d(Context context) {
        ArrayList<PackageInfo> arrayList = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();
        if (packageManager == null) {
            return arrayList;
        }
        List<PackageInfo> arrayList2 = new ArrayList<>(1);
        try {
            arrayList2 = packageManager.getInstalledPackages(64);
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
        }
        for (PackageInfo packageInfo : arrayList2) {
            ApplicationInfo applicationInfo = packageInfo.applicationInfo;
            if (applicationInfo != null && (applicationInfo.flags & 1) == 0) {
                arrayList.add(packageInfo);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private void m4804a(Context context, ArrayList<C1292a> arrayList) {
        String str;
        StringBuilder sb = new StringBuilder();
        sb.append(System.currentTimeMillis());
        try {
            JSONArray jSONArray = new JSONArray();
            Iterator<C1292a> it = arrayList.iterator();
            while (it.hasNext()) {
                JSONObject a = it.next().mo13972a();
                if (a != null) {
                    jSONArray.put(a);
                }
            }
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("app_apk", jSONArray);
            jSONObject.put("meta-data", sb.toString());
            str = C1262ay.C1263a.m4665a(jSONObject.toString().getBytes());
        } catch (Exception e) {
            C1255as.m4626c().mo13944b(e);
            str = "";
        }
        if (!TextUtils.isEmpty(str)) {
            C1300r.APP_APK.mo13990a(System.currentTimeMillis(), str);
        }
    }

    /* renamed from: com.baidu.mobstat.k$a */
    static class C1292a {

        /* renamed from: a */
        private String f4361a;

        /* renamed from: b */
        private String f4362b;

        /* renamed from: c */
        private String f4363c;

        /* renamed from: d */
        private String f4364d;

        public C1292a(String str, String str2, String str3, String str4) {
            str = str == null ? "" : str;
            str2 = str2 == null ? "" : str2;
            str3 = str3 == null ? "" : str3;
            str4 = str4 == null ? "" : str4;
            this.f4361a = str;
            this.f4362b = str2;
            this.f4363c = str3;
            this.f4364d = str4;
        }

        /* renamed from: a */
        public JSONObject mo13972a() {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("n", this.f4361a);
                jSONObject.put("v", this.f4362b);
                jSONObject.put("c", this.f4363c);
                jSONObject.put(Config.APP_VERSION_CODE, this.f4364d);
                return jSONObject;
            } catch (JSONException e) {
                C1255as.m4626c().mo13944b(e);
                return null;
            }
        }
    }
}
