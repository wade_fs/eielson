package com.baidu.mapapi.utils;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import com.baidu.mapframework.open.aidl.C1021a;

/* renamed from: com.baidu.mapapi.utils.d */
final class C1014d implements ServiceConnection {
    C1014d() {
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (C1012b.f3258v != null) {
            C1012b.f3258v.interrupt();
        }
        String b = C1012b.f3239c;
        Log.d(b, "onServiceConnected " + componentName);
        try {
            if (C1012b.f3240d != null) {
                C1021a unused = C1012b.f3240d = (C1021a) null;
            }
            C1021a unused2 = C1012b.f3240d = C1021a.C1022a.m3320a(iBinder);
            C1012b.f3240d.mo12858a(new C1015e(this));
        } catch (RemoteException e) {
            Log.d(C1012b.f3239c, "getComOpenClient ", e);
            if (C1012b.f3240d != null) {
                C1021a unused3 = C1012b.f3240d = (C1021a) null;
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        String b = C1012b.f3239c;
        Log.d(b, "onServiceDisconnected " + componentName);
        if (C1012b.f3240d != null) {
            C1021a unused = C1012b.f3240d = (C1021a) null;
            boolean unused2 = C1012b.f3257u = false;
        }
    }
}
