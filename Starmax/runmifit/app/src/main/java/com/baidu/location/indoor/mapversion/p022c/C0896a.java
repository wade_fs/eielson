package com.baidu.location.indoor.mapversion.p022c;

import android.content.Context;
import android.location.Location;
import com.baidu.location.BDLocation;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mobstat.Config;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashMap;

/* renamed from: com.baidu.location.indoor.mapversion.c.a */
public class C0896a {

    /* renamed from: a */
    private static C0896a f2251a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public C0899c f2252b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public String f2253c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f2254d = false;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f2255e = Config.EVENT_VIEW_RES_NAME;

    /* renamed from: f */
    private C0898b f2256f;

    /* renamed from: g */
    private String f2257g = CoordinateType.GCJ02;

    /* renamed from: h */
    private HashMap<String, C0900d> f2258h = new HashMap<>();

    /* renamed from: i */
    private HashMap<String, double[][]> f2259i = new HashMap<>();

    /* renamed from: j */
    private C0900d f2260j = null;

    /* renamed from: com.baidu.location.indoor.mapversion.c.a$a */
    public static class C0897a {

        /* renamed from: a */
        public double f2261a;

        /* renamed from: b */
        public double f2262b;

        /* renamed from: c */
        public double f2263c;

        /* renamed from: d */
        public double f2264d;

        /* renamed from: e */
        public double f2265e;

        /* renamed from: f */
        public double f2266f;

        /* renamed from: g */
        public double f2267g;

        /* renamed from: h */
        public double f2268h;

        public C0897a(String str) {
            mo10821a(str);
        }

        /* renamed from: a */
        public void mo10821a(String str) {
            String[] split = str.trim().split("\\|");
            this.f2261a = Double.valueOf(split[0]).doubleValue();
            this.f2262b = Double.valueOf(split[1]).doubleValue();
            this.f2263c = Double.valueOf(split[2]).doubleValue();
            this.f2264d = Double.valueOf(split[3]).doubleValue();
            this.f2265e = Double.valueOf(split[4]).doubleValue();
            this.f2266f = Double.valueOf(split[5]).doubleValue();
            this.f2267g = Double.valueOf(split[6]).doubleValue();
            this.f2268h = Double.valueOf(split[7]).doubleValue();
        }
    }

    /* renamed from: com.baidu.location.indoor.mapversion.c.a$b */
    private class C0898b extends Thread {

        /* renamed from: b */
        private String f2270b;

        /* renamed from: c */
        private String f2271c;

        public C0898b(String str, String str2) {
            this.f2270b = str;
            this.f2271c = str2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.mapversion.c.a.a(com.baidu.location.indoor.mapversion.c.a, boolean):boolean
         arg types: [com.baidu.location.indoor.mapversion.c.a, int]
         candidates:
          com.baidu.location.indoor.mapversion.c.a.a(com.baidu.location.indoor.mapversion.c.a, java.lang.String):java.lang.String
          com.baidu.location.indoor.mapversion.c.a.a(java.lang.String, java.lang.String):java.lang.String
          com.baidu.location.indoor.mapversion.c.a.a(double, double):void
          com.baidu.location.indoor.mapversion.c.a.a(java.lang.String, com.baidu.location.indoor.mapversion.c.a$c):void
          com.baidu.location.indoor.mapversion.c.a.a(com.baidu.location.indoor.mapversion.c.a, boolean):boolean */
        /* JADX WARNING: Removed duplicated region for block: B:30:0x011d A[Catch:{ Exception -> 0x0127 }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void run() {
            /*
                r7 = this;
                r0 = 0
                java.io.File r1 = new java.io.File     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a r2 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = r2.f2255e     // Catch:{ Exception -> 0x0127 }
                r1.<init>(r2)     // Catch:{ Exception -> 0x0127 }
                boolean r2 = r1.exists()     // Catch:{ Exception -> 0x0127 }
                if (r2 == 0) goto L_0x0018
                boolean r2 = r1.isDirectory()     // Catch:{ Exception -> 0x0127 }
                if (r2 != 0) goto L_0x001b
            L_0x0018:
                r1.mkdirs()     // Catch:{ Exception -> 0x0127 }
            L_0x001b:
                java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = "http://loc.map.baidu.com/cfgs/indoorloc/indoorroadnet"
                r2.<init>(r3)     // Catch:{ Exception -> 0x0127 }
                java.net.URLConnection r2 = r2.openConnection()     // Catch:{ Exception -> 0x0127 }
                java.net.HttpURLConnection r2 = (java.net.HttpURLConnection) r2     // Catch:{ Exception -> 0x0127 }
                r3 = 1
                r2.setDoInput(r3)     // Catch:{ Exception -> 0x0127 }
                r2.setDoOutput(r3)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = "POST"
                r2.setRequestMethod(r3)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = "Accept-encoding"
                java.lang.String r4 = "gzip"
                r2.addRequestProperty(r3, r4)     // Catch:{ Exception -> 0x0127 }
                java.io.OutputStream r3 = r2.getOutputStream()     // Catch:{ Exception -> 0x0127 }
                java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0127 }
                r4.<init>()     // Catch:{ Exception -> 0x0127 }
                java.lang.String r5 = "bldg="
                r4.append(r5)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r5 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                r4.append(r5)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r5 = "&md5="
                r4.append(r5)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r5 = r7.f2271c     // Catch:{ Exception -> 0x0127 }
                if (r5 != 0) goto L_0x005a
                java.lang.String r5 = "null"
                goto L_0x005c
            L_0x005a:
                java.lang.String r5 = r7.f2271c     // Catch:{ Exception -> 0x0127 }
            L_0x005c:
                r4.append(r5)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x0127 }
                byte[] r4 = r4.getBytes()     // Catch:{ Exception -> 0x0127 }
                r3.write(r4)     // Catch:{ Exception -> 0x0127 }
                int r3 = r2.getResponseCode()     // Catch:{ Exception -> 0x0127 }
                r4 = 0
                r5 = 200(0xc8, float:2.8E-43)
                if (r3 != r5) goto L_0x00ce
                java.lang.String r3 = "Hash"
                java.lang.String r3 = r2.getHeaderField(r3)     // Catch:{ Exception -> 0x0127 }
                java.io.InputStream r2 = r2.getInputStream()     // Catch:{ Exception -> 0x0127 }
                java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a r5 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                java.lang.String r6 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                java.lang.String r5 = r5.m2746a(r6, r3)     // Catch:{ Exception -> 0x0127 }
                r4.<init>(r1, r5)     // Catch:{ Exception -> 0x0127 }
                java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0127 }
                r1.<init>(r4)     // Catch:{ Exception -> 0x0127 }
                r5 = 1024(0x400, float:1.435E-42)
                byte[] r5 = new byte[r5]     // Catch:{ Exception -> 0x0127 }
            L_0x0093:
                int r6 = r2.read(r5)     // Catch:{ Exception -> 0x0127 }
                if (r6 < 0) goto L_0x009d
                r1.write(r5, r0, r6)     // Catch:{ Exception -> 0x0127 }
                goto L_0x0093
            L_0x009d:
                r1.flush()     // Catch:{ Exception -> 0x0127 }
                r1.close()     // Catch:{ Exception -> 0x0127 }
                java.lang.String r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.m2745a(r4)     // Catch:{ Exception -> 0x0127 }
                boolean r1 = r1.equalsIgnoreCase(r3)     // Catch:{ Exception -> 0x0127 }
                if (r1 == 0) goto L_0x00c7
                com.baidu.location.indoor.mapversion.c.a r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = r7.f2271c     // Catch:{ Exception -> 0x0127 }
                r1.m2750b(r2, r3)     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                java.lang.String unused = r1.f2253c = r2     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                boolean r1 = r1.m2754d()     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = "OK"
                r4 = r2
                goto L_0x0115
            L_0x00c7:
                java.lang.String r1 = "Download failed"
                r4.delete()     // Catch:{ Exception -> 0x0127 }
                r4 = r1
                goto L_0x0114
            L_0x00ce:
                r1 = 304(0x130, float:4.26E-43)
                if (r3 != r1) goto L_0x00fd
                com.baidu.location.indoor.mapversion.c.a r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                java.lang.String unused = r1.f2253c = r2     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                boolean r1 = r1.m2754d()     // Catch:{ Exception -> 0x0127 }
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0127 }
                r2.<init>()     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = "No roadnet update for "
                r2.append(r3)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                r2.append(r3)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = ","
                r2.append(r3)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r3 = r7.f2271c     // Catch:{ Exception -> 0x0127 }
                r2.append(r3)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r4 = r2.toString()     // Catch:{ Exception -> 0x0127 }
                goto L_0x0115
            L_0x00fd:
                r1 = 204(0xcc, float:2.86E-43)
                if (r3 != r1) goto L_0x0114
                java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0127 }
                r1.<init>()     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = "Not found bldg "
                r1.append(r2)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r2 = r7.f2270b     // Catch:{ Exception -> 0x0127 }
                r1.append(r2)     // Catch:{ Exception -> 0x0127 }
                java.lang.String r4 = r1.toString()     // Catch:{ Exception -> 0x0127 }
            L_0x0114:
                r1 = 0
            L_0x0115:
                com.baidu.location.indoor.mapversion.c.a r2 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a$c r2 = r2.f2252b     // Catch:{ Exception -> 0x0127 }
                if (r2 == 0) goto L_0x012b
                com.baidu.location.indoor.mapversion.c.a r2 = com.baidu.location.indoor.mapversion.p022c.C0896a.this     // Catch:{ Exception -> 0x0127 }
                com.baidu.location.indoor.mapversion.c.a$c r2 = r2.f2252b     // Catch:{ Exception -> 0x0127 }
                r2.mo10781a(r1, r4)     // Catch:{ Exception -> 0x0127 }
                goto L_0x012b
            L_0x0127:
                r1 = move-exception
                r1.printStackTrace()
            L_0x012b:
                com.baidu.location.indoor.mapversion.c.a r1 = com.baidu.location.indoor.mapversion.p022c.C0896a.this
                boolean unused = r1.f2254d = r0
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.indoor.mapversion.p022c.C0896a.C0898b.run():void");
        }
    }

    /* renamed from: com.baidu.location.indoor.mapversion.c.a$c */
    public interface C0899c {
        /* renamed from: a */
        void mo10781a(boolean z, String str);
    }

    /* renamed from: com.baidu.location.indoor.mapversion.c.a$d */
    public static class C0900d implements Serializable {

        /* renamed from: a */
        public String f2272a;

        /* renamed from: b */
        public String f2273b;

        /* renamed from: c */
        public C0897a f2274c;

        /* renamed from: d */
        public C0897a f2275d;

        /* renamed from: e */
        public C0897a f2276e;

        /* renamed from: f */
        public C0897a f2277f = this.f2275d;

        /* renamed from: g */
        public short[][] f2278g;

        /* renamed from: h */
        private String f2279h = CoordinateType.GCJ02;

        public C0900d(String str) {
            this.f2272a = str;
        }

        /* renamed from: a */
        public double mo10823a(double d) {
            return (d + this.f2277f.f2264d) * this.f2277f.f2263c;
        }

        /* renamed from: a */
        public C0897a mo10824a() {
            return this.f2277f;
        }

        /* renamed from: a */
        public void mo10825a(String str) {
            C0897a aVar;
            if (str != null) {
                this.f2279h = str.toLowerCase();
                if (this.f2279h.startsWith(CoordinateType.WGS84)) {
                    aVar = this.f2274c;
                } else if (this.f2279h.startsWith(BDLocation.BDLOCATION_GCJ02_TO_BD09)) {
                    aVar = this.f2276e;
                } else if (this.f2279h.startsWith(CoordinateType.GCJ02)) {
                    aVar = this.f2275d;
                } else {
                    return;
                }
                this.f2277f = aVar;
            }
        }

        /* renamed from: b */
        public double mo10826b(double d) {
            return (d + this.f2277f.f2266f) * this.f2277f.f2265e;
        }

        /* renamed from: b */
        public void mo10827b(String str) {
            String[] split = str.split("\\t");
            this.f2273b = split[1];
            this.f2274c = new C0897a(split[2]);
            this.f2276e = new C0897a(split[3]);
            this.f2275d = new C0897a(split[4]);
            this.f2277f = this.f2275d;
            this.f2278g = (short[][]) Array.newInstance(short.class, (int) this.f2277f.f2267g, (int) this.f2277f.f2268h);
            for (int i = 0; ((double) i) < this.f2277f.f2267g; i++) {
                for (int i2 = 0; ((double) i2) < this.f2277f.f2268h; i2++) {
                    this.f2278g[i][i2] = (short) (split[5].charAt((((int) this.f2277f.f2268h) * i) + i2) - '0');
                }
            }
        }

        /* renamed from: c */
        public double mo10828c(double d) {
            return (d / this.f2277f.f2263c) - this.f2277f.f2264d;
        }

        /* renamed from: d */
        public double mo10829d(double d) {
            return (d / this.f2277f.f2265e) - this.f2277f.f2266f;
        }
    }

    private C0896a(Context context) {
        this.f2255e = new File(context.getCacheDir(), this.f2255e).getAbsolutePath();
    }

    /* renamed from: a */
    public static C0896a m2740a() {
        return f2251a;
    }

    /* renamed from: a */
    public static C0896a m2741a(Context context) {
        if (f2251a == null) {
            f2251a = new C0896a(context);
        }
        return f2251a;
    }

    /* renamed from: a */
    public static String m2745a(File file) {
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            MappedByteBuffer map = fileInputStream.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, file.length());
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(map);
            String bigInteger = new BigInteger(1, instance.digest()).toString(16);
            fileInputStream.close();
            for (int length = 32 - bigInteger.length(); length > 0; length--) {
                bigInteger = "0" + bigInteger;
            }
            return bigInteger;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m2746a(String str, String str2) {
        return m2753d(str) + "_" + str2;
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public void m2750b(String str, String str2) {
        try {
            File file = new File(this.f2255e + "/" + m2746a(str, str2));
            if (file.exists()) {
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public String m2753d(String str) {
        return str;
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public boolean m2754d() {
        String str = this.f2253c;
        if (str == null) {
            return false;
        }
        File f = m2756f(str);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (!C0905d.m2783a(f, byteArrayOutputStream)) {
            return false;
        }
        this.f2258h.clear();
        this.f2259i.clear();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())));
        while (true) {
            try {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                } else if (readLine.split("\\t")[1].split("_")[0].equals("geo")) {
                    m2760j(readLine);
                } else {
                    C0900d dVar = new C0900d(this.f2253c);
                    dVar.mo10827b(readLine);
                    dVar.mo10825a(this.f2257g);
                    this.f2258h.put(dVar.f2273b.toLowerCase(), dVar);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        bufferedReader.close();
        return true;
    }

    /* renamed from: e */
    private String m2755e(String str) {
        File file = new File(this.f2255e);
        if (file.exists() && file.isDirectory()) {
            File[] listFiles = file.listFiles(new C0901b(this, str));
            int i = 0;
            if (listFiles == null || listFiles.length != 1) {
                while (listFiles != null && i < listFiles.length) {
                    listFiles[i].delete();
                    i++;
                }
            } else {
                String[] split = listFiles[0].getName().split("_");
                if (split.length < 2) {
                    return null;
                }
                return split[1];
            }
        }
        return null;
    }

    /* renamed from: f */
    private File m2756f(String str) {
        String e = m2755e(str);
        return new File(this.f2255e + "/" + m2746a(str, e));
    }

    /* renamed from: g */
    private boolean m2757g(String str) {
        File f = m2756f(str);
        return f.exists() && f.length() > 0;
    }

    /* renamed from: h */
    private boolean m2758h(String str) {
        return System.currentTimeMillis() - m2756f(str).lastModified() > 1296000000;
    }

    /* renamed from: i */
    private ArrayList<Double> m2759i(String str) {
        double d;
        ArrayList<Double> arrayList = new ArrayList<>();
        int i = 0;
        while (i < str.length()) {
            if (str.charAt(i) == ',') {
                int i2 = i + 1;
                i += 2;
                d = (double) Integer.valueOf(str.substring(i2, i)).intValue();
            } else if (str.charAt(i) == '.') {
                int i3 = i + 1;
                i += 4;
                d = Double.valueOf(str.substring(i3, i)).doubleValue();
            } else {
                int i4 = i + 2;
                double intValue = (double) Integer.valueOf(str.substring(i, i4)).intValue();
                i = i4;
                d = intValue;
            }
            arrayList.add(Double.valueOf(d));
        }
        return arrayList;
    }

    /* renamed from: j */
    private void m2760j(String str) {
        String[] split = str.split("\\t");
        String lowerCase = split[1].split("_")[1].toLowerCase();
        try {
            if (this.f2258h.containsKey(lowerCase)) {
                ArrayList<Double> i = m2759i(split[5]);
                int length = this.f2258h.get(lowerCase).f2278g.length;
                int length2 = this.f2258h.get(lowerCase).f2278g[0].length;
                double[][] dArr = (double[][]) Array.newInstance(double.class, length, length2);
                int i2 = 0;
                int i3 = 0;
                while (i2 < length) {
                    int i4 = i3;
                    for (int i5 = 0; i5 < length2; i5++) {
                        if (this.f2258h.get(lowerCase).f2278g[i2][i5] <= 0 || this.f2258h.get(lowerCase).f2278g[i2][i5] == 9) {
                            dArr[i2][i5] = 0.0d;
                        } else {
                            dArr[i2][i5] = i.get(i4).doubleValue();
                            i4++;
                        }
                    }
                    i2++;
                    i3 = i4;
                }
                this.f2259i.put(lowerCase.toLowerCase(), dArr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* renamed from: k */
    private void m2761k(String str) {
        if (!this.f2254d) {
            this.f2254d = true;
            this.f2256f = new C0898b(str, m2755e(str));
            this.f2256f.start();
        }
    }

    /* renamed from: a */
    public void mo10814a(double d, double d2) {
        double d3 = d;
        double d4 = d2;
        if (this.f2260j == null) {
            float[] fArr = new float[2];
            float[] fArr2 = new float[2];
            double d5 = d;
            Location.distanceBetween(d2, d5, d2, d3 + 0.01d, fArr);
            double d6 = (double) fArr[0];
            Double.isNaN(d6);
            Location.distanceBetween(d2, d5, d4 + 0.01d, d, fArr2);
            double d7 = (double) fArr2[0];
            Double.isNaN(d7);
            this.f2260j = new C0900d("outdoor");
            C0900d dVar = this.f2260j;
            dVar.f2273b = "out";
            dVar.f2277f = new C0897a("0|1.0|" + (d6 / 0.01d) + "|" + (-d3) + "|" + (d7 / 0.01d) + "|" + (-d2) + "|0|0");
        }
    }

    /* renamed from: a */
    public void mo10815a(String str) {
        this.f2257g = str;
    }

    /* renamed from: a */
    public void mo10816a(String str, C0899c cVar) {
        String str2 = this.f2253c;
        if (str2 == null || !str.equals(str2)) {
            this.f2252b = cVar;
            if (!m2757g(str) || m2758h(str)) {
                m2761k(str);
                return;
            }
            this.f2253c = str;
            m2754d();
            C0899c cVar2 = this.f2252b;
            if (cVar2 != null) {
                cVar2.mo10781a(true, "OK");
            }
        }
    }

    /* renamed from: b */
    public C0900d mo10817b(String str) {
        return this.f2258h.get(str.toLowerCase());
    }

    /* renamed from: b */
    public void mo10818b() {
        this.f2258h.clear();
        this.f2259i.clear();
        this.f2253c = null;
        this.f2254d = false;
    }

    /* renamed from: c */
    public C0900d mo10819c() {
        return this.f2260j;
    }

    /* renamed from: c */
    public double[][] mo10820c(String str) {
        return this.f2259i.get(str.toLowerCase());
    }
}
