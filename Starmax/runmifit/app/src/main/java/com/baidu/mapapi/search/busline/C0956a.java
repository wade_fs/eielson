package com.baidu.mapapi.search.busline;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.busline.a */
final class C0956a implements Parcelable.Creator<BusLineResult> {
    C0956a() {
    }

    /* renamed from: a */
    public BusLineResult createFromParcel(Parcel parcel) {
        return new BusLineResult(parcel);
    }

    /* renamed from: a */
    public BusLineResult[] newArray(int i) {
        return new BusLineResult[i];
    }
}
