package com.baidu.location.p016d;

import com.baidu.location.p016d.C0813l;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: com.baidu.location.d.p */
final class C0820p extends C0813l.C0816b {
    C0820p(String str, int i, String str2, String str3, String str4, int i2, int i3) {
        super(str, i, str2, str3, str4, i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x0151  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x016b  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> mo10614a(org.json.JSONObject r29, java.lang.String r30, int r31) {
        /*
            r28 = this;
            java.lang.String r0 = "y"
            java.lang.String r1 = "x"
            java.lang.String r2 = "rk"
            java.lang.String r3 = "tp"
            java.lang.String r4 = "ne"
            java.lang.String r5 = "pid"
            java.lang.String r6 = "\",\""
            java.util.Iterator r7 = r29.keys()
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
        L_0x001c:
            boolean r10 = r7.hasNext()
            java.lang.String r12 = "INSERT OR REPLACE INTO %s VALUES %s"
            r14 = 1
            if (r10 == 0) goto L_0x01c5
            java.lang.StringBuffer r10 = new java.lang.StringBuffer
            r10.<init>()
            java.lang.Object r15 = r7.next()
            java.lang.String r15 = (java.lang.String) r15
            r13 = r30
            com.baidu.location.p016d.C0813l.C0816b.m2257b(r9, r15, r13, r14)
            r16 = 0
            r14 = r29
            org.json.JSONArray r17 = r14.getJSONArray(r15)     // Catch:{ JSONException -> 0x0040 }
            r11 = r17
            goto L_0x0042
        L_0x0040:
            r11 = r16
        L_0x0042:
            if (r11 == 0) goto L_0x01a7
            r18 = r7
            r7 = 0
        L_0x0047:
            int r13 = r11.length()
            java.lang.String r19 = "RGCPOI"
            if (r7 >= r13) goto L_0x017e
            org.json.JSONObject r13 = r11.getJSONObject(r7)     // Catch:{ JSONException -> 0x013d }
            boolean r20 = r13.has(r5)     // Catch:{ JSONException -> 0x013d }
            if (r20 == 0) goto L_0x0064
            java.lang.String r20 = r13.getString(r5)     // Catch:{ JSONException -> 0x013d }
            r27 = r20
            r20 = r5
            r5 = r27
            goto L_0x0068
        L_0x0064:
            r20 = r5
            r5 = r16
        L_0x0068:
            boolean r21 = r13.has(r4)     // Catch:{ JSONException -> 0x0132 }
            if (r21 == 0) goto L_0x0079
            java.lang.String r21 = r13.getString(r4)     // Catch:{ JSONException -> 0x0132 }
            r27 = r21
            r21 = r4
            r4 = r27
            goto L_0x007d
        L_0x0079:
            r21 = r4
            r4 = r16
        L_0x007d:
            boolean r22 = r13.has(r3)     // Catch:{ JSONException -> 0x0129 }
            if (r22 == 0) goto L_0x008e
            java.lang.String r22 = r13.getString(r3)     // Catch:{ JSONException -> 0x0129 }
            r27 = r22
            r22 = r3
            r3 = r27
            goto L_0x0092
        L_0x008e:
            r22 = r3
            r3 = r16
        L_0x0092:
            boolean r23 = r13.has(r2)     // Catch:{ JSONException -> 0x0122 }
            if (r23 == 0) goto L_0x00a7
            int r23 = r13.getInt(r2)     // Catch:{ JSONException -> 0x0122 }
            java.lang.Integer r23 = java.lang.Integer.valueOf(r23)     // Catch:{ JSONException -> 0x0122 }
            r27 = r23
            r23 = r2
            r2 = r27
            goto L_0x00ab
        L_0x00a7:
            r23 = r2
            r2 = r16
        L_0x00ab:
            boolean r24 = r13.has(r1)     // Catch:{ JSONException -> 0x011d }
            if (r24 == 0) goto L_0x00c0
            double r24 = r13.getDouble(r1)     // Catch:{ JSONException -> 0x011d }
            java.lang.Double r24 = java.lang.Double.valueOf(r24)     // Catch:{ JSONException -> 0x011d }
            r27 = r24
            r24 = r1
            r1 = r27
            goto L_0x00c4
        L_0x00c0:
            r24 = r1
            r1 = r16
        L_0x00c4:
            boolean r25 = r13.has(r0)     // Catch:{ JSONException -> 0x011a }
            if (r25 == 0) goto L_0x00d3
            double r25 = r13.getDouble(r0)     // Catch:{ JSONException -> 0x011a }
            java.lang.Double r13 = java.lang.Double.valueOf(r25)     // Catch:{ JSONException -> 0x011a }
            goto L_0x00d5
        L_0x00d3:
            r13 = r16
        L_0x00d5:
            int r25 = r10.length()     // Catch:{ JSONException -> 0x011a }
            r26 = r0
            java.lang.String r0 = ","
            if (r25 <= 0) goto L_0x00e2
            r10.append(r0)     // Catch:{ JSONException -> 0x0149 }
        L_0x00e2:
            r25 = r11
            java.lang.String r11 = "(\""
            r10.append(r11)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r5)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r6)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r15)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r6)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r4)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r6)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r3)     // Catch:{ JSONException -> 0x0118 }
            java.lang.String r3 = "\","
            r10.append(r3)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r1)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r0)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r13)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r0)     // Catch:{ JSONException -> 0x0118 }
            r10.append(r2)     // Catch:{ JSONException -> 0x0118 }
            java.lang.String r0 = ")"
            r10.append(r0)     // Catch:{ JSONException -> 0x0118 }
            goto L_0x014b
        L_0x0118:
            goto L_0x014b
        L_0x011a:
            r26 = r0
            goto L_0x0149
        L_0x011d:
            r26 = r0
            r24 = r1
            goto L_0x0149
        L_0x0122:
            r26 = r0
            r24 = r1
            r23 = r2
            goto L_0x0149
        L_0x0129:
            r26 = r0
            r24 = r1
            r23 = r2
            r22 = r3
            goto L_0x0149
        L_0x0132:
            r26 = r0
            r24 = r1
            r23 = r2
            r22 = r3
            r21 = r4
            goto L_0x0149
        L_0x013d:
            r26 = r0
            r24 = r1
            r23 = r2
            r22 = r3
            r21 = r4
            r20 = r5
        L_0x0149:
            r25 = r11
        L_0x014b:
            int r0 = r7 % 50
            r1 = 49
            if (r0 != r1) goto L_0x016b
            java.util.Locale r0 = java.util.Locale.US
            r1 = 2
            java.lang.Object[] r2 = new java.lang.Object[r1]
            r1 = 0
            r2[r1] = r19
            java.lang.String r3 = r10.toString()
            r4 = 1
            r2[r4] = r3
            java.lang.String r0 = java.lang.String.format(r0, r12, r2)
            r8.add(r0)
            r10.setLength(r1)
            goto L_0x016c
        L_0x016b:
            r1 = 0
        L_0x016c:
            int r7 = r7 + 1
            r5 = r20
            r4 = r21
            r3 = r22
            r2 = r23
            r1 = r24
            r11 = r25
            r0 = r26
            goto L_0x0047
        L_0x017e:
            r26 = r0
            r24 = r1
            r23 = r2
            r22 = r3
            r21 = r4
            r20 = r5
            r1 = 0
            int r0 = r10.length()
            if (r0 <= 0) goto L_0x01b5
            java.util.Locale r0 = java.util.Locale.US
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r1] = r19
            java.lang.String r1 = r10.toString()
            r3 = 1
            r2[r3] = r1
            java.lang.String r0 = java.lang.String.format(r0, r12, r2)
            r8.add(r0)
            goto L_0x01b5
        L_0x01a7:
            r26 = r0
            r24 = r1
            r23 = r2
            r22 = r3
            r21 = r4
            r20 = r5
            r18 = r7
        L_0x01b5:
            r7 = r18
            r5 = r20
            r4 = r21
            r3 = r22
            r2 = r23
            r1 = r24
            r0 = r26
            goto L_0x001c
        L_0x01c5:
            int r0 = r9.length()
            if (r0 <= 0) goto L_0x01e0
            java.util.Locale r0 = java.util.Locale.US
            r1 = 2
            java.lang.Object[] r1 = new java.lang.Object[r1]
            java.lang.String r2 = "RGCUPDATE"
            r3 = 0
            r1[r3] = r2
            r2 = 1
            r1[r2] = r9
            java.lang.String r0 = java.lang.String.format(r0, r12, r1)
            r8.add(r0)
            goto L_0x01e2
        L_0x01e0:
            r2 = 1
            r3 = 0
        L_0x01e2:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.Object[] r1 = new java.lang.Object[r2]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r31)
            r1[r3] = r2
            java.lang.String r2 = "DELETE FROM RGCPOI WHERE pid NOT IN (SELECT pid FROM RGCPOI LIMIT %d);"
            java.lang.String r0 = java.lang.String.format(r0, r2, r1)
            r8.add(r0)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0820p.mo10614a(org.json.JSONObject, java.lang.String, int):java.util.List");
    }
}
