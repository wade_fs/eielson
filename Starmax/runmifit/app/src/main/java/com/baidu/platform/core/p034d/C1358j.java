package com.baidu.platform.core.p034d;

import com.baidu.mapapi.search.route.BikingRoutePlanOption;
import com.baidu.mapapi.search.route.DrivingRoutePlanOption;
import com.baidu.mapapi.search.route.IndoorRoutePlanOption;
import com.baidu.mapapi.search.route.MassTransitRoutePlanOption;
import com.baidu.mapapi.search.route.OnGetRoutePlanResultListener;
import com.baidu.mapapi.search.route.TransitRoutePlanOption;
import com.baidu.mapapi.search.route.WalkingRoutePlanOption;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.d.j */
public class C1358j extends C1316a implements C1353e {

    /* renamed from: b */
    private OnGetRoutePlanResultListener f4464b = null;

    /* renamed from: a */
    public void mo14057a() {
        this.f4422a.lock();
        this.f4464b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14058a(OnGetRoutePlanResultListener onGetRoutePlanResultListener) {
        this.f4422a.lock();
        this.f4464b = onGetRoutePlanResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14059a(BikingRoutePlanOption bikingRoutePlanOption) {
        C1349a aVar = new C1349a();
        aVar.mo14021a(SearchType.BIKE_ROUTE);
        return mo14016a(new C1350b(bikingRoutePlanOption), this.f4464b, aVar);
    }

    /* renamed from: a */
    public boolean mo14060a(DrivingRoutePlanOption drivingRoutePlanOption) {
        C1351c cVar = new C1351c();
        cVar.mo14021a(SearchType.DRIVE_ROUTE);
        return mo14016a(new C1352d(drivingRoutePlanOption), this.f4464b, cVar);
    }

    /* renamed from: a */
    public boolean mo14061a(IndoorRoutePlanOption indoorRoutePlanOption) {
        C1354f fVar = new C1354f();
        fVar.mo14021a(SearchType.INDOOR_ROUTE);
        return mo14016a(new C1355g(indoorRoutePlanOption), this.f4464b, fVar);
    }

    /* renamed from: a */
    public boolean mo14062a(MassTransitRoutePlanOption massTransitRoutePlanOption) {
        C1356h hVar = new C1356h();
        hVar.mo14021a(SearchType.MASS_TRANSIT_ROUTE);
        return mo14016a(new C1357i(massTransitRoutePlanOption), this.f4464b, hVar);
    }

    /* renamed from: a */
    public boolean mo14063a(TransitRoutePlanOption transitRoutePlanOption) {
        C1361m mVar = new C1361m();
        mVar.mo14021a(SearchType.TRANSIT_ROUTE);
        return mo14016a(new C1362n(transitRoutePlanOption), this.f4464b, mVar);
    }

    /* renamed from: a */
    public boolean mo14064a(WalkingRoutePlanOption walkingRoutePlanOption) {
        C1363o oVar = new C1363o();
        oVar.mo14021a(SearchType.WALK_ROUTE);
        return mo14016a(new C1364p(walkingRoutePlanOption), this.f4464b, oVar);
    }
}
