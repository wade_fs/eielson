package com.baidu.mapapi.map;

import android.os.Bundle;
import android.util.Log;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.model.inner.GeoPoint;

public final class TileOverlayOptions {

    /* renamed from: c */
    private static Bundle f2783c;

    /* renamed from: j */
    private static final String f2784j = TileOverlayOptions.class.getSimpleName();

    /* renamed from: a */
    private int f2785a = 209715200;

    /* renamed from: b */
    private TileProvider f2786b;

    /* renamed from: d */
    private int f2787d = 20;
    public int datasource;

    /* renamed from: e */
    private int f2788e = 3;

    /* renamed from: f */
    private int f2789f = 15786414;

    /* renamed from: g */
    private int f2790g = -20037726;

    /* renamed from: h */
    private int f2791h = -15786414;

    /* renamed from: i */
    private int f2792i = 20037726;
    public String urlString;

    public TileOverlayOptions() {
        f2783c = new Bundle();
        f2783c.putInt("rectr", this.f2789f);
        f2783c.putInt("rectb", this.f2790g);
        f2783c.putInt("rectl", this.f2791h);
        f2783c.putInt("rectt", this.f2792i);
    }

    /* renamed from: a */
    private TileOverlayOptions m2990a(int i, int i2) {
        this.f2787d = i;
        this.f2788e = i2;
        return this;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo11488a() {
        f2783c.putString("url", this.urlString);
        f2783c.putInt("datasource", this.datasource);
        f2783c.putInt("maxDisplay", this.f2787d);
        f2783c.putInt("minDisplay", this.f2788e);
        f2783c.putInt("sdktiletmpmax", this.f2785a);
        return f2783c;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public TileOverlay mo11489a(BaiduMap baiduMap) {
        return new TileOverlay(baiduMap, this.f2786b);
    }

    public TileOverlayOptions setMaxTileTmp(int i) {
        this.f2785a = i;
        return this;
    }

    public TileOverlayOptions setPositionFromBounds(LatLngBounds latLngBounds) {
        if (latLngBounds != null) {
            GeoPoint ll2mc = CoordUtil.ll2mc(latLngBounds.northeast);
            GeoPoint ll2mc2 = CoordUtil.ll2mc(latLngBounds.southwest);
            double latitudeE6 = ll2mc.getLatitudeE6();
            double longitudeE6 = ll2mc2.getLongitudeE6();
            double latitudeE62 = ll2mc2.getLatitudeE6();
            double longitudeE62 = ll2mc.getLongitudeE6();
            if (latitudeE6 <= latitudeE62 || longitudeE62 <= longitudeE6) {
                Log.e(f2784j, "BDMapSDKException: bounds is illegal, use default bounds");
            } else {
                f2783c.putInt("rectr", (int) longitudeE62);
                f2783c.putInt("rectb", (int) latitudeE62);
                f2783c.putInt("rectl", (int) longitudeE6);
                f2783c.putInt("rectt", (int) latitudeE6);
            }
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: bound can not be null");
    }

    public TileOverlayOptions tileProvider(TileProvider tileProvider) {
        int maxDisLevel;
        String str;
        String str2;
        if (tileProvider == null) {
            return null;
        }
        if (tileProvider instanceof UrlTileProvider) {
            this.datasource = 1;
            String tileUrl = ((UrlTileProvider) tileProvider).getTileUrl();
            if (tileUrl == null || "".equals(tileUrl) || !tileUrl.contains("{x}") || !tileUrl.contains("{y}") || !tileUrl.contains("{z}")) {
                str = f2784j;
                str2 = "tile url template is illegal, must contains {x}、{y}、{z}";
            } else {
                this.urlString = tileUrl;
                this.f2786b = tileProvider;
                maxDisLevel = tileProvider.getMaxDisLevel();
                int minDisLevel = tileProvider.getMinDisLevel();
                if (maxDisLevel <= 21 || minDisLevel < 3) {
                    Log.e(f2784j, "display level is illegal");
                } else {
                    m2990a(maxDisLevel, minDisLevel);
                }
                return this;
            }
        } else if (tileProvider instanceof FileTileProvider) {
            this.datasource = 0;
            this.f2786b = tileProvider;
            maxDisLevel = tileProvider.getMaxDisLevel();
            int minDisLevel2 = tileProvider.getMinDisLevel();
            if (maxDisLevel <= 21) {
            }
            Log.e(f2784j, "display level is illegal");
            return this;
        } else {
            str = f2784j;
            str2 = "tileProvider must be UrlTileProvider or FileTileProvider";
        }
        Log.e(str, str2);
        return null;
    }
}
