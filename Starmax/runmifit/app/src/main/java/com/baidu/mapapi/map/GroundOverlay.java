package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1082h;

public final class GroundOverlay extends Overlay {

    /* renamed from: j */
    private static final String f2469j = GroundOverlay.class.getSimpleName();

    /* renamed from: a */
    int f2470a;

    /* renamed from: b */
    BitmapDescriptor f2471b;

    /* renamed from: c */
    LatLng f2472c;

    /* renamed from: d */
    double f2473d;

    /* renamed from: e */
    double f2474e;

    /* renamed from: f */
    float f2475f;

    /* renamed from: g */
    float f2476g;

    /* renamed from: h */
    LatLngBounds f2477h;

    /* renamed from: i */
    float f2478i;

    GroundOverlay() {
        this.type = C1082h.ground;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Bundle mo10900a(Bundle bundle) {
        super.mo10900a(bundle);
        bundle.putBundle("image_info", this.f2471b.mo11047b());
        if (this.f2470a == 1) {
            GeoPoint ll2mc = CoordUtil.ll2mc(this.f2477h.southwest);
            double longitudeE6 = ll2mc.getLongitudeE6();
            double latitudeE6 = ll2mc.getLatitudeE6();
            GeoPoint ll2mc2 = CoordUtil.ll2mc(this.f2477h.northeast);
            double longitudeE62 = ll2mc2.getLongitudeE6();
            double latitudeE62 = ll2mc2.getLatitudeE6();
            this.f2473d = longitudeE62 - longitudeE6;
            this.f2474e = latitudeE62 - latitudeE6;
            this.f2472c = CoordUtil.mc2ll(new GeoPoint(latitudeE6 + (this.f2474e / 2.0d), longitudeE6 + (this.f2473d / 2.0d)));
            this.f2475f = 0.5f;
            this.f2476g = 0.5f;
        }
        double d = this.f2473d;
        if (d <= 0.0d || this.f2474e <= 0.0d) {
            throw new IllegalStateException("BDMapSDKException: when you add ground overlay, the width and height must greater than 0");
        }
        bundle.putDouble("x_distance", d);
        if (this.f2474e == 2.147483647E9d) {
            double d2 = this.f2473d;
            double height = (double) this.f2471b.f2437a.getHeight();
            Double.isNaN(height);
            double d3 = d2 * height;
            double width = (double) ((float) this.f2471b.f2437a.getWidth());
            Double.isNaN(width);
            this.f2474e = (double) ((int) (d3 / width));
        }
        bundle.putDouble("y_distance", this.f2474e);
        GeoPoint ll2mc3 = CoordUtil.ll2mc(this.f2472c);
        bundle.putDouble("location_x", ll2mc3.getLongitudeE6());
        bundle.putDouble("location_y", ll2mc3.getLatitudeE6());
        bundle.putFloat("anchor_x", this.f2475f);
        bundle.putFloat("anchor_y", this.f2476g);
        bundle.putFloat("transparency", this.f2478i);
        return bundle;
    }

    public float getAnchorX() {
        return this.f2475f;
    }

    public float getAnchorY() {
        return this.f2476g;
    }

    public LatLngBounds getBounds() {
        return this.f2477h;
    }

    public double getHeight() {
        return this.f2474e;
    }

    public BitmapDescriptor getImage() {
        return this.f2471b;
    }

    public LatLng getPosition() {
        return this.f2472c;
    }

    public float getTransparency() {
        return this.f2478i;
    }

    public double getWidth() {
        return this.f2473d;
    }

    public void setAnchor(float f, float f2) {
        if (f >= 0.0f && f <= 1.0f && f2 >= 0.0f && f2 <= 1.0f) {
            this.f2475f = f;
            this.f2476g = f2;
            this.listener.mo11330b(super);
        }
    }

    public void setDimensions(int i) {
        this.f2473d = (double) i;
        this.f2474e = 2.147483647E9d;
        this.listener.mo11330b(super);
    }

    public void setDimensions(int i, int i2) {
        this.f2473d = (double) i;
        this.f2474e = (double) i2;
        this.listener.mo11330b(super);
    }

    public void setImage(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f2471b = bitmapDescriptor;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: image can not be null");
    }

    public void setPosition(LatLng latLng) {
        if (latLng != null) {
            this.f2470a = 2;
            this.f2472c = latLng;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: position can not be null");
    }

    public void setPositionFromBounds(LatLngBounds latLngBounds) {
        if (latLngBounds != null) {
            this.f2470a = 1;
            this.f2477h = latLngBounds;
            this.listener.mo11330b(super);
            return;
        }
        throw new IllegalArgumentException("BDMapSDKException: bounds can not be null");
    }

    public void setTransparency(float f) {
        if (f <= 1.0f && f >= 0.0f) {
            this.f2478i = f;
            this.listener.mo11330b(super);
        }
    }
}
