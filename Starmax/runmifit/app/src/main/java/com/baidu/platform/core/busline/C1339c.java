package com.baidu.platform.core.busline;

import com.baidu.mapapi.search.busline.BusLineSearchOption;
import com.baidu.mapapi.search.busline.OnGetBusLineSearchResultListener;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.busline.c */
public class C1339c extends C1316a implements IBusLineSearch {

    /* renamed from: b */
    OnGetBusLineSearchResultListener f4456b = null;

    /* renamed from: a */
    public void mo14043a() {
        this.f4422a.lock();
        this.f4456b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14044a(OnGetBusLineSearchResultListener onGetBusLineSearchResultListener) {
        this.f4422a.lock();
        this.f4456b = onGetBusLineSearchResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14045a(BusLineSearchOption busLineSearchOption) {
        C1337a aVar = new C1337a();
        aVar.mo14021a(SearchType.BUS_LINE_DETAIL);
        return mo14016a(new C1338b(busLineSearchOption), this.f4456b, aVar);
    }
}
