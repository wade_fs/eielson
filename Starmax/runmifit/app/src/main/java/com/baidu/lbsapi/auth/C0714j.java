package com.baidu.lbsapi.auth;

import java.util.Hashtable;

/* renamed from: com.baidu.lbsapi.auth.j */
class C0714j implements Runnable {

    /* renamed from: a */
    final /* synthetic */ int f1093a;

    /* renamed from: b */
    final /* synthetic */ boolean f1094b;

    /* renamed from: c */
    final /* synthetic */ String f1095c;

    /* renamed from: d */
    final /* synthetic */ String f1096d;

    /* renamed from: e */
    final /* synthetic */ Hashtable f1097e;

    /* renamed from: f */
    final /* synthetic */ LBSAuthManager f1098f;

    C0714j(LBSAuthManager lBSAuthManager, int i, boolean z, String str, String str2, Hashtable hashtable) {
        this.f1098f = lBSAuthManager;
        this.f1093a = i;
        this.f1094b = z;
        this.f1095c = str;
        this.f1096d = str2;
        this.f1097e = hashtable;
    }

    public void run() {
        C0702a.m1616a("status = " + this.f1093a + "; forced = " + this.f1094b + "checkAK = " + this.f1098f.m1610b(this.f1095c));
        int i = this.f1093a;
        if (i == 601 || this.f1094b || i == -1 || this.f1098f.m1610b(this.f1095c)) {
            C0702a.m1616a("authenticate sendAuthRequest");
            String[] b = C0703b.m1624b(LBSAuthManager.f1068a);
            C0702a.m1616a("authStrings.length:" + b.length);
            if (b == null || b.length <= 1) {
                this.f1098f.m1606a(this.f1094b, this.f1096d, this.f1097e, this.f1095c);
                return;
            }
            C0702a.m1616a("more sha1 auth");
            this.f1098f.m1607a(this.f1094b, this.f1096d, this.f1097e, b, this.f1095c);
            return;
        }
        if (602 == this.f1093a) {
            C0702a.m1616a("authenticate wait ");
            if (LBSAuthManager.f1069d != null) {
                LBSAuthManager.f1069d.mo10217b();
            }
        } else {
            C0702a.m1616a("authenticate else");
        }
        this.f1098f.m1605a(null, this.f1095c);
    }
}
