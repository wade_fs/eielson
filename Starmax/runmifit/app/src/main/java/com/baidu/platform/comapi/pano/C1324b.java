package com.baidu.platform.comapi.pano;

import com.baidu.mapapi.http.HttpClient;
import com.baidu.platform.comapi.pano.C1322a;

/* renamed from: com.baidu.platform.comapi.pano.b */
class C1324b extends HttpClient.ProtoResultCallback {

    /* renamed from: a */
    final /* synthetic */ C1322a.C1323a f4445a;

    /* renamed from: b */
    final /* synthetic */ C1322a f4446b;

    C1324b(C1322a aVar, C1322a.C1323a aVar2) {
        this.f4446b = aVar;
        this.f4445a = aVar2;
    }

    public void onFailed(HttpClient.HttpStateError httpStateError) {
        this.f4445a.mo12839a(httpStateError);
    }

    public void onSuccess(String str) {
        this.f4445a.mo12841a(this.f4446b.m4920a(str));
    }
}
