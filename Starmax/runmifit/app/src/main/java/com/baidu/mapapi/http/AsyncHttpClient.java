package com.baidu.mapapi.http;

import android.os.Build;
import com.baidu.mapapi.http.HttpClient;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsyncHttpClient {
    /* access modifiers changed from: private */

    /* renamed from: a */
    public int f2350a = 10000;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public int f2351b = 10000;

    /* renamed from: c */
    private ExecutorService f2352c = Executors.newCachedThreadPool();

    /* renamed from: com.baidu.mapapi.http.AsyncHttpClient$a */
    private static abstract class C0916a implements Runnable {
        private C0916a() {
        }

        /* synthetic */ C0916a(C0917a aVar) {
            this();
        }

        /* renamed from: a */
        public abstract void mo10891a();

        public void run() {
            mo10891a();
        }
    }

    static {
        if (Build.VERSION.SDK_INT <= 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    public void get(String str, HttpClient.ProtoResultCallback protoResultCallback) {
        if (str != null) {
            this.f2352c.submit(new C0917a(this, protoResultCallback, str));
            return;
        }
        throw new IllegalArgumentException("URI cannot be null");
    }

    /* access modifiers changed from: protected */
    public boolean isAuthorized() {
        int permissionCheck = PermissionCheck.permissionCheck();
        return permissionCheck == 0 || permissionCheck == 602 || permissionCheck == 601;
    }
}
