package com.baidu.mapapi.search.share;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.SearchResult;

public class ShareUrlResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<ShareUrlResult> CREATOR = new C1005a();

    /* renamed from: a */
    private String f3129a;

    /* renamed from: b */
    private int f3130b;

    public ShareUrlResult() {
    }

    protected ShareUrlResult(Parcel parcel) {
        this.f3129a = parcel.readString();
        this.f3130b = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public String getUrl() {
        return this.f3129a;
    }

    public void setType(int i) {
        this.f3130b = i;
    }

    public void setUrl(String str) {
        this.f3129a = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f3129a);
        parcel.writeInt(this.f3130b);
    }
}
