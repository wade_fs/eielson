package com.baidu.platform.core.p034d;

import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.d.l */
/* synthetic */ class C1360l {

    /* renamed from: a */
    static final /* synthetic */ int[] f4467a = new int[SearchType.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    static {
        /*
            com.baidu.platform.base.SearchType[] r0 = com.baidu.platform.base.SearchType.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.platform.core.p034d.C1360l.f4467a = r0
            int[] r0 = com.baidu.platform.core.p034d.C1360l.f4467a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.baidu.platform.base.SearchType r1 = com.baidu.platform.base.SearchType.TRANSIT_ROUTE     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.baidu.platform.core.p034d.C1360l.f4467a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.baidu.platform.base.SearchType r1 = com.baidu.platform.base.SearchType.DRIVE_ROUTE     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.baidu.platform.core.p034d.C1360l.f4467a     // Catch:{ NoSuchFieldError -> 0x002a }
            com.baidu.platform.base.SearchType r1 = com.baidu.platform.base.SearchType.WALK_ROUTE     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.platform.core.p034d.C1360l.<clinit>():void");
    }
}
