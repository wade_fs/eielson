package com.baidu.location;

import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.location.Address;
import com.baidu.location.p019g.C0855k;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public final class BDLocation implements Parcelable {
    public static final String BDLOCATION_BD09LL_TO_GCJ02 = "bd09ll2gcj";
    public static final String BDLOCATION_BD09_TO_GCJ02 = "bd092gcj";
    public static final String BDLOCATION_GCJ02_TO_BD09 = "bd09";
    public static final String BDLOCATION_GCJ02_TO_BD09LL = "bd09ll";
    public static final String BDLOCATION_WGS84_TO_GCJ02 = "gps2gcj";
    public static final Parcelable.Creator<BDLocation> CREATOR = new C0722a();
    public static final int GPS_ACCURACY_BAD = 3;
    public static final int GPS_ACCURACY_GOOD = 1;
    public static final int GPS_ACCURACY_MID = 2;
    public static final int GPS_ACCURACY_UNKNOWN = 0;
    public static final int GPS_RECTIFY_INDOOR = 1;
    public static final int GPS_RECTIFY_NONE = 0;
    public static final int GPS_RECTIFY_OUTDOOR = 2;
    public static final int INDOOR_LOCATION_NEARBY_SURPPORT_TRUE = 2;
    public static final int INDOOR_LOCATION_SOURCE_BLUETOOTH = 4;
    public static final int INDOOR_LOCATION_SOURCE_MAGNETIC = 2;
    public static final int INDOOR_LOCATION_SOURCE_SMALLCELLSTATION = 8;
    public static final int INDOOR_LOCATION_SOURCE_UNKNOWN = 0;
    public static final int INDOOR_LOCATION_SOURCE_WIFI = 1;
    public static final int INDOOR_LOCATION_SURPPORT_FALSE = 0;
    public static final int INDOOR_LOCATION_SURPPORT_TRUE = 1;
    public static final int INDOOR_NETWORK_STATE_HIGH = 2;
    public static final int INDOOR_NETWORK_STATE_LOW = 0;
    public static final int INDOOR_NETWORK_STATE_MIDDLE = 1;
    public static final int LOCATION_WHERE_IN_CN = 1;
    public static final int LOCATION_WHERE_OUT_CN = 0;
    public static final int LOCATION_WHERE_UNKNOW = 2;
    public static final int OPERATORS_TYPE_MOBILE = 1;
    public static final int OPERATORS_TYPE_TELECOMU = 3;
    public static final int OPERATORS_TYPE_UNICOM = 2;
    public static final int OPERATORS_TYPE_UNKONW = 0;
    public static final int TypeCacheLocation = 65;
    public static final int TypeCriteriaException = 62;
    public static final int TypeGpsLocation = 61;
    public static final int TypeNetWorkException = 63;
    public static final int TypeNetWorkLocation = 161;
    public static final int TypeNone = 0;
    public static final int TypeOffLineLocation = 66;
    public static final int TypeOffLineLocationFail = 67;
    public static final int TypeOffLineLocationNetworkFail = 68;
    public static final int TypeServerCheckKeyError = 505;
    public static final int TypeServerDecryptError = 162;
    public static final int TypeServerError = 167;
    public static final int USER_INDDOR_TRUE = 1;
    public static final int USER_INDOOR_FALSE = 0;
    public static final int USER_INDOOR_UNKNOW = -1;

    /* renamed from: A */
    private int f1106A;

    /* renamed from: B */
    private String f1107B;

    /* renamed from: C */
    private int f1108C;

    /* renamed from: D */
    private String f1109D;

    /* renamed from: E */
    private int f1110E;

    /* renamed from: F */
    private int f1111F;

    /* renamed from: G */
    private int f1112G;

    /* renamed from: H */
    private int f1113H;

    /* renamed from: I */
    private String f1114I;

    /* renamed from: J */
    private String f1115J;

    /* renamed from: K */
    private String f1116K;

    /* renamed from: L */
    private List<Poi> f1117L;

    /* renamed from: M */
    private String f1118M;

    /* renamed from: N */
    private String f1119N;

    /* renamed from: O */
    private String f1120O;

    /* renamed from: P */
    private Bundle f1121P;

    /* renamed from: Q */
    private int f1122Q;

    /* renamed from: R */
    private int f1123R;

    /* renamed from: S */
    private long f1124S;

    /* renamed from: T */
    private String f1125T;

    /* renamed from: U */
    private double f1126U;

    /* renamed from: V */
    private double f1127V;

    /* renamed from: a */
    private int f1128a;

    /* renamed from: b */
    private String f1129b;

    /* renamed from: c */
    private double f1130c;

    /* renamed from: d */
    private double f1131d;

    /* renamed from: e */
    private boolean f1132e;

    /* renamed from: f */
    private double f1133f;

    /* renamed from: g */
    private boolean f1134g;

    /* renamed from: h */
    private float f1135h;

    /* renamed from: i */
    private boolean f1136i;

    /* renamed from: j */
    private float f1137j;

    /* renamed from: k */
    private boolean f1138k;

    /* renamed from: l */
    private int f1139l;

    /* renamed from: m */
    private float f1140m;

    /* renamed from: n */
    private String f1141n;

    /* renamed from: o */
    private boolean f1142o;

    /* renamed from: p */
    private String f1143p;

    /* renamed from: q */
    private String f1144q;

    /* renamed from: r */
    private String f1145r;

    /* renamed from: s */
    private String f1146s;

    /* renamed from: t */
    private boolean f1147t;

    /* renamed from: u */
    private Address f1148u;

    /* renamed from: v */
    private String f1149v;

    /* renamed from: w */
    private String f1150w;

    /* renamed from: x */
    private String f1151x;

    /* renamed from: y */
    private boolean f1152y;

    /* renamed from: z */
    private int f1153z;

    public BDLocation() {
        this.f1128a = 0;
        this.f1129b = null;
        this.f1130c = Double.MIN_VALUE;
        this.f1131d = Double.MIN_VALUE;
        this.f1132e = false;
        this.f1133f = Double.MIN_VALUE;
        this.f1134g = false;
        this.f1135h = 0.0f;
        this.f1136i = false;
        this.f1137j = 0.0f;
        this.f1138k = false;
        this.f1139l = -1;
        this.f1140m = -1.0f;
        this.f1141n = null;
        this.f1142o = false;
        this.f1143p = null;
        this.f1144q = null;
        this.f1145r = null;
        this.f1146s = null;
        this.f1147t = false;
        this.f1148u = new Address.Builder().build();
        this.f1149v = null;
        this.f1150w = null;
        this.f1151x = null;
        this.f1152y = false;
        this.f1153z = 0;
        this.f1106A = 1;
        this.f1107B = null;
        this.f1109D = "";
        this.f1110E = -1;
        this.f1111F = 0;
        this.f1112G = 2;
        this.f1113H = 0;
        this.f1114I = null;
        this.f1115J = null;
        this.f1116K = null;
        this.f1117L = null;
        this.f1118M = null;
        this.f1119N = null;
        this.f1120O = null;
        this.f1121P = new Bundle();
        this.f1122Q = 0;
        this.f1123R = 0;
        this.f1124S = 0;
        this.f1125T = null;
        this.f1126U = Double.MIN_VALUE;
        this.f1127V = Double.MIN_VALUE;
    }

    private BDLocation(Parcel parcel) {
        this.f1128a = 0;
        this.f1129b = null;
        this.f1130c = Double.MIN_VALUE;
        this.f1131d = Double.MIN_VALUE;
        this.f1132e = false;
        this.f1133f = Double.MIN_VALUE;
        this.f1134g = false;
        this.f1135h = 0.0f;
        this.f1136i = false;
        this.f1137j = 0.0f;
        this.f1138k = false;
        this.f1139l = -1;
        this.f1140m = -1.0f;
        this.f1141n = null;
        this.f1142o = false;
        this.f1143p = null;
        this.f1144q = null;
        this.f1145r = null;
        this.f1146s = null;
        this.f1147t = false;
        this.f1148u = new Address.Builder().build();
        this.f1149v = null;
        this.f1150w = null;
        this.f1151x = null;
        this.f1152y = false;
        this.f1153z = 0;
        this.f1106A = 1;
        this.f1107B = null;
        this.f1109D = "";
        this.f1110E = -1;
        this.f1111F = 0;
        this.f1112G = 2;
        this.f1113H = 0;
        this.f1114I = null;
        this.f1115J = null;
        this.f1116K = null;
        this.f1117L = null;
        this.f1118M = null;
        this.f1119N = null;
        this.f1120O = null;
        this.f1121P = new Bundle();
        this.f1122Q = 0;
        this.f1123R = 0;
        this.f1124S = 0;
        this.f1125T = null;
        this.f1126U = Double.MIN_VALUE;
        this.f1127V = Double.MIN_VALUE;
        this.f1128a = parcel.readInt();
        this.f1129b = parcel.readString();
        this.f1130c = parcel.readDouble();
        this.f1131d = parcel.readDouble();
        this.f1133f = parcel.readDouble();
        this.f1135h = parcel.readFloat();
        this.f1137j = parcel.readFloat();
        this.f1139l = parcel.readInt();
        this.f1140m = parcel.readFloat();
        this.f1149v = parcel.readString();
        this.f1153z = parcel.readInt();
        this.f1150w = parcel.readString();
        this.f1151x = parcel.readString();
        this.f1107B = parcel.readString();
        String readString = parcel.readString();
        String readString2 = parcel.readString();
        String readString3 = parcel.readString();
        String readString4 = parcel.readString();
        String readString5 = parcel.readString();
        String readString6 = parcel.readString();
        parcel.readString();
        String readString7 = parcel.readString();
        String readString8 = parcel.readString();
        this.f1148u = new Address.Builder().country(readString7).countryCode(readString8).province(readString).city(readString2).cityCode(readString6).district(readString3).street(readString4).streetNumber(readString5).adcode(parcel.readString()).build();
        boolean[] zArr = new boolean[7];
        this.f1108C = parcel.readInt();
        this.f1109D = parcel.readString();
        this.f1144q = parcel.readString();
        this.f1145r = parcel.readString();
        this.f1146s = parcel.readString();
        this.f1106A = parcel.readInt();
        this.f1118M = parcel.readString();
        this.f1110E = parcel.readInt();
        this.f1111F = parcel.readInt();
        this.f1112G = parcel.readInt();
        this.f1113H = parcel.readInt();
        this.f1114I = parcel.readString();
        this.f1115J = parcel.readString();
        this.f1116K = parcel.readString();
        this.f1122Q = parcel.readInt();
        this.f1119N = parcel.readString();
        this.f1123R = parcel.readInt();
        this.f1120O = parcel.readString();
        this.f1125T = parcel.readString();
        this.f1124S = parcel.readLong();
        this.f1126U = parcel.readDouble();
        this.f1127V = parcel.readDouble();
        try {
            parcel.readBooleanArray(zArr);
            this.f1132e = zArr[0];
            this.f1134g = zArr[1];
            this.f1136i = zArr[2];
            this.f1138k = zArr[3];
            this.f1142o = zArr[4];
            this.f1147t = zArr[5];
            this.f1152y = zArr[6];
        } catch (Exception unused) {
        }
        ArrayList arrayList = new ArrayList();
        parcel.readList(arrayList, Poi.class.getClassLoader());
        if (arrayList.size() == 0) {
            this.f1117L = null;
        } else {
            this.f1117L = arrayList;
        }
        try {
            this.f1121P = parcel.readBundle();
        } catch (Exception e) {
            e.printStackTrace();
            this.f1121P = new Bundle();
        }
    }

    /* synthetic */ BDLocation(Parcel parcel, C0722a aVar) {
        this(parcel);
    }

    public BDLocation(BDLocation bDLocation) {
        this.f1128a = 0;
        ArrayList arrayList = null;
        this.f1129b = null;
        this.f1130c = Double.MIN_VALUE;
        this.f1131d = Double.MIN_VALUE;
        this.f1132e = false;
        this.f1133f = Double.MIN_VALUE;
        this.f1134g = false;
        this.f1135h = 0.0f;
        this.f1136i = false;
        this.f1137j = 0.0f;
        this.f1138k = false;
        this.f1139l = -1;
        this.f1140m = -1.0f;
        this.f1141n = null;
        this.f1142o = false;
        this.f1143p = null;
        this.f1144q = null;
        this.f1145r = null;
        this.f1146s = null;
        this.f1147t = false;
        this.f1148u = new Address.Builder().build();
        this.f1149v = null;
        this.f1150w = null;
        this.f1151x = null;
        this.f1152y = false;
        this.f1153z = 0;
        this.f1106A = 1;
        this.f1107B = null;
        this.f1109D = "";
        this.f1110E = -1;
        this.f1111F = 0;
        this.f1112G = 2;
        this.f1113H = 0;
        this.f1114I = null;
        this.f1115J = null;
        this.f1116K = null;
        this.f1117L = null;
        this.f1118M = null;
        this.f1119N = null;
        this.f1120O = null;
        this.f1121P = new Bundle();
        this.f1122Q = 0;
        this.f1123R = 0;
        this.f1124S = 0;
        this.f1125T = null;
        this.f1126U = Double.MIN_VALUE;
        this.f1127V = Double.MIN_VALUE;
        this.f1128a = bDLocation.f1128a;
        this.f1129b = bDLocation.f1129b;
        this.f1130c = bDLocation.f1130c;
        this.f1131d = bDLocation.f1131d;
        this.f1132e = bDLocation.f1132e;
        this.f1133f = bDLocation.f1133f;
        this.f1134g = bDLocation.f1134g;
        this.f1135h = bDLocation.f1135h;
        this.f1136i = bDLocation.f1136i;
        this.f1137j = bDLocation.f1137j;
        this.f1138k = bDLocation.f1138k;
        this.f1139l = bDLocation.f1139l;
        this.f1140m = bDLocation.f1140m;
        this.f1141n = bDLocation.f1141n;
        this.f1142o = bDLocation.f1142o;
        this.f1143p = bDLocation.f1143p;
        this.f1147t = bDLocation.f1147t;
        this.f1148u = new Address.Builder().country(bDLocation.f1148u.country).countryCode(bDLocation.f1148u.countryCode).province(bDLocation.f1148u.province).city(bDLocation.f1148u.city).cityCode(bDLocation.f1148u.cityCode).district(bDLocation.f1148u.district).street(bDLocation.f1148u.street).streetNumber(bDLocation.f1148u.streetNumber).adcode(bDLocation.f1148u.adcode).build();
        this.f1149v = bDLocation.f1149v;
        this.f1150w = bDLocation.f1150w;
        this.f1151x = bDLocation.f1151x;
        this.f1106A = bDLocation.f1106A;
        this.f1153z = bDLocation.f1153z;
        this.f1152y = bDLocation.f1152y;
        this.f1107B = bDLocation.f1107B;
        this.f1108C = bDLocation.f1108C;
        this.f1109D = bDLocation.f1109D;
        this.f1144q = bDLocation.f1144q;
        this.f1145r = bDLocation.f1145r;
        this.f1146s = bDLocation.f1146s;
        this.f1110E = bDLocation.f1110E;
        this.f1111F = bDLocation.f1111F;
        this.f1112G = bDLocation.f1111F;
        this.f1113H = bDLocation.f1113H;
        this.f1114I = bDLocation.f1114I;
        this.f1115J = bDLocation.f1115J;
        this.f1116K = bDLocation.f1116K;
        this.f1122Q = bDLocation.f1122Q;
        this.f1120O = bDLocation.f1120O;
        this.f1126U = bDLocation.f1126U;
        this.f1127V = bDLocation.f1127V;
        this.f1124S = bDLocation.f1124S;
        this.f1119N = bDLocation.f1119N;
        if (bDLocation.f1117L != null) {
            arrayList = new ArrayList();
            for (int i = 0; i < bDLocation.f1117L.size(); i++) {
                Poi poi = bDLocation.f1117L.get(i);
                arrayList.add(new Poi(poi.getId(), poi.getName(), poi.getRank()));
            }
        }
        this.f1117L = arrayList;
        this.f1118M = bDLocation.f1118M;
        this.f1121P = bDLocation.f1121P;
        this.f1123R = bDLocation.f1123R;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:113:0x02ef, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x02f0, code lost:
        r3 = r0;
        r11 = r8;
        r4 = null;
        r8 = null;
        r9 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:120:0x02ff, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x0300, code lost:
        r3 = r0;
        r11 = r8;
        r4 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x030d, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x030e, code lost:
        r3 = r0;
        r11 = r8;
        r4 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:137:0x031a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:138:0x031b, code lost:
        r3 = r0;
        r14 = r6;
        r11 = r8;
        r4 = r10;
        r8 = null;
        r10 = null;
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x033f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x0340, code lost:
        r3 = r0;
        r14 = r6;
        r4 = r10;
        r15 = r11;
        r10 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:0x0350, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x0351, code lost:
        r3 = r0;
        r4 = r10;
        r15 = r11;
        r10 = null;
        r11 = r8;
        r8 = r14;
        r14 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x0365, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:172:0x0366, code lost:
        r3 = r0;
        r4 = r10;
        r10 = r15;
        r15 = r11;
        r11 = r8;
        r8 = r14;
        r14 = r16;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:175:0x0379, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:176:0x037a, code lost:
        r3 = r0;
        r4 = null;
        r8 = null;
        r9 = null;
        r10 = null;
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x0523, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0529, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:257:?, code lost:
        r0.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:297:0x05ff, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:298:0x0600, code lost:
        r3 = r0;
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:299:0x0603, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:300:0x0604, code lost:
        r0.printStackTrace();
        r2 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:301:0x060a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:302:0x060b, code lost:
        r2 = false;
        r3 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:303:0x060d, code lost:
        r3.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:304:0x0610, code lost:
        r1.f1128a = r2 ? 1 : 0;
        r1.f1142o = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:307:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x022e, code lost:
        r4 = null;
        r6 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x0122 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0137 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:267:0x055a */
    /* JADX WARNING: Removed duplicated region for block: B:182:0x038a A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x03cc A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x03e7 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:196:0x0406 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0128 A[Catch:{ Exception -> 0x0137, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0421 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:206:0x043c A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0134 A[Catch:{ Exception -> 0x0137, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:211:0x0457 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:220:0x0482  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x013b A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x0536 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:261:0x0540 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:265:0x054a A[Catch:{ Exception -> 0x055a, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x0556 A[Catch:{ Exception -> 0x055a, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:270:0x055e A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x0564 A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x056f A[Catch:{ Exception -> 0x05ff, Error -> 0x0603 }] */
    /* JADX WARNING: Removed duplicated region for block: B:277:0x057f  */
    /* JADX WARNING: Removed duplicated region for block: B:299:0x0603 A[ExcHandler: Error (r0v1 'e' java.lang.Error A[CUSTOM_DECLARE]), Splitter:B:4:0x008e] */
    /* JADX WARNING: Removed duplicated region for block: B:311:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0122=Splitter:B:16:0x0122, B:179:0x0383=Splitter:B:179:0x0383, B:262:0x0544=Splitter:B:262:0x0544, B:21:0x0137=Splitter:B:21:0x0137, B:267:0x055a=Splitter:B:267:0x055a} */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:179:0x0383=Splitter:B:179:0x0383, B:21:0x0137=Splitter:B:21:0x0137, B:267:0x055a=Splitter:B:267:0x055a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public BDLocation(java.lang.String r19) {
        /*
            r18 = this;
            r1 = r18
            r2 = r19
            java.lang.String r3 = "addr"
            r18.<init>()
            r4 = 0
            r1.f1128a = r4
            r5 = 0
            r1.f1129b = r5
            r6 = 1
            r1.f1130c = r6
            r1.f1131d = r6
            r1.f1132e = r4
            r1.f1133f = r6
            r1.f1134g = r4
            r8 = 0
            r1.f1135h = r8
            r1.f1136i = r4
            r1.f1137j = r8
            r1.f1138k = r4
            r8 = -1
            r1.f1139l = r8
            r8 = -1082130432(0xffffffffbf800000, float:-1.0)
            r1.f1140m = r8
            r1.f1141n = r5
            r1.f1142o = r4
            r1.f1143p = r5
            r1.f1144q = r5
            r1.f1145r = r5
            r1.f1146s = r5
            r1.f1147t = r4
            com.baidu.location.Address$Builder r8 = new com.baidu.location.Address$Builder
            r8.<init>()
            com.baidu.location.Address r8 = r8.build()
            r1.f1148u = r8
            r1.f1149v = r5
            r1.f1150w = r5
            r1.f1151x = r5
            r1.f1152y = r4
            r1.f1153z = r4
            r8 = 1
            r1.f1106A = r8
            r1.f1107B = r5
            java.lang.String r9 = ""
            r1.f1109D = r9
            r10 = -1
            r1.f1110E = r10
            r1.f1111F = r4
            r10 = 2
            r1.f1112G = r10
            r1.f1113H = r4
            r1.f1114I = r5
            r1.f1115J = r5
            r1.f1116K = r5
            r1.f1117L = r5
            r1.f1118M = r5
            r1.f1119N = r5
            r1.f1120O = r5
            android.os.Bundle r11 = new android.os.Bundle
            r11.<init>()
            r1.f1121P = r11
            r1.f1122Q = r4
            r1.f1123R = r4
            r11 = 0
            r1.f1124S = r11
            r1.f1125T = r5
            r1.f1126U = r6
            r1.f1127V = r6
            if (r2 == 0) goto L_0x0614
            boolean r11 = r2.equals(r9)
            if (r11 == 0) goto L_0x008e
            goto L_0x0614
        L_0x008e:
            org.json.JSONObject r11 = new org.json.JSONObject     // Catch:{ Exception -> 0x060a, Error -> 0x0603 }
            r11.<init>(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r2 = "result"
            org.json.JSONObject r2 = r11.getJSONObject(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r12 = "error"
            java.lang.String r12 = r2.getString(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            int r12 = java.lang.Integer.parseInt(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLocType(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r13 = "time"
            java.lang.String r2 = r2.getString(r13)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setTime(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r2 = 61
            java.lang.String r13 = "gcj02"
            java.lang.String r14 = "radius"
            java.lang.String r15 = "point"
            java.lang.String r6 = "content"
            java.lang.String r7 = "in_cn"
            java.lang.String r5 = "x"
            java.lang.String r10 = "y"
            if (r12 != r2) goto L_0x0147
            org.json.JSONObject r2 = r11.getJSONObject(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            org.json.JSONObject r3 = r2.getJSONObject(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r6 = r3.getString(r10)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            double r9 = java.lang.Double.parseDouble(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLatitude(r9)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r3.getString(r5)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            double r5 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLongitude(r5)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r2.getString(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            float r3 = java.lang.Float.parseFloat(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setRadius(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = "s"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            float r3 = java.lang.Float.parseFloat(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setSpeed(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = "d"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            float r3 = java.lang.Float.parseFloat(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setDirection(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = "n"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setSatelliteNumber(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = "h"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0122
            java.lang.String r3 = "h"
            double r5 = r2.getDouble(r3)     // Catch:{ Exception -> 0x0122, Error -> 0x0603 }
            r1.setAltitude(r5)     // Catch:{ Exception -> 0x0122, Error -> 0x0603 }
        L_0x0122:
            boolean r3 = r2.has(r7)     // Catch:{ Exception -> 0x0137, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0134
            java.lang.String r2 = r2.getString(r7)     // Catch:{ Exception -> 0x0137, Error -> 0x0603 }
            int r2 = java.lang.Integer.parseInt(r2)     // Catch:{ Exception -> 0x0137, Error -> 0x0603 }
            r1.setLocationWhere(r2)     // Catch:{ Exception -> 0x0137, Error -> 0x0603 }
            goto L_0x0137
        L_0x0134:
            r1.setLocationWhere(r8)     // Catch:{ Exception -> 0x0137, Error -> 0x0603 }
        L_0x0137:
            int r2 = r1.f1106A     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r2 != 0) goto L_0x0142
            java.lang.String r2 = "wgs84"
            r1.setCoorType(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0614
        L_0x0142:
            r1.setCoorType(r13)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0614
        L_0x0147:
            r2 = 161(0xa1, float:2.26E-43)
            if (r12 != r2) goto L_0x05b0
            org.json.JSONObject r2 = r11.getJSONObject(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            org.json.JSONObject r6 = r2.getJSONObject(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r11 = r6.getString(r10)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            double r11 = java.lang.Double.parseDouble(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLatitude(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r6 = r6.getString(r5)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            double r11 = java.lang.Double.parseDouble(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLongitude(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r6 = r2.getString(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            float r6 = java.lang.Float.parseFloat(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setRadius(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r6 = "sema"
            boolean r6 = r2.has(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r6 == 0) goto L_0x021e
            java.lang.String r6 = "sema"
            org.json.JSONObject r6 = r2.getJSONObject(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r11 = "aptag"
            boolean r11 = r6.has(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r11 == 0) goto L_0x019b
            java.lang.String r11 = "aptag"
            java.lang.String r11 = r6.getString(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r12 = android.text.TextUtils.isEmpty(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r12 != 0) goto L_0x0199
            r1.f1144q = r11     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x019b
        L_0x0199:
            r1.f1144q = r9     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x019b:
            java.lang.String r11 = "aptagd"
            boolean r11 = r6.has(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r11 == 0) goto L_0x01ed
            java.lang.String r11 = "aptagd"
            org.json.JSONObject r11 = r6.getJSONObject(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r12 = "pois"
            org.json.JSONArray r11 = r11.getJSONArray(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.util.ArrayList r12 = new java.util.ArrayList     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r12.<init>()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r14 = 0
        L_0x01b5:
            int r15 = r11.length()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r14 >= r15) goto L_0x01e6
            org.json.JSONObject r15 = r11.getJSONObject(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r8 = "pname"
            java.lang.String r8 = r15.getString(r8)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r4 = "pid"
            java.lang.String r4 = r15.getString(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r16 = r9
            java.lang.String r9 = "pr"
            r19 = r10
            double r9 = r15.getDouble(r9)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Poi r15 = new com.baidu.location.Poi     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r15.<init>(r4, r8, r9)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r12.add(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            int r14 = r14 + 1
            r10 = r19
            r9 = r16
            r4 = 0
            r8 = 1
            goto L_0x01b5
        L_0x01e6:
            r16 = r9
            r19 = r10
            r1.f1117L = r12     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x01f1
        L_0x01ed:
            r16 = r9
            r19 = r10
        L_0x01f1:
            java.lang.String r4 = "poiregion"
            boolean r4 = r6.has(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r4 == 0) goto L_0x0207
            java.lang.String r4 = "poiregion"
            java.lang.String r4 = r6.getString(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r8 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r8 != 0) goto L_0x0207
            r1.f1145r = r4     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x0207:
            java.lang.String r4 = "regular"
            boolean r4 = r6.has(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r4 == 0) goto L_0x0222
            java.lang.String r4 = "regular"
            java.lang.String r4 = r6.getString(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r6 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r6 != 0) goto L_0x0222
            r1.f1146s = r4     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0222
        L_0x021e:
            r16 = r9
            r19 = r10
        L_0x0222:
            boolean r4 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r4 == 0) goto L_0x03bd
            org.json.JSONObject r4 = r2.getJSONObject(r3)     // Catch:{ Exception -> 0x022e, Error -> 0x0603 }
            r6 = 1
            goto L_0x0230
        L_0x022e:
            r4 = 0
            r6 = 0
        L_0x0230:
            if (r4 == 0) goto L_0x02d5
            java.lang.String r3 = "city"
            boolean r3 = r4.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0241
            java.lang.String r3 = "city"
            java.lang.String r9 = r4.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0243
        L_0x0241:
            r9 = r16
        L_0x0243:
            java.lang.String r3 = "city_code"
            boolean r3 = r4.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0252
            java.lang.String r3 = "city_code"
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0254
        L_0x0252:
            r3 = r16
        L_0x0254:
            java.lang.String r8 = "country"
            boolean r8 = r4.has(r8)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r8 == 0) goto L_0x0263
            java.lang.String r8 = "country"
            java.lang.String r8 = r4.getString(r8)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0265
        L_0x0263:
            r8 = r16
        L_0x0265:
            java.lang.String r10 = "country_code"
            boolean r10 = r4.has(r10)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r10 == 0) goto L_0x0274
            java.lang.String r10 = "country_code"
            java.lang.String r10 = r4.getString(r10)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0276
        L_0x0274:
            r10 = r16
        L_0x0276:
            java.lang.String r11 = "province"
            boolean r11 = r4.has(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r11 == 0) goto L_0x0285
            java.lang.String r11 = "province"
            java.lang.String r11 = r4.getString(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0287
        L_0x0285:
            r11 = r16
        L_0x0287:
            java.lang.String r12 = "district"
            boolean r12 = r4.has(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r12 == 0) goto L_0x0296
            java.lang.String r12 = "district"
            java.lang.String r12 = r4.getString(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0298
        L_0x0296:
            r12 = r16
        L_0x0298:
            java.lang.String r14 = "street"
            boolean r14 = r4.has(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r14 == 0) goto L_0x02a7
            java.lang.String r14 = "street"
            java.lang.String r14 = r4.getString(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x02a9
        L_0x02a7:
            r14 = r16
        L_0x02a9:
            java.lang.String r15 = "street_number"
            boolean r15 = r4.has(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r15 == 0) goto L_0x02ba
            java.lang.String r15 = "street_number"
            java.lang.String r15 = r4.getString(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r17 = r3
            goto L_0x02be
        L_0x02ba:
            r17 = r3
            r15 = r16
        L_0x02be:
            java.lang.String r3 = "adcode"
            boolean r3 = r4.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x02ce
            java.lang.String r3 = "adcode"
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r4 = r12
            goto L_0x02d1
        L_0x02ce:
            r4 = r12
            r3 = r16
        L_0x02d1:
            r12 = r17
            goto L_0x0388
        L_0x02d5:
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x0379, Error -> 0x0603 }
            java.lang.String r4 = ","
            java.lang.String[] r3 = r3.split(r4)     // Catch:{ Exception -> 0x0379, Error -> 0x0603 }
            int r4 = r3.length     // Catch:{ Exception -> 0x0379, Error -> 0x0603 }
            if (r4 <= 0) goto L_0x02e7
            r6 = 0
            r8 = r3[r6]     // Catch:{ Exception -> 0x0379, Error -> 0x0603 }
            r6 = 1
            goto L_0x02e9
        L_0x02e7:
            r6 = 1
            r8 = 0
        L_0x02e9:
            if (r4 <= r6) goto L_0x02f8
            r9 = r3[r6]     // Catch:{ Exception -> 0x02ef, Error -> 0x0603 }
            r6 = 2
            goto L_0x02fa
        L_0x02ef:
            r0 = move-exception
            r3 = r0
            r11 = r8
            r4 = 0
            r8 = 0
            r9 = 0
        L_0x02f5:
            r10 = 0
            goto L_0x0380
        L_0x02f8:
            r6 = 2
            r9 = 0
        L_0x02fa:
            if (r4 <= r6) goto L_0x0305
            r10 = r3[r6]     // Catch:{ Exception -> 0x02ff, Error -> 0x0603 }
            goto L_0x0306
        L_0x02ff:
            r0 = move-exception
            r3 = r0
            r11 = r8
            r4 = 0
        L_0x0303:
            r8 = 0
            goto L_0x02f5
        L_0x0305:
            r10 = 0
        L_0x0306:
            r6 = 3
            if (r4 <= r6) goto L_0x0312
            r6 = 3
            r6 = r3[r6]     // Catch:{ Exception -> 0x030d, Error -> 0x0603 }
            goto L_0x0313
        L_0x030d:
            r0 = move-exception
            r3 = r0
            r11 = r8
            r4 = r10
            goto L_0x0303
        L_0x0312:
            r6 = 0
        L_0x0313:
            r11 = 4
            if (r4 <= r11) goto L_0x0324
            r11 = 4
            r11 = r3[r11]     // Catch:{ Exception -> 0x031a, Error -> 0x0603 }
            goto L_0x0325
        L_0x031a:
            r0 = move-exception
            r3 = r0
            r14 = r6
            r11 = r8
            r4 = r10
            r8 = 0
            r10 = 0
            r12 = 0
            goto L_0x0382
        L_0x0324:
            r11 = 0
        L_0x0325:
            r12 = 5
            if (r4 <= r12) goto L_0x0337
            r12 = 5
            r12 = r3[r12]     // Catch:{ Exception -> 0x032c, Error -> 0x0603 }
            goto L_0x0338
        L_0x032c:
            r0 = move-exception
            r3 = r0
            r14 = r6
            r4 = r10
            r15 = r11
            r10 = 0
            r12 = 0
        L_0x0333:
            r11 = r8
            r8 = 0
            goto L_0x0383
        L_0x0337:
            r12 = 0
        L_0x0338:
            r14 = 6
            if (r4 <= r14) goto L_0x0346
            r14 = 6
            r14 = r3[r14]     // Catch:{ Exception -> 0x033f, Error -> 0x0603 }
            goto L_0x0347
        L_0x033f:
            r0 = move-exception
            r3 = r0
            r14 = r6
            r4 = r10
            r15 = r11
            r10 = 0
            goto L_0x0333
        L_0x0346:
            r14 = 0
        L_0x0347:
            r15 = 7
            if (r4 <= r15) goto L_0x0359
            r15 = 7
            r15 = r3[r15]     // Catch:{ Exception -> 0x0350, Error -> 0x0603 }
            r16 = r6
            goto L_0x035c
        L_0x0350:
            r0 = move-exception
            r3 = r0
            r4 = r10
            r15 = r11
            r10 = 0
            r11 = r8
            r8 = r14
            r14 = r6
            goto L_0x0383
        L_0x0359:
            r16 = r6
            r15 = 0
        L_0x035c:
            r6 = 8
            if (r4 <= r6) goto L_0x036f
            r4 = 8
            r3 = r3[r4]     // Catch:{ Exception -> 0x0365, Error -> 0x0603 }
            goto L_0x0370
        L_0x0365:
            r0 = move-exception
            r3 = r0
            r4 = r10
            r10 = r15
            r15 = r11
            r11 = r8
            r8 = r14
            r14 = r16
            goto L_0x0383
        L_0x036f:
            r3 = 0
        L_0x0370:
            r4 = r10
            r10 = r15
            r6 = 1
            r15 = r11
            r11 = r8
            r8 = r14
            r14 = r16
            goto L_0x0388
        L_0x0379:
            r0 = move-exception
            r3 = r0
            r4 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
        L_0x0380:
            r12 = 0
            r14 = 0
        L_0x0382:
            r15 = 0
        L_0x0383:
            r3.printStackTrace()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r3 = 0
            r6 = 0
        L_0x0388:
            if (r6 == 0) goto L_0x03c4
            com.baidu.location.Address$Builder r6 = new com.baidu.location.Address$Builder     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r6.<init>()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r6 = r6.country(r8)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r6 = r6.countryCode(r10)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r6 = r6.province(r11)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r6 = r6.city(r9)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r6 = r6.cityCode(r12)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r4 = r6.district(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r4 = r4.street(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r4 = r4.streetNumber(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address$Builder r3 = r4.adcode(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            com.baidu.location.Address r3 = r3.build()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1148u = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r3 = 1
            r1.f1142o = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x03c4
        L_0x03bd:
            r3 = 0
            r1.f1142o = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r3 = 0
            r1.setAddrStr(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x03c4:
            java.lang.String r3 = "floor"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x03df
            java.lang.String r3 = "floor"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1149v = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r1.f1149v     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x03df
            r3 = 0
            r1.f1149v = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x03df:
            java.lang.String r3 = "indoor"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x03fe
            java.lang.String r3 = "indoor"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r4 != 0) goto L_0x03fe
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setUserIndoorState(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x03fe:
            java.lang.String r3 = "loctp"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0419
            java.lang.String r3 = "loctp"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1107B = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r1.f1107B     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0419
            r3 = 0
            r1.f1107B = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x0419:
            java.lang.String r3 = "bldgid"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0434
            java.lang.String r3 = "bldgid"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1150w = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r1.f1150w     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0434
            r3 = 0
            r1.f1150w = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x0434:
            java.lang.String r3 = "bldg"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x044f
            java.lang.String r3 = "bldg"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1151x = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r1.f1151x     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r3 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x044f
            r3 = 0
            r1.f1151x = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x044f:
            java.lang.String r3 = "ibav"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x047a
            java.lang.String r3 = "ibav"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r4 = android.text.TextUtils.isEmpty(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r4 == 0) goto L_0x0467
        L_0x0463:
            r4 = 0
            r1.f1153z = r4     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x047a
        L_0x0467:
            java.lang.String r4 = "0"
            boolean r4 = r3.equals(r4)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r4 == 0) goto L_0x0470
            goto L_0x0463
        L_0x0470:
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1153z = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x047a:
            java.lang.String r3 = "indoorflags"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x052e
            java.lang.String r3 = "indoorflags"
            org.json.JSONObject r3 = r2.getJSONObject(r3)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            java.lang.String r4 = "area"
            boolean r4 = r3.has(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 == 0) goto L_0x04ab
            java.lang.String r4 = "area"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            int r4 = r4.intValue()     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 != 0) goto L_0x04a5
            r6 = 2
            r1.setIndoorLocationSurpport(r6)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            goto L_0x04ab
        L_0x04a5:
            r6 = 1
            if (r4 != r6) goto L_0x04ab
            r1.setIndoorLocationSurpport(r6)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
        L_0x04ab:
            java.lang.String r4 = "support"
            boolean r4 = r3.has(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 == 0) goto L_0x04c4
            java.lang.String r4 = "support"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            int r4 = r4.intValue()     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            r1.setIndoorLocationSource(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
        L_0x04c4:
            java.lang.String r4 = "inbldg"
            boolean r4 = r3.has(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 == 0) goto L_0x04d4
            java.lang.String r4 = "inbldg"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            r1.f1114I = r4     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
        L_0x04d4:
            java.lang.String r4 = "inbldgid"
            boolean r4 = r3.has(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 == 0) goto L_0x04e4
            java.lang.String r4 = "inbldgid"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            r1.f1115J = r4     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
        L_0x04e4:
            java.lang.String r4 = "polygon"
            boolean r4 = r3.has(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 == 0) goto L_0x04f5
            java.lang.String r4 = "polygon"
            java.lang.String r4 = r3.getString(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            r1.setIndoorSurpportPolygon(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
        L_0x04f5:
            java.lang.String r4 = "ret_fields"
            boolean r4 = r3.has(r4)     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            if (r4 == 0) goto L_0x052e
            java.lang.String r4 = "ret_fields"
            java.lang.String r3 = r3.getString(r4)     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            java.lang.String r4 = "\\|"
            java.lang.String[] r3 = r3.split(r4)     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            int r4 = r3.length     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            r6 = 0
        L_0x050b:
            if (r6 >= r4) goto L_0x052e
            r8 = r3[r6]     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            java.lang.String r9 = "="
            java.lang.String[] r8 = r8.split(r9)     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            r9 = 0
            r10 = r8[r9]     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            r9 = 1
            r8 = r8[r9]     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            android.os.Bundle r9 = r1.f1121P     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            r9.putString(r10, r8)     // Catch:{ Exception -> 0x0523, Error -> 0x0603 }
            int r6 = r6 + 1
            goto L_0x050b
        L_0x0523:
            r0 = move-exception
            r3 = r0
            r3.printStackTrace()     // Catch:{ Exception -> 0x0529, Error -> 0x0603 }
            goto L_0x052e
        L_0x0529:
            r0 = move-exception
            r3 = r0
            r3.printStackTrace()     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x052e:
            java.lang.String r3 = "gpscs"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0540
            java.lang.String r3 = "gpscs"
            int r3 = r2.getInt(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setGpsCheckStatus(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0544
        L_0x0540:
            r3 = 0
            r1.setGpsCheckStatus(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x0544:
            boolean r3 = r2.has(r7)     // Catch:{ Exception -> 0x055a, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0556
            java.lang.String r3 = r2.getString(r7)     // Catch:{ Exception -> 0x055a, Error -> 0x0603 }
            int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ Exception -> 0x055a, Error -> 0x0603 }
            r1.setLocationWhere(r3)     // Catch:{ Exception -> 0x055a, Error -> 0x0603 }
            goto L_0x055a
        L_0x0556:
            r3 = 1
            r1.setLocationWhere(r3)     // Catch:{ Exception -> 0x055a, Error -> 0x0603 }
        L_0x055a:
            int r3 = r1.f1106A     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 != 0) goto L_0x0564
            java.lang.String r3 = "wgs84"
            r1.setCoorType(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0567
        L_0x0564:
            r1.setCoorType(r13)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x0567:
            java.lang.String r3 = "navi"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0577
            java.lang.String r3 = "navi"
            java.lang.String r3 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1125T = r3     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
        L_0x0577:
            java.lang.String r3 = "nrl_point"
            boolean r3 = r2.has(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            if (r3 == 0) goto L_0x0614
            java.lang.String r3 = "nrl_point"
            org.json.JSONObject r2 = r2.getJSONObject(r3)     // Catch:{ all -> 0x05a9 }
            boolean r3 = r2.has(r5)     // Catch:{ all -> 0x05a9 }
            if (r3 == 0) goto L_0x0614
            r3 = r19
            boolean r4 = r2.has(r3)     // Catch:{ all -> 0x05a9 }
            if (r4 == 0) goto L_0x0614
            java.lang.String r3 = r2.getString(r3)     // Catch:{ all -> 0x05a9 }
            double r3 = java.lang.Double.parseDouble(r3)     // Catch:{ all -> 0x05a9 }
            r1.f1126U = r3     // Catch:{ all -> 0x05a9 }
            java.lang.String r2 = r2.getString(r5)     // Catch:{ all -> 0x05a9 }
            double r2 = java.lang.Double.parseDouble(r2)     // Catch:{ all -> 0x05a9 }
            r1.f1127V = r2     // Catch:{ all -> 0x05a9 }
            goto L_0x0614
        L_0x05a9:
            r2 = 1
            r1.f1126U = r2     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.f1127V = r2     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0614
        L_0x05b0:
            r3 = r10
            r2 = 66
            if (r12 == r2) goto L_0x05c3
            r2 = 68
            if (r12 != r2) goto L_0x05ba
            goto L_0x05c3
        L_0x05ba:
            r2 = 167(0xa7, float:2.34E-43)
            if (r12 != r2) goto L_0x0614
            r2 = 2
            r1.setLocationWhere(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0614
        L_0x05c3:
            org.json.JSONObject r2 = r11.getJSONObject(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            org.json.JSONObject r4 = r2.getJSONObject(r15)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r4.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            double r6 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLatitude(r6)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r4.getString(r5)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            double r3 = java.lang.Double.parseDouble(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setLongitude(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = r2.getString(r14)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            float r3 = java.lang.Float.parseFloat(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.setRadius(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.String r3 = "isCellChanged"
            java.lang.String r2 = r2.getString(r3)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            boolean r2 = java.lang.Boolean.parseBoolean(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            r1.m1658a(r2)     // Catch:{ Exception -> 0x05ff, Error -> 0x0603 }
            goto L_0x0142
        L_0x05ff:
            r0 = move-exception
            r3 = r0
            r2 = 0
            goto L_0x060d
        L_0x0603:
            r0 = move-exception
            r2 = r0
            r2.printStackTrace()
            r2 = 0
            goto L_0x0610
        L_0x060a:
            r0 = move-exception
            r2 = 0
            r3 = r0
        L_0x060d:
            r3.printStackTrace()
        L_0x0610:
            r1.f1128a = r2
            r1.f1142o = r2
        L_0x0614:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.BDLocation.<init>(java.lang.String):void");
    }

    /* renamed from: a */
    private void m1658a(Boolean bool) {
        this.f1147t = bool.booleanValue();
    }

    public int describeContents() {
        return 0;
    }

    public String getAdCode() {
        return this.f1148u.adcode;
    }

    public String getAddrStr() {
        return this.f1148u.address;
    }

    public Address getAddress() {
        return this.f1148u;
    }

    public double getAltitude() {
        return this.f1133f;
    }

    public String getBuildingID() {
        return this.f1150w;
    }

    public String getBuildingName() {
        return this.f1151x;
    }

    public String getCity() {
        return this.f1148u.city;
    }

    public String getCityCode() {
        return this.f1148u.cityCode;
    }

    public String getCoorType() {
        return this.f1141n;
    }

    public String getCountry() {
        return this.f1148u.country;
    }

    public String getCountryCode() {
        return this.f1148u.countryCode;
    }

    public long getDelayTime() {
        return this.f1124S;
    }

    @Deprecated
    public float getDerect() {
        return this.f1140m;
    }

    public float getDirection() {
        return this.f1140m;
    }

    public String getDistrict() {
        return this.f1148u.district;
    }

    public Location getExtraLocation(String str) {
        Bundle bundle = this.f1121P;
        if (bundle == null) {
            return null;
        }
        Parcelable parcelable = bundle.getParcelable(str);
        if (parcelable instanceof Location) {
            return (Location) parcelable;
        }
        return null;
    }

    public String getFloor() {
        return this.f1149v;
    }

    public double[] getFusionLocInfo(String str) {
        return this.f1121P.getDoubleArray(str);
    }

    public int getGpsAccuracyStatus() {
        return this.f1122Q;
    }

    public int getGpsCheckStatus() {
        return this.f1123R;
    }

    public int getIndoorLocationSource() {
        return this.f1113H;
    }

    public int getIndoorLocationSurpport() {
        return this.f1111F;
    }

    public String getIndoorLocationSurpportBuidlingID() {
        return this.f1115J;
    }

    public String getIndoorLocationSurpportBuidlingName() {
        return this.f1114I;
    }

    public int getIndoorNetworkState() {
        return this.f1112G;
    }

    public String getIndoorSurpportPolygon() {
        return this.f1116K;
    }

    public double getLatitude() {
        return this.f1130c;
    }

    public int getLocType() {
        return this.f1128a;
    }

    public String getLocTypeDescription() {
        return this.f1118M;
    }

    public String getLocationDescribe() {
        return this.f1144q;
    }

    public String getLocationID() {
        return this.f1119N;
    }

    public int getLocationWhere() {
        return this.f1106A;
    }

    public double getLongitude() {
        return this.f1131d;
    }

    public String getNetworkLocationType() {
        return this.f1107B;
    }

    public double getNrlLat() {
        return this.f1126U;
    }

    public double getNrlLon() {
        return this.f1127V;
    }

    public String getNrlResult() {
        return this.f1125T;
    }

    public int getOperators() {
        return this.f1108C;
    }

    public List<Poi> getPoiList() {
        return this.f1117L;
    }

    public String getProvince() {
        return this.f1148u.province;
    }

    public float getRadius() {
        return this.f1137j;
    }

    public String getRetFields(String str) {
        return this.f1121P.getString(str);
    }

    public String getRoadLocString() {
        return this.f1120O;
    }

    public int getSatelliteNumber() {
        this.f1138k = true;
        return this.f1139l;
    }

    @Deprecated
    public String getSemaAptag() {
        return this.f1144q;
    }

    public float getSpeed() {
        return this.f1135h;
    }

    public String getStreet() {
        return this.f1148u.street;
    }

    public String getStreetNumber() {
        return this.f1148u.streetNumber;
    }

    public String getTime() {
        return this.f1129b;
    }

    public int getUserIndoorState() {
        return this.f1110E;
    }

    public String getVdrJsonString() {
        Bundle bundle = this.f1121P;
        if (bundle == null || !bundle.containsKey("vdr")) {
            return null;
        }
        return this.f1121P.getString("vdr");
    }

    public boolean hasAddr() {
        return this.f1142o;
    }

    public boolean hasAltitude() {
        return this.f1132e;
    }

    public boolean hasRadius() {
        return this.f1136i;
    }

    public boolean hasSateNumber() {
        return this.f1138k;
    }

    public boolean hasSpeed() {
        return this.f1134g;
    }

    public boolean isCellChangeFlag() {
        return this.f1147t;
    }

    public boolean isIndoorLocMode() {
        return this.f1152y;
    }

    public boolean isNrlAvailable() {
        return (this.f1127V == Double.MIN_VALUE || this.f1126U == Double.MIN_VALUE) ? false : true;
    }

    public int isParkAvailable() {
        return this.f1153z;
    }

    public void setAddr(Address address) {
        if (address != null) {
            this.f1148u = address;
            this.f1142o = true;
        }
    }

    public void setAddrStr(String str) {
        this.f1143p = str;
        this.f1142o = str != null;
    }

    public void setAltitude(double d) {
        if (d < 9999.0d) {
            this.f1133f = d;
            this.f1132e = true;
        }
    }

    public void setBuildingID(String str) {
        this.f1150w = str;
    }

    public void setBuildingName(String str) {
        this.f1151x = str;
    }

    public void setCoorType(String str) {
        this.f1141n = str;
    }

    public void setDelayTime(long j) {
        this.f1124S = j;
    }

    public void setDirection(float f) {
        this.f1140m = f;
    }

    public void setExtraLocation(String str, Location location) {
        if (this.f1121P == null) {
            this.f1121P = new Bundle();
        }
        this.f1121P.putParcelable(str, location);
    }

    public void setFloor(String str) {
        this.f1149v = str;
    }

    public void setFusionLocInfo(String str, double[] dArr) {
        if (this.f1121P == null) {
            this.f1121P = new Bundle();
        }
        this.f1121P.putDoubleArray(str, dArr);
    }

    public void setGpsAccuracyStatus(int i) {
        this.f1122Q = i;
    }

    public void setGpsCheckStatus(int i) {
        this.f1123R = i;
    }

    public void setIndoorLocMode(boolean z) {
        this.f1152y = z;
    }

    public void setIndoorLocationSource(int i) {
        this.f1113H = i;
    }

    public void setIndoorLocationSurpport(int i) {
        this.f1111F = i;
    }

    public void setIndoorNetworkState(int i) {
        this.f1112G = i;
    }

    public void setIndoorSurpportPolygon(String str) {
        this.f1116K = str;
    }

    public void setLatitude(double d) {
        this.f1130c = d;
    }

    public void setLocType(int i) {
        String str;
        this.f1128a = i;
        if (i != 66) {
            if (i != 67) {
                if (i == 161) {
                    str = "NetWork location successful!";
                } else if (i == 162) {
                    str = "NetWork location failed because baidu location service can not decrypt the request query, please check the so file !";
                } else if (i == 167) {
                    str = "NetWork location failed because baidu location service can not caculate the location!";
                } else if (i != 505) {
                    switch (i) {
                        case 61:
                            setLocTypeDescription("GPS location successful!");
                            setUserIndoorState(0);
                            return;
                        case 62:
                            str = "Location failed beacuse we can not get any loc information!";
                            break;
                        case 63:
                            break;
                        default:
                            str = "UnKnown!";
                            break;
                    }
                } else {
                    str = "NetWork location failed because baidu location service check the key is unlegal, please check the key in AndroidManifest.xml !";
                }
            }
            str = "Offline location failed, please check the net (wifi/cell)!";
        } else {
            str = "Offline location successful!";
        }
        setLocTypeDescription(str);
    }

    public void setLocTypeDescription(String str) {
        this.f1118M = str;
    }

    public void setLocationDescribe(String str) {
        this.f1144q = str;
    }

    public void setLocationID(String str) {
        this.f1119N = str;
    }

    public void setLocationWhere(int i) {
        this.f1106A = i;
    }

    public void setLongitude(double d) {
        this.f1131d = d;
    }

    public void setNetworkLocationType(String str) {
        this.f1107B = str;
    }

    public void setNrlData(String str) {
        this.f1125T = str;
    }

    public void setOperators(int i) {
        this.f1108C = i;
    }

    public void setParkAvailable(int i) {
        this.f1153z = i;
    }

    public void setPoiList(List<Poi> list) {
        this.f1117L = list;
    }

    public void setRadius(float f) {
        this.f1137j = f;
        this.f1136i = true;
    }

    public void setRetFields(String str, String str2) {
        if (this.f1121P == null) {
            this.f1121P = new Bundle();
        }
        this.f1121P.putString(str, str2);
    }

    public void setRoadLocString(float f, float f2) {
        String str;
        String str2 = "";
        if (((double) f) > 0.001d) {
            str = String.format("%.2f", Float.valueOf(f));
        } else {
            str = str2;
        }
        if (((double) f2) > 0.001d) {
            str2 = String.format("%.2f", Float.valueOf(f2));
        }
        if (this.f1125T != null) {
            this.f1120O = String.format(Locale.US, "%s,%s,%s", this.f1125T, str, str2);
        }
    }

    public void setSatelliteNumber(int i) {
        this.f1139l = i;
    }

    public void setSpeed(float f) {
        this.f1135h = f;
        this.f1134g = true;
    }

    public void setTime(String str) {
        this.f1129b = str;
        setLocationID(C0855k.m2456a(str));
    }

    public void setUserIndoorState(int i) {
        this.f1110E = i;
    }

    public void setVdrJsonValue(String str) {
        if (this.f1121P == null) {
            this.f1121P = new Bundle();
        }
        this.f1121P.putString("vdr", str);
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f1128a);
        parcel.writeString(this.f1129b);
        parcel.writeDouble(this.f1130c);
        parcel.writeDouble(this.f1131d);
        parcel.writeDouble(this.f1133f);
        parcel.writeFloat(this.f1135h);
        parcel.writeFloat(this.f1137j);
        parcel.writeInt(this.f1139l);
        parcel.writeFloat(this.f1140m);
        parcel.writeString(this.f1149v);
        parcel.writeInt(this.f1153z);
        parcel.writeString(this.f1150w);
        parcel.writeString(this.f1151x);
        parcel.writeString(this.f1107B);
        parcel.writeString(this.f1148u.province);
        parcel.writeString(this.f1148u.city);
        parcel.writeString(this.f1148u.district);
        parcel.writeString(this.f1148u.street);
        parcel.writeString(this.f1148u.streetNumber);
        parcel.writeString(this.f1148u.cityCode);
        parcel.writeString(this.f1148u.address);
        parcel.writeString(this.f1148u.country);
        parcel.writeString(this.f1148u.countryCode);
        parcel.writeString(this.f1148u.adcode);
        parcel.writeInt(this.f1108C);
        parcel.writeString(this.f1109D);
        parcel.writeString(this.f1144q);
        parcel.writeString(this.f1145r);
        parcel.writeString(this.f1146s);
        parcel.writeInt(this.f1106A);
        parcel.writeString(this.f1118M);
        parcel.writeInt(this.f1110E);
        parcel.writeInt(this.f1111F);
        parcel.writeInt(this.f1112G);
        parcel.writeInt(this.f1113H);
        parcel.writeString(this.f1114I);
        parcel.writeString(this.f1115J);
        parcel.writeString(this.f1116K);
        parcel.writeInt(this.f1122Q);
        parcel.writeString(this.f1119N);
        parcel.writeInt(this.f1123R);
        parcel.writeString(this.f1120O);
        parcel.writeString(this.f1125T);
        parcel.writeLong(this.f1124S);
        parcel.writeDouble(this.f1126U);
        parcel.writeDouble(this.f1127V);
        parcel.writeBooleanArray(new boolean[]{this.f1132e, this.f1134g, this.f1136i, this.f1138k, this.f1142o, this.f1147t, this.f1152y});
        parcel.writeList(this.f1117L);
        parcel.writeBundle(this.f1121P);
    }
}
