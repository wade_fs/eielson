package com.baidu.mapapi.map;

import android.view.View;
import com.baidu.mapapi.map.SwipeDismissTouchListener;

/* renamed from: com.baidu.mapapi.map.s */
class C0944s implements SwipeDismissTouchListener.DismissCallbacks {

    /* renamed from: a */
    final /* synthetic */ SwipeDismissView f2869a;

    C0944s(SwipeDismissView swipeDismissView) {
        this.f2869a = swipeDismissView;
    }

    public boolean canDismiss(Object obj) {
        return true;
    }

    public void onDismiss(View view, Object obj) {
        if (this.f2869a.f2718a != null) {
            this.f2869a.f2718a.onDismiss();
        }
    }

    public void onNotify() {
        if (this.f2869a.f2718a != null) {
            this.f2869a.f2718a.onNotify();
        }
    }
}
