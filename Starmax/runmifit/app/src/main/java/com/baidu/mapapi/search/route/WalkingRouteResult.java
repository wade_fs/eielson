package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.search.core.SearchResult;
import com.baidu.mapapi.search.core.TaxiInfo;
import java.util.ArrayList;
import java.util.List;

public class WalkingRouteResult extends SearchResult implements Parcelable {
    public static final Parcelable.Creator<WalkingRouteResult> CREATOR = new C1004t();

    /* renamed from: a */
    private List<WalkingRouteLine> f3124a;

    /* renamed from: b */
    private TaxiInfo f3125b;

    /* renamed from: c */
    private SuggestAddrInfo f3126c;

    public WalkingRouteResult() {
    }

    protected WalkingRouteResult(Parcel parcel) {
        this.f3124a = new ArrayList();
        parcel.readList(this.f3124a, WalkingRouteLine.class.getClassLoader());
        this.f3125b = (TaxiInfo) parcel.readParcelable(TaxiInfo.class.getClassLoader());
        this.f3126c = (SuggestAddrInfo) parcel.readParcelable(SuggestAddrInfo.class.getClassLoader());
    }

    public int describeContents() {
        return 0;
    }

    public List<WalkingRouteLine> getRouteLines() {
        return this.f3124a;
    }

    public SuggestAddrInfo getSuggestAddrInfo() {
        return this.f3126c;
    }

    public TaxiInfo getTaxiInfo() {
        return this.f3125b;
    }

    public void setRouteLines(List<WalkingRouteLine> list) {
        this.f3124a = list;
    }

    public void setSuggestAddrInfo(SuggestAddrInfo suggestAddrInfo) {
        this.f3126c = suggestAddrInfo;
    }

    public void setTaxiInfo(TaxiInfo taxiInfo) {
        this.f3125b = taxiInfo;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeList(this.f3124a);
        parcel.writeParcelable(this.f3125b, 1);
        parcel.writeParcelable(this.f3126c, 1);
    }
}
