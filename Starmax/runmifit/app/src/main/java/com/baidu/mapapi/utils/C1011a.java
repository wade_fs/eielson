package com.baidu.mapapi.utils;

import com.baidu.mapapi.utils.CoordinateConverter;

/* renamed from: com.baidu.mapapi.utils.a */
/* synthetic */ class C1011a {

    /* renamed from: a */
    static final /* synthetic */ int[] f3236a = new int[CoordinateConverter.CoordType.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
    /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    static {
        /*
            com.baidu.mapapi.utils.CoordinateConverter$CoordType[] r0 = com.baidu.mapapi.utils.CoordinateConverter.CoordType.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.mapapi.utils.C1011a.f3236a = r0
            int[] r0 = com.baidu.mapapi.utils.C1011a.f3236a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.baidu.mapapi.utils.CoordinateConverter$CoordType r1 = com.baidu.mapapi.utils.CoordinateConverter.CoordType.COMMON     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.baidu.mapapi.utils.C1011a.f3236a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.baidu.mapapi.utils.CoordinateConverter$CoordType r1 = com.baidu.mapapi.utils.CoordinateConverter.CoordType.GPS     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.baidu.mapapi.utils.C1011a.f3236a     // Catch:{ NoSuchFieldError -> 0x002a }
            com.baidu.mapapi.utils.CoordinateConverter$CoordType r1 = com.baidu.mapapi.utils.CoordinateConverter.CoordType.BD09LL     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            int[] r0 = com.baidu.mapapi.utils.C1011a.f3236a     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.baidu.mapapi.utils.CoordinateConverter$CoordType r1 = com.baidu.mapapi.utils.CoordinateConverter.CoordType.BD09MC     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.utils.C1011a.<clinit>():void");
    }
}
