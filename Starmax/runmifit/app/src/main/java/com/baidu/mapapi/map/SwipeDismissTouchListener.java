package com.baidu.mapapi.map;

import android.animation.ValueAnimator;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;

public class SwipeDismissTouchListener implements View.OnTouchListener {

    /* renamed from: a */
    private int f2702a;

    /* renamed from: b */
    private int f2703b;

    /* renamed from: c */
    private int f2704c;

    /* renamed from: d */
    private long f2705d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public View f2706e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public DismissCallbacks f2707f;

    /* renamed from: g */
    private int f2708g = 1;

    /* renamed from: h */
    private float f2709h;

    /* renamed from: i */
    private float f2710i;

    /* renamed from: j */
    private boolean f2711j;

    /* renamed from: k */
    private int f2712k;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public Object f2713l;

    /* renamed from: m */
    private VelocityTracker f2714m;

    /* renamed from: n */
    private float f2715n;

    /* renamed from: o */
    private boolean f2716o;

    /* renamed from: p */
    private boolean f2717p;

    public interface DismissCallbacks {
        boolean canDismiss(Object obj);

        void onDismiss(View view, Object obj);

        void onNotify();
    }

    public SwipeDismissTouchListener(View view, Object obj, DismissCallbacks dismissCallbacks) {
        ViewConfiguration viewConfiguration = ViewConfiguration.get(view.getContext());
        this.f2702a = viewConfiguration.getScaledTouchSlop();
        this.f2703b = viewConfiguration.getScaledMinimumFlingVelocity();
        this.f2704c = viewConfiguration.getScaledMaximumFlingVelocity();
        this.f2705d = (long) view.getContext().getResources().getInteger(17694720);
        this.f2706e = view;
        this.f2706e.getContext();
        this.f2713l = obj;
        this.f2707f = dismissCallbacks;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2955a() {
        ViewGroup.LayoutParams layoutParams = this.f2706e.getLayoutParams();
        int height = this.f2706e.getHeight();
        ValueAnimator duration = ValueAnimator.ofInt(height, 1).setDuration(this.f2705d);
        duration.addListener(new C0942q(this, layoutParams, height));
        duration.addUpdateListener(new C0943r(this, layoutParams));
        duration.start();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0027, code lost:
        if (r9.f2714m == null) goto L_0x018c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x015e, code lost:
        if (r9.f2714m.getXVelocity() > 0.0f) goto L_0x0163;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:79:0x0188, code lost:
        if (r9.f2711j != false) goto L_0x002b;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouch(android.view.View r10, android.view.MotionEvent r11) {
        /*
            r9 = this;
            float r10 = r9.f2715n
            r0 = 0
            r11.offsetLocation(r10, r0)
            int r10 = r9.f2708g
            r1 = 2
            if (r10 >= r1) goto L_0x0013
            android.view.View r10 = r9.f2706e
            int r10 = r10.getWidth()
            r9.f2708g = r10
        L_0x0013:
            int r10 = r11.getActionMasked()
            r2 = 0
            r3 = 1
            if (r10 == 0) goto L_0x018d
            r4 = 0
            r5 = 3
            if (r10 == r3) goto L_0x00e2
            if (r10 == r1) goto L_0x004f
            if (r10 == r5) goto L_0x0025
            goto L_0x018c
        L_0x0025:
            android.view.VelocityTracker r10 = r9.f2714m
            if (r10 != 0) goto L_0x002b
            goto L_0x018c
        L_0x002b:
            android.view.View r10 = r9.f2706e
            android.view.ViewPropertyAnimator r10 = r10.animate()
            android.view.ViewPropertyAnimator r10 = r10.translationX(r0)
            long r5 = r9.f2705d
            android.view.ViewPropertyAnimator r10 = r10.setDuration(r5)
            r10.setListener(r4)
        L_0x003e:
            android.view.VelocityTracker r10 = r9.f2714m
            r10.recycle()
            r9.f2714m = r4
            r9.f2715n = r0
            r9.f2709h = r0
            r9.f2710i = r0
            r9.f2711j = r2
            goto L_0x018c
        L_0x004f:
            android.view.VelocityTracker r10 = r9.f2714m
            if (r10 != 0) goto L_0x0055
            goto L_0x018c
        L_0x0055:
            r10.addMovement(r11)
            float r10 = r11.getRawX()
            float r1 = r9.f2709h
            float r10 = r10 - r1
            float r1 = r11.getRawY()
            float r4 = r9.f2710i
            float r1 = r1 - r4
            float r4 = java.lang.Math.abs(r10)
            int r6 = r9.f2702a
            float r6 = (float) r6
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 <= 0) goto L_0x00d2
            float r1 = java.lang.Math.abs(r1)
            float r4 = java.lang.Math.abs(r10)
            r6 = 1073741824(0x40000000, float:2.0)
            float r4 = r4 / r6
            int r1 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x00d2
            r9.f2711j = r3
            int r0 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r0 <= 0) goto L_0x0089
            int r0 = r9.f2702a
            goto L_0x008c
        L_0x0089:
            int r0 = r9.f2702a
            int r0 = -r0
        L_0x008c:
            r9.f2712k = r0
            android.view.View r0 = r9.f2706e
            android.view.ViewParent r0 = r0.getParent()
            r0.requestDisallowInterceptTouchEvent(r3)
            boolean r0 = r9.f2716o
            if (r0 != 0) goto L_0x00a2
            r9.f2716o = r3
            com.baidu.mapapi.map.SwipeDismissTouchListener$DismissCallbacks r0 = r9.f2707f
            r0.onNotify()
        L_0x00a2:
            float r0 = java.lang.Math.abs(r10)
            int r1 = r9.f2708g
            int r1 = r1 / r5
            float r1 = (float) r1
            int r0 = (r0 > r1 ? 1 : (r0 == r1 ? 0 : -1))
            if (r0 <= 0) goto L_0x00ba
            boolean r0 = r9.f2717p
            if (r0 != 0) goto L_0x00bc
            r9.f2717p = r3
            com.baidu.mapapi.map.SwipeDismissTouchListener$DismissCallbacks r0 = r9.f2707f
            r0.onNotify()
            goto L_0x00bc
        L_0x00ba:
            r9.f2717p = r2
        L_0x00bc:
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r11)
            int r11 = r11.getActionIndex()
            int r11 = r11 << 8
            r11 = r11 | r5
            r0.setAction(r11)
            android.view.View r11 = r9.f2706e
            r11.onTouchEvent(r0)
            r0.recycle()
        L_0x00d2:
            boolean r11 = r9.f2711j
            if (r11 == 0) goto L_0x018c
            r9.f2715n = r10
            android.view.View r11 = r9.f2706e
            int r0 = r9.f2712k
            float r0 = (float) r0
            float r10 = r10 - r0
            r11.setTranslationX(r10)
            return r3
        L_0x00e2:
            android.view.VelocityTracker r10 = r9.f2714m
            if (r10 != 0) goto L_0x00e8
            goto L_0x018c
        L_0x00e8:
            float r10 = r11.getRawX()
            float r1 = r9.f2709h
            float r10 = r10 - r1
            android.view.VelocityTracker r1 = r9.f2714m
            r1.addMovement(r11)
            android.view.VelocityTracker r11 = r9.f2714m
            r1 = 1000(0x3e8, float:1.401E-42)
            r11.computeCurrentVelocity(r1)
            android.view.VelocityTracker r11 = r9.f2714m
            float r11 = r11.getXVelocity()
            float r1 = java.lang.Math.abs(r11)
            android.view.VelocityTracker r6 = r9.f2714m
            float r6 = r6.getYVelocity()
            float r6 = java.lang.Math.abs(r6)
            float r7 = java.lang.Math.abs(r10)
            int r8 = r9.f2708g
            int r8 = r8 / r5
            float r5 = (float) r8
            int r5 = (r7 > r5 ? 1 : (r7 == r5 ? 0 : -1))
            if (r5 <= 0) goto L_0x0129
            boolean r5 = r9.f2711j
            if (r5 == 0) goto L_0x0129
            int r10 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r10 <= 0) goto L_0x0125
            r10 = 1
            goto L_0x0126
        L_0x0125:
            r10 = 0
        L_0x0126:
            r3 = r10
            r10 = 1
            goto L_0x0163
        L_0x0129:
            int r5 = r9.f2703b
            float r5 = (float) r5
            int r5 = (r5 > r1 ? 1 : (r5 == r1 ? 0 : -1))
            if (r5 > 0) goto L_0x0161
            int r5 = r9.f2704c
            float r5 = (float) r5
            int r5 = (r1 > r5 ? 1 : (r1 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x0161
            int r5 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r5 >= 0) goto L_0x0161
            int r1 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r1 >= 0) goto L_0x0161
            boolean r1 = r9.f2711j
            if (r1 == 0) goto L_0x0161
            int r11 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r11 >= 0) goto L_0x0149
            r11 = 1
            goto L_0x014a
        L_0x0149:
            r11 = 0
        L_0x014a:
            int r10 = (r10 > r0 ? 1 : (r10 == r0 ? 0 : -1))
            if (r10 >= 0) goto L_0x0150
            r10 = 1
            goto L_0x0151
        L_0x0150:
            r10 = 0
        L_0x0151:
            if (r11 != r10) goto L_0x0155
            r10 = 1
            goto L_0x0156
        L_0x0155:
            r10 = 0
        L_0x0156:
            android.view.VelocityTracker r11 = r9.f2714m
            float r11 = r11.getXVelocity()
            int r11 = (r11 > r0 ? 1 : (r11 == r0 ? 0 : -1))
            if (r11 <= 0) goto L_0x0162
            goto L_0x0163
        L_0x0161:
            r10 = 0
        L_0x0162:
            r3 = 0
        L_0x0163:
            if (r10 == 0) goto L_0x0186
            android.view.View r10 = r9.f2706e
            android.view.ViewPropertyAnimator r10 = r10.animate()
            int r11 = r9.f2708g
            if (r3 == 0) goto L_0x0170
            goto L_0x0171
        L_0x0170:
            int r11 = -r11
        L_0x0171:
            float r11 = (float) r11
            android.view.ViewPropertyAnimator r10 = r10.translationX(r11)
            long r5 = r9.f2705d
            android.view.ViewPropertyAnimator r10 = r10.setDuration(r5)
            com.baidu.mapapi.map.p r11 = new com.baidu.mapapi.map.p
            r11.<init>(r9)
            r10.setListener(r11)
            goto L_0x003e
        L_0x0186:
            boolean r10 = r9.f2711j
            if (r10 == 0) goto L_0x003e
            goto L_0x002b
        L_0x018c:
            return r2
        L_0x018d:
            float r10 = r11.getRawX()
            r9.f2709h = r10
            float r10 = r11.getRawY()
            r9.f2710i = r10
            com.baidu.mapapi.map.SwipeDismissTouchListener$DismissCallbacks r10 = r9.f2707f
            java.lang.Object r0 = r9.f2713l
            boolean r10 = r10.canDismiss(r0)
            if (r10 == 0) goto L_0x01b0
            r9.f2716o = r2
            android.view.VelocityTracker r10 = android.view.VelocityTracker.obtain()
            r9.f2714m = r10
            android.view.VelocityTracker r10 = r9.f2714m
            r10.addMovement(r11)
        L_0x01b0:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapapi.map.SwipeDismissTouchListener.onTouch(android.view.View, android.view.MotionEvent):boolean");
    }
}
