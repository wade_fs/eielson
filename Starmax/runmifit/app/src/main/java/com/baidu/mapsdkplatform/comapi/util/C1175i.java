package com.baidu.mapsdkplatform.comapi.util;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import com.baidu.android.bbalbs.common.util.CommonParam;
import com.baidu.mapapi.VersionInfo;
import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import com.baidu.mapsdkplatform.comjni.util.C1184a;
import com.tencent.open.SocialOperation;
import com.tencent.stat.apkreader.ChannelReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/* renamed from: com.baidu.mapsdkplatform.comapi.util.i */
public class C1175i {

    /* renamed from: A */
    private static boolean f3907A = true;

    /* renamed from: B */
    private static int f3908B = 0;

    /* renamed from: C */
    private static int f3909C = 0;

    /* renamed from: D */
    private static Map<String, String> f3910D = new HashMap();

    /* renamed from: a */
    public static Context f3911a;

    /* renamed from: b */
    public static final int f3912b = Integer.parseInt(Build.VERSION.SDK);

    /* renamed from: c */
    public static float f3913c = 1.0f;

    /* renamed from: d */
    public static String f3914d;

    /* renamed from: e */
    private static final String f3915e = C1175i.class.getSimpleName();

    /* renamed from: f */
    private static C1184a f3916f = new C1184a();

    /* renamed from: g */
    private static String f3917g = "02";

    /* renamed from: h */
    private static String f3918h;

    /* renamed from: i */
    private static String f3919i;

    /* renamed from: j */
    private static String f3920j;

    /* renamed from: k */
    private static String f3921k;

    /* renamed from: l */
    private static int f3922l;

    /* renamed from: m */
    private static int f3923m;

    /* renamed from: n */
    private static int f3924n;

    /* renamed from: o */
    private static int f3925o;

    /* renamed from: p */
    private static int f3926p;

    /* renamed from: q */
    private static int f3927q;

    /* renamed from: r */
    private static String f3928r;

    /* renamed from: s */
    private static String f3929s = "baidu";

    /* renamed from: t */
    private static String f3930t = "";

    /* renamed from: u */
    private static String f3931u = "";

    /* renamed from: v */
    private static String f3932v = "";

    /* renamed from: w */
    private static String f3933w;

    /* renamed from: x */
    private static String f3934x;

    /* renamed from: y */
    private static String f3935y = "-1";

    /* renamed from: z */
    private static String f3936z = "-1";

    /* renamed from: a */
    public static void m4264a() {
        m4272d();
    }

    /* renamed from: a */
    public static void m4265a(String str) {
        f3928r = str;
        m4275f();
    }

    /* renamed from: a */
    public static void m4266a(String str, String str2) {
        f3935y = str2;
        f3936z = str;
        m4275f();
    }

    /* renamed from: a */
    public static byte[] m4267a(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 64).signatures[0].toByteArray();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: b */
    public static Bundle m4268b() {
        Bundle bundle = new Bundle();
        bundle.putString("cpu", f3930t);
        bundle.putString("resid", f3917g);
        bundle.putString(ChannelReader.CHANNEL_KEY, f3929s);
        bundle.putString("glr", f3931u);
        bundle.putString("glv", f3932v);
        bundle.putString("mb", m4276g());
        bundle.putString("sv", m4278i());
        bundle.putString("os", m4280k());
        bundle.putInt("dpi_x", m4281l());
        bundle.putInt("dpi_y", m4281l());
        bundle.putString("net", f3928r);
        bundle.putString("cuid", m4285p());
        bundle.putByteArray(SocialOperation.GAME_SIGNATURE, m4267a(f3911a));
        bundle.putString("pcn", f3911a.getPackageName());
        bundle.putInt("screen_x", m4277h());
        bundle.putInt("screen_y", m4279j());
        C1184a aVar = f3916f;
        if (aVar != null) {
            aVar.mo13568a(bundle);
        }
        return bundle;
    }

    /* renamed from: b */
    public static void m4269b(Context context) {
        Map<String, String> map;
        String str;
        f3911a = context;
        if (context.getFilesDir() != null) {
            f3933w = context.getFilesDir().getAbsolutePath();
        }
        if (context.getCacheDir() != null) {
            f3934x = context.getCacheDir().getAbsolutePath();
        }
        f3919i = Build.MODEL;
        f3920j = "Android" + Build.VERSION.SDK;
        f3918h = context.getPackageName();
        m4271c(context);
        m4273d(context);
        m4286q();
        try {
            LocationManager locationManager = (LocationManager) context.getSystemService("location");
            f3908B = locationManager.isProviderEnabled("gps") ? 1 : 0;
            f3909C = locationManager.isProviderEnabled("network") ? 1 : 0;
            f3910D.put("resid", AppMD5.encodeUrlParamsValue(f3917g));
            f3910D.put(ChannelReader.CHANNEL_KEY, AppMD5.encodeUrlParamsValue(m4282m()));
            f3910D.put("mb", AppMD5.encodeUrlParamsValue(m4276g()));
            f3910D.put("sv", AppMD5.encodeUrlParamsValue(m4278i()));
            f3910D.put("os", AppMD5.encodeUrlParamsValue(m4280k()));
            f3910D.put("dpi", AppMD5.encodeUrlParamsValue(String.format("%d,%d", Integer.valueOf(m4281l()), Integer.valueOf(m4281l()))));
            f3910D.put("cuid", AppMD5.encodeUrlParamsValue(m4285p()));
            f3910D.put("pcn", AppMD5.encodeUrlParamsValue(f3911a.getPackageName()));
            map = f3910D;
            str = String.format("%d,%d", Integer.valueOf(m4277h()), Integer.valueOf(m4279j()));
        } catch (Exception unused) {
            Log.w("baidumapsdk", "LocationManager error");
            f3910D.put("resid", AppMD5.encodeUrlParamsValue(f3917g));
            f3910D.put(ChannelReader.CHANNEL_KEY, AppMD5.encodeUrlParamsValue(m4282m()));
            f3910D.put("mb", AppMD5.encodeUrlParamsValue(m4276g()));
            f3910D.put("sv", AppMD5.encodeUrlParamsValue(m4278i()));
            f3910D.put("os", AppMD5.encodeUrlParamsValue(m4280k()));
            f3910D.put("dpi", AppMD5.encodeUrlParamsValue(String.format("%d,%d", Integer.valueOf(m4281l()), Integer.valueOf(m4281l()))));
            f3910D.put("cuid", AppMD5.encodeUrlParamsValue(m4285p()));
            f3910D.put("pcn", AppMD5.encodeUrlParamsValue(f3911a.getPackageName()));
            map = f3910D;
            str = String.format("%d,%d", Integer.valueOf(m4277h()), Integer.valueOf(m4279j()));
        } catch (Throwable th) {
            f3910D.put("resid", AppMD5.encodeUrlParamsValue(f3917g));
            f3910D.put(ChannelReader.CHANNEL_KEY, AppMD5.encodeUrlParamsValue(m4282m()));
            f3910D.put("mb", AppMD5.encodeUrlParamsValue(m4276g()));
            f3910D.put("sv", AppMD5.encodeUrlParamsValue(m4278i()));
            f3910D.put("os", AppMD5.encodeUrlParamsValue(m4280k()));
            f3910D.put("dpi", AppMD5.encodeUrlParamsValue(String.format("%d,%d", Integer.valueOf(m4281l()), Integer.valueOf(m4281l()))));
            f3910D.put("cuid", AppMD5.encodeUrlParamsValue(m4285p()));
            f3910D.put("pcn", AppMD5.encodeUrlParamsValue(f3911a.getPackageName()));
            f3910D.put("screen", AppMD5.encodeUrlParamsValue(String.format("%d,%d", Integer.valueOf(m4277h()), Integer.valueOf(m4279j()))));
            throw th;
        }
        map.put("screen", AppMD5.encodeUrlParamsValue(str));
        C1184a aVar = f3916f;
        if (aVar != null) {
            aVar.mo13567a();
        }
    }

    /* renamed from: c */
    public static String m4270c() {
        if (f3910D == null) {
            return null;
        }
        Date date = new Date();
        long time = date.getTime() + ((long) (date.getSeconds() * 1000));
        double d = (double) (time / 1000);
        double d2 = (double) (time % 1000);
        Double.isNaN(d2);
        Double.isNaN(d);
        double d3 = d + (d2 / 1000.0d);
        f3910D.put("ctm", AppMD5.encodeUrlParamsValue(String.format("%f", Double.valueOf(d3))));
        StringBuilder sb = new StringBuilder();
        for (Map.Entry entry : f3910D.entrySet()) {
            sb.append("&");
            sb.append((String) entry.getKey());
            sb.append("=");
            sb.append((String) entry.getValue());
        }
        return sb.toString();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.String.replace(char, char):java.lang.String}
     arg types: [int, int]
     candidates:
      ClspMth{java.lang.String.replace(java.lang.CharSequence, java.lang.CharSequence):java.lang.String}
      ClspMth{java.lang.String.replace(char, char):java.lang.String} */
    /* renamed from: c */
    private static void m4271c(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            f3921k = VersionInfo.getApiVersion();
            if (f3921k != null && !f3921k.equals("")) {
                f3921k = f3921k.replace('_', '.');
            }
            f3922l = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException unused) {
            f3921k = "1.0.0";
            f3922l = 1;
        }
    }

    /* renamed from: d */
    public static void m4272d() {
        C1184a aVar = f3916f;
        if (aVar != null) {
            aVar.mo13569b();
        }
    }

    /* renamed from: d */
    private static void m4273d(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        Display defaultDisplay = windowManager != null ? windowManager.getDefaultDisplay() : null;
        if (defaultDisplay != null) {
            f3923m = defaultDisplay.getWidth();
            f3924n = defaultDisplay.getHeight();
            defaultDisplay.getMetrics(displayMetrics);
        }
        f3913c = displayMetrics.density;
        f3925o = (int) displayMetrics.xdpi;
        f3926p = (int) displayMetrics.ydpi;
        if (f3912b > 3) {
            f3927q = displayMetrics.densityDpi;
        } else {
            f3927q = 160;
        }
        if (f3927q == 0) {
            f3927q = 160;
        }
    }

    /* renamed from: e */
    public static String m4274e() {
        return f3928r;
    }

    /* renamed from: f */
    public static void m4275f() {
        f3910D.put("net", AppMD5.encodeUrlParamsValue(m4274e()));
        f3910D.put("appid", AppMD5.encodeUrlParamsValue(f3935y));
        f3910D.put("bduid", "");
        if (f3916f != null) {
            Bundle bundle = new Bundle();
            bundle.putString("cpu", f3930t);
            bundle.putString("resid", f3917g);
            bundle.putString(ChannelReader.CHANNEL_KEY, f3929s);
            bundle.putString("glr", f3931u);
            bundle.putString("glv", f3932v);
            bundle.putString("mb", m4276g());
            bundle.putString("sv", m4278i());
            bundle.putString("os", m4280k());
            bundle.putInt("dpi_x", m4281l());
            bundle.putInt("dpi_y", m4281l());
            bundle.putString("net", f3928r);
            bundle.putString("cuid", m4285p());
            bundle.putString("pcn", f3911a.getPackageName());
            bundle.putInt("screen_x", m4277h());
            bundle.putInt("screen_y", m4279j());
            bundle.putString("appid", f3935y);
            bundle.putString("duid", f3936z);
            if (!TextUtils.isEmpty(f3914d)) {
                bundle.putString("token", f3914d);
            }
            f3916f.mo13568a(bundle);
            SysUpdateObservable.getInstance().updatePhoneInfo();
        }
    }

    /* renamed from: g */
    public static String m4276g() {
        return f3919i;
    }

    /* renamed from: h */
    public static int m4277h() {
        return f3923m;
    }

    /* renamed from: i */
    public static String m4278i() {
        return f3921k;
    }

    /* renamed from: j */
    public static int m4279j() {
        return f3924n;
    }

    /* renamed from: k */
    public static String m4280k() {
        return f3920j;
    }

    /* renamed from: l */
    public static int m4281l() {
        return f3927q;
    }

    /* renamed from: m */
    public static String m4282m() {
        return f3929s;
    }

    /* renamed from: n */
    public static String m4283n() {
        return f3918h;
    }

    /* renamed from: o */
    public static String m4284o() {
        return f3933w;
    }

    /* renamed from: p */
    public static String m4285p() {
        String str;
        try {
            str = CommonParam.m1545a(f3911a);
        } catch (Exception unused) {
            str = "";
        }
        return str == null ? "" : str;
    }

    /* renamed from: q */
    private static void m4286q() {
        f3928r = "0";
    }
}
