package com.baidu.mapapi.utils.poi;

import android.content.Context;
import android.util.Log;
import com.baidu.mapapi.http.HttpClient;
import com.baidu.platform.comapi.pano.C1322a;
import com.baidu.platform.comapi.pano.C1325c;

/* renamed from: com.baidu.mapapi.utils.poi.a */
final class C1017a implements C1322a.C1323a<C1325c> {

    /* renamed from: a */
    final /* synthetic */ Context f3269a;

    C1017a(Context context) {
        this.f3269a = context;
    }

    /* renamed from: a */
    public void mo12839a(HttpClient.HttpStateError httpStateError) {
        String str;
        int i = C1018b.f3271b[httpStateError.ordinal()];
        if (i == 1) {
            str = "current network is not available";
        } else if (i == 2) {
            str = "network inner error, please check network";
        } else {
            return;
        }
        Log.d("baidumapsdk", str);
    }

    /* renamed from: a */
    public void mo12841a(C1325c cVar) {
        String str;
        if (cVar == null) {
            Log.d("baidumapsdk", "pano info is null");
            return;
        }
        int i = C1018b.f3270a[cVar.mo14029a().ordinal()];
        if (i == 1) {
            str = "pano uid is error, please check param poi uid";
        } else if (i == 2) {
            str = "pano id not found for this poi point";
        } else if (i == 3) {
            str = "please check ak for permission";
        } else if (i == 4) {
            if (cVar.mo14033c() == 1) {
                try {
                    BaiduMapPoiSearch.m3309b(cVar.mo14032b(), this.f3269a);
                    return;
                } catch (Exception e) {
                    e.printStackTrace();
                    return;
                }
            } else {
                str = "this point do not support for pano show";
            }
        } else {
            return;
        }
        Log.d("baidumapsdk", str);
    }
}
