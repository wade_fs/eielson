package com.baidu.mapapi.search.route;

import android.os.Parcel;
import android.os.Parcelable;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.search.core.RouteLine;
import com.baidu.mapapi.search.core.RouteNode;
import com.baidu.mapapi.search.core.RouteStep;
import java.util.ArrayList;
import java.util.List;

public class DrivingRouteLine extends RouteLine<DrivingStep> implements Parcelable {
    public static final Parcelable.Creator<DrivingRouteLine> CREATOR = new C0988d();

    /* renamed from: b */
    private boolean f3029b;

    /* renamed from: c */
    private List<RouteNode> f3030c;

    /* renamed from: d */
    private int f3031d;

    /* renamed from: e */
    private int f3032e;

    public static class DrivingStep extends RouteStep implements Parcelable {
        public static final Parcelable.Creator<DrivingStep> CREATOR = new C0989e();

        /* renamed from: d */
        List<LatLng> f3033d;

        /* renamed from: e */
        int[] f3034e;

        /* renamed from: f */
        private int f3035f;

        /* renamed from: g */
        private RouteNode f3036g;

        /* renamed from: h */
        private RouteNode f3037h;

        /* renamed from: i */
        private String f3038i;

        /* renamed from: j */
        private String f3039j;

        /* renamed from: k */
        private String f3040k;

        /* renamed from: l */
        private String f3041l;

        /* renamed from: m */
        private int f3042m;

        public DrivingStep() {
        }

        protected DrivingStep(Parcel parcel) {
            super(parcel);
            this.f3035f = parcel.readInt();
            this.f3036g = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3037h = (RouteNode) parcel.readParcelable(RouteNode.class.getClassLoader());
            this.f3038i = parcel.readString();
            this.f3039j = parcel.readString();
            this.f3040k = parcel.readString();
            this.f3041l = parcel.readString();
            this.f3042m = parcel.readInt();
            this.f3033d = parcel.createTypedArrayList(LatLng.CREATOR);
            this.f3034e = parcel.createIntArray();
        }

        public int describeContents() {
            return 0;
        }

        public int getDirection() {
            return this.f3035f;
        }

        public RouteNode getEntrance() {
            return this.f3036g;
        }

        public String getEntranceInstructions() {
            return this.f3039j;
        }

        public RouteNode getExit() {
            return this.f3037h;
        }

        public String getExitInstructions() {
            return this.f3040k;
        }

        public String getInstructions() {
            return this.f3041l;
        }

        public int getNumTurns() {
            return this.f3042m;
        }

        public int[] getTrafficList() {
            return this.f3034e;
        }

        public List<LatLng> getWayPoints() {
            if (this.mWayPoints == null) {
                this.mWayPoints = CoordUtil.decodeLocationList(this.f3038i);
            }
            return this.f3033d;
        }

        public void setDirection(int i) {
            this.f3035f = i;
        }

        public void setEntrance(RouteNode routeNode) {
            this.f3036g = routeNode;
        }

        public void setEntranceInstructions(String str) {
            this.f3039j = str;
        }

        public void setExit(RouteNode routeNode) {
            this.f3037h = routeNode;
        }

        public void setExitInstructions(String str) {
            this.f3040k = str;
        }

        public void setInstructions(String str) {
            this.f3041l = str;
        }

        public void setNumTurns(int i) {
            this.f3042m = i;
        }

        public void setPathList(List<LatLng> list) {
            this.f3033d = list;
        }

        public void setPathString(String str) {
            this.f3038i = str;
        }

        public void setTrafficList(int[] iArr) {
            this.f3034e = iArr;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f3035f);
            parcel.writeParcelable(this.f3036g, 1);
            parcel.writeParcelable(this.f3037h, 1);
            parcel.writeString(this.f3038i);
            parcel.writeString(this.f3039j);
            parcel.writeString(this.f3040k);
            parcel.writeString(this.f3041l);
            parcel.writeInt(this.f3042m);
            parcel.writeTypedList(this.f3033d);
            parcel.writeIntArray(this.f3034e);
        }
    }

    public DrivingRouteLine() {
    }

    protected DrivingRouteLine(Parcel parcel) {
        super(parcel);
        this.f3029b = parcel.readByte() != 0;
        this.f3030c = new ArrayList();
        parcel.readList(this.f3030c, RouteNode.class.getClassLoader());
        this.f3031d = parcel.readInt();
        this.f3032e = parcel.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public int getCongestionDistance() {
        return this.f3031d;
    }

    public int getLightNum() {
        return this.f3032e;
    }

    public List<RouteNode> getWayPoints() {
        return this.f3030c;
    }

    @Deprecated
    public boolean isSupportTraffic() {
        return this.f3029b;
    }

    public void setCongestionDistance(int i) {
        this.f3031d = i;
    }

    public void setLightNum(int i) {
        this.f3032e = i;
    }

    public void setSupportTraffic(boolean z) {
        this.f3029b = z;
    }

    public void setWayPoints(List<RouteNode> list) {
        this.f3030c = list;
    }

    public void writeToParcel(Parcel parcel, int i) {
        super.setType(RouteLine.TYPE.DRIVESTEP);
        super.writeToParcel(parcel, i);
        parcel.writeByte(this.f3029b ? (byte) 1 : 0);
        parcel.writeList(this.f3030c);
        parcel.writeInt(this.f3031d);
        parcel.writeInt(this.f3032e);
    }
}
