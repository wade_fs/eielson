package com.baidu.lbsapi.auth;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import com.baidu.android.bbalbs.common.util.CommonParam;
import com.google.android.gms.dynamite.ProviderConstants;
import com.tamic.novate.download.MimeType;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class LBSAuthManager {
    public static final int CODE_AUTHENTICATE_SUCC = 0;
    public static final int CODE_AUTHENTICATING = 602;
    public static final int CODE_INNER_ERROR = -1;
    public static final int CODE_KEY_NOT_EXIST = 101;
    public static final int CODE_NETWORK_FAILED = -11;
    public static final int CODE_NETWORK_INVALID = -10;
    public static final int CODE_UNAUTHENTICATE = 601;
    public static final String VERSION = "1.0.23";
    /* access modifiers changed from: private */

    /* renamed from: a */
    public static Context f1068a;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public static C0717m f1069d;

    /* renamed from: e */
    private static int f1070e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public static Hashtable<String, LBSAuthManagerListener> f1071f = new Hashtable<>();

    /* renamed from: g */
    private static LBSAuthManager f1072g;

    /* renamed from: b */
    private C0705c f1073b = null;

    /* renamed from: c */
    private C0708e f1074c = null;

    /* renamed from: h */
    private boolean f1075h = false;

    /* renamed from: i */
    private final Handler f1076i = new C0713i(this, Looper.getMainLooper());

    private LBSAuthManager(Context context) {
        f1068a = context;
        C0717m mVar = f1069d;
        if (mVar != null && !mVar.isAlive()) {
            f1069d = null;
        }
        C0702a.m1617b("BaiduApiAuth SDK Version:1.0.23");
        m1613d();
    }

    /* renamed from: a */
    private int m1597a(String str) {
        int i;
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                jSONObject.put(NotificationCompat.CATEGORY_STATUS, -1);
            }
            int i2 = jSONObject.getInt(NotificationCompat.CATEGORY_STATUS);
            try {
                if (jSONObject.has("current") && i2 == 0) {
                    long j = jSONObject.getLong("current");
                    long currentTimeMillis = System.currentTimeMillis();
                    double d = (double) (currentTimeMillis - j);
                    Double.isNaN(d);
                    if (d / 3600000.0d < 24.0d) {
                        if (this.f1075h) {
                            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            if (!simpleDateFormat.format(Long.valueOf(currentTimeMillis)).equals(simpleDateFormat.format(Long.valueOf(j)))) {
                            }
                        }
                    }
                    i2 = CODE_UNAUTHENTICATE;
                }
                if (jSONObject.has("current") && i2 == 602) {
                    if (((double) ((System.currentTimeMillis() - jSONObject.getLong("current")) / 1000)) > 180.0d) {
                        return CODE_UNAUTHENTICATE;
                    }
                }
                return i2;
            } catch (JSONException e) {
                e = e;
                i = i2;
                e.printStackTrace();
                return i;
            }
        } catch (JSONException e2) {
            e = e2;
            i = -1;
            e.printStackTrace();
            return i;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:14:? */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:13:? */
    /* JADX INFO: additional move instructions added (6) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.String] */
    /* JADX WARN: Type inference failed for: r0v2, types: [java.io.BufferedReader] */
    /* JADX WARN: Type inference failed for: r0v3 */
    /* JADX WARN: Type inference failed for: r0v5 */
    /* JADX WARN: Type inference failed for: r1v14 */
    /* JADX WARN: Type inference failed for: r1v15 */
    /* JADX WARN: Type inference failed for: r0v7 */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0076, code lost:
        if (r6 == null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x0086, code lost:
        if (r6 == null) goto L_0x0089;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x0060  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0065  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x007e  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0083  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m1598a(int r6) throws java.io.IOException {
        /*
            r5 = this;
            r0 = 0
            java.io.File r1 = new java.io.File     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            r2.<init>()     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            java.lang.String r3 = "/proc/"
            r2.append(r3)     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            r2.append(r6)     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            java.lang.String r6 = "/cmdline"
            r2.append(r6)     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            java.lang.String r6 = r2.toString()     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            r1.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            r6.<init>(r1)     // Catch:{ FileNotFoundException -> 0x0079, IOException -> 0x0069, all -> 0x0056 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x004d }
            r1.<init>(r6)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x004d }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0049, all -> 0x0044 }
            r2.<init>(r1)     // Catch:{ FileNotFoundException -> 0x004b, IOException -> 0x0049, all -> 0x0044 }
            java.lang.String r0 = r2.readLine()     // Catch:{ FileNotFoundException -> 0x0042, IOException -> 0x0040, all -> 0x003a }
            r2.close()
            r1.close()
        L_0x0035:
            r6.close()
            goto L_0x0089
        L_0x003a:
            r0 = move-exception
            r4 = r2
            r2 = r6
            r6 = r0
            r0 = r4
            goto L_0x0059
        L_0x0040:
            goto L_0x006c
        L_0x0042:
            goto L_0x007c
        L_0x0044:
            r2 = move-exception
            r4 = r2
            r2 = r6
            r6 = r4
            goto L_0x0059
        L_0x0049:
            r2 = r0
            goto L_0x006c
        L_0x004b:
            r2 = r0
            goto L_0x007c
        L_0x004d:
            r1 = move-exception
            r2 = r6
            r6 = r1
            r1 = r0
            goto L_0x0059
        L_0x0052:
            r1 = r0
            goto L_0x006b
        L_0x0054:
            r1 = r0
            goto L_0x007b
        L_0x0056:
            r6 = move-exception
            r1 = r0
            r2 = r1
        L_0x0059:
            if (r0 == 0) goto L_0x005e
            r0.close()
        L_0x005e:
            if (r1 == 0) goto L_0x0063
            r1.close()
        L_0x0063:
            if (r2 == 0) goto L_0x0068
            r2.close()
        L_0x0068:
            throw r6
        L_0x0069:
            r6 = r0
            r1 = r6
        L_0x006b:
            r2 = r1
        L_0x006c:
            if (r2 == 0) goto L_0x0071
            r2.close()
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()
        L_0x0076:
            if (r6 == 0) goto L_0x0089
            goto L_0x0035
        L_0x0079:
            r6 = r0
            r1 = r6
        L_0x007b:
            r2 = r1
        L_0x007c:
            if (r2 == 0) goto L_0x0081
            r2.close()
        L_0x0081:
            if (r1 == 0) goto L_0x0086
            r1.close()
        L_0x0086:
            if (r6 == 0) goto L_0x0089
            goto L_0x0035
        L_0x0089:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.lbsapi.auth.LBSAuthManager.m1598a(int):java.lang.String");
    }

    /* renamed from: a */
    private String m1599a(Context context) {
        int myPid = Process.myPid();
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = ((ActivityManager) context.getSystemService("activity")).getRunningAppProcesses();
        if (runningAppProcesses != null) {
            for (ActivityManager.RunningAppProcessInfo runningAppProcessInfo : runningAppProcesses) {
                if (runningAppProcessInfo.pid == myPid) {
                    return runningAppProcessInfo.processName;
                }
            }
        }
        String str = null;
        try {
            str = m1598a(myPid);
        } catch (IOException unused) {
        }
        return str != null ? str : f1068a.getPackageName();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:26:? A[RETURN, SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String m1600a(android.content.Context r6, java.lang.String r7) {
        /*
            r5 = this;
            java.lang.String r0 = "无法在AndroidManifest.xml中获取com.baidu.android.lbs.API_KEY的值"
            java.lang.String r1 = ""
            java.lang.String r2 = r6.getPackageName()
            r3 = 101(0x65, float:1.42E-43)
            android.content.pm.PackageManager r6 = r6.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0051 }
            r4 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r6 = r6.getApplicationInfo(r2, r4)     // Catch:{ NameNotFoundException -> 0x0051 }
            android.os.Bundle r2 = r6.metaData     // Catch:{ NameNotFoundException -> 0x0051 }
            if (r2 != 0) goto L_0x002d
            java.util.Hashtable<java.lang.String, com.baidu.lbsapi.auth.LBSAuthManagerListener> r6 = com.baidu.lbsapi.auth.LBSAuthManager.f1071f     // Catch:{ NameNotFoundException -> 0x0051 }
            java.lang.Object r6 = r6.get(r7)     // Catch:{ NameNotFoundException -> 0x0051 }
            com.baidu.lbsapi.auth.LBSAuthManagerListener r6 = (com.baidu.lbsapi.auth.LBSAuthManagerListener) r6     // Catch:{ NameNotFoundException -> 0x0051 }
            if (r6 == 0) goto L_0x002b
            java.lang.String r2 = "AndroidManifest.xml的application中没有meta-data标签"
            java.lang.String r2 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r3, r2)     // Catch:{ NameNotFoundException -> 0x0051 }
            r6.onAuthResult(r3, r2)     // Catch:{ NameNotFoundException -> 0x0051 }
        L_0x002b:
            r6 = r1
            goto L_0x0063
        L_0x002d:
            android.os.Bundle r6 = r6.metaData     // Catch:{ NameNotFoundException -> 0x0051 }
            java.lang.String r2 = "com.baidu.lbsapi.API_KEY"
            java.lang.String r6 = r6.getString(r2)     // Catch:{ NameNotFoundException -> 0x0051 }
            if (r6 == 0) goto L_0x003d
            boolean r1 = r6.equals(r1)     // Catch:{ NameNotFoundException -> 0x004f }
            if (r1 == 0) goto L_0x0063
        L_0x003d:
            java.util.Hashtable<java.lang.String, com.baidu.lbsapi.auth.LBSAuthManagerListener> r1 = com.baidu.lbsapi.auth.LBSAuthManager.f1071f     // Catch:{ NameNotFoundException -> 0x004f }
            java.lang.Object r1 = r1.get(r7)     // Catch:{ NameNotFoundException -> 0x004f }
            com.baidu.lbsapi.auth.LBSAuthManagerListener r1 = (com.baidu.lbsapi.auth.LBSAuthManagerListener) r1     // Catch:{ NameNotFoundException -> 0x004f }
            if (r1 == 0) goto L_0x0063
            java.lang.String r2 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r3, r0)     // Catch:{ NameNotFoundException -> 0x004f }
            r1.onAuthResult(r3, r2)     // Catch:{ NameNotFoundException -> 0x004f }
            goto L_0x0063
        L_0x004f:
            goto L_0x0052
        L_0x0051:
            r6 = r1
        L_0x0052:
            java.util.Hashtable<java.lang.String, com.baidu.lbsapi.auth.LBSAuthManagerListener> r1 = com.baidu.lbsapi.auth.LBSAuthManager.f1071f
            java.lang.Object r7 = r1.get(r7)
            com.baidu.lbsapi.auth.LBSAuthManagerListener r7 = (com.baidu.lbsapi.auth.LBSAuthManagerListener) r7
            if (r7 == 0) goto L_0x0063
            java.lang.String r0 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r3, r0)
            r7.onAuthResult(r3, r0)
        L_0x0063:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.lbsapi.auth.LBSAuthManager.m1600a(android.content.Context, java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public synchronized void m1605a(String str, String str2) {
        if (str == null) {
            str = m1614e();
        }
        Message obtainMessage = this.f1076i.obtainMessage();
        try {
            JSONObject jSONObject = new JSONObject(str);
            if (!jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                jSONObject.put(NotificationCompat.CATEGORY_STATUS, -1);
            }
            if (!jSONObject.has("current")) {
                jSONObject.put("current", System.currentTimeMillis());
            }
            m1612c(jSONObject.toString());
            if (jSONObject.has("current")) {
                jSONObject.remove("current");
            }
            obtainMessage.what = jSONObject.getInt(NotificationCompat.CATEGORY_STATUS);
            obtainMessage.obj = jSONObject.toString();
            Bundle bundle = new Bundle();
            bundle.putString("listenerKey", str2);
            obtainMessage.setData(bundle);
            this.f1076i.sendMessage(obtainMessage);
        } catch (JSONException e) {
            e.printStackTrace();
            obtainMessage.what = -1;
            obtainMessage.obj = new JSONObject();
            Bundle bundle2 = new Bundle();
            bundle2.putString("listenerKey", str2);
            obtainMessage.setData(bundle2);
            this.f1076i.sendMessage(obtainMessage);
        }
        if (f1069d != null) {
            f1069d.mo10218c();
        }
        f1070e--;
        C0702a.m1616a("httpRequest called mAuthCounter-- = " + f1070e);
        if (f1070e == 0 && f1069d != null) {
            f1069d.mo10216a();
            f1069d = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1606a(boolean z, String str, Hashtable<String, String> hashtable, String str2) {
        String str3;
        String str4;
        String str5;
        String a = m1600a(f1068a, str2);
        if (a != null && !a.equals("")) {
            HashMap hashMap = new HashMap();
            hashMap.put("url", "https://api.map.baidu.com/sdkcs/verify");
            C0702a.m1616a("url:https://api.map.baidu.com/sdkcs/verify");
            hashMap.put("output", MimeType.JSON);
            hashMap.put("ak", a);
            C0702a.m1616a("ak:" + a);
            hashMap.put("mcode", C0703b.m1620a(f1068a));
            hashMap.put("from", "lbs_yunsdk");
            if (hashtable != null && hashtable.size() > 0) {
                for (Map.Entry entry : hashtable.entrySet()) {
                    String str6 = (String) entry.getKey();
                    String str7 = (String) entry.getValue();
                    if (!TextUtils.isEmpty(str6) && !TextUtils.isEmpty(str7)) {
                        hashMap.put(str6, str7);
                    }
                }
            }
            try {
                str3 = CommonParam.m1545a(f1068a);
            } catch (Exception e) {
                C0702a.m1616a("get cuid failed");
                e.printStackTrace();
                str3 = "";
            }
            C0702a.m1616a("cuid:" + str3);
            if (!TextUtils.isEmpty(str3)) {
                hashMap.put("cuid", str3);
            } else {
                hashMap.put("cuid", "");
            }
            hashMap.put("pcn", f1068a.getPackageName());
            hashMap.put(ProviderConstants.API_COLNAME_FEATURE_VERSION, VERSION);
            try {
                str4 = C0703b.m1626c(f1068a);
            } catch (Exception unused) {
                str4 = "";
            }
            if (!TextUtils.isEmpty(str4)) {
                hashMap.put("macaddr", str4);
            } else {
                hashMap.put("macaddr", "");
            }
            try {
                str5 = C0703b.m1619a();
            } catch (Exception unused2) {
                str5 = "";
            }
            if (!TextUtils.isEmpty(str5)) {
                hashMap.put("language", str5);
            } else {
                hashMap.put("language", "");
            }
            if (z) {
                hashMap.put("force", z ? "1" : "0");
            }
            if (str == null) {
                hashMap.put("from_service", "");
            } else {
                hashMap.put("from_service", str);
            }
            this.f1073b = new C0705c(f1068a);
            this.f1073b.mo10203a(hashMap, new C0715k(this, str2));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1607a(boolean z, String str, Hashtable<String, String> hashtable, String[] strArr, String str2) {
        String str3;
        String str4;
        String str5;
        String a = m1600a(f1068a, str2);
        if (a != null && !a.equals("")) {
            HashMap hashMap = new HashMap();
            hashMap.put("url", "https://api.map.baidu.com/sdkcs/verify");
            hashMap.put("output", MimeType.JSON);
            hashMap.put("ak", a);
            hashMap.put("from", "lbs_yunsdk");
            if (hashtable != null && hashtable.size() > 0) {
                for (Map.Entry entry : hashtable.entrySet()) {
                    String str6 = (String) entry.getKey();
                    String str7 = (String) entry.getValue();
                    if (!TextUtils.isEmpty(str6) && !TextUtils.isEmpty(str7)) {
                        hashMap.put(str6, str7);
                    }
                }
            }
            try {
                str3 = CommonParam.m1545a(f1068a);
            } catch (Exception unused) {
                str3 = "";
            }
            if (!TextUtils.isEmpty(str3)) {
                hashMap.put("cuid", str3);
            } else {
                hashMap.put("cuid", "");
            }
            hashMap.put("pcn", f1068a.getPackageName());
            hashMap.put(ProviderConstants.API_COLNAME_FEATURE_VERSION, VERSION);
            try {
                str4 = C0703b.m1626c(f1068a);
            } catch (Exception unused2) {
                str4 = "";
            }
            if (!TextUtils.isEmpty(str4)) {
                hashMap.put("macaddr", str4);
            } else {
                hashMap.put("macaddr", "");
            }
            try {
                str5 = C0703b.m1619a();
            } catch (Exception unused3) {
                str5 = "";
            }
            if (!TextUtils.isEmpty(str5)) {
                hashMap.put("language", str5);
            } else {
                hashMap.put("language", "");
            }
            if (z) {
                hashMap.put("force", z ? "1" : "0");
            }
            if (str == null) {
                hashMap.put("from_service", "");
            } else {
                hashMap.put("from_service", str);
            }
            this.f1074c = new C0708e(f1068a);
            this.f1074c.mo10206a(hashMap, strArr, new C0716l(this, str2));
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: b */
    public boolean m1610b(String str) {
        String str2;
        String a = m1600a(f1068a, str);
        try {
            JSONObject jSONObject = new JSONObject(m1614e());
            if (!jSONObject.has("ak")) {
                return true;
            }
            str2 = jSONObject.getString("ak");
            return (a == null || str2 == null || a.equals(str2)) ? false : true;
        } catch (JSONException e) {
            e.printStackTrace();
            str2 = "";
        }
    }

    /* renamed from: c */
    private void m1612c(String str) {
        Context context = f1068a;
        context.getSharedPreferences("authStatus_" + m1599a(f1068a), 0).edit().putString(NotificationCompat.CATEGORY_STATUS, str).commit();
    }

    /* renamed from: d */
    private void m1613d() {
        synchronized (LBSAuthManager.class) {
            if (f1069d == null) {
                f1069d = new C0717m("auth");
                f1069d.start();
                while (f1069d.f1103a == null) {
                    try {
                        C0702a.m1616a("wait for create auth thread.");
                        Thread.sleep(3);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /* renamed from: e */
    private String m1614e() {
        Context context = f1068a;
        return context.getSharedPreferences("authStatus_" + m1599a(f1068a), 0).getString(NotificationCompat.CATEGORY_STATUS, "{\"status\":601}");
    }

    public static LBSAuthManager getInstance(Context context) {
        if (f1072g == null) {
            synchronized (LBSAuthManager.class) {
                if (f1072g == null) {
                    f1072g = new LBSAuthManager(context);
                }
            }
        } else if (context != null) {
            f1068a = context;
        } else if (C0702a.f1077a) {
            C0702a.m1618c("input context is null");
            new RuntimeException("here").printStackTrace();
        }
        return f1072g;
    }

    public int authenticate(boolean z, String str, Hashtable<String, String> hashtable, LBSAuthManagerListener lBSAuthManagerListener) {
        synchronized (LBSAuthManager.class) {
            boolean z2 = false;
            if (hashtable != null) {
                String str2 = hashtable.get("zero_auth");
                if (str2 != null) {
                    if (Integer.valueOf(str2).intValue() == 1) {
                        z2 = true;
                    }
                }
            }
            this.f1075h = z2;
            String str3 = System.currentTimeMillis() + "";
            if (lBSAuthManagerListener != null) {
                f1071f.put(str3, lBSAuthManagerListener);
            }
            String a = m1600a(f1068a, str3);
            if (a != null) {
                if (!a.equals("")) {
                    f1070e++;
                    C0702a.m1616a(" mAuthCounter  ++ = " + f1070e);
                    String e = m1614e();
                    C0702a.m1616a("getAuthMessage from cache:" + e);
                    int a2 = m1597a(e);
                    if (a2 == 601) {
                        try {
                            m1612c(new JSONObject().put(NotificationCompat.CATEGORY_STATUS, (int) CODE_AUTHENTICATING).toString());
                        } catch (JSONException e2) {
                            e2.printStackTrace();
                        }
                    }
                    m1613d();
                    if (f1069d != null) {
                        if (f1069d.f1103a != null) {
                            C0702a.m1616a("mThreadLooper.mHandler = " + f1069d.f1103a);
                            f1069d.f1103a.post(new C0714j(this, a2, z, str3, str, hashtable));
                            return a2;
                        }
                    }
                    return -1;
                }
            }
            return 101;
        }
    }

    public String getCUID() {
        Context context = f1068a;
        if (context == null) {
            return "";
        }
        try {
            return CommonParam.m1545a(context);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getKey() {
        Context context = f1068a;
        if (context == null) {
            return "";
        }
        try {
            return getPublicKey(context);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getMCode() {
        Context context = f1068a;
        return context == null ? "" : C0703b.m1620a(context);
    }

    public String getPublicKey(Context context) throws PackageManager.NameNotFoundException {
        return context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData.getString("com.baidu.lbsapi.API_KEY");
    }
}
