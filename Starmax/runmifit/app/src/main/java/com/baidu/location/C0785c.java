package com.baidu.location;

import com.baidu.location.p013a.C0727c;

/* renamed from: com.baidu.location.c */
class C0785c extends Thread {

    /* renamed from: a */
    final /* synthetic */ LocationClient f1547a;

    C0785c(LocationClient locationClient) {
        this.f1547a = locationClient;
    }

    public void run() {
        try {
            if (this.f1547a.f1168E == null) {
                C0727c unused = this.f1547a.f1168E = new C0727c(this.f1547a.f1178f, this.f1547a.f1176d, this.f1547a);
            }
            if (this.f1547a.f1168E != null) {
                this.f1547a.f1168E.mo10443b();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
