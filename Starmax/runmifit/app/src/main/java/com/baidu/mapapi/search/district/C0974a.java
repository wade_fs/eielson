package com.baidu.mapapi.search.district;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.district.a */
final class C0974a implements Parcelable.Creator<DistrictResult> {
    C0974a() {
    }

    /* renamed from: a */
    public DistrictResult createFromParcel(Parcel parcel) {
        return new DistrictResult(parcel);
    }

    /* renamed from: a */
    public DistrictResult[] newArray(int i) {
        return new DistrictResult[i];
    }
}
