package com.baidu.mapapi.map;

import android.graphics.Bitmap;
import android.view.MotionEvent;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;
import com.baidu.mapsdkplatform.comapi.map.C1087l;
import javax.microedition.khronos.opengles.GL10;

/* renamed from: com.baidu.mapapi.map.l */
class C0935l implements C1087l {

    /* renamed from: a */
    final /* synthetic */ MapView f2852a;

    C0935l(MapView mapView) {
        this.f2852a = mapView;
    }

    /* renamed from: a */
    public void mo11538a() {
        String str;
        if (this.f2852a.f2573e != null && this.f2852a.f2573e.mo13086a() != null) {
            float f = this.f2852a.f2573e.mo13086a().mo12989E().f3414a;
            if (f < this.f2852a.f2573e.mo13086a().f3527b) {
                f = this.f2852a.f2573e.mo13086a().f3527b;
            } else if (f > this.f2852a.f2573e.mo13086a().f3505a) {
                f = this.f2852a.f2573e.mo13086a().f3505a;
            }
            if (Math.abs(this.f2852a.f2588u - f) > 0.0f) {
                int intValue = ((Integer) MapView.f2570q.get(Math.round(f))).intValue();
                double d = (double) intValue;
                double d2 = this.f2852a.f2573e.mo13086a().mo12989E().f3426m;
                Double.isNaN(d);
                int i = ((int) (d / d2)) / 2;
                this.f2852a.f2583o.setPadding(i, 0, i, 0);
                Object[] objArr = new Object[1];
                if (intValue >= 1000) {
                    objArr[0] = Integer.valueOf(intValue / 1000);
                    str = String.format(" %d公里 ", objArr);
                } else {
                    objArr[0] = Integer.valueOf(intValue);
                    str = String.format(" %d米 ", objArr);
                }
                this.f2852a.f2581m.setText(str);
                this.f2852a.f2582n.setText(str);
                float unused = this.f2852a.f2588u = f;
            }
            this.f2852a.m2928b();
            this.f2852a.requestLayout();
        }
    }

    /* renamed from: a */
    public void mo11539a(Bitmap bitmap) {
    }

    /* renamed from: a */
    public void mo11540a(MotionEvent motionEvent) {
    }

    /* renamed from: a */
    public void mo11541a(GeoPoint geoPoint) {
    }

    /* renamed from: a */
    public void mo11542a(C1064ab abVar) {
    }

    /* renamed from: a */
    public void mo11543a(String str) {
    }

    /* renamed from: a */
    public void mo11544a(GL10 gl10, C1064ab abVar) {
    }

    /* renamed from: a */
    public void mo11545a(boolean z) {
    }

    /* renamed from: a */
    public void mo11546a(boolean z, int i) {
    }

    /* renamed from: b */
    public void mo11547b() {
    }

    /* renamed from: b */
    public void mo11548b(GeoPoint geoPoint) {
    }

    /* renamed from: b */
    public void mo11549b(C1064ab abVar) {
    }

    /* renamed from: b */
    public boolean mo11550b(String str) {
        return false;
    }

    /* renamed from: c */
    public void mo11551c() {
    }

    /* renamed from: c */
    public void mo11552c(GeoPoint geoPoint) {
    }

    /* renamed from: c */
    public void mo11553c(C1064ab abVar) {
    }

    /* renamed from: d */
    public void mo11554d() {
    }

    /* renamed from: d */
    public void mo11555d(GeoPoint geoPoint) {
    }

    /* renamed from: e */
    public void mo11556e() {
    }

    /* renamed from: e */
    public void mo11557e(GeoPoint geoPoint) {
    }

    /* renamed from: f */
    public void mo11558f() {
    }
}
