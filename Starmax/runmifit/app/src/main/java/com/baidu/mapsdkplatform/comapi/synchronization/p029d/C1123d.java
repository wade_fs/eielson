package com.baidu.mapsdkplatform.comapi.synchronization.p029d;

import com.baidu.mapsdkplatform.comjni.util.AppMD5;
import java.util.LinkedHashMap;
import java.util.Map;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.d.d */
public final class C1123d {

    /* renamed from: a */
    private Map<String, String> f3699a;

    /* renamed from: a */
    public C1123d mo13200a(String str, String str2) {
        if (this.f3699a == null) {
            this.f3699a = new LinkedHashMap();
        }
        this.f3699a.put(str, str2);
        return this;
    }

    /* renamed from: a */
    public String mo13201a() {
        StringBuilder sb;
        Map<String, String> map = this.f3699a;
        if (map == null || map.isEmpty()) {
            return null;
        }
        String str = new String();
        int i = 0;
        for (String str2 : this.f3699a.keySet()) {
            String encodeUrlParamsValue = AppMD5.encodeUrlParamsValue(this.f3699a.get(str2));
            if (i != 0) {
                sb = new StringBuilder();
                sb.append(str);
                str = "&";
            }
            sb.append(str);
            sb.append(str2);
            sb.append("=");
            sb.append(encodeUrlParamsValue);
            str = sb.toString();
            i++;
        }
        return str;
    }
}
