package com.baidu.mapapi.synchronization;

import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.synchronization.SyncCoordinateConverter;

public final class RoleOptions {

    /* renamed from: a */
    private static final String f3180a = RoleOptions.class.getSimpleName();

    /* renamed from: b */
    private String f3181b;

    /* renamed from: c */
    private int f3182c;

    /* renamed from: d */
    private String f3183d;

    /* renamed from: e */
    private String f3184e;

    /* renamed from: f */
    private SyncCoordinateConverter.CoordType f3185f;

    /* renamed from: g */
    private LatLng f3186g;

    /* renamed from: h */
    private String f3187h;

    /* renamed from: i */
    private String f3188i;

    /* renamed from: j */
    private LatLng f3189j;

    /* renamed from: k */
    private String f3190k;

    /* renamed from: l */
    private String f3191l;

    /* renamed from: m */
    private LatLng f3192m;

    /* renamed from: n */
    private String f3193n;

    /* renamed from: o */
    private String f3194o;

    public RoleOptions() {
        this.f3181b = null;
        this.f3182c = 0;
        this.f3183d = null;
        this.f3184e = null;
        this.f3185f = SyncCoordinateConverter.CoordType.BD09LL;
        this.f3181b = null;
        this.f3182c = 0;
        this.f3183d = null;
        this.f3184e = null;
        this.f3186g = null;
        this.f3187h = null;
        this.f3188i = null;
        this.f3189j = null;
        this.f3190k = null;
        this.f3191l = null;
        this.f3192m = null;
        this.f3193n = null;
        this.f3194o = null;
        this.f3185f = SyncCoordinateConverter.CoordType.BD09LL;
    }

    /* renamed from: a */
    private LatLng m3252a(LatLng latLng) {
        return new SyncCoordinateConverter().from(this.f3185f).coord(latLng).convert();
    }

    public SyncCoordinateConverter.CoordType getCoordType() {
        return this.f3185f;
    }

    public String getDriverId() {
        return this.f3183d;
    }

    public LatLng getDriverPosition() {
        return this.f3192m;
    }

    public String getDriverPositionName() {
        return this.f3194o;
    }

    public String getDriverPositionPoiUid() {
        return this.f3193n;
    }

    public LatLng getEndPosition() {
        return this.f3189j;
    }

    public String getEndPositionName() {
        return this.f3191l;
    }

    public String getEndPositionPoiUid() {
        return this.f3190k;
    }

    public String getOrderId() {
        return this.f3181b;
    }

    public int getRoleType() {
        return this.f3182c;
    }

    public LatLng getStartPosition() {
        return this.f3186g;
    }

    public String getStartPositionName() {
        return this.f3188i;
    }

    public String getStartPositionPoiUid() {
        return this.f3187h;
    }

    public String getUserId() {
        return this.f3184e;
    }

    public RoleOptions setCoordType(SyncCoordinateConverter.CoordType coordType) {
        if (SyncCoordinateConverter.CoordType.BD09LL == coordType || SyncCoordinateConverter.CoordType.COMMON == coordType) {
            this.f3185f = coordType;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: CoordType only can be BD09LL or COMMON, please check!");
    }

    public RoleOptions setDriverId(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("BDMapSDKException: driverId is null");
        }
        this.f3183d = str;
        return this;
    }

    public RoleOptions setDriverPosition(LatLng latLng) {
        if (latLng == null) {
            this.f3192m = null;
            return this;
        }
        if (SyncCoordinateConverter.CoordType.COMMON == this.f3185f) {
            latLng = m3252a(latLng);
        }
        this.f3192m = latLng;
        return this;
    }

    public RoleOptions setDriverPositionName(String str) {
        this.f3194o = str;
        return this;
    }

    public RoleOptions setDriverPositionPoiUid(String str) {
        this.f3193n = str;
        return this;
    }

    public RoleOptions setEndPosition(LatLng latLng) {
        if (latLng != null) {
            if (SyncCoordinateConverter.CoordType.COMMON == this.f3185f) {
                latLng = m3252a(latLng);
            }
            this.f3189j = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: endPosition is null, must be applied!");
    }

    public RoleOptions setEndPositionName(String str) {
        this.f3191l = str;
        return this;
    }

    public RoleOptions setEndPositionPoiUid(String str) {
        this.f3190k = str;
        return this;
    }

    public RoleOptions setOrderId(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("BDMapSDKException: orderId is null.");
        }
        this.f3181b = str;
        return this;
    }

    public RoleOptions setRoleType(int i) {
        if (i == 0) {
            this.f3182c = i;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: role type is invalid: " + i);
    }

    public RoleOptions setStartPosition(LatLng latLng) {
        if (latLng != null) {
            if (SyncCoordinateConverter.CoordType.COMMON == this.f3185f) {
                latLng = m3252a(latLng);
            }
            this.f3186g = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: StartPosition is null, must be applied!");
    }

    public RoleOptions setStartPositionName(String str) {
        this.f3188i = str;
        return this;
    }

    public RoleOptions setStartPositionPoiUid(String str) {
        this.f3187h = str;
        return this;
    }

    public RoleOptions setUserId(String str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("BDMapSDKException: user id is null");
        }
        this.f3184e = str;
        return this;
    }
}
