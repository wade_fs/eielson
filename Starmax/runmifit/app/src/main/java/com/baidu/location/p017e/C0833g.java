package com.baidu.location.p017e;

import android.location.Location;
import android.os.Handler;
import android.os.Message;
import com.baidu.location.C0839f;

/* renamed from: com.baidu.location.e.g */
class C0833g extends Handler {

    /* renamed from: a */
    final /* synthetic */ C0826e f1788a;

    C0833g(C0826e eVar) {
        this.f1788a = eVar;
    }

    public void handleMessage(Message message) {
        Location location;
        String str;
        C0826e eVar;
        if (C0839f.isServing) {
            int i = message.what;
            if (i != 1) {
                if (i == 3) {
                    eVar = this.f1788a;
                    location = (Location) message.obj;
                    str = "&og=1";
                } else if (i == 4) {
                    eVar = this.f1788a;
                    location = (Location) message.obj;
                    str = "&og=2";
                } else {
                    return;
                }
                eVar.m2311a(str, location);
                return;
            }
            this.f1788a.m2331e((Location) message.obj);
        }
    }
}
