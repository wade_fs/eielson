package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.core.p */
final class C0972p implements Parcelable.Creator<TransitResultNode> {
    C0972p() {
    }

    /* renamed from: a */
    public TransitResultNode createFromParcel(Parcel parcel) {
        return new TransitResultNode(parcel);
    }

    /* renamed from: a */
    public TransitResultNode[] newArray(int i) {
        return new TransitResultNode[i];
    }
}
