package com.baidu.lbsapi.auth;

import android.content.Context;
import android.support.v4.app.NotificationCompat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/* renamed from: com.baidu.lbsapi.auth.e */
class C0708e {

    /* renamed from: a */
    private Context f1083a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public List<HashMap<String, String>> f1084b = null;

    /* renamed from: c */
    private C0709a<String> f1085c = null;

    /* renamed from: com.baidu.lbsapi.auth.e$a */
    interface C0709a<Result> {
        /* renamed from: a */
        void mo10207a(Result result);
    }

    protected C0708e(Context context) {
        this.f1083a = context;
    }

    /* renamed from: a */
    private List<HashMap<String, String>> m1638a(HashMap<String, String> hashMap, String[] strArr) {
        ArrayList arrayList = new ArrayList();
        if (strArr == null || strArr.length <= 0) {
            HashMap hashMap2 = new HashMap();
            for (String str : hashMap.keySet()) {
                String str2 = str.toString();
                hashMap2.put(str2, hashMap.get(str2));
            }
            arrayList.add(hashMap2);
        } else {
            for (String str3 : strArr) {
                HashMap hashMap3 = new HashMap();
                for (String str4 : hashMap.keySet()) {
                    String str5 = str4.toString();
                    hashMap3.put(str5, hashMap.get(str5));
                }
                hashMap3.put("mcode", str3);
                arrayList.add(hashMap3);
            }
        }
        return arrayList;
    }

    /* renamed from: a */
    private void m1640a(String str) {
        JSONObject jSONObject;
        if (str == null) {
            str = "";
        }
        try {
            jSONObject = new JSONObject(str);
            if (!jSONObject.has(NotificationCompat.CATEGORY_STATUS)) {
                jSONObject.put(NotificationCompat.CATEGORY_STATUS, -1);
            }
        } catch (JSONException unused) {
            jSONObject = new JSONObject();
            try {
                jSONObject.put(NotificationCompat.CATEGORY_STATUS, -1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        C0709a<String> aVar = this.f1085c;
        if (aVar != null) {
            aVar.mo10207a(jSONObject.toString());
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m1641a(List<HashMap<String, String>> list) {
        int i;
        C0702a.m1616a("syncConnect start Thread id = " + String.valueOf(Thread.currentThread().getId()));
        if (list == null || list.size() == 0) {
            C0702a.m1618c("syncConnect failed,params list is null or size is 0");
            return;
        }
        ArrayList arrayList = new ArrayList();
        int i2 = 0;
        while (i2 < list.size()) {
            C0702a.m1616a("syncConnect resuest " + i2 + "  start!!!");
            HashMap hashMap = list.get(i2);
            C0711g gVar = new C0711g(this.f1083a);
            if (gVar.mo10210a()) {
                String a = gVar.mo10209a(hashMap);
                if (a == null) {
                    a = "";
                }
                C0702a.m1616a("syncConnect resuest " + i2 + "  result:" + a);
                arrayList.add(a);
                try {
                    JSONObject jSONObject = new JSONObject(a);
                    if (jSONObject.has(NotificationCompat.CATEGORY_STATUS) && jSONObject.getInt(NotificationCompat.CATEGORY_STATUS) == 0) {
                        C0702a.m1616a("auth end and break");
                        m1640a(a);
                        return;
                    }
                } catch (JSONException unused) {
                    C0702a.m1616a("continue-------------------------------");
                }
            } else {
                C0702a.m1616a("Current network is not available.");
                arrayList.add(ErrorMessage.m1596a("Current network is not available."));
            }
            C0702a.m1616a("syncConnect end");
            i2++;
        }
        C0702a.m1616a("--iiiiii:" + i2 + "<><>paramList.size():" + list.size() + "<><>authResults.size():" + arrayList.size());
        if (list.size() > 0 && i2 == list.size() && arrayList.size() > 0 && i2 == arrayList.size() && i2 - 1 > 0) {
            try {
                JSONObject jSONObject2 = new JSONObject((String) arrayList.get(i));
                if (jSONObject2.has(NotificationCompat.CATEGORY_STATUS) && jSONObject2.getInt(NotificationCompat.CATEGORY_STATUS) != 0) {
                    C0702a.m1616a("i-1 result is not 0,return first result");
                    m1640a((String) arrayList.get(0));
                }
            } catch (JSONException e) {
                m1640a(ErrorMessage.m1596a("JSONException:" + e.getMessage()));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo10206a(HashMap<String, String> hashMap, String[] strArr, C0709a<String> aVar) {
        this.f1084b = m1638a(hashMap, strArr);
        this.f1085c = aVar;
        new Thread(new C0710f(this)).start();
    }
}
