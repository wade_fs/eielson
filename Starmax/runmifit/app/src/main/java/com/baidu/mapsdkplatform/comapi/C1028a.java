package com.baidu.mapsdkplatform.comapi;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import com.baidu.mapapi.SDKInitializer;
import com.baidu.mapapi.VersionInfo;
import com.baidu.mapsdkplatform.comapi.util.C1169d;
import com.baidu.mapsdkplatform.comapi.util.C1175i;
import com.baidu.mapsdkplatform.comapi.util.PermissionCheck;
import com.baidu.mapsdkplatform.comapi.util.SysUpdateObservable;
import com.baidu.mapsdkplatform.comjni.tools.C1182a;

/* renamed from: com.baidu.mapsdkplatform.comapi.a */
public class C1028a implements PermissionCheck.C1163c {

    /* renamed from: a */
    private static final String f3298a = C1028a.class.getSimpleName();

    /* renamed from: g */
    private static C1028a f3299g;

    /* renamed from: h */
    private static int f3300h = -100;

    /* renamed from: b */
    private Context f3301b;

    /* renamed from: c */
    private Handler f3302c;

    /* renamed from: d */
    private C1055e f3303d;

    /* renamed from: e */
    private String f3304e;

    /* renamed from: f */
    private int f3305f;

    static {
        NativeLoader.getInstance().loadLibrary(VersionInfo.getKitName());
        C1182a.m4377b();
    }

    private C1028a() {
    }

    /* renamed from: a */
    public static C1028a m3342a() {
        if (f3299g == null) {
            f3299g = new C1028a();
        }
        return f3299g;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m3343a(Message message) {
        Intent intent;
        if (message.what != 2012) {
            if (message.arg2 == 3) {
                this.f3301b.sendBroadcast(new Intent(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR));
            }
            if (message.arg2 == 2 || message.arg2 == 404 || message.arg2 == 5 || message.arg2 == 8) {
                intent = new Intent(SDKInitializer.SDK_BROADCAST_ACTION_STRING_NETWORK_ERROR);
            } else {
                return;
            }
        } else if (message.arg1 == 0) {
            intent = new Intent(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_OK);
        } else {
            Intent intent2 = new Intent(SDKInitializer.SDK_BROADTCAST_ACTION_STRING_PERMISSION_CHECK_ERROR);
            intent2.putExtra(SDKInitializer.SDK_BROADTCAST_INTENT_EXTRA_INFO_KEY_ERROR_CODE, message.arg1);
            intent2.putExtra(SDKInitializer.SDK_BROADTCAST_INTENT_EXTRA_INFO_KEY_ERROR_MESSAGE, (String) message.obj);
            intent = intent2;
        }
        this.f3301b.sendBroadcast(intent);
    }

    /* renamed from: f */
    private void m3345f() {
        C1055e eVar;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        Context context = this.f3301b;
        if (context != null && (eVar = this.f3303d) != null) {
            context.registerReceiver(eVar, intentFilter);
        }
    }

    /* renamed from: g */
    private void m3346g() {
        Context context;
        C1055e eVar = this.f3303d;
        if (eVar != null && (context = this.f3301b) != null) {
            context.unregisterReceiver(eVar);
        }
    }

    /* renamed from: a */
    public void mo12866a(Context context) {
        this.f3301b = context;
    }

    /* renamed from: a */
    public void mo12867a(PermissionCheck.C1162b bVar) {
        if (bVar != null) {
            if (bVar.f3877a == 0) {
                C1175i.f3914d = bVar.f3881e;
                C1175i.m4266a(bVar.f3878b, bVar.f3879c);
            } else {
                Log.e("baidumapsdk", "Authentication Error\n" + bVar.toString());
            }
            if (!(bVar.f3877a == PermissionCheck.f3867b || bVar.f3877a == PermissionCheck.f3866a || bVar.f3877a == PermissionCheck.f3868c)) {
                C1169d.m4246a().mo13393a(bVar.f3882f);
            }
            if (this.f3302c != null && bVar.f3877a != f3300h) {
                f3300h = bVar.f3877a;
                Message obtainMessage = this.f3302c.obtainMessage();
                obtainMessage.what = 2012;
                obtainMessage.arg1 = bVar.f3877a;
                obtainMessage.obj = bVar.f3880d;
                this.f3302c.sendMessage(obtainMessage);
            }
        }
    }

    /* renamed from: a */
    public void mo12868a(String str) {
        this.f3304e = str;
    }

    /* renamed from: b */
    public void mo12869b() {
        if (this.f3305f == 0) {
            if (this.f3301b != null) {
                this.f3303d = new C1055e();
                m3345f();
                SysUpdateObservable.getInstance().updateNetworkInfo(this.f3301b);
            } else {
                throw new IllegalStateException("BDMapSDKException: you have not supplyed the global app context info from SDKInitializer.initialize(Context) function.");
            }
        }
        this.f3305f++;
    }

    /* renamed from: c */
    public boolean mo12870c() {
        if (this.f3301b != null) {
            this.f3302c = new C1044b(this);
            C1175i.m4269b(this.f3301b);
            C1169d.m4246a().mo13394a(this.f3301b);
            C1175i.m4275f();
            PermissionCheck.init(this.f3301b);
            PermissionCheck.setPermissionCheckResultListener(this);
            PermissionCheck.permissionCheck();
            return true;
        }
        throw new IllegalStateException("BDMapSDKException: you have not supplyed the global app context info from SDKInitializer.initialize(Context) function.");
    }

    /* renamed from: d */
    public void mo12871d() {
        this.f3305f--;
        if (this.f3305f == 0) {
            m3346g();
            C1175i.m4264a();
        }
    }

    /* renamed from: e */
    public Context mo12872e() {
        Context context = this.f3301b;
        if (context != null) {
            return context;
        }
        throw new IllegalStateException("BDMapSDKException: you have not supplyed the global app context info from SDKInitializer.initialize(Context) function.");
    }
}
