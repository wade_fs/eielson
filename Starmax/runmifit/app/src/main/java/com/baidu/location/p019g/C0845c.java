package com.baidu.location.p019g;

import java.io.File;
import java.io.RandomAccessFile;

/* renamed from: com.baidu.location.g.c */
public class C0845c {

    /* renamed from: c */
    static C0845c f1859c;

    /* renamed from: a */
    String f1860a = "firll.dat";

    /* renamed from: b */
    int f1861b = 3164;

    /* renamed from: d */
    int f1862d = 0;

    /* renamed from: e */
    int f1863e = 20;

    /* renamed from: f */
    int f1864f = 40;

    /* renamed from: g */
    int f1865g = 60;

    /* renamed from: h */
    int f1866h = 80;

    /* renamed from: i */
    int f1867i = 100;

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004c, code lost:
        if (r4 != null) goto L_0x003d;
     */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0047 A[SYNTHETIC, Splitter:B:20:0x0047] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private long m2425a(int r8) {
        /*
            r7 = this;
            java.lang.String r0 = com.baidu.location.p019g.C0855k.m2472j()
            r1 = -1
            if (r0 != 0) goto L_0x0009
            return r1
        L_0x0009:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            java.lang.String r0 = java.io.File.separator
            r3.append(r0)
            java.lang.String r0 = r7.f1860a
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r3 = 0
            java.io.RandomAccessFile r4 = new java.io.RandomAccessFile     // Catch:{ Exception -> 0x004b, all -> 0x0043 }
            java.lang.String r5 = "rw"
            r4.<init>(r0, r5)     // Catch:{ Exception -> 0x004b, all -> 0x0043 }
            long r5 = (long) r8
            r4.seek(r5)     // Catch:{ Exception -> 0x004c, all -> 0x0041 }
            int r8 = r4.readInt()     // Catch:{ Exception -> 0x004c, all -> 0x0041 }
            long r5 = r4.readLong()     // Catch:{ Exception -> 0x004c, all -> 0x0041 }
            int r0 = r4.readInt()     // Catch:{ Exception -> 0x004c, all -> 0x0041 }
            if (r8 != r0) goto L_0x003d
            r4.close()     // Catch:{ IOException -> 0x003c }
        L_0x003c:
            return r5
        L_0x003d:
            r4.close()     // Catch:{ IOException -> 0x004f }
            goto L_0x004f
        L_0x0041:
            r8 = move-exception
            goto L_0x0045
        L_0x0043:
            r8 = move-exception
            r4 = r3
        L_0x0045:
            if (r4 == 0) goto L_0x004a
            r4.close()     // Catch:{ IOException -> 0x004a }
        L_0x004a:
            throw r8
        L_0x004b:
            r4 = r3
        L_0x004c:
            if (r4 == 0) goto L_0x004f
            goto L_0x003d
        L_0x004f:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p019g.C0845c.m2425a(int):long");
    }

    /* renamed from: a */
    public static C0845c m2426a() {
        if (f1859c == null) {
            f1859c = new C0845c();
        }
        return f1859c;
    }

    /* renamed from: a */
    private void m2427a(int i, long j) {
        String j2 = C0855k.m2472j();
        if (j2 != null) {
            try {
                RandomAccessFile randomAccessFile = new RandomAccessFile(j2 + File.separator + this.f1860a, "rw");
                randomAccessFile.seek((long) i);
                randomAccessFile.writeInt(this.f1861b);
                randomAccessFile.writeLong(j);
                randomAccessFile.writeInt(this.f1861b);
                randomAccessFile.close();
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: a */
    public void mo10720a(long j) {
        m2427a(this.f1862d, j);
    }

    /* renamed from: b */
    public long mo10721b() {
        return m2425a(this.f1862d);
    }

    /* renamed from: b */
    public void mo10722b(long j) {
        m2427a(this.f1863e, j);
    }

    /* renamed from: c */
    public long mo10723c() {
        return m2425a(this.f1863e);
    }

    /* renamed from: c */
    public void mo10724c(long j) {
        m2427a(this.f1865g, j);
    }

    /* renamed from: d */
    public long mo10725d() {
        return m2425a(this.f1865g);
    }
}
