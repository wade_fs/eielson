package com.baidu.mobstat;

import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

/* renamed from: com.baidu.mobstat.an */
public class C1245an {

    /* renamed from: a */
    private static final C1245an f4285a = new C1245an();

    /* renamed from: b */
    private HashMap<String, String> f4286b = new HashMap<>();

    /* renamed from: c */
    private HashMap<Character, Integer> f4287c = new HashMap<>();

    /* renamed from: d */
    private HashMap<String, String> f4288d = new HashMap<>();

    /* renamed from: e */
    private HashMap<Character, Integer> f4289e = new HashMap<>();

    /* renamed from: f */
    private HashMap<String, String> f4290f = new HashMap<>();

    /* renamed from: g */
    private HashMap<Character, Integer> f4291g = new HashMap<>();

    /* renamed from: com.baidu.mobstat.an$a */
    public static class C1247a {

        /* renamed from: a */
        public static int f4293a = 0;

        /* renamed from: b */
        public static int f4294b = 1;

        /* renamed from: c */
        public static int f4295c = 2;
    }

    /* renamed from: a */
    public static C1245an m4584a() {
        return f4285a;
    }

    /* renamed from: a */
    public String mo13926a(String str, int i) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (i == C1247a.f4293a) {
            String str2 = this.f4286b.get(str);
            if (!TextUtils.isEmpty(str2)) {
                return str2;
            }
            m4585a(str, this.f4287c, this.f4286b);
            return this.f4286b.get(str);
        } else if (i == C1247a.f4295c) {
            String str3 = this.f4290f.get(str);
            if (!TextUtils.isEmpty(str3)) {
                return str3;
            }
            m4585a(str, this.f4291g, this.f4290f);
            return this.f4290f.get(str);
        } else {
            String str4 = this.f4288d.get(str);
            if (!TextUtils.isEmpty(str4)) {
                return str4;
            }
            m4585a(str, this.f4289e, this.f4288d);
            return this.f4288d.get(str);
        }
    }

    /* renamed from: a */
    private void m4585a(String str, HashMap<Character, Integer> hashMap, HashMap<String, String> hashMap2) {
        int i = 0;
        char lowerCase = Character.toLowerCase(str.charAt(0));
        Integer num = hashMap.get(Character.valueOf(lowerCase));
        if (num != null) {
            i = num.intValue() + 1;
        }
        hashMap.put(Character.valueOf(lowerCase), Integer.valueOf(i));
        hashMap2.put(str, Character.toString(lowerCase) + i);
    }

    /* renamed from: a */
    public JSONObject mo13927a(int i) {
        HashMap<String, String> hashMap;
        if (i == C1247a.f4293a) {
            hashMap = this.f4286b;
        } else if (i == C1247a.f4295c) {
            hashMap = this.f4290f;
        } else {
            hashMap = this.f4288d;
        }
        JSONObject jSONObject = new JSONObject();
        if (hashMap == null) {
            return jSONObject;
        }
        ArrayList<Map.Entry> arrayList = new ArrayList<>(hashMap.entrySet());
        try {
            Collections.sort(arrayList, new Comparator<Map.Entry<String, String>>() {
                /* class com.baidu.mobstat.C1245an.C12461 */

                /* renamed from: a */
                public int compare(Map.Entry<String, String> entry, Map.Entry<String, String> entry2) {
                    return entry.getValue().compareTo(entry2.getValue());
                }
            });
        } catch (Exception unused) {
        }
        for (Map.Entry entry : arrayList) {
            try {
                jSONObject.put((String) entry.getValue(), (String) entry.getKey());
            } catch (Exception unused2) {
            }
        }
        return jSONObject;
    }

    /* renamed from: b */
    public void mo13929b(int i) {
        if (i == C1247a.f4293a) {
            this.f4287c.clear();
            this.f4286b.clear();
        } else if (i == C1247a.f4295c) {
            this.f4291g.clear();
            this.f4290f.clear();
        } else {
            this.f4289e.clear();
            this.f4288d.clear();
        }
    }

    /* renamed from: b */
    public void mo13928b() {
        mo13929b(C1247a.f4293a);
        mo13929b(C1247a.f4295c);
        mo13929b(C1247a.f4294b);
    }
}
