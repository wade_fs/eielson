package com.baidu.mapsdkplatform.comapi.map;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.h */
public enum C1082h {
    logo,
    popup,
    marker,
    ground,
    text,
    arc,
    dot,
    circle,
    polyline,
    polygon
}
