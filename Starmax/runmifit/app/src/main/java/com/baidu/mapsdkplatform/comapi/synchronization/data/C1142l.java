package com.baidu.mapsdkplatform.comapi.synchronization.data;

import android.view.View;
import com.baidu.mapapi.synchronization.DisplayOptions;
import com.baidu.mapapi.synchronization.RoleOptions;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.data.l */
public class C1142l {

    /* renamed from: a */
    private static final String f3773a = C1142l.class.getSimpleName();

    /* renamed from: b */
    private C1127d f3774b = C1127d.m3895a();

    public C1142l(RoleOptions roleOptions, DisplayOptions displayOptions) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13264a(roleOptions, displayOptions);
        }
    }

    /* renamed from: a */
    public void mo13312a() {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13266b();
        }
    }

    /* renamed from: a */
    public void mo13313a(int i) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13260a(i);
        }
    }

    /* renamed from: a */
    public void mo13314a(View view) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13261a(view);
        }
    }

    /* renamed from: a */
    public void mo13315a(DisplayOptions displayOptions) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13262a(displayOptions);
        }
    }

    /* renamed from: a */
    public void mo13316a(RoleOptions roleOptions) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13263a(roleOptions);
        }
    }

    /* renamed from: a */
    public void mo13317a(C1141k kVar) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13265a(kVar);
        }
    }

    /* renamed from: b */
    public void mo13318b() {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13269c();
        }
    }

    /* renamed from: b */
    public void mo13319b(int i) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13267b(i);
        }
    }

    /* renamed from: b */
    public void mo13320b(View view) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13268b(view);
        }
    }

    /* renamed from: c */
    public void mo13321c() {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13271d();
        }
    }

    /* renamed from: c */
    public void mo13322c(View view) {
        C1127d dVar = this.f3774b;
        if (dVar != null) {
            dVar.mo13270c(view);
        }
    }
}
