package com.baidu.mapsdkplatform.comapi.p024b.p025a;

import com.baidu.mapsdkplatform.comapi.util.C1174h;
import java.io.File;
import java.util.Arrays;

/* renamed from: com.baidu.mapsdkplatform.comapi.b.a.d */
class C1050d implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C1048c f3366a;

    C1050d(C1048c cVar) {
        this.f3366a = cVar;
    }

    public void run() {
        File[] listFiles;
        if (C1174h.m4256a().mo13403b() != null) {
            File file = new File(C1048c.f3361a);
            if (file.exists() && (listFiles = file.listFiles()) != null && listFiles.length != 0) {
                try {
                    Arrays.sort(listFiles, new C1051e());
                } catch (Exception unused) {
                }
                int length = listFiles.length;
                if (length > 10) {
                    length = 10;
                }
                for (int i = 0; i < length; i++) {
                    File file2 = listFiles[i];
                    if (!file2.isDirectory() && file2.exists() && file2.isFile() && file2.getName().contains(C1048c.f3362b) && (file2.getName().endsWith(".txt") || (file2.getName().endsWith(".zip") && file2.exists()))) {
                        boolean unused2 = this.f3366a.m3451a(file2);
                    }
                }
                if (listFiles.length > 10) {
                    this.f3366a.m3449a(listFiles);
                }
            }
        }
    }
}
