package com.baidu.location.indoor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import com.baidu.mobstat.Config;
import com.google.common.base.Ascii;
import com.google.common.primitives.UnsignedBytes;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/* renamed from: com.baidu.location.indoor.d */
public class C0860d {

    /* renamed from: a */
    private static final char[] f1998a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /* renamed from: b */
    private static C0860d f1999b = null;

    /* renamed from: c */
    private Context f2000c;

    /* renamed from: d */
    private boolean f2001d = false;

    /* renamed from: e */
    private boolean f2002e = false;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public BluetoothAdapter f2003f;

    /* renamed from: g */
    private boolean f2004g = false;

    /* renamed from: h */
    private C0862b f2005h;

    /* renamed from: i */
    private boolean f2006i = false;

    /* renamed from: j */
    private String f2007j = null;

    /* renamed from: k */
    private long f2008k = -1;
    /* access modifiers changed from: private */

    /* renamed from: l */
    public HashMap<String, ScanResult> f2009l = new HashMap<>();

    /* renamed from: m */
    private Handler f2010m = new Handler();

    /* renamed from: n */
    private Runnable f2011n = new C0863e(this);

    /* renamed from: o */
    private Object f2012o = null;

    /* renamed from: com.baidu.location.indoor.d$a */
    public static class C0861a implements Comparable<C0861a> {

        /* renamed from: a */
        public String f2013a;

        /* renamed from: b */
        public int f2014b;

        /* renamed from: c */
        public long f2015c;

        public C0861a(String str, int i, long j) {
            this.f2013a = str;
            this.f2014b = i;
            this.f2015c = j / 1000000;
        }

        /* renamed from: a */
        public int compareTo(C0861a aVar) {
            return Math.abs(this.f2014b) > Math.abs(aVar.f2014b) ? 1 : 0;
        }

        public String toString() {
            return this.f2013a.toUpperCase() + ";" + this.f2014b + ";" + this.f2015c;
        }
    }

    /* renamed from: com.baidu.location.indoor.d$b */
    public interface C0862b {
        /* renamed from: a */
        void mo10758a(boolean z, String str, String str2, String str3);
    }

    private C0860d(Context context) {
        this.f2000c = context;
        if (this.f2003f == null) {
            try {
                if (Build.VERSION.SDK_INT > 18) {
                    this.f2003f = ((BluetoothManager) context.getSystemService("bluetooth")).getAdapter();
                    this.f2004g = this.f2000c.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le");
                    return;
                }
                this.f2003f = BluetoothAdapter.getDefaultAdapter();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /* renamed from: a */
    public static C0860d m2490a(Context context) {
        if (f1999b == null) {
            f1999b = new C0860d(context);
        }
        return f1999b;
    }

    /* renamed from: a */
    private String m2491a(List<C0861a> list, int i) {
        StringBuilder sb = new StringBuilder();
        Collections.sort(list);
        sb.append(list.get(0).toString());
        int i2 = 1;
        while (i2 < list.size() && i2 < i) {
            sb.append("|");
            sb.append(list.get(i2).toString());
            i2++;
        }
        return sb.toString();
    }

    /* renamed from: a */
    public static String m2492a(byte[] bArr) {
        char[] cArr = new char[(bArr.length * 2)];
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & UnsignedBytes.MAX_VALUE;
            int i2 = i * 2;
            char[] cArr2 = f1998a;
            cArr[i2] = cArr2[b >>> 4];
            cArr[i2 + 1] = cArr2[b & Ascii.f4512SI];
        }
        return new String(cArr);
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2495a(HashMap<String, ScanResult> hashMap) {
        ArrayList<ScanResult> arrayList = new ArrayList<>(hashMap.values());
        ArrayList arrayList2 = new ArrayList();
        HashMap hashMap2 = new HashMap();
        HashMap hashMap3 = new HashMap();
        HashMap hashMap4 = new HashMap();
        ArrayList arrayList3 = new ArrayList();
        for (ScanResult scanResult : arrayList) {
            arrayList3.add(new C0861a(scanResult.getDevice().getAddress().replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, ""), scanResult.getRssi(), scanResult.getTimestampNanos()));
            if (this.f2001d) {
                scanResult.getScanRecord().getAdvertiseFlags();
                byte[] bytes = scanResult.getScanRecord().getBytes();
                if (bytes.length >= 26) {
                    String a = m2492a(Arrays.copyOfRange(bytes, 9, 25));
                    arrayList2.add(a);
                    hashMap2.put(a, scanResult.getDevice().getName());
                    hashMap3.put(a, m2492a(Arrays.copyOfRange(bytes, 0, 9)));
                    if (hashMap4.get(a) == null) {
                        hashMap4.put(a, 0);
                    }
                    hashMap4.put(a, Integer.valueOf(((Integer) hashMap4.get(a)).intValue() + 1));
                }
            }
        }
        String str = null;
        int i = 0;
        for (String str2 : hashMap4.keySet()) {
            if (((Integer) hashMap4.get(str2)).intValue() > i) {
                i = ((Integer) hashMap4.get(str2)).intValue();
                str = str2;
            }
        }
        boolean z = i > 3;
        C0862b bVar = this.f2005h;
        if (bVar != null && this.f2001d) {
            bVar.mo10758a(z, str, (String) hashMap2.get(str), (String) hashMap3.get(str));
            this.f2001d = false;
        }
        if (arrayList3.size() > 3) {
            this.f2007j = m2491a(arrayList3, 32);
            this.f2008k = System.currentTimeMillis();
        }
        if (this.f2006i) {
            mo10746a(true);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0069 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x006b A[RETURN] */
    /* renamed from: h */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m2497h() {
        /*
            r7 = this;
            java.lang.String r0 = ""
            java.io.File r1 = new java.io.File
            android.content.Context r2 = r7.f2000c
            java.io.File r2 = r2.getCacheDir()
            java.lang.String r3 = "ibct"
            r1.<init>(r2, r3)
            boolean r2 = r1.exists()
            r3 = 0
            if (r2 != 0) goto L_0x0017
            return r3
        L_0x0017:
            r2 = 0
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0041 }
            java.io.FileReader r5 = new java.io.FileReader     // Catch:{ Exception -> 0x0041 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0041 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x0041 }
            r1 = r2
            r2 = r0
        L_0x0024:
            java.lang.String r1 = r4.readLine()     // Catch:{ Exception -> 0x003f }
            if (r1 == 0) goto L_0x003a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x003f }
            r5.<init>()     // Catch:{ Exception -> 0x003f }
            r5.append(r2)     // Catch:{ Exception -> 0x003f }
            r5.append(r1)     // Catch:{ Exception -> 0x003f }
            java.lang.String r2 = r5.toString()     // Catch:{ Exception -> 0x003f }
            goto L_0x0024
        L_0x003a:
            r4.close()     // Catch:{ Exception -> 0x0041 }
            r1 = r2
            goto L_0x0048
        L_0x003f:
            r2 = move-exception
            goto L_0x0045
        L_0x0041:
            r1 = move-exception
            r6 = r2
            r2 = r1
            r1 = r6
        L_0x0045:
            r2.printStackTrace()
        L_0x0048:
            if (r1 == 0) goto L_0x006b
            java.lang.String r2 = r1.trim()
            boolean r0 = r2.equals(r0)
            if (r0 == 0) goto L_0x0055
            goto L_0x006b
        L_0x0055:
            java.lang.Long r0 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x006b }
            long r0 = r0.longValue()     // Catch:{ Exception -> 0x006b }
            long r4 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x006b }
            long r4 = r4 - r0
            r0 = 259200(0x3f480, double:1.28062E-318)
            int r2 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r2 >= 0) goto L_0x006b
            r0 = 1
            return r0
        L_0x006b:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.indoor.C0860d.m2497h():boolean");
    }

    /* renamed from: i */
    private void m2498i() {
        try {
            FileWriter fileWriter = new FileWriter(new File(this.f2000c.getCacheDir(), "ibct"));
            fileWriter.write(System.currentTimeMillis() + "");
            fileWriter.flush();
            fileWriter.close();
        } catch (Exception unused) {
        }
    }

    /* renamed from: a */
    public void mo10746a(boolean z) {
        boolean z2;
        if (this.f2003f != null) {
            try {
                if (Build.VERSION.SDK_INT >= 21) {
                    if (z) {
                        this.f2012o = new C0864f(this);
                        this.f2003f.getBluetoothLeScanner().startScan((ScanCallback) this.f2012o);
                        this.f2010m.postDelayed(this.f2011n, 3000);
                        z2 = true;
                    } else {
                        if (this.f2005h != null) {
                            this.f2003f.getBluetoothLeScanner().stopScan((ScanCallback) this.f2012o);
                        }
                        z2 = false;
                    }
                    this.f2001d = z2;
                }
            } catch (Exception unused) {
            }
        }
    }

    /* renamed from: a */
    public boolean mo10747a() {
        BluetoothAdapter bluetoothAdapter = this.f2003f;
        if (bluetoothAdapter != null && this.f2004g) {
            try {
                return bluetoothAdapter.isEnabled();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /* renamed from: a */
    public boolean mo10748a(C0862b bVar) {
        if (this.f2001d || this.f2002e) {
            return false;
        }
        this.f2002e = true;
        if (!mo10747a() || m2497h()) {
            return false;
        }
        m2498i();
        this.f2005h = bVar;
        mo10746a(true);
        return true;
    }

    /* renamed from: b */
    public boolean mo10749b() {
        if (!mo10747a()) {
            return false;
        }
        mo10746a(true);
        this.f2006i = true;
        return true;
    }

    /* renamed from: c */
    public void mo10750c() {
        this.f2002e = false;
        this.f2001d = false;
    }

    /* renamed from: d */
    public void mo10751d() {
        this.f2006i = false;
    }

    /* renamed from: e */
    public String mo10752e() {
        return this.f2007j;
    }

    /* renamed from: f */
    public long mo10753f() {
        return this.f2008k;
    }

    /* renamed from: g */
    public boolean mo10754g() {
        return System.currentTimeMillis() - this.f2008k <= 20000;
    }
}
