package com.baidu.mapapi.favorite;

import android.util.Log;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapsdkplatform.comapi.favrite.C1056a;
import com.baidu.mapsdkplatform.comapi.favrite.FavSyncPoi;
import com.baidu.mapsdkplatform.comapi.map.C1083i;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FavoriteManager {

    /* renamed from: a */
    private static FavoriteManager f2341a;

    /* renamed from: b */
    private static C1056a f2342b;

    private FavoriteManager() {
    }

    public static FavoriteManager getInstance() {
        if (f2341a == null) {
            f2341a = new FavoriteManager();
        }
        return f2341a;
    }

    public int add(FavoritePoiInfo favoritePoiInfo) {
        String str;
        if (f2342b == null) {
            str = "you may have not call init method!";
        } else if (favoritePoiInfo == null || favoritePoiInfo.f2345c == null) {
            str = "object or pt can not be null!";
        } else if (favoritePoiInfo.f2344b == null || favoritePoiInfo.f2344b.equals("")) {
            Log.e("baidumapsdk", "poiName can not be null or empty!");
            return -1;
        } else {
            FavSyncPoi a = C0915a.m2810a(favoritePoiInfo);
            int a2 = f2342b.mo12932a(a.f3376b, a);
            if (a2 == 1) {
                favoritePoiInfo.f2343a = a.f3375a;
                favoritePoiInfo.f2349g = Long.parseLong(a.f3382h);
            }
            return a2;
        }
        Log.e("baidumapsdk", str);
        return 0;
    }

    public boolean clearAllFavPois() {
        C1056a aVar = f2342b;
        if (aVar != null) {
            return aVar.mo12937c();
        }
        Log.e("baidumapsdk", "you may have not call init method!");
        return false;
    }

    public boolean deleteFavPoi(String str) {
        if (f2342b == null) {
            Log.e("baidumapsdk", "you may have not call init method!");
            return false;
        } else if (str == null || str.equals("")) {
            return false;
        } else {
            return f2342b.mo12933a(str);
        }
    }

    public void destroy() {
        C1056a aVar = f2342b;
        if (aVar != null) {
            aVar.mo12935b();
            f2342b = null;
            BMapManager.destroy();
            C1083i.m3652b();
        }
    }

    public List<FavoritePoiInfo> getAllFavPois() {
        JSONArray optJSONArray;
        C1056a aVar = f2342b;
        if (aVar == null) {
            Log.e("baidumapsdk", "you may have not call init method!");
            return null;
        }
        String f = aVar.mo12941f();
        if (f != null && !f.equals("")) {
            try {
                JSONObject jSONObject = new JSONObject(f);
                if (!(jSONObject.optInt("favpoinum") == 0 || (optJSONArray = jSONObject.optJSONArray("favcontents")) == null || optJSONArray.length() <= 0)) {
                    ArrayList arrayList = new ArrayList();
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        JSONObject jSONObject2 = optJSONArray.getJSONObject(i);
                        if (jSONObject2 != null) {
                            arrayList.add(C0915a.m2809a(jSONObject2));
                        }
                    }
                    return arrayList;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public FavoritePoiInfo getFavPoi(String str) {
        FavSyncPoi b;
        if (f2342b == null) {
            Log.e("baidumapsdk", "you may have not call init method!");
            return null;
        } else if (str == null || str.equals("") || (b = f2342b.mo12934b(str)) == null) {
            return null;
        } else {
            return C0915a.m2808a(b);
        }
    }

    public void init() {
        if (f2342b == null) {
            C1083i.m3649a();
            BMapManager.init();
            f2342b = C1056a.m3469a();
        }
    }

    public boolean updateFavPoi(String str, FavoritePoiInfo favoritePoiInfo) {
        String str2;
        if (f2342b == null) {
            str2 = "you may have not call init method!";
        } else if (str == null || str.equals("") || favoritePoiInfo == null) {
            return false;
        } else {
            if (favoritePoiInfo == null || favoritePoiInfo.f2345c == null) {
                str2 = "object or pt can not be null!";
            } else if (favoritePoiInfo.f2344b == null || favoritePoiInfo.f2344b.equals("")) {
                str2 = "poiName can not be null or empty!";
            } else {
                favoritePoiInfo.f2343a = str;
                return f2342b.mo12936b(str, C0915a.m2810a(favoritePoiInfo));
            }
        }
        Log.e("baidumapsdk", str2);
        return false;
    }
}
