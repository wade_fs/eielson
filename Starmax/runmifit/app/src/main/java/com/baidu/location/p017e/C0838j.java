package com.baidu.location.p017e;

import com.baidu.location.indoor.C0865g;
import com.baidu.location.p013a.C0750l;
import com.baidu.location.p013a.C0760t;
import com.baidu.location.p013a.C0767x;
import com.baidu.location.p017e.C0835i;

/* renamed from: com.baidu.location.e.j */
class C0838j implements Runnable {

    /* renamed from: a */
    final /* synthetic */ boolean f1809a;

    /* renamed from: b */
    final /* synthetic */ C0835i.C0837a f1810b;

    C0838j(C0835i.C0837a aVar, boolean z) {
        this.f1810b = aVar;
        this.f1809a = z;
    }

    public void run() {
        if (!C0835i.this.f1803j) {
            boolean unused = C0835i.this.f1803j = this.f1809a;
        }
        C0835i.this.m2385t();
        C0750l.m1883c().mo10486i();
        if (C0865g.m2519a().mo10768e()) {
            C0865g.m2519a().f2045a.obtainMessage(41).sendToTarget();
        }
        if (System.currentTimeMillis() - C0760t.m1939b() <= 5000) {
            C0767x.m1972a().mo10511c();
        }
    }
}
