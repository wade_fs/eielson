package com.baidu.platform.core.p033c;

import com.baidu.mapapi.search.poi.OnGetPoiSearchResultListener;
import com.baidu.mapapi.search.poi.PoiBoundSearchOption;
import com.baidu.mapapi.search.poi.PoiCitySearchOption;
import com.baidu.mapapi.search.poi.PoiDetailSearchOption;
import com.baidu.mapapi.search.poi.PoiIndoorOption;
import com.baidu.mapapi.search.poi.PoiNearbySearchOption;
import com.baidu.platform.base.C1316a;
import com.baidu.platform.base.SearchType;

/* renamed from: com.baidu.platform.core.c.f */
public class C1345f extends C1316a implements C1340a {

    /* renamed from: b */
    private OnGetPoiSearchResultListener f4459b = null;

    /* renamed from: a */
    public void mo14047a() {
        this.f4422a.lock();
        this.f4459b = null;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public void mo14048a(OnGetPoiSearchResultListener onGetPoiSearchResultListener) {
        this.f4422a.lock();
        this.f4459b = onGetPoiSearchResultListener;
        this.f4422a.unlock();
    }

    /* renamed from: a */
    public boolean mo14049a(PoiBoundSearchOption poiBoundSearchOption) {
        C1346g gVar = new C1346g(poiBoundSearchOption.mPageNum, poiBoundSearchOption.mPageCapacity);
        gVar.mo14021a(SearchType.POI_IN_BOUND_SEARCH);
        return mo14016a(new C1348i(poiBoundSearchOption), this.f4459b, gVar);
    }

    /* renamed from: a */
    public boolean mo14050a(PoiCitySearchOption poiCitySearchOption) {
        C1346g gVar = new C1346g(poiCitySearchOption.mPageNum, poiCitySearchOption.mPageCapacity);
        gVar.mo14021a(SearchType.POI_IN_CITY_SEARCH);
        return mo14016a(new C1348i(poiCitySearchOption), this.f4459b, gVar);
    }

    /* renamed from: a */
    public boolean mo14051a(PoiDetailSearchOption poiDetailSearchOption) {
        C1343d dVar = new C1343d();
        if (poiDetailSearchOption != null) {
            dVar.mo14054a(poiDetailSearchOption.isSearchByUids());
        }
        dVar.mo14021a(SearchType.POI_DETAIL_SEARCH);
        return mo14016a(new C1344e(poiDetailSearchOption), this.f4459b, dVar);
    }

    /* renamed from: a */
    public boolean mo14052a(PoiIndoorOption poiIndoorOption) {
        C1341b bVar = new C1341b();
        bVar.mo14021a(SearchType.INDOOR_POI_SEARCH);
        return mo14016a(new C1342c(poiIndoorOption), this.f4459b, bVar);
    }

    /* renamed from: a */
    public boolean mo14053a(PoiNearbySearchOption poiNearbySearchOption) {
        C1346g gVar = new C1346g(poiNearbySearchOption.mPageNum, poiNearbySearchOption.mPageCapacity);
        gVar.mo14021a(SearchType.POI_NEAR_BY_SEARCH);
        return mo14016a(new C1348i(poiNearbySearchOption), this.f4459b, gVar);
    }
}
