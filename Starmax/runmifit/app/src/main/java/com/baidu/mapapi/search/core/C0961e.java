package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.core.e */
final class C0961e implements Parcelable.Creator<PoiChildrenInfo> {
    C0961e() {
    }

    /* renamed from: a */
    public PoiChildrenInfo createFromParcel(Parcel parcel) {
        return new PoiChildrenInfo(parcel);
    }

    /* renamed from: a */
    public PoiChildrenInfo[] newArray(int i) {
        return new PoiChildrenInfo[i];
    }
}
