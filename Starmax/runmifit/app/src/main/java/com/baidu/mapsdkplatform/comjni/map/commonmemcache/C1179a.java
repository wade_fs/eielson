package com.baidu.mapsdkplatform.comjni.map.commonmemcache;

/* renamed from: com.baidu.mapsdkplatform.comjni.map.commonmemcache.a */
public class C1179a {

    /* renamed from: a */
    private long f3946a;

    /* renamed from: b */
    private JNICommonMemCache f3947b;

    public C1179a() {
        this.f3946a = 0;
        this.f3947b = null;
        this.f3947b = new JNICommonMemCache();
    }

    /* renamed from: a */
    public long mo13532a() {
        if (this.f3946a == 0) {
            this.f3946a = this.f3947b.Create();
        }
        return this.f3946a;
    }

    /* renamed from: b */
    public void mo13533b() {
        long j = this.f3946a;
        if (j != 0) {
            this.f3947b.Init(j);
        }
    }
}
