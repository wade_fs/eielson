package com.baidu.mobstat;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.baidu.mobstat.BaiduStatJSInterface;
import com.baidu.mobstat.MtjConfig;
import java.util.ArrayList;
import java.util.Map;

public class StatService {
    public static final int EXCEPTION_LOG = 1;
    public static final int JAVA_EXCEPTION_LOG = 16;

    /* renamed from: a */
    private static boolean f4233a = false;

    public interface WearListener {
        boolean onSendLogData(String str);
    }

    public static void trackWebView(Context context, WebView webView, WebChromeClient webChromeClient) {
    }

    /* renamed from: a */
    private static boolean m4494a(Class<?> cls, String str) {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        boolean z = false;
        for (int i = 2; i < stackTrace.length; i++) {
            StackTraceElement stackTraceElement = stackTrace[i];
            if (stackTraceElement.getMethodName().equals(str)) {
                try {
                    Class<?> cls2 = Class.forName(stackTraceElement.getClassName());
                    while (cls2.getSuperclass() != null && cls2.getSuperclass() != cls) {
                        cls2 = cls2.getSuperclass();
                    }
                    z = true;
                } catch (Exception unused) {
                }
            }
        }
        return z;
    }

    public static void enableDeviceMac(Context context, boolean z) {
        CooperService.instance().enableDeviceMac(context, z);
        BDStatCore.instance().init(context);
    }

    public static synchronized void setGlobalExtraInfo(Context context, ExtraInfo extraInfo) {
        synchronized (StatService.class) {
            if (context != null) {
                CooperService.instance().setHeaderExt(context, extraInfo);
                BDStatCore.instance().init(context);
            }
        }
    }

    public static synchronized void onResume(Activity activity) {
        synchronized (StatService.class) {
            if (m4493a(activity, "onResume(...)")) {
                if (!m4494a(Activity.class, "onResume")) {
                    C1256at.m4629c().mo13946c("[WARNING] onResume must be called in Activity.onResume()");
                } else {
                    BDStatCore.instance().onResume(activity, false);
                }
            }
        }
    }

    @Deprecated
    public static synchronized void onResume(Fragment fragment) {
        synchronized (StatService.class) {
            if (fragment == null) {
                C1256at.m4629c().mo13946c("[WARNING] onResume parameter invalid");
            } else if (!m4494a(Fragment.class, "onResume")) {
                C1256at.m4629c().mo13946c("[WARNING] onResume must be called in Fragment.onResume()");
            } else if (fragment.getActivity() == null) {
                C1256at.m4629c().mo13946c("[WARNING] can not get FragmentActivity, fragment may not attached to activity");
            } else {
                BDStatCore.instance().onResume(fragment);
            }
        }
    }

    @Deprecated
    public static synchronized void onResume(android.app.Fragment fragment) {
        synchronized (StatService.class) {
            if (fragment == null) {
                C1256at.m4629c().mo13946c("[WARNING] onResume parameter invalid");
            } else if (!m4494a(fragment.getClass(), "onResume")) {
                C1256at.m4629c().mo13946c("[WARNING] onResume must be called in Fragment.onResume()");
            } else if (fragment.getActivity() == null) {
                C1256at.m4629c().mo13946c("[WARNING] can not get Activity, fragment may not attached to activity");
            } else {
                BDStatCore.instance().onResume(fragment);
            }
        }
    }

    public static synchronized void onPageStart(Context context, String str) {
        synchronized (StatService.class) {
            if (context != null) {
                if (!TextUtils.isEmpty(str)) {
                    BDStatCore.instance().onPageStart(context, str);
                    return;
                }
            }
            C1256at.m4629c().mo13946c("[WARNING] onPageStart parameter invalid");
        }
    }

    /* renamed from: a */
    private static synchronized void m4486a(Context context, String str, ExtraInfo extraInfo) {
        synchronized (StatService.class) {
            if (context != null) {
                if (!TextUtils.isEmpty(str)) {
                    BDStatCore.instance().onPageEnd(context, str, extraInfo);
                    return;
                }
            }
            C1256at.m4629c().mo13946c("[WARNING] onPageEnd parameter invalid");
        }
    }

    public static synchronized void onPageEnd(Context context, String str) {
        synchronized (StatService.class) {
            m4486a(context, str, null);
        }
    }

    public static synchronized void onPause(Activity activity, ExtraInfo extraInfo) {
        synchronized (StatService.class) {
            if (m4493a(activity, "onPause(...)")) {
                if (!m4494a(Activity.class, "onPause")) {
                    C1256at.m4629c().mo13946c("[WARNING] onPause must be called in Activity.onPause");
                } else {
                    BDStatCore.instance().onPause(activity, false, extraInfo);
                }
            }
        }
    }

    public static synchronized void onPause(Activity activity) {
        synchronized (StatService.class) {
            onPause(activity, null);
        }
    }

    @Deprecated
    public static synchronized void onPause(Fragment fragment) {
        synchronized (StatService.class) {
            if (fragment == null) {
                C1256at.m4629c().mo13946c("[WARNING] onPause parameter invalid");
            } else if (!m4494a(Fragment.class, "onPause")) {
                C1256at.m4629c().mo13946c("[WARNING] onPause must be called in Fragment.onPause()");
            } else {
                BDStatCore.instance().onPause(fragment);
            }
        }
    }

    @Deprecated
    public static synchronized void onPause(android.app.Fragment fragment) {
        synchronized (StatService.class) {
            if (fragment == null) {
                C1256at.m4629c().mo13946c("[WARNING] onPause parameter invalid");
            } else if (!m4494a(fragment.getClass(), "onPause")) {
                C1256at.m4629c().mo13946c("[WARNING] onPause must be called in android.app.Fragment.onPause()");
            } else {
                BDStatCore.instance().onPause(fragment);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.StatService.a(android.content.Context, boolean):void
     arg types: [android.content.Context, int]
     candidates:
      com.baidu.mobstat.StatService.a(android.content.Context, java.lang.String):boolean
      com.baidu.mobstat.StatService.a(java.lang.Class<?>, java.lang.String):boolean
      com.baidu.mobstat.StatService.a(android.content.Context, boolean):void */
    public static void setOn(Context context, int i) {
        if (m4493a(context, "setOn(...)") && !f4233a) {
            f4233a = true;
            if ((i & 1) != 0) {
                m4491a(context, false);
            } else if ((i & 16) != 0) {
                m4491a(context, true);
            }
            BDStatCore.instance().init(context);
        }
    }

    public static void start(Context context) {
        if (m4493a(context, "start(...)")) {
            boolean a = C1278bi.m4755a(Application.class, "onCreate");
            if (a) {
                C1256at.m4629c().mo13946c("[WARNING] start 方法被 Application.onCreate()调用，not a good practice; 可能由于多进程反复重启等原因造成Application.onCreate() 方法多次被执行，导致启动次数高；建议埋点在统计路径触发的第一个页面中，比如APP主页面中");
            }
            BDStatCore.instance().onSessionStart(context, a);
        }
    }

    @Deprecated
    public static void setSendLogStrategy(Context context, SendStrategyEnum sendStrategyEnum, int i, boolean z) {
        if (m4493a(context, "setSendLogStrategy(...)")) {
            boolean a = C1278bi.m4755a(Application.class, "onCreate");
            if (a) {
                C1256at.m4629c().mo13946c("[WARNING] setSendLogStrategy 方法被 Application.onCreate()调用，not a good practice; 可能由于多进程反复重启等原因造成Application.onCreate() 方法多次被执行，导致启动次数高；建议埋点在统计路径触发的第一个页面中，比如APP主页面中");
            }
            BDStatCore.instance().onSessionStart(context, a);
            LogSender.instance().setSendLogStrategy(context.getApplicationContext(), sendStrategyEnum, i, z);
        }
    }

    @Deprecated
    public static void setSendLogStrategy(Context context, SendStrategyEnum sendStrategyEnum, int i) {
        setSendLogStrategy(context, sendStrategyEnum, i, false);
    }

    /* renamed from: a */
    private static void m4491a(Context context, boolean z) {
        if (m4493a(context, "onError(...)")) {
            BDStatCore.instance().init(context);
            ExceptionAnalysis.getInstance().openExceptionAnalysis(context.getApplicationContext(), z);
        }
    }

    /* renamed from: a */
    private static void m4487a(Context context, String str, String str2, int i, ExtraInfo extraInfo, Map<String, String> map) {
        if (m4493a(context, "onEvent(...)") && !TextUtils.isEmpty(str)) {
            boolean a = C1278bi.m4755a(Application.class, "onCreate");
            if (a) {
                C1256at.m4629c().mo13946c("[WARNING] onEvent 方法被 Application.onCreate()调用，not a good practice; 可能由于多进程反复重启等原因造成Application.onCreate() 方法多次被执行，导致启动次数高；建议埋点在统计路径触发的第一个页面中，比如APP主页面中");
            }
            BDStatCore.instance().onEvent(context.getApplicationContext(), str, str2, i, extraInfo, C1278bi.m4754a(map), a);
        }
    }

    public static void onEvent(Context context, String str, String str2, int i, Map<String, String> map) {
        m4487a(context, str, str2, i, (ExtraInfo) null, map);
    }

    public static void onEvent(Context context, String str, String str2, int i) {
        m4487a(context, str, str2, i, (ExtraInfo) null, (Map<String, String>) null);
    }

    /* renamed from: a */
    private static void m4489a(Context context, String str, String str2, ExtraInfo extraInfo) {
        m4487a(context, str, str2, 1, extraInfo, (Map<String, String>) null);
    }

    public static void onEvent(Context context, String str, String str2) {
        m4489a(context, str, str2, null);
    }

    public static void onEventStart(Context context, String str, String str2) {
        if (m4493a(context, "onEventStart(...)") && !TextUtils.isEmpty(str)) {
            BDStatCore.instance().onEventStart(context.getApplicationContext(), str, str2, false);
        }
    }

    public static void onEventEnd(Context context, String str, String str2) {
        m4490a(context, str, str2, (ExtraInfo) null, (Map<String, String>) null);
    }

    public static void onEventEnd(Context context, String str, String str2, Map<String, String> map) {
        m4490a(context.getApplicationContext(), str, str2, (ExtraInfo) null, map);
    }

    /* renamed from: a */
    private static void m4490a(Context context, String str, String str2, ExtraInfo extraInfo, Map<String, String> map) {
        if (m4493a(context, "onEventEnd(...)") && !TextUtils.isEmpty(str)) {
            BDStatCore.instance().onEventEnd(context.getApplicationContext(), str, str2, extraInfo, C1278bi.m4754a(map));
        }
    }

    /* renamed from: a */
    private static void m4488a(Context context, String str, String str2, long j, ExtraInfo extraInfo, Map<String, String> map) {
        if (!m4493a(context, "onEventDuration(...)") || TextUtils.isEmpty(str)) {
            return;
        }
        if (j <= 0) {
            C1256at.m4629c().mo13943b("[WARNING] onEventDuration duration must be greater than zero");
            return;
        }
        boolean a = C1278bi.m4755a(Application.class, "onCreate");
        if (a) {
            C1256at.m4629c().mo13946c("[WARNING] onEventDuration 方法被 Application.onCreate()调用，not a good practice; 可能由于多进程反复重启等原因造成Application.onCreate() 方法多次被执行，导致启动次数高；建议埋点在统计路径触发的第一个页面中，比如APP主页面中");
        }
        BDStatCore.instance().onEventDuration(context.getApplicationContext(), str, str2, j, extraInfo, C1278bi.m4754a(map), a);
    }

    public static void onEventDuration(Context context, String str, String str2, long j, Map<String, String> map) {
        m4488a(context, str, str2, j, (ExtraInfo) null, map);
    }

    public static void onEventDuration(Context context, String str, String str2, long j) {
        m4488a(context, str, str2, j, (ExtraInfo) null, (Map<String, String>) null);
    }

    /* renamed from: a */
    private static boolean m4493a(Context context, String str) {
        if (context != null) {
            return true;
        }
        C1256at c = C1256at.m4629c();
        c.mo13943b("[WARNING] " + str + ", context is null, invalid");
        return false;
    }

    public static void setAppKey(String str) {
        PrefOperate.setAppKey(str);
    }

    public static String getAppKey(Context context) {
        return PrefOperate.getAppKey(context);
    }

    @Deprecated
    public static void setAppChannel(String str) {
        PrefOperate.setAppChannel(str);
    }

    public static void setAppChannel(Context context, String str, boolean z) {
        PrefOperate.setAppChannel(context, str, z);
        BDStatCore.instance().init(context);
    }

    public static void setLogSenderDelayed(int i) {
        LogSender.instance().setLogSenderDelayed(i);
    }

    public static void setSessionTimeOut(int i) {
        BDStatCore.instance().setSessionTimeOut(i);
    }

    public static void setDebugOn(boolean z) {
        C1256at.m4629c().mo13948a(z);
    }

    public static void setForTv(Context context, boolean z) {
        BasicStoreTools.getInstance().setForTV(context, z);
        BDStatCore.instance().init(context);
    }

    /* renamed from: a */
    private static void m4492a(WebView webView) {
        if (Build.VERSION.SDK_INT >= 11 && Build.VERSION.SDK_INT <= 18) {
            webView.removeJavascriptInterface("searchBoxJavaBridge_");
            webView.removeJavascriptInterface("accessibility");
            webView.removeJavascriptInterface("accessibilityTraversal");
        }
    }

    public static void bindJSInterface(Context context, WebView webView) {
        bindJSInterface(context, webView, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.baidu.mobstat.StatService.a(android.content.Context, android.webkit.WebView, android.webkit.WebViewClient, android.webkit.WebChromeClient, boolean):void
     arg types: [android.content.Context, android.webkit.WebView, android.webkit.WebViewClient, ?[OBJECT, ARRAY], int]
     candidates:
      com.baidu.mobstat.StatService.a(android.content.Context, java.lang.String, java.lang.String, com.baidu.mobstat.ExtraInfo, java.util.Map<java.lang.String, java.lang.String>):void
      com.baidu.mobstat.StatService.a(android.content.Context, android.webkit.WebView, android.webkit.WebViewClient, android.webkit.WebChromeClient, boolean):void */
    public static void bindJSInterface(Context context, WebView webView, WebViewClient webViewClient) {
        m4485a(context, webView, webViewClient, (WebChromeClient) null, false);
    }

    /* renamed from: a */
    private static void m4485a(Context context, WebView webView, WebViewClient webViewClient, WebChromeClient webChromeClient, boolean z) {
        if (context == null) {
            C1256at.m4629c().mo13946c("[WARNING] context is null, invalid");
        } else if (webView == null) {
            C1256at.m4629c().mo13946c("[WARNING] webview is null, invalid");
        } else {
            m4492a(webView);
            WebSettings settings = webView.getSettings();
            settings.setJavaScriptEnabled(true);
            settings.setDefaultTextEncodingName("UTF-8");
            settings.setJavaScriptCanOpenWindowsAutomatically(true);
            if (!z) {
                webView.setWebViewClient(new BaiduStatJSInterface.CustomWebViewClient(context, webViewClient, null, null));
            } else {
                C1261ax axVar = new C1261ax();
                webView.addJavascriptInterface(axVar, "WebViewInterface");
                BaiduStatJSInterface.CustomWebChromeViewClient customWebChromeViewClient = new BaiduStatJSInterface.CustomWebChromeViewClient(context, webChromeClient, new ArrayList(), axVar);
                webView.setWebChromeClient(customWebChromeViewClient);
                webView.setTag(-96001, customWebChromeViewClient);
            }
            BDStatCore.instance().init(context);
        }
    }

    public static String getTestDeviceId(Context context) {
        return C1276bh.m4720b(context);
    }

    public static String getSdkVersion() {
        return CooperService.instance().getMTJSDKVersion();
    }

    public static void onErised(Context context, String str, String str2, String str3) {
        if (m4493a(context, "onErised(...)")) {
            if (str == null || "".equals(str)) {
                C1256at.m4629c().mo13946c("[WARNING] AppKey is invalid");
            } else {
                BDStatCore.instance().onErised(context, str, str2, str3);
            }
        }
    }

    public static void setUserId(Context context, String str) {
        if (context != null) {
            CooperService.instance().setUserId(context, str);
            BDStatCore.instance().init(context);
        }
    }

    public static void recordException(Context context, Throwable th) {
        if (context != null && th != null) {
            ExceptionAnalysis.getInstance().saveCrashInfo(context, th, false);
        }
    }

    public static void setAppVersionName(Context context, String str) {
        if (!TextUtils.isEmpty(str)) {
            if (str.length() > 256) {
                str = str.substring(0, 256);
            }
            CooperService.instance().setAppVersionName(context, str);
        }
    }

    public static synchronized void setPushId(Context context, MtjConfig.PushPlatform pushPlatform, String str) {
        synchronized (StatService.class) {
            if (context != null) {
                if (pushPlatform != null) {
                    if (TextUtils.isEmpty(str)) {
                        str = "";
                    }
                    if (str.length() > 1024) {
                        str = str.substring(0, 1024);
                    }
                    CooperService.instance().setPushId(context, pushPlatform.value(), pushPlatform.showName(), str);
                    BDStatCore.instance().init(context);
                }
            }
        }
    }
}
