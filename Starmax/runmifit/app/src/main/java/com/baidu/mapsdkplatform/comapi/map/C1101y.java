package com.baidu.mapsdkplatform.comapi.map;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.y */
public enum C1101y {
    DEFAULT(1),
    INDOOR(2),
    STREET(3);
    

    /* renamed from: d */
    private final int f3624d;

    private C1101y(int i) {
        this.f3624d = i;
    }
}
