package com.baidu.mapsdkplatform.comapi.synchronization.p027b;

import com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c;

/* renamed from: com.baidu.mapsdkplatform.comapi.synchronization.b.c */
/* synthetic */ class C1107c {

    /* renamed from: a */
    static final /* synthetic */ int[] f3657a = new int[C1115c.C1116a.values().length];

    /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
    static {
        /*
            com.baidu.mapsdkplatform.comapi.synchronization.c.c$a[] r0 = com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c.C1116a.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.f3657a = r0
            int[] r0 = com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.f3657a     // Catch:{ NoSuchFieldError -> 0x0014 }
            com.baidu.mapsdkplatform.comapi.synchronization.c.c$a r1 = com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c.C1116a.SUCCESS     // Catch:{ NoSuchFieldError -> 0x0014 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
            r2 = 1
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
        L_0x0014:
            int[] r0 = com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.f3657a     // Catch:{ NoSuchFieldError -> 0x001f }
            com.baidu.mapsdkplatform.comapi.synchronization.c.c$a r1 = com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c.C1116a.INNER_ERROR     // Catch:{ NoSuchFieldError -> 0x001f }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
            r2 = 2
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
        L_0x001f:
            int[] r0 = com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.f3657a     // Catch:{ NoSuchFieldError -> 0x002a }
            com.baidu.mapsdkplatform.comapi.synchronization.c.c$a r1 = com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c.C1116a.SERVER_ERROR     // Catch:{ NoSuchFieldError -> 0x002a }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
            r2 = 3
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
        L_0x002a:
            int[] r0 = com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.f3657a     // Catch:{ NoSuchFieldError -> 0x0035 }
            com.baidu.mapsdkplatform.comapi.synchronization.c.c$a r1 = com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c.C1116a.NETWORK_ERROR     // Catch:{ NoSuchFieldError -> 0x0035 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
            r2 = 4
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
        L_0x0035:
            int[] r0 = com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.f3657a     // Catch:{ NoSuchFieldError -> 0x0040 }
            com.baidu.mapsdkplatform.comapi.synchronization.c.c$a r1 = com.baidu.mapsdkplatform.comapi.synchronization.p028c.C1115c.C1116a.REQUEST_ERROR     // Catch:{ NoSuchFieldError -> 0x0040 }
            int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
            r2 = 5
            r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
        L_0x0040:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.synchronization.p027b.C1107c.<clinit>():void");
    }
}
