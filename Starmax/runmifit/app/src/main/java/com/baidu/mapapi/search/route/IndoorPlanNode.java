package com.baidu.mapapi.search.route;

import com.baidu.mapapi.model.LatLng;

public class IndoorPlanNode {

    /* renamed from: a */
    private LatLng f3051a = null;

    /* renamed from: b */
    private String f3052b = null;

    public IndoorPlanNode(LatLng latLng, String str) {
        this.f3051a = latLng;
        this.f3052b = str;
    }

    public String getFloor() {
        return this.f3052b;
    }

    public LatLng getLocation() {
        return this.f3051a;
    }
}
