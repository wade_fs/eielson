package com.baidu.mapapi.utils.poi;

import com.baidu.mapapi.model.LatLng;

public class PoiParaOption {

    /* renamed from: a */
    String f3265a;

    /* renamed from: b */
    String f3266b;

    /* renamed from: c */
    LatLng f3267c;

    /* renamed from: d */
    int f3268d;

    public PoiParaOption center(LatLng latLng) {
        this.f3267c = latLng;
        return this;
    }

    public LatLng getCenter() {
        return this.f3267c;
    }

    public String getKey() {
        return this.f3266b;
    }

    public int getRadius() {
        return this.f3268d;
    }

    public String getUid() {
        return this.f3265a;
    }

    public PoiParaOption key(String str) {
        this.f3266b = str;
        return this;
    }

    public PoiParaOption radius(int i) {
        this.f3268d = i;
        return this;
    }

    public PoiParaOption uid(String str) {
        this.f3265a = str;
        return this;
    }
}
