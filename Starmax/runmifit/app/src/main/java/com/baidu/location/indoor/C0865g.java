package com.baidu.location.indoor;

import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.baidu.location.BDLocation;
import com.baidu.location.C0839f;
import com.baidu.location.Jni;
import com.baidu.location.indoor.C0856a;
import com.baidu.location.indoor.C0881m;
import com.baidu.location.indoor.C0908p;
import com.baidu.location.indoor.mapversion.p020a.C0884a;
import com.baidu.location.indoor.mapversion.p021b.C0895a;
import com.baidu.location.indoor.mapversion.p022c.C0896a;
import com.baidu.location.indoor.mapversion.p022c.C0902c;
import com.baidu.location.p013a.C0723a;
import com.baidu.location.p013a.C0754n;
import com.baidu.location.p013a.C0762v;
import com.baidu.location.p014b.C0770a;
import com.baidu.location.p017e.C0822b;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p017e.C0834h;
import com.baidu.location.p017e.C0835i;
import com.baidu.location.p019g.C0844b;
import com.baidu.location.p019g.C0849e;
import com.baidu.location.p019g.C0855k;
import com.baidu.mapsdkplatform.comapi.location.CoordinateType;
import com.baidu.mobstat.Config;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/* renamed from: com.baidu.location.indoor.g */
public class C0865g {

    /* renamed from: g */
    private static C0865g f2018g;
    /* access modifiers changed from: private */

    /* renamed from: A */
    public String f2019A;

    /* renamed from: B */
    private String f2020B;
    /* access modifiers changed from: private */

    /* renamed from: C */
    public int f2021C;

    /* renamed from: D */
    private int f2022D;
    /* access modifiers changed from: private */

    /* renamed from: E */
    public C0859c<String> f2023E;

    /* renamed from: F */
    private int f2024F;

    /* renamed from: G */
    private C0859c<String> f2025G;
    /* access modifiers changed from: private */

    /* renamed from: H */
    public double f2026H;
    /* access modifiers changed from: private */

    /* renamed from: I */
    public double f2027I;
    /* access modifiers changed from: private */

    /* renamed from: J */
    public double f2028J;
    /* access modifiers changed from: private */

    /* renamed from: K */
    public boolean f2029K;

    /* renamed from: L */
    private boolean f2030L;
    /* access modifiers changed from: private */

    /* renamed from: M */
    public List<C0874h> f2031M;
    /* access modifiers changed from: private */

    /* renamed from: N */
    public int f2032N;
    /* access modifiers changed from: private */

    /* renamed from: O */
    public int f2033O;

    /* renamed from: P */
    private int f2034P;
    /* access modifiers changed from: private */

    /* renamed from: Q */
    public C0856a f2035Q;
    /* access modifiers changed from: private */

    /* renamed from: R */
    public String f2036R;
    /* access modifiers changed from: private */

    /* renamed from: S */
    public C0860d f2037S;
    /* access modifiers changed from: private */

    /* renamed from: T */
    public boolean f2038T;
    /* access modifiers changed from: private */

    /* renamed from: U */
    public C0908p f2039U;

    /* renamed from: V */
    private C0908p.C0909a f2040V;

    /* renamed from: W */
    private C0884a f2041W;
    /* access modifiers changed from: private */

    /* renamed from: X */
    public int f2042X;
    /* access modifiers changed from: private */

    /* renamed from: Y */
    public BDLocation f2043Y;

    /* renamed from: Z */
    private boolean f2044Z;

    /* renamed from: a */
    public C0869d f2045a;

    /* renamed from: aa */
    private boolean f2046aa;
    /* access modifiers changed from: private */

    /* renamed from: ab */
    public boolean f2047ab;
    /* access modifiers changed from: private */

    /* renamed from: ac */
    public boolean f2048ac;

    /* renamed from: ad */
    private C0868c f2049ad;
    /* access modifiers changed from: private */

    /* renamed from: ae */
    public C0870e f2050ae;
    /* access modifiers changed from: private */

    /* renamed from: af */
    public C0871f f2051af;

    /* renamed from: ag */
    private C0867b f2052ag;

    /* renamed from: b */
    public SimpleDateFormat f2053b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public int f2054c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f2055d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public int f2056e;

    /* renamed from: f */
    private boolean f2057f;
    /* access modifiers changed from: private */

    /* renamed from: h */
    public long f2058h;
    /* access modifiers changed from: private */

    /* renamed from: i */
    public volatile boolean f2059i;
    /* access modifiers changed from: private */

    /* renamed from: j */
    public C0881m f2060j;

    /* renamed from: k */
    private C0873g f2061k;

    /* renamed from: l */
    private C0875i f2062l;

    /* renamed from: m */
    private long f2063m;
    /* access modifiers changed from: private */

    /* renamed from: n */
    public boolean f2064n;

    /* renamed from: o */
    private boolean f2065o;
    /* access modifiers changed from: private */

    /* renamed from: p */
    public long f2066p;
    /* access modifiers changed from: private */

    /* renamed from: q */
    public long f2067q;
    /* access modifiers changed from: private */

    /* renamed from: r */
    public int f2068r;

    /* renamed from: s */
    private String f2069s;

    /* renamed from: t */
    private C0881m.C0882a f2070t;
    /* access modifiers changed from: private */

    /* renamed from: u */
    public int f2071u;

    /* renamed from: v */
    private int f2072v;
    /* access modifiers changed from: private */

    /* renamed from: w */
    public String f2073w;
    /* access modifiers changed from: private */

    /* renamed from: x */
    public String f2074x;
    /* access modifiers changed from: private */

    /* renamed from: y */
    public C0880l f2075y;
    /* access modifiers changed from: private */

    /* renamed from: z */
    public String f2076z;

    /* renamed from: com.baidu.location.indoor.g$a */
    class C0866a {
    }

    /* renamed from: com.baidu.location.indoor.g$b */
    class C0867b {

        /* renamed from: a */
        public String f2077a;

        /* renamed from: b */
        public int f2078b;

        /* renamed from: c */
        public String f2079c;

        /* renamed from: e */
        private ArrayList<Double> f2081e;

        /* renamed from: f */
        private ArrayList<String> f2082f;

        /* renamed from: g */
        private Map<String, Double> f2083g;

        /* renamed from: h */
        private int f2084h;

        /* renamed from: i */
        private Map<String, Integer> f2085i;

        public C0867b() {
            this.f2077a = null;
            this.f2081e = null;
            this.f2082f = null;
            this.f2083g = null;
            this.f2084h = 0;
            this.f2078b = 0;
            this.f2079c = null;
            this.f2085i = null;
            this.f2081e = new ArrayList<>();
            this.f2082f = new ArrayList<>();
            this.f2085i = new HashMap();
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public int m2585a(BDLocation bDLocation) {
            Object valueOf;
            if (!bDLocation.getBuildingID().equals(this.f2079c)) {
                this.f2079c = bDLocation.getBuildingID();
                m2591a();
            }
            if (m2592b(bDLocation.getRetFields("p_floor")) != 0) {
                this.f2078b = 0;
                return 1;
            }
            try {
                if (this.f2082f.size() == 0) {
                    for (Map.Entry entry : this.f2083g.entrySet()) {
                        this.f2082f.add(entry.getKey());
                        this.f2081e.add(entry.getValue());
                    }
                } else {
                    ArrayList<String> arrayList = new ArrayList<>();
                    ArrayList arrayList2 = new ArrayList();
                    Iterator<String> it = this.f2082f.iterator();
                    while (it.hasNext()) {
                        arrayList.add(it.next());
                        arrayList2.add(Double.valueOf(0.0d));
                    }
                    HashMap hashMap = new HashMap();
                    for (Map.Entry entry2 : this.f2083g.entrySet()) {
                        String str = (String) entry2.getKey();
                        Double d = (Double) entry2.getValue();
                        hashMap.put(str, d);
                        if (!this.f2082f.contains(str)) {
                            arrayList.add(str);
                            arrayList2.add(d);
                        }
                    }
                    double d2 = 0.0d;
                    for (Double d3 : this.f2083g.values()) {
                        d2 += d3.doubleValue();
                    }
                    for (int i = 0; i < arrayList.size(); i++) {
                        if (hashMap.containsKey(arrayList.get(i))) {
                            valueOf = hashMap.get(arrayList.get(i));
                        } else {
                            double d4 = 1.0d - d2;
                            double size = (double) (this.f2084h - hashMap.size());
                            Double.isNaN(size);
                            valueOf = Double.valueOf(d4 / size);
                        }
                        arrayList2.set(i, valueOf);
                    }
                    ArrayList arrayList3 = new ArrayList();
                    for (int i2 = 0; i2 < arrayList.size(); i2++) {
                        arrayList3.add(Double.valueOf(0.0d));
                    }
                    for (int i3 = 0; i3 < this.f2082f.size(); i3++) {
                        Double d5 = this.f2081e.get(i3);
                        ArrayList<Double> a = m2590a(arrayList, this.f2082f.get(i3));
                        for (int i4 = 0; i4 < arrayList.size(); i4++) {
                            arrayList3.set(i4, Double.valueOf(((Double) arrayList3.get(i4)).doubleValue() + (d5.doubleValue() * a.get(i4).doubleValue() * ((Double) arrayList2.get(i4)).doubleValue())));
                        }
                    }
                    this.f2082f = arrayList;
                    this.f2081e = m2589a(arrayList3);
                }
                double d6 = 0.0d;
                String str2 = null;
                for (int i5 = 0; i5 < this.f2082f.size(); i5++) {
                    if (this.f2081e.get(i5).doubleValue() > d6) {
                        double doubleValue = this.f2081e.get(i5).doubleValue();
                        str2 = this.f2082f.get(i5);
                        d6 = doubleValue;
                    }
                }
                this.f2077a = str2;
            } catch (Exception unused) {
                this.f2078b = 0;
            }
            this.f2078b = 1;
            return 0;
        }

        /* renamed from: a */
        private int m2587a(String str) {
            if (this.f2085i.containsKey(str)) {
                return this.f2085i.get(str).intValue();
            }
            int i = 1000;
            try {
                if (!str.startsWith("F")) {
                    if (!str.startsWith("f")) {
                        if (str.startsWith("B") || str.startsWith("b")) {
                            i = -Integer.parseInt(str.substring(1));
                        }
                        this.f2085i.put(str, Integer.valueOf(i));
                        return i;
                    }
                }
                i = Integer.parseInt(str.substring(1)) - 1;
            } catch (Exception unused) {
            }
            this.f2085i.put(str, Integer.valueOf(i));
            return i;
        }

        /* renamed from: a */
        private ArrayList<Double> m2589a(ArrayList<Double> arrayList) {
            ArrayList<Double> arrayList2 = new ArrayList<>();
            Double valueOf = Double.valueOf(0.0d);
            Iterator<Double> it = arrayList.iterator();
            while (it.hasNext()) {
                valueOf = Double.valueOf(valueOf.doubleValue() + it.next().doubleValue());
            }
            Iterator<Double> it2 = arrayList.iterator();
            while (it2.hasNext()) {
                arrayList2.add(Double.valueOf(it2.next().doubleValue() / valueOf.doubleValue()));
            }
            return arrayList2;
        }

        /* renamed from: a */
        private ArrayList<Double> m2590a(ArrayList<String> arrayList, String str) {
            ArrayList<Double> arrayList2 = new ArrayList<>();
            double[] dArr = {180.0d, 10.0d, 1.0d};
            int a = m2587a(str);
            Iterator<String> it = arrayList.iterator();
            if (a == 1000) {
                while (it.hasNext()) {
                    arrayList2.add(Double.valueOf(it.next().equals(str) ? dArr[0] : dArr[2]));
                }
                return arrayList2;
            }
            while (it.hasNext()) {
                int a2 = m2587a(it.next());
                int i = a2 == 1000 ? 2 : a > a2 ? a - a2 : a2 - a;
                if (i > 2) {
                    i = 2;
                }
                arrayList2.add(Double.valueOf(dArr[i]));
            }
            return arrayList2;
        }

        /* renamed from: a */
        private void m2591a() {
            this.f2081e.clear();
            this.f2082f.clear();
            this.f2085i.clear();
        }

        /* renamed from: b */
        private int m2592b(String str) {
            try {
                String[] split = str.split(";");
                if (split.length <= 1) {
                    return 1;
                }
                this.f2084h = Integer.parseInt(split[0]);
                this.f2083g = new HashMap();
                for (int i = 1; i < split.length; i++) {
                    String[] split2 = split[i].split(Config.TRACE_TODAY_VISIT_SPLIT);
                    this.f2083g.put(split2[0], Double.valueOf(Double.parseDouble(split2[1])));
                }
                return 0;
            } catch (Exception unused) {
                return 1;
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public String m2593b() {
            return this.f2077a;
        }
    }

    /* renamed from: com.baidu.location.indoor.g$c */
    class C0868c {

        /* renamed from: a */
        public ArrayList<ArrayList<Float>> f2086a = null;

        /* renamed from: b */
        public double[] f2087b = null;

        /* renamed from: d */
        private float f2089d = -0.18181887f;

        /* renamed from: e */
        private float f2090e = -0.90904963f;

        /* renamed from: f */
        private float f2091f = -0.55321634f;

        /* renamed from: g */
        private float f2092g = -0.05259979f;

        /* renamed from: h */
        private float f2093h = 24.0f;

        /* renamed from: i */
        private float f2094i = 8.61f;

        /* renamed from: j */
        private float f2095j = 4.25f;

        /* renamed from: k */
        private float f2096k = 60.39f;

        /* renamed from: l */
        private float f2097l = 15.6f;

        /* renamed from: m */
        private float f2098m = 68.07f;

        /* renamed from: n */
        private float f2099n = 11.61f;

        public C0868c() {
        }

        /* renamed from: a */
        public double mo10772a(double d, double d2, double d3, double d4) {
            double[] a = mo10773a(d2, d3);
            double abs = Math.abs(d4 - a[0]);
            return abs > a[1] * 2.0d ? d + abs : d;
        }

        /* renamed from: a */
        public double[] mo10773a(double d, double d2) {
            return C0770a.m1980a().mo10519a(d, d2);
        }
    }

    /* renamed from: com.baidu.location.indoor.g$d */
    public class C0869d extends Handler {
        public C0869d() {
        }

        public void handleMessage(Message message) {
            if (C0839f.isServing) {
                int i = message.what;
                if (i == 21) {
                    C0865g.this.m2524a(message);
                } else if (i == 41) {
                    C0865g.this.m2555k();
                } else if (i != 801) {
                    super.dispatchMessage(message);
                } else {
                    C0865g.this.m2525a((BDLocation) message.obj);
                }
            }
        }
    }

    /* renamed from: com.baidu.location.indoor.g$e */
    class C0870e {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public double f2102b = -1.0d;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public long f2103c = 0;

        /* renamed from: d */
        private long f2104d = 0;
        /* access modifiers changed from: private */

        /* renamed from: e */
        public long f2105e = 0;
        /* access modifiers changed from: private */

        /* renamed from: f */
        public long f2106f = 0;

        /* renamed from: g */
        private long f2107g = 0;

        /* renamed from: h */
        private long f2108h = 0;

        /* renamed from: i */
        private long f2109i = 0;

        /* renamed from: j */
        private long f2110j = 0;

        /* renamed from: k */
        private long f2111k = 0;

        /* renamed from: l */
        private double f2112l = 0.0d;

        /* renamed from: m */
        private double f2113m = 0.0d;

        /* renamed from: n */
        private double f2114n = 0.0d;

        /* renamed from: o */
        private double f2115o = 0.0d;

        /* renamed from: p */
        private int f2116p = 0;

        /* renamed from: q */
        private int f2117q = 0;

        /* renamed from: r */
        private C0834h f2118r = null;

        /* renamed from: s */
        private long f2119s = 0;

        /* renamed from: t */
        private int f2120t = 0;

        /* renamed from: u */
        private int f2121u = 0;

        public C0870e() {
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m2597a() {
            this.f2102b = -1.0d;
            this.f2103c = 0;
            this.f2104d = 0;
            this.f2106f = 0;
            this.f2107g = 0;
            this.f2108h = 0;
            this.f2109i = 0;
            this.f2110j = 0;
            this.f2111k = 0;
            this.f2112l = 0.0d;
            this.f2113m = 0.0d;
            this.f2116p = 0;
            this.f2117q = 0;
            this.f2118r = null;
            this.f2119s = 0;
            this.f2120t = 0;
            this.f2121u = 0;
            this.f2105e = 0;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m2598a(double d, double d2, double d3, long j) {
            this.f2110j = j;
            this.f2121u = 0;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public void m2599a(Location location, boolean z) {
            this.f2111k = System.currentTimeMillis();
            double longitude = location.getLongitude();
            double latitude = location.getLatitude();
            double d = this.f2112l;
            if (d != 0.0d) {
                float[] fArr = new float[2];
                Location.distanceBetween(this.f2113m, d, latitude, longitude, fArr);
                if (fArr[0] < 20.0f) {
                    this.f2116p++;
                } else {
                    this.f2116p = 0;
                }
                if (fArr[0] < 5.0f) {
                    this.f2117q++;
                } else {
                    this.f2117q = 0;
                }
            }
            this.f2112l = longitude;
            this.f2113m = latitude;
            if (location.hasSpeed() && location.getSpeed() > 3.0f) {
                this.f2108h = System.currentTimeMillis();
            }
            if (location.getAccuracy() >= 50.0f || z) {
                this.f2120t = 0;
            } else {
                this.f2120t++;
            }
            if (this.f2120t > 10 && System.currentTimeMillis() - this.f2103c > 30000) {
                C0865g.this.mo10767d();
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public boolean m2602a(double d, double d2, double d3) {
            if (!C0865g.this.f2050ae.m2608c()) {
                return true;
            }
            long currentTimeMillis = System.currentTimeMillis();
            long j = this.f2106f;
            if (j != 0 && currentTimeMillis - j > 10000) {
                return true;
            }
            if (this.f2117q >= 5 && d3 < 15.0d && currentTimeMillis - this.f2103c > 20000) {
                float[] fArr = new float[2];
                Location.distanceBetween(this.f2115o, this.f2114n, d2, d, fArr);
                if (fArr[0] > 30.0f) {
                    return true;
                }
            }
            return false;
        }

        /* access modifiers changed from: private */
        /* renamed from: a */
        public boolean m2603a(BDLocation bDLocation, double d, String str) {
            long currentTimeMillis = System.currentTimeMillis();
            this.f2109i = currentTimeMillis;
            this.f2102b = d;
            this.f2114n = bDLocation.getLongitude();
            this.f2115o = bDLocation.getLatitude();
            if (str.equals("wifi")) {
                this.f2103c = currentTimeMillis;
            }
            if (str.equals("gps")) {
                this.f2105e = currentTimeMillis;
            }
            if (m2612e()) {
                this.f2106f = currentTimeMillis;
            }
            C0865g gVar = C0865g.this;
            boolean unused = gVar.f2055d = gVar.mo10761a(bDLocation.getLongitude(), bDLocation.getLatitude());
            if (C0865g.this.f2055d || C0865g.this.f2054c == 1) {
                this.f2107g = currentTimeMillis;
            }
            long j = this.f2119s;
            if (j != 0 && currentTimeMillis - j > 30000 && currentTimeMillis - this.f2110j < 10000 && currentTimeMillis - this.f2111k < 10000) {
                return false;
            }
            if (this.f2120t > 10 && currentTimeMillis - this.f2103c > 30000) {
                return false;
            }
            if (currentTimeMillis - this.f2107g > 10000 && currentTimeMillis - this.f2103c > 30000) {
                return false;
            }
            long j2 = this.f2106f;
            return j2 == 0 || currentTimeMillis - j2 <= 60000;
        }

        /* access modifiers changed from: private */
        /* renamed from: b */
        public boolean m2607b() {
            System.currentTimeMillis();
            if (C0865g.this.f2064n || this.f2116p < 3) {
                return false;
            }
            if (!C0835i.m2376a().mo10692h().contains("&wifio") && C0865g.this.f2054c != 1) {
                return false;
            }
            this.f2121u = 1;
            return true;
        }

        /* renamed from: c */
        private boolean m2608c() {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.f2108h < 10000 && currentTimeMillis - this.f2103c > 30000) {
                return false;
            }
            if (currentTimeMillis - this.f2111k >= 10000) {
                return true;
            }
            long j = this.f2110j;
            return j == 0 || currentTimeMillis - j <= 16000 || currentTimeMillis - this.f2103c <= 30000;
        }

        /* access modifiers changed from: private */
        /* renamed from: d */
        public void m2610d() {
            C0834h q = C0835i.m2376a().mo10700q();
            if (q.f1789a != null) {
                long currentTimeMillis = System.currentTimeMillis();
                C0834h hVar = this.f2118r;
                if (hVar == null || !q.mo10671b(hVar)) {
                    if (currentTimeMillis - this.f2119s < 10000) {
                        this.f2104d = currentTimeMillis;
                    }
                    this.f2119s = currentTimeMillis;
                    this.f2118r = q;
                }
            }
        }

        /* access modifiers changed from: private */
        /* renamed from: e */
        public boolean m2612e() {
            if (this.f2121u == 1 || !m2608c() || this.f2102b > 25.0d || System.currentTimeMillis() - this.f2109i > 30000) {
                return false;
            }
            this.f2106f = System.currentTimeMillis();
            return true;
        }
    }

    /* renamed from: com.baidu.location.indoor.g$f */
    private class C0871f {

        /* renamed from: a */
        public int f2122a = 10;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public List<C0872a> f2124c = Collections.synchronizedList(new ArrayList());

        /* renamed from: com.baidu.location.indoor.g$f$a */
        private class C0872a {

            /* renamed from: a */
            public double f2125a;

            /* renamed from: b */
            public double f2126b;

            /* renamed from: c */
            public double f2127c;

            public C0872a(double d, double d2, double d3) {
                this.f2125a = d;
                this.f2126b = d2;
                this.f2127c = d3;
            }
        }

        public C0871f() {
        }

        /* renamed from: a */
        public void mo10775a(BDLocation bDLocation) {
            this.f2124c.add(new C0872a(bDLocation.getLongitude(), bDLocation.getLatitude(), C0865g.this.f2050ae.f2102b));
        }

        public String toString() {
            if (this.f2124c.size() == 0) {
                return "";
            }
            StringBuffer stringBuffer = new StringBuffer();
            double d = this.f2124c.get(0).f2125a;
            double d2 = this.f2124c.get(0).f2126b;
            stringBuffer.append(String.format("%.6f:%.6f:%.1f", Double.valueOf(d), Double.valueOf(d2), Double.valueOf(this.f2124c.get(0).f2127c)));
            int size = (this.f2124c.size() > this.f2122a ? this.f2124c.size() - this.f2122a : 0) + 1;
            while (size < this.f2124c.size()) {
                stringBuffer.append(String.format(";%.0f:%.0f:%.1f", Double.valueOf((this.f2124c.get(size).f2125a - d) * 1000000.0d), Double.valueOf((this.f2124c.get(size).f2126b - d2) * 1000000.0d), Double.valueOf(this.f2124c.get(size).f2127c)));
                size++;
                d = d;
            }
            return stringBuffer.toString();
        }
    }

    /* renamed from: com.baidu.location.indoor.g$g */
    class C0873g extends Thread {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public volatile boolean f2130b = true;

        /* renamed from: c */
        private long f2131c = 0;

        /* renamed from: d */
        private long f2132d = 0;

        /* renamed from: e */
        private long f2133e = 0;

        C0873g() {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, long):long
         arg types: [com.baidu.location.indoor.g, int]
         candidates:
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, double):double
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, int):int
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, com.baidu.location.e.h):java.lang.String
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, java.lang.String):java.lang.String
          com.baidu.location.indoor.g.a(com.baidu.location.BDLocation, int):void
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, android.os.Message):void
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, com.baidu.location.BDLocation):void
          com.baidu.location.indoor.g.a(java.lang.String, java.lang.String):void
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, boolean):boolean
          com.baidu.location.indoor.g.a(double, double):boolean
          com.baidu.location.indoor.g.a(android.location.Location, java.util.ArrayList<java.util.ArrayList<java.lang.Float>>):boolean
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, long):long */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.g.c(com.baidu.location.indoor.g, boolean):boolean
         arg types: [com.baidu.location.indoor.g, int]
         candidates:
          com.baidu.location.indoor.g.c(com.baidu.location.indoor.g, double):double
          com.baidu.location.indoor.g.c(com.baidu.location.indoor.g, int):int
          com.baidu.location.indoor.g.c(com.baidu.location.indoor.g, boolean):boolean */
        public void run() {
            while (this.f2130b) {
                if (C0865g.this.f2054c != 1 || C0865g.this.f2055d) {
                    long unused = C0865g.this.f2058h = 3000L;
                } else {
                    long unused2 = C0865g.this.f2058h = 5000L;
                }
                if (C0865g.this.f2060j.mo10786c() == 1) {
                    this.f2132d = System.currentTimeMillis();
                }
                boolean z = System.currentTimeMillis() - this.f2131c > 17500;
                if (System.currentTimeMillis() - this.f2132d < 5000) {
                    if (System.currentTimeMillis() - this.f2131c > 10000) {
                        z = true;
                    }
                    if (System.currentTimeMillis() - this.f2131c > C0865g.this.f2058h) {
                        z = true;
                    }
                }
                if (z) {
                    C0835i.m2376a().mo10693i();
                    C0865g.this.f2060j.mo10789f();
                    this.f2131c = System.currentTimeMillis();
                    boolean unused3 = C0865g.this.f2059i = false;
                }
                if (C0835i.m2376a().mo10701r()) {
                    this.f2133e = 0;
                } else {
                    this.f2133e++;
                    if (this.f2133e >= 10) {
                        this.f2130b = false;
                        C0865g.this.mo10767d();
                        return;
                    }
                }
                if (C0865g.this.f2064n && C0865g.this.f2050ae != null && System.currentTimeMillis() - C0865g.this.f2067q > 30000 && System.currentTimeMillis() - C0865g.this.f2050ae.f2106f > 30000) {
                    C0865g.m2519a().mo10767d();
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException unused4) {
                    this.f2130b = false;
                    return;
                }
            }
        }
    }

    /* renamed from: com.baidu.location.indoor.g$h */
    private class C0874h {

        /* renamed from: a */
        public int f2134a;

        /* renamed from: b */
        public double f2135b;

        /* renamed from: c */
        public double f2136c;

        /* renamed from: d */
        public int f2137d = 1;

        /* renamed from: e */
        public double f2138e;

        public C0874h(int i, double d, double d2, double d3) {
            this.f2134a = i;
            this.f2135b = d;
            this.f2136c = d2;
            this.f2138e = d3;
        }

        public String toString() {
            if (this.f2136c == this.f2138e) {
                return String.format("%d:%.1f:%.2f", Integer.valueOf(this.f2137d), Double.valueOf(this.f2136c), Double.valueOf(this.f2135b));
            }
            return String.format("%d:%.1f:%.2f:%.1f", Integer.valueOf(this.f2137d), Double.valueOf(this.f2136c), Double.valueOf(this.f2135b), Double.valueOf(this.f2138e));
        }
    }

    /* renamed from: com.baidu.location.indoor.g$i */
    class C0875i extends C0849e {

        /* renamed from: b */
        private boolean f2141b = false;

        /* renamed from: c */
        private boolean f2142c = false;

        /* renamed from: d */
        private String f2143d = null;

        /* renamed from: e */
        private String f2144e = null;

        /* renamed from: f */
        private long f2145f = 0;

        /* renamed from: q */
        private C0866a f2146q = null;

        /* renamed from: r */
        private long f2147r = 0;

        /* renamed from: s */
        private long f2148s = 0;

        public C0875i() {
            this.f1885k = new HashMap();
        }

        /* renamed from: a */
        public void mo10444a() {
            this.f1882h = C0855k.m2466e();
            if (C0865g.this.f2074x == null || C0865g.this.f2075y == null || !C0865g.this.f2074x.equals(C0865g.this.f2075y.mo10782a())) {
                this.f2143d = "&nd_idf=1&indoor_polygon=1" + this.f2143d;
            }
            this.f1883i = 1;
            String encodeTp4 = Jni.encodeTp4(this.f2143d);
            this.f2143d = null;
            this.f1885k.put("bloc", encodeTp4);
            this.f2147r = System.currentTimeMillis();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.g.d(com.baidu.location.indoor.g, boolean):boolean
         arg types: [com.baidu.location.indoor.g, int]
         candidates:
          com.baidu.location.indoor.g.d(com.baidu.location.indoor.g, int):int
          com.baidu.location.indoor.g.d(com.baidu.location.indoor.g, boolean):boolean */
        /* renamed from: a */
        public void mo10446a(boolean z) {
            if (!z || this.f1884j == null) {
                C0865g.m2569v(C0865g.this);
                int unused = C0865g.this.f2042X = 0;
                this.f2141b = false;
                if (C0865g.this.f2068r > 40) {
                    C0865g.this.mo10767d();
                } else {
                    return;
                }
            } else {
                try {
                    String str = this.f1884j;
                    if (!C0865g.this.f2064n) {
                        this.f2141b = false;
                        return;
                    }
                    BDLocation bDLocation = new BDLocation(str);
                    if (bDLocation.getLocType() == 161 && bDLocation.getBuildingID() != null) {
                        BDLocation unused2 = C0865g.this.f2043Y = new BDLocation(bDLocation);
                    }
                    String indoorLocationSurpportBuidlingName = bDLocation.getIndoorLocationSurpportBuidlingName();
                    if (indoorLocationSurpportBuidlingName != null && !C0865g.this.f2035Q.mo10739a(indoorLocationSurpportBuidlingName)) {
                        C0865g.this.f2035Q.mo10740a(indoorLocationSurpportBuidlingName, (C0856a.C0857a) null);
                    }
                    if (C0865g.this.f2037S != null) {
                        C0865g.this.f2037S.mo10748a(new C0879k(this));
                    }
                    C0754n.m1911a().mo10493b(true);
                    if (bDLocation.getBuildingName() != null) {
                        String unused3 = C0865g.this.f2019A = bDLocation.getBuildingName();
                    }
                    if (bDLocation.getFloor() != null) {
                        long unused4 = C0865g.this.f2066p = System.currentTimeMillis();
                        this.f2148s = System.currentTimeMillis();
                        int i = (int) (this.f2148s - this.f2147r);
                        if (i > 10000) {
                            int unused5 = C0865g.this.f2042X = 0;
                        } else if (i < 3000) {
                            int unused6 = C0865g.this.f2042X = 2;
                        } else {
                            int unused7 = C0865g.this.f2042X = 1;
                        }
                        if (bDLocation.getFloor().contains("-a")) {
                            boolean unused8 = C0865g.this.f2029K = true;
                            bDLocation.setFloor(bDLocation.getFloor().split("-")[0]);
                        } else {
                            boolean unused9 = C0865g.this.f2029K = false;
                        }
                        C0865g.this.f2023E.add(bDLocation.getFloor());
                    }
                    Message obtainMessage = C0865g.this.f2045a.obtainMessage(21);
                    obtainMessage.obj = bDLocation;
                    obtainMessage.sendToTarget();
                } catch (Exception unused10) {
                }
            }
            if (this.f1885k != null) {
                this.f1885k.clear();
            }
            this.f2141b = false;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, double):double
         arg types: [com.baidu.location.indoor.g, int]
         candidates:
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, int):int
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, long):long
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, com.baidu.location.e.h):java.lang.String
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, java.lang.String):java.lang.String
          com.baidu.location.indoor.g.a(com.baidu.location.BDLocation, int):void
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, android.os.Message):void
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, com.baidu.location.BDLocation):void
          com.baidu.location.indoor.g.a(java.lang.String, java.lang.String):void
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, boolean):boolean
          com.baidu.location.indoor.g.a(double, double):boolean
          com.baidu.location.indoor.g.a(android.location.Location, java.util.ArrayList<java.util.ArrayList<java.lang.Float>>):boolean
          com.baidu.location.indoor.g.a(com.baidu.location.indoor.g, double):double */
        /* renamed from: b */
        public void mo10508b() {
            if (this.f2141b) {
                this.f2142c = true;
            } else if (C0865g.this.f2054c != 1 || C0865g.this.f2055d || System.currentTimeMillis() - this.f2145f >= 30000 || System.currentTimeMillis() - C0865g.this.f2050ae.f2103c <= 30000) {
                StringBuffer stringBuffer = new StringBuffer(1024);
                String h = C0822b.m2279a().mo10631f().mo10623h();
                String f = C0826e.m2306a().mo10643f();
                double unused = C0865g.this.f2028J = 0.5d;
                C0834h q = C0835i.m2376a().mo10700q();
                String a = C0865g.this.m2521a(q);
                if (a == null) {
                    a = q.mo10666a(C0865g.this.f2056e, true, false);
                }
                if (a != null && a.length() >= 10) {
                    String str = this.f2144e;
                    if (str == null || !str.equals(a)) {
                        this.f2144e = a;
                        this.f2141b = true;
                        stringBuffer.append(h);
                        if (f != null) {
                            stringBuffer.append(f);
                        }
                        stringBuffer.append("&coor=gcj02");
                        stringBuffer.append("&lt=1");
                        stringBuffer.append(a);
                        if (!(C0865g.this.f2060j == null || C0865g.this.f2033O > 2 || C0865g.this.f2060j.mo10791h() == null)) {
                            stringBuffer.append("&idsl=" + C0865g.this.f2060j.mo10791h());
                        }
                        int size = C0865g.this.f2031M.size();
                        stringBuffer.append(C0865g.this.m2520a(size));
                        int unused2 = C0865g.this.f2032N = size;
                        C0865g.m2573z(C0865g.this);
                        stringBuffer.append("&drsi=" + C0865g.this.f2033O);
                        stringBuffer.append("&drc=" + C0865g.this.f2071u);
                        if (!(C0865g.this.f2026H == 0.0d || C0865g.this.f2027I == 0.0d)) {
                            stringBuffer.append("&lst_idl=" + String.format(Locale.CHINA, "%.5f:%.5f", Double.valueOf(C0865g.this.f2026H), Double.valueOf(C0865g.this.f2027I)));
                        }
                        int unused3 = C0865g.this.f2071u = 0;
                        stringBuffer.append("&idpfv=1");
                        stringBuffer.append("&iflxy=" + C0865g.this.f2051af.toString());
                        C0865g.this.f2051af.f2124c.clear();
                        if (C0865g.this.f2060j != null && C0865g.this.f2060j.mo10790g()) {
                            stringBuffer.append("&pdr2=1");
                        }
                        if (!(C0865g.this.f2037S == null || C0865g.this.f2037S.mo10752e() == null || !C0865g.this.f2037S.mo10754g())) {
                            stringBuffer.append("&bleand=");
                            stringBuffer.append(C0865g.this.f2037S.mo10752e());
                            stringBuffer.append("&bleand_et=");
                            stringBuffer.append(C0865g.this.f2037S.mo10753f());
                        }
                        C0865g.m2513D(C0865g.this);
                        if (C0865g.this.f2036R != null) {
                            stringBuffer.append(C0865g.this.f2036R);
                            String unused4 = C0865g.this.f2036R = (String) null;
                        }
                        String d = C0723a.m1729a().mo10432d();
                        if (d != null) {
                            stringBuffer.append(d);
                        }
                        stringBuffer.append(C0844b.m2417a().mo10713a(true));
                        this.f2143d = stringBuffer.toString();
                        ExecutorService b = C0762v.m1943a().mo10504b();
                        if (b != null) {
                            mo10731a(b, C0855k.f1962f);
                        } else {
                            mo10734c(C0855k.f1962f);
                        }
                        this.f2145f = System.currentTimeMillis();
                    }
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0012, code lost:
            return;
         */
        /* renamed from: c */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public synchronized void mo10499c() {
            /*
                r1 = this;
                monitor-enter(r1)
                boolean r0 = r1.f2141b     // Catch:{ all -> 0x0013 }
                if (r0 == 0) goto L_0x0007
                monitor-exit(r1)
                return
            L_0x0007:
                boolean r0 = r1.f2142c     // Catch:{ all -> 0x0013 }
                if (r0 == 0) goto L_0x0011
                r0 = 0
                r1.f2142c = r0     // Catch:{ all -> 0x0013 }
                r1.mo10508b()     // Catch:{ all -> 0x0013 }
            L_0x0011:
                monitor-exit(r1)
                return
            L_0x0013:
                r0 = move-exception
                monitor-exit(r1)
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.indoor.C0865g.C0875i.mo10499c():void");
        }
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x00a0 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C0865g() {
        /*
            r5 = this;
            r5.<init>()
            r0 = 0
            r5.f2054c = r0
            r5.f2055d = r0
            r1 = 32
            r5.f2056e = r1
            r1 = 3000(0xbb8, double:1.482E-320)
            r5.f2058h = r1
            r1 = 1
            r5.f2059i = r1
            r2 = 0
            r5.f2045a = r2
            r5.f2060j = r2
            r5.f2061k = r2
            r5.f2062l = r2
            r3 = 0
            r5.f2063m = r3
            r5.f2064n = r0
            r5.f2065o = r0
            r5.f2066p = r3
            r5.f2067q = r3
            r5.f2068r = r0
            r5.f2069s = r2
            r5.f2071u = r0
            r5.f2072v = r0
            r5.f2073w = r2
            r5.f2074x = r2
            r5.f2075y = r2
            r5.f2076z = r2
            r5.f2019A = r2
            r5.f2020B = r2
            r5.f2021C = r0
            r3 = 3
            r5.f2022D = r3
            r5.f2023E = r2
            r3 = 20
            r5.f2024F = r3
            r5.f2025G = r2
            r3 = 0
            r5.f2026H = r3
            r5.f2027I = r3
            r3 = 4600877379321698714(0x3fd999999999999a, double:0.4)
            r5.f2028J = r3
            r5.f2029K = r0
            r5.f2030L = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            java.util.List r1 = java.util.Collections.synchronizedList(r1)
            r5.f2031M = r1
            r1 = -1
            r5.f2032N = r1
            r5.f2033O = r0
            r5.f2034P = r0
            r5.f2036R = r2
            r5.f2037S = r2
            r5.f2038T = r0
            java.text.SimpleDateFormat r1 = new java.text.SimpleDateFormat
            java.lang.String r3 = "yyyy-MM-dd HH:mm:ss"
            r1.<init>(r3)
            r5.f2053b = r1
            r1 = 2
            r5.f2042X = r1
            r5.f2043Y = r2
            r5.f2044Z = r0
            r5.f2046aa = r0
            r5.f2047ab = r0
            r5.f2048ac = r0
            r5.f2049ad = r2
            r5.f2050ae = r2
            r5.f2051af = r2
            r5.f2052ag = r2
            r5.f2057f = r0
            com.baidu.location.indoor.g$d r0 = new com.baidu.location.indoor.g$d
            r0.<init>()
            r5.f2045a = r0
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()     // Catch:{ Exception -> 0x00a0 }
            com.baidu.location.indoor.mapversion.p022c.C0896a.m2741a(r0)     // Catch:{ Exception -> 0x00a0 }
        L_0x00a0:
            android.content.Context r0 = com.baidu.location.C0839f.getServiceContext()     // Catch:{ Exception -> 0x00a7 }
            com.baidu.location.indoor.mapversion.p022c.C0902c.m2779a(r0)     // Catch:{ Exception -> 0x00a7 }
        L_0x00a7:
            com.baidu.location.indoor.p r0 = new com.baidu.location.indoor.p
            r0.<init>()
            r5.f2039U = r0
            com.baidu.location.indoor.p r0 = r5.f2039U
            r1 = 1000(0x3e8, double:4.94E-321)
            r0.mo10838a(r1)
            com.baidu.location.indoor.h r0 = new com.baidu.location.indoor.h
            r0.<init>(r5)
            r5.f2040V = r0
            com.baidu.location.indoor.i r0 = new com.baidu.location.indoor.i
            r0.<init>(r5)
            r5.f2070t = r0
            com.baidu.location.indoor.m r0 = new com.baidu.location.indoor.m
            android.content.Context r1 = com.baidu.location.C0839f.getServiceContext()
            com.baidu.location.indoor.m$a r2 = r5.f2070t
            r0.<init>(r1, r2)
            r5.f2060j = r0
            com.baidu.location.indoor.g$i r0 = new com.baidu.location.indoor.g$i
            r0.<init>()
            r5.f2062l = r0
            com.baidu.location.indoor.c r0 = new com.baidu.location.indoor.c
            int r1 = r5.f2022D
            r0.<init>(r1)
            r5.f2023E = r0
            com.baidu.location.indoor.c r0 = new com.baidu.location.indoor.c
            int r1 = r5.f2024F
            r0.<init>(r1)
            r5.f2025G = r0
            com.baidu.location.indoor.a r0 = new com.baidu.location.indoor.a
            android.content.Context r1 = com.baidu.location.C0839f.getServiceContext()
            r0.<init>(r1)
            r5.f2035Q = r0
            com.baidu.location.indoor.g$c r0 = new com.baidu.location.indoor.g$c
            r0.<init>()
            r5.f2049ad = r0
            r5.m2552i()
            com.baidu.location.indoor.g$e r0 = new com.baidu.location.indoor.g$e
            r0.<init>()
            r5.f2050ae = r0
            com.baidu.location.indoor.g$f r0 = new com.baidu.location.indoor.g$f
            r0.<init>()
            r5.f2051af = r0
            com.baidu.location.indoor.g$b r0 = new com.baidu.location.indoor.g$b
            r0.<init>()
            r5.f2052ag = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.indoor.C0865g.<init>():void");
    }

    /* renamed from: D */
    static /* synthetic */ int m2513D(C0865g gVar) {
        int i = gVar.f2034P;
        gVar.f2034P = i + 1;
        return i;
    }

    /* renamed from: a */
    public static synchronized C0865g m2519a() {
        C0865g gVar;
        synchronized (C0865g.class) {
            if (f2018g == null) {
                f2018g = new C0865g();
            }
            gVar = f2018g;
        }
        return gVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m2520a(int i) {
        if (this.f2031M.size() == 0) {
            return "&dr=0:0";
        }
        StringBuilder sb = new StringBuilder();
        sb.append("&dr=");
        int i2 = 1;
        this.f2031M.get(0).f2137d = 1;
        sb.append(this.f2031M.get(0).toString());
        int i3 = this.f2031M.get(0).f2134a;
        while (i2 < this.f2031M.size() && i2 <= i) {
            this.f2031M.get(i2).f2137d = this.f2031M.get(i2).f2134a - i3;
            sb.append(";");
            sb.append(this.f2031M.get(i2).toString());
            i3 = this.f2031M.get(i2).f2134a;
            i2++;
        }
        return sb.toString();
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public String m2521a(C0834h hVar) {
        int a = hVar.mo10664a();
        if (a <= this.f2056e) {
            return hVar.mo10666a(this.f2056e, true, true) + "&aprk=0";
        }
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i = 0; i < a; i++) {
            String lowerCase = hVar.f1789a.get(i).BSSID.replaceAll(Config.TRACE_TODAY_VISIT_SPLIT, "").toLowerCase();
            C0856a aVar = this.f2035Q;
            if (aVar == null || !aVar.mo10741b(lowerCase)) {
                arrayList2.add(hVar.f1789a.get(i));
            } else {
                arrayList.add(hVar.f1789a.get(i));
            }
        }
        String str = arrayList.size() > 0 ? "&aprk=3" : "";
        if (str.equals("")) {
            C0856a aVar2 = this.f2035Q;
            str = (aVar2 == null || !aVar2.mo10508b()) ? "&aprk=1" : "&aprk=2";
        }
        arrayList.addAll(arrayList2);
        hVar.f1789a = arrayList;
        String a2 = hVar.mo10666a(this.f2056e, true, true);
        return a2 + str;
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2524a(Message message) {
        C0881m mVar;
        C0880l lVar;
        if (this.f2064n) {
            this.f2065o = false;
            BDLocation bDLocation = (BDLocation) message.obj;
            if (bDLocation.getLocType() == 161) {
                m2559m();
                if (!(bDLocation.getIndoorSurpportPolygon() == null || bDLocation.getIndoorLocationSurpportBuidlingID() == null || ((lVar = this.f2075y) != null && lVar.mo10782a().equals(bDLocation.getBuildingID())))) {
                    String[] split = bDLocation.getIndoorSurpportPolygon().split("\\|");
                    Location[] locationArr = new Location[split.length];
                    for (int i = 0; i < split.length; i++) {
                        String[] split2 = split[i].split(",");
                        Location location = new Location("gps");
                        location.setLatitude(Double.valueOf(split2[1]).doubleValue());
                        location.setLongitude(Double.valueOf(split2[0]).doubleValue());
                        locationArr[i] = location;
                    }
                    this.f2075y = new C0880l(bDLocation.getIndoorLocationSurpportBuidlingID(), locationArr);
                }
                if (this.f2030L && this.f2037S != null) {
                    if ((((bDLocation.getIndoorLocationSource() >> 2) & 1) == 1) && this.f2037S.mo10747a()) {
                        this.f2030L = false;
                        this.f2037S.mo10749b();
                    }
                }
                this.f2068r = 0;
                if (bDLocation.getBuildingID() != null) {
                    this.f2065o = true;
                    bDLocation.setIndoorLocMode(true);
                    if (bDLocation.getRetFields("tp") == null || !bDLocation.getRetFields("tp").equalsIgnoreCase("ble")) {
                        this.f2038T = false;
                    } else {
                        bDLocation.setRadius(8.0f);
                        bDLocation.setNetworkLocationType("ble");
                        this.f2038T = true;
                    }
                    String retFields = bDLocation.getRetFields("pdr2");
                    if (!(retFields == null || !retFields.equals("1") || (mVar = this.f2060j) == null)) {
                        mVar.mo10784a(true);
                    }
                    this.f2074x = bDLocation.getBuildingID();
                    this.f2076z = bDLocation.getBuildingName();
                    this.f2020B = bDLocation.getNetworkLocationType();
                    this.f2021C = bDLocation.isParkAvailable();
                    int unused = this.f2052ag.m2585a(bDLocation);
                    if (bDLocation.getFloor().equals(m2558l())) {
                        if (this.f2073w == null) {
                            this.f2073w = bDLocation.getFloor();
                        }
                        C0896a.m2740a().mo10814a(bDLocation.getLongitude(), bDLocation.getLatitude());
                        m2530a(bDLocation.getBuildingName(), bDLocation.getFloor());
                        if (bDLocation.getFloor().equals(m2558l())) {
                            if (!bDLocation.getFloor().equalsIgnoreCase(this.f2073w) && this.f2047ab) {
                                this.f2050ae.m2597a();
                                C0895a.m2739c();
                                this.f2048ac = C0895a.m2734a(bDLocation.getFloor());
                            }
                            this.f2073w = bDLocation.getFloor();
                            C0881m mVar2 = this.f2060j;
                            if (mVar2 != null && mVar2.mo10788e() >= 0.0d && bDLocation.getDirection() <= 0.0f) {
                                bDLocation.setDirection((float) this.f2060j.mo10788e());
                            }
                            double[] a = C0895a.m2736a(bDLocation);
                            if (!(a == null || a[0] == -1.0d || a[0] != 0.0d)) {
                                bDLocation.setLongitude(a[1]);
                                bDLocation.setLatitude(a[2]);
                                bDLocation.setFusionLocInfo("res", a);
                                bDLocation.setRadius((float) a[5]);
                                bDLocation.setDirection((float) a[6]);
                                bDLocation.setSpeed((float) a[8]);
                                if (!this.f2050ae.m2603a(bDLocation, a[5], "wifi")) {
                                    mo10767d();
                                    return;
                                }
                            }
                            this.f2027I = bDLocation.getLatitude();
                            this.f2026H = bDLocation.getLongitude();
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            } else if (bDLocation.getLocType() == 63) {
                this.f2068r++;
                if (this.f2068r > 10) {
                    mo10767d();
                } else {
                    return;
                }
            } else {
                this.f2068r = 0;
            }
            if (this.f2065o) {
                if (bDLocation.getTime() == null) {
                    bDLocation.setTime(this.f2053b.format(new Date()));
                }
                BDLocation bDLocation2 = new BDLocation(bDLocation);
                bDLocation2.setNetworkLocationType(bDLocation2.getNetworkLocationType() + "2");
                C0908p pVar = this.f2039U;
                if (pVar == null || !pVar.mo10841c()) {
                    m2526a(bDLocation2, 21);
                } else {
                    this.f2039U.mo10839a(bDLocation2);
                }
            }
            this.f2062l.mo10499c();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2525a(BDLocation bDLocation) {
    }

    /* access modifiers changed from: private */
    /* renamed from: a */
    public void m2526a(BDLocation bDLocation, int i) {
        if (bDLocation != null) {
            if (bDLocation.getNetworkLocationType().startsWith("vps")) {
                if (bDLocation.getLongitude() == -1.0d && bDLocation.getLatitude() == -1.0d) {
                    bDLocation.setUserIndoorState(-1);
                } else {
                    bDLocation.setUserIndoorState(1);
                }
                bDLocation.setIndoorNetworkState(this.f2042X);
                C0723a.m1729a().mo10422a(bDLocation);
                return;
            }
            if (this.f2043Y != null) {
                if (bDLocation.getAddrStr() == null && this.f2043Y.getAddrStr() != null) {
                    bDLocation.setAddr(this.f2043Y.getAddress());
                    bDLocation.setAddrStr(this.f2043Y.getAddrStr());
                }
                if (bDLocation.getPoiList() == null && this.f2043Y.getPoiList() != null) {
                    bDLocation.setPoiList(this.f2043Y.getPoiList());
                }
                if (bDLocation.getLocationDescribe() == null && this.f2043Y.getLocationDescribe() != null) {
                    bDLocation.setLocationDescribe(this.f2043Y.getLocationDescribe());
                }
                if (bDLocation.getNrlResult() == null) {
                    bDLocation.setNrlData(this.f2043Y.getNrlResult());
                }
            }
            if (bDLocation != null) {
                bDLocation.setTime(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(new Date(System.currentTimeMillis())));
                if (bDLocation.getNetworkLocationType().contains("2")) {
                    String networkLocationType = bDLocation.getNetworkLocationType();
                    bDLocation.setNetworkLocationType(networkLocationType.substring(0, networkLocationType.length() - 1));
                    bDLocation.setUserIndoorState(1);
                    bDLocation.setIndoorNetworkState(this.f2042X);
                    C0723a.m1729a().mo10422a(bDLocation);
                    BDLocation bDLocation2 = new BDLocation(bDLocation);
                    bDLocation2.setRadius(this.f2038T ? 8.0f : 15.0f);
                    Message obtainMessage = this.f2045a.obtainMessage(801);
                    obtainMessage.obj = bDLocation2;
                    obtainMessage.sendToTarget();
                }
            }
        }
    }

    /* renamed from: a */
    private void m2530a(String str, String str2) {
        String str3 = this.f2076z;
        if (str3 == null || !str3.equals(str) || !this.f2047ab) {
            C0896a a = C0896a.m2740a();
            a.mo10815a(CoordinateType.GCJ02);
            a.mo10816a(str, new C0878j(this, str, str2));
        }
    }

    /* renamed from: g */
    static /* synthetic */ int m2549g(C0865g gVar) {
        int i = gVar.f2071u;
        gVar.f2071u = i + 1;
        return i;
    }

    /* renamed from: i */
    private void m2552i() {
    }

    /* renamed from: j */
    private void m2554j() {
        this.f2023E.clear();
        this.f2025G.clear();
        this.f2066p = 0;
        this.f2068r = 0;
        this.f2021C = 0;
        this.f2072v = 0;
        this.f2073w = null;
        this.f2074x = null;
        this.f2076z = null;
        this.f2019A = null;
        this.f2020B = null;
        this.f2030L = true;
        this.f2028J = 0.4d;
        this.f2038T = false;
        this.f2026H = 0.0d;
        this.f2027I = 0.0d;
        this.f2029K = false;
        this.f2033O = 0;
        this.f2071u = 0;
        this.f2069s = null;
        this.f2067q = 0;
        this.f2050ae.m2597a();
        C0895a.m2739c();
        if (this.f2047ab) {
            C0896a.m2740a().mo10818b();
        }
        this.f2048ac = false;
        this.f2047ab = false;
        C0754n.m1911a().mo10493b(false);
        C0860d dVar = this.f2037S;
        if (dVar != null) {
            dVar.mo10750c();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: k */
    public void m2555k() {
        if (this.f2064n) {
            this.f2059i = true;
            this.f2050ae.m2610d();
            this.f2062l.mo10508b();
            this.f2063m = System.currentTimeMillis();
        }
    }

    /* renamed from: l */
    private String m2558l() {
        if (this.f2052ag.f2078b == 1 && this.f2052ag.f2077a != null) {
            return this.f2052ag.m2593b();
        }
        HashMap hashMap = new HashMap();
        int size = this.f2023E.size();
        String str = null;
        int i = -1;
        int i2 = 0;
        String str2 = "";
        while (i2 < size) {
            try {
                String str3 = (String) this.f2023E.get(i2);
                str2 = str2 + str3 + "|";
                hashMap.put(str3, hashMap.containsKey(str3) ? Integer.valueOf(((Integer) hashMap.get(str3)).intValue() + 1) : 1);
                i2++;
            } catch (Exception unused) {
                return this.f2073w;
            }
        }
        for (String str4 : hashMap.keySet()) {
            if (((Integer) hashMap.get(str4)).intValue() > i) {
                i = ((Integer) hashMap.get(str4)).intValue();
                str = str4;
            }
        }
        return str;
    }

    /* renamed from: m */
    private void m2559m() {
        for (int i = this.f2032N; i >= 0 && this.f2031M.size() > 0; i--) {
            this.f2031M.remove(0);
        }
        this.f2032N = -1;
    }

    /* renamed from: v */
    static /* synthetic */ int m2569v(C0865g gVar) {
        int i = gVar.f2068r;
        gVar.f2068r = i + 1;
        return i;
    }

    /* renamed from: z */
    static /* synthetic */ int m2573z(C0865g gVar) {
        int i = gVar.f2033O;
        gVar.f2033O = i + 1;
        return i;
    }

    /* renamed from: a */
    public boolean mo10761a(double d, double d2) {
        Map<String, C0902c.C0904b> d3;
        C0902c a = C0902c.m2778a();
        if (!a.mo10832c() || !a.mo10831b() || (d3 = a.mo10833d()) == null) {
            return false;
        }
        String str = null;
        Iterator<String> it = d3.keySet().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            C0902c.C0904b bVar = d3.get(it.next());
            if (d > bVar.f2295e && d < bVar.f2293c && d2 > bVar.f2296f && d2 < bVar.f2294d) {
                String str2 = bVar.f2292b;
                str = bVar.f2291a;
                String str3 = bVar.f2297g;
                break;
            }
        }
        return str != null;
    }

    /* renamed from: a */
    public boolean mo10762a(Location location, ArrayList<ArrayList<Float>> arrayList) {
        String str;
        if (arrayList.size() == 0 || !C0826e.m2306a().mo10647j()) {
            return false;
        }
        if (!this.f2064n && location.getSpeed() > 3.0f) {
            return false;
        }
        double[] coorEncrypt = Jni.coorEncrypt(location.getLongitude(), location.getLatitude(), BDLocation.BDLOCATION_WGS84_TO_GCJ02);
        double d = coorEncrypt[0];
        double d2 = coorEncrypt[1];
        double accuracy = (double) location.getAccuracy();
        double bearing = (double) location.getBearing();
        double altitude = location.getAltitude();
        double speed = (double) location.getSpeed();
        boolean z = mo10761a(d, d2) || this.f2054c == 1;
        if (!this.f2064n && !z) {
            return false;
        }
        try {
            double d3 = speed;
            this.f2050ae.m2599a(location, z);
            if (this.f2050ae.m2607b()) {
                mo10766c();
                return true;
            } else if (!mo10768e()) {
                return false;
            } else {
                double d4 = d3;
                double d5 = altitude;
                double d6 = bearing;
                double d7 = accuracy;
                if (this.f2050ae.m2602a(d, d2, accuracy)) {
                    C0895a.m2739c();
                }
                double[] a = C0895a.m2735a(d, d2, this.f2049ad.mo10772a(d7, d, d2, d5), d6, d4);
                if (a == null) {
                    return false;
                }
                if (a[0] == -1.0d) {
                    return false;
                }
                if (a[0] != 0.0d) {
                    return false;
                }
                BDLocation bDLocation = new BDLocation();
                bDLocation.setAltitude(d5);
                bDLocation.setLatitude(a[2]);
                bDLocation.setLongitude(a[1]);
                if (this.f2038T) {
                    bDLocation.setRadius(8.0f);
                } else {
                    bDLocation.setRadius(15.0f);
                }
                bDLocation.setDirection((float) d6);
                bDLocation.setSpeed((float) d4);
                bDLocation.setLocType(BDLocation.TypeNetWorkLocation);
                bDLocation.setNetworkLocationType("gps");
                if (System.currentTimeMillis() - this.f2050ae.f2103c < 20000) {
                    bDLocation.setFloor(this.f2073w);
                    bDLocation.setBuildingName(this.f2076z);
                    str = this.f2074x;
                } else {
                    str = null;
                    bDLocation.setFloor(null);
                    bDLocation.setBuildingName(null);
                }
                bDLocation.setBuildingID(str);
                bDLocation.setIndoorLocMode(true);
                this.f2027I = bDLocation.getLatitude();
                this.f2026H = bDLocation.getLongitude();
                bDLocation.setFusionLocInfo("res", a);
                bDLocation.setRadius((float) a[5]);
                bDLocation.setDirection((float) a[6]);
                bDLocation.setSpeed((float) a[8]);
                bDLocation.setTime(this.f2053b.format(new Date()));
                BDLocation bDLocation2 = new BDLocation(bDLocation);
                String networkLocationType = bDLocation2.getNetworkLocationType();
                bDLocation2.setNetworkLocationType(networkLocationType + "2");
                if (this.f2039U == null || !this.f2039U.mo10841c()) {
                    m2526a(bDLocation2, 21);
                } else {
                    this.f2039U.mo10839a(bDLocation2);
                }
                if (this.f2050ae.m2603a(bDLocation, a[5], "gps")) {
                    return true;
                }
                mo10767d();
                return true;
            }
        } catch (Exception unused) {
            return false;
        }
    }

    /* renamed from: a */
    public boolean mo10763a(Bundle bundle) {
        if (bundle == null || this.f2041W == null) {
            return false;
        }
        C0884a.C0888d dVar = new C0884a.C0888d();
        dVar.mo10799b(bundle.getString("bid")).mo10800c(bundle.getString("code"));
        dVar.mo10795a(bundle.getDouble("fov")).mo10798a(bundle.getFloatArray("gravity"));
        dVar.mo10796a(bundle.getString("image"));
        dVar.mo10797a(bundle.getBoolean("force_online"));
        this.f2041W.mo10792a(dVar);
        return true;
    }

    /* renamed from: b */
    public synchronized void mo10764b() {
        if (this.f2064n) {
            this.f2023E.clear();
        }
    }

    /* renamed from: b */
    public boolean mo10765b(Bundle bundle) {
        if (bundle == null) {
            return false;
        }
        this.f2054c = bundle.getInt("mode");
        return true;
    }

    /* renamed from: c */
    public synchronized void mo10766c() {
        if (!this.f2064n) {
            C0895a.m2738b();
            this.f2066p = System.currentTimeMillis();
            this.f2067q = System.currentTimeMillis();
            this.f2060j.mo10783a();
            this.f2061k = new C0873g();
            this.f2061k.start();
            this.f2065o = false;
            this.f2064n = true;
            this.f2037S = C0860d.m2490a(C0839f.getServiceContext());
            this.f2033O = 0;
            this.f2071u = 0;
            C0754n.m1911a().mo10493b(true);
        }
    }

    /* renamed from: d */
    public synchronized void mo10767d() {
        if (this.f2064n) {
            this.f2064n = false;
            this.f2060j.mo10785b();
            if (this.f2039U != null && this.f2039U.mo10841c()) {
                this.f2039U.mo10837a();
            }
            if (this.f2035Q != null) {
                this.f2035Q.mo10499c();
            }
            if (this.f2037S != null) {
                this.f2037S.mo10751d();
            }
            if (this.f2061k != null) {
                boolean unused = this.f2061k.f2130b = false;
                this.f2061k.interrupt();
                this.f2061k = null;
            }
            m2554j();
            this.f2065o = false;
            C0723a.m1729a().mo10428c();
        }
    }

    /* renamed from: e */
    public boolean mo10768e() {
        return this.f2064n;
    }

    /* renamed from: f */
    public boolean mo10769f() {
        return this.f2064n && this.f2050ae.m2612e();
    }

    /* renamed from: g */
    public String mo10770g() {
        return this.f2073w;
    }

    /* renamed from: h */
    public String mo10771h() {
        return this.f2074x;
    }
}
