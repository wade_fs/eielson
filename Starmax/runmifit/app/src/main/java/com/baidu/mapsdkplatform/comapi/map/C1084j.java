package com.baidu.mapsdkplatform.comapi.map;

import android.content.Context;
import android.graphics.Rect;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import com.baidu.mapapi.common.EnvironmentUtilities;
import com.baidu.mapapi.common.SysOSUtil;
import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.MapRenderer;
import com.baidu.mobstat.Config;
import javax.microedition.khronos.egl.EGL10;
import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.egl.EGLContext;
import javax.microedition.khronos.egl.EGLDisplay;

/* renamed from: com.baidu.mapsdkplatform.comapi.map.j */
public class C1084j extends GLSurfaceView implements GestureDetector.OnDoubleTapListener, GestureDetector.OnGestureListener, MapRenderer.C1061a {

    /* renamed from: a */
    private static final String f3567a = C1084j.class.getSimpleName();

    /* renamed from: b */
    private Handler f3568b;

    /* renamed from: c */
    private MapRenderer f3569c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public int f3570d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public int f3571e;

    /* renamed from: f */
    private GestureDetector f3572f;
    /* access modifiers changed from: private */

    /* renamed from: g */
    public C1078e f3573g;

    /* renamed from: com.baidu.mapsdkplatform.comapi.map.j$a */
    static class C1085a {

        /* renamed from: a */
        float f3574a;

        /* renamed from: b */
        float f3575b;

        /* renamed from: c */
        float f3576c;

        /* renamed from: d */
        float f3577d;

        /* renamed from: e */
        boolean f3578e;

        /* renamed from: f */
        float f3579f;

        /* renamed from: g */
        float f3580g;

        /* renamed from: h */
        double f3581h;

        C1085a() {
        }

        public String toString() {
            return "MultiTouch{x1=" + this.f3574a + ", x2=" + this.f3575b + ", y1=" + this.f3576c + ", y2=" + this.f3577d + ", mTwoTouch=" + this.f3578e + ", centerX=" + this.f3579f + ", centerY=" + this.f3580g + ", length=" + this.f3581h + '}';
        }
    }

    public C1084j(Context context, C1102z zVar, String str, int i) {
        super(context);
        if (context != null) {
            setEGLContextClientVersion(2);
            this.f3572f = new GestureDetector(context, this);
            EnvironmentUtilities.initAppDirectory(context);
            if (this.f3573g == null) {
                this.f3573g = new C1078e(context, str, i);
            }
            this.f3573g.mo13008a(context.hashCode());
            m3657g();
            this.f3573g.mo13006a();
            this.f3573g.mo13020a(zVar);
            m3658h();
            this.f3573g.mo13012a(this.f3568b);
            this.f3573g.mo13048f();
            setBackgroundColor(0);
            return;
        }
        throw new RuntimeException("BDMapSDKException: when you create an mapview, the context can not be null");
    }

    /* renamed from: a */
    private static boolean m3654a(int i, int i2, int i3, int i4, int i5, int i6) {
        EGL10 egl10 = (EGL10) EGLContext.getEGL();
        EGLDisplay eglGetDisplay = egl10.eglGetDisplay(EGL10.EGL_DEFAULT_DISPLAY);
        egl10.eglInitialize(eglGetDisplay, new int[2]);
        int[] iArr = new int[1];
        return egl10.eglChooseConfig(eglGetDisplay, new int[]{12324, i, 12323, i2, 12322, i3, 12321, i4, 12325, i5, 12326, i6, 12344}, new EGLConfig[100], 100, iArr) && iArr[0] > 0;
    }

    /* renamed from: g */
    private void m3657g() {
        try {
            if (m3654a(8, 8, 8, 8, 24, 0)) {
                setEGLConfigChooser(8, 8, 8, 8, 24, 0);
            } else if (m3654a(5, 6, 5, 0, 24, 0)) {
                setEGLConfigChooser(5, 6, 5, 0, 24, 0);
            } else {
                setEGLConfigChooser(true);
            }
        } catch (IllegalArgumentException unused) {
            setEGLConfigChooser(true);
        }
        this.f3569c = new MapRenderer(this, this);
        this.f3569c.mo12944a(this.f3573g.f3534j);
        setRenderer(this.f3569c);
        setRenderMode(1);
    }

    /* renamed from: h */
    private void m3658h() {
        this.f3568b = new C1086k(this);
    }

    /* renamed from: a */
    public C1078e mo13086a() {
        return this.f3573g;
    }

    /* renamed from: a */
    public void mo13087a(float f, float f2) {
        C1078e eVar = this.f3573g;
        if (eVar != null && eVar.f3533i != null) {
            this.f3573g.mo13031b(f, f2);
        }
    }

    /* renamed from: a */
    public void mo13088a(int i) {
        int i2;
        if (this.f3573g != null) {
            Message message = new Message();
            message.what = 50;
            message.obj = Long.valueOf(this.f3573g.f3534j);
            boolean q = this.f3573g.mo13070q();
            if (i == 3) {
                i2 = 0;
            } else {
                if (q) {
                    i2 = 1;
                }
                this.f3568b.sendMessage(message);
            }
            message.arg1 = i2;
            this.f3568b.sendMessage(message);
        }
    }

    /* renamed from: a */
    public void mo13089a(String str, Rect rect) {
        C1078e eVar = this.f3573g;
        if (eVar != null && eVar.f3533i != null) {
            if (rect != null) {
                int i = rect.left;
                int i2 = this.f3571e < rect.bottom ? 0 : this.f3571e - rect.bottom;
                int width = rect.width();
                int height = rect.height();
                if (i >= 0 && i2 >= 0 && width > 0 && height > 0) {
                    if (width > this.f3570d) {
                        width = Math.abs(rect.width()) - (rect.right - this.f3570d);
                    }
                    if (height > this.f3571e) {
                        height = Math.abs(rect.height()) - (rect.bottom - this.f3571e);
                    }
                    if (i > SysOSUtil.getScreenSizeX() || i2 > SysOSUtil.getScreenSizeY()) {
                        this.f3573g.f3533i.mo13476a(str, (Bundle) null);
                        requestRender();
                        return;
                    }
                    this.f3570d = width;
                    this.f3571e = height;
                    Bundle bundle = new Bundle();
                    bundle.putInt(Config.EVENT_HEAT_X, i);
                    bundle.putInt("y", i2);
                    bundle.putInt("width", width);
                    bundle.putInt("height", height);
                    this.f3573g.f3533i.mo13476a(str, bundle);
                } else {
                    return;
                }
            } else {
                this.f3573g.f3533i.mo13476a(str, (Bundle) null);
            }
            requestRender();
        }
    }

    /* renamed from: a */
    public boolean mo13090a(float f, float f2, float f3, float f4) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null) {
            return false;
        }
        return this.f3573g.mo13024a(f, f2, f3, f4);
    }

    /* renamed from: b */
    public void mo13091b() {
        C1078e eVar = this.f3573g;
        if (eVar != null) {
            eVar.mo13077u();
        }
    }

    /* renamed from: b */
    public void mo13092b(int i) {
        C1078e eVar = this.f3573g;
        if (eVar != null) {
            if (eVar.f3532h != null) {
                for (C1087l lVar : this.f3573g.f3532h) {
                    if (lVar != null) {
                        lVar.mo11558f();
                    }
                }
            }
            this.f3573g.mo13034b(this.f3568b);
            this.f3573g.mo13032b(i);
            this.f3573g = null;
        }
        Handler handler = this.f3568b;
        if (handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    /* renamed from: b */
    public boolean mo13093b(float f, float f2) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null) {
            return false;
        }
        return this.f3573g.mo13045d(f, f2);
    }

    /* renamed from: c */
    public void mo13094c() {
        C1078e eVar = this.f3573g;
        if (eVar != null) {
            eVar.mo13079v();
        }
    }

    /* renamed from: c */
    public boolean mo13095c(float f, float f2) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null) {
            return false;
        }
        return this.f3573g.mo13040c(f, f2);
    }

    /* renamed from: d */
    public void mo13096d() {
        getHolder().setFormat(-3);
        this.f3573g.f3533i.mo13526s();
    }

    /* renamed from: d */
    public boolean mo13097d(float f, float f2) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null) {
            return false;
        }
        return this.f3573g.mo13041c((int) f, (int) f2);
    }

    /* renamed from: e */
    public void mo13098e() {
        getHolder().setFormat(-1);
        this.f3573g.f3533i.mo13527t();
    }

    /* renamed from: f */
    public void mo12948f() {
    }

    public boolean onDoubleTap(MotionEvent motionEvent) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null || !this.f3573g.f3535k) {
            return true;
        }
        GeoPoint b = this.f3573g.mo13030b((int) motionEvent.getX(), (int) motionEvent.getY());
        if (b != null) {
            if (this.f3573g.f3532h != null) {
                for (C1087l lVar : this.f3573g.f3532h) {
                    if (lVar != null) {
                        lVar.mo11548b(b);
                    }
                }
            }
            if (this.f3573g.f3530f) {
                C1064ab E = this.f3573g.mo12989E();
                E.f3414a += 1.0f;
                if (!this.f3573g.f3531g) {
                    E.f3417d = b.getLongitudeE6();
                    E.f3418e = b.getLatitudeE6();
                }
                BaiduMap.mapStatusReason |= 1;
                this.f3573g.mo13016a(E, 300);
                C1078e eVar2 = this.f3573g;
                C1078e.f3479m = System.currentTimeMillis();
                return true;
            }
        }
        return false;
    }

    public boolean onDoubleTapEvent(MotionEvent motionEvent) {
        return false;
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null || !this.f3573g.f3535k) {
            return true;
        }
        if (!this.f3573g.f3529e) {
            return false;
        }
        float sqrt = (float) Math.sqrt((double) ((f * f) + (f2 * f2)));
        if (sqrt <= 500.0f) {
            return false;
        }
        BaiduMap.mapStatusReason |= 1;
        this.f3573g.mo12985A();
        this.f3573g.mo13003a(34, (int) (sqrt * 0.6f), ((int) motionEvent2.getX()) | (((int) motionEvent2.getY()) << 16));
        this.f3573g.mo12997M();
        return true;
    }

    public void onLongPress(MotionEvent motionEvent) {
        C1078e eVar = this.f3573g;
        if (eVar != null && eVar.f3533i != null && this.f3573g.f3535k) {
            String a = this.f3573g.f3533i.mo13471a(-1, (int) motionEvent.getX(), (int) motionEvent.getY(), this.f3573g.f3536l);
            if (a == null || a.equals("")) {
                if (this.f3573g.f3532h != null) {
                    for (C1087l lVar : this.f3573g.f3532h) {
                        GeoPoint b = this.f3573g.mo13030b((int) motionEvent.getX(), (int) motionEvent.getY());
                        if (lVar != null) {
                            lVar.mo11552c(b);
                        }
                    }
                }
            } else if (this.f3573g.f3532h != null) {
                for (C1087l lVar2 : this.f3573g.f3532h) {
                    if (lVar2 != null) {
                        if (lVar2.mo11550b(a)) {
                            this.f3573g.f3539p = true;
                        } else {
                            lVar2.mo11552c(this.f3573g.mo13030b((int) motionEvent.getX(), (int) motionEvent.getY()));
                        }
                    }
                }
            }
        }
    }

    public void onPause() {
        super.onPause();
        C1078e eVar = this.f3573g;
        if (eVar != null && eVar.f3533i != null) {
            this.f3573g.f3533i.mo13497c();
        }
    }

    public void onResume() {
        super.onResume();
        C1078e eVar = this.f3573g;
        if (eVar != null && eVar.f3533i != null) {
            if (this.f3573g.f3532h != null) {
                for (C1087l lVar : this.f3573g.f3532h) {
                    if (lVar != null) {
                        lVar.mo11554d();
                    }
                }
            }
            this.f3573g.f3533i.mo13512g();
            this.f3573g.f3533i.mo13500d();
            this.f3573g.f3533i.mo13521n();
            setRenderMode(1);
        }
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f, float f2) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x005a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onSingleTapConfirmed(android.view.MotionEvent r7) {
        /*
            r6 = this;
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3573g
            r1 = 1
            if (r0 == 0) goto L_0x00ab
            com.baidu.mapsdkplatform.comjni.map.basemap.a r0 = r0.f3533i
            if (r0 == 0) goto L_0x00ab
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3573g
            boolean r0 = r0.f3535k
            if (r0 != 0) goto L_0x0011
            goto L_0x00ab
        L_0x0011:
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3573g
            com.baidu.mapsdkplatform.comjni.map.basemap.a r0 = r0.f3533i
            r2 = -1
            float r3 = r7.getX()
            int r3 = (int) r3
            float r4 = r7.getY()
            int r4 = (int) r4
            com.baidu.mapsdkplatform.comapi.map.e r5 = r6.f3573g
            int r5 = r5.f3536l
            java.lang.String r0 = r0.mo13471a(r2, r3, r4, r5)
            r2 = 0
            if (r0 == 0) goto L_0x007a
            java.lang.String r3 = ""
            boolean r3 = r0.equals(r3)
            if (r3 != 0) goto L_0x007a
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x004f }
            r3.<init>(r0)     // Catch:{ JSONException -> 0x004f }
            java.lang.String r0 = "px"
            float r2 = r7.getX()     // Catch:{ JSONException -> 0x004d }
            int r2 = (int) r2     // Catch:{ JSONException -> 0x004d }
            r3.put(r0, r2)     // Catch:{ JSONException -> 0x004d }
            java.lang.String r0 = "py"
            float r7 = r7.getY()     // Catch:{ JSONException -> 0x004d }
            int r7 = (int) r7     // Catch:{ JSONException -> 0x004d }
            r3.put(r0, r7)     // Catch:{ JSONException -> 0x004d }
            goto L_0x0054
        L_0x004d:
            r7 = move-exception
            goto L_0x0051
        L_0x004f:
            r7 = move-exception
            r3 = r2
        L_0x0051:
            r7.printStackTrace()
        L_0x0054:
            com.baidu.mapsdkplatform.comapi.map.e r7 = r6.f3573g
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r7 = r7.f3532h
            if (r7 == 0) goto L_0x00ab
            com.baidu.mapsdkplatform.comapi.map.e r7 = r6.f3573g
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r7 = r7.f3532h
            java.util.Iterator r7 = r7.iterator()
        L_0x0062:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x00ab
            java.lang.Object r0 = r7.next()
            com.baidu.mapsdkplatform.comapi.map.l r0 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r0
            if (r3 == 0) goto L_0x0062
            if (r0 == 0) goto L_0x0062
            java.lang.String r2 = r3.toString()
            r0.mo11543a(r2)
            goto L_0x0062
        L_0x007a:
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3573g
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            if (r0 == 0) goto L_0x00ab
            com.baidu.mapsdkplatform.comapi.map.e r0 = r6.f3573g
            java.util.List<com.baidu.mapsdkplatform.comapi.map.l> r0 = r0.f3532h
            java.util.Iterator r0 = r0.iterator()
        L_0x0088:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00ab
            java.lang.Object r2 = r0.next()
            com.baidu.mapsdkplatform.comapi.map.l r2 = (com.baidu.mapsdkplatform.comapi.map.C1087l) r2
            if (r2 != 0) goto L_0x0097
            goto L_0x0088
        L_0x0097:
            com.baidu.mapsdkplatform.comapi.map.e r3 = r6.f3573g
            float r4 = r7.getX()
            int r4 = (int) r4
            float r5 = r7.getY()
            int r5 = (int) r5
            com.baidu.mapapi.model.inner.GeoPoint r3 = r3.mo13030b(r4, r5)
            r2.mo11541a(r3)
            goto L_0x0088
        L_0x00ab:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.mapsdkplatform.comapi.map.C1084j.onSingleTapConfirmed(android.view.MotionEvent):boolean");
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        C1078e eVar = this.f3573g;
        if (eVar == null || eVar.f3533i == null) {
            return true;
        }
        super.onTouchEvent(motionEvent);
        if (this.f3573g.f3532h != null) {
            for (C1087l lVar : this.f3573g.f3532h) {
                if (lVar != null) {
                    lVar.mo11540a(motionEvent);
                }
            }
        }
        if (this.f3572f.onTouchEvent(motionEvent)) {
            return true;
        }
        return this.f3573g.mo13028a(motionEvent);
    }

    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
        super.surfaceChanged(surfaceHolder, i, i2, i3);
        C1078e eVar = this.f3573g;
        if (eVar != null && eVar.f3533i != null) {
            MapRenderer mapRenderer = this.f3569c;
            mapRenderer.f3403a = i2;
            mapRenderer.f3404b = i3;
            this.f3570d = i2;
            this.f3571e = i3;
            mapRenderer.f3405c = 0;
            C1064ab E = this.f3573g.mo12989E();
            if (E.f3419f == 0 || E.f3419f == -1 || E.f3419f == (E.f3423j.left - E.f3423j.right) / 2) {
                E.f3419f = -1;
            }
            if (E.f3420g == 0 || E.f3420g == -1 || E.f3420g == (E.f3423j.bottom - E.f3423j.top) / 2) {
                E.f3420g = -1;
            }
            E.f3423j.left = 0;
            E.f3423j.top = 0;
            E.f3423j.bottom = i3;
            E.f3423j.right = i2;
            this.f3573g.mo13015a(E);
            this.f3573g.mo13009a(this.f3570d, this.f3571e);
        }
    }

    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        super.surfaceCreated(surfaceHolder);
        if (surfaceHolder != null && !surfaceHolder.getSurface().isValid()) {
            surfaceDestroyed(surfaceHolder);
        }
    }
}
