package com.baidu.mapapi.search.route;

public class WalkingRoutePlanOption {
    public PlanNode mFrom = null;
    public PlanNode mTo = null;

    public WalkingRoutePlanOption from(PlanNode planNode) {
        this.mFrom = planNode;
        return this;
    }

    /* renamed from: to */
    public WalkingRoutePlanOption mo12459to(PlanNode planNode) {
        this.mTo = planNode;
        return this;
    }
}
