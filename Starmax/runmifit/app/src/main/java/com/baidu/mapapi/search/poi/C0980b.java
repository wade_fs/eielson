package com.baidu.mapapi.search.poi;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.poi.b */
final class C0980b implements Parcelable.Creator<PoiDetailSearchResult> {
    C0980b() {
    }

    /* renamed from: a */
    public PoiDetailSearchResult createFromParcel(Parcel parcel) {
        return new PoiDetailSearchResult(parcel);
    }

    /* renamed from: a */
    public PoiDetailSearchResult[] newArray(int i) {
        return new PoiDetailSearchResult[i];
    }
}
