package com.baidu.location.p016d;

import com.baidu.location.p016d.C0813l;

/* JADX INFO: Failed to restore enum class, 'enum' modifier removed */
/* renamed from: com.baidu.location.d.m */
final class C0817m extends C0813l.C0816b {
    C0817m(String str, int i, String str2, String str3, String str4, int i2, int i3) {
        super(str, i, str2, str3, str4, i2, i3);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x0145  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> mo10614a(org.json.JSONObject r27, java.lang.String r28, int r29) {
        /*
            r26 = this;
            java.lang.String r0 = "dist"
            java.lang.String r1 = "ct"
            java.lang.String r2 = "ctc"
            java.lang.String r3 = "prov"
            java.lang.String r4 = "cyc"
            java.lang.String r5 = "cy"
            java.lang.String r6 = "\",\""
            java.util.Iterator r7 = r27.keys()
            java.lang.StringBuffer r8 = new java.lang.StringBuffer
            r8.<init>()
            java.lang.StringBuffer r9 = new java.lang.StringBuffer
            r9.<init>()
            java.util.ArrayList r10 = new java.util.ArrayList
            r10.<init>()
            r12 = 0
        L_0x0022:
            boolean r13 = r7.hasNext()
            java.lang.String r14 = "RGCUPDATE"
            java.lang.String r15 = "RGCAREA"
            java.lang.String r11 = "INSERT OR REPLACE INTO %s VALUES %s"
            r16 = r14
            if (r13 == 0) goto L_0x0183
            java.lang.Object r13 = r7.next()
            java.lang.String r13 = (java.lang.String) r13
            r14 = r27
            r17 = r7
            org.json.JSONObject r7 = r14.getJSONObject(r13)     // Catch:{ JSONException -> 0x0131 }
            boolean r18 = r7.has(r5)     // Catch:{ JSONException -> 0x0131 }
            r19 = 0
            if (r18 == 0) goto L_0x0051
            java.lang.String r18 = r7.getString(r5)     // Catch:{ JSONException -> 0x0131 }
            r25 = r18
            r18 = r5
            r5 = r25
            goto L_0x0055
        L_0x0051:
            r18 = r5
            r5 = r19
        L_0x0055:
            boolean r20 = r7.has(r4)     // Catch:{ JSONException -> 0x0126 }
            if (r20 == 0) goto L_0x0066
            java.lang.String r20 = r7.getString(r4)     // Catch:{ JSONException -> 0x0126 }
            r25 = r20
            r20 = r4
            r4 = r25
            goto L_0x006a
        L_0x0066:
            r20 = r4
            r4 = r19
        L_0x006a:
            boolean r21 = r7.has(r3)     // Catch:{ JSONException -> 0x011d }
            if (r21 == 0) goto L_0x007b
            java.lang.String r21 = r7.getString(r3)     // Catch:{ JSONException -> 0x011d }
            r25 = r21
            r21 = r3
            r3 = r25
            goto L_0x007f
        L_0x007b:
            r21 = r3
            r3 = r19
        L_0x007f:
            boolean r22 = r7.has(r2)     // Catch:{ JSONException -> 0x0116 }
            if (r22 == 0) goto L_0x0090
            java.lang.String r22 = r7.getString(r2)     // Catch:{ JSONException -> 0x0116 }
            r25 = r22
            r22 = r2
            r2 = r25
            goto L_0x0094
        L_0x0090:
            r22 = r2
            r2 = r19
        L_0x0094:
            boolean r23 = r7.has(r1)     // Catch:{ JSONException -> 0x0111 }
            if (r23 == 0) goto L_0x00a5
            java.lang.String r23 = r7.getString(r1)     // Catch:{ JSONException -> 0x0111 }
            r25 = r23
            r23 = r1
            r1 = r25
            goto L_0x00a9
        L_0x00a5:
            r23 = r1
            r1 = r19
        L_0x00a9:
            boolean r24 = r7.has(r0)     // Catch:{ JSONException -> 0x010e }
            if (r24 == 0) goto L_0x00b3
            java.lang.String r19 = r7.getString(r0)     // Catch:{ JSONException -> 0x010e }
        L_0x00b3:
            r7 = r19
            int r19 = r8.length()     // Catch:{ JSONException -> 0x010e }
            if (r19 <= 0) goto L_0x00c3
            r19 = r0
            java.lang.String r0 = ","
            r8.append(r0)     // Catch:{ JSONException -> 0x013d }
            goto L_0x00c5
        L_0x00c3:
            r19 = r0
        L_0x00c5:
            java.lang.String r0 = "(\""
            r8.append(r0)     // Catch:{ JSONException -> 0x013d }
            r8.append(r13)     // Catch:{ JSONException -> 0x013d }
            r8.append(r6)     // Catch:{ JSONException -> 0x013d }
            r8.append(r5)     // Catch:{ JSONException -> 0x013d }
            r8.append(r6)     // Catch:{ JSONException -> 0x013d }
            r8.append(r4)     // Catch:{ JSONException -> 0x013d }
            r8.append(r6)     // Catch:{ JSONException -> 0x013d }
            r8.append(r3)     // Catch:{ JSONException -> 0x013d }
            r8.append(r6)     // Catch:{ JSONException -> 0x013d }
            r8.append(r1)     // Catch:{ JSONException -> 0x013d }
            r8.append(r6)     // Catch:{ JSONException -> 0x013d }
            r8.append(r2)     // Catch:{ JSONException -> 0x013d }
            r8.append(r6)     // Catch:{ JSONException -> 0x013d }
            r8.append(r7)     // Catch:{ JSONException -> 0x013d }
            java.lang.String r0 = "\","
            r8.append(r0)     // Catch:{ JSONException -> 0x013d }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ JSONException -> 0x013d }
            r2 = 1000(0x3e8, double:4.94E-321)
            long r0 = r0 / r2
            r8.append(r0)     // Catch:{ JSONException -> 0x013d }
            java.lang.String r0 = ",\"\")"
            r8.append(r0)     // Catch:{ JSONException -> 0x013d }
            r0 = r28
            r1 = 0
            com.baidu.location.p016d.C0813l.C0816b.m2257b(r9, r13, r0, r1)     // Catch:{ JSONException -> 0x010c }
            goto L_0x013f
        L_0x010c:
            goto L_0x013f
        L_0x010e:
            r19 = r0
            goto L_0x013d
        L_0x0111:
            r19 = r0
            r23 = r1
            goto L_0x013d
        L_0x0116:
            r19 = r0
            r23 = r1
            r22 = r2
            goto L_0x013d
        L_0x011d:
            r19 = r0
            r23 = r1
            r22 = r2
            r21 = r3
            goto L_0x013d
        L_0x0126:
            r19 = r0
            r23 = r1
            r22 = r2
            r21 = r3
            r20 = r4
            goto L_0x013d
        L_0x0131:
            r19 = r0
            r23 = r1
            r22 = r2
            r21 = r3
            r20 = r4
            r18 = r5
        L_0x013d:
            r0 = r28
        L_0x013f:
            int r1 = r12 % 50
            r2 = 49
            if (r1 != r2) goto L_0x0170
            int r1 = r8.length()
            if (r1 <= 0) goto L_0x0170
            java.util.Locale r1 = java.util.Locale.US
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r15
            r5 = 1
            r3[r5] = r8
            java.lang.String r1 = java.lang.String.format(r1, r11, r3)
            r10.add(r1)
            java.util.Locale r1 = java.util.Locale.US
            java.lang.Object[] r2 = new java.lang.Object[r2]
            r2[r4] = r16
            r2[r5] = r9
            java.lang.String r1 = java.lang.String.format(r1, r11, r2)
            r10.add(r1)
            r8.setLength(r4)
            goto L_0x0171
        L_0x0170:
            r4 = 0
        L_0x0171:
            int r12 = r12 + 1
            r7 = r17
            r5 = r18
            r0 = r19
            r4 = r20
            r3 = r21
            r2 = r22
            r1 = r23
            goto L_0x0022
        L_0x0183:
            r4 = 0
            int r0 = r8.length()
            if (r0 <= 0) goto L_0x01ae
            java.util.Locale r0 = java.util.Locale.US
            r1 = 2
            java.lang.Object[] r2 = new java.lang.Object[r1]
            r2[r4] = r15
            r3 = 1
            r2[r3] = r8
            java.lang.String r0 = java.lang.String.format(r0, r11, r2)
            r10.add(r0)
            java.util.Locale r0 = java.util.Locale.US
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r4] = r16
            r1[r3] = r9
            java.lang.String r0 = java.lang.String.format(r0, r11, r1)
            r10.add(r0)
            r8.setLength(r4)
            goto L_0x01af
        L_0x01ae:
            r3 = 1
        L_0x01af:
            java.util.Locale r0 = java.util.Locale.US
            java.lang.Object[] r1 = new java.lang.Object[r3]
            java.lang.Integer r2 = java.lang.Integer.valueOf(r29)
            r1[r4] = r2
            java.lang.String r2 = "DELETE FROM RGCAREA WHERE gridkey NOT IN (SELECT gridkey FROM RGCAREA LIMIT %d);"
            java.lang.String r0 = java.lang.String.format(r0, r2, r1)
            r10.add(r0)
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.location.p016d.C0817m.mo10614a(org.json.JSONObject, java.lang.String, int):java.util.List");
    }
}
