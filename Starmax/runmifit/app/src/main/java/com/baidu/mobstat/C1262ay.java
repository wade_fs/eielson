package com.baidu.mobstat;

import android.text.TextUtils;
import java.security.SecureRandom;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/* renamed from: com.baidu.mobstat.ay */
public final class C1262ay {

    /* renamed from: com.baidu.mobstat.ay$b */
    public static class C1264b {
        /* renamed from: a */
        public static byte[] m4670a(int i, byte[] bArr) throws Exception {
            int i2 = i - 1;
            if (i2 < 0 || C1269bc.f4340a.length <= i2) {
                return new byte[0];
            }
            SecretKeySpec secretKeySpec = new SecretKeySpec(C1269bc.f4340a[i2].getBytes(), "AES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(1, secretKeySpec);
            return instance.doFinal(bArr);
        }

        /* renamed from: b */
        public static byte[] m4671b(int i, byte[] bArr) throws Exception {
            int i2 = i - 1;
            if (i2 < 0 || C1269bc.f4340a.length <= i2) {
                return new byte[0];
            }
            SecretKeySpec secretKeySpec = new SecretKeySpec(C1269bc.f4340a[i2].getBytes(), "AES");
            Cipher instance = Cipher.getInstance("AES/ECB/PKCS5Padding");
            instance.init(2, secretKeySpec);
            return instance.doFinal(bArr);
        }

        /* renamed from: c */
        public static String m4672c(int i, byte[] bArr) {
            try {
                return C1268bb.m4693b(m4670a(i, bArr));
            } catch (Exception unused) {
                return "";
            }
        }

        /* renamed from: d */
        public static String m4673d(int i, byte[] bArr) {
            String c = m4672c(i, bArr);
            if (TextUtils.isEmpty(c)) {
                return "";
            }
            return c + "|" + i;
        }
    }

    /* renamed from: com.baidu.mobstat.ay$a */
    public static class C1263a {
        /* renamed from: a */
        public static byte[] m4667a(byte[] bArr, byte[] bArr2, byte[] bArr3) throws Exception {
            SecretKeySpec secretKeySpec = new SecretKeySpec(bArr, "AES");
            IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr2);
            Cipher instance = Cipher.getInstance("AES/CBC/PKCS5Padding");
            instance.init(1, secretKeySpec, ivParameterSpec);
            return instance.doFinal(bArr3);
        }

        /* renamed from: a */
        public static byte[] m4666a() throws Exception {
            KeyGenerator instance = KeyGenerator.getInstance("AES");
            instance.init(128, new SecureRandom());
            return instance.generateKey().getEncoded();
        }

        /* renamed from: b */
        public static byte[] m4669b() {
            byte[] bArr = new byte[16];
            new SecureRandom().nextBytes(bArr);
            return bArr;
        }

        /* renamed from: a */
        public static String m4665a(byte[] bArr) {
            try {
                return m4668b(m4666a(), m4669b(), bArr);
            } catch (Exception unused) {
                return "";
            }
        }

        /* renamed from: b */
        public static String m4668b(byte[] bArr, byte[] bArr2, byte[] bArr3) {
            try {
                byte[] a = m4667a(bArr, bArr2, C1270bd.m4695a(bArr3));
                return C1268bb.m4693b(a) + "|" + C1275bg.m4706a(bArr) + "|" + C1275bg.m4706a(bArr2);
            } catch (Exception unused) {
                return "";
            }
        }
    }
}
