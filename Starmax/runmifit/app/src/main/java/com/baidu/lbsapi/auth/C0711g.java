package com.baidu.lbsapi.auth;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import javax.net.ssl.HttpsURLConnection;

/* renamed from: com.baidu.lbsapi.auth.g */
public class C0711g {

    /* renamed from: a */
    private Context f1087a;

    /* renamed from: b */
    private String f1088b = null;

    /* renamed from: c */
    private HashMap<String, String> f1089c = null;

    /* renamed from: d */
    private String f1090d = null;

    public C0711g(Context context) {
        this.f1087a = context;
    }

    /* renamed from: a */
    private String m1644a(Context context) {
        NetworkInfo activeNetworkInfo;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (!(connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null)) {
                if (activeNetworkInfo.isAvailable()) {
                    String extraInfo = activeNetworkInfo.getExtraInfo();
                    if (extraInfo != null) {
                        if (extraInfo.trim().toLowerCase().equals("cmwap") || extraInfo.trim().toLowerCase().equals("uniwap") || extraInfo.trim().toLowerCase().equals("3gwap") || extraInfo.trim().toLowerCase().equals("ctwap")) {
                            return extraInfo.trim().toLowerCase().equals("ctwap") ? "ctwap" : "cmwap";
                        }
                    }
                    return "wifi";
                }
            }
            return null;
        } catch (Exception e) {
            if (C0702a.f1077a) {
                e.printStackTrace();
            }
            return null;
        }
    }

    /* JADX WARN: Failed to insert an additional move for type inference into block B:117:0x0195 */
    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:104:0x0166 */
    /* JADX WARN: Failed to insert an additional move for type inference into block B:91:0x0136 */
    /* JADX WARN: Type inference failed for: r5v4, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r5v6, types: [java.io.OutputStream] */
    /* JADX WARN: Type inference failed for: r5v14 */
    /* JADX WARN: Type inference failed for: r5v15 */
    /* JADX WARN: Type inference failed for: r5v16, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r5v17, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r9v3, types: [java.io.InputStream] */
    /* JADX WARN: Type inference failed for: r5v23 */
    /* JADX WARN: Type inference failed for: r5v24 */
    /* JADX WARN: Type inference failed for: r5v28 */
    /* JADX WARN: Type inference failed for: r5v29 */
    /* JADX WARN: Type inference failed for: r5v30 */
    /* JADX WARN: Type inference failed for: r5v32 */
    /* JADX WARN: Type inference failed for: r5v33 */
    /* JADX WARN: Type inference failed for: r5v34 */
    /* JADX WARNING: Code restructure failed: missing block: B:101:0x0161, code lost:
        if (com.baidu.lbsapi.auth.C0702a.f1077a == false) goto L_0x01c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x018f, code lost:
        if (com.baidu.lbsapi.auth.C0702a.f1077a == false) goto L_0x01c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:127:0x01c0, code lost:
        if (com.baidu.lbsapi.auth.C0702a.f1077a == false) goto L_0x01c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x0102, code lost:
        if (com.baidu.lbsapi.auth.C0702a.f1077a == false) goto L_0x01c4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x0104, code lost:
        r14.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:80:0x0124, code lost:
        r14 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:81:0x0125, code lost:
        r5 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x0128, code lost:
        r14 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x0129, code lost:
        r5 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x012b, code lost:
        r14 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x012c, code lost:
        r5 = r8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:86:0x012e, code lost:
        r14 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x012f, code lost:
        r5 = r8;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x016a A[Catch:{ all -> 0x0131 }] */
    /* JADX WARNING: Removed duplicated region for block: B:110:0x0188 A[SYNTHETIC, Splitter:B:110:0x0188] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0199 A[Catch:{ all -> 0x0131 }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x01b9 A[SYNTHETIC, Splitter:B:123:0x01b9] */
    /* JADX WARNING: Removed duplicated region for block: B:129:0x01c6 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x01f6  */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0204  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x021d A[SYNTHETIC, Splitter:B:139:0x021d] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00b5 A[Catch:{ all -> 0x0109 }] */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f5 A[Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00fa A[SYNTHETIC, Splitter:B:60:0x00fa] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x0116 A[Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x0124 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:7:0x0033] */
    /* JADX WARNING: Removed duplicated region for block: B:94:0x013a A[Catch:{ all -> 0x0131 }] */
    /* JADX WARNING: Removed duplicated region for block: B:97:0x015a A[SYNTHETIC, Splitter:B:97:0x015a] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:117:0x0195=Splitter:B:117:0x0195, B:91:0x0136=Splitter:B:91:0x0136, B:104:0x0166=Splitter:B:104:0x0166} */
    /* JADX WARNING: Unknown variable types count: 4 */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1645a(javax.net.ssl.HttpsURLConnection r14) {
        /*
            r13 = this;
            java.lang.String r0 = "httpsPost failed,IOException:"
            java.lang.String r1 = "UTF-8"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "https Post start,url:"
            r2.append(r3)
            java.lang.String r3 = r13.f1088b
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.baidu.lbsapi.auth.C0702a.m1616a(r2)
            java.util.HashMap<java.lang.String, java.lang.String> r2 = r13.f1089c
            if (r2 != 0) goto L_0x0027
            java.lang.String r14 = "httpsPost request paramters is null."
            java.lang.String r14 = com.baidu.lbsapi.auth.ErrorMessage.m1596a(r14)
            r13.f1090d = r14
            return
        L_0x0027:
            r2 = 1
            r3 = 200(0xc8, float:2.8E-43)
            r4 = -1
            r5 = 0
            r6 = 0
            r7 = -11
            java.io.OutputStream r8 = r14.getOutputStream()     // Catch:{ MalformedURLException -> 0x0193, IOException -> 0x0164, Exception -> 0x0134 }
            java.io.BufferedWriter r9 = new java.io.BufferedWriter     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            java.io.OutputStreamWriter r10 = new java.io.OutputStreamWriter     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            r10.<init>(r8, r1)     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            r9.<init>(r10)     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            java.util.HashMap<java.lang.String, java.lang.String> r10 = r13.f1089c     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            java.lang.String r10 = m1646b(r10)     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            r9.write(r10)     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            java.util.HashMap<java.lang.String, java.lang.String> r10 = r13.f1089c     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            java.lang.String r10 = m1646b(r10)     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            com.baidu.lbsapi.auth.C0702a.m1616a(r10)     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            r9.flush()     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            r9.close()     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            r14.connect()     // Catch:{ MalformedURLException -> 0x012e, IOException -> 0x012b, Exception -> 0x0128, all -> 0x0124 }
            java.io.InputStream r9 = r14.getInputStream()     // Catch:{ IOException -> 0x00ae, all -> 0x00aa }
            int r10 = r14.getResponseCode()     // Catch:{ IOException -> 0x00a6, all -> 0x00a2 }
            if (r3 != r10) goto L_0x0091
            java.io.BufferedReader r11 = new java.io.BufferedReader     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            java.io.InputStreamReader r12 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r12.<init>(r9, r1)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            r11.<init>(r12)     // Catch:{ IOException -> 0x008d, all -> 0x0088 }
            java.lang.StringBuffer r1 = new java.lang.StringBuffer     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
            r1.<init>()     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
        L_0x0071:
            int r5 = r11.read()     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
            if (r5 == r4) goto L_0x007c
            char r5 = (char) r5     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
            r1.append(r5)     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
            goto L_0x0071
        L_0x007c:
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
            r13.f1090d = r1     // Catch:{ IOException -> 0x0086, all -> 0x0084 }
            r5 = r11
            goto L_0x0091
        L_0x0084:
            r1 = move-exception
            goto L_0x008a
        L_0x0086:
            r1 = move-exception
            goto L_0x008f
        L_0x0088:
            r1 = move-exception
            r11 = r5
        L_0x008a:
            r5 = r9
            goto L_0x010a
        L_0x008d:
            r1 = move-exception
            r11 = r5
        L_0x008f:
            r5 = r9
            goto L_0x00b1
        L_0x0091:
            if (r9 == 0) goto L_0x009b
            if (r5 == 0) goto L_0x009b
            r5.close()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
            r9.close()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x009b:
            if (r14 == 0) goto L_0x00a0
            r14.disconnect()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x00a0:
            r6 = 1
            goto L_0x00f8
        L_0x00a2:
            r1 = move-exception
            r11 = r5
            r5 = r9
            goto L_0x00ac
        L_0x00a6:
            r1 = move-exception
            r11 = r5
            r5 = r9
            goto L_0x00b0
        L_0x00aa:
            r1 = move-exception
            r11 = r5
        L_0x00ac:
            r10 = -1
            goto L_0x010a
        L_0x00ae:
            r1 = move-exception
            r11 = r5
        L_0x00b0:
            r10 = -1
        L_0x00b1:
            boolean r2 = com.baidu.lbsapi.auth.C0702a.f1077a     // Catch:{ all -> 0x0109 }
            if (r2 == 0) goto L_0x00d0
            r1.printStackTrace()     // Catch:{ all -> 0x0109 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0109 }
            r2.<init>()     // Catch:{ all -> 0x0109 }
            java.lang.String r9 = "httpsPost parse failed;"
            r2.append(r9)     // Catch:{ all -> 0x0109 }
            java.lang.String r9 = r1.getMessage()     // Catch:{ all -> 0x0109 }
            r2.append(r9)     // Catch:{ all -> 0x0109 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0109 }
            com.baidu.lbsapi.auth.C0702a.m1616a(r2)     // Catch:{ all -> 0x0109 }
        L_0x00d0:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0109 }
            r2.<init>()     // Catch:{ all -> 0x0109 }
            r2.append(r0)     // Catch:{ all -> 0x0109 }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x0109 }
            r2.append(r1)     // Catch:{ all -> 0x0109 }
            java.lang.String r1 = r2.toString()     // Catch:{ all -> 0x0109 }
            java.lang.String r1 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r7, r1)     // Catch:{ all -> 0x0109 }
            r13.f1090d = r1     // Catch:{ all -> 0x0109 }
            if (r5 == 0) goto L_0x00f3
            if (r11 == 0) goto L_0x00f3
            r11.close()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
            r5.close()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x00f3:
            if (r14 == 0) goto L_0x00f8
            r14.disconnect()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x00f8:
            if (r8 == 0) goto L_0x01c4
            r8.close()     // Catch:{ IOException -> 0x00ff }
            goto L_0x01c4
        L_0x00ff:
            r14 = move-exception
            boolean r0 = com.baidu.lbsapi.auth.C0702a.f1077a
            if (r0 == 0) goto L_0x01c4
        L_0x0104:
            r14.printStackTrace()
            goto L_0x01c4
        L_0x0109:
            r1 = move-exception
        L_0x010a:
            if (r5 == 0) goto L_0x0114
            if (r11 == 0) goto L_0x0114
            r11.close()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
            r5.close()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x0114:
            if (r14 == 0) goto L_0x0119
            r14.disconnect()     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x0119:
            throw r1     // Catch:{ MalformedURLException -> 0x0120, IOException -> 0x011d, Exception -> 0x011a, all -> 0x0124 }
        L_0x011a:
            r14 = move-exception
            r5 = r8
            goto L_0x0136
        L_0x011d:
            r14 = move-exception
            r5 = r8
            goto L_0x0166
        L_0x0120:
            r14 = move-exception
            r5 = r8
            goto L_0x0195
        L_0x0124:
            r14 = move-exception
            r5 = r8
            goto L_0x021b
        L_0x0128:
            r14 = move-exception
            r5 = r8
            goto L_0x0135
        L_0x012b:
            r14 = move-exception
            r5 = r8
            goto L_0x0165
        L_0x012e:
            r14 = move-exception
            r5 = r8
            goto L_0x0194
        L_0x0131:
            r14 = move-exception
            goto L_0x021b
        L_0x0134:
            r14 = move-exception
        L_0x0135:
            r10 = -1
        L_0x0136:
            boolean r0 = com.baidu.lbsapi.auth.C0702a.f1077a     // Catch:{ all -> 0x0131 }
            if (r0 == 0) goto L_0x013d
            r14.printStackTrace()     // Catch:{ all -> 0x0131 }
        L_0x013d:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r0.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r1 = "httpsPost failed,Exception:"
            r0.append(r1)     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = r14.getMessage()     // Catch:{ all -> 0x0131 }
            r0.append(r14)     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = r0.toString()     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r7, r14)     // Catch:{ all -> 0x0131 }
            r13.f1090d = r14     // Catch:{ all -> 0x0131 }
            if (r5 == 0) goto L_0x01c4
            r5.close()     // Catch:{ IOException -> 0x015e }
            goto L_0x01c4
        L_0x015e:
            r14 = move-exception
            boolean r0 = com.baidu.lbsapi.auth.C0702a.f1077a
            if (r0 == 0) goto L_0x01c4
            goto L_0x0104
        L_0x0164:
            r14 = move-exception
        L_0x0165:
            r10 = -1
        L_0x0166:
            boolean r1 = com.baidu.lbsapi.auth.C0702a.f1077a     // Catch:{ all -> 0x0131 }
            if (r1 == 0) goto L_0x016d
            r14.printStackTrace()     // Catch:{ all -> 0x0131 }
        L_0x016d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r1.<init>()     // Catch:{ all -> 0x0131 }
            r1.append(r0)     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = r14.getMessage()     // Catch:{ all -> 0x0131 }
            r1.append(r14)     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = r1.toString()     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r7, r14)     // Catch:{ all -> 0x0131 }
            r13.f1090d = r14     // Catch:{ all -> 0x0131 }
            if (r5 == 0) goto L_0x01c4
            r5.close()     // Catch:{ IOException -> 0x018c }
            goto L_0x01c4
        L_0x018c:
            r14 = move-exception
            boolean r0 = com.baidu.lbsapi.auth.C0702a.f1077a
            if (r0 == 0) goto L_0x01c4
            goto L_0x0104
        L_0x0193:
            r14 = move-exception
        L_0x0194:
            r10 = -1
        L_0x0195:
            boolean r0 = com.baidu.lbsapi.auth.C0702a.f1077a     // Catch:{ all -> 0x0131 }
            if (r0 == 0) goto L_0x019c
            r14.printStackTrace()     // Catch:{ all -> 0x0131 }
        L_0x019c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0131 }
            r0.<init>()     // Catch:{ all -> 0x0131 }
            java.lang.String r1 = "httpsPost failed,MalformedURLException:"
            r0.append(r1)     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = r14.getMessage()     // Catch:{ all -> 0x0131 }
            r0.append(r14)     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = r0.toString()     // Catch:{ all -> 0x0131 }
            java.lang.String r14 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r7, r14)     // Catch:{ all -> 0x0131 }
            r13.f1090d = r14     // Catch:{ all -> 0x0131 }
            if (r5 == 0) goto L_0x01c4
            r5.close()     // Catch:{ IOException -> 0x01bd }
            goto L_0x01c4
        L_0x01bd:
            r14 = move-exception
            boolean r0 = com.baidu.lbsapi.auth.C0702a.f1077a
            if (r0 == 0) goto L_0x01c4
            goto L_0x0104
        L_0x01c4:
            if (r6 == 0) goto L_0x01f2
            if (r3 == r10) goto L_0x01f2
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r0 = "httpsPost failed,statusCode:"
            r14.append(r0)
            r14.append(r10)
            java.lang.String r14 = r14.toString()
            com.baidu.lbsapi.auth.C0702a.m1616a(r14)
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r0)
            r14.append(r10)
            java.lang.String r14 = r14.toString()
            java.lang.String r14 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r7, r14)
            r13.f1090d = r14
            return
        L_0x01f2:
            java.lang.String r14 = r13.f1090d
            if (r14 != 0) goto L_0x0204
            java.lang.String r14 = "httpsPost failed,mResult is null"
            com.baidu.lbsapi.auth.C0702a.m1616a(r14)
            java.lang.String r14 = "httpsPost failed,internal error"
            java.lang.String r14 = com.baidu.lbsapi.auth.ErrorMessage.m1595a(r4, r14)
            r13.f1090d = r14
            return
        L_0x0204:
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            java.lang.String r0 = "httpsPost success end,parse result = "
            r14.append(r0)
            java.lang.String r0 = r13.f1090d
            r14.append(r0)
            java.lang.String r14 = r14.toString()
            com.baidu.lbsapi.auth.C0702a.m1616a(r14)
            return
        L_0x021b:
            if (r5 == 0) goto L_0x0229
            r5.close()     // Catch:{ IOException -> 0x0221 }
            goto L_0x0229
        L_0x0221:
            r0 = move-exception
            boolean r1 = com.baidu.lbsapi.auth.C0702a.f1077a
            if (r1 == 0) goto L_0x0229
            r0.printStackTrace()
        L_0x0229:
            goto L_0x022b
        L_0x022a:
            throw r14
        L_0x022b:
            goto L_0x022a
        */
        throw new UnsupportedOperationException("Method not decompiled: com.baidu.lbsapi.auth.C0711g.m1645a(javax.net.ssl.HttpsURLConnection):void");
    }

    /* renamed from: b */
    private static String m1646b(HashMap<String, String> hashMap) throws UnsupportedEncodingException {
        StringBuilder sb = new StringBuilder();
        boolean z = true;
        for (Map.Entry entry : hashMap.entrySet()) {
            if (z) {
                z = false;
            } else {
                sb.append("&");
            }
            sb.append(URLEncoder.encode((String) entry.getKey(), "UTF-8"));
            sb.append("=");
            sb.append(URLEncoder.encode((String) entry.getValue(), "UTF-8"));
        }
        return sb.toString();
    }

    /* renamed from: b */
    private HttpsURLConnection m1647b() {
        String str;
        try {
            URL url = new URL(this.f1088b);
            C0702a.m1616a("https URL: " + this.f1088b);
            String a = m1644a(this.f1087a);
            if (a != null) {
                if (!a.equals("")) {
                    C0702a.m1616a("checkNetwork = " + a);
                    HttpsURLConnection httpsURLConnection = (HttpsURLConnection) (a.equals("cmwap") ? url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.172", 80))) : a.equals("ctwap") ? url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.0.0.200", 80))) : url.openConnection());
                    httpsURLConnection.setHostnameVerifier(new C0712h(this));
                    httpsURLConnection.setDoInput(true);
                    httpsURLConnection.setDoOutput(true);
                    httpsURLConnection.setRequestMethod("POST");
                    httpsURLConnection.setConnectTimeout(50000);
                    httpsURLConnection.setReadTimeout(50000);
                    return httpsURLConnection;
                }
            }
            C0702a.m1618c("Current network is not available.");
            this.f1090d = ErrorMessage.m1595a(-10, "Current network is not available.");
            return null;
        } catch (MalformedURLException e) {
            if (C0702a.f1077a) {
                e.printStackTrace();
                C0702a.m1616a(e.getMessage());
            }
            str = "Auth server could not be parsed as a URL.";
            this.f1090d = ErrorMessage.m1595a(-11, str);
            return null;
        } catch (Exception e2) {
            if (C0702a.f1077a) {
                e2.printStackTrace();
                C0702a.m1616a(e2.getMessage());
            }
            str = "Init httpsurlconnection failed.";
            this.f1090d = ErrorMessage.m1595a(-11, str);
            return null;
        }
    }

    /* renamed from: c */
    private HashMap<String, String> m1648c(HashMap<String, String> hashMap) {
        HashMap<String, String> hashMap2 = new HashMap<>();
        for (String str : hashMap.keySet()) {
            String str2 = str.toString();
            hashMap2.put(str2, hashMap.get(str2));
        }
        return hashMap2;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String mo10209a(HashMap<String, String> hashMap) {
        this.f1089c = m1648c(hashMap);
        this.f1088b = this.f1089c.get("url");
        HttpsURLConnection b = m1647b();
        if (b == null) {
            C0702a.m1618c("syncConnect failed,httpsURLConnection is null");
        } else {
            m1645a(b);
        }
        return this.f1090d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo10210a() {
        NetworkInfo activeNetworkInfo;
        C0702a.m1616a("checkNetwork start");
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) this.f1087a.getSystemService("connectivity");
            if (connectivityManager == null || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isAvailable()) {
                return false;
            }
            C0702a.m1616a("checkNetwork end");
            return true;
        } catch (Exception e) {
            if (C0702a.f1077a) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
