package com.baidu.location.p014b;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import com.baidu.location.C0839f;
import com.baidu.location.p013a.C0750l;
import com.baidu.location.p017e.C0826e;
import com.baidu.location.p019g.C0855k;

/* renamed from: com.baidu.location.b.b */
public class C0772b {

    /* renamed from: a */
    private static C0772b f1495a;
    /* access modifiers changed from: private */

    /* renamed from: b */
    public boolean f1496b = false;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public Handler f1497c = null;

    /* renamed from: d */
    private AlarmManager f1498d = null;

    /* renamed from: e */
    private C0773a f1499e = null;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public PendingIntent f1500f = null;

    /* renamed from: g */
    private long f1501g = 0;

    /* renamed from: com.baidu.location.b.b$a */
    private class C0773a extends BroadcastReceiver {
        private C0773a() {
        }

        /* synthetic */ C0773a(C0772b bVar, C0774c cVar) {
            this();
        }

        public void onReceive(Context context, Intent intent) {
            if (C0772b.this.f1496b && intent.getAction().equals("com.baidu.location.autonotifyloc_7.8.2") && C0772b.this.f1497c != null) {
                PendingIntent unused = C0772b.this.f1500f = null;
                C0772b.this.f1497c.sendEmptyMessage(1);
            }
        }
    }

    private C0772b() {
    }

    /* renamed from: a */
    public static synchronized C0772b m1993a() {
        C0772b bVar;
        synchronized (C0772b.class) {
            if (f1495a == null) {
                f1495a = new C0772b();
            }
            bVar = f1495a;
        }
        return bVar;
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public void m1998f() {
        if (System.currentTimeMillis() - this.f1501g >= 1000) {
            PendingIntent pendingIntent = this.f1500f;
            if (pendingIntent != null) {
                this.f1498d.cancel(pendingIntent);
                this.f1500f = null;
            }
            if (this.f1500f == null) {
                this.f1500f = PendingIntent.getBroadcast(C0839f.getServiceContext(), 0, new Intent("com.baidu.location.autonotifyloc_7.8.2"), 134217728);
                this.f1498d.set(0, System.currentTimeMillis() + ((long) C0855k.f1923X), this.f1500f);
            }
            Message message = new Message();
            message.what = 22;
            if (System.currentTimeMillis() - this.f1501g >= ((long) C0855k.f1924Y)) {
                this.f1501g = System.currentTimeMillis();
                if (!C0826e.m2306a().mo10647j()) {
                    C0750l.m1883c().mo10478b(message);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: g */
    public void m1999g() {
        if (this.f1496b) {
            try {
                if (this.f1500f != null) {
                    this.f1498d.cancel(this.f1500f);
                    this.f1500f = null;
                }
                C0839f.getServiceContext().unregisterReceiver(this.f1499e);
            } catch (Exception unused) {
            }
            this.f1498d = null;
            this.f1499e = null;
            this.f1497c = null;
            this.f1496b = false;
        }
    }

    /* renamed from: b */
    public void mo10523b() {
        if (!this.f1496b && C0855k.f1923X >= 10000) {
            if (this.f1497c == null) {
                this.f1497c = new C0774c(this);
            }
            this.f1498d = (AlarmManager) C0839f.getServiceContext().getSystemService(NotificationCompat.CATEGORY_ALARM);
            this.f1499e = new C0773a(this, null);
            C0839f.getServiceContext().registerReceiver(this.f1499e, new IntentFilter("com.baidu.location.autonotifyloc_7.8.2"), "android.permission.ACCESS_FINE_LOCATION", null);
            this.f1500f = PendingIntent.getBroadcast(C0839f.getServiceContext(), 0, new Intent("com.baidu.location.autonotifyloc_7.8.2"), 134217728);
            this.f1498d.set(0, System.currentTimeMillis() + ((long) C0855k.f1923X), this.f1500f);
            this.f1496b = true;
            this.f1501g = System.currentTimeMillis();
        }
    }

    /* renamed from: c */
    public void mo10524c() {
        Handler handler;
        if (this.f1496b && (handler = this.f1497c) != null) {
            handler.sendEmptyMessage(2);
        }
    }

    /* renamed from: d */
    public void mo10525d() {
        Handler handler;
        if (this.f1496b && (handler = this.f1497c) != null) {
            handler.sendEmptyMessage(1);
        }
    }

    /* renamed from: e */
    public void mo10526e() {
        Handler handler;
        if (this.f1496b && (handler = this.f1497c) != null) {
            handler.sendEmptyMessage(1);
        }
    }
}
