package com.baidu.mapapi.map;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.ViewGroup;

/* renamed from: com.baidu.mapapi.map.q */
class C0942q extends AnimatorListenerAdapter {

    /* renamed from: a */
    final /* synthetic */ ViewGroup.LayoutParams f2864a;

    /* renamed from: b */
    final /* synthetic */ int f2865b;

    /* renamed from: c */
    final /* synthetic */ SwipeDismissTouchListener f2866c;

    C0942q(SwipeDismissTouchListener swipeDismissTouchListener, ViewGroup.LayoutParams layoutParams, int i) {
        this.f2866c = swipeDismissTouchListener;
        this.f2864a = layoutParams;
        this.f2865b = i;
    }

    public void onAnimationEnd(Animator animator) {
        this.f2866c.f2707f.onDismiss(this.f2866c.f2706e, this.f2866c.f2713l);
        this.f2866c.f2706e.setTranslationX(0.0f);
        this.f2864a.height = this.f2865b;
        this.f2866c.f2706e.setLayoutParams(this.f2864a);
    }
}
