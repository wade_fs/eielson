package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.LatLngBounds;

public final class GroundOverlayOptions extends OverlayOptions {

    /* renamed from: a */
    int f2479a;

    /* renamed from: b */
    boolean f2480b = true;

    /* renamed from: c */
    Bundle f2481c;

    /* renamed from: d */
    private BitmapDescriptor f2482d;

    /* renamed from: e */
    private LatLng f2483e;

    /* renamed from: f */
    private int f2484f;

    /* renamed from: g */
    private int f2485g;

    /* renamed from: h */
    private float f2486h = 0.5f;

    /* renamed from: i */
    private float f2487i = 0.5f;

    /* renamed from: j */
    private LatLngBounds f2488j;

    /* renamed from: k */
    private float f2489k = 1.0f;

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public Overlay mo10909a() {
        int i;
        LatLngBounds latLngBounds;
        LatLng latLng;
        int i2;
        GroundOverlay groundOverlay = new GroundOverlay();
        groundOverlay.f2662A = this.f2480b;
        groundOverlay.f2665z = this.f2479a;
        groundOverlay.f2663B = this.f2481c;
        BitmapDescriptor bitmapDescriptor = this.f2482d;
        if (bitmapDescriptor != null) {
            groundOverlay.f2471b = bitmapDescriptor;
            if (this.f2488j == null && (latLng = this.f2483e) != null) {
                int i3 = this.f2484f;
                if (i3 <= 0 || (i2 = this.f2485g) <= 0) {
                    throw new IllegalArgumentException("BDMapSDKException: when you add ground overlay, the width and height must greater than 0");
                }
                groundOverlay.f2472c = latLng;
                groundOverlay.f2475f = this.f2486h;
                groundOverlay.f2476g = this.f2487i;
                groundOverlay.f2473d = (double) i3;
                groundOverlay.f2474e = (double) i2;
                i = 2;
            } else if (this.f2483e != null || (latLngBounds = this.f2488j) == null) {
                throw new IllegalStateException("BDMapSDKException: when you add ground overlay, you must set one of position or bounds");
            } else {
                groundOverlay.f2477h = latLngBounds;
                i = 1;
            }
            groundOverlay.f2470a = i;
            groundOverlay.f2478i = this.f2489k;
            return groundOverlay;
        }
        throw new IllegalStateException("BDMapSDKException: when you add ground overlay, you must set the image");
    }

    public GroundOverlayOptions anchor(float f, float f2) {
        if (f >= 0.0f && f <= 1.0f && f2 >= 0.0f && f2 <= 1.0f) {
            this.f2486h = f;
            this.f2487i = f2;
        }
        return this;
    }

    public GroundOverlayOptions dimensions(int i) {
        this.f2484f = i;
        this.f2485g = Integer.MAX_VALUE;
        return this;
    }

    public GroundOverlayOptions dimensions(int i, int i2) {
        this.f2484f = i;
        this.f2485g = i2;
        return this;
    }

    public GroundOverlayOptions extraInfo(Bundle bundle) {
        this.f2481c = bundle;
        return this;
    }

    public float getAnchorX() {
        return this.f2486h;
    }

    public float getAnchorY() {
        return this.f2487i;
    }

    public LatLngBounds getBounds() {
        return this.f2488j;
    }

    public Bundle getExtraInfo() {
        return this.f2481c;
    }

    public int getHeight() {
        int i = this.f2485g;
        return i == Integer.MAX_VALUE ? (int) (((float) (this.f2484f * this.f2482d.f2437a.getHeight())) / ((float) this.f2482d.f2437a.getWidth())) : i;
    }

    public BitmapDescriptor getImage() {
        return this.f2482d;
    }

    public LatLng getPosition() {
        return this.f2483e;
    }

    public float getTransparency() {
        return this.f2489k;
    }

    public int getWidth() {
        return this.f2484f;
    }

    public int getZIndex() {
        return this.f2479a;
    }

    public GroundOverlayOptions image(BitmapDescriptor bitmapDescriptor) {
        if (bitmapDescriptor != null) {
            this.f2482d = bitmapDescriptor;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: image can not be null");
    }

    public boolean isVisible() {
        return this.f2480b;
    }

    public GroundOverlayOptions position(LatLng latLng) {
        if (latLng != null) {
            this.f2483e = latLng;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: position can not be null");
    }

    public GroundOverlayOptions positionFromBounds(LatLngBounds latLngBounds) {
        if (latLngBounds != null) {
            this.f2488j = latLngBounds;
            return this;
        }
        throw new IllegalArgumentException("BDMapSDKException: bounds can not be null");
    }

    public GroundOverlayOptions transparency(float f) {
        if (f <= 1.0f && f >= 0.0f) {
            this.f2489k = f;
        }
        return this;
    }

    public GroundOverlayOptions visible(boolean z) {
        this.f2480b = z;
        return this;
    }

    public GroundOverlayOptions zIndex(int i) {
        this.f2479a = i;
        return this;
    }
}
