package com.baidu.platform.core.p035e;

import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.inner.Point;
import com.baidu.mapapi.search.share.LocationShareURLOption;
import com.baidu.platform.base.C1320e;
import com.baidu.platform.domain.C1379c;
import com.baidu.platform.util.C1381a;

/* renamed from: com.baidu.platform.core.e.b */
public class C1366b extends C1320e {
    public C1366b(LocationShareURLOption locationShareURLOption) {
        m5109a(locationShareURLOption);
    }

    /* renamed from: a */
    private void m5109a(LocationShareURLOption locationShareURLOption) {
        this.f4435a.mo14094a("qt", "cs");
        Point ll2point = CoordUtil.ll2point(locationShareURLOption.mLocation);
        C1381a aVar = this.f4435a;
        aVar.mo14094a("geo", ll2point.f2890x + "|" + ll2point.f2891y);
        this.f4435a.mo14094a("t", locationShareURLOption.mName);
        this.f4435a.mo14094a("cnt", locationShareURLOption.mSnippet);
        mo14027b(false);
        mo14026a(false);
    }

    /* renamed from: a */
    public String mo14025a(C1379c cVar) {
        return cVar.mo14092q();
    }
}
