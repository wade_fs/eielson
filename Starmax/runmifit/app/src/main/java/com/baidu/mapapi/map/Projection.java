package com.baidu.mapapi.map;

import android.graphics.Point;
import android.graphics.PointF;
import com.baidu.mapapi.model.CoordUtil;
import com.baidu.mapapi.model.LatLng;
import com.baidu.mapapi.model.inner.GeoPoint;
import com.baidu.mapsdkplatform.comapi.map.C1064ab;
import com.baidu.mapsdkplatform.comapi.map.C1078e;

public final class Projection {

    /* renamed from: a */
    private C1078e f2698a;

    Projection(C1078e eVar) {
        this.f2698a = eVar;
    }

    public LatLng fromScreenLocation(Point point) {
        C1078e eVar;
        if (point == null || (eVar = this.f2698a) == null) {
            return null;
        }
        return CoordUtil.mc2ll(eVar.mo13030b(point.x, point.y));
    }

    public float metersToEquatorPixels(float f) {
        if (f <= 0.0f) {
            return 0.0f;
        }
        double d = (double) f;
        double K = this.f2698a.mo12995K();
        Double.isNaN(d);
        return (float) (d / K);
    }

    public PointF toOpenGLLocation(LatLng latLng, MapStatus mapStatus) {
        if (latLng == null || mapStatus == null) {
            return null;
        }
        GeoPoint ll2mc = CoordUtil.ll2mc(latLng);
        C1064ab abVar = mapStatus.f2538a;
        return new PointF((float) (ll2mc.getLongitudeE6() - abVar.f3417d), (float) (ll2mc.getLatitudeE6() - abVar.f3418e));
    }

    public PointF toOpenGLNormalization(LatLng latLng, MapStatus mapStatus) {
        if (latLng == null || mapStatus == null) {
            return null;
        }
        GeoPoint ll2mc = CoordUtil.ll2mc(latLng);
        C1064ab.C1065a aVar = mapStatus.f2538a.f3424k;
        double abs = (double) Math.abs(aVar.f3434b - aVar.f3433a);
        double abs2 = (double) Math.abs(aVar.f3435c - aVar.f3436d);
        double longitudeE6 = ll2mc.getLongitudeE6();
        double d = (double) aVar.f3433a;
        Double.isNaN(d);
        Double.isNaN(abs);
        double latitudeE6 = ll2mc.getLatitudeE6();
        double d2 = (double) aVar.f3436d;
        Double.isNaN(d2);
        Double.isNaN(abs2);
        return new PointF((float) ((((longitudeE6 - d) * 2.0d) / abs) - 1.0d), (float) ((((latitudeE6 - d2) * 2.0d) / abs2) - 1.0d));
    }

    public Point toScreenLocation(LatLng latLng) {
        if (latLng == null || this.f2698a == null) {
            return null;
        }
        return this.f2698a.mo13005a(CoordUtil.ll2mc(latLng));
    }
}
