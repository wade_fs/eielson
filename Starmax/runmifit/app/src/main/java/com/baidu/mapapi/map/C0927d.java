package com.baidu.mapapi.map;

import android.os.Bundle;
import com.baidu.mapsdkplatform.comapi.map.C1091o;

/* renamed from: com.baidu.mapapi.map.d */
class C0927d implements C1091o {

    /* renamed from: a */
    final /* synthetic */ BaiduMap f2838a;

    C0927d(BaiduMap baiduMap) {
        this.f2838a = baiduMap;
    }

    /* renamed from: a */
    public Bundle mo11559a(int i, int i2, int i3) {
        Tile a;
        this.f2838a.f2388I.lock();
        try {
            if (this.f2838a.f2387H != null && (a = this.f2838a.f2387H.mo11128a(i, i2, i3)) != null) {
                return a.toBundle();
            }
            this.f2838a.f2388I.unlock();
            return null;
        } finally {
            this.f2838a.f2388I.unlock();
        }
    }
}
