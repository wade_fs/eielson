package com.baidu.mapapi.search.poi;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.poi.c */
final class C0981c implements Parcelable.Creator<PoiFilter> {
    C0981c() {
    }

    /* renamed from: a */
    public PoiFilter createFromParcel(Parcel parcel) {
        return new PoiFilter(parcel);
    }

    /* renamed from: a */
    public PoiFilter[] newArray(int i) {
        return new PoiFilter[i];
    }
}
