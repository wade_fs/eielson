package com.baidu.mapapi.map;

import android.content.Context;
import android.os.Bundle;
import com.baidu.mapsdkplatform.comapi.map.C1072ai;

/* renamed from: com.baidu.mapapi.map.e */
class C0928e implements C1072ai {

    /* renamed from: a */
    final /* synthetic */ BaiduMap f2839a;

    C0928e(BaiduMap baiduMap) {
        this.f2839a = baiduMap;
    }

    /* renamed from: a */
    public Bundle mo11560a(int i, int i2, int i3, Context context) {
        Tile a;
        this.f2839a.f2389J.lock();
        try {
            if (this.f2839a.f2386G != null && (a = this.f2839a.f2386G.mo11483a(i, i2, i3)) != null) {
                return a.toBundle();
            }
            this.f2839a.f2389J.unlock();
            return null;
        } finally {
            this.f2839a.f2389J.unlock();
        }
    }
}
