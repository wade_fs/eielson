package com.baidu.location.indoor.mapversion.p020a;

import android.text.TextUtils;
import android.util.Base64;
import com.baidu.mobstat.Config;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/* renamed from: com.baidu.location.indoor.mapversion.a.d */
public class C0892d {

    /* renamed from: a */
    private HashMap<String, C0891c> f2243a;

    /* renamed from: b */
    private HashSet<C0893a> f2244b;
    /* access modifiers changed from: private */

    /* renamed from: c */
    public File f2245c;
    /* access modifiers changed from: private */

    /* renamed from: d */
    public boolean f2246d;
    /* access modifiers changed from: private */

    /* renamed from: e */
    public String f2247e;
    /* access modifiers changed from: private */

    /* renamed from: f */
    public String f2248f;

    /* renamed from: com.baidu.location.indoor.mapversion.a.d$a */
    public interface C0893a {
        /* renamed from: a */
        void mo10802a(boolean z);
    }

    /* renamed from: a */
    private void m2720a(boolean z) {
        Iterator<C0893a> it = this.f2244b.iterator();
        while (it.hasNext()) {
            it.next().mo10802a(z);
        }
        this.f2246d = false;
    }

    /* renamed from: b */
    private boolean m2723b(String str) {
        if (str != null) {
            try {
                if (!str.equalsIgnoreCase("")) {
                    JSONArray optJSONArray = new JSONObject(str).optJSONArray("item");
                    this.f2243a = new HashMap<>();
                    for (int i = 0; i < optJSONArray.length(); i++) {
                        C0891c cVar = new C0891c(optJSONArray.getJSONObject(i));
                        if (cVar.mo10804b() != null) {
                            if (cVar.mo10805c() != null) {
                                this.f2243a.put(C0891c.m2711a(cVar.mo10805c()), cVar);
                            }
                        }
                    }
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    /* renamed from: c */
    private void m2725c() {
        new Thread(new C0894e(this)).start();
    }

    /* access modifiers changed from: private */
    /* renamed from: d */
    public boolean m2726d() {
        try {
            File file = new File(this.f2245c, this.f2247e);
            long currentTimeMillis = System.currentTimeMillis();
            file.lastModified();
            if (file.exists()) {
                if (currentTimeMillis - file.lastModified() <= Config.MAX_LOG_DATA_EXSIT_TIME) {
                    StringBuilder sb = new StringBuilder();
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
                    this.f2248f = bufferedReader.readLine();
                    while (true) {
                        String readLine = bufferedReader.readLine();
                        if (readLine != null) {
                            sb.append(readLine);
                            sb.append("\n");
                        } else {
                            bufferedReader.close();
                            m2720a(m2723b(new String(Base64.decode(sb.toString(), 0))));
                            this.f2246d = false;
                            return true;
                        }
                    }
                }
            }
            this.f2246d = false;
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            this.f2246d = false;
        }
    }

    /* renamed from: a */
    public C0891c mo10809a(String str, String str2) {
        String str3;
        if (this.f2243a == null || (str3 = this.f2247e) == null || !str.equalsIgnoreCase(str3)) {
            return null;
        }
        return this.f2243a.get(C0891c.m2711a(str2));
    }

    /* renamed from: a */
    public void mo10810a(String str) {
        if (!this.f2246d) {
            this.f2246d = true;
            if (TextUtils.isEmpty(str)) {
                m2720a(false);
                return;
            }
            String str2 = this.f2247e;
            if (str2 == null || !str.equalsIgnoreCase(str2) || !mo10811a()) {
                this.f2247e = str;
                this.f2248f = null;
                if (!m2726d()) {
                    m2725c();
                    return;
                }
                return;
            }
            m2720a(true);
        }
    }

    /* renamed from: a */
    public boolean mo10811a() {
        HashMap<String, C0891c> hashMap = this.f2243a;
        return hashMap != null && hashMap.size() > 0;
    }

    /* renamed from: b */
    public String mo10812b() {
        if (!mo10811a()) {
            return "A001";
        }
        Object[] array = this.f2243a.keySet().toArray();
        return (array.length <= 0 || this.f2243a.get(array[0].toString()) == null) ? "A001" : this.f2243a.get(array[0].toString()).mo10805c();
    }
}
