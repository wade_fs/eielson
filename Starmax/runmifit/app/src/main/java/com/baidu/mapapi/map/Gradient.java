package com.baidu.mapapi.map;

import android.graphics.Color;
import java.util.HashMap;

public class Gradient {

    /* renamed from: a */
    private final int f2462a;

    /* renamed from: b */
    private final int[] f2463b;

    /* renamed from: c */
    private final float[] f2464c;

    /* renamed from: com.baidu.mapapi.map.Gradient$a */
    private class C0919a {
        /* access modifiers changed from: private */

        /* renamed from: b */
        public final int f2466b;
        /* access modifiers changed from: private */

        /* renamed from: c */
        public final int f2467c;
        /* access modifiers changed from: private */

        /* renamed from: d */
        public final float f2468d;

        private C0919a(int i, int i2, float f) {
            this.f2466b = i;
            this.f2467c = i2;
            this.f2468d = f;
        }
    }

    public Gradient(int[] iArr, float[] fArr) {
        this(iArr, fArr, 1000);
    }

    private Gradient(int[] iArr, float[] fArr, int i) {
        if (iArr == null || fArr == null) {
            throw new IllegalArgumentException("BDMapSDKException: colors and startPoints should not be null");
        } else if (iArr.length != fArr.length) {
            throw new IllegalArgumentException("BDMapSDKException: colors and startPoints should be same length");
        } else if (iArr.length != 0) {
            int i2 = 1;
            while (i2 < fArr.length) {
                if (fArr[i2] > fArr[i2 - 1]) {
                    i2++;
                } else {
                    throw new IllegalArgumentException("BDMapSDKException: startPoints should be in increasing order");
                }
            }
            this.f2462a = i;
            this.f2463b = new int[iArr.length];
            this.f2464c = new float[fArr.length];
            System.arraycopy(iArr, 0, this.f2463b, 0, iArr.length);
            System.arraycopy(fArr, 0, this.f2464c, 0, fArr.length);
        } else {
            throw new IllegalArgumentException("BDMapSDKException: No colors have been defined");
        }
    }

    /* renamed from: a */
    private static int m2870a(int i, int i2, float f) {
        int alpha = (int) ((((float) (Color.alpha(i2) - Color.alpha(i))) * f) + ((float) Color.alpha(i)));
        float[] fArr = new float[3];
        Color.RGBToHSV(Color.red(i), Color.green(i), Color.blue(i), fArr);
        float[] fArr2 = new float[3];
        Color.RGBToHSV(Color.red(i2), Color.green(i2), Color.blue(i2), fArr2);
        if (fArr[0] - fArr2[0] > 180.0f) {
            fArr2[0] = fArr2[0] + 360.0f;
        } else if (fArr2[0] - fArr[0] > 180.0f) {
            fArr[0] = fArr[0] + 360.0f;
        }
        float[] fArr3 = new float[3];
        for (int i3 = 0; i3 < 3; i3++) {
            fArr3[i3] = ((fArr2[i3] - fArr[i3]) * f) + fArr[i3];
        }
        return Color.HSVToColor(alpha, fArr3);
    }

    /* renamed from: a */
    private HashMap<Integer, C0919a> m2871a() {
        HashMap<Integer, C0919a> hashMap = new HashMap<>();
        if (this.f2464c[0] != 0.0f) {
            hashMap.put(0, new C0919a(Color.argb(0, Color.red(this.f2463b[0]), Color.green(this.f2463b[0]), Color.blue(this.f2463b[0])), this.f2463b[0], ((float) this.f2462a) * this.f2464c[0]));
        }
        for (int i = 1; i < this.f2463b.length; i++) {
            int i2 = i - 1;
            Integer valueOf = Integer.valueOf((int) (((float) this.f2462a) * this.f2464c[i2]));
            int[] iArr = this.f2463b;
            int i3 = iArr[i2];
            int i4 = iArr[i];
            float[] fArr = this.f2464c;
            hashMap.put(valueOf, new C0919a(i3, i4, (fArr[i] - fArr[i2]) * ((float) this.f2462a)));
        }
        float[] fArr2 = this.f2464c;
        if (fArr2[fArr2.length - 1] != 1.0f) {
            int length = fArr2.length - 1;
            Integer valueOf2 = Integer.valueOf((int) (((float) this.f2462a) * fArr2[length]));
            int[] iArr2 = this.f2463b;
            hashMap.put(valueOf2, new C0919a(iArr2[length], iArr2[length], ((float) this.f2462a) * (1.0f - this.f2464c[length])));
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a */
    public int[] mo11091a(double d) {
        HashMap<Integer, C0919a> a = m2871a();
        int[] iArr = new int[this.f2462a];
        C0919a aVar = a.get(0);
        int i = 0;
        for (int i2 = 0; i2 < this.f2462a; i2++) {
            if (a.containsKey(Integer.valueOf(i2))) {
                aVar = a.get(Integer.valueOf(i2));
                i = i2;
            }
            iArr[i2] = m2870a(aVar.f2466b, aVar.f2467c, ((float) (i2 - i)) / aVar.f2468d);
        }
        if (d != 1.0d) {
            for (int i3 = 0; i3 < this.f2462a; i3++) {
                int i4 = iArr[i3];
                double alpha = (double) Color.alpha(i4);
                Double.isNaN(alpha);
                iArr[i3] = Color.argb((int) (alpha * d), Color.red(i4), Color.green(i4), Color.blue(i4));
            }
        }
        return iArr;
    }
}
