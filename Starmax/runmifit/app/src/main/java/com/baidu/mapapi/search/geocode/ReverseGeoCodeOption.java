package com.baidu.mapapi.search.geocode;

import com.baidu.mapapi.model.LatLng;

public class ReverseGeoCodeOption {

    /* renamed from: a */
    private int f2973a = 10;

    /* renamed from: b */
    private int f2974b = 0;

    /* renamed from: c */
    private LatLng f2975c = null;

    /* renamed from: d */
    private int f2976d = 0;

    /* renamed from: e */
    private int f2977e = 1000;

    public int getLatestAdmin() {
        return this.f2976d;
    }

    public LatLng getLocation() {
        return this.f2975c;
    }

    public int getPageNum() {
        return this.f2974b;
    }

    public int getPageSize() {
        return this.f2973a;
    }

    public int getRadius() {
        return this.f2977e;
    }

    public ReverseGeoCodeOption location(LatLng latLng) {
        this.f2975c = latLng;
        return this;
    }

    public ReverseGeoCodeOption newVersion(int i) {
        this.f2976d = i;
        return this;
    }

    public ReverseGeoCodeOption pageNum(int i) {
        if (i < 0) {
            i = 0;
        }
        this.f2974b = i;
        return this;
    }

    public ReverseGeoCodeOption pageSize(int i) {
        if (i <= 0) {
            i = 10;
        } else if (i > 100) {
            this.f2973a = 100;
            return this;
        }
        this.f2973a = i;
        return this;
    }

    public ReverseGeoCodeOption radius(int i) {
        if (i < 0) {
            i = 0;
        } else if (i > 1000) {
            this.f2977e = 1000;
            return this;
        }
        this.f2977e = i;
        return this;
    }
}
