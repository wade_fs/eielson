package com.baidu.location.p016d;

import com.baidu.location.p013a.C0762v;
import com.baidu.location.p016d.C0800f;
import java.util.concurrent.ExecutorService;

/* renamed from: com.baidu.location.d.g */
class C0803g implements Runnable {

    /* renamed from: a */
    final /* synthetic */ C0800f.C0802a f1653a;

    C0803g(C0800f.C0802a aVar) {
        this.f1653a = aVar;
    }

    public void run() {
        ExecutorService c = C0762v.m1943a().mo10505c();
        if (c != null) {
            this.f1653a.mo10731a(c, "https://ofloc.map.baidu.com/offline_loc");
        } else {
            this.f1653a.mo10734c("https://ofloc.map.baidu.com/offline_loc");
        }
    }
}
