package com.baidu.mapapi.search.core;

import android.os.Parcel;
import android.os.Parcelable;

/* renamed from: com.baidu.mapapi.search.core.a */
final class C0957a implements Parcelable.Creator<BusInfo> {
    C0957a() {
    }

    /* renamed from: a */
    public BusInfo createFromParcel(Parcel parcel) {
        return new BusInfo(parcel);
    }

    /* renamed from: a */
    public BusInfo[] newArray(int i) {
        return new BusInfo[i];
    }
}
